<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ViAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://cdn.datatables.net/v/bs4/dt-1.11.3/b-2.1.0/b-html5-2.1.0/datatables.min.css',
        
        'https://cdn.usebootstrap.com/bootstrap/4.4.1/css/bootstrap.min.css',

        'https://adminlte.io/themes/AdminLTE/dist/css/AdminLTE.min.css',

        'https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css'
    ];
    public $js = [
        // visual inspection system libraries prerequisities
        // 'js/library/chart.js',
        'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.6.1/chart.min.js',
        
        'https://cdn.datatables.net/v/bs4/dt-1.11.3/b-2.1.0/b-html5-2.1.0/datatables.min.js',
        // 'js/library/datatable.js',
        
        'https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js',
        // 'js/library/buttonPrint.js',
        
        'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js',
        // 'js/library/pdfMake.js',
        
        'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js',
        // 'js/library/fonts.js',
        
        'https://cdn.datatables.net/buttons/2.1.0/js/buttons.colVis.min.js',
        // 'js/library/colVis.js',
        
        'https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js',
        // 'js/library/datatableSelect.js'
        
        'https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js',
        
        // 'https://cdn.usebootstrap.com/bootstrap/4.4.1/js/bootstrap.min.js',
        // 'https://cdn.usebootstrap.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js',
        // 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js',

        'https://adminlte.io/themes/AdminLTE/dist/js/adminlte.min.js',
        // 'http://localhost:8070/fro-v2-visdat_system/vendor/almasaeed2010/adminlte/bower_components/jquery/dist/jquery.min.js'

    ];
    public $depends = [
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
