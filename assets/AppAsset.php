<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/tagsinput.css',
        'css/jquery.dataTables.min.css',
        'css/datatables.min.css',
        'css/select2.min.css',
    ];
    public $js = [
        'js/datatables.js',
        'js/sweetalert.min.js',
        'js/jquery.dataTables.min.js',
        'js/tagsinput.js',
        'js/main.js',
        'js/platform.js',
        'js/select2.min.js',
        'https://code.jquery.com/jquery-3.5.1.js',
        'https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js',
        'https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap.min.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
