 SELECT spl.nomo,
    aa.snfg,
    aa.snfg_komponen,
    spl.nama_fg,
        CASE
            WHEN aa.lanjutan_split_batch > 1::numeric AND spl.status::text = 'PAUSE'::text THEN 'PLANNER'::character varying
            ELSE aa.posisi
        END AS posisi,
    spl.start,
    spl.due,
        CASE
            WHEN aa.state = 'Waiting For Next Operation'::text AND idl.snfg_komponen IS NULL AND aa.posisi::text <> 'PLANNER'::text THEN 'Waiting For Next Shift'::text
            WHEN aa.lanjutan_split_batch > 1::numeric AND spl.status::text = 'PAUSE'::text THEN 'PAUSE'::text
            ELSE aa.state
        END AS status,
    date_trunc('second'::text, aa."timestamp")::timestamp(0) without time zone AS "timestamp",
        CASE
            WHEN 'now'::text::date > spl.due THEN 'OVERDUE'::text
            ELSE 'DUE'::text
        END AS ontime,
    aa.lanjutan_split_batch,
    round(date_part('epoch'::text, spl.due::timestamp with time zone - aa."timestamp")::numeric / 3600::numeric, 1) AS delta,
    date_trunc('minute'::text, aa."timestamp")::timestamp(0) without time zone AS "time"
   FROM ( SELECT DISTINCT ON (data.snfg_komponen, data.lanjutan_split_batch) data.snfg_komponen,
            data.snfg,
            data.posisi,
            data.state,
            data."timestamp",
            data.lanjutan_split_batch
           FROM ( SELECT DISTINCT scm_planner.lanjutan_split_batch,
                    scm_planner.snfg_komponen,
                    scm_planner.snfg,
                    scm_planner.posisi,
                    scm_planner."timestamp",
                        CASE
                            WHEN scm_planner.status::text <> 'UNHOLD'::text THEN scm_planner.status::text
                            ELSE 'Waiting For Next Operation'::text
                        END AS state
                   FROM scm_planner
                  WHERE scm_planner.status::text <> 'CONTINUE'::text
                UNION ALL
                 SELECT DISTINCT penimbangan.lanjutan_split_batch,
                    penimbangan.snfg_komponen,
                    sp.snfg,
                    penimbangan.posisi,
                    penimbangan."timestamp",
                        CASE
                            WHEN penimbangan.state::text = 'STOP'::text THEN 'Waiting For Next Operation'::text
                            ELSE penimbangan.jenis_penimbangan::text
                        END AS state
                   FROM penimbangan
                     JOIN scm_planner sp ON sp.snfg_komponen::text = penimbangan.snfg_komponen::text
                  WHERE penimbangan.snfg_komponen IS NOT NULL
                UNION ALL
                 SELECT DISTINCT penimbangan.lanjutan_split_batch,
                    sp.snfg_komponen,
                    sp.snfg,
                    penimbangan.posisi,
                    penimbangan."timestamp",
                        CASE
                            WHEN penimbangan.state::text = 'STOP'::text THEN 'Waiting For Next Operation'::text
                            ELSE penimbangan.jenis_penimbangan::text
                        END AS state
                   FROM penimbangan
                     JOIN scm_planner sp ON sp.nomo::text = penimbangan.nomo::text
                  WHERE penimbangan.snfg_komponen IS NULL AND penimbangan.nomo IS NOT NULL
                UNION ALL
                 SELECT DISTINCT pengolahan.lanjutan_split_batch,
                    pengolahan.snfg_komponen,
                    sp.snfg,
                    pengolahan.posisi,
                    pengolahan."timestamp",
                        CASE
                            WHEN pengolahan.state::text = 'STOP'::text THEN 'Waiting For Next Operation'::text
                            ELSE pengolahan.jenis_olah::text
                        END AS state
                   FROM pengolahan
                     JOIN scm_planner sp ON sp.snfg_komponen::text = pengolahan.snfg_komponen::text
                  WHERE pengolahan.snfg_komponen IS NOT NULL
                UNION ALL
                 SELECT DISTINCT pengolahan.lanjutan_split_batch,
                    sp.snfg_komponen,
                    sp.snfg,
                    pengolahan.posisi,
                    pengolahan."timestamp",
                        CASE
                            WHEN pengolahan.state::text = 'STOP'::text THEN 'Waiting For Next Operation'::text
                            ELSE pengolahan.jenis_olah::text
                        END AS state
                   FROM pengolahan
                     JOIN scm_planner sp ON sp.nomo::text = pengolahan.nomo::text
                  WHERE pengolahan.snfg_komponen IS NULL AND pengolahan.nomo IS NOT NULL
                UNION ALL
                 SELECT DISTINCT qc_bulk.lanjutan_split_batch,
                    qc_bulk.snfg_komponen,
                    sp.snfg,
                    qc_bulk.posisi,
                    qc_bulk."timestamp",
                        CASE
                            WHEN qc_bulk.state::text = 'STOP'::text AND (qc_bulk.status::text = ANY (ARRAY['RELASE'::character varying::text, 'RELEASE_UNCOMFORMITY'::character varying::text])) THEN 'Waiting For Next Operation'::text
                            WHEN qc_bulk.state::text = 'STOP'::text AND (qc_bulk.status::text <> ALL (ARRAY['RELASE'::character varying::text, 'RELEASE_UNCOMFORMITY'::character varying::text])) THEN qc_bulk.status::text
                            ELSE qc_bulk.jenis_periksa::text
                        END AS state
                   FROM qc_bulk
                     JOIN scm_planner sp ON sp.snfg_komponen::text = qc_bulk.snfg_komponen::text
                  WHERE qc_bulk.snfg_komponen IS NOT NULL
                UNION ALL
                 SELECT DISTINCT qc_bulk.lanjutan_split_batch,
                    sp.snfg_komponen,
                    sp.snfg,
                    qc_bulk.posisi,
                    qc_bulk."timestamp",
                        CASE
                            WHEN qc_bulk.state::text = 'STOP'::text AND (qc_bulk.status::text = ANY (ARRAY['RELASE'::character varying::text, 'RELEASE_UNCOMFORMITY'::character varying::text])) THEN 'Waiting For Next Operation'::text
                            WHEN qc_bulk.state::text = 'STOP'::text AND (qc_bulk.status::text <> ALL (ARRAY['RELASE'::character varying::text, 'RELEASE_UNCOMFORMITY'::character varying::text])) THEN qc_bulk.status::text
                            ELSE qc_bulk.jenis_periksa::text
                        END AS state
                   FROM qc_bulk
                     JOIN scm_planner sp ON sp.nomo::text = qc_bulk.nomo::text
                  WHERE qc_bulk.snfg_komponen IS NULL AND qc_bulk.nomo IS NOT NULL
                UNION ALL
                 SELECT DISTINCT kemas_1.lanjutan_split_batch,
                    kemas_1.snfg_komponen,
                    sp.snfg,
                    kemas_1.posisi,
                    kemas_1."timestamp",
                        CASE
                            WHEN kemas_1.state::text = 'STOP'::text THEN 'Waiting For Next Operation'::text
                            ELSE kemas_1.jenis_kemas::text
                        END AS state
                   FROM kemas_1
                     JOIN scm_planner sp ON kemas_1.snfg_komponen::text = sp.snfg_komponen::text
                UNION ALL
                 SELECT DISTINCT kemas_2.lanjutan_split_batch,
                    sp.snfg_komponen,
                    kemas_2.snfg,
                    kemas_2.posisi,
                    kemas_2."timestamp",
                        CASE
                            WHEN kemas_2.state::text = 'STOP'::text AND kemas_2.batch_split IS NULL AND kemas_2.palet_flag IS NULL THEN 'Waiting For Next Operation'::text
                            WHEN kemas_2.state::text = 'STOP'::text AND kemas_2.batch_split = 1 THEN concat('Waiting For Next Operation (SB) : ', kemas_2.jumlah_realisasi)
                            WHEN kemas_2.state::text = 'STOP'::text AND kemas_2.is_done = 1 AND kemas_2.lanjutan_split_batch > 1::numeric THEN concat('Waiting For Next Operation (SB) : ', kemas_2.jumlah_realisasi)
                            ELSE kemas_2.jenis_kemas::text
                        END AS state
                   FROM kemas_2
                     JOIN scm_planner sp ON sp.snfg::text = kemas_2.snfg::text
                  WHERE kemas_2.snfg IS NOT NULL AND kemas_2.snfg_komponen IS NULL
                UNION ALL
                 SELECT DISTINCT kemas_2.lanjutan_split_batch,
                    kemas_2.snfg_komponen,
                    sp.snfg,
                    kemas_2.posisi,
                    kemas_2."timestamp",
                        CASE
                            WHEN kemas_2.state::text = 'STOP'::text AND kemas_2.batch_split IS NULL AND kemas_2.palet_flag IS NULL THEN 'Waiting For Next Operation'::text
                            WHEN kemas_2.state::text = 'STOP'::text AND kemas_2.batch_split = 1 THEN concat('Waiting For Next Operation (SB) : ', kemas_2.jumlah_realisasi)
                            WHEN kemas_2.state::text = 'STOP'::text AND kemas_2.is_done = 1 AND kemas_2.lanjutan_split_batch > 1::numeric THEN concat('Waiting For Next Operation (SB) : ', kemas_2.jumlah_realisasi)
                            ELSE kemas_2.jenis_kemas::text
                        END AS state
                   FROM kemas_2
                     JOIN scm_planner sp ON sp.snfg_komponen::text = kemas_2.snfg_komponen::text
                  WHERE kemas_2.snfg IS NULL AND kemas_2.snfg_komponen IS NOT NULL
                UNION ALL
                 SELECT DISTINCT qc_fg.lanjutan_split_batch,
                    sp.snfg_komponen,
                    qc_fg.snfg,
                    qc_fg.posisi,
                    qc_fg."timestamp",
                        CASE
                            WHEN qc_fg.state::text = 'STOP'::text AND qc_fg.status::text = 'RELEASE'::text THEN 'Waiting For Next Operation'::text
                            WHEN qc_fg.state::text = 'STOP'::text AND qc_fg.status::text <> 'RELEASE'::text THEN qc_fg.status::text
                            ELSE qc_fg.jenis_periksa::text
                        END AS state
                   FROM qc_fg
                     JOIN scm_planner sp ON sp.snfg::text = qc_fg.snfg::text
                  WHERE qc_fg.snfg IS NOT NULL AND qc_fg.snfg_komponen IS NULL
                UNION ALL
                 SELECT DISTINCT qc_fg.lanjutan_split_batch,
                    qc_fg.snfg_komponen,
                    sp.snfg,
                    qc_fg.posisi,
                    qc_fg."timestamp",
                        CASE
                            WHEN qc_fg.state::text = 'STOP'::text AND qc_fg.status::text = 'RELEASE'::text THEN 'Waiting For Next Operation'::text
                            WHEN qc_fg.state::text = 'STOP'::text AND qc_fg.status::text <> 'RELEASE'::text THEN qc_fg.status::text
                            ELSE qc_fg.jenis_periksa::text
                        END AS state
                   FROM qc_fg
                     JOIN scm_planner sp ON sp.snfg_komponen::text = qc_fg.snfg_komponen::text
                  WHERE qc_fg.snfg_komponen IS NOT NULL AND qc_fg.snfg IS NULL) data
          ORDER BY data.snfg_komponen, data.lanjutan_split_batch, data."timestamp" DESC) aa
     LEFT JOIN scm_planner spl ON aa.snfg_komponen::text = spl.snfg_komponen::text
     LEFT JOIN ( SELECT DISTINCT ON (is_done_list.snfg_komponen, is_done_list.lanjutan_split_batch) is_done_list.snfg_komponen,
            is_done_list.snfg,
            is_done_list.posisi,
            is_done_list.jenis_proses,
            is_done_list.lanjutan_split_batch,
            max(is_done_list."timestamp") AS "timestamp",
            max(is_done_list.lanjutan) AS lanjutan
           FROM is_done_list
          GROUP BY is_done_list.snfg_komponen, is_done_list.lanjutan_split_batch, is_done_list.snfg, is_done_list.posisi, is_done_list.jenis_proses
          ORDER BY is_done_list.snfg_komponen, is_done_list.lanjutan_split_batch, (max(is_done_list."timestamp")) DESC) idl ON aa.snfg_komponen::text = idl.snfg_komponen::text AND aa.posisi::text = idl.posisi::text AND idl.lanjutan_split_batch = aa.lanjutan_split_batch
  WHERE spl.npd::text = 'Y'::text;