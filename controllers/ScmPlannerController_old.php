<?php

namespace app\controllers;

use Yii;
use app\models\ScmPlanner;
use app\models\PosisiProsesRaw;
use app\models\ScmPlannerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use app\models\User;
use yii\filters\AccessControl;
/**
 * ScmPlannerController implements the CRUD actions for ScmPlanner model.
 */
class ScmPlannerController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create', 'update', 'delete','index','view'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_PLANNER
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all ScmPlanner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScmPlannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ScmPlanner model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ScmPlanner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ScmPlanner();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['create']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateAutoplan()
    {
        $model = new ScmPlanner();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['create_autoplan']);
        } else {
            return $this->render('create_autoplan', [
                'model' => $model,
            ]);
        }
    }

    public function actionImportExcel()
    {

        $inputFile = 'uploads/planner.xls';
        try{
                $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFile);

        }catch(Exception $e)
        {
            die('Error');
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for( $row = 1; $row <= $highestRow; $row++)
        {
            $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);

            if($row == 1)
            {
                continue;
            }
            $planner = new ScmPlanner();
            $planner->week = $rowData[0][0];
            $planner->sediaan = $rowData[0][1];
            $planner->streamline = $rowData[0][2];
            $planner->line_timbang = $rowData[0][3];
            $planner->line_olah_premix = $rowData[0][4];
            $planner->line_olah = $rowData[0][5];
            $planner->line_olah_2 = $rowData[0][6];
            $planner->line_adjust_olah_1 = $rowData[0][7];
            $planner->line_adjust_olah_2 = $rowData[0][8];
            $planner->line_press = $rowData[0][9];
            $planner->line_kemas_1 = $rowData[0][10];
            $planner->line_kemas_2 = $rowData[0][11];
            $planner->start = $rowData[0][12];
            $planner->due = $rowData[0][13];
            $planner->leadtime = $rowData[0][14];
            $planner->kode_jadwal = $rowData[0][15];
            $planner->nors = $rowData[0][16];
            $planner->nomo = $rowData[0][17];
            $planner->snfg = $rowData[0][18];
            $planner->snfg_komponen = $rowData[0][19];
            $planner->koitem_bulk = $rowData[0][20];
            $planner->koitem_fg = $rowData[0][21];
            $planner->nama_bulk = $rowData[0][22];
            $planner->nama_fg = $rowData[0][23];
            $planner->besar_batch = $rowData[0][24];
            $planner->besar_lot = $rowData[0][25];
            $planner->lot_ke = $rowData[0][26];
            $planner->tglpermintaan = $rowData[0][27];
            $planner->no_formula_br = $rowData[0][28];
            $planner->jumlah_press = $rowData[0][29];
            $planner->jumlah_pcs = $rowData[0][30];
            $planner->keterangan = $rowData[0][31];
            $planner->kategori_sop = $rowData[0][32];
            $planner->kategori_detail = $rowData[0][33];
            $planner->nobatch = $rowData[0][34];
            $planner->status = $rowData[0][35];
            $planner->line_terima = $rowData[0][36];
            $planner->alokasi = $rowData[0][37];
            $planner->save();

            print_r($planner->getErrors());
        }
        return $this->redirect(['create']);
        //die('okay');

    }

    public function actionDownload() {
       $path = Yii::getAlias('@webroot') . '/uploads';

       $file = $path . '/planner.xls';

       if (file_exists($file)) {

       Yii::$app->response->sendFile($file);

      }
    }

    public function actionDownloadAutoplan() {
       $path = Yii::getAlias('@webroot') . '/uploads';

       $file = $path . '/planner_master_data.xls';

       if (file_exists($file)) {

       Yii::$app->response->sendFile($file);

      }
    }

    public function actionExecute() {

      $model = new ScmPlanner();

      $file = '/srv/warehouse/rd_job/planner/denormalize_planner.sh';

      if (file_exists($file)) {

        exec('sudo /srv/warehouse/rd_job/planner/denormalize_planner.sh 2>&1', $output);
        //print_r($output);  // to see the response to your command
        return $this->render('create_autoplan', [
            'model' => $model,
        ]);

      } else {

        throw new \yii\web\NotFoundHttpException();
      }

      //  $path = Yii::getAlias('@webroot') . '/uploads';
       //
      //  $file = $path . '/planner_master_data.xls';
       //
      //  if (file_exists($file)) {
       //
      //  Yii::$app->response->sendFile($file);


    }

    public function actionUpload()

    {
        $fileName = 'file';
        $uploadPath = 'uploads';

        if (isset($_FILES[$fileName])) {
            $file = \yii\web\UploadedFile::getInstanceByName($fileName);

            //Print file data
            //print_r($file);

            if ($file->saveAs($uploadPath . '/' . $file->name)) {
                //Now save file data to database

                echo \yii\helpers\Json::encode($file);
            }
        }else{

            return $this->render('create');

        }

        return false;
    }
    /**
     * Updates an existing ScmPlanner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionGetPlannerMo($nomo)
    {
        $sql="select string_agg(snfg,',') as snfg,
                     string_agg(snfg_komponen,',') as snfg_komponen,
                     string_agg(streamline,',') as streamline,
                     string_agg(start::text,',') as start,
                     string_agg(due::text,',') as due,
                     string_agg(nama_bulk,',') as nama_bulk,
                     string_agg(nama_fg,',') as nama_fg,
                     string_agg(besar_batch::text,',') as besar_batch,
                     string_agg(kode_jadwal,',') as kode_jadwal,
                     string_agg(status,',') as status
            from scm_planner where nomo='".$nomo."'";
        $planner= ScmPlanner::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($planner);
    }

    public function actionGetPlannerMoMo($nomo)
    {
        $sql="select string_agg(snfg,'  ') as snfg,
                     string_agg(snfg_komponen,' , ') as snfg_komponen,
                     string_agg(streamline,'  ') as streamline,
                     string_agg(start::text,'  ') as start,
                     string_agg(due::text,'  ') as due,
                     string_agg(nama_bulk,'  ') as nama_bulk,
                     string_agg(nama_fg,'  ') as nama_fg,
                     string_agg(besar_batch::text,'  ') as besar_batch,
                     string_agg(kode_jadwal,'  ') as kode_jadwal
            from scm_planner where nomo='".$nomo."'";
        $planner= ScmPlanner::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($planner);
    }

    public function actionGetKomponenMo($nomo)
    {
        $sql="select *
              from scm_planner
              where nomo='".$nomo."'";
        $planners= ScmPlanner::findBySql($sql)->all();

        foreach($planners as $planner){
                echo $planner->snfg_komponen." , ";
        }
    }

    public function actionGetPlanner($snfg_komponen)
    {
        $sql="select * from scm_planner where snfg_komponen='".$snfg_komponen."'";
        $planner= ScmPlanner::findBySql($sql)->one();
        echo Json::encode($planner);
    }

    // public function actionGetPlanner($snfg_komponen)
    // {
    //     $sql="select * from scm_planner where snfg_komponen='".$snfg_komponen."'";
    //     $planner= ScmPlanner::findBySql($sql)->one();
    //     echo Json::encode($planner);

    // }

    public function actionGetPpr($snfg_komponen)
    {
        $sql="select * from posisi_proses_raw where snfg_komponen ='".$snfg_komponen."'";
        $ppr= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($ppr);

    }

    public function actionGetLastMo($nomo)
    {
        $sql="
                select distinct on (nomo)
                       lanjutan_split_batch,
                       lanjutan,
                       state,
                       posisi,
                       snfg_komponen,
                       jenis_proses
                from posisi_proses_raw
                where nomo='".$nomo."'
                order by nomo,timestamp desc
             ";
        $lastmo= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastmo);
    }

    public function actionGetLast($snfg_komponen)
    {
        $sql="
                select distinct on (snfg_komponen)
                       lanjutan_split_batch,
                       lanjutan,
                       state,
                       posisi,
                       snfg_komponen,
                       jenis_proses
                from posisi_proses_raw
                where snfg_komponen='".$snfg_komponen."'
                order by snfg_komponen,timestamp desc
            ";
        $last= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($last);
    }

    public function actionGetLastInduk($snfg)
    {
        $sql="
                select distinct on (snfg)
                       lanjutan_split_batch,
                       lanjutan,
                       state,
                       posisi,
                       snfg_komponen,
                       jenis_proses
                from posisi_proses_raw
                where snfg='".$snfg."'
                order by snfg,timestamp desc
            ";
        $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }

    // revisi ini juga jadi distinct on
    public function actionGetLastIndukSnfg($snfg,$lanjutan_split_batch)
    {
        $sql="  select  distinct on (snfg)
                        state,
                        posisi,
                        lanjutan_split_batch,
                        jenis_proses
                from posisi_proses_raw
                where snfg='".$snfg."'  and lanjutan_split_batch=coalesce('".$lanjutan_split_batch."',1)
                order by snfg,timestamp desc
             ";
        $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }

    // Ini dipakai untuk QC FG , untuk menampilkan status terakhir dari Jadwal Per SB Per Palet

    // UNUSED PER FEBRUARI 12 2017

    // public function actionGetLastProsesSnfg($snfg,$lanjutan_split_batch,$palet_ke)
    // {
    //     $sql="  select  distinct on (snfg)
    //                     state,
    //                     posisi,
    //                     lanjutan_split_batch,
    //                     jenis_proses
    //             from posisi_proses_raw
    //             where snfg='".$snfg."'  and lanjutan_split_batch=coalesce('".$lanjutan_split_batch."',1)
    //             and palet_ke=coalesce('".$palet_ke."',1)
    //             order by snfg,timestamp desc
    //          ";
    //     $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
    //     echo Json::encode($lastinduk);
    // }


    // INI yang dipakai > Feb 12 2017

    public function actionGetLastProsesSnfg($snfg,$lanjutan_split_batch)
    {
        $sql="  select  distinct on (snfg)
                        state,
                        posisi,
                        lanjutan_split_batch,
                        jenis_proses
                from posisi_proses_raw
                where snfg='".$snfg."'  and lanjutan_split_batch=coalesce('".$lanjutan_split_batch."',1)
                order by snfg,timestamp desc
             ";
        $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }

    // Ini dipakai untuk QC FG , untuk menampilkan status terakhir dari Jadwal Per SB Per Palet

    // UNUSED PER FEBRUARI 12 2017

    // public function actionGetLastProsesKomponen($snfg_komponen,$lanjutan_split_batch,$palet_ke)
    // {
    //     $sql="  select  distinct on (snfg_komponen)
    //                     state,
    //                     posisi,
    //                     lanjutan_split_batch,
    //                     jenis_proses
    //             from posisi_proses_raw
    //             where snfg_komponen='".$snfg_komponen."'  and lanjutan_split_batch=coalesce('".$lanjutan_split_batch."',1) and palet_ke=coalesce('".$palet_ke."',1)
    //             order by snfg_komponen,timestamp desc
    //          ";
    //     $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
    //     echo Json::encode($lastinduk);
    // }

    // Dipakai > Februari 12 2017

    public function actionGetLastProsesKomponen($snfg_komponen,$lanjutan_split_batch)
    {
        $sql="  select  distinct on (snfg_komponen)
                        state,
                        posisi,
                        lanjutan_split_batch,
                        jenis_proses
                from posisi_proses_raw
                where snfg_komponen='".$snfg_komponen."'  and lanjutan_split_batch=coalesce('".$lanjutan_split_batch."',1) 
                order by snfg_komponen,timestamp desc
             ";
        $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }

    // revisi ini jadi distinct on orde by timestamp desc
    public function actionGetLastIndukKomponen($snfg_komponen,$lanjutan_split_batch)
    {
        $sql="  select  distinct on (snfg_komponen)
                        state,
                        posisi,
                        lanjutan_split_batch,
                        jenis_proses
                from posisi_proses_raw
                where snfg_komponen='".$snfg_komponen."'  and lanjutan_split_batch=coalesce('".$lanjutan_split_batch."',1)
                order by snfg_komponen,timestamp desc
             ";
        $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }

    public function actionGetLastPaletSnfg($snfg,$palet_ke)
    {
        $sql="  select  distinct on (snfg)
                        state,
                        posisi,
                        palet_ke,
                        jenis_proses
                from posisi_proses_raw
                where snfg='".$snfg."'  and palet_ke=coalesce('".$palet_ke."',1)
                order by snfg,timestamp desc
             ";
        $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }


// GET LIST KENDALA DROP DOWN

    // KEMAS

        // KOMPONEN

            public function actionGetKendalaKemasKomponen($snfg_komponen)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg_komponen' => $snfg_komponen])
                                ->count();


                $sql = "
                        SELECT distinct d.kemas as keterangan
                        FROM scm_planner s
                        INNER JOIN dim_kendala d on s.sediaan=d.sediaan
                        WHERE s.snfg_komponen='".$snfg_komponen."' and d.kemas is not null
                        ORDER by d.kemas asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Kendala'>Select Kendala</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Komponen Tidak Valid</option>";
                }
            }

        // SNFG

            public function actionGetKendalaKemasSnfg($snfg)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg' => $snfg])
                                ->count();


                $sql = "
                        SELECT distinct d.kemas as keterangan
                        FROM scm_planner s
                        INNER JOIN dim_kendala d on s.sediaan=d.sediaan
                        WHERE s.snfg='".$snfg."' and d.kemas is not null
                        ORDER by d.kemas asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Kendala'>Select Kendala</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Induk Tidak Valid</option>";
                }
            }

    // PENGOLAHAN

        // KOMPONEN

            public function actionGetKendalaOlahKomponen($snfg_komponen)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg_komponen' => $snfg_komponen])
                                ->count();


                $sql = "
                        SELECT distinct d.pengolahan as keterangan
                        FROM scm_planner s
                        INNER JOIN dim_kendala d on s.sediaan=d.sediaan
                        WHERE s.snfg_komponen='".$snfg_komponen."' and d.pengolahan is not null
                        ORDER by d.pengolahan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Kendala'>Select Kendala</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Komponen Tidak Valid</option>";
                }
            }

        // NOMO

            public function actionGetKendalaOlahNomo($nomo)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['nomo' => $nomo])
                                ->count();


                $sql = "
                        SELECT distinct d.pengolahan as keterangan
                        FROM scm_planner s
                        INNER JOIN dim_kendala d on s.sediaan=d.sediaan
                        WHERE s.nomo='".$nomo."' and d.pengolahan is not null
                        ORDER by d.pengolahan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Kendala'>Select Kendala</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                    }

                }
                else{
                        echo "<option>Nomor MO tidak Valid</option>";
                }
            }

    // PENIMBANGAN

        // KOMPONEN

            public function actionGetKendalaTimbangKomponen($snfg_komponen)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg_komponen' => $snfg_komponen])
                                ->count();


                $sql = "
                        SELECT distinct d.penimbangan as keterangan
                        FROM scm_planner s
                        INNER JOIN dim_kendala d on s.sediaan=d.sediaan
                        WHERE s.snfg_komponen='".$snfg_komponen."' and d.penimbangan is not null
                        ORDER by d.penimbangan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Kendala'>Select Kendala</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Komponen Tidak Valid</option>";
                }
            }

        // NOMO

            public function actionGetKendalaTimbangNomo($nomo)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['nomo' => $nomo])
                                ->count();


                $sql = "
                        SELECT distinct d.penimbangan as keterangan
                        FROM scm_planner s
                        INNER JOIN dim_kendala d on s.sediaan=d.sediaan
                        WHERE s.nomo='".$nomo."' and d.penimbangan is not null
                        ORDER by d.penimbangan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Kendala'>Select Kendala</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                    }

                }
                else{
                        echo "<option>Nomor MO Tidak Valid</option>";
                }
            }    

// GET NAMA MESIN DROP DOWN

    // KEMAS

        // KOMPONEN

            public function actionGetLineKemasKomponen($snfg_komponen)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg_komponen' => $snfg_komponen])
                                ->count();


                $sql = "
                        SELECT distinct d.kemas as line_kemas_1
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.snfg_komponen='".$snfg_komponen."' and d.kemas is not null
                        ORDER by d.kemas asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Line'>Select Line</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_kemas_1."'>".$QcFg->line_kemas_1."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Komponen Tidak Valid</option>";
                }
            }

        // SNFG

            public function actionGetLineKemasSnfg($snfg)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg' => $snfg])
                                ->count();


                $sql = "
                        SELECT distinct d.kemas as line_kemas_1
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.snfg='".$snfg."' and d.kemas is not null
                        ORDER by d.kemas asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Line'>Select Line</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_kemas_1."'>".$QcFg->line_kemas_1."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Induk Tidak Valid</option>";
                }
            }

    // PENGOLAHAN

        // KOMPONEN

            public function actionGetLineOlahKomponen($snfg_komponen)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg_komponen' => $snfg_komponen])
                                ->count();


                $sql = "
                        SELECT distinct d.pengolahan as line_olah
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.snfg_komponen='".$snfg_komponen."' and d.pengolahan is not null
                        ORDER by d.pengolahan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Line'>Select Line</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_olah."'>".$QcFg->line_olah."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Komponen Tidak Valid</option>";
                }
            }

        // NOMO

            public function actionGetLineOlahNomo($nomo)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['nomo' => $nomo])
                                ->count();


                $sql = "
                        SELECT distinct d.pengolahan as line_olah
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.nomo='".$nomo."' and d.pengolahan is not null
                        ORDER by d.pengolahan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Line'>Select Line</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_olah."'>".$QcFg->line_olah."</options>";
                    }

                }
                else{
                        echo "<option>Nomor MO tidak Valid</option>";
                }
            }

    // PENIMBANGAN

        // KOMPONEN

            public function actionGetLineTimbangKomponen($snfg_komponen)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg_komponen' => $snfg_komponen])
                                ->count();


                $sql = "
                        SELECT distinct d.penimbangan as line_timbang
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.snfg_komponen='".$snfg_komponen."' and d.penimbangan is not null
                        ORDER by d.penimbangan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Line'>Select Line</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_timbang."'>".$QcFg->line_timbang."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Komponen Tidak Valid</option>";
                }
            }

        // NOMO

            public function actionGetLineTimbangNomo($nomo)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['nomo' => $nomo])
                                ->count();


                $sql = "
                        SELECT distinct d.penimbangan as line_timbang
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.nomo='".$nomo."' and d.penimbangan is not null
                        ORDER by d.penimbangan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Line'>Select Line</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_timbang."'>".$QcFg->line_timbang."</options>";
                    }

                }
                else{
                        echo "<option>Nomor MO Tidak Valid</option>";
                }
            }
    


    // revisi ini jadi distinct on orde by timestamp desc
    public function actionGetLastPaletKomponen($snfg_komponen,$palet_ke)
    {
        $sql="  select  distinct on (snfg_komponen)
                        state,
                        posisi,
                        palet_ke,
                        jenis_proses
                from posisi_proses_raw
                where snfg_komponen='".$snfg_komponen."'  and palet_ke=coalesce('".$palet_ke."',1)
                order by snfg_komponen,timestamp desc
             ";
        $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }


    public function actionGetPprr($snfg)
    {
        $sql="select string_agg(is_done,',') as is_done, string_agg(snfg,',') as snfg, string_agg(snfg_komponen,',') as snfg_komponen, string_agg(posisi,',') as posisi,string_agg(state,',') as state,string_agg(jenis_proses,',') as jenis_proses,string_agg(lanjutan::text,',') as lanjutan from posisi_proses_raw where snfg='".$snfg."'";
        $pprr= PosisiProsesRaw::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($pprr);

    }

    public function actionGetPp($nomo)
    {
        $sql="select string_agg(is_done,',') as is_done, string_agg(snfg,',') as snfg, string_agg(snfg_komponen,',') as snfg_komponen, string_agg(posisi,',') as posisi,string_agg(state,',') as state,string_agg(jenis_proses,',') as jenis_proses,string_agg(lanjutan::text,',') as lanjutan from posisi_proses_raw where nomo='".$nomo."'";
        $pp= PosisiProsesRaw::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($pp);

    }

    public function actionGetSnfg($snfg)
    {
        $sql="select * from scm_planner where snfg='".$snfg."'";
        $planner= ScmPlanner::findBySql($sql)->one();
        echo Json::encode($planner);

    }

    public function actionGetPlannerSnfg($snfg)
    {
        $sql="select
                     string_agg(nomo,',') as nomo,
                     string_agg(snfg_komponen,',') as snfg_komponen,
                     string_agg(streamline,',') as streamline,
                     string_agg(start::text,',') as start,
                     string_agg(due::text,',') as due,
                     string_agg(nama_bulk,',') as nama_bulk,
                     string_agg(nama_fg,',') as nama_fg,
                     string_agg(besar_batch::text,',') as besar_batch,
                     string_agg(kode_jadwal,',') as kode_jadwal,
                     string_agg(status,',') as status
            from scm_planner where snfg='".$snfg."'";
        $planner= ScmPlanner::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($planner);
    }


    /**
     * Deletes an existing ScmPlanner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ScmPlanner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScmPlanner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ScmPlanner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
