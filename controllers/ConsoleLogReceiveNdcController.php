<?php

namespace app\controllers;

use Yii;
use app\models\LogReceiveNdc;
use app\models\LogReceiveNdcSearch;
use app\models\SuratJalan;
use app\models\SuratJalanRincian;
use yii\console\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * LogReceiveNdcController implements the CRUD actions for LogReceiveNdc model.
 */
class ConsoleLogReceiveNdcController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    // Redha Script Integrate to WMS NDC Testing

    public function actionPushReceivingWmsndc(){

        // Import API Configuration File

            require_once Yii::$app->basePath.'/config/api_wmsndc.php';

        // Using Json encoder plugin

        // Get Outstanding Receiving with 'sync_wmsndc' status on 'surat_jalan' table equals to string value 'WAIT'
        
        // Parse Alokasi String, removing prefix "NDC " with split_part


        $sql = "
            SELECT 
                sj.nosj as document_number,
                split_part(sj.alokasi,' ',2) as warehouse
            FROM surat_jalan sj
            WHERE 
                sync_wmsndc ='WAIT'
            ";
        $outstanding_sj = LogReceiveNdc::findBySql($sql)->asArray()->all();

        $json_outstanding_sj = Json::encode($outstanding_sj);




        // Iterate all Outstanding and Call API to Insert

        foreach($outstanding_sj as $sj){

            // Convert to JSON Object
                $obj = Json::encode($sj);


            // Catch if there are anomalies in data

                $sql = "
                    SELECT 
                        count(*) as count
                    FROM  log_receive_ndc l 
                    WHERE l.nosj = :nosj
                    AND ( expired_date is NULL OR odoo_code is NULL )
                        
                    ";
                $outlier_items = LogReceiveNdc::findBySql($sql,[":nosj" => $sj['document_number'] ])->asArray()->one();

                // if there are outlier / anomali data then return error

                if( $outlier_items['count'] > 0){

                    print_r(json_encode(array("message"=>"ERROR for ".$sj['document_number']."! missing Items in Odoo Code / Expired Date")));

                }else{

                    // Proceed to trigger API

                    // Trigger API Call using CURL for Create Receive Document

                        $curl = curl_init();
                        // Prevent Printing Response without our command
                        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
                        curl_setopt_array($curl, array(
                          CURLOPT_URL => $api_url.'?r=api/create-ndc-receive-order',
                          CURLOPT_CUSTOMREQUEST => 'POST',
                          CURLOPT_POSTFIELDS => $obj,
                          CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/json'
                          ),
                        ));

                        $response = curl_exec($curl);
                        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                        curl_close($curl);

                    // END of Create Receive Document 

                    // If Succeed then proceed Create Receive ITEMS
                        if($statusCode==201){
                            
                            // Decode JSON Object to PHP Array
                            $res = Json::decode($response);

                            // Update Surat Jalan to 'SYNCING' which means the state is in progress for synchronizing to WMS

                            $connection = Yii::$app->getDb();
                            $command = $connection->createCommand("

                              UPDATE surat_jalan
                              SET
                                  sync_wmsndc = 'SYNCING'
                              WHERE nosj = '".$sj['document_number']."'
                              ");

                            $result = $command->execute();

                            // If Succeed then Call API to push items to WMS NDC from 'log_receive_ndc' table
                            
                            if($result){

                                // Get Details per Item

                                $sql = "
                                    SELECT 
                                        ".$res['id']." as id,
                                        l.odoo_code as default_code,
                                        l.qty,
                                        l.nobatch,
                                        l.expired_date,
                                        l.nourut as nopalet
                                    FROM surat_jalan sj
                                    LEFT JOIN log_receive_ndc l on sj.nosj = l.nosj
                                    WHERE sj.nosj = '".$sj['document_number']."'
                                    AND l.status='RECEIVED'
                                        
                                    ";
                                $receive_items = LogReceiveNdc::findBySql($sql)->asArray()->all();
                                $json_receive_items = Json::encode($receive_items);


                                // Trigger API Call using CURL for Receive Items

                                $curl2 = curl_init();
                                // Prevent Printing Response without our command
                                curl_setopt($curl2,CURLOPT_RETURNTRANSFER,1);
                                curl_setopt_array($curl2, array(
                                  CURLOPT_URL => $api_url.'?r=api/create-ndc-receive-order-item',
                                  CURLOPT_CUSTOMREQUEST => 'POST',
                                  CURLOPT_POSTFIELDS => $json_receive_items,
                                  CURLOPT_HTTPHEADER => array(
                                    'Content-Type: application/json'
                                  ),
                                ));

                                $responseItems = curl_exec($curl2);
                                $statusCodeItems = curl_getinfo($curl2, CURLINFO_HTTP_CODE);

                                curl_close($curl2);

                                // If Succeed update status on 'surat_jalan' to 'DONE' , else if Fail then revert back to Wait

                                if($statusCodeItems==201){

                                    $connection = Yii::$app->getDb();
                                    $command = $connection->createCommand("

                                      UPDATE surat_jalan
                                      SET
                                          sync_wmsndc = 'DONE'
                                      WHERE nosj = '".$sj['document_number']."'
                                      ");

                                    $result3 = $command->execute();

                                    print_r($responseItems);

                                }else{

                                    $connection = Yii::$app->getDb();
                                    $command = $connection->createCommand("

                                      UPDATE surat_jalan
                                      SET
                                          sync_wmsndc = 'WAIT'
                                      WHERE nosj = '".$sj['document_number']."'
                                      ");

                                    $result3 = $command->execute();

                                    print_r($responseItems);

                                }


                            }

                            // Else then revert the 'surat_jalan' sync_wmsndc status to 'WAIT' so it can be executed again in the next run
                            
                            else{

                                $connection = Yii::$app->getDb();
                                $command = $connection->createCommand("

                                  UPDATE surat_jalan
                                  SET
                                      sync_wmsndc = 'WAIT'
                                  WHERE nosj = '".$sj['nosj']."'
                                  ");

                                $result2 = $command->execute();

                            }
                        

                        } 

                    // END OF Create Receive Items

                } // END OF Proceed to trigger API 

            

        } // END Foreach oustanding_sj   

    }


    // END Of Redha Script


    /**
     * Finds the LogReceiveNdc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogReceiveNdc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogReceiveNdc::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
