<?php

namespace app\controllers;

use Yii;
use app\models\ScmPlanner;
use app\models\MasterDataNa;
use app\models\MasterDataSediaan;
use app\models\PosisiProsesRaw;
use app\models\ScmPlannerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use app\models\User;
use yii\filters\AccessControl;
use ripcord;
use app\models\OdooMcLog;
use app\models\ArrayOdoo;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;

/**
 * ScmPlannerController implements the CRUD actions for ScmPlanner model.
 */
class ScmPlannerController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create', 'update', 'delete','index','view'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_PLANNER
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_PLANNER
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all ScmPlanner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScmPlannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setSort([
             'defaultOrder' => [ 'id' => SORT_DESC],
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPeriksaStatusSnfg($snfg)
    {
        $sql="select
                  snfg,status
              from scm_planner
              where snfg='".$snfg."'
              order by id desc";
        $planner= ScmPlanner::findBySql($sql)->one();
        if (!empty($planner)){
            return $planner->status;
        } else {
            return 0;
        }
    }


    public function actionPdf($id) {
        // Your SQL query here

        // Fetch Data
        $model = new ScmPlanner();
        date_default_timezone_set('Asia/Jakarta');

        $sql="select
                    id,koitem_fg
              from scm_planner
              order by id desc
              limit ".$id."
              ";

          // Get Array From Database
          $myarray = ScmPlanner::findBySql($sql)->asArray()->all();
          $lastElement = end($myarray);
          $lastId = $lastElement['id'];

        // Populate Data to HTML Template
        $content_array = '';
        foreach($myarray as $value){
          $model = $this->findModel($value['id']);

          $na_number = MasterDataNa::find()->where(['koitem'=>$value['koitem_fg']])->one()['na_number'];

          $content_array .=  $this->renderPartial('report', [
              'model' => $model,
              'compare_id' => $value['id'],
              'last_id' => $lastId,
              'na_number' => $na_number,
          ]);

        }

        // Render as PDF
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => ([148,210]),
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content_array,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '
              .table {
                table-layout: auto;
                width: 100%;
                height: 20px;
              }
              .border, th, tr, td {
                border: 1px solid black;
              }
              .boxed {
                  border: 2px solid black; padding: 5px;
              }
            ',
            // set mPDF properties on the fly
            // 'options' => ['title' => 'Surat Jalan - Flowreport - '.$model->nomo],
            // call mPDF methods on the fly
            'options' => [
                // 'title' => 'Title',
                // 'showWatermarkText' => true,
                // 'showWatermarkImage' => true,
            ],
            'methods' => [
              'SetHTMLHeader'=>['


                <p style="text-align: left;font-size: 10px; font-weight: bold;">
                  <img src="../web/images/paragon.jpeg" height="3%"> PT Paragon Technology & Innovation - Surat Perintah Kerja - Flow Report
                </p>


                '],
              // 'SetWatermarkText' => ['FF.PPC.0001'],
              // 'SetHTMLFooter'=>[''],
            ]
          ]);


          /*------------------------------------*/
          Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
          $headers = Yii::$app->response->headers;
          $headers->add('Content-Type', 'application/pdf');


          return $pdf->render();

    }

    public function actionGetNamaFgOdoo($snfg)
    {
        $koitem_fg = ScmPlanner::find()->where(['snfg'=>$snfg])->one()['koitem_fg'];
        $sediaan = ScmPlanner::find()->where(['snfg'=>$snfg])->one()['sediaan'];

        if ($sediaan == 'V'){
            $connection2 = Yii::$app->db_varcos;
        }else{
            $connection2 = Yii::$app->db2;
        }
        $connection2 = Yii::$app->db2;
        $command2 = $connection2->createCommand("

            SELECT name FROM product_template
              WHERE default_code=:koitem_fg;
            ",
            [':koitem_fg'=> $koitem_fg]);

        $result2 = $command2->queryOne();

        $nama_fg = $result2['name'];
        // print_r($nama_fg);
        if (!empty($nama_fg)){
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

                UPDATE scm_planner
                SET
                  nama_fg=:nama_fg
                WHERE
                  koitem_fg=:koitem_fg;
                ",
                [':nama_fg'=> $nama_fg,
                  ':koitem_fg'=>$koitem_fg]);

            $result = $command->queryAll();
        }

        return $this->redirect(['qc-fg-v2/create-rincian','snfg'=>$snfg]);
    }

    public function actionTestAja()
    {

      $items = [
        array("id" => 1, "name" => "Cyrus", "email" => "risus@consequatdolorvitae.org"),
        array("id" => 2, "name" => "Justin", "email" => "ac.facilisis.facilisis@at.ca"),
        array("id" => 3, "name" => "Mason", "email" => "in.cursus.et@arcuacorci.ca"),
        array("id" => 4, "name" => "Fulton", "email" => "a@faucibusorciluctus.edu"),
        array("id" => 5, "name" => "Neville", "email" => "eleifend@consequatlectus.com"),
        array("id" => 6, "name" => "Jasper", "email" => "lectus.justo@miAliquam.com"),
        array("id" => 7, "name" => "Neville", "email" => "Morbi.non.sapien@dapibusquam.org"),
        array("id" => 8, "name" => "Neville", "email" => "condimentum.eget@egestas.edu"),
        array("id" => 9, "name" => "Ronan", "email" => "orci.adipiscing@interdumligulaeu.com"),
        array("id" => 10, "name" => "Raphael", "email" => "nec.tempus@commodohendrerit.co.uk"),
      ];

      print_r($items);
        // return $this->render('test-aja', [
        //     'items' => $items,
        //    ]);
    }

    public function actionRawIndex()
    {
        $searchModel = new ScmPlannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        print_r($searchModel);
        // return $this->render('index', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        // ]);
    }

    /**
     * Displays a single ScmPlanner model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ScmPlanner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new ScmPlanner();

        $searchModel = new ScmPlannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setSort([
             'defaultOrder' => [ 'id' => SORT_DESC],
        ]);

        if ($model->load(Yii::$app->request->post())){

            // Get Koitem FG
            // $koitem_fg=Yii::$app->request->post('ScmPlanner')['koitem_fg'];
            $koitem_fg=Yii::$app->request->post('koitem_fg');

              // Get Parapos
              $json = ScmPlanner::http_request("https://sfa-api.pti-cosmetics.com/product?active=eq.true&old_koitem=eq.".$koitem_fg);

              // Decode JSON & Get Odoo Code from API, if not exist then empty
              $array = json_decode($json,TRUE);
              if(!empty($array[0]['default_code'])){
                $odoo_code = $array[0]['default_code'];
              } else{
                $odoo_code = '';
              }

            // Assign Odoo Code Value
            $model->odoo_code=$odoo_code;
            // Save to DB
            $model->save();
            // Redirect to Create Page
            if (isset($_GET['obj'])) {
              return $this->redirect(['bulk-sisa/index']);
            } else {
              return $this->redirect(['create']);
            }

        } else {

            return $this->render('create', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionPrintJadwal()
    {
        $model = new ScmPlanner();

        if ($model->load(Yii::$app->request->post())) {
            $id=Yii::$app->request->post('ScmPlanner')['id'];
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['pdf', 'id' => $id ]);
        } else {
            return $this->render('print-jadwal', [
                'model' => $model,
            ]);
        }
    }

    public function actionGenerateSpk()
    {

        return $this->render('generate-spk');
    }

    public function actionPrintJadwalDateRange()
    {


        // List of Sediaan Array
        $model = new ScmPlanner();

         $sql3="
                SELECT
                    id,sediaan
                FROM master_data_sediaan
                ORDER BY id
            ";

        // Query Result to Array
        $list_sediaan = ArrayHelper::map(MasterDataSediaan::findBySql($sql3)->all(), 'sediaan','sediaan');


        // If Page POST then pass parameters to actionPdfRange
        if ($model->load(Yii::$app->request->post())) {


            // $id=Yii::$app->request->post('ScmPlanner')['id'];

            $tanggal_start=Yii::$app->request->post('ScmPlanner')['tanggal_start'];
            $tanggal_stop=Yii::$app->request->post('ScmPlanner')['tanggal_stop'];
            $sediaan=Yii::$app->request->post('ScmPlanner')['sediaan'];


            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['pdf-range', 'tgl_start' => $tanggal_start, 'tgl_stop' => $tanggal_stop, 'sediaan' => $sediaan]);
        } else {
            return $this->render('print-jadwal-date-range', [
                'model' => $model,
                'list_sediaan' => $list_sediaan,
            ]);
        }
    }

    public function actionPdfRange($tgl_start,$tgl_stop,$sediaan) {

        // Fetch Data

          $model = new ScmPlanner();

          date_default_timezone_set('Asia/Jakarta');

          if($tgl_start==$tgl_stop){
            $sql="select
                        id,koitem_fg
                  from scm_planner
                  where start='".$tgl_start."'
                  and sediaan='".$sediaan."'
                  order by id
                  ";

          }else{
            $sql="select
                        id,koitem_fg
                  from scm_planner
                  where start between '".$tgl_start."' and '".$tgl_stop."'
                  and sediaan='".$sediaan."'
                  order by id
                  ";
          }

          // Convert to Array
          $myarray = ScmPlanner::findBySql($sql)->asArray()->all();
          $lastElement = end($myarray);
          $lastId = $lastElement['id'];


        // Populate Content into HTML Format
          $content_array = '';
          foreach($myarray as $value){
            $model = $this->findModel($value['id']);

            $na_number = MasterDataNa::find()->where(['koitem'=>$value['koitem_fg']])->one()['na_number'];

            $content_array .=  $this->renderPartial('report', [
                'model' => $model,
                'compare_id' => $value['id'],
                'last_id' => $lastId,
                'na_number' => $na_number,
            ]);

           }
        // Set variable company name and image for Varcos
        if ($sediaan == 'V'){
          $image = 'varcos.png';
          $company = 'PT Varcos Citra Internasional';
        }else{
          $image = 'paragon.jpeg';
          $company = 'PT Paragon Technology & Innovation';
        }



        // Render PDF
          $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => ([148,210]),
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content_array,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '
              .table {
                table-layout: auto;
                width: 100%;
                height: 20px;
              }
              .border, th, tr, td {
                border: 1px solid black;
              }
              .boxed {
                  border: 2px solid black; padding: 5px;
              }
            ',
            // set mPDF properties on the fly
            // 'options' => ['title' => 'Surat Jalan - Flowreport - '.$model->nomo],
            // call mPDF methods on the fly
            'options' => [
                // 'title' => 'Title',
                // 'showWatermarkText' => true,
                // 'showWatermarkImage' => true,
            ],
            'methods' => [
              'SetHTMLHeader'=>['
                <p style="text-align: left;font-size: 10px; font-weight: bold;">
                <img src="../web/images/'.$image.'" height="3%"> '.$company.' - Surat Perintah Kerja - Flow Report
                </p>

                '],
              // 'SetWatermarkText' => ['FF.PPC.0001'],
              // 'SetHTMLFooter'=>[''],
            ]
          ]);


          /*------------------------------------*/
          Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
          $headers = Yii::$app->response->headers;
          $headers->add('Content-Type', 'application/pdf');


          return $pdf->render();


        // return $content->render();
    }

    public function actionScanMaterialConsumption()
    {

        // $this->layout = '//main-qr-layout';

        return $this->render('scan-material-consumption', [
        ]);
    }

    public function actionProduce($nomo)
    {
        $url = "https://odoomrp.pti-cosmetics.com";
        $db = "db_odoomrp_2";
        $username = "fro@email.com";
        $password = "123456";

        require_once('../../ripcord/ripcord.php');

        $common = ripcord::client($url.'/xmlrpc/2/common');


        $uid = $common->authenticate($db, $username, $password, array());
        $models = ripcord::client("$url/xmlrpc/2/object");

        $array = $models->execute_kw($db, $uid, $password,
              'mrp.production', 'search',
                array(array(array('name', '=', $nomo))));


          // Set MO ID (Get From MO)
        $mo_id=$array[0];

        $create = $models->execute_kw($db, $uid, $password, 'mrp.product.produce', 'create', array(array('production_id'=>$mo_id,'product_qty'=>1)));
        $create_id = $create;

      // Produce (Button) Trigger

        $do_produce = $models->execute_kw($db, $uid, $password, 'mrp.product.produce', 'do_produce', array(array($create_id)));

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionMarkDone($nomo)
    {
        $url = "https://odoomrp.pti-cosmetics.com";
        $db = "db_odoomrp_2";
        $username = "fro@email.com";
        $password = "123456";

        require_once('../../ripcord/ripcord.php');

        $common = ripcord::client($url.'/xmlrpc/2/common');


        $uid = $common->authenticate($db, $username, $password, array());
        $models = ripcord::client("$url/xmlrpc/2/object");

        $array = $models->execute_kw($db, $uid, $password,
              'mrp.production', 'search',
                array(array(array('name', '=', $nomo))));


          // Set MO ID (Get From MO)
        $mo_id=$array[0];

        $done = $models->execute_kw($db, $uid, $password, 'mrp.production', 'button_mark_done', array(array($mo_id)));

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionGetMoStatus($nomo)
    {
        $url = "https://odoomrp.pti-cosmetics.com";
        $db = "db_odoomrp_2";
        $username = "fro@email.com";
        $password = "123456";

          require_once('../../ripcord/ripcord.php');
          // require_once(Yii::$app->basePath.'/controllers/ripcord.php');


          $common = ripcord::client($url.'/xmlrpc/2/common');


          $uid = $common->authenticate($db, $username, $password, array());
          $models = ripcord::client("$url/xmlrpc/2/object");



          $nomor_mo = $nomo;
          //

          // Search and Trial Edit One of the BOM
            $bom = $models->execute_kw($db, $uid, $password,
                'mrp.production', 'search_read',
                  array(array(array('name', '=', $nomor_mo))),
                  array('fields'=>array('id', 'name', 'state')));


          // Set MO ID (Get From MO)
            print_r($bom[0]['state']);

    }


    public function actionProduceGetArrayOdoo($nomo)
    {
          $url = "https://odoomrp.pti-cosmetics.com";
          $db = "db_odoomrp_2";
          $username = "fro@email.com";
          $password = "123456";

          require_once('../../ripcord/ripcord.php');
          // require_once(Yii::$app->basePath.'/controllers/ripcord.php');


          $common = ripcord::client($url.'/xmlrpc/2/common');


          $uid = $common->authenticate($db, $username, $password, array());
          $models = ripcord::client("$url/xmlrpc/2/object");



          $nomor_mo = $nomo;
          //
            $array = $models->execute_kw($db, $uid, $password,
              'mrp.production', 'search',
                array(array(array('name', '=', $nomor_mo))));


          // Set MO ID (Get From MO)
            $mo_id=$array[0];

          // Produce Method
            $produce = $models->execute_kw($db, $uid, $password, 'mrp.production', 'open_produce_product', array(array($mo_id)));
            //  Get ID of MRP Production
            $produce_id = $produce['res_id'];


            $create = $models->execute_kw($db, $uid, $password, 'mrp.product.produce', 'create', array(array('production_id'=>$mo_id,'product_qty'=>1)));
            $create_id = $create;

          // Produce (Button) Trigger

            $do_produce = $models->execute_kw($db, $uid, $password, 'mrp.product.produce', 'do_produce', array(array($create_id)));




          // Search and Trial Edit One of the BOM
            $bom = $models->execute_kw($db, $uid, $password,
                'stock.move.line', 'search_read',
                  array(array(array('reference', '=', $nomor_mo))),
                  array('fields'=>array('id', 'reference', 'product_id','product_uom_qty','qty_done','state')));

          print_r($bom);


          // return $this->render('index-odoo', [
          //     'bom' => $bom,
          //    ]);

          //   $product_id = $bom[0]['id'];

          //   $write = $models->execute_kw($db, $uid, $password, 'stock.move.line', 'write',
          //   array(array($product_id), array('qty_done'=>25)));

          // // Mark As Done
          //    $done = $models->execute_kw($db, $uid, $password, 'mrp.production', 'button_mark_done', array(array($mo_id)));


          // print_r($write);






          // print_r($produce_id);

          // Write Produced Qty
          // $write_produced = $models->execute_kw($db, $uid, $password, 'mrp.product.produce', 'write', array(array(180),array('product_qty'=>1)));

          //  Get ID of MRP Production
          // $produce_id = $produce['res_id'];

          // print_r($write_produced);



          // $do_produce = $models->execute_kw($db, $uid, $password, 'mrp.product.produce', 'do_produce', array(array(180)));
          // print_r($do_produce);

          // print_r($produce['id']);

          // print_r($produce);

    }

    public function actionTestMcLog($nomo)
    {

          $sql="select
                    max(lanjutan) as lanjutan,
                    bom_id,
                    max(write_date) as write_date,
                    sum(qty) as qty,
                    nomor_mo
              from odoo_mc_log where nomor_mo='".$nomo."'
              GROUP BY bom_id,nomor_mo
              ";
          $mc_log_array= OdooMcLog::findBySql($sql)->all();

          print_r($mc_log_array);

    }


    public function actionTestArrayOdoo($nomo)
    {
          // Left Join Function
          function left_join($table1, $table2) {
              array_walk(
                  $table1,
                  function(&$entry, $key, $joinTable) {
                      foreach($joinTable as $joinKey => $joinValue) {
                          if ($joinValue["bom_id"] == $entry["id"]) {

                              $entry = array_merge($entry, $joinValue);

                          }


                      }
                  },
                  $table2
              );
              return $table1;
          }

          // Setup Connection

          $url = "https://odoomrp.pti-cosmetics.com";
          $db = "db_odoomrp_2";
          $username = "fro@email.com";
          $password = "123456";

          require_once('../../ripcord/ripcord.php');
          // require_once(Yii::$app->basePath.'/controllers/ripcord.php');


          $common = ripcord::client($url.'/xmlrpc/2/common');


          $uid = $common->authenticate($db, $username, $password, array());
          $models = ripcord::client("$url/xmlrpc/2/object");



          $nomor_mo = $nomo;
           $mrp_array = $models->execute_kw($db, $uid, $password,
                'mrp.production', 'search_read',
                  array(array(array('name', '=', $nomor_mo))),
                  array('fields'=>array('id', 'name', 'state','write_date')));


          // Set MO ID (Get From MO)
            $mo_state = $mrp_array[0]['state'];
            $mo_writedate = $mrp_array[0]['write_date'];


          // Search and Trial Edit One of the BOM
            $bom = $models->execute_kw($db, $uid, $password,
                'stock.move.line', 'search_read',
                  array(array(array('reference', '=', $nomor_mo))),
                  // array('fields'=>array('id', 'reference', 'product_id','product_uom_qty','qty_done','state')));
                  array('fields'=>array('id','product_id','product_uom_qty','qty_done','write_date')));
          // print_r($bom[0]['product_id'][1]);

          foreach($bom as $key => $value)
          {
            $bom[$key]['product_id'] = $bom[$key]['product_id'][1];
          };

          //  Query From Database
          $sql="select
                    max(lanjutan) as lanjutan,
                    bom_id,
                    max(write_date) as write_date,
                    sum(qty) as qty,
                    nomor_mo
              from odoo_mc_log where nomor_mo='".$nomo."'
              GROUP BY bom_id,nomor_mo
              ORDER BY bom_id
              ";

          // Get Array From Database
          $mc_log_array= OdooMcLog::findBySql($sql)->asArray()->all();


          //  Join Array from Database and Odoo
          $result = left_join($bom, $mc_log_array);

          foreach($result as $key=>$value){


            if (!isset($result[$key]['lanjutan'])){
              $result[$key]['lanjutan'] = null;
              $result[$key]['bom_id'] = null;
              $result[$key]['write_date'] = null;
              $result[$key]['qty'] = null;
              $result[$key]['nomor_mo'] = null;

            }

          }

          print_r($result);

    }

    public function actionGetArrayOdoo($nomo)
    {

          // Left Join Function
          function left_join($table1, $table2) {
              array_walk(
                  $table1,
                  function(&$entry, $key, $joinTable) {
                      foreach($joinTable as $joinKey => $joinValue) {
                          if ($joinValue["bom_id"] == $entry["id"]) {

                              $entry = array_merge($entry, $joinValue);

                          }


                      }
                  },
                  $table2
              );
              return $table1;
          }

          //  Query From Database
          $sql="select
                    max(lanjutan) as lanjutan,
                    bom_id,
                    max(write_date) as write_date,
                    sum(qty) as qty,
                    nomor_mo
              from odoo_mc_log where nomor_mo='".$nomo."'
              GROUP BY bom_id,nomor_mo
              ORDER BY bom_id
              ";

          // Get Array From Database
          $mc_log = OdooMcLog::findBySql($sql)->asArray()->all();



          $url = "https://odoomrp.pti-cosmetics.com";
          $db = "db_odoomrp_2";
          $username = "fro@email.com";
          $password = "123456";

          require_once('../../ripcord/ripcord.php');
          // require_once(Yii::$app->basePath.'/controllers/ripcord.php');


          $common = ripcord::client($url.'/xmlrpc/2/common');


          $uid = $common->authenticate($db, $username, $password, array());
          $models = ripcord::client("$url/xmlrpc/2/object");



          $nomor_mo = $nomo;
          //
          //   $array = $models->execute_kw($db, $uid, $password,
          //     'mrp.production', 'search',
          //       array(array(array('name', '=', $nomor_mo))));


          // // Set MO ID (Get From MO)
          //   $mo_id=$array[0];

          // // Produce Method
          //   $produce = $models->execute_kw($db, $uid, $password, 'mrp.production', 'open_produce_product', array(array($mo_id)));
          //   //  Get ID of MRP Production
          //   $produce_id = $produce['res_id'];


          //   $create = $models->execute_kw($db, $uid, $password, 'mrp.product.produce', 'create', array(array('production_id'=>$mo_id,'product_qty'=>1)));
          //   $create_id = $create;

          // // Produce (Button) Trigger

          //   $do_produce = $models->execute_kw($db, $uid, $password, 'mrp.product.produce', 'do_produce', array(array($create_id)));

           $mrp_array = $models->execute_kw($db, $uid, $password,
                'mrp.production', 'search_read',
                  array(array(array('name', '=', $nomor_mo))),
                  array('fields'=>array('id', 'name', 'state','write_date')));


          // Set MO ID (Get From MO)
            $mo_state = $mrp_array[0]['state'];
            $mo_writedate = $mrp_array[0]['write_date'];


          // Search and Trial Edit One of the BOM
            $bom = $models->execute_kw($db, $uid, $password,
                'stock.move.line', 'search_read',
                  array(array(array('reference', '=', $nomor_mo))),
                  // array('fields'=>array('id', 'reference', 'product_id','product_uom_qty','qty_done','state')));
                  array('fields'=>array('id','product_id','product_uom_qty','qty_done','write_date')));
          // print_r($bom[0]['product_id'][1]);

          foreach($bom as $key => $value)
          {
            $bom[$key]['product_id'] = $bom[$key]['product_id'][1];
          }


          $bom = left_join($bom, $mc_log);

          foreach($bom as $key=>$value){


            if (!isset($bom[$key]['lanjutan'])){
              $bom[$key]['lanjutan'] = null;
              $bom[$key]['bom_id'] = null;
              $bom[$key]['write_date'] = null;
              $bom[$key]['qty'] = null;
              $bom[$key]['nomor_mo'] = $nomo;

            }

          }


          // print_r($bom);

        $this->layout = '//main-sidebar-collapse';



          return $this->render('index-odoo', [
              'bom' => $bom,
              'mo_state' => $mo_state,
              'mo_writedate' => $mo_writedate,
              'nomor_mo' => $nomo,
             ]);

          //   $product_id = $bom[0]['id'];

          //   $write = $models->execute_kw($db, $uid, $password, 'stock.move.line', 'write',
          //   array(array($product_id), array('qty_done'=>25)));

          // // Mark As Done
          //    $done = $models->execute_kw($db, $uid, $password, 'mrp.production', 'button_mark_done', array(array($mo_id)));


          // print_r($write);






          // print_r($produce_id);

          // Write Produced Qty
          // $write_produced = $models->execute_kw($db, $uid, $password, 'mrp.product.produce', 'write', array(array(180),array('product_qty'=>1)));

          //  Get ID of MRP Production
          // $produce_id = $produce['res_id'];

          // print_r($write_produced);



          // $do_produce = $models->execute_kw($db, $uid, $password, 'mrp.product.produce', 'do_produce', array(array(180)));
          // print_r($do_produce);

          // print_r($produce['id']);

          // print_r($produce);

    }

    public function actionTestOdoo($nomo)
    {
          $url = "https://odoomrp.pti-cosmetics.com";
          $db = "db_odoomrp_2";
          $username = "fro@email.com";
          $password = "123456";

          require_once('../../ripcord/ripcord.php');
          // require_once(Yii::$app->basePath.'/controllers/ripcord.php');


          $common = ripcord::client($url.'/xmlrpc/2/common');


          $uid = $common->authenticate($db, $username, $password, array());
          $models = ripcord::client("$url/xmlrpc/2/object");



          $nomor_mo = $nomo;
          //
            $array = $models->execute_kw($db, $uid, $password,
              'mrp.production', 'search',
                array(array(array('name', '=', $nomor_mo))));


          // Set MO ID (Get From MO)
            $mo_id=$array[0];

          // Produce Method
            $produce = $models->execute_kw($db, $uid, $password, 'mrp.production', 'open_produce_product', array(array($mo_id)));
            //  Get ID of MRP Production
            $produce_id = $produce['res_id'];


            $create = $models->execute_kw($db, $uid, $password, 'mrp.product.produce', 'create', array(array('production_id'=>$mo_id,'product_qty'=>1)));
            $create_id = $create;

          // Produce (Button) Trigger

            $do_produce = $models->execute_kw($db, $uid, $password, 'mrp.product.produce', 'do_produce', array(array($create_id)));


          // Search and Trial Edit One of the BOM
            $bom = $models->execute_kw($db, $uid, $password,
                'stock.move.line', 'search_read',
                  array(array(array('reference', '=', $nomor_mo))),
                  array('fields'=>array('id', 'reference', 'product_id','product_uom_qty','qty_done','state')));

            $product_id = $bom[0]['id'];

            $write = $models->execute_kw($db, $uid, $password, 'stock.move.line', 'write',
            array(array($product_id), array('qty_done'=>25)));

          // Mark As Done
             $done = $models->execute_kw($db, $uid, $password, 'mrp.production', 'button_mark_done', array(array($mo_id)));


          // print_r($write);






          // print_r($produce_id);

          // Write Produced Qty
          // $write_produced = $models->execute_kw($db, $uid, $password, 'mrp.product.produce', 'write', array(array(180),array('product_qty'=>1)));

          //  Get ID of MRP Production
          // $produce_id = $produce['res_id'];

          // print_r($write_produced);



          // $do_produce = $models->execute_kw($db, $uid, $password, 'mrp.product.produce', 'do_produce', array(array(180)));
          // print_r($do_produce);

          // print_r($produce['id']);

          // print_r($produce);

    }

    public function actionCreateAutoplan()
    {
        $model = new ScmPlanner();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['create_autoplan']);
        } else {
            return $this->render('create_autoplan', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateReposisi()
    {
        $model = new ScmPlanner();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['create_reposisi']);
        } else {
            return $this->render('create_reposisi', [
                'model' => $model,
            ]);
        }
    }

    public function actionImportExcel()
    {
        $dirWeb = Yii::getAlias("@dirWeb");
        if (strpos($dirWeb,"flowreport_development")!== false){
            exec('sudo -u ptiadmin /srv/warehouse/rd_job/planner/import_csv_jadwal_testing.sh');
        }else{
            exec('sudo -u ptiadmin /srv/warehouse/rd_job/planner/import_csv_jadwal.sh');
        }
    
        // exec('sudo -u ptiadmin /srv/warehouse/rd_job/planner/import_csv_jadwal.sh');


        // $inputFile = 'uploads/planner.xls';
        // try{
        //         $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
        //         $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
        //         $objPHPExcel = $objReader->load($inputFile);

        // }catch(Exception $e)
        // {
        //     die('Error');
        // }

        // $sheet = $objPHPExcel->getSheet(0);
        // $highestRow = $sheet->getHighestRow();
        // $highestColumn = $sheet->getHighestColumn();

        // echo 'test';

        // for( $row = 1; $row <= $highestRow; $row++)
        // {
        //     $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);
        //     if($row == 1)
        //     {
        //         continue;
        //     }
        //     $planner = new ScmPlanner();
        //     $planner->week = $rowData[0][0];
        //     $planner->sediaan = $rowData[0][1];
        //     $planner->streamline = $rowData[0][2];
        //     $planner->line_timbang = $rowData[0][3];
        //     $planner->line_olah_premix = $rowData[0][4];
        //     $planner->line_olah = $rowData[0][5];
        //     $planner->line_olah_2 = $rowData[0][6];
        //     $planner->line_adjust_olah_1 = $rowData[0][7];
        //     $planner->line_adjust_olah_2 = $rowData[0][8];
        //     $planner->line_press = $rowData[0][9];
        //     $planner->line_kemas_1 = $rowData[0][10];
        //     $planner->line_kemas_2 = $rowData[0][11];
        //     $planner->start = $rowData[0][12];
        //     $planner->due = $rowData[0][13];
        //     $planner->leadtime = $rowData[0][14];
        //     $planner->kode_jadwal = $rowData[0][15];
        //     $planner->nors = $rowData[0][16];
        //     $planner->nomo = $rowData[0][17];
        //     $planner->snfg = $rowData[0][18];
        //     $planner->snfg_komponen = $rowData[0][19];
        //     $planner->koitem_bulk = $rowData[0][20];
        //     $planner->koitem_fg = $rowData[0][21];
        //     $planner->nama_bulk = $rowData[0][22];
        //     $planner->nama_fg = $rowData[0][23];
        //     $planner->besar_batch = $rowData[0][24];
        //     $planner->besar_lot = $rowData[0][25];
        //     $planner->lot_ke = $rowData[0][26];
        //     $planner->tglpermintaan = $rowData[0][27];
        //     $planner->no_formula_br = $rowData[0][28];
        //     $planner->jumlah_press = $rowData[0][29];
        //     $planner->jumlah_pcs = $rowData[0][30];
        //     $planner->keterangan = $rowData[0][31];
        //     $planner->kategori_sop = $rowData[0][32];
        //     $planner->kategori_detail = $rowData[0][33];
        //     $planner->nobatch = $rowData[0][34];
        //     $planner->status = $rowData[0][35];
        //     $planner->line_terima = $rowData[0][36];
        //     $planner->alokasi = $rowData[0][37];
        //     $planner->npd = $rowData[0][38];
        //     $planner->line_end = $rowData[0][39];
        //     $planner->save();
        //     print_r($planner->getErrors());
        // }
        return $this->redirect(['create']);


        //die('okay');

    }

    public function actionDownload() {
       $path = Yii::getAlias('@webroot') . '/uploads';

       $file = $path . '/planner1.xls';

       if (file_exists($file)) {

       Yii::$app->response->sendFile($file);

      }
    }

    public function actionDownloadAutoplan() {
       $path = Yii::getAlias('@webroot') . '/uploads';

       $file = $path . '/planner_master_data.xls';

       if (file_exists($file)) {

       Yii::$app->response->sendFile($file);

      }
    }

    public function actionDownloadKebutuhan() {
       $path = Yii::getAlias('@webroot') . '/uploads';

       $file = $path . '/kebutuhan_reposisi.xlsx';

       if (file_exists($file)) {

       Yii::$app->response->sendFile($file);

      }
    }

    public function actionDownloadPo() {
       $path = Yii::getAlias('@webroot') . '/uploads';

       $file = $path . '/po_reposisi.csv';

       if (file_exists($file)) {

       Yii::$app->response->sendFile($file);

      }
    }

    public function actionDownloadHasilReposisi() {
       $path = Yii::getAlias('@webroot') . '/uploads';

       $file = $path . '/hasil_reposisi.csv';

       if (file_exists($file)) {

       Yii::$app->response->sendFile($file);

      }
    }


    public function actionExecute() {

      $model = new ScmPlanner();

      $file = '/srv/warehouse/rd_job/planner/denormalize_planner.sh';

      if (file_exists($file)) {

        exec('sudo /srv/warehouse/rd_job/planner/denormalize_planner.sh 2>&1', $output);
        // print_r($output);  // to see the response to your command

        // exec('whoami 2>&1', $output);
        // print_r($output);

        return $this->render('create_autoplan', [
            'model' => $model,
        ]);

      } else {

        throw new \yii\web\NotFoundHttpException();
      }

      //  $path = Yii::getAlias('@webroot') . '/uploads';
       //
      //  $file = $path . '/planner_master_data.xls';
       //
      //  if (file_exists($file)) {
       //
      //  Yii::$app->response->sendFile($file);


    }

    public function actionExecuteReposisi() {

      //$model = new ScmPlanner();

            $script = '/srv/warehouse/rd_job/reposisi/main_reposisi.sh';
            $file = '/var/www/html/flowreport/web/uploads/hasil_reposisi.csv';

              if (file_exists($script)) {

                exec('sudo /srv/warehouse/rd_job/reposisi/main_reposisi.sh 2>&1', $output);


                // print_r($output);
                return (Yii::$app->response->sendFile($file));

                // return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));

              } else {

                throw new \yii\web\NotFoundHttpException();
              }

    }

    public function actionUpload()

    {
        $fileName = 'file';
        $uploadPath = 'uploads';

        if (isset($_FILES[$fileName])) {
            $file = \yii\web\UploadedFile::getInstanceByName($fileName);

            //Print file data
            //print_r($file);

            // if ($file->saveAs($uploadPath . '/' . $file->name)) {
            if ($file->saveAs($uploadPath . '/planner.xls')) {
                //Now save file data to database
                echo \yii\helpers\Json::encode($file);
            }
        }else{

            return $this->render('create');

        }

        return false;
    }



    public function actionUploadKebutuhan()

    {
        $fileName = 'file';
        $uploadPath = 'uploads';

        if (isset($_FILES[$fileName])) {
            $file = \yii\web\UploadedFile::getInstanceByName($fileName);

            //Print file data
            //print_r($file);

            if ($file->saveAs($uploadPath . '/' . $file->name)) {
                //Now save file data to database

                echo \yii\helpers\Json::encode($file);
            }
        }else{

            return $this->render('create');

        }

        return false;
    }

    public function actionUploadPo()

    {
        $fileName = 'file';
        $uploadPath = 'uploads';

        if (isset($_FILES[$fileName])) {
            $file = \yii\web\UploadedFile::getInstanceByName($fileName);

            //Print file data
            //print_r($file);

            if ($file->saveAs($uploadPath . '/' . $file->name)) {
                //Now save file data to database

                echo \yii\helpers\Json::encode($file);
            }
        }else{

            return $this->render('create');

        }

        return false;
    }

    /**
     * Updates an existing ScmPlanner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateData($id,$nomo)
    {
        // Left Join Function
          function left_join($table1, $table2) {
              array_walk(
                  $table1,
                  function(&$entry, $key, $joinTable) {
                      foreach($joinTable as $joinKey => $joinValue) {
                          if ($joinValue["bom_id"] == $entry["id"]) {

                              $entry = array_merge($entry, $joinValue);

                          }


                      }
                  },
                  $table2
              );
              return $table1;
          }

          //  Query From Database
          $sql="select
                    max(lanjutan) as lanjutan,
                    bom_id,
                    max(write_date) as write_date,
                    sum(qty) as qty,
                    nomor_mo
              from odoo_mc_log where bom_id=".$id."
              GROUP BY bom_id,nomor_mo
              ORDER BY bom_id
              ";

          // Get Array From Database
          $mc_log = OdooMcLog::findBySql($sql)->asArray()->all();



         // $model = $this->findModel($id);
         // print_r($model);
          $url = "https://odoomrp.pti-cosmetics.com";
          $db = "db_odoomrp_2";
          $username = "fro@email.com";
          $password = "123456";

          require_once('../../ripcord/ripcord.php');
          // require_once(Yii::$app->basePath.'/controllers/ripcord.php');




          $common = ripcord::client($url.'/xmlrpc/2/common');


          $uid = $common->authenticate($db, $username, $password, array());
          $models = ripcord::client("$url/xmlrpc/2/object");

          $bom = $models->execute_kw($db, $uid, $password,
                'stock.move.line', 'search_read',
                  array(array(array('id', '=', $id))),
                  array('fields'=>array('id', 'reference', 'product_id','product_uom_qty','qty_done','state')));

          foreach($bom as $key => $value)
          {
            $bom[$key]['product_id'] = $bom[$key]['product_id'][1];
          }


          $bom = left_join($bom, $mc_log);

          foreach($bom as $key=>$value){


            if (!isset($bom[$key]['lanjutan'])){
              $bom[$key]['lanjutan'] = null;
              $bom[$key]['bom_id'] = null;
              $bom[$key]['write_date'] = null;
              $bom[$key]['qty'] = null;
              $bom[$key]['nomor_mo'] = null;

            }

          }


         // print_r($bom);

        if (Yii::$app->request->post()) {


          // echo $_POST["lanjutan"];

          if(empty($_POST["lanjutan"])){
            $lanjutan=1;
          }else{
            $lanjutan=$_POST["lanjutan"]+1;
          }

          // echo $lanjutan;


          $connection2 = Yii::$app->getDb();
          $command2 = $connection2->createCommand("

            INSERT INTO odoo_mc_log (bom_id,lanjutan,qty,nomor_mo)
            VALUES (
                :id,
                :lanjutan,
                :qty,
                :nomor_mo
            );
            ",
            [':id' => (int)$id,':lanjutan'=>$lanjutan,':qty'=>(float)$_POST['actual_qty'],':nomor_mo'=>$nomo]);

          $result2 = $command2->queryAll();


            // $write = $models->execute_kw($db, $uid, $password, 'stock.move.line', 'write',
            // array(array($id), array('qty_done'=>$_POST["actual_qty"])));

            // // echo $_POST["actual_qty"];
            // return $this->redirect(Yii::$app->request->referrer);

          // if($_POST['lanjutan']){

          // }


          $write = $models->execute_kw($db, $uid, $password, 'stock.move.line', 'write',
          array(array((int)$id), array('qty_done'=>(float)$_POST['actual_qty']+(float)$_POST['qty'])));



          return $this->redirect(Yii::$app->request->referrer);


        }else{
            return $this->renderAjax('_update-data', [
                    'bom' => $bom,
                ]);
        }

        // if ($bom->load(Yii::$app->request->post())) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }else{
        //     // return $this->renderAjax('_update-data', [
        //     //         'bom' => $bom,
        //     //     ]);
        //     print_r($bom);
        // }

        // return $this->renderAjax('_update-data', [
        //         // 'model' => $model,
        //         'id' => $id,
        //     ]);


        // $model = $this->findModel($id);

        // if ($model->load(Yii::$app->request->post())) {
        //     if($model->save())
        //     {
        //         return $this->redirect(Yii::$app->request->referrer);
        //     }else{
        //         return $this->redirect(Yii::$app->request->referrer);
        //     }
        // } else {
        //     return $this->renderAjax('update-data', [
        //         // 'model' => $model,

        //     ]);
        // }
    }

    public function actionGetPlannerMo($nomo)
    {
        $sql="select string_agg(snfg,',') as snfg,
                     string_agg(snfg_komponen,',') as snfg_komponen,
                     string_agg(streamline,',') as streamline,
                     string_agg(start::text,',') as start,
                     string_agg(due::text,',') as due,
                     string_agg(nama_bulk,',') as nama_bulk,
                     string_agg(nama_fg,',') as nama_fg,
                     string_agg(besar_batch::text,',') as besar_batch,
                     string_agg(kode_jadwal,',') as kode_jadwal,
                     string_agg(status,',') as status
            from scm_planner where nomo='".$nomo."'";
        $planner= ScmPlanner::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($planner);
    }

    public function actionGetPlannerMoMo($nomo)
    {
        $sql="select string_agg(snfg,'  ') as snfg,
                     string_agg(snfg_komponen,' , ') as snfg_komponen,
                     string_agg(streamline,'  ') as streamline,
                     string_agg(start::text,'  ') as start,
                     string_agg(due::text,'  ') as due,
                     string_agg(nama_bulk,'  ') as nama_bulk,
                     string_agg(nama_fg,'  ') as nama_fg,
                     string_agg(besar_batch::text,'  ') as besar_batch,
                     string_agg(kode_jadwal,'  ') as kode_jadwal
            from scm_planner where nomo='".$nomo."'";
        $planner= ScmPlanner::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($planner);
    }

    public function actionGetKomponenMo($nomo)
    {
        $sql="select *
              from scm_planner
              where nomo='".$nomo."'";
        $planners= ScmPlanner::findBySql($sql)->all();

        foreach($planners as $planner){
                echo $planner->snfg_komponen." , ";
        }
    }

    public function actionGetPlanner($snfg_komponen)
    {
        $sql="select * from scm_planner where snfg_komponen='".$snfg_komponen."'";
        $planner= ScmPlanner::findBySql($sql)->one();
        echo Json::encode($planner);
    }

    public function actionGetPlannerRaw($snfg_komponen)
    {
        $sql="select * from scm_planner where snfg_komponen='".$snfg_komponen."'";
        $planner= ScmPlanner::findBySql($sql)->all();
        echo Json::encode($planner);
    }

    // public function actionGetPlanner($snfg_komponen)
    // {
    //     $sql="select * from scm_planner where snfg_komponen='".$snfg_komponen."'";
    //     $planner= ScmPlanner::findBySql($sql)->one();
    //     echo Json::encode($planner);

    // }

    public function actionGetPpr($snfg_komponen)
    {
        $sql="select * from posisi_proses_raw where snfg_komponen ='".$snfg_komponen."'";
        $ppr= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($ppr);

    }

    public function actionGetPprRaw($snfg_komponen)
    {
        $sql="select * from posisi_proses_raw where snfg_komponen ='".$snfg_komponen."'";
        $ppr= PosisiProsesRaw::findBySql($sql)->all();
        echo Json::encode($ppr);

    }

    public function actionGetLastMo($nomo)
    {
        $sql="
                select distinct on (nomo)
                       lanjutan_split_batch,
                       lanjutan,
                       state,
                       posisi,
                       snfg_komponen,
                       jenis_proses
                from posisi_proses_raw
                where nomo='".$nomo."'
                order by nomo,timestamp desc
             ";
        $lastmo= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastmo);
    }

    public function actionGetLast($snfg_komponen)
    {
        $sql="
                select distinct on (snfg_komponen)
                       lanjutan_split_batch,
                       lanjutan,
                       state,
                       posisi,
                       snfg_komponen,
                       jenis_proses,
                       is_done
                from posisi_proses_raw
                where snfg_komponen='".$snfg_komponen."'
                order by snfg_komponen,timestamp desc
            ";
        $last= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($last);
    }

    public function actionGetLastInduk($snfg)
    {
        $sql="
                select distinct on (snfg)
                       lanjutan_split_batch,
                       lanjutan,
                       state,
                       posisi,
                       snfg_komponen,
                       jenis_proses
                from posisi_proses_raw
                where snfg='".$snfg."'
                order by snfg,timestamp desc
            ";
        $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }

    // Fungsi untuk check SNFG nya apakah Komponennya sudah beres semua atau belum
    public function actionCheckSnfgDoneKemas2($snfg)
    {
        $sql="
                SELECT
                    snfg,
                    CASE WHEN is_not_kemas2 = counter AND is_done = counter then 'Done'
                         --WHEN is_kemas2 != counter AND is_done != counter then 'Not Done'
                         WHEN is_kemas2 = counter then 'Out'
                         ELSE 'Not Done'
                    end as is_done
                FROM
                    (SELECT snfg,
                            count(snfg) as counter,
                            sum(case when posisi!='KEMAS 2' then 1 else 0 end) as is_not_kemas2,
                            sum(case when state in ('STOP','UNHOLD','CONTINUE') and is_done='Done' then 1 else 0 end) as is_done,
                            sum(case when posisi='KEMAS 2' then 1 else 0 end) as is_kemas2
                    from posisi_proses_raw
                    where snfg='".$snfg."'
                    GROUP BY snfg
                    ) a

            ";
        $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }

    // revisi ini juga jadi distinct on
    public function actionGetLastIndukSnfg($snfg,$lanjutan_split_batch)
    {
        $sql="  select  distinct on (snfg)
                        state,
                        posisi,
                        lanjutan_split_batch,
                        jenis_proses
                from posisi_proses_raw
                where snfg='".$snfg."'  and lanjutan_split_batch=coalesce('".$lanjutan_split_batch."',1)
                order by snfg,timestamp desc
             ";
        $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }

    // Ini dipakai untuk QC FG , untuk menampilkan status terakhir dari Jadwal Per SB Per Palet

    // UNUSED PER FEBRUARI 12 2017

    // public function actionGetLastProsesSnfg($snfg,$lanjutan_split_batch,$palet_ke)
    // {
    //     $sql="  select  distinct on (snfg)
    //                     state,
    //                     posisi,
    //                     lanjutan_split_batch,
    //                     jenis_proses
    //             from posisi_proses_raw
    //             where snfg='".$snfg."'  and lanjutan_split_batch=coalesce('".$lanjutan_split_batch."',1)
    //             and palet_ke=coalesce('".$palet_ke."',1)
    //             order by snfg,timestamp desc
    //          ";
    //     $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
    //     echo Json::encode($lastinduk);
    // }


    // INI yang dipakai > Feb 12 2017

    public function actionGetLastProsesSnfg($snfg,$lanjutan_split_batch)
    {
        $sql="  select  distinct on (snfg)
                        state,
                        posisi,
                        lanjutan_split_batch,
                        jenis_proses
                from posisi_proses_raw
                where snfg='".$snfg."'  and lanjutan_split_batch=coalesce('".$lanjutan_split_batch."',1)
                order by snfg,timestamp desc
             ";
        $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }

    // Ini dipakai untuk QC FG , untuk menampilkan status terakhir dari Jadwal Per SB Per Palet

    // UNUSED PER FEBRUARI 12 2017

    // public function actionGetLastProsesKomponen($snfg_komponen,$lanjutan_split_batch,$palet_ke)
    // {
    //     $sql="  select  distinct on (snfg_komponen)
    //                     state,
    //                     posisi,
    //                     lanjutan_split_batch,
    //                     jenis_proses
    //             from posisi_proses_raw
    //             where snfg_komponen='".$snfg_komponen."'  and lanjutan_split_batch=coalesce('".$lanjutan_split_batch."',1) and palet_ke=coalesce('".$palet_ke."',1)
    //             order by snfg_komponen,timestamp desc
    //          ";
    //     $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
    //     echo Json::encode($lastinduk);
    // }

    // Dipakai > Februari 12 2017

    public function actionGetLastProsesKomponen($snfg_komponen,$lanjutan_split_batch)
    {
        $sql="  select  distinct on (snfg_komponen)
                        state,
                        posisi,
                        lanjutan_split_batch,
                        jenis_proses
                from posisi_proses_raw
                where snfg_komponen='".$snfg_komponen."'  and lanjutan_split_batch=coalesce('".$lanjutan_split_batch."',1)
                order by snfg_komponen,timestamp desc
             ";
        $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }

    // revisi ini jadi distinct on orde by timestamp desc
    public function actionGetLastIndukKomponen($snfg_komponen,$lanjutan_split_batch)
    {
        $sql="  select  distinct on (snfg_komponen)
                        state,
                        posisi,
                        lanjutan_split_batch,
                        jenis_proses
                from posisi_proses_raw
                where snfg_komponen='".$snfg_komponen."'  and lanjutan_split_batch=coalesce('".$lanjutan_split_batch."',1)
                order by snfg_komponen,timestamp desc
             ";
        $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }

    public function actionGetLastPaletSnfg($snfg,$palet_ke)
    {
        $sql="  select  distinct on (snfg)
                        state,
                        posisi,
                        palet_ke,
                        jenis_proses
                from posisi_proses_raw
                where snfg='".$snfg."'  and palet_ke=coalesce('".$palet_ke."',1)
                order by snfg,timestamp desc
             ";
        $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }


// GET LIST KENDALA DROP DOWN

    // KEMAS

        // KOMPONEN

            public function actionGetKendalaKemasKomponen($snfg_komponen)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg_komponen' => $snfg_komponen])
                                ->count();


                $sql = "
                        SELECT distinct d.kemas as keterangan
                        FROM scm_planner s
                        INNER JOIN dim_kendala d on s.sediaan=d.sediaan
                        WHERE s.snfg_komponen='".$snfg_komponen."' and d.kemas is not null
                        ORDER by d.kemas asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Kendala'>Select Kendala</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Komponen Tidak Valid</option>";
                }
            }

        // SNFG

            public function actionGetKendalaKemasSnfg($snfg)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg' => $snfg])
                                ->count();


                $sql = "
                        SELECT distinct d.kemas as keterangan
                        FROM scm_planner s
                        INNER JOIN dim_kendala d on s.sediaan=d.sediaan
                        WHERE s.snfg='".$snfg."' and d.kemas is not null
                        ORDER by d.kemas asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Kendala'>Select Kendala</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Induk Tidak Valid</option>";
                }
            }

    // PENGOLAHAN

        // KOMPONEN

            public function actionGetKendalaOlahKomponen($snfg_komponen)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg_komponen' => $snfg_komponen])
                                ->count();


                $sql = "
                        SELECT distinct d.pengolahan as keterangan
                        FROM scm_planner s
                        INNER JOIN dim_kendala d on s.sediaan=d.sediaan
                        WHERE s.snfg_komponen='".$snfg_komponen."' and d.pengolahan is not null
                        ORDER by d.pengolahan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Kendala'>Select Kendala</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Komponen Tidak Valid</option>";
                }
            }

        // NOMO

            public function actionGetKendalaOlahNomo($nomo)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['nomo' => $nomo])
                                ->count();


                $sql = "
                        SELECT distinct d.pengolahan as keterangan
                        FROM scm_planner s
                        INNER JOIN dim_kendala d on s.sediaan=d.sediaan
                        WHERE s.nomo='".$nomo."' and d.pengolahan is not null
                        ORDER by d.pengolahan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Kendala'>Select Kendala</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                    }

                }
                else{
                        echo "<option>Nomor MO tidak Valid</option>";
                }
            }

    // PENIMBANGAN

        // KOMPONEN

            public function actionGetKendalaTimbangKomponen($snfg_komponen)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg_komponen' => $snfg_komponen])
                                ->count();


                $sql = "
                        SELECT distinct d.penimbangan as keterangan
                        FROM scm_planner s
                        INNER JOIN dim_kendala d on s.sediaan=d.sediaan
                        WHERE s.snfg_komponen='".$snfg_komponen."' and d.penimbangan is not null
                        ORDER by d.penimbangan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Kendala'>Select Kendala</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Komponen Tidak Valid</option>";
                }
            }

        // NOMO

            public function actionGetKendalaTimbangNomo($nomo)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['nomo' => $nomo])
                                ->count();


                $sql = "
                        SELECT distinct d.penimbangan as keterangan
                        FROM scm_planner s
                        INNER JOIN dim_kendala d on s.sediaan=d.sediaan
                        WHERE s.nomo='".$nomo."' and d.penimbangan is not null
                        ORDER by d.penimbangan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Kendala'>Select Kendala</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                    }

                }
                else{
                        echo "<option>Nomor MO Tidak Valid</option>";
                }
            }


            public function actionGetKendalaTimbangId($id)
            {


                $sql = "
                        SELECT
                          distinct d.penimbangan as keterangan
                        FROM flow_input_mo fim
                        INNER JOIN scm_planner s on fim.nomo = s.nomo
                        INNER JOIN dim_kendala d on s.sediaan = d.sediaan
                        WHERE fim.id=".$id." and d.penimbangan is not null
                        ORDER by d.penimbangan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                echo "<option value=''>Select Kendala</options>";
                foreach($QcFgs as $QcFg){
                    echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                }

            }

            public function actionGetKendalaPraKemasId($id)
            {


                $sql = "
                        SELECT
                          distinct d.kemas as keterangan
                        FROM flow_pra_kemas fim
                        INNER JOIN scm_planner s on fim.snfg = s.snfg
                        INNER JOIN dim_kendala d on s.sediaan = d.sediaan
                        WHERE fim.id=".$id." and d.kemas is not null
                        ORDER by d.kemas asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                echo "<option value=''>Select Kendala</options>";
                foreach($QcFgs as $QcFg){
                    echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                }

            }


            public function actionGetKendalaKemasId($id)
            {


                $sql = "
                        SELECT
                          distinct d.kemas as keterangan
                        FROM flow_input_snfg fim
                        INNER JOIN scm_planner s on fim.snfg = s.snfg
                        INNER JOIN dim_kendala d on s.sediaan = d.sediaan
                        WHERE fim.id=".$id." and d.kemas is not null
                        ORDER by d.kemas asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                echo "<option value=''>Select Kendala</options>";
                foreach($QcFgs as $QcFg){
                    echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                }

            }

            public function actionGetKendalaKemasKomponenId($id)
            {


                $sql = "
                        SELECT
                          distinct d.kemas as keterangan
                        FROM flow_input_snfgkomponen fim
                        INNER JOIN scm_planner s on fim.snfg_komponen = s.snfg_komponen
                        INNER JOIN dim_kendala d on s.sediaan = d.sediaan
                        WHERE fim.id=".$id." and d.kemas is not null
                        ORDER by d.kemas asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                echo "<option value=''>Select Kendala</options>";
                foreach($QcFgs as $QcFg){
                    echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                }

            }


            public function actionGetKendalaOlahId($id)
            {


                $sql = "
                        SELECT
                          distinct d.pengolahan as keterangan
                        FROM flow_input_mo fim
                        INNER JOIN scm_planner s on fim.nomo = s.nomo
                        INNER JOIN dim_kendala d on s.sediaan = d.sediaan
                        WHERE fim.id=".$id." and d.pengolahan is not null
                        ORDER by d.pengolahan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                echo "<option value=''>Select Kendala</options>";
                foreach($QcFgs as $QcFg){
                    echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
                }

            }

// GET NAMA MESIN DROP DOWN

    // KEMAS

        // KOMPONEN

            public function actionGetLineKemasKomponen($snfg_komponen)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg_komponen' => $snfg_komponen])
                                ->count();


                $sql = "
                        SELECT distinct d.kemas as line_kemas_1
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.snfg_komponen='".$snfg_komponen."' and d.kemas is not null
                        ORDER by d.kemas asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Line'>Select Line</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_kemas_1."'>".$QcFg->line_kemas_1."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Komponen Tidak Valid</option>";
                }
            }

        // SNFG

            public function actionGetLineKemasSnfg($snfg)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg' => $snfg])
                                ->count();


                $sql = "
                        SELECT distinct d.kemas as line_kemas_1
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.snfg='".$snfg."' and d.kemas is not null
                        ORDER by d.kemas asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Line'>Select Line</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_kemas_1."'>".$QcFg->line_kemas_1."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Induk Tidak Valid</option>";
                }
            }

            public function actionGetLineKemasSnfg2($snfg)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg' => $snfg])
                                ->count();


                $sql = "
                        SELECT distinct d.nama_line as line_kemas_1
                        FROM master_data_line d
                        ORDER by d.nama_line asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value=''>Select Line</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_kemas_1."'>".$QcFg->line_kemas_1."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Induk Tidak Valid</option>";
                }
            }

            public function actionGetLineKemasSnfgNew($snfg)
            {

                $sql = "
                        SELECT distinct on (sediaan) *
                        FROM scm_planner
                        WHERE snfg='".$snfg."'
                        ORDER BY sediaan,snfg
                        ";

                $sediaan_array = ScmPlanner::findBySql($sql)->one();
                $sediaan = $sediaan_array->sediaan;

                $countQcFg = ScmPlanner::find()
                                ->where(['snfg' => $snfg])
                                ->count();


                $sql = "
                        SELECT distinct d.kemas as line_kemas_1
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.snfg='".$snfg."' and d.kemas is not null
                        ORDER by d.kemas asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Line'>Select Line</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_kemas_1."'>".$QcFg->line_kemas_1."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Induk Tidak Valid</option>";
                }
            }


            public function actionGetLineByFrontendIp($frontendip)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg' => $snfg])
                                ->count();


                $sql = "
                        SELECT distinct d.kemas as line_kemas_1
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.snfg='".$snfg."' and d.kemas is not null
                        ORDER by d.kemas asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Line'>Select Line</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_kemas_1."'>".$QcFg->line_kemas_1."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Induk Tidak Valid</option>";
                }
            }

    // PENGOLAHAN

        // KOMPONEN

            public function actionGetLineOlahKomponen($snfg_komponen)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg_komponen' => $snfg_komponen])
                                ->count();


                $sql = "
                        SELECT distinct d.pengolahan as line_olah
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.snfg_komponen='".$snfg_komponen."' and d.pengolahan is not null
                        ORDER by d.pengolahan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Line'>Select Line</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_olah."'>".$QcFg->line_olah."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Komponen Tidak Valid</option>";
                }
            }

        // Edit Jadwal Komponen

            public function actionGetLineOlahKomponenEdit($snfg_komponen,$nama_line)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg_komponen' => $snfg_komponen])
                                ->count();


                $sql = "
                        SELECT distinct d.pengolahan as line_olah
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.snfg_komponen='".$snfg_komponen."' and d.pengolahan is not null
                        ORDER by d.pengolahan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value=".$nama_line.">".$nama_line."</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_olah."'>".$QcFg->line_olah."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Komponen Tidak Valid</option>";
                }
            }

        // NOMO

            public function actionGetLineOlahNomo($nomo)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['nomo' => $nomo])
                                ->count();


                $sql = "
                        SELECT distinct d.pengolahan as line_olah
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.nomo='".$nomo."' and d.pengolahan is not null
                        ORDER by d.pengolahan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Line'>Select Line</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_olah."'>".$QcFg->line_olah."</options>";
                    }

                }
                else{
                        echo "<option>Nomor MO tidak Valid</option>";
                }
            }

        // NOMO EDIT JADWAL

            public function actionGetLineOlahNomoEdit($nomo,$nama_line)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['nomo' => $nomo])
                                ->count();


                $sql = "
                        SELECT distinct d.pengolahan as line_olah
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.nomo='".$nomo."' and d.pengolahan is not null
                        ORDER by d.pengolahan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value=".$nama_line.">".$nama_line."</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_olah."'>".$QcFg->line_olah."</options>";
                    }

                }
                else{
                        echo "<option>Nomor MO tidak Valid</option>";
                }
            }

    // PENIMBANGAN

        // KOMPONEN

            public function actionGetLineTimbangKomponen($snfg_komponen)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg_komponen' => $snfg_komponen])
                                ->count();


                $sql = "
                        SELECT distinct d.penimbangan as line_timbang
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.snfg_komponen='".$snfg_komponen."' and d.penimbangan is not null
                        ORDER by d.penimbangan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Line'>Select Line</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_timbang."'>".$QcFg->line_timbang."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Komponen Tidak Valid</option>";
                }
            }

        // KOMPONEN EDIT JADWAL

            public function actionGetLineTimbangKomponenEdit($snfg_komponen,$nama_line)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['snfg_komponen' => $snfg_komponen])
                                ->count();


                $sql = "
                        SELECT distinct d.penimbangan as line_timbang
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.snfg_komponen='".$snfg_komponen."' and d.penimbangan is not null
                        ORDER by d.penimbangan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value=".$nama_line.">".$nama_line."</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_timbang."'>".$QcFg->line_timbang."</options>";
                    }

                }
                else{
                        echo "<option>SNFG Komponen Tidak Valid</option>";
                }
            }

        // NOMO

            public function actionGetLineTimbangNomo($nomo)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['nomo' => $nomo])
                                ->count();


                $sql = "
                        SELECT distinct d.penimbangan as line_timbang
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.nomo='".$nomo."' and d.penimbangan is not null
                        ORDER by d.penimbangan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Line'>Select Line</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_timbang."'>".$QcFg->line_timbang."</options>";
                    }

                }
                else{
                        echo "<option>Nomor MO Tidak Valid</option>";
                }
            }


        // EDIT JADWAL

            public function actionGetLineTimbangNomoEdit($nomo,$nama_line)
            {
                $countQcFg = ScmPlanner::find()
                                ->where(['nomo' => $nomo])
                                ->count();


                $sql = "
                        SELECT distinct d.penimbangan as line_timbang
                        FROM scm_planner s
                        INNER JOIN dim_line d on s.sediaan=d.sediaan
                        WHERE s.nomo='".$nomo."' and d.penimbangan is not null
                        ORDER by d.penimbangan asc
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='".$nama_line."'>".$nama_line."</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->line_timbang."'>".$QcFg->line_timbang."</options>";
                    }

                }
                else{
                        echo "<option>Nomor MO Tidak Valid</option>";
                }
            }


    public function actionCheckSnfg($snfg)
    {
        $sql="select count(*) as id from scm_planner where snfg='".$snfg."' and snfg is not null";
        $planner= ScmPlanner::findBySql($sql)->one();
        echo Json::encode($planner);
    }

    public function actionCheckNomo($nomo)
    {
        $sql="select count(*) as id from scm_planner where nomo='".$nomo."' and nomo is not null";
        $planner= ScmPlanner::findBySql($sql)->one();
        echo Json::encode($planner);
    }

    public function actionPeriksaStatusNomo($nomo)
    {
        $sql="select
                  nomo,status
              from scm_planner
              where nomo='".$nomo."'";
        $planner= ScmPlanner::findBySql($sql)->one();
        echo Json::encode($planner);
    }

    public function actionCheckSnfgKomponen($snfg_komponen)
    {
        $sql="select count(*) as id from scm_planner where snfg_komponen='".$snfg_komponen."' and snfg_komponen is not null";
        $planner= ScmPlanner::findBySql($sql)->one();
        echo Json::encode($planner);

    }

    public function actionGetPdtOlah()
    {


        echo "<option value='PENGEMBERAN'>PENGEMBERAN</options>
              <option value='TURUN_BULK'>TURUN_BULK</options>
              <option value='ADJUST'>ADJUST</options>";

    }


    public function actionGetPdtKemas()
    {


        $sql = "
                SELECT
                  keterangan
                FROM dim_planned_down_time
                ORDER by id asc
                ";

        $QcFgs = ScmPlanner::findBySql($sql)->all();

        echo "<option value='Select Kendala'>Select Kendala</options>";
        foreach($QcFgs as $QcFg){
            echo "<option value='".$QcFg->keterangan."'>".$QcFg->keterangan."</options>";
        }


    }


    // revisi ini jadi distinct on orde by timestamp desc
    public function actionGetLastPaletKomponen($snfg_komponen,$palet_ke)
    {
        $sql="  select  distinct on (snfg_komponen)
                        state,
                        posisi,
                        palet_ke,
                        jenis_proses
                from posisi_proses_raw
                where snfg_komponen='".$snfg_komponen."'  and palet_ke=coalesce('".$palet_ke."',1)
                order by snfg_komponen,timestamp desc
             ";
        $lastinduk= PosisiProsesRaw::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }


    public function actionGetPprr($snfg)
    {
        $sql="select string_agg(is_done,',') as is_done, string_agg(snfg,',') as snfg, string_agg(snfg_komponen,',') as snfg_komponen, string_agg(posisi,',') as posisi,string_agg(state,',') as state,string_agg(jenis_proses,',') as jenis_proses,string_agg(lanjutan::text,',') as lanjutan from posisi_proses_raw where snfg='".$snfg."'";
        $pprr= PosisiProsesRaw::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($pprr);

    }

    public function actionGetPosisiprosesraw($snfg)
    {
        $sql="select
                    is_done,
                    snfg,
                    snfg_komponen,
                    posisi,
                    state,
                    jenis_proses,
                    lanjutan
              from posisi_proses_raw where snfg='".$snfg."'";
        $pprr= PosisiProsesRaw::findBySql($sql)->all();
        //print_r($pprr);
        echo Json::encode($pprr);
    }


    public function actionGetPp($nomo)
    {
        $sql="select string_agg(is_done,',') as is_done, string_agg(snfg,',') as snfg, string_agg(snfg_komponen,',') as snfg_komponen, string_agg(posisi,',') as posisi,string_agg(state,',') as state,string_agg(jenis_proses,',') as jenis_proses,string_agg(lanjutan::text,',') as lanjutan from posisi_proses_raw where nomo='".$nomo."'";
        $pp= PosisiProsesRaw::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($pp);

    }

    public function actionGetLastprosesMo($nomo)
    {
        $sql="select * from posisi_proses_raw where nomo='".$nomo."'";
        $pp= PosisiProsesRaw::findBySql($sql)->all();
        //print_r($pprr);
        echo Json::encode($pp);

    }

    public function actionGetSnfg($snfg)
    {
        $sql="select * from scm_planner where snfg='".$snfg."'";
        $planner= ScmPlanner::findBySql($sql)->one();
        echo Json::encode($planner);

    }

    public function actionGetPlannerSnfg($snfg)
    {
        $sql="select
                     string_agg(nomo,',') as nomo,
                     string_agg(snfg_komponen,',') as snfg_komponen,
                     string_agg(streamline,',') as streamline,
                     string_agg(start::text,',') as start,
                     string_agg(due::text,',') as due,
                     string_agg(nama_bulk,',') as nama_bulk,
                     string_agg(nama_fg,',') as nama_fg,
                     string_agg(besar_batch::text,',') as besar_batch,
                     string_agg(kode_jadwal,',') as kode_jadwal,
                     string_agg(status,',') as status
            from scm_planner where snfg='".$snfg."'";
        $planner= ScmPlanner::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($planner);
    }


    public function actionCronUpdateOdooCode()
    {
        $id_null = ScmPlanner::find()->select('koitem_fg')->distinct()->where("odoo_code is null and koitem_fg is not null")->all();
        foreach ($id_null as $id) {

        }
        print_r ('<pre>');
        print_r ($id_null);
    }

    public function actionGetPlannerSnfgRaw($snfg)
    {
        $sql="select
                     nomo,
                     snfg_komponen,
                     streamline,
                     start,
                     due,
                     nama_bulk,
                     nama_fg,
                     besar_batch,
                     kode_jadwal,
                     status
            from scm_planner where snfg='".$snfg."'";
        $planner= ScmPlanner::findBySql($sql)->all();
        //print_r($pprr);
        echo Json::encode($planner);
    }


    /**
     * Deletes an existing ScmPlanner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ScmPlanner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScmPlanner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ScmPlanner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
