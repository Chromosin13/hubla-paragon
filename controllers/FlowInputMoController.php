<?php

namespace app\controllers;

use Yii;
use app\models\FlowInputMo;
use app\models\ScmPlanner;
use app\models\JenisProsesOlah;
use app\models\JenisProsesTimbang;
use app\models\LogJadwalTimbangRm;
use app\models\LogTaskChecker;
use app\models\LogTaskPengolahan;
use app\models\LogTaskPengolahanSearch;
use app\models\FlowInputMoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use app\models\User;
use yii\filters\AccessControl;
use app\models\Kendala;
use app\models\Model;
use yii\helpers\ArrayHelper;
use app\models\Downtime;
use app\models\DowntimeSearch;
use yii\data\SqlDataProvider;
use yii\base\ErrorException;
use app\models\WeigherFgMapDevice;
use app\models\WeigherFgMapDeviceSearch;
use app\models\MasterDataFormula;
use app\models\BillOfMaterials;
use app\models\DeviceMapRm;
use app\models\LogPenimbanganRm;
use app\models\MasterDataLine;
use app\models\MasterDataFormulaSearch;
use yii\data\ArrayDataProvider;


/**
 * FlowInputMoController implements the CRUD actions for FlowInputMo model.
 */
class FlowInputMoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FlowInputMo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FlowInputMoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FlowInputMo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FlowInputMo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FlowInputMo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionResolvePenimbangan($nomo,$lp_id)
    {
        $model = new FlowInputMo();

        // Populate Grid Data
        $searchModel = new FlowInputMoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("nomo='".$nomo."' and posisi='PENIMBANGAN'");

        $this->layout = '//main-sidebar-collapse';

        //Check Last State of a Nomo
        $sql="

        SELECT
            distinct on (nomo)
            id,
            nomo,
            datetime_start,
            datetime_stop
        FROM flow_input_mo
        WHERE nomo = '".$nomo."' and posisi='PENIMBANGAN'
        ORDER BY nomo,id desc
        ";

        $row = FlowInputMo::findBySql($sql)->one();

        // Assign Variable Null to prevent error if it's the first data entry
        if(empty($row)){
            $last_datetime_start = null;
            $last_datetime_stop = null;
            $last_id = null;
        }else{
            $last_datetime_start = $row->datetime_start;
            $last_datetime_stop = $row->datetime_stop;
            $last_id = $row->id;
        }


        // Get Penimbangan Lanjutan
         $sql="
                SELECT nama_line
                FROM flow_input_mo
                WHERE nomo = '".$nomo."'
                and posisi='PENIMBANGAN'
                and lanjutan = 1
        ";

        $nama_line_row = FlowInputMo::findBySql($sql)->one();

        if(empty($nama_line_row)){
            $nama_line = null;
        }else{
            $nama_line = $nama_line_row->nama_line;
        }


        // Check If Post Value True
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $connection3 = Yii::$app->getDb();
            $command3 = $connection3->createCommand("
                UPDATE pusat_resolusi_log
                SET proses_id = :id
                WHERE id = :lp_id;
                ",
                [':id'=> $model->id,':lp_id'=>$lp_id]
            );

            $result3 = $command3->queryAll();

            return $this->redirect(['downtime/index-penimbangan', 'id' => $model->id]);
        } else {
            return $this->render('resolve-penimbangan', [
                'model' => $model,
                'nomo' => $nomo,
                'last_datetime_start' => $last_datetime_start,
                'last_datetime_stop' => $last_datetime_stop,
                'last_id' => $last_id,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'nama_line' => $nama_line,
            ]);
        }
    }


    public function actionResolvePengolahan($nomo,$lp_id)
    {
        $model = new FlowInputMo();

        // Populate Grid Data
        $searchModel = new FlowInputMoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("nomo='".$nomo."' and posisi='PENGOLAHAN'");

        $this->layout = '//main-sidebar-collapse';

        //Check Last State of a Nomo
        $sql="

        SELECT
            distinct on (nomo)
            id,
            nomo,
            datetime_start,
            datetime_stop
        FROM flow_input_mo
        WHERE nomo = '".$nomo."' and posisi='PENGOLAHAN'
        ORDER BY nomo,id desc
        ";

        $row = FlowInputMo::findBySql($sql)->one();

        // Assign Variable Null to prevent error if it's the first data entry
        if(empty($row)){
            $last_datetime_start = null;
            $last_datetime_stop = null;
            $last_id = null;
        }else{
            $last_datetime_start = $row->datetime_start;
            $last_datetime_stop = $row->datetime_stop;
            $last_id = $row->id;
        }


        // Get Penimbangan Lanjutan
         $sql="
                SELECT nama_line
                FROM flow_input_mo
                WHERE nomo = '".$nomo."'
                and posisi='PENGOLAHAN'
                and lanjutan = 1
        ";

        $nama_line_row = FlowInputMo::findBySql($sql)->one();

        if(empty($nama_line_row)){
            $nama_line = null;
        }else{
            $nama_line = $nama_line_row->nama_line;
        }


        // Check If Post Value True
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            // Populate SNFG Komponen to Pengolahan Batch
            $connection2 = Yii::$app->getDb();
            $command2 = $connection2->createCommand("
                INSERT INTO pengolahan_batch (pengolahan_id,snfg_komponen)
                SELECT fim.id as pengolahan_id,sp.snfg_komponen as snfg_komponen
                FROM flow_input_mo fim
                INNER JOIN scm_planner sp on sp.nomo = fim.nomo
                WHERE fim.id = :id
                ORDER BY sp.snfg_komponen;
                ",
                [':id'=> $model->id]
            );

            $result2 = $command2->queryAll();

            // Update Pusat Resolusi Log id with id from new row (id triggered from lupa input)
            $connection3 = Yii::$app->getDb();
            $command3 = $connection3->createCommand("
                UPDATE pusat_resolusi_log
                SET proses_id = :id
                WHERE id = :lp_id;
                ",
                [':id'=> $model->id,':lp_id'=>$lp_id]
            );

            $result3 = $command3->queryAll();

            return $this->redirect(['downtime/index-pengolahan', 'id' => $model->id]);

        } else {
            return $this->render('resolve-pengolahan', [
                'model' => $model,
                'nomo' => $nomo,
                'last_datetime_start' => $last_datetime_start,
                'last_datetime_stop' => $last_datetime_stop,
                'last_id' => $last_id,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'nama_line' => $nama_line,
            ]);
        }
    }


    // public function actionCreatePenimbangan2($nomo)
    // {
    //     $model = new FlowInputMo();

    //     // Populate Grid Data
    //     $searchModel = new FlowInputMoSearch();
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    //     $dataProvider->query->where("nomo='".$nomo."' and posisi='PENIMBANGAN'");

    //     $this->layout = '//main-sidebar-collapse';

    //     //Check Last State of a Nomo
    //     $sql="

    //     SELECT
    //         distinct on (nomo)
    //         id,
    //         nomo,
    //         datetime_start,
    //         datetime_stop
    //     FROM flow_input_mo
    //     WHERE nomo = '".$nomo."' and posisi='PENIMBANGAN'
    //     ORDER BY nomo,id desc
    //     ";

    //     $row = FlowInputMo::findBySql($sql)->one();

    //     // Assign Variable Null to prevent error if it's the first data entry
    //     if(empty($row)){
    //         $last_datetime_start = null;
    //         $last_datetime_stop = null;
    //         $last_id = null;
    //     }else{
    //         $last_datetime_start = $row->datetime_start;
    //         $last_datetime_stop = $row->datetime_stop;
    //         $last_id = $row->id;
    //     }

    //     // Jenis Proses Check Status Jadwal, If Found then Don't Display
    //     $sql3="
    //             SELECT
    //                 jenis_proses_timbang
    //             FROM jenis_proses_timbang
    //             WHERE jenis_proses_timbang not in
    //             (SELECT jenis_proses FROM status_jadwal WHERE
    //             nojadwal='".$nomo."' and posisi='PENIMBANGAN')
    //         ";

    //     $jenis_proses_list = ArrayHelper::map(JenisProsesTimbang::findBySql($sql3)->all(), 'jenis_proses_timbang','jenis_proses_timbang');

    //     // //Check Is Done state of a Nomo
    //     // $sql2="

    //     // SELECT
    //     //     sum(coalesce(status,0)) as id
    //     // FROM status_jadwal
    //     // WHERE nojadwal = '".$nomo."' and posisi='PENIMBANGAN and ';
    //     // ";

    //     // $jadwal = FlowInputMo::findBySql($sql2)->one();
    //     // $is_done = $jadwal->id;


    //     // Get Penimbangan Lanjutan
    //      $sql="
    //             SELECT nama_line
    //             FROM flow_input_mo
    //             WHERE nomo = '".$nomo."'
    //             and posisi='PENIMBANGAN'
    //             and lanjutan = 1
    //     ";

    //     $nama_line_row = FlowInputMo::findBySql($sql)->one();

    //     if(empty($nama_line_row)){
    //         $nama_line = null;
    //     }else{
    //         $nama_line = $nama_line_row->nama_line;
    //     }


    //     // Check If Snfg Last Status is Start, if yes then redirect to actionStopPengolahan
    //     if(!empty($last_datetime_start)&&(empty($last_datetime_stop))){
    //         return $this->redirect(['stop-penimbangan','nomo' => $nomo,'last_id'=>$last_id]);
    //     }

    //     // Check If Post Value True
    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['check-penimbangan']);
    //     } else {
    //         return $this->render('create-penimbangan', [
    //             'model' => $model,
    //             'nomo' => $nomo,
    //             'last_datetime_start' => $last_datetime_start,
    //             'last_datetime_stop' => $last_datetime_stop,
    //             'last_id' => $last_id,
    //             'searchModel' => $searchModel,
    //             'dataProvider' => $dataProvider,
    //             'nama_line' => $nama_line,
    //             'jenis_proses_list' => $jenis_proses_list,
    //         ]);
    //     }
    // }


    public function actionCreatePenimbangan($nomo)
    {

        $mod = new ScmPlanner();

        $result = $mod->periksaStatusNomo($nomo);

        if($result=='UNHOLD'){

            $model = new FlowInputMo();

            // Populate Grid Data
            $searchModel = new FlowInputMoSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where("nomo='".$nomo."' and posisi='PENIMBANGAN'");

            $this->layout = '//main-sidebar-collapse';

            //Check Last State of a Nomo
            $sql="

            SELECT
                distinct on (nomo)
                id,
                nomo,
                datetime_start,
                datetime_stop
            FROM flow_input_mo
            WHERE nomo = '".$nomo."' and posisi='PENIMBANGAN'
            ORDER BY nomo,id desc
            ";

            $row = FlowInputMo::findBySql($sql)->one();

            // Assign Variable Null to prevent error if it's the first data entry
            if(empty($row)){
                $last_datetime_start = null;
                $last_datetime_stop = null;
                $last_id = null;
            }else{
                $last_datetime_start = $row->datetime_start;
                $last_datetime_stop = $row->datetime_stop;
                $last_id = $row->id;
            }

            // Jenis Proses Check Status Jadwal, If Found then Don't Display
            $sql3="
                    SELECT
                        jenis_proses_timbang
                    FROM jenis_proses_timbang
                    WHERE jenis_proses_timbang not in
                    (SELECT jenis_proses FROM status_jadwal WHERE
                    nojadwal='".$nomo."' and posisi='PENIMBANGAN')
                ";

            $jenis_proses_list = ArrayHelper::map(JenisProsesTimbang::findBySql($sql3)->all(), 'jenis_proses_timbang','jenis_proses_timbang');


            // Get Penimbangan Lanjutan
             $sql="
                    SELECT nama_line
                    FROM flow_input_mo
                    WHERE nomo = '".$nomo."'
                    and posisi='PENIMBANGAN'
                    and lanjutan = 1
            ";

            $nama_line_row = FlowInputMo::findBySql($sql)->one();

            if(empty($nama_line_row)){
                $nama_line = null;
            }else{
                $nama_line = $nama_line_row->nama_line;
            }


            // Check If Snfg Last Status is Start, if yes then redirect to actionStopPengolahan
            if(!empty($last_datetime_start)&&(empty($last_datetime_stop))){
                return $this->redirect(['stop-penimbangan','nomo' => $nomo,'last_id'=>$last_id]);
            }

            // Check If Post Value True
            if ($model->load(Yii::$app->request->post()) ) {

                // Cek Line dari PE
                 $sql7="
                        SELECT nama_line FROM log_penimbangan_rm WHERE nomo = '".$nomo."' 
                ";

                $line_pe = LogPenimbanganRm::findBySql($sql7)->one()['nama_line'];

                $line_liquid = ['LWE01','LWE02'];
                $line_powder = ['PWE01','PWE02'];

                $timbangan_new = "";

                if ($model->nama_line != $line_pe){
                    //IF LINE MAPPING LWE01 to LWE02 or INVERSE
                    if (( in_array($model->nama_line,$line_liquid) && in_array($line_pe,$line_liquid) ) || ( in_array($model->nama_line,$line_liquid) && ($line_pe=='PilihLine' || empty($line_pe)) ) ){
                        /******* GET TASK **********/
                        $connection_ = Yii::$app->db;
                        $command_ = $connection_->createCommand("

                        SELECT
                            *
                        FROM log_penimbangan_rm
                        WHERE nomo = :nomo
                        ",
                        [':nomo' => $nomo,
                        ]);

                        $data_ = $command_->queryAll();

                        if ($model->nama_line == 'LWE02'){
                            for ($i = 0; $i < count($data_); $i++) {
                                if ($data_[$i]['nama_line'] != "LWE03"){
                                    if ($data_[$i]['timbangan'] == "LWA 01"){
                                        $timbangan_new = "LWB 01";
                                    } else if ($data_[$i]['timbangan'] == "LWA 02"){
                                        $timbangan_new = "LWB 02";
                                    } else if ($data_[$i]['timbangan'] == "LWA 03"){
                                        $timbangan_new = "LWB 03";
                                    } else if ($data_[$i]['timbangan'] == "LWC 01"){
                                        $timbangan_new = "LWC 02";
                                    } else if ($data_[$i]['timbangan'] == "LWC 03"){
                                        $timbangan_new = "LWC 04";
                                    } else {
                                        $timbangan_new = $data_[$i]['timbangan'];
                                    }
                                    // Update timbangan
                                    $connection = Yii::$app->getDb();
                                    $command = $connection->createCommand("
                                    UPDATE log_penimbangan_rm
                                    SET timbangan = '".$timbangan_new."', no_timbangan = '".$timbangan_new."', nama_line = '".$model->nama_line."'
                                    WHERE id = :id;
                                    ",
                                    [
                                     ':id'=> $data_[$i]['id']
                                    ]);
                                    $result = $command->queryAll();

                                }

                            }

                        }else{

                            for ($i = 0; $i < count($data_); $i++) {
                                if ($data_[$i]['nama_line'] != "LWE03"){
                                    if ($data_[$i]['timbangan'] == "LWB 01"){
                                        $timbangan_new = "LWA 01";
                                    } else if ($data_[$i]['timbangan'] == "LWB 02"){
                                        $timbangan_new = "LWA 02";
                                    } else if ($data_[$i]['timbangan'] == "LWB 03"){
                                        $timbangan_new = "LWA 03";
                                    } else if ($data_[$i]['timbangan'] == "LWC 02"){
                                        $timbangan_new = "LWC 01";
                                    } else if ($data_[$i]['timbangan'] == "LWC 04"){
                                        $timbangan_new = "LWC 03";
                                    } else {
                                        $timbangan_new = $data_[$i]['timbangan'];
                                    }
                                    // Update timbangan
                                    $connection = Yii::$app->getDb();
                                    $command = $connection->createCommand("
                                    UPDATE log_penimbangan_rm
                                    SET timbangan = '".$timbangan_new."', no_timbangan = '".$timbangan_new."', nama_line = '".$model->nama_line."'
                                    WHERE id = :id;
                                    ",
                                    [
                                     ':id'=> $data_[$i]['id']
                                    ]);
                                    $result = $command->queryAll();
                                }
                                
                            }

                        }

                        $model->switch_line = true;
                    
                    //IF LINE MAPPING PWE01 to PWE02 or INVERSE
                    }else if (( in_array($model->nama_line,$line_powder) && in_array($line_pe,$line_powder) ) || ( in_array($model->nama_line,$line_powder) && ($line_pe=='PilihLine' || empty($line_pe) ) )){
                        /******* GET TASK **********/
                        $connection_ = Yii::$app->db;
                        $command_ = $connection_->createCommand("

                        SELECT
                            *
                        FROM log_penimbangan_rm
                        WHERE nomo = :nomo
                        ",
                        [':nomo' => $nomo,
                        ]);

                        $data_ = $command_->queryAll();


                        if ($model->nama_line=='PWE02'){

                            for ($i = 0; $i < count($data_); $i++) {
                                if ($data_[$i]['timbangan'] == "PWA 01"){
                                    $timbangan_new = "PWA 03";
                                } else if ($data_[$i]['timbangan'] == "PWB 02"){
                                    $timbangan_new = "PWB 04";
                                } else if ($data_[$i]['timbangan'] == "PWC 01"){
                                    $timbangan_new = "PWC 02";
                                } else if ($data_[$i]['timbangan'] == "PWA 02"){
                                    $timbangan_new = "PWA 04";
                                } else if ($data_[$i]['timbangan'] == "PWB 01"){
                                    $timbangan_new = "PWB 03";
                                } else {
                                    $timbangan_new = $data_[$i]['timbangan'];
                                }
                                // Update timbangan
                                $connection = Yii::$app->getDb();
                                $command = $connection->createCommand("
                                UPDATE log_penimbangan_rm
                                SET timbangan = '".$timbangan_new."', no_timbangan = '".$timbangan_new."', nama_line = '".$model->nama_line."'
                                WHERE id = :id;
                                ",
                                [
                                 ':id'=> $data_[$i]['id']
                                ]);
                                $result = $command->queryAll();
                            }

                        }else{
                            for ($i = 0; $i < count($data_); $i++) {
                                if ($data_[$i]['timbangan'] == "PWA 03"){
                                    $timbangan_new = "PWA 01";
                                } else if ($data_[$i]['timbangan'] == "PWB 04"){
                                    $timbangan_new = "PWB 02";
                                } else if ($data_[$i]['timbangan'] == "PWC 02"){
                                    $timbangan_new = "PWC 01";
                                } else if ($data_[$i]['timbangan'] == "PWA 04"){
                                    $timbangan_new = "PWA 02";
                                } else if ($data_[$i]['timbangan'] == "PWB 03"){
                                    $timbangan_new = "PWB 01";
                                } else {
                                    $timbangan_new = $data_[$i]['timbangan'];
                                }
                                // Update timbangan
                                $connection = Yii::$app->getDb();
                                $command = $connection->createCommand("
                                UPDATE log_penimbangan_rm
                                SET timbangan = '".$timbangan_new."', no_timbangan = '".$timbangan_new."', nama_line = '".$model->nama_line."'
                                WHERE id = :id;
                                ",
                                [
                                 ':id'=> $data_[$i]['id']
                                ]);
                                $result = $command->queryAll();
                            }
                        }

                        $model->switch_line = true;

                    }else{
                        if ($line_pe != 'PilihLine'){
                            Yii::$app->session->setFlash('danger','Anda memilih Line yang berbeda dengan yang dibuat oleh PE. Sistem tidak mempunyai mapping timbangan dari line '.$line_pe.' ke '.$model->nama_line);
                            return $this->redirect(['create-penimbangan-rm', 'nomo'=>$nomo]);                            
                        }else{
                            $model->switch_line = true;
                        }

                    }
                    
                }

                $model->save();

                return $this->redirect(['check-penimbangan-2']);
            } else {
                return $this->render('create-penimbangan-2', [
                    'model' => $model,
                    'nomo' => $nomo,
                    'last_datetime_start' => $last_datetime_start,
                    'last_datetime_stop' => $last_datetime_stop,
                    'last_id' => $last_id,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'nama_line' => $nama_line,
                    'jenis_proses_list' => $jenis_proses_list,
                ]);
            }
        }else{
            throw new \yii\web\HttpException(401,
          'Jadwal telah di HOLD / CANCEL mohon hubungi Planner');
        }



    }




    public function actionCreatePenimbanganException($nomo)
    {
        $model = new FlowInputMo();

        // Populate Grid Data
        $searchModel = new FlowInputMoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("nomo='".$nomo."' and posisi='PENIMBANGAN'");

        $this->layout = '//main-sidebar-collapse';

        //Check Last State of a Nomo
        $sql="

        SELECT
            distinct on (nomo)
            id,
            nomo,
            datetime_start,
            datetime_stop
        FROM flow_input_mo
        WHERE nomo = '".$nomo."' and posisi='PENIMBANGAN'
        ORDER BY nomo,id desc
        ";

        $row = FlowInputMo::findBySql($sql)->one();

        // Assign Variable Null to prevent error if it's the first data entry
        if(empty($row)){
            $last_datetime_start = null;
            $last_datetime_stop = null;
            $last_id = null;
        }else{
            $last_datetime_start = $row->datetime_start;
            $last_datetime_stop = $row->datetime_stop;
            $last_id = $row->id;
        }


        // Get Penimbangan Lanjutan
         $sql="
                SELECT nama_line
                FROM flow_input_mo
                WHERE nomo = '".$nomo."'
                and posisi='PENIMBANGAN'
                and lanjutan = 1
        ";

        $nama_line_row = FlowInputMo::findBySql($sql)->one();

        if(empty($nama_line_row)){
            $nama_line = null;
        }else{
            $nama_line = $nama_line_row->nama_line;
        }


        // Check If Post Value True
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['check-penimbangan']);
        } else {
            return $this->render('create-penimbangan', [
                'model' => $model,
                'nomo' => $nomo,
                'last_datetime_start' => $last_datetime_start,
                'last_datetime_stop' => $last_datetime_stop,
                'last_id' => $last_id,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'nama_line' => $nama_line,
            ]);
        }
    }

    public function actionChangeSiklusParalel($id)
    {
        $model = FlowInputMo::find()->where(['id'=>$id])->one();
        $model->siklus = 0;

        if ($model->save()){
            $value = ['status'=>'success'];
        }else{
            $value =['status'=>'failed'];
        }
        echo Json::encode($value);
    }

    public function actionCheckState($nomo)
    {
        $sql="

        SELECT
            distinct on (nomo)
            nomo,
            datetime_start
        FROM flow_input_mo
        WHERE nomo = '".$nomo."'
        ORDER BY nomo,id desc
        ";

        $row = FlowInputMo::findBySql($sql)->one();

        echo $row->datetime_start;
        //echo $row->nomo;
        //echo Json::encode($row);

    }


    public function actionGetLatestLanjutan($nomo)
    {
        $sql="

        SELECT
            lanjutan
        FROM flow_input_mo
        WHERE nomo = '".$nomo."'
        AND posisi='PENIMBANGAN'
        ORDER BY id desc
        LIMIT 1
        ";

        $row = FlowInputMo::findBySql($sql)->one();
        //print_r($row);

        echo $row['lanjutan'];

        //echo $row->nomo;
        //echo Json::encode($row);

    }

    public function actionCheckPenimbanganTab()
    {
        $server_name = $_SERVER['SERVER_NAME'];
        if($server_name == '10.3.5.102'){
             echo "<script>location.href='".Yii::getALias('@dnsUrl')."flow-input-mo/check-penimbangan-tab';</script>";
        }
        $frontend_ip = $_SERVER['REMOTE_ADDR'];

        $sql="
        SELECT
            *
        FROM weigher_fg_map_device
        WHERE frontend_ip = '".$frontend_ip."'";

        $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

        if(empty($map_device_array)){
            $sbc_ip = '10.128.1.161';
            $line = 'Line A';
        }else{
            $sbc_ip = $map_device_array->sbc_ip;
            $line = $map_device_array->line;

        }


        $model = new FlowInputMo();

        $searchModel = new FlowInputMoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query->where("posisi='PENIMBANGAN' and datetime_start is not null and datetime_stop is null");

        $this->layout = '//main-sidebar-collapse';

        return $this->render('check-penimbangan-tab', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'frontend_ip' => $frontend_ip,
                'sbc_ip' => $sbc_ip,
                'line' => $line,
        ]);
        // if ($model->load(Yii::$app->request->post())) {
        //     // return $this->redirect(['create-penimbangan', 'kode_unik_formula' => $model->kode_unik_formula]);
        // } else {

        // }
    }

    public function actionCheckPenimbangan()
    {

        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            *
        FROM device_map_rm
        WHERE device_ip = '".$ip."'";

        $map_device_array = DeviceMapRm::findBySql($sql)->one();

        if (!empty($map_device_array)){
            // return $this->redirect(['check-penimbangan-rm']);
        }


        $model = new FlowInputMo();

        $searchModel = new FlowInputMoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("posisi='PENIMBANGAN' and datetime_start is not null and datetime_stop is null");

        $this->layout = '//main-sidebar-collapse';

        return $this->render('check-penimbangan', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
        // if ($model->load(Yii::$app->request->post())) {
        //     // return $this->redirect(['create-penimbangan', 'nomo' => $model->nomo]);
        // } else {

        // }
    }

    public function actionCheckPenimbangan2()
    {
        $model = new FlowInputMo();

        $searchModel = new FlowInputMoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("posisi='PENIMBANGAN' and datetime_start is not null and datetime_stop is null");

        $this->layout = '//main-sidebar-collapse';

        return $this->render('check-penimbangan-2', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
        // if ($model->load(Yii::$app->request->post())) {
        //     // return $this->redirect(['create-penimbangan', 'nomo' => $model->nomo]);
        // } else {

        // }
    }

    /**
     * Render Scan Barcode QR SNFG Kemas 2 Page
     * @return mixed
     */
    public function actionCheckPengolahanTab()
    {

            $frontend_ip = $_SERVER['REMOTE_ADDR'];

            $sql="
            SELECT
                *
            FROM weigher_fg_map_device
            WHERE frontend_ip = '".$frontend_ip."'";

            $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

            if(empty($map_device_array)){
                $sbc_ip = '10.128.1.161';
                $line = 'Line A';
            }else{
                $sbc_ip = $map_device_array->sbc_ip;
                $line = $map_device_array->line;

            }


            $model = new FlowInputMo();

            $searchModel = new FlowInputMoSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // $dataProvider->query->where("posisi='PENIMBANGAN' and datetime_start is not null and datetime_stop is null");

            $this->layout = '//main-sidebar-collapse';

            return $this->render('check-pengolahan-tab', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'frontend_ip' => $frontend_ip,
                    'sbc_ip' => $sbc_ip,
                    'line' => $line,
            ]);
            // if ($model->load(Yii::$app->request->post())) {
            //     // return $this->redirect(['create-penimbangan', 'kode_unik_formula' => $model->kode_unik_formula]);
            // } else {

            // }
    }


    public function actionCheckPengolahan()
    {
        $model = new FlowInputMo();

        $searchModel = new FlowInputMoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("posisi='PENGOLAHAN' and datetime_start is not null and datetime_stop is null");

        $count = Yii::$app->db->createCommand("
            SELECT COUNT(*) FROM (SELECT *
                      FROM
                         (SELECT distinct on (nomo) *
                          FROM flow_input_mo
                          ORDER by nomo,datetime_write desc) a
                      WHERE posisi='PENIMBANGAN') a
        ")->queryScalar();

        $dataProvider2 = new SqlDataProvider([
            'sql' => "
                      SELECT *
                      FROM
                         (SELECT distinct on (nomo) *
                          FROM flow_input_mo
                          ORDER by nomo,datetime_write desc) a
                      WHERE posisi='PENIMBANGAN'
                      ",
            // 'params' => [':publish' => 1],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->layout = '//main-sidebar-collapse';

        return $this->render('check-pengolahan', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'dataProvider2' => $dataProvider2,
            ]);

        // if ($model->load(Yii::$app->request->post())) {
        //     // return $this->redirect(['create-pengolahan', 'nomo' => $model->nomo]);
        // } else {

        // }
    }

    public function actionCheckPengolahanNew()
    {
        $model = new FlowInputMo();

        $searchModel = new FlowInputMoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("posisi='PENGOLAHAN' and datetime_start is not null and datetime_stop is null");

        $this->layout = '//main-sidebar-collapse';

        return $this->render('check-pengolahan-new', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
        // if ($model->load(Yii::$app->request->post())) {
        //     // return $this->redirect(['create-penimbangan', 'nomo' => $model->nomo]);
        // } else {

        // }
    }

    public function actionCheckKodeBb($id,$kode_bb)
    {
        $sql = "
        SELECT kode_bb
        FROM log_task_pengolahan
        WHERE  id = ".$id;

        $row = LogTaskPengolahan::findBySql($sql)->all();

        // echo $row;
        if ($row[0]->kode_bb == $kode_bb){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionCheckStatusNomo($nomo)
    {
        $sql = "
        SELECT *
        FROM flow_input_mo
        WHERE  nomo = '".$nomo."' and posisi = 'PENGOLAHAN' order by id desc";

        $row = FlowInputMo::findBySql($sql)->one();

        if (empty($row)){
            $result = ['status'=>'generate'];
        }else{
            if (empty($row->datetime_stop)){
                $result = ['status'=>'continue','siklus'=>$row->siklus];
            }else{
                $result = ['status'=>'generate'];
            }
        }

        echo Json::encode($result);

    }

    public function actionCheckStatusTask($nomo)
    {
        $sql = "
        SELECT *
        FROM log_task_pengolahan
        WHERE  nomo = '".$nomo."'";

        $row = LogTaskPengolahan::findBySql($sql)->all();

        $sql = "
        SELECT kode_bb
        FROM log_task_pengolahan
        WHERE  nomo = '".$nomo."' and status is null ";

        $taskUnchecked = LogTaskChecker::findBySql($sql)->all();

        // $sql = "
        // SELECT nomo,datetime_stop
        // FROM flow_input_mo
        // WHERE  nomo = '".$nomo."' and posisi = 'CHECKER' order by datetime_start desc limit 1 ";

        // $processChecker= LogTaskChecker::findBySql($sql)->one();

        // echo $row;
        if (!empty($row)){
            if(empty($taskUnchecked)){
                echo 2;
            }else{
                echo 1;
            }
        } else {
            echo 0;
        }
    }

    public function actionCheckTask($nomo,$siklus)
    {
        date_default_timezone_set('Asia/Jakarta');

        if ($siklus == 0){
            $sql = "
            SELECT *
            FROM log_task_pengolahan
            WHERE  nomo = '".$nomo."' AND status is NULL";
        }else{
            $sql = "
            SELECT *
            FROM log_task_pengolahan
            WHERE  nomo = '".$nomo."' AND siklus = '".$siklus."' AND status is NULL";
        }

        $row = LogTaskPengolahan::findBySql($sql)->all();

        if (empty($row)){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionUpdateStatus($id_bb,$log,$pic)
    {
        date_default_timezone_set('Asia/Jakarta');

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

        UPDATE log_task_pengolahan
        SET status = 'OK', log = :log, pic = :pic, timestamp_checked = :timestamp_checked
        WHERE id_bb = :id_bb;
        ", 
        [':id_bb'=> $id_bb,
         ':log'=>$log,
         ':pic'=>$pic,
         ':timestamp_checked'=>date('Y-m-d h:i A'),
        ]);

        $result = $command->queryAll();

        // echo $row;
        if ($result){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionCheckQueuePengolahan($nomo,$siklus,$id_bb){

        //Check queue by siklus and kode_olah
        if ($siklus == 0){
            $sql = "
                    SELECT a.id,
                     a.siklus,
                     a.kode_olah
                    FROM ( SELECT DISTINCT ON (log_task_pengolahan.siklus, log_task_pengolahan.kode_olah) log_task_pengolahan.id,
                            log_task_pengolahan.siklus,
                            log_task_pengolahan.kode_olah
                           FROM log_task_pengolahan
                          WHERE log_task_pengolahan.nomo::text = :nomo AND log_task_pengolahan.status IS NULL) a
                    ORDER BY a.siklus, (COALESCE(substring(a.kode_olah::text, '^(\d+)'::text)::integer, 99999999)), (substring(a.kode_olah::text, '[a-zA-z_-]+'::text)), (COALESCE(substring(a.kode_olah::text, '(\d+)$'::text)::integer, 0))
                    LIMIT 1";

            $queue = LogTaskPengolahan::findBySql($sql,[':nomo'=>$nomo])->one();

        }else{
            $sql = "
                    SELECT a.id,
                     a.siklus,
                     a.kode_olah
                    FROM ( SELECT DISTINCT ON (log_task_pengolahan.siklus, log_task_pengolahan.kode_olah) log_task_pengolahan.id,
                            log_task_pengolahan.siklus,
                            log_task_pengolahan.kode_olah
                           FROM log_task_pengolahan
                          WHERE log_task_pengolahan.nomo::text = :nomo AND siklus = :siklus AND log_task_pengolahan.status IS NULL) a
                    ORDER BY a.siklus, (COALESCE(substring(a.kode_olah::text, '^(\d+)'::text)::integer, 99999999)), (substring(a.kode_olah::text, '[a-zA-z_-]+'::text)), (COALESCE(substring(a.kode_olah::text, '(\d+)$'::text)::integer, 0))
                    LIMIT 1";

            $queue = LogTaskPengolahan::findBySql($sql,[':nomo'=>$nomo,':siklus'=>$siklus])->one();

        }

        $bb = LogTaskPengolahan::find()->where(['id_bb'=>$id_bb])->one();

        //Check id siklus and kode_olah bb is in queue
        if ($bb->siklus == $queue->siklus and $bb->kode_olah == $queue->kode_olah){
            echo 1;
        }else{
            echo 0;
        }

        // Check if operator klik BB not by order
        // $sql="
        //     SELECT *
        //     FROM log_task_pengolahan
        //     WHERE id_bb = '".$id_bb."'
        //     ";

        // $task = LogTaskPengolahan::findBySql($sql)->one();
        // $last_serial_checked = $task->serial_manual -1;

        // if ($siklus == 0){
        //     $sql="
        //         SELECT *
        //         FROM log_task_pengolahan
        //         WHERE nomo = '".$nomo."' and serial_manual = ".$last_serial_checked."
        //         ";

        //     $last_task_checked = LogTaskPengolahan::findBySql($sql)->one();            
        // }else{
        //     $sql="
        //         SELECT *
        //         FROM log_task_pengolahan
        //         WHERE nomo = '".$nomo."' and siklus = '".$siklus."' and serial_manual = ".$last_serial_checked."
        //         ";

        //     $last_task_checked = LogTaskPengolahan::findBySql($sql)->one();
        // }

        // if (!empty($last_task_checked) && empty($last_task_checked->status)){
        //     echo 0;
        // }else{
        //     echo 1;
        // }
    }

    // public function actionScanBb($id,$wo)
    // {
    //     $sql="
    //         SELECT *
    //         FROM log_task_pengolahan
    //         WHERE id = '".$wo."'
    //         ";

    //     $task = LogTaskPengolahan::findBySql($sql)->one();

    //     // Get Current Selected Operator
    //     $sql2="
    //             SELECT
    //                  *
    //             FROM flow_input_mo
    //             WHERE id = ".$id;

    //     $op = FlowInputMo::findBySql($sql2)->one();

    //     $operator = $op->nama_operator;

    //     return $this->render('scan-bb', [
    //         'task' => $task,
    //         'op' => $op,
    //         'operator' => $operator,
    //         'id'=>$id,
    //         'id_bb'=>$task->id_bb,
    //         'wo'=>$wo,
    //         'nomo'=>$task->nomo,
    //     ]);
    // }

    public function actionInputBbManual($id)
    {
        // $sql="
        //     SELECT *
        //     FROM log_task_pengolahan
        //     WHERE id = '".$wo."'
        //     ";

        // $task = LogTaskPengolahan::findBySql($sql)->one();

        // Get Current Selected Operator
        $sql2="
                SELECT
                     *
                FROM flow_input_mo
                WHERE id = ".$id;

        $op = FlowInputMo::findBySql($sql2)->one();

        $operator = $op->nama_operator;
        $nomo = $op->nomo;
        $siklus = $op->siklus;
        if (empty($siklus)){
            $siklus = 0;
        }

        return $this->render('_input-bb-manual', [
            'op' => $op,
            'operator' => $operator,
            'id'=>$id,
            'siklus'=>$siklus,
            'nomo'=>$nomo,
        ]);
    }

    public function actionCheckBb($fim_id,$id_bb)
    {
        $sql_fim="select * from flow_input_mo where id= :id";
        $check_fim = FlowInputMo::findBySql($sql_fim,[':id'=>$fim_id])->one();
        
        $sql="select * from log_task_pengolahan where nomo = :nomo and id_bb=:id_bb";
        $check= LogTaskPengolahan::findBySql($sql,[':id_bb'=>$id_bb,':nomo'=>$check_fim->nomo])->one();

        if (!empty($check)) {
            if ($check->status == 'OK'){
                echo 2;
            }else{
                echo 1;
            }
        } else {
            echo 0;
        }
    }

    public function actionCheckBbManual($nomo,$siklus,$kode_internal,$kode_olah,$qty,$uom){
        if ($siklus == 0){

            $sql = "
            SELECT id,id_bb,status
            FROM log_task_pengolahan
            WHERE  nomo = :nomo 
                    and kode_internal = :kode_internal  
                    and kode_olah = :kode_olah
                    and qty = :qty 
                    and uom = :uom 
                    ";

            $row = LogTaskPengolahan::findBySql($sql,
                        [
                            ':nomo'=>$nomo,
                            ':kode_internal'=>$kode_internal,
                            ':kode_olah'=>$kode_olah,
                            ':qty'=>$qty,
                            ':uom'=>$uom,
                        ])->one();                
        } else {
            $sql = "
            SELECT id,id_bb,status
            FROM log_task_pengolahan
            WHERE  nomo = :nomo 
                    and siklus = :siklus  
                    and kode_internal = :kode_internal  
                    and kode_olah = :kode_olah
                    and qty = :qty 
                    and uom = :uom 
                    ";

            $row = LogTaskPengolahan::findBySql($sql,
                        [
                            ':nomo'=>$nomo,
                            ':siklus'=>$siklus,
                            ':kode_internal'=>$kode_internal,
                            ':kode_olah'=>$kode_olah,
                            ':qty'=>$qty,
                            ':uom'=>$uom,
                        ])->one();  
        }

        if (!empty($row)){
            if(empty($row->status)){
                $value = ["status"=>"exist","id_bb"=>$row->id_bb];
            }else{
                $value = ["status"=>"scanned"];
            }
        }else{
            $value = ["status"=>"not-exist"];
        }

        echo Json::encode($value);


           
    }

    public function actionUpdateStatusBbManual($id,$log,$pic){
        $sql = "
        UPDATE log_task_pengolahan
        SET status = 'OK',
            log = 'manual-detail',
            pic = '".$pic."',
            timestamp_checked = current_timestamp
        WHERE  id = ".$id." ";

        $row = Yii::$app->db->createCommand($sql)->execute(); 

        if ($row>0){
            echo 1;
        }else{
            echo 0;
        }
    }

    public function actionGetLanjutanPenimbangan($nomo,$jenis_penimbangan,$posisi)
    {
        $sql="
        SELECT
            coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM flow_input_mo
        WHERE nomo = '".$nomo."'
        and jenis_penimbangan = '".$jenis_penimbangan."'
        and posisi = '".$posisi."'
        ";

        $lanjutan= FlowInputMo::findBySql($sql)->one();
        echo Json::encode($lanjutan);
    }

    public function actionFree($stock,$input,$threshold,$free)
    {

        // Calculate Expected Actual Qty Received (Free Product + Initial Demand)
        $multiplier = floor($input/$threshold);
        $freeqty = $multiplier*$free;
        $result = $input+$freeqty;


        // If Stock Level is above Expected Actual Qty Received then Expected Actual Qty Received
        if($stock>=$result){
            echo $result;
        }
        // Else If Stock Level is below the Expected Actual Qty Received, Then Decrease the Multiplier and Compare to Stock
        else if($stock<$result){

            while($stock<$result){
                $multiplier  = $multiplier - 1;
                $result = ($multiplier*$threshold)+($free*$multiplier);
            }
            echo $result;

        }

    }


    public function actionGetLanjutanPengolahan($nomo,$jenis_pengolahan,$posisi)
    {
        $sql="
        SELECT
            coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM flow_input_mo
        WHERE nomo = '".$nomo."'
        and jenis_penimbangan = '".$jenis_pengolahan."'
        and posisi = '".$posisi."'
        ";

        $lanjutan= FlowInputMo::findBySql($sql)->one();
        echo Json::encode($lanjutan);
    }

    public function actionSendDataToChecker($id)
    {
        $fim = $this->findModel($id);
        
        if ($fim->siklus==0){
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

            SELECT 
                *
            FROM log_penimbangan_rm
            WHERE nomo = '".$fim->nomo."' 
            ORDER BY siklus, (COALESCE(substring(kode_olah::text, '^(\d+)'::text)::integer, 99999999)), 
                (substring(kode_olah::text, '[a-zA-z_-]+'::text)), 
                (COALESCE(substring(kode_olah::text, '(\d+)$'::text)::integer, 0)),
                coalesce(left(substring(nama_bb from '\[(.+)\]'),position('/' in substring(nama_bb from '\[(.+)\]'))-1)::integer,0)
                ");

            $list_formula = $command->queryAll();

            $connection2 = Yii::$app->db;
            $command2 = $connection2->createCommand("

            SELECT 
                id_bb,id
            FROM log_task_checker
            WHERE nomo = '".$fim->nomo."' ORDER BY kode_olah ASC");

            $list_task_checker = $command2->queryAll();     

            $list_bb_checker = [];
            foreach ($list_task_checker as $list){
                $list_bb_checker[] = $list['id_bb'];
            }       


            $connection3 = Yii::$app->db;
            $command3 = $connection3->createCommand("

            SELECT 
                id_bb,id
            FROM log_task_pengolahan
            WHERE nomo = '".$fim->nomo."' ORDER BY kode_olah ASC");

            $list_task_pengolahan= $command3->queryAll();

            $list_bb_olah = [];

            if (!empty($list_task_pengolahan)){
                foreach ($list_task_pengolahan as $list){
                    $list_bb_olah[] = $list['id_bb'];
                }

                $connection3 = Yii::$app->db;
                $command3 = $connection3->createCommand("

                SELECT 
                    coalesce(max(serial_manual),0) as max
                FROM log_task_pengolahan
                WHERE nomo = '".$fim->nomo."' ");

                $max_serial= $command3->queryOne()['max'];
                $serial_manual = $max_serial + 1; 
            } else {
                $serial_manual = 1;
            }           

            for ($x=0; $x < count($list_formula); $x++)
            {
                if ($list_formula[$x]['kode_bb'] == 'AIR-RO--L')
                {
                    $status = "OK";
                    $pic = "System";
                } else {
                    $status = NULL;
                    $pic = NULL;
                }

                //INSERT INTO LOG TASK CHECKER
                if (!in_array($list_formula[$x]['id_bb'], $list_bb_checker)){
                    $connection2 = Yii::$app->getDb(); 
                    $command2 = $connection2->createCommand("

                    INSERT INTO log_task_checker(kode_bb,nama_bb,qty,uom,kode_olah,status,pic,nomo,siklus,id_bb,kode_internal)
                    SELECT  :kode_bb as kode_bb, 
                            :nama_bb as nama_bb,
                            :qty as qty, 
                            :uom as uom,
                            :kode_olah as kode_olah,
                            :status as status,
                            :pic as pic,
                            :nomo as nomo,
                            :siklus as siklus,
                            :id_bb as id_bb,
                            :kode_internal as kode_internal", 
                    
                    [':kode_bb' => $list_formula[$x]['kode_bb'],
                     ':nama_bb' => $list_formula[$x]['nama_bb'],
                     ':qty' => $list_formula[$x]['weight'],
                     ':uom' => $list_formula[$x]['satuan'],
                     ':kode_olah' => $list_formula[$x]['kode_olah'],
                     ':status' => $status,
                     ':pic' => $pic,
                     ':nomo' => $fim->nomo,
                     ':siklus' => $list_formula[$x]['siklus'],
                     ':id_bb' => $list_formula[$x]['id_bb'],
                     ':kode_internal' => $list_formula[$x]['kode_internal'],
                    ]);

                    $result2 = $command2->queryAll();                    
                }else{
                    $result2 = true;                    
                }
               
                //INSERT INTO LOG TASK PENGOLAHAN
                if(!in_array($list_formula[$x]['id_bb'],$list_bb_olah)){
                    
                    $connection3 = Yii::$app->getDb(); 
                    $command3 = $connection3->createCommand("

                    INSERT INTO log_task_pengolahan(kode_bb,nama_bb,qty,uom,kode_olah,status,pic,nomo,siklus,id_bb,serial_manual,kode_internal)
                    SELECT  :kode_bb as kode_bb, 
                            :nama_bb as nama_bb,
                            :qty as qty, 
                            :uom as uom,
                            :kode_olah as kode_olah,
                            :status as status,
                            :pic as pic,
                            :nomo as nomo,
                            :siklus as siklus,
                            :id_bb as id_bb,
                            :serial_manual as serial_manual,
                            :kode_internal as kode_internal", 
                    
                    [':kode_bb' => $list_formula[$x]['kode_bb'],
                     ':nama_bb' => $list_formula[$x]['nama_bb'],
                     ':qty' => $list_formula[$x]['weight'],
                     ':uom' => $list_formula[$x]['satuan'],
                     ':kode_olah' => $list_formula[$x]['kode_olah'],
                     ':status' => $status,
                     ':pic' => $pic,
                     ':nomo' => $fim->nomo,
                     ':siklus' => $list_formula[$x]['siklus'],
                     ':id_bb' => $list_formula[$x]['id_bb'],
                     ':serial_manual' => $serial_manual,
                     ':kode_internal' => $list_formula[$x]['kode_internal'],
                    ]);

                    $result3 = $command3->queryAll();  
                    $serial_manual++;                  
                }else{
                    $result3 = true;                    

                }

            }
        }else if ($fim->siklus > 0){
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

            SELECT 
                *
            FROM log_penimbangan_rm
            WHERE nomo = '".$fim->nomo."' and siklus = '".$fim->siklus."' ORDER BY kode_olah ASC");

            $list_formula = $command->queryAll();

            $connection2 = Yii::$app->db;
            $command2 = $connection2->createCommand("

            SELECT 
                id_bb,id
            FROM log_task_checker
            WHERE nomo = '".$fim->nomo."' and siklus = '".$fim->siklus."' ORDER BY kode_olah ASC");

            $list_task_checker = $command2->queryAll();     

            $list_bb_checker = [];
            foreach ($list_task_checker as $list){
                $list_bb_checker[] = $list['id_bb'];
            }       


            $connection3 = Yii::$app->db;
            $command3 = $connection3->createCommand("

            SELECT 
                id_bb,id
            FROM log_task_pengolahan
            WHERE nomo = '".$fim->nomo."' and siklus = '".$fim->siklus."' ORDER BY kode_olah ASC");

            $list_task_pengolahan= $command3->queryAll();  

            $list_bb_olah = [];

            //GET LIST BB THAT ALREADY INSERT
            if (!empty($list_task_pengolahan)){
                foreach ($list_task_pengolahan as $list){
                    $list_bb_olah[] = $list['id_bb'];
                }
            }

            //GET SERIAL MANUAL
            $connection3 = Yii::$app->db;
            $command3 = $connection3->createCommand("

            SELECT 
                coalesce(max(serial_manual),0) as max
            FROM log_task_pengolahan
            WHERE nomo = '".$fim->nomo."' ");

            $max_serial= $command3->queryOne()['max'];
            $serial_manual = $max_serial + 1;         

            for ($x=0; $x < count($list_formula); $x++)
            {
                if ($list_formula[$x]['kode_bb'] == 'AIR-RO--L')
                {
                    $status = "OK";
                    $pic = "System";
                } else {
                    $status = NULL;
                    $pic = NULL;
                }

                //INSERT INTO LOG TASK CHECKER
                if (!in_array($list_formula[$x]['id_bb'],$list_bb_checker)){
                    $connection2 = Yii::$app->getDb(); 
                    $command2 = $connection2->createCommand("

                    INSERT INTO log_task_checker(kode_bb,nama_bb,qty,uom,kode_olah,status,pic,nomo,siklus,id_bb,kode_internal)
                    SELECT  :kode_bb as kode_bb, 
                            :nama_bb as nama_bb,
                            :qty as qty, 
                            :uom as uom,
                            :kode_olah as kode_olah,
                            :status as status,
                            :pic as pic,
                            :nomo as nomo,
                            :siklus as siklus,
                            :id_bb as id_bb,
                            :kode_internal as kode_internal", 
                    
                    [':kode_bb' => $list_formula[$x]['kode_bb'],
                     ':nama_bb' => $list_formula[$x]['nama_bb'],
                     ':qty' => $list_formula[$x]['weight'],
                     ':uom' => $list_formula[$x]['satuan'],
                     ':kode_olah' => $list_formula[$x]['kode_olah'],
                     ':status' => $status,
                     ':pic' => $pic,
                     ':nomo' => $fim->nomo,
                     ':siklus' => $list_formula[$x]['siklus'],
                     ':id_bb' => $list_formula[$x]['id_bb'],
                     ':kode_internal' => $list_formula[$x]['kode_internal'],
                    ]);

                    $result2 = $command2->queryAll();

                }else{
                    $result2 = true;
                }

                //INSERT INTO LOG TASK PENGOLAHAN
                if (!in_array($list_formula[$x]['id_bb'],$list_bb_olah)){
                    $connection3 = Yii::$app->getDb(); 
                    $command3 = $connection3->createCommand("

                    INSERT INTO log_task_pengolahan (kode_bb,nama_bb,qty,uom,kode_olah,status,pic,nomo,siklus,id_bb,serial_manual,kode_internal)
                    SELECT  :kode_bb as kode_bb, 
                            :nama_bb as nama_bb,
                            :qty as qty, 
                            :uom as uom,
                            :kode_olah as kode_olah,
                            :status as status,
                            :pic as pic,
                            :nomo as nomo,
                            :siklus as siklus,
                            :id_bb as id_bb,
                            :serial_manual as serial_manual,
                            :kode_internal as kode_internal", 
                    
                    [':kode_bb' => $list_formula[$x]['kode_bb'],
                     ':nama_bb' => $list_formula[$x]['nama_bb'],
                     ':qty' => $list_formula[$x]['weight'],
                     ':uom' => $list_formula[$x]['satuan'],
                     ':kode_olah' => $list_formula[$x]['kode_olah'],
                     ':status' => $status,
                     ':pic' => $pic,
                     ':nomo' => $fim->nomo,
                     ':siklus' => $list_formula[$x]['siklus'],
                     ':id_bb' => $list_formula[$x]['id_bb'],
                     ':serial_manual' => $serial_manual,
                     ':kode_internal' => $list_formula[$x]['kode_internal'],
                    ]);
                    $result3 = $command3->queryAll();
                    $serial_manual++;
                }else{
                    $result3=true;
                }
            }
        }else{
            $result2 =true;
        }
        
        if ($result2){
            echo 1;
        } else {
            echo 0;
        }

    }


    public function actionStopPenimbangan($nomo,$last_id) 
    {

        $model = new FlowInputMo();

        // Populate Grid Data
        $searchModel = new FlowInputMoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("nomo='".$nomo."' and posisi='PENIMBANGAN'");

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");

        $this->layout = '//main-sidebar-collapse';

        // Check If Post Value True
        if ($model->load(Yii::$app->request->post())){

            $array_id = FlowInputMo::find()->where(['id' => $last_id])->one();
            $jenis_proses = $array_id->jenis_penimbangan;

            $staging=Yii::$app->request->post('FlowInputMo')['staging'];
            if(empty($staging)){
                $staging=null;
            }

            // Get Staging value
            // $nama_operator=Yii::$app->request->post('FlowInputMo')['nama_operator'];
            // if(empty($nama_operator)){
            //     $nama_operator=null;
            // }

            // Get Is Done Value
            $is_done=Yii::$app->request->post('FlowInputMo')['is_done'];

            // Insert Is Done Value to status_jadwal
            if(!empty($is_done)){

                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("

                INSERT INTO status_jadwal (nojadwal,status,table_name,posisi,jenis_proses)
                SELECT
                    :nomo as nojadwal,
                    1 as status,
                    'flow_input_mo' as table_name,
                    'PENIMBANGAN' as posisi,
                    :jenis_proses as jenis_proses
                ON CONFLICT (nojadwal,posisi,jenis_proses)
                DO NOTHING;
                ",
                [':nomo' => $nomo,':jenis_proses' => $jenis_proses]);

                $result = $command->queryAll();

                $status_ = LogPenimbanganRmController::actionInsertLogFroMrp($nomo,$last_id,$is_done);
                if ($status_ == 'ready'){
                    LogPenimbanganRmController::actionInsertLogFroMrpDetail($nomo);
                }
            }

            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE flow_input_mo
            SET datetime_stop = :current_time,
                datetime_write = :current_time,
                staging = :staging
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i A'),
             ':id'=> $last_id,
             ':staging'=>$staging
            ]);

            $result = $command->queryAll();

            $this->actionSendDataToChecker($last_id);

            // Redirect to Downtime
            return $this->redirect(['downtime/index-penimbangan', 'id' => $last_id]);
            // return $this->redirect(['create-kemas1', 'snfg_komponen' => $snfg_komponen]);
            // return $this->redirect(['check-kemas1']);

        } else {
            return $this->render('_form-penimbangan-stop', [
                'model' => $model,
                'nomo' => $nomo,
                'last_id' => $last_id,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,

            ]);
        }

        // date_default_timezone_set('Asia/Jakarta');
        // $current_time = date("H:i:s");

        // $connection = Yii::$app->getDb();
        // $command = $connection->createCommand("

        //     UPDATE flow_input_mo
        //     SET datetime_stop = :current_time,
        //         datetime_write = :current_time
        //     WHERE id = :id;
        //     ",
        //     [':current_time' => date('Y-m-d h:i A'),':id'=> $last_id]);

        // $result = $command->queryAll();

        // // Redirect to Downtime
        // return $this->redirect(['downtime/index-penimbangan', 'id' => $last_id]);

        // // Redirect to Referrer
        // // return $this->redirect(Yii::$app->request->referrer);

    }

    public function actionStopPengolahan($nomo,$last_id)
    {
        $model = new FlowInputMo();

        // Populate Grid Data
        $searchModel = new FlowInputMoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("nomo='".$nomo."' and posisi='PENGOLAHAN'");

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");

        $this->layout = '//main-sidebar-collapse';

        // Check If Post Value True
        if ($model->load(Yii::$app->request->post())){

            // Jenis Proses
            $array_id = FlowInputMo::find()->where(['id' => $last_id])->one();
            $jenis_proses = $array_id->jenis_penimbangan;


            // Get Staging value
            $staging=Yii::$app->request->post('FlowInputMo')['staging'];
            if(empty($staging)){
                $staging=null;
            }


            // Get Staging value
            // $nama_operator=Yii::$app->request->post('FlowInputMo')['nama_operator'];
            // if(empty($nama_operator)){
            //     $nama_operator=null;
            // }

            // Get Is Done Value
            $is_done=Yii::$app->request->post('FlowInputMo')['is_done'];

            // Insert Is Done Value to status_jadwal
            if(!empty($is_done)){

                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("

                INSERT INTO status_jadwal (nojadwal,status,table_name,posisi,jenis_proses)
                SELECT
                    :nomo as nojadwal,
                    1 as status,
                    'flow_input_mo' as table_name,
                    'PENGOLAHAN' as posisi,
                    :jenis_proses as jenis_proses
                ON CONFLICT (nojadwal,posisi,jenis_proses)
                DO NOTHING;
                ",
                [':nomo' => $nomo, ':jenis_proses' => $jenis_proses]);

                $result = $command->queryAll();
            }

            // Assign Value to the ID
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE flow_input_mo
            SET datetime_stop = :current_time,
                datetime_write = :current_time,
                staging = :staging
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i A'),
             ':id'=> $last_id,
             ':staging'=>$staging
            ]);

            $result = $command->queryAll();

            // Insert Pengolahan Batch (SNFG_KOMPONEN VALUE)
            $connection2 = Yii::$app->getDb();
            $command2 = $connection2->createCommand("
                INSERT INTO pengolahan_batch (pengolahan_id,snfg_komponen)
                SELECT fim.id as pengolahan_id,sp.snfg_komponen as snfg_komponen
                FROM flow_input_mo fim
                INNER JOIN scm_planner sp on sp.nomo = fim.nomo
                WHERE fim.id = :id
                ORDER BY sp.snfg_komponen;
                ",
                [':id'=> $last_id]
            );

            $result2 = $command2->queryAll();

            // Redirect to Downtime
            return $this->redirect(['downtime/index-pengolahan', 'id' => $last_id]);
            // return $this->redirect(['create-kemas1', 'snfg_komponen' => $snfg_komponen]);
             // return $this->redirect(['check-kemas1']);
        } else {
            return $this->render('_form-pengolahan-stop', [
                'model' => $model,
                'nomo' => $nomo,
                'last_id' => $last_id,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,

            ]);
        }
    }

    public function actionCreatePengolahan($nomo)
    {


        $mod = new ScmPlanner();

        $result = $mod->periksaStatusNomo($nomo);

        if($result=='UNHOLD'){


            $model = new FlowInputMo();

            // Populate Grid Data
            $searchModel = new FlowInputMoSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where("nomo='".$nomo."' and posisi='PENGOLAHAN'");

            $this->layout = '//main-sidebar-collapse';

            //Check Last State of a Nomo
            $sql="

            SELECT
                distinct on (nomo)
                id,
                nomo,
                datetime_start,
                datetime_stop,
                nama_line
            FROM flow_input_mo
            WHERE nomo = '".$nomo."' and posisi='PENGOLAHAN'
            ORDER BY nomo,id desc
            ";

            $row = FlowInputMo::findBySql($sql)->one();

            // Assign Variable Null to prevent error if it's the first data entry
            if(empty($row)){
                $last_datetime_start = null;
                $last_datetime_stop = null;
                $last_id = null;
                $nama_line = null;
            }else{
                $last_datetime_start = $row->datetime_start;
                $last_datetime_stop = $row->datetime_stop;
                $last_id = $row->id;
                $nama_line = $row->nama_line;
            }

            // Jenis Proses Check Status Jadwal, If Found then Don't Display
                $sql3="
                    SELECT
                        jenis_proses_olah
                    FROM jenis_proses_olah
                    WHERE jenis_proses_olah not in
                    (SELECT jenis_proses FROM status_jadwal WHERE
                    nojadwal='".$nomo."' and posisi='PENGOLAHAN')
                ";

            $jenis_proses_list = ArrayHelper::map(JenisProsesOlah::findBySql($sql3)->all(), 'jenis_proses_olah','jenis_proses_olah');

            // Check If Snfg Last Status is Start, if yes then redirect to actionStopPengolahan
            if(!empty($last_datetime_start)&&(empty($last_datetime_stop))){
                return $this->redirect(['stop-pengolahan','nomo' => $nomo,'last_id'=>$last_id]);
            }

            //Generate batch pengolahan
            $nobatch = $this->actionGenerateBatchOlah($nomo);

            // Check If Post Value True
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['check-pengolahan']);
            } else {
                return $this->render('create-pengolahan', [
                    'model' => $model,
                    'nomo' => $nomo,
                    'last_datetime_start' => $last_datetime_start,
                    'last_datetime_stop' => $last_datetime_stop,
                    'last_id' => $last_id,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'nama_line' => $nama_line,
                    'jenis_proses_list' => $jenis_proses_list,
                    'nobatch' => $nobatch,
                ]);
            }


        }else{
            throw new \yii\web\HttpException(401,
          'Jadwal telah di HOLD / CANCEL mohon hubungi Planner');
        }
    }

    public function actionCreatePengolahanNew($nomo)
    {


        $mod = new ScmPlanner();

        $result = $mod->periksaStatusNomo($nomo);

        if($result=='UNHOLD'){


            $model = new FlowInputMo();

            // Populate Grid Data
            $searchModel = new FlowInputMoSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where("nomo='".$nomo."' and posisi='PENGOLAHAN'");

            $this->layout = '//main-sidebar-collapse';

            //Check Last State of a Nomo
            $sql="

            SELECT
                distinct on (nomo)
                id,
                nomo,
                datetime_start,
                datetime_stop,
                nama_line,
                siklus
            FROM flow_input_mo
            WHERE nomo = '".$nomo."' and posisi='PENGOLAHAN'
            ORDER BY nomo,id desc
            ";

            $row = FlowInputMo::findBySql($sql)->one();

            // Assign Variable Null to prevent error if it's the first data entry
            if(empty($row)){
                $last_datetime_start = null;
                $last_datetime_stop = null;
                $last_id = null;
                $nama_line = null;
            }else{
                $last_datetime_start = $row->datetime_start;
                $last_datetime_stop = $row->datetime_stop;
                $last_id = $row->id;
                $nama_line = $row->nama_line;
            }

            // Jenis Proses Check Status Jadwal, If Found then Don't Display
                $sql3="
                    SELECT
                        jenis_proses_olah
                    FROM jenis_proses_olah
                    WHERE jenis_proses_olah not in
                    (SELECT jenis_proses FROM status_jadwal WHERE
                    nojadwal='".$nomo."' and posisi='PENGOLAHAN')
                ";

            $jenis_proses_list = ArrayHelper::map(JenisProsesOlah::findBySql($sql3)->all(), 'jenis_proses_olah','jenis_proses_olah');

            // Check If Snfg Last Status is Start, if yes then redirect to actionStopPengolahan
            if(!empty($last_datetime_start)&&(empty($last_datetime_stop))){
                return $this->redirect(['task','nomo' => $nomo,'siklus'=>$row->siklus]);
            } 

            //Generate batch pengolahan
            $nobatch = $this->actionGenerateBatchOlah($nomo);

            // Check If Post Value True
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['task','nomo' => $nomo,'siklus'=>$model->siklus]);
                // return $this->redirect(['check-pengolahan-new']);
            } else {
                return $this->render('create-pengolahan-new', [
                    'model' => $model,
                    'nomo' => $nomo,
                    'last_datetime_start' => $last_datetime_start,
                    'last_datetime_stop' => $last_datetime_stop,
                    'last_id' => $last_id,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'nama_line' => $nama_line,
                    'jenis_proses_list' => $jenis_proses_list,
                    'nobatch' => $nobatch,
                ]);
            }


        }else{
            throw new \yii\web\HttpException(401,
          'Jadwal telah di HOLD / CANCEL mohon hubungi Planner');
        }
    }

    public function actionTask($nomo,$siklus)
    {
        $this->layout = '//main-sidebar-collapse';
        $sql = "SELECT nomo, nama_bulk,snfg
                FROM scm_planner 
                WHERE nomo = '".$nomo."' ";
        $sp = ScmPlanner::findBySql($sql)->one();

        if ($siklus == 0){

            $sql = "SELECT *
                FROM log_task_pengolahan WHERE nomo = '".$nomo."'
                order by siklus, (COALESCE(substring(kode_olah::text, '^(\d+)'::text)::integer, 99999999)), 
                (substring(kode_olah::text, '[a-zA-z_-]+'::text)), 
                (COALESCE(substring(kode_olah::text, '(\d+)$'::text)::integer, 0)),
                coalesce(left(substring(nama_bb from '\[(.+)\]'),position('/' in substring(nama_bb from '\[(.+)\]'))-1)::integer,0)
            ";
            $data = LogTaskPengolahan::findBySql($sql)->all();
            
            $dataProvider= new ArrayDataProvider([
                'allModels'=>$data,
                'pagination'=>false,
            ]);


            $searchModel = new LogTaskPengolahanSearch();
            // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // // $dataProvider->query->where("nomo='".$nomo."'")->orderBy(['kode_olah'=>SORT_ASC,'id'=>SORT_ASC]);
            // $dataProvider->query->where("nomo='".$nomo."'")
            // ->orderBy("siklus, (COALESCE(substring(kode_olah::text, '^(\d+)'::text)::integer, 99999999)), 
            //             (substring(kode_olah::text, '[a-zA-z_-]+'::text)), 
            //             (COALESCE(substring(kode_olah::text, '(\d+)$'::text)::integer, 0))");
            // $dataProvider->pagination = false;


            $sql = "
            SELECT *
            FROM flow_input_mo
            WHERE  nomo = '".$nomo."' and posisi = 'PENGOLAHAN' order by id desc";

            $row = FlowInputMo::findBySql($sql)->one();            
        }else{

            $sql = "SELECT *
                FROM log_task_pengolahan WHERE nomo = '".$nomo."' and siklus = '".$siklus."'
                order by siklus, (COALESCE(substring(kode_olah::text, '^(\d+)'::text)::integer, 99999999)), 
                (substring(kode_olah::text, '[a-zA-z_-]+'::text)), 
                (COALESCE(substring(kode_olah::text, '(\d+)$'::text)::integer, 0)),
                coalesce(left(substring(nama_bb from '\[(.+)\]'),position('/' in substring(nama_bb from '\[(.+)\]'))-1)::integer,0)
                ";
            $data = LogTaskPengolahan::findBySql($sql)->all();
            
            $dataProvider= new ArrayDataProvider([
                'allModels'=>$data,
                'pagination'=>false,
            ]);

            $searchModel = new LogTaskPengolahanSearch();
            // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // $dataProvider->query->where("nomo='".$nomo."' and siklus = '".$siklus."' ")->orderBy(['kode_olah'=>SORT_ASC]);
            // $dataProvider->pagination = false;


            $sql = "
            SELECT *
            FROM flow_input_mo
            WHERE  nomo = '".$nomo."' and siklus = '".$siklus."' and posisi = 'PENGOLAHAN' order by id desc";

            $row = FlowInputMo::findBySql($sql)->one();
        }

        $operator = $row->nama_operator;
        $nobatch = $row->nobatch;


        return $this->render('task', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'nomo' => $nomo,
            'operator' => $operator,
            'siklus' => $siklus,
            'nobatch' => $nobatch,
            'fim_id' => $row->id,
            'sp' => $sp,
        ]);
    }

    public function actionGenerateBatchOlah($nomo)
    {
        // $record_olah = Yii::$app->db->createCommand("SELECT * FROM 
        //                                                 (SELECT fim.id,fim.nomo,pb.nobatch 
        //                                                     FROM flow_input_mo fim 
        //                                                     JOIN pengolahan_batch pb ON fim.id = pb.pengolahan_id 
        //                                                     WHERE fim.nomo = '".$nomo."'
        //                                                 ) data 
        //                                             WHERE nobatch is not null")->queryOne();
        
        date_default_timezone_set('Asia/Jakarta');
        
        $record_olah = Yii::$app->db->createCommand("SELECT * FROM flow_input_mo 
                                                    WHERE nomo = '".$nomo."' AND nobatch is not null ORDER BY datetime_start ASC")->queryOne();
        
        if (empty($record_olah)){

            $koitem_bulk = ScmPlanner::find()->where(['nomo'=>$nomo])->one()['koitem_bulk'];
            // $first_record = FlowInputMo::find()->where("nomo = '".$nomo."' and posisi = 'PENGOLAHAN'")->orderBy(['datetime_start'=>SORT_ASC])->one();
            $first_record['datetime_start'] = date('Y-m-d H:i:s');

            $year_mapping = [1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',6=>'F',7=>'G',
                              8=>'H',9=>'I',10=>'J',11=>'K',12=>'L',13=>'M',14=>'N',
                              15=>'O',16=>'P',17=>'Q',18=>'R',19=>'S',20=>'T',21=>'U',
                              22=>'V',23=>'W',24=>'X',25=>'Y',26=>'Z'];
    
            $month_mapping = [1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',6=>'F',7=>'G',
                              8=>'H',9=>'I',10=>'J',11=>'K',12=>'L'];
    
            $serial_mapping = [0=>'0',1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',
                                6=>'F',7=>'G',8=>'H',9=>'I',10=>'J',11=>'K'];
    
            $current_year = (int)date('Y',strtotime($first_record['datetime_start']));
            $current_month = (int)date('m',strtotime($first_record['datetime_start']));
            $current_date = date('d',strtotime($first_record['datetime_start']));
            // var_dump ($first_record['datetime_start']);
            // var_dump ($current_year);
            
            //Buat mappping tahun berdasarkan acuan 2017 -> D
            $y = $year_mapping[$current_year - 2017 + 4];
    
            $m = $month_mapping[$current_month];
            $d = $current_date;
            // var_dump ($d);
    
            //Get NOMO tanpa sn -> ex:'/F0001'
            $nomo_without_serial = explode('/',$nomo)[0];
            for ($serial=1; $serial<= 10; $serial++)
            {
                $nobatch = $m.$y.$d.$serial_mapping[$serial];
                // $check = Yii::$app->db->createCommand("SELECT pb.* FROM pengolahan_batch pb LEFT JOIN
                //                                 flow_input_mo fim ON pb.pengolahan_id = fim.id
                //                                 WHERE nomo LIKE '".$nomo_without_serial."%' AND pb.nobatch = '".$nobatch."'
                //                                  ")->queryOne();
                $check = Yii::$app->db->createCommand("SELECT * FROM flow_input_mo
                                                 WHERE nomo LIKE '".$nomo_without_serial."%' AND nobatch = '".$nobatch."'
                                                  ")->queryOne();
                if (empty($check)){
                  break;
                }
                // print_r('<pre>');
                // print_r($check);
                // print_r($serial);
                // print_r($nobatch);
            }
        }else{
            $nobatch = $record_olah['nobatch'];
        }
        return $nobatch;
    }

    public function actionReadBatchOlah($nomo)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $record_olah = Yii::$app->db->createCommand("SELECT nobatch FROM flow_input_mo 
                                                    WHERE nomo = '".$nomo."' AND nobatch is not null ORDER BY datetime_start ASC")->queryOne();
        
        $nobatch = $record_olah['nobatch'];
        $value = ['nobatch'=>$nobatch];
        echo Json::encode($value);
    }

    /**
     * Updates an existing FlowInputMo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $sql="select
                        posisi
              from flow_input_mo where id=".$id;
        $pp= FlowInputMo::findBySql($sql)->one();
        $posisi = $pp->posisi;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'posisi' => $posisi,
            ]);
        }
    }

    public function actionGetLogInputPenimbangan($nojadwal)
    {
        $sql="select
                        datetime_write,
                        nomo,
                        jenis_penimbangan,
                        lanjutan,
                        nama_operator,
                        id
              from flow_input_mo where nomo='".$nojadwal."'
              and posisi='PENIMBANGAN'";
        $pp= FlowInputMo::findBySql($sql)->all();
        // print_r($pprr);
        echo Json::encode($pp);

    }

    public function actionGetLogInputPengolahan($nojadwal)
    {
        $sql="select
                        datetime_write,
                        nomo,
                        jenis_penimbangan,
                        lanjutan,
                        nama_operator,
                        id
              from flow_input_mo where nomo='".$nojadwal."'
              and posisi='PENGOLAHAN'";
        $pp= FlowInputMo::findBySql($sql)->all();
        // print_r($pprr);
        echo Json::encode($pp);

    }


    /**
     * Deletes an existing FlowInputMo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the FlowInputMo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FlowInputMo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FlowInputMo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCreatePenimbanganRm($nomo)
    {
        // print_r('teslkjak');
        // exit();
        $mod = new ScmPlanner();

        $result = $mod->periksaStatusNomo($nomo);

        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            *
        FROM device_map_rm
        WHERE device_ip = '".$ip."'";

        $map_device_array = DeviceMapRm::findBySql($sql)->one();

        $operator = $map_device_array->operator;
        $nama_line = $map_device_array->nama_line;

        if($result=='UNHOLD'){
            // print_r('expression');
            // exit();
            //if ($operator == 1){
            $model = new FlowInputMo();

            // Populate Grid Data
            $searchModel = new FlowInputMoSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where("nomo='".$nomo."' and posisi='PENIMBANGAN'");

            $this->layout = '//main-sidebar-collapse';

            //Check Last State of a Nomo
            $sql="

            SELECT
                distinct on (nomo)
                id,
                nomo,
                datetime_start,
                datetime_stop,
                is_split_line
            FROM flow_input_mo
            WHERE nomo = :nomo and  posisi='PENIMBANGAN'
            ORDER BY nomo,id desc
            ";

            $row = FlowInputMo::findBySql($sql,[':nomo'=>$nomo])->one();


            // Assign Variable Null to prevent error if it's the first data entry
            if(empty($row)){
                $last_datetime_start = null;
                $last_datetime_stop = null;
                $last_id = null;
                $nama_operator = null;
                $is_split_line = null;
            }else{
                $last_datetime_start = $row->datetime_start;
                $last_datetime_stop = $row->datetime_stop;
                $last_id = $row->id;
                $nama_operator = $row->nama_operator;
                $is_split_line = $row->is_split_line;
            }

            // Jenis Proses Check Status Jadwal, If Found then Don't Display
            $sql3="
                    SELECT
                        jenis_proses_timbang
                    FROM jenis_proses_timbang
                    WHERE jenis_proses_timbang not in
                    (SELECT jenis_proses FROM status_jadwal WHERE
                    nojadwal='".$nomo."' and posisi='PENIMBANGAN')
                ";

            $jenis_proses_list = ArrayHelper::map(JenisProsesTimbang::findBySql($sql3)->all(), 'jenis_proses_timbang','jenis_proses_timbang');


            // // Get Penimbangan Lanjutan
            //  $sql="
            //         SELECT nama_line
            //         FROM flow_input_mo
            //         WHERE nomo = '".$nomo."'
            //         and posisi='PENIMBANGAN'
            //         and lanjutan = 1
            // ";

            // $nama_line_row = FlowInputMo::findBySql($sql)->one();

            // if(empty($nama_line_row)){
            //     $nama_line = null;
            // }else{
            //     $nama_line = $nama_line_row->nama_line;
            // }

            // Get List Line
             $sql8="
                    SELECT nama_line
                    FROM master_data_line
                    WHERE posisi = 'PENIMBANGAN'
            ";

            $nama_line_all = ArrayHelper::map(MasterDataLine::findBySql($sql8)->all(), 'nama_line','nama_line');


             // Get Penimbangan Lanjutan
             $sql6="
                    SELECT koitem_fg
                    FROM scm_planner
                    WHERE nomo = '".$nomo."'
            ";

            $scm_planner = ScmPlanner::findBySql($sql6)->one();

            // Get Penimbangan Lanjutan
             $sql7="
                    SELECT no_formula, no_revisi
                    FROM log_penimbangan_rm
                    WHERE nomo = '".$nomo."'
                    ORDER BY id ASC
            ";

            $log_penimbangan_rm = LogPenimbanganRm::findBySql($sql7)->one();
            $no_formula = $log_penimbangan_rm->no_formula;
            $no_revisi = $log_penimbangan_rm->no_revisi;

            // Check If Post Value True
            if ($model->load(Yii::$app->request->post())) {
                // Cek Line dari PE
                 $sql7="
                        SELECT nama_line FROM log_penimbangan_rm WHERE nomo = '".$nomo."' 
                ";

                $line_pe = LogPenimbanganRm::findBySql($sql7)->one()['nama_line'];

                $line_liquid = ['LWE01','LWE02','LWE03'];
                $line_powder = ['PWE01','PWE02'];

                $timbangan_new = "";


                if ($model->nama_line != $line_pe){
                    //IF LINE MAPPING LWE01 to LWE02 or INVERSE
                    if (( in_array($model->nama_line,$line_liquid) && in_array($line_pe,$line_liquid) ) || ( in_array($model->nama_line,$line_liquid) && ($line_pe=='PilihLine' || empty($line_pe)) ) ){
                        /******* GET TASK **********/
                        $connection_ = Yii::$app->db;
                        $command_ = $connection_->createCommand("

                        SELECT
                            *
                        FROM log_penimbangan_rm
                        WHERE nomo = :nomo
                        ",
                        [':nomo' => $nomo,
                        ]);

                        $data_ = $command_->queryAll();

                        if ($model->nama_line == 'LWE02'){
                            for ($i = 0; $i < count($data_); $i++) {
                                if ($data_[$i]['nama_line'] != "LWE03"){
                                    if ($data_[$i]['timbangan'] == "LWA 01"){
                                        $timbangan_new = "LWB 01";
                                    } else if ($data_[$i]['timbangan'] == "LWA 02"){
                                        $timbangan_new = "LWB 02";
                                    } else if ($data_[$i]['timbangan'] == "LWA 03"){
                                        $timbangan_new = "LWB 03";
                                    } else if ($data_[$i]['timbangan'] == "LWC 01"){
                                        $timbangan_new = "LWC 02";
                                    } else if ($data_[$i]['timbangan'] == "LWC 03"){
                                        $timbangan_new = "LWC 04";
                                    } else {
                                        $timbangan_new = $data_[$i]['timbangan'];
                                    }
                                    // Update timbangan
                                    $connection = Yii::$app->getDb();
                                    $command = $connection->createCommand("
                                    UPDATE log_penimbangan_rm
                                    SET timbangan = '".$timbangan_new."', no_timbangan = '".$timbangan_new."', nama_line = '".$model->nama_line."'
                                    WHERE id = :id;
                                    ",
                                    [
                                     ':id'=> $data_[$i]['id']
                                    ]);
                                    $result = $command->queryAll();
                                }
                            }    
                            

                        }else if ($model->nama_line == 'LWE01'){

                            for ($i = 0; $i < count($data_); $i++) {
                                if ($data_[$i]['nama_line'] != "LWE03"){
                                    if ($data_[$i]['timbangan'] == "LWB 01"){
                                        $timbangan_new = "LWA 01";
                                    } else if ($data_[$i]['timbangan'] == "LWB 02"){
                                        $timbangan_new = "LWA 02";
                                    } else if ($data_[$i]['timbangan'] == "LWB 03"){
                                        $timbangan_new = "LWA 03";
                                    } else if ($data_[$i]['timbangan'] == "LWC 02"){
                                        $timbangan_new = "LWC 01";
                                    } else if ($data_[$i]['timbangan'] == "LWC 04"){
                                        $timbangan_new = "LWC 03";
                                    } else {
                                        $timbangan_new = $data_[$i]['timbangan'];
                                    }
                                    // Update timbangan
                                    $connection = Yii::$app->getDb();
                                    $command = $connection->createCommand("
                                    UPDATE log_penimbangan_rm
                                    SET timbangan = '".$timbangan_new."', no_timbangan = '".$timbangan_new."', nama_line = '".$model->nama_line."'
                                    WHERE id = :id;
                                    ",
                                    [
                                     ':id'=> $data_[$i]['id']
                                    ]);
                                    $result = $command->queryAll();
                                }
                                
                            }

                        }else{}

                        $model->switch_line = true;
                    
                    //IF LINE MAPPING PWE01 to PWE02 or INVERSE
                    }else if (( in_array($model->nama_line,$line_powder) && in_array($line_pe,$line_powder) ) || ( in_array($model->nama_line,$line_powder) && ($line_pe=='PilihLine' || empty($line_pe) ) )){
                        /******* GET TASK **********/
                        $connection_ = Yii::$app->db;
                        $command_ = $connection_->createCommand("

                        SELECT
                            *
                        FROM log_penimbangan_rm
                        WHERE nomo = :nomo
                        ",
                        [':nomo' => $nomo,
                        ]);

                        $data_ = $command_->queryAll();


                        if ($model->nama_line=='PWE02'){

                            for ($i = 0; $i < count($data_); $i++) {
                                if ($data_[$i]['timbangan'] == "PWA 01"){
                                    $timbangan_new = "PWA 03";
                                } else if ($data_[$i]['timbangan'] == "PWB 02"){
                                    $timbangan_new = "PWB 04";
                                } else if ($data_[$i]['timbangan'] == "PWC 01"){
                                    $timbangan_new = "PWC 02";
                                } else if ($data_[$i]['timbangan'] == "PWA 02"){
                                    $timbangan_new = "PWA 04";
                                } else if ($data_[$i]['timbangan'] == "PWB 01"){
                                    $timbangan_new = "PWB 03";
                                } else {
                                    $timbangan_new = $data_[$i]['timbangan'];
                                }
                                // Update timbangan
                                $connection = Yii::$app->getDb();
                                $command = $connection->createCommand("
                                UPDATE log_penimbangan_rm
                                SET timbangan = '".$timbangan_new."', no_timbangan = '".$timbangan_new."', nama_line = '".$model->nama_line."'
                                WHERE id = :id;
                                ",
                                [
                                 ':id'=> $data_[$i]['id']
                                ]);
                                $result = $command->queryAll();
                            }

                        }else if ($model->nama_line=='PWE01'){
                            for ($i = 0; $i < count($data_); $i++) {
                                if ($data_[$i]['timbangan'] == "PWA 03"){
                                    $timbangan_new = "PWA 01";
                                } else if ($data_[$i]['timbangan'] == "PWB 04"){
                                    $timbangan_new = "PWB 02";
                                } else if ($data_[$i]['timbangan'] == "PWC 02"){
                                    $timbangan_new = "PWC 01";
                                } else if ($data_[$i]['timbangan'] == "PWA 04"){
                                    $timbangan_new = "PWA 02";
                                } else if ($data_[$i]['timbangan'] == "PWB 03"){
                                    $timbangan_new = "PWB 01";
                                } else {
                                    $timbangan_new = $data_[$i]['timbangan'];
                                }
                                // Update timbangan
                                $connection = Yii::$app->getDb();
                                $command = $connection->createCommand("
                                UPDATE log_penimbangan_rm
                                SET timbangan = '".$timbangan_new."', no_timbangan = '".$timbangan_new."', nama_line = '".$model->nama_line."'
                                WHERE id = :id;
                                ",
                                [
                                 ':id'=> $data_[$i]['id']
                                ]);
                                $result = $command->queryAll();
                            }
                        }else{}

                        $model->switch_line = true;

                    }else{
                        if (strpos($model->nama_line,'SW')!==False){
                            // Update timbangan
                            $connection = Yii::$app->getDb();
                            $command = $connection->createCommand("
                            UPDATE log_penimbangan_rm
                            SET nama_line = :nama_line
                            WHERE nomo = :nomo;
                            ",
                            [
                             ':nama_line'=> $model->nama_line,
                             ':nomo'=> $nomo
                            ]);
                            $result = $command->queryAll();
                            $model->switch_line = null;

                        }else{
                            if ($line_pe != 'PilihLine' && !empty($line_pe)){
                                Yii::$app->session->setFlash('danger','Anda memilih Line yang berbeda dengan yang dibuat oleh PE. Sistem tidak mempunyai mapping timbangan dari line '.$line_pe.' ke '.$model->nama_line);
                                return $this->redirect(['create-penimbangan-rm', 'nomo'=>$nomo]);                            
                            }else{
                                $model->switch_line = true;
                            }

                        }


                    }
                    
                }

                $model->is_split_line = $is_split_line;


               
                $model->save();
                $id = $model->id;
                // print_r($model->nama_line);
                // print_r($line_pe);
                // exit();

                echo "<script>location.href='".Yii::getALias('@ipUrl')."flow-input-mo%2Fcheck-bom-rm&id=";
                echo $id;
                // echo "&operator=";
                // echo $operator;
                echo "';</script>";
                // return $this->redirect(['check-bom-rm',
                //     'id' => $model->id]);
            } else {
                
                return $this->render('create-penimbangan-rm', [
                    'model' => $model,
                    'nomo' => $nomo,
                    'last_datetime_start' => $last_datetime_start,
                    'last_datetime_stop' => $last_datetime_stop,
                    'last_id' => $last_id,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'nama_line' => $nama_line,
                    'jenis_proses_list' => $jenis_proses_list,
                    'no_formula' => $no_formula,
                    'no_revisi' => $no_revisi,
                    'nama_line_all' => $nama_line_all,
                    'operator' => $operator,
                    'nama_operator' => $nama_operator
                ]);
            }
        }else{
            throw new \yii\web\HttpException(401,
          'Jadwal telah di HOLD / CANCEL mohon hubungi Planner');
        }
    }

    public function actionCheckPage($nomo)
    {
         // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        // print_r($ip);
        // exit();

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            *
        FROM device_map_rm
        WHERE device_ip = :ip AND operator != 0
        ORDER BY operator ASC";

        $map_device_array = DeviceMapRm::findBySql($sql,[':ip'=>$ip])->one();

        $operator = $map_device_array->operator;
        $nama_line = $map_device_array->nama_line;

        //Check Last State of a Nomo and Line
        $sql1="

        SELECT
            distinct on (nomo)
            id,
            nomo,
            datetime_stop
        FROM flow_input_mo
        WHERE nomo = :nomo and nama_line = :nama_line and posisi='PENIMBANGAN'
        ORDER BY nomo,id desc
        ";

        $flow_input_mo = FlowInputMo::findBySql($sql1,[':nomo'=>$nomo,':nama_line'=>$nama_line])->one();

        //Check if the Process Engineer already verify the formula
        $sql2="
            SELECT
                COUNT(nomo)
            FROM log_penimbangan_rm
            WHERE nomo = '".$nomo."'
            ";

        $task = LogPenimbanganRm::findBySql($sql2)->scalar();

        //Check if the operator has a task or not
        $sql3="
            SELECT
                COUNT(nomo)
            FROM log_penimbangan_rm
            WHERE nomo = '".$nomo."' AND status = 'Selesai Ditimbang' AND nama_operator <> 'REPACK'
            ";

        $isEverDone = LogPenimbanganRm::findBySql($sql3)->scalar();

       

        // Page Routing

        // PE belum verify formula
        if(empty($task)){
            $value = array(
            "page"=>"no-task",
            "id"=>null);
        } else { // PE sudah verify formula

            // Belum pernah scan start jadwal
            if(empty($flow_input_mo)){
                if (strpos($nama_line,'LW')!==false){
                    //Check Last State of a Nomo
                    $sql1="

                    SELECT
                        distinct on (nomo)
                        id,
                        nomo,
                        datetime_stop,
                        nama_line,
                        is_split_line
                    FROM flow_input_mo
                    WHERE nomo = :nomo and posisi='PENIMBANGAN'
                    ORDER BY nomo,id desc
                    ";

                    $flow_input_mo = FlowInputMo::findBySql($sql1,[':nomo'=>$nomo])->one();
                    if (!empty($flow_input_mo)){
                        if (!empty($flow_input_mo->is_split_line) and $flow_input_mo->is_split_line == 1){
                            // Jika tablet yg sedang scan milik operator 1
                            if ($operator==1){
                                $value = array("page"=>"create", "id"=>null);
                            } else { // Jika tablet yg sedang scan bukan milik operator 1
                                $value = array("page"=>"stay", "id"=>null);
                            }
                        }else{
                            $value = array("page"=>"split-not-active", "id"=>$flow_input_mo->nama_line);
                        }
                    }else{
                        // Jika tablet yg sedang scan milik operator 1
                        if ($operator==1){
                            $value = array("page"=>"create", "id"=>null);
                        } else { // Jika tablet yg sedang scan bukan milik operator 1
                            $value = array("page"=>"stay", "id"=>null);
                        }
                    }
                }else{
                    // Jika tablet yg sedang scan milik operator 1
                    if ($operator==1){
                        $value = array("page"=>"create", "id"=>null);
                    } else { // Jika tablet yg sedang scan bukan milik operator 1
                        $value = array("page"=>"stay", "id"=>null);
                    }
                }

                
            } else { // Sudah pernah scan start jadwal

                // Jadwal sudah di stop
                if(!empty($flow_input_mo->datetime_stop) && ($operator == 1 || $operator == 11) ){
                    $sql4 = "SELECT id 
                                FROM status_jadwal 
                                WHERE nojadwal = '".$nomo."' AND table_name = 'flow_input_mo' AND posisi = 'PENIMBANGAN' ";
                    $isDone = Yii::$app->db->createCommand($sql4)->queryOne();
                    if (!empty($isDone)){
                        $value = array("page"=>"done", "id"=>null);
                    }else{
                        $value = array("page"=>"create", "id"=>null);
                    }
                } else { // Jadwal masih berjalan

                    // Jika sudah pernah menimbang, setidaknya 1x
                    if ($isEverDone>0) {
                        $value = array("page"=>"task", "id"=>$flow_input_mo->id);
                    } else { // Jika belum pernah sama sekali

                        // Jika tablet yg sedang scan milik operator 1
                        if ($operator==1){
                            $value = array("page"=>"bom-confirmation", "id"=>$flow_input_mo->id);
                        } else { // Jika tablet yg sedang scan bukan milik operator 1
                            $value = array("page"=>"task", "id"=>$flow_input_mo->id);
                        }
                    }
                }
            }
        }

        // if(empty($row)&&empty($task)&&($operator==1)){
        //     $value = array(
        //     "page"=>"create",
        //     "id"=>null);
        // }else if(!empty($row->datetime_stop)){
        //     $value = array(
        //     "page"=>"done",
        //     "id"=>$row->id);
        // }else if(!empty($row)&&empty($task)&&($operator==1)){
        //     $value = array(
        //     "page"=>"bom-confirmation",
        //     "id"=>$row->id);
        // }else if(!empty($row)&&!empty($task)&&($operator==1)){
        //     $value = array(
        //     "page"=>"task-kapten",
        //     "id"=>$row->id);
        // }else if(!empty($row)&&!empty($task)&&($operator!=1)){
        //     $value = array(
        //     "page"=>"task-operator",
        //     "id"=>$row->id);
        // }else if(empty($row)&&empty($task)&&($operator!=1)){
        //     $value = array(
        //     "page"=>"stay",
        //     "id"=>null);
        // } else if(!empty($row)&&($row->datetime_stop!=null)&&!empty($task)){
        //     $value = array(
        //     "page"=>"done",
        //     "id"=>null);
        // }
        // else {
        //     $value = array(
        //     "page"=>"stay",
        //     "id"=>null);
        // }

        //$sql="select count(*) as id from scm_planner where nomo='".$nomo."' and nomo is not null";
        //$planner= ScmPlanner::findBySql($sql)->one();
        echo Json::encode($value);
    }

    public function actionCheckOperator1($id){// check op list to validate op1 has to be assigned
       
       $sql9="
            SELECT nomo,no_formula, no_revisi, nama_operator, nama_line, siklus
            FROM flow_input_mo
            WHERE id = ".$id."
            ORDER BY datetime_start DESC;";

        $data = FlowInputMo::findBySql($sql9)->one();
        $nomo = $data->nomo;
        $nama_operator = $data->nama_operator;
        $no_formula = $data->no_formula;
        $no_revisi = $data->no_revisi;
        $siklus = $data->siklus;

        if ($siklus == -1){
            $sql="
                SELECT operator
                FROM log_penimbangan_rm
                WHERE nomo = '".$data->nomo."' AND is_repack = 1 AND operator = 1
                ORDER BY operator,id ASC
                ";

                $op1 = LogPenimbanganRm::findBySql($sql)->one();            
        }else if ($siklus != -1 && $siklus != 0){
            $sql="
                SELECT operator
                FROM log_penimbangan_rm
                WHERE nomo = '".$data->nomo."' AND siklus = ".$siklus." AND operator = 1
                ORDER BY operator,id ASC
                ";

                $op1 = LogPenimbanganRm::findBySql($sql)->one();            
        }else{
            $sql="
                SELECT operator
                FROM log_penimbangan_rm
                WHERE nomo = '".$data->nomo."' AND operator = 1
                ORDER BY operator,id ASC
                ";

                $op1 = LogPenimbanganRm::findBySql($sql)->one();
        }

        if (!empty($op1)){
            $value = array("op1"=>1);
        }else{
            $value = array("op1"=>0);
        }
        echo Json::encode($value);
    }

    public function actionCheckBomRm($id)
    {

        // $formula_model = new MasterDataFormula();

        // $arrayOp = array(2,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3);
        // echo '<pre>';
        // print_r($arrayOp);
        // echo '</pre>';
        $status = "Belum Ditimbang";

        $sql9="

            SELECT nomo,no_formula, no_revisi, nama_operator, nama_line, siklus
            FROM flow_input_mo
            WHERE id = ".$id."
            ORDER BY datetime_start DESC;";

        $data = FlowInputMo::findBySql($sql9)->one();
        $nomo = $data->nomo;
        $nama_operator = $data->nama_operator;
        $no_formula = $data->no_formula;
        $no_revisi = $data->no_revisi;
        $siklus = $data->siklus;
        // $nama_line = $data->nama_line;
        
        // $sqll="

        //     SELECT nama_line
        //     FROM log_jadwal_timbang_rm
        //     WHERE nomo = '".$nomo."'";

        // $line_row = LogJadwalTimbangRm::findBySql($sqll)->one();
        $nama_line = $data->nama_line;

        //$isFormulaExist = $formula_model->isFormulaExist($data->no_formula,$data->no_revisi);

        $this->layout = '//main-sidebar-collapse';

        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            *
        FROM device_map_rm
        WHERE device_ip = :ip
	    AND nama_line = :nama_line";

        $map_device_array = DeviceMapRm::findBySql($sql,[':ip'=>$ip,':nama_line'=>$nama_line])->one();

        // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
        if(empty($map_device_array)){
            $device_ip = 'BELUM TERDAFTAR';
            $operator = 1;
        }else{
            $device_ip = $map_device_array->device_ip;
            $operator = $map_device_array->operator;
        }

        // if (strpos($nama_line,'LW')!==false){
        //     $this->actionAdjustOperatorRepack($nomo,$siklus);
        // }

        if ($operator == 1){
            if($siklus==0 || empty($siklus)){
                $fim = FlowInputMo::find()->where(['id'=>$id])->one();
                $fim->siklus = 0;
                $fim->save();
                
                $sql="
                SELECT *
                FROM log_penimbangan_rm
                WHERE nomo = :nomo and nama_line = :nama_line
                ORDER BY operator,id ASC
                ";

                $bom = LogPenimbanganRm::findBySql($sql,[':nomo'=>$data->nomo,':nama_line'=>$nama_line])->all();

                $countBom="
                SELECT count(1)
                FROM log_penimbangan_rm
                WHERE nomo = :nomo and nama_line = :nama_line
                ";

                $bomRows = LogPenimbanganRm::findBySql($countBom,[':nomo'=>$data->nomo,':nama_line'=>$nama_line])->scalar();

                $countSiklus="
                SELECT distinct (siklus)
                FROM log_penimbangan_rm
                WHERE nomo = :nomo and nama_line = :nama_line
                ";

                $qtySiklus = LogPenimbanganRm::findBySql($countSiklus,[':nomo'=>$data->nomo,':nama_line'=>$nama_line])->all();
            }else if ($siklus == -1){
                $sql="
                SELECT *
                FROM log_penimbangan_rm
                WHERE nomo = :nomo AND is_repack = 1
                ORDER BY operator,id ASC
                ";
                $bom = LogPenimbanganRm::findBySql($sql,[':nomo'=>$data->nomo])->all();

                $countBom="
                SELECT count(1)
                FROM log_penimbangan_rm
                WHERE nomo = :nomo AND is_repack = 1
                ";

                $bomRows = LogPenimbanganRm::findBySql($countBom,[':nomo'=>$data->nomo])->scalar();

                $countSiklus="
                SELECT distinct (siklus)
                FROM log_penimbangan_rm
                WHERE nomo = :nomo AND is_repack = 1
                ";

                $qtySiklus = LogPenimbanganRm::findBySql($countSiklus,[':nomo'=>$data->nomo])->all();
            }else {
                
                $sql="
                SELECT *
                FROM log_penimbangan_rm
                WHERE nomo = :nomo AND siklus = :siklus and nama_line = :nama_line
                ORDER BY operator,id ASC
                ";

                $bom = LogPenimbanganRm::findBySql($sql,[':nomo'=>$data->nomo,':siklus'=>$siklus,':nama_line'=>$nama_line])->all();

                $countBom="
                SELECT count(1)
                FROM log_penimbangan_rm
                WHERE nomo = :nomo AND siklus = :siklus and nama_line = :nama_line
                ";

                $bomRows = LogPenimbanganRm::findBySql($countBom,[':nomo'=>$data->nomo,':siklus'=>$siklus,':nama_line'=>$nama_line])->scalar();

                // $countSiklus="
                // SELECT distinct (a.siklus)
                // FROM log_penimbangan_rm a
                // JOIN flow_input_mo b
                // ON a.nomo = '".$data->nomo."' AND b.id = ".$id." AND a.nomo = b.nomo
                // ";

                // $qtySiklus = BillOfMaterials::findBySql($countSiklus)->all();
                $qtySiklus = [['siklus'=>$siklus]];
            }
        }else if ($operator == 11) {
            if($siklus==0 || empty($siklus)){
                $fim = FlowInputMo::find()->where(['id'=>$id])->one();
                $fim->siklus = 0;
                $fim->save();
            }

            $sql="
                SELECT *
                FROM log_penimbangan_rm
                WHERE nomo = :nomo and kode_bb = 'AIR-RO--L'
                ORDER BY operator,id ASC
                ";

                $bom = LogPenimbanganRm::findBySql($sql,[':nomo'=>$data->nomo])->all();

                $countBom="
                SELECT count(1)
                FROM log_penimbangan_rm
                WHERE nomo = :nomo and kode_bb = 'AIR-RO--L'
                ";

                $bomRows = LogPenimbanganRm::findBySql($countBom,[':nomo'=>$data->nomo])->scalar();

                $countSiklus="
                SELECT distinct (siklus)
                FROM log_penimbangan_rm
                WHERE nomo = :nomo and kode_bb = 'AIR-RO--L'
                ";

                $qtySiklus = LogPenimbanganRm::findBySql($countSiklus,[':nomo'=>$data->nomo])->all();
        }

      
        // $searchModel = new MasterDataFormulaSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('check-bom-rm', [
            // 'searchModel' => $searchModel,
            // 'dataProvider' => $dataProvider,
            //'kode_bb' => $bom2['kode_bb'],
            'id' => $id,
            'nomo' => $nomo,
            'bom' => $bom,
            'bomRows' => $bomRows,
            'nama_line' => $nama_line,
            'device_ip' => $device_ip,
            'operator' => $operator,
            'nomo' => $nomo,
            'no_formula' => $no_formula,
            'no_revisi' => $no_revisi,
            'nama_operator' => $nama_operator,
            'qtySiklus' => $qtySiklus,
            'siklus' => $siklus
        ]);
    }

    public function actionAdjustOperatorRepack($nomo,$siklus){
        $sql="
            SELECT *
            FROM log_penimbangan_rm
            WHERE nomo = '".$nomo."' AND is_repack = 1
            ORDER BY operator,id ASC
            ";

        $boms = LogPenimbanganRm::findBySql($sql)->all();

        if ($siklus == -1){
            foreach ($boms as $bom){
                if ($bom->operator==4){
                    $new_operator = 1;
                } else if ($bom->operator==5){
                    $new_operator = 2;
                } else if ($bom->operator==6){
                    $new_operator = 3;
                }else{
                    $new_operator = $bom->operator;
                }
                
                if ($new_operator != $bom->operator){
                    $update = Yii::$app->db->createCommand("UPDATE log_penimbangan_rm SET operator = ".$new_operator." where id = ".$bom->id." ")->execute();
                }
            }            
        }else{
            foreach ($boms as $bom){
                if ($bom->operator==1){
                    $new_operator = 4;
                } else if ($bom->operator==2){
                    $new_operator = 5;
                } else if ($bom->operator==3){
                    $new_operator = 6;
                }else{
                    $new_operator = $bom->operator;
                }

                if ($new_operator != $bom->operator){
                    $update = Yii::$app->db->createCommand("UPDATE log_penimbangan_rm SET operator = ".$new_operator." where id = ".$bom->id." ")->execute();
                }
            }
        }

    }

    public function actionUpdateOperator($id,$operator){
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

        UPDATE log_penimbangan_rm
        SET operator = :operator
        WHERE id = :id;
        ",
        [':id'=> $id,
         'operator'=>$operator
        ]);

        $result = $command->queryAll();

        if ($result){
        // Redirect to Downtime
            $value = array(
            "status"=>"success");
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    public function actionUpdateTimbangan($id,$timbangan){
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

        UPDATE log_penimbangan_rm
        SET no_timbangan = :timbangan, timbangan = :timbangan
        WHERE id = :id;
        ",
        [':id'=> $id,
         'timbangan'=>$timbangan
        ]);

        $result = $command->queryAll();

        if ($result){
        // Redirect to Downtime
            $value = array(
            "status"=>"success");
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    public function actionActivateSplitLine($id,$now)
    {
        if (!empty($now) and $now==1){
            $update = 0;
        } else {
            $update = 1;
        }

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

        UPDATE flow_input_mo
        SET is_split_line = :value
        WHERE id = :id;
        ",
        [':id'=> $id,
         ':value'=>$update
        ]);

        $result = $command->queryAll();

        if($result){
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionDeactivateSplitLine($id,$now,$nomo)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            SELECT count(id) FROM flow_input_mo WHERE nomo = :nomo AND is_split_line = 1 AND datetime_stop is null", [':nomo'=>$nomo]);
        $line_split_active = $command->queryScalar();

        if ($line_split_active <= 1){
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE flow_input_mo
            SET is_split_line = :value
            WHERE nomo = :nomo;
            ",
            [':nomo'=> $nomo,
             ':value'=>0
            ]);

            $result = $command->queryAll();
            
            if($result){
                echo Json::encode(['status'=>'success']);
                // return $this->redirect(Yii::$app->request->referrer);
            }
        } else {
            echo Json::encode(['status'=>'cannot-off']);
        }

        
    }

    public function actionCheckPenimbanganRm()
    {
        // Get Device Information
        $model = new FlowInputMo();

        $searchModel = new FlowInputMoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("posisi='PENIMBANGAN' and datetime_start is not null and datetime_stop is null");

        $this->layout = '//main-sidebar-collapse';


        return $this->render('check-penimbangan-rm', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
        // if ($model->load(Yii::$app->request->post())) {
        //     // return $this->redirect(['create-penimbangan', 'nomo' => $model->nomo]);
        // } else {

        // }
    }

    public function actionCheckPenimbanganRmQr()
    {
        $server_name = $_SERVER['SERVER_NAME'];
        if($server_name == '10.3.5.102'){
             echo "<script>location.href='".Yii::getALias('@dnsUrl')."flow-input-mo/check-penimbangan-rm-qr';</script>";
        }
        $model = new FlowInputMo();

        $searchModel = new FlowInputMoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query->where("posisi='PENIMBANGAN' and datetime_start is not null and datetime_stop is null");

        $this->layout = '//main-sidebar-collapse';

        return $this->render('check-penimbangan-rm-qr', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
            // if ($model->load(Yii::$app->request->post())) {
            //     // return $this->redirect(['create-penimbangan', 'kode_unik_formula' => $model->kode_unik_formula]);
            // } else {

            // }
    }

    /**
     * Lists all MasterDataFormula models.
     * @return mixed
     */
    public function actionBomConfirmation()
    {

        // $searchModel = new MasterDataFormulaSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('bom-confirmation',
        [
            // 'searchModel' => $searchModel,
            // 'dataProvider' => $dataProvider,
        ]
        );
    }
}
