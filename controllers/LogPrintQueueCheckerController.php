<?php

namespace app\controllers;

use Yii;
use app\models\LogPrintQueueChecker;
use app\models\DeviceMapRm;
use app\models\LogPrintQueueCheckerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LogPrintQueueCheckerController implements the CRUD actions for LogPrintQueueChecker model.
 */
class LogPrintQueueCheckerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionPrintZebra($id,$new=false)
    {
        $model = LogPrintQueueChecker::find()->where(['id'=>$id])->one();

        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $client_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $client_ip = $_SERVER['REMOTE_ADDR'];
        }

        $sbc_ip = DeviceMapRm::find()->where(['device_ip' => $client_ip])->one()['raspi_ip'];
        // $sbc_user = DeviceMapRm::find()->where(['device_ip' => $client_ip])->one()['sbc_user'];
        $sbc_user = 'ariefram';

        if ($sbc_ip){
            $client_id = str_replace(".","",$client_ip);

            $myfile = fopen("label_checker_".$client_id.".zpl", "w");

            // $myfile = fopen("label_qcbulk.zpl", "w");

            $txt3 = "^FX Begin setup
              ^XA
              ~TA120
              ~JSN
              ^LT0
              ^MNW
              ^MTT
              ^PON
              ^PMN
              ^LH0,0
              ^JMA
              ^PR4,4
              ~SD25
              ^JUS
              ^LRN
              ^CI0
              ^MD0
              ^CI13,196,36
              ^XZ
              ^FX End setup

              ^FX Begin label format
              ^XA
              ^MMT
              ^LL0264
              ^PW583
              ^LS0

              ^FX Line Vertical Right
              ^FO420,220
              ^GB0,50,3^FS

              ^FX Line Vertical 2nd From Right Above
              ^FO330,20
              ^GB0,200,3^FS

              ^FX Line Vertical 2nd From Right Below
              ^FO301,200
              ^GB0,150,3^FS

              ^FX Line Horizontal First
              ^FO20,100
              ^GB310,0,3^FS

              ^FX Line Horizontal Second Left
              ^FO20,200
              ^GB310,0,3^FS

              ^FX Line Horizontal Second Right
              ^FO300,220
              ^GB260,0,3^FS


              ^FX Line Horizontal Left Bottom
              ^FO20,275
              ^GB283,0,3^FS

              ^FX Line Horizontal Second Row From Right Bottom
              ^FO303,270
              ^GB255,0,3^FS

              ^FX Line Horizontal First Row From Right Bottom
              ^FO303,314
              ^GB255,0,3^FS

              ^FX Nama Status
              ^FO40,30
              ^ARN,16
              ^FX Field block 507 dots wide, 2 lines max
              ^FB260,2,,C,
              ^FD CHECKED^FS

              ^FX QR kode BB
              ^FO380,20,0
              ^BQN,0,4,H,7
              ^FDMA,M:".$model->nomo."|S:".$model->siklus."^FS

              ^FX Kode BB Human readable
              ^FO350,165
              ^A0N15,18
              ^FX Field block 507 dots wide, 3 lines max
              ^FB180,3,,L,
              ^FD M:".$model->nomo."|S:".$model->siklus."^FS

              ^FX NOMO Label
              ^FO20,210
              ^ADN5,5
              ^FD NOMO^FS

              ^FX NOMO Value
              ^FO38,230
              ^A0N10,20
              ^FX Field block 507 dots wide, 2 lines max
              ^FB260,2,,C,
              ^FD".$model->nomo."^FS

              ^FX Time Checked Label
              ^FO310,278
              ^ABN5,5
              ^FDWaktu Check^FS

              ^FX Time Checked Value
              ^FO310,295
              ^AJN5,20
              ^FX Field block 507 dots wide, 1 lines max
              ^FB230,1,,C,
              ^FD".$model->timestamp_checked."^FS


              ^FX Nama Bulk Label
              ^FO30,110
              ^ADN5,5
              ^FDNama Bulk^FS

              ^FX Nama Bulk Value
              ^FO18,130
              ^A0N10,20
              ^FX Field block 507 dots wide, 3 lines max
              ^FB300,3,,C,
              ^FD ".$model->nama_bulk." ^FS

              ^FX Keterangan Label
              ^FO30,280
              ^ADN5,5
              ^FDKeterangan^FS

              ^FX Keterangan Value
              ^FO40,300
              ^A0N10,20
              ^FX Field block 507 dots wide, 2 lines max
              ^FB260,2,,L,
              ^FD ^FS

              ^FX Operator Label
              ^FO430,228
              ^ABN10,15
              ^FDOperator^FS

              ^FX Operator Value
              ^FO425,245
              ^A0N10,20
              ^FX Field block 507 dots wide, 1 lines max
              ^FB115,1,,C,
              ^FD ".$model->operator."^FS

              ^FX Siklus Label
              ^FO315,228
              ^ABN5,5
              ^FDSiklus^FS

              ^FX Siklus value
              ^FO310,245
              ^AAN,15,15
              ^FX Field block 507 dots wide, 1 lines max
              ^FB100,1,,C,
              ^FD ".$model->siklus."^FS

              ^FX Footer value
              ^FO20,345
              ^A0N,5,5
              ^FX Field block 507 dots wide, 1 lines max
              ^FB100,1,,L,
              ^FDCHECKER-MES^FS

              ^FX Print quantity
              ^PQ1,0,1,Y
              ^FX End label format
              ^XZ";

            fwrite($myfile, $txt3);
            fclose($myfile);


            // // Script to copy unique label to remote address
            exec('scp /var/www/html/flowreport/web/label_checker_'.$client_id.'.zpl '.$sbc_user.'@'.$sbc_ip.':/home/'.$sbc_user.'/label_checker_'.$client_id.'.zpl 2>&1', $output);

            // // Execute Printing remotely
            exec("ssh ".$sbc_user."@".$sbc_ip." 'lpr -P Zebra_Technologies_ZTC_GT800 -o raw /home/".$sbc_user."/label_checker_".$client_id.".zpl'");
            // Yii::$app->session->setFlash('success', "Device terdaftar di database");
        }else{

            Yii::$app->session->setFlash('danger', "Device tidak terdaftar di database");

        }

        if(!$new){
            return $this->redirect(Yii::$app->request->referrer);

        }


        // print_r($nobatch);
    }

    /**
     * Lists all LogPrintQueueChecker models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogPrintQueueCheckerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexPrint()
    {
        $searchModel = new LogPrintQueueCheckerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['id'=>SORT_DESC]);

        return $this->render('index-print', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single LogPrintQueueChecker model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LogPrintQueueChecker model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LogPrintQueueChecker();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LogPrintQueueChecker model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LogPrintQueueChecker model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LogPrintQueueChecker model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogPrintQueueChecker the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogPrintQueueChecker::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
