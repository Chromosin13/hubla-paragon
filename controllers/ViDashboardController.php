<?php

namespace app\controllers;

use app\models\ViInfo;
use app\models\ViSchedule;
use app\models\ViTimeStamp;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class ViDashboardController extends Controller {
    private $data;
    public $layout = 'base';
    public function init() {
        $this->data['info'] = new ViInfo;
        $this->data['schedule'] = new ViSchedule;
        $this->data['time_stamp'] = new ViTimeStamp;

        parent::init();
    }

    public function actionInspect($param1, $param2) {
        $product['schedule'] = $this->data['schedule']->getProductScheduleBy('id', $param2);
        $product['schedule'] = $this->data['schedule']->getItemCode($product['schedule']['id']);
        $product['info'] = $this->data['info']->getProductInfoBy('koitem_fg', $product['schedule']['koitem']);

        $productCard = $this->renderPartial('viewAssets/productCards', [
            'product' => $product,
        ]);
        
        $setPeriodChart = $this->renderPartial('viewAssets/periodOptions');
        
        $chart = $this->renderPartial('viewAssets/chart', [
            'setPeriodChart' => $setPeriodChart,
        ]);
        
        $tracibilityTable = $this->renderPartial('viewAssets/tracibilityTable', [
            'setPeriodChart' => $setPeriodChart,
            'product' => $product,
        ]);

        $stopInspection = $this->renderPartial('viewAssets/stopInspection', [
            'product' => $product
        ]);

        if ($param1 == 'done') {
            $js = "@web/js/visual-inspection/doneDashboard.js";
        } else if ($param1 == 'current') {
            $js = "@web/js/visual-inspection/currentDashboard.js";
        } else if ($param1 == 'debug') {
            $js = "@web/js/debugCurrentDashboard.js";
        }


        return $this->render('inspect', [
            'product' => $product,
            
            'productCard' => $productCard,
            'setPeriodChart' => $setPeriodChart,
            'chart' => $chart,
            'tracibilityTable' => $tracibilityTable,
            'stopInspection' => $stopInspection,

            'js' => $js
        ]);
    }
}