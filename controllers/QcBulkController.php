<?php

namespace app\controllers;

use Yii;
use app\models\QcBulk;
use app\models\QcBulkSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use app\models\User;
use yii\filters\AccessControl;
use app\models\Kendala;
use app\models\Model;
use app\models\ArrayHelper;

/**
 * QcBulkController implements the CRUD actions for QcBulk model.
 */
class QcBulkController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create', 'update', 'delete','index','view'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_QCBULK
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all QcBulk models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QcBulkSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single QcBulk model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new QcBulk model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new QcBulk();
        $modelsKendala = [new Kendala];

        if ($model->load(Yii::$app->request->post()) && $model->save() ) 
        {

            $modelsKendala = Model::createMultiple(Kendala::classname());
            Model::loadMultiple($modelsKendala, Yii::$app->request->post());

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsKendala) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($modelsKendala as $modelKendala) 
                        {
                            $modelKendala->qc_bulk_id = $model->id;
                            if (! ($flag = $modelKendala->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['create']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        } else 

        {
            return $this->render('create', [
                'model' => $model,
                'modelsKendala' => (empty($modelsKendala)) ? [new Kendala] : $modelsKendala
            ]);
        }

        // $model = new QcBulk();

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     //return $this->redirect(['view', 'id' => $model->id]);
        //     return $this->redirect(['create']);
        // } else {
        //     return $this->render('create', [
        //         'model' => $model,
        //     ]);
        // }
    }

    /**
     * Updates an existing QcBulk model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $modelsKendala = $model->kendalas;

        if ($model->load(Yii::$app->request->post()) && $model->save()) 
        {
            $oldIDs = ArrayHelper::map($modelsKendala, 'id', 'id');

            $modelsKendala = Model::createMultiple(Kendala::classname(), $modelsKendala);

            Model::loadMultiple($modelsKendala, Yii::$app->request->post());

            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsKendala, 'id', 'id')));

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsKendala) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {

                        if (! empty($deletedIDs)) {
                            Kendala::deleteAll(['id' => $deletedIDs]);
                        }

                        foreach ($modelsKendala as $modelKendala) {
                            $modelKendala->qc_bulk_id = $model->id;
                            if (! ($flag = $modelKendala->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    } else {
                        $transaction->rollBack();
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }

            //return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelsKendala' => (empty($modelsKendala)) ? [new Kendala] : $modelsKendala
            ]);
        }


        // $model = $this->findModel($id);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // } else {
        //     return $this->render('update', [
        //         'model' => $model,
        //     ]);
        // }
    }

    public function actionLanjutanQcBulk($snfg_komponen,$jenis_periksa)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM 
            (
                select sp.nomo, qc_bulk.snfg_komponen, qc_bulk.jenis_periksa, qc_bulk.state, qc_bulk.lanjutan
                from qc_bulk 
                inner join scm_planner sp on sp.snfg_komponen = qc_bulk.snfg_komponen
                where state='STOP' and qc_bulk.nomo is null and qc_bulk.snfg_komponen is not null 
                and qc_bulk.snfg_komponen='".$snfg_komponen."'

                union all
                
                select p.nomo,sp.snfg_komponen, p.jenis_periksa, p.state, p.lanjutan 
                from qc_bulk p
                inner join scm_planner sp on sp.nomo=p.nomo
                where p.state='STOP' and p.nomo is not null and sp.snfg_komponen='".$snfg_komponen."'
            ) a
        WHERE a.snfg_komponen ='".$snfg_komponen."'
        and a.jenis_periksa = '".$jenis_periksa."'
        ";
        $QcBulk= QcBulk::findBySql($sql)->one();
        echo Json::encode($QcBulk);

    }

    public function actionLanjutanQcBulkmo($nomo,$jenis_periksa)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM 
            (
                select sp.nomo, qc_bulk.snfg_komponen, qc_bulk.jenis_periksa, qc_bulk.state, qc_bulk.lanjutan
                from qc_bulk 
                inner join scm_planner sp on sp.snfg_komponen = qc_bulk.snfg_komponen
                where state='STOP' and qc_bulk.nomo is null and qc_bulk.snfg_komponen is not null and sp.nomo='".$nomo."'


                union all
                
                select p.nomo,sp.snfg_komponen, p.jenis_periksa, p.state, p.lanjutan 
                from qc_bulk p
                inner join scm_planner sp on sp.nomo=p.nomo
                where p.state='STOP' and p.nomo is not null and p.nomo='".$nomo."'
            ) a
        WHERE a.nomo ='".$nomo."'
        and a.jenis_periksa = '".$jenis_periksa."'
        ";
        $QcBulk= QcBulk::findBySql($sql)->one();
        echo Json::encode($QcBulk);

    }

    public function actionLanjutanIstQcBulk($snfg_komponen,$jenis_periksa,$lanjutan)
    {
        $sql="
                SELECT coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist
                FROM 
                    (
                        select sp.nomo, qc_bulk.snfg_komponen, qc_bulk.jenis_periksa, qc_bulk.state, qc_bulk.lanjutan,qc_bulk.lanjutan_ist
                        from qc_bulk 
                        inner join scm_planner sp on sp.snfg_komponen = qc_bulk.snfg_komponen
                        where state='ISTIRAHAT STOP' and qc_bulk.nomo is null and qc_bulk.snfg_komponen is not null

                        union all
                        
                        select p.nomo,sp.snfg_komponen, p.jenis_periksa, p.state, p.lanjutan, p.lanjutan_ist 
                        from qc_bulk p
                        inner join scm_planner sp on sp.nomo=p.nomo
                        where p.state='ISTIRAHAT STOP' and p.nomo is not null
                    ) a
                    WHERE a.snfg_komponen ='".$snfg_komponen."'
                    and a.jenis_periksa = '".$jenis_periksa."'
                    and a.lanjutan = '".$lanjutan."'
        ";
        $QcBulk= QcBulk::findBySql($sql)->one();
        echo Json::encode($QcBulk);
    }

    public function actionLanjutanIstQcBulkmo($nomo,$jenis_periksa,$lanjutan)
    {
        $sql="
                SELECT coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist
                FROM 
                    (
                        select sp.nomo, qc_bulk.snfg_komponen, qc_bulk.jenis_periksa, qc_bulk.state, qc_bulk.lanjutan,qc_bulk.lanjutan_ist
                        from qc_bulk 
                        inner join scm_planner sp on sp.snfg_komponen = qc_bulk.snfg_komponen
                        where state='ISTIRAHAT STOP' and qc_bulk.nomo is null and qc_bulk.snfg_komponen is not null

                        union all
                        
                        select p.nomo,sp.snfg_komponen, p.jenis_periksa, p.state, p.lanjutan, p.lanjutan_ist 
                        from qc_bulk p
                        inner join scm_planner sp on sp.nomo=p.nomo
                        where p.state='ISTIRAHAT STOP' and p.nomo is not null
                    ) a
                WHERE a.nomo ='".$nomo."'
                and a.jenis_periksa = '".$jenis_periksa."'
                and a.lanjutan = '".$lanjutan."'
                ";
        $QcBulk= QcBulk::findBySql($sql)->one();
        echo Json::encode($QcBulk);
    }

    /**
     * Deletes an existing QcBulk model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the QcBulk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QcBulk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QcBulk::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
