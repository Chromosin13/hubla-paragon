<?php

namespace app\controllers;

use Yii;
use app\models\LogReceiveNdc;
use app\models\SuratJalan;
use app\models\SuratJalanRincian;
use app\models\SuratJalanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SuratJalanController implements the CRUD actions for SuratJalan model.
 */
class SuratJalanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SuratJalan models.
     * @return mixed
     */
    public function actionIndex()
    {

        $this->layout = '//main-sidebar-collapse';
        
        $searchModel = new SuratJalanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SuratJalan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SuratJalan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SuratJalan();

        if ($model->load(Yii::$app->request->post())){
             
             $model->save();
             $model->nosj = 'DRAFT/'.$model->id;
             $model->save();

             return $this->redirect(['surat-jalan-rincian/create-rincian', 'id' => $model->id]);
        } 
        else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateQr()
    {
        $model = new SuratJalan();

        if ($model->load(Yii::$app->request->post())){

            $model->save();
            $model->nosj = 'DRAFT/'.$model->id;
            $model->save();

            return $this->redirect('http://factory.pti-cosmetics.com/flowreport/web/index.php?r=surat-jalan-rincian/create-rincian-qr&id='.$model->id);
            // return $this->redirect(['qc-fg-v2/create-surjal-rincian-qr', 'id' => $model->id]);
        } 
        else {
            return $this->render('create-qr', [
                'model' => $model,
            ]);
        }
    }

    public function actionCheckSuratJalan($surat_jalan){
        $surjal = SuratJalan::find()->where(['nosj'=>$surat_jalan])->one();
        if (!empty($surjal)){
            if($surjal->is_received == 1){
                echo 2;
            }else{
                $cek_log_receive = LogReceiveNdc::find()->where(['nosj'=>$surat_jalan])->one();
                if (!empty($cek_log_receive)){
                    echo 1;
                }else{// redirect to old receiving
                    echo 3;
                }
            }
        }else{
            echo 0;
        }
    }



    public function actionCreateRincian($id)
    {
        $model = new SuratJalan();

        $searchModel = new SuratJalanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("id=".$id."");


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['surat-jalan-rincian/create-rincian', 'id' => $id]);
        } else {
            return $this->render('surat-jalan-rincian/create-rincian', [
                'model' => $model,
                'id' => $id,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }


    public function actionCreateRincianQr($id)
    {
        $model = new SuratJalan();

        $searchModel = new SuratJalanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("id=".$id."");


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['surat-jalan-rincian/create-rincian-qr', 'id' => $id]);
        } else {
            return $this->render('surat-jalan-rincian/create-rincian-qr', [
                'model' => $model,
                'id' => $id,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionIndexSurjalReceived()
    {
        $model = new SuratJalan();

        $searchModel = new SuratJalanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("is_recieved = 1");


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['surat-jalan-rincian/create-rincian-qr', 'id' => $id]);
        } else {
            return $this->render('surat-jalan/index-surjal-recieved', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing SuratJalan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SuratJalan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->is_received){
            Yii::$app->session->setFlash('danger','Surat Jalan sudah di terima NDC, tidak bisa di delete!');
            return $this->redirect(Yii::$app->request->referrer);
        }else{
            $model_rincian = SuratJalanRIncian::find()->where(['surat_jalan_id'=>$model->id])->all();
            $delete_rincian = Yii::$app->db->createCommand("DELETE FROM surat_jalan_rincian WHERE surat_jalan_id = ".$model->id." ")->execute();
            $this->findModel($id)->delete();

            return $this->redirect(['index']);            
        }
    }

    /**
     * Finds the SuratJalan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SuratJalan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SuratJalan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
