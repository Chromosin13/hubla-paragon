<?php
namespace app\controllers;

use app\models\ViInfo;
use app\models\ViSchedule;
use app\models\ViTimeStamp;
use yii\filters\AccessControl;
use yii\web\Controller;
use DateTime;
use Yii;

class ViApiController extends Controller
{

    private $tempId = 0;
    private $data;
    public function init() {
        $this->data['info'] = new ViInfo;
        $this->data['schedule'] = new ViSchedule;
        $this->data['time_stamp'] = new ViTimeStamp;

        parent::init();
    }
    
    // public function actionIndex() {
    //     \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;     
    //     // return $this->data['schedule']->getUser();
    //     return array(
    //         'status' => 'It Works!'
    //     );
    // }
    
    private function scheduleValidity($param) {
        date_default_timezone_set('Asia/Jakarta');
        // $now = new DateTime();
        $now = new DateTime("2021-12-21 15:36:00");
        // $datetimeStart = new DateTime($param['datetime_start']);
        // return $now->diff($datetimeStart);
        return $now->diff(DateTime::createFromFormat("Y-m-d H:i:s", $param));
    }
    
    private function scheduleRunValidity($param) {
        date_default_timezone_set('Asia/Jakarta');
        // $now = new DateTime();
        $now = new DateTime("2021-12-21 13:35:00");
        // $datetimeStart = new DateTime($param['datetime_start']);
        // return $now->diff($datetimeStart);
        return $now->diff(DateTime::createFromFormat("Y-m-d H:i:s", $param));
    }
    
    public function actionApiGetRunningSchedule($line) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;     
        $postData = Yii::$app->request->post();
        
        $product['schedule'] = $this->data['schedule']->getRunningSchedule($line);
        $product['info'] = $this->data['info']->getProductInfoBy('snfg_komponen', $product['schedule']['snfg']);
        
        if ($postData['temp_id'] < $product['schedule']['id']) {
            
            $interval = $this->scheduleValidity($product['schedule']['datetime_start']);
            
            if ($interval->y == 0 && $interval->m == 0 && $interval->d == 0 && $interval->h == 0 && $interval->i <= 1 && $interval->s <= 59) {
                $this->tempId = $product['schedule']['id'];
                $result = [
                    'temp_id' => $this->tempId,
                    'info' => $product['info'],
                    'schedule' => $product['schedule'],
                    // 'start_time' => $product['schedule']['datetime_start'],
                    // 'interval' => $interval,
                    // 'threshold' => $thresholdStringify,
                    'instruction' => 'start'
                ];
            } else {
                $result = [
                    'temp_id' => $this->tempId,
                    'info' => 'null',
                    'schedule' => 'null',
                    // 'id' => $product['schedule']['id'],
                    // 'start_time' => $product['schedule']['datetime_start'],
                    // 'interval' => $interval,
                    'instruction' => 'wait'
                ];
            }
            // $this->tempId = $product['schedule']['id'];
            
        } else if ($postData['temp_id'] == $product['schedule']['id']) {
            $interval = $this->scheduleRunValidity($product['schedule']['datetime_start']);
            
            if ($interval->h >= 8) {
                $result = [
                    'temp_id' => 0,
                    'info' => $product['info'],
                    'schedule' => $product['schedule'],
                    // 'interval' => $interval,
                    'instruction' => 'stop'
                ];
            } else {
                $this->tempId = $product['schedule']['id'];
                $result = [
                    'temp_id' => $this->tempId,
                    'info' => $product['info'],
                    'schedule' => $product['schedule'],
                    // 'interval' => $interval,
                    'instruction' => 'check'
                ];
            }
        } else if ($postData['temp_id'] > $product['schedule']['id']) {
            $result = [
                'temp_id' => $postData['temp_id'],
                'info' => null,
                'schedule' => null,
                'instruction' => 'wait'
            ];
        }

        return $result;
    }
    
    public function actionApiGetAllHistoricSchedule() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = $this->data['schedule']->getDoneSchedule();
        // echo $result;
        return $result;
    }

    public function actionApiPostInspectionResult() {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

        $postData = Yii::$app->request->post();
        $product['schedule'] = $this->data['schedule']->getProductScheduleBy('id', $postData['id']);            
        $product['count'] = $this->data['time_stamp']->getPeriodicInspectionResult($postData);
        // $product['time_stamp'] = $this->data['time_stamp']->getHistoryData($product['schedule']);

        return $product['count'];
    }
    
    public function actionApiPostTraceDefect() {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;     
        $postData = Yii::$app->request->post();
        // return gettype(filter_var($postData['is_stop'], FILTER_VALIDATE_BOOLEAN));
        // return filter_var($postData['is_stop'], FILTER_VALIDATE_BOOLEAN);
        $product['schedule'] = $this->data['schedule']->getProductScheduleBy('id', $postData['id_schedule']);
        
        if ($this->data['time_stamp']->addInspectionResult($product['schedule'], $postData) > 0
        ) {
            $product['time_stamp'] = $this->data['time_stamp']->getCurrentHistoryData($product['schedule']);
            // return $product['time_stamp'];
            // return $postData;
        }
    }

    public function actionApiGetCheckDbConnection() {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        return array(
            'connection_status' => 'true'
        );
    }
    
    public function actionApiPostDebug() {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;     
        $data = Yii::$app->request->post();
        return array('status' => true, 'data'=> $data);
    }
    
    public function actionApiGetDebug() {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;     
        
        return array(
            'status' => 'ok'
        );
    }
    
    public function actionApiValidateSku() {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;     
        $data = Yii::$app->request->post();
        $result = $this->data['info']->validateSku($data['koitem_fg']);
        if (count((array)$result) == 1) {
            return array (
                'nama_fg' => null
            );
        } else {
            return array(
                'nama_fg' => $result['nama_fg']
            );
        };
    }
}
