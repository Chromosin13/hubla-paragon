<?php

namespace app\controllers;

use Yii;
use app\models\LogFormulaBreakdown;
use app\models\LogFormulaBreakdownSearch;
use app\models\FlowInputMo;
use app\models\FlowInputGbb;
use app\models\LogPickingGbb;
use app\models\LogBrDetail;
use app\models\LogJadwalTimbangRm;
use app\models\LogJadwalTimbangRmNew;
use app\models\LogApprovalBr;
use app\models\LogApprovalBrSearch;
use app\models\ListReviewer;
use app\models\ListApprover;
use app\models\LogJadwalTimbangRmSearch;
use app\models\LogJadwalTimbangRmNewSearch;
use app\models\LogVerifiedBrSearch;
use app\models\LogFormulaBreakdownSplit;
use app\models\MasterDataTimbanganRm;
use app\models\KodeActionPenimbanganRm;
use app\models\LogPenimbanganRm;
use app\models\LogMasterBr;
use app\models\LogMasterBrSearch;
use app\models\LogBrDetailSearch;
use app\models\ScmPlanner;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\DateTime;
use yii\helpers\Json;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;

/**
 * LogFormulaBreakdownController implements the CRUD actions for LogFormulaBreakdown model.
 */
class LogFormulaBreakdownController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LogFormulaBreakdown models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogFormulaBreakdownSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LogFormulaBreakdown model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LogFormulaBreakdown model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LogFormulaBreakdown();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LogFormulaBreakdown model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LogFormulaBreakdown model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LogFormulaBreakdown model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogFormulaBreakdown the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogFormulaBreakdown::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Displays a single LogFormulaBreakdown model.
     * @param integer $id
     * @return mixed
     */
    public function actionMasterBr()
    {
        date_default_timezone_set('Asia/Jakarta');
        // $ddate = date("Y-m-d");
        // $duedt = explode("-", $ddate);
        // $date  = mktime(0, 0, 0, $duedt[1], $duedt[2], $duedt[0]);
        // $week  = (int)date('W', $date);
        // $week2  = $week-1;
        // $week3  = $week+1;

        $this->layout = '//main-sidebar-collapse';

        // $sql="
        //     SELECT DISTINCT kode_bulk, formula reference FROM log_formula_breakdown ORDER BY kode_bulk ASC, formula_reference ASC
        // ";

        // $list_kode_bulk = LogFormulaBreakdown::findBySql($sql)->all();


        $searchModel = new LogMasterBrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('master-br', [
            // 'model' => $this->findModel($id),
            // 'week' => $week,
            // 'list_jadwal' => $list_jadwal,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

     public function actionTestUpdateKodeInternal()
    {
        $connection = Yii::$app->db_warehouse;
        $command = $connection->createCommand("

        SELECT distinct on (component_internal_code) component_internal_code, component_code from daily_consumption_default where component_internal_code is not null
        ");

        $lists = $command->queryAll();

        $connection = Yii::$app->db;

        foreach ($lists as $list) {
            # code...
            $command = $connection->createCommand("UPDATE log_formula_breakdown SET kode_internal = '".$list['component_internal_code']."' WHERE kode_bb = '".$list['component_code']."' AND kode_internal is null ");
            $update = $command->queryAll();

        }

        print_r("done");
    }

    public function actionScheduleList2()
    {
        date_default_timezone_set('Asia/Jakarta');
        $ddate = date("Y-m-d");
        $duedt = explode("-", $ddate);
        $date  = mktime(0, 0, 0, $duedt[1], $duedt[2], $duedt[0]);
        $week  = (int)date('W', $date);
        $week2  = $week-1;
        $week3  = $week+1;

        $connection = Yii::$app->db_warehouse;
        $command = $connection->createCommand("

        SELECT distinct(name), scheduled_quantity, scheduled_start, reference, fg_name, location, week
        FROM daily_consumption_default
        WHERE  name ilike '%BULK%' AND week => :week AND week <= :week2 AND name ilike '%MO-%' ORDER BY scheduled_start ASC
        ",
        [':week' => $week2,
         ':week2' => $week3,
        ]);

        $list = $command->queryAll();

        $dataProvider = $list;

        return $this->render('schedule-list2', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single LogFormulaBreakdown model.
     * @param integer $id
     * @return mixed
     */
    // public function actionScheduleList()
    // {
    //     date_default_timezone_set('Asia/Jakarta');
    //     $ddate = date("Y-m-d");
    //     $duedt = explode("-", $ddate);
    //     $date  = mktime(0, 0, 0, $duedt[1], $duedt[2], $duedt[0]);
    //     $week  = (int)date('W', $date);
    //     //$week2  = $week-1;
    //     //$week3  = $week+1;
    //     $week2  = (int)date('W', strtotime("-1 week", $date));
    //     $week3  = (int)date('W', strtotime("+1 week", $date));

    /*    $this->layout = '//main-sidebar-collapse';

        $status = "unverified";

        $sql2="
        SELECT
            *
        FROM log_jadwal_timbang_rm
        WHERE week = '".$week2."'";

        $sql="
        SELECT
            *
        FROM log_jadwal_timbang_rm
        WHERE week = '".$week."'";

        $sql3="
        SELECT
            *
        FROM log_jadwal_timbang_rm
        WHERE week = '".$week3."'";

        $list_jadwal_week_before = LogJadwalTimbangRm::findBySql($sql2)->all();
        $list_jadwal_week_now = LogJadwalTimbangRm::findBySql($sql)->all();
        $list_jadwal_week_after = LogJadwalTimbangRm::findBySql($sql3)->all();

        if (empty($list_jadwal_week_before)){
            $connection = Yii::$app->db_warehouse;
            $command = $connection->createCommand("

            SELECT distinct(name), scheduled_quantity, scheduled_start, reference, fg_name, location, week
            FROM daily_consumption_default
            WHERE  name ilike '%BULK%' AND week = :week AND name ilike '%MO-%' ORDER BY scheduled_start ASC
            ",
            [':week' => (string)$week2,
            ]);

            $list = $command->queryAll();

            foreach ($list as $lists) {
                $sql4="
                    SELECT
                        line_timbang
                    FROM scm_planner
                    WHERE nomo = '".$lists["name"]."'";

                $nama_line_array = ScmPlanner::findBySql($sql4)->one();

                if (empty($nama_line_array->line_timbang))
                {
                    $line_timbang = "Pilih Line";
                } else if (strpos($nama_line_array->line_timbang, '/') !== false)
                {
                    $line_timbang = "Pilih Line";
                } else {
                    $line_timbang = str_replace(' ', '', $nama_line_array->line_timbang);
                }

                $connection2 = Yii::$app->db;
                $command2 = $connection2->createCommand("
                    INSERT INTO log_jadwal_timbang_rm (nomo,nama_fg,qty_batch,scheduled_start,formula_reference,week, lokasi,status, nama_line,is_sync)
                    SELECT :nomo as nomo, :nama_fg as nama_fg, :qty_batch as qty_batch, :scheduled_start as scheduled_start, :formula_reference as formula_reference, :week as week, :lokasi as lokasi, :status as status , :nama_line as nama_line, :is_sync as is_sync
                    ",
                    [':nomo' => $lists["name"],
                     ':nama_fg' => $lists["fg_name"],
                     ':qty_batch' => $lists["scheduled_quantity"],
                     ':scheduled_start' => $lists["scheduled_start"],
                     ':formula_reference' => $lists["reference"],
                     ':week' => $lists["week"],
                     ':lokasi' => $lists["location"],
                     ':status' => $status,
                     ':nama_line' => $line_timbang,
                     ':is_sync' => "unsync"
                    ]
                );
                $result2 = $command2->queryAll();
            }
        }

        if (empty($list_jadwal_week_now)){
            $connection = Yii::$app->db_warehouse;
            $command = $connection->createCommand("

            SELECT distinct(name), scheduled_quantity, scheduled_start, reference, fg_name, location, week
            FROM daily_consumption_default
            WHERE  name ilike '%BULK%' AND week = :week AND name ilike '%MO-%' ORDER BY scheduled_start ASC
            ",
            [':week'=> (string)$week,
            ]);

            $list = $command->queryAll();

            foreach ($list as $lists) {
                $sql4="
                    SELECT
                        line_timbang
                    FROM scm_planner
                    WHERE nomo = '".$lists["name"]."'";

                $nama_line_array = ScmPlanner::findBySql($sql4)->one();

                if (empty($nama_line_array->line_timbang))
                {
                    $line_timbang = "Pilih Line";
                } else if (strpos($nama_line_array->line_timbang, '/') !== false)
                {
                    $line_timbang = "Pilih Line";
                } else {
                    $line_timbang = str_replace(' ', '', $nama_line_array->line_timbang);
                }

                $connection2 = Yii::$app->db;
                $command2 = $connection2->createCommand("
                    INSERT INTO log_jadwal_timbang_rm (nomo,nama_fg,qty_batch,scheduled_start,formula_reference,week, lokasi,status,nama_line,is_sync)
                    SELECT :nomo as nomo, :nama_fg as nama_fg, :qty_batch as qty_batch, :scheduled_start as scheduled_start, :formula_reference as formula_reference, :week as week, :lokasi as lokasi, :status as status, :nama_line as nama_line, :is_sync as is_sync
                    ",
                    [':nomo' => $lists["name"],
                     ':nama_fg' => $lists["fg_name"],
                     ':qty_batch' => $lists["scheduled_quantity"],
                     ':scheduled_start' => $lists["scheduled_start"],
                     ':formula_reference' => $lists["reference"],
                     ':week' => $lists["week"],
                     ':lokasi' => $lists["location"],
                     ':status' => $status,
                     ':nama_line' => $line_timbang,
                     ':is_sync'=> "unsync"
                    ]
                );
                $result2 = $command2->queryAll();
            }
        }

        if (empty($list_jadwal_week_after)){
            $connection = Yii::$app->db_warehouse;
            $command = $connection->createCommand("

            SELECT distinct(name), scheduled_quantity, scheduled_start, reference, fg_name, location, week
            FROM daily_consumption_default
            WHERE  name ilike '%BULK%' AND week = :week AND name ilike '%MO-%' ORDER BY scheduled_start ASC
            ",
            [':week'=> (string)$week3,
            ]);

            $list = $command->queryAll();

            foreach ($list as $lists) {
                $sql4="
                    SELECT
                        line_timbang
                    FROM scm_planner
                    WHERE nomo = '".$lists["name"]."'";

                $nama_line_array = ScmPlanner::findBySql($sql4)->one();

                if (empty($nama_line_array->line_timbang))
                {
                    $line_timbang = "Pilih Line";
                } else if (strpos($nama_line_array->line_timbang, '/') !== false)
                {
                    $line_timbang = "Pilih Line";
                } else {
                    $line_timbang = str_replace(' ', '', $nama_line_array->line_timbang);
                }

                $connection2 = Yii::$app->db;
                $command2 = $connection2->createCommand("
                    INSERT INTO log_jadwal_timbang_rm (nomo,nama_fg,qty_batch,scheduled_start,formula_reference,week, lokasi,status, nama_line,is_sync)
                    SELECT :nomo as nomo, :nama_fg as nama_fg, :qty_batch as qty_batch, :scheduled_start as scheduled_start, :formula_reference as formula_reference, :week as week, :lokasi as lokasi, :status as status , :nama_line as nama_line, :is_sync as is_sync
                    ",
                    [':nomo' => $lists["name"],
                     ':nama_fg' => $lists["fg_name"],
                     ':qty_batch' => $lists["scheduled_quantity"],
                     ':scheduled_start' => $lists["scheduled_start"],
                     ':formula_reference' => $lists["reference"],
                     ':week' => $lists["week"],
                     ':lokasi' => $lists["location"],
                     ':status' => $status,
                     ':nama_line' => $line_timbang,
                     ':is_sync' => "unsync"
                    ]
                );
                $result2 = $command2->queryAll();
            }
        }

        $sql="
            SELECT
                *
            FROM log_jadwal_timbang_rm
            WHERE week = '".$week."' OR week = '".$week2."' OR week = '".$week3."'";

        $list_jadwal = LogJadwalTimbangRm::findBySql($sql)->all();

        // $searchModel = new LogJadwalTimbangRmSearch();

        // $query = LogJadwalTimbangRm::find()->orWhere(['week' => $week2])->orWhere(['week' => $week])->orWhere(['week' => $week3])->orderBy(['id' => SORT_ASC]);

        // $dataProvider = new ActiveDataProvider([
        //     'query' => $query,
        //     'pagination' => [
        //         'pageSize' => 20,
        //     ],
        //     'sort' => [
        //         'defaultOrder' => [
        //             // 'id' => SORT_ASC,
        //             //'scheduled_start' => SORT_ASC,
        //             'week' => SORT_ASC,
        //         ]
        //     ],
        // ]);
        $searchModel = new LogJadwalTimbangRmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$week,$week2,$week3);

        return $this->render('schedule-list', [
            // 'model' => $this->findModel($id),
            'week' => $week,
            'list_jadwal' => $list_jadwal,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }*/

    public function actionScheduleList($sediaan=null)
    {
        date_default_timezone_set('Asia/Jakarta');
        $ddate = date("Y-m-d");
        $duedt = explode("-", $ddate);
        $date  = mktime(0, 0, 0, $duedt[1], $duedt[2], $duedt[0]);
        $week  = (int)date('W', $date);
        //$week2  = $week-1;
        //$week3  = $week+1;
        $week2  = (int)date('W', strtotime("-1 week", $date));
        $week4  = (int)date('W', strtotime("-2 week", $date));
        $week5  = (int)date('W', strtotime("-3 week", $date));
        $week3  = (int)date('W', strtotime("+1 week", $date));

        $this->layout = '//main-sidebar-collapse';

        $status = "unverified";

        $sqlweek = "select date_part('week'::text, now()) AS week";

        $sql="
            SELECT
                *
            FROM log_jadwal_timbang_rm_new
            WHERE week in (".$week5.",".$week4.",".$week2.",".$week.",".$week3.")
            order by week asc";

        $list_jadwal = LogJadwalTimbangRmNew::findBySql($sql)->all();

        // $searchModel = new LogJadwalTimbangRmSearch();

        // $query = LogJadwalTimbangRm::find()->orWhere(['week' => $week2])->orWhere(['week' => $week])->orWhere(['week' => $week3])->orderBy(['id' => SORT_ASC]);

        // $dataProvider = new ActiveDataProvider([
        //     'query' => $query,
        //     'pagination' => [
        //         'pageSize' => 20,
        //     ],
        //     'sort' => [
        //         'defaultOrder' => [
        //             // 'id' => SORT_ASC,
        //             //'scheduled_start' => SORT_ASC,
        //             'week' => SORT_ASC,
        //         ]
        //     ],
        // ]);
        $searchModel = new LogJadwalTimbangRmNewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$week,$week2,$week3);
        $dataProvider->query->andWhere("week in (".$week5.",".$week4.",".$week2.",".$week.",".$week3.") ");
        if (!empty($sediaan)){
            if($sediaan == 'L'){
                $dataProvider->query->andWhere("nama_line ilike '%LW%' ");
            }else if($sediaan == 'P'){
                $dataProvider->query->andWhere("nama_line ilike '%PW%' ");
            }else if($sediaan == 'S'){
                $dataProvider->query->andWhere("nama_line ilike '%SW%' ");
            }else if($sediaan == 'V'){
                $dataProvider->query->andWhere("nama_line ilike '%VW%' ");
            }else{
            }
        }
        $dataProvider->query->orderBy(['custom_order'=>SORT_ASC,'status'=>SORT_DESC,'scheduled_start'=>SORT_ASC]);

        return $this->render('schedule-list', [
            // 'model' => $this->findModel($id),
            'week' => $week,
            'sediaan' => $sediaan,
            'list_jadwal' => $list_jadwal,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionListScheduleReviewer()
    {
        date_default_timezone_set('Asia/Jakarta');
        $ddate = date("Y-m-d");
        $duedt = explode("-", $ddate);
        $date  = mktime(0, 0, 0, $duedt[1], $duedt[2], $duedt[0]);
        $week  = (int)date('W', $date);
        //$week2  = $week-1;
        //$week3  = $week+1;
        $week2  = (int)date('W', strtotime("-1 week", $date));
        $week4  = (int)date('W', strtotime("-2 week", $date));
        $week5  = (int)date('W', strtotime("-3 week", $date));
        $week3  = (int)date('W', strtotime("+1 week", $date));

        $this->layout = '//main-sidebar-collapse';

        // $status = "unverified";

        // $sqlweek = "select date_part('week'::text, now()) AS week";

        $username = Yii::$app->user->identity->username;
        if ($username == 'adminbr'){
            $sql="
            SELECT
                reviewer
            FROM list_reviewer
            ORDER by id asc";

            $reviewer = ListReviewer::findBySql($sql)->one();

            $searchModel = new LogApprovalBrSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // $dataProvider->query->andWhere("status_reviewer = 'IN PROGRESS' or status_reviewer = 'REJECT' or status_reviewer = 'QUEUE'")->orderBy(['timestamp_creator'=>SORT_DESC]);
            $dataProvider->query->select('log_approval_br.*, log.scheduled_start, log.status')
            ->distinct('log_approval_br.nomo')
            ->innerJoin('log_jadwal_timbang_rm_new log', "log_approval_br.nomo = log.nomo")
            ->andWhere("log_approval_br.status_reviewer = 'IN PROGRESS' or log_approval_br.status_reviewer = 'REJECT' or log_approval_br.status_reviewer = 'QUEUE'")
            ->andWhere("log.status is null")
            ->orderBy(['log.scheduled_start'=>SORT_ASC,'timestamp_creator'=>SORT_ASC]);
            $dataProvider->pagination = false;

            $searchModel2 = new LogApprovalBrSearch();
            $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);
            // $dataProvider2->query->andWhere("status_reviewer = 'APPROVED'")->orderBy(['timestamp_reviewer'=>SORT_DESC]);
            $dataProvider2->query->select('log_approval_br.*, log.scheduled_start, log.status')
            ->distinct('log_approval_br.nomo')
            ->innerJoin('log_jadwal_timbang_rm_new log', "log_approval_br.nomo = log.nomo")
            ->andWhere("log_approval_br.status_reviewer = 'APPROVED'")
            ->andWhere("log.status = 'verified'")
            ->orderBy(['log.scheduled_start'=>SORT_DESC,'timestamp_reviewer'=>SORT_DESC]);
            $dataProvider2->pagination = false;
        }else{
            $sql="
            SELECT
                reviewer
            FROM list_reviewer
            WHERE username = '".$username."'
            ORDER by id asc";

            $reviewer = ListReviewer::findBySql($sql)->one();

            $searchModel = new LogApprovalBrSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // $dataProvider->query->andWhere("reviewer = '".Yii::$app->user->identity->username."' and (status_reviewer = 'IN PROGRESS' or status_reviewer = 'REJECT' or status_reviewer = 'QUEUE')")->orderBy(['timestamp_creator'=>SORT_ASC]);
            $dataProvider->query->select('log_approval_br.*, log.scheduled_start, log.status')
            ->distinct('log_approval_br.nomo')
            ->innerJoin('log_jadwal_timbang_rm_new log', "log_approval_br.nomo = log.nomo")
            ->andWhere("log_approval_br.reviewer = :reviewer and (log_approval_br.status_reviewer = 'IN PROGRESS' or log_approval_br.status_reviewer = 'REJECT' or log_approval_br.status_reviewer = 'QUEUE')",[':reviewer'=>Yii::$app->user->identity->username])
            ->andWhere("log.status is null")
            ->orderBy(['log.scheduled_start'=>SORT_ASC,'timestamp_creator'=>SORT_ASC]);
            $dataProvider->pagination = false;

            $searchModel2 = new LogApprovalBrSearch();
            $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);
            // $dataProvider2->query->andWhere("reviewer = '".Yii::$app->user->identity->username."' and status_reviewer = 'APPROVED'")->orderBy(['timestamp_reviewer'=>SORT_DESC]);
            $dataProvider2->query->select('log_approval_br.*, log.scheduled_start, log.status')
            ->distinct('log_approval_br.nomo')
            ->innerJoin('log_jadwal_timbang_rm_new log', "log_approval_br.nomo = log.nomo")
            ->andWhere("log_approval_br.reviewer = :reviewer and log_approval_br.status_reviewer = 'APPROVED'",[':reviewer'=>Yii::$app->user->identity->username])
            ->andWhere("log.status = 'verified'")
            ->orderBy(['log.scheduled_start'=>SORT_DESC,'timestamp_reviewer'=>SORT_DESC]);
            $dataProvider2->pagination = false;
        }



        if (Yii::$app->request->post('hasEditable')) {

            $deliveryId = Yii::$app->request->post('editableKey');
            $modelDelivery = LogApprovalBr::findOne($deliveryId);

            $out = Json::encode(['output' => '', 'message' => '']);
            $post = [];


            $posted = current($_POST['LogApprovalBr']);
            $post['LogApprovalBr'] = $posted;
            if ($modelDelivery->load($post)) {
                $modelDelivery->save();
            }

            echo $out;
            return;

        }

        // $totalCount = Yii::$app->db
        //             ->createCommand('SELECT * FROM log_approval_br a join log_jadwal_timbang_rm_new b ON a.nomo = b.nomo AND a.reviewer = :reviewer', [':reviewer' => $reviewer->reviewer])
        //             ->queryScalar();

        // $dataProvider = new SqlDataProvider([
        //     'sql' => 'SELECT * FROM log_approval_br a join log_jadwal_timbang_rm_new b ON a.nomo = b.nomo AND a.reviewer = :reviewer',
        //     'params' => [':reviewer' => $reviewer->reviewer],
        //     'totalCount' => $totalCount,
        //     //'sort' =>false, to remove the table header sorting
        //     'sort' => [
        //         // 'attributes' => [
        //         //     'title' => [
        //         //         'asc' => ['title' => SORT_ASC],
        //         //         'desc' => ['title' => SORT_DESC],
        //         //         'default' => SORT_DESC,
        //         //         'label' => 'Post Title',
        //         //     ],
        //         //     'author' => [
        //         //         'asc' => ['author' => SORT_ASC],
        //         //         'desc' => ['author' => SORT_DESC],
        //         //         'default' => SORT_DESC,
        //         //         'label' => 'Name',
        //         //     ],
        //         //     'created_on'
        //         // ],
        //     ],
        //     'pagination' => [
        //         'pageSize' => 10,
        //     ],
        // ]);

        // if (empty($dataProvider)){
        //     $dataProvider = NULL;
        // }

        return $this->render('list-schedule-reviewer', [
            // 'model' => $this->findModel($id),
            // 'week' => $week,
            // 'list_jadwal' => $list_jadwal,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'dataProvider2' => $dataProvider2,
            'searchModel2' => $searchModel2,
        ]);
    }

    public function actionListScheduleApprover()
    {
        date_default_timezone_set('Asia/Jakarta');
        $ddate = date("Y-m-d");
        $duedt = explode("-", $ddate);
        $date  = mktime(0, 0, 0, $duedt[1], $duedt[2], $duedt[0]);
        $week  = (int)date('W', $date);
        //$week2  = $week-1;
        //$week3  = $week+1;
        $week2  = (int)date('W', strtotime("-1 week", $date));
        $week4  = (int)date('W', strtotime("-2 week", $date));
        $week5  = (int)date('W', strtotime("-3 week", $date));
        $week3  = (int)date('W', strtotime("+1 week", $date));

        $this->layout = '//main-sidebar-collapse';

        // $status = "unverified";

        // $sqlweek = "select date_part('week'::text, now()) AS week";

        $username = Yii::$app->user->identity->username;

        if ($username=='adminbr'){
           $sql="
            SELECT
                approver
            FROM list_approver
            WHERE username = '".$username."'
            ORDER by id asc";

            $reviewer = ListReviewer::findBySql($sql)->one();

            $searchModel = new LogApprovalBrSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // $dataProvider->query->andWhere("(status_approver = 'IN PROGRESS' or status_approver = 'REJECT' or status_approver = 'QUEUE') and status_reviewer = 'APPROVED'")->orderBy(['timestamp_reviewer'=>SORT_DESC]);

            $dataProvider->query->select('log_approval_br.*, log.scheduled_start, log.status')
            ->distinct('log_approval_br.nomo')
            ->innerJoin('log_jadwal_timbang_rm_new log', "log_approval_br.nomo = log.nomo")
            ->andWhere("(log_approval_br.status_approver = 'IN PROGRESS' or log_approval_br.status_approver = 'REJECT' or log_approval_br.status_approver = 'QUEUE') and log_approval_br.status_reviewer = 'APPROVED'")
            ->andWhere("log.status is null")
            ->orderBy(['log.scheduled_start'=>SORT_ASC,'timestamp_reviewer'=>SORT_ASC]);
            $dataProvider->pagination = false;

            $searchModel2 = new LogApprovalBrSearch();
            $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);
            // $dataProvider2->query->andWhere("status_approver = 'APPROVED'")->orderBy(['timestamp_approver'=>SORT_DESC]);

            $dataProvider2->query->select('log_approval_br.*, log.scheduled_start, log.status')
            ->distinct('log_approval_br.nomo')
            ->innerJoin('log_jadwal_timbang_rm_new log', "log_approval_br.nomo = log.nomo")
            ->andWhere("status_approver = 'APPROVED'")
            ->andWhere("log.status = 'verified'")
            ->orderBy(['log.scheduled_start'=>SORT_DESC,'timestamp_approver'=>SORT_DESC]);
            $dataProvider2->pagination = false;

        }else{
            $sql="
            SELECT
                approver
            FROM list_approver
            WHERE username = '".$username."'
            ORDER by id asc";

            $reviewer = ListReviewer::findBySql($sql)->one();

            $searchModel = new LogApprovalBrSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->andWhere("approver = '".Yii::$app->user->identity->username."' and (status_approver = 'IN PROGRESS' or status_approver = 'REJECT' or status_approver = 'QUEUE') and status_reviewer = 'APPROVED' ")->orderBy(['timestamp_reviewer'=>SORT_DESC]);

            $dataProvider->query->select('log_approval_br.*, log.scheduled_start, log.status')
            ->distinct('log_approval_br.nomo')
            ->innerJoin('log_jadwal_timbang_rm_new log', "log_approval_br.nomo = log.nomo")
            ->andWhere("approver = :approver and (log_approval_br.status_approver = 'IN PROGRESS' or log_approval_br.status_approver = 'REJECT' or log_approval_br.status_approver = 'QUEUE') and log_approval_br.status_reviewer = 'APPROVED'",[':approver'=>Yii::$app->user->identity->username])
            ->andWhere("log.status is null")
            ->orderBy(['log.scheduled_start'=>SORT_ASC,'timestamp_reviewer'=>SORT_ASC]);
            $dataProvider->pagination = false;



            $searchModel2 = new LogApprovalBrSearch();
            $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);
            // $dataProvider2->query->andWhere("approver = '".Yii::$app->user->identity->username."' and status_approver = 'APPROVED'")->orderBy(['timestamp_approver'=>SORT_DESC]);
            $dataProvider2->query->select('log_approval_br.*, log.scheduled_start, log.status')
            ->distinct('log_approval_br.nomo')
            ->innerJoin('log_jadwal_timbang_rm_new log', "log_approval_br.nomo = log.nomo")
            ->andWhere("approver = :approver and status_approver = 'APPROVED'",[':approver'=>Yii::$app->user->identity->username])
            ->andWhere("log.status = 'verified'")
            ->orderBy(['log.scheduled_start'=>SORT_DESC,'timestamp_approver'=>SORT_DESC]);
            $dataProvider2->pagination = false;

        }



        if (Yii::$app->request->post('hasEditable')) {

            $deliveryId = Yii::$app->request->post('editableKey');
            $modelDelivery = LogApprovalBr::findOne($deliveryId);

            $out = Json::encode(['output' => '', 'message' => '']);
            $post = [];


            $posted = current($_POST['LogApprovalBr']);
            $post['LogApprovalBr'] = $posted;
            if ($modelDelivery->load($post)) {
                $modelDelivery->save();
            }

            echo $out;
            return;

        }

        // $totalCount = Yii::$app->db
        //             ->createCommand('SELECT * FROM log_approval_br a join log_jadwal_timbang_rm_new b ON a.nomo = b.nomo AND a.reviewer = :reviewer', [':reviewer' => $reviewer->reviewer])
        //             ->queryScalar();

        // $dataProvider = new SqlDataProvider([
        //     'sql' => 'SELECT * FROM log_approval_br a join log_jadwal_timbang_rm_new b ON a.nomo = b.nomo AND a.reviewer = :reviewer',
        //     'params' => [':reviewer' => $reviewer->reviewer],
        //     'totalCount' => $totalCount,
        //     //'sort' =>false, to remove the table header sorting
        //     'sort' => [
        //         // 'attributes' => [
        //         //     'title' => [
        //         //         'asc' => ['title' => SORT_ASC],
        //         //         'desc' => ['title' => SORT_DESC],
        //         //         'default' => SORT_DESC,
        //         //         'label' => 'Post Title',
        //         //     ],
        //         //     'author' => [
        //         //         'asc' => ['author' => SORT_ASC],
        //         //         'desc' => ['author' => SORT_DESC],
        //         //         'default' => SORT_DESC,
        //         //         'label' => 'Name',
        //         //     ],
        //         //     'created_on'
        //         // ],
        //     ],
        //     'pagination' => [
        //         'pageSize' => 10,
        //     ],
        // ]);

        // if (empty($dataProvider)){
        //     $dataProvider = NULL;
        // }

        return $this->render('list-schedule-approver', [
            // 'model' => $this->findModel($id),
            // 'week' => $week,
            // 'list_jadwal' => $list_jadwal,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'dataProvider2' => $dataProvider2,
            'searchModel2' => $searchModel2,
        ]);
    }

    public function actionVerificationHistory()
    {
        date_default_timezone_set('Asia/Jakarta');

        $this->layout = '//main-sidebar-collapse';

        $searchModel = new LogVerifiedBrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query->andWhere("week in (".$week5.",".$week4.",".$week2.",".$week.",".$week3.") ")->orderBy(['custom_order'=>SORT_ASC,'scheduled_start'=>SORT_ASC]);

        return $this->render('verification-history', [
            // 'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionListVerificationHistory($nomo)
    {
        $searchModelVerification = new LogVerifiedBrSearch();
        $verificationHistory = $searchModelVerification->search2(Yii::$app->request->queryParams,$nomo);
        return $this->render('list-verification-history',[
            'verificationHistory'=>$verificationHistory,
        ]);
        // print_r ($data);
    }

    public function insertBoM($nomo)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        SELECT *
        FROM daily_consumption_default
        WHERE  name = :nomo
        ",
        [':nomo'=> $nomo,
        ]);

        $list_formula = $command->queryAll();

        // GET LIST TASK
        $sql = "
        SELECT *
        FROM scm_planner
        WHERE  nomo = '".$nomo."'
        ";

        $scm_planner = ScmPlanner::findBySql($sql)->all();
        $koitem_fg = $scm_planner->koitem_fg;

    }

    public function actionCopyReference($kode_bulk,$reference,$reference_copy,$qty_batch)
    {
        //Cek apakah reference_copy ada di master data
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        SELECT *
        FROM log_formula_breakdown
        WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch order by id asc
        ",
        [':kode_bulk'=> $kode_bulk,
         ':reference'=>$reference_copy,
         ':qty_batch'=>$qty_batch,
        ]);

        $list_formula_copy = $command->queryAll();

        // $connection5 = Yii::$app->db;
        // $command5 = $connection5->createCommand("

        // SELECT *
        // FROM log_formula_breakdown
        // WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference
        // ",
        // [':kode_bulk'=> $kode_bulk,
        //  ':reference'=>$reference_copy,
        // //  ':qty_batch'=>$qty_batch,
        // ]);

        // $is_reference_copy_exist = $command5->queryAll();

        if (!empty($list_formula_copy)){
            $connection6 = Yii::$app->db;
            $command6 = $connection6->createCommand("

            DELETE
            FROM log_formula_breakdown
            WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch
            ",
            [':kode_bulk'=> $kode_bulk,
            ':reference'=>$reference,
            ':qty_batch'=>$qty_batch,
            ]);

            $delete = $command6->execute();

            $connection7 = Yii::$app->db;
            $command7 = $connection7->createCommand("

            DELETE
            FROM log_formula_breakdown_split
            WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch
            ",
            [':kode_bulk'=> $kode_bulk,
            ':reference'=>$reference,
            ':qty_batch'=>$qty_batch,
            ]);

            $delete = $command7->execute();

            foreach ($list_formula_copy as $lists) {

                $connection2 = Yii::$app->db;
                $command2 = $connection2->createCommand("
                    INSERT INTO log_formula_breakdown (kode_bulk,nama_fg,formula_reference,lokasi,scheduled_start,week,kode_bb,kode_internal,qty,uom,timbangan,nama_line,nama_bb,is_split,operator,siklus,qty_batch,kode_olah,action,keterangan,is_repack,no_dokumen,no_revisi,urutan,serial_manual,version)
                    SELECT :kode_bulk as kode_bulk, :nama_fg as nama_fg, :formula_reference as formula_reference, :lokasi as lokasi, :scheduled_start as scheduled_start, :week as week, :kode_bb as kode_bb, :kode_internal as kode_internal,:qty as qty, :uom as uom, :timbangan as timbangan, :nama_line as nama_line, :nama_bb as nama_bb, :is_split as is_split, :operator as operator, :siklus as siklus, :qty_batch as qty_batch, :kode_olah as kode_olah, :action as action, :keterangan as keterangan, :is_repack as is_repack, :no_dokumen as no_dokumen, :no_revisi as no_revisi, :urutan as urutan, :serial_manual as serial_manual, :version as version;
                    ",
                    [':kode_bulk' => $lists["kode_bulk"],
                    ':nama_fg' => $lists["nama_fg"],
                    ':formula_reference' => $reference,
                    ':lokasi' => $lists["lokasi"],
                    ':scheduled_start' => $lists["scheduled_start"],
                    ':week' => $lists["week"],
                    ':kode_bb' => $lists["kode_bb"],
                    ':kode_internal' => $lists["kode_internal"],
                    ':qty' => $lists["qty"],
                    ':uom' => $lists["uom"],
                    ':timbangan' => $lists["timbangan"],
                    ':nama_line' => $lists["nama_line"],
                    ':nama_bb' => $lists["nama_bb"],
                    ':is_split' => $lists["is_split"],
                    ':operator' => $lists["operator"],
                    ':siklus' => $lists["siklus"],
                    ':qty_batch' => $lists["qty_batch"],
                    ':kode_olah' => $lists["kode_olah"],
                    ':action' => $lists["action"],
                    ':keterangan' => $lists["keterangan"],
                    ':is_repack' => $lists["is_repack"],
                    ':no_dokumen' => $lists["no_dokumen"],
                    ':no_revisi' => $lists["no_revisi"],
                    ':urutan' => $lists["urutan"],
                    ':serial_manual' => $lists["serial_manual"],
                    ':version' => $lists["version"],
                    ]
                );
                $result2 = $command2->queryAll();

                if ($lists["is_split"] > 0 and $lists["is_split"] < 99){

                    // GET THE DETAIL OF SCHEDULE FROM DB FRO
                    $sql = "
                    SELECT id
                    FROM log_formula_breakdown WHERE kode_bulk = '".$kode_bulk."' and formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." ORDER BY id desc
                    ";

                    $row = LogFormulaBreakdown::findBySql($sql)->one();

                    $connection2 = Yii::$app->db;
                    $command2 = $connection2->createCommand("

                    SELECT *
                    FROM log_formula_breakdown_split
                    WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch AND log_formula_breakdown_id = :id
                    ORDER BY id ASC
                    ",
                    [':kode_bulk'=> $kode_bulk,
                    ':reference'=>$reference_copy,
                    ':id'=>$lists['id'],
                    ':qty_batch'=>$qty_batch
                    ]);

                    $list_breakdown = $command2->queryAll();

                    if (!empty($list_breakdown)){
                        foreach ($list_breakdown as $lists2) {

                            $connection2 = Yii::$app->db;
                            $command2 = $connection2->createCommand("
                                INSERT INTO log_formula_breakdown_split (log_formula_breakdown_id,kode_bulk,qty,uom,formula_reference,siklus,operator,timbangan,kode_olah,keterangan,qty_batch)
                                SELECT :log_formula_breakdown_id as log_formula_breakdown_id, :kode_bulk as kode_bulk, :qty as qty, :uom as uom, :formula_reference as formula_reference, :siklus as siklus, :operator as operator, :timbangan as timbangan, :kode_olah as kode_olah, :keterangan as keterangan, :qty_batch as qty_batch
                                ",
                                [':kode_bulk' => $lists2["kode_bulk"],
                                ':log_formula_breakdown_id' => $row->id,
                                ':formula_reference' => $reference,
                                ':qty' => $lists2["qty"],
                                ':uom' => $lists2["uom"],
                                ':timbangan' => $lists2["timbangan"],
                                ':operator' => $lists2["operator"],
                                ':siklus' => $lists2["siklus"],
                                ':kode_olah' => $lists2["kode_olah"],
                                ':keterangan' => $lists2["keterangan"],
                                ':qty_batch' => $qty_batch
                                ]
                            );
                            $result3 = $command2->queryAll();
                        }
                    }
                }
            }

            $this->queryUpdSerialManual($kode_bulk, $reference, $qty_batch);

            if ($result2){
                return 1;
            }else{
                return 0;
            }
        }else{
            return 2;
        }

    }

    public function actionCheckData($nomo)
    {
        // GET THE DETAIL OF SCHEDULE FROM DB WAREHOUSE
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        SELECT distinct(name),fg_name,scheduled_quantity,scheduled_start,reference,week,location
        FROM daily_consumption_default
        WHERE  name = :nomo
        ",
        [':nomo'=> $nomo,
        ]);

        $row1 = $command->queryAll();

        // GET THE DETAIL OF SCHEDULE FROM DB FRO
        $sql = "
        SELECT *
        FROM log_jadwal_timbang_rm_new
        WHERE  nomo = '".$nomo."'
        ";

        $row2 = LogJadwalTimbangRmNew::findBySql($sql)->one();
        //$line = $scm_planner->line_timbang;

        if (($row2->nama_fg==$row1[0]['fg_name']) && ($row2->scheduled_start==$row1[0]['scheduled_start']) && ($row2->lokasi==$row1[0]['location']) && ($row2->formula_reference==$row1[0]['reference'])){

            echo 1;

        } else {
            echo 0;
        }

    }

    public function actionCheckHeader($nomo,$formula_reference,$qty_batch)
    {
        $kode_bulk = explode('MO-',$nomo)[1];
        $kode_bulk = explode('/',$kode_bulk)[0];
        $sql = "SELECT * FROM log_br_detail WHERE kode_bulk = :kode_bulk and formula_reference = :formula_reference and qty_batch = :qty_batch";
        $br_detail = LogBrDetail::findBySql($sql,[':kode_bulk'=>$kode_bulk,':formula_reference'=>$formula_reference,':qty_batch'=>$qty_batch])->one();

        $sql_approval = "SELECT * FROM log_approval_br WHERE nomo = :nomo";
        $approval = LogApprovalBr::findBySql($sql_approval,[':nomo'=>$nomo])->one();

        if (!empty($br_detail)){
            if (!empty($br_detail->status_formula) and !empty($br_detail->no_dokumen) and !empty($br_detail->bentuk_sediaan) and !empty($br_detail->brand) and (!empty($br_detail->no_revisi) or $br_detail->no_revisi == 0)){
                $no_dokumen = explode('.',$br_detail->no_dokumen);
                $count_parse = count($no_dokumen);
                if ($count_parse == 5 and strpos(strtolower($no_dokumen[3]),'f')!==false){
                    if (!empty($approval)){
                        if (!empty($approval->reviewer) and !empty($approval->approver)){
                            echo 'ok';
                        }else{
                            echo 'approval-null';
                        }
                    }else{
                        echo 'approval-null';
                    }
                }else{
                    echo 'dokumen-fail';
                }
            }else{
                echo 'header-null';
            }
        }else{
            echo 'header-null';
        }

    }

    public function actionSyncKodebb($nomo,$id,$value)
    {
        $kode = explode( '/',$nomo)[0];
        $kode_bulk = explode( 'MO-',$kode)[1];

        // GET THE DETAIL OF SCHEDULE FROM DB WAREHOUSE
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        SELECT component,total_component_qty,total_component_uom,component_internal_code,scheduled_quantity,reference
        FROM daily_consumption_default
        WHERE  name = :nomo and component_code = :component_code
        ",
        [':nomo'=> $nomo,
         ':component_code' => $value,
        ]);

        $row1 = $command->queryAll();

        if (!empty($row1)){
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

                UPDATE log_formula_breakdown
                SET nama_bb = :nama_bb, qty = :qty, uom = :uom, kode_internal = :kode_internal
                WHERE id = :id;
                ",
                [':id'=> $id,
                 ':nama_bb'=>$row1[0]["component"],
                 ':qty'=>$row1[0]["total_component_qty"],
                 ':uom'=>$row1[0]["total_component_uom"],
                 ':kode_internal'=>$row1[0]["component_internal_code"],
                ]);

            $result = $command->queryAll();

            $connection = Yii::$app->db;
            $command = $connection->createCommand("

                UPDATE log_formula_breakdown
                SET nama_bb = :nama_bb, kode_internal = :kode_internal
                WHERE kode_bb = :kode_bb and kode_bulk = :kode_bulk and formula_reference = :formula_reference and qty_batch = :qty_batch
                ",
                ['kode_bb'=> $value,
                 ':kode_bulk'=>$kode_bulk,
                 ':formula_reference'=>$row1[0]['reference'],
                 ':qty_batch'=>$row1[0]['scheduled_quantity'],
                 ':nama_bb'=>$row1[0]["component"],
                 ':kode_internal'=>$row1[0]["component_internal_code"],
                ]);

            $result = $command->queryAll();

            if ($result){
                $value = array(
                "status"=>"success", "nama_bb"=>$row1[0]["component"], "qty"=>$row1[0]["total_component_qty"], "uom"=>$row1[0]["total_component_uom"], "kode_internal"=>$row1[0]["component_internal_code"]);
            } else {
                $value = array(
                "status"=>"not-success2");
            }
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    public function actionSyncKodeinternal($kode_bb,$id)
    {
        // GET THE DETAIL OF SCHEDULE FROM DB WAREHOUSE
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        SELECT kode_internal
        FROM log_formula_breakdown
        WHERE  kode_bb = :kode_bb AND kode_internal is not NULL
        ",
        [':kode_bb'=> $kode_bb
        ]);

        $row1 = $command->queryOne();

        if (!empty($row1)){
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

                UPDATE log_formula_breakdown
                SET kode_internal = :kode_internal
                WHERE id = :id;
                ",
                [':id'=> $id,
                 ':kode_internal'=>$row1["kode_internal"],
                ]);

            $result = $command->queryAll();

            if ($result){
                $value = array(
                "status"=>"success", "kode_internal"=>$row1["kode_internal"]);
            } else {
                $value = array(
                "status"=>"not-success2");
            }
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    public function actionUpdateApproval($nomo,$kode_bulk,$reference,$qty_batch)
    {
        date_default_timezone_set('Asia/Jakarta');

        $sql = "
        SELECT *
        FROM log_approval_br
        WHERE  nomo = '".$nomo."'
        ";

        $log = LogApprovalBr::findBySql($sql)->one();

        // if ($log->current_pic <> 'CREATOR'){
            // echo 2;
        // } else {
            $connection = Yii::$app->db;
            $command = $connection->createCommand("
                UPDATE log_approval_br
                SET status_creator = :status_creator, timestamp_creator = :timestamp_creator,status_reviewer = :status_reviewer, timestamp_reviewer = :timestamp_reviewer, current_pic = 'REVIEWER', current_status = 'QUEUE'
                WHERE nomo = :nomo;
                ",
                [':nomo' => $nomo,
                 ':status_reviewer' => 'QUEUE',
                 ':timestamp_reviewer' => date('Y-m-d H:i:s'),
                 ':status_creator' => 'SENT',
                 ':timestamp_creator' => date('Y-m-d H:i:s'),
                ]
            );

            $result = $command->queryAll();

            $connection2 = Yii::$app->getDb();
            $command2 = $connection2->createCommand("

            UPDATE log_formula_breakdown
            SET is_lock = 1
            WHERE kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch;
            ",
            [':kode_bulk'=> $kode_bulk,
             ':reference'=>$reference,
             ':qty_batch'=>$qty_batch
            ]);

            $result2 = $command2->queryAll();

            $connection3 = Yii::$app->getDb();
            $command3 = $connection3->createCommand("

            UPDATE log_br_detail
            SET is_lock = 1
            WHERE kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch;
            ",
            [':kode_bulk'=> $kode_bulk,
             ':reference'=>$reference,
             ':qty_batch'=>$qty_batch
            ]);

            $result3 = $command3->queryAll();

            if ($result and $result2 and $result3){
                echo 1;
            } else {
                echo 0;
            }
        // }

    }

    public function actionUpdateStatusReviewer($nomo)
    {
        date_default_timezone_set('Asia/Jakarta');

        $connection = Yii::$app->db;
        $command = $connection->createCommand("
            UPDATE log_approval_br
            SET status_reviewer = :status_reviewer, timestamp_reviewer = :timestamp_reviewer, timestamp_approver = :timestamp_approver, current_pic = 'REVIEWER', current_status = 'IN PROGRESS'
            WHERE nomo = :nomo;
            ",
            [':nomo' => $nomo,
             ':status_reviewer' => 'IN PROGRESS',
             ':timestamp_reviewer' => date('Y-m-d H:i:s'),
             ':timestamp_approver' => date('Y-m-d H:i:s'),
            ]
        );

        $result = $command->queryAll();

        if ($result){
            echo 1;
        } else {
            echo 0;
        }

    }

    public function actionUpdateStatusApprover($nomo)
    {
        date_default_timezone_set('Asia/Jakarta');

        $connection = Yii::$app->db;
        $command = $connection->createCommand("
            UPDATE log_approval_br
            SET status_approver = :status_approver, timestamp_approver = :timestamp_approver, current_pic = 'APPROVER', current_status = 'IN PROGRESS'
            WHERE nomo = :nomo;
            ",
            [':nomo' => $nomo,
             ':status_approver' => 'IN PROGRESS',
             ':timestamp_approver' => date('Y-m-d H:i:s'),
            ]
        );

        $result = $command->queryAll();

        if ($result){
            echo 1;
        } else {
            echo 0;
        }

    }

    public function actionSyncData($nomo)
    {
        // GET THE DETAIL OF SCHEDULE FROM DB WAREHOUSE
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        SELECT distinct(name),fg_name,scheduled_quantity,scheduled_start,reference,week,location
        FROM daily_consumption_default
        WHERE  name = :nomo
        ",
        [':nomo'=> $nomo,
        ]);

        $row1 = $command->queryAll();

        // GET THE DETAIL OF SCHEDULE FROM DB FRO
        $sql = "
        SELECT *
        FROM log_jadwal_timbang_rm_new
        WHERE  nomo = '".$nomo."'
        ";

        $row2 = LogJadwalTimbangRmNew::findBySql($sql)->one();
        //$line = $scm_planner->line_timbang;

        // GET THE NEWEST LINE NAME FROM DB FRO
        $sql2 = "
        SELECT line_timbang
        FROM scm_planner
        WHERE  nomo = '".$nomo."'
        ";

        $scm_planner = ScmPlanner::findBySql($sql2)->one();

        if (($row2->nama_fg==$row1[0]['fg_name']) && ($row2->qty_batch==$row1[0]['scheduled_quantity']) && ($row2->scheduled_start==$row1[0]['scheduled_start']) && ($row2->week==$row1[0]['week']) && ($row2->lokasi==$row1[0]['location']) && ($row2->nama_line==str_replace(' ','',$scm_planner->line_timbang)) && ($row2->formula_reference==$row1[0]['reference'])){

            echo 2;

        } else {
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE log_jadwal_timbang_rm_new
            SET nama_fg = :nama_fg,
                qty_batch = :qty_batch,
                scheduled_start = :scheduled_start,
                week = :week,
                lokasi = :lokasi,
                nama_line = :nama_line,
                formula_reference = :reference
            WHERE nomo = :nomo;
            ",
            [':nomo'=> $nomo,
             ':nama_fg'=> $row1[0]['fg_name'],
             ':qty_batch'=> $row1[0]['scheduled_quantity'],
             ':scheduled_start'=> $row1[0]['scheduled_start'],
             ':week'=> $row1[0]['week'],
             ':lokasi'=> $row1[0]['location'],
             ':nama_line' => str_replace(' ','',$scm_planner->line_timbang),
             // ':nama_line' => "LWE 02",
             ':reference' => $row1[0]['reference']
            ]);

            $result = $command->queryAll();

            if ($result) {
                echo 1;
            } else {
                echo 0;
            }
        }

    }

    /**
     * Displays a single LogFormulaBreakdown model.
     * @param integer $id
     * @return mixed
     */
    public function actionInputDetailBr($nomo,$reference)
    {
        $searchModel = new LogFormulaBreakdownSearch();
        $kode_bulk = strtok($nomo, '/');
        //$query->andWhere(['nomo' => $nomo]);
        //$searchModel->kode_bulk = $nomo;
        //$dataProvider = $searchModel->search($query);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new FlowInputMo();

        $this->layout = '//main-sidebar-collapse';

        date_default_timezone_set('Asia/Jakarta');
        $ddate = date("Y-m-d");
        $duedt = explode("-", $ddate);
        $date  = mktime(0, 0, 0, $duedt[1], $duedt[2], $duedt[0]);
        $week  = (int)date('W', $date);

        $connection = Yii::$app->db_warehouse;
        $command = $connection->createCommand("

        SELECT *
        FROM daily_consumption_default
        WHERE  name = :nomo
        ",
        [':nomo'=> $nomo,
        ]);

        $list_formula = $command->queryAll();

        $connection2 = Yii::$app->db;
        $command2 = $connection2->createCommand("

        SELECT *
        FROM log_formula_breakdown
        WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference ORDER BY id ASC
        ",
        [':kode_bulk'=> $kode_bulk,
         ':reference'=> $reference,
        ]);

        $bom = $command2->queryAll();

        $timbangan = "";
        $nama_line = "null";
        $is_split = 0;
        $operator = 0;
        $siklus = 1;

        if (empty($bom)){
            foreach ($list_formula as $lists) {
                if($lists["total_component_uom"] == "kg"){
                    if($lists["total_component_qty"] > 30){
                        $timbangan = "LWB 01";
                    }
                }

                $connection2 = Yii::$app->db;
                $command2 = $connection2->createCommand("
                    INSERT INTO log_formula_breakdown (kode_bulk,nama_fg,formula_reference,lokasi,scheduled_start,week,kode_bb,qty,uom,timbangan,nama_line,nama_bb,is_split,operator,siklus)
                    SELECT :kode_bulk as kode_bulk, :nama_fg as nama_fg, :formula_reference as formula_reference, :lokasi as lokasi, :scheduled_start as scheduled_start, :week as week, :kode_bb as kode_bb, :qty as qty, :uom as uom, :timbangan as timbangan, :nama_line as nama_line, :nama_bb as nama_bb, :is_split as is_split, :operator as operator, :siklus as siklus
                    ",
                    [':kode_bulk' => $kode_bulk,
                     ':nama_fg' => $lists["fg_name"],
                     ':formula_reference' => $lists["reference"],
                     ':lokasi' => $lists["location"],
                     ':scheduled_start' => $lists["scheduled_start"],
                     ':week' => $lists["week"],
                     ':kode_bb' => $lists["component_code"],
                     ':qty' => $lists["total_component_qty"],
                     ':uom' => $lists["total_component_uom"],
                     ':timbangan' => $timbangan,
                     ':nama_line' => $nama_line,
                     ':nama_bb' => $lists["component"],
                     ':is_split' => $is_split,
                     ':operator' => $operator,
                     ':siklus' => $siklus
                    ]
                );
                $result2 = $command2->queryAll();
            }

            $connection2 = Yii::$app->db;
            $command2 = $connection2->createCommand("

            SELECT *
            FROM log_formula_breakdown
            WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference ORDER BY id ASC
            ",
            [':kode_bulk'=> $kode_bulk,
             ':reference' => $reference
            ]);

            $bom = $command2->queryAll();
        }

        $sql5 = "
        SELECT COUNT(*)
        FROM log_formula_breakdown
        WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND is_split <> 0
        ";

        $is_split = LogFormulaBreakdown::findBySql($sql5)->scalar();

        $sql3="
            SELECT
                qty_batch
            FROM log_jadwal_timbang_rm
            WHERE nomo = '".$nomo."'";

        $qty_batch = LogJadwalTimbangRm::findBySql($sql3)->scalar();

        if ($is_split != 0){
            $sql5 = "
            SELECT *
            FROM log_formula_breakdown_split
            WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' ORDER BY timestamp ASC
            ";

            $data_split = LogFormulaBreakdownSplit::findBySql($sql5)->all();

            return $this->render('input-detail-br', [
                // 'model' => $this->findModel($id),
                'dataProvider' => $dataProvider,
                'week' => $week,
                'list_formula' => $list_formula,
                'nomo' => $nomo,
                'model' => $model,
                'bom' => $bom,
                'searchModel' => $searchModel,
                'qty_batch' => $qty_batch,
                'reference' => $reference,
                'nomo' => $nomo,
                'is_split' => $is_split,
                'data_split' => $data_split
            ]);
        } else {
            return $this->render('input-detail-br', [
                // 'model' => $this->findModel($id),
                'dataProvider' => $dataProvider,
                'week' => $week,
                'list_formula' => $list_formula,
                'nomo' => $nomo,
                'model' => $model,
                'bom' => $bom,
                'searchModel' => $searchModel,
                'qty_batch' => $qty_batch,
                'reference' => $reference,
                'nomo' => $nomo,
                'is_split' => $is_split,
            ]);
        }
    }

    public function actionSyncSplitBb($nomo,$reference,$qty_batch){
        $kode_bulk = strtok($nomo, '/');
        $kode_bulk = preg_replace("/MO-/","",$kode_bulk,1);

        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        SELECT DISTINCT ON (log_formula_breakdown_id) log_formula_breakdown_id as id_parent
        FROM log_formula_breakdown_split
        WHERE log_formula_breakdown_id in (select id from log_formula_breakdown where kode_bulk = :kode_bulk and formula_reference = :reference and qty_batch = :qty_batch order by id asc)
        ",
        [':kode_bulk'=> $kode_bulk,
         ':reference'=> $reference,
         ':qty_batch'=> $qty_batch
        ]);

        $list_id_parent= $command->queryAll();

        // print_r($list_id_parent);
        // exit();

        foreach($list_id_parent as $list){
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

            SELECT COUNT(id)
            FROM log_formula_breakdown_split
            WHERE  log_formula_breakdown_id = :id_parent
            ",
            [':id_parent'=> $list['id_parent'],
            ]);

            $jumlah_split= $command->queryScalar();
            // print_r($jumlah_split);
            // exit();
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

            UPDATE log_formula_breakdown
            SET is_split = :is_split
            WHERE  id = :id_parent
            ",
            [':id_parent'=> $list['id_parent'],
             ':is_split'=> $jumlah_split
            ]);

            $update= $command->execute();

        }
    }

    /**
     * Displays a single LogFormulaBreakdown model.
     * @param integer $id
     * @return mixed
     */
    public function actionInputDetailBr2($nomo,$reference,$reference_copy=null,$qty_batch)
    {
        $this->actionSyncSplitBb($nomo,$reference,$qty_batch);
        $searchModel = new LogFormulaBreakdownSearch();
        $kode_bulk = strtok($nomo, '/');
        $kode_bulk = preg_replace("/MO-/","",$kode_bulk,1);
        //$query->andWhere(['nomo' => $nomo]);
        //$searchModel->kode_bulk = $nomo;
        //$dataProvider = $searchModel->search($query);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModelVerification = new LogVerifiedBrSearch();
        $verificationHistory = $searchModelVerification->search(Yii::$app->request->queryParams,$nomo);

        $pic = Yii::$app->user->identity->username;

        $model = new FlowInputMo();

        $this->layout = '//main-sidebar-collapse';

        date_default_timezone_set('Asia/Jakarta');
        $ddate = date("Y-m-d");
        $duedt = explode("-", $ddate);
        $date  = mktime(0, 0, 0, $duedt[1], $duedt[2], $duedt[0]);
        $week  = (int)date('W', $date);

        $list_formula = $this->querySelDCD($nomo);
        $replace_arr = ['bulk ','Bulk ','BULK ',' bulk',' Bulk',' BULK'];
        if (strpos(strtolower($list_formula[0]['bulk_name']),'bulk')!==false){
            $nama_bulk = str_replace($replace_arr,'',$list_formula[0]['bulk_name']);
        }
        $bom = $this->querySelOldLFB($kode_bulk,$reference,$qty_batch);
        if (count($bom) > 0) {
          $this->autosyncBOMwBR('old',$kode_bulk,$nomo,$reference,$reference_copy,$qty_batch);
        }

        $sql_fim="
            SELECT
                *
            FROM flow_input_mo
            WHERE nomo = '".$nomo."' AND posisi = 'PENIMBANGAN'";

        $fim = ScmPlanner::findBySql($sql_fim)->all();
        if (empty($fim)){
            $status_timbang = 'BELUM DITIMBANG';
        } else {
            $status_timbang = 'SUDAH / SEDANG DITIMBANG';
        }

        $sqll="
            SELECT
                sediaan,nama_fg
            FROM scm_planner
            WHERE nomo = '".$nomo."'";

        $sp = ScmPlanner::findBySql($sqll)->one();
        $sediaan = $sp->sediaan;

        $connection3 = Yii::$app->db;
        $command3 = $connection3->createCommand("

        SELECT *
        FROM log_br_detail
        WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch
        ",
        [':kode_bulk'=> $kode_bulk,
         ':reference'=> $reference,
         ':qty_batch' => $qty_batch
        ]);

        $existBrDetail = $command3->queryAll();

        if(strpos(strtolower($list_formula[0]['fg_name']), 'wardah') !== false){
            $brand = 'Wardah';
        } else if(strpos(strtolower($list_formula[0]['fg_name']), 'make') !== false){
            $brand = 'Make Over';
        } else if(strpos(strtolower($list_formula[0]['fg_name']), 'emina') !== false){
            $brand = 'Emina';
        } else if(strpos(strtolower($list_formula[0]['fg_name']), 'kahf') !== false){
            $brand = 'Kahf';
        }  else if(strpos(strtolower($list_formula[0]['fg_name']), 'omg') !== false){
            $brand = 'OMG';
        }  else if(strpos(strtolower($list_formula[0]['fg_name']), 'putri') !== false){
            $brand = 'Putri';
        }  else if(strpos(strtolower($list_formula[0]['fg_name']), 'biodef') !== false){
            $brand = 'Biodef';
        } else {
            if(strpos(strtolower($sp->nama_fg), 'wardah') !== false){
                $brand = 'Wardah';
            } else if(strpos(strtolower($sp->nama_fg), 'make') !== false){
                $brand = 'Make Over';
            } else if(strpos(strtolower($sp->nama_fg), 'emina') !== false){
                $brand = 'Emina';
            } else if(strpos(strtolower($sp->nama_fg), 'kahf') !== false){
                $brand = 'Kahf';
            }  else if(strpos(strtolower($sp->nama_fg), 'omg') !== false){
                $brand = 'OMG';
            }  else if(strpos(strtolower($sp->nama_fg), 'putri') !== false){
                $brand = 'Putri';
            }  else if(strpos(strtolower($sp->nama_fg), 'biodef') !== false){
                $brand = 'Biodef';
            } else {
                $brand = '';
            }
        }

        if (empty($existBrDetail)){
            $connection_ = Yii::$app->db;
            $command_ = $connection_->createCommand("
                INSERT INTO log_br_detail (kode_bulk,formula_reference, qty_batch, brand, sediaan,nama_bulk)
                SELECT :kode_bulk as kode_bulk, :formula_reference as formula_reference, :qty_batch as qty_batch, :brand as brand, :sediaan as sediaan, :nama_bulk as nama_bulk
                ",
                [':kode_bulk' => $kode_bulk,
                 ':formula_reference' => $list_formula[0]["reference"],
                 ':qty_batch' => $list_formula[0]["scheduled_quantity"],
                 ':brand' => $brand,
                 ':sediaan' => $sp->sediaan,
                 ':nama_bulk' => $nama_bulk,
                ]
            );
            $result_ = $command_->queryAll();
        }

        $is_split = 0;

        $siklus = 1;

        // $list_formula = '1000000.120';
        // $qty_batch = number_format((float)$list_formula, 3, '.', '');
        // $qty_batch = number_format((float)$list_formula[0]['scheduled_quantity'], 4, '.', '');
        // print_r($qty_batch);
        // exit();

        if (empty($bom)){

            $line_timbang = 'Pilih Line';
            $qty_batch = $list_formula[0]['scheduled_quantity'];

            $serial = 1;

            foreach ($list_formula as $lists) {

                if ($lists["component_code"]=="AIR-RO--L"){
                    $nama_bb = "Air R/O";
                    $kode_internal = "SLV1-AAD";
                    $operator = 11;
                    $timbangan = "RO 01";
                    $kode_olah = "-";
                } else {
                    $nama_bb = $lists["component"];
                    $kode_internal = $lists["component_internal_code"];
                    $operator = NULL;
                    $timbangan = NULL;
                    $kode_olah = NULL;
                }

                $connection2 = Yii::$app->db;
                $command2 = $connection2->createCommand("
                    INSERT INTO log_formula_breakdown (kode_bulk,nama_fg,formula_reference,lokasi,scheduled_start,week,kode_bb,qty,uom,timbangan,nama_line,nama_bb,is_split,operator,siklus,qty_batch,kode_olah,kode_internal,is_repack,urutan, serial_manual,version)
                    SELECT :kode_bulk as kode_bulk, :nama_fg as nama_fg, :formula_reference as formula_reference, :lokasi as lokasi, :scheduled_start as scheduled_start, :week as week, :kode_bb as kode_bb, :qty as qty, :uom as uom, :timbangan as timbangan, :nama_line as nama_line, :nama_bb as nama_bb, :is_split as is_split, :operator as operator, :siklus as siklus, :qty_batch as qty_batch, :kode_olah as kode_olah, :kode_internal as kode_internal, :is_repack as is_repack, :urutan as urutan, :serial_manual as serial_manual, :version as version
                    ",
                    [':kode_bulk' => $kode_bulk,
                     ':nama_fg' => $lists["fg_name"],
                     ':formula_reference' => $lists["reference"],
                     ':lokasi' => $lists["location"],
                     ':scheduled_start' => $lists["scheduled_start"],
                     ':week' => $lists["week"],
                     ':kode_bb' => $lists["component_code"],
                     ':qty' => $lists["total_component_qty"],
                     ':uom' => $lists["total_component_uom"],
                     ':timbangan' => $timbangan,
                     ':nama_line' => $line_timbang,
                     ':nama_bb' => $nama_bb,
                     ':is_split' => $is_split,
                     ':operator' => $operator,
                     ':siklus' => $siklus,
                     ':qty_batch' => number_format((float)$lists["scheduled_quantity"], 4, '.', ''),
                     ':kode_olah' => $kode_olah,
                     ':kode_internal' => $kode_internal,
                     ':is_repack' => 0,
                     ':urutan' => 1,
                     ':serial_manual' => $serial++,
                     ':version' => 'old/new',
                    ]
                );
                $result2 = $command2->queryAll();
            }

            $connection2 = Yii::$app->db;
            $command2 = $connection2->createCommand("

            SELECT *
            FROM log_formula_breakdown
            WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch AND version = 'old/new' ORDER BY uom DESC, qty ASC
            ",
            [':kode_bulk'=> $kode_bulk,
             ':reference' => $reference,
             ':qty_batch' => $qty_batch
            ]);

            $bom = $command2->queryAll();

            // $connection_2 = Yii::$app->db;
            // $command_2 = $connection_2->createCommand("
            //     INSERT INTO log_approval_br (nomo, kode_bulk, formula_reference, qty_batch, creator, status_creator, timestamp_creator, current_pic, current_status)
            //     SELECT :nomo as nomo, :kode_bulk as kode_bulk, :formula_reference as formula_reference, :qty_batch as qty_batch, :creator as creator, :status_creator as status_creator, :timestamp_creator as timestamp_creator, :current_pic as current_pic, :current_status as current_status
            //     ",
            //     [':nomo' => $nomo,
            //      ':kode_bulk' => $kode_bulk,
            //      ':formula_reference' => $list_formula[0]["reference"],
            //      ':qty_batch' => $list_formula[0]["scheduled_quantity"],
            //      ':creator' => Yii::$app->user->identity->authKey,
            //      ':status_creator' => 'DRAFT',
            //      ':timestamp_creator' => date('Y-m-d h:i A'),
            //      ':current_pic' => 'CREATOR',
            //      ':current_status' => 'DRAFT'
            //     ]
            // );
            // $result_2 = $command_2->queryAll();
            // $serial = 1;

        } else {
            $line_timbang = $bom[0]['nama_line'];
            $qty_batch = $list_formula[0]['scheduled_quantity'];

            if (empty($bom[0]['serial_manual'])){
                for ($x = 1; $x <= count($bom); $x++) {

                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("

                    UPDATE log_formula_breakdown
                    SET urutan = 1, serial_manual = :serial_manual, version = 'old/new'
                    WHERE id = :id;
                    ",
                    [':serial_manual' => $x,
                     ':id'=> $bom[$x-1]['id'],
                    ]);

                    $result = $command->queryAll();
                }

                if(strpos($bom[0]['nama_fg'], 'Wardah') !== false){
                    $brand = 'Wardah';
                } else if(strpos(strtolower($bom[0]['nama_fg']), 'make') !== false){
                    $brand = 'Make Over';
                } else if(strpos(strtolower($bom[0]['nama_fg']), 'emina') !== false){
                    $brand = 'Emina';
                } else if(strpos(strtolower($bom[0]['nama_fg']), 'kahf') !== false){
                    $brand = 'Kahf';
                }  else if(strpos(strtolower($bom[0]['nama_fg']), 'omg') !== false){
                    $brand = 'OMG';
                }  else if(strpos(strtolower($bom[0]['nama_fg']), 'putri') !== false){
                    $brand = 'Putri';
                }  else if(strpos(strtolower($bom[0]['nama_fg']), 'biodef') !== false){
                    $brand = 'Biodef';
                } else {
                    if(strpos(strtolower($sp->nama_fg), 'wardah') !== false){
                        $brand = 'Wardah';
                    } else if(strpos(strtolower($sp->nama_fg), 'make') !== false){
                        $brand = 'Make Over';
                    } else if(strpos(strtolower($sp->nama_fg), 'emina') !== false){
                        $brand = 'Emina';
                    } else if(strpos(strtolower($sp->nama_fg), 'kahf') !== false){
                        $brand = 'Kahf';
                    }  else if(strpos(strtolower($sp->nama_fg), 'omg') !== false){
                        $brand = 'OMG';
                    }  else if(strpos(strtolower($sp->nama_fg), 'putri') !== false){
                        $brand = 'Putri';
                    }  else if(strpos(strtolower($sp->nama_fg), 'biodef') !== false){
                        $brand = 'Biodef';
                    } else {
                        $brand = '';
                    }
                }

                // $connection_ = Yii::$app->db;
                // $command_ = $connection_->createCommand("
                //     INSERT INTO log_br_detail (kode_bulk,formula_reference, qty_batch, brand, sediaan, koitem_bulk, no_dokumen, no_revisi,no_smb,nama_bulk)
                //     SELECT :kode_bulk as kode_bulk, :formula_reference as formula_reference, :qty_batch as qty_batch, :brand as brand, :sediaan as sediaan, :koitem_bulk as koitem_bulk, :no_dokumen as no_dokumen, :no_revisi as no_revisi, :no_smb as no_smb, :nama_bulk as nama_bulk
                //     ",
                //     [':kode_bulk' =>$kode_bulk,
                //      ':formula_reference' => $bom[0]['formula_reference'],
                //      ':qty_batch' => $bom[0]['qty_batch'],
                //      ':brand' => $brand,
                //      ':sediaan' => $sp->sediaan,
                //      ':koitem_bulk' => $sp->koitem_bulk,
                //      ':no_dokumen' => $bom[0]['no_dokumen'],
                //      ':no_revisi' => $bom[0]['no_revisi'],
                //      ':no_smb' => $list_formula[0]["no_smb"],
                //      ':nama_bulk' => $list_formula[0]["bulk_name"],
                //     ]
                // );
                // $result_ = $command_->queryAll();
            }

            // if (($sediaan == 'P') and ($qty_batch != $bom[0]['qty_batch']))
            if ($qty_batch != $bom[0]['qty_batch'])
            {
                $connection3 = Yii::$app->db;
                $command3 = $connection3->createCommand("

                UPDATE log_formula_breakdown
                SET qty = qty * :x
                WHERE kode_bulk = :kode_bulk and formula_reference = :reference AND qty_batch = :qty_batch;
                ",
                [':x' => ($qty_batch / $bom[0]['qty_batch']),
                 ':kode_bulk' => $kode_bulk,
                 ':reference' => $reference,
                 ':qty_batch' => $qty_batch
                ]);

                $result3 = $command3->queryAll();

                $connection2 = Yii::$app->db;
                $command2 = $connection2->createCommand("

                SELECT *
                FROM log_formula_breakdown
                WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch AND version = 'old/new'  ORDER BY uom DESC, qty DESC
                ",
                [':kode_bulk'=> $kode_bulk,
                 ':reference' => $reference,
                 ':qty_batch' => $qty_batch
                ]);

                $bom = $command2->queryAll();

            }
        }

        $sql5 = "
        SELECT COUNT(*)
        FROM log_formula_breakdown
        WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = '".$qty_batch."' AND is_split <> 0
        ";

        $is_split = LogFormulaBreakdown::findBySql($sql5)->scalar();

        if ($line_timbang == "Pilih Line"){
            $connection5 = Yii::$app->db;
            $command5 = $connection5->createCommand("

            SELECT distinct on (kode_timbangan)kode_timbangan,uom
                    FROM master_data_timbangan_rm
                    ORDER BY kode_timbangan ASC
            ");

            $timbangans = $command5->queryAll();

        } else {
            $connection5 = Yii::$app->db;
            $command5 = $connection5->createCommand("

                SELECT kode_timbangan,uom
                FROM master_data_timbangan_rm
                WHERE line=:line_timbang
                ORDER BY id ASC
            ",
            [':line_timbang'=> $line_timbang
            ]);

            $timbangans = $command5->queryAll();
        }

        $connection5 = Yii::$app->db;
        $command5 = $connection5->createCommand("

            SELECT kode_timbangan,uom
            FROM master_data_timbangan_rm
            WHERE line=:line_timbang
            ORDER BY id ASC
        ",
        [':line_timbang'=> 'LWE03'
        ]);

        $timbangans_repack = $command5->queryAll();
        ////////////////////////////////////////////////////////////////////////

        $connection6 = Yii::$app->db;
        $command6 = $connection6->createCommand("

        SELECT kode
                FROM kode_action_penimbangan_rm
                ORDER BY id ASC
        ");

        $actions = $command6->queryAll();


        if (empty($bom[0]['lokasi'])){
            $lokasi = "";
        } else {
            $lokasi = $bom[0]['lokasi'];
        }

        if (empty($bom[0]['lokasi'])){
            $lokasi = "";
        } else {
            $lokasi = $bom[0]['lokasi'];
        }

        if ($is_split != 0){

            if ($qty_batch != $bom[0]['qty_batch'])
            {
                $connection4 = Yii::$app->db;
                $command4 = $connection4->createCommand("

                UPDATE log_formula_breakdown_split
                SET qty = qty * :x
                WHERE kode_bulk = :kode_bulk and formula_reference = :reference AND qty_batch = '".$qty_batch."' ;
                ",
                [':x' => ($qty_batch / $bom[0]['qty_batch']),
                 ':kode_bulk' => $kode_bulk,
                 ':reference' => $reference
                ]);

                $result4 = $command4->queryAll();

                $connection5 = Yii::$app->db;
                $command5 = $connection5->createCommand("

                UPDATE log_formula_breakdown
                SET qty_batch = :qty_batch
                WHERE kode_bulk = :kode_bulk and formula_reference = :reference AND qty_batch = '".$qty_batch."' ;
                ",
                [':qty_batch' => $qty_batch,
                 ':kode_bulk' => $kode_bulk,
                 ':reference' => $reference
                ]);

                $result5 = $command5->queryAll();
            }

            $sql5 = "
            SELECT *
            FROM log_formula_breakdown_split
            WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = '".$qty_batch."' ORDER BY id ASC
            ";

            $data_split = LogFormulaBreakdownSplit::findBySql($sql5)->all();

            $this->queryUpdSerialManual($kode_bulk, $reference, $qty_batch);
            $bom = $this->querySelOldLFB($kode_bulk,$reference,$qty_batch);

            return $this->render('input-detail-br2', [
                // 'model' => $this->findModel($id),
                'dataProvider' => $dataProvider,
                'week' => $week,
                'list_formula' => $list_formula,
                'nomo' => $nomo,
                'model' => $model,
                'bom' => $bom,
                'searchModel' => $searchModel,
                'qty_batch' => $qty_batch,
                'reference' => $reference,
                'nomo' => $nomo,
                'is_split' => $is_split,
                'data_split' => $data_split,
                'nama_line' => $line_timbang,
                // 'id_jadwal' => $log_timbang_array['id'],
                'timbangans' => $timbangans,
                'timbangans_repack' => $timbangans_repack,
                'lokasi' => $lokasi,
                'kode_bulk' => $kode_bulk,
                'actions' => $actions,
                'pic' => $pic,
                'verificationHistory' => $verificationHistory,
                'status_timbang' => $status_timbang,
                //'timbangans2' => $timbangans2
            ]);
        } else {
            if ($qty_batch != $bom[0]['qty_batch'])
            {
                $connection5 = Yii::$app->db;
                $command5 = $connection5->createCommand("

                UPDATE log_formula_breakdown
                SET qty_batch = :qty_batch
                WHERE kode_bulk = :kode_bulk and formula_reference = :reference;
                ",
                [':qty_batch' => $qty_batch,
                 ':kode_bulk' => $kode_bulk,
                 ':reference' => $reference
                ]);

                $result5 = $command5->queryAll();
            }

            return $this->render('input-detail-br2', [
                // 'model' => $this->findModel($id),
                'dataProvider' => $dataProvider,
                'week' => $week,
                'list_formula' => $list_formula,
                'nomo' => $nomo,
                'model' => $model,
                'bom' => $bom,
                'searchModel' => $searchModel,
                'qty_batch' => $qty_batch,
                'reference' => $reference,
                'nomo' => $nomo,
                'is_split' => $is_split,
                'nama_line' => $line_timbang,
                // 'id_jadwal' => $log_timbang_array['id'],
                'timbangans' => $timbangans,
                'timbangans_repack' => $timbangans_repack,
                'lokasi' => $lokasi,
                'kode_bulk' => $kode_bulk,
                'actions' => $actions,
                'pic' => $pic,
                'verificationHistory' => $verificationHistory,
                'status_timbang' => $status_timbang,
                //'timbangans2' => $timbangans2
            ]);
        }
    }

    public function actionPreviewVerifiedItems($kode_bulk,$reference,$qty_batch)
    {
        $sql = "SELECT * FROM (SELECT id,
                                kode_bb,
                                kode_internal,
                                nama_bb,
                                qty,
                                uom,
                                siklus,
                                kode_olah,
                                action,
                                keterangan,
                                operator,
                                timbangan
                        FROM log_formula_breakdown WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND is_split = 0
                        UNION
                        SELECT lfb.id,
                                lfb.kode_bb,
                                lfb.kode_internal,
                                lfb.nama_bb,
                                lfbs.qty,
                                lfbs.uom,
                                lfbs.siklus,
                                lfbs.kode_olah,
                                lfb.action,
                                lfbs.keterangan,
                                lfbs.operator,
                                lfbs.timbangan
                        FROM log_formula_breakdown_split lfbs JOIN log_formula_breakdown lfb ON lfbs.log_formula_breakdown_id= lfb.id
                        WHERE lfbs.kode_bulk = '".$kode_bulk."' AND lfbs.formula_reference = '".$reference."' AND lfbs.qty_batch = ".$qty_batch.") data
                    ORDER BY id asc
                ";
        $data = Yii::$app->db->createCommand($sql)->queryAll();

        // $sql_split = "SELECT lfb.id,
        //                 lfb.kode_bb,
        //                 lfb.kode_internal,
        //                 lfb.nama_bb,
        //                 lfbs.qty,
        //                 lfbs.uom,
        //                 lfbs.siklus,
        //                 lfbs.kode_olah,
        //                 lfb.action,
        //                 lfbs.keterangan,
        //                 lfbs.operator,
        //                 lfbs.timbangan
        //         FROM log_formula_breakdown_split lfbs JOIN log_formula_breakdown lfb ON lfbs.log_formula_breakdown_id= lfb.id
        //         WHERE lfbs.kode_bulk = '".$kode_bulk."' AND lfbs.formula_reference = '".$reference."' AND lfbs.qty_batch = ".$qty_batch."
        //         ";
        // $data_split = Yii::$app->db->createCommand($sql_split)->queryAll();

        // $allData = ArrayHelper::merge($data,$data_split);
        // $allData = asort($allData);
        // print_r('<pre>');
        // print_r($allData);
        // exit();
        $dataProvider= new ArrayDataProvider([
            'allModels'=>$data,
            // 'sort' => [
            //           'attributes' => ['id'],
            // ],
            'pagination'=>false,
        ]);
        return $this->renderAjax('preview-verified-items',[
            'dataProvider'=>$dataProvider,
        ]);
        // print_r ($data);
    }

    public function actionDeleteDetailBr($nomo,$reference,$qty_batch){
        $kode_bulk = substr(explode('/',$nomo)[0] , 3 , strlen(explode('/',$nomo)[0])-3);
        // $data = LogFormulaBreakdown::find()->where("kode_bulk = '".$kode_bulk."' and formula_reference = '".$reference."' and qty_batch = ".$qty_batch." ")->all()
        $delete = Yii::$app->db->createCommand()
                    ->delete('log_formula_breakdown',
                                "kode_bulk = '".$kode_bulk."' and formula_reference = '".$reference."' and qty_batch = ".$qty_batch." "
                            )
                    ->execute();
        $delete_split = Yii::$app->db->createCommand()
                    ->delete('log_formula_breakdown_split',
                                "kode_bulk = '".$kode_bulk."' and formula_reference = '".$reference."' and qty_batch = ".$qty_batch." "
                            )
                    ->execute();
        $delete_br_detail = Yii::$app->db->createCommand()
                    ->delete('log_br_detail',
                                "kode_bulk = '".$kode_bulk."' and formula_reference = '".$reference."' and qty_batch = ".$qty_batch." "
                            )
                    ->execute();
        $delete_approval_br = Yii::$app->db->createCommand()
                    ->delete('log_approval_br',
                                "nomo = '".$nomo."' "
                            )
                    ->execute();
        return $this->redirect(Yii::$app->request->referrer);

    }

    public function actionCheckOperatorComplete($kode_bulk,$reference,$qty_batch)
    {
        $sql = "
        SELECT operator
        FROM log_formula_breakdown
        WHERE  kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND is_split = 0 AND (operator = 0 OR operator is null) AND kode_bb <> 'AIR-RO--L'
        ";

        $row = LogFormulaBreakdown::findBySql($sql)->all();

        $sqlh = "
        SELECT *
        FROM log_formula_breakdown
        WHERE  kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND is_split <> 0
        ";

        $opr = LogFormulaBreakdown::findBySql($sqlh)->all();

        if (!empty($opr)){
            $sql2 = "
            SELECT operator
            FROM log_formula_breakdown_split
            WHERE  kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND (operator = 0 OR operator is null)
            ";

            $row2 = LogFormulaBreakdownSplit::findBySql($sql2)->all();

            if (count($row)==0 and count($row2)==0){
                echo 1;
            } else {
                echo 0;
            }
        } else {
            if (count($row)==0){
                echo 1;
            } else {
                echo 0;
            }
        }

    }

    public function actionCheckTimbanganComplete($kode_bulk,$reference,$qty_batch)
    {
        $sql = "
        SELECT timbangan
        FROM log_formula_breakdown
        WHERE  kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND is_split = 0 AND (timbangan = '-' OR timbangan is null OR timbangan='') AND kode_bb != 'AIR-RO--L'
        ";

        $row = LogFormulaBreakdown::findBySql($sql)->all();

        $sql2 = "
        SELECT timbangan
        FROM log_formula_breakdown_split
        WHERE  kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND (timbangan = '-' OR timbangan is null OR timbangan='')
        ";

        $row2 = LogFormulaBreakdownSplit::findBySql($sql2)->all();

        if (count($row)==0 and count($row2)==0){
            echo 1;
        } else {
            echo 0;
        }

    }

    public function actionCheckKodeOlahComplete($kode_bulk,$reference,$qty_batch)
    {
        $sql = "
        SELECT kode_olah
        FROM log_formula_breakdown
        WHERE  kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND is_split = 0 AND (kode_olah = '' OR kode_olah is null OR kode_olah = '-') AND kode_bb <> 'AIR-RO--L'
        ";

        $row = LogFormulaBreakdown::findBySql($sql)->all();

        $sql2 = "
        SELECT kode_olah
        FROM log_formula_breakdown_split
        WHERE  kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND (kode_olah = '' OR kode_olah is null OR kode_olah = '-')
        ";

        $row2 = LogFormulaBreakdownSplit::findBySql($sql2)->all();

        if (count($row)==0 and count($row2)==0){
            echo 1;
        } else {
            echo 0;
        }

    }

    public function actionCheckKodeInternalComplete($kode_bulk,$reference,$qty_batch)
    {
        $sql = "
        SELECT kode_internal
        FROM log_formula_breakdown
        WHERE  kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND is_split = 0 AND kode_bb != 'AIR-RO--L' AND (kode_internal = '' OR kode_internal is null OR kode_internal = '-') AND kode_bb <> 'AIR-RO--L'
        ";

        $row = LogFormulaBreakdown::findBySql($sql)->all();

        // $sql2 = "
        // SELECT kode_internal
        // FROM log_formula_breakdown_split
        // WHERE  kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND (kode_internal = '' OR kode_internal is null OR kode_internal = '-')
        // ";

        // $row2 = LogFormulaBreakdownSplit::findBySql($sql2)->all();

        if (count($row)==0){
            echo 1;
        } else {
            echo 0;
        }

    }

    /**
     * Displays a single LogFormulaBreakdown model.
     * @param integer $id
     * @return mixed
     */
    public function actionEditMasterBr($kode_bulk,$reference)
    {
        //$searchModel = new LogFormulaBreakdownSearch();
        //$kode_bulk = strtok($nomo, '/');
        //$query->andWhere(['nomo' => $nomo]);
        //$searchModel->kode_bulk = $nomo;
        //$dataProvider = $searchModel->search($query);
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        //$model = new FlowInputMo();

        $this->layout = '//main-sidebar-collapse';

        date_default_timezone_set('Asia/Jakarta');
        $ddate = date("Y-m-d");
        $duedt = explode("-", $ddate);
        $date  = mktime(0, 0, 0, $duedt[1], $duedt[2], $duedt[0]);
        $week  = (int)date('W', $date);

        $connection2 = Yii::$app->db;
        $command2 = $connection2->createCommand("

        SELECT *
        FROM log_formula_breakdown
        WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference ORDER BY id ASC
        ",
        [':kode_bulk'=> $kode_bulk,
         ':reference'=> $reference,
        ]);

        $bom = $command2->queryAll();

        $sql2 = "
        SELECT COUNT(*)
        FROM log_formula_breakdown
        WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = ".$reference." AND is_split <> 0
        ";

        $is_split = LogFormulaBreakdown::findBySql($sql2)->scalar();

        // $sql3="
        //     SELECT
        //         qty_batch,id
        //     FROM log_jadwal_timbang_rm
        //     WHERE nomo = '".$nomo."'";

        // $log_timbang_array = LogJadwalTimbangRm::findBySql($sql3)->one();

        ////////////////////////////////////////////////////////////////////////
        // $sql5 = "
        //         SELECT nama_line
        //         FROM log_jadwal_timbang_rm
        //         WHERE nomo = '".$nomo."'
        //         ORDER BY id desc
        //         ";

        // $line_timbang_array = LogJadwalTimbangRm::findBySql($sql5)->one();

        // if ($line_timbang_array->nama_line == NULL)
        // {
        //     $line_timbang = "Pilih Line";
        // } else if (strpos($line_timbang_array->nama_line, '/') !== false){
        //     $line_timbang = "Pilih Line";
        // } else {
        //     $line_timbang = $line_timbang_array->nama_line;
        // }

        if ($bom[0]['nama_line'] == "Pilih Line"){
            $connection5 = Yii::$app->db;
            $command5 = $connection5->createCommand("

            SELECT kode_timbangan
                    FROM master_data_timbangan_rm
                    ORDER BY kode_timbangan ASC
            ");

            $timbangans = $command5->queryAll();

            // $sql = "
            //         SELECT kode_timbangan
            //         FROM master_data_timbangan_rm
            //         ORDER BY kode_timbangan ASC
            //         ";

            // $timbangans = MasterDataTimbanganRm::findBySql($sql)->all();
            // $timbangans2 = ArrayHelper::map(MasterDataTimbanganRm::findBySql($sql)->all(), 'kode_timbangan','kode_timbangan');

        } else {
            $connection5 = Yii::$app->db;
            $command5 = $connection5->createCommand("

                SELECT kode_timbangan
                FROM master_data_timbangan_rm
                WHERE line=:line_timbang
                ORDER BY id ASC
            ",
            [':line_timbang'=> $bom[0]['nama_line']
            ]);

            $timbangans = $command5->queryAll();

            // $sql = "
            //         SELECT kode_timbangan
            //         FROM master_data_timbangan_rm
            //         WHERE line='".$line_timbang."'
            //         ORDER BY id ASC
            //         ";

            // $timbangans = MasterDataTimbanganRm::findBySql($sql)->all();
            // $timbangans2 = ArrayHelper::map(MasterDataTimbanganRm::findBySql($sql)->all(), 'kode_timbangan','kode_timbangan');
        }

        // print("<pre>");
        // print_r(Json::encode($timbangans));
        // print("</pre>");

        // echo "<select class='form-control form-control-sm text-center' style='height:2em; text-align:center; float:left;'><option value='Pilih Timbangan'>Pilih Timbangan</options>";
        // foreach($timbangans as $timbangan){
        //     echo "<option value='".$timbangan->kode_timbangan."'>".$timbangan->kode_timbangan."</options>";
        // }
        // echo "</select>";
        ////////////////////////////////////////////////////////////////////////

        $connection6 = Yii::$app->db;
        $command6 = $connection6->createCommand("

        SELECT kode
                FROM kode_action_penimbangan_rm
                ORDER BY id ASC
        ");

        $actions = $command6->queryAll();

        $lokasi = $bom[0]['lokasi'];

        if ($is_split != 0){
            $sql5 = "
            SELECT *
            FROM log_formula_breakdown_split
            WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = ".$reference." ORDER BY id ASC
            ";

            $data_split = LogFormulaBreakdownSplit::findBySql($sql5)->all();

            return $this->render('edit-master-br', [
                // 'model' => $this->findModel($id),
                //'dataProvider' => $dataProvider,
                //'week' => $week,
                //'list_formula' => $list_formula,
                //'nomo' => $nomo,
                //'model' => $model,
                'bom' => $bom,
                //'searchModel' => $searchModel,
                'qty_batch' => $bom[0]['qty_batch'],
                'reference' => $reference,
                //'nomo' => $nomo,
                'is_split' => $is_split,
                'data_split' => $data_split,
                'nama_line' => $bom[0]["nama_line"],
                //'id_jadwal' => $log_timbang_array['id'],
                'timbangans' => $timbangans,
                'lokasi' => $lokasi,
                'kode_bulk' => $kode_bulk,
                'actions' => $actions,
                //'timbangans2' => $timbangans2
            ]);
        } else {
            return $this->render('edit-master-br', [
                // 'model' => $this->findModel($id),
                //'dataProvider' => $dataProvider,
                //'week' => $week,
                //'list_formula' => $list_formula,
                //'nomo' => $nomo,
                //'model' => $model,
                'bom' => $bom,
                //'searchModel' => $searchModel,
                'qty_batch' => $bom[0]['qty_batch'],
                'reference' => $reference,
                //'nomo' => $nomo,
                'is_split' => $is_split,
                'nama_line' => $bom[0]["nama_line"],
                //'id_jadwal' => $log_timbang_array['id'],
                'timbangans' => $timbangans,
                'lokasi' => $lokasi,
                'kode_bulk' => $kode_bulk,
                'actions' => $actions,
                //'timbangans2' => $timbangans2
            ]);
        }
    }

    public function actionAddSplitWeighing($id){

        // GET LIST TASK
        $sql = "
        SELECT is_split
        FROM log_formula_breakdown
        WHERE id = ".$id."
        ";

        $qty_split = LogFormulaBreakdown::findBySql($sql)->all();
        $split = $qty_split[0]['is_split'];
        $split = $split+1;
        //var_dump();

        //echo "<script>console.log("; echo $qty_split[0]['is_split']; echo ");</script>";

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

        UPDATE log_formula_breakdown
        SET is_split = :is_split
        WHERE id = :id;
        ",
        [':id'=> $id,
         ':is_split'=>$split
        ]);

        $result = $command->queryAll();

        if ($result){
        // Redirect to Downtime
            $value = array(
            "status"=>"success");
        } else {
            $value = array(
            "status"=>"not-success");
        }
        // $value = array(
        //     "status"=>$qty_split[0]['is_split']);

        echo Json::encode($value);
    }

    public function actionApproveReviewer($nomo)
    {
        date_default_timezone_set('Asia/Jakarta');

        // GET LIST TASK
        $sql = "
        SELECT *
        FROM log_approval_br
        WHERE nomo = '".$nomo."'
        ";

        $row = LogApprovalBr::findBySql($sql)->one();

        if (!empty($row->reject_note_reviewer)){

            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE log_approval_br
            SET status_reviewer = 'APPROVED', status_approver = 'QUEUE', timestamp_reviewer = :timestamp_reviewer, timestamp_approver = :timestamp_approver, current_pic = 'APPROVER', current_status = 'QUEUE'
            WHERE nomo = :nomo;
            ",
            [':nomo'=> $nomo,
             ':timestamp_reviewer'=> date('Y-m-d H:i:s'),
             ':timestamp_approver'=> date('Y-m-d H:i:s')
            ]);

            $result = $command->queryAll();

            if ($result){
            // Redirect to Downtime
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 2;
        }
    }

    public function actionRejectReviewer($nomo,$kode_bulk,$reference,$qty_batch)
    {
        date_default_timezone_set('Asia/Jakarta');

        // GET LIST TASK
        $sql = "
        SELECT *
        FROM log_approval_br
        WHERE nomo = '".$nomo."'
        ";

        $row = LogApprovalBr::findBySql($sql)->one();

        if (!empty($row->reject_note_reviewer)){

            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE log_approval_br
            SET status_reviewer = 'REJECT', timestamp_reviewer = :timestamp_reviewer, status_creator = :status_creator, timestamp_creator = :timestamp_creator, current_pic = 'CREATOR', current_status = 'REJECT'
            WHERE nomo = :nomo;
            ",
            [':nomo'=> $nomo,
             ':status_creator'=> 'DRAFT',
             ':timestamp_creator'=> date('Y-m-d H:i:s'),
             ':timestamp_reviewer'=> date('Y-m-d H:i:s')
            ]);

            $result = $command->queryAll();

            $connection2 = Yii::$app->getDb();
            $command2 = $connection2->createCommand("

            UPDATE log_formula_breakdown
            SET is_lock = 0
            WHERE kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch;
            ",
            [':kode_bulk'=> $kode_bulk,
             ':reference'=>$reference,
             ':qty_batch'=>$qty_batch
            ]);

            $result2 = $command2->queryAll();

            $connection3 = Yii::$app->getDb();
            $command3 = $connection3->createCommand("

            UPDATE log_br_detail
            SET is_lock = 0
            WHERE kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch;
            ",
            [':kode_bulk'=> $kode_bulk,
             ':reference'=>$reference,
             ':qty_batch'=>$qty_batch
            ]);

            $result3 = $command3->queryAll();

            if ($result and $result2 and $result3){
            // Redirect to Downtime
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 2;
        }
    }

    public function actionApproveApprover($nomo,$kode_bulk,$reference,$qty_batch)
    {
        date_default_timezone_set('Asia/Jakarta');

        // GET LIST TASK
        $sql = "
        SELECT *
        FROM log_approval_br
        WHERE nomo = '".$nomo."'
        ";

        $row = LogApprovalBr::findBySql($sql)->one();

        if (!empty($row->reject_note_approver)){

            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE log_approval_br
            SET status_approver = 'APPROVED', timestamp_approver = :timestamp_approver, current_pic = 'APPROVER', current_status = 'APPROVED', log = 'approval'
            WHERE nomo = :nomo;
            ",
            [':nomo'=> $nomo,
             ':timestamp_approver'=> date('Y-m-d H:i:s')
            ]);

            $result = $command->queryAll();

            $connection2 = Yii::$app->getDb();
            $command2 = $connection2->createCommand("

            UPDATE log_formula_breakdown
            SET is_lock = 1
            WHERE kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch;
            ",
            [':kode_bulk'=> $kode_bulk,
             ':reference'=>$reference,
             ':qty_batch'=>$qty_batch
            ]);

            $result2 = $command2->queryAll();

            $connection3 = Yii::$app->getDb();
            $command3 = $connection3->createCommand("

            UPDATE log_br_detail
            SET is_lock = 1, is_can_send = 1
            WHERE kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch;
            ",
            [':kode_bulk'=> $kode_bulk,
             ':reference'=>$reference,
             ':qty_batch'=>$qty_batch
            ]);

            $result3 = $command3->queryAll();

            if ($result){
            // Redirect to Downtime
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 2;
        }
    }

    public function actionRejectApprover($nomo,$kode_bulk,$reference,$qty_batch)
    {
        date_default_timezone_set('Asia/Jakarta');

        // GET LIST TASK
        $sql = "
        SELECT *
        FROM log_approval_br
        WHERE nomo = '".$nomo."'
        ";

        $row = LogApprovalBr::findBySql($sql)->one();

        if (!empty($row->reject_note_approver)){

            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE log_approval_br
            SET status_approver = 'REJECT', timestamp_approver = :timestamp_approver, status_creator = :status_creator, timestamp_creator = :timestamp_creator, current_pic = 'CREATOR', current_status = 'REJECT'
            WHERE nomo = :nomo;
            ",
            [':nomo'=> $nomo,
             ':status_creator'=> 'DRAFT',
             ':timestamp_creator'=> date('Y-m-d H:i:s'),
             ':timestamp_approver'=> date('Y-m-d H:i:s')
            ]);

            $result = $command->queryAll();

            $connection2 = Yii::$app->getDb();
            $command2 = $connection2->createCommand("

            UPDATE log_formula_breakdown
            SET is_lock = 0
            WHERE kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch;
            ",
            [':kode_bulk'=> $kode_bulk,
             ':reference'=>$reference,
             ':qty_batch'=>$qty_batch
            ]);

            $result2 = $command2->queryAll();

            $connection3 = Yii::$app->getDb();
            $command3 = $connection3->createCommand("

            UPDATE log_br_detail
            SET is_lock = 0
            WHERE kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch;
            ",
            [':kode_bulk'=> $kode_bulk,
             ':reference'=>$reference,
             ':qty_batch'=>$qty_batch
            ]);

            $result3 = $command3->queryAll();

            if ($result and $result2 and $result3){
            // Redirect to Downtime
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 2;
        }
    }

    public function actionMinusSplitWeighing($id){

        // GET LIST TASK
        $sql = "
        SELECT is_split
        FROM log_formula_breakdown
        WHERE id = ".$id."
        ";

        $qty_split = LogFormulaBreakdown::findBySql($sql)->one();
        $split = $qty_split['is_split'];
        $split = $split-1;

        if ($split<0){
            $split = 0;
        }

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

        UPDATE log_formula_breakdown
        SET is_split = :is_split
        WHERE id = :id;
        ",
        [':id'=> $id,
         ':is_split'=>$split
        ]);

        $result = $command->queryAll();

        $value = array(
            "status"=>$qty_split['is_split']);

        echo Json::encode($value);
    }

    public function actionGetSplit($id){

        // GET LIST TASK
        $sql = "
        SELECT is_split
        FROM log_formula_breakdown
        WHERE id = ".$id."
        ";

        $qty_split = LogFormulaBreakdown::findBySql($sql)->all();
        $split = $qty_split[0]['is_split'];

        echo $split;
    }

    public function actionGetDetail($id){

        // GET LIST TASK
        $sql = "
        SELECT *
        FROM log_formula_breakdown
        WHERE id = ".$id."
        ";

        $row = LogFormulaBreakdown::findBySql($sql)->one();
        if (empty($row)){
            echo "";
        } else {
            echo Json::encode($row);
        }
    }

    public function actionInsertSplitQty($id,$qty){

        // GET LIST TASK
        $sql = "
        SELECT *
        FROM log_formula_breakdown
        WHERE id = ".$id."
        ";

        $data = LogFormulaBreakdown::findBySql($sql)->all();
        // print_r('<pre>');
        // print_r($data);
        // exit();
        $operator = $data[0]["operator"];

        $connection2 = Yii::$app->db;
        $command2 = $connection2->createCommand("
            INSERT INTO log_formula_breakdown_split (log_formula_breakdown_id,kode_bulk,qty,uom,formula_reference,siklus,operator,qty_batch)
            SELECT :log_formula_breakdown_id as log_formula_breakdown_id, :kode_bulk as kode_bulk, :qty as qty, :uom as uom, :reference as formula_reference, :siklus as siklus, :operator as operator, :qty_batch as qty_batch
            ",
            [':log_formula_breakdown_id' => $id,
             ':kode_bulk' => $data[0]["kode_bulk"],
             ':qty' => $qty,
             ':uom' => $data[0]["uom"],
             ':reference' => $data[0]["formula_reference"],
             ':siklus' => $data[0]["siklus"],
             ':operator' => "0",
             ':qty_batch' => $data[0]["qty_batch"]
            ]
        );
        $result2 = $command2->queryAll();

        $insert_id = Yii::$app->db->getLastInsertID();

        $connection3 = Yii::$app->db;
        $command3 = $connection3->createCommand("
            INSERT INTO log_formula_breakdown (kode_bulk,nama_fg,formula_reference,lokasi,scheduled_start,week,kode_bb,qty,uom,timbangan,nama_line,nama_bb,is_split,operator,siklus,qty_batch,kode_olah,kode_internal,is_repack,urutan,serial_manual,action,keterangan,version)
            SELECT :kode_bulk as kode_bulk, :nama_fg as nama_fg, :formula_reference as formula_reference, :lokasi as lokasi, :scheduled_start as scheduled_start, :week as week, :kode_bb as kode_bb, :qty as qty, :uom as uom, :timbangan as timbangan, :nama_line as nama_line, :nama_bb as nama_bb, :is_split as is_split, :operator as operator, :siklus as siklus, :qty_batch as qty_batch, :kode_olah as kode_olah, :kode_internal as kode_internal, :is_repack as is_repack, :urutan as urutan, :serial_manual as serial_manual, :action as action, :keterangan as keterangan, :version as version
            ",
            [':kode_bulk' => $data[0]["kode_bulk"],
             ':nama_fg' => $data[0]["nama_fg"],
             ':formula_reference' => $data[0]["formula_reference"],
             ':lokasi' => $data[0]["lokasi"],
             ':scheduled_start' => $data[0]["scheduled_start"],
             ':week' => $data[0]["week"],
             ':kode_bb' => $data[0]["kode_bb"],
             ':qty' => number_format((float)"0", 5, '.', ''),
             ':uom' => $data[0]["uom"],
             ':timbangan' => $data[0]["timbangan"],
             ':nama_line' => $data[0]["nama_line"],
             ':nama_bb' => $data[0]["nama_bb"]." [".$data[0]["is_split"]."/".$data[0]["is_split"]."]",
             ':is_split' => 99,
             ':operator' => $data[0]["operator"],
             ':siklus' => 1,
             ':qty_batch' => $data[0]["qty_batch"],
             ':kode_olah' => NULL,
             ':kode_internal' => $data[0]["kode_internal"],
             ':is_repack' => $data[0]["is_repack"],
             ':urutan' => $data[0]["is_split"],
             ':serial_manual' => $data[0]["serial_manual"].".".$data[0]["is_split"],
             ':action' => $data[0]["action"],
             ':keterangan' => $data[0]["keterangan"],
             ':version' =>  'new',
            ]
        );
        $result3 = $command3->queryAll();

        $flagUpdate = 0;

        if($data[0]["is_split"]>1){
            $nama_bb = strtok($data[0]["nama_bb"], '[');
            $flagUpdate = 1;
        } else{
            $nama_bb = $data[0]["nama_bb"];
            $flagUpdate = 0;
        }
        // Program update postfix urutan split di nama fg
        if ($flagUpdate){
            for ($x = 1; $x < $data[0]["is_split"]; $x++) {

                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("

                UPDATE log_formula_breakdown
                SET nama_bb = :nama_bb
                WHERE kode_bulk = :kode_bulk and formula_reference = :formula_reference and qty_batch = :qty_batch and kode_bb = :kode_bb and is_split = 99 and urutan = :urutan;
                ",
                [':nama_bb' => $nama_bb." [".$x."/".$data[0]["is_split"]."]",
                 ':kode_bulk'=> $data[0]["kode_bulk"],
                 ':formula_reference'=> $data[0]["formula_reference"],
                 ':qty_batch'=> $data[0]["qty_batch"],
                 ':kode_bb'=> $data[0]["kode_bb"],
                 ':urutan'=> $x,
                ]);

                $result = $command->queryAll();
            }
            $flagUpdate = 0;
        }

        if ($result2){
            $sql = "
            SELECT id
            FROM log_formula_breakdown_split
            ORDER BY id DESC
            ";

            $id = LogFormulaBreakdown::findBySql($sql)->scalar();
        // Redirect to Downtime
            $value = array(
            "status"=>$id);
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    public function actionGetReviewer($nomo)
    {
        $sql = "
                SELECT sediaan
                FROM scm_planner
                WHERE nomo='".$nomo."'
                ORDER BY id desc
                ";

        $sediaan_array = ScmPlanner::findBySql($sql)->one();
        $sediaan = $sediaan_array->sediaan;

        $sql2 = "
                SELECT reviewer
                FROM list_reviewer
                WHERE sediaan='".$sediaan."'
                ORDER BY reviewer ASC
                ";

        $reviewer = ListReviewer::findBySql($sql2)->all();

        echo "<select id='reviewer' class='form-control form-control-sm text-center' style='height:2em; text-align:center; float:left;'><option value='Pilih Reviewer'>Pilih Reviewer</options>";
        foreach($reviewer as $r){
            echo "<option value='".$r->username."'>".$r->reviewer."</options>";
        }
        echo "</select>";
    }

    public function actionUpdateSplitQty($id_split,$qty)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

            UPDATE log_formula_breakdown_split
            SET qty = :qty
            WHERE id = :id;
            ",
            [':id'=> $id_split,
             ':qty'=>$qty
            ]);

        $result = $command->queryAll();

        if ($result){
            $value = array(
            "status"=>"success");
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    public function actionUpdateQty($id,$qty)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

            UPDATE log_formula_breakdown
            SET qty = :qty
            WHERE id = :id;
            ",
            [':id'=> $id,
             ':qty'=>$qty
            ]);

        $result = $command->queryAll();

        if ($result){
            $value = array(
            "status"=>"success");
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    public function actionDeleteSplitQty($id_split)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

            DELETE FROM log_formula_breakdown_split
            WHERE id = :id;
            ",
            [':id'=> $id_split,
            ]);

        $result = $command->queryAll();

        if ($result){
            $value = array(
            "status"=>"success");
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    /*// public function actionGetQtyBatch($kode_bulk,$reference)
    // {

    //     $sql = "
    //     SELECT qty_batch
    //     FROM log_formula_breakdown
    //     WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = ".$reference."
    //     ";

    //     $sum = LogFormulaBreakdown::findBySql($sql)->one();

    //     echo $sum->qty_batch;
    // }*/

    public function actionGetSum($kode_bulk,$reference,$qty_batch)
    {
        $sql = "
        SELECT COUNT(*)
        FROM log_formula_breakdown
        WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND is_split <> 0
        ";

        $is_split = LogFormulaBreakdown::findBySql($sql)->scalar();

        if ($is_split == 0){
            $sql2 = "
                SELECT sum(a.qty_total)
                FROM (SELECT
                        CASE WHEN uom = 'kg' then qty
                             ELSE qty/1000
                        END AS qty_total
                      FROM log_formula_breakdown WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND is_split = 0) a
            ";

            $sum = LogFormulaBreakdown::findBySql($sql2)->scalar();

        } else {
            $sql2 = "
                SELECT sum(a.qty_total)
                FROM (SELECT
                        CASE WHEN uom = 'kg' then qty
                             ELSE qty/1000
                        END AS qty_total
                      FROM log_formula_breakdown WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND is_split = 0) a
            ";

            $sum_non_split = LogFormulaBreakdown::findBySql($sql2)->scalar();

            $sql3 = "
                SELECT sum(a.qty_total)
                FROM (SELECT
                        CASE WHEN uom = 'kg' then qty
                             ELSE qty/1000
                        END AS qty_total
                      FROM log_formula_breakdown_split
                      WHERE log_formula_breakdown_id in (
                          SELECT id FROM log_formula_breakdown WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND is_split <> 0)) a

            ";

            $sum_split = LogFormulaBreakdownSplit::findBySql($sql3)->scalar();

            $sum = $sum_non_split + $sum_split;
        }

        echo $sum;
    }

    public function actionUpdateSiklusSplit($id_split,$siklus)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

            UPDATE log_formula_breakdown_split
            SET siklus = :siklus
            WHERE id = :id;
            ",
            [':id'=> $id_split,
             ':siklus'=>$siklus
            ]);

        $result = $command->queryAll();

        if ($result){
            $value = array(
            "status"=>"success");
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    public function actionUpdateSiklus($id,$siklus)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

            UPDATE log_formula_breakdown
            SET siklus = :siklus
            WHERE id = :id;
            ",
            [':id'=> $id,
             ':siklus'=>$siklus
            ]);

        $result = $command->queryAll();

        if ($result){
            $value = array(
            "status"=>"success");
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    public function actionGetSumPerItem($id)
    {
        $sql = "
        SELECT is_split
        FROM log_formula_breakdown
        WHERE id = '".$id."'
        ";

        $is_split = LogFormulaBreakdown::findBySql($sql)->one();

        if ($is_split->is_split == 0){
            $sql2 = "
                SELECT qty
                FROM log_formula_breakdown
                WHERE id = '".$id."'
            ";

            $sum_array = LogFormulaBreakdown::findBySql($sql2)->one();
            $sum = $sum_array->qty;

        } else {
            $sql = "
                SELECT sum(qty)
                FROM log_formula_breakdown_split
                WHERE log_formula_breakdown_id = '".$id."'
            ";

            $sum = LogFormulaBreakdownSplit::findBySql($sql)->scalar();
        }

        echo $sum;
    }

    public function actionGetStandardQtyPerItem($id)
    {

        $sql = "
            SELECT qty
            FROM log_formula_breakdown
            WHERE id = '".$id."'
        ";

        $sum_split = LogFormulaBreakdown::findBySql($sql)->one();

        echo $sum_split->qty;
    }

    public function actionGetStandardQty($kode_bulk,$reference,$qty_batch)
    {

        $sql = "
        SELECT qty_batch
        FROM log_formula_breakdown
        WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch."
        ";

        $sum = LogFormulaBreakdown::findBySql($sql)->one();

        echo $sum->qty_batch;
    }

    public function actionUpdateLine($kode_bulk,$reference,$nama_line,$nomo)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

            UPDATE log_formula_breakdown
            SET nama_line = :nama_line,
                timbangan = :timbangan
            WHERE kode_bulk = :kode_bulk and formula_reference = :reference;
            ",
            [':kode_bulk'=> $kode_bulk,
             ':reference'=>$reference,
             ':nama_line'=> $nama_line,
             ':timbangan'=> ""
            ]);

        $result = $command->queryAll();

        // $connection2 = Yii::$app->db;
        // $command2 = $connection2->createCommand("

        //     UPDATE log_jadwal_timbang_rm_new
        //     SET nama_line = :nama_line
        //     WHERE nomo = :nomo;
        //     ",
        //     [':nomo'=> $nomo,
        //      ':nama_line'=> $nama_line
        //     ]);

        // $result2 = $command2->queryAll();

        $sqlh = "
        SELECT *
        FROM log_formula_breakdown
        WHERE  kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND is_split <> 0
        ";

        $line = LogFormulaBreakdown::findBySql($sqlh)->all();

        if (!empty($opr)){
            $connection3 = Yii::$app->db;
            $command3 = $connection3->createCommand("

                UPDATE log_formula_breakdown_split
                SET timbangan = :timbangan
                WHERE kode_bulk = :kode_bulk and formula_reference = :reference;
                ",
                [':kode_bulk'=> $kode_bulk,
                 ':reference'=>$reference,
                 ':timbangan'=> ""
                ]);

            $result3 = $command3->queryAll();

            if ($result and $result3){
                echo 1;
            } else {
                echo 0;
            }
        } else {
            if ($result){
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    public function actionLockJurnal($kode_bulk,$reference,$qty_batch)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

            UPDATE log_br_detail
            SET is_lock = 1
            WHERE kode_bulk = :kode_bulk and formula_reference = :reference and qty_batch = :qty_batch;
            ",
            [':kode_bulk'=> $kode_bulk,
             ':reference'=>$reference,
             ':qty_batch'=> $qty_batch,
            ]);

        $result = $command->queryAll();

        $connection2 = Yii::$app->db;
        $command2 = $connection2->createCommand("

            UPDATE log_formula_breakdown
            SET is_lock = 1
            WHERE kode_bulk = :kode_bulk and formula_reference = :reference and qty_batch = :qty_batch;
            ",
            [':kode_bulk'=> $kode_bulk,
             ':reference'=>$reference,
             ':qty_batch'=> $qty_batch,
            ]);

        $result2 = $command2->queryAll();

        if ($result and $result2){
            echo 1;
        } else {
            echo 0;
        }


    }

    public function actionSendDataToGbb($nomo)
    {
        date_default_timezone_set('Asia/Jakarta');

        $sql2 = "
        SELECT
            nomo,nama_fg,snfg,koitem_fg
        FROM scm_planner
        WHERE nomo = :nomo";

        $scm_planner = ScmPlanner::findBySql($sql2,[':nomo'=>$nomo])->one();

        $sql3 = "SELECT * FROM flow_input_gbb WHERE nomo = :nomo";
        $alreadyStart = FlowInputGbb::findBySql($sql3,[':nomo'=>$nomo])->one();

        $sql4 = "SELECT * FROM log_picking_gbb WHERE nomo = :nomo";
        $isExist = LogPickingGbb::findBySql($sql4,[':nomo'=>$nomo])->one();

        if (empty($alreadyStart)){

            if (!empty($isExist)){
                $connection5 = Yii::$app->db;
                $command5 = $connection5->createCommand("
                DELETE
                FROM log_picking_gbb
                WHERE nomo = '".$nomo."'");

                $delete = $command5->queryAll();
            }

            $connection3 = Yii::$app->db;
            $command3 = $connection3->createCommand("

            SELECT DISTINCT ON (kode_bb, operator) lpr.nomo,lpr.no_formula,lpr.nama_line,lpr.mo_odoo,lpr.kode_bb,lpr.kode_internal,lpr.nama_bb, kode_olah,siklus,lpr.weight,lpr.satuan,lpr.operator,koitem_fg,no_smb
            FROM log_penimbangan_rm lpr
            WHERE lpr.nomo = :nomo

            ",
            [':nomo'=> $nomo,
            ]);
            $list_bb = $command3->queryAll();

            foreach ($list_bb as $list){
                if (strpos($list['kode_bb'],'BULK')!==false){
                    $jenis_bb = 'antara';
                }else{
                    $jenis_bb = 'rm';
                }
                $connection2 = Yii::$app->getDb();
                $command2 = $connection2->createCommand("

                INSERT INTO log_picking_gbb (nomo, nama_line,mo_odoo, kode_bb,kode_internal,nama_bb,kode_olah,siklus,jenis_bb,weight,satuan,status,koitem_fg)
                SELECT :nomo as nomo,
                        :nama_line as nama_line,
                        :mo_odoo as mo_odoo,
                        :kode_bb as kode_bb,
                        :kode_internal as kode_internal,
                        :nama_bb as nama_bb,
                        :kode_olah as kode_olah,
                        :siklus as siklus,
                        :jenis_bb as jenis_bb,
                        :weight as weight,
                        :satuan as satuan,
                        :status as status,
                        :koitem_fg as koitem_fg",

                [':nomo' => $nomo,
                 ':nama_line' => $list['nama_line'],
                 ':mo_odoo' => $list['mo_odoo'],
                 ':kode_bb' => $list['kode_bb'],
                 ':kode_internal' => $list['kode_internal'],
                 ':nama_bb' => $list['nama_bb'],
                 ':kode_olah' => $list['kode_olah'],
                 ':siklus' => $list['siklus'],
                 ':jenis_bb' => $jenis_bb,
                 ':weight' => $list['weight'],
                 ':satuan' => $list['satuan'],
                 ':status' => 'not-done',
                 ':koitem_fg' => $list['koitem_fg']
                ]);

                $result2 = $command2->queryAll();
                $save = false;

            }

            if ($result2){
                echo 1;
            } else {
                echo 0;
            }
        } else {

            echo 2;
        }

    }

    public function actionSendDataToWeighing($nomo,$kode_bulk,$reference,$qty_batch)
    {
        date_default_timezone_set('Asia/Jakarta');


        $result2 = 0;
        $result4 = 0;

        $connection3 = Yii::$app->db;
        $command3 = $connection3->createCommand("

        SELECT nomo as mo_odoo, no_smb
        FROM daily_consumption_default
        WHERE  name = :nomo
        ",
        [':nomo'=> $nomo,
        ]);
        $dcd = $command3->queryOne();


        // print_r('<pre>');
        // print_r($dcd['no_smb']);
        // exit();

        // $connection3 = Yii::$app->db;
        // $command3 = $connection3->createCommand("

        // SELECT *
        // FROM log_br_detail
        // WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch
        // ",
        // [':kode_bulk'=> $kode_bulk,
        //  ':reference'=> $reference,
        //  ':qty_batch' => $qty_batch
        // ]);
        // $br_detail = $command3->queryAll();

        $sql = "
        SELECT
            *
        FROM log_penimbangan_rm
        WHERE nomo = '".$nomo."'";

        $nomoExist = LogPenimbanganRm::findBySql($sql)->all();

        $sql6 = "
        SELECT
            nomo
        FROM flow_input_mo
        WHERE nomo = '".$nomo."' AND posisi = 'PENIMBANGAN'";

        $alreadyStart = FlowInputMo::findBySql($sql6)->all();

        $sql2 = "
        SELECT
            nomo,nama_fg,snfg,koitem_fg
        FROM scm_planner
        WHERE nomo = '".$nomo."'";

        $scm_planner = ScmPlanner::findBySql($sql2)->one();

        // print_r('<pre>');
        // print_r($nomoExist);
        // print_r('<pre>');
        // print_r($alreadyStart);
        // print_r('<pre>');
        // print_r($scm_planner);
        // exit();

        if (empty($alreadyStart)){

            if (!empty($nomoExist)){
                $connection5 = Yii::$app->db;
                $command5 = $connection5->createCommand("

                DELETE
                FROM log_penimbangan_rm
                WHERE nomo = '".$nomo."'");

                $delete = $command5->queryAll();
            }

            $connection = Yii::$app->db;
            $command = $connection->createCommand("

            SELECT
                *
            FROM log_formula_breakdown
            WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." ORDER BY id ASC");

            $list_formula = $command->queryAll();

            for ($x=0; $x < count($list_formula); $x++)
            {
                if ($list_formula[$x]['is_split']==0){

                    if (strpos(strtolower($list_formula[$x]['keterangan']), 'repack') !== false) {
                        $status = "Belum Ditimbang";
                        $nama_operator = "";
                        $realisasi = null;
                        $is_repack = 1;
                        if (strpos(str_replace(' ', '', $list_formula[$x]['nama_line']),'LW')!==false){
                            $nama_line = 'LWE03';
                        }else{
                            $nama_line = str_replace(' ', '', $list_formula[$x]['nama_line']);
                        }
                        // $realisasi = $list_formula[$x]['qty'];
                    } else if (strpos(strtolower($list_formula[$x]['keterangan']), 'base') !== false ){
                        $status = "Selesai Ditimbang";
                        $nama_operator = "BASE";
                        $realisasi = $list_formula[$x]['qty'];
                        $is_repack = null;
                        $nama_line = str_replace(' ', '', $list_formula[$x]['nama_line']);

                    } else {
                        $status = "Belum Ditimbang";
                        $nama_operator = "";
                        $realisasi = NULL;
                        $is_repack = NULL;
                        $nama_line = str_replace(' ', '', $list_formula[$x]['nama_line']);

                    }

                    $lpr = Yii::$app->db
                            ->createCommand("
                                            WITH cte AS (
                                                select cast(right(id_bb,length(id_bb)-2) as integer)+1 new_serial from log_penimbangan_rm where date_part('year'::text, timestamp) = date_part('year'::text, 'now'::text::date) 
                                                and id_bb is not null order by cast(right(id_bb,length(id_bb)-2) as integer) desc limit 1
                                                )
                                            SELECT concat(right(date_part('year'::text, 'now'::text::date)::character varying::text, 2), cast(cte.new_serial as text)) AS new_id
                                               FROM cte")
                            ->queryOne();
                    $id_bb = $lpr['new_id'];

                    $save = true;
                    while ($save){
                        //try insert to log_penimbangan_rm if id_bb duplicate and error, change id_bb
                        try{
                            $connection2 = Yii::$app->getDb();
                            $command2 = $connection2->createCommand("

                            INSERT INTO log_penimbangan_rm(nomo,no_formula,no_revisi,naitem_fg,kode_bb,nama_bb,kode_olah,siklus,weight,satuan,timbangan,realisasi,satuan_realisasi,operator,no_timbangan, koitem_fg, status, is_split, nama_operator, kode_internal, nama_line,is_repack,id_bb,no_smb,mo_odoo)
                            SELECT :nomo as nomo,
                                    :no_formula as no_formula,
                                    :no_revisi as no_revisi,
                                    :nama_fg as naitem_fg,
                                    :kode_bb as kode_bb,
                                    :nama_bb as nama_bb,
                                    :kode_olah as kode_olah,
                                    :siklus as siklus,
                                    :weight as weight,
                                    :satuan as satuan,
                                    :timbangan as timbangan,
                                    :realisasi as realisasi,
                                    :satuan_realisasi as satuan_realisasi,
                                    :operator as operator,
                                    :timbangan as no_timbangan,
                                    :koitem_fg as koitem_fg,
                                    :status as status,
                                    :split as is_split,
                                    :nama_operator as nama_operator,
                                    :kode_internal as kode_internal,
                                    :nama_line as nama_line,
                                    :is_repack as is_repack,
                                    :id_bb as id_bb,
                                    :no_smb as no_smb,
                                    :mo_odoo as mo_odoo",

                            [':nomo' => $nomo,
                             ':no_formula' => $list_formula[$x]['no_dokumen'],
                             ':no_revisi' => $list_formula[$x]['no_revisi'],
                             ':nama_fg' => $scm_planner->nama_fg,
                             ':kode_bb' => $list_formula[$x]['kode_bb'],
                             ':nama_bb' => $list_formula[$x]['nama_bb'],
                             ':kode_olah' => $list_formula[$x]['kode_olah'],
                             ':siklus' => $list_formula[$x]['siklus'],
                             ':weight' => $list_formula[$x]['qty'],
                             ':satuan' => $list_formula[$x]['uom'],
                             ':timbangan' => $list_formula[$x]['timbangan'],
                             ':realisasi' => $realisasi,
                             ':satuan_realisasi' => $list_formula[$x]['uom'],
                             ':operator' => $list_formula[$x]['operator'],
                             ':koitem_fg' => $scm_planner->koitem_fg,
                             ':status' => $status,
                             ':split' => 0,
                             ':nama_operator' => $nama_operator,
                             ':kode_internal' => $list_formula[$x]['kode_internal'],
                             ':nama_line' => $nama_line,
                             ':is_repack' => $is_repack,
                             ':id_bb' => $id_bb,
                             ':no_smb' => $dcd['no_smb'],
                             ':mo_odoo' => $dcd['mo_odoo']
                            ]);

                            $result2 = $command2->queryAll();
                            $save = false;

                        }catch (\Exception $e) {
                            $next_serial = (int)substr($id_bb,2,strlen($id_bb))+1;
                            $next_id_bb = substr($id_bb,0,2).$next_serial;
                            $id_bb = $next_id_bb;
                            // print_r('<br>');
                            // print_r($id_bb);
                            // print_r('<br>');
                            // print_r($dcd['no_smb']);
                            // print_r($e->getMessage());
                        }
                    }



                } else {


                    $connection3 = Yii::$app->db;
                    $command3 = $connection3->createCommand("

                    SELECT
                        *
                    FROM log_formula_breakdown_split
                    WHERE log_formula_breakdown_id = '".$list_formula[$x]['id']."' ORDER BY id ASC");

                    $list_formula_split = $command3->queryAll();

                    for ($i=0; $i < count($list_formula_split); $i++)
                    {
                        if (strpos(strtolower($list_formula_split[$i]['keterangan']), 'repack') !== false) {
                            $status = "Belum Ditimbang";
                            $nama_operator = "";
                            $realisasi = null;
                            $is_repack = 1;
                            if (strpos(str_replace(' ', '', $list_formula[$x]['nama_line']),'LW')!==false){
                                $nama_line = 'LWE03';
                            }else{
                                $nama_line = str_replace(' ', '', $list_formula[$x]['nama_line']);
                            }
                            // $realisasi = $list_formula_split[$i]['qty'];
                        } else if (strpos(strtolower($list_formula_split[$i]['keterangan']), 'base') !== false ){
                            $status = "Selesai Ditimbang";
                            $nama_operator = "BASE";
                            $realisasi = $list_formula_split[$i]['qty'];
                            $is_repack = null;
                            $nama_line = str_replace(' ', '', $list_formula[$x]['nama_line']);
                        } else {
                            $status = "Belum Ditimbang";
                            $nama_operator = "";
                            $realisasi = NULL;
                            $is_repack = null;
                            $nama_line = str_replace(' ', '', $list_formula[$x]['nama_line']);
                        }

                        $lpr = Yii::$app->db->createCommand("WITH cte as (SELECT max(coalesce(nullif(split_part(id_bb, concat(right(date_part('year', current_date)::varchar,2)) , 2),''),'0')::INTEGER)+1 AS new_serial FROM log_penimbangan_rm
                        WHERE date_part('year', timestamp)=date_part('year', CURRENT_DATE))
                        SELECT concat(right(date_part('year', current_date)::varchar,2)   ,cte.new_serial) new_id from cte")->queryOne();
                        $id_bb = $lpr['new_id'];

                        $save = true;
                        while ($save){
                            //try insert to log_penimbangan_rm if id_bb duplicate and error, change id_bb
                            try{
                                $connection4 = Yii::$app->getDb();
                                $command4 = $connection4->createCommand("


                                INSERT INTO log_penimbangan_rm(nomo,no_formula,no_revisi,naitem_fg,kode_bb,nama_bb,kode_olah,siklus,weight,satuan,timbangan,realisasi,satuan_realisasi,operator,no_timbangan, koitem_fg, status, is_split, nama_operator, kode_internal, nama_line,is_repack,id_bb,no_smb,mo_odoo)
                                SELECT :nomo as nomo,
                                        :no_formula as no_formula,
                                        :no_revisi as no_revisi,
                                        :nama_fg as naitem_fg,
                                        :kode_bb as kode_bb,
                                        :nama_bb as nama_bb,
                                        :kode_olah as kode_olah,
                                        :siklus as siklus,
                                        :weight as weight,
                                        :satuan as satuan,
                                        :timbangan as timbangan,
                                        :realisasi as realisasi,
                                        :satuan_realisasi as satuan_realisasi,
                                        :operator as operator,
                                        :timbangan as no_timbangan,
                                        :koitem_fg as koitem_fg,
                                        :status as status,
                                        :split as is_split,
                                        :nama_operator as nama_operator,
                                        :kode_internal as kode_internal,
                                        :nama_line as nama_line,
                                        :is_repack as is_repack,
                                        :id_bb as id_bb,
                                        :no_smb as no_smb,
                                        :mo_odoo as mo_odoo",

                                [':nomo' => $nomo,
                                 ':no_formula' => $list_formula[$x]['no_dokumen'],
                                 ':no_revisi' => $list_formula[$x]['no_revisi'],
                                 ':nama_fg' => $scm_planner->nama_fg,
                                 ':kode_bb' => $list_formula[$x]['kode_bb'],
                                 ':nama_bb' => $list_formula[$x]['nama_bb'],
                                 ':kode_olah' => $list_formula_split[$i]['kode_olah'],
                                 ':siklus' => $list_formula_split[$i]['siklus'],
                                 ':weight' => $list_formula_split[$i]['qty'],
                                 ':satuan' => $list_formula_split[$i]['uom'],
                                 ':timbangan' => $list_formula_split[$i]['timbangan'],
                                 ':realisasi' => $realisasi,
                                 ':satuan_realisasi' => $list_formula_split[$i]['uom'],
                                 ':operator' => $list_formula_split[$i]['operator'],
                                 ':koitem_fg' => $scm_planner->koitem_fg,
                                 ':status' => $status,
                                 ':split' => 0,
                                 ':nama_operator' => $nama_operator,
                                 ':kode_internal' => $list_formula[$x]['kode_internal'],
                                 ':nama_line' => $nama_line,
                                 ':is_repack' => $is_repack,
                                 ':id_bb' => $id_bb,
                                 ':no_smb' => $dcd['no_smb'],
                                 ':mo_odoo' => $dcd['mo_odoo']
                                ]);


                                $result4 = $command4->queryAll();
                                $save = false;

                            }catch (\Exception $e) {
                                $next_serial = (int)substr($id_bb,2,strlen($id_bb))+1;
                                $next_id_bb = substr($id_bb,0,2).$next_serial;
                                $id_bb = $next_id_bb;
                                // print_r('<br>');
                                // print_r($id_bb);
                                // print_r('<br>');
                                // print_r($e->getMessage());
                            }

                        }


                    }
                }
            }

            if ($result2 or $result4){
                echo 1;
            } else {
                echo 0;
            }
        } else {

            echo 2;
        }

    }

    public function actionSendDataToWeighing2($nomo,$kode_bulk,$reference,$qty_batch,$param)
    {
        date_default_timezone_set('Asia/Jakarta');

        $result2 = 0;
        $result4 = 0;

        $sql = "
        SELECT
            nomo
        FROM log_penimbangan_rm
        WHERE nomo = '".$nomo."'";

        $nomoExist = LogPenimbanganRm::findBySql($sql)->all();

        $sql6 = "
        SELECT
            nomo
        FROM flow_input_mo
        WHERE nomo = '".$nomo."' AND posisi = 'PENIMBANGAN'";

        $alreadyStart = FlowInputMo::findBySql($sql6)->all();

        $sql2 = "
        SELECT
            nomo,snfg,nama_fg,koitem_fg
        FROM scm_planner
        WHERE nomo = '".$nomo."'";

        $scm_planner = ScmPlanner::findBySql($sql2)->one();

        $connection3 = Yii::$app->db;
        $command3 = $connection3->createCommand("

        SELECT nomo as mo_odoo, no_smb
        FROM daily_consumption_default
        WHERE  name = :nomo
        ",
        [':nomo'=> $nomo,
        ]);
        $dcd = $command3->queryOne();
        // print_r($dcd['no_smb']);
        // exit();

        if (empty($alreadyStart)){

            if (!empty($nomoExist)){
                $connection5 = Yii::$app->db;
                $command5 = $connection5->createCommand("

                DELETE
                FROM log_penimbangan_rm
                WHERE nomo = '".$nomo."'");

                $delete = $command5->queryAll();
            }

            $connection = Yii::$app->db;
            $command = $connection->createCommand("

            SELECT
                *
            FROM log_formula_breakdown
            WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch."
            ORDER BY siklus, (COALESCE(substring(kode_olah::text, '^(\d+)'::text)::integer, 99999999)),
            (substring(kode_olah::text, '[a-zA-z_-]+'::text)),
            (COALESCE(substring(kode_olah::text, '(\d+)$'::text)::integer, 0)),
            urutan asc
            ");

            $list_formula = $command->queryAll();

            $connection = Yii::$app->db;
            $command = $connection->createCommand("

            SELECT
                no_dokumen
            FROM log_br_detail
            WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." ");

            $br_detail = $command->queryOne();

            $status = "Belum Ditimbang";
            $nama_operator = NULL;
            $realisasi = NULL;

            for ($x=0; $x < count($list_formula); $x++)
            {
                if ($list_formula[$x]['is_split']==0 OR $list_formula[$x]['is_split']==99)
                {

                    // if (strpos(strtolower($list_formula[$x]['keterangan']), 'base') !== false ){
                    //     $status = "Selesai Ditimbang";
                    // } else {
                    //     $status = "Belum Ditimbang";
                    // }

                    if (strpos(strtolower($list_formula[$x]['keterangan']), 'repack') !== false or $list_formula[$x]['is_repack']==1) {
                        $status = "Belum Ditimbang";
                        $nama_operator = "";
                        $realisasi = null;
                        $is_repack = 1;
                        if (strpos(str_replace(' ', '', $list_formula[$x]['nama_line']),'LW')!==false){
                            $nama_line = 'LWE03';
                        }else{
                            $nama_line = str_replace(' ', '', $list_formula[$x]['nama_line']);
                        }
                        // $realisasi = $list_formula[$x]['qty'];
                    } else if (strpos(strtolower($list_formula[$x]['keterangan']), 'base') !== false ){
                        $status = "Selesai Ditimbang";
                        $nama_operator = "BASE";
                        $realisasi = $list_formula[$x]['qty'];
                        $is_repack = null;
                        $nama_line = str_replace(' ', '', $list_formula[$x]['nama_line']);

                    } else {
                        $status = "Belum Ditimbang";
                        $nama_operator = "";
                        $realisasi = NULL;
                        $is_repack = NULL;
                        $nama_line = str_replace(' ', '', $list_formula[$x]['nama_line']);
                    }

                    $lpr = Yii::$app->db
                            ->createCommand("
                                            WITH cte AS (
                                                select cast(right(id_bb,length(id_bb)-2) as integer)+1 new_serial from log_penimbangan_rm where date_part('year'::text, timestamp) = date_part('year'::text, 'now'::text::date) 
                                                and id_bb is not null order by cast(right(id_bb,length(id_bb)-2) as integer) desc limit 1
                                                )
                                            SELECT concat(right(date_part('year'::text, 'now'::text::date)::character varying::text, 2), cast(cte.new_serial as text)) AS new_id
                                               FROM cte")
                            ->queryOne();
                    $id_bb = $lpr['new_id'];

                    $save = true;
                    while ($save){
                        try {
                            $connection2 = Yii::$app->getDb();
                            $command2 = $connection2->createCommand("


                            INSERT INTO log_penimbangan_rm(nomo,no_formula,no_revisi,naitem_fg,kode_bb,nama_bb,kode_olah,siklus,weight,satuan,timbangan,realisasi,satuan_realisasi,operator,no_timbangan, koitem_fg, status, is_split, nama_operator, kode_internal, is_repack,id_bb,no_smb,mo_odoo,nama_line)
                            SELECT :nomo as nomo,
                                    :no_formula as no_formula,
                                    :no_revisi as no_revisi,
                                    :nama_fg as naitem_fg,
                                    :kode_bb as kode_bb,
                                    :nama_bb as nama_bb,
                                    :kode_olah as kode_olah,
                                    :siklus as siklus,
                                    :weight as weight,
                                    :satuan as satuan,
                                    :timbangan as timbangan,
                                    :realisasi as realisasi,
                                    :satuan_realisasi as satuan_realisasi,
                                    :operator as operator,
                                    :timbangan as no_timbangan,
                                    :koitem_fg as koitem_fg,
                                    :status as status,
                                    :split as is_split,
                                    :nama_operator as nama_operator,
                                    :kode_internal as kode_internal,
                                    :is_repack as is_repack,
                                    :id_bb as id_bb,
                                    :no_smb as no_smb,
                                    :mo_odoo as mo_odoo,
                                    :nama_line as nama_line",

                            [':nomo' => $nomo,
                             ':no_formula' => $br_detail['no_dokumen'],
                             ':no_revisi' => $list_formula[$x]['no_revisi'],
                             ':nama_fg' => $scm_planner->nama_fg,
                             ':kode_bb' => $list_formula[$x]['kode_bb'],
                             ':nama_bb' => $list_formula[$x]['nama_bb'],
                             ':kode_olah' => $list_formula[$x]['kode_olah'],
                             ':siklus' => $list_formula[$x]['siklus'],
                             ':weight' => $list_formula[$x]['qty'],
                             ':satuan' => $list_formula[$x]['uom'],
                             ':timbangan' => $list_formula[$x]['timbangan'],
                             ':realisasi' => $realisasi,
                             ':satuan_realisasi' => $list_formula[$x]['uom'],
                             ':operator' => $list_formula[$x]['operator'],
                             ':koitem_fg' => $scm_planner->koitem_fg,
                             ':status' => $status,
                             ':split' => 0,
                             ':nama_operator' => $nama_operator,
                             ':kode_internal' => $list_formula[$x]['kode_internal'],
                             ':is_repack' => $is_repack,
                             ':id_bb' => $id_bb,
                             ':no_smb' => $dcd['no_smb'],
                             ':mo_odoo' => $dcd['mo_odoo'],
                             ':nama_line' => $nama_line
                            ]);

                            $result2 = $command2->queryAll();


                            //when model->save() not error then run this
                            $save = false;
                        } catch (\Exception $e) {
                            $next_serial = (int)substr($id_bb,2,strlen($id_bb))+1;
                            $next_id_bb = substr($id_bb,0,2).$next_serial;
                            $id_bb = $next_id_bb;
                        }
                    }


                }
            }

            if ($param == 1){
                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("

                UPDATE log_approval_br
                SET status_reviewer = 'APPROVED', timestamp_reviewer = :timestamp_reviewer, status_approver = 'APPROVED', timestamp_approver = :timestamp_approver, current_pic = 'APPROVER', current_status = 'APPROVED', log = 'direct'
                WHERE nomo = :nomo;
                ",
                [':nomo'=> $nomo,
                 ':timestamp_reviewer'=> date('Y-m-d H:i:s'),
                 ':timestamp_approver'=> date('Y-m-d H:i:s')
                ]);

                $result = $command->queryAll();
            }

            if ($result2){
                echo 1;
            } else {
                echo 0;
            }
        } else {

            echo 2;
        }

    }

    public function actionUpdateStatus($nomo,$pic)
    {
        date_default_timezone_set('Asia/Jakarta');
        //$current_time = date("Y-m-d H:i:s");
        $nama_line = LogPenimbanganRm::find()->where(['nomo'=>$nomo])->one()['nama_line'];
        $status = "verified";

        // Stop Jadwal
        $connection = Yii::$app->db;
        $command = $connection->createCommand()
                    ->insert('log_verified_br',
                            [
                                'nomo'=> $nomo ,
                                'timestamp'=> date('Y-m-d H:i:s') ,
                                'status'=> $status,
                                'pic' => $pic,
                                'nama_line'=>$nama_line
                            ]);

        $result = $command->execute();

        // $command = $connection->createCommand("

        // UPDATE log_jadwal_timbang_rm
        // SET timestamp = :current_time,
        //     status = :status
        // WHERE nomo = :nomo;
        // ",
        // [':current_time' => date('Y-m-d h:i:s'),
        //  ':nomo'=> $nomo,
        //  ':status'=>$status
        // ]);

        // $model->save();

        // $result = $command->queryAll();

        if ($result){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionUpdateUom($id,$uom)
    {
        date_default_timezone_set('Asia/Jakarta');
        //$current_time = date("Y-m-d H:i:s");

        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        UPDATE log_formula_breakdown
        SET uom = :uom
        WHERE id = :id;
        ",
        [':uom' => $uom,
         ':id'=> $id
        ]);

        $result = $command->queryAll();

        if ($result)
        {
            $value = array(
            "status"=>"success");
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    public function actionUpdateUomSplit($id_split,$uom)
    {
        date_default_timezone_set('Asia/Jakarta');
        //$current_time = date("Y-m-d H:i:s");

        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        UPDATE log_formula_breakdown_split
        SET uom = :uom
        WHERE id = :id;
        ",
        [':uom' => $uom,
         ':id'=> $id_split
        ]);

        $result = $command->queryAll();

        if ($result)
        {
            $value = array(
            "status"=>"success");
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    public function actionUpdateNomor($kode_bulk,$reference,$qty_batch,$dokumen,$revisi)
    {
        date_default_timezone_set('Asia/Jakarta');

        // Stop Jadwal
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        UPDATE log_formula_breakdown
        SET no_dokumen = :dokumen,
            no_revisi = :revisi
        WHERE kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch;
        ",
        [':dokumen' => $dokumen,
         ':revisi'=> $revisi,
         ':kode_bulk'=>$kode_bulk,
         ':reference' => $reference,
         ':qty_batch' => $qty_batch
        ]);

        // $model->save();

        $result = $command->queryAll();

        if ($result){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionUpdateOperator($id,$operator,$line,$uom)
    {
        //date_default_timezone_set('Asia/Jakarta');
        //$current_time = date("Y-m-d H:i:s");
        //$status = "verified";
        // print_r($operator);
        // print_r($line);
        // print_r($uom);

        // $kode_timbangan = Yii::$app->db->createCommand("SELECT * FROM master_data_timbangan_rm WHERE uom = '".$uom."' and operator = '".$operator."' and line = '".$line."'")->queryOne()['kode_timbangan'];


        $bb = LogFormulaBreakdown::findBySql("SELECT qty,uom,operator,keterangan FROM log_formula_breakdown WHERE id = :id ",[':id'=>$id])->one();
        $keterangan =  $bb->keterangan;

        // Update operator
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        UPDATE log_formula_breakdown
        SET operator = :operator
        WHERE id = :id;
        ",
        [':operator'=> $operator,
         ':id'=>$id
        ]);

        $result = $command->queryAll();

        // $sql_timbangan = "
        //     SELECT kode_timbangan,uom
        //     FROM master_data_timbangan_rm
        //     WHERE line='".$line."' and uom = '".$uom."' and kode_timbangan != '".$kode_timbangan."'
        //     ORDER BY id ASC
        //     ";

        // Cek apakah bb adalah repack
        if(strpos(strtolower($keterangan),'repack')!==false && strpos($line,'LW')!==false){
            $kode_timbangan = Yii::$app->db->createCommand("SELECT * FROM master_data_timbangan_rm WHERE uom = '".$uom."' and operator = '".$operator."' and line = 'LWE03'")->queryOne()['kode_timbangan'];

            $sql_timbangan = "
                SELECT kode_timbangan,uom
                FROM master_data_timbangan_rm
                WHERE line='LWE03' and uom = '".$uom."'
                ORDER BY id ASC
                ";
        }else{
            $kode_timbangan = Yii::$app->db->createCommand("SELECT * FROM master_data_timbangan_rm WHERE uom = '".$uom."' and operator = '".$operator."' and line = '".$line."'")->queryOne()['kode_timbangan'];

            $sql_timbangan = "
                SELECT kode_timbangan,uom
                FROM master_data_timbangan_rm
                WHERE line='".$line."' and uom = '".$uom."' and (operator in (".$operator.", 0))
                ORDER BY id ASC
                ";

        }

        $timbangans = MasterDataTimbanganRm::findBySql($sql_timbangan)->all();
        if ($kode_timbangan!==''){
            $command2 = $connection->createCommand("
            UPDATE log_formula_breakdown
            SET timbangan = :timbangan
            WHERE id = :id;
            ",
            [':timbangan'=> $kode_timbangan,
             ':id'=>$id
            ]);

            $result2 = $command2->queryAll();
        }

        if (empty($timbangans)){
            echo "-";
        } else {
            echo "<option value='Pilih Timbangan'>Pilih Timbangan</options>";
            echo "<option selected='selected'>".$kode_timbangan."</options>";
            foreach($timbangans as $timbangan){
                echo "<option value='".$timbangan->kode_timbangan."'>".$timbangan->kode_timbangan."</options>";
            }
        }
    }

    public function actionUpdateOperatorSplit($id_split,$operator,$line,$uom)
    {
        //date_default_timezone_set('Asia/Jakarta');
        //$current_time = date("Y-m-d H:i:s");
        //$status = "verified";
        $kode_timbangan = Yii::$app->db->createCommand("SELECT * FROM master_data_timbangan_rm WHERE uom = '".$uom."' and operator = '".$operator."' and line = '".$line."'")->queryOne()['kode_timbangan'];

        $bb = LogFormulaBreakdownSplit::findBySql("SELECT qty,uom,operator,keterangan FROM log_formula_breakdown_split WHERE id = ".$id_split." ")->one();
        $keterangan =  $bb->keterangan;

        // Stop Jadwal
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        UPDATE log_formula_breakdown_split
        SET operator = :operator
        WHERE id = :id;
        ",
        [':operator'=> $operator,
         ':id'=>$id_split
        ]);

        $result = $command->queryAll();

        // $sql_timbangan = "
        //     SELECT kode_timbangan,uom
        //     FROM master_data_timbangan_rm
        //     WHERE line='".$line."' and uom = '".$uom."' and kode_timbangan != '".$kode_timbangan."'
        //     ORDER BY id ASC
        //     ";

        // Cek apakah bb adalah repack
        if(strpos(strtolower($keterangan),'repack')!==false && strpos($line,'LW')!==false){
            $kode_timbangan = Yii::$app->db->createCommand("SELECT * FROM master_data_timbangan_rm WHERE uom = '".$uom."' and operator = '".$operator."' and line = 'LWE03'")->queryOne()['kode_timbangan'];

            $sql_timbangan = "
                SELECT kode_timbangan,uom
                FROM master_data_timbangan_rm
                WHERE line='LWE03' and uom = '".$uom."'
                ORDER BY id ASC
                ";
        }else{
            $kode_timbangan = Yii::$app->db->createCommand("SELECT * FROM master_data_timbangan_rm WHERE uom = '".$uom."' and operator = '".$operator."' and line = '".$line."'")->queryOne()['kode_timbangan'];

            $sql_timbangan = "
                SELECT kode_timbangan,uom
                FROM master_data_timbangan_rm
                WHERE line='".$line."' and uom = '".$uom."' and (operator in (".$operator.", 0))
                ORDER BY id ASC
                ";

        }


        $timbangans = MasterDataTimbanganRm::findBySql($sql_timbangan)->all();
        if ($kode_timbangan!==''){
            $command2 = $connection->createCommand("
            UPDATE log_formula_breakdown_split
            SET timbangan = :timbangan
            WHERE id = :id;
            ",
            [':timbangan'=> $kode_timbangan,
             ':id'=>$id_split
            ]);

            $result2 = $command2->queryAll();
        }

        if (empty($timbangans)){
            echo "-";
        } else {
            echo "<option value='Pilih Timbangan'>Pilih Timbangan</options>";
            echo "<option selected='selected'>".$kode_timbangan."</options>";
            foreach($timbangans as $timbangan){
                echo "<option value='".$timbangan->kode_timbangan."'>".$timbangan->kode_timbangan."</options>";
            }
        }
    }

    public function actionUpdateTimbangan($id,$timbangan)
    {
        //date_default_timezone_set('Asia/Jakarta');
        //$current_time = date("Y-m-d H:i:s");
        //$status = "verified";

        // Stop Jadwal
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        UPDATE log_formula_breakdown
        SET timbangan = :timbangan
        WHERE id = :id;
        ",
        [':timbangan'=> $timbangan,
         ':id'=>$id
        ]);

        $result = $command->queryAll();

        if ($result){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionUpdateTimbanganSplit($id_split,$timbangan)
    {
        //date_default_timezone_set('Asia/Jakarta');
        //$current_time = date("Y-m-d H:i:s");
        //$status = "verified";

        // Stop Jadwal
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        UPDATE log_formula_breakdown_split
        SET timbangan = :timbangan
        WHERE id = :id;
        ",
        [':timbangan'=> $timbangan,
         ':id'=>$id_split
        ]);

        $result = $command->queryAll();

        if ($result){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionGetOperatorRepack($idItem,$idSplit=null,$is_repack)
    {
        if (empty($idSplit)){
            $operator = LogFormulaBreakdown::find()->where(['id'=>$idItem])->one()['operator'];
        }else{
            $operator = LogFormulaBreakdownSplit::find()->where("id = ".$idSplit." ")->one()['operator'];
        }

        if (empty($idSplit)){
            $update = Yii::$app->db->createCommand("UPDATE log_formula_breakdown SET operator = null, timbangan = null where id = ".$idItem." ")->execute();

        }else{
            $update = Yii::$app->db->createCommand("UPDATE log_formula_breakdown_split SET operator = null, timbangan = null where id = ".$idSplit." ")->execute();
        }

            echo '<option value="0" selected>Pilih Operator</option>';
            echo '<option value="1">1</option>';
            echo '<option value="2">2</option>';
            echo '<option value="3">3</option>';


    }

    // public function actionGetTimbangNomo($nomo)
    // {
    //     $sql = "
    //             SELECT nama_line
    //             FROM log_jadwal_timbang_rm
    //             WHERE nomo = '".$nomo."'
    //             ORDER BY id desc
    //             ";

    //     $line_timbang_array = LogJadwalTimbangRm::findBySql($sql)->one();
    //     if ($line_timbang_array->nama_line == NULL)
    //     {
    //         $line_timbang = "Pilih Line";
    //     } else if (strpos($line_timbang_array->nama_line, '/') !== false){
    //         $line_timbang = "Pilih Line";
    //     } else {
    //         $line_timbang = $line_timbang_array->nama_line;
    //     }

    //     if ($line_timbang == "Pilih Line"){
    //         $sql = "
    //                 SELECT kode_timbangan
    //                 FROM master_data_timbangan_rm
    //                 ORDER BY kode_timbangan ASC
    //                 ";

    //         $timbangans = MasterDataTimbanganRm::findBySql($sql)->all();

    //     } else {
    //         $sql = "
    //                 SELECT kode_timbangan
    //                 FROM master_data_timbangan_rm
    //                 WHERE line='".$line_timbang."'
    //                 ORDER BY id ASC
    //                 ";

    //         $timbangans = MasterDataTimbanganRm::findBySql($sql)->all();
    //     }

    //     echo "<select class='form-control form-control-sm text-center' style='height:2em; text-align:center; float:left;'><option value='Pilih Timbangan'>Pilih Timbangan</options>";
    //     foreach($timbangans as $timbangan){
    //         echo "<option value='".$timbangan->kode_timbangan."'>".$timbangan->kode_timbangan."</options>";
    //     }
    //     echo "</select>";
    // }*/

    public function actionGetDetailLog($kode_bulk,$reference)
    {

        $sql = "
            SELECT timestamp,last_user,sediaan
            FROM log_master_br
            WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."'
        ";

        $log_data = LogMasterBr::findBySql($sql)->one();

        // if (!empty($log_data)){
        // // Redirect to Downtime
        //     $value = array(
        //     "timestamp"=>"success");
        //     // return $this->redirect(['create-kemas1', 'snfg_komponen' => $snfg_komponen]);
        //      // return $this->redirect(['check-kemas1']);
        // } else {
        //     $value = array(
        //     "status"=>"not-success");
        // }

        if (!empty($log_data)){
            echo Json::encode($log_data);
        } else {
            echo 0;
        }
    }


    public function actionGetDecimalDigit($timbangan)
    {

        $sql = "
            SELECT angka_belakang_koma
            FROM master_data_timbangan_rm
            WHERE kode_timbangan = '".$timbangan."'
        ";

        $decimal_digit = MasterDataTimbanganRm::findBySql($sql)->one()['angka_belakang_koma'];

        // if (!empty($log_data)){
        // // Redirect to Downtime
        //     $value = array(
        //     "timestamp"=>"success");
        //     // return $this->redirect(['create-kemas1', 'snfg_komponen' => $snfg_komponen]);
        //      // return $this->redirect(['check-kemas1']);
        // } else {
        //     $value = array(
        //     "status"=>"not-success");
        // }

        if (empty($decimal_digit) || $decimal_digit == 0){
            return 6;
        } else {
            return $decimal_digit;
        }
    }

    public function actionUpdateKodeOlah($id,$value,$kode_bulk,$reference,$qty_batch)
    {
        if ($value == ""){
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

                UPDATE log_formula_breakdown
                SET kode_olah = :kode_olah
                WHERE id = :id;
                ",
                [':id'=> $id,
                 ':kode_olah'=>$value
                ]);

            $result = $command->queryAll();

            if ($result){
                // $value = array(
                // "status"=>"success");
                echo 1;
            } else {
                // $value = array(
                // "status"=>"not-success");
                echo 0;
            }
        } else {
            $sql = "
                SELECT kode_bb
                FROM log_formula_breakdown
                WHERE id = ".$id ;

            $kode_bb = LogFormulaBreakdown::findBySql($sql)->one();

            $sql2 = "
                SELECT *
                FROM log_formula_breakdown
                WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND kode_olah = '".$value."' AND kode_bb <> '".$kode_bb->kode_bb."' AND is_split < 99
            ";

            $isKodeOlahExist = LogFormulaBreakdown::findBySql($sql2)->all();

            if (empty($isKodeOlahExist)){
                $connection = Yii::$app->db;
                $command = $connection->createCommand("

                    UPDATE log_formula_breakdown
                    SET kode_olah = :kode_olah
                    WHERE id = :id;
                    ",
                    [':id'=> $id,
                     ':kode_olah'=>$value
                    ]);

                $result = $command->queryAll();

                if ($result){
                    // $value = array(
                    // "status"=>"success");
                    echo 1;
                } else {
                    // $value = array(
                    // "status"=>"not-success");
                    echo 0;
                }
            } else {
                echo 2;
            }
        }

        // echo Json::encode($value);
    }

    public function actionUpdateKodeInternal($id,$value)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

            UPDATE log_formula_breakdown
            SET kode_internal = :kode_internal
            WHERE id = :id;
            ",
            [':id'=> $id,
             ':kode_internal'=>$value
            ]);

        $result = $command->queryAll();

        if ($result){
            $value = array(
            "status"=>"success");
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    public function actionUpdateKodeOlahSplit($id,$id_split,$value,$kode_bulk,$reference,$qty_batch)
    {
        if ($value == ""){
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

                UPDATE log_formula_breakdown
                SET kode_olah = :kode_olah
                WHERE id = :id;
                ",
                [':id'=> $id,
                 ':kode_olah'=>$value
                ]);

            $result = $command->queryAll();

            if ($result){
                // $value = array(
                // "status"=>"success");
                echo 1;
            } else {
                // $value = array(
                // "status"=>"not-success");
                echo 0;
            }
        } else {
            $sql = "
                SELECT kode_bb
                FROM log_formula_breakdown
                WHERE id = ".$id ;

            $kode_bb = LogFormulaBreakdown::findBySql($sql)->one();

            $sql2 = "
                SELECT *
                FROM log_formula_breakdown
                WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND kode_olah = '".$value."' AND kode_bb <> '".$kode_bb->kode_bb."' AND is_split < 99
            ";

            $isKodeOlahExist = LogFormulaBreakdown::findBySql($sql2)->all();

            // $sql3 = "
            //     SELECT *
            //     FROM log_formula_breakdown_split
            //     WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch= ".$qty_batch." AND kode_olah = '".$value."' AND kode_bb <> '".$kode_bb."'
            // ";

            // $isKodeOlahExist = LogFormulaBreakdownSplit::findBySql($sql3)->all();

            if (empty($isKodeOlahExist))
            {
                $connection = Yii::$app->db;
                $command = $connection->createCommand("

                    UPDATE log_formula_breakdown_split
                    SET kode_olah = :kode_olah
                    WHERE id = :id;
                    ",
                    [':id'=> $id_split,
                     ':kode_olah'=>$value
                    ]);

                $result = $command->queryAll();

                if ($result){
                    // $value = array(
                    // "status"=>"success");
                    echo 1;
                } else {
                    // $value = array(
                    // "status"=>"not-success");
                    echo 0;
                }
            } else {
                echo 2;
            }
        }

        // echo Json::encode($value);
    }

    public function actionUpdateAction($id,$value)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

            UPDATE log_formula_breakdown
            SET action = :action
            WHERE id = :id;
            ",
            [':id'=> $id,
             ':action'=>$value
            ]);

        $result = $command->queryAll();

        if ($result){
            $value = array(
            "status"=>"success");
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    public function actionUpdateKodebb($id,$value)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

            UPDATE log_formula_breakdown
            SET kode_bb = :kode_bb
            WHERE id = :id;
            ",
            [':id'=> $id,
             ':kode_bb'=>$value
            ]);

        $result = $command->queryAll();

        if ($result){
            $value = array(
            "status"=>"success");
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    public function actionUpdateKeterangan($id,$value)
    {
        $connection3 = Yii::$app->db;
        $command3 = $connection3->createCommand("

            SELECT * FROM log_formula_breakdown
            WHERE id = :id;
            ",
            [':id'=> $id,
            ]);

        $result3 = $command3->queryAll();

        if ($result3[0]["is_split"] > 0){
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

                UPDATE log_formula_breakdown
                SET keterangan = :keterangan
                WHERE id = :id;
                ",
                [':id'=> $id,
                 ':keterangan'=>$value
                ]);

            $result = $command->queryAll();

            $connection2 = Yii::$app->db;
            $command2 = $connection2->createCommand("

                UPDATE log_formula_breakdown_split
                SET keterangan = :keterangan
                WHERE log_formula_breakdown_id = :id;
                ",
                [':id'=> $id,
                 ':keterangan'=>$value
                ]);

            $result2 = $command2->queryAll();

            if ($result && $result2){
                $value = array(
                "status"=>"success");
            } else {
                $value = array(
                "status"=>"not-success");
            }
        } else {
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

                UPDATE log_formula_breakdown
                SET keterangan = :keterangan
                WHERE id = :id;
                ",
                [':id'=> $id,
                 ':keterangan'=>$value
                ]);

            $result = $command->queryAll();

            if ($result){
                $value = array(
                "status"=>"success");
            } else {
                $value = array(
                "status"=>"not-success");
            }
        }

        echo Json::encode($value);
    }

    public function actionUpdateKeteranganSplit($id_split,$value)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

            UPDATE log_formula_breakdown_split
            SET keterangan = :keterangan
            WHERE id = :id;
            ",
            [':id'=> $id_split,
             ':keterangan'=>$value
            ]);

        $result = $command->queryAll();

        if ($result){
            $value = array(
            "status"=>"success");
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    function like_match($pattern, $subject){
        $pattern = str_replace('%', '.*', preg_quote($pattern, '/'));
        return (bool) preg_match("/^{$pattern}$/i", $subject);
    }

    public function comparingBRwBOM($version,$kode_bulk,$nomo,$reference,$qty_batch){
        if ($version == 'new') {
            $existBom = $this->querySelNewLFB($kode_bulk, $reference, $qty_batch);
        }else{
            // $existBom = $this->querySelOldLFB($kode_bulk, $reference, $qty_batch);
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

            SELECT *
            FROM log_formula_breakdown
            WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference
            AND qty_batch = :qty_batch ORDER BY uom DESC, qty ASC
            ",
            [':kode_bulk'=> $kode_bulk,
             ':reference'=> $reference,
             ':qty_batch' => $qty_batch
            ]);

            $existBom = $command->queryAll();
        }
        // $existBom = $this->querySelLFBParentOnly($kode_bulk, $reference, $qty_batch);

        $listFormula = $this->querySelDCD($nomo);
        $listIndex = [];
        $listNamaBB= [];
        $listKodeBB= [];
        // Cek parent duplicate
        for ($i=0; $i < count($existBom); $i++) {
            $isAvailable = false;
            for ($j=0; $j < count($listFormula); $j++) {
                if($existBom[$i]['kode_bb'] == $listFormula[$j]['component_code']){
                    if($existBom[$i]['kode_bb'] == 'AIR-RO--L'){
                        $air_ro = LogFormulaBreakdown::find()
                            ->where([
                                'kode_bulk'=> $kode_bulk,
                                'formula_reference'=> $reference,
                                'qty_batch' => $qty_batch,
                                'kode_bb'=> $existBom[$i]['kode_bb']
                            ])->one();
                        $air_ro->nama_bb = 'Air R/O';
                        $air_ro->kode_olah = '-';
                        $air_ro->kode_internal = 'SLV1-AAD';
                        $air_ro->update();
                    }

                    $isAvailable = true;
                    break;
                }else{
                    $isAvailable = false;
                }
            }
            if($isAvailable == false){
                array_push($listIndex,$existBom[$i]['id']);
            }
        }

        // Cek ketika ada kode bb dan nama bb sama,
        // akan dimasukkan ke list lalu delete
        /*for ($i=0; $i < count($existBom); $i++) {
            if (in_array($existBom[$i]['nama_bb'], $listNamaBB) &&
            in_array($existBom[$i]['kode_bb'], $listKodeBB)) {
                array_push($listIndex,$i);
            }else{
                array_push($listNamaBB,$existBom[$i]['nama_bb']);
                array_push($listKodeBB,$existBom[$i]['kode_bb']);
            }
        }*/

        if(!empty($listIndex)){
            $this->queryDelLFB($listIndex);
        }

        // for ($i=0; $i < count($listIndex); $i++) {
        //   $this->queryDelLFB($existBom[$listIndex[$i]]['kode_bulk'],$existBom[$listIndex[$i]]['formula_reference'],$existBom[$listIndex[$i]]['qty_batch'],$existBom[$listIndex[$i]]['kode_bb']);
        //   if (($existBom[$listIndex[$i]]['is_split'] > 0) && ($existBom[$listIndex[$i]]['is_split'] < 99) ) {
        //       $listSplit = $this->querySelLFBSplitOnly($existBom[$i]);
        //       print_r($listSplit);
        //       for ($j=0; $j < count($listSplit); $j++) {
        //           $this->queryDelLFB($listSplit[$j]['kode_bulk'],$listSplit[$j]['formula_reference'],$listSplit[$j]['qty_batch'],$listSplit[$j]['kode_bb']);
        //       }
        //       $this->queryDelLFBSplit($existBom[$listIndex[$i]]['kode_bulk'],$existBom[$listIndex[$i]]['formula_reference'],$existBom[$listIndex[$i]]['qty_batch']);
        //   }
        // }
    }

    public function comparingBOMwBR($version,$kode_bulk,$nomo,$reference,$qty_batch){
        if ($version == 'new') {
            $existBom = $this->querySelNewLFB($kode_bulk, $reference, $qty_batch);
        }else{
            $existBom = $this->querySelOldLFB($kode_bulk, $reference, $qty_batch);
        }
        $listFormula = $this->querySelDCD($nomo);
        $listIndex = [];
        for ($i=0; $i < count($listFormula); $i++) {
            $isAvailable = false;
            for ($j=0; $j < count($existBom); $j++) {
                if($listFormula[$i]['component_code'] == $existBom[$j]['kode_bb']){
                    // print_r($j+1 .". Ada <br>");
                    $isAvailable = true;
                    break;
                }else{
                    // print_r($j+1 .". Gada <br>");
                    $isAvailable = false;
                }
            }
            // print_r($isAvailable."<br>");
            if($isAvailable == false){
                array_push($listIndex,$i);
            }
        }
        for ($i=0; $i < count($listIndex); $i++) {
            $this->queryInsLFB($version, $kode_bulk, $listFormula[$listIndex[$i]], $nomo);
        }
    }

    public function comparingCopyBRwBOM($version,$kode_bulk,$nomo,$reference,$reference_copy,$qty_batch){
        /*if ($version == 'new') {
            $listFormula = $this->querySelCopyDCD($kode_bulk,$reference);
            $existBom = $this->querySelNewLFB($kode_bulk, $reference, $qty_batch);
        }else{
            $listFormula = $this->querySelCopyDCD($kode_bulk,$reference_copy);
            $existBom = $this->querySelOldLFB($kode_bulk, $reference, $qty_batch);
        }*/
        $existBom = $this->querySelLFBParentOnly($kode_bulk, $reference, $qty_batch);
        if ($version == 'new') {
            $listFormula = $this->querySelCopyDCD($kode_bulk,$reference);
        }else{
            $listFormula = $this->querySelCopyDCD($kode_bulk,$reference);
        }
        $listIndex = [];
        $listNamaBB= [];
        $listKodeBB= [];
        for ($i=0; $i < count($existBom); $i++) {
            $isAvailable = false;
            for ($j=0; $j < count($listFormula); $j++) {
                if($existBom[$i]['kode_bb'] == $listFormula[$j]['component_code']){
                    // print_r($j+1 .". Ada <br>");
                    if($existBom[$i]['kode_bb'] == 'AIR-RO--L'){
                        $air_ro = LogFormulaBreakdown::find()
                            ->where([
                                'kode_bulk'=> $kode_bulk,
                                'formula_reference'=> $reference,
                                'qty_batch' => $qty_batch,
                                'kode_bb'=> $existBom[$i]['kode_bb']
                            ])->one();
                        $air_ro->nama_bb = 'Air R/O';
                        $air_ro->update();
                    }
                    $isAvailable = true;
                    break;
                }else{
                    $isAvailable = false;
                }
            }
            // print_r($isAvailable."<br>");
            if($isAvailable == false){
                array_push($listIndex,$i);
            }
        }
        for ($i=0; $i < count($existBom); $i++) {
            if (in_array($existBom[$i]['nama_bb'], $listNamaBB) &&
            in_array($existBom[$i]['kode_bb'], $listKodeBB)) {
                array_push($listIndex,$i);
            }else{
                array_push($listNamaBB,$existBom[$i]['nama_bb']);
                array_push($listKodeBB,$existBom[$i]['kode_bb']);
            }
        }
        for ($i=0; $i < count($listIndex); $i++) {
          // $this->queryDelLFB($existBom[$listIndex[$i]]['kode_bulk'],$existBom[$listIndex[$i]]['formula_reference'],$existBom[$listIndex[$i]]['qty_batch'],$existBom[$listIndex[$i]]['kode_bb']);
          if (($existBom[$listIndex[$i]]['is_split'] > 0) && ($existBom[$listIndex[$i]]['is_split'] < 99) ) {
              $this->queryDelLFBSplit($existBom[$listIndex[$i]]['kode_bulk'],$existBom[$listIndex[$i]]['formula_reference'],$existBom[$listIndex[$i]]['qty_batch']);
              $listSplit = $this->querySelLFBSplitOnly($existBom[$i]);
              for ($j=0; $j < count($listSplit); $j++) {
                  $this->queryDelLFB($listSplit[$j]['kode_bulk'],$listSplit[$j]['formula_reference'],$listSplit[$j]['qty_batch'],$listSplit[$j]['kode_bb']);
              }
          }
        }
    }

    public function comparingCopyBOMwBR($version,$kode_bulk,$nomo,$reference,$reference_copy,$qty_batch){
        // printf("This reference: ".$reference." and This C. Reference: ".$reference_copy);
        if ($version == 'new') {
            $listFormula = $this->querySelCopyDCD($kode_bulk,$reference);
            $existBom = $this->querySelNewLFB($kode_bulk, $reference, $qty_batch);
        }else{
            $listFormula = $this->querySelCopyDCD($kode_bulk,$reference);
            $existBom = $this->querySelOldLFB($kode_bulk, $reference, $qty_batch);
        }
        $listIndex = [];
        for ($i=0; $i < count($listFormula); $i++) {
            $isAvailable = false;
            for ($j=0; $j < count($existBom); $j++) {
                if($listFormula[$i]['component_code'] == $existBom[$j]['kode_bb']){
                    // print_r($j+1 .". Ada <br>");
                    $isAvailable = true;
                    break;
                }else{
                    // print_r($j+1 .". Gada <br>");
                    $isAvailable = false;
                }
            }
            // print_r($isAvailable."<br>");
            if($isAvailable == false){
                array_push($listIndex,$i);
            }
        }
        for ($i=0; $i < count($listIndex); $i++) {
            $this->queryInsLFB($version, $kode_bulk, $listFormula[$listIndex[$i]], $nomo);
        }
    }

    public function queryInsLFB($version,$kode_bulk, $newFormula, $nomo){
      $sql_line="
          SELECT
              *
          FROM log_jadwal_timbang_rm_new
          WHERE nomo = '".$nomo."'";

      $row_line = LogJadwalTimbangRmNew::findBySql($sql_line)->one();

      if (empty($row_line)){
          $line_timbang = 'Pilih Line';
      } else {
          $line_timbang = $row_line->nama_line;
      }

      if ($newFormula["component_code"]=="AIR-RO--L"){
          $operator = 11;
          $timbangan = "RO 01";
          $kode_olah = "-";
          $newFormula['component'] = "Air R/O";
      } else {
          $operator = NULL;
          $timbangan = NULL;
          $kode_olah = NULL;
      }

      if($version == 'new'){
          $existBom = $this->querySelNewLFB($kode_bulk,$newFormula["reference"],number_format((float)$newFormula["scheduled_quantity"], 4, '.', ''));
      }else{
          $existBom = $this->querySelOldLFB($kode_bulk,$newFormula["reference"],number_format((float)$newFormula["scheduled_quantity"], 4, '.', ''));
      }
      $serial_manual = 0;
      for ($i=0; $i < count($existBom); $i++) {
          $qtyComponent =  number_format((float)$newFormula["total_component_qty"], 5, '.', '');
          if($newFormula["total_component_uom"] == 'kg' && $existBom[$i]['uom'] == 'kg'){
              if ($existBom[$i]['qty'] >= ($qtyComponent)) {
                  $serial_manual = $existBom[$i]['serial_manual']-1;
                  break;
              }else if(count($existBom) > 1){
                  if($i+1 == count($existBom)){
                      $serial_manual = $existBom[$i]['serial_manual']+1;
                      break;
                  }else if($existBom[$i+1]['uom'] == 'g'){
                      $serial_manual = $existBom[$i]['serial_manual']+1;
                      break;
                  }
              }
          }else if($newFormula["total_component_uom"] == 'g' && $existBom[$i]['uom'] == 'g'){
              if ($existBom[$i]['qty'] >= $qtyComponent) {
                  $serial_manual = $existBom[$i]['serial_manual']-1;
                  break;
              }else if($i == count($existBom)-1){
                  $serial_manual = $existBom[$i]['serial_manual']+1;
                  break;
              }
          }
      }

      $connection = Yii::$app->db;
      $command = $connection->createCommand("
          INSERT INTO log_formula_breakdown (kode_bulk,nama_fg,formula_reference,lokasi,scheduled_start,week,kode_bb,qty,uom,timbangan,nama_line,nama_bb,is_split,operator,siklus,qty_batch,kode_olah,kode_internal,is_repack,urutan, serial_manual,version)
          SELECT :kode_bulk as kode_bulk, :nama_fg as nama_fg, :formula_reference as formula_reference, :lokasi as lokasi, :scheduled_start as scheduled_start, :week as week, :kode_bb as kode_bb, :qty as qty, :uom as uom, :timbangan as timbangan, :nama_line as nama_line, :nama_bb as nama_bb, :is_split as is_split, :operator as operator, :siklus as siklus, :qty_batch as qty_batch, :kode_olah as kode_olah, :kode_internal as kode_internal, :is_repack as is_repack, :urutan as urutan, :serial_manual as serial_manual, :version as version
          ",
          [':kode_bulk' => $kode_bulk,
           ':nama_fg' => $newFormula["fg_name"],
           ':formula_reference' => $newFormula["reference"],
           ':lokasi' => $newFormula["location"],
           ':scheduled_start' => $newFormula["scheduled_start"],
           ':week' => $newFormula["week"],
           ':kode_bb' => $newFormula["component_code"],
           ':qty' => number_format((float)$newFormula["total_component_qty"], 5, '.', ''),
           ':uom' => $newFormula["total_component_uom"],
           ':timbangan' => $timbangan,
           ':nama_line' => $line_timbang,
           ':nama_bb' => $newFormula["component"],
           ':is_split' => 0,
           ':operator' => $operator,
           ':siklus' => 1,
           ':qty_batch' => number_format((float)$newFormula["scheduled_quantity"], 4, '.', ''),
           ':kode_olah' => $kode_olah,
           ':kode_internal' => $newFormula["component_internal_code"],
           ':is_repack' => 0,
           ':urutan' => 1,
           ':serial_manual' => $serial_manual,
           ':version' => 'old/new',
          ]
      );
      $result = $command->queryAll();
    }

    public function queryDelLFB($listIndex){

        foreach ($listIndex as $id){
            $modelLFB = LogFormulaBreakdown::find()->where(['id'=>$id])->one();

            $connection = Yii::$app->db;
            $command = $connection->createCommand("
            DELETE FROM log_formula_breakdown_split
            WHERE log_formula_breakdown_id = :id
            ",
            [':id'=> $id
           ]);
           $res1 = $command->queryAll();

            $connection = Yii::$app->db;
            $command = $connection->createCommand("
            DELETE FROM log_formula_breakdown
            WHERE kode_bb = :kode_bb and kode_bulk = :kode_bulk and formula_reference = :formula_reference and qty_batch = :qty_batch
            ",
            [
                ':kode_bb'=> $modelLFB->kode_bb,
                ':kode_bulk'=> $modelLFB->kode_bulk,
                ':formula_reference'=> $modelLFB->formula_reference,
                ':qty_batch'=> $modelLFB->qty_batch,
           ]);
           $res1 = $command->queryAll();
        }

    }

    public function queryDelLFBSplit($kode_bulk, $reference, $qty_batch){
        $connection = Yii::$app->db;
        $command = $connection->createCommand("
        DELETE FROM log_formula_breakdown_split
        WHERE kode_bulk = :kode_bulk AND
        formula_reference::int = :reference AND qty_batch = :qty_batch
        ",
        [':kode_bulk'=> $kode_bulk,
         ':reference'=> $reference,
         ':qty_batch' => $qty_batch,
       ]);
       $res = $command->queryAll();
    }

    public function querySelDCD($nomo){
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        SELECT *
        FROM daily_consumption_default
        WHERE  name = :nomo order by total_component_uom desc, total_component_qty asc
        ",
        [':nomo'=> $nomo,
        ]);

        $list_formula = $command->queryAll();
        return $list_formula;
    }

    public function querySelCopyDCD($kode_bulk, $reference){
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        SELECT *
        FROM daily_consumption_default
        WHERE  name LIKE :nomo AND reference = :reference order by total_component_uom desc, total_component_qty asc
        ",
        [
          ':nomo'=> '%'.$kode_bulk.'%',
          ':reference' => $reference
        ]);

        $list_formula = $command->queryAll();
        return $list_formula;
    }

    public function querySelNewLFB($kode_bulk, $reference, $qty_batch){
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        SELECT *
        FROM log_formula_breakdown
        WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference
        AND qty_batch = :qty_batch AND is_split != 99
        ",
        [':kode_bulk'=> $kode_bulk,
         ':reference'=> $reference,
         ':qty_batch' => $qty_batch
        ]);

        $existBom = $command->queryAll();
        return $existBom;
    }

    public function querySelOldLFB($kode_bulk, $reference, $qty_batch){
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        SELECT *
        FROM log_formula_breakdown
        WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference
        AND qty_batch = :qty_batch AND (version is NULL or version = 'old/new')
        and is_split < 99 ORDER BY uom DESC, qty ASC
        ",
        [':kode_bulk'=> $kode_bulk,
         ':reference'=> $reference,
         ':qty_batch' => $qty_batch
        ]);

        $existBom = $command->queryAll();
        return $existBom;
    }

    public function querySelLFBParentOnly($kode_bulk, $reference, $qty_batch){
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        SELECT *
        FROM log_formula_breakdown
        WHERE nama_bb not like '%[%]%' AND kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch ORDER BY uom DESC, qty ASC
        ",
        [
          ':kode_bulk'=> $kode_bulk,
          ':reference'=> $reference,
          ':qty_batch' => $qty_batch
        ]);

        $existBom = $command->queryAll();
        return $existBom;
    }

    public function querySelLFBSplitOnly($parent){
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        SELECT *
        FROM log_formula_breakdown
        WHERE kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch
        AND is_split = 99 AND kode_bb = :kode_bb
        ORDER BY id ASC
        ",
        [
          ':kode_bulk'=> $parent['kode_bulk'],
          ':kode_bb'=> $parent['kode_bb'],
          ':reference'=> $parent['formula_reference'],
          ':qty_batch' => $parent['qty_batch']
        ]);

        $existBom = $command->queryAll();
        return $existBom;
    }

    public function querySelLFBSplit($component){
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        SELECT *
        FROM log_formula_breakdown_split
        WHERE  log_formula_breakdown_id=".$component['id']." ORDER BY id ASC
        ");

        $split = $command->queryAll();
        return $split;
    }

    public function querySelLFBSplitAll($component){
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        SELECT *
        FROM log_formula_breakdown_split
        WHERE kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch AND log_formula_breakdown_id = :log_formula_breakdown_id
        ORDER BY id ASC
        ",[
          ":kode_bulk" => $component['kode_bulk'],
          ":reference" => $component['formula_reference'],
          ":qty_batch" => $component['qty_batch'],
          ":log_formula_breakdown_id" => $component['id']
        ]);

        $split = $command->queryAll();
        return $split;
    }

    public function queryUpdSerialManual($kode_bulk, $reference, $qty_batch){
        $connection = Yii::$app->db;
        //query select parent only
        $lfb = $this->querySelLFBParentOnly($kode_bulk, $reference, $qty_batch);
        // printf(count($lfb).'<br>');

        for ($x = 0; $x < count($lfb); $x++) {
            if ($lfb[$x]['kode_bb'] == 'AIR-RO--L'){
                $command = $connection->createCommand("

                UPDATE log_formula_breakdown
                SET urutan = 1, serial_manual = :serial_manual, version = 'old/new', kode_olah = '-', kode_internal = 'SLV1-AAD'
                WHERE id = :id;
                ",
                [':serial_manual' => $x+1,
                 ':id'=> $lfb[$x]['id'],
                ]);

            }else{
                $command = $connection->createCommand("

                UPDATE log_formula_breakdown
                SET urutan = 1, serial_manual = :serial_manual, version = 'old/new'
                WHERE id = :id;
                ",
                [':serial_manual' => $x+1,
                 ':id'=> $lfb[$x]['id'],
                ]);

            }


            $result = $command->queryAll();
            if ($lfb[$x]['is_split'] > 0 && $lfb[$x]['is_split'] < 99) {
                $listSplit = $this->querySelLFBSplitOnly($lfb[$x]);
                // printf(count($listSplit).'<br>');
                // printf($lfb[$x]['kode_bb'].'<br>');
                for ($i=0; $i < count($listSplit); $i++) {
                    $query = $connection->createCommand("

                    UPDATE log_formula_breakdown
                    SET nama_bb = :nama_bb, urutan = :urutan, serial_manual = :serial_manual, version = 'new'
                    WHERE id = :id;
                    ",
                    [
                      ':urutan' => ($i+1),
                      ':nama_bb'=> $lfb[$x]['nama_bb'].' ['.($i+1).'/'.count($listSplit).']',
                      ':serial_manual' => ($x+1).".".($i+1),
                      ':id'=> $listSplit[$i]['id'],
                    ]);

                    $result = $query->queryAll();
                }
            }
        }
    }

    public function queryUpdNamaBB($id,$nama_bb){
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        UPDATE log_formula_breakdown
        SET nama_bb = :nama_bb
        WHERE id = :id;
        ",
        [':nama_bb' => $nama_bb,
         ':id'=> $id,
        ]);

        $result = $command->queryAll();
    }

    public function queryUpdLastUpdate($kode_bulk, $reference, $qty_batch){
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        UPDATE log_synchronize
        SET last_update = :last_update
        WHERE kode_bulk = :kode_bulk AND formula_reference = :reference
        AND qty_batch = :qty_batch;
        ",
        [
          ':kode_bulk' => $kode_bulk,
          ':reference'=> $reference,
          ':qty_batch'=> $qty_batch,
          ':last_update'=> date("Y-m-d H:i:s"),
        ]);

        $result = $command->queryAll();
    }

    public function queryInsLastUpdate($kode_bulk, $reference, $qty_batch){
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        INSERT INTO log_synchronize
        (kode_bulk,formula_reference,qty_batch,last_update)
        VALUES (:kode_bulk, :reference, :qty_batch, :last_update);
        ",
        [
          ':kode_bulk' => $kode_bulk,
          ':reference'=> $reference,
          ':qty_batch'=> $qty_batch,
          ':last_update'=> date("Y-m-d H:i:s"),
        ]);

        $result = $command->queryAll();
    }

    public function queryDelLastUpdate($kode_bulk, $reference, $qty_batch){
      $connection = Yii::$app->db;
      $command = $connection->createCommand("
      DELETE FROM log_synchronize
      WHERE kode_bulk = :kode_bulk AND
      formula_reference = :reference AND qty_batch = :qty_batch
      ",
      [':kode_bulk'=> $kode_bulk,
       ':reference'=> $reference,
       ':qty_batch'=> $qty_batch,
     ]);
     $res = $command->queryAll();
     return $res;
    }

    public function cekLogSynchronize($kode_bulk, $reference, $qty_batch){
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

        SELECT * FROM log_synchronize
        WHERE kode_bulk = :kode_bulk AND formula_reference = :reference
        AND qty_batch = :qty_batch;
        ",
        [':kode_bulk' => $kode_bulk,
         ':reference'=> $reference,
         ':qty_batch'=> $qty_batch,
        ]);

        $result = $command->queryAll();
        return $result;
    }

    public function autosyncBOMwBR($version,$kode_bulk,$nomo,$reference,$reference_copy,$qty_batch){
        $cekSync = $this->cekLogSynchronize($kode_bulk,$reference,$qty_batch);
        if($reference_copy != ''){
            if (count($cekSync) == 0) {
                print_r('Copy Reference Insert<br>');
                $this->comparingBRwBOM($version,$kode_bulk,$nomo,$reference,$qty_batch);
                $this->comparingBOMwBR($version,$kode_bulk,$nomo,$reference,$qty_batch);
                $this->queryUpdSerialManual($kode_bulk, $reference, $qty_batch);
                $this->queryInsLastUpdate($kode_bulk, $reference, $qty_batch);
            }else{
                print_r('Copy Reference Update <br>');
                $this->comparingBRwBOM($version,$kode_bulk,$nomo,$reference,$qty_batch);
                $this->comparingBOMwBR($version,$kode_bulk,$nomo,$reference,$qty_batch);
                $this->queryUpdSerialManual($kode_bulk, $reference, $qty_batch);
                $this->queryUpdLastUpdate($kode_bulk, $reference, $qty_batch);
            }
        }else{
            if(count($cekSync) == 0){
                // print_r("Not Copy Reference But Synchronizing Insert<br>");
                $this->comparingBRwBOM($version,$kode_bulk,$nomo,$reference,$qty_batch);
                $this->comparingBOMwBR($version,$kode_bulk,$nomo,$reference,$qty_batch);
                $this->queryUpdSerialManual($kode_bulk, $reference, $qty_batch);
                $this->queryInsLastUpdate($kode_bulk, $reference, $qty_batch);
            }else if((count($cekSync) > 0 && date("Y-m-d", strtotime($cekSync[0]['last_update']))
 != date("Y-m-d"))){
                // print_r("Not Copy Reference But Synchronizing Update<br>");
                $this->comparingBRwBOM($version,$kode_bulk,$nomo,$reference,$qty_batch);
                $this->comparingBOMwBR($version,$kode_bulk,$nomo,$reference,$qty_batch);
                $this->queryUpdSerialManual($kode_bulk, $reference, $qty_batch);
                $this->queryUpdLastUpdate($kode_bulk, $reference, $qty_batch);
            }else{
                // print_r("Not Synchronizing");
            }
        }

        // $this->queryUpdSerialManual($kode_bulk, $reference, $qty_batch);
    }

    public function actionResetSplit($parent){
        $connection = Yii::$app->db;
        $command = $connection->createCommand("
        DELETE FROM log_formula_breakdown
        WHERE kode_bulk = :kode_bulk AND
        formula_reference = :reference AND qty_batch = :qty_batch AND
        kode_bb = :kode_bb AND is_split = 99
        ",
        [':kode_bulk'=> $parent['kode_bulk'],
         ':reference'=> $parent['formula_reference'],
         ':qty_batch'=> $parent['qty_batch'],
         ':kode_bb' => $parent['kode_bb']
       ]);
       $res = $command->queryAll();

       if (strpos(strtoupper($parent['keterangan']), "REPACK") !== false) {
         $query = $connection->createCommand("
         UPDATE log_formula_breakdown
         SET is_split = 0, is_repack = 1
         WHERE id = ".$parent['id'].";
         ");
         $result = $query->queryAll();
       }else{
         $query = $connection->createCommand("
         UPDATE log_formula_breakdown
         SET is_split = 0
         WHERE id = ".$parent['id'].";
         ");
         $result = $query->queryAll();
       }
    }

    public function actionDeleteLogSync($nomo, $reference, $qty_batch){
      $kode_bulk = strtok($nomo, '/');
      $kode_bulk = preg_replace("/MO-/","",$kode_bulk,1);
      $res = $this->queryDelLastUpdate($kode_bulk, $reference, $qty_batch);
      if ($res) {
        return 1;
      }else{
        return 0;
      }
    }

    public function queryInsSplitLFB(){
      $connection3 = Yii::$app->db;
      $command3 = $connection3->createCommand("
          INSERT INTO log_formula_breakdown (kode_bulk,nama_fg,formula_reference,lokasi,scheduled_start,week,kode_bb,qty,uom,timbangan,nama_line,nama_bb,is_split,operator,siklus,qty_batch,kode_olah,kode_internal,is_repack,urutan,serial_manual,action,keterangan,version)
          SELECT :kode_bulk as kode_bulk, :nama_fg as nama_fg, :formula_reference as formula_reference, :lokasi as lokasi, :scheduled_start as scheduled_start, :week as week, :kode_bb as kode_bb, :qty as qty, :uom as uom, :timbangan as timbangan, :nama_line as nama_line, :nama_bb as nama_bb, :is_split as is_split, :operator as operator, :siklus as siklus, :qty_batch as qty_batch, :kode_olah as kode_olah, :kode_internal as kode_internal, :is_repack as is_repack, :urutan as urutan, :serial_manual as serial_manual, :action as action, :keterangan as keterangan, :version as version
          ",
          [':kode_bulk' => $existBom[$x-1]['kode_bulk'],
           ':nama_fg' => $existBom[$x-1]['nama_fg'],
           ':formula_reference' => $existBom[$x-1]['formula_reference'],
           ':lokasi' => $existBom[$x-1]['lokasi'],
           ':scheduled_start' => $existBom[$x-1]['scheduled_start'],
           ':week' => $existBom[$x-1]['week'],
           ':kode_bb' => $existBom[$x-1]['kode_bb'],
           ':qty' => number_format((float)$split[$y-1]['qty'], 5, '.', ''),
           ':uom' => $split[$y-1]['uom'],
           ':timbangan' => $split[$y-1]['timbangan'],
           ':nama_line' => $existBom[$x-1]['nama_line'],
           ':nama_bb' => $existBom[$x-1]['nama_bb']." [".$y."/".count($split)."]",
           ':is_split' => 99,
           ':operator' => $split[$y-1]['operator'],
           ':siklus' => $split[$y-1]['siklus'],
           ':qty_batch' => number_format((float)$existBom[$x-1]['qty_batch'], 4, '.', ''),
           ':kode_olah' => $split[$y-1]['kode_olah'],
           ':kode_internal' => $existBom[$x-1]['kode_internal'],
           ':is_repack' => $existBom[$x-1]['is_repack'],
           ':urutan' =>$existBom[$x-1]['is_split'],
           ':serial_manual' => $x.".".$y,
           ':action' => $existBom[$x-1]['action'],
           ':keterangan' => $split[$y-1]['keterangan'],
           ':version' =>  'new',
          ]
      );
      $result3 = $command3->queryAll();
    }

    public function copySplitToLFB($parent,$x){
        $listItem = $this->querySelLFBSplit($parent);
        for ($i=0; $i < count($listItem); $i++) {
            $connection3 = Yii::$app->db;
            if (strpos(strtoupper($listItem[$i]['keterangan']), "REPACK") !== false) {
              $isRepack = 1;
            }else{
              $isRepack = 0;
            }
            $command3 = $connection3->createCommand("
                INSERT INTO log_formula_breakdown (kode_bulk,nama_fg,formula_reference,lokasi,scheduled_start,week,kode_bb,qty,uom,timbangan,nama_line,nama_bb,is_split,operator,siklus,qty_batch,kode_olah,kode_internal,is_repack,urutan,serial_manual,action,keterangan,version)
                SELECT :kode_bulk as kode_bulk, :nama_fg as nama_fg, :formula_reference as formula_reference, :lokasi as lokasi, :scheduled_start as scheduled_start, :week as week, :kode_bb as kode_bb, :qty as qty, :uom as uom, :timbangan as timbangan, :nama_line as nama_line, :nama_bb as nama_bb, :is_split as is_split, :operator as operator, :siklus as siklus, :qty_batch as qty_batch, :kode_olah as kode_olah, :kode_internal as kode_internal, :is_repack as is_repack, :urutan as urutan, :serial_manual as serial_manual, :action as action, :keterangan as keterangan, :version as version
                ",
                [':kode_bulk' => $parent['kode_bulk'],
                 ':nama_fg' => $parent['nama_fg'],
                 ':formula_reference' => $parent['formula_reference'],
                 ':lokasi' => $parent['lokasi'],
                 ':scheduled_start' => $parent['scheduled_start'],
                 ':week' => $parent['week'],
                 ':kode_bb' => $parent['kode_bb'],
                 ':qty' => number_format((float)$listItem[$i]['qty'], 5, '.', ''),
                 ':uom' => $listItem[$i]['uom'],
                 ':timbangan' => $listItem[$i]['timbangan'],
                 ':nama_line' => $parent['nama_line'],
                 ':nama_bb' => $parent['nama_bb']." [".$i."/".count($listItem)."]",
                 ':is_split' => 99,
                 ':operator' => $listItem[$i]['operator'],
                 ':siklus' => $listItem[$i]['siklus'],
                 ':qty_batch' => number_format((float)$parent['qty_batch'], 4, '.', ''),
                 ':kode_olah' => $listItem[$i]['kode_olah'],
                 ':kode_internal' => $parent['kode_internal'],
                 ':is_repack' => $isRepack,
                 ':urutan' =>$parent['is_split'],
                 ':serial_manual' => ($x+1).".".($i+1),
                 ':action' => $parent['action'],
                 ':keterangan' => $listItem[$i]['keterangan'],
                 ':version' =>  'new',
                ]
            );
            $result3 = $command3->queryAll();
        }
        $connection = Yii::$app->db;
        $query = $connection->createCommand("
        UPDATE log_formula_breakdown
        SET is_split = ".count($listItem)."
        WHERE id = ".$parent['id'].";
        ");
        $result = $query->queryAll();
    }

    public function actionFormBatchRecord($nomo,$reference,$reference_copy=null,$qty_batch,$pos)
    {
        $kode_bulk = strtok($nomo, '/');
        // $kode_bulk = str_replace("MO-","",$kode_bulk);
        $kode_bulk = preg_replace("/MO-/","",$kode_bulk,1);

        $searchModelVerification = new LogVerifiedBrSearch();
        $verificationHistory = $searchModelVerification->search(Yii::$app->request->queryParams,$nomo);

        $pic = Yii::$app->user->identity->username;

        $model = new LogFormulaBreakdown();

        $this->layout = '//main-sidebar-collapse';

        date_default_timezone_set('Asia/Jakarta');
        $ddate = date("Y-m-d");
        $duedt = explode("-", $ddate);
        $date  = mktime(0, 0, 0, $duedt[1], $duedt[2], $duedt[0]);
        $week  = (int)date('W', $date);

        $list_formula = $this->querySelDCD($nomo);
        $replace_arr = ['bulk ','Bulk ','BULK ',' bulk',' Bulk',' BULK'];
        if (strpos(strtolower($list_formula[0]['bulk_name']),'bulk')!==false){
            $nama_bulk = str_replace($replace_arr,'',$list_formula[0]['bulk_name']);
        }else{
            $nama_bulk = $list_formula[0]['bulk_name'];
        }
        $existBom = $this->querySelNewLFB($kode_bulk,$reference,$qty_batch);
        if (count($existBom) > 0) {
          $this->autosyncBOMwBR('new',$kode_bulk,$nomo,$reference,$reference_copy,$qty_batch);
        }

        $sql_fim="
            SELECT
                *
            FROM flow_input_mo
            WHERE nomo = '".$nomo."' AND posisi = 'PENIMBANGAN'";

        $fim = ScmPlanner::findBySql($sql_fim)->all();
        if (empty($fim)){
            $status_timbang = 'BELUM DITIMBANG';
        } else {
            $status_timbang = 'SUDAH / SEDANG DITIMBANG';
        }

        $sqll="
            SELECT
                sediaan,nama_fg,line_timbang
            FROM scm_planner
            WHERE nomo = '".$nomo."'";

        $sp = ScmPlanner::findBySql($sqll)->one();

        if (!empty($sp->line_timbang)){
            if (strpos(strtolower($sp->line_timbang),'lw') !== false){
                $sediaan = 'L';
            }else if (strpos(strtolower($sp->line_timbang),'pw') !== false){
                $sediaan = 'P';
            }else if (strpos(strtolower($sp->line_timbang),'sw') !== false){
                $sediaan = 'S';
            }else if (strpos(strtolower($sp->line_timbang),'vw') !== false){
                $sediaan = 'V';
            }else{
                $sediaan = $sp->sediaan;
            }

        }else{
            $sediaan = $sp->sediaan;

        }


        $is_split = 0;

        $siklus = 1;

        $sql_fim="
            SELECT
                *
            FROM flow_input_mo
            WHERE nomo = '".$nomo."' AND posisi = 'PENIMBANGAN'";

        $fim = ScmPlanner::findBySql($sql_fim)->all();
        if (empty($fim)){
            $status_timbang = 'BELUM DITIMBANG';
        } else {
            $status_timbang = 'SUDAH / SEDANG DITIMBANG';
        }

        $connection3 = Yii::$app->db;
        $command3 = $connection3->createCommand("

        SELECT *
        FROM log_br_detail
        WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch
        ",
        [':kode_bulk'=> $kode_bulk,
         ':reference'=> $reference,
         ':qty_batch' => $qty_batch
        ]);

        $existBrDetail = $command3->queryAll();

        $connection4 = Yii::$app->db;
        $command4 = $connection4->createCommand("

        SELECT *
        FROM log_approval_br
        WHERE  nomo = :nomo
        ",
        [':nomo'=> $nomo,
        ]);

        $existLogApproval = $command4->queryAll();


        $qty_batch = $list_formula[0]['scheduled_quantity'];


        if(strpos(strtolower($list_formula[0]['fg_name']), 'wardah') !== false){
            $brand = 'Wardah';
        } else if(strpos(strtolower($list_formula[0]['fg_name']), 'make') !== false){
            $brand = 'Make Over';
        } else if(strpos(strtolower($list_formula[0]['fg_name']), 'emina') !== false){
            $brand = 'Emina';
        } else if(strpos(strtolower($list_formula[0]['fg_name']), 'kahf') !== false){
            $brand = 'Kahf';
        }  else if(strpos(strtolower($list_formula[0]['fg_name']), 'omg') !== false){
            $brand = 'OMG';
        }  else if(strpos(strtolower($list_formula[0]['fg_name']), 'putri') !== false){
            $brand = 'Putri';
        }  else if(strpos(strtolower($list_formula[0]['fg_name']), 'biodef') !== false){
            $brand = 'Biodef';
        } else {
            if(strpos(strtolower($sp->nama_fg), 'wardah') !== false){
                $brand = 'Wardah';
            } else if(strpos(strtolower($sp->nama_fg), 'make') !== false){
                $brand = 'Make Over';
            } else if(strpos(strtolower($sp->nama_fg), 'emina') !== false){
                $brand = 'Emina';
            } else if(strpos(strtolower($sp->nama_fg), 'kahf') !== false){
                $brand = 'Kahf';
            }  else if(strpos(strtolower($sp->nama_fg), 'omg') !== false){
                $brand = 'OMG';
            }  else if(strpos(strtolower($sp->nama_fg), 'putri') !== false){
                $brand = 'Putri';
            }  else if(strpos(strtolower($sp->nama_fg), 'biodef') !== false){
                $brand = 'Biodef';
            } else {
                $brand = '';
            }
        }

        if (empty($existBrDetail))
        {
            $connection_ = Yii::$app->db;
            $command_ = $connection_->createCommand("
                INSERT INTO log_br_detail (kode_bulk,formula_reference, qty_batch, brand, sediaan,no_smb,nama_bulk)
                SELECT :kode_bulk as kode_bulk, :formula_reference as formula_reference, :qty_batch as qty_batch, :brand as brand, :sediaan as sediaan, :no_smb as no_smb, :nama_bulk as nama_bulk
                ",
                [
                  ':kode_bulk' => $kode_bulk,
                 ':formula_reference' => $list_formula[0]["reference"],
                 ':qty_batch' => $list_formula[0]["scheduled_quantity"],
                 ':brand' => $brand,
                 ':sediaan' => $sp->sediaan,
                 ':no_smb' => $list_formula[0]["no_smb"],
                 ':nama_bulk' => $nama_bulk,
                ]

            );
            $result_ = $command_->queryAll();
        }

        if (empty($existLogApproval))
        {
            $sql = "SELECT
                        lab.*,
                        lbd.is_can_send
                    FROM log_br_detail lbd
                    JOIN (select * from log_approval_br where approver is not null and reviewer is not null order by id desc) lab
                        ON lbd.kode_bulk = lab.kode_bulk and lbd.formula_reference = lab.formula_reference and lbd.qty_batch = lab.qty_batch
                    WHERE lbd.is_can_send = 1 and lbd.kode_bulk = :kode_bulk and lbd.formula_reference = :formula_reference and lbd.qty_batch = :qty_batch
                    ORDER BY lab.id desc limit 1";
            $history_approval = LogApprovalBr::findBySql($sql,[':kode_bulk'=>$kode_bulk,':formula_reference'=>$reference,':qty_batch'=>$qty_batch])->one();

            if(!empty($history_approval)){
                $reviewer = $history_approval->reviewer;
                $approver = $history_approval->approver;
            }else{
                $reviewer = null;
                $approver = null;
            }

            $connection_2 = Yii::$app->db;
            $command_2 = $connection_2->createCommand("
                    INSERT INTO log_approval_br (nomo, kode_bulk, formula_reference, qty_batch, creator, reviewer, approver, status_creator, timestamp_creator, current_pic, current_status,nama_bulk)
                    SELECT :nomo as nomo, :kode_bulk as kode_bulk, :formula_reference as formula_reference, :qty_batch as qty_batch, :creator as creator, :reviewer as reviewer, :approver as approver, :status_creator as status_creator, :timestamp_creator as timestamp_creator, :current_pic as current_pic, :current_status as current_status, :nama_bulk as nama_bulk
                    ",
                    [':nomo' => $nomo,
                     ':kode_bulk' => $kode_bulk,
                     ':formula_reference' => $list_formula[0]["reference"],
                     ':qty_batch' => $list_formula[0]["scheduled_quantity"],
                     ':creator' => Yii::$app->user->identity->authKey,
                     ':reviewer' => $reviewer,
                     ':approver' => $approver,
                     ':status_creator' => 'DRAFT',
                     ':timestamp_creator' => date('Y-m-d h:i A'),
                     ':current_pic' => 'CREATOR',
                     ':current_status' => 'DRAFT',
                     ':nama_bulk' => $nama_bulk
                    ]
                );
            $result_2 = $command_2->queryAll();

        }else{
            //UPDATE log_approval_br jika ada perubahan data reference atau qty batch di odoo MRP
            $sql = "UPDATE log_approval_br set formula_reference=:reference, qty_batch=:qty_batch WHERE nomo=:nomo";
            $update = Yii::$app->db->createCommand($sql,[':reference'=>$list_formula[0]['reference'],':qty_batch'=>$list_formula[0]['scheduled_quantity'],':nomo'=>$nomo])->execute();
        }

        if (empty($existBom)){

            $sql_line="
                SELECT
                    *
                FROM log_jadwal_timbang_rm_new
                WHERE nomo = '".$nomo."'";

            $row_line = LogJadwalTimbangRmNew::findBySql($sql_line)->one();

            if (empty($row_line)){
                $line_timbang = 'Pilih Line';
            } else {
                $line_timbang = $row_line->nama_line;
            }

            $serial = 1;

            foreach ($list_formula as $lists) {

                if ($lists["component_code"]=="AIR-RO--L"){
                    $nama_bb = "Air R/O";
                    $operator = 11;
                    $timbangan = "RO 01";
                    $kode_olah = "-";
                    $kode_internal = 'SLV1-AAD';
                } else {
                    $nama_bb = $lists["component"];
                    $operator = NULL;
                    $timbangan = NULL;
                    $kode_olah = NULL;
                    $kode_internal = $lists["component_internal_code"];
                }

                $connection2 = Yii::$app->db;
                $command2 = $connection2->createCommand("
                    INSERT INTO log_formula_breakdown (kode_bulk,nama_fg,formula_reference,lokasi,scheduled_start,week,kode_bb,qty,uom,timbangan,nama_line,nama_bb,is_split,operator,siklus,qty_batch,kode_olah,kode_internal,is_repack,urutan, serial_manual,version)
                    SELECT :kode_bulk as kode_bulk, :nama_fg as nama_fg, :formula_reference as formula_reference, :lokasi as lokasi, :scheduled_start as scheduled_start, :week as week, :kode_bb as kode_bb, :qty as qty, :uom as uom, :timbangan as timbangan, :nama_line as nama_line, :nama_bb as nama_bb, :is_split as is_split, :operator as operator, :siklus as siklus, :qty_batch as qty_batch, :kode_olah as kode_olah, :kode_internal as kode_internal, :is_repack as is_repack, :urutan as urutan, :serial_manual as serial_manual, :version as version
                    ",
                    [':kode_bulk' => $kode_bulk,
                     ':nama_fg' => $lists["fg_name"],
                     ':formula_reference' => $lists["reference"],
                     ':lokasi' => $lists["location"],
                     ':scheduled_start' => $lists["scheduled_start"],
                     ':week' => $lists["week"],
                     ':kode_bb' => $lists["component_code"],
                     ':qty' => number_format((float)$lists["total_component_qty"], 5, '.', ''),
                     ':uom' => $lists["total_component_uom"],
                     ':timbangan' => $timbangan,
                     ':nama_line' => $line_timbang,
                     ':nama_bb' => $nama_bb,
                     ':is_split' => $is_split,
                     ':operator' => $operator,
                     ':siklus' => $siklus,
                     ':qty_batch' => number_format((float)$lists["scheduled_quantity"], 4, '.', ''),
                     ':kode_olah' => $kode_olah,
                     ':kode_internal' => $kode_internal,
                     ':is_repack' => 0,
                     ':urutan' => 1,
                     ':serial_manual' => $serial++,
                     ':version' => 'old/new',
                    ]
                );
                $result2 = $command2->queryAll();
            }

        } else {
            $line_timbang = $existBom[0]['nama_line'];
            $qty_batch = $existBom[0]['qty_batch']; // qty_batch dari Nomo
            // if (($sediaan == 'P') and ($qty_batch != $bom[0]['qty_batch']))

            if (empty($existBom[0]['serial_manual'])){
                for ($x = 1; $x <= count($existBom); $x++) {

                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("

                    UPDATE log_formula_breakdown
                    SET urutan = 1, serial_manual = :serial_manual, version = 'old/new'
                    WHERE id = :id;
                    ",
                    [':serial_manual' => $x,
                     ':id'=> $existBom[$x-1]['id'],
                    ]);

                    $result = $command->queryAll();

                    if ($existBom[$x-1]['is_split'] > 0){

                        $connection4 = Yii::$app->db;
                        $command4 = $connection4->createCommand("

                        SELECT *
                        FROM log_formula_breakdown_split
                        WHERE  log_formula_breakdown_id=".$existBom[$x-1]['id']." ORDER BY id ASC
                        ");

                        $split = $command4->queryAll();

                        for ($y = 1; $y <= count($split); $y++) {

                            // INSERT (NEW UI)
                            $connection3 = Yii::$app->db;
                            $command3 = $connection3->createCommand("
                                INSERT INTO log_formula_breakdown (kode_bulk,nama_fg,formula_reference,lokasi,scheduled_start,week,kode_bb,qty,uom,timbangan,nama_line,nama_bb,is_split,operator,siklus,qty_batch,kode_olah,kode_internal,is_repack,urutan,serial_manual,action,keterangan,version)
                                SELECT :kode_bulk as kode_bulk, :nama_fg as nama_fg, :formula_reference as formula_reference, :lokasi as lokasi, :scheduled_start as scheduled_start, :week as week, :kode_bb as kode_bb, :qty as qty, :uom as uom, :timbangan as timbangan, :nama_line as nama_line, :nama_bb as nama_bb, :is_split as is_split, :operator as operator, :siklus as siklus, :qty_batch as qty_batch, :kode_olah as kode_olah, :kode_internal as kode_internal, :is_repack as is_repack, :urutan as urutan, :serial_manual as serial_manual, :action as action, :keterangan as keterangan, :version as version
                                ",
                                [':kode_bulk' => $existBom[$x-1]['kode_bulk'],
                                 ':nama_fg' => $existBom[$x-1]['nama_fg'],
                                 ':formula_reference' => $existBom[$x-1]['formula_reference'],
                                 ':lokasi' => $existBom[$x-1]['lokasi'],
                                 ':scheduled_start' => $existBom[$x-1]['scheduled_start'],
                                 ':week' => $existBom[$x-1]['week'],
                                 ':kode_bb' => $existBom[$x-1]['kode_bb'],
                                 ':qty' => number_format((float)$split[$y-1]['qty'], 5, '.', ''),
                                 ':uom' => $split[$y-1]['uom'],
                                 ':timbangan' => $split[$y-1]['timbangan'],
                                 ':nama_line' => $existBom[$x-1]['nama_line'],
                                 ':nama_bb' => $existBom[$x-1]['nama_bb']." [".$y."/".count($split)."]",
                                 ':is_split' => 99,
                                 ':operator' => $split[$y-1]['operator'],
                                 ':siklus' => $split[$y-1]['siklus'],
                                 ':qty_batch' => number_format((float)$existBom[$x-1]['qty_batch'], 4, '.', ''),
                                 ':kode_olah' => $split[$y-1]['kode_olah'],
                                 ':kode_internal' => $existBom[$x-1]['kode_internal'],
                                 ':is_repack' => $existBom[$x-1]['is_repack'],
                                 ':urutan' =>$y,
                                 ':serial_manual' => $x.".".$y,
                                 ':action' => $existBom[$x-1]['action'],
                                 ':keterangan' => $split[$y-1]['keterangan'],
                                 ':version' =>  'new',
                                ]
                            );
                            $result3 = $command3->queryAll();
                        }
                    }
                }


            }//if empty $serial_manual
            else{
                // foreach ($existBom as $bom){
                //     if($bom->is_split > 0){
                //         $connection2 = Yii::$app->db;
                //         $command2 = $connection2->createCommand("

                //         SELECT id,urutan,serial_manual,is_split,version
                //         FROM log_formula_breakdown
                //         WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch AND serial_manual > :batas_bawah AND serial_manual < :batas_atas
                //         ",
                //         [':kode_bulk'=> $bom->kode_bulk,
                //          ':reference'=> $bom->reference,
                //          ':qty_batch' => $bom->qty_batch,
                //          ':batas_bawah'=> $bom->serial_manual,
                //          ':batas_bawah'=> $bom->serial_manual+1,
                //         ]);

                //         $list_bom_split_v2 = $command2->queryAll();

                //         $jumlah_breakdown = count($list_bom_split_v2);

                //         if ($bom->is_split>$jumlah_breakdown){
                //             //Cari breakdown paling akhir di log_formula_breakdown_split
                //             $selisih = $bom->is_split - $jumlah_breakdown;
                //             $connection2 = Yii::$app->db;
                //             $command2 = $connection2->createCommand("

                //             SELECT qty,uom,siklus,kode_olah,keterangan,operator,timbangan
                //             FROM log_formula_breakdown_split
                //             WHERE  log_formula_breakdown_id = :log_formula_breakdown_id
                //             ORDER BY id DESC LIMIT :selisih
                //             ",
                //             [':log_formula_breakdown_id'=> $bom->id,
                //              ':selisih'=> $selisih,
                //             ]);

                //             $list_bom_split_v1 = $command2->queryAll();

                //             //Insert ke log_formula_breakdown dengan urutan paling akhir, seperti tambah split bb
                //             foreach($list_bom_split_v1 as $bom_split){

                //             }
                //             $connection3 = Yii::$app->db;
                //             $command3 = $connection3->createCommand("
                //                 INSERT INTO log_formula_breakdown (kode_bulk,nama_fg,formula_reference,lokasi,scheduled_start,week,kode_bb,qty,uom,timbangan,nama_line,nama_bb,is_split,operator,siklus,qty_batch,kode_olah,kode_internal,is_repack,urutan, serial_manual,version)
                //                 SELECT :kode_bulk as kode_bulk, :nama_fg as nama_fg, :formula_reference as formula_reference, :lokasi as lokasi, :scheduled_start as scheduled_start, :week as week, :kode_bb as kode_bb, :qty as qty, :uom as uom, :timbangan as timbangan, :nama_line as nama_line, :nama_bb as nama_bb, :is_split as is_split, :operator as operator, :siklus as siklus, :qty_batch as qty_batch, :kode_olah as kode_olah, :kode_internal as kode_internal, :is_repack as is_repack, :urutan as urutan, :serial_manual as serial_manual, :version as version
                //                 ",
                //                 [':kode_bulk' => $bom->kode_bulk,
                //                  ':nama_fg' => $bom->nama_fg,
                //                  ':formula_reference' => $bom->formula_reference,
                //                  ':lokasi' => $bom->lokasi,
                //                  ':scheduled_start' => $bom->scheduled_start,
                //                  ':week' => $bom->week,
                //                  ':kode_bb' => $bom->kode_bb,
                //                  ':qty' => number_format((float)$lists["total_component_qty"], 5, '.', ''),
                //                  ':uom' => $lists["total_component_uom"],
                //                  ':timbangan' => $timbangan,
                //                  ':nama_line' => $line_timbang,
                //                  ':nama_bb' => $nama_bb,
                //                  ':is_split' => $is_split,
                //                  ':operator' => $operator,
                //                  ':siklus' => $siklus,
                //                  ':qty_batch' => number_format((float)$lists["scheduled_quantity"], 4, '.', ''),
                //                  ':kode_olah' => $kode_olah,
                //                  ':kode_internal' => $lists["component_internal_code"],
                //                  ':is_repack' => 0,
                //                  ':urutan' => 1,
                //                  ':serial_manual' => $serial++,
                //                  ':version' => 'old/new',
                //                 ]
                //             );
                //             $result3 = $command2->queryAll();
                //         }
                //     }
                // }
            }//else empty $serial_manual



            if ($qty_batch != $existBom[0]['qty_batch'])
            {
                return $this->redirect(['schedule-list']);
            }

            $this->actionUpdateBatchReal($nomo,$reference,$qty_batch);
            //Update angka_belakang_koma setiap reload page
            foreach ($existBom as $bom){
                $bom['qty_batch'] = number_format((float)$bom['qty_batch'], 4, '.', '');

                if (!empty($bom['timbangan']) && ($bom['version'] == 'old/new' || $bom['version'] == 'new')){
                    $sql_timbangan = "
                        SELECT angka_belakang_koma
                        FROM master_data_timbangan_rm
                        WHERE kode_timbangan = '".$bom['timbangan']."' " ;

                    $row_timbangan = MasterDataTimbanganRm::findBySql($sql_timbangan)->one();
                    // print_r('<pre>');
                    // print_r($row_timbangan);
                    if (!empty($row_timbangan['angka_belakang_koma'])){
                        $bom['qty'] = number_format((float)$bom['qty'], $row_timbangan['angka_belakang_koma'], '.', '');
                    } else {
                        $bom['qty'] = number_format((float)$bom['qty'], 5, '.', '');
                    }
                    $sql_update = "UPDATE log_formula_breakdown SET qty = ".$bom['qty'].", qty_batch=".$bom['qty_batch']." WHERE id = ".$bom['id']." ";
                    $update = Yii::$app->db->createCommand($sql_update)->execute();
                }else{
                    $bom['qty'] = number_format((float)$bom['qty'], 5, '.', '');
                    $sql_update = "UPDATE log_formula_breakdown SET qty = ".$bom['qty'].", qty_batch=".$bom['qty_batch']." WHERE id = ".$bom['id']." ";
                    $update = Yii::$app->db->createCommand($sql_update)->execute();
                }
            }
            // exit();
        }

        if (empty($existBom[0]['lokasi'])){
            $lokasi = "";
        } else {
            $lokasi = $existBom[0]['lokasi'];
        }

        $sql2 = "
                SELECT reviewer,username
                FROM list_reviewer
                WHERE sediaan='".$sediaan."'
                ORDER BY reviewer ASC
                ";

        $reviewer = ListReviewer::findBySql($sql2)->all();

        $sql8 = "
                SELECT is_lock,is_can_send
                FROM log_br_detail
                WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch;

        $row_detail_br = LogBrDetail::findBySql($sql8)->one();

        if (!empty($row_detail_br)){
            $is_lock = $row_detail_br->is_lock;
            if (!empty($row_detail_br->is_can_send)){
                $is_can_send = $row_detail_br->is_can_send;
            } else {
                $is_can_send = 0;
            }
        } else {
            $is_lock = 0;
            $is_can_send = 0;

        }

        // $sql_check = "
        //         SELECT current_pic,current_status
        //         FROM log_approval_br
        //         WHERE nomo like :kode_bulk
        //         AND qty_batch = :qty_batch
        //         AND formula_reference = :reference
        //         AND current_status = :status
        //         AND current_pic = :pic";

        // $check_approval = LogApprovalBr::findBySql($sql_check,
        //                     [':kode_bulk'=>'%'.$kode_bulk.'%',
        //                     ':reference'=>$reference,
        //                     ':qty_batch'=>$qty_batch,
        //                     ':status'=>'APPROVED',
        //                     ':pic'=>'APPROVER'
        //                     ])->one();

        // if(!empty($check_approval) ){
        //     $connection = Yii::$app->getDb();
        //     $command = $connection->createCommand("

        //     UPDATE log_approval_br
        //     SET status_approver = 'APPROVED', timestamp_approver = :timestamp_approver, current_pic = 'APPROVER', current_status = 'APPROVED', log = 'approval'
        //     WHERE nomo = :nomo;
        //     ",
        //     [':nomo'=> $nomo,
        //      ':timestamp_approver'=> date('Y-m-d H:i:s')
        //     ]);

        //     $result = $command->queryAll();

        // }


        $sql7 = "
                SELECT current_pic,current_status
                FROM log_approval_br
                WHERE nomo = :nomo";

        $row_approval = LogApprovalBr::findBySql($sql7,[':nomo'=>$nomo])->one();

        // if (!empty($row_approval)){
        //     if ($row_approval->current_pic == 'APPROVER' and $row_approval->current_status == 'APPROVED'){
        //         $is_can_send = 1;
        //     } else {
        //         $is_can_send = 0;
        //     }
        // } else {
        //     $is_can_send = 0;
        // }




        $sql5 = "
                SELECT approver,username
                FROM list_approver
                ORDER BY approver ASC
                ";

        $approver = ListApprover::findBySql($sql5)->all();

        $sql_id = "
            SELECT id,nama_fg
            FROM log_formula_breakdown
            WHERE kode_bulk='".$kode_bulk."' and formula_reference='".$reference."' and qty_batch = ".$qty_batch." order by id asc" ;

        $idBom = LogFormulaBreakdown::findBySql($sql_id)->one();

        $sql7 = "
                SELECT distinct(line)
                FROM master_data_timbangan_rm
                WHERE sediaan='".$sediaan."'
                ORDER BY line ASC
                ";

        $lines = MasterDataTimbanganRm::findBySql($sql7)->all();

        $searchModelDetailBr = new LogBrDetailSearch();
        // $dataProvider = $searchModel->searchBr(Yii::$app->request->queryParams,$kode_bulk,$reference,$qty_batch);
        $detailBr = $searchModelDetailBr->search(Yii::$app->request->queryParams);
        $detailBr->query->where("kode_bulk='".$kode_bulk."' and formula_reference='".$reference."' and qty_batch = ".$qty_batch);

        $searchModelApproval = new LogApprovalBrSearch();
        // $dataProvider = $searchModel->searchBr(Yii::$app->request->queryParams,$kode_bulk,$reference,$qty_batch);
        $approvalBr = $searchModelApproval->search(Yii::$app->request->queryParams);
        $approvalBr->query->where("nomo='".$nomo."'");

        // $sql = "SELECT *
        //         FROM log_formula_breakdown WHERE kode_bulk = :kode_bulk and formula_reference = :formula_reference and qty_batch = :qty_batch
        //         ORDER BY uom desc, cast(split_part(cast(serial_manual as text),'.',1) as int) asc, id asc
        //         ";
        // $data = LogFormulaBreakdown::findBySql($sql,[':kode_bulk'=>$kode_bulk,':formula_reference'=>$reference,':qty_batch'=>$qty_batch])->all();

        // $dataProvider= new ArrayDataProvider([
        //     'allModels'=>$data,
        //     'pagination'=>false,
        // ]);

        // $searchModel = new LogFormulaBreakdownSearch();
        // $searchModel = new \app\models\LogFormulaBreakdown();
        if (Yii::$app->request->post('hasEditable')) {

            $attribute = Yii::$app->request->post('editableAttribute');
            $id = Yii::$app->request->post('editableKey');

            $out = Json::encode(['output' => '', 'message' => '']);
            $post = [];

            if ($attribute == 'status_formula' OR $attribute == 'no_dokumen' OR $attribute == 'no_revisi' OR $attribute == 'status_jurnal' OR $attribute == 'bentuk_sediaan' OR $attribute == 'brand'){
                $model_detail_br = LogBrDetail::findOne($id);
                $posted = current($_POST['LogBrDetail']);
                $post['LogBrDetail'] = $posted;

                if ($model_detail_br->load($post)) {
                    if ($attribute == 'no_dokumen'){

                        $sql = "UPDATE log_formula_breakdown SET no_dokumen = '".$model_detail_br->no_dokumen."' WHERE kode_bulk='".$kode_bulk."' AND formula_reference='".$reference."' AND qty_batch = ".$qty_batch."  ";
                        $update = Yii::$app->db->createCommand($sql)->execute();

                    }
                    else if ($attribute == 'no_revisi'){
                        $sql = "UPDATE log_formula_breakdown SET no_revisi = '".$model_detail_br->no_revisi."' WHERE kode_bulk='".$kode_bulk."' AND formula_reference='".$reference."' AND qty_batch = ".$qty_batch."  ";
                        $update = Yii::$app->db->createCommand($sql)->execute();
                    }

                    $model_detail_br->save();
                }
            } else if ($attribute == 'reviewer' OR $attribute == 'approver'  OR $attribute == 'reject_note_reviewer'  OR $attribute == 'reject_note_approver'){
                $model_approval = LogApprovalBr::findOne($id);
                $posted = current($_POST['LogApprovalBr']);
                $post['LogApprovalBr'] = $posted;

                if ($model_approval->load($post)) {
                    $model_approval->save();
                }
            } else {
                $model_bom = LogFormulaBreakdown::findOne($id);
                $posted = current($_POST['LogFormulaBreakdown']);
                $post['LogFormulaBreakdown'] = $posted;

                if ($model_bom->load($post)) {

                    if ($attribute == 'operator'){
                        if ($model_bom->is_repack == 1 and strpos($model_bom->nama_line,'LW')!==false){
                            $sql_timbangan = "
                                SELECT kode_timbangan,angka_belakang_koma
                                FROM master_data_timbangan_rm
                                WHERE line = 'LWE03' and uom = '".$model_bom->uom."' and operator = ".$model_bom->operator." order by id asc" ;
                        }else{
                            $sql_timbangan = "
                                SELECT kode_timbangan,angka_belakang_koma
                                FROM master_data_timbangan_rm
                                WHERE line = '".$model_bom->nama_line."' and uom = '".$model_bom->uom."' and operator = ".$model_bom->operator." order by id asc" ;
                        }


                        $row_timbangan = MasterDataTimbanganRm::findBySql($sql_timbangan)->all();
                        if (count($row_timbangan) == 1){
                            $model_bom->timbangan = $row_timbangan[0]['kode_timbangan'];
                            $model_bom->qty = number_format((float)$model_bom->qty,$row_timbangan[0]['angka_belakang_koma'], '.', '');
                        } else {
                            $model_bom->timbangan = NULL;
                            $model_bom->qty = number_format((float)$model_bom->qty,5,'.','');
                        }


                        $model_bom->save();

                    } else if ($attribute == 'uom') {
                        if (!empty($model_bom->operator) ){
                            if ($model_bom->is_repack == 1 and strpos($model_bom->nama_line,'LW')!==false){
                                $sql_timbangan = "
                                    SELECT kode_timbangan,angka_belakang_koma
                                    FROM master_data_timbangan_rm
                                    WHERE line = 'LWE03' and uom = '".$model_bom->uom."' and operator = ".$model_bom->operator." order by kode_timbangan asc" ;
                            }else{
                                $sql_timbangan = "
                                    SELECT kode_timbangan,angka_belakang_koma
                                    FROM master_data_timbangan_rm
                                    WHERE line = '".$model_bom->nama_line."' and uom = '".$model_bom->uom."' and operator = ".$model_bom->operator." order by kode_timbangan asc" ;
                            }


                            $row_timbangan = MasterDataTimbanganRm::findBySql($sql_timbangan)->all();
                            if (count($row_timbangan) == 1){
                                $model_bom->timbangan = $row_timbangan[0]['kode_timbangan'];
                                $model_bom->qty = number_format((float)$model_bom->qty,$row_timbangan[0]['angka_belakang_koma'],'.','');
                            } else {
                                $model_bom->timbangan = NULL;
                                $model_bom->qty = number_format((float)$model_bom->qty,5,'.','');
                            }
                        }

                        $model_bom->save();
                    } else if ($attribute == 'timbangan') {
                        $sql_timbangan = "
                            SELECT angka_belakang_koma
                            FROM master_data_timbangan_rm
                            WHERE kode_timbangan = '".$model_bom->timbangan."' " ;

                        $row_timbangan = MasterDataTimbanganRm::findBySql($sql_timbangan)->one();
                        if (!empty($row_timbangan['angka_belakang_koma'])){
                            $model_bom->qty = number_format((float)$model_bom->qty,$row_timbangan['angka_belakang_koma'],'.','');
                        } else {
                            $model_bom->qty = number_format((float)$model_bom->qty,5,'.','');
                        }
                        // $model_bom->timbangan = $row_timbangan->kode_timbangan;

                        $model_bom->save();
                    } else if ($attribute == 'qty') {
                        $modelLFB = new LogFormulaBreakdown();
                        $model_bom->qty = number_format((float)$model_bom->qty, $modelLFB->get_decimal($model_bom->timbangan),'.','');

                        $model_bom->save();
                    } else if ($attribute == 'action') {
                        $connection = Yii::$app->getDb();
                        $command = $connection->createCommand("

                        UPDATE log_formula_breakdown
                        SET action = :action
                        WHERE kode_bb = :kode_bb;
                        ",
                        [':action'=> $model_bom->action,
                         'kode_bb'=> $model_bom->kode_bb,
                        ]);

                        $result = $command->queryAll();

                        $model_bom->save();
                    } else if ($attribute == 'kode_olah') {
                        $sql = "
                        SELECT kode_olah
                        FROM log_formula_breakdown
                        WHERE  kode_bulk = '".$model_bom->kode_bulk."' AND formula_reference = '".$model_bom->formula_reference."' AND qty_batch = ".$model_bom->qty_batch." AND kode_bb <> '".$model_bom->kode_bb."' AND kode_olah <> '' AND kode_olah = '".$model_bom->kode_olah."'
                        ";

                        $row = LogFormulaBreakdown::findBySql($sql)->all();

                         if (empty($row)){
                            $model_bom->save();
                         } else {
                            $out = Json::encode(['output' => 'Kode Olah Tidak Boleh Sama', 'message' => 'Kode Olah Tidak Boleh Sama']);
                         }
                    } else {
                        $model_bom->save();
                    }

                    // $model_bom->save();
                }
            }

            echo $out;
            // if ($attribute == 'uom' or $attribute == 'operator' or $attribute == 'qty' or $attribute == 'action'){
            //     return $this->refresh();
            // } else {
            //     return;
            // }
            return;

        }

        $this->queryUpdSerialManual($kode_bulk, $reference, $qty_batch);
        $existBom = $this->querySelNewLFB($kode_bulk,$reference,$qty_batch);

        $searchModel = new LogFormulaBreakdownSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andwhere("kode_bulk='".$kode_bulk."' and formula_reference='".$reference."' and qty_batch = ".$qty_batch." and (version = 'old/new' or version = 'new')");
        $dataProvider->query->orderby("serial_manual asc");
        $dataProvider->pagination  = false;

        return $this->render('form-batch-record', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            // 'searchModel' => $searchModel,
            'detailBr' => $detailBr,
            'week' => $week,
            // 'list_formula' => $list_formula,
            'nomo' => $nomo,
            'idBom' => $idBom->id,
            'nama_bulk' => $idBom->nama_fg,
            'model' => $model,
            'existBom' => $existBom,
            'searchModel' => $searchModel,
            'qty_batch' => $qty_batch,
            'reference' => $reference,
            // 'nomo' => $nomo,
            // 'is_split' => $is_split,
            'nama_line' => $line_timbang,
            // 'id_jadwal' => $log_timbang_array['id'],
            // 'timbangans' => $timbangans,
            'lokasi' => $lokasi,
            'kode_bulk' => $kode_bulk,
            // 'actions' => $actions,
            'pic' => $pic,
            'verificationHistory' => $verificationHistory,
            'status_timbang' => $status_timbang,
            'reviewer'=>$reviewer,
            'approver'=>$approver,
            'approvalBr' =>$approvalBr,
            'sediaan'=>$sediaan,
            'lines' => $lines,
            'pos' => $pos,
            'is_lock' => $is_lock,
            'is_can_send' => $is_can_send,
            'row_approval' => $row_approval
        ]);
    }

    public function actionUpdateBatchReal($nomo,$reference,$qty_batch)
    {
        $kode_bulk = strtok($nomo, '/');
        $kode_bulk = preg_replace("/MO-/","",$kode_bulk,1);

        //UPdate qty_batch_real setiap reload page
        $connection_gr = Yii::$app->db;
        $command_gr = $connection_gr->createCommand("

        SELECT SUM(qty)/1000 as sum
        FROM log_formula_breakdown
        WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch AND (is_split = 0 or is_split = 99) AND uom = 'g'
        ",
        [':kode_bulk'=> $kode_bulk,
         ':reference'=> $reference,
         ':qty_batch' => $qty_batch
        ]);

        $sum_gr = $command_gr->queryScalar();

        $connection_kg = Yii::$app->db;
        $command_kg = $connection_kg->createCommand("

        SELECT SUM(qty) as sum
        FROM log_formula_breakdown
        WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch AND (is_split = 0 or is_split = 99) AND uom = 'kg'
        ",
        [':kode_bulk'=> $kode_bulk,
         ':reference'=> $reference,
         ':qty_batch' => $qty_batch
        ]);

        $sum_kg = $command_kg->queryScalar();

        $qty_batch_real = $sum_gr + $sum_kg;

        $update = Yii::$app->db->createCommand("
                    UPDATE log_approval_br
                    SET qty_batch_real = :qty_batch_real
                    WHERE nomo = :nomo ",
                    [':nomo'=>$nomo,
                    ':qty_batch_real'=>number_format((float)$qty_batch_real, 4, '.', '')
                    ])->execute();
    }

    public function actionSynchronize($nomo,$reference,$qty_batch){
        $kode_bulk = strtok($nomo, '/');
        $kode_bulk = preg_replace("/MO-/","",$kode_bulk,1);
        // $itemParent = $this->querySelLFBParentOnly($kode_bulk,$reference,$qty_batch);
        $itemParent = $this->querySelNewLFB($kode_bulk,$reference,$qty_batch);
        for ($i=0; $i < count($itemParent); $i++) {
            print_r($itemParent[$i]['is_split']);
            // exit();
            if($itemParent[$i]['is_split'] > 0 && $itemParent[$i]['is_split'] < 99){
                $listItem = $this->querySelLFBSplitAll($itemParent[$i]);
                if(count($listItem) > 0){
                    $this->actionResetSplit($itemParent[$i]);
                    $this->copySplitToLFB($itemParent[$i],$i);
                }else{
                    $this->actionResetSplit($itemParent[$i]);
                }
            }
            else{
                $this->actionResetSplit($itemParent[$i]);
            }
        }
        exit();
    }

    public function actionAddSplit($id){

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

        UPDATE log_formula_breakdown
        SET is_split = is_split+1
        WHERE id = :id;
        ",
        [':id'=> $id
        ]);

        $result = $command->queryAll();

        // GET LIST TASK
        $sql = "
        SELECT *
        FROM log_formula_breakdown
        WHERE id = ".$id."
        ";

        $data = LogFormulaBreakdown::findBySql($sql)->one();

        $flagUpdate = 0;

        if($data->is_split>1){
            $nama_bb = strtok($data->nama_bb, '[');
            $flagUpdate = 1;
        } else{
            $nama_bb = $data->nama_bb;
            $flagUpdate = 0;
        }

        // INSERT (NEW UI)
        $connection2 = Yii::$app->db;
        $command2 = $connection2->createCommand("
            INSERT INTO log_formula_breakdown (kode_bulk,nama_fg,formula_reference,lokasi,scheduled_start,week,kode_bb,qty,uom,timbangan,nama_line,nama_bb,is_split,operator,siklus,qty_batch,kode_olah,kode_internal,is_repack,urutan,serial_manual,action,keterangan,version)
            SELECT :kode_bulk as kode_bulk, :nama_fg as nama_fg, :formula_reference as formula_reference, :lokasi as lokasi, :scheduled_start as scheduled_start, :week as week, :kode_bb as kode_bb, :qty as qty, :uom as uom, :timbangan as timbangan, :nama_line as nama_line, :nama_bb as nama_bb, :is_split as is_split, :operator as operator, :siklus as siklus, :qty_batch as qty_batch, :kode_olah as kode_olah, :kode_internal as kode_internal, :is_repack as is_repack, :urutan as urutan, :serial_manual as serial_manual, :action as action, :keterangan as keterangan, :version as version
            ",
            [':kode_bulk' => $data->kode_bulk,
             ':nama_fg' => $data->nama_fg,
             ':formula_reference' => $data->formula_reference,
             ':lokasi' => $data->lokasi,
             ':scheduled_start' => $data->scheduled_start,
             ':week' => $data->week,
             ':kode_bb' => $data->kode_bb,
             ':qty' => number_format("0",5,'.',''),
             ':uom' => $data->uom,
             ':timbangan' => $data->timbangan,
             ':nama_line' => $data->nama_line,
             ':nama_bb' => $nama_bb." [".$data->is_split."/".$data->is_split."]",
             ':is_split' => 99,
             ':operator' => $data->operator,
             ':siklus' => 1,
             ':qty_batch' => $data->qty_batch,
             ':kode_olah' => NULL,
             ':kode_internal' => $data->kode_internal,
             ':is_repack' => $data->is_repack,
             ':urutan' => $data->is_split,
             ':serial_manual' => $data->serial_manual.".".$data->is_split,
             ':action' => $data->action,
             ':keterangan' => $data->keterangan,
             ':version' =>  'new',
            ]
        );
        $result2 = $command2->queryAll();

        // Program update postfix urutan split di nama fg
        if ($flagUpdate){
            for ($x = 1; $x < $data->is_split; $x++) {

                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("

                UPDATE log_formula_breakdown
                SET nama_bb = :nama_bb
                WHERE kode_bulk = :kode_bulk and formula_reference = :formula_reference and qty_batch = :qty_batch and kode_bb = :kode_bb and is_split = 99 and urutan = :urutan;
                ",
                [':nama_bb' => $nama_bb." [".$x."/".$data->is_split."]",
                 ':kode_bulk'=> $data->kode_bulk,
                 ':formula_reference'=> $data->formula_reference,
                 ':qty_batch'=> $data->qty_batch,
                 ':kode_bb'=> $data->kode_bb,
                 ':urutan'=> $x,
                ]);

                $result = $command->queryAll();
            }
            $flagUpdate = 0;
        }

        // INSERT (UI LAMA) ke log_formula_breakdown_split
        $connection3 = Yii::$app->db;
        $command3 = $connection3->createCommand("
            INSERT INTO log_formula_breakdown_split (log_formula_breakdown_id,kode_bulk,qty,uom,formula_reference,siklus,operator,qty_batch)
            SELECT :log_formula_breakdown_id as log_formula_breakdown_id, :kode_bulk as kode_bulk, :qty as qty, :uom as uom, :reference as formula_reference, :siklus as siklus, :operator as operator, :qty_batch as qty_batch
            ",
            [':log_formula_breakdown_id' => $id,
             ':kode_bulk' => $data->kode_bulk,
             ':qty' => number_format("0",5,'.',''),
             ':uom' => $data->uom,
             ':reference' => $data->formula_reference,
             ':siklus' => $data->siklus,
             ':operator' => $data->operator,
             ':qty_batch' => $data->qty_batch
            ]
        );
        $result3 = $command3->queryAll();

        // $insert_id = Yii::$app->db->getLastInsertID();

        if ($result2){
        // Redirect to Downtime
            return 1;
        } else {
            return 0;
        }

        // echo Json::encode($value);
    }

    public function actionMinusSplit($id){


        // GET LIST TASK
        $sql = "
        SELECT *
        FROM log_formula_breakdown
        WHERE id = ".$id."
        ";

        $data = LogFormulaBreakdown::findBySql($sql)->one();

        // // GET BB PARENT
        $sql = "
        SELECT *
        FROM log_formula_breakdown
        WHERE kode_bulk = '".$data->kode_bulk."' and formula_reference = '".$data->formula_reference."' and qty_batch = ".$data->qty_batch." and kode_bb = '".$data->kode_bb."'and urutan = 1 and serial_manual::character varying not ilike '%.%'
        ";

        $bb_parent = LogFormulaBreakdown::findBySql($sql)->one();

        // GET LIST TASK
        $sql2 = "
        SELECT count(urutan)
        FROM log_formula_breakdown
        WHERE kode_bulk = '".$data->kode_bulk."' and formula_reference = '".$data->formula_reference."' and qty_batch = ".$data->qty_batch." and kode_bb = '".$data->kode_bb."'";

        $count = LogFormulaBreakdown::findBySql($sql2)->scalar();


        if ($count - $data->urutan == 1){
            $urutan_induk = strtok($data->serial_manual, '.');

            $connection6 = Yii::$app->db;
            $command6 = $connection6->createCommand("

            DELETE
            FROM log_formula_breakdown
            WHERE  id = :id",
            [':id'=> $id,
            ]);

            $delete = $command6->execute();

            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE log_formula_breakdown
            SET is_split = :is_split
            WHERE id = :id
            ",
            [':id'=> $bb_parent->id,
             ':is_split'=> $count-2,
            ]);

            $result = $command->queryAll();

            $counter_max = $data->urutan-1;

            $nama_bb = strtok($data->nama_bb, '[');

            if ($data->urutan > 1){
                for ($x = 1; $x < $data->urutan; $x++) {

                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("

                    UPDATE log_formula_breakdown
                    SET nama_bb = :nama_bb
                    WHERE kode_bulk = :kode_bulk and formula_reference = :formula_reference and qty_batch = :qty_batch and kode_bb = :kode_bb and is_split = 99 and urutan = :urutan;
                    ",
                    [':nama_bb' => $nama_bb." [".$x."/".$counter_max."]",
                     ':kode_bulk'=> $data->kode_bulk,
                     ':formula_reference'=> $data->formula_reference,
                     ':qty_batch'=> $data->qty_batch,
                     ':kode_bb'=> $data->kode_bb,
                     ':urutan'=> $x,
                    ]);

                    $result = $command->queryAll();
                }

            }

            //Adjust split bb in log_formula_breakdown_split
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            SELECT count(id)
            FROM log_formula_breakdown_split
            WHERE log_formula_breakdown_id = :parent_id
            ",
            [':parent_id'=> $bb_parent->id,
            ]);

            $count_split = $command->queryScalar();

            //Delete bb split yang kelebihan di log_formula_breakdown_split
            if($count_split>$counter_max){
                $limit = $count_split - $counter_max;
                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("

                DELETE
                FROM log_formula_breakdown_split
                WHERE id in (SELECT id FROM log_formula_breakdown_split WHERE log_formula_breakdown_id = ".$bb_parent->id." ORDER BY id DESC LIMIT ".$limit.")
                ");

                $delete = $command->execute();

            }


            if ($result){
                return 1;
            } else {
                return 0;
            }
        } else {
            if ($bb_parent->is_split != $count-1){
                $counter_max = $count-1;

                //Reset is_split dan breakdown yang ada
                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("
                UPDATE log_formula_breakdown
                SET is_split = :is_split
                WHERE id = :id
                ",
                [':id'=> $bb_parent->id,
                 ':is_split'=> 0,
                ]);

                $result = $command->queryAll();

                //Delete Breakdown BB yang ada
                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("
                DELETE
                FROM log_formula_breakdown
                WHERE kode_bulk = '".$data->kode_bulk."' and formula_reference = '".$data->formula_reference."' and qty_batch = ".$data->qty_batch." and kode_bb = '".$data->kode_bb."' and serial_manual::character varying ilike '%.%' ");

                $delete = $command->execute();
                return 1;
            }else{
                return 2;
            }
        }

    }

    public function actionUpdateLine2($kode_bulk,$reference,$qty_batch,$nama_line)
    {
        // GET FORMULA
        $sql = "
        SELECT *
        FROM log_formula_breakdown
        WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." order by id asc"
        ;

        $data = LogFormulaBreakdown::findBySql($sql)->all();

        for ($i=0; $i < count($data); $i++)
        {
            if (!empty($data[$i]['operator'])){
                if ($data[$i]['kode_bb']!='AIR-RO--L'){
                    $sql_timbangan = "
                        SELECT kode_timbangan
                        FROM master_data_timbangan_rm
                        WHERE line = '".$nama_line."' and uom = '".$data[$i]['uom']."' and operator = ".$data[$i]['operator']." order by id asc" ;

                    $row_timbangan = MasterDataTimbanganRm::findBySql($sql_timbangan)->all();

                    if (count($row_timbangan) == 1){
                        $connection2 = Yii::$app->db;
                        $command2 = $connection2->createCommand("

                            UPDATE log_formula_breakdown
                            SET nama_line = :nama_line,
                                timbangan = :timbangan
                            WHERE id = :id;
                            ",
                            [':id'=> $data[$i]['id'],
                             ':timbangan'=> $row_timbangan[0]['kode_timbangan'],
                             ':nama_line'=> $nama_line,
                            ]);

                        $result = $command2->queryAll();
                    } else {
                        $connection2 = Yii::$app->db;
                        $command2 = $connection2->createCommand("

                            UPDATE log_formula_breakdown
                            SET nama_line = :nama_line,
                                timbangan = :timbangan
                            WHERE id = :id;
                            ",
                            [':id'=> $data[$i]['id'],
                             ':timbangan'=> NULL,
                             ':nama_line'=> $nama_line,
                            ]);

                        $result = $command2->queryAll();
                    }

                }
            } else {
                $connection3 = Yii::$app->db;
                $command3 = $connection3->createCommand("

                    UPDATE log_formula_breakdown
                    SET nama_line = :nama_line,
                        timbangan = :timbangan
                    WHERE id = :id;
                    ",
                    [':id'=> $data[$i]['id'],
                     ':timbangan'=> NULL,
                     ':nama_line'=> $nama_line,
                    ]);

                $result = $command3->queryAll();
            }
        }

        if ($result){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionUpdateRepack($id,$value)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("

            UPDATE log_formula_breakdown
            SET is_repack = :value
            WHERE id = :id
            ",
            [':value'=> $value,
             ':id'=>$id,
            ]);

        $update = $command->queryAll();

        if ($update){
            $result = ['status'=>1,'value'=>$value];
            // echo 1;
        } else {
            $result = ['status'=>0,'value'=>$value];
            // echo 0;
        }
        echo Json::encode($result);
    }

    public function actionOpenEdit($nomo,$kode_bulk,$reference,$qty_batch)
    {
        $sql = "
        SELECT *
        FROM log_approval_br
        WHERE  nomo = '".$nomo."'
        ";

        $log = LogApprovalBr::findBySql($sql)->one();

        if ($log->current_pic=='CREATOR' or ($log->current_pic == 'APPROVER' and $log->current_status == 'APPROVED')){
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

                UPDATE log_formula_breakdown
                SET is_lock = 0
                WHERE kode_bulk = :kode_bulk AND formula_reference = :formula_reference AND qty_batch = :qty_batch
                ",
                [':kode_bulk'=> $kode_bulk,
                 ':formula_reference'=>$reference,
                 ':qty_batch'=>$qty_batch,
                ]);

            $result = $command->queryAll();

            $connection2 = Yii::$app->db;
            $command2 = $connection2->createCommand("

                UPDATE log_br_detail
                SET is_lock = 0, is_can_send = 0
                WHERE kode_bulk = :kode_bulk AND formula_reference = :formula_reference AND qty_batch = :qty_batch
                ",
                [':kode_bulk'=> $kode_bulk,
                 ':formula_reference'=>$reference,
                 ':qty_batch'=>$qty_batch,
                ]);

            $result2 = $command2->queryAll();

            $connection3 = Yii::$app->getDb();
            $command3 = $connection3->createCommand("

            UPDATE log_approval_br
            SET status_creator = 'DRAFT', timestamp_creator = :timestamp_creator, status_reviewer = NULL, timestamp_reviewer = NULL, status_approver = NULL, timestamp_approver = NULL, current_pic = 'CREATOR', current_status = 'DRAFT'
            WHERE nomo = :nomo;
            ",
            [':nomo'=> $nomo,
             ':timestamp_creator'=> date('Y-m-d H:i:s')
            ]);

            $result3 = $command3->queryAll();

            if ($result and $result2){
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 2;
        }
    }

    public function actionGetRepackStatus($id)
    {
        $sql = "
        SELECT is_repack
        FROM log_formula_breakdown
        WHERE  id = ".$id;

        $row = LogFormulaBreakdown::findBySql($sql)->one();

        if ($row->is_repack==1){
            // echo count($row);
            echo 1;
        } else {
            echo 0;
        }

    }

    public function actionGetDecimal($timbangan)
    {
        $sql = "
        SELECT angka_belakang_koma
        FROM master_data_timbangan_rm
        WHERE  kode_timbangan = '".$timbangan."'";

        $row = MasterDataTimbanganRm::findBySql($sql)->one();

        if (!empty($row)){
            // echo count($row);
            echo $row->angka_belakang_koma;
        } else {
            echo 6;
        }

    }

    public function actionGetKodeBb($id)
    {
        $sql = "
        SELECT kode_bb
        FROM log_formula_breakdown
        WHERE  id = ".$id;

        $row = LogFormulaBreakdown::findBySql($sql)->one();

        echo $row->kode_bb;

    }

    public function actionCheckDetailBr($kode_bulk,$reference,$qty_batch)
    {
        $sql = "
        SELECT *
        FROM log_br_detail
        WHERE  kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND no_dokumen is null and no_revisi is null and status_formula is null
        ";

        $row = LogBrDetail::findBySql($sql)->all();

        if (count($row)==0){
            // echo count($row);
            echo 1;
        } else {
            echo 0;
        }

    }

    public function actionCheckAngkaBelakangKoma($kode_bulk,$reference,$qty_batch){
        $connection2 = Yii::$app->db;
        $command2 = $connection2->createCommand("

        SELECT *
        FROM log_formula_breakdown
        WHERE  kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch ORDER BY uom DESC, qty ASC
        ",
        [':kode_bulk'=> $kode_bulk,
         ':reference'=> $reference,
         ':qty_batch' => $qty_batch
        ]);

        $existBom = $command2->queryAll();


        //Update angka_belakang_koma setiap reload page
        foreach ($existBom as $bom){
            if (!empty($bom['timbangan']) && (($bom['version'] == 'old/new' || $bom['version'] == 'new'))  ){
                $sql_timbangan = "
                    SELECT angka_belakang_koma
                    FROM master_data_timbangan_rm
                    WHERE kode_timbangan = '".$bom['timbangan']."' " ;

                $row_timbangan = MasterDataTimbanganRm::findBySql($sql_timbangan)->one();
                // print_r('<pre>');
                // print_r($row_timbangan);
                if (!empty($row_timbangan['angka_belakang_koma'])){
                    $bom['qty'] = number_format((float)$bom['qty'], $row_timbangan['angka_belakang_koma'], '.', '');
                } else {
                    $bom['qty'] = number_format((float)$bom['qty'], 5, '.', '');
                }
                $sql_update = "UPDATE log_formula_breakdown SET qty = ".$bom['qty']." WHERE id = ".$bom['id']." ";
                $update = Yii::$app->db->createCommand($sql_update)->execute();
            }
        }
        echo 1;
    }

    public function actionCheckApproval($nomo)
    {
        $sql = "
        SELECT *
        FROM log_approval_br
        WHERE  nomo = '".$nomo."' and reviewer is null and approver is null
        ";

        $row = LogApprovalBr::findBySql($sql)->all();

        if (count($row)==0){
            // echo count($row);
            echo 1;
        } else {
            echo 0;
        }

    }

    public function actionCheckVersion($kode_bulk,$reference,$qty_batch)
    {
        $sql = "
        SELECT version
        FROM log_formula_breakdown
        WHERE  kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch."
        ";

        $row = LogFormulaBreakdown::findBySql($sql)->all();

        if (!empty($row)){
            if ($row[0]['version']=='new') {
                echo 1;
            } else {
                echo 2;
            }
        } else {
            echo 0;
        }

    }

    public function actionGetSumNew($kode_bulk,$reference,$qty_batch)
    {
        $sql2 = "
            SELECT sum(a.qty_total)
            FROM (SELECT
                    CASE WHEN uom = 'kg' then qty
                         ELSE qty/1000
                    END AS qty_total
                  FROM log_formula_breakdown WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND (is_split = 0 or is_split = 99)) a
        ";

        $sum = LogFormulaBreakdown::findBySql($sql2)->scalar();

        echo $sum;
    }

    public function actionCheckOperator($kode_bulk,$reference,$qty_batch)
    {
        $sql = "
        SELECT operator
        FROM log_formula_breakdown
        WHERE  kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND (is_split = 0 or is_split = 99) AND (operator = 0 OR operator is null) AND kode_bb <> 'AIR-RO--L'
        ";

        $row = LogFormulaBreakdown::findBySql($sql)->all();

        if (count($row)==0){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionCheckTimbangan($kode_bulk,$reference,$qty_batch)
    {
        $sql = "
        SELECT timbangan
        FROM log_formula_breakdown
        WHERE  kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND (is_split = 0 or is_split = 99) AND timbangan is null AND kode_bb <> 'AIR-RO--L'
        ";

        $row = LogFormulaBreakdown::findBySql($sql)->all();

        if (count($row)==0){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionCheckKodeOlah($kode_bulk,$reference,$qty_batch)
    {
        $sql = "
        SELECT kode_olah
        FROM log_formula_breakdown
        WHERE  kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND (is_split = 0 or is_split = 99) AND kode_bb <> 'AIR-RO--L' AND (kode_olah is null OR kode_olah = '')
        ";

        $row = LogFormulaBreakdown::findBySql($sql)->all();

        if (count($row)==0){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionCheckKodeInternal($kode_bulk,$reference,$qty_batch)
    {
        $sql = "
        SELECT kode_internal
        FROM log_formula_breakdown
        WHERE  kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."' AND qty_batch = ".$qty_batch." AND (is_split = 0 or is_split = 99) AND kode_internal is null AND kode_bb <> 'AIR-RO--L'
        ";

        $row = LogFormulaBreakdown::findBySql($sql)->all();

        if (count($row)==0){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionPdf($nomo,$reference,$qty_batch) {
        // Your SQL query here
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d');

        $this->actionUpdateBatchReal($nomo,$reference,$qty_batch);


        $kode_bulk = strtok($nomo, '/');
        $kode_bulk = preg_replace("/MO-/","",$kode_bulk,1);

        $planner = Yii::$app->db->createCommand("
                    SELECT kode_jadwal
                    FROM scm_planner
                    WHERE nomo = :nomo",
                    [':nomo'=>$nomo])->queryOne();

        $connection2 = Yii::$app->db;
        $command2 = $connection2->createCommand("

        SELECT *
        FROM log_br_detail
        WHERE kode_bulk = '".$kode_bulk."' and formula_reference = '".$reference."' and qty_batch = ".$qty_batch);

        $header_komponen = $command2->queryAll();
        $no_dokumen = $header_komponen[0]['no_dokumen'];
        $index_F = strpos($no_dokumen,'.F');
        $reference_code = substr($no_dokumen, $index_F+1, strlen($no_dokumen)-$index_F+1);

        // $connection2 = Yii::$app->db;
        // $command2 = $connection2->createCommand("

        // SELECT sum(cycle_time_std)
        // FROM log_formula_breakdown
        // WHERE kode_bulk = '".$kode_bulk."' and formula_reference = '".$reference."' and qty_batch = ".$qty_batch." AND (is_split = 0 or is_split = 99)
        // GROUP BY kode_bulk,formula_reference,qty_batch
        // ");

        // $total_cycle_time = $command2->queryScalar();

        $connection2 = Yii::$app->db;
        $command2 = $connection2->createCommand("

        SELECT *
        FROM log_formula_breakdown
        WHERE kode_bulk = '".$kode_bulk."' and formula_reference = '".$reference."' and qty_batch = ".$qty_batch." AND (is_split = 0 or is_split = 99)
        ORDER BY siklus, (COALESCE(substring(kode_olah::text, '^(\d+)'::text)::integer, 99999999)),
            (substring(kode_olah::text, '[a-zA-z_-]+'::text)),
            (COALESCE(substring(kode_olah::text, '(\d+)$'::text)::integer, 0)),
            urutan asc
        ");

        $bom_komponen = $command2->queryAll();

        $sql = "
        SELECT distinct(siklus)
        FROM log_formula_breakdown
        WHERE kode_bulk = '".$kode_bulk."' and formula_reference = '".$reference."' and qty_batch = ".$qty_batch. " AND (is_split = 0 or is_split = 99) order by siklus";

        $siklus_komponen = LogFormulaBreakdown::findBySql($sql)->all();
        $siklus = count($siklus_komponen);

        $sql2 = "  SELECT a.operator,
                    a.siklus
                   FROM ( SELECT DISTINCT operator,
                            siklus
                           FROM log_formula_breakdown
                          WHERE kode_bulk = :kode_bulk AND formula_reference = :reference AND qty_batch = :qty_batch AND (is_split = 0 OR is_split = 99)
                          ORDER BY siklus) a
                  ORDER BY (
                        CASE
                            WHEN a.operator = 11 THEN 1
                            WHEN a.operator = 1 THEN 2
                            WHEN a.operator = 2 THEN 3
                            WHEN a.operator = 3 THEN 4
                            WHEN a.operator = 4 THEN 5
                            WHEN a.operator = 5 THEN 6
                            WHEN a.operator = 6 THEN 7
                            ELSE NULL::integer
                        END)";
        // SELECT distinct(operator),siklus
        // FROM log_formula_breakdown
        // WHERE kode_bulk = '".$kode_bulk."' and formula_reference = '".$reference."' and qty_batch = ".$qty_batch." AND (is_split = 0 or is_split = 99) order by siklus asc,operator asc";

        $operator_komponen = LogFormulaBreakdown::findBySql($sql2,[':kode_bulk'=>$kode_bulk,':reference'=>$reference,':qty_batch'=>$qty_batch])->all();
        //$siklus = count($siklus_komponen);

        // GET LIST OF WEIGHER
        $weigherListSql = "
        SELECT distinct(timbangan),uom,operator,siklus
        FROM log_formula_breakdown
        WHERE kode_bulk = '".$kode_bulk."' and formula_reference = '".$reference."' and qty_batch = ".$qty_batch." AND (is_split = 0 or is_split = 99) order by uom desc, timbangan asc";

        $weigherList = LogFormulaBreakdown::findBySql($weigherListSql)->all();


        // GET LIST OF UOM
        $uomListSql = "
        SELECT distinct(siklus),operator,uom
        FROM log_formula_breakdown
        WHERE kode_bulk = '".$kode_bulk."' and formula_reference = '".$reference."' and qty_batch = ".$qty_batch." AND (is_split = 0 or is_split = 99) order by siklus asc,operator asc,uom desc";

        $uomList = LogFormulaBreakdown::findBySql($uomListSql)->all();

        // print_r('<pre>');
        // print_r($weigherList);
        // exit();

        if ($header_komponen[0]['sediaan'] == 'L' ){
            $kode_timbangan_kg = array("LWA", "LWB");
            $kode_timbangan_gram = array("LWC", "LWD");
        } else if ($header_komponen[0]['sediaan'] == 'P' ){
            $kode_timbangan_kg = array("PWA");
            $kode_timbangan_gram = array("PWB","PWC");
        } else if ($header_komponen[0]['sediaan'] == 'S' ){
            $kode_timbangan_kg = array("SWA");
            $kode_timbangan_gram = array("SWB","SWC");
        }else{
            $kode_timbangan_kg = array("VWA");
            $kode_timbangan_gram = array("VWB","VWC");
        }

        // GET LIST OF PERSON
        $sql9 = "
        SELECT *
        FROM log_approval_br
        WHERE nomo = '".$nomo."' order by id desc";

        $pic = LogApprovalBr::findBySql($sql9)->one();

        // GET LIST OF PERSON
        $sql10 = "
        SELECT reviewer
        FROM list_reviewer
        WHERE username = '".$pic->reviewer."' order by id desc";

        $pic_reviewer = LogApprovalBr::findBySql($sql10)->one();

        // GET LIST OF PERSON
        $sql11 = "
        SELECT approver
        FROM list_approver
        WHERE username = '".$pic->approver."' order by id desc";

        $pic_approver = LogApprovalBr::findBySql($sql11)->one();

        // print_r(count($bom_komponen));
        // $jml_data = count($bom_komponen);
        // $mid = ceil($jml_data/2);
        // print_r('<pre>');
        // print_r($mid);
        // exit();
        // return $this->render('batch-record', [
        //     'header_komponen' => $header_komponen,
        //     'bom_komponen' => $bom_komponen,
        //     'date' =>$date,
        //     // 'taskQty' => $taskQty,
        //     // 'task' => $task,
        //     'planner' => $planner,
        //     // 'date' => $date,
        //     // 'flow_input_mo' => $flow_input_mo,
        //     // 'splitTask' => $splitTask,
        //     // 'splitTaskQty' => $splitTaskQty,
        //     // 'brand' => $brand,
        //     'weigherList' => $weigherList,
        //     'uomList' => $uomList,
        //     // 'list_operator' => $list_operator,
        //     // 'qtyOpr' => $qtyOpr,
        //     'siklus' => $siklus,
        //     'siklus_komponen' => $siklus_komponen,
        //     'operator_komponen' => $operator_komponen,
        //     // 'master_timbangan' => $masterTimbangan,
        //     'timbangan_kg' => $kode_timbangan_kg,
        //     'timbangan_gram' => $kode_timbangan_gram,
        //     'pic' => $pic,
        //     'reviewer' => $pic_reviewer->reviewer,
        //     'approver' => $pic_approver->approver,
        //     // 'total_cycle_time' => $total_cycle_time,
        // ]);
        // exit();

        $content =  $this->renderPartial('batch-record', [
            'header_komponen' => $header_komponen,
            'bom_komponen' => $bom_komponen,
            'date' =>$date,
            // 'taskQty' => $taskQty,
            // 'task' => $task,
            'planner' => $planner,
            // 'date' => $date,
            // 'flow_input_mo' => $flow_input_mo,
            // 'splitTask' => $splitTask,
            // 'splitTaskQty' => $splitTaskQty,
            // 'brand' => $brand,
            'weigherList' => $weigherList,
            'uomList' => $uomList,
            // 'list_operator' => $list_operator,
            // 'qtyOpr' => $qtyOpr,
            'siklus' => $siklus,
            'siklus_komponen' => $siklus_komponen,
            'operator_komponen' => $operator_komponen,
            // 'master_timbangan' => $masterTimbangan,
            'timbangan_kg' => $kode_timbangan_kg,
            'timbangan_gram' => $kode_timbangan_gram,
            'pic' => $pic,
            'reviewer' => $pic_reviewer->reviewer,
            'approver' => $pic_approver->approver,
            // 'total_cycle_time' => $total_cycle_time,
        ]);

        // $content = $this->renderPartial('report', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        //     'nalang' => $nalang,
        //     'tglrencanabayar' => $tglrencanabayar,
        //     'norek' => $norek,
        //     'nama_rekening' => $nama_rekening,
        //     'bank' => $bank,
        //     'bayar_via' => $bayar_via,
        // ]);

        // $content = $this->renderPartial('index', ['model' => $model]);

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
        // set to use core fonts only
        // 'mode' => Pdf::MODE_CORE,
        'mode' => Pdf::MODE_UTF8,
        // A4 paper format
        'format' => Pdf::FORMAT_FOLIO,
        //FILENAME
        'filename' => $header_komponen[0]['nama_bulk'].' '.$reference_code.' '.number_format($header_komponen[0]['qty_batch'],0).' Kg BR Timbang.pdf',
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT,
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER,
        // your html content input
        'content' => $content,
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting
        'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
        'cssInline' => 'p#keterangan{margin:0;}
                        .kv-heading-1{font-size:18px}
                        .bold{font-weight:bold;}
                        .underline{text-decoration: underline;}
                        .italic{font-style: italic;}',
        // set mPDF properties on the fly
        'options' => ['title' => 'Batch Record : '.$header_komponen[0]['no_dokumen'].'.'.$header_komponen[0]['no_revisi'],'keywords' => 'krajee, grid, export, yii2-grid, pdf'],
        // call mPDF methods on the fly
        'methods' => [
        'SetHeader'=>['Penimbangan Raw Material - PT Paragon Technology & Innovation'],
        // 'SetFooter'=>$footer,
        'SetFooter'=>['<table><th>rama</th><th>dhani</th></table>This Batch Record is computer generated, no signature required'],
        ],
        // 'marginFooter'=>10,
        'marginBottom'=>30,

        ]);

        /*------------------------------------*/
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        /*------------------------------------*/

        // return the pdf output as per the destination setting
        return $pdf->render();

        // return $content->render();
    }
}
