<?php

namespace app\controllers;

use Yii;
use app\models\MasterDataBarcodeMalaysia;
use app\models\MasterDataBarcodeMalaysiaSearch;
use app\models\BarcodeMalaysiaPrinter;
use app\models\BarcodeMalaysiaPrinterSearch;
use app\models\WeigherFgMapDevice;
use app\models\ScmPlanner;
use app\models\FlowInputSnfg;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MasterDataBarcodeMalaysiaController implements the CRUD actions for MasterDataBarcodeMalaysia model.
 */
class MasterDataBarcodeMalaysiaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterDataBarcodeMalaysia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterDataBarcodeMalaysiaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterDataBarcodeMalaysia model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterDataBarcodeMalaysia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterDataBarcodeMalaysia();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MasterDataBarcodeMalaysia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterDataBarcodeMalaysia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterDataBarcodeMalaysia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterDataBarcodeMalaysia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterDataBarcodeMalaysia::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCheckSnfg()
    {
        $server_name = $_SERVER['SERVER_NAME'];
        if($server_name == 'factory.pti-cosmetics.com'){
             echo "<script>location.href='".Yii::getALias('@ipUrl')."master-data-barcode-malaysia%2Fcheck-snfg';</script>";
        }
        $model = new BarcodeMalaysiaPrinter();

        $searchModel = new BarcodeMalaysiaPrinterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query->where("datetime_start is not null and datetime_stop is null");

        $this->layout = '//main-sidebar-collapse'; 

        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];            
        }else{
            $frontend_ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT 
            *
        FROM weigher_fg_map_device
        WHERE frontend_ip = '".$frontend_ip."'";

        $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

        // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
        if(empty($map_device_array)){
            $sbc_ip = 'Tidak Terdaftar';
        }else{
            $sbc_ip = $map_device_array->sbc_ip;
        }

        date_default_timezone_set('Asia/Jakarta');
        $current_date = date("Y-m-d");
        $btsBawah = $current_date." 00:00:00";
        $btsAtas = $current_date." 23:59:59";
        //$current_time = date('Y-m-d h:i A');

        $sql2="select sum(qty_request) from barcode_malaysia_printer where timestamp>='".$btsBawah."' and timestamp <= '".$btsAtas."'";

        $qty_today = BarcodeMalaysiaPrinter::findBySql($sql2)->scalar();

        if (empty($qty_today)){
            $qty_today = 0;
        }

        $sql3="select sum(qty_request) from barcode_malaysia_printer";

        $qty_total = BarcodeMalaysiaPrinter::findBySql($sql3)->scalar();

        if (empty($qty_total)){
            $qty_total = 0;
        }

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['print-label', 'snfg' => $model->snfg]);
        } else {
            return $this->render('check-snfg', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'frontend_ip' => $frontend_ip,
                'sbc_ip' => $sbc_ip,
                'qty_today' => $qty_today,
                'qty_total' => $qty_total,
            ]);
        }
    }

    public function actionPrintLabel($snfg)
    {

        $model = new BarcodeMalaysiaPrinter();
 
        // Populate Grid Data
        $searchModel = new BarcodeMalaysiaPrinterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';

        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];            
        }else{
            $frontend_ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT 
            *
        FROM weigher_fg_map_device
        WHERE frontend_ip = '".$frontend_ip."'";

        $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

        // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
        if(empty($map_device_array)){
            $lokasi = 'Tidak Terdaftar';
        }else{
            $lokasi = $map_device_array->keterangan;
        }

        /********** GET DATA FROM SCM PLANNER *************/
        $sql1="
        
        SELECT 
            *
        FROM scm_planner
        WHERE snfg = '".$snfg."'
        ORDER BY id desc
        ";

        $scm_planner = ScmPlanner::findBySql($sql1)->one();

        if (empty($scm_planner->nama_fg)){
            $koitem_fg = "";
            $nama_fg = "";
        } else {
            $koitem_fg = $scm_planner->koitem_fg;
            $nama_fg = $scm_planner->nama_fg;
         }
        /**************************************************/

        /***** GET BARCODE ******/
        $sql4="
        
        SELECT 
            *
        FROM master_data_barcode_malaysia
        WHERE koitem_lama ilike '%".$koitem_fg."%' or koitem_baru ilike '%".$koitem_fg."%'
        ORDER BY id desc
        ";

        $data4 = MasterDataBarcodeMalaysia::findBySql($sql4)->one();

        if (empty($data4)){
            $barcode = "";
        } else {
            $barcode = $data4->barcode_harmonize;
        }
        /**************************************************/

        // Check If Post Value True
        if ($model->load(Yii::$app->request->post())) {
            $sql5="select sum(qty_request) from barcode_malaysia_printer where snfg ='".$snfg."'";

            $qty_total = BarcodeMalaysiaPrinter::findBySql($sql5)->scalar();

            if (empty($qty_total)){
                $qty_total = 0;
            }

            $model->status = "not-start";

            $model->qty_total_per_snfg = $qty_total+$model->qty_request;
            $model->koitem_fg = $koitem_fg;
            $model->lokasi = $lokasi;
            $model->barcode = $model->barcode;
            // $model->save();
            // echo "lol";
            // print_r($model->getErrors());
            if ($model->save()){
                // print_r($model->getErrors());
                return $this->redirect(['check-snfg']);
            }
        } else {
            return $this->render('print-label', [
                'model' => $model,
                'snfg' => $snfg,
                'koitem_fg' => $koitem_fg,
                'nama_fg' => $nama_fg,
                'barcode' => $barcode,
            ]);
        }
    }

    /**
     * Stop Jadwal Kemas 2 Inline Function
     * @param $snfg      
     * @return mixed
     */
    public function actionCheckSnfgExist($snfg)
    {
        $model = new BarcodeMalaysiaPrinter();
 
        // Populate Grid Data
        $searchModel = new BarcodeMalaysiaPrinterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';

        $sql="select * from scm_planner where snfg='".$snfg."'";
        $check= ScmPlanner::findBySql($sql)->one();

        $sql2="select * from flow_input_snfg where snfg='".$snfg."'";
        $check2= FlowInputSnfg::findBySql($sql2)->one();

        if (!empty($check)) {
            if (!empty($check2)){
                echo 1;
            } else {
                echo 2;
            }
        } else {
            echo 0;
        }       
    }
}
