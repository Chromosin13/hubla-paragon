<?php

namespace app\controllers;

use Yii;
use app\models\QcTimbangFg;
use app\models\QcTimbangFgSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use app\models\User;
use yii\filters\AccessControl;
use app\models\QcFg;
use app\models\Kemas2;

/**
 * QcTimbangFgController implements the CRUD actions for QcTimbangFg model.
 */
class QcTimbangFgController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create', 'update', 'delete','index','view'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_QCTIMBANGFG
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all QcTimbangFg models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QcTimbangFgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single QcTimbangFg model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new QcTimbangFg model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new QcTimbangFg();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['create']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing QcTimbangFg model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionBatchSplitAssignKomponen($snfg_komponen)
    {


        $countQcFg = QcFg::find()
                        ->where(['snfg_komponen' => $snfg_komponen])
                        ->count();


        $sql = "
                SELECT distinct lanjutan_split_batch
                FROM qc_fg
                WHERE snfg_komponen='".$snfg_komponen."' and state='STOP'
                ";

        $QcFgs = QcFg::findBySql($sql)->all();

        if($countQcFg > 0)
        {
            echo "<option value='Select SB'>Select SB</options>";
            foreach($QcFgs as $QcFg){
                echo "<option value='".$QcFg->lanjutan_split_batch."'>".$QcFg->lanjutan_split_batch."</options>";
            }

        }
        else{
                echo "<option>1</option>";
        }
    }

    public function actionBatchSplitAssignSnfg($snfg)
    {
        $countQcFg = QcFg::find()
                        ->where(['snfg' => $snfg])
                        ->count();


        $sql = "
                SELECT distinct lanjutan_split_batch
                FROM qc_fg
                WHERE snfg='".$snfg."' and state='STOP'
                ";

        $QcFgs = QcFg::findBySql($sql)->all();


        if($countQcFg > 0)
        {
            echo "<option value='Select SB'>Select SB</options>";
            foreach($QcFgs as $QcFg){
                echo "<option value='".$QcFg->lanjutan_split_batch."'>".$QcFg->lanjutan_split_batch."</options>";
            }

        }
        else{
                echo "<option>1</option>";
        }
    }

    public function actionCheckBatchSplitKomponen($snfg_komponen)
    {
        $sql="
                select distinct on (qc_fg.snfg_komponen) 
                    qc_fg.snfg_komponen
                    ,state
                    ,is_done
                    ,lanjutan_split_batch
                    ,case when k2.maxlsb>1 then 1
                          else -1
                     end last_status
                from qc_fg 
                left join (
                            select  snfg_komponen, 
                                    max(lanjutan_split_batch) maxlsb
                            from kemas_2
                            where snfg_komponen='".$snfg_komponen."'
                            group by snfg_komponen 
                          ) k2 on k2.snfg_komponen = qc_fg.snfg_komponen
                WHERE qc_fg.snfg_komponen ='".$snfg_komponen."'
                order by snfg_komponen,timestamp desc
                ";
        $QcTimbangFg= QcTimbangFg::findBySql($sql)->one();
        //print_r($QcFg);
        echo Json::encode($QcTimbangFg);
    }

    public function actionCheckBatchSplitSnfg($snfg)
    {
        $sql="
                select distinct on (qc_fg.snfg) 
                    qc_fg.snfg
                    ,state
                    ,is_done
                    ,lanjutan_split_batch
                    ,case when k2.maxlsb>1 then 1
                          else -1
                     end last_status
                from qc_fg 
                left join (
                            select  snfg, 
                                    max(lanjutan_split_batch) maxlsb
                            from kemas_2
                            where snfg='".$snfg."'
                            group by snfg 
                          ) k2 on k2.snfg = qc_fg.snfg
                WHERE qc_fg.snfg ='".$snfg."'
                order by snfg,timestamp desc
                ";
        $QcTimbangFg= QcTimbangFg::findBySql($sql)->one();
        //print_r($QcFg);
        echo Json::encode($QcTimbangFg);
    }

    public function actionPaletAssignKomponen($snfg_komponen)
    {
        $countQcFg = Kemas2::find()
                        ->where(['snfg_komponen' => $snfg_komponen])
                        ->count();


        $sql = "
                SELECT distinct palet_ke
                FROM kemas_2
                WHERE snfg_komponen='".$snfg_komponen."' and state='STOP'
                ";

        $QcFgs = QcFg::findBySql($sql)->all();

        if($countQcFg > 0)
        {
            echo "<option value='Select Palet'>Select Palet</options>";
            foreach($QcFgs as $QcFg){
                echo "<option value='".$QcFg->palet_ke."'>".$QcFg->palet_ke."</options>";
            }

        }
        else{
                echo "<option>1</option>";
        }
    }

    public function actionPaletAssignSnfg($snfg)
    {
        $countQcFg = Kemas2::find()
                        ->where(['snfg' => $snfg])
                        ->count();


        $sql = "
                SELECT distinct palet_ke
                FROM kemas_2
                WHERE snfg='".$snfg."' and state='STOP'
                ";

        $QcFgs = QcFg::findBySql($sql)->all();

        if($countQcFg > 0)
        {
            echo "<option value='Select Palet'>Select Palet</options>";
            foreach($QcFgs as $QcFg){

                echo "<option value='".$QcFg->palet_ke."'>".$QcFg->palet_ke."</options>";
            }

        }
        else{
                echo "<option>1</option>";
        }
    }

    public function actionLanjutanQcTimbangFg($snfg_komponen,$jenis_periksa)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM 
            (
                select sp.snfg, qc_timbang_fg.snfg_komponen, qc_timbang_fg.jenis_periksa, qc_timbang_fg.state, qc_timbang_fg.lanjutan
                from qc_timbang_fg 
                inner join scm_planner sp on sp.snfg_komponen = qc_timbang_fg.snfg_komponen
                where state='STOP' and qc_timbang_fg.snfg is null and qc_timbang_fg.snfg_komponen is not null 
                and qc_timbang_fg.snfg_komponen='".$snfg_komponen."'

                union all
                
                select p.snfg,sp.snfg_komponen, p.jenis_periksa, p.state, p.lanjutan 
                from qc_timbang_fg p
                inner join scm_planner sp on sp.snfg=p.snfg
                where p.state='STOP' and p.snfg is not null and sp.snfg_komponen='".$snfg_komponen."'
            ) a
        WHERE a.snfg_komponen ='".$snfg_komponen."'
        and a.jenis_periksa = '".$jenis_periksa."'
        ";

        $QcTimbangFg= QcTimbangFg::findBySql($sql)->one();
        echo Json::encode($QcTimbangFg);

    }

    public function actionLanjutanQcTimbangFgsnfg($snfg,$jenis_periksa)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM 
            (
                select qc_timbang_fg.snfg, sp.snfg_komponen, qc_timbang_fg.jenis_periksa, qc_timbang_fg.state, qc_timbang_fg.lanjutan
                from qc_timbang_fg 
                inner join scm_planner sp on sp.snfg = qc_timbang_fg.snfg
                where state='STOP' and qc_timbang_fg.snfg is not null and qc_timbang_fg.snfg_komponen is null 
                and qc_timbang_fg.snfg='".$snfg."'

                union all
                
                select sp.snfg,p.snfg_komponen, p.jenis_periksa, p.state, p.lanjutan 
                from qc_timbang_fg p
                inner join scm_planner sp on sp.snfg_komponen=p.snfg_komponen
                where p.state='STOP' and p.snfg is null and sp.snfg='".$snfg."'
            ) a
        WHERE a.snfg ='".$snfg."'
        and a.jenis_periksa = '".$jenis_periksa."'
        ";

        $QcTimbangFg= QcTimbangFg::findBySql($sql)->one();
        echo Json::encode($QcTimbangFg);

    }

    // public function actionLanjutanQcTimbangFg($snfg,$jenis_periksa)
    // {
    //     $sql="select coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan from qc_timbang_fg where snfg='".$snfg."' and jenis_periksa='".$jenis_periksa."' and state='STOP'";
    //     $QcTimbangFg= QcTimbangFg::findBySql($sql)->one();
    //     echo Json::encode($QcTimbangFg);

    // }



    public function actionLanjutanIstQcTimbangFg($snfg_komponen,$jenis_periksa,$lanjutan)
    {
        $sql="
                SELECT coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist
                FROM 
                    (
                        select sp.snfg, qc_timbang_fg.snfg_komponen, qc_timbang_fg.jenis_periksa, qc_timbang_fg.state, qc_timbang_fg.lanjutan,qc_timbang_fg.lanjutan_ist
                        from qc_timbang_fg 
                        inner join scm_planner sp on sp.snfg_komponen = qc_timbang_fg.snfg_komponen
                        where state='ISTIRAHAT STOP' and qc_timbang_fg.snfg is null and qc_timbang_fg.snfg_komponen is not null and qc_timbang_fg.snfg_komponen='".$snfg_komponen."'

                        union all
                        
                        select p.snfg,sp.snfg_komponen, p.jenis_periksa, p.state, p.lanjutan, p.lanjutan_ist 
                        from qc_timbang_fg p
                        inner join scm_planner sp on sp.snfg=p.snfg
                        where p.state='ISTIRAHAT STOP' and p.snfg is not null and sp.snfg_komponen='".$snfg_komponen."'
                    ) a
                WHERE a.snfg_komponen ='".$snfg_komponen."'
                and a.jenis_periksa = '".$jenis_periksa."'
                and a.lanjutan = '".$lanjutan."'
                ";
        $QcTimbangFg= QcTimbangFg::findBySql($sql)->one();
        echo Json::encode($QcTimbangFg);
    }

    public function actionLanjutanIstQcTimbangFgsnfg($snfg,$jenis_periksa,$lanjutan)
    {
        $sql="
                SELECT coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist
                FROM 
                    (
                        select sp.snfg, qc_timbang_fg.snfg_komponen, qc_timbang_fg.jenis_periksa, qc_timbang_fg.state, qc_timbang_fg.lanjutan,qc_timbang_fg.lanjutan_ist
                        from qc_timbang_fg 
                        inner join scm_planner sp on sp.snfg_komponen = qc_timbang_fg.snfg_komponen
                        where state='ISTIRAHAT STOP' and qc_timbang_fg.snfg is null and qc_timbang_fg.snfg_komponen is not null and sp.snfg='".$snfg."'

                        union all
                        
                        select p.snfg,sp.snfg_komponen, p.jenis_periksa, p.state, p.lanjutan, p.lanjutan_ist 
                        from qc_timbang_fg p
                        inner join scm_planner sp on sp.snfg=p.snfg
                        where p.state='ISTIRAHAT STOP' and p.snfg is not null and p.snfg='".$snfg."'
                    ) a
                WHERE a.snfg ='".$snfg."'
                and a.jenis_periksa = '".$jenis_periksa."'
                and a.lanjutan = '".$lanjutan."'
                ";
        $QcTimbangFg= QcTimbangFg::findBySql($sql)->one();
        echo Json::encode($QcTimbangFg);
    }
    

    // public function actionLanjutanIstQcTimbangFg($snfg,$jenis_periksa,$lanjutan)
    // {
    //     $sql="select coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist from qc_timbang_fg where snfg='".$snfg."' and jenis_periksa='".$jenis_periksa."' and state='ISTIRAHAT STOP' and lanjutan=".$lanjutan." ";
    //     $QcTimbangFg= QcTimbangFg::findBySql($sql)->one();
    //     echo Json::encode($QcTimbangFg);
    // }

    /**
     * Deletes an existing QcTimbangFg model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the QcTimbangFg model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QcTimbangFg the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QcTimbangFg::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
