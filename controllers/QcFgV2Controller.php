<?php

namespace app\controllers;

use Yii;
use app\models\QcFgV2;
use app\models\ScmPlanner;
use app\models\FlowInputSnfg;
use app\models\QcFgV2Search;
use app\models\LogReceiveNdc;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\filters\AccessControl;
use app\components\AccessRule;
use kartik\mpdf\Pdf;
use app\models\User;
use app\models\SuratJalan;
use app\models\SuratJalanRincian;
use app\models\SuratJalanSearch;
use app\models\PengecekanUmum;
use app\models\WeigherFgMapDevice;
use app\models\DeviceMapQcfg;
use app\models\FlowPraKemas;
use yii\web\JsExpression;

// use yii\helpers\Json;
/**
 * QcFgV2Controller implements the CRUD actions for QcFgV2 model.
 */
class QcFgV2Controller extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create', 'update', 'delete','index','view','terima-karantina','terima-ndc','print-label','print-zebra','scan-karantina','update-karantina'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_QCFG,
                            User::ROLE_KARANTINA,
                            User::ROLE_NDC,
                        ],
                    ],
                    [
                        'actions' => ['terima-karantina','scan-karantina','update-karantina'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_KARANTINA,
                            User::ROLE_QCFG,
                        ],
                    ],
                    [
                        'actions' => ['terima-ndc'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_NDC,
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_QCFG
                        ],
                    ],
                    [
                        'actions' => ['print-label','print-zebra'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_QCFG
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all QcFgV2 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QcFgV2Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionConsoleInsertTest(){

        $connection = Yii::$app->getDb();
        date_default_timezone_set('Asia/Jakarta');
        $command = $connection->createCommand("
            INSERT INTO qc_fg_v2_auto_is_done (snfg)
            SELECT
                    'A' as snfg
            ;");
        $result = $command->queryAll();

    }

    /**
     * Displays a single QcFgV2 model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
      * actionViewKarantina
      * Displays a single QcFgV2 model in karantina format.
      * @author Redha Hari
      * @param string $snfg
      * @return json.
    */
    public function actionViewKarantina($id)
    {
        return $this->render('view-karantina', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionScanBarcodeTerimaNdc()
    {

        return $this->render('scan-barcode-terima-ndc', [
        ]);
    }

    public function actionFormTerimaNdc($surat_jalan)
    {
        $sql = "SELECT * from log_receive_ndc where nosj='".$surat_jalan."' and status = 'RECEIVED'";
        $check = LogReceiveNdc::findBySql($sql)->all();
        if (!empty($check)){
            return $this->redirect(['log-receive-ndc/task', 'nosj'=>$surat_jalan , 'operator'=>$check[0]['pic'] ]);
            // print_r($check[0]['pic']);
            // exit();
        }

        return $this->render('_form-terima-ndc', [
            'surat_jalan' => $surat_jalan,
        ]);
    }



    public function actionKonfirmasiTerimaNdcQr($surat_jalan,$operator)
    {

        $nosj = $surat_jalan;

        $sql = "
            SELECT
                id,
                is_received
            FROM
                surat_jalan
            WHERE nosj='".$surat_jalan."'
            LIMIT 1
           ";

        $sj = SuratJalan::findBySql($sql)->one();

        if(empty($sj->id)){
            throw new \yii\web\HttpException(404,'Tidak Ditemukan');
        }

        else{

            $this->layout = '//main-qr-layout';

            date_default_timezone_set('Asia/Jakarta');

            $model = new QcFgV2();

            $searchModel = new QcFgV2Search();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where("surat_jalan_id=".$sj->id."");


            // Get Related Information

            if ($model->load(Yii::$app->request->post())){

                // return $this->redirect(['create-rincian-qr', 'id' => $id]);
                return $this->redirect(['qc-fg-v2/terima-surat-jalan-ndc', 'surat_jalan' => $surat_jalan]);

            } else {

                return $this->render('terima-ndc-qr', [
                    'model' => $model,
                    'nosj' => $nosj,
                    'is_received' => $sj->is_received,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]);
            }


        }

    }

    /**
      * actionHalamanTerimaNdc
      * @author Redha Hari
      * @return void redirect to halaman-terima-ndc.
    */
    public function actionHalamanTerimaNdc($e=null)

    {
        if ($e=='id-not-found'){
            Yii::$app->session->setFlash('danger',"TERDAPAT KODE ODOO YANG TIDAK DITEMUKAN");
        }else if ($e=='success'){
            Yii::$app->session->setFlash('success',"DATA BERHASIL DI UPDATE KE ODOO");
        }else if ($e=='sudah-update'){
            Yii::$app->session->setFlash('danger',"SURAT JALAN SUDAH PERNAH DI UPDATE KE ODOO");
        }else if ($e=='json-fail'){
            Yii::$app->session->setFlash('danger',"FILE JSON TIDAK TERBENTUK, HUBUNGI TIM IT");
        }else if ($e=='odoo-not-updated'){
            Yii::$app->session->setFlash('danger',"DATA GAGAL DI UPDATE KE ODOO");
        }else{}

        $searchModel = new SuratJalanSearch();
        $dataRecieved = $searchModel->search(Yii::$app->request->queryParams);
        $dataRecieved->query->andWhere("is_received = 1")->orderBy(['timestamp'=>SORT_DESC]);
        $dataNotrecieved = $searchModel->search(Yii::$app->request->queryParams);
        $dataNotrecieved->query->andWhere("is_received is null")->andWhere("nosj not like 'DRAFT'")->orderBy(['timestamp'=>SORT_DESC]);

        return $this->render('halaman-terima-ndc', [
            'searchModel'=>$searchModel,
            'dataRecieved'=>$dataRecieved,
            'dataNotrecieved'=>$dataNotrecieved
        ]);
    }

    /**
     * Creates a new QcFgV2 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new QcFgV2();

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['create-rincian', 'snfg' => $model->snfg]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateTab()
    {

            $frontend_ip = $_SERVER['REMOTE_ADDR'];

            $sql="
            SELECT
                *
            FROM weigher_fg_map_device
            WHERE frontend_ip = '".$frontend_ip."'";

            $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

            if(empty($map_device_array)){
                $sbc_ip = '10.128.1.161';
                $line = 'Line A';
            }else{
                $sbc_ip = $map_device_array->sbc_ip;
                $line = $map_device_array->line;

            }


            $model = new QcFgV2();

            $searchModel = new QcFgV2Search();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // $dataProvider->query->where("posisi='PENIMBANGAN' and datetime_start is not null and datetime_stop is null");

            $this->layout = '//main-sidebar-collapse';

            return $this->render('create-tab', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'frontend_ip' => $frontend_ip,
                    'sbc_ip' => $sbc_ip,
                    'line' => $line,
            ]);
            // if ($model->load(Yii::$app->request->post())) {
            //     // return $this->redirect(['create-penimbangan', 'kode_unik_formula' => $model->kode_unik_formula]);
            // } else {

            // }
    }

    /**
      * actionCheckSnfg
      * Calculate SNFG count existing in scm_planner and qc_fg_v2
      * Create Condition when count of snfg in scm_planner and qc_fg_v2 is at certain conditions
      * @author Redha Hari
      * @param string $snfg
      * @return json.
    */
    public function actionCheckSnfg($snfg)
    {
        $sql="
                SELECT
                    sp.snfg,
                    count(sp.snfg) as count_sp,
                    count(qbe.snfg) as count_qbe,
                    case when count(sp.snfg)=0 then 0
                         when count(sp.snfg)>=1 and count(qbe.snfg) =0 then 1
                         when count(sp.snfg)>=1 and count(qbe.snfg) >=1 then 2 end as id
                FROM scm_planner sp
                LEFT JOIN qc_fg_v2 qbe on sp.snfg = qbe.snfg
                where sp.snfg = '".$snfg."'
                GROUP BY sp.snfg
             ";
        $ch = QcFgV2::findBySql($sql)->one();
        echo Json::encode($ch);
    }

    public function actionUpdateIp($frontend_ip)
    {
        $cookie_name = 'device_identity';

        $device = DeviceMapQcfg::find()->where(['frontend_ip'=>$frontend_ip])->one();

        if (empty($device)){

            if(isset($_COOKIE[$cookie_name])) {
                $sql = "SELECT * FROM device_map_qcfg WHERE MD5(frontend_ip) = :ip";
                $select = DeviceMapQcfg::findBySql($sql,[':ip'=>$_COOKIE[$cookie_name]])->one(); 

                $sql = "UPDATE device_map_qcfg set frontend_ip = :new_ip WHERE MD5(frontend_ip) = :ip";
                $update = Yii::$app->db->createCommand($sql,[':new_ip'=>$frontend_ip,':ip'=>$_COOKIE[$cookie_name]])->execute();
                if ($update > 0){
                    setcookie($cookie_name,hash('md5',$frontend_ip));
                }
            }

        }else{
            setcookie($cookie_name,hash('md5',$device->frontend_ip));
        }

    }

    /**
      * actionScanKarantina
      * Triggered from Barcode Scan SNFG
      * Render Rincian Page (Index) for an SNFG
      * @author Redha Hari
      * @return mixed render
    */
    public function actionCreateRincian($snfg)
    {   
        // Get Device Information
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $frontend_ip = $_SERVER['REMOTE_ADDR'];
        }

        $this->actionUpdateIp($frontend_ip);

        $this->layout = '//main-sidebar-collapse';
        $this->actionRefreshNobatch($snfg);

        $mod = new ScmPlanner();

        $result = $mod->periksaStatusSnfg($snfg);

        $a = json_decode($result);

        $kode_item = strtok($snfg,'/');
        $sp = ScmPlanner::find()->select(['odoo_code'])->where("snfg = '".$snfg."' and odoo_code is not null")->one();
        $odoo_code = $sp['odoo_code'];

        //Cek apakah item tersebut FG atau Packaging
        $sql = "SELECT pt.default_code, pc.complete_name
                FROM product_template pt
                JOIN product_category pc on pt.categ_id = pc.id
                WHERE default_code = :kode_item";
        $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();

        if(empty($product_detail)){
            $sql = "SELECT pt.default_code, pc.complete_name
                FROM varcos.product_template pt
                JOIN varcos.product_category pc on pt.categ_id = pc.id
                WHERE default_code = :kode_item";
            $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();
        }

        if (!empty($product_detail)){
            if (strpos(strtolower($product_detail['complete_name']),'packaging')!==false){
                $category = 'packaging';
            }else{
                $category = 'fg';
            }
        }else{
            $category = 'null';
        }

        $fim = FlowInputSnfg::find()->select(['nobatch','exp_date','fg_name_odoo'])->where("snfg = '".$snfg."' and nobatch is not null")->orderBy(['datetime_start'=>SORT_DESC])->one();

        if (empty($fim)){
            $fim = FlowPraKemas::find()->select(['nobatch','exp_date','fg_name_odoo'])->where("snfg = '".$snfg."' ")->orderBy(['datetime_start'=>SORT_DESC])->one();
        }

        $nobatch = $fim['nobatch'];
        $nama_fg = $fim['fg_name_odoo'];
        // $exp_date = $fim['exp_date'];

        if($a->status=='UNHOLD'){

            $model = new QcFgV2();

            $searchModel = new QcFgV2Search();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where("snfg='".$snfg."'");

            if(Yii::$app->request->post('hasEditable'))
            {
                $attribute = Yii::$app->request->post('editableAttribute');
                $id = Yii::$app->request->post('editableKey');

                $out = Json::encode(['output'=>'','message'=>'']);
                $post = [];

                $model_qcfg = QcFgV2::findOne($id);
                $posted = current($_POST['QcFgV2']);
                $post['QcFgV2'] = $posted;

                if($model_qcfg->load($post))
                {
                    if ($attribute == 'nobatch'){

                        $sql = "
                            SELECT nobatch,exp_date
                            FROM flow_input_snfg
                            WHERE snfg = :snfg and nobatch = :nobatch order by id desc" ;

                        $record = FlowInputSnfg::findBySql($sql,[':snfg'=>$snfg,':nobatch'=>$model_qcfg->nobatch])->one();

                        if(empty($record)){
                            $sql = "
                                SELECT nobatch,exp_date
                                FROM flow_pra_kemas
                                WHERE snfg = :snfg and nobatch = :nobatch order by id desc" ;

                            $record = FlowPraKemas::findBySql($sql,[':snfg'=>$snfg,':nobatch'=>$model_qcfg->nobatch])->one();
                        }

                        if (!empty($record)){
                            $model_qcfg->exp_date = $record->exp_date;
                            $model_qcfg->save();
                            $null = false;


                        } else {
                            $model_qcfg->exp_date = NULL;
                            $model_qcfg->nobatch = NULL;
                            $model_qcfg->save();
                            $null = true;
                        }



                    }else{
                        $model_qcfg->save();
                    }
                }

                if ($null){
                    $out = Json::encode(['output'=>'','message'=>'']);
                    return $out;
                    // return;
                    // return $this->redirect(Yii::$app->request->referrer);

                }else{
                    echo $out;
                    return;
                }

            }

            if ($model->load(Yii::$app->request->post()) ) {

                date_default_timezone_set('Asia/Jakarta');
                // $year = date("y");
                $fim = FlowInputSnfg::find()->select(['nobatch','exp_date','fg_name_odoo'])->where("snfg = '".$snfg."' and nobatch = '".$model->nobatch."' is not null")->orderBy(['datetime_start'=>SORT_DESC])->one();

                $qcfg = Yii::$app->db->createCommand("WITH cte as (SELECT max(coalesce(nullif(split_part(id_palet, concat('P',right(date_part('year', current_date)::varchar,2)) , 2),''),'0')::INTEGER)+1 AS new_serial FROM qc_fg_v2
                    WHERE date_part('year', timestamp)=date_part('year', CURRENT_DATE))
                    SELECT concat('P',  right(date_part('year', current_date)::varchar,2)   ,cte.new_serial) new_id from cte")->queryOne();
                $model->id_palet = $qcfg['new_id'];
                $model->qty_karantina = $model->qty;
                $model->odoo_code = $odoo_code;
                // $model->nama_fg = $nama_fg;

                $save = true;
                while ($save){
                    try {
                        $model->save();

                        //when model->save() not error then run this
                        $save = false;
                    } catch (\Exception $e) {
                        $next_serial = (int)substr($model->id_palet,3,strlen($model->id_palet))+1;
                        $next_id_palet = substr($model->id_palet,0,3).$next_serial;
                        $model->id_palet = $next_id_palet;
                    }
                }

                return $this->redirect(['create-rincian', 'snfg' => $snfg]);
            } else {
                return $this->render('create-rincian', [
                    'model' => $model,
                    'snfg' => $snfg,
                    'category' => $category,
                    'nobatch' => $nobatch,
                    'nama_fg' => $nama_fg,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,

                ]);
            }


        }else{
            throw new \yii\web\HttpException(401,
          'Jadwal telah di HOLD / CANCEL mohon hubungi Planner');
        }
    }

    public function actionRefreshNobatch($snfg)
    {
        $sql = "UPDATE qc_fg_v2 qc SET exp_date = fis.exp_date 
                FROM
                (select distinct on (nobatch) snfg,nobatch,exp_date FROM flow_input_snfg WHERE snfg = :snfg )fis
                WHERE qc.snfg = :snfg and qc.nobatch = fis.nobatch";
        $update = Yii::$app->db->createCommand($sql,[':snfg'=>$snfg])->execute();
    }
    // public function actionCreateSurjalRincianQrOld($id)
    // {
    //     $surjal_rincian = SuratJalanRincian::find()->where(['surat_jalan_id'=>$id])->all();

    //     if (!empty($surjal_rincian)){
    //         foreach ($surjal_rincian as $rincian){
    //             $id_surjal_rincian = $rincian['id'];
    //             $odoo_updated = empty($rincian->odoo_updated)? 0 : $rincian->odoo_updated;
    //             $connection = Yii::$app->db;
    //             $update = $connection->createCommand("UPDATE qc_fg_v2 SET surat_jalan_id = ".$id.", odoo_updated = ".$odoo_updated." WHERE snfg = '".$rincian->snfg."' AND nourut = ".$rincian->nourut." ")->execute();

    //             $surjal = SuratJalan::find()->where(['id'=>$id])->one();
    //             if (strpos($surjal->nosj,'DRAFT')!==false){
    //                 $delete = Yii::$app->db->createCommand("DELETE FROM surat_jalan_rincian WHERE id = ".$id_surjal_rincian." ")->execute();
    //             }

    //         }
    //     }

    //     date_default_timezone_set('Asia/Jakarta');

    //     $model = new QcFgV2();

    //     $searchModel = new QcFgV2Search();
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    //     $dataProvider->query->where("surat_jalan_id=".$id."");



    //     //Editable Column check, if there is a post data
    //     if(Yii::$app->request->post('hasEditable'))
    //     {
    //         $operatorId = Yii::$app->request->post('editableKey');
    //         $operator = QcFgV2::findOne($operatorId);

    //         $out = Json::encode(['output'=>'','message'=>'']);
    //         $post = [];
    //         $posted = current($_POST['QcFgV2']);
    //         $post['QcFgV2'] = $posted;
    //         if($operator->load($post))
    //         {
    //             $operator->save();

    //         }
    //         echo $out;
    //         return;
    //     }

    //     // Get Related Information

    //     if ($model->load(Yii::$app->request->post())){
    //         $alokasi_surjal = SuratJalan::find()->where(['id'=>$id])->one()['alokasi'];
    //         $surjal_rincian = SuratJalanRincian::find()->where(['surat_jalan_id'=>$id])->all();

    //         // if (!empty($surjal_rincian)){
    //         //     foreach ($surjal_rincian as $rincian){
    //         //         $id_surjal_rincian = $rincian['id'];
    //         //         $connection = Yii::$app->db;
    //         //         $update = $connection->createCommand("UPDATE qc_fg_v2 SET surat_jalan_id = ".$id." WHERE snfg = '".$rincian->snfg."' AND nourut = ".$rincian->nourut." ")->execute();

    //         //         // $delete = Yii::$app->db->createCommand("DELETE FROM surat_jalan_rincian WHERE id = ".$id_surjal_rincian." ")->execute();
    //         //     }
    //         // }

    //         if (!empty($model->id_palet) && empty($model->snfg)){
    //             $data_palet = QcFgV2::find()->where(['id_palet'=>$model->id_palet])->one();
    //         }else{
    //             $data_palet = QcFgV2::find()->where(['and',['snfg'=>$model->snfg],['nourut'=>$model->nourut]])->one();
    //         }

    //         if (empty($alokasi_surjal)){
    //             $update_alokasi_surjal = Yii::$app->db
    //                                         ->createCommand("
    //                                             UPDATE surat_jalan
    //                                             SET alokasi = :alokasi
    //                                             WHERE id = :id;",
    //                                             [
    //                                                 ':alokasi' => $data_palet['alokasi'],
    //                                                 ':id' => $id,
    //                                             ]
    //                                         )->execute();
    //             $alokasi_surjal = $data_palet['alokasi'];
    //         }

    //         if (!empty($data_palet['surat_jalan_id']) || $data_palet['surat_jalan_id'] != ''){
    //             $exist = 1;
    //         }else{
    //             $exist = 0;
    //         }

    //         if (!empty($data_palet['timestamp_terima_karantina']) ){
    //             $karantina = 1;
    //         }else{
    //             $karantina = 0;
    //         }

    //         if($exist == 0){

    //           if ( $data_palet['alokasi'] == $alokasi_surjal || ($data_palet['alokasi'] != $alokasi_surjal && strpos($data_palet['alokasi'],'NDC') !== false) ){

    //             if($karantina == 1){
    //                 $connection = Yii::$app->getDb();
    //                 $command = $connection->createCommand("UPDATE qc_fg_v2 SET surat_jalan_id = ".$id." ,timestamp_surjal = current_timestamp WHERE snfg = '".$data_palet['snfg']."'  and nourut = ".$data_palet['nourut']." ");
    //                 $update = $command->execute();
    //                 // $model->save();
    //                 // // $model->save();
    //                 // $connection = Yii::$app->getDb();
    //                 // $command = $connection->createCommand("INSERT INTO qc_fg_v2 (snfg,nourut,jumlah_koli)
    //                 //                                         VALUES (
    //                 //                                             :snfg,
    //                 //                                             :nourut,
    //                 //                                             :jumlah_koli
    //                 //                                         );
    //                 //                                         ",
    //                 //                                         [
    //                 //                                             ':snfg' => $model->snfg,
    //                 //                                             ':nourut' => $model->nourut,
    //                 //                                             ':jumlah_koli' => $model->jumlah_koli
    //                 //                                         ]);
    //                 // $insert = $command->execute();


    //                 // $surjal_rincian_id = SuratJalanRincian::find()->where('and',['snfg'=>$model->snfg],['nourut'=>$model->nourut])->one()

    //                 // $connection2 = Yii::$app->getDb();
    //                 // $command2 = $connection2->createCommand("

    //                 //       UPDATE surat_jalan_rincian sjr
    //                 //       SET
    //                 //           alokasi=q.alokasi,
    //                 //           qty=q.qty_karantina,
    //                 //           nama_fg =q.nama_fg,
    //                 //           jumlah_koli =q.jumlah_koli ,
    //                 //           nobatch =q.nobatch
    //                 //       FROM (select a.*, f.nama_fg from qc_fg_v2 a
    //                 //             left join (select distinct snfg,fg_name_odoo as nama_fg from flow_input_snfg) f on a.snfg=f.snfg
    //                 //            )  q
    //                 //       WHERE sjr.snfg = q.snfg  and sjr.nourut = q.nourut and sjr.id = ".$model->id.";
    //                 //       ");

    //                 // $result2 = $command2->queryAll();

    //                 return $this->redirect(['create-surjal-rincian-qr', 'id' => $id]);

    //             }else{
    //                 return $this->redirect(['surat-jalan-rincian/belum-karantina']);
    //             }

    //           }else{
    //               return $this->redirect(['surat-jalan-rincian/beda-alokasi']);
    //           }

    //         }else{
    //             return $this->redirect(['surat-jalan-rincian/sudah-scan','snfg'=>$data_palet['snfg'],'nourut'=>$data_palet['nourut']]);
    //         }

    //     } else {

    //         return $this->render('create-surjal-rincian-qr', [
    //             'model' => $model,
    //             'id' => $id,
    //             'searchModel' => $searchModel,
    //             'dataProvider' => $dataProvider,
    //         ]);
    //     }
    // }

    public function actionCekAlokasiKirim($id)
    {
        $sql = "SELECT DISTINCT ON (alokasi) alokasi, id_palet,id FROM qc_fg_v2 WHERE surat_jalan_id = ".$id." ";
        $cek_alokasi = QcFgV2::findBySql($sql)->all();
        if (count($cek_alokasi)==1){
            echo 1;
        }else{
            echo 0;
        }


    }

    // public function actionConfirmSuratJalanOld($id)
    // {


    //     //  Update Null Surat Jalan Document Number refer to the Surat Jalan ID
    //     $connection = Yii::$app->getDb();
    //     $command = $connection->createCommand("

    //       UPDATE surat_jalan
    //       SET
    //           nosj=concat('".date('Y-m-d')."','/',id),
    //           is_received = 1,
    //           sync_wmsndc = 'WAIT'
    //       WHERE id = ".$id.";
    //       ");

    //     $result = $command->execute();
    //     $nosj = SuratJalan::find()->where(['id'=>$id])->one()['nosj'];

    //     $cek_log_receive = LogReceiveNdc::find()->where(['nosj'=>$nosj])->all();
    //     $list_log_receive = [];
    //     foreach ($cek_log_receive as $data){
    //         $list_log_receive[] = $data['id_palet'];
    //     }


    //     if($result){
    //         $list_palet = QcFgV2::find()->where(['surat_jalan_id'=>$id])->all();
    //         foreach ($list_palet as $palet){

    //             // Insert into Surat Jalan Rincian
    //             // Rama Please confirm kenapa adding item via QR / Input Manual ID Palet tidak menginsert ke Surat Jalan Rincian? tetapi hanya update status di qc_fg_v2 ?
    //             // if (!in_array($palet['id_palet'],$list_log_receive)){
    //             //     $connection = Yii::$app->getDb();
    //             //     $command = $connection->createCommand("
    //             //         INSERT INTO surat_jalan_rincian (surat_jalan_id,snfg,nourut,qty,nama_fg,jumlah_koli,nobatch,alokasi)
    //             //         SELECT
    //             //             :surat_jalan_id as surat_jalan_id,
    //             //             :snfg as snfg,
    //             //             :nourut as nourut,
    //             //             :qty as qty,
    //             //             :nama_fg as nama_fg,
    //             //             :jumlah_koli as jumlah_koli,
    //             //             :nobatch as nobatch,
    //             //             :alokasi as alokasi
    //             //         ",
    //             //         [
    //             //          ':surat_jalan_id' => $id,
    //             //          ':snfg' => $palet->snfg,
    //             //          ':nourut' => $palet->nourut,
    //             //          ':qty'=>$palet->qty_karantina,
    //             //          ':nama_fg' => $palet->nama_fg,
    //             //          ':jumlah_koli' => $palet->jumlah_koli,
    //             //          ':nobatch' => $palet->nobatch,
    //             //          ':alokasi' => $palet->alokasi
    //             //         ]
    //             //     );

    //             //     $insert = $command->execute();
    //             // }


    //             //Insert into log receive ndc

    //             if (!in_array($palet['id_palet'],$list_log_receive)){
    //                 $connection = Yii::$app->getDb();
    //                 $command = $connection->createCommand("
    //                     INSERT INTO log_receive_ndc (id_palet,nosj,nama_fg,snfg,nourut,nobatch,qty,odoo_code,expired_date)
    //                     SELECT  :id_palet as id_palet,
    //                             :nosj as nosj,
    //                             :nama_fg as nama_fg,
    //                             :snfg as snfg,
    //                             :nourut as nourut,
    //                             :nobatch as nobatch,
    //                             :qty as qty,
    //                             s.odoo_code as odoo_code,
    //                             f.exp_date as expired_date
    //                     FROM (
    //                         SELECT DISTINCT on (snfg) *
    //                         FROM scm_planner
    //                         WHERE odoo_code is not null
    //                     ) s
    //                     LEFT JOIN (
    //                         SELECT DISTINCT on (snfg,nobatch) *
    //                         FROM flow_input_snfg
    //                     ) f on f.snfg=:snfg and f.nobatch=:nobatch
    //                     WHERE s.snfg=:snfg and f.nobatch=:nobatch
    //                     ",
    //                     [
    //                      ':id_palet' => $palet->id_palet,
    //                      ':nosj' => $nosj,
    //                      ':nama_fg' => $palet->nama_fg,
    //                      ':snfg' => $palet->snfg,
    //                      ':nourut' => $palet->nourut,
    //                      ':nobatch' => $palet->nobatch,
    //                      ':qty'=>$palet->qty_karantina
    //                     ]
    //                 );

    //                 $insert = $command->execute();
    //             }


    //         }

    //         return $this->redirect(Yii::$app->request->referrer);
    //     }else{
    //         Yii::$app->session->setFlash('danger','Gagal konfirmasi surat jalan');
    //         return $this->redirect(Yii::$app->request->referrer);
    //     }

    // }

    public function actionPdf($id) {
        // Your SQL query here
        //$model = new ApTable();
        date_default_timezone_set('Asia/Jakarta');

        $model = new SuratJalanRincian();

           $sql = "SELECT nosj,timestamp
                   FROM surat_jalan
                   WHERE id=".$id."
                   LIMIT 1
                   ";
           $suratjalan = SuratJalan::findBySql($sql)->one();
           $nosj = $suratjalan->nosj;
           $timestamp = $suratjalan->timestamp;

        $searchModel = new QcFgV2Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("surat_jalan_id=".$id." ");

        $content =  $this->renderPartial('report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'nosj' => $nosj,
            'timestamp' => $timestamp,
            'id' => $id,
        ]);

        // $content = $this->renderPartial('report', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        //     'nalang' => $nalang,
        //     'tglrencanabayar' => $tglrencanabayar,
        //     'norek' => $norek,
        //     'nama_rekening' => $nama_rekening,
        //     'bank' => $bank,
        //     'bayar_via' => $bayar_via,
        // ]);

        // $content = $this->renderPartial('index', ['model' => $model]);

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_CORE,
        // A4 paper format
        'format' => Pdf::FORMAT_A4,
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT,
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER,
        // your html content input
        'content' => $content,
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting
        'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
        'cssInline' => '.kv-heading-1{font-size:18px}',
        // set mPDF properties on the fly
        'options' => ['title' => 'Surat Jalan - Flowreport - '.$nosj],
        // call mPDF methods on the fly
        'methods' => [
        'SetHeader'=>['Flowreport - PT Paragon Technology & Innovation'],
        'SetFooter'=>['<div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>QC Karantina</th>
              <th>Driver NDC</th>
              <th>Security</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td><br></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>(.......................)</td>
              <td>(.......................)</td>
              <td>(.......................)</td>
            </tr>
            </tbody>
          </table>
        </div> {PAGENO}'],
        ]
        ]);

        /*------------------------------------*/
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        /*------------------------------------*/

        // return the pdf output as per the destination setting
        return $pdf->render();

        // return $content->render();
    }

    public function actionScanKonfirmasiTerimaNdcQr()
    {

        $this->layout = '//main-qr-layout';

        return $this->render('scan-konfirmasi-terima-ndc-qr', [
        ]);
    }

    /**
      * actionScanKarantina
      * Get List of Nourut for a SNFG with status 'SIAP KIRIM KE NDC'
      * @author Redha Hari
      * @param string $snfg
      * @return string list of nourut in HTML List for Dropdown.
    */
    public function actionGetNourutKarantina($snfg)
    {
        $countQcFg = QcFgV2::find()
                        ->where(['snfg' => $snfg])
                        ->count();


        $sql = "
                SELECT nourut
                FROM qc_fg_v2
                WHERE snfg='".$snfg."' and status='SIAP KIRIM KE NDC'
                ORDER by nourut
                ";

        $QcFgs = QcFgV2::findBySql($sql)->all();

        if($countQcFg > 0)
        {
            foreach($QcFgs as $QcFg){
                echo "<option value='".$QcFg->nourut."'>".$QcFg->nourut."</options>";
            }

        }
        else{
                echo "<option>SNFG Tidak Valid</option>";
        }
    }


    /**
     * Updates an existing QcFgV2 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
      * actionScanKarantina
      * Render QR Scan Karantina Page
      * @author Redha Hari
      * @return void redirect to scan-karantina.
    */
    public function actionScanKarantina()
    {
        $model = new QcFgV2();

        return $this->render('scan-karantina', [
            'model' => $model,
        ]);
    }

    public function actionCheckIdPalet($id_palet)
    {
        $palet = QcFgV2::find()->where(['id_palet'=>$id_palet])->one();
        if (!empty($palet)){
            $value = ['status'=>'exist','snfg'=>$palet->snfg,'nourut'=>$palet->nourut];
            echo Json::encode($value);
        }else{
            $value = ['status'=>'null'];
            echo Json::encode($value);
        }
    }
    /**
      * actionUpdateKarantina
      *
      * on QR scan _form-scan-karantina
      * Normal : If POST Add Timestamp Karantina, and status to 'SIAP KIRIM NDC' for an nourut and snfg in qc_fg_v2 table, else Render Form Page.
      * Exception : Detects if POST/Previously Updated then redirect to summary page.
      *
      * @author Redha Hari
      * @param string $snfg
      * @param int $nourut
      * @return void if POST/Previously Updated then redirect to summary page else render form page.
      */
    public function actionUpdateKarantina($snfg=null,$nourut=null,$id_palet=null)
    {

        if (!empty($id_palet) && empty($snfg)){
            // Get Value
            $sql="
                    select
                        id,
                        timestamp_terima_karantina
                    from qc_fg_v2
                    where id_palet='".$id_palet."'
                    limit 1
                 ";
            $arr = QcFgV2::findBySql($sql)->one();
        }else{
            // Get Value
            $sql="
                    select
                        id,
                        timestamp_terima_karantina
                    from qc_fg_v2
                    where snfg='".$snfg."' and nourut=".$nourut."
                    limit 1
                 ";
            $arr = QcFgV2::findBySql($sql)->one();
        }

        $id = $arr['id'];

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())){

            date_default_timezone_set('Asia/Jakarta');
            $current_time = date("H:i:s");


            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE qc_fg_v2
            SET timestamp_terima_karantina = :current_time,
                status='SIAP KIRIM KE NDC',
                inspektor_karantina= :inspektor_karantina
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),
            ':id'=> $id,
            ':inspektor_karantina'=> $model->inspektor_karantina,
            ]);

            $result = $command->queryAll();

            // $model->save();

            return $this->redirect(['view-karantina', 'id' => $model->id]);
        } else {


            if(empty($arr['timestamp_terima_karantina'])){
                return $this->render('update-karantina', [
                    'model' => $model,
                ]);
            }else{
                return $this->redirect(['view-karantina', 'id' => $arr->id]);
            }
        }
    }



    /**
      * actionIsDone
      * Update Is Done value to 1 for a SNFG in qc_fg_v2 table.
      *
      * @author Redha Hari
      * @param string $snfg
      * @return void redirect to previous page.
    */
    public function actionIsDone($snfg)
    {

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
                UPDATE qc_fg_v2
                SET is_done = 1
                WHERE snfg = :snfg;
            ",
            [':snfg'=> $snfg]);

        $result = $command->queryAll();

        // return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
      * actionUndone
      * Update Is Done value to null for a SNFG in qc_fg_v2 table.
      *
      * @author Redha Hari
      * @param string $snfg
      * @return void redirect to previous page.
    */
    public function actionUndone($snfg)
    {

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
                UPDATE qc_fg_v2
                SET is_done = null
                WHERE snfg = :snfg;
            ",
            [':snfg'=> $snfg]);

        $result = $command->queryAll();

        // return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
      * actionPrintLabelTest
      * Print ZPL Label in PDF
      *
      * @author Redha Hari
      * @param int $id
      * @return file PDF File.
    */
    public function actionPrintLabeltest($id)
    {


        $model = $this->findModel($id);

        $sql="
                select distinct nama_fg
                from scm_planner
                where snfg='".$model->snfg."'
                limit 1
             ";
        $arr = ScmPlanner::findBySql($sql)->one();

        $sql="
                select fg_name_odoo, exp_date,nobatch
                from flow_input_snfg
                where snfg='".$model->snfg."' order by datetime_start desc
                limit 1
             ";
        $fis = FlowInputSnfg::findBySql($sql)->one();

        $nama_fg = $fis->fg_name_odoo;

            // $txt = "


            // ^XA
            // ^MMT
            // ^PW609
            // ^LL0406
            // ^LS0

            // ^FO0,30^A0N,50,50^FB440,1,0,C^FD".$model->status_check."\&^FS
            // ^FT0,145^A0N,20,20^FB440,2,0,C^FD".$nama_fg."\&^FS
            // ^FO450,5^BY5,1.0,10^BQN,2,5^FDMA,".$model->snfg."@".$model->nourut."@".$model->jumlah_koli."^FS

            // ^BY2,2,60^FT25,230^BCN,,Y,N
            // ^FD>:".$model->snfg."^FS

            // ^FO350,340^A0N,30,30^FH\^FDPALET KE-".$model->nourut."^FS
            // ^FO350,300^A0N,30,30^FH\^FD".$model->qty." Pcs^FS

            // ^FO0,260^GFA,4125,4125,33,,:::gM0UFE,gM07TF8,gM01SFE,gL0187RFC2,gM0C1RF0E,gM0707PFC1C,gM0783PF078,gM03E0OFC0F8,gM01F03NF83F,gM01FC0MFE07F,gN0FE03LF81FE,gN0FF00KFE03FC,gN07FC037FF980FFC,gN03FE00800201FF8,gN03FC8L07FF,gN01FEM0IF,gN01FFL01FFE,gO0FF8K04FFE,gO07FCK01FFC,gO07FEK03FF8,gO03FFK07FF8,gO03FF8J07FF,gO01FF8J0IF,gP0FFCI01FFE,gP0FFEI01FFC,gP07FFI03FFC,gP03FF8007FF8,gP03FFC00IF8,gP01FFE00IF,gP01FFE81FFE,gQ0IF81FFE,gQ07FF87FFC,gQ07FF87FF8,gQ03FF87FF8,gQ03FF87FF,gQ01FF87FF,gR0FFC7FE,gR0FFC7FC,gR07FC7FC,gR07FC7F8,gR03FC7F8,gR01FC7F,gR01FC7E,gS0FC7E,gS07C7C,gS07C78,gS03C78,gS03C7,gS01C7,gT0CE,gT0CC,gT04C,gT048,,:::::::::M01IFK03EJ07FFEL03EK01IF8J0IFJ03EJ0E,M01IFEJ07EJ07IFCK03FK07IFEI03IFCI03FJ0E,M01JFJ07FJ07IFEK07FJ01JFCI0FF0FFI03F8I0E,M01C00F8I077J07803FK077J03FI0C001F001F8003F8I0E,M01C007CI0E7J07I0F8J0778I07CL03CI07C003BCI0E,M01C003CI0E78I07I078J0E38I0F8L078I03E0039CI0E,M01C001E001E38I07I038J0E38001FM0FJ01E0039EI0E,M01C001E001C38I07I03CI01C3C001EM0EK0F0038EI0E,M01CI0E001C3CI07I03CI01C1C003CL01EK070038FI0E,M01CI0E00381CI07I03CI01C1C003CL01CK0780387I0E,M01CI0E00381CI07I03CI0381E0038L03CK0380387800E,M01CI0E00781EI07I03CI0380E0038L03CK0380383C00E,M01CI0E00700EI07I03CI0780E0078L038K0380381C00E,M01C001E00700EI07I03CI0700F0078L038K03C0381E00E,M01C001E00F00FI07I078I070070078L038K03C0380E00E,M01C003C00E007I07I078I0F007807M038K01C0380F00E,M01C007C00E0078007I0FJ0E007807M038K01C0380700E,M01C01F801E0078007803EI01E003807K0E038K01C0380780E,M01C7FF001JF80070FFCI01JFC078J0E038K03C0380380E,M01C7FC003JFC0071FF8I01JFC078J0E038K03C03803C0E,M01C7EI03JFC0071FF8I03JFC078J0E038K03C03801E0E,M01CK038001C0070078I038001E078J0E03CK03803800E0E,M01CK07I01E007803CI038001E038J0E03CK03803800F0E,M01CK07J0E007001EI07J0E03CJ0E01CK0780380070E,M01CK0FJ0E007001EI07J0F03CJ0E01EK0700380078E,M01CK0EJ0F007I0FI0FJ0701EJ0E01EK0F00380038E,M01CK0EJ07007I0F800EJ0701FJ0E00FJ01E0038003CE,M01CJ01EJ07007I07800EJ0780F8I0E0078I01E0038001CE,M01CJ01CJ07807I03C01EJ03807CI0E007CI07C0038001EE,M01CJ03CJ03807I03C01CJ03803FI0E003FI0F80038I0FE,M01CJ038J03807I01E03CJ03C01FF03E001FC07FI038I0FE,M01CJ038J03C07J0F038J01C007IFEI07IFCI038I07E,M03CJ078J01C078I0F038J01E001IFCI01IFJ038I03F,h01F8K01F,,:::::::::M01FCE86441182198406C0C8840102085CI0821041B10210FE4330C1,N0208080411C230440821008402830802I0C21842090228I0418C1,N02080804114220441011004802808801I0220044048228K08A1,N02081004112240241012004800428801I0A20I4048404I080091,N020C1006112240241002003004424801I0920240040I4I080491,N02081004I1240241002042007C24801I092024004487CI080489,N02081004I1200241012042008I2801I08A0144044882I080889,N02080804110A20400811042008I2801I0800144080882I040885,N02080404110610840C20842I0221806I08600C218300210461083,P0683CJ040F03C3C078M087K020041E001K01E002,,:::^FS

            // ^PQ1,0,1,Y^XZ
            // ";

            $txt = "
            ^FX Begin setup
            ^XA
            ~TA120
            ~JSN
            ^LT0
            ^MNW
            ^MTT
            ^PON
            ^PMN
            ^LH0,0
            ^JMA
            ^PR4,4
            ~SD25
            ^JUS
            ^LRN
            ^CI0
            ^MD0
            ^CI13,196,36
            ^XZ
            ^FX End setup

            ^FX Begin label format
            ^XA
            ^MMT
            ^LL0264
            ^PW583
            ^LS0

            ^FX Line Vertical
            ^FO400,20
            ^GB0,190,2^FS

            ^FX Line Vertical
            ^FO150,210
            ^GB0,45,2^FS

            ^FX Line Vertical
            ^FO260,210
            ^GB0,140,2^FS

            ^FX Line Vertical
            ^FO340,255
            ^GB0,50,2^FS

            ^FX Line Vertical
            ^FO420,210
            ^GB0,140,2^FS

            ^FX Line Horizontal First
            ^FO20,70
            ^GB380,0,2^FS

            ^FX Line Horizontal
            ^FO20,160
            ^GB380,0,2^FS

            ^FX Line Horizontal
            ^FO20,210
            ^GB560,0,2^FS

            ^FX Line Horizontal
            ^FO20,255
            ^GB560,0,2^FS

            ^FX Line Horizontal
            ^FO260,305
            ^GB160,0,2^FS

            ^FX Nama Status
            ^FO20,15
            ^ARN,16
            ^FX Field block 507 dots wide, 2 lines max
            ^FB400,2,,C,
            ^FDRELEASE^FS

            ^FX Keterangan Pending
            ^FO20,45
            ^ALN,20,20
            ^FX Field block 507 dots wide, 2 lines max
            ^FB400,2,,C,
            ^FD ^FS

            ^FX QR kode BB
            ^FT415,180,0
            ^BQN,2,5,H,7
            ^FDMA,".$model->snfg."@".$model->nourut."@".$model->jumlah_koli."@".$model->qty."@".$arr->odoo_code."^FS

            ^FX Nama Item Label
            ^FO30,78
            ^ADN5,5
            ^FDNama Item^FS

            ^FX Nama Item Value
            ^FO20,100
            ^A0N10,20
            ^FX Field block 507 dots wide, 3 lines max
            ^FB380,3,,C,
            ^FD".$fis->fg_name_odoo."^FS

            ^FX SNFG Label
            ^FO20,167
            ^ABN5,5
            ^FD SNFG^FS

            ^FX SNFG Value
            ^FO38,185
            ^ADN10,20
            ^FX Field block 507 dots wide, 2 lines max
            ^FB350,1,,C,
            ^FD".$model->snfg."^FS

            ^FX Qty Koli Label
            ^FO265,260
            ^ABN5,5
            ^FDQty.Koli^FS

            ^FX Qty Koli Value
            ^FO265,280
            ^AQN10,10
            ^FX Field block 507 dots wide, 1 lines max
            ^FB70,1,,C,
            ^FD".$model->jumlah_koli."^FS

           ^FX Inspektor QC Label
            ^FO270,215
            ^ABN5,5
            ^FDQCFG^FS

            ^FX Inspektor QC Value
            ^FO270,233
            ^A0N5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB140,1,,C,
            ^FD".$model->inspektor_qcfg."^FS

            ^FX Kode Odoo Label
            ^FO160,215
            ^ABN5,5
            ^FDKode Odoo^FS

            ^FX Kode Odoo Value
            ^FO160,233
            ^ACN5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB100,1,,C,
            ^FD".$arr->odoo_code."^FS

            ^FX Exp Date Label
            ^FO430,215
            ^ABN5,5
            ^FDExp.Date^FS

            ^FX Exp Date Value
            ^FO430,233
            ^ACN5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB150,1,,C,
            ^FD".$fis->exp_date."^FS

            ^FX No.Urut Label
            ^FO345,260
            ^ABN5,5
            ^FDNo.Urut^FS

            ^FX No.Urut Value
            ^FO343,280
            ^AJN5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB70,1,,C,
            ^FD".$model->nourut."^FS

            ^FX No.Batch Label
            ^FO20,215
            ^ABN5,5
            ^FD No. Batch^FS

            ^FX No.Batch Value
            ^FO28,232
            ^ACN10,20
            ^FX Field block 507 dots wide, 2 lines max
            ^FB110,1,,C,
            ^FD".$fis->nobatch."^FS

           ^FX Qty per palet Title
           ^FO430,260
           ^ABN5,5
           ^FX Field block 507 dots wide, 3 lines max
           ^FB150,1,,C
           ^FDQty per Palet^FS

           ^FX Qty per Palet human readable
           ^FO425,285
           ^APN,95,95
           ^FX Field block 507 dots wide, 3 lines max
           ^FB160,1,,C
           ^FD".$model->qty."^FS

            ^FX Keterangan Label
            ^FO30,263
            ^ABN5,5
            ^FDKeterangan^FS

            ^FX Keterangan Value
            ^FO30,282
            ^ACN10,10
            ^FX Field block 507 dots wide, 2 lines max
            ^FB230,2,,L,
            ^FD ^FS

            ^FX Alokasi Label
            ^FO270,310
            ^ABN5,5
            ^FDAlokasi^FS

            ^FX Alokasi Value
            ^FO270,325
            ^ACN10,19
            ^FX Field block 507 dots wide, 1 lines max
            ^FB150,2,,C,
            ^FD".$model->alokasi."^FS

            ^FX Print quantity
            ^PQ1,0,1,Y
            ^FX End label format
            ^XZ
            ";

            $curl = curl_init();
            // adjust print density (8dpmm), label width (4 inches), label height (6 inches), and label index (0) as necessary
            curl_setopt($curl, CURLOPT_URL, "http://api.labelary.com/v1/printers/8dpmm/labels/3x2/0/");
            curl_setopt($curl, CURLOPT_POST, TRUE);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $txt);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Accept: application/pdf")); // omit this line to get PNG images back
            $result = curl_exec($curl);

            if (curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200) {
                $file = fopen("label.pdf", "w"); // change file name for PNG images
                fwrite($file, $result);
                fclose($file);

            } else {
                print_r("Error: $result");
            }

            curl_close($curl);


            $pdf = '/var/www/html/flowreport/web/label.pdf';
        return (Yii::$app->response->sendFile($pdf));
    }

    /**
      * actionPrintLabel
      * Print ZPL Label in PDF
      *
      * @author Redha Hari
      * @param int $id
      * @return file PDF File.
    */
    public function actionPrintLabel($id)
    {


        $model = $this->findModel($id);

        $sql="
                select distinct nama_fg,odoo_code
                from scm_planner
                where snfg='".$model->snfg."'
                limit 1
             ";

        $arr = ScmPlanner::findBySql($sql)->one();

        $sql="
                select fg_name_odoo, exp_date
                from flow_input_snfg
                where snfg='".$model->snfg."' and nobatch = '".$model->nobatch."' order by datetime_start desc
                limit 1
             ";
        $fis = FlowInputSnfg::findBySql($sql)->one();

        if (empty($fis)){

            $sql="
                    select fg_name_odoo, exp_date
                    from flow_pra_kemas
                    where snfg='".$model->snfg."' and nobatch = '".$model->nobatch."' order by datetime_start desc
                    limit 1
                 ";
            $fpk = FlowPraKemas::findBySql($sql)->one();
            if (empty($fpk)){
                Yii::$app->session->setFlash('danger','Nomor Batch tidak ditemukan pada record Kemas, pastikan Nomor Batch sudah benar!');
                return $this->redirect(Yii::$app->request->referrer);
            }else{
                $nama_fg = $fpk->fg_name_odoo;
            }

        }else{
            $nama_fg = $fis->fg_name_odoo;
        }


        // I : ID table
        // O : Kode Odoo
        // Q : Quantity per trolley
        // N : nomor trolley
        // B : nomor batch
        // E : expired date

        // $nama_fg = $arr->nama_fg;
        // $odoo_code = $arr->odoo_code;
        $is_trial_varcos = ScmPlanner::find()->where("snfg = '".$model->snfg."' and keterangan ilike '%Trial Produksi Varcos di J4%' ")->one();

            $txt3 = "
            ^FX Begin setup
            ^XA
            ~TA120
            ~JSN
            ^LT0
            ^MNW
            ^MTT
            ^PON
            ^PMN
            ^LH0,0
            ^JMA
            ^PR4,4
            ~SD25
            ^JUS
            ^LRN
            ^CI0
            ^MD0
            ^CI13,196,36
            ^XZ
            ^FX End setup

            ^FX Begin label format
            ^XA
            ^MMT
            ^LL0264
            ^PW583
            ^LS0

            ^FX Line Vertical
            ^FO370,20
            ^GB0,190,2^FS

            ^FX Line Vertical
            ^FO150,210
            ^GB0,95,2^FS

            ^FX Line Vertical
            ^FO260,210
            ^GB0,95,2^FS

            ^FX Line Vertical
            ^FO340,255
            ^GB0,50,2^FS

            ^FX Line Vertical
            ^FO420,210
            ^GB0,140,2^FS

            ^FX Line Horizontal First
            ^FO20,55
            ^GB350,0,2^FS

            ^FX Line Horizontal
            ^FO20,160
            ^GB350,0,2^FS

            ^FX Line Horizontal
            ^FO20,210
            ^GB560,0,2^FS

            ^FX Line Horizontal
            ^FO20,255
            ^GB560,0,2^FS

            ^FX Line Horizontal
            ^FO20,305
            ^GB400,0,2^FS

            ^FX Nama Status
            ^FO20,15
            ^ARN,16
            ^FX Field block 507 dots wide, 2 lines max
            ^FB370,2,,C,
            ^FD".$model->status_check."^FS

            ^FX Keterangan Pending
            ^FO20,45
            ^ALN,20,20
            ^FX Field block 507 dots wide, 2 lines max
            ^FB400,2,,C,
            ^FD ^FS

            ^FX QR kode BB
            ^FT385,210,0
            ^BQN,2,5,H,7
            ^FDMA,I:".$model->id_palet."|O:".$model->odoo_code."|Q:".$model->qty."|N:".$model->nourut."|B:".$model->nobatch."|E:".$model->exp_date."^FS

            ^FX Nama Item Label
            ^FO30,60
            ^ADN5,5
            ^FDNama Item^FS

            ^FX Nama Item Value
            ^FO20,81
            ^A0N10,20
            ^CI28
            ^FX Field block 507 dots wide, 3 lines max
            ^FB350,4,,C,
            ^FD".$nama_fg."^FS

            ^FX SNFG Label
            ^FO20,167
            ^ABN5,5
            ^FD SNFG^FS

            ^FX SNFG Value
            ^FO38,185
            ^ADN10,20
            ^FX Field block 507 dots wide, 2 lines max
            ^FB350,1,,C,
            ^FD".$model->snfg."^FS

            ^FX Keterangan Label
            ^FO30,310
            ^ABN5,5
            ^FDKeterangan^FS

            ^FX Keterangan Value
            ^FO35,325
            ^ACN10,10
            ^FX Field block 507 dots wide, 1 lines max
            ^FB390,1,,L,
            ^FD^FS

            ^FX Qty Koli Label
            ^FO265,260
            ^ABN5,5
            ^FDQty.Koli^FS

            ^FX Qty Koli Value
            ^FO265,280
            ^AJN5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB70,1,,C,
            ^FD".$model->jumlah_koli."^FS

           ^FX Inspektor QC Label
            ^FO270,215
            ^ABN5,5
            ^FDQCFG^FS

            ^FX Inspektor QC Value
            ^FO270,233
            ^A0N5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB140,1,,C,
            ^FD".$model->inspektor_qcfg."^FS

            ^FX Kode Odoo Label
            ^FO160,215
            ^ABN5,5
            ^FDKode Odoo^FS

            ^FX Kode Odoo Value
            ^FO160,233
            ^ACN5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB100,1,,C,
            ^FD".$model->odoo_code."^FS

            ^FX No Batch Label
            ^FO160,260
            ^ABN5,5
            ^FDNo Batch^FS

            ^FX No Batch Value
            ^FO160,278
            ^ACN10,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB100,1,,C,
            ^FD".$model->nobatch."^FS

            ^FX Exp Date Label
            ^FO430,215
            ^ABN5,5
            ^FDExp.Date^FS

            ^FX Exp Date Value
            ^FO430,233
            ^ACN5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB150,1,,C,
            ^FD".$model->exp_date."^FS

            ^FX No.Urut Label
            ^FO345,260
            ^ABN5,5
            ^FDNo.Urut^FS

            ^FX No.Urut Value
            ^FO343,280
            ^AJN5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB70,1,,C,
            ^FD".$model->nourut."^FS

           ^FX Qty per palet Title
           ^FO430,260
           ^ABN5,5
           ^FX Field block 507 dots wide, 3 lines max
           ^FB150,1,,C
           ^FDQty per Palet^FS

           ^FX Qty per Palet human readable
           ^FO425,285
           ^APN,75,75
           ^FX Field block 507 dots wide, 3 lines max
           ^FB160,1,,C
           ^FD".$model->qty."^FS

            ^FX ID Palet Label
            ^FO30,263
            ^ABN5,5
            ^FDID Palet^FS

            ^FX ID Palet Value
            ^FO30,282
            ^ACN10,10
            ^FX Field block 507 dots wide, 2 lines max
            ^FB120,2,,C,
            ^FD".$model->id_palet."^FS

            ^FX Alokasi Label
            ^FO30,215
            ^ABN5,5
            ^FD^FS

            ^FX Alokasi Value
            ^FO20,215
            ^ACN10,19
            ^FX Field block 507 dots wide, 1 lines max
            ^FB150,2,,C,
            ^FD".$model->alokasi."^FS

            ^FX Print quantity
            ^PQ1,0,1,Y
            ^FX End label format
            ^XZ
            ";


            $curl = curl_init();
            // adjust print density (8dpmm), label width (4 inches), label height (6 inches), and label index (0) as necessary
            curl_setopt($curl, CURLOPT_URL, "http://api.labelary.com/v1/printers/8dpmm/labels/3x2/0/");
            curl_setopt($curl, CURLOPT_POST, TRUE);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $txt3);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Accept: application/pdf")); // omit this line to get PNG images back
            $result = curl_exec($curl);

            if (curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200) {
                $file = fopen("label.pdf", "w"); // change file name for PNG images
                fwrite($file, $result);
                fclose($file);

            } else {
                print_r("Error: $result");
            }

            curl_close($curl);


            $pdf = Yii::getAlias("@dirWeb").'/label.pdf';
        return (Yii::$app->response->sendFile($pdf));
    }

    public function actionPrinterMapping($id){
        //Get Frontend IP
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $client_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $client_ip = $_SERVER['REMOTE_ADDR'];
        }

        //Check if frontend device have connection to multiple printer
        $printers = Yii::$app->db->createCommand("SELECT * FROM device_map_qcfg WHERE frontend_ip = '".$client_ip."' ")->queryAll();
        //Frontend device doesn't have multiple printer
        if (empty($printers)){
            return $this->redirect(['print-zebra','id'=>$id]);

        // //Frontend device has multiple printer
        }else{
            return $this->renderAjax('printer-mapping',[
                'printers'=>$printers,
                'id'=>$id,
            ]);
        }

    }

    /**
      * actionPrintZebra
      * Print ZPL execute zebra local printing remotely.
      *
      * @author Redha Hari
      * @param int $id
      * @return exec execute zebra local printing remotely.
    */
    public function actionPrintZebra($id,$printer_id=null)
    {


        $model = $this->findModel($id);
        $kode_item = strtok($model->snfg,'/');

        $sql="
                select odoo_code
                from scm_planner
                where snfg='".$model->snfg."'
                limit 1
             ";
        $arr = ScmPlanner::findBySql($sql)->one();

        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $client_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $client_ip = $_SERVER['REMOTE_ADDR'];
        }


        if (!empty($printer_id)){
            $device = DeviceMapQcfg::find()->where(['id'=>$printer_id])->one();
            $sbc_ip = $device['sbc_ip'];
            $sbc_user = $device['sbc_user'];
        }else{
            Yii::$app->session->setFlash('danger','Device tidak terdaftar di database');
            return $this->redirect(['create-rincian','snfg'=>$model->snfg]);
            // $device = DeviceMapQcfg::find()->where(['frontend_ip'=>$client_ip])->one();
            // $sbc_ip = $device['sbc_ip'];
            // $sbc_user = $device['sbc_user'];
        }


        // $client_ip = $_SERVER['REMOTE_ADDR'];
        $client_id = str_replace(".","",$client_ip);
        // $is_trial_varcos = ScmPlanner::find()->where("snfg = '".$model->snfg."' and sediaan ilike 'V' ")->one();
        // $is_trial_varcos = ScmPlanner::find()->where("snfg = '".$model->snfg."' and keterangan ilike '%Trial Produksi Varcos di J4%' ")->one();

        if(empty($sbc_ip)){
            Yii::$app->session->setFlash('danger','SBC Device tidak terdaftar di database');
        }

        $myfile = fopen("labelqcfg_".$client_id.".zpl", "w");

        //Cek apakah item tersebut FG atau Packaging
        $sql = "SELECT pt.default_code, pc.complete_name
                FROM product_template pt
                JOIN product_category pc on pt.categ_id = pc.id
                WHERE default_code = :kode_item";
        $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();

        if(empty($product_detail)){
            $sql = "SELECT pt.default_code, pc.complete_name
                FROM varcos.product_template pt
                JOIN varcos.product_category pc on pt.categ_id = pc.id
                WHERE default_code = :kode_item";
            $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();
        }

        if (!empty($product_detail)){
            if (strpos(strtolower($product_detail['complete_name']),'packaging')!==false){
                $category = 'packaging';
            }else{
                $category = 'fg';
            }
        }else{
            $category = 'null';
        }

        $sql="
            select fg_name_odoo, exp_date
            from flow_input_snfg
            where snfg='".$model->snfg."' and nobatch = '".$model->nobatch."' order by datetime_start desc
            limit 1
         ";
        $fis = FlowInputSnfg::findBySql($sql)->one();

        if(empty($fis))
        {
            $sql="
                select fg_name_odoo, exp_date
                from flow_pra_kemas
                where snfg='".$model->snfg."' and nobatch = '".$model->nobatch."' order by datetime_start desc
                limit 1
             ";
            $fis = FlowPraKemas::findBySql($sql)->one();

        }


        if (empty($fis)){
            Yii::$app->session->setFlash('danger','Nomor Batch tidak ditemukan pada record Kemas, pastikan Nomor Batch sudah benar!');
            return $this->redirect(Yii::$app->request->referrer);
        }

        $nama_fg = $fis->fg_name_odoo;

            $txt3 = "
            ^FX Begin setup
            ^XA
            ~TA120
            ~JSN
            ^LT0
            ^MNW
            ^MTT
            ^PON
            ^PMN
            ^LH0,0
            ^JMA
            ^PR4,4
            ~SD25
            ^JUS
            ^LRN
            ^CI0
            ^MD0
            ^CI13,196,36
            ^XZ
            ^FX End setup

            ^FX Begin label format
            ^XA
            ^MMT
            ^LL0264
            ^PW583
            ^LS0

            ^FX Line Vertical
            ^FO370,20
            ^GB0,190,2^FS

            ^FX Line Vertical
            ^FO150,210
            ^GB0,95,2^FS

            ^FX Line Vertical
            ^FO260,210
            ^GB0,95,2^FS

            ^FX Line Vertical
            ^FO340,255
            ^GB0,50,2^FS

            ^FX Line Vertical
            ^FO420,210
            ^GB0,140,2^FS

            ^FX Line Horizontal First
            ^FO20,55
            ^GB350,0,2^FS

            ^FX Line Horizontal
            ^FO20,160
            ^GB350,0,2^FS

            ^FX Line Horizontal
            ^FO20,210
            ^GB560,0,2^FS

            ^FX Line Horizontal
            ^FO20,255
            ^GB560,0,2^FS

            ^FX Line Horizontal
            ^FO20,305
            ^GB400,0,2^FS

            ^FX Nama Status
            ^FO20,15
            ^ARN,16
            ^FX Field block 507 dots wide, 2 lines max
            ^FB370,2,,C,
            ^FD".$model->status_check."^FS

            ^FX Keterangan Pending
            ^FO20,45
            ^ALN,20,20
            ^FX Field block 507 dots wide, 2 lines max
            ^FB400,2,,C,
            ^FD ^FS

            ^FX QR kode BB
            ^FT385,210,0
            ^BQN,2,5,H,7
            ^FDMA,I:".$model->id_palet."|O:".$model->odoo_code."|Q:".$model->qty."|N:".$model->nourut."|B:".$model->nobatch."|E:".$model->exp_date."^FS

            ^FX Nama Item Label
            ^FO30,60
            ^ADN5,5
            ^FDNama Item^FS

            ^FX Nama Item Value
            ^FO20,81
            ^A0N10,20
            ^CI28
            ^FX Field block 507 dots wide, 3 lines max
            ^FB350,4,,C,
            ^FD".$nama_fg."^FS

            ^FX SNFG Label
            ^FO20,167
            ^ABN5,5
            ^FD SNFG^FS

            ^FX SNFG Value
            ^FO20,185
            ^ADN10,20
            ^FX Field block 507 dots wide, 2 lines max
            ^FB350,1,,C,
            ^FD".$model->snfg."^FS

            ^FX Keterangan Label
            ^FO30,310
            ^ABN5,5
            ^FDKeterangan^FS

            ^FX Keterangan Value
            ^FO35,325
            ^ACN10,10
            ^FX Field block 507 dots wide, 1 lines max
            ^FB390,1,,L,
            ^FD^FS

            ^FX Qty Koli Label
            ^FO265,260
            ^ABN5,5
            ^FDQty.Koli^FS

            ^FX Qty Koli Value
            ^FO265,280
            ^AJN5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB70,1,,C,
            ^FD".$model->jumlah_koli."^FS

           ^FX Inspektor QC Label
            ^FO270,215
            ^ABN5,5
            ^FDQCFG^FS

            ^FX Inspektor QC Value
            ^FO270,233
            ^A0N5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB140,1,,C,
            ^FD".$model->inspektor_qcfg."^FS

            ^FX Kode Odoo Label
            ^FO160,215
            ^ABN5,5
            ^FDKode Odoo^FS

            ^FX Kode Odoo Value
            ^FO160,233
            ^ACN5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB100,1,,C,
            ^FD".$model->odoo_code."^FS

            ^FX No Batch Label
            ^FO160,260
            ^ABN5,5
            ^FDNo Batch^FS

            ^FX No Batch Value
            ^FO160,278
            ^ACN10,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB100,1,,C,
            ^FD".$model->nobatch."^FS

            ^FX Exp Date Label
            ^FO430,215
            ^ABN5,5
            ^FDExp.Date^FS

            ^FX Exp Date Value
            ^FO430,233
            ^ACN5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB150,1,,C,
            ^FD".$model->exp_date."^FS

            ^FX No.Urut Label
            ^FO345,260
            ^ABN5,5
            ^FDNo.Urut^FS

            ^FX No.Urut Value
            ^FO343,280
            ^AJN5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB70,1,,C,
            ^FD".$model->nourut."^FS

           ^FX Qty per palet Title
           ^FO430,260
           ^ABN5,5
           ^FX Field block 507 dots wide, 3 lines max
           ^FB150,1,,C
           ^FDQty per Palet^FS

           ^FX Qty per Palet human readable
           ^FO425,285
           ^APN,75,75
           ^FX Field block 507 dots wide, 3 lines max
           ^FB160,1,,C
           ^FD".$model->qty."^FS

            ^FX ID Palet Label
            ^FO30,263
            ^ABN5,5
            ^FDID Palet^FS

            ^FX ID Palet Value
            ^FO30,282
            ^ACN10,10
            ^FX Field block 507 dots wide, 2 lines max
            ^FB120,2,,C,
            ^FD".$model->id_palet."^FS

            ^FX Alokasi Label
            ^FO30,215
            ^ABN5,5
            ^FD^FS

            ^FX Alokasi Value
            ^FO20,215
            ^ACN10,19
            ^FX Field block 507 dots wide, 1 lines max
            ^FB150,2,,C,
            ^FD".$model->alokasi."^FS

            ^FX Print quantity
            ^PQ1,0,1,Y
            ^FX End label format
            ^XZ
            ";



            fwrite($myfile, $txt3);
            fclose($myfile);


            // echo file_get_contents( "labelsnfg.prn" );

        // echo 'scp /var/www/html/flowreport_training5/web/labelqcfg_'.$client_id.'.zpl ptiadmin@'.$client_ip.':/home/ptiadmin/labelqcfg_'.$client_id.'.zpl';


        // In order to run the script below on the server
        // - ssh-keygen (skip this if it already exists)
        // - Enter2..  (skip this if it already exists)
        // - ssh-copy-id -i ~/.ssh/id_rsa.pub [user]@[remote ip host]
        // - Done!

        // Script to copy unique label to remote address

        exec('scp '.Yii::getAlias("@dirWeb").'/labelqcfg_'.$client_id.'.zpl '.$sbc_user.'@'.$sbc_ip.':/home/'.$sbc_user.'/labelqcfg_'.$client_id.'.zpl 2>&1', $output);
        
        // exec('whoami 2>&1',$output);

        // Execute Printing remotely
        exec("ssh ".$sbc_user."@".$sbc_ip." 'lpr -P Zebra_Technologies_ZTC_GT800_ -o raw /home/".$sbc_user."/labelqcfg_".$client_id.".zpl'");

        // print_r($output);
        // echo 'scp /var/www/html/flowreport/web/labelqcfg_'.$client_id.'.zpl pi@'.$client_ip.':/home/pi/labelqcfg_'.$client_id.'.zpl';

        return $this->redirect(Yii::$app->request->referrer);
    }


    /**
      * actionPrintZebralocal
      * Print ZPL execute zebra local printing remotely.
      *
      * @author Redha Hari
      * @param int $id
      * @return exec execute zebra local printing remotely.
    */
    public function actionPrintZebralocal($id)
    {


        $model = $this->findModel($id);

        $sql="
                select distinct nama_fg
                from scm_planner
                where snfg='".$model->snfg."'
                limit 1
             ";
        $arr = ScmPlanner::findBySql($sql)->one();
        $nama_fg = $arr->nama_fg;

        $client_ip = $_SERVER['REMOTE_ADDR'];
        $client_id = str_replace(".","",$client_ip);

        $myfile = fopen("labelqcfg_".$client_id.".zpl", "w");

            $txt =
            "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR4,4~SD15^JUS^LRN^CI0^XZ
            ^XA
            ^MMT
            ^PW609
            ^LL0406
            ^LS0
            ^FO0,30^A0N,50,50^FB440,1,0,C^FD".$model->status_check."\&^FS
            ^FO450,5^BY5,1.0,10^BQN,2,5^FDMA,".$model->snfg."@".$model->nourut."@".$model->jumlah_koli."^FS

            ^BY1,2,60^FT60,140^BCN,,Y,N
            ^FD>:".$model->snfg."^FS

            ^FT380,340^A0N,30,30^FH\^FDPALET KE-".$model->nourut."^FS
            ^FT290,340^A0N,28,28^FH\^FDPcs^FS


            ^FO1,180^GB606,0,3^FS
            ^FO0,300^GB607,0,3^FS

            ^FT0,245^A0N,20,20^FB600,2,0,C^FD".$nama_fg."\&^FS


            ^FO330,300^GB0,64,4^FS

            ^FT35,340^A0N,38,40^FH\^FD".$model->qty."^FS
            ^PQ1,0,1,Y^XZ";


            $txt2=
            "^XA
            ^MMT
            ^PW609
            ^LL0406
            ^LS0

            ^FO0,30^A0N,50,50^FB440,1,0,C^FD".$model->status_check."\&^FS
            ^FT0,145^A0N,20,20^FB440,2,0,C^FD".$nama_fg."\&^FS
            ^FO450,5^BY5,1.0,10^BQN,2,5^FDMA,".$model->snfg."@".$model->nourut."@".$model->jumlah_koli."^FS

            ^BY2,2,60^FT5,230^BCN,,Y,N
            ^FD>:".$model->snfg."^FS

            ^FT35,340^A0N,30,30^FH\^FDPALET KE-".$model->nourut."^FS

            ^FT35,300^A0N,30,30^FH\^FD".$model->qty." Pcs^FS
            ^PQ1,0,1,Y^XZ";

            fwrite($myfile, $txt2);
            fclose($myfile);


            // echo file_get_contents( "labelsnfg.prn" );

        // echo 'scp /var/www/html/flowreport_training5/web/labelqcfg_'.$client_id.'.zpl ptiadmin@'.$client_ip.':/home/ptiadmin/labelqcfg_'.$client_id.'.zpl';


        // In order to run the script below on the server
        // - ssh-keygen (skip this if it already exists)
        // - Enter2..  (skip this if it already exists)
        // - ssh-copy-id -i ~/.ssh/id_rsa.pub [user]@[remote ip host]
        // - Done!



        // Script to copy unique label to remote address
        exec('scp /var/www/html/flowreport/web/labelqcfg_'.$client_id.'.zpl ptiadmin@'.$client_ip.':/home/ptiadmin/labelqcfg_'.$client_id.'.zpl 2>&1', $output);
        // exec('whoami 2>&1',$output);

        // Execute Printing remotely
        exec("ssh ptiadmin@".$client_ip." 'lpr -P Zebra_Technologies_ZTC_GT800_ -o raw /home/ptiadmin/labelqcfg_".$client_id.".zpl'");

        // print_r($output);
        // echo 'scp /var/www/html/flowreport/web/labelqcfg_'.$client_id.'.zpl pi@'.$client_ip.':/home/pi/labelqcfg_'.$client_id.'.zpl';

        return $this->redirect(Yii::$app->request->referrer);
    }


    /**
      * actionTerimaKarantina
      * Add Timestamp, and status 'SIAP KIRIM NDC' for a specific id in qc_fg_v2 table.
      * @author Redha Hari
      * @param int $id
      * @return void redirect to previous page.
    */
    public function actionTerimaKarantina($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_fg_v2
            SET timestamp_terima_karantina = :current_time,
                status='SIAP KIRIM KE NDC'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        // return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
      * actionTerimaKarantina
      * Add Timestamp, and status 'SUDAH TERIMA NDC' for a specific id in qc_fg_v2 table.
      * @author Redha Hari
      * @param int $id
      * @return void redirect to previous page.
    */
    public function actionTerimaNdc($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_fg_v2
            SET timestamp_terima_ndc = :current_time,
                status='SUDAH DITERIMA NDC'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        // return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
        // return $this->refresh();
        return $this->redirect(Yii::$app->request->referrer);
    }



    /**
      * actionTerimaSuratJalanNdc
      * model surat_jalan : add flag is_received = 1 for a surat_jalan.
      * model qc_fg_v2 : Add Timestamp and status as 'SUDAH DITERIMA NDC' for all snfg & nourut listed in surat jalan.
      * @author Redha Hari
      * @param string $surat_jalan
      * @return void redirect to halaman-terima-ndc.
    */
    public function actionTerimaSuratJalanNdc($surat_jalan)
    {
        $sj_id = SuratJalan::find()->where(['nosj'=>$surat_jalan])->one()['id'];

        $rincian = Yii::$app->db->createCommand("
        SELECT DISTINCT ON (id) * FROM (SELECT sp.odoo_code,sjr.nobatch as no_batch, sjr.qty as quantity, sp.koitem_fg,sp.sediaan, sjr.id, sj.nosj,sjr.snfg,sjr.nourut,sjr.odoo_updated FROM surat_jalan_rincian sjr
                LEFT JOIN scm_planner sp on sjr.snfg = sp.snfg
                INNER JOIN surat_jalan sj on sjr.surat_jalan_id = sj.id
            WHERE sjr.surat_jalan_id = ".$sj_id.")main
            ")->queryAll();

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");

        $sql = "
                SELECT
                    snfg,
                    nourut
                FROM surat_jalan sj
                 INNER JOIN surat_jalan_rincian sjr on sj.id = sjr.surat_jalan_id
                 WHERE sj.nosj='".$surat_jalan."'
                ";

        $QcFgs = QcFgV2::findBySql($sql)->all();

        $sql = "UPDATE surat_jalan SET sync_wmsndc = 'WAIT' WHERE nosj = '".$surat_jalan."'";
        $update = Yii::$app->db->createCommand($sql)->execute();

        $item_json = json_encode($rincian);
        $create_json_file = file_put_contents("update_stock_ndc/data.json", $item_json);
        if ($create_json_file)
        {
            echo "JSON file created successfully...";
            require (Yii::getAlias('@webroot').'/update_stock_ndc/create_receive_from_fro.php');
        }else{
            echo "Oops! Error creating json file...";
            return $this->redirect(['qc-fg-v2/halaman-terima-ndc', 'e'=>'json-fail']);
        }
        return $this->redirect(['qc-fg-v2/halaman-terima-ndc', 'e'=>'json-fail']);


        // $cookies = Yii::$app->response->cookies;

        // $cookies->add(new \yii\web\Cookie([
        //     'name' => 'item',
        //     'value' => $item_json,
        //     // 'expire' => time() + 10,
        // ]));

        // if (($cookie = $cookies->get('item')) !== null) {
        //     $item = $cookie->value;
        //     print_r($item);
        // }
    }

    // public function actionTerimaSuratJalanNdc($surat_jalan)
    // {
    //     $sj_id = SuratJalan::find()->where(['nosj'=>$surat_jalan])->one()['id'];

    //     $rincian = Yii::$app->db->createCommand("
    //     SELECT DISTINCT ON (id) * FROM (SELECT sp.odoo_code,sjr.nobatch as no_batch, sjr.qty as quantity, sp.koitem_fg,sp.sediaan, sjr.id, sj.nosj,sjr.snfg,sjr.nourut,sjr.odoo_updated FROM qc_fg_v2 sjr
    //             LEFT JOIN scm_planner sp on sjr.snfg = sp.snfg
    //             INNER JOIN surat_jalan sj on sjr.surat_jalan_id = sj.id
    //         WHERE sjr.surat_jalan_id = ".$sj_id.")main
    //         ")->queryAll();

    //     date_default_timezone_set('Asia/Jakarta');
    //     $current_time = date("H:i:s");

    //     $sql = "
    //             SELECT
    //                 snfg,
    //                 nourut
    //             FROM surat_jalan sj
    //              INNER JOIN qc_fg_v2 sjr on sj.id = sjr.surat_jalan_id
    //              WHERE sj.nosj='".$surat_jalan."'
    //             ";

    //     $QcFgs = QcFgV2::findBySql($sql)->all();

    //     $log_receive = LogReceiveNdc::find()->where(['nosj'=>$surat_jalan])->all();

    //     foreach ($log_receive as $data){
    //         $sql = "UPDATE qc_fg_v2 SET timestamp_terima_karantina = '".$data['timestamp_terima']."' WHERE id_palet = '".$data['id_palet']."'";

    //         $update = Yii::$app->db->createCommand($sql)->execute();
    //     }

    //     $sql = "UPDATE surat_jalan SET is_received = 1 WHERE nosj = '".$surat_jalan."'";
    //     $update = Yii::$app->db->createCommand($sql)->execute();
    //     return $this->redirect(['qc-fg-v2/halaman-terima-ndc']);

    //     /*print_r('<pre>');
    //     print_r($QcFgs);
    //     exit();*/

    //     // $item_json = json_encode($rincian);
    //     // $create_json_file = file_put_contents("update_stock_ndc/data.json", $item_json);
    //     // if ($create_json_file)
    //     // {
    //     //     echo "JSON file created successfully...";
    //     //     require (Yii::getAlias('@webroot').'/update_stock_ndc/create_receive_from_fro.php');
    //     // }else{
    //     //     echo "Oops! Error creating json file...";
    //     //     return $this->redirect(['qc-fg-v2/halaman-terima-ndc', 'e'=>'json-fail']);
    //     // }

    //     // $cookies = Yii::$app->response->cookies;

    //     // $cookies->add(new \yii\web\Cookie([
    //     //     'name' => 'item',
    //     //     'value' => $item_json,
    //     //     // 'expire' => time() + 10,
    //     // ]));

    //     // if (($cookie = $cookies->get('item')) !== null) {
    //     //     $item = $cookie->value;
    //     //     print_r($item);
    //     // }
    // }

    public function actionTerimaSurjalManual()
    {
        $surat_jalan = '2021-04-06/20279';

        $sj_id = SuratJalan::find()->where(['nosj'=>$surat_jalan])->one()['id'];
        // $surjal_rdc = SuratJalanRincian::find()->where("surat_jalan_id = ".$sj_id." and alokasi != 'NDC' and alokasi is not null")->one();

        // if(Yii::$app->user->identity->username == 'karantina' && empty($surjal_rdc)){
        //     Yii::$app->session->setFlash('danger','User anda tidak punya izin untuk scan terima Surat Jalan NDC');
        //     return $this->redirect(['surat-jalan-rincian/konfirmasi-terima-ndc-qr','surat_jalan'=>$surat_jalan]);
        // }

        $rincian = Yii::$app->db->createCommand("
        SELECT DISTINCT ON (id) * FROM (SELECT sp.odoo_code,sjr.nobatch as no_batch, sjr.qty as quantity, sp.koitem_fg, sjr.id, sj.nosj FROM surat_jalan_rincian sjr
                LEFT JOIN scm_planner sp on sjr.snfg = sp.snfg
                INNER JOIN surat_jalan sj on sjr.surat_jalan_id = sj.id
            WHERE sjr.surat_jalan_id = ".$sj_id.")main
            ")->queryAll();

        // $item_json = json_encode($rincian);
        // $create_json_file = file_put_contents("update_stock_ndc/data.json", $item_json);
        // if ($create_json_file)
        //     {echo "JSON file created successfully...";}
        // else
        //     {echo "Oops! Error creating json file...";}


        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        // $connection2 = Yii::$app->getDb();
        // $command2 = $connection2->createCommand("

        //     UPDATE surat_jalan
        //     SET is_received = 1
        //     WHERE nosj ='".$surat_jalan."';
        //     "
        // );

        // $result2 = $command2->queryAll();

        $sql = "
                SELECT
                    snfg,
                    nourut
                FROM surat_jalan sj
                 INNER JOIN surat_jalan_rincian sjr on sj.id = sjr.surat_jalan_id
                 WHERE sj.nosj='".$surat_jalan."'
                ";

        $QcFgs = QcFgV2::findBySql($sql)->all();


        // foreach($QcFgs as $QcFg){

        //     $connection = Yii::$app->getDb();
        //     $command = $connection->createCommand("

        //         UPDATE qc_fg_v2
        //         SET timestamp_terima_ndc = :current_time,
        //             status='SUDAH DITERIMA NDC'
        //         WHERE snfg =:snfg and nourut =:nourut ;
        //         ",
        //         [':current_time' => date('Y-m-d h:i:s A'),':snfg'=> $QcFg->snfg,':nourut'=>$QcFg->nourut]);

        //     $result = $command->queryAll();

        // }

        $item_json = json_encode($rincian);
        $create_json_file = file_put_contents("update_stock_ndc/data.json", $item_json);
        if ($create_json_file)
        {
            echo "JSON file created successfully...";
            require ('/var/www/html/flowreport/web/update_stock_ndc/create_receive_from_fro.php');
        }else{
            echo "Oops! Error creating json file...";
            // Yii::$app->session->setFlash('danger', 'File Json gagal terbentuk, hubungi IT support!')
            return $this->redirect(['qc-fg-v2/halaman-terima-ndc', 'e'=>'json-fail']);
        }


        // require ('/var/www/html/flowreport/web/update_stock_ndc/create_receive_from_fro.php');

        // return $this->redirect(['qc-fg-v2/halaman-terima-ndc']);
        echo 'DONE';
    }

    /**
      * actionRelease
      * model qc_fg_v2 : Add Timestamp Check and status_check as 'RELEASE' for an id
      * model qc_fg_v2_history_status : Log status_check and timestamp changes
      * @author Redha Hari
      * @param int $id
      * @return void redirect to previous page.
    */
    public function actionRelease($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_fg_v2
            SET timestamp_check = :current_time,
                status_check='RELEASE'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();


        $connection2 = Yii::$app->getDb();
        $command2 = $connection2->createCommand("

            INSERT INTO qc_fg_v2_history_status (qcfgv2_id,status_check,timestamp_check)
            VALUES (
                :id,
                'RELEASE',
                :current_time
            );
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result2 = $command2->queryAll();

        $sql9="

        SELECT snfg,nourut
        FROM qc_fg_v2
        WHERE id = '".$id."'
        ";


        $row9 = QcFgV2::findBySql($sql9)->one();
        $no_urut = $row9['nourut'];
        $snfg = $row9['snfg'];

        $connection3 = Yii::$app->getDb();
        $command3 = $connection3->createCommand("
            UPDATE pengecekan_umum
            SET is_done = 1
            WHERE no_palet = :nourut AND snfg = :snfg;
            ",
            [':nourut'=> $no_urut,
            ':snfg'=> $snfg,

        ]);

        $result3 = $command3->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
      * actionPending
      * model qc_fg_v2 : Add Timestamp Check and status_check as 'PENDING' for an id
      * model qc_fg_v2_history_status : Log status_check and timestamp changes
      * @author Redha Hari
      * @param int $id
      * @return void redirect to previous page.
    */
    public function actionPending($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


       $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_fg_v2
            SET timestamp_check = :current_time,
                status_check='PENDING'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();


        $connection2 = Yii::$app->getDb();
        $command2 = $connection2->createCommand("

            INSERT INTO qc_fg_v2_history_status (qcfgv2_id,status_check,timestamp_check)
            VALUES (
                :id,
                'PENDING',
                :current_time
            );
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result2 = $command2->queryAll();

        $sql9="

        SELECT snfg,nourut
        FROM qc_fg_v2
        WHERE id = '".$id."'
        ";


        $row9 = QcFgV2::findBySql($sql9)->one();
        $no_urut = $row9['nourut'];
        $snfg = $row9['snfg'];

        $connection3 = Yii::$app->getDb();
        $command3 = $connection3->createCommand("
            UPDATE pengecekan_umum
            SET is_done = 1
            WHERE no_palet = :nourut AND snfg = :snfg;
            ",
            [':nourut'=> $no_urut,
            ':snfg'=> $snfg,

        ]);

        $result3 = $command3->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
      * actionRework
      * model qc_fg_v2 : Add Timestamp Check and status_check as 'REWORK' for an id
      * model qc_fg_v2_history_status : Log status_check and timestamp changes
      * @author Redha Hari
      * @param int $id
      * @return void redirect to previous page.
    */
    public function actionRework($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_fg_v2
            SET timestamp_check = :current_time,
                status_check='REWORK'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();


        $connection2 = Yii::$app->getDb();
        $command2 = $connection2->createCommand("

            INSERT INTO qc_fg_v2_history_status (qcfgv2_id,status_check,timestamp_check)
            VALUES (
                :id,
                'REWORK',
                :current_time
            );
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result2 = $command2->queryAll();

        $sql9="SELECT snfg,nourut
        FROM qc_fg_v2
        WHERE id = '".$id."'
        ";

        $row9 = QcFgV2::findBySql($sql9)->one();
        $no_urut = $row9['nourut'];
        $snfg = $row9['snfg'];

        $connection3 = Yii::$app->getDb();
        $command3 = $connection3->createCommand("
            UPDATE pengecekan_umum
            SET is_done = 1
            WHERE no_palet = :nourut AND snfg = :snfg;
            ",
            [':nourut'=> $no_urut,
            ':snfg'=> $snfg,

        ]);

        $result3 = $command3->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
      * actionREJECT
      * model qc_fg_v2 : Add Timestamp Check and status_check as 'REJECT' for an id
      * model qc_fg_v2_history_status : Log status_check and timestamp changes
      * @author Redha Hari
      * @param int $id
      * @return void redirect to previous page.
    */
    public function actionReject($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_fg_v2
            SET timestamp_check = :current_time,
                status_check='REJECT'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();


        $connection2 = Yii::$app->getDb();
        $command2 = $connection2->createCommand("

            INSERT INTO qc_fg_v2_history_status (qcfgv2_id,status_check,timestamp_check)
            VALUES (
                :id,
                'REJECT',
                :current_time
            );
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result2 = $command2->queryAll();

        $sql9="

        SELECT snfg,nourut
        FROM qc_fg_v2
        WHERE id = '".$id."'
        ";


        $row9 = QcFgV2::findBySql($sql9)->one();
        $no_urut = $row9['nourut'];
        $snfg = $row9['snfg'];

        $connection3 = Yii::$app->getDb();
        $command3 = $connection3->createCommand("
            UPDATE pengecekan_umum
            SET is_done = 1
            WHERE no_palet = :nourut AND snfg = :snfg;
            ",
            [':nourut'=> $no_urut,
            ':snfg'=> $snfg,

        ]);

        $result3 = $command3->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
      * actionCekMikro
      * model qc_fg_v2 : Add Timestamp Check and status_check as 'CEK MIKRO' for an id
      * model qc_fg_v2_history_status : Log status_check and timestamp changes
      * @author Redha Hari
      * @param int $id
      * @return void redirect to previous page.
    */
    public function actionCekMikro($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_fg_v2
            SET timestamp_check = :current_time,
                status_check='CEK MIKRO'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();


        $connection2 = Yii::$app->getDb();
        $command2 = $connection2->createCommand("

            INSERT INTO qc_fg_v2_history_status (qcfgv2_id,status_check,timestamp_check)
            VALUES (
                :id,
                'CEK MIKRO',
                :current_time
            );
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result2 = $command2->queryAll();

        $sql9="

        SELECT snfg,nourut
        FROM qc_fg_v2
        WHERE id = '".$id."'
        ";


        $row9 = QcFgV2::findBySql($sql9)->one();
        $no_urut = $row9['nourut'];
        $snfg = $row9['snfg'];

        $connection3 = Yii::$app->getDb();
        $command3 = $connection3->createCommand("
            UPDATE pengecekan_umum
            SET is_done = 1
            WHERE no_palet = :nourut AND snfg = :snfg;
            ",
            [':nourut'=> $no_urut,
            ':snfg'=> $snfg,

        ]);

        $result3 = $command3->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionGetExpDate($snfg,$nobatch)
    {
        $sql="  select exp_date
                from flow_input_snfg
                where snfg='".$snfg."' and nobatch = '".$nobatch."'
             ";
        $fim= FlowInputSnfg::findBySql($sql)->one();

        if(!empty($fim)){

            echo Json::encode($fim);

        }else{
            $sql="  select exp_date
                    from flow_pra_kemas
                    where snfg='".$snfg."' and nobatch = '".$nobatch."'
                 ";
            $fpk= FlowPraKemas::findBySql($sql)->one();

            if (!empty($fpk)){
                echo Json::encode($fpk);
            }else{
                $fim2['exp_date']=null;
                echo Json::encode($fim2);
            }

        }

    }

    /**
      * actionGetNourut
      * model qc_fg_v2 : Get Latest Sequence nourut for an SNFG
      * @author Redha Hari
      * @param string $snfg
      * @return json
    */
    public function actionGetNourut($snfg)
    {
        $sql="  select coalesce(max(nourut),0)+1 as nourut
                from qc_fg_v2
                where snfg='".$snfg."'
             ";
        $lastinduk= QcFgV2::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }

    /**
     * Deletes an existing QcFgV2 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->identity->role == 90 && (!empty($model->timestamp_terima_karantina) || !empty($model->timestamp_terima_ndc)) )
        {
            Yii::$app->session->setFlash('danger','Anda tidak bisa menghapus palet / trolly yang sudah scan Karantina atau NDC');
        }else{
            $this->findModel($id)->delete();
        }
        // return $this->redirect(['index']);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDeleteSurjalRincian($id)
    {
        $model = $this->findModel($id);
        $model->surat_jalan_id = null;
        $model->save();
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the QcFgV2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QcFgV2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QcFgV2::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
