<?php

namespace app\controllers;

use Yii;
use app\models\FlowInputSnfg;
use app\models\JenisProsesKemas2;
use app\models\WeigherFgMapDevice;
use app\models\MasterDataKoli;
use app\models\MasterDataLine;
use app\models\MasterDataNetto;
use app\models\MasterDataBarcodeMalaysia;
use app\models\FlowInputSnfgSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use yii\helpers\ArrayHelper;
use app\models\ScmPlanner;
use app\models\FlowInputSnfgkomponen;
use app\models\LogInlineWeigher;
use yii\db\Query;

/**
 * FlowInputSnfgController implements the CRUD actions for FlowInputSnfg model.
 */
class FlowInputSnfgController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FlowInputSnfg models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FlowInputSnfgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FlowInputSnfg model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Creates a new FlowInputSnfg model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FlowInputSnfg();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateLakban($id)
    {
        $model = $this->findModel($id);
        //$model = FlowInputSnfg()->findModel($id);

        if ($model->load(Yii::$app->request->post()))
        {
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

            INSERT INTO log_perubahan_lakban (flow_input_snfg_id,snfg,lakban,reason)
            SELECT
                :id as flow_input_snfg_id,
                :snfg as snfg,
                :lakban as lakban,
                :reason as reason
            ",
            [
                ':id' => $id,
                ':snfg' => $model->snfg,
                ':lakban' => $model->lakban,
                ':reason' => $model->reason,
            ]);

            $result = $command->queryAll();

            if ($model->save()){
                return $this->redirect(['check-snfg-lakban']);
            } else {
                return $this->render('update-lakban', [
                    'model' => $model,
                    'lakban' => $model->lakban
                ]);
            }
        } else {
            return $this->render('update-lakban', [
                'model' => $model,
                'lakban' => $model->lakban
            ]);
        }
    }

    public function actionIsFisExist($snfg)
    {
        $sql="
            SELECT * FROM flow_input_snfg
            WHERE snfg = '".$snfg."' order by id desc
        ";

        $result = FlowInputSnfg::findBySql($sql)->one();

        if (!empty($result)){
            echo Json::encode(["id" => $result->id]);
        } else {
            echo Json::encode(["id" => 0]);
        }

    }

    public function actionCheckSnfgLakban()
    {
        // $model = new FlowInputSnfg();

        // $searchModel = new FlowInputSnfgSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query->where("datetime_start is not null and datetime_stop is null");

        $this->layout = '//main-sidebar-collapse';

        return $this->render('check-snfg-lakban');
        //}
    }

    /**
     * Render Scan Barcode SNFG Kemas 2 Page
     * @return mixed
     */
    public function actionCheckKemas2()
    {
        $model = new FlowInputSnfg();

        $searchModel = new FlowInputSnfgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("datetime_start is not null and datetime_stop is null");

        $this->layout = '//main-sidebar-collapse';

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['create-kemas2', 'snfg' => $model->snfg]);
        } else {
            return $this->render('check-kemas2', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Render Scan Barcode SNFG Kemas 2 Page Inline
     * @return mixed
     */
    public function actionCheckKemas2Inline()
    {
        // Get Device Information
            if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }else{
                $frontend_ip = $_SERVER['REMOTE_ADDR'];
            }

            $sql="
            SELECT
                *
            FROM weigher_fg_map_device
            WHERE frontend_ip = '".$frontend_ip."'";

            $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

            if(empty($map_device_array)){
                $sbc_ip = 'LINKAGE NOT FOUND!';
                $line = 'LINKAGE NOT FOUND!';
                $keterangan = 'LINKAGE NOT FOUND!';
                $is_line_exist = false;
            }else{
                $sbc_ip = $map_device_array->sbc_ip;
                $line = $map_device_array->line;
                $keterangan = $map_device_array->keterangan;

                $masterLine = MasterDataLine::find()->where(['nama_line'=>$line])->one();
                if(!empty($masterLine)){
                    $is_line_exist = true;
                }else{
                    $is_line_exist = false;
                }
            }

        $model = new FlowInputSnfg();

        $searchModel = new FlowInputSnfgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("datetime_start is not null and datetime_stop is null");

        $this->layout = '//main-sidebar-collapse';

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['create-kemas2-inline', 'snfg' => $model->snfg]);
        } else {
            return $this->render('check-kemas2-inline', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'frontend_ip' => $frontend_ip,
                'sbc_ip' => $sbc_ip,
                'line' => $line,
                'keterangan' => $keterangan,
                'is_line_exist' => $is_line_exist,
            ]);
        }
    }

    public function actionCheckAvailabilityLine($snfg,$nama_line)
    {
        // Get Device Information
        $last_kemas = FlowInputSnfg::find()->where("nama_line = '".$nama_line."' and datetime_stop is null")->orderBy(['datetime_start'=>SORT_DESC])->one();
        $cutoff = date('Y-m-d H:i:s', strtotime('-1 days'));

        if (!empty($last_kemas)){
            if ($last_kemas->datetime_start >= $cutoff){
                echo 0;
            }else{
                echo 1;
            }
        }else{
            echo 1;
        }


    }

    public function actionLineNotAvailable($nama_line){
        $last_kemas = FlowInputSnfg::find()->where("nama_line = '".$nama_line."' and datetime_stop is null")->orderBy(['datetime_start'=>SORT_DESC])->one();
        $model = $last_kemas;

        return $this->renderAjax('line-not-available', [
            'model' => $model,
        ]);
    }

    public function actionSplitNotActive($nama_line,$snfg){
        // $last_kemas = FlowInputSnfg::find()->where("nama_line = '".$nama_line."' and datetime_stop is null")->orderBy(['datetime_start'=>SORT_DESC])->one();
        // $model = $last_kemas;

        return $this->renderAjax('split-not-active', [
            'nama_line' => $nama_line,
        ]);
    }

    public function actionUpdateIpTablet(){
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $frontend_ip = $_SERVER['REMOTE_ADDR'];
        }

        $model = new WeigherFgMapDevice();

        if ($model->load(Yii::$app->request->post()) ) {
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

            UPDATE weigher_fg_map_device
            SET frontend_ip = :frontend_ip
            WHERE line = :line;
            ",
            [':frontend_ip'=> $model->frontend_ip,
             ':line'=>$model->line
            ]);

            $result = $command->queryAll();
            return $this->redirect(['check-kemas2-inline']);
        } else {
            return $this->renderAjax('update-ip-tab', [
                'model' => $model,
                'frontend_ip' => $frontend_ip,
            ]);
        }



    }
    /**
     * actionWeigherFg
     * Render WeigherFG IoT page for Operators
     * @param integer $id
     * @return mixed
     */
    public function actionWeigherFg($id)
    {

        // Hide Sidebar by Rendering Workorder Layout
        $this->layout = '//main-sidebar-collapse-workorder';
        $innerQty = 0;

        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $frontend_ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            *
        FROM weigher_fg_map_device
        WHERE frontend_ip = '".$frontend_ip."'";

        $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

        // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
        if(empty($map_device_array)){
            $sbc_ip = '10.128.1.161';
            $line = 'Line A';
        }else{
            $sbc_ip = $map_device_array->sbc_ip;
            $line = $map_device_array->line;
        }

        // Get SNFG Array
        $sql="
        SELECT
            snfg,is_split_line,datetime_stop
        FROM flow_input_snfg
        WHERE id = ".$id;

        $snfg_array = FlowInputSnfg::findBySql($sql)->one();

        $koitem = strtok($snfg_array->snfg, '/');

        if ($snfg_array->is_split_line == 1){
            $status_split = 'AKTIF';
        } else {
            $status_split = 'TIDAK AKTIF';
        }

        // Get SNFG Array
        $sql4="
        SELECT
            *
        FROM master_data_koli
        WHERE koitem = '".$koitem."' AND inner_box = 1 ";

        $mdk_array = MasterDataKoli::findBySql($sql4)->one();
        if(empty($mdk_array)){
            $isInner = 0;
            $innerQty = 0;
            $is_counter = 0;
        }else{
            $isInner = $mdk_array->inner_box;
            $is_counter = 1;
            $innerQty = $mdk_array->qty / $mdk_array->qty_inside_innerbox;

            // Update Is Counter - Frontend IP Array
            $sql="
            UPDATE weigher_fg_map_device
            SET is_counter = 1
            WHERE frontend_ip = '".$frontend_ip."'";

            $update_is_counter = Yii::$app->db->createCommand($sql)->execute();

        }

        // Get List Operator Array
        $sql3="
                SELECT
                     unnest(string_to_array(nama_operator, ',')) as nama_operator
                FROM flow_input_snfg
                WHERE id = ".$id;

        $list_operator = ArrayHelper::map(FlowInputSnfg::findBySql($sql3)->all(), 'nama_operator','nama_operator');

        // Get Operator Count
        $sql5="
                SELECT count(*) as id
                FROM
                (SELECT
                     unnest(string_to_array(nama_operator, ',')) as nama_operator
                FROM flow_input_snfg
                WHERE id = ".$id.") a ";

        $sum_operator = FlowInputSnfg::findBySql($sql5)->one();

        // Get Operator Count
        $sql7="
                select count(snfg) from flow_input_snfg where snfg = '".$snfg_array->snfg."' and datetime_stop is null ";

        $count_snfg_split = FlowInputSnfg::findBySql($sql7)->scalar();

        // Get Current Selected Operator
        $sql4="
                SELECT
                     distinct on (flow_input_snfg_id)
                     nama_operator
                FROM flow_input_snfg_wo_operator
                WHERE flow_input_snfg_id = ".$id."
                ORDER BY flow_input_snfg_id, id desc";

        $op_terpilih = FlowInputSnfg::findBySql($sql4)->one();
        $operator_terpilih = $op_terpilih['nama_operator'];

        // Get List Operator Innerbox Array
        // $connection2 = Yii::$app->db;
        // $command2 = $connection2->createCommand("

        //         SELECT
        //              unnest(string_to_array(nama_operator, ',')) as nama_operator
        //         FROM flow_input_snfg
        //         WHERE id = ".$id);

        // $list_operator_innerbox = ArrayHelper::map($command2->queryAll(),'nama_operator','nama_operator');
        $sql11="
                SELECT
                     unnest(string_to_array(nama_operator, ',')) as nama_operator
                FROM flow_input_snfg
                WHERE id = ".$id;

        $list_operator_innerbox = ArrayHelper::map(FlowInputSnfg::findBySql($sql11)->all(), 'nama_operator','nama_operator');


        // Get Current Selected Operator Innerbox
        // $connection3 = Yii::$app->db;
        // $command3 = $connection3->createCommand("

        //         SELECT
        //              distinct on (flow_input_snfg_id)
        //              nama_operator
        //         FROM flow_input_snfg_wo_operator_innerbox
        //         WHERE flow_input_snfg_id = ".$id."
        //         ORDER BY flow_input_snfg_id, id desc");

        // $op_terpilih_innerbox = $command3->queryOne();

        // if (empty($op_terpilih_innerbox)){
        //     $operator_terpilih_innerbox = "";
        // } else {
        //     $operator_terpilih_innerbox = $op_terpilih_innerbox['nama_operator'];
        // }
        $sql12="
                SELECT
                     distinct on (flow_input_snfg_id)
                     nama_operator
                FROM flow_input_snfg_wo_operator_innerbox
                WHERE flow_input_snfg_id = ".$id."
                ORDER BY flow_input_snfg_id, id desc";

        $op_terpilih_innerbox = FlowInputSnfg::findBySql($sql12)->one();
        if (empty($op_terpilih_innerbox)){
            $operator_terpilih_innerbox = "";
        } else {
            $operator_terpilih_innerbox = $op_terpilih_innerbox['nama_operator'];
        }


        // Get Tare
        $sql6="
                SELECT
                     distinct on (flow_input_snfg_id)
                     status as status_tare
                FROM flow_input_snfg_wo_tare
                WHERE flow_input_snfg_id = ".$id."
                ORDER BY flow_input_snfg_id, id desc";

        $object_status_tare = FlowInputSnfg::findBySql($sql6)->one();
        if(empty($object_status_tare->status_tare)){
            $status_tare='tare_off';
        }else{
            $status_tare=$object_status_tare->status_tare;
        }
        // $operator_terpilih = $object_status_tare['status_tare'];


        $searchModel = new FlowInputSnfgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('weigher-fg', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'frontend_ip' => $frontend_ip,
            'sbc_ip' => $sbc_ip,
            'line' => $line,
            'id' => $id,
            'snfg' => $snfg_array->snfg,
            'list_operator' => $list_operator,
            'operator_terpilih' => $operator_terpilih,
            'list_operator_innerbox' => $list_operator_innerbox,
            'operator_terpilih_innerbox' => $operator_terpilih_innerbox,
            'sum_operator' => $sum_operator->id,
            'status_tare' => $status_tare,
            'isInner' => $isInner,
            'status_split' => $status_split,
            'is_counter' => $is_counter,
            'innerQty' => $innerQty,
            'countSplit' => $count_snfg_split,
            'datetime_stop' => $snfg_array->datetime_stop
        ]);
    }

    public function actionDeleteRow($id_fis,$id_wo)
    {
        $id_fis_new = "999".$id_fis;
        $id_fis_new = (int)$id_fis_new;

        $connection = Yii::$app->db;
        $command = $connection->createCommand("

            UPDATE flow_input_snfg_wo
            SET flow_input_snfg_id = :id_fis_new
            WHERE id = :id_wo;
            ",
            [':id_wo'=> $id_wo,
             ':id_fis_new'=>$id_fis_new,
            ]);

        $result = $command->queryAll();

        if ($result){
            $value = array(
            "status"=>"success");
        } else {
            $value = array(
            "status"=>"not-success");
        }

        echo Json::encode($value);
    }

    public function actionCheckActiveSnfg($nama_line,$snfg)
    {
        date_default_timezone_set('Asia/Jakarta');
        // $date = date("Y-m-d");
        $current_time = date("Y-m-d H:i:s");
        $yesterday_time = date("Y-m-d H:i:s",strtotime("-1 days"));

        $sql="
            SELECT * FROM flow_input_snfg
            WHERE nama_line = '".$nama_line."' AND snfg = '".$snfg."' AND datetime_stop is null order by id desc
        ";

        $result = FlowInputSnfg::findBySql($sql)->all();

        if (!empty($result)){
            echo Json::encode(["route" => "to-stop"]);
        } else {
            // Check if line is used by another snfg
            $sql2="
            SELECT * FROM flow_input_snfg
            WHERE nama_line = '".$nama_line."' AND datetime_start > '".$yesterday_time."' AND datetime_start < '".$current_time."' AND datetime_stop is null order by id desc";

            $result2 = FlowInputSnfg::findBySql($sql2)->all();

            if (!empty($result2)){

                $array = ["route"=>"line-already-used"];
                echo Json::encode($array);
            } else {

                // Check if snfg is run in another line
                $sql3="SELECT * FROM flow_input_snfg WHERE snfg = :snfg AND datetime_stop is null ";
                $result3 = FlowInputSnfg::findBySql($sql3,[':snfg'=>$snfg])->one();

                // If snfg is run in another line
                if (!empty($result3)){

                    // If split line active
                    if($result3->is_split_line == 1){
                        echo Json::encode(["route" => "to-create"]);
                    }else{
                        echo Json::encode(["route" => "split-not-active","nama_line"=>$result3->nama_line]);
                    }
                }else{ //If snfg is not run in another line
                    echo Json::encode(["route" => "to-create"]);
                }
            }
        }

    }

    public function actionCheckIsLineActive($nama_line)
    {
        date_default_timezone_set('Asia/Jakarta');
        // $date = date("Y-m-d");
        $current_time = date("Y-m-d H:i:s");
        $yesterday_time = date("Y-m-d H:i:s",strtotime("-1 days"));

        $sql2="
        SELECT * FROM flow_input_snfg
        WHERE nama_line = '".$nama_line."' AND datetime_start > '".$yesterday_time."' AND datetime_start < '".$current_time."' AND datetime_stop is null order by id desc";

        $result2 = FlowInputSnfg::findBySql($sql2)->all();

        if (!empty($result2)){
            foreach ($result2 as $result_) {
                $array[] = $result_->snfg;
            }
            echo Json::encode($array);
        } else {
            echo 0;
        }
    }

    public function actionCheckIsSplit($snfg)
    {
        date_default_timezone_set('Asia/Jakarta');

        $sql2="
        SELECT is_split_line FROM flow_input_snfg
        WHERE snfg = '".$snfg."' order by id desc";

        $result2 = FlowInputSnfg::findBySql($sql2)->one();

        if (!empty($result2)){
            echo $result2->is_split_line;
        } else {
            echo 99;
        }
    }

    public function actionCheckIfSplitLine($snfg)
    {
        date_default_timezone_set('Asia/Jakarta');
        // $date = date("Y-m-d");
        $current_time = date("Y-m-d H:i:s");
        $yesterday_time = date("Y-m-d H:i:s",strtotime("-1 days"));

        $sql="
            SELECT id,nama_line FROM flow_input_snfg
            WHERE snfg = '".$snfg."' AND datetime_start > '".$yesterday_time."' AND datetime_start < '".$current_time."' AND datetime_stop is null order by id desc
        ";

        $result = FlowInputSnfg::findBySql($sql)->all();

        if (!empty($result)){
            foreach ($result as $data) {
                $list[] = array("id"=>$data->id,"nama_line"=>$data->nama_line,"snfg"=>$snfg,"route"=>"to-stop");
            }
            echo Json::encode($list);
        } else {
            $var = json_decode('[{"route" : "to-create"}]');
            return Json::encode($var);
        }

    }

    /**
     * actionWeigherFgPreview
     * Render WeigherFG IoT page for Users
     * @param integer $id
     * @return mixed
     */
    public function actionWeigherFgPreview($id)
    {

        // Hide Sidebar by Rendering Workorder Layout
        $this->layout = '//main-sidebar-collapse-workorder';


        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $frontend_ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get SNFG Array
        $sql="
        SELECT
            distinct snfg as snfg, nama_line
        FROM flow_input_snfg
        WHERE id = ".$id;

        $snfg_array = FlowInputSnfg::findBySql($sql)->one();

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            *
        FROM weigher_fg_map_device
        WHERE frontend_ip = '".$frontend_ip."'";

        $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

        // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
        if(empty($map_device_array)){
            $sbc_ip = '10.128.1.161';
            $line = 'Line A';
        }else{
            $sbc_ip = $map_device_array->sbc_ip;
            $line = $map_device_array->line;

        }

        // Get List Operator Array
        $sql3="
                SELECT
                     unnest(string_to_array(nama_operator, ',')) as nama_operator
                FROM flow_input_snfg
                WHERE id = ".$id;

        $list_operator = ArrayHelper::map(FlowInputSnfg::findBySql($sql3)->all(), 'nama_operator','nama_operator');

        // Get Operator Count
        $sql5="
                SELECT count(*) as id
                FROM
                (SELECT
                     unnest(string_to_array(nama_operator, ',')) as nama_operator
                FROM flow_input_snfg
                WHERE id = ".$id.") a ";

        $sum_operator = FlowInputSnfg::findBySql($sql5)->one();

        // Get Current Selected Operator
        $sql4="
                SELECT
                     distinct on (flow_input_snfg_id)
                     nama_operator
                FROM flow_input_snfg_wo_operator
                WHERE flow_input_snfg_id = ".$id."
                ORDER BY flow_input_snfg_id, id desc";

        $op_terpilih = FlowInputSnfg::findBySql($sql4)->one();
        $operator_terpilih = $op_terpilih['nama_operator'];

        $searchModel = new FlowInputSnfgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('weigher-fg-preview', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'frontend_ip' => $frontend_ip,
            'sbc_ip' => $sbc_ip,
            'line' => $line,
            'id' => $id,
            'snfg' => $snfg_array->snfg,
            'line_pack' => $snfg_array->nama_line,
            'list_operator' => $list_operator,
            'operator_terpilih' => $operator_terpilih,
            'sum_operator' => $sum_operator->id,
        ]);
    }


    /**
     * actionInsertSwo
     * Triggers from view weigher-fg
     * Triggered when Operator changes the Operator Karbox Name by clicking the Trigger Button
     * Logs Operator Changes (Historical)
     * @param integer $id
     * @param string $nama_operator
     * @return void previous page
     */
    public function actionInsertSwo($id,$nama_operator)
    {
        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $frontend_ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            is_counter
        FROM weigher_fg_map_device
        WHERE frontend_ip = '".$frontend_ip."'";

        $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            INSERT INTO flow_input_snfg_wo_operator (flow_input_snfg_id,nama_operator)
            SELECT
                :id as flow_input_snfg_id,
                :nama_operator as nama_operator
            ;
            ",
            [
                ':id' => $id,
                ':nama_operator' => $nama_operator
            ]);

            $result = $command->queryAll();

        if ($map_device_array->is_counter == 0){

            $connection2 = Yii::$app->getDb();
            $command2 = $connection2->createCommand("

            INSERT INTO flow_input_snfg_wo_operator_innerbox (flow_input_snfg_id,nama_operator)
            SELECT
                :id as flow_input_snfg_id,
                :nama_operator as nama_operator
            ;
            ",
            [
                ':id' => $id,
                ':nama_operator' => $nama_operator
            ]);

            $result2 = $command2->queryAll();
        } else {
            $result2 = true;
        }

            if($result && $result2){
                return $this->redirect(Yii::$app->request->referrer);
            }
    }

    /**
     * actionInsertOperatorInnerbox
     * Triggers from view weigher-fg
     * Triggered when Operator changes the Operator Innerbox Name by clicking the Trigger Button
     * Logs Operator Changes (Historical)
     * @param integer $id
     * @param string $nama_operator
     * @return void previous page
     */
    public function actionInsertOperatorInnerbox($id,$nama_operator)
    {
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            INSERT INTO flow_input_snfg_wo_operator_innerbox (flow_input_snfg_id,nama_operator)
            SELECT
                :id as flow_input_snfg_id,
                :nama_operator as nama_operator
            ;
            ",
            [
                ':id' => $id,
                ':nama_operator' => $nama_operator
            ]);

            $result = $command->queryAll();

            if($result){
                return $this->redirect(Yii::$app->request->referrer);
            }
    }

    /**
     * actionInsertTare
     * Triggers from view weigher-fg
     * Triggered when Operator clicks Tare On button
     * Logs Operator Changes (Historical)
     * @param integer $id
     * @param string $nama_operator
     * @return void previous page
     */
    public function actionInsertTare($id)
    {


            // Set Tare Status Alternating (On/Off)
            $sql6="
                    SELECT
                         distinct on (flow_input_snfg_id)
                         status as status_tare
                    FROM flow_input_snfg_wo_tare
                    WHERE flow_input_snfg_id = ".$id."
                    ORDER BY flow_input_snfg_id, id desc";

            $object_status_tare = FlowInputSnfg::findBySql($sql6)->one();
            if(($object_status_tare['status_tare'])=='tare_on'){
                $status_tare='tare_off';
            }else{
                $status_tare='tare_on';
            }


            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            INSERT INTO flow_input_snfg_wo_tare (flow_input_snfg_id,status)
            SELECT
                :flow_input_snfg_id as flow_input_snfg_id,
                :status as status
            ;
            ",
            [
                ':flow_input_snfg_id' => $id,
                ':status' => $status_tare
            ]);

            $result = $command->queryAll();

            if($result){
                return $this->redirect(Yii::$app->request->referrer);
            }
    }

    public function actionActivateSplitLine($id,$now)
    {
        if ($now=='AKTIF'){
            $update = 0;
        } else {
            $update = 1;
        }

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

        UPDATE flow_input_snfg
        SET is_split_line = :value
        WHERE id = :id;
        ",
        [':id'=> $id,
         ':value'=>$update
        ]);

        $result = $command->queryAll();

        if($result){
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionDeactivateSplitLine($id,$now,$snfg)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            SELECT count(id) FROM flow_input_snfg WHERE snfg = :snfg AND is_split_line = 1 AND datetime_stop is null", [':snfg'=>$snfg]);
        $line_split_active = $command->queryScalar();

        if ($line_split_active <= 1){
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE flow_input_snfg
            SET is_split_line = :value
            WHERE snfg = :snfg;
            ",
            [':snfg'=> $snfg,
             ':value'=>0
            ]);

            $result = $command->queryAll();

            if($result){
                echo Json::encode(['status'=>'success']);
                // return $this->redirect(Yii::$app->request->referrer);
            }
        } else {
            echo Json::encode(['status'=>'cannot-off']);
        }


    }

    public function actionPrintZebradirect($w,$id)
    {

        // exec("ssh ptiadmin@10.128.1.161 'lpr -P Zebra_Technologies_ZTC_GT800_ -o raw /home/ptiadmin/labelasset_101281161_test.zpl'");
        // return $this->redirect(Yii::$app->request->referrer);


        $frontend_ip = $_SERVER['REMOTE_ADDR'];

        $sql="
        SELECT
            *
        FROM weigher_fg_map_device
        WHERE frontend_ip = '".$frontend_ip."'";

        $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

        $sbc_ip = $map_device_array->sbc_ip;
        $sbc_id = str_replace(".","",$sbc_ip);
        // $line = $map_device_array->line;

        // Get Sequence
        $sql="
            select
                count(id)+1 as id
            from flow_input_snfg_wo
            where flow_input_snfg_id=".$id;

        $sequence_array = FlowInputSnfg::findBySql($sql)->one();

        // Get Label Data from Master Data
        $sql="
            select
              fis.nama_operator,
              sp.nama_fg,
              sk.koli,
              fis.nobatch,
              (current_timestamp at time zone 'gmt -7')::date as pack_date,
              (current_timestamp at time zone 'gmt -7' + interval '30 days')::date as exp_date
            from flow_input_snfg fis
            left join (select distinct on (snfg) * from scm_planner order by snfg) sp on fis.snfg=sp.snfg
            left join standar_koli sk on sk.koitem = sp.koitem_fg
            where fis.id=".$id;

        $label_array = FlowInputSnfg::findBySql($sql)->one();

        $myfile = fopen("koli_".$sbc_id.".zpl", "w");

        $txt = '
                ^XA
                ^CFd0,10,10
                ^LRY
                ^MD30
                ^PW600
                ^PON
                ^FO17,85^FDOP : '.$label_array->nama_operator.'^FS^FO33,67^FS
                ^FB500,2,0^FO17,15^FD'.$label_array->nama_fg.'^FS
                ^FO140,127^FDPck. '.$label_array->pack_date.'^FS
                ^FO140,157^FDExp. '.$label_array->exp_date.'^FS
                ^FO140,187^FDWgt. '.$w.' kg^FS
                ^FO380,127^FDQty. '.$label_array->koli.' Pcs^FS
                ^FO380,157^FDBch. '.$label_array->nobatch.'^FS
                ^FO380,187^FDUrt. Ke-'.$sequence_array->id.'^FS
                ^FO17,110^BY5,1.0,10^BQN,2,5^FDMA,'.$id.'_'.$sequence_array->id.'^FS
                ^XZ
        ';

        fwrite($myfile, $txt);
        fclose($myfile);

        // Write Into Database
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            INSERT INTO flow_input_snfg_wo (weight,flow_input_snfg_id,pack_date,exp_date,nourut)
            SELECT
                :weight as weight,
                :flow_input_snfg_id as flow_input_snfg_id,
                :pack_date as pack_date,
                :exp_date as exp_date,
                :nourut as nourut

            ;
            ",
            [
                ':weight' => $w,
                ':flow_input_snfg_id' => $id,
                ':pack_date' => $label_array->pack_date,
                ':exp_date' => $label_array->exp_date,
                ':nourut' => $sequence_array->id
            ]);

            $result = $command->queryAll();

            if($result){
                 // Script to copy unique label to remote address
                exec('scp /var/www/html/flowreport/web/koli_'.$sbc_id.'.zpl pi@'.$sbc_ip.':/home/pi/koli_'.$sbc_id.'.zpl 2>&1', $output);
                // exec('whoami 2>&1',$output);

                 // Execute Printing remotely
                exec("ssh pi@".$sbc_ip." 'lpr -P Zebra_Technologies_ZTC_GT800_ -o raw /home/pi/koli_".$sbc_id.".zpl'");
                return $this->redirect(Yii::$app->request->referrer);
            }


        // $client_ip = $_SERVER['REMOTE_ADDR'];

        // $client_id = str_replace(".","",$client_ip);

        // print_r($client_ip);

        // Execute Printing remotely
            // exec("ssh ptiadmin@".$client_ip." 'lpr -P Zebra_Technologies_ZTC_GT800_ -o raw /home/ptiadmin/labelasset_101281161_test.zpl'");



            // exec("ssh ptiadmin@".$sbc_ip." 'lpr -P Zebra_Technologies_ZTC_GT800_ -o raw /home/ptiadmin/labelasset_101281161_test.zpl'");
        // print_r($output);
        // echo 'scp /var/www/html/flowreport/web/labelsnfg_'.$client_id.'.zpl pi@'.$client_ip.':/home/pi/labelsnfg_'.$client_id.'.zpl';

    }

    public function actionRestartProgram($ip){
        try {
            $connection = ssh2_connect($ip, 22);
            ssh2_auth_password($connection, 'pi', 'ptiuser1234');

            $stream = ssh2_exec($connection, 'sudo killall python');
            if ($stream){
                $stream2 = ssh2_exec($connection, 'cancel -a');
                if ($stream2){
                    $stream3 = ssh2_exec($connection, 'sudo python /home/pi/QCFG_Fix.py &');
                    $value = array("status"=>"done");
                }
            }

        }
        catch (exception $e) {
            $value = array("status"=>"not-done");
        }

        echo Json::encode($value);
    }

    public function actionClearQueue($ip){
        try {
            $connection = ssh2_connect($ip, 22);
            ssh2_auth_password($connection, 'pi', 'ptiuser1234');

            $stream = ssh2_exec($connection, 'cancel -a');
            if ($stream){
                $value = array("status"=>"done");
            }

        }
        catch (exception $e) {
            $value = array("status"=>"not-done");
        }

        echo Json::encode($value);
    }

    public function actionRestartRaspi($ip){
        try {
            $connection = ssh2_connect($ip, 22);
            ssh2_auth_password($connection, 'pi', 'ptiuser1234');

            $stream2 = ssh2_exec($connection, 'cancel -a');
            if ($stream2){
                $stream = ssh2_exec($connection, 'sudo reboot now');
                if ($stream){
                    $value = array("status"=>"done");
                }
            }

        }
        catch (exception $e) {
            $value = array("status"=>"not-done");
        }

        echo Json::encode($value);
    }

    public function actionInsertLogProblem($id,$event,$status){
        $sql="
        SELECT nama_line FROM flow_input_snfg WHERE id =".$id." ORDER BY id DESC";

        $nama_line_array = FlowInputSnfg::findBySql($sql)->one();
        $nama_line = $nama_line_array->nama_line;

        $connection2 = Yii::$app->getDb();
        $command2 = $connection2->createCommand("

        INSERT INTO log_inline_weigher (flow_input_snfg_id,event,nama_line,status)
        SELECT
            :id as flow_input_snfg_id,
            :event as event,
            :nama_line as nama_line,
            :status as status
        ;
        ",
        [
            ':id' => $id,
            ':event' => $event,
            ':nama_line' => $nama_line,
            ':status' => $status
        ]);

        $result = $command2->queryAll();

        if ($result) {
            $value = array("status"=>"done");
        } else {
            $value = array("status"=>"not-done");
        }

        echo Json::encode($value);
    }

    public function actionPrintZebrafile($w,$id)
    {

        $sql="
            select
              fis.nama_operator,
              sp.nama_fg,
              sk.koli,
              fis.nobatch,
              (current_timestamp at time zone 'gmt -7')::date as pack_date,
              (current_timestamp at time zone 'gmt -7' + interval '30 days')::date as exp_date
            from flow_input_snfg fis
            left join (select distinct on (snfg) * from scm_planner order by snfg) sp on fis.snfg=sp.snfg
            left join standar_koli sk on sk.koitem = sp.koitem_fg
            where fis.id=".$id;

        $label_array = FlowInputSnfg::findBySql($sql)->one();

        $sql="
            select
                count(id)+1 as id
            from flow_input_snfg_wo
            where flow_input_snfg_id=".$id;

        $sequence_array = FlowInputSnfg::findBySql($sql)->one();


            $txt = '
                ^XA
                ^CFd0,10,10
                ^LRY
                ^MD30
                ^PW600
                ^PON
                ^FO17,85^FDOP : '.$label_array->nama_operator.'^FS^FO33,67^FS
                ^FB500,2,0^FO17,15^FD'.$label_array->nama_fg.'^FS
                ^FO140,127^FDPck. '.$label_array->pack_date.'^FS
                ^FO140,157^FDExp. '.$label_array->exp_date.'^FS
                ^FO140,187^FDWgt. '.$w.' kg^FS
                ^FO380,127^FDQty. '.$label_array->koli.' Pcs^FS
                ^FO380,157^FDBch. '.$label_array->nobatch.'^FS
                ^FO380,187^FDUrt. Ke-'.$sequence_array->id.'^FS
                ^FO17,110^BY5,1.0,10^BQN,2,5^FDMA,'.$id.'_'.$sequence_array->id.'^FS
                ^XZ
            ';

            $curl = curl_init();
            // adjust print density (8dpmm), label width (4 inches), label height (6 inches), and label index (0) as necessary
            curl_setopt($curl, CURLOPT_URL, "http://api.labelary.com/v1/printers/8dpmm/labels/3x1.3/0/");
            curl_setopt($curl, CURLOPT_POST, TRUE);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $txt);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Accept: application/pdf")); // omit this line to get PNG images back
            $result = curl_exec($curl);

            if (curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200) {
                $file = fopen("label.pdf", "w"); // change file name for PNG images
                fwrite($file, $result);
                fclose($file);

            } else {
                print_r("Error: $result");
            }

            curl_close($curl);


            $pdf = '/var/www/html/flowreport/web/label.pdf';



            // Write Into Database
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            INSERT INTO flow_input_snfg_wo (weight,flow_input_snfg_id,pack_date,exp_date,nourut)
            SELECT
                :weight as weight,
                :flow_input_snfg_id as flow_input_snfg_id,
                :pack_date as pack_date,
                :exp_date as exp_date,
                :nourut as nourut

            ;
            ",
            [
                ':weight' => $w,
                ':flow_input_snfg_id' => $id,
                ':pack_date' => $label_array->pack_date,
                ':exp_date' => $label_array->exp_date,
                ':nourut' => $sequence_array->id
            ]);

            $result = $command->queryAll();

            if($result){
                return (Yii::$app->response->sendFile($pdf));
                // return $this->redirect(Yii::$app->request->referrer);
            }

    }

    /**
     * Render Scan Barcode QR SNFG Kemas 2 Page
     * @return mixed
     */
    public function actionCheckKemas2Qr()
    {

            // $frontend_ip = $_SERVER['REMOTE_ADDR'];

            if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }else{
                $frontend_ip = $_SERVER['REMOTE_ADDR'];
            }

            $sql="
            SELECT
                *
            FROM weigher_fg_map_device
            WHERE frontend_ip = '".$frontend_ip."'";

            $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

            if(empty($map_device_array)){
                $sbc_ip = '10.128.1.161';
                $line = 'Line A';
            }else{
                $sbc_ip = $map_device_array->sbc_ip;
                $line = $map_device_array->line;

            }


            $model = new FlowInputSnfg();

            $searchModel = new FlowInputSnfgSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // $dataProvider->query->where("posisi='PENIMBANGAN' and datetime_start is not null and datetime_stop is null");

            $this->layout = '//main-sidebar-collapse';

            return $this->render('check-kemas2-qr', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'frontend_ip' => $frontend_ip,
                    'sbc_ip' => $sbc_ip,
                    'line' => $line,
            ]);
            // if ($model->load(Yii::$app->request->post())) {
            //     // return $this->redirect(['create-penimbangan', 'kode_unik_formula' => $model->kode_unik_formula]);
            // } else {

            // }
    }

    // Testing QR Plugin ZXing Check Kemas2
    public function actionCheckKemas2QrZxing()
    {

            $frontend_ip = $_SERVER['REMOTE_ADDR'];

            $sql="
            SELECT
                *
            FROM weigher_fg_map_device
            WHERE frontend_ip = '".$frontend_ip."'";

            $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

            if(empty($map_device_array)){
                $sbc_ip = '10.128.1.161';
                $line = 'Line A';
            }else{
                $sbc_ip = $map_device_array->sbc_ip;
                $line = $map_device_array->line;

            }


            $model = new FlowInputSnfg();

            $searchModel = new FlowInputSnfgSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // $dataProvider->query->where("posisi='PENIMBANGAN' and datetime_start is not null and datetime_stop is null");

            $this->layout = '//main-sidebar-collapse';

            return $this->render('check-kemas2-qr-zxing', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'frontend_ip' => $frontend_ip,
                    'sbc_ip' => $sbc_ip,
                    'line' => $line,
            ]);
            // if ($model->load(Yii::$app->request->post())) {
            //     // return $this->redirect(['create-penimbangan', 'kode_unik_formula' => $model->kode_unik_formula]);
            // } else {

            // }
    }

    /**
     * Render Scan Barcode QR SNFG Kemas 2 Page
     * @return mixed
     */
    public function actionCheckKemas2Tab()
    {

            $frontend_ip = $_SERVER['REMOTE_ADDR'];

            $sql="
            SELECT
                *
            FROM weigher_fg_map_device
            WHERE frontend_ip = '".$frontend_ip."'";

            $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

            if(empty($map_device_array)){
                $sbc_ip = '10.128.1.161';
                $line = 'Line A';
            }else{
                $sbc_ip = $map_device_array->sbc_ip;
                $line = $map_device_array->line;

            }


            $model = new FlowInputSnfg();

            $searchModel = new FlowInputSnfgSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // $dataProvider->query->where("posisi='PENIMBANGAN' and datetime_start is not null and datetime_stop is null");

            $this->layout = '//main-sidebar-collapse';

            return $this->render('check-kemas2-tab', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'frontend_ip' => $frontend_ip,
                    'sbc_ip' => $sbc_ip,
                    'line' => $line,
            ]);
            // if ($model->load(Yii::$app->request->post())) {
            //     // return $this->redirect(['create-penimbangan', 'kode_unik_formula' => $model->kode_unik_formula]);
            // } else {

            // }
    }

    /**
     * Retrieve New Sequence of Lanjutan of particular parameter
     * @param $snfg string
     * @param $jenis_kemas string
     * @param $posisi string
     * @return mixed
     */
    public function actionGetLanjutanKemas2($snfg,$jenis_kemas,$posisi)
    {
        $sql="
        SELECT
            coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM flow_input_snfg
        WHERE snfg = '".$snfg."'
        and jenis_kemas = '".$jenis_kemas."'
        and posisi = '".$posisi."'
        ";

        $lanjutan= FlowInputSnfg::findBySql($sql)->one();
        echo Json::encode($lanjutan);
    }

    /**
     * Render Page Form for Kemas 2
     * This will render a page which the results typically comes from barcode scanning result
     * Redirected from actionCheckKemas2[/QR/Inline]
     * @param $snfg
     * @return mixed
     */
    public function actionCreateKemas2($snfg)
    {

        $mod = new ScmPlanner();

        $result = $mod->periksaStatusSnfg($snfg);

        $a = json_decode($result);

        // print_r($a->status);
        // die;

        if($a->status == "not-found"){
            throw new \yii\web\HttpException(401,
          'Jadwal untuk SNFG tidak ditemukan. Pastikan input sudah benar atau tidak typoo. Jika input sudah benar, pastikan PPC sudah upload jadwal untuk SNFG ini.');
        }

        if($a->status=='UNHOLD')
        {

            // Get Device Information
                // if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                //     $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                // }else{
                //     $frontend_ip = $_SERVER['REMOTE_ADDR'];
                // }

                // $sql="
                // SELECT
                //     *
                // FROM weigher_fg_map_device
                // WHERE frontend_ip = '".$frontend_ip."'";

                // $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

                // if(empty($map_device_array)){
                //     $sbc_ip = 'LINKAGE NOT FOUND!';
                //     $line_tmp = 'LINKAGE NOT FOUND!';
                //     $keterangan = 'LINKAGE NOT FOUND!';
                // }else{
                //     $sbc_ip = $map_device_array->sbc_ip;
                //     $line_tmp = $map_device_array->line;
                //     $keterangan = $map_device_array->keterangan;

                // }

                // $sqlk="
                // SELECT
                //     *
                // FROM master_data_line
                // WHERE nama_line = '".$line_tmp."'";

                // $master_line = MasterDataLine::findBySql($sqlk)->all();

                // if(empty($master_line)){
                //     $line = NULL;
                // }else{
                //     $line = $line_tmp;
                // }

                //Get Sediaan
                $sp = ScmPlanner::find()->where(['snfg'=>$snfg])->one();
                $sediaan = $sp->sediaan;
                $odoo_code = $sp->odoo_code;

                $model = new FlowInputSnfg();

                // Populate Grid Data
                $searchModel = new FlowInputSnfgSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->query->where("snfg='".$snfg."' and posisi='KEMAS_2'");

                $this->layout = '//main-sidebar-collapse';

                //Check Last State of a SNFG
                $sql="

                SELECT
                    distinct on (snfg)
                    id,
                    snfg,
                    datetime_start,
                    datetime_stop,
                    is_done_wo,
                    is_split_line
                FROM flow_input_snfg
                WHERE snfg = '".$snfg."' and posisi='KEMAS_2'
                ORDER BY snfg,id desc
                ";

                $row = FlowInputSnfg::findBySql($sql)->one();

                // Assign Variable Null to prevent error if it's the first data entry
                if(empty($row)){
                    $last_datetime_start = null;
                    $last_datetime_stop = null;
                    $last_id = null;
                    $is_done_wo = null;
                    $is_split = null;
                }else{
                    $last_datetime_start = $row->datetime_start;
                    $last_datetime_stop = $row->datetime_stop;
                    $last_id = $row->id;
                    $is_done_wo = $row->is_done_wo;
                    if ($row->is_split_line == 1){
                        $is_split = 1;
                    } else {
                        $is_split = 0;
                    }
                }


                //Check Last State of a Snfg
                // $sql="

                // select nama_line
                // from flow_input_snfg
                // where snfg = '".$snfg."'
                // and posisi='KEMAS_2'
                // and lanjutan = 1
                // ";

                // $nama_line_row = FlowInputSnfg::findBySql($sql)->one();

                // if(empty($nama_line_row)){
                //     $nama_line = null;
                // }else{
                //     $nama_line = $nama_line_row->nama_line;
                // }


                // Jenis Proses Check Status Jadwal, If Found then Don't Display
                $sql3="
                        SELECT
                            jenis_proses_kemas2
                        FROM jenis_proses_kemas2
                        WHERE jenis_proses_kemas2 not in
                        (SELECT jenis_proses FROM status_jadwal WHERE
                        nojadwal='".$snfg."' and posisi='KEMAS_2')
                    ";

                $jenis_proses_list = ArrayHelper::map(JenisProsesKemas2::findBySql($sql3)->all(), 'jenis_proses_kemas2','jenis_proses_kemas2');

                /******* GET NO SMB (NOMO) FROM ODOO MRP **********/
                $connection8 = Yii::$app->db2;
                $command8 = $connection8->createCommand("

                SELECT
                    name
                FROM mrp_production
                WHERE origin = :snfg AND state = 'confirmed'
                ",
                [':snfg' => $snfg,
                ]);

                $data8 = $command8->queryAll();

                if(empty($data8)){
                    $nosmb = "-";
                } else {
                    $nosmb = $data8[0]['name'];
                }
                /**************************************************/

                /**************************************************/
                /************** GET NO BATCH **********************/
                //Check Last State of a Snfg
                // $sql9="

                // select snfg_komponen
                // from scm_planner
                // where snfg = '".$snfg."' and sediaan = 'L'
                // ";

                // $scm_planner = ScmPlanner::findBySql($sql9)->one();
                // if(empty($scm_planner)){
                //     $nobatch = "-";
                // } else {
                //     $snfg_komponen = $scm_planner->snfg_komponen;

                //     $sql10="

                //     select nobatch
                //     from pengolahan_batch
                //     where snfg_komponen = '".$snfg_komponen."' ORDER BY id desc
                //     ";

                //     $nobatch_row = ScmPlanner::findBySql($sql10)->one();
                //     if(empty($nobatch_row)){
                //         $nobatch = "-";
                //     } else {
                //         $nobatch = $nobatch_row->nobatch;
                //     }
                // }

                //Get NoBatch dari Olah
                $result = $this->actionGenerateBatchKemas($snfg);
                $nobatch = $result ['nobatch'];
                $exp_date = $result ['exp_date'];

                // Check If Snfg Last Status is Start, if yes then redirect to actionStopKemas1
                if(!empty($last_datetime_start)&&(empty($last_datetime_stop))&&(!$is_split)){
                    return $this->redirect(['stop-kemas2','snfg' => $snfg,'last_id'=>$last_id,'pos'=>'pusat']);
                }
                // if(!empty($last_datetime_start)&&(empty($last_datetime_stop))){

                //     if($is_done_wo==1){
                //         // return $this->redirect(['stop-kemas2-inline','snfg' => $snfg,'last_id'=>$last_id]);
                //         return $this->redirect(['stop-kemas2','snfg' => $snfg,'last_id'=>$last_id]);
                //     }else{
                //         return $this->redirect(['weigher-fg','id'=>$last_id]);
                //     }

                // }

                /********* GET PRODUCT NAME FROM ODOO MRP *********/
                $connection = Yii::$app->db2;
                $command = $connection->createCommand("

                SELECT
                    pt.description as odoo_code,
                    pt.name
                FROM product_template pt
                WHERE pt.default_code = :koitem
                ",
                [':koitem' => strtok($snfg, '/'),
                ]);

                $data2 = $command->queryAll();

                if (empty($data2)){
                    $connection = Yii::$app->db_varcos;
                    $command = $connection->createCommand("

                    SELECT
                        pt.default_code as odoo_code,
                        pt.name
                    FROM product_template pt
                    WHERE pt.old_koitem = :koitem
                    ",
                    [':koitem' => strtok($snfg, '/'),
                    ]);
                    $data2 = $command->queryAll();
                }

                // if ($sediaan == 'V'){
                //     $connection = Yii::$app->db_varcos;
                //     $command = $connection->createCommand("

                //     SELECT
                //         pt.default_code as odoo_code,
                //         pt.name
                //     FROM product_template pt
                //     WHERE pt.old_koitem = :koitem
                //     ",
                //     [':koitem' => strtok($snfg, '/'),
                //     ]);
                //     $data2 = $command->queryAll();


                // }else{
                //     $connection = Yii::$app->db2;
                //     $command = $connection->createCommand("

                //     SELECT
                //         pt.description as odoo_code,
                //         pt.name
                //     FROM product_template pt
                //     WHERE pt.default_code = :koitem
                //     ",
                //     [':koitem' => strtok($snfg, '/'),
                //     ]);

                //     $data2 = $command->queryAll();

                // }

                // print_r($data2);

                if (empty($data2)){
                    $fg_name = "";
                } else {
                    $fg_name = $data2[0]['name'];
                }
                /**************************************************/


                /******* GET PRODUCT BARCODE FROM ODOO MRP ********/
                $kode_item = strtok($snfg, '/');

                if (strpos($kode_item, 'MY-') !== false) {
                    $sqlj="
                    select *
                    from master_data_barcode_malaysia
                    where koitem_lama = '".$kode_item."' or koitem_baru = '".$kode_item."'
                    ";

                    $mdbm = MasterDataBarcodeMalaysia::findBySql($sqlj)->one();

                    if (empty($mdbm)){
                        $barcode = "";
                    } else {
                        $barcode = $mdbm->barcode_harmonize;
                    }
                } else {

                    $connection3 = Yii::$app->db_paragon;
                    $command3 = $connection3->createCommand("

                    SELECT
                        pt.old_koitem,
                        pp.barcode
                    FROM product_product pp
                    LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id
                    WHERE pt.old_koitem = :koitem
                    ",
                    [':koitem' => strtok($snfg, '/'),
                    ]);

                    $data3 = $command3->queryAll();

                    if (empty($data3)){
                        $connection4 = Yii::$app->db_varcos;
                        $command4 = $connection4->createCommand("

                        SELECT
                            pt.old_koitem,
                            pp.barcode
                        FROM product_product pp
                        LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id
                        WHERE pt.old_koitem = :koitem
                        ",
                        [':koitem' => strtok($snfg, '/'),
                        ]);

                        $data4 = $command4->queryAll();

                        if (empty($data4)){
                            $barcode = "";
                        } else {
                            $barcode = $data4[0]['barcode'];
                        }
                    } else {
                        $barcode = $data3[0]['barcode'];
                    }
                }


                /**************************************************/

                // Check If Post Value True
                // if ($model->load(Yii::$app->request->post()) && $model->save()) {
                if ($model->load(Yii::$app->request->post())) {

                    $model->barcode = $model->barcode;
                    $model->fg_name_odoo = $model->fg_name_odoo;
                    $model->nama_line = $model->nama_line;
                    $model->posisi_scan = "pusat";

                    if ($model->exp_date){
                        $update = Yii::$app->db->createCommand("UPDATE flow_input_snfg set exp_date = :exp_date where snfg = :snfg and nobatch = :nobatch",[':exp_date'=>$model->exp_date,':snfg'=>$model->snfg,':nobatch'=>$model->nobatch])->execute();
                    }
                    //Check Last State of a Snfg
                    // $sql_batch="

                    // select nobatch
                    // from scm_planner
                    // where snfg = '".$snfg."'
                    // ";

                    // $nobatch_sp = ScmPlanner::findBySql($sql_batch)->one();
                    $kode_tahun = "";
                    if (!empty($model->nobatch)){
                        $kode_tahun = substr($model->nobatch,1,1);
                    } else {
                        $kode_tahun = "";
                    }

                    if (strpos($snfg, '/BS') !== false and $kode_tahun == "G") {
                        $month = date("m",strtotime($model->exp_date));
                        if ($month == 1 or $month == 7){
                            $model->lakban = 'HIJAU';
                        } else if ($month == 2 or $month == 8){
                            $model->lakban = 'ORANYE';
                        } else if ($month == 3 or $month == 9){
                            $model->lakban = 'UNGU';
                        } else if ($month == 4 or $month == 10){
                            $model->lakban = 'PUTIH';
                        } else if ($month == 5 or $month == 11){
                            $model->lakban = 'TOSKA';
                        } else if ($month == 6 or $month == 12){
                            $model->lakban = 'MERAH MUDA';
                        }
                    } else {
                        $month = date("m",strtotime($model->exp_date));
                        if ($month == 1 or $month == 2){
                            $model->lakban = 'HIJAU';
                        } else if ($month == 3 or $month == 4){
                            $model->lakban = 'ORANYE';
                        } else if ($month == 5 or $month == 6){
                            $model->lakban = 'UNGU';
                        } else if ($month == 7 or $month == 8){
                            $model->lakban = 'PUTIH';
                        } else if ($month == 9 or $month == 10){
                            $model->lakban = 'TOSKA';
                        } else if ($month == 11 or $month == 12){
                            $model->lakban = 'MERAH MUDA';
                        }
                    }
                    $model->reason = "-";
                    $model->lakban_old = $model->lakban;
                    $model->save();
                    $id = $model->id;

                    return $this->redirect(['check-kemas2']);
                } else {
                    return $this->render('create-kemas2', [
                        'model' => $model,
                        'snfg' => $snfg,
                        'last_datetime_start' => $last_datetime_start,
                        'last_datetime_stop' => $last_datetime_stop,
                        'last_id' => $last_id,
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        // 'nama_line' => $line,
                        'jenis_proses_list' => $jenis_proses_list,
                        // 'frontend_ip' => $frontend_ip,
                        // 'sbc_ip' => $sbc_ip,
                        'odoo_code' => $odoo_code,
                        'nosmb' => $nosmb,
                        'nobatch' => $nobatch,
                        'exp_date' => $exp_date,
                        'barcode' => $barcode,
                        'fg_name' => $fg_name,
                        // 'keterangan' => $keterangan
                    ]);
                }

        }else {
            throw new \yii\web\HttpException(401,
          'Jadwal telah di HOLD / CANCEL mohon hubungi Planner');
        }
    }

    public function actionUpdateIp($frontend_ip)
    {
        $cookie_name = 'device_identity';

        $device = DeviceMapQcbulk::find()->where(['frontend_ip'=>$frontend_ip])->one();

        if (empty($device)){

            if(isset($_COOKIE[$cookie_name])) {
                $sql = "SELECT * FROM device_map_qcbulk WHERE MD5(frontend_ip) = :ip";
                $select = DeviceMapQcbulk::findBySql($sql,[':ip'=>$_COOKIE[$cookie_name]])->one(); 

                $sql = "UPDATE device_map_qcbulk set frontend_ip = :new_ip WHERE MD5(frontend_ip) = :ip";
                $update = Yii::$app->db->createCommand($sql,[':new_ip'=>$frontend_ip,':ip'=>$_COOKIE[$cookie_name]])->execute();
                if ($update > 0){
                    setcookie($cookie_name,hash('md5',$frontend_ip));
                }
            }

        }else{
            setcookie($cookie_name,hash('md5',$device->frontend_ip));
        }

    }


    /**
     * Render Page Form for Kemas 2 Inline
     * This will render a page which the results typically comes from barcode scanning result
     * Redirected from actionCheckKemas2[/QR/Inline]
     * @param $snfg
     * @return mixed
     */
    public function actionCreateKemas2Inline($snfg)
    {
        // Get Device Information
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $frontend_ip = $_SERVER['REMOTE_ADDR'];
        }

        // $this->actionUpdateIp($frontend_ip);

        $mod = new ScmPlanner();

        $result = $mod->periksaStatusSnfg($snfg);

        $a = json_decode($result);

        // print_r($a->status);
        // die;

        //Get Sediaan
        $sediaan = ScmPlanner::find()->where(['snfg'=>$snfg])->one()['sediaan'];

        if($a->status == "not-found"){
            throw new \yii\web\HttpException(401,
          'Jadwal untuk SNFG tidak ditemukan. Pastikan input sudah benar atau tidak typoo. Jika input sudah benar, pastikan PPC sudah upload jadwal untuk SNFG ini.');
        }

        if($a->status=='UNHOLD')
        {
            // Get Device Information
            if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }else{
                $frontend_ip = $_SERVER['REMOTE_ADDR'];
            }

            $sql="
            SELECT
                *
            FROM weigher_fg_map_device
            WHERE frontend_ip = '".$frontend_ip."'";

            $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

            if(empty($map_device_array)){
                $sbc_ip = 'LINKAGE NOT FOUND!';
                $line = NULL;
                $keterangan = 'LINKAGE NOT FOUND!';
            }else{
                $sbc_ip = $map_device_array->sbc_ip;
                $line = $map_device_array->line;
                $keterangan = $map_device_array->keterangan;
            }

            // $sqlk="
            // SELECT
            //     *
            // FROM master_data_line
            // WHERE nama_line = '".$line_tmp."'";

            // $master_line = MasterDataLine::findBySql($sqlk)->all();

            // if(empty($master_line)){
            //     $line = NULL;
            // }else{
            //     $line = $line_tmp;
            // }


            $model = new FlowInputSnfg();

            // Populate Grid Data
            $searchModel = new FlowInputSnfgSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where("snfg='".$snfg."' and posisi='KEMAS_2'");

            $this->layout = '//main-sidebar-collapse';

            //Check Last record of a SNFG which run in certain line
            $sql="

            SELECT
                id,
                snfg,
                datetime_start,
                datetime_stop,
                is_done_wo,
                nama_line,
                is_split_line
            FROM flow_input_snfg
            WHERE snfg = :snfg and nama_line = :nama_line and posisi='KEMAS_2' and datetime_stop is null
            ORDER BY snfg,id desc
            ";

            $row = FlowInputSnfg::findBySql($sql,['snfg'=>$snfg,'nama_line'=>$line])->one();

            if(!empty($row)){
                $last_datetime_start = $row->datetime_start;
                $last_datetime_stop = $row->datetime_stop;
                $last_id = $row->id;
                $nama_line = $row->nama_line;
                $is_done_wo = $row->is_done_wo;
                if ($row->is_split_line == 1){
                    $is_split = 1;
                } else {
                    $is_split = 0;
                }

            }else{
                //Check Last record of a SNFG
                $sql="

                SELECT
                    id,
                    snfg,
                    datetime_start,
                    datetime_stop,
                    is_done_wo,
                    nama_line,
                    is_split_line
                FROM flow_input_snfg
                WHERE snfg = :snfg and posisi='KEMAS_2' and datetime_stop is null
                ORDER BY snfg,id desc
                ";

                $last_record = FlowInputSnfg::findBySql($sql,['snfg'=>$snfg])->one();

                if (!empty($last_record)){
                    $last_datetime_start = $last_record->datetime_start;
                    $last_datetime_stop = $last_record->datetime_stop;
                    $last_id = $last_record->id;
                    $nama_line = $last_record->nama_line;
                    $is_done_wo = $last_record->is_done_wo;
                    if ($last_record->is_split_line == 1){
                        $is_split = 1;
                    } else {
                        $is_split = 0;
                    }
                }else{
                    // Assign Variable Null to prevent error if it's the first data entry
                    $last_datetime_start = null;
                    $last_datetime_stop = null;
                    $last_id = null;
                    $is_done_wo = null;
                    $is_split = null;
                    $nama_line = null;
                }

            }


            //Check Last State of a Snfg
            // $sql="

            // select nama_line
            // from flow_input_snfg
            // where snfg = '".$snfg."'
            // and posisi='KEMAS_2'
            // and lanjutan = 1
            // ";

            // $nama_line_row = FlowInputSnfg::findBySql($sql)->one();

            // if(empty($nama_line_row)){
            //     $nama_line = null;
            // }else{
            //     $nama_line = $nama_line_row->nama_line;
            // }


            // Jenis Proses Check Status Jadwal, If Found then Don't Display
            $sql3="
                    SELECT
                        jenis_proses_kemas2
                    FROM jenis_proses_kemas2
                    WHERE jenis_proses_kemas2 not in
                    (SELECT jenis_proses FROM status_jadwal WHERE
                    nojadwal='".$snfg."' and posisi='KEMAS_2')
                ";

            $jenis_proses_list = ArrayHelper::map(JenisProsesKemas2::findBySql($sql3)->all(), 'jenis_proses_kemas2','jenis_proses_kemas2');

            /******* GET NO SMB (NOMO) FROM ODOO MRP **********/
            $connection8 = Yii::$app->db2;
            $command8 = $connection8->createCommand("

            SELECT
                name
            FROM mrp_production
            WHERE origin = :snfg AND state = 'confirmed'
            ",
            [':snfg' => $snfg,
            ]);

            $data8 = $command8->queryAll();

            if(empty($data8)){
                $nosmb = "-";
            } else {
                $nosmb = $data8[0]['name'];
            }
            /**************************************************/

            /**************************************************/
            /************** GET NO BATCH **********************/
            //Check Last State of a Snfg
            $sql9="

            select snfg_komponen, nomo, odoo_code
            from scm_planner
            where snfg = '".$snfg."'
            ";

            $scm_planner = ScmPlanner::findBySql($sql9)->one();
            if(empty($scm_planner)){
                $nobatch = "-";
                $odoo_code = null;
            } else {
                $snfg_komponen = $scm_planner->snfg_komponen;
                $odoo_code = $scm_planner->odoo_code;

                $sql10="

                select nobatch
                from pengolahan_batch
                where snfg_komponen = '".$snfg_komponen."' ORDER BY id desc
                ";

                $nobatch_row = ScmPlanner::findBySql($sql10)->one();
                if(empty($nobatch_row)){
                    $nobatch = "-";
                } else {
                    $nobatch = $nobatch_row->nobatch;
                }
            }

            //Get NoBatch dari Olah
            $result = $this->actionGenerateBatchKemas($snfg);
            $nobatch = $result ['nobatch'];
            $exp_date = $result ['exp_date'];


            // Check If Snfg Last Status is Start, if yes then redirect to actionStopKemas1
            if(!empty($last_datetime_start)&&(empty($last_datetime_stop)) ){
                // Memastikan redirect ke work order atau ke halaman stop hanya jika nama line di tab sama dengan nama line di record sebelumnya.
                if ($line == $nama_line){
                    if($is_done_wo==1){
                        return $this->redirect(['stop-kemas2','snfg' => $snfg,'last_id'=>$last_id,'pos'=>'inline']);
                    }else{
                        // return $this->redirect(['weigher-fg','id'=>$last_id]);
                        echo "<script>location.href='".Yii::getAlias('@ipUrl')."flow-input-snfg%2Fweigher-fg&id=";
                        echo $last_id;
                        echo "';</script>";
                    }
                }

            }


            /********* GET PRODUCT NAME FROM ODOO MRP *********/
            $connection = Yii::$app->db2;
            $command = $connection->createCommand("

            SELECT
                pt.description as odoo_code,
                pt.name
            FROM product_template pt
            WHERE pt.default_code = :koitem
            ",
            [':koitem' => strtok($snfg, '/'),
            ]);

            $data2 = $command->queryAll();

            if (empty($data2)){
                $connection = Yii::$app->db_varcos;
                $command = $connection->createCommand("

                SELECT
                    pt.default_code as odoo_code,
                    pt.name
                FROM product_template pt
                WHERE pt.old_koitem = :koitem
                ",
                [':koitem' => strtok($snfg, '/'),
                ]);
                $data2 = $command->queryAll();

            }
            // if ($sediaan == 'V'){
            //     $connection = Yii::$app->db_varcos;
            //     $command = $connection->createCommand("

            //     SELECT
            //         pt.default_code as odoo_code,
            //         pt.name
            //     FROM product_template pt
            //     WHERE pt.old_koitem = :koitem
            //     ",
            //     [':koitem' => strtok($snfg, '/'),
            //     ]);
            //     $data2 = $command->queryAll();


            // }else{
            //     $connection = Yii::$app->db2;
            //     $command = $connection->createCommand("

            //     SELECT
            //         pt.description as odoo_code,
            //         pt.name
            //     FROM product_template pt
            //     WHERE pt.default_code = :koitem
            //     ",
            //     [':koitem' => strtok($snfg, '/'),
            //     ]);

            //     $data2 = $command->queryAll();
            // }

            if (empty($data2)){
                $fg_name = "";
            } else {
                $fg_name = $data2[0]['name'];
            }
            /**************************************************/


            /******* GET PRODUCT BARCODE FROM ODOO MRP ********/
            $kode_item = strtok($snfg, '/');

            if (strpos($kode_item, 'MY-') !== false) {
                $sqlj="
                select *
                from master_data_barcode_malaysia
                where koitem_lama = '".$kode_item."' or koitem_baru = '".$kode_item."'
                ";

                $mdbm = MasterDataBarcodeMalaysia::findBySql($sqlj)->one();

                if (empty($mdbm)){
                    $barcode = "";
                } else {
                    $barcode = $mdbm->barcode_harmonize;
                }
            } else {

                $connection3 = Yii::$app->db_paragon;
                $command3 = $connection3->createCommand("

                SELECT
                    pt.old_koitem,
                    pp.barcode
                FROM product_product pp
                LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id
                WHERE pt.old_koitem = :koitem
                ",
                [':koitem' => strtok($snfg, '/'),
                ]);

                $data3 = $command3->queryAll();

                if (empty($data3)){
                    $barcode = "";
                } else {
                    $barcode = $data3[0]['barcode'];
                }
            }
            /**************************************************/

            // Check If Post Value True
            // if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->load(Yii::$app->request->post())) {
                $model->barcode = $model->barcode;
                $model->fg_name_odoo = $model->fg_name_odoo;
                $model->nama_line = $model->nama_line;
                $model->posisi_scan = "inline";

                if ($model->exp_date){
                    $update = Yii::$app->db->createCommand("UPDATE flow_input_snfg set exp_date = :exp_date where snfg = :snfg and nobatch = :nobatch",[':exp_date'=>$model->exp_date,':snfg'=>$model->snfg,':nobatch'=>$model->nobatch])->execute();
                }

                //Check Last State of a Snfg
                // $sql_batch="

                // select nobatch
                // from scm_planner
                // where snfg = '".$snfg."'
                // ";

                // $nobatch_sp = ScmPlanner::findBySql($sql_batch)->one();
                $kode_tahun = "";
                if (!empty($model->nobatch)){
                    $kode_tahun = substr($model->nobatch,1,1);
                } else {
                    $kode_tahun = "";
                }

                if (strpos($snfg, '/BS') !== false and $kode_tahun == "G") {
                    $month = date("m",strtotime($model->exp_date));
                    if ($month == 1 or $month == 7){
                        $model->lakban = 'HIJAU';
                    } else if ($month == 2 or $month == 8){
                        $model->lakban = 'ORANYE';
                    } else if ($month == 3 or $month == 9){
                        $model->lakban = 'UNGU';
                    } else if ($month == 4 or $month == 10){
                        $model->lakban = 'PUTIH';
                    } else if ($month == 5 or $month == 11){
                        $model->lakban = 'TOSKA';
                    } else if ($month == 6 or $month == 12){
                        $model->lakban = 'MERAH MUDA';
                    }
                } else {
                    $month = date("m",strtotime($model->exp_date));
                    if ($month == 1 or $month == 2){
                        $model->lakban = 'HIJAU';
                    } else if ($month == 3 or $month == 4){
                        $model->lakban = 'ORANYE';
                    } else if ($month == 5 or $month == 6){
                        $model->lakban = 'UNGU';
                    } else if ($month == 7 or $month == 8){
                        $model->lakban = 'PUTIH';
                    } else if ($month == 9 or $month == 10){
                        $model->lakban = 'TOSKA';
                    } else if ($month == 11 or $month == 12){
                        $model->lakban = 'MERAH MUDA';
                    }
                }
                if ($is_split == 1){
                    $model->is_split_line = 1;
                } else {
                    $model->is_split_line = 0;
                }
                $model->reason = "-";
                $model->lakban_old = $model->lakban;
                $model->save();
                $id = $model->id;

                echo "<script>location.href='".Yii::getAlias('@ipUrl')."flow-input-snfg%2Fweigher-fg&id=";
                echo $id;
                echo "';</script>";
            } else {
                return $this->render('create-kemas2-inline', [
                    'model' => $model,
                    'snfg' => $snfg,
                    'last_datetime_start' => $last_datetime_start,
                    'last_datetime_stop' => $last_datetime_stop,
                    'last_id' => $last_id,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'nama_line' => $line,
                    'jenis_proses_list' => $jenis_proses_list,
                    'frontend_ip' => $frontend_ip,
                    'sbc_ip' => $sbc_ip,
                    'exp_date' => $exp_date,
                    'nosmb' => $nosmb,
                    'nobatch' => $nobatch,
                    'barcode' => $barcode,
                    'fg_name' => $fg_name,
                    'odoo_code' => $odoo_code,
                    'is_split' => $is_split
                ]);
            }

        }
        else {
            throw new \yii\web\HttpException(401,
          'Jadwal telah di HOLD / CANCEL mohon hubungi Planner');
        }

    }


    public function actionResolveKemas2($snfg,$lp_id)
    {
        $model = new FlowInputSnfg();

        // Populate Grid Data
        $searchModel = new FlowInputSnfgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("snfg='".$snfg."' and posisi='KEMAS_2'");

        $this->layout = '//main-sidebar-collapse';

        //Check Last State of a Snfg
        $sql="

        SELECT
            distinct on (snfg)
            id,
            snfg,
            datetime_start,
            datetime_stop
        FROM flow_input_snfg
        WHERE snfg = '".$snfg."' and posisi='KEMAS_2'
        ORDER BY snfg,id desc
        ";

        $row = FlowInputSnfg::findBySql($sql)->one();

        // Assign Variable Null to prevent error if it's the first data entry
        if(empty($row)){
            $last_datetime_start = null;
            $last_datetime_stop = null;
            $last_id = null;
        }else{
            $last_datetime_start = $row->datetime_start;
            $last_datetime_stop = $row->datetime_stop;
            $last_id = $row->id;
        }


        //Check Last State of a Snfg
        $sql="

        select nama_line
        from flow_input_snfg
        where snfg = '".$snfg."'
        and posisi='KEMAS_2'
        and lanjutan = (select max(lanjutan) from flow_input_snfgkomponen where snfg = '".$snfg."')
        ";

        $nama_line_row = FlowInputSnfg::findBySql($sql)->one();

        if(empty($nama_line_row)){
            $nama_line = null;
        }else{
            $nama_line = $nama_line_row->nama_line;
        }



        // Check If Snfg Last Status is Start, if yes then redirect to actionStopKemas1
        if(!empty($last_datetime_start)&&(empty($last_datetime_stop))){
            return $this->redirect(['stop-kemas2','snfg' => $snfg,'last_id'=>$last_id]);
        }

        // Check If Post Value True
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $connection3 = Yii::$app->getDb();
            $command3 = $connection3->createCommand("
                UPDATE pusat_resolusi_log
                SET proses_id = :id
                WHERE id = :lp_id;
                ",
                [':id'=> $model->id,':lp_id'=>$lp_id]
            );

            $result3 = $command3->queryAll();

            return $this->redirect(['downtime/index-kemas2', 'id' => $model->id]);
        } else {
            return $this->render('resolve-kemas2', [
                'model' => $model,
                'snfg' => $snfg,
                'last_datetime_start' => $last_datetime_start,
                'last_datetime_stop' => $last_datetime_stop,
                'last_id' => $last_id,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'nama_line' => $nama_line,
            ]);
        }
    }

    /**
     * Updates an existing FlowInputSnfg model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Stop Jadwal Kemas 2 Function
     * @param $snfg
     * @return mixed
     */
    public function actionStopKemas2($snfg,$last_id,$pos)
    {
        $model = new FlowInputSnfg();
        $jenis_kemas = FlowInputSnfg::find()->where(['id'=>$last_id])->one()['jenis_kemas'];
        // Populate Grid Data
        $searchModel = new FlowInputSnfgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("snfg='".$snfg."' and posisi='KEMAS_2'")->orderby(['datetime_start'=>SORT_DESC]);

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");

        $this->layout = '//main-sidebar-collapse';

        //Get No Batch yg sudah diisi di form start scan
        $sql="

        select nobatch
        from flow_input_snfg
        where snfg = '".$snfg."' and id = ".$last_id;

        $nobatch_row = FlowInputSnfg::findBySql($sql)->one();
        if (empty($nobatch_row)){
            $batch = NULL;
        } else {
            $batch = $nobatch_row->nobatch;
        }

        //Get Data Netto
        $koitem_fg = explode('/',$snfg)[0];
        $komponens = ScmPlanner::find()->where(['snfg'=>$snfg])->all();
        $jumlah_komponen = count($komponens);

        if ($jumlah_komponen > 1){
            $koitem_bulk=[];
            foreach ($komponens as $komponen){
                $koitem_bulk_array[] = "'".$komponen['koitem_bulk']."'";
            }
            $koitem_bulk = implode(',',$koitem_bulk_array);

            $netto_min = Yii::$app->db->createCommand("SELECT SUM(netto_min) FROM master_data_product WHERE koitem_fg = '".$koitem_fg."' AND koitem_bulk != '#N/A' AND koitem_bulk is not null ")->queryScalar();
        }else{
            $data = Yii::$app->db->createCommand("SELECT netto_min FROM master_data_product WHERE koitem_fg = '".$koitem_fg."' AND netto_min is not null ")->queryOne();
            $netto_min = $data['netto_min'];
            if (empty($netto_min)){
                $data = Yii::$app->db->createCommand("SELECT netto_klaim FROM master_data_product WHERE koitem_fg = '".$koitem_fg."' AND uom_klaim is not null AND uom_klaim != 'ml' ")->queryOne();
                $netto_min = $data['netto_klaim'];
            }
        }

        // Check If Post Value True
        if ($model->load(Yii::$app->request->post())){

            // Jenis Proses
                $array_id = FlowInputSnfg::find()->where(['id' => $last_id])->one();
                $jenis_proses = $array_id->jenis_kemas;


            // Get Posted Value
                $jumlah_realisasi=Yii::$app->request->post('FlowInputSnfg')['jumlah_realisasi'];
                if(empty($jumlah_realisasi)){
                    $jumlah_realisasi=0;
                }

                $nobatch=Yii::$app->request->post('FlowInputSnfg')['nobatch'];
                if(empty($nobatch)){
                    $nobatch=null;
                }

                $counter=Yii::$app->request->post('FlowInputSnfg')['counter'];
                if(empty($counter)){
                    $counter=null;
                }

                // Get Staging value
                $staging=Yii::$app->request->post('FlowInputSnfg')['staging'];
                // Get Is Done Value
                $is_done=Yii::$app->request->post('FlowInputSnfg')['is_done'];

                $netto_1=Yii::$app->request->post('FlowInputSnfg')['netto_1'];
                if(empty($netto_1)){
                    $netto_1=0;
                }

                $netto_2=Yii::$app->request->post('FlowInputSnfg')['netto_2'];
                if(empty($netto_2)){
                    $netto_2=0;
                }

                $netto_3=Yii::$app->request->post('FlowInputSnfg')['netto_3'];
                if(empty($netto_3)){
                    $netto_3=0;
                }

                $netto_4=Yii::$app->request->post('FlowInputSnfg')['netto_4'];
                if(empty($netto_4)){
                    $netto_4=0;
                }

                $netto_5=Yii::$app->request->post('FlowInputSnfg')['netto_5'];
                if(empty($netto_5)){
                    $netto_5=0;
                }

            // Insert Netto & Reject
            $connection2 = Yii::$app->getDb();
            $command2 = $connection2->createCommand("

                INSERT INTO mpq_monitoring (flow_input_snfg_id,netto_1,netto_2,netto_3,netto_4,netto_5)
                SELECT
                    :flow_input_snfg_id as flow_input_snfg_id,
                    :netto_1 as netto_1,
                    :netto_2 as netto_2,
                    :netto_3 as netto_3,
                    :netto_4 as netto_4,
                    :netto_5 as netto_5
            ",
            [
                ':flow_input_snfg_id' => $last_id,
                ':netto_1' => $netto_1,
                ':netto_2' => $netto_2,
                ':netto_3' => $netto_3,
                ':netto_4' => $netto_4,
                ':netto_5' => $netto_5
            ]);

            $result = $command2->queryAll();

            // Stop Jadwal
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE flow_input_snfg
            SET datetime_stop = :current_time,
                datetime_write = :current_time,
                jumlah_realisasi = :jumlah_realisasi,
                counter = :counter,
                nobatch = :nobatch,
                staging = :staging
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i A'),
             ':id'=> $last_id,
             ':jumlah_realisasi'=>$jumlah_realisasi,
             ':counter'=>$counter,
             ':nobatch'=>$nobatch,
             ':staging'=>$staging
            ]);

            $result = $command->queryAll();

            $this->actionAutoOffSplitLine($snfg);

            // Insert Is Done Value to status_jadwal
            if(!empty($is_done)){

                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("

                INSERT INTO status_jadwal (nojadwal,status,table_name,posisi,jenis_proses)
                SELECT
                    :snfg as nojadwal,
                    1 as status,
                    'flow_input_snfg' as table_name,
                    'KEMAS_2' as posisi,
                    :jenis_proses as jenis_proses
                ON CONFLICT (nojadwal,posisi,jenis_proses)
                DO NOTHING;
                ",
                [':snfg' => $snfg, ':jenis_proses' => $jenis_proses]);

                $result = $command->queryAll();

                return $this->redirect(['rekonsiliasi-packaging/create', 'snfg'=>$snfg,'flow_input_snfg_id'=>$last_id, 'pos'=>$pos]);

            }

            // Redirect to Downtime
            return $this->redirect(['downtime/index-kemas2', 'id' => $last_id, 'pos' => $pos]);

        } else {
            return $this->render('_form-kemas2-stop', [
                'model' => $model,
                'jenis_kemas' => $jenis_kemas,
                'snfg' => $snfg,
                'last_id' => $last_id,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'batch' => $batch,
                'pos'=>$pos,
                'netto_min' => $netto_min,
            ]);
        }
    }

    public function actionAutoOffSplitLine($snfg)
    {
        $sql = "SELECT count(id) FROM flow_input_snfg WHERE snfg = :snfg AND is_split_line = 1 and datetime_stop is null";
        $line_split_active = Yii::$app->db->createCommand($sql,[':snfg'=>$snfg])->queryScalar();
        if($line_split_active <= 1){
            $sql_update = "UPDATE flow_input_snfg SET is_split_line = 0 WHERE snfg = :snfg";
            $update = Yii::$app->db->createCommand($sql_update,[':snfg'=>$snfg])->execute();
        }
    }

    /**
     * Stop Jadwal Kemas 2 Inline Function
     * @param $snfg
     * @return mixed
     */
    public function actionStopKemas2Inline($snfg,$last_id)
    {
        $model = new FlowInputSnfg();

        // Populate Grid Data
        $searchModel = new FlowInputSnfgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("snfg='".$snfg."' and posisi='KEMAS_2'");

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");

        $this->layout = '//main-sidebar-collapse';

        // Check If Post Value True
        if ($model->load(Yii::$app->request->post())){

            // Jenis Proses
                $array_id = FlowInputSnfg::find()->where(['id' => $last_id])->one();
                $jenis_proses = $array_id->jenis_kemas;

            // Get Posted Value
                $jumlah_realisasi=Yii::$app->request->post('FlowInputSnfg')['jumlah_realisasi'];
                if(empty($jumlah_realisasi)){
                    $jumlah_realisasi=null;
                }

                // $nobatch=Yii::$app->request->post('FlowInputSnfg')['nobatch'];
                // if(empty($nobatch)){
                //     $nobatch=null;
                // }

                $counter=Yii::$app->request->post('FlowInputSnfg')['counter'];
                if(empty($counter)){
                    $counter=null;
                }

                // Get Staging value
                $staging=Yii::$app->request->post('FlowInputSnfg')['staging'];
                if(empty($staging)){
                    $staging=null;
                }

                // Get Is Done Value
                $is_done=Yii::$app->request->post('FlowInputSnfg')['is_done'];


            // Insert Is Done Value to status_jadwal
            if(!empty($is_done)){

                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("

                INSERT INTO status_jadwal (nojadwal,status,table_name,posisi,jenis_proses)
                SELECT
                    :snfg as nojadwal,
                    1 as status,
                    'flow_input_snfg' as table_name,
                    'KEMAS_2' as posisi,
                    :jenis_proses as jenis_proses
                ON CONFLICT (nojadwal,posisi,jenis_proses)
                DO NOTHING;
                ",
                [':snfg' => $snfg, ':jenis_proses' => $jenis_proses]);

                $result = $command->queryAll();
            }


            // Stop Jadwal
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE flow_input_snfg
            SET datetime_stop = :current_time,
                datetime_write = :current_time,
                jumlah_realisasi = :jumlah_realisasi,
                counter = :counter,
                staging = :staging
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i A'),
             ':id'=> $last_id,
             ':jumlah_realisasi'=>$jumlah_realisasi,
             'counter'=>$counter,
             'staging'=>$staging
            ]);

            // $model->save();

            $result = $command->queryAll();

            // Redirect to Downtime
            return $this->redirect(['downtime/index-kemas2-inline', 'id' => $last_id]);
            // return $this->redirect(['create-kemas1', 'snfg_komponen' => $snfg_komponen]);
             // return $this->redirect(['check-kemas1']);
        } else {




            return $this->render('_form-kemas2-stop-inline', [
                'model' => $model,
                'snfg' => $snfg,
                'last_id' => $last_id,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,

            ]);
        }
    }

    /**
     * Stop WorkOrder
     * @param $snfg
     * @param $last_id
     * @return mixed
     */
    public function actionStopWorkOrder($snfg,$last_id,$pos)
    {
            // Stop Jadwal
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE flow_input_snfg
            SET is_done_wo = 1
            WHERE id = :id;
            ",
            [
             ':id'=> $last_id
            ]);

            // $model->save();

            $result = $command->queryAll();

            // Redirect to Downtime
            return $this->redirect(['stop-kemas2','snfg' => $snfg, 'last_id' => $last_id, 'pos' => $pos]);
    }

    public function actionGetLogInputKemas2($nojadwal)
    {
        $sql="select
                        datetime_write,
                        snfg,
                        jenis_kemas,
                        lanjutan,
                        nama_operator,
                        id
              from flow_input_snfg where snfg='".$nojadwal."'
              and posisi='KEMAS_2'";
        $pp= FlowInputSnfg::findBySql($sql)->all();
        // print_r($pprr);
        echo Json::encode($pp);

    }

    public function actionGenerateBatchKemas($snfg)
    {
        date_default_timezone_set('Asia/Jakarta');

        $year_mapping = [2014=>'A',2015=>'B',2016=>'C',2017=>'D',2018=>'E',2019=>'F',2020=>'G',
                        2021=>'H',2022=>'I',2023=>'J',2024=>'K',2025=>'L',2026=>'M',2027=>'N',
                        2028=>'O',2029=>'P',2030=>'Q',2031=>'R',2032=>'S',2033=>'T',2034=>'U',
                        2035=>'V',2036=>'W',2037=>'X',2038=>'Y',2039=>'Z'];

        $month_mapping = [1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',6=>'F',7=>'G',
                        8=>'H',9=>'I',10=>'J',11=>'K',12=>'L'];

        $serial_mapping = [0=>'0',1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',
                            6=>'F',7=>'G',8=>'H',9=>'I',10=>'J',11=>'K'];

        //Cek snfg sudah punya nobatch atau belum
        $current_nobatch = FlowInputSnfg::find()->where("snfg='".$snfg."' and nobatch is not null")->one()['nobatch'];
        if (empty($current_nobatch)){
            $jadwal_bulk_sisa = ScmPlanner::find()->where("snfg = '".$snfg."' and nobatch is not null and nobatch not in ('-','') ")->all();

            //Jika jadwal adalah jadwal bulk sisa
            if (!empty($jadwal_bulk_sisa)){
                $list_date = [];
                foreach($jadwal_bulk_sisa as $data){
                    if (strlen($data['nobatch']) <= 6){
                        $m = substr($data['nobatch'],0,1);
                        $y = substr($data['nobatch'],1,1);
                        $d = substr($data['nobatch'],2,2);
                        $s = substr($data['nobatch'],4,1);

                        $list_date[]=$y.$m.$d.$s;
                    }

                }

                if (!empty($list_date)){
                    sort($list_date);
                    $first_nobatch = $list_date[0];

                    $y = substr($first_nobatch,0,1);
                    $m = substr($first_nobatch,1,1);
                    $d = substr($first_nobatch,2,2);
                    $s = substr($first_nobatch,4,1);

                    $nobatch=$m.$y.$d.$s;

                    //Get NOMO tanpa sn -> ex:'/F0001'
                    $start_serial = array_search($s,$serial_mapping);
                    $snfg_without_serial = explode('/',$snfg)[0];
                    for ($serial=$start_serial; $serial<= 10; $serial++)
                    {
                        $nobatch = $m.$y.$d.$serial_mapping[$serial];
                        $check = Yii::$app->db->createCommand("SELECT nobatch FROM flow_input_snfg
                                                                WHERE snfg LIKE '".$snfg_without_serial."%' AND nobatch = '".$nobatch."'
                                                                ")->queryOne();
                        if (empty($check)){
                            break;
                        }
                    }


                    Print_r('Batch kemas menggunakan bulk sisa');
                }else{
                    $nobatch=null;
                }


            }else{//Jika bukan jadwal bulk sisa

                //Get nomo yang terdapat pada satu snfg
                $list_jadwal = ScmPlanner::find()->where(['snfg'=>$snfg])->all();
                $list_komponen = [];
                foreach ($list_jadwal as $jadwal){
                    $list_komponen[] = $jadwal['snfg_komponen'];
                }
                $jumlah_komponen = count($list_komponen);

                if ($jumlah_komponen == 1){ //Jika bukan item fg yang memiliki komponen
                    $pengolahan_batch = Yii::$app->db->createCommand("SELECT nobatch FROM flow_input_mo
                                                                        WHERE nomo IN (SELECT nomo FROM scm_planner WHERE snfg = '".$snfg."' and trim(nomo) NOT IN ('','-') and nomo is not null)
                                                                        AND nobatch is not null
                                                                        ORDER BY datetime_start ASC LIMIT 1
                                                                        ")->queryOne();
                    //Jika ada proses olahnya
                    if (!empty($pengolahan_batch)){
                        $nobatch = $pengolahan_batch['nobatch'];
                        $first_record = null;
                        Print_r('Batch kemas FG tanpa komponen');

                    }else{// Jika tidak ada proses olahnya
                        $nobatch = null;
                        $first_record['datetime_start'] = date('Y-m-d H:i:s');
                        Print_r('Batch kemas FG tanpa komponen, tanpa proses olah');
                    }

                }else{ //Jika FG memiliki komponen
                        $pengolahan_batch = Yii::$app->db->createCommand("SELECT nobatch FROM flow_input_mo
                                                                        WHERE nomo IN (SELECT nomo FROM scm_planner WHERE snfg = '".$snfg."' and trim(nomo) NOT IN ('','-') and nomo is not null)
                                                                        AND nobatch is not null
                                                                        ORDER BY datetime_start ASC LIMIT 1
                                                                        ")->queryOne();
                        //Jika ada proses olahnya
                        if (!empty($pengolahan_batch)){
                            $nobatch = $pengolahan_batch['nobatch'];
                            $first_record = null;
                            // Print_r('Batch kemas FG dengan komponen');

                        }else{// Jika tidak ada proses olahnya
                            $nobatch = null;
                            $first_record['datetime_start'] = date('Y-m-d H:i:s');
                            // Print_r('Batch kemas FG dengan komponen, tanpa record olah');
                        }

                }//$jumlah_komponen==1

            }//!empty($bulk_sisa)

        }else{
            $nobatch = $current_nobatch;
            Print_r('Batch kemas sudah ada sebelumnya');
        }//empty($current_nobatch)

        if (!empty($nobatch)){
            $m = substr($nobatch,0,1);
            $y = substr($nobatch,1,1);
            $d = substr($nobatch,2,2);
            $s = substr($nobatch,4,1);

            //Get Exp Date
            $year = array_search($y,$year_mapping);
            $month = array_search($m,$month_mapping);
            $snfg_without_serial = explode('/',$snfg)[0];

            // $sl_fg = Yii::$app->db->createCommand("SELECT sl_fg from master_data_product where koitem_fg = '".$snfg_without_serial."' and sl_fg is not null ")->queryOne()['sl_fg'];
            $sl_fg = Yii::$app->db->createCommand("SELECT coalesce(shelf_life_month,0) as shelf_life_month,coalesce(shelf_life_year,0) as shelf_life_year from mdm_product where kode_item = '".$snfg_without_serial."' ")->queryOne();


            $first_olah = $year.'-'.$month.'-'.$d;
            if (!empty($sl_fg)){
                $exp_date = date('Y-m-d', strtotime($first_olah. ' + '.$sl_fg['shelf_life_year'].' years + '.$sl_fg['shelf_life_month'].' months'));
            }else{
                $exp_date = null;
            }
            // return $nobatch;
            // print_r ('Year Code: '.$y);
            // print_r ('<br>');
            // print_r ('Year : '.$year);
            // print_r ('<br>');
            // print_r ('Nobatch : '.$nobatch);
            // print_r ('<br>');
            // print_r ('First olah : '.$first_olah);
            // print_r ('<br>');
            // print_r ('sl_fg : '.$sl_fg);
            // print_r ('<br>');
            // print_r ('Exp date : '.$exp_date);
        }else{
            $exp_date = null;
        }


        $data = ['nobatch'=>$nobatch,'exp_date'=>$exp_date];
        return $data;
        // print_r($data);
    }

    // public function actionStopKemas2($last_id)
    // {

    //     date_default_timezone_set('Asia/Jakarta');
    //     $current_time = date("H:i:s");

    //     $connection = Yii::$app->getDb();
    //     $command = $connection->createCommand("

    //         UPDATE flow_input_snfg
    //         SET datetime_stop = :current_time,
    //             datetime_write = :current_time
    //         WHERE id = :id;
    //         ",
    //         [':current_time' => date('Y-m-d h:i A'),':id'=> $last_id]);

    //     $result = $command->queryAll();

    //     // Redirect to Downtime
    //     return $this->redirect(['downtime/index-kemas2', 'id' => $last_id]);

    //     // Redirect to Referrer
    //     // return $this->redirect(Yii::$app->request->referrer);

    // }

    /**
     * Deletes an existing FlowInputSnfg model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */

    public function actionQueryTest($snfg)
    {
        $sql = Yii::$app->db2->createCommand(
            "SELECT * FROM ptimfg_snfg
             WHERE id in (1,2,3)"
                )->queryAll();

        print_r('<pre>');
        print_r($sql);
        print_r('</pre>');
    }
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetSisaBulk($snfg){
      $scmp = ScmPlanner::find()->where(['snfg'=>$snfg])->all();
      $qtyBulkSisa = array();
      $bulkKemas2 = array();
      $listFg = array();
      for ($i=0; $i < count($scmp); $i++) {
        // printf($scmp[$i]->snfg_komponen."<br>");
        $bulkKemas1 = FlowInputSnfgkomponen::find()->where(['snfg_komponen'=>$scmp[$i]->snfg_komponen])->one();
        if (!$bulkKemas1) {
          array_push($bulkKemas2,$scmp[$i]->nomo);
        }
      }
      for ($i=0; $i < count($bulkKemas2); $i++) {
        // printf($bulkKemas2[$i]."<br>");
        $bulk_sisa = $this->queryGetBulkSisa($bulkKemas2[$i]);
        // printf($bulk_sisa['qty']."<br>");
        if ($bulk_sisa) {
          // printf($bulk_sisa['qty']."<br>");
          if ($bulk_sisa['qty'] == null) {
            array_push($qtyBulkSisa,0);
          }else{
            array_push($qtyBulkSisa,$bulk_sisa['qty']);
          }
        }else{
          // printf("Null"."<br>");
          array_push($qtyBulkSisa,0);
        }
      }
      for ($i=0; $i < count($bulkKemas2); $i++) {
        array_push($listFg,array(
          "nomo" => $bulkKemas2[$i],
          "qty" => $qtyBulkSisa[$i],
        ));
      }
      $json = json_encode($listFg);
      // print_r($qtyBulkSisa);
      // printf($json);
      // exit();
      return base64_encode($json);
    }

    public function queryGetBulkSisa($nomo){
      $query = new Query();
      $res = $query
      ->from('log_bulk_sisa')
      ->where('nomo = :nomo',[':nomo' => $nomo])
      ->one();
      return $res;
    }

    /**
     * Finds the FlowInputSnfg model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FlowInputSnfg the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FlowInputSnfg::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
