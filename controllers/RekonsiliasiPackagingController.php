<?php

namespace app\controllers;

use Yii;
use app\models\FlowInputSnfg;
use app\models\MasterDataNetto;
use app\models\MpqMonitoring;
use app\models\RekonsiliasiPackaging;
use app\models\RekonsiliasiPackagingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\filters\AccessControl;
use app\components\AccessRule;
use yii\helpers\ArrayHelper;
use app\base\Model;
use yii\web\UploadedFile;
use kartik\mpdf\Pdf;
use yii\data\SqlDataProvider;

/**
 * RekonsiliasiPackagingController implements the CRUD actions for RekonsiliasiPackaging model.
 */
class RekonsiliasiPackagingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RekonsiliasiPackaging models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RekonsiliasiPackagingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RekonsiliasiPackaging model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RekonsiliasiPackaging model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($snfg, $flow_input_snfg_id, $pos)
    {
        $this->layout = '//main-sidebar-collapse';
        $model_mm = MpqMonitoring::find()->where(['flow_input_snfg_id'=>$flow_input_snfg_id])->one();
        $jenis_proses = FlowInputSnfg::find()->where(['id'=>$flow_input_snfg_id])->one()['jenis_kemas'];

        $command= Yii::$app->db->createCommand("SELECT
                                                      snfg,
                                                      qty_mo,
                                                      sum(qty_supply) qty_supply,
                                                      koitem_pack,
                                                      qty_component,
                                                      nama_pack,
                                                      uom_id
                                                from bom_mrp
                                                where snfg = '".$snfg."' and product_uom_id = 1 and uom_id = 1
                                                group by koitem_pack,snfg,qty_mo,qty_component,nama_pack,uom_id
                                              ");
        $results = $command->queryAll();

        if(empty($results)){
            $command= Yii::$app->db->createCommand("SELECT
                                                          snfg,
                                                          qty_mo,
                                                          sum(qty_supply) qty_supply,
                                                          koitem_pack,
                                                          qty_component,
                                                          nama_pack,
                                                          uom_id
                                                    from bom_mrp_varcos
                                                    where snfg = '".$snfg."' and product_uom_id in (1,22) and uom_id in (1,22)
                                                    group by koitem_pack,snfg,qty_mo,qty_component,nama_pack,uom_id
                                                  ");
            $results = $command->queryAll();            
        }

        //Get hasil kemas (pcs)
        $hasil_kemas = Yii::$app->db->createCommand("SELECT sum(jumlah_realisasi) FROM jenis_kemas_snfg WHERE snfg = '".$snfg."' AND jenis_kemas != 'FILLING' group by snfg")->queryScalar();
        if(empty($hasil_kemas)){
            $hasil_kemas = 0;
        }
        //Get netto_min (gr)
        $koitem_fg = explode('/',$snfg)[0];
        $netto_min = MasterDataNetto::find()->where(['koitem'=>$koitem_fg])->orderBy(['timestamp'=>SORT_DESC])->one()['netto_min'];
        if (empty($netto_min)){
            $netto_min = Yii::$app->db->createCommand("SELECT (netto_1+netto_2+netto_3+netto_4+netto_5)/5 AS netto
                                                        FROM mpq_monitoring
                                                        WHERE flow_input_snfg_id IN (
                                                            SELECT DISTINCT on (snfg) id FROM flow_input_snfg WHERE snfg = '".$snfg."')
                                                        ")->queryScalar();

        }

        //Get realisasi olah (gr)
        $realisasi_olah = Yii::$app->db->createCommand("SELECT sum(besar_batch_real) AS realisasi_olah FROM batch_per_komponen WHERE snfg_komponen in (
                                                            SELECT snfg_komponen FROM scm_planner WHERE snfg='".$snfg."') ")
                                        ->queryScalar();


        $sisa_bulk_standar = $realisasi_olah - $hasil_kemas * $netto_min / 1000;


        // print_r ($realisasi_olah);
        // exit();



        if (!empty($results)){
            $array_insert = [];
            $check = RekonsiliasiPackaging::find()->where(['snfg'=>$snfg])->all();
            if (empty($check)){
                foreach ($results as $result) {
                    // code...
                    $qty_supply = $result['qty_mo']*$result['qty_component'];
                    $qty_realisasi = $hasil_kemas*$result['qty_component'];
                    $array_insert[]= "(
                                        '".$result['snfg']."',
                                        '".$result['koitem_pack']."',
                                        '".str_replace("'","''",$result['nama_pack'])."',
                                        ".$result['qty_mo'].",
                                        ".$result['qty_component'].",
                                        ".$result['qty_supply'].",
                                        ".$hasil_kemas.",
                                        ".$qty_realisasi.",
                                        '".$result['uom_id']."')";
                }
                $data = implode(',',$array_insert);

                $insert = Yii::$app->db->createCommand("INSERT INTO rekonsiliasi_packaging (snfg,koitem_pack,nama_pack,qty_mo,qty_component,qty_supply,hasil_kemas,qty_realisasi,uom)
                                              VALUES ".$data."")->queryAll();

            }else{
                foreach ($results as $result) {
                    // code...
                    $qty_supply = $result['qty_mo']*$result['qty_component'];
                    $qty_realisasi = $hasil_kemas*$result['qty_component'];
                    $connection = Yii::$app->db->createCommand();
                    $update = $connection->update('rekonsiliasi_packaging',
                                                    [
                                                        'hasil_kemas'=>$hasil_kemas,
                                                        'qty_realisasi'=>$qty_realisasi
                                                    ],
                                                    [
                                                        'snfg'=>$result['snfg'],
                                                        'koitem_pack'=>$result['koitem_pack']
                                                    ])->execute();
                }
            }

        }

        $model = $model_mm;
        $modelsRekonsiliasiPackaging = RekonsiliasiPackaging::find()->where(['snfg'=>$snfg])->all();

        if ($model->load(Yii::$app->request->post())) {

            $oldIDs = ArrayHelper::map($modelsRekonsiliasiPackaging, 'id', 'id');
            $modelsRekonsiliasiPackaging = Model::createMultiple(RekonsiliasiPackaging::classname(), $modelsRekonsiliasiPackaging);
            Model::loadMultiple($modelsRekonsiliasiPackaging, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsRekonsiliasiPackaging, 'id', 'id')));

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsRekonsiliasiPackaging) && $valid;
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (!empty($deletedIDs)) {
                            RekonsiliasiPackaging::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($modelsRekonsiliasiPackaging as $modelRekonsiliasiPackaging) {
                            $modelRekonsiliasiPackaging->flow_input_snfg_id = $model->flow_input_snfg_id;
                            if (! ($flag = $modelRekonsiliasiPackaging->save(false))) {
                                $transaction->rollBack();
                                // return $this->redirect(['rekonsiliasi-packaging/create-rincian', 'snfg'=>$snfg,'flow_input_snfg_id' => $flow_input_snfg_id]);
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['downtime/index-kemas2', 'id' => $flow_input_snfg_id, 'pos' => $pos]);
                    }
                }catch(Exception $e) {
                $transaction->rollBack();
                // return $this->redirect(['rekonsiliasi-packaging/create-rincian', 'snfg'=>$snfg,'flow_input_snfg_id' => $flow_input_snfg_id]);
                }
            }
        }

        /*$searchModel = new RekonsiliasiPackagingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['snfg'=>$snfg]);

        if(Yii::$app->request->post('hasEditable'))
        {
            $operatorId = Yii::$app->request->post('editableKey');
            $operator = RekonsiliasiPackaging::findOne($operatorId);

            $out = Json::encode(['output'=>'','message'=>'']);
            $post = [];
            $posted = current($_POST['RekonsiliasiPackaging']);
            $post['RekonsiliasiPackaging'] = $posted;
            if($operator->load($post))
            {
                $operator->save();

            }
            echo $out;
            return;
        }

        print_r ($result);
        if ($model->load(Yii::$app->request->post()) && $model->save() ) {

          return $this->redirect(['downtime/index-kemas2', 'id' => $flow_input_snfg_id]);
        }*/
        else {
            return $this->render('create-rincian', [
                'sisa_bulk_standar' => $sisa_bulk_standar,
                'snfg' => $snfg,
                'model' => $model,
                'modelsRekonsiliasiPackaging' => (empty($modelsRekonsiliasiPackaging)) ? [new RekonsiliasiPackaging] : $modelsRekonsiliasiPackaging
                // 'dataProvider' => $dataProvider,
                // 'searchModel' => $searchModel,
            ]);
        }
    }

    /**
     * Updates an existing RekonsiliasiPackaging model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RekonsiliasiPackaging model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RekonsiliasiPackaging model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RekonsiliasiPackaging the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RekonsiliasiPackaging::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
