<?php

namespace app\controllers;

use Yii;
use app\models\ListKendalaScf;
use app\models\ListKendalaScfSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ListKendalaScfController implements the CRUD actions for ListKendalaScf model.
 */
class ListKendalaScfController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ListKendalaScf models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ListKendalaScfSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ListKendalaScf model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ListKendalaScf model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ListKendalaScf();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ListKendalaScf model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    public function actionGetLevel2($level_1)
    {
        $sql = "
                SELECT distinct on (level_2)
                  level_2
                FROM list_kendala_scf
                WHERE level_1 = :level_1
                ORDER by level_2,id asc
                ";

        $lists = ListKendalaScf::findBySql($sql,[':level_1'=>$level_1])->all();

        echo "<option value='Select Kendala'>Select Kendala</options>";
        foreach($lists as $list){
            echo "<option value='".$list->level_2."'>".$list->level_2."</options>";
        }

    }

    public function actionGetLevel3($level_2)
    {
        $sql = "
                SELECT distinct on (level_3)
                  level_3
                FROM list_kendala_scf
                WHERE level_2 = :level_2
                ORDER by level_3,id asc
                ";

        $lists = ListKendalaScf::findBySql($sql,[':level_2'=>$level_2])->all();

        echo "<option value='Select Kendala'>Select Kendala</options>";
        foreach($lists as $list){
            echo "<option value='".$list->level_3."'>".$list->level_3."</options>";
        }

    }

    /**
     * Deletes an existing ListKendalaScf model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ListKendalaScf model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ListKendalaScf the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ListKendalaScf::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
