<?php

namespace app\controllers;

use Yii;
use app\models\Penimbangan;
use app\models\Dummy;
use app\models\PenimbanganSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use app\models\User;
use yii\filters\AccessControl;
use app\models\Kendala;
use app\models\Model;
use yii\helpers\ArrayHelper;

/**
 * PenimbanganController implements the CRUD actions for Penimbangan model.
 */
class PenimbanganController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create', 'update', 'delete','index','view'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_PENIMBANGAN
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN
                        ],
                    ],
                ],
            ],

        ];
    }

    /**
     * Lists all Penimbangan models.
     * @return mixed
     */
    // public function actionIndex()
    // {
    //     $searchModel = new PenimbanganSearch();
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    //     return $this->render('index', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }

    /**
     * Displays a single Penimbangan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Penimbangan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {

    //     $model = new Penimbangan();
    //     $modelsKendala = [new Kendala];

    //     if ($model->load(Yii::$app->request->post()) && $model->save() ) 
    //     {

    //         //

    //         $nomo=Yii::$app->request->post('Penimbangan')['nomo'];

    //         $connection = Yii::$app->getDb();
    //         date_default_timezone_set('Asia/Jakarta');
    //         $command = $connection->createCommand("
    //             INSERT INTO pit_std_leadtime_jadwal (nojadwal,proses,jenis_proses,std_type,value,time)
    //             SELECT  :nomo as nojadwal,
    //                     'PENIMBANGAN' as proses,
    //                     'timbang' as jenis_proses,
    //                     ksl.std_type,
    //                     ksl.value,
    //                     :last_update as time
    //             FROM (select distinct koitem_fg,koitem_bulk from scm_planner where nomo=:nomo) a 
    //             INNER JOIN (select * from konfigurasi_std_leadtime where jenis='timbang') ksl ON ksl.koitem = a.koitem_fg and a.koitem_bulk = ksl.koitem_bulk
    //             ON CONFLICT (nojadwal,jenis_proses,std_type)
    //             DO NOTHING;
    //             ", 
    //             [':last_update' => date('Y-m-d h:i:s A'),':nomo'=> $nomo]);
    //         $result = $command->queryAll();



    //         //

            
    //         $modelsKendala = Model::createMultiple(Kendala::classname());
    //         Model::loadMultiple($modelsKendala, Yii::$app->request->post());

    //         // validate all models
    //         $valid = $model->validate();
    //         $valid = Model::validateMultiple($modelsKendala) && $valid;

    //         if ($valid) {
    //             $transaction = \Yii::$app->db->beginTransaction();
    //             try {
    //                 if ($flag = $model->save(false)) {
    //                     foreach ($modelsKendala as $modelKendala) 
    //                     {
    //                         $modelKendala->penimbangan_id = $model->id;
    //                         if (! ($flag = $modelKendala->save(false))) {
    //                             $transaction->rollBack();
    //                             break;
    //                         }
    //                     }
    //                 }
    //                 if ($flag) {
    //                     $transaction->commit();
    //                     return $this->redirect(['create']);
    //                 }
    //             } catch (Exception $e) {
    //                 $transaction->rollBack();
    //             }
    //         }
    //     } else 

    //     {
    //         return $this->render('create', [
    //             'model' => $model,
    //             'modelsKendala' => (empty($modelsKendala)) ? [new Kendala] : $modelsKendala
    //         ]);
    //     }

    //     // $model = new Penimbangan();

    //     // if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //     //     //return $this->redirect(['view', 'id' => $model->id]);
    //     //     return $this->redirect(['create']);
    //     // } else {
    //     //     return $this->render('create', [
    //     //         'model' => $model,
    //     //     ]);
    //     // }
    // }

    public function actionCreateResolusi()
    {

        $model = new Penimbangan();
        $modelsKendala = [new Kendala];

        if ($model->load(Yii::$app->request->post()) && $model->save() ) 
        {

            //

            $nomo=Yii::$app->request->post('Penimbangan')['nomo'];

            $connection = Yii::$app->getDb();
            date_default_timezone_set('Asia/Jakarta');
            $command = $connection->createCommand("
                INSERT INTO pit_std_leadtime_jadwal (nojadwal,proses,jenis_proses,std_type,value,time)
                SELECT  :nomo as nojadwal,
                        'PENIMBANGAN' as proses,
                        'timbang' as jenis_proses,
                        ksl.std_type,
                        ksl.value,
                        :last_update as time
                FROM (select distinct koitem_fg,koitem_bulk from scm_planner where nomo=:nomo) a 
                INNER JOIN (select * from konfigurasi_std_leadtime where jenis='timbang') ksl ON ksl.koitem = a.koitem_fg and a.koitem_bulk = ksl.koitem_bulk
                ON CONFLICT (nojadwal,jenis_proses,std_type)
                DO NOTHING;
                ", 
                [':last_update' => date('Y-m-d h:i:s A'),':nomo'=> $nomo]);
            $result = $command->queryAll();



            //

            
            $modelsKendala = Model::createMultiple(Kendala::classname());
            Model::loadMultiple($modelsKendala, Yii::$app->request->post());

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsKendala) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($modelsKendala as $modelKendala) 
                        {
                            $modelKendala->penimbangan_id = $model->id;
                            if (! ($flag = $modelKendala->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['create-resolusi']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        } else 

        {
            return $this->render('create-resolusi', [
                'model' => $model,
                'modelsKendala' => (empty($modelsKendala)) ? [new Kendala] : $modelsKendala
            ]);
        }

        // $model = new Penimbangan();

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     //return $this->redirect(['view', 'id' => $model->id]);
        //     return $this->redirect(['create']);
        // } else {
        //     return $this->render('create', [
        //         'model' => $model,
        //     ]);
        // }
    }

    public function actionGetPenimbangan($snfg_komponen,$jenis_penimbangan)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM 
            (
                select sp.nomo, penimbangan.snfg_komponen, penimbangan.jenis_penimbangan, penimbangan.state, penimbangan.lanjutan
                from penimbangan 
                inner join scm_planner sp on sp.snfg_komponen = penimbangan.snfg_komponen
                where state='STOP' and penimbangan.nomo is null and penimbangan.snfg_komponen is not null

                union all
                
                select p.nomo,sp.snfg_komponen, p.jenis_penimbangan, p.state, p.lanjutan 
                from penimbangan p
                inner join scm_planner sp on sp.nomo=p.nomo
                where p.state='STOP' and p.nomo is not null
            ) a
        WHERE a.snfg_komponen ='".$snfg_komponen."'
        and a.jenis_penimbangan = '".$jenis_penimbangan."'
        ";
        $penimbangan= Penimbangan::findBySql($sql)->one();
        echo Json::encode($penimbangan);

    }

    // Change December 4 2017
    public function actionCheckLine($nama_line)
    {
        $sql="
        SELECT *
        FROM line_monitoring_timbang
        WHERE nama_line ='".$nama_line."'
        ";
        $penimbangan= Penimbangan::findBySql($sql)->one();
        echo Json::encode($penimbangan);

    }

    public function actionGetPenimbanganmo($nomo,$jenis_penimbangan)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM 
            (
                select sp.nomo, penimbangan.snfg_komponen, penimbangan.jenis_penimbangan, penimbangan.state, penimbangan.lanjutan
                from penimbangan 
                inner join scm_planner sp on sp.snfg_komponen = penimbangan.snfg_komponen
                where state='STOP' and penimbangan.nomo is null and penimbangan.snfg_komponen is not null

                union all
                
                select p.nomo,sp.snfg_komponen, p.jenis_penimbangan, p.state, p.lanjutan 
                from penimbangan p
                inner join scm_planner sp on sp.nomo=p.nomo
                where p.state='STOP' and p.nomo is not null
            ) a
        WHERE a.nomo ='".$nomo."'
        and a.jenis_penimbangan = '".$jenis_penimbangan."'
        ";
        $penimbangan= Penimbangan::findBySql($sql)->one();
        echo Json::encode($penimbangan);

    }

    public function actionLanjutanIstPenimbangan($snfg_komponen,$jenis_penimbangan,$lanjutan)
    {
        $sql="
                SELECT coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist
                FROM 
                    (
                        select sp.nomo, penimbangan.snfg_komponen, penimbangan.jenis_penimbangan, penimbangan.state, penimbangan.lanjutan,penimbangan.lanjutan_ist
                        from penimbangan 
                        inner join scm_planner sp on sp.snfg_komponen = penimbangan.snfg_komponen
                        where state='ISTIRAHAT STOP' and penimbangan.nomo is null and penimbangan.snfg_komponen is not null

                        union all
                        
                        select p.nomo,sp.snfg_komponen, p.jenis_penimbangan, p.state, p.lanjutan, p.lanjutan_ist 
                        from penimbangan p
                        inner join scm_planner sp on sp.nomo=p.nomo
                        where p.state='ISTIRAHAT STOP' and p.nomo is not null
                    ) a
                    WHERE a.snfg_komponen ='".$snfg_komponen."'
                    and a.jenis_penimbangan = '".$jenis_penimbangan."'
                    and a.lanjutan = '".$lanjutan."'
        ";
        $penimbangan= Penimbangan::findBySql($sql)->one();
        echo Json::encode($penimbangan);
    }

    public function actionLanjutanIstPenimbanganmo($nomo,$jenis_penimbangan,$lanjutan)
    {
        $sql="
                SELECT coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist
                FROM 
                    (
                        select sp.nomo, penimbangan.snfg_komponen, penimbangan.jenis_penimbangan, penimbangan.state, penimbangan.lanjutan,penimbangan.lanjutan_ist
                        from penimbangan 
                        inner join scm_planner sp on sp.snfg_komponen = penimbangan.snfg_komponen
                        where state='ISTIRAHAT STOP' and penimbangan.nomo is null and penimbangan.snfg_komponen is not null

                        union all
                        
                        select p.nomo,sp.snfg_komponen, p.jenis_penimbangan, p.state, p.lanjutan, p.lanjutan_ist 
                        from penimbangan p
                        inner join scm_planner sp on sp.nomo=p.nomo
                        where p.state='ISTIRAHAT STOP' and p.nomo is not null
                    ) a
                WHERE a.nomo ='".$nomo."'
                and a.jenis_penimbangan = '".$jenis_penimbangan."'
                and a.lanjutan = '".$lanjutan."'
                ";
        $penimbangan= Penimbangan::findBySql($sql)->one();
        echo Json::encode($penimbangan);
    }

    /**
     * Updates an existing Penimbangan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelsKendala = $model->kendalas;

        if ($model->load(Yii::$app->request->post()) && $model->save()) 
        {
            $oldIDs = ArrayHelper::map($modelsKendala, 'id', 'id');

            $modelsKendala = Model::createMultiple(Kendala::classname(), $modelsKendala);

            Model::loadMultiple($modelsKendala, Yii::$app->request->post());

            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsKendala, 'id', 'id')));

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsKendala) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {

                        if (! empty($deletedIDs)) {
                            Kendala::deleteAll(['id' => $deletedIDs]);
                        }

                        foreach ($modelsKendala as $modelKendala) {
                            $modelKendala->penimbangan_id = $model->id;
                            if (! ($flag = $modelKendala->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    } else {
                        $transaction->rollBack();
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }

            //return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelsKendala' => (empty($modelsKendala)) ? [new Kendala] : $modelsKendala
            ]);
        }

        // $model = $this->findModel($id);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // } else {
        //     return $this->render('update', [
        //         'model' => $model,
        //     ]);
        // }
    }

    /**
     * Deletes an existing Penimbangan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionGetLogInput($nojadwal)
    {
        $sql="select    
                        waktu,
                        nomo as nojadwal,
                        state,
                        jenis_penimbangan as jenis_proses,
                        lanjutan,
                        is_done,
                        id 
              from penimbangan where nomo='".$nojadwal."'";
        $pp= Dummy::findBySql($sql)->all();
        //print_r($pprr);
        echo Json::encode($pp);

    }

    public function actionGetLastResolusi($nojadwal)
    {
        $sql="select    
                        waktu,
                        nomo as nojadwal,
                        state,
                        jenis_penimbangan as jenis_proses,
                        lanjutan,
                        is_done,
                        id 
              from penimbangan where nomo='".$nojadwal."'
              ORDER BY waktu desc 
              LIMIT 1
              ";
        $pp= Dummy::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($pp);
    }

    /**
     * Finds the Penimbangan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Penimbangan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Penimbangan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
