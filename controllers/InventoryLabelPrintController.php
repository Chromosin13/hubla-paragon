<?php

namespace app\controllers;

use Yii;
use app\models\WeigherFgMapDevice;
use app\models\InventoryLabelPrint;
use app\models\InventoryLabelPrintSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InventoryLabelPrintController implements the CRUD actions for InventoryLabelPrint model.
 */
class InventoryLabelPrintController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InventoryLabelPrint models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InventoryLabelPrintSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexPrint()
    {
        $model = new InventoryLabelPrint();

        if ($model->load(Yii::$app->request->post()) ) {
            $device_code_arr = explode(',',$model->device_code);
            // print_r($device_code_arr);
            $client_ip = $_SERVER['REMOTE_ADDR'];
            $client_id = str_replace(".","",$client_ip);
            $board = WeigherFgMapDevice::find()->where(['frontend_ip'=>$client_ip])->one();
            if (!empty($board)){
              $sbc_user = $board->sbc_user;
              $sbc_ip = $board->sbc_ip;
            }else{
              Yii::$app->session->setFlash('danger','IP Device anda belum terdaftar!!');
              return $this->redirect(['index-print']);
            }

            foreach($device_code_arr as $code){

                  $myfile = fopen("labelinventory_".$client_id.".zpl", "w");

                      $txt =
                            "^XA

                            ^LH150,0
                            ^FX Title
                            ^FO0,10
                            ^A0N,25,25
                            ^FB320,1,0,C
                            ^FDASSET IOT^FS  

                            ^FX QR Code 1 Left

                            ^FO85,30
                            ^FB300,1,0,C
                            ^BQN,2,7^FDMA,".$code."^FS

                            ^FX Kode Device (Top)
                            ^FO0,205
                            ^AAN,30
                            ^FB320,1,0,C
                            ^FD".$code."^FS

                            ^XZ";


                      fwrite($myfile, $txt);
                      fclose($myfile);


                      // echo file_get_contents( "labelsnfg.prn" );

                  // echo 'scp /var/www/html/flowreport_training5/web/labelsnfg_'.$client_id.'.zpl ptiadmin@'.$client_ip.':/home/ptiadmin/labelsnfg_'.$client_id.'.zpl';


                  // Script to copy unique label to remote address
                  exec('scp /var/www/html/flowreport/web/labelinventory_'.$client_id.'.zpl '.$sbc_user.'@'.$sbc_ip.':/home/'.$sbc_user.'/labelinventory_'.$client_id.'.zpl 2>&1', $output);
                  // exec('whoami 2>&1',$output);

                  // Execute Printing remotely
                  exec("ssh ".$sbc_user."@".$sbc_ip." 'lpr -P Zebra_Technologies_ZTC_GT800_ -o raw /home/".$sbc_user."/labelinventory_".$client_id.".zpl'");

                  // print_r($output);
                  // echo 'scp /var/www/html/flowreport/web/labelsnfg_'.$client_id.'.zpl pi@'.$client_ip.':/home/pi/labelsnfg_'.$client_id.'.zpl';

                  return $this->redirect(['index-print']);
              }
        } else {
            return $this->render('index-print', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays a single InventoryLabelPrint model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new InventoryLabelPrint model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new InventoryLabelPrint();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing InventoryLabelPrint model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing InventoryLabelPrint model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the InventoryLabelPrint model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InventoryLabelPrint the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InventoryLabelPrint::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
