<?php

namespace app\controllers;

use Yii;
use app\models\AchievementLeadtimeKemas2;
use app\models\AchievementLeadtimeKemas2Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AchievementLeadtimeKemas2Controller implements the CRUD actions for AchievementLeadtimeKemas2 model.
 */
class AchievementLeadtimeKemas2Controller extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AchievementLeadtimeKemas2 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AchievementLeadtimeKemas2Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSemisolid()
    {
        $searchModel = new AchievementLeadtimeKemas2Search();
        $searchModel->sediaan='S';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSemisolidPressfillingpacking()
    {
        $searchModel = new AchievementLeadtimeKemas2Search();
        $searchModel->sediaan='S';
        $searchModel->jenis_kemas="PRESS+FILLING+PACKING";
        $info = 'Semisolid PRESS+FILLING+PACKING';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionSemisolidFillingpackinginline()
    {
        $searchModel = new AchievementLeadtimeKemas2Search();
        $searchModel->sediaan='S';
        $searchModel->jenis_kemas='FILLING-PACKING_INLINE';
        $info = 'Semisolid FILLING-PACKING_INLINE';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionSemisolidFlamepackinginline()
    {
        $searchModel = new AchievementLeadtimeKemas2Search();
        $searchModel->sediaan='S';
        $searchModel->jenis_kemas='FLAME-PACKING_INLINE';
        $info = 'Semisolid FLAME-PACKING_INLINE';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionSemisolidPacking()
    {
        $searchModel = new AchievementLeadtimeKemas2Search();
        $searchModel->sediaan='S';
        $searchModel->jenis_kemas='PACKING';
        $info = 'Semisolid PACKING';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }


    public function actionPowder()
    {
        $searchModel = new AchievementLeadtimeKemas2Search();
        $searchModel->sediaan='P';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPowderPressfillingpacking()
    {
        $searchModel = new AchievementLeadtimeKemas2Search();
        $searchModel->sediaan='P';
        $searchModel->jenis_kemas="PRESS+FILLING+PACKING";
        $info = 'Powder PRESS+FILLING+PACKING';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionPowderFillingpackinginline()
    {
        $searchModel = new AchievementLeadtimeKemas2Search();
        $searchModel->sediaan='P';
        $searchModel->jenis_kemas='FILLING-PACKING_INLINE';
        $info = 'Powder FILLING-PACKING_INLINE';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionPowderFlamepackinginline()
    {
        $searchModel = new AchievementLeadtimeKemas2Search();
        $searchModel->sediaan='P';
        $searchModel->jenis_kemas='FLAME-PACKING_INLINE';
        $info = 'Powder FLAME-PACKING_INLINE';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionPowderPacking()
    {
        $searchModel = new AchievementLeadtimeKemas2Search();
        $searchModel->sediaan='P';
        $searchModel->jenis_kemas='PACKING';
        $info = 'Powder PACKING';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionLiquid()
    {
        $searchModel = new AchievementLeadtimeKemas2Search();
        $searchModel->sediaan='L';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLiquidPressfillingpacking()
    {
        $searchModel = new AchievementLeadtimeKemas2Search();
        $searchModel->sediaan='L';
        $searchModel->jenis_kemas="PRESS+FILLING+PACKING";
        $info = 'Liquid PRESS+FILLING+PACKING';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionLiquidFillingpackinginline()
    {
        $searchModel = new AchievementLeadtimeKemas2Search();
        $searchModel->sediaan='L';
        $searchModel->jenis_kemas='FILLING-PACKING_INLINE';
        $info = 'Liquid FILLING-PACKING_INLINE';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionLiquidFlamepackinginline()
    {
        $searchModel = new AchievementLeadtimeKemas2Search();
        $searchModel->sediaan='L';
        $searchModel->jenis_kemas='FLAME-PACKING_INLINE';
        $info = 'Powder FLAME-PACKING_INLINE';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionLiquidPacking()
    {
        $searchModel = new AchievementLeadtimeKemas2Search();
        $searchModel->sediaan='L';
        $searchModel->jenis_kemas='PACKING';
        $info = 'Powder PACKING';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    /**
     * Displays a single AchievementLeadtimeKemas2 model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AchievementLeadtimeKemas2 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AchievementLeadtimeKemas2();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AchievementLeadtimeKemas2 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AchievementLeadtimeKemas2 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AchievementLeadtimeKemas2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AchievementLeadtimeKemas2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AchievementLeadtimeKemas2::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
