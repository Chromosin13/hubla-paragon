<?php

namespace app\controllers;

use Yii;
use app\models\QcBulkEntryKomponen;
use app\models\ScmPlanner;
use app\models\PengolahanBatch;
use app\models\QcBulkEntryKomponenSearch;
use app\models\WeigherFgMapDevice;
use app\models\DeviceMapQcbulk;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * QcBulkEntryKomponenController implements the CRUD actions for QcBulkEntryKomponen model.
 */
class QcBulkEntryKomponenController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'lead-delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all QcBulkEntryKomponen models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QcBulkEntryKomponenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPrintLabel($id)
    {


        $model = $this->findModel($id);

        $date = substr($model->timestamp, 0,10);
        $sl_storage = substr($model->savelife_storage, 0,10);

        // $connection = Yii::$app->getDb();
        // $command = $connection->createCommand("

        //         SELECT nobatch, besar_batch_real
        //         FROM pengolahan_batch
        //         WHERE snfg_komponen='".$model->snfg_komponen."' and besar_batch_real is not null
        //         LIMIT 1
        //         ");

        // $result = $command->queryAll();
        // $nobatch = $result[0]['nobatch'];
        // $besar_batch_real = $result[0]['besar_batch_real'];




        $sql="
                select distinct nama_fg,odoo_code,koitem_bulk,nama_bulk
                from scm_planner
                where snfg_komponen='".$model->snfg_komponen."'
                limit 1
             ";
        $arr = ScmPlanner::findBySql($sql)->one();
        $nama_fg = $arr->nama_fg;
        $odoo_code = $arr->odoo_code;
        $koitem_bulk = $arr->koitem_bulk;
        $nama_bulk = $arr->nama_bulk;

        $jumlah_storage = (new yii\db\Query())
                        ->select(['no_storage',])
                        ->from('qc_bulk_entry_komponen')
                        ->where(['nomo'=>$model->nomo])
                        ->distinct()
                        ->count();


            $txt3 = "^FX Begin setup
              ^XA
              ~TA120
              ~JSN
              ^LT0
              ^MNW
              ^MTT
              ^PON
              ^PMN
              ^LH0,0
              ^JMA
              ^PR4,4
              ~SD25
              ^JUS
              ^LRN
              ^CI28
              ^MD0

              ^XZ
              ^FX End setup

              ^FX Begin label format
              ^XA
              ^MMT
              ^LL0264
              ^PW583
              ^LS0
              ^CI28

              ^FX Line Vertical
              ^FO310,20
              ^GB0,330,3^FS

              ^FX Line Vertical
              ^FO420,200
              ^GB0,115,3^FS

              ^FX Line Horizontal
              ^FO20,95
              ^GB290,0,3^FS

              ^FX Line Horizontal
              ^FO20,200
              ^GB540,0,3^FS

              ^FX Line Horizontal
              ^FO20,270
              ^GB540,0,3^FS

              ^FX Line Horizontal
              ^FO310,314
              ^GB250,0,3^FS

              ^FX Nama Status
              ^FO40,30
              ^ARN,16
              ^FX Field block 507 dots wide, 2 lines max
              ^FB260,2,,C,
              ^FD".$model->status."^FS

              ^FX Nama Bulk Label
              ^FO30,105
              ^ADN,5,5
              ^FDNama Bulk^FS

              ^FX Nama Bulk Value
              ^FO18,125
              ^A0N,20,20
              ^FX Field block 507 dots wide, 3 lines max
              ^FB290,4,,C,
              ^FD".$nama_bulk."^FS

              ^FX QR kode BB
              ^FO380,20,0
              ^BQN,0,4,H,7
              ^FDMA,".$koitem_bulk."@".$model->nobatch."^FS

              ^FX Kode Bulk Human readable
              ^FO320,165
              ^A0N,20,20
              ^FX Field block 507 dots wide, 3 lines max
              ^FB240,3,,C,
              ^FD".$koitem_bulk."^FS

              ^FX SNFG Komponen Label
              ^FO20,210
              ^ADN,5,5
              ^FD SNFG Komponen^FS

              ^FX SNFG Komponen Value
              ^FO20,230
              ^A0N,20,20
              ^FX Field block 507 dots wide, 2 lines max
              ^FB290,2,,C,
              ^FD".$model->snfg_komponen."^FS

              ^FX No.Batch Label
              ^FO320,210
              ^ADN,5,5
              ^FDNo.Batch^FS

              ^FX No.Batch value
              ^FO320,233
              ^A0N,20,20
              ^FX Field block 507 dots wide, 1 lines max
              ^FB100,1,,C,
              ^FD".$model->nobatch."^FS

              ^FX Inspector Label
              ^FO430,210
              ^ADN,5,5
              ^FDQC Bulk^FS

              ^FX Inspector Value
              ^FO425,233
              ^A0N,20,20
              ^FX Field block 507 dots wide, 1 lines max
              ^FB115,2,,C,
              ^FD".$model->nama_inspektor."^FS

              ^FX Keterangan Label
              ^FO30,280
              ^ADN,5,5
              ^FDKeterangan^FS

              ^FX Keterangan Value
              ^FO40,300
              ^A0N,20,20
              ^FX Field block 507 dots wide, 2 lines max
              ^FB260,2,,L,
              ^FD".$model->keterangan."^FS

              ^FX No.Storage Label
              ^FO320,278
              ^ABN,5,5
              ^FDNo.Storage^FS

              ^FX No.Storage Value
              ^FO320,295
              ^A0N,20,20
              ^FX Field block 507 dots wide, 1 lines max
              ^FB90,1,,C,
              ^FD".$model->no_storage."/".$jumlah_storage."^FS

             ^FX Tanggal Check Label
              ^FO430,277
              ^ABN,5,5
              ^FDTgl.Check^FS

              ^FX Tanggal Check Value
              ^FO425,294
              ^A0N,20,20
              ^FX Field block 507 dots wide, 1 lines max
              ^FB130,1,,C,
              ^FD".$date."^FS

              ^FX Exp Date Label
              ^FO305,323
              ^ADN,5,5
              ^FD Exp. ^FS

              ^FX Exp Date Value
              ^FO370,325
              ^A0N,25,25
              ^FX Field block 507 dots wide, 1 lines max
              ^FB200,1,,L,
              ^FD".$sl_storage."^FS

              ^FX Footer value
              ^FO20,345
              ^A0N,5,5
              ^FX Field block 507 dots wide, 1 lines max
              ^FB100,1,,L,
              ^FDQC BULK-MES^FS

              ^FX Print quantity
              ^PQ1,0,1,Y
              ^FX End label format
              ^XZ";

            $curl = curl_init();
            // adjust print density (8dpmm), label width (4 inches), label height (6 inches), and label index (0) as necessary
            curl_setopt($curl, CURLOPT_URL, "http://api.labelary.com/v1/printers/8dpmm/labels/3x2/0/");
            curl_setopt($curl, CURLOPT_POST, TRUE);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $txt3);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Accept: application/pdf")); // omit this line to get PNG images back
            $result = curl_exec($curl);

            if (curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200) {
                $file = fopen("label_qcbulk.pdf", "w"); // change file name for PNG images
                fwrite($file, $result);
                fclose($file);

            } else {
                print_r("Error: $result");
            }

            curl_close($curl);


            $pdf = '/var/www/html/flowreport/web/label_qcbulk.pdf';
        return (Yii::$app->response->sendFile($pdf));
        print_r($tomorrow);
        echo date(DATE_ATOM, mktime(0, 0, 0, 13, 13, 2000)); //mktime({hour},{minute},{second},{month},{date},{year})
        // print_r($umur_storage);
    }


    public function actionPrintZebraLocal($id)
    {


        $model = $this->findModel($id);

        $sql="
                select distinct nama_fg,odoo_code
                from scm_planner
                where snfg_komponen='".$model->snfg_komponen."'
                limit 1
             ";
        $arr = ScmPlanner::findBySql($sql)->one();
        $nama_fg = $arr->nama_fg;
        $odoo_code = $arr->odoo_code;


        $sql="
                select besar_batch_real,nobatch
                from batch_per_komponen
                where snfg_komponen='".$model->snfg_komponen."'
                limit 1
             ";
        $ar = QcBulkEntryKomponen::findBySql($sql)->one();
        $nobatch = $ar->nobatch;
        $besar_batch_real = $ar->besar_batch_real;

        $client_ip = $_SERVER['REMOTE_ADDR'];
        $client_id = str_replace(".","",$client_ip);

        // print_r($client_ip);


        // $myfile = fopen("label_qcbulk_".$client_id.".zpl", "w");

        $myfile = fopen("label_qcbulk.zpl", "w");



        $txt3 = "
            ^FX Begin setup
                    ^XA
                    ~TA120
                    ~JSN
                    ^LT0
                    ^MNW
                    ^MTT
                    ^PON
                    ^PMN
                    ^LH0,0
                    ^JMA
                    ^PR4,4
                    ~SD25
                    ^JUS
                    ^LRN
                    ^CI0
                    ^MD0
                    ^CI13,196,36
                    ^XZ
                    ^FX End setup

                    ^FX Begin label format
                    ^XA
                    ^MMT
                    ^LL0264
                    ^PW583
                    ^LS0

                    ^FX Line Vertical Right
                    ^FO420,30
                    ^GB0,220,3^FS

                    ^FX Line Vertical 2nd From Right
                    ^FO301,130
                    ^GB0,60,2^FS

                    ^FX Line Vertical 2nd From Right 2nd Row
                    ^FO301,190
                    ^GB0,60,2^FS

                    ^FX Line Vertical Last Row First From Right
                    ^FO301,240
                    ^GB0,120,2^FS

                    ^FX Line Vertical Last Row First From Right
                    ^FO165,250
                    ^GB0,120,2^FS

                    ^FX Line Horizontal
                    ^FO40,130
                    ^GB380,0,2^FS


                    ^FX Line Horizontal Lower Right
                    ^FO301,302
                    ^GB400,0,2^FS

                    ^FX Line Horizontal
                    ^FO40,190
                    ^GB523,0,2^FS

                    ^FX Line Horizontal
                    ^FO40,250
                    ^GB523,0,2^FS

                    ^FX Item Name Title
                    ^FO40,40
                    ^ADN5,5
                    ^FDItem Name^FS

                    ^FX Nama BB
                    ^FO40,65
                    ^ARN,16
                    ^FX Field block 507 dots wide, 2 lines max
                    ^FB370,2,,
                    ^FD".$nama_fg."^FS

                    ^FX QR kode BB
                    ^FO440,30,0
                    ^BQN,0,4,H,7
                    ^FDMA,".$model->snfg_komponen."@".$model->no_storage."^FS

                    ^FX Kode BB Human readable
                    ^FO420,165
                    ^A0N10,5
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB150,1,,C,
                    ^FD".$model->snfg_komponen."@".$model->no_storage."^FS

                    ^FX SNFG Komponen Label
                    ^FO40,135
                    ^ADN5,5
                    ^FDSNFG Komponen^FS

                    ^FX SNFG Komponen Value
                    ^FO0,160
                    ^A0N10,20
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB330,1,,C,
                    ^FD".$model->snfg_komponen."^FS

                    ^FX No Urut Label
                    ^FO310,135
                    ^ADN5,5
                    ^FDNo Urut^FS

                    ^FX No Urut Value
                    ^FO200,160
                    ^A0N20,22
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB330,1,,C,
                    ^FD".$model->no_storage."^FS
                    Operator

                    ^FX No.MO Label
                    ^FO40,195
                    ^ADN5,5
                    ^FDNo.MO^FS

                    ^FX No.MO Value
                    ^FO40,220
                    ^A0N10,20
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB270,1,,C,
                    ^FD".$model->nomo."^FS

                    ^FX Operator Label
                    ^FO310,200
                    ^A0N10,15
                    ^FDOperator^FS

                    ^FX Operator Value
                    ^FO230,225
                    ^A0N10,20
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB270,1,,C,
                    ^FD".$model->nama_inspektor."^FS

                    ^FXTimestamp Label
                    ^FO435,200
                    ^A0N10,15
                    ^FDTimestamp^FS

                    ^FXTimestamp Value
                    ^FO360,225
                    ^A0N10,15
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB270,1,,C,
                    ^FD".$model->timestamp."^FS


                    ^FX No.Batch Label
                    ^FO310,255
                    ^ADN5,5
                    ^FDNo.Batch^FS

                    ^FX Odoo Code Label
                    ^FO310,310
                    ^ADN5,5
                    ^FDOdoo Code^FS

                    ^FX Quantity Label
                    ^FO40,255
                    ^ADN5,5
                    ^FDQuantity^FS

                    ^FX Quantity human readable
                    ^FO30,290
                    ^A0N,20,25
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB100,1,,C,
                    ^FD".$besar_batch_real."^FS

                    ^FX Status Label
                    ^FO180,255
                    ^ADN5,5
                    ^FDStatus^FS

                    ^FX Status value
                    ^FO140,290
                    ^A0N,25,25
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB200,1,,C,
                    ^FD".$model->status."^FS

                    ^FX No.Batch value
                    ^FO330,278
                    ^AQN,10,10
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB200,1,,C,
                    ^FD".$nobatch."^FS

                    ^FX Odoo Code value
                    ^FO330,333
                    ^AQN,10,10
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB200,1,,C,
                    ^FD".$odoo_code."^FS

                    ^FX Footer value
                    ^FO420,345
                    ^A0N,5,5
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB200,1,,C,
                    ^FDQC BULK-MES^FS

                    ^FX UOM Quantity human readable
                    ^FO80,290
                    ^AQN,10,10
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB100,1,,C,
                    ^FDkg^FS

                    ^FX Print quantity
                    ^PQ1,0,1,Y
                    ^FX End label format
                    ^XZ
            ";


        fwrite($myfile, $txt3);
        fclose($myfile);


        exec("lpr -P Zebra_Technologies_ZTC_GT800_ -o raw /var/www/html/flowreport/web/label_qcbulk.zpl");

        // echo file_get_contents( "labelsnfg.prn" );

        // echo 'scp /var/www/html/flowreport_training5/web/labelsnfg_'.$client_id.'.zpl ptiadmin@'.$client_ip.':/home/ptiadmin/labelsnfg_'.$client_id.'.zpl';


        // In order to run the script below on the server
        // - ssh-keygen (skip this if it already exists)
        // - Enter2..  (skip this if it already exists)
        // - ssh-copy-id -i ~/.ssh/id_rsa.pub [user]@[remote ip host]
        // - Done!

        // exec("cp /var/www/html/flowreport/web/label_qcbulk_".$client_id.".zpl /home/pi/label_qcbulk_'.$client_id.'.zpl");
        // exec("lpr -P Zebra_Technologies_ZTC_GT800_ -o raw /home/pi/label_qcbulk_".$client_id.".zpl");



        // // Script to copy unique label to remote address
        // exec('scp /var/www/html/flowreport/web/label_qcbulk_'.$client_id.'.zpl pi@'.$client_ip.':/home/pi/label_qcbulk_'.$client_id.'.zpl 2>&1', $output);

        // // Execute Printing remotely
        // exec("ssh pi@".$client_ip." 'lpr -P Zebra_Technologies_ZTC_GT800_ -o raw /home/pi/label_qcbulk_".$client_id.".zpl'");


        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPrintZebra($id)
    {

        $model = $this->findModel($id);

        $date = substr($model->timestamp, 0,10);
        $sl_storage = substr($model->savelife_storage, 0,10);

        // $connection = Yii::$app->getDb();
        // $command = $connection->createCommand("

        //         SELECT nobatch, besar_batch_real
        //         FROM pengolahan_batch
        //         WHERE snfg_komponen='".$model->snfg_komponen."' and besar_batch_real is not null
        //         LIMIT 1
        //         ");

        // $result = $command->queryAll();
        // $besar_batch = $result[0]['nobatch'];

        $sql="
                select distinct nama_fg,odoo_code,koitem_bulk,nama_bulk
                from scm_planner
                where snfg_komponen='".$model->snfg_komponen."'
                limit 1
             ";
        $arr = ScmPlanner::findBySql($sql)->one();
        $nama_fg = $arr->nama_fg;
        $odoo_code = $arr->odoo_code;
        $nama_bulk = $arr->nama_bulk;
        $koitem_bulk = $arr->koitem_bulk;

        $jumlah_storage = (new yii\db\Query())
                        ->select(['no_storage',])
                        ->from('qc_bulk_entry_komponen')
                        ->where(['nomo'=>$model->nomo])
                        ->distinct()
                        ->count();


        // $sql="
        //         select besar_batch_real,nobatch
        //         from batch_per_komponen
        //         where snfg_komponen='".$model->snfg_komponen."'
        //         limit 1
        //      ";
        // $ar = QcBulkEntryKomponen::findBySql($sql)->one();
        // $nobatch = $ar->nobatch;
        // $besar_batch_real = $ar->besar_batch_real;

        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $client_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $client_ip = $_SERVER['REMOTE_ADDR'];
        }

        $sbc_ip = DeviceMapQcbulk::find()->where(['frontend_ip' => $client_ip])->one()['sbc_ip'];
        $sbc_user = DeviceMapQcbulk::find()->where(['frontend_ip' => $client_ip])->one()['sbc_user'];

        // print_r($sbc_ip);


        if ($sbc_ip){
            $client_id = str_replace(".","",$client_ip);

            $myfile = fopen("label_qcbulk_".$client_id.".zpl", "w");

            // $myfile = fopen("label_qcbulk.zpl", "w");

            $txt3 = "^FX Begin setup
              ^XA
              ~TA120
              ~JSN
              ^LT0
              ^MNW
              ^MTT
              ^PON
              ^PMN
              ^LH0,0
              ^JMA
              ^PR4,4
              ~SD25
              ^JUS
              ^LRN
              ^CI28
              ^MD0

              ^XZ
              ^FX End setup

              ^FX Begin label format
              ^XA
              ^MMT
              ^LL0264
              ^PW583
              ^LS0
              ^CI28

              ^FX Line Vertical
              ^FO310,20
              ^GB0,330,3^FS

              ^FX Line Vertical
              ^FO420,200
              ^GB0,115,3^FS

              ^FX Line Horizontal
              ^FO20,95
              ^GB290,0,3^FS

              ^FX Line Horizontal
              ^FO20,200
              ^GB540,0,3^FS

              ^FX Line Horizontal
              ^FO20,270
              ^GB540,0,3^FS

              ^FX Line Horizontal
              ^FO310,314
              ^GB250,0,3^FS

              ^FX Nama Status
              ^FO40,30
              ^ARN,16
              ^FX Field block 507 dots wide, 2 lines max
              ^FB260,2,,C,
              ^FD".$model->status."^FS

              ^FX Nama Bulk Label
              ^FO30,105
              ^ADN,5,5
              ^FDNama Bulk^FS

              ^FX Nama Bulk Value
              ^FO18,125
              ^A0N,20,20
              ^FX Field block 507 dots wide, 3 lines max
              ^FB290,4,,C,
              ^FD".$nama_bulk."^FS

              ^FX QR kode BB
              ^FO380,20,0
              ^BQN,0,4,H,7
              ^FDMA,".$koitem_bulk."@".$model->nobatch."^FS

              ^FX Kode Bulk Human readable
              ^FO320,165
              ^A0N,20,20
              ^FX Field block 507 dots wide, 3 lines max
              ^FB240,3,,C,
              ^FD".$koitem_bulk."^FS

              ^FX SNFG Komponen Label
              ^FO20,210
              ^ADN,5,5
              ^FD SNFG Komponen^FS

              ^FX SNFG Komponen Value
              ^FO20,230
              ^A0N,20,20
              ^FX Field block 507 dots wide, 2 lines max
              ^FB290,2,,C,
              ^FD".$model->snfg_komponen."^FS

              ^FX No.Batch Label
              ^FO320,210
              ^ADN,5,5
              ^FDNo.Batch^FS

              ^FX No.Batch value
              ^FO320,233
              ^A0N,20,20
              ^FX Field block 507 dots wide, 1 lines max
              ^FB100,1,,C,
              ^FD".$model->nobatch."^FS

              ^FX Inspector Label
              ^FO430,210
              ^ADN,5,5
              ^FDQC Bulk^FS

              ^FX Inspector Value
              ^FO425,233
              ^A0N,20,20
              ^FX Field block 507 dots wide, 1 lines max
              ^FB115,2,,C,
              ^FD".$model->nama_inspektor."^FS

              ^FX Keterangan Label
              ^FO30,280
              ^ADN,5,5
              ^FDKeterangan^FS

              ^FX Keterangan Value
              ^FO40,300
              ^A0N,20,20
              ^FX Field block 507 dots wide, 2 lines max
              ^FB260,2,,L,
              ^FD".$model->keterangan."^FS

              ^FX No.Storage Label
              ^FO320,278
              ^ABN,5,5
              ^FDNo.Storage^FS

              ^FX No.Storage Value
              ^FO320,295
              ^A0N,20,20
              ^FX Field block 507 dots wide, 1 lines max
              ^FB90,1,,C,
              ^FD".$model->no_storage."/".$jumlah_storage."^FS

             ^FX Tanggal Check Label
              ^FO430,277
              ^ABN,5,5
              ^FDTgl.Check^FS

              ^FX Tanggal Check Value
              ^FO425,294
              ^A0N,20,20
              ^FX Field block 507 dots wide, 1 lines max
              ^FB130,1,,C,
              ^FD".$date."^FS

              ^FX Exp Date Label
              ^FO305,323
              ^ADN,5,5
              ^FD Exp. ^FS

              ^FX Exp Date Value
              ^FO370,325
              ^A0N,25,25
              ^FX Field block 507 dots wide, 1 lines max
              ^FB200,1,,L,
              ^FD".$sl_storage."^FS

              ^FX Footer value
              ^FO20,345
              ^A0N,5,5
              ^FX Field block 507 dots wide, 1 lines max
              ^FB100,1,,L,
              ^FDQC BULK-MES^FS

              ^FX Print quantity
              ^PQ1,0,1,Y
              ^FX End label format
              ^XZ";

            fwrite($myfile, $txt3);
            fclose($myfile);

            // In order to run the script below on the server
            // - ssh-keygen (skip this if it already exists)
            // - Enter2..  (skip this if it already exists)
            // - ssh-copy-id -i ~/.ssh/id_rsa.pub [user]@[remote ip host]
            // - Done!

            // exec("cp /var/www/html/flowreport/web/label_qcbulk_".$client_id.".zpl /home/pi/label_qcbulk_'.$client_id.'.zpl");
            // exec("lpr -P Zebra_Technologies_ZTC_GT800_ -o raw /home/pi/label_qcbulk_".$client_id.".zpl");



            // Script to copy unique label to remote address
            exec('scp /var/www/html/flowreport/web/label_qcbulk_'.$client_id.'.zpl '.$sbc_user.'@'.$sbc_ip.':/home/'.$sbc_user.'/label_qcbulk_'.$client_id.'.zpl 2>&1', $output);

            // Execute Printing remotely
            exec("ssh ".$sbc_user."@".$sbc_ip." 'lpr -P Zebra_Technologies_ZTC_GT800 -o raw /home/".$sbc_user."/label_qcbulk_".$client_id.".zpl'");
            // Yii::$app->session->setFlash('success', "Device terdaftar di database");
        }else{

            Yii::$app->session->setFlash('danger', "Device tidak terdaftar di database");

        }


        return $this->redirect(Yii::$app->request->referrer);
        // print_r($nobatch);
    }

    public function actionPrintZebraMultiple()
    {
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $client_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $client_ip = $_SERVER['REMOTE_ADDR'];
        }

        $sbc_ip = DeviceMapQcbulk::find()->where(['frontend_ip' => $client_ip])->one()['sbc_ip'];
        $sbc_user = DeviceMapQcbulk::find()->where(['frontend_ip' => $client_ip])->one()['sbc_user'];


        $action=Yii::$app->request->post('action');
        $selection=Yii::$app->request->post('selection');//typecasting
        // print_r($selection);
        if (!empty($selection))
        {
            foreach($selection as $id)
            {
                $row=QcBulkEntryKomponen::findOne((int)$id);//make a typecasting
                //do your stuff
                $date = substr($row->timestamp, 0,10);
                $sl_storage = substr($row->savelife_storage, 0,10);

                $sql="
                        select distinct nama_fg,odoo_code,nama_bulk,koitem_bulk
                        from scm_planner
                        where snfg_komponen='".$row->snfg_komponen."'
                        limit 1
                     ";
                $arr = ScmPlanner::findBySql($sql)->one();
                $nama_fg = $arr->nama_fg;
                $odoo_code = $arr->odoo_code;
                $nama_bulk = $arr->nama_bulk;
                $koitem_bulk = $arr->koitem_bulk;

                $jumlah_storage = (new yii\db\Query())
                                ->select(['no_storage',])
                                ->from('qc_bulk_entry_komponen')
                                ->where(['nomo'=>$row->nomo])
                                ->distinct()
                                ->count();

                if ($sbc_ip){
                    $client_id = str_replace(".","",$client_ip);

                    $myfile = fopen("label_qcbulk_".$client_id.".zpl", "w");

                    // $myfile = fopen("label_qcbulk.zpl", "w");

                    $txt3 = "^FX Begin setup
                      ^XA
                      ~TA120
                      ~JSN
                      ^LT0
                      ^MNW
                      ^MTT
                      ^PON
                      ^PMN
                      ^LH0,0
                      ^JMA
                      ^PR4,4
                      ~SD25
                      ^JUS
                      ^LRN
                      ^CI28
                      ^MD0

                      ^XZ
                      ^FX End setup

                      ^FX Begin label format
                      ^XA
                      ^MMT
                      ^LL0264
                      ^PW583
                      ^LS0
                      ^CI28

                      ^FX Line Vertical
                      ^FO310,20
                      ^GB0,330,3^FS

                      ^FX Line Vertical
                      ^FO420,200
                      ^GB0,115,3^FS

                      ^FX Line Horizontal
                      ^FO20,95
                      ^GB290,0,3^FS

                      ^FX Line Horizontal
                      ^FO20,200
                      ^GB540,0,3^FS

                      ^FX Line Horizontal
                      ^FO20,270
                      ^GB540,0,3^FS

                      ^FX Line Horizontal
                      ^FO310,314
                      ^GB250,0,3^FS

                      ^FX Nama Status
                      ^FO40,30
                      ^ARN,16
                      ^FX Field block 507 dots wide, 2 lines max
                      ^FB260,2,,C,
                      ^FD".$row->status."^FS

                      ^FX Nama Bulk Label
                      ^FO30,105
                      ^ADN,5,5
                      ^FDNama Bulk^FS

                      ^FX Nama Bulk Value
                      ^FO18,125
                      ^A0N,20,20
                      ^FX Field block 507 dots wide, 3 lines max
                      ^FB290,4,,C,
                      ^FD".$nama_bulk."^FS

                      ^FX QR kode BB
                      ^FO380,20,0
                      ^BQN,0,4,H,7
                      ^FDMA,".$koitem_bulk."@".$row->nobatch."^FS

                      ^FX Kode Bulk Human readable
                      ^FO320,165
                      ^A0N,20,20
                      ^FX Field block 507 dots wide, 3 lines max
                      ^FB240,3,,C,
                      ^FD".$koitem_bulk."^FS

                      ^FX SNFG Komponen Label
                      ^FO20,210
                      ^ADN,5,5
                      ^FD SNFG Komponen^FS

                      ^FX SNFG Komponen Value
                      ^FO20,230
                      ^A0N,20,20
                      ^FX Field block 507 dots wide, 2 lines max
                      ^FB290,2,,C,
                      ^FD".$row->snfg_komponen."^FS

                      ^FX No.Batch Label
                      ^FO320,210
                      ^ADN,5,5
                      ^FDNo.Batch^FS

                      ^FX No.Batch value
                      ^FO320,233
                      ^A0N,20,20
                      ^FX Field block 507 dots wide, 1 lines max
                      ^FB100,1,,C,
                      ^FD".$row->nobatch."^FS

                      ^FX Inspector Label
                      ^FO430,210
                      ^ADN,5,5
                      ^FDQC Bulk^FS

                      ^FX Inspector Value
                      ^FO425,233
                      ^A0N,20,20
                      ^FX Field block 507 dots wide, 1 lines max
                      ^FB115,2,,C,
                      ^FD".$row->nama_inspektor."^FS

                      ^FX Keterangan Label
                      ^FO30,280
                      ^ADN,5,5
                      ^FDKeterangan^FS

                      ^FX Keterangan Value
                      ^FO40,300
                      ^A0N,20,20
                      ^FX Field block 507 dots wide, 2 lines max
                      ^FB260,2,,L,
                      ^FD".$row->keterangan."^FS

                      ^FX No.Storage Label
                      ^FO320,278
                      ^ABN,5,5
                      ^FDNo.Storage^FS

                      ^FX No.Storage Value
                      ^FO320,295
                      ^A0N,20,20
                      ^FX Field block 507 dots wide, 1 lines max
                      ^FB90,1,,C,
                      ^FD".$row->no_storage."/".$jumlah_storage."^FS

                     ^FX Tanggal Check Label
                      ^FO430,277
                      ^ABN,5,5
                      ^FDTgl.Check^FS

                      ^FX Tanggal Check Value
                      ^FO425,294
                      ^A0N,20,20
                      ^FX Field block 507 dots wide, 1 lines max
                      ^FB130,1,,C,
                      ^FD".$date."^FS

                      ^FX Exp Date Label
                      ^FO305,323
                      ^ADN,5,5
                      ^FD Exp. ^FS

                      ^FX Exp Date Value
                      ^FO370,325
                      ^A0N,25,25
                      ^FX Field block 507 dots wide, 1 lines max
                      ^FB200,1,,L,
                      ^FD".$sl_storage."^FS

                      ^FX Footer value
                      ^FO20,345
                      ^A0N,5,5
                      ^FX Field block 507 dots wide, 1 lines max
                      ^FB100,1,,L,
                      ^FDQC BULK-MES^FS

                      ^FX Print quantity
                      ^PQ1,0,1,Y
                      ^FX End label format
                      ^XZ";

                    fwrite($myfile, $txt3);
                    fclose($myfile);

                    // In order to run the script below on the server
                    // - ssh-keygen (skip this if it already exists)
                    // - Enter2..  (skip this if it already exists)
                    // - ssh-copy-id -i ~/.ssh/id_rsa.pub [user]@[remote ip host]
                    // - Done!

                    // exec("cp /var/www/html/flowreport/web/label_qcbulk_".$client_id.".zpl /home/pi/label_qcbulk_'.$client_id.'.zpl");
                    // exec("lpr -P Zebra_Technologies_ZTC_GT800_ -o raw /home/pi/label_qcbulk_".$client_id.".zpl");

                    // Script to copy unique label to remote address
                    exec('scp /var/www/html/flowreport/web/label_qcbulk_'.$client_id.'.zpl '.$sbc_user.'@'.$sbc_ip.':/home/'.$sbc_user.'/label_qcbulk_'.$client_id.'.zpl 2>&1', $output);

                    // Execute Printing remotely
                    exec("ssh ".$sbc_user."@".$sbc_ip." 'lpr -P Zebra_Technologies_ZTC_GT800 -o raw /home/".$sbc_user."/label_qcbulk_".$client_id.".zpl'");
                    // Yii::$app->session->setFlash('success', "Device terdaftar di database");
                }else{

                    Yii::$app->session->setFlash('danger', "Device tidak terdaftar di database");

                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer);

    }

    public function actionPrintZebraTest($id)
    {

        $model = $this->findModel($id);

        $date = substr($model->timestamp, 0,10);
        $sl_storage = substr($model->savelife_storage, 0,10);

        // $connection = Yii::$app->getDb();
        // $command = $connection->createCommand("

        //         SELECT nobatch, besar_batch_real
        //         FROM pengolahan_batch
        //         WHERE snfg_komponen='".$model->snfg_komponen."' and besar_batch_real is not null
        //         LIMIT 1
        //         ");

        // $result = $command->queryAll();
        // $besar_batch = $result[0]['nobatch'];

        $sql="
                select distinct nama_fg,odoo_code,koitem_bulk,nama_bulk
                from scm_planner
                where snfg_komponen='".$model->snfg_komponen."'
                limit 1
             ";
        $arr = ScmPlanner::findBySql($sql)->one();
        $nama_fg = $arr->nama_fg;
        $odoo_code = $arr->odoo_code;
        $nama_bulk = $arr->nama_bulk;
        $koitem_bulk = $arr->koitem_bulk;

        $jumlah_storage = (new yii\db\Query())
                        ->select(['no_storage',])
                        ->from('qc_bulk_entry_komponen')
                        ->where(['nomo'=>$model->nomo])
                        ->distinct()
                        ->count();


        // $sql="
        //         select besar_batch_real,nobatch
        //         from batch_per_komponen
        //         where snfg_komponen='".$model->snfg_komponen."'
        //         limit 1
        //      ";
        // $ar = QcBulkEntryKomponen::findBySql($sql)->one();
        // $nobatch = $ar->nobatch;
        // $besar_batch_real = $ar->besar_batch_real;

        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $client_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $client_ip = $_SERVER['REMOTE_ADDR'];
        }

        $sbc_ip = DeviceMapQcbulk::find()->where(['frontend_ip' => $client_ip])->one()['sbc_ip'];
        $sbc_user = DeviceMapQcbulk::find()->where(['frontend_ip' => $client_ip])->one()['sbc_user'];

        // print_r($sbc_ip);


        if ($sbc_ip){
            $client_id = str_replace(".","",$client_ip);

            $myfile = fopen("label_qcbulk_".$client_id.".zpl", "w");

            // $myfile = fopen("label_qcbulk.zpl", "w");

            $txt3 = "
                    ^FX Begin setup
                    ^XA
                    ~TA120
                    ~JSN
                    ^LT0
                    ^MNW
                    ^MTT
                    ^PON
                    ^PMN
                    ^LH0,0
                    ^JMA
                    ^PR4,4
                    ~SD25
                    ^JUS
                    ^LRN
                    ^CI0
                    ^MD0
                    ^CI13,196,36
                    ^XZ
                    ^FX End setup

                    ^FX Begin label format
                    ^XA
                    ^MMT
                    ^LL0264
                    ^PW583
                    ^LS0

                    ^FX Line Vertical Right
                    ^FO420,200
                    ^GB0,115,3^FS

                    ^FX Line Vertical 2nd From Right Above
                    ^FO360,20
                    ^GB0,180,3^FS

                    ^FX Line Vertical 2nd From Right Below
                    ^FO301,200
                    ^GB0,150,3^FS

                    ^FX Line Horizontal First
                    ^FO20,100
                    ^GB340,0,3^FS

                    ^FX Line Horizontal Second
                    ^FO20,200
                    ^GB543,0,3^FS

                    ^FX Line Horizontal Left Bottom
                    ^FO20,275
                    ^GB283,0,3^FS

                    ^FX Line Horizontal Second Row From Right Bottom
                    ^FO303,260
                    ^GB255,0,3^FS

                    ^FX Line Horizontal First Row From Right Bottom
                    ^FO303,314
                    ^GB255,0,3^FS

                    ^FX Nama Status
                    ^FO40,30
                    ^ARN,16
                    ^FX Field block 507 dots wide, 2 lines max
                    ^FB260,2,,C,
                    ^FD ".$model->status."^FS

                    ^FX QR kode BB
                    ^FO390,20,0
                    ^BQN,0,4,H,7
                    ^FDMA,".$koitem_bulk." @ ".$model->nobatch."^FS

                    ^FX Kode BB Human readable
                    ^FO390,165
                    ^A0N10,5
                    ^FX Field block 507 dots wide, 2 lines max
                    ^FB130,2,,L,
                    ^FD".$koitem_bulk." @ ".$model->nobatch."^FS

                    ^FX SNFG Komponen Label
                    ^FO20,210
                    ^ADN5,5
                    ^FD SNFG Komponen^FS

                    ^FX SNFG Komponen Value
                    ^FO38,230
                    ^A0N10,20
                    ^FX Field block 507 dots wide, 2 lines max
                    ^FB260,2,,C,
                    ^FD".$model->snfg_komponen."^FS

                    ^FX No.Storage Label
                    ^FO310,268
                    ^ABN5,5
                    ^FDNo.Storage^FS

                    ^FX No.Storage Value
                    ^FO310,290
                    ^AJN5,20
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB100,1,,C,
                    ^FD".$model->no_storage."/".$jumlah_storage."^FS

                   ^FX Tanggal Check Label
                    ^FO430,267
                    ^ABN5,5
                    ^FDTgl.Check^FS

                    ^FX Tanggal Check Value
                    ^FO425,289
                    ^A0N5,19
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB130,1,,C,
                    ^FD ".$date."^FS


                    ^FX Nama Bulk Label
                    ^FO30,110
                    ^ADN5,5
                    ^FDNama Bulk^FS

                    ^FX Nama Bulk Value
                    ^FO38,130
                    ^A0N10,20
                    ^FX Field block 507 dots wide, 3 lines max
                    ^FB320,3,,C,
                    ^FD ".$nama_bulk."^FS

                    ^FX Keterangan Label
                    ^FO30,280
                    ^ADN5,5
                    ^FDKeterangan^FS

                    ^FX Keterangan Value
                    ^FO40,300
                    ^A0N10,20
                    ^FX Field block 507 dots wide, 2 lines max
                    ^FB260,2,,L,
                    ^FD ".$model->keterangan."^FS

                    ^FX Inspector Label
                    ^FO430,210
                    ^ABN10,15
                    ^FDQC Bulk^FS

                    ^FX Inspector Value
                    ^FO425,230
                    ^A0N10,20
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB115,1,,C,
                    ^FD ".$model->nama_inspektor."^FS

                    ^FX SaveLife Storage
                    ^FO305,323
                    ^ADN10,15
                    ^FD Exp. ^FS

                    ^FX SaveLife Storage Value
                    ^FO375,325
                    ^AKN20,20
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB120,1,,C,
                    ^FD ".$sl_storage." ^FS

                    ^FX No.Batch Label
                    ^FO315,208
                    ^ABN5,5
                    ^FDNo.Batch^FS

                    ^FX No.Batch value
                    ^FO200,228
                    ^AJN,20,20
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB300,1,,C,
                    ^FD ".$model->nobatch."^FS

                    ^FX Footer value
                    ^FO20,345
                    ^A0N,5,5
                    ^FX Field block 507 dots wide, 1 lines max
                    ^FB100,1,,L,
                    ^FDQC BULK-MES^FS

                    ^FX Print quantity
                    ^PQ1,0,1,Y
                    ^FX End label format
                    ^XZ";

            fwrite($myfile, $txt3);
            fclose($myfile);


            // exec("lpr -P Zebra_Technologies_ZTC_GT800_ -o raw /var/www/html/flowreport/web/label_qcbulk.zpl");

            // echo file_get_contents( "labelsnfg.prn" );

            // echo 'scp /var/www/html/flowreport_training5/web/labelsnfg_'.$client_id.'.zpl ptiadmin@'.$client_ip.':/home/ptiadmin/labelsnfg_'.$client_id.'.zpl';


            // In order to run the script below on the server
            // - ssh-keygen (skip this if it already exists)
            // - Enter2..  (skip this if it already exists)
            // - ssh-copy-id -i ~/.ssh/id_rsa.pub [user]@[remote ip host]
            // - Done!

            // exec("cp /var/www/html/flowreport/web/label_qcbulk_".$client_id.".zpl /home/pi/label_qcbulk_'.$client_id.'.zpl");
            // exec("lpr -P Zebra_Technologies_ZTC_GT800_ -o raw /home/pi/label_qcbulk_".$client_id.".zpl");



            // // Script to copy unique label to remote address
            exec('scp /var/www/html/flowreport/web/label_qcbulk_'.$client_id.'.zpl '.$sbc_user.'@'.$sbc_ip.':/home/'.$sbc_user.'/label_qcbulk_'.$client_id.'.zpl 2>&1', $output);

            // // Execute Printing remotely
            exec("ssh ".$sbc_user."@".$sbc_ip." 'lpr -P Zebra_Technologies_ZTC_GT800_ -o raw /home/".$sbc_user."/label_qcbulk_".$client_id.".zpl'");
            // Yii::$app->session->setFlash('success', "Device terdaftar di database");
        }else{

            Yii::$app->session->setFlash('danger', "Device tidak terdaftar di database");

        }


        return $this->redirect(Yii::$app->request->referrer);
        // print_r($nobatch);
    }


    public function actionGenerateKomponen($nomo)
    {


        $countnomo="
                select count(*) as id from qc_bulk_entry_komponen where nomo='".$nomo."'
             ";
        $arr = QcBulkEntryKomponen::findBySql($countnomo)->one();
        $count_nomo = $arr->id;


        if($count_nomo==0){
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

                INSERT INTO qc_bulk_entry_komponen (snfg_komponen,nomo,no_storage)
                SELECT
                        snfg_komponen,
                        nomo,
                        1 as no_storage
                FROM scm_planner
                WHERE nomo=:nomo;

                ",
                [':nomo' => $nomo]);

            $result = $command->queryAll();

            return $this->redirect(['create-rincian', 'nomo' => $nomo]);
        }else if($count_nomo>=1){
            return $this->redirect(['create-rincian', 'nomo' => $nomo]);
        }
    }

    public function actionCheckNomo($nomo)
    {
        $sql="
                select count(nomo) as id
                from qc_bulk_entry_komponen
                where nomo='".$nomo."'
             ";
        $ch = QcBulkEntryKomponen::findBySql($sql)->one();
        echo Json::encode($ch);
    }

    public function actionUpdateIp($frontend_ip)
    {
        $cookie_name = 'device_identity';

        $device = DeviceMapQcbulk::find()->where(['frontend_ip'=>$frontend_ip])->one();

        if (empty($device)){

            if(isset($_COOKIE[$cookie_name])) {
                $sql = "SELECT * FROM device_map_qcbulk WHERE MD5(frontend_ip) = :ip";
                $select = DeviceMapQcbulk::findBySql($sql,[':ip'=>$_COOKIE[$cookie_name]])->one(); 

                $sql = "UPDATE device_map_qcbulk set frontend_ip = :new_ip WHERE MD5(frontend_ip) = :ip";
                $update = Yii::$app->db->createCommand($sql,[':new_ip'=>$frontend_ip,':ip'=>$_COOKIE[$cookie_name]])->execute();
                if ($update > 0){
                    setcookie($cookie_name,hash('md5',$frontend_ip));
                }
            }

        }else{
            setcookie($cookie_name,hash('md5',$device->frontend_ip));
        }

    }

    public function actionCreateRincian($nomo)
    {
        // Get Device Information
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $frontend_ip = $_SERVER['REMOTE_ADDR'];
        }

        $this->actionUpdateIp($frontend_ip);
        $this->layout = '//main-sidebar-collapse';

        $model = new QcBulkEntryKomponen();

        $searchModel = new QcBulkEntryKomponenSearch();
        $dataProviderUtuh = $searchModel->search(Yii::$app->request->queryParams);
        $dataProviderUtuh->query->where("nomo='".$nomo."'")->andWhere("snfg_komponen not like '%/BS%'")->orderBy(['snfg_komponen'=>SORT_ASC, 'no_storage'=>SORT_ASC, 'timestamp'=>SORT_ASC]);
        $dataProviderSisa = $searchModel->search(Yii::$app->request->queryParams);
        $dataProviderSisa->query->where("nomo='".$nomo."'")->andWhere("snfg_komponen like '%/BS%'")->orderBy(['snfg_komponen'=>SORT_ASC, 'no_storage'=>SORT_ASC, 'timestamp'=>SORT_ASC]);

        // Editable
        if(Yii::$app->request->post('hasEditable'))
        {
            $operatorId = Yii::$app->request->post('editableKey');
            $operator = QcBulkEntryKomponen::findOne($operatorId);

            $out = Json::encode(['output'=>'','message'=>'']);
            $post = [];
            $posted = current($_POST['QcBulkEntryKomponen']);
            $post['QcBulkEntryKomponen'] = $posted;
            if($operator->load($post))
            {
                $operator->save();

            }
            echo $out;
            return;
        }

        // Get Related Information

        $planner_sql="
                select nama_fg, snfg_komponen, nama_bulk
                from scm_planner
                where nomo='".$nomo."'
                limit 1
             ";
        $arr = ScmPlanner::findBySql($planner_sql)->one();
        $nama_fg = $arr->nama_fg;
        $nama_bulk = $arr->nama_bulk;

        $nobatch = PengolahanBatch::find()->where(['snfg_komponen'=>$arr->snfg_komponen])->andWhere(['not',['nobatch'=>null]])->one()['nobatch'];

        if ($model->load(Yii::$app->request->post()) ) {

            $connection = Yii::$app->getDb();

            //Get Date of Latest Olah
            if (strpos($model->snfg_komponen,'/BS') !== false) {
              $command = $connection->createCommand("
              SELECT MAX (timestamp) last_check
              FROM (SELECT timestamp FROM qc_bulk_entry_komponen where nomo = '".$nomo."') as a;");

              $result = $command->queryAll();

              $last_date = substr($result[0]['last_check'], 0,10);
            }else{
              $command = $connection->createCommand("
                      SELECT MAX (datetime_stop) last_olah
                      FROM (SELECT datetime_stop FROM flow_input_mo where nomo = '".$nomo."' and posisi ='PENGOLAHAN') as a ");

              $result = $command->queryAll();

              $last_date = substr($result[0]['last_olah'], 0,10);
            }

            $datetime = getDate(strtotime($last_date));

            $unit = Yii::$app->request->post('QcBulkEntryKomponen')['unit'];
                if(empty($unit)){ $unit='days'; }

            if ($unit == 'days'){
                $savelife_storage = date(DATE_ATOM, mktime(0, 0, 0, $datetime['mon'], $datetime['mday']+$model->savelife, $datetime['year'])); //mktime({hour},{minute},{second},{month},{date},{year})
            }else{
                $savelife_storage = date(DATE_ATOM, mktime(0, 0, 0, $datetime['mon']+$model->savelife, $datetime['mday'], $datetime['year'])); //mktime({hour},{minute},{second},{month},{date},{year})
            }

            date_default_timezone_set('Asia/Jakarta');

            //Insert data more than one row

            for ($x=1; $x<=$model->storage; $x++){
                $sql="  select  coalesce(max(nourut),0)+1 as nourut
                    from qc_bulk_entry_komponen
                    where snfg_komponen='".$model->snfg_komponen."' and no_storage = ".$x."
                 ";
                $nourut= QcBulkEntryKomponen::findBySql($sql)->one()['nourut'];
                // $nourut = (new yii\db\Query())
                //         ->select(['no_storage',])
                //         ->from('qc_bulk_entry_komponen')
                //         ->where(['nomo'=>$model->nomo])
                //         ->distinct()
                //         ->count();
                $command = $connection->createCommand("
                    INSERT INTO qc_bulk_entry_komponen (no_storage, nourut, nomo, snfg_komponen, nama_inspektor, keterangan, savelife_storage, nobatch)
                    VALUES (".$x.",".$nourut.",'".$nomo."','".$model->snfg_komponen."','".$model->nama_inspektor."','".$model->keterangan."', '".$savelife_storage."', '".$model->nobatch."')
                    "
                    );

                $result = $command->queryAll();
            }

            // $connection = Yii::$app->getDb();
            // $command = $connection->createCommand("
            //     UPDATE qc_fg_v2
            //     SET qty_karantina = qty
            //     WHERE id = :id;
            // ",
            // [':id'=> $model->id]);
            if (strpos($model->snfg_komponen,'/BS') !== false) {
              return $this->redirect(['create-rincian', 'nomo' => $nomo, 'obj' => $_GET['obj'], 'jenis' => 'sisa']);
            }else{
              return $this->redirect(['create-rincian', 'nomo' => $nomo]);
            }
        } else {
            return $this->render('create-rincian', [
                'model' => $model,
                'nomo' => $nomo,
                'nama_fg' => $nama_fg,
                'nama_bulk' => $nama_bulk,
                'searchModel' => $searchModel,
                'dataProviderUtuh' => $dataProviderUtuh,
                'dataProviderSisa' => $dataProviderSisa,
                'nobatch' => $nobatch,
            ]);
        }
    }

    public function actionGetKomponen($nomo)
    {
                $countQcFg = ScmPlanner::find()
                                ->where(['nomo' => $nomo])
                                ->count();


                $sql = "
                        SELECT snfg_komponen
                        FROM scm_planner
                        WHERE nomo='".$nomo."'
                        ";

                $QcFgs = ScmPlanner::findBySql($sql)->all();

                if($countQcFg > 0)
                {
                    echo "<option value='Select Komponen'>Select Komponen</options>";
                    foreach($QcFgs as $QcFg){
                        echo "<option value='".$QcFg->snfg_komponen."'>".$QcFg->snfg_komponen."</options>";
                    }

                }
                else{
                        echo "<option>Nomo Tidak Valid</option>";
                }
    }

    /**
     * Displays a single QcBulkEntryKomponen model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new QcBulkEntryKomponen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new QcBulkEntryKomponen();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing QcBulkEntryKomponen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing QcBulkEntryKomponen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
        // print_r("expression");
    }

    public function actionInsert($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");
        $arr = QcBulkEntryKomponen::find()->where(['id'=>$id])->one();
        // print_r($arr->nomo);

        $nourut = $arr->nourut + 1;
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            INSERT INTO qc_bulk_entry_komponen (no_storage, nourut, nomo, snfg_komponen, nama_inspektor, keterangan, savelife_storage, nobatch)
            VALUES (".$arr->no_storage.", ".$nourut.",'".$arr->nomo."','".$arr->snfg_komponen."', '".$arr->nama_inspektor."','".$arr->keterangan."', '".$arr->savelife_storage."', '".$arr->nobatch."')
            "
            );

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionAdjust($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry_komponen
            SET timestamp = :current_time,
                status='ADJUST'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionOkFilling($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry_komponen
            SET timestamp = :current_time,
                status='OK FILLING'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionRelease($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry_komponen
            SET timestamp = :current_time,
                status='RELEASE'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionUncomformity1($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry_komponen
            SET timestamp = :current_time,
                status='UNCOMFORMITY 1'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionUncomformity2($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry_komponen
            SET timestamp = :current_time,
                status='UNCOMFORMITY 2'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionMasukSpesifikasi($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");

        $model = $this->findModel($id);
        if (strpos($model->snfg_komponen,'/BS') !== false) {
          $update = $this->queryUpdStatusBulkSisa($model->nomo,$model->snfg_komponen,$model->no_storage,'On Progress','in PRO');
        }

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry_komponen
            SET timestamp = :current_time,
                status='MASUK SPESIFIKASI'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionTidakMasukSpesifikasi($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry_komponen
            SET timestamp = :current_time,
                status='TIDAK MASUK SPESIFIKASI'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionPending($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");

        $model = $this->findModel($id);
        if (strpos($model->snfg_komponen,'/BS') !== false) {
          $update = $this->queryUpdStatusBulkSisa($model->nomo,$model->snfg_komponen,$model->no_storage,'Pending','in QC Bulk');
        }

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry_komponen
            SET timestamp = :current_time,
                status='PENDING'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionPending1($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry_komponen
            SET timestamp = :current_time,
                status='PENDING 1'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPending2($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry_komponen
            SET timestamp = :current_time,
                status='PENDING 2'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionRework($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");

        $model = $this->findModel($id);
        if (strpos($model->snfg_komponen,'/BS') !== false) {
          $update = $this->queryUpdStatusBulkSisa($model->nomo,$model->snfg_komponen,$model->no_storage,'Rework','in PPC');
        }

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry_komponen
            SET timestamp = :current_time,
                status='REWORK'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionCekMikro($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry_komponen
            SET timestamp = :current_time,
                status='CEK MIKRO'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionCekHomogenitas($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry_komponen
            SET timestamp = :current_time,
                status='CEK HOMOGENITAS'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionParallelMikro($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry_komponen
            SET timestamp = :current_time,
                status='PARALLEL MIKRO'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);;
    }

    public function actionPendingMikro($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry_komponen
            SET timestamp = :current_time,
                status='PENDING MIKRO'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);;
    }

    /**
      * actionREJECT
      * model qc_fg_v2 : Add Timestamp Check and status_check as 'REJECT' for an id
      * model qc_fg_v2_history_status : Log status_check and timestamp changes
      * @author Redha Hari
      * @param int $id
      * @return void redirect to previous page.
    */
    public function actionReject($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");

        $model = $this->findModel($id);
        if (strpos($model->snfg_komponen,'/BS') !== false) {
          $update = $this->queryUpdStatusBulkSisaReject($model->nomo,$model->snfg_komponen,$model->no_storage,'Rejected','in WPM',date('Y-m-d'));
        }

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry_komponen
            SET timestamp = :current_time,
                status='REJECT'
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionGetNourut($snfg_komponen)
    {
        $sql="  select  coalesce(max(nourut),0)+1 as nourut
                from qc_bulk_entry_komponen
                where snfg_komponen='".$snfg_komponen."'
             ";
        $lastinduk= QcBulkEntryKomponen::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }

    public function actionGetNostorage($snfg_komponen)
    {
        $sql="  select  coalesce(max(no_storage),0)+1 as no_storage
                from qc_bulk_entry_komponen
                where snfg_komponen='".$snfg_komponen."'
             ";
        $lastinduk= QcBulkEntryKomponen::findBySql($sql)->one();
        echo Json::encode($lastinduk);
    }

    /**
     * Finds the QcBulkEntryKomponen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QcBulkEntryKomponen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QcBulkEntryKomponen::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function queryUpdStatusBulkSisa($nomo,$snfg,$storage,$status,$pic){
      $connection = Yii::$app->db;
      $command = $connection->createCommand("
      UPDATE log_bulk_sisa SET status = :status, pic = :pic
      WHERE snfg_baru = :snfg AND nomo = :nomo AND no_storage = :storage
      ",
      [
        ':nomo' => $nomo,
        ':snfg' => $snfg,
        ':storage' => $storage,
        ':status' => $status,
        ':pic' => $pic,
      ]);

      $res = $command->queryAll();
      return $res;
    }

    public function queryUpdStatusBulkSisaReject($nomo,$snfg,$storage,$status,$pic,$rejected){
      $connection = Yii::$app->db;
      $command = $connection->createCommand("
      UPDATE log_bulk_sisa SET status = :status, pic = :pic, datetime_rejected = :rejected
      WHERE snfg_baru = :snfg AND nomo = :nomo AND no_storage = :storage
      ",
      [
        ':nomo' => $nomo,
        ':snfg' => $snfg,
        ':storage' => $storage,
        ':status' => $status,
        ':pic' => $pic,
        ':rejected' => $rejected,
      ]);

      $res = $command->queryAll();
      return $res;
    }
}
