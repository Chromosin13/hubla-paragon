<?php

namespace app\controllers;

use Yii;
use app\models\AdminlogFg;
use app\models\DataGrn;
use app\models\UserAdmin;
use app\models\NamaAdmin;
use app\models\WeigherFgMapDevice;
use app\models\AdminlogFgSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use app\base\Model;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\db\Query;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
/**
 * AdminlogFg implements the CRUD actions forAdminlogFg model.
 */
class AdminlogFgController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AdminlogFg models.
     * @return mixed
     */
     public function actionIndex()
     {
        $model = new AdminlogFg();
        // $model2 = new UserAdmin();
        $searchModel = new AdminlogFgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = (['datetime_start' => SORT_DESC, 'status_print' => SORT_DESC]);

        // If Request POST
        if ($model->load(Yii::$app->request->post())){
            $nopo = Yii::$app->request->post('AdminlogFg')['nopo'];
            // $admin_id = Yii::$app->request->post('AdminlogFg')['admin_id'];

            $sql= "
                SELECT
                    *
                FROM data_grn
                WHERE state = 'assigned' and nomor_po = '".$nopo."'
            ";

            $datas= DataGrn::findBySql($sql)->all();
            // print_r ('<pre>');
            // print_r ($datas);
            // print_r ('</pre>');

            $date = date("ymd");
            $first = true;
            foreach ($datas as $data) {
                $po = false;
                if (!$first) {
                    $po = NamaAdmin::findOne(['nomor_po' => $data->nomor_po ]);
                }
                $first = false;

                if(!$po){
                //Insert NOPO ke nama_admin, tanpa nama adminnya.
                $sqlinsert = Yii::$app->db->createCommand(
                            "INSERT INTO nama_admin(nomor_po)
                             VALUES ('".$data->nomor_po."')"
                            )->execute();
                }

                $sqlinsert2 = Yii::$app->db->createCommand(
                    "INSERT INTO adminlog_fg(nopo,koitem_fg,nama_fg,supplier,tgl_po,tgl_plan_kirim,qty_demand,price_unit,qty_kirim,sml_id)
                     VALUES (
                        '".$data->nomor_po."',
                        '".$data->kode_item."',
                        '".$data->nama_pabrik."',
                        '".$data->supplier."',
                        '".$data->tgl_po."',
                        '".$data->tgl_plan_kirim."',
                        '".$data->qty_demand."',
                        '".$data->price_unit."',
                        '".$data->qty_kirim."',
                        '".$data->sml_id."'
                    )"
                    )->execute();
            }

            return $this->redirect(['create-rincian', 'nopo' => $nopo]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
      }

    //   public function actionUpdateItem($nopo)
    //   {
    //     //$po = NamaAdmin::findOne(['nomor_po' => $no_po]);
    //
    //     $sql= "
    //         SELECT
    //             *
    //         FROM data_grn
    //         WHERE state = 'assigned' and nomor_po = '". $nopo ."'
    //     ";
    //
    //     $datas= DataGrn::findBySql($sql)->all();
    //     print_r ('<pre>');
    //     print_r ($data);
    //     print_r ('</pre>');
    //
    //     $first = true;
    //     foreach ($datas as $data) {
    //         if(!$po){
    //             $sqlinsert = Yii::$app->db->createCommand(
    //                         "INSERT INTO nama_admin(nomor_po)
    //                          VALUES ('".$d->nomor_po."')"
    //                         )->execute();
    //             }
    //
    //                 $sqlinsert2 = Yii::$app->db->createCommand(
    //                             "INSERT INTO adminlog(no_po,kode_item,nama_item,supplier,tgl_po,tgl_plan_kirim,qty_demand,price_unit,qty_kirim)
    //                              VALUES (
    //                                 '".$d->nomor_po."',
    //                                 '".$d->kode_item."',
    //                                 '".$d->nama_pabrik."',
    //                                 '".$d->supplier."',
    //                                 '".$d->tgl_po."',
    //                                 '".$d->tgl_plan_kirim."',
    //                                 '".$d->qty_demand."',
    //                                 '".$d->price_unit."',
    //
    //                                 '".$d->qty_kirim."'
    //                             )"
    //                             )->execute();
    //
    //     }
    //
    //     return $this->redirect(['create-rincian', 'no_po' => $no_po]);
    // }

    public function actionCheckNopo($nopo)
    {
        $sql="
        SELECT count(*) as id
        FROM adminlog_fg
        WHERE nopo = '".$nopo."'
        and nopo is not null
        ";

        $data= AdminlogFg::findBySql($sql)->one();
        echo Json::encode($data);
    }

    public function actionCreateRincian($nopo)
    {
      $sql2 = "
                SELECT *
                FROM nama_admin
                WHERE nomor_po = '". $nopo ."'
                AND nama is null
                ";
        $data2 = NamaAdmin::findBySql($sql2)->one();


        //$model = $this->findModelPo($no_po);
        $model = $data2;

        $sql = "
                SELECT *
                FROM adminlog_fg
                WHERE nopo = '". $nopo ."'
                AND no_surjal is null
                ";
        $data = AdminlogFg::findBySql($sql)->all();

        // $modelsAdminlog = $model->adminlogs;
        $modelsAdminlogFg = $data;

        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';

        // echo '<pre>';
        // print_r($modelAdmin);
        // echo '</pre>';



        if ($model->load(Yii::$app->request->post())) {

            $oldIDs = ArrayHelper::map($modelsAdminlogFg, 'id', 'id');
            $modelsAdminlogFg = Model::createMultiple(AdminlogFg::classname(), $modelsAdminlogFg);
            Model::loadMultiple($modelsAdminlogFg, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsAdminlogFg, 'id', 'id')));

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsAdminlogFg) && $valid;
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (!empty($deletedIDs)) {
                            AdminlogFg::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($modelsAdminlogFg as $modelAdminlog) {
                            $modelAdminlog->admin_id = $model->id;
                            if (! ($flag = $modelAdminlog->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                    $transaction->commit();
                    return $this->redirect(['index']);
                    }
                }catch(Exception $e) {
                $transaction->rollBack();
                }
            }
        }
        return $this->render('_form-rincian', [
            'model' => $model,
            'nopo' => $nopo,
            'modelsAdminlogFg' => (empty($modelsAdminlogFg)) ? [new AdminlogFg] : $modelsAdminlogFg
        ]);
    }

    public function actionPrintLabel($id)
    {


        $model = $this->findModel($id);

        $sql6="
                select koitem_fg,nama_fg,supplier,batch
                from adminlog_fg
                where nopo='".$model->nopo."'
                limit 1
             ";
        // $array_kodeitem = AdminlogFg::findBySql($sql6)->one();
        // $array_nosurjal = AdminlogFg::findBySql($sql6)->one();
        // $array_namaitem = AdminlogFg::findBySql($sql6)->one();
        // $array_supplier = AdminlogFg::findBySql($sql6)->one();
        // $array_nobatch = AdminlogFg::findBySql($sql6)->one();
        //
        // $kode_item = $array_kodeitem->koitem_fg;
        // $no_surjal = $array_nosurjal->no_surjal;
        // $nama_item = $array_namaitem->nama_fg;
        // $supplier = $array_supplier->supplier;

        $array = AdminlogFg::findBySql($sql6)->one();

        $koitem_fg = $array->koitem_fg;
        $no_surjal = $array->no_surjal;
        $nama_fg = $array->nama_fg;
        $supplier = $array->supplier;

        $batch = $array->batch;

        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $client_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $client_ip = $_SERVER['REMOTE_ADDR'];
        }

        print_r($client_ip);
        $sbc_ip = WeigherFgMapDevice::find()->where(['frontend_ip' => $client_ip])->one()['sbc_ip'];

        print_r($sbc_ip);


        if ($sbc_ip){
            $client_id = str_replace(".","",$client_ip);

            $myfile = fopen("label_admin_".$client_id.".zpl", "w");


            $txt = "

           ^FX Begin setup
                    ^XA
                    ~TA120
                    ~JSN
                    ^LT0
                    ^MNW
                    ^MTT
                    ^PON
                    ^PMN
                    ^LH0,0
                    ^JMA
                    ^PR4,4
                    ~SD25
                    ^JUS
                    ^LRN
                    ^CI0
                    ^MD0
                    ^CI13,196,36
                    ^XZ
            ^FX End setup

            ^XA
            ^MMT
            ^LL0240
            ^PW850
            ^LS0

            ^FO195,40^BY1,1,1^BQN,2,5^FDMA,".$model->nopo."^FS
            ^CF0,40
            ^FO190,175^FD".$model->batch."^FS

            ^FO545,40^BY1,1,1^BQN,2,5^FDMA,".$model->nopo."^FS
            ^CF0,40
            ^FO540,175^FD".$model->batch."^FS
            ^XZ
            ";

            fwrite($myfile, $txt);
            fclose($myfile);


            // // Script to copy unique label to remote address
            exec('scp /var/www/html/flowreport/web/label_admin_'.$client_id.'.zpl '.$sbc_user.'@'.$sbc_ip.':/home/'.$sbc_user.'/label_admin_'.$client_id.'.zpl 2>&1', $output);
            // exec('scp /var/www/html/packaging/web/label_admin_'.$client_id.'.zpl ahmadhafauzi@'.$sbc_ip.':/home/ahmadhafauzi/label_admin_'.$client_id.'.zpl 2>&1', $output);

            // // Execute Printing remotely
            exec("ssh ".$sbc_user."@".$sbc_ip." 'lpr -P Zebra_Technologies_ZTC_GT800_admin -o raw /home/".$sbc_user."label_admin_".$client_id.".zpl'");
            // exec("ssh ahmadhafauzi@".$sbc_ip." 'lpr -P Zebra_Technologies_ZTC_GT800_admin -o raw /home/ahmadhafauzi/label_admin_".$client_id.".zpl'");
            // Yii::$app->session->setFlash('success', "Device terdaftar di database");

        }else{

            Yii::$app->session->setFlash('danger', "Device tidak terdaftar di database");
        }

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
                            UPDATE adminlog_fg alg
                            SET datetime_stop = :current_time,
                                status_print = 'OK'
                            FROM (select nopo from adminlog_fg where id=:id) as tmp
                            WHERE alg.nopo=tmp.nopo;
                            ",
                            [ ':id'=>$id,
                              ':current_time' => date('Y-m-d h:i:s'),
                            ]
                        );
            $result = $command->queryAll();

        return $this->redirect(Yii::$app->request->referrer);

    }


    public function actionGetPo($nopo,$koitem_fg=false)
    {
        /*Yii::$app->db1;
        $sql = new Query();*/
        $sql= "
        SELECT
            *
        FROM data_purchase_order
        WHERE nomor_po = '".$nopo."'
        ";

        if ($koitem_fg) {
            $sql .= " and kode_item= '".$koitem_fg."'";
        }
        /*$command = $sql->createCommand();
        $data= $command->queryAll();*/
        //$data= Packaging::findBySql($sql)->all();
        $data= DataPurchaseOrder::findBySql($sql)->all();
        echo Json::encode($data);
        //print_r($data);
    }

    /**
     * Creates a new AdminlogFg model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AdminlogFg();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AdminlogFg model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AdminlogFg model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AdminlogFg model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdminlogFg the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AdminlogFg::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
