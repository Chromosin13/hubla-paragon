<?php

namespace app\controllers;

use Yii;
use app\models\DashboardLeadtimePenimbangan;
use app\models\DashboardLeadtimePenimbanganSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
/**
 * DashboardLeadtimePenimbanganController implements the CRUD actions for DashboardLeadtimePenimbangan model.
 */
class DashboardLeadtimePenimbanganController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DashboardLeadtimePenimbangan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DashboardLeadtimePenimbanganSearch();
        $searchModel->upload_date = '2017-01-01 - 2018-12-31';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';

        // Query Sediaan
        $sql3="
                SELECT
                    DISTINCT sediaan
                FROM dashboard_leadtime_penimbangan
            ";

            $sediaan_list = ArrayHelper::map(DashboardLeadtimePenimbangan::findBySql($sql3)->all(), 'sediaan','sediaan');

        // Query Nama FG
        $sql3="
                SELECT
                    DISTINCT nama_fg
                FROM dashboard_leadtime_penimbangan
            ";

            $nama_fg_list = ArrayHelper::map(DashboardLeadtimePenimbangan::findBySql($sql3)->all(), 'nama_fg','nama_fg');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'sediaan_list' => $sediaan_list,
            'nama_fg_list' => $nama_fg_list,
        ]);
    }

    /**
     * Displays a single DashboardLeadtimePenimbangan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DashboardLeadtimePenimbangan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DashboardLeadtimePenimbangan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DashboardLeadtimePenimbangan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DashboardLeadtimePenimbangan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DashboardLeadtimePenimbangan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DashboardLeadtimePenimbangan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DashboardLeadtimePenimbangan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
