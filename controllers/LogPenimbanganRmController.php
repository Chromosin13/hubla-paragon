<?php

namespace app\controllers;

use Yii;
use app\models\LogPenimbanganRm;
use app\models\MasterDataTimbanganRm;
use app\models\LogPenimbanganRmOperator;
use app\models\LogPenimbanganRmSplit;
use app\models\FlowInputMo;
use app\models\DeviceMapRm;
use app\models\BillOfMaterials;
use app\models\ScmPlanner;
use app\models\LogJadwalTimbangRm;
use app\models\LogPenimbanganRmSearch;
use app\models\DailyConsumption;
use app\models\LogApprovalBr;
use app\models\LogBrDetail;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use kartik\mpdf\Pdf;
use yii\data\ArrayDataProvider;

/**
 * LogPenimbanganRmController implements the CRUD actions for LogPenimbanganRm model.
 */
class LogPenimbanganRmController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LogPenimbanganRm models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogPenimbanganRmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all LogPenimbanganRm models.
     * @return mixed
     */
    public function actionTest()
    {
        $sql9="

            SELECT realisasi, satuan_realisasi, kode_internal, kode_bb, log, log_realisasi, no_smb,nomo,nobatch FROM log_penimbangan_rm WHERE nomo = 'MO-CON-LBBL02-BULK/G0012'
            ";

        $data = Yii::$app->db->createCommand($sql9)->queryAll();
        // print_r($data);

        $item_json = json_encode($data);
        // print_r('<pre>');
        // print_r($data);
        $create_json_file = file_put_contents("update_stock_ndc/data_rmw.json", $item_json);
        if ($create_json_file)
        {
            echo "JSON file created successfully...";
            // require (Yii::getAlias('@webroot').'/update_stock_ndc/create_receive_from_fro.php');
        }else{
            echo "Oops! Error creating json file...";
            // return $this->redirect(['qc-fg-v2/halaman-terima-ndc', 'e'=>'json-fail']);
        }

        // return $this->render('test', [
            // 'data' => $data,
        // ]);
    }

    /**
     * Lists all LogPenimbanganRm models.
     * @return mixed
     */
    public function actionStart($id)
    {
        $searchModel = new LogPenimbanganRmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';

        //$status = "Belum Ditimbang";

        //$arrayOp = array(2,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3);

        // $sql="

        //     SELECT nomo,no_formula, no_revisi
        //     FROM flow_input_mo
        //     WHERE id = ".$id.";";

        // $data = FlowInputMo::findBySql($sql)->one();

        // $connection = Yii::$app->getDb();
        // $command = $connection->createCommand("

        // INSERT INTO log_penimbangan_rm(nomo,no_formula,no_revisi,naitem_fg,kode_bb,nama_bb,kode_olah,siklus,weight,satuan,cycle_time,timbangan,operator,no_timbangan, koitem_fg, status, is_split)
        // SELECT c.nomo as nomo,
        //         a.no_formula as no_formula,
        //         b.no_revisi as no_revisi,
        //         c.nama_fg as naitem_fg,
        //         b.kode_bb as kode_bb,
        //         b.nama_bb as nama_bb,
        //         b.kode_olah as kode_olah,
        //         b.siklus as siklus,
        //         b.weight as weight,
        //         b.satuan as satuan,
        //         b.cycle_time as cycle_time,
        //         b.timbangan as timbangan,
        //         b.operator:operator},function as operator,
        //         b.no_timbangan as no_timbangan,
        //         c.koitem_fg as koitem_fg,
        //         :status as status,
        //         :split as is_split
        //     FROM master_data_formula a
        // JOIN scm_planner c ON c.nomo = :nomo AND a.koitem_fg = c.koitem_fg
        // JOIN bill_of_materials b ON a.no_formula = b.no_formula AND a.no_formula = :no_formula AND b.no_revisi = :no_revisi AND b.koitem_fg = c.koitem_fg
        // ",
        // [':nomo' => $data->nomo, ':no_formula' => $data->no_formula, ':no_revisi' => $data->no_revisi, ':status' => $status, ':split' => 0]);

        // $result = $command->queryAll();

        return $this->render('start', [
            'id' => $id,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all LogPenimbanganRm models.
     * @return mixed
     */
    public function actionTask($id)
    {
        $searchModel = new LogPenimbanganRmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';

        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            *
        FROM device_map_rm
        WHERE device_ip = '".$ip."' ";

        $map_device_array = DeviceMapRm::findBySql($sql)->one();

        // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
        if(empty($map_device_array)){
            $device_ip = 'BELUM TERDAFTAR';
            $operator = 1;
            $nama_line = 'BELUM TERDAFTAR';
        }else{
            $device_ip = $map_device_array->device_ip;
            $operator = $map_device_array->operator;
            $nama_line = $map_device_array->nama_line;
        }

        $sql9="

            SELECT nomo,no_formula, no_revisi, nama_line, siklus, is_split_line
            FROM flow_input_mo
            WHERE id = ".$id.";";

        $data = FlowInputMo::findBySql($sql9)->one();
        $nomo = $data->nomo;
        $no_formula = $data->no_formula;
        $no_revisi = $data->no_revisi;
        $siklus = $data->siklus;
        $is_split_line = $data->is_split_line;
        // $nama_line = $data->nama_line;

        // $sqll="

        //     SELECT nama_line
        //     FROM log_jadwal_timbang_rm
        //     WHERE nomo = '".$nomo."'";

        // $line_row = LogJadwalTimbangRm::findBySql($sqll)->one();

        // $nama_line = $data->nama_line;

        

        

        if ($operator != 11){
            if ($siklus == 0){
                // GET LIST TASK ALL
                $sql10 = "
                SELECT *
                FROM log_penimbangan_rm
                WHERE nomo = :nomo AND operator = :operator AND nama_line = :nama_line AND kode_bb != 'AIR-RO--L' ORDER by id ASC
                ";
                $task = LogPenimbanganRm::findBySql($sql10,[':nomo'=>$nomo, ':operator'=>$operator,':nama_line'=>$nama_line])->all();

            }else if($siklus==-1){
                // GET LIST TASK REPACK H-1
                $sql10 = "
                SELECT *
                FROM log_penimbangan_rm
                WHERE nomo = :nomo AND operator = :operator AND nama_line = :nama_line AND is_repack = 1 AND kode_bb != 'AIR-RO--L' ORDER by id ASC
                ";
                $task = LogPenimbanganRm::findBySql($sql10,[':nomo'=>$nomo, ':operator'=>$operator,':nama_line'=>$nama_line])->all();

            }else{
                // GET LIST TASK SIKLUS
                $sql10 = "
                SELECT *
                FROM log_penimbangan_rm
                WHERE nomo = :nomo AND operator = :operator AND nama_line = :nama_line AND siklus = :siklus AND kode_bb != 'AIR-RO--L' ORDER by id ASC
                ";
                $task = LogPenimbanganRm::findBySql($sql10,[':nomo'=>$nomo, ':operator'=>$operator,':nama_line'=>$nama_line,':siklus'=>$siklus])->all();

            }
        }else{
            // GET LIST TASK AIR RO
            $sql10 = "
            SELECT *
            FROM log_penimbangan_rm
            WHERE nomo = :nomo AND kode_bb = 'AIR-RO--L' ORDER by id ASC
            ";
            $task = LogPenimbanganRm::findBySql($sql10,[':nomo'=>$nomo])->all();

        }
        


        if (empty($task)){
            echo "<script>window.alert('Tablet ini tidak memiliki task');";
            echo "window.location = 'http://factory.pti-cosmetics.com/flowreport/web/index.php?r=flow-input-mo/check-penimbangan-rm';";
            echo "</script>";
        } else {

            $naitem_fg = $task[0]['naitem_fg'];
            if ($operator != 11){
                if ($siklus == 0){
                    //COUNT TASK ALL
                    $countTasks="
                        SELECT COUNT(1)
                        FROM log_penimbangan_rm
                        WHERE nomo = :nomo AND operator = :operator AND nama_line = :nama_line AND kode_bb != 'AIR-RO--L'
                        ";
                    $taskQty = LogPenimbanganRm::findBySql($countTasks,[':nomo'=>$nomo,':operator'=>$operator,':nama_line'=>$nama_line])->scalar();

                }else if ($siklus==-1){
                    //COUNT TASK REPACK H-1
                    $countTasks="
                        SELECT COUNT(1)
                        FROM log_penimbangan_rm
                        WHERE nomo = :nomo AND operator = :operator AND nama_line = :nama_line AND is_repack = 1 AND kode_bb != 'AIR-RO--L'
                        ";
                    $taskQty = LogPenimbanganRm::findBySql($countTasks,[':nomo'=>$nomo,':operator'=>$operator,':nama_line'=>$nama_line])->scalar();

                }else{
                    //COUNT TASK SIKLUS
                    $countTasks="
                        SELECT COUNT(1)
                        FROM log_penimbangan_rm
                        WHERE nomo = :nomo AND operator = :operator AND nama_line = :nama_line AND siklus = :siklus AND kode_bb != 'AIR-RO--L'
                        ";
                    $taskQty = LogPenimbanganRm::findBySql($countTasks,[':nomo'=>$nomo,':operator'=>$operator,':nama_line'=>$nama_line,':siklus'=>$siklus])->scalar();

                }
            } else {
                //COUNT TASK AIR RO
                $countTasks = "
                    SELECT COUNT(1)
                    FROM log_penimbangan_rm
                    WHERE nomo = '".$nomo."' AND kode_bb = 'AIR-RO--L' ORDER by id ASC
                    ";
                $taskQty = LogPenimbanganRm::findBySql($countTasks)->scalar();
            }


            // Get List Operator Array
            $sql3="
                    SELECT
                         unnest(string_to_array(nama_operator, ',')) as nama_operator
                    FROM flow_input_mo
                    WHERE id = ".$id;

            $list_operator = ArrayHelper::map(FlowInputMo::findBySql($sql3)->all(), 'nama_operator','nama_operator');

            // Get Operator Count
            $sql5="
                    SELECT count(*) as id
                    FROM
                    (SELECT
                         unnest(string_to_array(nama_operator, ',')) as nama_operator
                    FROM flow_input_mo
                    WHERE id = ".$id.") a ";

            $sum_operator = FlowInputMo::findBySql($sql5)->one();

            // Get Current Selected Operator
            $sql4="
                    SELECT
                         distinct on (flow_input_mo_id)
                         nama_operator
                    FROM log_penimbangan_rm_operator
                    WHERE flow_input_mo_id = ".$id." AND operator = '".$operator."'
                    ORDER BY flow_input_mo_id, id desc";

            $op_terpilih = FlowInputMo::findBySql($sql4)->one();

            $operator_ = explode(',',$op_terpilih['nama_operator']);

            if (count($operator_)>1){
                $operator_terpilih = $operator_[0];
                $operator_terpilih2 = $operator_[1];
            } else {
                $operator_terpilih = $op_terpilih['nama_operator'];
                $operator_terpilih2 = "";
            }

            $countSiklus="
            SELECT distinct (siklus)
            FROM log_penimbangan_rm
            WHERE nomo = '".$data->nomo."'
            ";

            $countRepack="
            SELECT distinct (siklus)
            FROM log_penimbangan_rm
            WHERE nomo = '".$data->nomo."' AND is_repack = 1
            ";
    
            if ($siklus == 0){
                $qtySiklus = BillOfMaterials::findBySql($countSiklus)->all();
            }else if ($siklus ==-1){
                $qtySiklus = BillOfMaterials::findBySql($countRepack)->all();
            }else{
                $qtySiklus = [['siklus'=>$siklus]];
            }


            return $this->render('task', [
            'siklus' => $siklus,
            'taskQty' => $taskQty,
            'nomo' => $nomo,
            'no_formula' => $no_formula,
            'no_revisi' => $no_revisi,
            'naitem_fg' => $naitem_fg,
            'task' => $task,
            'nama_line' => $nama_line,
            'device_ip' => $device_ip,
            'operator' => $operator,
            'id' => $id,
            'list_operator' => $list_operator,
            'operator_terpilih' => $operator_terpilih,
            'operator_terpilih2' => $operator_terpilih2,
            'sum_operator' => $sum_operator->id,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'qtySiklus' => $qtySiklus,
            'is_split_line' => $is_split_line,
            'op_terpilih' => $op_terpilih
        ]);
        }
    }

    /**
     * Lists all LogPenimbanganRm models.
     * @return mixed
     */
    public function actionAutoMassa($id,$wo,$log)
    {
        $server_name = $_SERVER['SERVER_NAME'];
        if($server_name == 'factory.pti-cosmetics.com'){
             echo "<script>location.href='".Yii::getALias('@ipUrl')."log-penimbangan-rm/auto-massa&id=".$id."&wo=".$wo."&log=".$log."';</script>";
        }
        $searchModel = new LogPenimbanganRmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';

        $sql9="

            SELECT nomo,no_formula, no_revisi
            FROM flow_input_mo
            WHERE id = ".$id.";";

        $data = FlowInputMo::findBySql($sql9)->one();
        $nomo = $data->nomo;
        $no_formula = $data->no_formula;
        $no_revisi = $data->no_revisi;

        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }


        // GET LIST TASK
        $sql10 = "
        SELECT *
        FROM log_penimbangan_rm
        WHERE id = '".$wo."'
        ";

        $task = LogPenimbanganRm::findBySql($sql10)->all();
        $naitem_fg = $task[0]['naitem_fg'];
        $weight = $task[0]['weight'];
        $satuan = $task[0]['satuan'];
        $nama_bb = $task[0]['nama_bb'];
        $kode_bb = $task[0]['kode_bb'];
        $timbangan = $task[0]['timbangan'];
        $is_repack = $task[0]['is_repack'];
        $kode_internal = $task[0]['kode_internal'];

        // Get Mapping Line - Frontend IP Array
        $sqll="
        SELECT
            *
        FROM device_map_rm
        WHERE timbangan = '".$timbangan."'";

        $map_device_array2 = DeviceMapRm::findBySql($sqll)->one();
        $raspi_ip = $map_device_array2->raspi_ip;

        // Get decimal digit
        $sqll="
        SELECT
            angka_belakang_koma,galat
        FROM master_data_timbangan_rm
        WHERE kode_timbangan = '".$timbangan."'";

        $data_timbangan = MasterDataTimbanganRm::findBySql($sqll)->one();
        $decimal_digit = $data_timbangan['angka_belakang_koma'];
        
        if ($is_repack == 1){
            $master_repack = Yii::$app->db->createCommand("SELECT kode_internal, galat FROM master_bahan_baku_repack WHERE kode_internal = '".$kode_internal."'")->queryOne();
            $galat = $master_repack['galat'];
        }else{
            $galat = $data_timbangan['galat'];
        }

        $galat=0;

        // Get Current Selected Operator
        $sql4="
                SELECT
                     distinct on (flow_input_mo_id)
                     nama_operator
                FROM log_penimbangan_rm_operator
                WHERE flow_input_mo_id = ".$id."
                ORDER BY flow_input_mo_id, id desc";

        $op_terpilih = FlowInputMo::findBySql($sql4)->one();
        $operator_terpilih = $op_terpilih['nama_operator'];


        if ($task[0]['is_split']==1){
            $countSplit="
                SELECT COUNT(1)
                FROM log_penimbangan_rm_split
                WHERE log_penimbangan_rm_id = '".$wo."'
                ";

            $splitQty = LogPenimbanganRmSplit::findBySql($countSplit)->scalar();

            $split_data_sql="
                SELECT *
                FROM log_penimbangan_rm_split
                WHERE log_penimbangan_rm_id = '".$wo."'
                ";

            $split_data = LogPenimbanganRmSplit::findBySql($split_data_sql)->all();
        } else {
            $splitQty = 0;
            $split_data = 0;
        }

        return $this->render('auto-massa', [
            'nomo' => $nomo,
            'no_formula' => $no_formula,
            'no_revisi' => $no_revisi,
            'naitem_fg' => $naitem_fg,
            'weight' => $weight,
            'satuan' => $satuan,
            'nama_bb' => $nama_bb,
            'kode_bb' => $kode_bb,
            'timbangan' => $timbangan,
            'task' => $task,
            'raspi_ip' => $raspi_ip,
            'operator_terpilih' => $operator_terpilih,
            'id' => $id,
            'wo' => $wo,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'log'=>$log,
            'decimal_digit'=>$decimal_digit,
            'galat'=>$galat,
            'splitQty' => $splitQty,
            'split_data' => $split_data,
            'is_split' => $task[0]['is_split']
        ]);
    }

    /**
     * Lists all LogPenimbanganRm models.
     * @return mixed
     */
    public function actionScanBb($id,$wo)
    {
        $server_name = $_SERVER['SERVER_NAME'];
        if($server_name == '10.3.5.102'){
             echo "<script>location.href='".Yii::getALias('@dnsUrl')."log-penimbangan-rm/scan-bb&id=".$id."&wo=".$wo."';</script>";
        }
        $searchModel = new LogPenimbanganRmSearch();
        $FlowInputMo = FlowInputMo::find()->where(['id'=>$id])->one();
        $nama_line = $FlowInputMo->nama_line;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $log_penimbangan_rm_sql="
            SELECT kode_bb,nama_bb,is_split,no_timbangan
            FROM log_penimbangan_rm
            WHERE id = '".$wo."'
            ";

        $log_penimbangan_rm = LogPenimbanganRm::findBySql($log_penimbangan_rm_sql)->one();

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            *
        FROM device_map_rm
        WHERE device_ip = '".$ip."' and nama_line = '".$nama_line."'";

        $map_device_array = DeviceMapRm::findBySql($sql)->one();

        // Get Current Selected Operator
        $sql4="
                SELECT
                     distinct on (flow_input_mo_id)
                     nama_operator
                FROM log_penimbangan_rm_operator
                WHERE flow_input_mo_id = ".$id." AND operator = '".$map_device_array->operator."'
                ORDER BY flow_input_mo_id, id desc";

        $op_terpilih = FlowInputMo::findBySql($sql4)->one();

        $operator = $op_terpilih->nama_operator;

        if ($log_penimbangan_rm->is_split==1){
            $countSplit="
                SELECT COUNT(1)
                FROM log_penimbangan_rm_split
                WHERE log_penimbangan_rm_id = '".$wo."'
                ";

            $splitQty = LogPenimbanganRmSplit::findBySql($countSplit)->scalar();

            $split_data_sql="
                SELECT *
                FROM log_penimbangan_rm_split
                WHERE log_penimbangan_rm_id = '".$wo."'
                ";

            $split_data = LogPenimbanganRmSplit::findBySql($split_data_sql)->all();
        } else {
            $splitQty = 0;
            $split_data = 0;
        }

        return $this->render('scan-bb', [
            'id' => $id,
            'wo' => $wo,
            'kode_bb' => $log_penimbangan_rm->kode_bb,
            'nama_bb' => $log_penimbangan_rm->nama_bb,
            'is_split' => $log_penimbangan_rm->is_split,
            'splitQty' => $splitQty,
            'split_data' => $split_data,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'operator' => $operator,
            'timbangan' => $log_penimbangan_rm->no_timbangan,
        ]);
    }

    public function actionScanCekRepack($id,$wo)
    {
        $searchModel = new LogPenimbanganRmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $log_penimbangan_rm_sql="
            SELECT kode_bb,nama_bb,is_split,no_timbangan,id_bb
            FROM log_penimbangan_rm
            WHERE id = '".$wo."'
            ";

        $log_penimbangan_rm = LogPenimbanganRm::findBySql($log_penimbangan_rm_sql)->one();

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            *
        FROM device_map_rm
        WHERE device_ip = '".$ip."'";

        $map_device_array = DeviceMapRm::findBySql($sql)->one();

        // Get Current Selected Operator
        $sql4="
                SELECT
                     distinct on (flow_input_mo_id)
                     nama_operator
                FROM log_penimbangan_rm_operator
                WHERE flow_input_mo_id = ".$id." AND operator = '".$map_device_array->operator."'
                ORDER BY flow_input_mo_id, id desc";

        $op_terpilih = FlowInputMo::findBySql($sql4)->one();

        $operator = $op_terpilih->nama_operator;

        if ($log_penimbangan_rm->is_split==1){
            $countSplit="
                SELECT COUNT(1)
                FROM log_penimbangan_rm_split
                WHERE log_penimbangan_rm_id = '".$wo."'
                ";

            $splitQty = LogPenimbanganRmSplit::findBySql($countSplit)->scalar();

            $split_data_sql="
                SELECT *
                FROM log_penimbangan_rm_split
                WHERE log_penimbangan_rm_id = '".$wo."'
                ";

            $split_data = LogPenimbanganRmSplit::findBySql($split_data_sql)->all();
        } else {
            $splitQty = 0;
            $split_data = 0;
        }

        return $this->render('scan-cek-repack', [
            'id' => $id,
            'id_bb' => $log_penimbangan_rm->id_bb,
            'wo' => $wo,
            'kode_bb' => $log_penimbangan_rm->kode_bb,
            'nama_bb' => $log_penimbangan_rm->nama_bb,
            'is_split' => $log_penimbangan_rm->is_split,
            'splitQty' => $splitQty,
            'split_data' => $split_data,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'operator' => $operator,
            'timbangan' => $log_penimbangan_rm->no_timbangan,
        ]);
    }

    public function actionCheckBb($wo,$kode_bb)
    {
        $sql="select * from log_penimbangan_rm where id=".$wo;
        $check= LogPenimbanganRm::findBySql($sql)->one();

        // Get Original IP Address from direct IP / DNS access
        // if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //     $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        // }else{
        //     $ip = $_SERVER['REMOTE_ADDR'];
        // }

        // Get Mapping Line - Frontend IP Array
        $sql2="
        SELECT
            *
        FROM device_map_rm
        WHERE timbangan = '".$check->timbangan."'";

        $map_device_array = DeviceMapRm::findBySql($sql2)->one();

        if(!empty($map_device_array)){
            // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
            //$operator = $map_device_array->operator;
            $weigher_connection = $map_device_array->weigher_connection;

            if (($check->kode_bb == $kode_bb || $check->kode_internal == $kode_bb) && ($weigher_connection == 1)) {
                echo 1;
            } else if (($check->kode_bb == $kode_bb || $check->kode_internal == $kode_bb) && ($weigher_connection != 1)) {
                echo 2;
            } else {
                echo 0;
            }
        }else{
            if (($check->kode_bb == $kode_bb || $check->kode_internal == $kode_bb) ) {
                echo 2;
            } else {
                echo 0;
            }
        }

        
    }

    /**
     * Stop Jadwal Kemas 2 Inline Function
     * @param $snfg
     * @return mixed
     */
    public function actionInsertBatch($wo,$nobatch)
    {
        $model = new LogPenimbanganRm();

        // Populate Grid Data
        $searchModel = new LogPenimbanganRmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';

        // Stop Jadwal
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

        UPDATE log_penimbangan_rm
        SET nobatch = :nobatch
        WHERE id = :wo;
        ",
        [':nobatch'=> $nobatch,
         ':wo'=> $wo
        ]);

        $result = $command->queryAll();

        if ($result){
            echo 1;
        } else {
            echo 0;
        }
    }

    /**
     * Stop Jadwal Kemas 2 Inline Function
     * @param $snfg
     * @return mixed
     */
    public function actionAddBatch($id,$wo,$realisasi,$log,$log_realisasi)
    {
        $sql="select kode_bb,nama_bb,satuan,nobatch,is_split,kode_internal,log from log_penimbangan_rm where id=".$wo;
        $check= LogPenimbanganRm::findBySql($sql)->one();

        $sql2="select * from log_penimbangan_rm_split where log_penimbangan_rm_id=".$wo."order by id desc";
        $isNew= LogPenimbanganRmSplit::findBySql($sql2)->all();

        if (empty($isNew)){
            $urutan = 0;
            $qty = $realisasi;
        } else {
            $urutan = $isNew[0]['urutan'];
            $realisasi_split = 0; 
            foreach ($isNew as $new){
                $realisasi_split += $new['realisasi']; 
            }
            $qty = $realisasi-$realisasi_split;
        }

        if ($qty > 0){
            $connection2 = Yii::$app->getDb();
            $command2 = $connection2->createCommand("
                INSERT INTO log_penimbangan_rm_split (log_penimbangan_rm_id,kode_bb,no_batch,realisasi,satuan_realisasi,urutan, flow_input_mo_id,nama_bb,kode_internal,log,log_realisasi)
                SELECT :wo as log_penimbangan_rm_id, :kode_bb as kode_bb, :no_batch as no_batch, :realisasi as realisasi, :satuan as satuan_realisasi, :urutan as urutan, :flow_input_mo_id as flow_input_mo_id, :nama_bb as nama_bb , :kode_internal as kode_internal, :log as log, :log_realisasi as log_realisasi
                ",
                [':wo' => $wo,
                 ':realisasi' => $realisasi,
                 ':kode_bb' => $check->kode_bb,
                 ':no_batch' => $check->nobatch,
                 ':realisasi' => $qty,
                 ':satuan' => $check->satuan,
                 ':urutan' => $urutan+1,
                 ':flow_input_mo_id' => $id,
                 ':nama_bb' => $check->nama_bb,
                 ':kode_internal' => $check->kode_internal,
                 ':log'=>$log,
                 ':log_realisasi'=>$log_realisasi
                ]
            );

            $result2 = $command2->queryAll();
            
            if ($check->is_split == 0){
                // Update is_split status
                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("
                    UPDATE log_penimbangan_rm
                    SET is_split = 1
                    WHERE id = :wo;
                ",
                [':wo'=> $wo
                ]);
                $result = $command->queryAll();

                // Copy Log status from Log_penimbangan_rm ke Log_penimbangan_rm_split
                $connection3 = Yii::$app->getDb();
                $command3 = $connection3->createCommand("
                    UPDATE log_penimbangan_rm_split
                    SET log = :log
                    WHERE log_penimbangan_rm_id = :wo;
                ",
                [':wo'=> $wo,
                 ':log'=> $check->log
                ]);
                $result3 = $command3->queryAll();

                if ($result2 && $result && $result3){
                // Redirect to Downtime
                    $value = array(
                    "status"=>"success");
                    // return $this->redirect(['create-kemas1', 'snfg_komponen' => $snfg_komponen]);
                     // return $this->redirect(['check-kemas1']);
                } else {
                    $value = array(
                    "status"=>"not-success");
                }

                echo Json::encode($value);
            } else {

                if ($result2){
                // Redirect to Downtime
                    $value = array(
                    "status"=>"success");
                    // return $this->redirect(['create-kemas1', 'snfg_komponen' => $snfg_komponen]);
                     // return $this->redirect(['check-kemas1']);
                } else {
                    $value = array(
                    "status"=>"not-success");
                }

                echo Json::encode($value);
            }
            
        }else{
            $value = array(
            "status"=>"massa-null");

            echo Json::encode($value);
        }

    }

    public function actionUpdateLogInput($wo,$log)
    {
        $sql="select is_split from log_penimbangan_rm where id=".$wo;
        $check= LogPenimbanganRm::findBySql($sql)->one();

        if ($check->is_split == 0){
            // Update Log
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
                UPDATE log_penimbangan_rm
                SET log = :log
                WHERE id = :wo;
            ",
            [':wo'=> $wo,
             ':log'=> $log
            ]);
            $result = $command->queryAll();
        } else {
            $sql2="select * from log_penimbangan_rm_split where log_penimbangan_rm_id=".$wo."";
            $split= LogPenimbanganRmSplit::findBySql($sql2)->one();

            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
                UPDATE log_penimbangan_rm_split
                SET log = :log
                WHERE log_penimbangan_rm_id = :wo and urutan = :urutan;
            ",
            [':wo'=> $wo,
             ':log'=> $log,
             ':urutan'=>count($split)
            ]);
            $result = $command->queryAll();
        }

        if ($result){
            echo 1;
        } else {
            echo 0;
        }

    }

    public function actionUpdateLogRepack($id_bb,$log)
    {
        // $sql="select is_split from log_penimbangan_rm where id=".$wo;
        // $check= LogPenimbanganRm::findBySql($sql)->one();

        // if ($check->is_split == 0){
            // Update Log
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            UPDATE log_penimbangan_rm
            SET log_cek_repack = :log, status = :status
            WHERE id_bb = :id_bb;
        ",
        [':id_bb'=> $id_bb,
         ':log'=> $log,
         ':status'=> 'Sudah Ditimbang'
        ]);
        $result = $command->queryAll();
        // } else {
        //     $sql2="select * from log_penimbangan_rm_split where log_penimbangan_rm_id=".$wo."";
        //     $split= LogPenimbanganRmSplit::findBySql($sql2)->one();

        //     $connection = Yii::$app->getDb();
        //     $command = $connection->createCommand("
        //         UPDATE log_penimbangan_rm_split
        //         SET log = :log
        //         WHERE log_penimbangan_rm_id = :wo and urutan = :urutan;
        //     ",
        //     [':wo'=> $wo,
        //      ':log'=> $log,
        //      ':urutan'=>count($split)
        //     ]);
        //     $result = $command->queryAll();
        // }

        if ($result){
            echo 1;
        } else {
            echo 0;
        }

    }

    /**
     * Displays a single LogPenimbanganRm model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Stop Jadwal Kemas 2 Inline Function
     * @param $snfg
     * @return mixed
     */
    public function actionInputMassa($id,$wo,$log)
    {
        $model = new LogPenimbanganRm();

        // Populate Grid Data
        $searchModel = new LogPenimbanganRmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 5];

        $sql="select * from log_penimbangan_rm where id=".$wo;
        $check= LogPenimbanganRm::findBySql($sql)->one();


        $this->layout = '//main-sidebar-collapse';

        return $this->render('input-massa', [
            'satuan' => $check->satuan,
            'id' => $id,
            'wo' => $wo,
            'log'=>$log,
            'kode_bb' => $check->kode_bb,
            'nama_bb' => $check->nama_bb,
            'weight' => $check->weight,
            'timbangan' => $check->timbangan,
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'kode_internal' => $check->kode_internal,
        ]);
    }

    /**
     * Stop Jadwal Kemas 2 Inline Function
     * @param $snfg
     * @return mixed
     */
    public function actionStopPenimbangan($id,$wo)
    {
        $model = new LogPenimbanganRm();

        // Populate Grid Data
        $searchModel = new LogPenimbanganRmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");
        $status = "Selesai Ditimbang";

        $this->layout = '//main-sidebar-collapse';

        // Check If Post Value True
        if ($model->load(Yii::$app->request->post())){

            // Get Posted Value
                $jumlah_realisasi=Yii::$app->request->post('LogPenimbanganRm')['realisasi'];
                if(empty($jumlah_realisasi)){
                    $jumlah_realisasi=null;
                }

                $satuan_realisasi=Yii::$app->request->post('LogPenimbanganRm')['satuan_realisasi'];

            // // Insert Is Done Value to status_jadwal
            // if(!empty($is_done)){

            //     $connection = Yii::$app->getDb();
            //     $command = $connection->createCommand("

            //     INSERT INTO status_jadwal (nojadwal,status,table_name,posisi,jenis_proses)
            //     SELECT
            //         :snfg as nojadwal,
            //         1 as status,
            //         'flow_input_snfg' as table_name,
            //         'KEMAS_2' as posisi,
            //         :jenis_proses as jenis_proses
            //     ON CONFLICT (nojadwal,posisi,jenis_proses)
            //     DO NOTHING;
            //     ",
            //     [':snfg' => $snfg, ':jenis_proses' => $jenis_proses]);

            //     $result = $command->queryAll();
            // }


            // Stop Jadwal
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE log_penimbangan_rm
            SET datetime_stop = :current_time,
                datetime_write = :current_time,
                realisasi = :jumlah_realisasi,
                satuan_realisasi = :satuan_realisasi,
                status = :status
            WHERE id = :wo;
            ",
            [':current_time' => date('Y-m-d h:i A'),
             ':wo'=> $wo,
             ':jumlah_realisasi'=>$jumlah_realisasi,
             ':satuan_realisasi'=>$satuan_realisasi,
             ':status'=>$status
            ]);

            // $model->save();

            $result = $command->queryAll();

            // echo "<script>window.alert(";
            // echo $result;
            // echo ");</script>";

            // Redirect to Downtime
            return $this->redirect(['task', 'id' => $id]);
            // return $this->redirect(['create-kemas1', 'snfg_komponen' => $snfg_komponen]);
             // return $this->redirect(['check-kemas1']);
        } else {
            return $this->render('input-massa', [
                'model' => $model,
                'id' => $id,
                'wo' => $wo,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,

            ]);
        }
    }

    /**
     * Stop Jadwal Kemas 2 Inline Function
     * @param $snfg
     * @return mixed
     */
    public function actionUpdateLog($id,$wo,$realisasi,$log,$log_realisasi)
    {
        $model = new LogPenimbanganRm();

        // Populate Grid Data
        $searchModel = new LogPenimbanganRmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");
        $status = "Selesai Ditimbang";

        $fim = FlowInputMo::find()->where(['id'=>$id])->one();

        $this->layout = '//main-sidebar-collapse';

        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $sql8="SELECT kode_bb,nama_bb,satuan,nobatch,is_split,kode_internal,is_repack,weight,timbangan
                FROM log_penimbangan_rm 
                WHERE id=".$wo;
        $check= LogPenimbanganRm::findBySql($sql8)->one();

        $sql9="SELECT nomo,id,nama_line
                FROM flow_input_mo 
                WHERE id=".$id;
        $fim= FlowInputMo::findBySql($sql9)->one();

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            *
        FROM device_map_rm
        WHERE device_ip = '".$ip."' and nama_line = '".$fim->nama_line."'";

        $map_device_array = DeviceMapRm::findBySql($sql)->one();

        // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
        $operator = $map_device_array['operator'];

        $sql0="SELECT nama_operator 
                FROM log_penimbangan_rm_operator 
                WHERE flow_input_mo_id=".$id." and operator=".$operator." order by id desc";
        $nama_op= LogPenimbanganRmOperator::findBySql($sql0)->one();

        $data_timbangan_sql="
            SELECT galat, kode_timbangan, angka_belakang_koma
            FROM master_data_timbangan_rm
            WHERE kode_timbangan = '".$check->timbangan."'
            ";

        $data_timbangan = MasterDataTimbanganRm::findBySql($data_timbangan_sql)->one();
         // $galat = $data_timbangan->galat;

        if ($check->is_repack == 1){
            $master_repack = Yii::$app->db->createCommand("SELECT kode_internal, galat FROM master_bahan_baku_repack WHERE kode_internal = '".$check->kode_internal."'")->queryOne();
            $galat = $master_repack['galat'];
            if ($fim->siklus == -1){
                $status = 'Cek Timbang';
            }
            // $status = 'Cek Timbang';
        }else{
            $galat = $data_timbangan['galat'];
        }


        
        if (($check->weight-$galat)<=floatval($realisasi)&&floatval($realisasi)<=($check->weight+$galat)){

            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE log_penimbangan_rm
            SET datetime_stop = :current_time,
                datetime_write = :current_time,
                realisasi = :jumlah_realisasi,
                status = :status,
                log_realisasi = :log_realisasi,
                nama_operator = :nama_operator
            WHERE id = :wo;
            ",
            [':current_time' => date('Y-m-d h:i A'),
             ':wo'=> $wo,
             ':jumlah_realisasi'=>$realisasi,
             ':status'=>$status,
             ':log_realisasi'=>$log_realisasi,
             ':nama_operator' => $nama_op->nama_operator
            ]);
            $result = $command->queryAll();

            if ($check->is_split == 1){
                $sql="SELECT id,urutan,realisasi FROM log_penimbangan_rm_split WHERE log_penimbangan_rm_id=".$wo."order by id desc";
                $split_items= LogPenimbanganRmSplit::findBySql($sql)->all();

                if (empty($split_items)){
                    $urutan = 0;
                    $qty = $realisasi;
                } else {
                    $urutan = $split_items[0]['urutan'];
                    $realisasi_split = 0; 
                    foreach ($split_items as $item){
                        $realisasi_split += $item['realisasi']; 
                    }
                    $qty = $realisasi-$realisasi_split;
                }

                if ($qty>0){
                    $connection2 = Yii::$app->getDb();
                    $command2 = $connection2->createCommand("
                        INSERT INTO log_penimbangan_rm_split (log_penimbangan_rm_id,kode_bb,no_batch,realisasi,satuan_realisasi,urutan, flow_input_mo_id, nama_bb, kode_internal, log,log_realisasi)
                        SELECT :wo as log_penimbangan_rm_id, :kode_bb as kode_bb, :no_batch as no_batch, :realisasi as realisasi, :satuan as satuan_realisasi, :urutan as urutan, :flow_input_mo_id as flow_input_mo_id, :nama_bb as nama_bb, :kode_internal as kode_internal, :log as log, :log_realisasi as log_realisasi
                        ",
                        [':wo' => $wo,
                         ':realisasi' => $realisasi,
                         ':kode_bb' => $check->kode_bb,
                         ':no_batch' => $check->nobatch,
                         ':realisasi' => $qty,
                         ':satuan' => $check->satuan,
                         ':urutan' => $urutan+1,
                         ':flow_input_mo_id' => $id,
                         ':nama_bb' => $check->nama_bb,
                         ':kode_internal' => $check->kode_internal,
                         ':log' => $log,
                         ':log_realisasi' => $log_realisasi
                        ]
                    );

                    $result2 = $command2->queryAll();
                }

                $qtySplitSql ="
                SELECT count(no_batch)
                FROM log_penimbangan_rm_split
                WHERE log_penimbangan_rm_id = ".$wo."
                ";

                $qtySplit = LogPenimbanganRmSplit::findBySql($qtySplitSql)->scalar();

                $batchSplitSql ="
                SELECT no_batch
                FROM log_penimbangan_rm_split
                WHERE log_penimbangan_rm_id = ".$wo." ORDER BY id ASC
                ";

                $batchSplit = LogPenimbanganRmSplit::findBySql($batchSplitSql)->all();


                if (strpos($batchSplit[0]['no_batch'], 'GBM') !== false) {
                    $no_batch_all = "GBMJTK6/";
                    for ($x = 0; $x < $qtySplit; $x++) {
                        $no_batch_all = $no_batch_all.''.substr($batchSplit[$x]['no_batch'], strpos($batchSplit[$x]['no_batch'], "/") + 1);
                        if ($x < ($qtySplit-1)){
                            $no_batch_all = $no_batch_all.'+';
                        }
                    }
                } else {
                    $no_batch_all = "";
                    for ($x = 0; $x < $qtySplit; $x++) {
                        $no_batch_all = $no_batch_all.''.$batchSplit[$x]['no_batch'];
                        if ($x < ($qtySplit-1)){
                            $no_batch_all = $no_batch_all.'+';
                        }
                    }
                }
            } else {
                $no_batch_all = $check->nobatch;
            }

            // Populate SNFG Komponen to Pengolahan Batch
            $connection3 = Yii::$app->getDb();
            $command3 = $connection3->createCommand("
                INSERT INTO log_penimbangan_rm_print_queue (flow_input_mo_id,nomo,kode_bb,nama_bb,weight,timbangan,nama_operator,satuan,no_batch,kode_olah,id_bb)
                SELECT :id as flow_input_mo_id, log.nomo as nomo, log.kode_bb as kode_bb, log.nama_bb as nama_bb, :realisasi as weight, :timbangan as timbangan, :nama_operator as nama_operator, :satuan as satuan, :no_batch as no_batch, log.kode_olah as kode_olah, log.id_bb as id_bb
                FROM log_penimbangan_rm log
                JOIN flow_input_mo flow ON flow.id = :id AND log.nomo = flow.nomo AND log.id = :wo;
                ",
                [':id'=> $id,
                 ':realisasi' => $realisasi,
                 ':timbangan' => $check->timbangan,
                 ':nama_operator' => $nama_op->nama_operator,
                 ':satuan' => $check->satuan,
                 ':wo' => $wo,
                 ':no_batch' => $no_batch_all
                ]
            );

            $result3 = $command3->queryAll();

            if ($result){
            // Redirect to Downtime
                $value = array(
                "status"=>"success");
                // return $this->redirect(['create-kemas1', 'snfg_komponen' => $snfg_komponen]);
                 // return $this->redirect(['check-kemas1']);
            } else {
                $value = array(
                "status"=>"not-success");
            }
        } else {
            $value = array(
            "status"=>"wrong-mass");
        } 


        echo Json::encode($value);

    }

    /**
     * Stop Jadwal Kemas 2 Inline Function
     * @param $snfg
     * @return mixed
     */
    public function actionStopJadwalPenimbangan($id)
    {
        $model = new LogPenimbanganRm();

        // Populate Grid Data
        $searchModel = new LogPenimbanganRmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");
        $status = "Selesai Ditimbang";

        // $this->layout = '//main-sidebar-collapse';

        // $nomo = LogPenimbanganRm::find()->where("id = ".$id." and nomo is not null")->one()['nomo'];
        $model_olah = FlowInputMo::find()->where(['id'=>$id])->one();
        // print_r($nomo);
        // print_r($jenis_penimbangan);
        $is_done = 0;
        // Check Belum Ditimbang item
        $logRM = LogPenimbanganRm::find()->where("nomo = '".$model_olah['nomo']."' and status in ('Belum Ditimbang','Cek Timbang') and kode_bb != 'AIR-RO--L' ")->all();
        if (empty($logRM)){

            $connection1 = Yii::$app->getDb();

            $command1 = $connection1->createCommand("

            INSERT INTO status_jadwal (nojadwal,status,table_name,posisi,jenis_proses)
            SELECT
                :nomo as nojadwal,
                1 as status,
                'flow_input_mo' as table_name,
                'PENIMBANGAN' as posisi,
                :jenis_proses as jenis_proses
            ON CONFLICT (nojadwal,posisi,jenis_proses)
            DO NOTHING;
            ",
            [':nomo' => $model_olah['nomo'],':jenis_proses' => $model_olah['jenis_penimbangan']]);

            $result1 = $command1->execute();            
            $is_done = 1;
            $this->actionInsertLogFroMrp($model_olah['nomo'],$id);
        }


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

        UPDATE flow_input_mo
        SET datetime_stop = :current_time,
            datetime_write = :current_time
        WHERE id = :id;
        ",
        [':current_time' => date('Y-m-d h:i A'),
         ':id'=> $id,
        ]);

        // $model->save();

        $result = $command->queryAll();

        $this->actionAutoOffSplitLine($model_olah['nomo']);

        if ($result){
        // Redirect to Downtime
            $value = array(
                "status"=>"success", 'is_done'=>$is_done);
            // return $this->redirect(['create-kemas1', 'snfg_komponen' => $snfg_komponen]);
             // return $this->redirect(['check-kemas1']);
        } else {
            $value = array(
            "status"=>"not-success");
        }

        return Json::encode($value);
    }

    public function actionAutoOffSplitLine($nomo)
    {
        $sql = "SELECT count(id) FROM flow_input_mo WHERE nomo = :nomo AND is_split_line = 1 and datetime_stop is null";
        $line_split_active = Yii::$app->db->createCommand($sql,[':nomo'=>$nomo])->queryScalar();
        if($line_split_active <= 1){
            $sql_update = "UPDATE flow_input_mo SET is_split_line = 0 WHERE nomo = :nomo";
            $update = Yii::$app->db->createCommand($sql_update,[':nomo'=>$nomo])->execute();
        }
    }

    /**
     * Stop Jadwal Kemas 2 Inline Function
     * @param $snfg
     * @return mixed
     */
    public function actionGotoBr($id,$siklus,$nama_line)
    {

        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            *
        FROM device_map_rm
        WHERE device_ip = :ip and nama_line = :nama_line";

        $map_device_array = DeviceMapRm::findBySql($sql,[':ip'=>$ip,':nama_line'=>$nama_line])->one();

        // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
        $operator = $map_device_array->operator;

        if ($siklus == 0){
            $isDonesql ="
                SELECT count(a.status)
                FROM log_penimbangan_rm a
                JOIN  flow_input_mo b
                ON b.id = :id AND a.nomo = b.nomo AND a.status = 'Belum Ditimbang' AND a.kode_bb != 'AIR-RO--L'
                ";
            
            $isDone = LogPenimbanganRm::findBySql($isDonesql,['id'=>$id])->scalar();


        }else{

            $isDonesql ="
                SELECT count(a.status)
                FROM log_penimbangan_rm a
                JOIN  flow_input_mo b
                ON b.id = :id AND a.nomo = b.nomo AND a.status = 'Belum Ditimbang' AND a.siklus = :siklus AND a.kode_bb != 'AIR-RO--L'
                ";

            $isDone = LogPenimbanganRm::findBySql($isDonesql,['id'=>$id,'siklus'=>$siklus])->scalar();

        }


        if (($isDone==0) && ($operator == 1)){
            $value = array(
                "status"=>"done");
        } else {
            $value = array(
                "status"=>"not-done");
        }

        echo Json::encode($value);
    }

    /**
     * Stop Jadwal Kemas 2 Inline Function
     * @param $snfg
     * @return mixed
     */
    public function actionFinishTimbang($id,$nama_line)
    {

        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            *
        FROM device_map_rm
        WHERE device_ip = :ip and nama_line = :nama_line";

        $map_device_array = DeviceMapRm::findBySql($sql,[':ip'=>$ip,':nama_line'=>$nama_line])->one();

        // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
        $operator = $map_device_array->operator;

        $fim = FlowInputMo::find()->where(['id'=>$id])->one();
        $siklus = $fim->siklus;
        $nomo = $fim->nomo;

        if ($siklus==0){
            $isDonesql ="
                SELECT count(a.status)
                FROM log_penimbangan_rm a
                JOIN  flow_input_mo b
                ON a.nomo = b.nomo WHERE b.id = '".$id."' and a.nama_line = '".$nama_line."' AND a.status in ('Belum Ditimbang','Cek Timbang') AND kode_bb != 'AIR-RO--L'
                ";
        //Jika Repack
        }else if ($siklus == -1){
            $isDonesql ="
                SELECT count(a.status)
                FROM log_penimbangan_rm a
                JOIN  flow_input_mo b
                ON a.nomo = b.nomo WHERE b.id = '".$id."' AND a.status in ('Belum Ditimbang') AND is_repack = 1 AND kode_bb != 'AIR-RO--L'
                ";
        }else{
            $isDonesql ="
                SELECT count(a.status)
                FROM log_penimbangan_rm a
                JOIN  flow_input_mo b
                ON a.nomo = b.nomo WHERE b.id = '".$id."' and a.nama_line = '".$nama_line."' AND a.status in ('Belum Ditimbang','Cek Timbang') AND a.siklus = '".$siklus."' AND kode_bb != 'AIR-RO--L'
                ";
        }

        $isDone = LogPenimbanganRm::findBySql($isDonesql)->scalar();

        if (($isDone==0) && ($operator == 1)){
            $status_ = $this->actionInsertLogFroMrp($nomo,$id);
            
            if ($status_ == 'ready'){
                $this->actionInsertLogFroMrpDetail($nomo);
            }

            $value = array(
                "status"=>"done");
            // exit();
        } else {
            $value = array(
                "status"=>"not-done");
        }

        echo Json::encode($value);
    }

    public function actionInsertLogFroMrpDetail($nomo)
    {
        $sql = " SELECT data.nomo,
                    dcd.no_smb,
                    data.kode_bb,
                    data.kode_internal,
                    data2.realisasi,
                    data.satuan_realisasi,
                    data.nobatch,
                    data.log,
                    data.is_split
                   FROM ( SELECT DISTINCT ON (a.nomo, a.kode_bb, a.satuan_realisasi, a.nobatch) a.nomo,
                            a.no_smb,
                            a.kode_bb,
                            a.kode_internal,
                            a.satuan_realisasi,
                            a.nobatch,
                            a.log,
                            a.is_split
                           FROM ( SELECT lpr.nomo,
                                    lpr.no_smb,
                                    lprs.kode_bb,
                                    lprs.kode_internal,
                                    'kg'::text AS satuan_realisasi,
                                    lprs.no_batch AS nobatch,
                                    lprs.log,
                                    1 AS is_split
                                   FROM log_penimbangan_rm_split lprs
                                     JOIN log_penimbangan_rm lpr ON lprs.log_penimbangan_rm_id = lpr.id
                                UNION ALL
                                 SELECT log_penimbangan_rm.nomo,
                                    log_penimbangan_rm.no_smb,
                                    log_penimbangan_rm.kode_bb,
                                    log_penimbangan_rm.kode_internal,
                                    'kg'::text AS satuan_realisasi,
                                    log_penimbangan_rm.nobatch,
                                    log_penimbangan_rm.log,
                                    0 AS is_split
                                   FROM log_penimbangan_rm
                                  WHERE log_penimbangan_rm.is_split = 0) a) data
                     LEFT JOIN ( SELECT DISTINCT ON (daily_consumption_default.name) daily_consumption_default.name,
                            daily_consumption_default.no_smb
                           FROM daily_consumption_default) dcd ON dcd.name::text = data.nomo::text
                     LEFT JOIN ( SELECT b.nomo,
                            b.no_smb,
                            b.kode_bb,
                            sum(b.realisasi) AS realisasi,
                            b.satuan_realisasi,
                            b.nobatch
                           FROM ( SELECT lpr.nomo,
                                    lpr.no_smb,
                                    lprs.kode_bb,
                                        CASE
                                            WHEN lprs.satuan_realisasi::text = 'g'::text THEN round(lprs.realisasi / 1000::numeric, 8)
                                            ELSE lprs.realisasi
                                        END AS realisasi,
                                    'kg'::text AS satuan_realisasi,
                                    lprs.no_batch AS nobatch
                                   FROM log_penimbangan_rm_split lprs
                                     JOIN log_penimbangan_rm lpr ON lprs.log_penimbangan_rm_id = lpr.id
                                UNION ALL
                                 SELECT log_penimbangan_rm.nomo,
                                    log_penimbangan_rm.no_smb,
                                    log_penimbangan_rm.kode_bb,
                                        CASE
                                            WHEN log_penimbangan_rm.satuan_realisasi::text = 'g'::text THEN round(log_penimbangan_rm.realisasi / 1000::numeric, 8)
                                            ELSE log_penimbangan_rm.realisasi
                                        END AS realisasi,
                                    'kg'::text AS satuan_realisasi,
                                    log_penimbangan_rm.nobatch
                                   FROM log_penimbangan_rm
                                  WHERE log_penimbangan_rm.is_split = 0) b
                          GROUP BY b.nomo, b.no_smb, b.kode_bb, b.satuan_realisasi, b.nobatch) data2 ON data.nomo::text = data2.nomo::text AND data.kode_bb::text = data2.kode_bb::text AND data.satuan_realisasi = data2.satuan_realisasi AND data.nobatch::text = data2.nobatch::text
                  WHERE data.nomo::text = :nomo AND data.kode_bb::text <> 'AIR-RO--L'::text";
        
        // $sql = "SELECT realisasi,satuan_realisasi,kode_internal,kode_bb,log,no_smb,nomo,nobatch FROM log_penimbangan_rm WHERE nomo = '".$nomo."' and kode_bb != 'AIR-RO--L' ";
        $rincian_bb = Yii::$app->db->createCommand($sql,[':nomo'=>$nomo])->queryAll();
        foreach ($rincian_bb as $rincian) {
            $connection2 = Yii::$app->getDb();
            $command2 = $connection2->createCommand("

            INSERT INTO log_fro_mrp_detail (nomo,no_smb,kode_bb,nobatch,realisasi,satuan_realisasi,log,is_split)
            SELECT
                :nomo as nomo,
                :no_smb as no_smb,
                :kode_bb as kode_bb,
                :nobatch as nobatch,
                :realisasi as realisasi,
                :satuan_realisasi as satuan_realisasi,
                :log as log,
                :is_split as is_split
            ;
            ",
            [
                ':nomo' => $rincian['nomo'],
                ':no_smb' => $rincian['no_smb'],
                ':kode_bb' => $rincian['kode_bb'],
                ':nobatch' => $rincian['nobatch'],
                ':realisasi' => $rincian['realisasi'],
                ':satuan_realisasi' => $rincian['satuan_realisasi'],
                ':log' => $rincian['log'],
                ':is_split' => $rincian['is_split']
            ]);

            $result2 = $command2->queryAll();
        //     print_r('<pre>');
        //     print_r($rincian);
        }

    }

    public function actionInsertLogFroMrp($nomo,$id,$is_done = false){

        date_default_timezone_set('Asia/Jakarta');
        $current_timestamp = date('Y-m-d H:i:s');
        // print_r($current_timestamp);
        $search_last_status = Yii::$app->db->createCommand("
                                SELECT * FROM log_fro_mrp 
                                WHERE nomo = :nomo",
                                [':nomo'=>$nomo])->queryOne();

        if (!empty($search_last_status)){
            $last_status = $search_last_status['status'];

            //IF status not-ready, recheck if all raw material has status 'Sudah Ditimbang'
            if ($last_status == 'not-ready'){
                $isDonesql ="
                    SELECT count(a.status)
                    FROM log_penimbangan_rm a
                    JOIN  flow_input_mo b
                    ON a.nomo = b.nomo 
                    WHERE b.id = '".$id."' AND a.status in ('Belum Ditimbang','Cek Timbang') AND kode_bb != 'AIR-RO--L'
                    ";
                $isDone = LogPenimbanganRm::findBySql($isDonesql)->scalar();
                if ($isDone==0 || $is_done){
                    $update = Yii::$app->db->createCommand("UPDATE log_fro_mrp SET status = :status, last_updated=:last_updated WHERE nomo = :nomo",
                                                            [':nomo'=>$nomo,
                                                            ':status'=>'ready',
                                                            'last_updated'=>$current_timestamp
                                                            ])->execute();

                    $status = 'ready';
                }else{
                    $status = 'not-ready';
                }
            }else{
                $status = null;
            }
        }else{
            //Data in log_fro_mrp is not exist, Insert data to log

            //Check if all raw material has status 'Sudah Ditimbang'
            $isDonesql ="
                SELECT count(a.status)
                FROM log_penimbangan_rm a
                JOIN  flow_input_mo b
                ON a.nomo = b.nomo WHERE b.id = '".$id."' AND a.status in ('Belum Ditimbang','Cek Timbang') AND kode_bb != 'AIR-RO--L'
                ";
            $isDone = LogPenimbanganRm::findBySql($isDonesql)->scalar();
            if ($isDone==0 || $is_done){
                //Insert data with status 'ready'
                $insert = Yii::$app->db->createCommand("INSERT INTO log_fro_mrp (nomo,status,last_updated) values (:nomo,:status,:last_updated)",
                                                        [
                                                            ':nomo'=>$nomo,
                                                            ':status'=>'ready',
                                                            ':last_updated'=>$current_timestamp
                                                        ])->execute();
            
                $status = 'ready';

            }else{
                //Insert data with status 'not-ready'
                $insert = Yii::$app->db->createCommand("INSERT INTO log_fro_mrp (nomo,status,last_updated) values (:nomo,:status,:last_updated)",
                                                        [
                                                            ':nomo'=>$nomo,
                                                            ':status'=>'not-ready',
                                                            ':last_updated'=>$current_timestamp
                                                        ])->execute();
                $status = 'not-ready';
            }
            
        }
        return $status;
            
    }

    public function actionSendDataToOdoo($nomo)
    {
        // $sql = "SELECT lpr.no_smb,lprs.kode_bb,lprs.kode_internal,lprs.realisasi,lprs.satuan_realisasi,lprs.no_batch as nobatch,lprs.log, 1 as is_split 
        //         from log_penimbangan_rm_split lprs
        //         join log_penimbangan_rm lpr on lprs.log_penimbangan_rm_id = lpr.id
        //         where lpr.nomo = '".$nomo."'
        //         union
        //         select no_smb, kode_bb,kode_internal,realisasi,satuan_realisasi, nobatch, log, 0 as is_split from log_penimbangan_rm where nomo = '".$nomo."'
        //         and is_split = 0";
        $sql = " SELECT data.nomo,
                            data.no_smb,
                            data.kode_bb,
                            data.kode_internal,
                            data2.realisasi,
                            data.satuan_realisasi,
                            data.nobatch,
                            data.log,
                            data.is_split
                           FROM ( SELECT DISTINCT ON (a.nomo, a.kode_bb, a.nobatch) a.nomo,
                                    a.no_smb,
                                    a.kode_bb,
                                    a.kode_internal,
                                    a.realisasi,
                                    a.satuan_realisasi,
                                    a.nobatch,
                                    a.log,
                                    a.is_split
                                   FROM ( SELECT lpr.nomo,
                                            lpr.no_smb,
                                            lprs.kode_bb,
                                            lprs.kode_internal,
                                            lprs.realisasi,
                                            lprs.satuan_realisasi,
                                            lprs.no_batch AS nobatch,
                                            lprs.log,
                                            1 AS is_split
                                           FROM log_penimbangan_rm_split lprs
                                             JOIN log_penimbangan_rm lpr ON lprs.log_penimbangan_rm_id = lpr.id
                                        UNION ALL
                                         SELECT log_penimbangan_rm.nomo,
                                            log_penimbangan_rm.no_smb,
                                            log_penimbangan_rm.kode_bb,
                                            log_penimbangan_rm.kode_internal,
                                            log_penimbangan_rm.realisasi,
                                            log_penimbangan_rm.satuan_realisasi,
                                            log_penimbangan_rm.nobatch,
                                            log_penimbangan_rm.log,
                                            0 AS is_split
                                           FROM log_penimbangan_rm
                                          WHERE log_penimbangan_rm.is_split = 0) a) data
                             LEFT JOIN ( SELECT b.nomo,
                                    b.no_smb,
                                    b.kode_bb,
                                    sum(b.realisasi) AS realisasi,
                                    b.satuan_realisasi,
                                    b.nobatch
                                   FROM ( SELECT lpr.nomo,
                                            lpr.no_smb,
                                            lprs.kode_bb,
                                            lprs.realisasi,
                                            lprs.satuan_realisasi,
                                            lprs.no_batch AS nobatch
                                           FROM log_penimbangan_rm_split lprs
                                             JOIN log_penimbangan_rm lpr ON lprs.log_penimbangan_rm_id = lpr.id
                                        UNION ALL
                                         SELECT log_penimbangan_rm.nomo,
                                            log_penimbangan_rm.no_smb,
                                            log_penimbangan_rm.kode_bb,
                                            log_penimbangan_rm.realisasi,
                                            log_penimbangan_rm.satuan_realisasi,
                                            log_penimbangan_rm.nobatch
                                           FROM log_penimbangan_rm
                                          WHERE log_penimbangan_rm.is_split = 0) b
                                  GROUP BY b.nomo, b.no_smb, b.kode_bb, b.satuan_realisasi, b.nobatch) data2 ON data.nomo::text = data2.nomo::text AND data.kode_bb::text = data2.kode_bb::text AND data.nobatch::text = data2.nobatch::text
                          WHERE data.nomo::text = :nomo and data.kode_bb != 'AIR-RO--L'";
        // $sql = "SELECT realisasi,satuan_realisasi,kode_internal,kode_bb,log,no_smb,nomo,nobatch FROM log_penimbangan_rm WHERE nomo = '".$nomo."' and kode_bb != 'AIR-RO--L' ";
        $rincian_bb = Yii::$app->db->createCommand($sql,[':nomo'=>$nomo])->queryAll();

        // print_r('<pre>');
        // print_r($rincian_bb);
        // exit();
        $item_json = json_encode($rincian_bb);
        $create_json_file = file_put_contents("update_stock_ndc/data_rmw.json", $item_json);
        if ($create_json_file)
        {
            // echo "JSON file created successfully...";
            require (Yii::getAlias('@webroot').'/update_stock_ndc/mrp_fro.php');
        }else{
            // echo "Oops! Error creating json file...";
            // return $this->redirect(['qc-fg-v2/halaman-terima-ndc', 'e'=>'json-fail']);
        }
    }

    public function actionFinishTimbangRepack($id,$nama_line)
    {

        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            *
        FROM device_map_rm
        WHERE device_ip = :ip and nama_line = :nama_line";

        $map_device_array = DeviceMapRm::findBySql($sql,[':ip'=>$ip,':nama_line'=>$nama_line])->one();


        // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
        $operator = $map_device_array->operator;

        $fim = FlowInputMo::find()->where(['id'=>$id])->one();
        $siklus = $fim->siklus;
        $nomo = $fim->nomo;

        $isDonesql ="
                SELECT count(a.status)
                FROM log_penimbangan_rm a
                JOIN  flow_input_mo b
                ON b.id = '".$id."' AND a.nomo = b.nomo AND a.status = 'Belum Ditimbang' AND is_repack = 1 AND kode_bb != 'AIR-RO--L'
                ";

        $isDone = LogPenimbanganRm::findBySql($isDonesql)->scalar();

        if (($isDone==0) && ($operator == 1)){
            $status_ = $this->actionInsertLogFroMrp($nomo,$id);

            if ($status_ == 'ready'){
                $this->actionInsertLogFroMrpDetail($nomo);
            }

            $value = array(
                "status"=>"done");
        } else {
            $value = array(
                "status"=>"not-done");
        }

        echo Json::encode($value);
    }

    public function actionFinishTimbangRo($id)
    {

        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            *
        FROM device_map_rm
        WHERE device_ip = '".$ip."'";

        $map_device_array = DeviceMapRm::findBySql($sql)->one();

        // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
        $operator = $map_device_array->operator;

        $siklus = FlowInputMo::find()->where(['id'=>$id])->one()['siklus'];

        $isDoneROsql = "
                SELECT count(a.status)
                FROM log_penimbangan_rm a
                JOIN  flow_input_mo b
                ON b.id = '".$id."' AND a.nomo = b.nomo AND a.status = 'Belum Ditimbang' AND a.kode_bb = 'AIR-RO--L'
                ";


        $isDoneRO = LogPenimbanganRm::findBySql($isDoneROsql)->scalar();

        if ($isDoneRO==0 && $operator == 11){
            $value = array(
                "status"=>"done");
        } else {
            $value = array(
                "status"=>"not-done");
        }

        echo Json::encode($value);
    }

    public function actionListVerifiedItems($nomo)
    {
        $sql = "SELECT kode_bb,
                        kode_internal,
                        nama_bb,
                        weight,
                        satuan,
                        siklus,
                        kode_olah,
                        operator,
                        timbangan
                FROM log_penimbangan_rm WHERE nomo = '".$nomo."'
                order by satuan desc, weight asc";
        $data = LogPenimbanganRm::findBySql($sql)->all();
        $dataProvider= new ArrayDataProvider([
            'allModels'=>$data,
            'pagination'=>false,
        ]);
        return $this->render('list-verified-items',[
            'dataProvider'=>$dataProvider,
        ]);
        // print_r ($data);
    }

    /**
     * Creates a new LogPenimbanganRm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LogPenimbanganRm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LogPenimbanganRm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LogPenimbanganRm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LogPenimbanganRm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogPenimbanganRm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogPenimbanganRm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * actionInsertSwo
     * Triggers from view weigher-fg
     * Triggered when Operator changes the Operator Name by clicking the Trigger Button
     * Logs Operator Changes (Historical)
     * @param integer $id
     * @param string $nama_operator
     * @return void previous page
     */
    public function actionInsertSelectedOperator($id,$nama_line,$nama_operator)
    {
        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            *
        FROM device_map_rm
        WHERE device_ip = '".$ip."' and nama_line = '".$nama_line."' ";

        $map_device_array = DeviceMapRm::findBySql($sql)->one();

        // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
        $operator = $map_device_array->operator;

        $isAlreadySelected = LogPenimbanganRmOperator::checkSelectedOperator($id,$nama_operator,$operator);

        //selected in same device = -1, delete operator name
        //selected in other device = -2, block
        //not selected in any device = 0, insert operator name
        //update operator name = 1, update operator name
        //max operator reached = 2, block

        if ($isAlreadySelected == -2){
            echo "<script>window.alert('Operator sudah terpilih di perangkat lain!');";
            echo "window.location = 'index.php?r=log-penimbangan-rm/task&id="; echo $id; echo "';";
            echo "</script>";
        } else if ($isAlreadySelected == 2){
            echo "<script>window.alert('Tidak bisa memilih operator lebih dari 2 orang di Tab yang sama!');";
            echo "window.location = 'index.php?r=log-penimbangan-rm/task&id="; echo $id; echo "';";
            echo "</script>";

        } else if ($isAlreadySelected == -1){

            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
            SELECT * FROM log_penimbangan_rm_operator 
            WHERE flow_input_mo_id = :flow_input_mo_id AND operator = :operator ORDER BY id desc
            ",
            [
                ':flow_input_mo_id' => $id,
                ':operator' => $operator
            ]);
            $prev_operator = $command->queryOne();

            $list_prev_operator_name = explode(',',$prev_operator['nama_operator']);

            if (count($list_prev_operator_name) > 1){
                $nama_op1 = $list_prev_operator_name[0];
                $nama_op2 = $list_prev_operator_name[1];

                if ($nama_op1 == $nama_operator){
                    $new_operator_name = $nama_op2;
                }else{
                    $new_operator_name = $nama_op1;
                }
            }else{
                $nama_op1 = $list_prev_operator_name[0];
                $nama_op2 = null;

                $new_operator_name = $nama_op2;

            }

            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            INSERT INTO log_penimbangan_rm_operator (flow_input_mo_id,nama_operator,operator)
            SELECT
                :id as flow_input_mo_id,
                :nama_operator as nama_operator,
                :operator as operator
            ;
            ",
            [
                ':id' => $id,
                ':nama_operator' => $new_operator_name,
                ':operator' => $operator
            ]);

            $result = $command->queryAll();

            if($result){
                return $this->redirect(Yii::$app->request->referrer);
            }


        
        } else if ($isAlreadySelected == 1){

            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
            SELECT * FROM log_penimbangan_rm_operator 
            WHERE flow_input_mo_id = :flow_input_mo_id AND operator = :operator ORDER BY id desc
            ",
            [
                ':flow_input_mo_id' => $id,
                ':operator' => $operator
            ]);
            $prev_operator = $command->queryOne();

            $prev_operator_name = $prev_operator['nama_operator'];

            if(!empty($prev_operator_name)){
                $new_operator_name = $prev_operator_name.','.$nama_operator;
            }else{
                $new_operator_name = $nama_operator;
            }


            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            INSERT INTO log_penimbangan_rm_operator (flow_input_mo_id,nama_operator,operator)
            SELECT
                :id as flow_input_mo_id,
                :nama_operator as nama_operator,
                :operator as operator
            ;
            ",
            [
                ':id' => $id,
                ':nama_operator' => $new_operator_name,
                ':operator' => $operator
            ]);

            $result = $command->queryAll();

            if($result){
                return $this->redirect(Yii::$app->request->referrer);
            }


        } else if ($isAlreadySelected == 0){
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            INSERT INTO log_penimbangan_rm_operator (flow_input_mo_id,nama_operator,operator)
            SELECT
                :id as flow_input_mo_id,
                :nama_operator as nama_operator,
                :operator as operator
            ;
            ",
            [
                ':id' => $id,
                ':nama_operator' => $nama_operator,
                ':operator' => $operator
            ]);

            $result = $command->queryAll();

            if($result){
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        
        //Jika operator sama dengan yg sudah di pilih di tab yang sama, maka delete nama operator
        // } else if ($isAlreadySelected == 9){
        //     $sqll="
        //     SELECT
        //         *
        //     FROM log_penimbangan_rm_operator
        //     WHERE flow_input_mo_id = '".$id."' AND operator = ".$operator."
        //     ORDER BY id DESC";

        //     $array = LogPenimbanganRmOperator::findBySql($sqll)->one();

        //     $operator_ = explode(',',$array['nama_operator']);

        //     if (count($operator_)>1){
        //         $op1 = $operator_[0];
        //         $op2 = $operator_[1];

        //         if ($op1 == $nama_operator){
        //             // Hapus Operator
        //             $connection2 = Yii::$app->getDb();
        //             $command2 = $connection2->createCommand("

        //             INSERT INTO log_penimbangan_rm_operator (flow_input_mo_id,nama_operator,operator)
        //             SELECT
        //                 :id as flow_input_mo_id,
        //                 :nama_operator as nama_operator,
        //                 :operator as operator
        //             ;
        //             ",
        //             [
        //                 ':id' => $id,
        //                 ':nama_operator' => $op2,
        //                 ':operator' => $operator
        //             ]);
        //         } else  if ($op2 == $nama_operator){
        //             // Hapus Operator
        //             $connection2 = Yii::$app->getDb();
        //             $command2 = $connection2->createCommand("

        //             INSERT INTO log_penimbangan_rm_operator (flow_input_mo_id,nama_operator,operator)
        //             SELECT
        //                 :id as flow_input_mo_id,
        //                 :nama_operator as nama_operator,
        //                 :operator as operator
        //             ;
        //             ",
        //             [
        //                 ':id' => $id,
        //                 ':nama_operator' => $op1,
        //                 ':operator' => $operator
        //             ]);
        //         }


        //         $result2 = $command2->queryAll();

        //         if($result2){
        //             return $this->redirect(Yii::$app->request->referrer);
        //         }
            
        //     } else {
        //         //$op1 = $array->nama_operator;
        //         $connection = Yii::$app->getDb();
        //         $command = $connection->createCommand("

        //         INSERT INTO log_penimbangan_rm_operator (flow_input_mo_id,nama_operator,operator)
        //         SELECT
        //             :id as flow_input_mo_id,
        //             :nama_operator as nama_operator,
        //             :operator as operator
        //         ;
        //         ",
        //         [
        //             ':id' => $id,
        //             ':nama_operator' => NULL,
        //             ':operator' => $operator
        //         ]);

        //         $result = $command->queryAll();

        //         if($result){
        //             return $this->redirect(Yii::$app->request->referrer);
        //         }
        //     }
        // } else if ($isAlreadySelected == 2) {
        //     // Get Mapping Line - Frontend IP Array
        //     $sqll="
        //     SELECT
        //         *
        //     FROM log_penimbangan_rm_operator
        //     WHERE flow_input_mo_id = '".$id."' AND operator = ".$operator."
        //     ORDER BY id DESC";

        //     $array = LogPenimbanganRmOperator::findBySql($sqll)->one();
        //     $nama_operator_exist = $array->nama_operator;

        //     if (!empty($array) && ($array->nama_operator != NULL)){
        //         $connection = Yii::$app->getDb();
        //         $command = $connection->createCommand("

        //         INSERT INTO log_penimbangan_rm_operator (flow_input_mo_id,nama_operator,operator)
        //         SELECT
        //             :id as flow_input_mo_id,
        //             :nama_operator as nama_operator,
        //             :operator as operator
        //         ;
        //         ",
        //         [
        //             ':id' => $id,
        //             ':nama_operator' => $nama_operator.",".$nama_operator_exist,
        //             ':operator' => $operator
        //         ]);

        //         $result = $command->queryAll();

        //         if($result){
        //             return $this->redirect(Yii::$app->request->referrer);
        //         }
        //     } else {
        //         $connection = Yii::$app->getDb();
        //         $command = $connection->createCommand("

        //         INSERT INTO log_penimbangan_rm_operator (flow_input_mo_id,nama_operator,operator)
        //         SELECT
        //             :id as flow_input_mo_id,
        //             :nama_operator as nama_operator,
        //             :operator as operator
        //         ;
        //         ",
        //         [
        //             ':id' => $id,
        //             ':nama_operator' => $nama_operator,
        //             ':operator' => $operator
        //         ]);

        //         $result = $command->queryAll();

        //         if($result){
        //             return $this->redirect(Yii::$app->request->referrer);
        //         }
        //     }
        // } else {
        //     $connection = Yii::$app->getDb();
        //     $command = $connection->createCommand("

        //     INSERT INTO log_penimbangan_rm_operator (flow_input_mo_id,nama_operator,operator)
        //     SELECT
        //         :id as flow_input_mo_id,
        //         :nama_operator as nama_operator,
        //         :operator as operator
        //     ;
        //     ",
        //     [
        //         ':id' => $id,
        //         ':nama_operator' => $nama_operator,
        //         ':operator' => $operator
        //     ]);

        //     $result = $command->queryAll();

        //     if($result){
        //         return $this->redirect(Yii::$app->request->referrer);
        //     }
        // }
    }

    /**
     * Lists all LogPenimbanganRm models.
     * @return mixed
     */
    public function actionPrintDocument($id)
    {
        $model = new LogPenimbanganRm();

        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT
            *
        FROM device_map_rm
        WHERE device_ip = '".$ip."'";

        $map_device_array = DeviceMapRm::findBySql($sql)->one();

        // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
        if(empty($map_device_array)){
            $operator = 1;
        }else{
            $operator = $map_device_array->operator;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('print-document', [
                'model' => $model,
                'id' => $id,
                'operator' => $operator
            ]);
        }
    }

    public function actionPdf($id,$siklus) {
        // Your SQL query here

        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d');

        $sqlg = "
        SELECT *
        FROM flow_input_mo
        WHERE id = '".$id."'
        ";

        $flow_input_mo = FlowInputMo::findBySql($sqlg)->one();

        // $sql_ = "
        // SELECT kode_bulk,formula_reference,qty_batch
        // FROM log_approval_br
        // WHERE nomo = :nomo
        // ";

        // $approvalBr = LogApprovalBr::findBySql($sql_,[':nomo' => $flow_input_mo->nomo])->one();

        // print_r($flow_input_mo->nomo);
        // exit();

        // $sql_header = "
        // SELECT no_dokumen
        // FROM log_br_detail
        // WHERE kode_bulk = :kode_bulk and formula_reference = :formula_reference and qty_batch = :qty_batch
        // ";

        // $header_komponen = LogBrDetail::findBySql($sql_header,[
        //     ':kode_bulk' => $approvalBr->kode_bulk,
        //     ':formula_reference' => $approvalBr->formula_reference,
        //     ':qty_batch' => $approvalBr->qty_batch
        //     ])->all();        

        $sql = "
        SELECT *
        FROM scm_planner
        WHERE nomo = '".$flow_input_mo->nomo."'
        ";

        $planner = ScmPlanner::findBySql($sql)->one();


       
        // Get Operator Count
        // $sql5="
        //         SELECT count(*) as id
        //         FROM
        //         (SELECT
        //              unnest(string_to_array(nama_operator, ',')) as nama_operator
        //         FROM flow_input_mo
        //         WHERE id = ".$id.") a ";

        // $qtyOpr = FlowInputMo::findBySql($sql5)->one();

        if ($siklus==0){
            // GET LIST OF WEIGHER ALL siklus including repack liquid
            $weigherListSql = "
             SELECT a.no_timbangan,
                a.operator
               FROM ( SELECT DISTINCT no_timbangan,
                        operator
                       FROM log_penimbangan_rm
                      WHERE nomo = :nomo) a
              ORDER BY (
                    CASE
                        WHEN a.operator = 11 THEN 1
                        WHEN a.operator = 1 THEN 2
                        WHEN a.operator = 2 THEN 3
                        WHEN a.operator = 3 THEN 4
                        WHEN a.operator = 4 THEN 5
                        WHEN a.operator = 5 THEN 6
                        WHEN a.operator = 6 THEN 7
                        ELSE NULL::integer
                    END), a.no_timbangan";

            // $weigherListSql = "
            // SELECT distinct(no_timbangan)
            // FROM log_penimbangan_rm
            // WHERE nomo = :nomo ";
            $weigherList = LogPenimbanganRm::findBySql($weigherListSql,[':nomo'=>$flow_input_mo->nomo])->all();

            // GET LIST TASK
            $sql10 = "
            SELECT *
            FROM log_penimbangan_rm
            WHERE nomo = '".$flow_input_mo->nomo."'  
            ORDER BY siklus, (COALESCE(substring(kode_olah::text, '^(\d+)'::text)::integer, 99999999)), 
            (substring(kode_olah::text, '[a-zA-z_-]+'::text)), 
            (COALESCE(substring(kode_olah::text, '(\d+)$'::text)::integer, 0)),
            id
            ";

            $task = LogPenimbanganRm::findBySql($sql10)->all();
            $naitem_fg = $task[0]['naitem_fg'];

            $countTasks="
                SELECT COUNT(1)
                FROM log_penimbangan_rm
                WHERE nomo = '".$flow_input_mo->nomo."' 
                ";

            $taskQty = LogPenimbanganRm::findBySql($countTasks)->scalar();

            // GET LIST SPLIT TASK
            $sqlSplit = "
            SELECT *
            FROM log_penimbangan_rm_split
            WHERE flow_input_mo_id in (SELECT id FROM flow_input_mo WHERE nomo = :nomo and posisi = 'PENIMBANGAN') 
            ORDER BY id ASC
            ";

            $splitTask = LogPenimbanganRmSplit::findBySql($sqlSplit,[':nomo'=>$flow_input_mo->nomo])->all();

            $countSplitTasks="
                SELECT COUNT(1)
                FROM log_penimbangan_rm_split
                WHERE flow_input_mo_id in (SELECT id FROM flow_input_mo WHERE nomo = :nomo and posisi = 'PENIMBANGAN') 
                ";

            $splitTaskQty = LogPenimbanganRmSplit::findBySql($countSplitTasks,[':nomo'=>$flow_input_mo->nomo])->scalar();

            // Get List Operator Array
            $sql3="
                    SELECT DISTINCT ON (nama_operator) nama_operator FROM 
                    (
                        SELECT
                             unnest(string_to_array(nama_operator, ',')) as nama_operator
                        FROM log_penimbangan_rm
                        WHERE nomo = :nomo
                    )a ";

            // $list_operator = FlowInputMo::findBySql($sql3,[':nomo'=>$flow_input_mo->nomo])->all();
            
            $list_operator_arr = ArrayHelper::map(FlowInputMo::findBySql($sql3,[':nomo'=>$flow_input_mo->nomo])->all(), 'nama_operator','nama_operator');
            $list_operator = implode(',',$list_operator_arr);
        }else{
            // GET LIST OF WEIGHER
            $weigherListSql = "
             SELECT a.no_timbangan,
                a.operator
               FROM ( SELECT DISTINCT no_timbangan,
                        operator
                       FROM log_penimbangan_rm
                      WHERE nomo = :nomo AND siklus = :siklus) a
              ORDER BY (
                    CASE
                        WHEN a.operator = 11 THEN 1
                        WHEN a.operator = 1 THEN 2
                        WHEN a.operator = 2 THEN 3
                        WHEN a.operator = 3 THEN 4
                        WHEN a.operator = 4 THEN 5
                        WHEN a.operator = 5 THEN 6
                        WHEN a.operator = 6 THEN 7
                        ELSE NULL::integer
                    END), a.no_timbangan";
            $weigherList = LogPenimbanganRm::findBySql($weigherListSql,[':nomo'=>$flow_input_mo->nomo,':siklus'=>$siklus])->all();

            // GET LIST TASK
            $sql10 = "
            SELECT *
            FROM log_penimbangan_rm
            WHERE nomo = '".$flow_input_mo->nomo."' and siklus = ".$siklus."  
            ORDER BY siklus, (COALESCE(substring(kode_olah::text, '^(\d+)'::text)::integer, 99999999)), 
            (substring(kode_olah::text, '[a-zA-z_-]+'::text)), 
            (COALESCE(substring(kode_olah::text, '(\d+)$'::text)::integer, 0)),
            id
            ";

            $task = LogPenimbanganRm::findBySql($sql10)->all();
            $naitem_fg = $task[0]['naitem_fg'];

            $countTasks="
                SELECT COUNT(1)
                FROM log_penimbangan_rm
                WHERE nomo = :nomo and siklus = :siklus 
                ";

            $taskQty = LogPenimbanganRm::findBySql($countTasks,[':nomo'=>$flow_input_mo->nomo,':siklus'=>$siklus])->scalar();

            // GET LIST SPLIT TASK
            $sqlSplit = "
            SELECT *
            FROM log_penimbangan_rm_split
            WHERE log_penimbangan_rm_id in (SELECT id FROM log_penimbangan_rm WHERE nomo = :nomo AND siklus = :siklus) ORDER BY id ASC
            ";

            $splitTask = LogPenimbanganRmSplit::findBySql($sqlSplit,[':nomo'=>$flow_input_mo->nomo,':siklus'=>$siklus])->all();

            $countSplitTasks="
                SELECT COUNT(1)
                FROM log_penimbangan_rm_split
                WHERE log_penimbangan_rm_id in (SELECT id FROM log_penimbangan_rm WHERE nomo = :nomo AND siklus = :siklus)
                ";

            $splitTaskQty = LogPenimbanganRmSplit::findBySql($countSplitTasks,[':nomo'=>$flow_input_mo->nomo,':siklus'=>$siklus])->scalar();

            // Get List Operator Array
            $sql3="
                    SELECT DISTINCT ON (nama_operator) nama_operator FROM 
                    (
                        SELECT
                             unnest(string_to_array(nama_operator, ',')) as nama_operator
                        FROM log_penimbangan_rm
                        WHERE nomo = :nomo and siklus = :siklus
                    )a ";

            // $list_operator = FlowInputMo::findBySql($sql3,[':nomo'=>$flow_input_mo->nomo])->all();
            
            $list_operator_arr = ArrayHelper::map(FlowInputMo::findBySql($sql3,[':nomo'=>$flow_input_mo->nomo,':siklus'=>$siklus])->all(), 'nama_operator','nama_operator');
            $list_operator = implode(',',$list_operator_arr);
        }

        // print_r ($splitTaskQty);
        // exit();
        

        // if (strpos($planner->nama_fg, 'ardah') !== false) {
        //     $brand = "Wardah New Look";
        // } else if (strpos($planner->nama_fg, 'OVER') !== false) {
        //     $brand = "MAKE OVER";
        // } else if (strpos($planner->nama_fg, 'mina') !== false) {
        //     $brand = "Emina";
        // } else if (strpos($planner->nama_fg, 'utri') !== false) {
        //     $brand = "Putri";
        // } else {
        //     $brand = "";
        // }

        if(strpos(strtolower($planner->nama_fg), 'ardah') !== false){
            $brand = 'Wardah';
        } else if(strpos(strtolower($planner->nama_fg), 'make') !== false){
            $brand = 'Make Over';
        } else if(strpos(strtolower($planner->nama_fg), 'emina') !== false){
            $brand = 'Emina';
        } else if(strpos(strtolower($planner->nama_fg), 'kahf') !== false){
            $brand = 'Kahf';
        }  else if(strpos(strtolower($planner->nama_fg), 'omg') !== false){
            $brand = 'OMG';
        }  else if(strpos(strtolower($planner->nama_fg), 'putri') !== false){
            $brand = 'Putri';
        }  else if(strpos(strtolower($planner->nama_fg), 'biodef') !== false){
            $brand = 'Biodef';
        } else {
            $brand = '';
        }



        $content =  $this->renderPartial('batch-record', [
            'taskQty' => $taskQty,
            'task' => $task,
            'planner' => $planner,
            // 'header_komponen' => $header_komponen,
            'date' => $date,
            'flow_input_mo' => $flow_input_mo,
            'splitTask' => $splitTask,
            'splitTaskQty' => $splitTaskQty,
            'brand' => $brand,
            'weigherList' => $weigherList,
            'list_operator' => $list_operator,
            // 'qtyOpr' => $qtyOpr,
            'siklus' => $siklus
        ]);

        // $content = $this->renderPartial('report', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        //     'nalang' => $nalang,
        //     'tglrencanabayar' => $tglrencanabayar,
        //     'norek' => $norek,
        //     'nama_rekening' => $nama_rekening,
        //     'bank' => $bank,
        //     'bayar_via' => $bayar_via,
        // ]);

        // $content = $this->renderPartial('index', ['model' => $model]);

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_CORE,
        // A4 paper format
        'format' => Pdf::FORMAT_FOLIO,
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT,
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER,
        // your html content input
        'content' => $content,
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting
        'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
        'cssInline' => '.kv-heading-1{font-size:18px}',
        // set mPDF properties on the fly
        'options' => ['title' => 'BR Resume - Raw Material - '.$id],
        // call mPDF methods on the fly
        'methods' => [
        'SetHeader'=>['Penimbangan Raw Material - PT Paragon Technology & Innovation'],
        'SetFooter'=>['This Batch Record is computer generated, no signature required'],
        ]
        ]);

        /*------------------------------------*/
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        /*------------------------------------*/

        // return the pdf output as per the destination setting
        return $pdf->render();

        // return $content->render();
    }

    /**
     * Displays a single LogPenimbanganRm model.
     * @param integer $id
     * @return mixed
     */
    public function actionBatchRecord($id)
    {
        // GET LIST TASK
        $sql10 = "
        SELECT *
        FROM log_penimbangan_rm
        WHERE nomo = 'MO-FT-TBS-BULK/E0058' AND no_formula = 'BR.RND.EM0008.F03.01' AND no_revisi = '2' AND operator = '2'
        ";

        $task = LogPenimbanganRm::findBySql($sql10)->all();
        $naitem_fg = $task[0]['naitem_fg'];

        $countTasks="
            SELECT COUNT(1)
            FROM log_penimbangan_rm
            WHERE nomo = 'MO-FT-TBS-BULK/E0058' AND no_formula = 'BR.RND.EM0008.F03.01' AND no_revisi = '2' AND operator = '2'
            ";

        $taskQty = LogPenimbanganRm::findBySql($countTasks)->scalar();

        return $this->render('batch-record', [
            'model' => $this->findModel($id),
            'taskQty' => $taskQty,
            'task' => $task,
        ]);
    }
}
