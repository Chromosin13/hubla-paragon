<?php

namespace app\controllers;

use Yii;
use app\models\LogFormulaBreakdown;
use app\models\LogFormulaBreakdownSplit;
use app\models\MasterDataTimbanganRm;
use app\models\MasterDataTimbanganRmSearch;
use app\models\LogJadwalTimbangRm;
use app\models\ScmPlanner;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/** 
 * MasterDataTimbanganRmController implements the CRUD actions for MasterDataTimbanganRm model.
 */
class MasterDataTimbanganRmController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterDataTimbanganRm models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterDataTimbanganRmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterDataTimbanganRm model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterDataTimbanganRm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterDataTimbanganRm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MasterDataTimbanganRm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterDataTimbanganRm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterDataTimbanganRm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterDataTimbanganRm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterDataTimbanganRm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.'); 
        }
    }

    public function actionGetTimbangNomo($nomo)
    {
        $sql = "
                SELECT nama_line
                FROM log_jadwal_timbang_rm
                WHERE nomo = '".$nomo."'
                ORDER BY id desc
                ";

        $line_timbang_array = LogJadwalTimbangRm::findBySql($sql)->one();
        if ($line_timbang_array->nama_line == NULL)
        {
            $line_timbang = "Pilih Line";
        } else if (strpos($line_timbang_array->nama_line, '/') !== false){
            $line_timbang = "Pilih Line";
        } else {
            $line_timbang = $line_timbang_array->nama_line;
        }

        if ($line_timbang == "Pilih Line"){
            $sql = "
                    SELECT kode_timbangan
                    FROM master_data_timbangan_rm 
                    ORDER BY kode_timbangan ASC
                    ";

            $timbangans = MasterDataTimbanganRm::findBySql($sql)->all();
            
        } else {
            $sql = "
                    SELECT kode_timbangan
                    FROM master_data_timbangan_rm 
                    WHERE line='".$line_timbang."'
                    ORDER BY id ASC
                    ";

            $timbangans = MasterDataTimbanganRm::findBySql($sql)->all();
        }

        echo "<select class='form-control form-control-sm text-center' style='height:2em; text-align:center; float:left;'><option value='Pilih Timbangan'>Pilih Timbangan</options>";
        foreach($timbangans as $timbangan){
            echo "<option value='".$timbangan->kode_timbangan."'>".$timbangan->kode_timbangan."</options>";
        }
        echo "</select>";
    }

    public function actionGetTimbangKodeBulk($kode_bulk,$reference)
    {
        $sql = "
                SELECT nama_line
                FROM log_formula_breakdown
                WHERE kode_bulk = '".$kode_bulk."' AND reference = '".$reference."'
                ORDER BY id desc
                ";

        $line_timbang_array = LogJadwalTimbangRm::findBySql($sql)->one();
        if ($line_timbang_array->nama_line == NULL)
        {
            $line_timbang = "Pilih Line";
        } else if (strpos($line_timbang_array->nama_line, '/') !== false){
            $line_timbang = "Pilih Line";
        } else {
            $line_timbang = $line_timbang_array->nama_line;
        }

        if ($line_timbang == "Pilih Line"){
            $sql = "
                    SELECT kode_timbangan
                    FROM master_data_timbangan_rm 
                    ORDER BY kode_timbangan ASC
                    ";

            $timbangans = MasterDataTimbanganRm::findBySql($sql)->all();
            
        } else {
            $sql = "
                    SELECT kode_timbangan
                    FROM master_data_timbangan_rm 
                    WHERE line='".$line_timbang."'
                    ORDER BY id ASC
                    ";

            $timbangans = MasterDataTimbanganRm::findBySql($sql)->all();
        }

        echo "<select class='form-control form-control-sm text-center' style='height:2em; text-align:center; float:left;'><option value='Pilih Timbangan'>Pilih Timbangan</options>";
        foreach($timbangans as $timbangan){
            echo "<option value='".$timbangan->kode_timbangan."'>".$timbangan->kode_timbangan."</options>";
        }
        echo "</select>";
    }

    public function actionGetLineTimbangRmNomo($nomo)
    {
        $sql = "
                SELECT sediaan,line_timbang
                FROM scm_planner
                WHERE nomo='".$nomo."'
                ORDER BY id desc
                ";

        $sediaan_array = ScmPlanner::findBySql($sql)->one();

        if (strpos(strtolower($sediaan_array->line_timbang),'lw') !== false){
            $sediaan = 'L';
        }else if (strpos(strtolower($sediaan_array->line_timbang),'pw') !== false){
            $sediaan = 'P';
        }else if (strpos(strtolower($sediaan_array->line_timbang),'sw') !== false){
            $sediaan = 'S';
        }else if (strpos(strtolower($sediaan_array->line_timbang),'vw') !== false){
            $sediaan = 'V';
        }else{
            $sediaan = $sediaan_array->sediaan;
        }

        $sql = "
                SELECT distinct(line)
                FROM master_data_timbangan_rm 
                WHERE sediaan='".$sediaan."'
                ORDER BY line ASC
                ";

        $lines = MasterDataTimbanganRm::findBySql($sql)->all();

        echo "<select id='line' class='form-control form-control-sm text-center' style='height:2em; text-align:center; float:left;'><option value='Pilih Line'>Pilih Line</options>";
        foreach($lines as $line){
            echo "<option value='".$line->line."'>".$line->line."</options>";
        }
        echo "</select>";
    }

    public function actionGetLineTimbangRmKodeBulk($kode_bulk)
    {
        $sql = "
                SELECT sediaan
                FROM scm_planner
                WHERE nomo ilike '%".$kode_bulk."%'
                ORDER BY id desc
                ";

        $sediaan_array = ScmPlanner::findBySql($sql)->one();
        $sediaan = $sediaan_array->sediaan;

        $sql = "
                SELECT distinct(line)
                FROM master_data_timbangan_rm 
                WHERE sediaan='".$sediaan."'
                ORDER BY line ASC
                ";

        $lines = MasterDataTimbanganRm::findBySql($sql)->all();

        echo "<select id='line' class='form-control form-control-sm text-center' style='height:2em; text-align:center; float:left;'><option value='Pilih Line'>Pilih Line</options>";
        foreach($lines as $line){
            echo "<option value='".$line->line."'>".$line->line."</options>";
        }
        echo "</select>";
    }

    public function actionGetTimbanganByLine($nama_line,$idItem,$idSplit,$operator=null)
    {   
        $connection = Yii::$app->db;

        if ($idSplit == 0){
            $sql = "
            SELECT uom
            FROM log_formula_breakdown 
            WHERE id=".$idItem."
            ";

            $uom = LogFormulaBreakdown::findBySql($sql)->one()['uom'];
            if (!empty($operator)){
                $kode_timbangan = Yii::$app->db->createCommand("SELECT * FROM master_data_timbangan_rm WHERE uom = '".$uom."' and operator = '".$operator."' and line = '".$nama_line."'")->queryOne()['kode_timbangan'];    
            }else{
                $kode_timbangan = null;
            }
            
            
            $command2 = $connection->createCommand("
            UPDATE log_formula_breakdown
            SET timbangan = :timbangan
            WHERE id = :id;
            ", 
            [':timbangan'=> $kode_timbangan,
             ':id'=>$idItem
            ]);

            $result2 = $command2->queryAll();
        }else{
            $sql_split = "
            SELECT uom
            FROM log_formula_breakdown_split 
            WHERE id=".$idSplit."
            ";

            $uom = LogFormulaBreakdownSplit::findBySql($sql_split)->one()['uom'];
            if (!empty($operator)){
                $kode_timbangan = Yii::$app->db->createCommand("SELECT * FROM master_data_timbangan_rm WHERE uom = '".$uom."' and operator = '".$operator."' and line = '".$nama_line."' ")->queryOne()['kode_timbangan'];
            }else{
                $kode_timbangan = null;
            }

            $command2 = $connection->createCommand("
            UPDATE log_formula_breakdown_split
            SET timbangan = :timbangan
            WHERE id = :id;
            ", 
            [':timbangan'=> $kode_timbangan,
             ':id'=>$idSplit
            ]);

            $result2 = $command2->queryAll();

        }
    }

    public function actionGetTimbanganRepack($uom,$kode_timbangan=null)
    {   
        $connection = Yii::$app->db;

        // if ($idSplit == 0){
        //     $sql = "
        //     SELECT uom
        //     FROM log_formula_breakdown 
        //     WHERE id=".$idItem."
        //     ";

        //     $uom = LogFormulaBreakdown::findBySql($sql)->one()['uom'];
        //     if (!empty($operator)){
        //         $kode_timbangan = Yii::$app->db->createCommand("SELECT * FROM master_data_timbangan_rm WHERE uom = '".$uom."' and operator = '".$operator."' and line = '".$nama_line."'")->queryOne()['kode_timbangan'];    
        //     }else{
        //         $kode_timbangan = null;
        //     }
            
            
        //     $command2 = $connection->createCommand("
        //     UPDATE log_formula_breakdown
        //     SET timbangan = :timbangan
        //     WHERE id = :id;
        //     ", 
        //     [':timbangan'=> $kode_timbangan,
        //      ':id'=>$idItem
        //     ]);

        //     $result2 = $command2->queryAll();
        // }else{
        //     $sql_split = "
        //     SELECT uom
        //     FROM log_formula_breakdown_split 
        //     WHERE id=".$idSplit."
        //     ";

        //     $uom = LogFormulaBreakdownSplit::findBySql($sql_split)->one()['uom'];
        //     if (!empty($operator)){
        //         $kode_timbangan = Yii::$app->db->createCommand("SELECT * FROM master_data_timbangan_rm WHERE uom = '".$uom."' and operator = '".$operator."' and line = '".$nama_line."' ")->queryOne()['kode_timbangan'];
        //     }else{
        //         $kode_timbangan = null;
        //     }

        //     $command2 = $connection->createCommand("
        //     UPDATE log_formula_breakdown_split
        //     SET timbangan = :timbangan
        //     WHERE id = :id;
        //     ", 
        //     [':timbangan'=> $kode_timbangan,
        //      ':id'=>$idSplit
        //     ]);

        //     $result2 = $command2->queryAll();

        // }


        $sql_timbangan = "
            SELECT distinct kode_timbangan,id
            FROM master_data_timbangan_rm 
            WHERE line='LWE03' and uom = '".$uom."' and kode_timbangan != '".$kode_timbangan."'
            ORDER BY id ASC
            ";

            $timbangans = MasterDataTimbanganRm::findBySql($sql_timbangan)->all();

        if (empty($timbangans)){
            echo "-";
        } else {
            echo "<option value='Pilih Timbangan'>Pilih Timbangan</options>";
            echo "<option selected='selected'>".$kode_timbangan."</options>";
            foreach($timbangans as $timbangan){
                echo "<option value='".$timbangan->kode_timbangan."'>".$timbangan->kode_timbangan."</options>";
            }
        }
    }
}
