<?php

namespace app\controllers;
use yii\db\Query;

class LiveDashboardController extends \yii\web\Controller
{
    public function actionIndex()
    {
      if (isset($_GET['sediaan'])) {
        $sediaan = $_GET['sediaan'];
      } else {
        $sediaan = 'liquid';
      }

      if ($sediaan == 'liquid') {
        $code = 'L';
      }else if ($sediaan == 'semsol') {
        $code = 'S';
      }else if ($sediaan == 'varcos') {
        $code = 'V';
      }else if ($sediaan == 'powder') {
        $code = 'P';
      }

      $this->layout = '//main-sidebar-collapse';

      if ($sediaan == 'varcos') {
        $query = new Query();
        $res = $query->select('date,week,sediaan,
        SUM(qty_input_daily) as qty_input_daily, SUM(qty_palet_input_daily) as qty_palet_input_daily,
        SUM(qty_input_weekly) as qty_input_weekly, SUM(qty_palet_input_weekly) as qty_palet_input_weekly,
        SUM(qty_output_daily) as qty_output_daily, SUM(qty_palet_output_daily) as qty_palet_output_daily,
        SUM(qty_output_weekly) as qty_output_weekly, SUM(qty_palet_output_weekly) as qty_palet_output_weekly')
        ->from('live_dashboard_kfg')
        ->where(['sediaan' => 'J6'])
        ->orWhere(['sediaan' => $code])
        ->groupBy('sediaan,date,week')
        ->one();
      }else{
        $query = new Query();
        $res = $query->from('live_dashboard_kfg')
        ->where('sediaan= :sediaan',[':sediaan'=>$code])
        ->one();
      }

      if ($res['week'] == null ||$res['week'] == '') {
        $data['week']= '<i>Belum ada data</i>';
      }else{
        $data['week'] = '<b>WEEK '.$res['week'].'</b>';
      }

      if ($res['date'] == null ||$res['date'] == '') {
        $data['date'] = '<i>Belum ada data</i>';
      }else{
        setlocale(LC_TIME,'id_ID.UTF8');
        $data['date'] = '<b>'.strtoupper(strftime("%d %B %Y",strtotime($res['date']))).'</b>';
      }

      return $this->render('index',[
        'res' => $res,
        'data' => $data,
        'sediaan' => $sediaan,
      ]);
    }

}
