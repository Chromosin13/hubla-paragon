<?php

namespace app\controllers;

use Yii;
use app\models\DashboardLeadtimePengolahan;
use app\models\DashboardLeadtimePengolahanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
/**
 * DashboardLeadtimePengolahanController implements the CRUD actions for DashboardLeadtimePengolahan model.
 */
class DashboardLeadtimePengolahanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DashboardLeadtimePengolahan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DashboardLeadtimePengolahanSearch();
        $searchModel->upload_date = '2017-01-01 - 2018-12-31';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';


        // Query Sediaan
        $sql3="
                SELECT
                    DISTINCT sediaan
                FROM dashboard_leadtime_pengolahan
            ";

            $sediaan_list = ArrayHelper::map(DashboardLeadtimePengolahan::findBySql($sql3)->all(), 'sediaan','sediaan');

        // Query Nama FG
        $sql3="
                SELECT
                    DISTINCT nama_fg
                FROM dashboard_leadtime_pengolahan
            ";

            $nama_fg_list = ArrayHelper::map(DashboardLeadtimePengolahan::findBySql($sql3)->all(), 'nama_fg','nama_fg');


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'sediaan_list' => $sediaan_list,
            'nama_fg_list' => $nama_fg_list,
        ]);
    }

    /**
     * Displays a single DashboardLeadtimePengolahan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionJsonLeadtimePengolahan()
    {
        $sql="
        
        SELECT 
            array_agg(t.*) nomo
        FROM 
            (
                select 
                    extract(epoch from upload_date)*1000 as upload_date,
                    avg(case when sum_leadtime_net<'00:00'::interval then 0 else EXTRACT(EPOCH FROM sum_leadtime_net)/60 end)
                from dashboard_leadtime_pengolahan
                group by upload_date
                order by upload_date desc
            ) t
        ;
        ";

        $row = DashboardLeadtimePengolahan::findBySql($sql)->all();
        
        // clean
        $remov = array('(', ')','"','{','}');
        $replc   = array('[', ']','','[',']');

        echo $json = str_replace($remov,$replc,$row[0]->nomo);
        
        // echo 'asdas';

    }

    /**
     * Creates a new DashboardLeadtimePengolahan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DashboardLeadtimePengolahan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DashboardLeadtimePengolahan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DashboardLeadtimePengolahan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DashboardLeadtimePengolahan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DashboardLeadtimePengolahan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DashboardLeadtimePengolahan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
