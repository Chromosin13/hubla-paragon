<?php

namespace app\controllers;

use Yii;
use app\models\SamplingLabel;
use app\models\SamplingLabelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use ZipArchive;



/**
 * SamplingLabelController implements the CRUD actions for SamplingLabel model.
 */
class SamplingLabelController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SamplingLabel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SamplingLabelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SamplingLabel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionScanQr()
    {
        $model = new SamplingLabel();
        $searchModel = new SamplingLabelSearch();

        return $this->render('_form-scan',[
            'model'=>$model,
        ]);


    }
    public function actionScanQrCamera()
    {
        $model = new SamplingLabel();
        $searchModel = new SamplingLabelSearch();

        return $this->render('_form-scan-camera',[
            'model'=>$model,
        ]);


    }

    /**
     * Creates a new SamplingLabel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($data)
    {
        $model = new SamplingLabel();
        $datas = explode('|',$data);
        $barcode = null;
        $odoo_code = null;
        $nobatch = null;
        $qty = null;
        $expired = null;
        foreach($datas as $d){
          $raw = explode(':',$d);
          if ($raw[0]=='C'){
              $barcode = $raw[1];
          }
          if ($raw[0]=='O'){
              $odoo_code = $raw[1];
          }
          if ($raw[0]=='Q'){
              $qty = $raw[1];
          }
          if ($raw[0]=='B'){
              $nobatch = $raw[1];
          }
          if ($raw[0]=='E'){
              $expired = $raw[1];
          }

        }

        if ($model->load(Yii::$app->request->post()) ) {
            $model->save();
            $imageId = $model->id;
            $all_imgName = null;
            $all_imgUrl = null;
            $x = null;

            $images = UploadedFile::getInstances($model,'upload');
            if($images){
                foreach($images as $image){
                    $x++;
                    $imgName = 'label_'.$imageId.'_'.$x.'.'.$image->getExtension();
                    $all_imgName .= $imgName.',';
                    $image->saveAs(Yii::getAlias('@labelImgPath').'/'.$imgName);//here we need to give path where to upload this function works as move_uploaded_file in php
                    $imgUrl = Yii::getAlias('@labelImgUrl').'/'.$imgName;
                    $all_imgUrl .= $imgUrl.',';
                }
            // print_r($imageId);
                $model->image = $all_imgName;
                $model->image_url = $all_imgUrl;
            }
            if ($model->save()){
              Yii::$app->session->setFlash('success','Data berhasil disimpan');
            }else{
              Yii::$app->session->setFlash('danger','Data gagal disimpan');
            }
            return $this->redirect(['sampling-label/scan-qr']);
        } else {
            return $this->render('create', [
              'barcode' => $barcode,
              'odoo_code' => $odoo_code,
              'nobatch' => $nobatch,
              'qty' => $qty,
              'expired' => $expired,
              'data' => $data,
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SamplingLabel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SamplingLabel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SamplingLabel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SamplingLabel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SamplingLabel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
