<?php

namespace app\controllers;
use yii\db\Query;
use Yii;

class WelcomePageController extends \yii\web\Controller
{

    public function accessRules()
    {
        return array(
            array('allow',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->layout = 'main-login';
        return $this->render('index');
    }

    public function actionListPeserta(){
      $this->layout = 'main-login';
      $listPeserta = $this->queryGetAllPeserta();
      $totalAttendees = $this->queryCountAttendes();
      return $this->render('list-peserta',[
        'listPeserta' => $listPeserta,
        'totalAttendees' => $totalAttendees,
      ]);
    }

    public function actionGetDataById($id){
      $res = $this->queryGetData($id);
      if ($res) {
        return json_encode($res);
      }else{
        return 0;
      }
    }

    public function actionUpdTimestamp($id){
      $res = $this->queryUpdTimestamp($id);
      if ($res) {
        return 1;
      }else{
        return 0;
      }
    }

    public function actionGetAllPeserta(){
      $query = new Query();
      $res = $query
      ->from('user_slp')
      // ->where('timestamp is not null')
      ->where("status like '%PESERTA%'")
      ->orderBy('timestamp asc')
      ->all();
      return base64_encode(json_encode($res));
    }

    public function actionCountAttendees(){
      $query = new Query();
      $res = $query
      ->select('count(*) total')
      ->from('user_slp')
      ->where('timestamp is not null')
      ->andWhere("status like '%PESERTA%'")
      ->one();
      return $res['total'];
    }

    public function actionResetAttendance(){
      $connection = Yii::$app->db;
      $command = $connection->createCommand("
        UPDATE user_slp SET timestamp = null
      ");
      $res = $command->queryAll();
      if ($res) {
        return true;
      }else{
        return false;
      }
    }

    protected function queryCountAttendes(){
      $query = new Query();
      $res = $query
      ->select('count(*) total')
      ->from('user_slp')
      ->where('timestamp is not null')
      ->andWhere("status like '%PESERTA%'")
      ->one();
      return $res;
    }

    protected function queryGetAllPeserta(){
      $query = new Query();
      $res = $query
      ->from('user_slp')
      // ->where('timestamp is not null')
      ->where("status like '%PESERTA%'")
      ->orderBy('timestamp asc')
      ->all();
      return $res;
    }

    protected function queryGetData($id){
      $query = new Query();
      $res = $query
      ->from('user_slp')
      ->where('id = :id',[':id' => $id])
      ->one();
      return $res;
    }

    protected function queryUpdTimestamp($id){
      date_default_timezone_set('Asia/Jakarta');
      $connection = Yii::$app->db;
      $command = $connection->createCommand("
      UPDATE user_slp SET timestamp = :timestamp
      WHERE id = :id
      ",
      [
        ':id' => $id,
        ':timestamp' => date("Y-m-d H:i:s")
      ]);

      $res = $command->queryAll();
      return $res;
    }

}
