<?php

namespace app\controllers;

use Yii;
use app\models\ScmPlanner;
use app\models\ScmPlannerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use app\models\User;
use yii\filters\AccessControl;
/**
 * ScmPlannerController implements the CRUD actions for ScmPlanner model.
 */
class ScmPlannerController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create', 'update', 'delete','index','view'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_PLANNER
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all ScmPlanner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScmPlannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ScmPlanner model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ScmPlanner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ScmPlanner();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['create']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionImportExcel()
    {

        $inputFile = 'uploads/planner.xls';
        try{
                $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFile);

        }catch(Exception $e)
        {
            die('Error');
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for( $row = 1; $row <= $highestRow; $row++)
        {
            $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);

            if($row == 1)
            {
                continue;
            }
            $planner = new ScmPlanner();
            $planner->sediaan = $rowData[0][0];
            $planner->streamline = $rowData[0][1];
            $planner->line_timbang = $rowData[0][2];
            $planner->line_olah = $rowData[0][3];
            $planner->line_olah_2 = $rowData[0][4];
            $planner->line_press = $rowData[0][5];
            $planner->line_kemas_1 = $rowData[0][6];
            $planner->line_kemas_2 = $rowData[0][7];
            $planner->start = $rowData[0][8];
            $planner->due = $rowData[0][9];
            $planner->leadtime = $rowData[0][10];
            $planner->kode_jadwal = $rowData[0][11];
            $planner->snfg = $rowData[0][12];
            $planner->snfg_komponen = $rowData[0][13];
            $planner->koitem_bulk = $rowData[0][14];
            $planner->koitem_fg = $rowData[0][15];
            $planner->nama_bulk = $rowData[0][16];
            $planner->nama_fg = $rowData[0][17];
            $planner->besar_batch = $rowData[0][18];
            $planner->besar_lot = $rowData[0][19];
            $planner->lot = $rowData[0][20];
            $planner->tglpermintaan = $rowData[0][21];
            $planner->jumlah = $rowData[0][22];
            $planner->keterangan = $rowData[0][23];
            $planner->kategori_sop = $rowData[0][24];
            $planner->kategori_detail = $rowData[0][25];
            $planner->nobatch = $rowData[0][26];
            $planner->status = $rowData[0][27];
            $planner->week = $rowData[0][28];
            $planner->line_olah_premix = $rowData[0][29];
            $planner->line_adjust_olah_1 = $rowData[0][30];
            $planner->line_adjust_olah_2 = $rowData[0][31];
            $planner->line_ayak = $rowData[0][32];
            $planner->line_press_filling_packing = $rowData[0][33];
            $planner->line_filling = $rowData[0][34];
            $planner->line_filling_packing_inline = $rowData[0][35];
            $planner->line_flame_packing_inline = $rowData[0][36];
            $planner->line_packing = $rowData[0][37];
            $planner->snfg_induk = $rowData[0][38];
            $planner->kode_sl = $rowData[0][39];
            $planner->jumlah_press = $rowData[0][40];
            $planner->alokasi = $rowData[0][41];
            $planner->save();

            print_r($planner->getErrors());
        }
        return $this->redirect(['create']);
        //die('okay');

    }

    public function actionDownload() {
       $path = Yii::getAlias('@webroot') . '/uploads';

       $file = $path . '/planner.xls';

       if (file_exists($file)) {

       Yii::$app->response->sendFile($file);

      } 
    }

    public function actionUpload()
    
    {
        $fileName = 'file';
        $uploadPath = 'uploads';

        if (isset($_FILES[$fileName])) {
            $file = \yii\web\UploadedFile::getInstanceByName($fileName);

            //Print file data
            //print_r($file);

            if ($file->saveAs($uploadPath . '/' . $file->name)) {
                //Now save file data to database

                echo \yii\helpers\Json::encode($file);
            }
        }else{

            return $this->render('create');

        }

        return false;
    }
    /**
     * Updates an existing ScmPlanner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionGetPlanner($snfg_komponen)
    {
        $sql="select * from scm_planner where snfg_komponen='".$snfg_komponen."'";
        $planner= ScmPlanner::findBySql($sql)->one();
        echo Json::encode($planner);

    }

    public function actionGetSnfg($snfg)
    {
        $sql="select * from scm_planner where snfg='".$snfg."'";
        $planner= ScmPlanner::findBySql($sql)->one();
        echo Json::encode($planner);

    }


    /**
     * Deletes an existing ScmPlanner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ScmPlanner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScmPlanner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ScmPlanner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
