<?php

namespace app\controllers;

use Yii;
use app\models\Pengolahan;
use app\models\PengolahanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use app\models\User;
use yii\filters\AccessControl;
use yii\base\Model;
use yii\base\Action;
use yii\web\Response;
/**
 * PengolahanController implements the CRUD actions for Pengolahan model.
 */

class PengolahanController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create', 'update', 'delete','index','view','test'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view','test'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_PENGOLAHAN
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Pengolahan models.
     * @return mixed
     */

    public function actionTest()
    {
        $model = new Pengolahan();

        $request = Yii::$app->getRequest();
        if ($request->isPost && $request->post('ajax') !== null) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = ActiveForm::validate($model);
            return $result;
        }

        if ($model->load(Yii::$app->request->post())) {
            if (!$model->validate()) {
                Yii::error('Validation errors: ' . print_r($model->getErrors(), true));
            }
        }
        
        return $this->render('create', ['model' => $model]);
    }

    public function actionIndex()
    {
        $searchModel = new PengolahanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pengolahan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pengolahan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */


    public function actionCreate()
    {
        $model = new Pengolahan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           // return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['create']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    // public function actionCreate()
    // {
    //     $model = new Pengolahan();
    //     if($model->load(Yii::$app->request->post()))
    //     {
    //         $transaction = Yii::$app->db->beginTransaction();
    //         try{
    //             $model->items = Yii::$app->request->post('Item',[]);
    //             if($model->save()){
    //                 $transaction->commit();
    //                 return $this->redirect(['view','id'=>$model->id]);
    //             }else{
    //                 $transaction->rollback();
    //             }
    //         }catch(Exception $e){
    //             $transaction->rollback();
    //             throw $e;
    //         }
    //     }
    //     return $this->render('create',['model'=>$model]);
    // }

    /**
     * Updates an existing Pengolahan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionGetPengolahanKomponen($snfg_komponen)
    {
        $sql="select distinct on (snfg_komponen) * from pengolahan where snfg_komponen='".$snfg_komponen."' and state='STOP' order by snfg_komponen,id desc";
        $pengolahan= Pengolahan::findBySql($sql)->one();
        echo Json::encode($pengolahan);

    }

    public function actionGetPengolahan($snfg)
    {
        $sql="select distinct on (snfg) * from pengolahan where snfg='".$snfg."' and state='STOP' order by snfg,id desc";
        $pengolahan= Pengolahan::findBySql($sql)->one();
        echo Json::encode($pengolahan);

    }

    public function actionLanjutanPengolahan($snfg_komponen,$jenis_olah)
    {
        $sql="select coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan from pengolahan where snfg_komponen='".$snfg_komponen."' and jenis_olah='".$jenis_olah."' and state='STOP'";
        $pengolahan= Pengolahan::findBySql($sql)->one();
        echo Json::encode($pengolahan);

    }

    /**
     * Deletes an existing Pengolahan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pengolahan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pengolahan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pengolahan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
