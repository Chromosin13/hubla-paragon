<?php

namespace app\controllers;

use Yii;
use app\models\LogFroMrp;
use app\models\LogReceiveNdcSearch;
use app\models\SuratJalan;
use app\models\SuratJalanRincian;
use yii\console\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * LogReceiveNdcController implements the CRUD actions for LogReceiveNdc model.
 */
class ConsoleLogFroMrpController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionRunUpdateToMrp(){

        date_default_timezone_set('Asia/Jakarta');
        $check_log = LogFroMrp::find()->where("status ='ready' and last_updated > 'now'::timestamp - '2 days'::interval ")->all();
        // print_r($check_log);
        foreach($check_log as $log){
            $sql = "SELECT * FROM log_fro_mrp_detail WHERE nomo = :nomo";
            $rincian_bb = Yii::$app->db->createCommand($sql,[':nomo'=>$log->nomo])->queryAll();

            if (!empty($rincian_bb)){
                // print_r('<pre>');
                // print_r($rincian_bb);
                // exit();
                $item_json = json_encode($rincian_bb);
                $create_json_file = file_put_contents(Yii::getAlias('@dirRpc')."/update_stock_ndc/data_rmw.json", $item_json);
                if ($create_json_file)
                {
                    // echo "JSON file created successfully...";
                    require (Yii::getAlias('@dirRpc').'/update_stock_ndc/mrp_fro.php');
                }else{
                    // echo "Oops! Error creating json file...";
                    // return $this->redirect(['qc-fg-v2/halaman-terima-ndc', 'e'=>'json-fail']);
                }

            }else{
                
                $current_timestamp = date("Y-m-d H:i:s");

                $update_log = Yii::$app->db->createCommand("
                                    UPDATE log_fro_mrp 
                                    SET last_updated = :last_updated, status = 'cancelled',keterangan = 'Rincian item tidak tergenerate' 
                                    WHERE nomo = :nomo",
                                    [
                                        ':last_updated'=>$current_timestamp,
                                        ':nomo'=>$log->nomo
                                    ])->execute();
            }

        }

    }

    // END Of Redha Script


    /**
     * Finds the LogReceiveNdc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogReceiveNdc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogReceiveNdc::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
