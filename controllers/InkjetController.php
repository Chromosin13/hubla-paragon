<?php

namespace app\controllers;

use Yii;
use app\models\Inkjet;
use app\models\InkjetSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use app\models\User;
use yii\filters\AccessControl;
/**
 * InkjetController implements the CRUD actions for Inkjet model.
 */
class InkjetController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create', 'update', 'delete','index','view'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Inkjet models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InkjetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Inkjet model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Inkjet model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Inkjet();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['create']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Inkjet model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionLanjutanInkjet($snfg,$jenis_inkjet)
    {
        $sql="select coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan from inkjet where snfg='".$snfg."' and jenis_inkjet='".$jenis_inkjet."' and state='STOP'";
        $inkjet= Inkjet::findBySql($sql)->one();
        echo Json::encode($inkjet);

    }

    public function actionLanjutanIstInkjet($snfg,$jenis_inkjet,$lanjutan)
    {
        $sql="select coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist from inkjet where snfg='".$snfg."' and jenis_inkjet='".$jenis_inkjet."' and state='ISTIRAHAT STOP' and lanjutan=".$lanjutan." ";
        $inkjet= Inkjet::findBySql($sql)->one();
        echo Json::encode($inkjet);
    }

    /**
     * Deletes an existing Inkjet model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Inkjet model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Inkjet the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Inkjet::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
