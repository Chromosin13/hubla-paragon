<?php

namespace app\controllers;

use Yii;
use app\base\Model;
use app\models\SamplerQcFg;
use app\models\PenerimaanlogFg;
use app\models\NamaSamplerqcFg;
use app\models\SamplerQcFgSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;
use yii\web\UploadedFile;
use yii\data\SqlDataProvider;
/**
 * SamplerQcFgController implements the CRUD actions for SamplerQcFg model.
 */
class SamplerQcFgController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SamplerQcFg models.
     * @return mixed
     */
     public function actionIndex()
     {
         $searchModel = new SamplerQcFgSearch();
         $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         $dataProvider->query->andWhere("status_analis is null and kategori != 'Sekunder'")->orderBy(['priority'=>SORT_DESC, 'datetime_start'=>SORT_ASC])->all();
         //$dataProvider->sort->defaultOrder = ['datetime_start' => SORT_DESC]

             $count = Yii::$app->db->createCommand('
                 SELECT * FROM sampler_qc_fg WHERE status_analis is null and kategori=:kategori order by priority DESC, datetime_start ASC
             ', [':kategori' => 'Primer'])->queryScalar();

             // add conditions that should always apply here

             // $dataProvider = new SqlDataProvider([
             //     'sql' => 'SELECT *
             //               FROM sampler_qc_fg
             //               WHERE status_analis is null and kategori=:kategori
             //               order by priority DESC, datetime_start ASC',
             //     'params' => [':kategori' => 'Skunder'],
             //     'pagination' => [
             //         'pageSize' => 1
             //     ],
             // ]);

             // $kategoriskunder = $dataProvider->getModels();

         return $this->render('index', [
             'searchModel' => $searchModel,
             'dataProvider' => $dataProvider,
             // 'kategoriskunder' => $kategoriskunder,
         ]);
     }

     public function actionUpdateItem($nopo)
    {

        $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
                            SELECT min(batch)
                            FROM penerimaanlog_fg
                            WHERE nopo = '".$nopo."'
                            and flag_sampler is null
                            "
                        );
            $min_batch = $command->queryOne();

        $sql19="
        SELECT *
        FROM penerimaanlog_fg
        WHERE nopo = '".$nopo."'
        and batch = '".$min_batch['min']."'
        and flag_sampler is null
        ";

        $data= PenerimaanlogFg::findBySql($sql19)->all();
        $nomor_batch = $min_batch['min'];



        // $data= OperatorLogistik::findBySql($sql19)->all();
        $first = true;
        foreach ($data as $d) {
            // print_r("<pre>");
            // print_r($d);
            // print_r("</pre>");

            $po = false;
            if (!$first) {
                $po = NamaSamplerqcFgFg::findOne(['nomor_po' => $d->nopo ]);
            }
            $first = false;

            if(!$po){


                $sqlinsert = Yii::$app->db->createCommand(
                            "INSERT INTO nama_samplerqc_fg(nomor_po)
                             VALUES ('".$d->nopo."')"
                            )->execute();
            }
                // if($sqlinsert){
                    $sqlinsert1 = Yii::$app->db->createCommand(
                                "INSERT INTO sampler_qc_fg(nopo,koitem_fg,nama_fg,supplier,no_surjal,batch,penempatan,koordinat_penempatan,jumlah_kedatangan_batch,date_penerimaanlog,jumlah_palet)
                                 VALUES (
                                    '".$d->nopo."',
                                    '".$d->koitem_fg."',
                                    '".$d->nama_fg."',
                                    '".$d->supplier."',
                                    '".$d->no_surjal."',
                                    '".$d->batch."',
                                    '".$d->penempatan."',
                                    '".$d->koordinat_penempatan."',
                                    '".$d->jumlah."',
                                    '".$d->datetime_start."',
                                    '".$d->jumlah_palet."'

                                )"
                                )->execute();

        }

        return $this->redirect(['create-rincian', 'nopo' => $nopo]);

    }

    public function actionCreateRincian($nopo)
    {

        //$model = $this->findModelPo($nopo);
        //$modelsSamplerQcFg = $model->samplerQcs;
        // $model = $this->findModelPo($nopo);

        $sql2 = "
                SELECT *
                FROM nama_samplerqc_fg
                WHERE nomor_po = '". $nopo ."'
                AND nama is null
                ";
        $data2 = NamaSamplerqcFg::findBySql($sql2)->one();

        $model = $data2;

        $sql = "
                SELECT *
                FROM sampler_qc_fg
                WHERE nopo = '". $nopo ."'
                AND pengambilan_sampel is null
                ";

        $data = SamplerQcFg::findBySql($sql)->all();
        $data13 = SamplerQcFg::findBySql($sql)->one();

        $modelsSamplerQcFg = $data;

        $nomor_batch = $data13['batch'];

        if ($model->load(Yii::$app->request->post())) {

            $oldIDs = ArrayHelper::map($modelsSamplerQcFg, 'id', 'id');
            $modelsSamplerQcFg = Model::createMultiple(SamplerQcFg::classname(), $modelsSamplerQcFg);
            Model::loadMultiple($modelsSamplerQcFg, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsSamplerQcFg, 'id', 'id')));

            // validate all modelsS
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsSamplerQcFg) && $valid;
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (!empty($deletedIDs)) {
                            SamplerQc::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($modelsSamplerQcFg as $modelSamplerQcFg) {
                            $modelSamplerQcFg->sampler_id = $model->id;
                            if (! ($flag = $modelSamplerQcFg->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                    $transaction->commit();

                    date_default_timezone_set('Asia/Jakarta');
                    //update sampler_qc_fg
                    $current_time = date("H:i:s");
                        $connection = Yii::$app->getDb();
                        $command = $connection->createCommand("
                                        UPDATE sampler_qc_fg
                                        SET datetime_stop = :current_time
                                        where nopo=:nopo
                                        and batch=:batch
                                        ",
                                        [ ':nopo'=>$nopo,
                                          ':batch' =>$nomor_batch,
                                          ':current_time' => date('Y-m-d h:i A'),
                                        ]
                                    );
                        $result = $command->queryAll();

                    $connection2 = Yii::$app->getDb();
                        $command2 = $connection2->createCommand("
                                        UPDATE penerimaanlog_fg
                                        SET flag_sampler = 'OK'
                                        where nopo=:nopo
                                        and batch=:batch
                                        ",
                                        [ ':nopo'=>$nopo,
                                          ':batch' =>$nomor_batch,
                                        ]
                                    );
                        $result = $command2->queryAll();

                    return $this->redirect(['index']);

                    }
                }catch(Exception $e) {
                $transaction->rollBack();
                }
            }
        }
        return $this->render('_form-rincian', [
            'model' => $model,
            //'model_sqc' => $model_sqc,
            'nopo' => $nopo,
            'modelsSamplerQcFg' => (empty($modelsSamplerQcFg)) ? [new SamplerQcFg] : $modelsSamplerQcFg
        ]);
    }


    /**
     * Displays a single SamplerQcFg model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SamplerQcFg model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SamplerQcFg();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SamplerQcFg model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SamplerQcFg model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SamplerQcFg model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SamplerQcFg the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SamplerQcFg::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
