<?php

namespace app\controllers;

use Yii;
use app\models\DashboardLeadtimeKemas2;
use app\models\DashboardLeadtimeKemas2Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
/**
 * DashboardLeadtimeKemas2Controller implements the CRUD actions for DashboardLeadtimeKemas2 model.
 */
class DashboardLeadtimeKemas2Controller extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DashboardLeadtimeKemas2 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DashboardLeadtimeKemas2Search();
        $searchModel->upload_date = '2017-01-01 - 2018-12-31';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';


        // Query Sediaan
        $sql3="
                SELECT
                    DISTINCT sediaan
                FROM dashboard_leadtime_kemas2
            ";

            $sediaan_list = ArrayHelper::map(DashboardLeadtimeKemas2::findBySql($sql3)->all(), 'sediaan','sediaan');

        // Query Nama FG
        $sql3="
                SELECT
                    DISTINCT nama_fg
                FROM dashboard_leadtime_kemas2
            ";

            $nama_fg_list = ArrayHelper::map(DashboardLeadtimeKemas2::findBySql($sql3)->all(), 'nama_fg','nama_fg');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'sediaan_list' => $sediaan_list,
            'nama_fg_list' => $nama_fg_list,
        ]);
    }

    /**
     * Displays a single DashboardLeadtimeKemas2 model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DashboardLeadtimeKemas2 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DashboardLeadtimeKemas2();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DashboardLeadtimeKemas2 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DashboardLeadtimeKemas2 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DashboardLeadtimeKemas2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DashboardLeadtimeKemas2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DashboardLeadtimeKemas2::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
