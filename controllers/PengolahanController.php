<?php

namespace app\controllers;

use Yii;
use app\models\Pengolahan;
use app\models\Dummy;
use app\models\PengolahanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use app\models\User;
use yii\filters\AccessControl;
//use yii\base\Model;
use yii\base\Action;
use yii\web\Response;
use app\models\PengolahanItem;
use app\models\Kendala;
use app\models\Model;
use yii\helpers\ArrayHelper;

/**
 * PengolahanController implements the CRUD actions for Pengolahan model.
 */

class PengolahanController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create', 'update', 'delete','index','view'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_PENGOLAHAN
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Pengolahan models.
     * @return mixed
     */

    // public function actionIndex()
    // {
    //     $searchModel = new PengolahanSearch();
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    //     return $this->render('index', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }

    // public function actionEditableStart()
    // {
    //     $searchModel = new PengolahanSearch(
    //                         ['state'=>"START",]);
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    //     return $this->render('editable_start', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }

    // public function actionEditableStop()
    // {
    //     $searchModel = new PengolahanSearch(
    //                         ['state'=>"STOP",]);
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    //     return $this->render('editable_stop', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }

    /**
     * Displays a single Pengolahan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pengolahan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    // public function actionCreateResolusi()
    // {
    //     $model = new Pengolahan();
    //     $modelsPengolahanItem = [new PengolahanItem];
    //     $modelsKendala = [new Kendala];

    //     if ($model->load(Yii::$app->request->post()) && $model->save() && Yii::$app->request->post('Pengolahan')['is_done'] != 1) 
    //     {   

    //         // Record Standar Leadtime PIT

    //         $nomo=Yii::$app->request->post('Pengolahan')['nomo'];
    //         $jenis_proses=Yii::$app->request->post('Pengolahan')['jenis_olah'];

    //         $connection = Yii::$app->getDb();
    //         date_default_timezone_set('Asia/Jakarta');
    //         $command = $connection->createCommand("
    //             INSERT INTO pit_std_leadtime_jadwal (nojadwal,proses,jenis_proses,std_type,value,time)
    //             SELECT  :nomo as nojadwal,
    //                     'PENGOLAHAN' as proses,
    //                     :jenis_proses as jenis_proses,
    //                     ksl.std_type,
    //                     ksl.value,
    //                     :last_update as time
    //             FROM (select distinct koitem_fg,koitem_bulk from scm_planner where nomo=:nomo) a 
    //             INNER JOIN (select k.* from konfigurasi_std_leadtime k
    //                          inner join jenis_jenis_proses_rel j on k.jenis = j.jenis 
    //                           where j.jenis_proses=:jenis_proses
    //                         ) ksl ON ksl.koitem = a.koitem_fg and a.koitem_bulk = ksl.koitem_bulk
    //             ON CONFLICT (nojadwal,jenis_proses,std_type)
    //             DO NOTHING;
    //             ", 
    //             [':last_update' => date('Y-m-d h:i:s A'),':nomo'=> $nomo,':jenis_proses'=> $jenis_proses]);
    //         $result = $command->queryAll();

    //         //

    //         $modelsPengolahanItem = Model::createMultiple(PengolahanItem::classname());
    //         $modelsKendala = Model::createMultiple(Kendala::classname());
    //         Model::loadMultiple($modelsPengolahanItem, Yii::$app->request->post());
    //         Model::loadMultiple($modelsKendala, Yii::$app->request->post());

    //         // validate all models
    //         $valid = $model->validate();
    //         $valid = Model::validateMultiple($modelsPengolahanItem) && $valid;
    //         $valid = Model::validateMultiple($modelsKendala) && $valid;

    //         if ($valid) {
    //             $transaction = \Yii::$app->db->beginTransaction();
    //             try {
    //                 if ($flag = $model->save(false)) {
    //                     foreach ($modelsPengolahanItem as $modelPengolahanItem) 
    //                     {
    //                         $modelPengolahanItem->pengolahan_id = $model->id;
    //                         if (! ($flag = $modelPengolahanItem->save(false))) {
    //                             $transaction->rollBack();
    //                             break;
    //                         }
    //                     }
    //                     foreach ($modelsKendala as $modelKendala) 
    //                     {
    //                         $modelKendala->pengolahan_id = $model->id;
    //                         if (! ($flag = $modelKendala->save(false))) {
    //                             $transaction->rollBack();
    //                             break;
    //                         }
    //                     }
    //                 }
    //                 if ($flag) {
    //                     $transaction->commit();
    //                     return $this->redirect(['pusat-resolusi/create']);
    //                 }
    //             } catch (Exception $e) {
    //                 $transaction->rollBack();
    //             }
    //         }

    //        // return $this->redirect(['view', 'id' => $model->id]);
    //        //return $this->redirect(['create']);
    //     } 

    //     elseif (Yii::$app->request->post('Pengolahan')['is_done'] == 1 )
    //     {
    //         $modelsPengolahanItem = Model::createMultiple(PengolahanItem::classname());
    //         $modelsKendala = Model::createMultiple(Kendala::classname());
    //         Model::loadMultiple($modelsPengolahanItem, Yii::$app->request->post());
    //         Model::loadMultiple($modelsKendala, Yii::$app->request->post());


    //         // validate all models
    //         $valid = $model->validate();
    //         $valid = Model::validateMultiple($modelsPengolahanItem) && $valid;
    //         $valid = Model::validateMultiple($modelsKendala) && $valid;

    //         if ($valid) {
    //             $transaction = \Yii::$app->db->beginTransaction();
    //             try {
    //                 if ($flag = $model->save(false)) {
    //                     foreach ($modelsPengolahanItem as $modelPengolahanItem) 
    //                     {
    //                         $modelPengolahanItem->pengolahan_id = $model->id;
    //                         if (! ($flag = $modelPengolahanItem->save(false))) {
    //                             $transaction->rollBack();
    //                             break;
    //                         }
    //                     }
    //                     foreach ($modelsKendala as $modelKendala) 
    //                     {
    //                         $modelKendala->pengolahan_id = $model->id;
    //                         if (! ($flag = $modelKendala->save(false))) {
    //                             $transaction->rollBack();
    //                             break;
    //                         }
    //                     }
    //                 }
    //                 if ($flag) {
    //                     $transaction->commit();
    //                     return $this->redirect(['update-pengolahanitem', 'id' => $model->id]);
    //                     //return $this->redirect(['view', 'id' => $model->id]);
    //                 }
    //             } catch (Exception $e) {
    //                 $transaction->rollBack();
    //             }
    //         }

    //     }  
    //     else 
    //     {
    //         return $this->render('create-resolusi', [
    //             'model' => $model,
    //             'modelsPengolahanItem' => (empty($modelsPengolahanItem)) ? [new PengolahanItem] : $modelsPengolahanItem,
    //             'modelsKendala' => (empty($modelsKendala)) ? [new Kendala] : $modelsKendala
    //         ]);
    //     }
    // }

    // public function actionCreate()
    // {
    //     $model = new Pengolahan();
    //     $modelsPengolahanItem = [new PengolahanItem];
    //     $modelsKendala = [new Kendala];

    //     if ($model->load(Yii::$app->request->post()) && $model->save() && Yii::$app->request->post('Pengolahan')['is_done'] != 1) 
    //     {   

    //         // Record Standar Leadtime PIT

    //         $nomo=Yii::$app->request->post('Pengolahan')['nomo'];
    //         $jenis_proses=Yii::$app->request->post('Pengolahan')['jenis_olah'];

    //         $connection = Yii::$app->getDb();
    //         date_default_timezone_set('Asia/Jakarta');
    //         $command = $connection->createCommand("
    //             INSERT INTO pit_std_leadtime_jadwal (nojadwal,proses,jenis_proses,std_type,value,time)
    //             SELECT  :nomo as nojadwal,
    //                     'PENGOLAHAN' as proses,
    //                     :jenis_proses as jenis_proses,
    //                     ksl.std_type,
    //                     ksl.value,
    //                     :last_update as time
    //             FROM (select distinct koitem_fg,koitem_bulk from scm_planner where nomo=:nomo) a 
    //             INNER JOIN (select k.* from konfigurasi_std_leadtime k
    //                          inner join jenis_jenis_proses_rel j on k.jenis = j.jenis 
    //                           where j.jenis_proses=:jenis_proses
    //                         ) ksl ON ksl.koitem = a.koitem_fg and a.koitem_bulk = ksl.koitem_bulk
    //             ON CONFLICT (nojadwal,jenis_proses,std_type)
    //             DO NOTHING;
    //             ", 
    //             [':last_update' => date('Y-m-d h:i:s A'),':nomo'=> $nomo,':jenis_proses'=> $jenis_proses]);
    //         $result = $command->queryAll();

    //         //

    //         $modelsPengolahanItem = Model::createMultiple(PengolahanItem::classname());
    //         $modelsKendala = Model::createMultiple(Kendala::classname());
    //         Model::loadMultiple($modelsPengolahanItem, Yii::$app->request->post());
    //         Model::loadMultiple($modelsKendala, Yii::$app->request->post());

    //         // validate all models
    //         $valid = $model->validate();
    //         $valid = Model::validateMultiple($modelsPengolahanItem) && $valid;
    //         $valid = Model::validateMultiple($modelsKendala) && $valid;

    //         if ($valid) {
    //             $transaction = \Yii::$app->db->beginTransaction();
    //             try {
    //                 if ($flag = $model->save(false)) {
    //                     foreach ($modelsPengolahanItem as $modelPengolahanItem) 
    //                     {
    //                         $modelPengolahanItem->pengolahan_id = $model->id;
    //                         if (! ($flag = $modelPengolahanItem->save(false))) {
    //                             $transaction->rollBack();
    //                             break;
    //                         }
    //                     }
    //                     foreach ($modelsKendala as $modelKendala) 
    //                     {
    //                         $modelKendala->pengolahan_id = $model->id;
    //                         if (! ($flag = $modelKendala->save(false))) {
    //                             $transaction->rollBack();
    //                             break;
    //                         }
    //                     }
    //                 }
    //                 if ($flag) {
    //                     $transaction->commit();
    //                     return $this->redirect(['create']);
    //                 }
    //             } catch (Exception $e) {
    //                 $transaction->rollBack();
    //             }
    //         }

    //        // return $this->redirect(['view', 'id' => $model->id]);
    //        //return $this->redirect(['create']);
    //     } 

    //     elseif (Yii::$app->request->post('Pengolahan')['is_done'] == 1 )
    //     {
    //         $modelsPengolahanItem = Model::createMultiple(PengolahanItem::classname());
    //         $modelsKendala = Model::createMultiple(Kendala::classname());
    //         Model::loadMultiple($modelsPengolahanItem, Yii::$app->request->post());
    //         Model::loadMultiple($modelsKendala, Yii::$app->request->post());


    //         // validate all models
    //         $valid = $model->validate();
    //         $valid = Model::validateMultiple($modelsPengolahanItem) && $valid;
    //         $valid = Model::validateMultiple($modelsKendala) && $valid;

    //         if ($valid) {
    //             $transaction = \Yii::$app->db->beginTransaction();
    //             try {
    //                 if ($flag = $model->save(false)) {
    //                     foreach ($modelsPengolahanItem as $modelPengolahanItem) 
    //                     {
    //                         $modelPengolahanItem->pengolahan_id = $model->id;
    //                         if (! ($flag = $modelPengolahanItem->save(false))) {
    //                             $transaction->rollBack();
    //                             break;
    //                         }
    //                     }
    //                     foreach ($modelsKendala as $modelKendala) 
    //                     {
    //                         $modelKendala->pengolahan_id = $model->id;
    //                         if (! ($flag = $modelKendala->save(false))) {
    //                             $transaction->rollBack();
    //                             break;
    //                         }
    //                     }
    //                 }
    //                 if ($flag) {
    //                     $transaction->commit();
    //                     return $this->redirect(['update-pengolahanitem', 'id' => $model->id]);
    //                     //return $this->redirect(['view', 'id' => $model->id]);
    //                 }
    //             } catch (Exception $e) {
    //                 $transaction->rollBack();
    //             }
    //         }

    //     }  
    //     else 
    //     {
    //         return $this->render('create', [
    //             'model' => $model,
    //             'modelsPengolahanItem' => (empty($modelsPengolahanItem)) ? [new PengolahanItem] : $modelsPengolahanItem,
    //             'modelsKendala' => (empty($modelsKendala)) ? [new Kendala] : $modelsKendala
    //         ]);
    //     }
    // }


    /**
     * Updates an existing Pengolahan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /*
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelsPengolahanItem = $model->pengolahanItems;

        $oldIDs = ArrayHelper::map($modelsPengolahanItem, 'id', 'id');
        print_r($oldIDs);
        $modelsPengolahanItem = Model::createMultiple(PengolahanItem::classname(), $modelsPengolahanItem);
        Model::loadMultiple($modelsPengolahanItem, Yii::$app->request->post());
        $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsPengolahanItem, 'id', 'id')));

        // ajax validation
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ArrayHelper::merge(
                ActiveForm::validateMultiple($modelsPengolahanItem),
                ActiveForm::validate($model)
            );
        }

        // validate all models
        $valid = $model->validate();
        $valid = Model::validateMultiple($modelsPengolahanItem) && $valid;

        if ($valid) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if ($flag = $model->save(false)) {
                    if (! empty($deletedIDs)) {
                        PengolahanItem::deleteAll(['id' => $deletedIDs]);
                    }
                    foreach ($modelsPengolahanItem as $modelPengolahanItem) {
                        $modelPengolahanItem->pengolahan_id = $model->id;
                        if (! ($flag = $modelPengolahanItem->save(false))) {
                            $transaction->rollBack();
                            break;
                        }
                    }
                }
                if ($flag) {
                    $transaction->commit();
                    //return $this->redirect(['view', 'id' => $model->id]);
                }
            } catch (Exception $e) {
                $transaction->rollBack();
            }
        }
        return $this->render('update', [
            'model' => $model,
            'modelsPengolahanItem' => (empty($modelsPengolahanItem)) ? [new PengolahanItem] : $modelsPengolahanItem
        ]);
    }
    */
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelsPengolahanItem = $model->pengolahanItems; //!! NEW
        $modelsKendala = $model->kendalas;

        if ($model->load(Yii::$app->request->post()) && $model->save()) 
        {
            $oldIDs = ArrayHelper::map($modelsPengolahanItem, 'id', 'id');
            $oldIDs2 = ArrayHelper::map($modelsKendala, 'id', 'id');

            $modelsPengolahanItem = Model::createMultiple(PengolahanItem::classname(), $modelsPengolahanItem);
            $modelsKendala = Model::createMultiple(Kendala::classname(), $modelsKendala);

            Model::loadMultiple($modelsPengolahanItem, Yii::$app->request->post());
            Model::loadMultiple($modelsKendala, Yii::$app->request->post());

            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsPengolahanItem, 'id', 'id')));
            $deletedIDs2 = array_diff($oldIDs2, array_filter(ArrayHelper::map($modelsKendala, 'id', 'id')));

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsPengolahanItem) && $valid;
            $valid = Model::validateMultiple($modelsKendala) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (! empty($deletedIDs)) {
                            PengolahanItem::deleteAll(['id' => $deletedIDs]);
                        }

                        if (! empty($deletedIDs2)) {
                            Kendala::deleteAll(['id' => $deletedIDs2]);
                        }

                        foreach ($modelsPengolahanItem as $modelPengolahanItem) {
                            $modelPengolahanItem->pengolahan_id = $model->id;
                            if (! ($flag = $modelPengolahanItem->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }

                        foreach ($modelsKendala as $modelKendala) {
                            $modelKendala->pengolahan_id = $model->id;
                            if (! ($flag = $modelKendala->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    } else {
                        $transaction->rollBack();
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }

            //return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelsPengolahanItem' => (empty($modelsPengolahanItem)) ? [new PengolahanItem] : $modelsPengolahanItem,
                'modelsKendala' => (empty($modelsKendala)) ? [new Kendala] : $modelsKendala
            ]);
        }
    }


    public function actionUpdatePengolahanitem($id)
    {
        $model = $this->findModel($id);
        $modelsPengolahanItem = $model->pengolahanItems; //!! NEW

        if ($model->load(Yii::$app->request->post()) && $model->save()) 
        {
            $oldIDs = ArrayHelper::map($modelsPengolahanItem, 'id', 'id');
            $modelsPengolahanItem = Model::createMultiple(PengolahanItem::classname(), $modelsPengolahanItem);
            Model::loadMultiple($modelsPengolahanItem, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsPengolahanItem, 'id', 'id')));

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsPengolahanItem) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (! empty($deletedIDs)) {
                            PengolahanItem::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($modelsPengolahanItem as $modelPengolahanItem) {
                            $modelPengolahanItem->pengolahan_id = $model->id;
                            if (! ($flag = $modelPengolahanItem->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        //return $this->redirect(['view', 'id' => $model->id]);
                        return $this->redirect(['create']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }


        } 

        else {
            return $this->render('update-pengolahanitem', [
                'model' => $model,
                'modelsPengolahanItem' => (empty($modelsPengolahanItem)) ? [new PengolahanItem] : $modelsPengolahanItem
            ]);
        }
    }
    
    // Change December 4 2017
    public function actionCheckLine($nama_line)
    {
        $sql="
        SELECT *
        FROM line_monitoring_olah
        WHERE nama_line ='".$nama_line."'
        ";
        $pengolahan= Pengolahan::findBySql($sql)->one();
        echo Json::encode($pengolahan);

    }



    public function actionGetPengolahanKomponen($snfg_komponen)
    {
        $sql="select distinct on (snfg_komponen) * from pengolahan where snfg_komponen='".$snfg_komponen."' and state='STOP' order by snfg_komponen,id desc";
        $pengolahan= Pengolahan::findBySql($sql)->one();
        echo Json::encode($pengolahan);

    }

    public function actionGetPengolahan($snfg)
    {
        $sql="select distinct on (snfg) * from pengolahan where snfg='".$snfg."' and state='STOP' order by snfg,id desc";
        $pengolahan= Pengolahan::findBySql($sql)->one();
        echo Json::encode($pengolahan);

    }


    // GEt Last Jenis Olah untuk QC Bulk
    public function actionGetJenisOlahKomponen($snfg_komponen)
    {
        $sql="
        
        SELECT 
            distinct on (T.snfg_komponen_sp) T.nomo,T.jenis_olah,T.timestamp,snfg_komponen_sp snfg_komponen
        FROM
            (   
                select  
                        p.*,sp.snfg_komponen snfg_komponen_sp 
                from pengolahan p

                left join scm_planner sp on sp.snfg_komponen = p.snfg_komponen
                
                where p.nomo is null and sp.snfg_komponen='".$snfg_komponen."'
                and p.state='STOP'
                and is_done=1
            
            union all 
            
                select 
                        p.*,sp.snfg_komponen snfg_komponen_sp
                from pengolahan p

                left join scm_planner sp on sp.nomo = p.nomo
                
                where p.snfg_komponen is null and sp.snfg_komponen = '".$snfg_komponen."'
                and p.state='STOP'
                and is_done=1

            ) T
        ORDER BY T.snfg_komponen_sp,T.timestamp desc

        ";
        $jenisOlah= Pengolahan::findBySql($sql)->one();
        echo Json::encode($jenisOlah);
    }

    // GEt Last Jenis Olah untuk QC Bulk
    public function actionGetJenisOlahMo($nomo)
    {
        $sql="
        
        SELECT 
            distinct on (T.snfg_komponen_sp) T.nomo,T.jenis_olah,T.timestamp,snfg_komponen_sp snfg_komponen
        FROM
            (   
                select  
                        p.*,sp.snfg_komponen snfg_komponen_sp 
                from pengolahan p

                left join scm_planner sp on sp.snfg_komponen = p.snfg_komponen
                
                where p.nomo is null and sp.nomo='".$nomo."'
                and state='STOP'
                and is_done=1
            
            union all 
            
                select 
                        p.*,sp.snfg_komponen snfg_komponen_sp
                from pengolahan p

                left join scm_planner sp on sp.nomo = p.nomo
                
                where p.snfg_komponen is null and sp.nomo = '".$nomo."'
                and state='STOP'
                and is_done=1
            ) T
        ORDER BY T.snfg_komponen_sp,T.timestamp desc

        ";
        $jenisOlah= Pengolahan::findBySql($sql)->one();
        echo Json::encode($jenisOlah);
    }


    public function actionLanjutanPengolahan($snfg_komponen,$jenis_olah)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM 
            (
                select sp.nomo, pengolahan.snfg_komponen, pengolahan.jenis_olah, pengolahan.state, pengolahan.lanjutan
                from pengolahan 
                inner join scm_planner sp on sp.snfg_komponen = pengolahan.snfg_komponen
                where state='STOP' and pengolahan.nomo is null and pengolahan.snfg_komponen is not null

                union all
                
                select p.nomo,sp.snfg_komponen, p.jenis_olah, p.state, p.lanjutan 
                from pengolahan p
                inner join scm_planner sp on sp.nomo=p.nomo
                where p.state='STOP' and p.nomo is not null
            ) a
        WHERE a.snfg_komponen ='".$snfg_komponen."'
        and a.jenis_olah = '".$jenis_olah."'
        ";
        $pengolahan= Pengolahan::findBySql($sql)->one();
        echo Json::encode($pengolahan);

    }

    public function actionLanjutanPengolahanmo($nomo,$jenis_olah)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM 
            (
                select sp.nomo, pengolahan.snfg_komponen, pengolahan.jenis_olah, pengolahan.state, pengolahan.lanjutan
                from pengolahan 
                inner join scm_planner sp on sp.snfg_komponen = pengolahan.snfg_komponen
                where state='STOP' and pengolahan.nomo is null and pengolahan.snfg_komponen is not null

                union all
                
                select p.nomo,sp.snfg_komponen, p.jenis_olah, p.state, p.lanjutan 
                from pengolahan p
                inner join scm_planner sp on sp.nomo=p.nomo
                where p.state='STOP' and p.nomo is not null
            ) a
        WHERE a.nomo ='".$nomo."'
        and a.jenis_olah = '".$jenis_olah."'
        ";
        $pengolahan= Pengolahan::findBySql($sql)->one();
        echo Json::encode($pengolahan);

    }

    // Old without Nomo
    // public function actionLanjutanPengolahan($snfg_komponen,$jenis_olah)
    // {
    //     $sql="select coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan from pengolahan where snfg_komponen='".$snfg_komponen."' and jenis_olah='".$jenis_olah."' and state='STOP'";
    //     $pengolahan= Pengolahan::findBySql($sql)->one();
    //     echo Json::encode($pengolahan);

    // }

    public function actionLanjutanIstPengolahan($snfg_komponen,$jenis_olah,$lanjutan)
    {
        $sql="
                SELECT coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist
                FROM 
                    (
                        select sp.nomo, pengolahan.snfg_komponen, pengolahan.jenis_olah, pengolahan.state, pengolahan.lanjutan,pengolahan.lanjutan_ist
                        from pengolahan 
                        inner join scm_planner sp on sp.snfg_komponen = pengolahan.snfg_komponen
                        where state='ISTIRAHAT STOP' and pengolahan.nomo is null and pengolahan.snfg_komponen is not null

                        union all
                        
                        select p.nomo,sp.snfg_komponen, p.jenis_olah, p.state, p.lanjutan, p.lanjutan_ist 
                        from pengolahan p
                        inner join scm_planner sp on sp.nomo=p.nomo
                        where p.state='ISTIRAHAT STOP' and p.nomo is not null
                    ) a
                    WHERE a.snfg_komponen ='".$snfg_komponen."'
                    and a.jenis_olah = '".$jenis_olah."'
                    and a.lanjutan = '".$lanjutan."'
        ";
        $pengolahan= Pengolahan::findBySql($sql)->one();
        echo Json::encode($pengolahan);
    }

    public function actionLanjutanIstPengolahanmo($nomo,$jenis_olah,$lanjutan)
    {
        $sql="
                SELECT coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist
                FROM 
                    (
                        select sp.nomo, pengolahan.snfg_komponen, pengolahan.jenis_olah, pengolahan.state, pengolahan.lanjutan,pengolahan.lanjutan_ist
                        from pengolahan 
                        inner join scm_planner sp on sp.snfg_komponen = pengolahan.snfg_komponen
                        where state='ISTIRAHAT STOP' and pengolahan.nomo is null and pengolahan.snfg_komponen is not null

                        union all
                        
                        select p.nomo,sp.snfg_komponen, p.jenis_olah, p.state, p.lanjutan, p.lanjutan_ist 
                        from pengolahan p
                        inner join scm_planner sp on sp.nomo=p.nomo
                        where p.state='ISTIRAHAT STOP' and p.nomo is not null
                    ) a
                WHERE a.nomo ='".$nomo."'
                and a.jenis_olah = '".$jenis_olah."'
                and a.lanjutan = '".$lanjutan."'
                ";
        $pengolahan= Pengolahan::findBySql($sql)->one();
        echo Json::encode($pengolahan);
    }

    // public function actionLanjutanIstPengolahan($snfg_komponen,$jenis_olah,$lanjutan)
    // {
    //     $sql="select coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist from pengolahan where snfg_komponen='".$snfg_komponen."' and jenis_olah='".$jenis_olah."' and state='ISTIRAHAT STOP' and lanjutan=".$lanjutan." ";
    //     $pengolahan= Pengolahan::findBySql($sql)->one();
    //     echo Json::encode($pengolahan);
    // }

    /**
     * Deletes an existing Pengolahan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetLogInput($nojadwal)
    {
        $sql="select    
                        waktu,
                        nomo,
                        state,
                        jenis_olah as jenis_proses,
                        lanjutan,
                        is_done,
                        id 
              from pengolahan where nomo='".$nojadwal."'";
        $pp= Dummy::findBySql($sql)->all();
        //print_r($pprr);
        echo Json::encode($pp);

    }

    public function actionGetLastResolusi($nojadwal)
    {
        $sql="select    
                        waktu,
                        nomo as nojadwal,
                        state,
                        jenis_olah as jenis_proses,
                        lanjutan,
                        is_done,
                        id 
              from pengolahan where nomo='".$nojadwal."'
              ORDER BY waktu desc 
              LIMIT 1
              ";
        $pp= Dummy::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($pp);
    }

    /**
     * Finds the Pengolahan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pengolahan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pengolahan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}