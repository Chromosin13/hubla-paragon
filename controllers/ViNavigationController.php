<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use app\models\ViSchedule;
use app\models\ViTimeStamp;
use app\models\ViInfo;
use yii\web\Controller;
use yii\data\SqlDataProvider;

class ViNavigationController extends Controller
{
    private $data;
    public $layout = 'base';
    public function init() {
        $this->data['info'] = new ViInfo;
        $this->data['schedule'] = new ViSchedule;
        $this->data['time_stamp'] = new ViTimeStamp;

        parent::init();
    }

    public function actionIndex() {
        $products['done'] = $this->data['schedule']->getDoneSchedule();
        
        if (sizeof($products['done']) != 0) {
            $products['message'] = "";
        } else {
            $producs['message'] = "Belum ada data hasil inspeksi";
        }
        
        $currentTable = $this->renderPartial('viewAssets/currentTable');

        $historicTable = $this->renderPartial('viewAssets/historicTable', [
            'products' => $products
        ]);
        
        return $this->render('index', [
            'products' => $products,
            'currentTable' => $currentTable,
            'historicTable' => $historicTable
        ]);
    }
}     