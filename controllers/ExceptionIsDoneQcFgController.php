<?php

namespace app\controllers;

use Yii;
use app\models\ExceptionIsDoneQcFg;
use app\models\ExceptionIsDoneQcFgSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ExceptionIsDoneQcFgController implements the CRUD actions for ExceptionIsDoneQcFg model.
 */
class ExceptionIsDoneQcFgController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ExceptionIsDoneQcFg models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ExceptionIsDoneQcFgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdateData($snfg)
    {

        $model = new ExceptionIsDoneQcFg();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
                    UPDATE qc_fg_v2
                    SET is_done = 1
                    WHERE snfg = :snfg;
                ", 
                [':snfg'=> $snfg]);

            $result = $command->queryAll();
            if($result)
            {
                

                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        else{
            return $this->renderAjax('create', [
                    'snfg' => $snfg,
                    'model' => $model,
                ]);
        }
    }

    /**
     * Displays a single ExceptionIsDoneQcFg model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ExceptionIsDoneQcFg model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ExceptionIsDoneQcFg();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ExceptionIsDoneQcFg model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ExceptionIsDoneQcFg model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ExceptionIsDoneQcFg model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ExceptionIsDoneQcFg the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ExceptionIsDoneQcFg::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
