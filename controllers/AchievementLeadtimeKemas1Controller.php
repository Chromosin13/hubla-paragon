<?php

namespace app\controllers;

use Yii;
use app\models\AchievementLeadtimeKemas1;
use app\models\AchievementLeadtimeKemas1Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AchievementLeadtimeKemas1Controller implements the CRUD actions for AchievementLeadtimeKemas1 model.
 */
class AchievementLeadtimeKemas1Controller extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AchievementLeadtimeKemas1 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AchievementLeadtimeKemas1Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSemisolid()
    {
        $searchModel = new AchievementLeadtimeKemas1Search();
        $searchModel->sediaan='S';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSemisolidPress()
    {
        $searchModel = new AchievementLeadtimeKemas1Search();
        $searchModel->sediaan='S';
        $searchModel->jenis_kemas='PRESS';
        $info = 'Semisolid Press';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionSemisolidFilling()
    {
        $searchModel = new AchievementLeadtimeKemas1Search();
        $searchModel->sediaan='S';
        $searchModel->jenis_kemas='FILLING';
        $info = 'Semisolid Filling';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }


    public function actionPowder()
    {
        $searchModel = new AchievementLeadtimeKemas1Search();
        $searchModel->sediaan='P';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPowderPress()
    {
        $searchModel = new AchievementLeadtimeKemas1Search();
        $searchModel->sediaan='P';
        $searchModel->jenis_kemas='PRESS';
        $info = 'Powder Press';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionPowderFilling()
    {
        $searchModel = new AchievementLeadtimeKemas1Search();
        $searchModel->sediaan='P';
        $searchModel->jenis_kemas='FILLING';
        $info = 'Powder Filling';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }


    public function actionLiquid()
    {
        $searchModel = new AchievementLeadtimeKemas1Search();
        $searchModel->sediaan='L';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

    public function actionLiquidPress()
    {
        $searchModel = new AchievementLeadtimeKemas1Search();
        $searchModel->sediaan='L';
        $searchModel->jenis_kemas='PRESS';
        $info = 'Liquid Press';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionLiquidFilling()
    {
        $searchModel = new AchievementLeadtimeKemas1Search();
        $searchModel->sediaan='L';
        $searchModel->jenis_kemas='FILLING';
        $info = 'Liquid Filling';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    /**
     * Displays a single AchievementLeadtimeKemas1 model.
     * @param integer $id
     * @return mixed
     */
    // public function actionView($id)
    // {
    //     return $this->render('view', [
    //         'model' => $this->findModel($id),
    //     ]);
    // }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AchievementLeadtimeKemas1 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AchievementLeadtimeKemas1();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AchievementLeadtimeKemas1 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AchievementLeadtimeKemas1 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AchievementLeadtimeKemas1 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AchievementLeadtimeKemas1 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AchievementLeadtimeKemas1::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
