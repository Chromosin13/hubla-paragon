<?php

namespace app\controllers;

use Yii;
use app\models\MasterDataLine;
use app\models\MasterDataSediaan;
use app\models\MasterDataLineSearch;
use app\models\FlowInputSnfg;
use app\models\ScmPlanner;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use app\components\AccessRule;
use app\models\User;
use yii\filters\AccessControl;
/**
 * MasterDataLineController implements the CRUD actions for MasterDataLine model.
 */
class MasterDataLineController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterDataLine models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterDataLineSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    // // Get List of Line
    //     public function actionGetLineKemasSnfg($snfg)//testing
    //     {

    //         $sql = "
    //                 SELECT distinct on (sediaan) *
    //                 FROM scm_planner
    //                 WHERE snfg='".$snfg."'
    //                 ORDER BY sediaan,snfg
    //                 ";

    //         $sediaan_array = ScmPlanner::findBySql($sql)->all();
    //         // $sediaan = $sediaan_array->sediaan;

    //         if (SIZEOF($sediaan_array)>1){
    //             $sql = "
    //                 SELECT nama_line
    //                 FROM master_data_line
    //                 WHERE (sediaan='".$sediaan_array[0]->sediaan."' or sediaan='".$sediaan_array[1]->sediaan."')  and posisi='KEMAS'
    //                 ORDER by nama_line asc
    //                 ";
    //         }else{
    //             $sql = "
    //                 SELECT nama_line
    //                 FROM master_data_line
    //                 WHERE sediaan='".$sediaan."' and posisi='KEMAS'
    //                 ORDER by nama_line asc
    //                 ";
    //         }

    //         $lines = MasterDataLine::findBySql($sql)->all();

    //         echo "<option value='Select Line'>Select Line</options>";
    //         foreach($lines as $line){
    //             echo "<option value='".$line->nama_line."'>".$line->nama_line."</options>";
    //         }
    //     }

        // Get List of Line
        public function actionGetLineKemasSnfg($snfg)//testing
        {

            $sql = "
                    SELECT distinct on (sediaan) *
                    FROM scm_planner
                    WHERE snfg='".$snfg."'
                    ORDER BY sediaan,snfg
                    ";

            $jadwal_array = ScmPlanner::findBySql($sql)->all();

            // $sql2 = "
            //         SELECT distinct on (jenis_kemas) *
            //         FROM flow_input_snfg
            //         WHERE snfg='".$snfg."' and jenis_kemas = '".$jenis_kemas."'
            //         ORDER BY jenis_kemas,datetime_stop DESC
            //         ";

            // $line_array = FlowInputSnfg::findBySql($sql2)->one();
            // $sediaan = $sediaan_array->sediaan;
            // if (empty($line_array)){
  
            $sediaan_array=[];
            if (SIZEOF($jadwal_array)>1){
                foreach ($jadwal_array as $jadwal) {
                    $sediaan_array[]=$jadwal->sediaan;
                }
                $sediaan_string= implode("','",$sediaan_array);

                $sql = "
                    SELECT nama_line
                    FROM master_data_line
                    WHERE sediaan in ('".$sediaan_string."')  and posisi='KEMAS'
                    ORDER by nama_line asc
                    ";
            }else{

                $sql = "
                    SELECT nama_line
                    FROM master_data_line
                    WHERE sediaan='".$jadwal_array[0]->sediaan."' and posisi='KEMAS' 
                    ORDER by nama_line asc
                    ";
            }

            $lines = MasterDataLine::findBySql($sql)->all();

            echo "<option value=''>Select Line</options>";
            foreach($lines as $line){
                echo "<option value='".$line->nama_line."'>".$line->nama_line."</options>";
            }

            // }else{
            //     echo "<option value='".$line_array->nama_line."'>".$line_array->nama_line."</options>";
            // }

        }

        public function actionGetLineKemas()
        {

            $sql = "
                    SELECT nama_line
                    FROM master_data_line
                    WHERE posisi='KEMAS'
                    ORDER by nama_line asc
                    ";

            $lines = MasterDataLine::findBySql($sql)->all();

            echo "<option value='Select Line'>Select Line</options>";
            foreach($lines as $line){
                echo "<option value='".$line->nama_line."'>".$line->nama_line."</options>";
            }
        }


        public function actionGetLineKemasKomponen($snfg_komponen)
        {
            $sql = "
                    SELECT distinct on (sediaan) *
                    FROM scm_planner
                    WHERE snfg_komponen='".$snfg_komponen."'
                    ORDER BY sediaan,snfg_komponen
                    ";

            $sediaan_array = ScmPlanner::findBySql($sql)->one();
            $sediaan = $sediaan_array->sediaan;

            $sql = "
                    SELECT nama_line
                    FROM master_data_line
                    WHERE sediaan='".$sediaan."' and posisi='KEMAS'
                    ORDER by nama_line asc
                    ";

            $lines = MasterDataLine::findBySql($sql)->all();

            echo "<option value='Select Line'>Select Line</options>";
            foreach($lines as $line){
                echo "<option value='".$line->nama_line."'>".$line->nama_line."</options>";
            }
        }

        public function actionGetLineOlahNomo($nomo)
        {
            $sql = "
                    SELECT distinct on (sediaan) *
                    FROM scm_planner
                    WHERE nomo='".$nomo."'
                    ORDER BY sediaan,nomo
                    ";

            $sediaan_array = ScmPlanner::findBySql($sql)->one();
            $sediaan = $sediaan_array->sediaan;

            $sql = "
                    SELECT nomo
                    FROM scm_planner
                    WHERE keterangan ilike '%dual plant%'
                    ";

            $rows = ScmPlanner::findBySql($sql)->all();
            $nomo_arr = [];
            foreach($rows as $row){
                $nomo_arr[] = $row['nomo'];
            }

            //cek item tersebut numpang olah di plant lain atau tidak
            if (in_array($nomo, $nomo_arr)){
                $sql = "
                        SELECT nama_line
                        FROM master_data_line
                        WHERE sediaan not ilike '%off%' and posisi='PENGOLAHAN'
                        ORDER by nama_line asc
                        ";

                $lines = MasterDataLine::findBySql($sql)->all();

                echo "<option value='Select Line'>Select Line</options>";
                foreach($lines as $line){
                    echo "<option value='".$line->nama_line."'>".$line->nama_line."</options>";
                }
            }else{
                $sql = "
                        SELECT nama_line
                        FROM master_data_line
                        WHERE sediaan='".$sediaan."' and posisi='PENGOLAHAN'
                        ORDER by nama_line asc
                        ";

                $lines = MasterDataLine::findBySql($sql)->all();

                echo "<option value='Select Line'>Select Line</options>";
                foreach($lines as $line){
                    echo "<option value='".$line->nama_line."'>".$line->nama_line."</options>";
                }
            }
        }


        public function actionGetLineTimbangNomo($nomo)
        {
            // $sql = "
            //         SELECT distinct on (sediaan) *
            //         FROM scm_planner
            //         WHERE nomo='".$nomo."'
            //         ORDER BY sediaan,nomo
            //         ";

            // $sediaan_array = ScmPlanner::findBySql($sql)->one();
            // $sediaan = $sediaan_array->sediaan;

            $sql = "
                    SELECT line_timbang,sediaan
                    FROM scm_planner
                    WHERE nomo = '".$nomo."'
                    ";

            $planner = ScmPlanner::findBySql($sql)->one();

            $line_timbang = $planner->line_timbang;

            if (!empty($line_timbang) and $line_timbang != '' and $line_timbang != '-')
            if (strpos($line_timbang,'LW')!==false){
                $sediaan = 'L';
            } else if (strpos($line_timbang,'PW')!==false){
                $sediaan = 'P';
            } else if (strpos($line_timbang,'SW')!==false){
                $sediaan = 'S';
            } else if (strpos($line_timbang,'VW')!==false){
                $sediaan = 'V';
            } else {
                $sediaan=null;
            }

            if (empty($sediaan)){
                $sediaan = $planner->sediaan;
            }
            
            // $nomo_arr = [];
            // foreach($rows as $row){
            //     $nomo_arr[] = $row['nomo'];
            // }

            // if (in_array($nomo, $nomo_arr)){
            //     $sql = "
            //             SELECT nama_line
            //             FROM master_data_line
            //             WHERE sediaan in ('L','S','P','V') and posisi='PENIMBANGAN'
            //             ORDER by nama_line asc
            //             ";

            //     $lines = MasterDataLine::findBySql($sql)->all();

            //     echo "<option value='Select Line'>Select Line</options>";
            //     foreach($lines as $line){
            //         echo "<option value='".$line->nama_line."'>".$line->nama_line."</options>";
            //     }
            // }else{
                $sql = "
                        SELECT nama_line
                        FROM master_data_line
                        WHERE sediaan='".$sediaan."' and posisi='PENIMBANGAN'
                        ORDER by nama_line asc
                        ";

                $lines = MasterDataLine::findBySql($sql)->all();

                echo "<option value='Select Line'>Select Line</options>";
                foreach($lines as $line){
                    echo "<option value='".$line->nama_line."'>".$line->nama_line."</options>";
                }
            // }
        }


    /**
     * Displays a single MasterDataLine model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterDataLine model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterDataLine();

        // Query Get Nama Lab Active
        $sql3="
                SELECT
                    id,sediaan
                FROM master_data_sediaan
                ORDER BY id
            ";

        // Query Result to Array
        $list_sediaan = ArrayHelper::map(MasterDataSediaan::findBySql($sql3)->all(), 'sediaan','sediaan');



        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'list_sediaan' => $list_sediaan,
            ]);
        }
    }

    /**
     * Updates an existing MasterDataLine model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterDataLine model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterDataLine model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterDataLine the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterDataLine::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
