<?php

namespace app\controllers;

use Yii;
use app\models\Kemas1;
use app\models\Dummy;
use app\models\Kemas1Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use app\models\User;
use yii\filters\AccessControl;
use app\models\Kendala;
use app\models\PlannedDownTime;
use app\models\Model;
use yii\helpers\ArrayHelper;
use app\models\DimLine;
use app\models\ScmPlanner;

/**
 * Kemas1Controller implements the CRUD actions for Kemas1 model.
 */
class Kemas1Controller extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create', 'update', 'delete','index','view'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_KEMAS
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Kemas1 models.
     * @return mixed
     */
    // public function actionIndex()
    // {
    //     $searchModel = new Kemas1Search();
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    //     return $this->render('index', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }

    /**
     * Displays a single Kemas1 model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Kemas1 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {

    //     $model = new Kemas1();
    //     $modelsKendala = [new Kendala];

    //     // VAR

    //     // Get State

    //         $state =Yii::$app->request->post('Kemas1')['state'];

    //     // Get Nomesin
    //         $namaline=Yii::$app->request->post('Kemas1')['nama_line'];

    //         $nomesin_sql = " 
    //                 select id
    //                 from dim_line 
    //                 where kemas='".$namaline."'
    //                 limit 1 
    //               ";

    //         $nomesin_array = DimLine::findBySql($nomesin_sql)->one();
    //         $nomesin = $nomesin_array['id'];


    //     // Get Koitem
    //         $snfg_komponen=Yii::$app->request->post('Kemas1')['snfg_komponen'];

    //         $sql_itemfg = " 
    //                 select koitem_fg
    //                 from scm_planner 
    //                 where snfg_komponen='".$snfg_komponen."'
    //                 limit 1 
    //               ";

    //         $koitem_array = ScmPlanner::findBySql($sql_itemfg)->one();
    //         $koitem = $koitem_array['koitem_fg'];


    //     // Get TPM

    //         $jenis_kemas=Yii::$app->request->post('Kemas1')['jenis_kemas'];

    //         $tpm_sql = " 

    //             SELECT 
    //                 case when leadtime.value is null then 0 
    //                 when leadtime.value=0 then 0 
    //                 else round(mpq.value/leadtime.value,2) end as jumlah_pcs
    //             FROM 
    //                 (SELECT
    //                     coalesce(value,0) as value
    //                 FROM konfigurasi_std_leadtime k
    //                 WHERE k.std_type='P'
    //                 and k.jenis='mpq'
    //                 and k.koitem='".$koitem."'
    //                 LIMIT 1) as mpq
    //             LEFT JOIN 
    //                 (SELECT 
    //                     value
    //                 FROM konfigurasi_std_leadtime k
    //                 LEFT JOIN jenis_jenis_proses_rel j on j.jenis = k.jenis
    //                 WHERE k.std_type='P'
    //                 and j.jenis_proses='".$jenis_kemas."'
    //                 and k.koitem='".$koitem."'
    //                 LIMIT 1) as leadtime
    //             ON 1=1
    //            ";

    //         $tpm_array = ScmPlanner::findBySql($tpm_sql)->one();
    //         $tpm = $tpm_array['jumlah_pcs'];
    //         if(empty($tpm)){
    //             $tpm = 0;
    //         }

    //     if ($model->load(Yii::$app->request->post()) && $model->save() ) 
    //     {

    //         // Record Standar Leadtime PIT

    //         $snfg_komponen=Yii::$app->request->post('Kemas1')['snfg_komponen'];
    //         $jenis_proses=Yii::$app->request->post('Kemas1')['jenis_kemas'];

    //         // Penanda Kemas 1
    //         $j=1;

    //         $connection = Yii::$app->getDb();
    //         date_default_timezone_set('Asia/Jakarta');
    //         $command = $connection->createCommand("
    //             INSERT INTO pit_std_leadtime_jadwal (nojadwal,proses,jenis_proses,std_type,value,time)
    //             SELECT  :snfg_komponen as nojadwal,
    //                     'KEMAS 1' as proses,
    //                     :jenis_proses as jenis_proses,
    //                     ksl.std_type,
    //                     ksl.value,
    //                     :last_update as time
    //             FROM (select distinct koitem_fg,koitem_bulk from scm_planner where snfg_komponen=:snfg_komponen) a 
    //             INNER JOIN (select k.* from konfigurasi_std_leadtime k
    //                          inner join jenis_jenis_proses_rel j on k.jenis = j.jenis 
    //                           where j.jenis_proses=:jenis_proses
    //                         ) ksl ON ksl.koitem = a.koitem_fg and a.koitem_bulk = ksl.koitem_bulk
    //             ON CONFLICT (nojadwal,jenis_proses,std_type)
    //             DO NOTHING;
    //             ", 
    //             [':last_update' => date('Y-m-d h:i:s A'),':snfg_komponen'=> $snfg_komponen,':jenis_proses'=> $jenis_proses]);
    //         $result = $command->queryAll();


    //         $command2 = $connection->createCommand("
    //             INSERT INTO pit_std_leadtime_jadwal (nojadwal,proses,jenis_proses,std_type,value,time)
    //             SELECT  :snfg_komponen as nojadwal,
    //                     'MPQ' as proses,
    //                     'MPQ' as jenis_proses,
    //                     ksl.std_type,
    //                     ksl.value,
    //                     :last_update as time
    //             FROM (select distinct koitem_fg,koitem_bulk from scm_planner where snfg_komponen=:snfg_komponen) a 
    //             INNER JOIN (select k.* from konfigurasi_std_leadtime k
    //                          where k.jenis = 'mpq'
    //                         ) ksl ON ksl.koitem = a.koitem_fg and a.koitem_bulk = ksl.koitem_bulk
    //             ON CONFLICT (nojadwal,jenis_proses,std_type)
    //             DO NOTHING;
    //             ", 
    //             [':last_update' => date('Y-m-d h:i:s A'),':snfg_komponen'=> $snfg_komponen]);
    //         $result2 = $command2->queryAll();

    //         // Tembak ke Counter

    //             if($state=="START"){

    //                 $url="http://10.3.5.104/ptidms/php_action/setproduction.php?no=".$nomesin."&item=".$koitem."&snfg=".$snfg_komponen."&tpm=".$tpm."";

    //                 $ch=curl_init();

    //                 curl_setopt($ch,CURLOPT_URL, $url);

    //                 if(curl_errno($ch)){
    //                     die("Could not send request to DMS");

    //                 } else {

    //                     $result=curl_exec($ch);
    //                     curl_close($ch);
    //                     echo $url;

    //                 }
    //             }

    //         //

    //         $modelsKendala = Model::createMultiple(Kendala::classname());
    //         Model::loadMultiple($modelsKendala, Yii::$app->request->post());

    //         // validate all models
    //         $valid = $model->validate();
    //         $valid = Model::validateMultiple($modelsKendala) && $valid;

    //         if ($valid) {
    //             $transaction = \Yii::$app->db->beginTransaction();
    //             try {
    //                 if ($flag = $model->save(false)) {
    //                     foreach ($modelsKendala as $modelKendala) 
    //                     {
    //                         $modelKendala->kemas_1_id = $model->id;
    //                         if (! ($flag = $modelKendala->save(false))) {
    //                             $transaction->rollBack();
    //                             break;
    //                         }
    //                     }
    //                 }
    //                 if ($flag) {
    //                     $transaction->commit();

    //                     if($state=="STOP"){
    //                         // return Yii::$app->runAction('achievement-leadtime-kemas1/view', ['id' => $model->id]);

    //                         return $this->redirect(['planned-down-time/index','i'=>$model->id,'j'=>$j]);

    //                     } else {
    //                         return $this->redirect(['create']);
    //                     }
    //                 }
    //             } catch (Exception $e) {
    //                 $transaction->rollBack();
    //             }
    //         }
    //     } else 

    //     {
    //         return $this->render('create', [
    //             'model' => $model,
    //             'modelsKendala' => (empty($modelsKendala)) ? [new Kendala] : $modelsKendala
    //         ]);
    //     }

    //     // $model = new Kemas1();

    //     // if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //     //     //return $this->redirect(['view', 'id' => $model->id]);
    //     //     return $this->redirect(['create']);

    //     // } else {
    //     //     return $this->render('create', [
    //     //         'model' => $model,
    //     //     ]);
    //     // }
    // }

    public function actionCreateResolusi()
    {

        $model = new Kemas1();
        $modelsKendala = [new Kendala];

        // VAR

        // Get State

            $state =Yii::$app->request->post('Kemas1')['state'];

        // Get Nomesin
            $namaline=Yii::$app->request->post('Kemas1')['nama_line'];

            $nomesin_sql = " 
                    select id
                    from dim_line 
                    where kemas='".$namaline."'
                    limit 1 
                  ";

            $nomesin_array = DimLine::findBySql($nomesin_sql)->one();
            $nomesin = $nomesin_array['id'];


        // Get Koitem
            $snfg_komponen=Yii::$app->request->post('Kemas1')['snfg_komponen'];

            $sql_itemfg = " 
                    select koitem_fg
                    from scm_planner 
                    where snfg_komponen='".$snfg_komponen."'
                    limit 1 
                  ";

            $koitem_array = ScmPlanner::findBySql($sql_itemfg)->one();
            $koitem = $koitem_array['koitem_fg'];


        // Get TPM

            $jenis_kemas=Yii::$app->request->post('Kemas1')['jenis_kemas'];

            $tpm_sql = " 

                SELECT 
                    case when leadtime.value is null then 0 
                    when leadtime.value=0 then 0 
                    else round(mpq.value/leadtime.value,2) end as jumlah_pcs
                FROM 
                    (SELECT
                        coalesce(value,0) as value
                    FROM konfigurasi_std_leadtime k
                    WHERE k.std_type='P'
                    and k.jenis='mpq'
                    and k.koitem='".$koitem."'
                    LIMIT 1) as mpq
                LEFT JOIN 
                    (SELECT 
                        value
                    FROM konfigurasi_std_leadtime k
                    LEFT JOIN jenis_jenis_proses_rel j on j.jenis = k.jenis
                    WHERE k.std_type='P'
                    and j.jenis_proses='".$jenis_kemas."'
                    and k.koitem='".$koitem."'
                    LIMIT 1) as leadtime
                ON 1=1
               ";

            $tpm_array = ScmPlanner::findBySql($tpm_sql)->one();
            $tpm = $tpm_array['jumlah_pcs'];
            if(empty($tpm)){
                $tpm = 0;
            }

        if ($model->load(Yii::$app->request->post()) && $model->save() ) 
        {

            // Record Standar Leadtime PIT

            $snfg_komponen=Yii::$app->request->post('Kemas1')['snfg_komponen'];
            $jenis_proses=Yii::$app->request->post('Kemas1')['jenis_kemas'];

            // Penanda Kemas 1
            $j=1;

            $connection = Yii::$app->getDb();
            date_default_timezone_set('Asia/Jakarta');
            $command = $connection->createCommand("
                INSERT INTO pit_std_leadtime_jadwal (nojadwal,proses,jenis_proses,std_type,value,time)
                SELECT  :snfg_komponen as nojadwal,
                        'KEMAS 1' as proses,
                        :jenis_proses as jenis_proses,
                        ksl.std_type,
                        ksl.value,
                        :last_update as time
                FROM (select distinct koitem_fg,koitem_bulk from scm_planner where snfg_komponen=:snfg_komponen) a 
                INNER JOIN (select k.* from konfigurasi_std_leadtime k
                             inner join jenis_jenis_proses_rel j on k.jenis = j.jenis 
                              where j.jenis_proses=:jenis_proses
                            ) ksl ON ksl.koitem = a.koitem_fg and a.koitem_bulk = ksl.koitem_bulk
                ON CONFLICT (nojadwal,jenis_proses,std_type)
                DO NOTHING;
                ", 
                [':last_update' => date('Y-m-d h:i:s A'),':snfg_komponen'=> $snfg_komponen,':jenis_proses'=> $jenis_proses]);
            $result = $command->queryAll();


            $command2 = $connection->createCommand("
                INSERT INTO pit_std_leadtime_jadwal (nojadwal,proses,jenis_proses,std_type,value,time)
                SELECT  :snfg_komponen as nojadwal,
                        'MPQ' as proses,
                        'MPQ' as jenis_proses,
                        ksl.std_type,
                        ksl.value,
                        :last_update as time
                FROM (select distinct koitem_fg,koitem_bulk from scm_planner where snfg_komponen=:snfg_komponen) a 
                INNER JOIN (select k.* from konfigurasi_std_leadtime k
                             where k.jenis = 'mpq'
                            ) ksl ON ksl.koitem = a.koitem_fg and a.koitem_bulk = ksl.koitem_bulk
                ON CONFLICT (nojadwal,jenis_proses,std_type)
                DO NOTHING;
                ", 
                [':last_update' => date('Y-m-d h:i:s A'),':snfg_komponen'=> $snfg_komponen]);
            $result2 = $command2->queryAll();

            // Tembak ke Counter

                if($state=="START"){

                    $url="http://10.3.5.104/ptidms/php_action/setproduction.php?no=".$nomesin."&item=".$koitem."&snfg=".$snfg_komponen."&tpm=".$tpm."";

                    $ch=curl_init();

                    curl_setopt($ch,CURLOPT_URL, $url);

                    if(curl_errno($ch)){
                        die("Could not send request to DMS");

                    } else {

                        $result=curl_exec($ch);
                        curl_close($ch);
                        echo $url;

                    }
                }

            //

            $modelsKendala = Model::createMultiple(Kendala::classname());
            Model::loadMultiple($modelsKendala, Yii::$app->request->post());

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsKendala) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($modelsKendala as $modelKendala) 
                        {
                            $modelKendala->kemas_1_id = $model->id;
                            if (! ($flag = $modelKendala->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();

                        if($state=="STOP"){
                            // return Yii::$app->runAction('achievement-leadtime-kemas1/view', ['id' => $model->id]);

                            return $this->redirect(['planned-down-time/index','i'=>$model->id,'j'=>$j]);

                        } else {
                            return $this->redirect(['pusat-resolusi/create']);
                        }
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        } else 

        {
            return $this->render('create-resolusi', [
                'model' => $model,
                'modelsKendala' => (empty($modelsKendala)) ? [new Kendala] : $modelsKendala
            ]);
        }

        // $model = new Kemas1();

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     //return $this->redirect(['view', 'id' => $model->id]);
        //     return $this->redirect(['create']);

        // } else {
        //     return $this->render('create', [
        //         'model' => $model,
        //     ]);
        // }
    }

    /**
     * Updates an existing Kemas1 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $modelsKendala = $model->kendalas;

        if ($model->load(Yii::$app->request->post()) && $model->save()) 
        {
            $oldIDs = ArrayHelper::map($modelsKendala, 'id', 'id');

            $modelsKendala = Model::createMultiple(Kendala::classname(), $modelsKendala);

            Model::loadMultiple($modelsKendala, Yii::$app->request->post());

            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsKendala, 'id', 'id')));

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsKendala) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {

                        if (! empty($deletedIDs)) {
                            Kendala::deleteAll(['id' => $deletedIDs]);
                        }

                        foreach ($modelsKendala as $modelKendala) {
                            $modelKendala->kemas_1_id = $model->id;
                            if (! ($flag = $modelKendala->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    } else {
                        $transaction->rollBack();
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }

            //return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelsKendala' => (empty($modelsKendala)) ? [new Kendala] : $modelsKendala
            ]);
        }


        // $model = $this->findModel($id);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // } else {
        //     return $this->render('update', [
        //         'model' => $model,
        //     ]);
        // }
    }

    public function actionLanjutanSplitBatchKemas1($snfg_komponen,$jenis_kemas)
    {
        $sql=
        "
        SELECT coalesce(max(coalesce(lanjutan_split_batch,0)),0)+1 as lanjutan_split_batch
        FROM 
            (
                SELECT  sp.snfg, 
                        kemas_1.snfg_komponen,
                        kemas_1.jenis_kemas,
                        kemas_1.state,
                        kemas_1.lanjutan,
                        kemas_1.lanjutan_split_batch
                from kemas_1 
                inner join scm_planner sp on sp.snfg_komponen = kemas_1.snfg_komponen
                where   state='STOP'
                        and kemas_1.batch_split=1 
                        and kemas_1.snfg is null 
                        and kemas_1.snfg_komponen is not null 
                        and kemas_1.snfg_komponen='".$snfg_komponen."'
                        
                UNION ALL

                SELECT  sp.snfg, 
                        kemas_2.snfg_komponen,
                        kemas_2.jenis_kemas,
                        kemas_2.state,
                        kemas_2.lanjutan,
                        kemas_2.lanjutan_split_batch
                from kemas_2 
                inner join scm_planner sp on sp.snfg_komponen = kemas_2.snfg_komponen
                where   state='STOP'
                        and kemas_2.batch_split=1 
                        and kemas_2.snfg is null 
                        and kemas_2.snfg_komponen is not null 
                        and kemas_2.snfg_komponen='".$snfg_komponen."'

                UNION ALL

                SELECT  p.snfg,
                        sp.snfg_komponen,
                        p.jenis_kemas,
                        p.state,
                        p.lanjutan,
                        p.lanjutan_split_batch               
                from kemas_2 p
                inner join scm_planner sp on sp.snfg=p.snfg
                where   p.state='STOP' 
                        and p.batch_split=1
                        and p.snfg is not null 
                        and sp.snfg_komponen='".$snfg_komponen."'
            ) a
        WHERE a.snfg_komponen ='".$snfg_komponen."'
        and a.jenis_kemas = '".$jenis_kemas."'";
        $Kemas1= Kemas1::findBySql($sql)->one();
        echo Json::encode($Kemas1);
    }

    public function actionLanjutanKemas1($snfg_komponen,$jenis_kemas)
    {
        $sql="select coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan from kemas_1 where snfg_komponen='".$snfg_komponen."' and jenis_kemas='".$jenis_kemas."' and state='STOP'";
        $Kemas1= Kemas1::findBySql($sql)->one();
        echo Json::encode($Kemas1);

    }

    public function actionLanjutanIstKemas1($snfg_komponen,$jenis_kemas,$lanjutan)
    {
        $sql="select coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist from kemas_1 where snfg_komponen='".$snfg_komponen."' and jenis_kemas='".$jenis_kemas."' and state='ISTIRAHAT STOP' and lanjutan=".$lanjutan." ";
        $Kemas1= Kemas1::findBySql($sql)->one();
        echo Json::encode($Kemas1);
    }

    /**
     * Deletes an existing Kemas1 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetLogInput($nojadwal)
    {
        $sql="select    
                        waktu,
                        snfg_komponen as nojadwal,
                        state,
                        jenis_kemas as jenis_proses,
                        lanjutan,
                        is_done,
                        id 
              from kemas_1 where snfg_komponen='".$nojadwal."'";
        $pp= Dummy::findBySql($sql)->all();
        //print_r($pprr);
        echo Json::encode($pp);

    }

    public function actionGetLastResolusi($nojadwal)
    {
        $sql="select    
                        waktu,
                        snfg_komponen as nojadwal,
                        state,
                        jenis_kemas as jenis_proses,
                        lanjutan,
                        is_done,
                        id 
              from kemas_1 where snfg_komponen='".$nojadwal."'
              ORDER BY waktu desc 
              LIMIT 1
              ";
        $pp= Dummy::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($pp);
    }

    // Change December 4 2017
    public function actionCheckLine($nama_line)
    {
        $sql="
        SELECT  nama_line,
                nojadwal as snfg_komponen,
                waktu,
                state,
                lanjutan

        FROM line_monitoring_kemas
        WHERE nama_line ='".$nama_line."'
        ";
        $Kemas1= Kemas1::findBySql($sql)->one();
        echo Json::encode($Kemas1);

    }
 


    /**
     * Finds the Kemas1 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kemas1 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kemas1::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
