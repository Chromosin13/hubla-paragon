<?php

namespace app\controllers;

use Yii;
use app\models\QcBulkEntry;
use app\models\Pengolahan;
use app\models\StatusJadwal;
use app\models\FlowInputMo;
use app\models\ScmPlanner;
use app\models\QcBulkEntrySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\filters\AccessControl;
use app\components\AccessRule;
use app\models\User;
use yii\base\ErrorException;
use app\models\WeigherFgMapDevice;

/**
 * QcBulkEntryController implements the CRUD actions for QcBulkEntry model.
 */
class QcBulkEntryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create', 'update', 'delete','index','view'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_QCBULK
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all QcBulkEntry models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QcBulkEntrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single QcBulkEntry model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new QcBulkEntry model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new QcBulkEntry();

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    public function actionCreate()
    {
        $model = new QcBulkEntry();

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['create-rincian', 'nomo' => $model->nomo]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Render Scan Barcode QR MO QCBulk Page
     * @return mixed
     */
    public function actionCreateTab()
    {

            $frontend_ip = $_SERVER['REMOTE_ADDR'];

            $sql="
            SELECT 
                *
            FROM weigher_fg_map_device
            WHERE frontend_ip = '".$frontend_ip."'";

            $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

            if(empty($map_device_array)){
                $sbc_ip = '10.128.1.161';
                $line = 'Line A';
            }else{
                $sbc_ip = $map_device_array->sbc_ip;
                $line = $map_device_array->line;

            }


            $model = new QcBulkEntry();

            $searchModel = new QcBulkEntrySearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // $dataProvider->query->where("posisi='PENIMBANGAN' and datetime_start is not null and datetime_stop is null");

            $this->layout = '//main-sidebar-collapse';

            return $this->render('create-tab', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'frontend_ip' => $frontend_ip,
                    'sbc_ip' => $sbc_ip,
                    'line' => $line,
            ]);
            // if ($model->load(Yii::$app->request->post())) {
            //     // return $this->redirect(['create-penimbangan', 'kode_unik_formula' => $model->kode_unik_formula]);
            // } else {
                
            // }
    }

    public function actionCheckNomo($nomo)
    {
        $sql="  
                SELECT
                    sp.nomo,
                    count(sp.nomo) as count_sp,
                    count(qbe.nomo) as count_qbe,
                    case when count(sp.nomo)=0 then 0 
                         when count(sp.nomo)>=1 and count(qbe.nomo) =0 then 1
                         when count(sp.nomo)>=1 and count(qbe.nomo) >=1 then 2 end as id
                FROM scm_planner sp 
                LEFT JOIN qc_bulk_entry qbe on sp.nomo = qbe.nomo
                where sp.nomo = '".$nomo."'
                GROUP BY sp.nomo

             ";
        $ch = QcBulkEntry::findBySql($sql)->one();
        echo Json::encode($ch);
    }

    public function actionCheckRelease($nomo)
    {
        $sql="  
                select  sum(case when (status='RELEASE' or status='RELEASE UNCOMFORMITY' or status='PARALLEL MIKRO') then 1 else 0 end ) as id,
                        nomo
                from qc_bulk_entry 
                where nomo='".$nomo."'
                GROUP BY nomo
             ";
        $ch = QcBulkEntry::findBySql($sql)->one();
        echo Json::encode($ch);
    }

    public function actionCheckLastOlah($nomo)
    {
        $sql="  
                select distinct on (nomo) state,is_done 
                from pengolahan 
                where nomo='".$nomo."'
                order by nomo,waktu desc

             ";
        $ch = Pengolahan::findBySql($sql)->one();
        echo Json::encode($ch);
    }

    public function actionCheckLastOlahNew($nomo)
    {
        $sql="  
                select distinct on (nojadwal) status 
                from status_jadwal 
                where nojadwal='".$nomo."' and posisi='PENGOLAHAN'
                order by nojadwal,timestamp desc

             ";
        $ch = StatusJadwal::findBySql($sql)->one();
        echo Json::encode($ch);
    }


    public function actionCreateRincian($nomo)
    {
        
        
        $mod = new ScmPlanner();       

        $result = $mod->periksaStatusNomo($nomo);
        
        if($result=='UNHOLD'){

            $this->layout = '//main-sidebar-collapse';
            
            $model = new QcBulkEntry();



            $searchModel = new QcBulkEntrySearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where("nomo='".$nomo."'");


            // Get Related Information

            $planner_sql="  
                    select nama_fg
                    from scm_planner 
                    where nomo='".$nomo."'
                    limit 1 
                 ";
            $arr = ScmPlanner::findBySql($planner_sql)->one();
            $nama_fg = $arr->nama_fg;


            $nourut_sql="  
                    select coalesce(max(nourut),0)+1 as nourut
                    from qc_bulk_entry
                    where nomo='".$nomo."'
                    limit 1 
                 ";
            $arr = QcBulkEntry::findBySql($nourut_sql)->one();
            $nourut = $arr->nourut;

            if(Yii::$app->request->post('hasEditable'))
            {
                $operatorId = Yii::$app->request->post('editableKey');
                $operator = QcBulkEntry::findOne($operatorId);
                
                $out = Json::encode(['output'=>'','message'=>'']);
                $post = [];
                $posted = current($_POST['QcBulkEntry']);
                $post['QcBulkEntry'] = $posted;
                if($operator->load($post))
                {
                    $operator->save();

                }
                echo $out;
                return;
            }


            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                // $connection = Yii::$app->getDb();
                // $command = $connection->createCommand("
                //     UPDATE qc_fg_v2
                //     SET qty_karantina = qty
                //     WHERE id = :id;
                // ", 
                // [':id'=> $model->id]);

                // $result = $command->queryAll();

                return $this->redirect(['create-rincian', 'nomo' => $nomo]);
            } else {
                return $this->render('create-rincian', [
                    'model' => $model,
                    'nomo' => $nomo,
                    'nama_fg' => $nama_fg,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'nourut' => $nourut,
                ]);
            }
    
        }else{
            throw new \yii\web\HttpException(401,
          'Jadwal telah di HOLD / CANCEL mohon hubungi Planner');
        }
    }

    public function actionAdjust($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry
            SET timestamp = :current_time,
                status='ADJUST'
            WHERE id = :id;
            ", 
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();
             
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionRelease($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry
            SET timestamp = :current_time,
                status='RELEASE'
            WHERE id = :id;
            ", 
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();
             
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionReleaseUncomformity($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry
            SET timestamp = :current_time,
                status='RELEASE UNCOMFORMITY'
            WHERE id = :id;
            ", 
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();
             
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPending1($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry
            SET timestamp = :current_time,
                status='PENDING 1'
            WHERE id = :id;
            ", 
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();
             
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPending2($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry
            SET timestamp = :current_time,
                status='PENDING 2'
            WHERE id = :id;
            ", 
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();
             
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionRework($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry
            SET timestamp = :current_time,
                status='REWORK'
            WHERE id = :id;
            ", 
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();
             
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionCekMikro($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry
            SET timestamp = :current_time,
                status='CEK MIKRO'
            WHERE id = :id;
            ", 
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();
             
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionCekHomogenitas($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry
            SET timestamp = :current_time,
                status='CEK HOMOGENITAS'
            WHERE id = :id;
            ", 
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();
             
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionParallelMikro($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry
            SET timestamp = :current_time,
                status='PARALLEL MIKRO'
            WHERE id = :id;
            ", 
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();
             
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPendingMikro($id)
    {

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");


        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

            UPDATE qc_bulk_entry
            SET timestamp = :current_time,
                status='PENDING MIKRO'
            WHERE id = :id;
            ", 
            [':current_time' => date('Y-m-d h:i:s A'),':id'=> $id]);

        $result = $command->queryAll();
             
        return $this->redirect(Yii::$app->request->referrer);
    }


    /**
     * Updates an existing QcBulkEntry model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    //  Backup
    // public function actionUpdate($id)
    // {
    //     $model = $this->findModel($id);

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     } else {
    //         return $this->render('update', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) 
            {
                return $this->redirect(Yii::$app->request->referrer);
            }else{
                return $this->redirect(Yii::$app->request->referrer);
            } 
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    

    /**
     * Deletes an existing QcBulkEntry model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(Yii::$app->request->referrer);
        // return $this->redirect(['index']);

    }

    /**
     * Finds the QcBulkEntry model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QcBulkEntry the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QcBulkEntry::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
