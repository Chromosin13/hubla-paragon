<?php

namespace app\controllers;

use Yii;
use app\models\ScmPlanner;
use app\models\LogPickingGbb;
use app\models\LogPickingGbbSearch;
use app\models\FlowInputGbb;
use app\models\FlowInputGbbSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\data\ArrayDataProvider;

/**
 * FlowInputGbbController implements the CRUD actions for FlowInputGbb model.
 */
class FlowInputGbbController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FlowInputGbb models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FlowInputGbbSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FlowInputGbb model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FlowInputGbb model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FlowInputGbb();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCheckPage($nomo)
    {
         // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        //Check Last State of a Nomo and Line
        $sql1="

        SELECT
            distinct on (nomo)
            id,
            nomo,
            datetime_stop
        FROM flow_input_mo
        WHERE nomo = :nomo and datetime_start is not null and datetime_stop is null
        ORDER BY nomo,id desc
        ";

        $is_running = FlowInputGbb::findBySql($sql1,[':nomo'=>$nomo])->one();

        //Check if the Process Engineer already verify the formula
        $sql2="
            SELECT
                COUNT(nomo)
            FROM log_picking_gbb
            WHERE nomo = '".$nomo."'
            ";

        $task = LogPickingGbb::findBySql($sql2)->scalar();

        // //Check if the operator has a task or not
        // $sql3="
        //     SELECT
        //         COUNT(nomo)
        //     FROM log_penimbangan_rm
        //     WHERE nomo = '".$nomo."' AND status = 'Selesai Ditimbang' AND nama_operator <> 'REPACK'
        //     ";

        // $isEverDone = LogPenimbanganRm::findBySql($sql3)->scalar();

       

        // Page Routing

        // PE belum verify formula
        if(empty($task)){
            $value = array(
            "page"=>"no-task",
            "id"=>null);
        } else { // PE sudah verify formula

            if(empty($is_running)){
                $value = array("page"=>"create", "id"=>null);
            } else { // Jika tablet yg sedang scan bukan milik operator 1
                $value = array("page"=>"continue", "id"=>$is_running->id);
            }
                
        }

        echo Json::encode($value);
    }

    public function actionCheckFlowGbb()
    {
        // Get Device Information
        $model = new FlowInputGbb();

        $searchModel = new FlowInputGbbSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("datetime_start is not null and datetime_stop is null");

        $this->layout = '//main-sidebar-collapse';


        return $this->render('check-flow-gbb', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
        // if ($model->load(Yii::$app->request->post())) {
        //     // return $this->redirect(['create-penimbangan', 'nomo' => $model->nomo]);
        // } else {

        // }
    }

    public function actionCreateSupplyRm($nomo)
    {
        $mod = new ScmPlanner();

        $result = $mod->periksaStatusNomo($nomo);

        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        if($result=='UNHOLD'){
            $model = new FlowInputGbb();
            // Populate Grid Data
            $searchModel = new FlowInputGbbSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where("nomo='".$nomo."'");

            $this->layout = '//main-sidebar-collapse';

            //Check Last State of a Nomo
            $sql="

            SELECT
                distinct on (nomo)
                id,
                nomo,
                datetime_start,
                datetime_stop
            FROM flow_input_mo
            WHERE nomo = :nomo and  posisi='PENIMBANGAN'
            ORDER BY nomo,id desc
            ";

            $row = FlowInputGbb::findBySql($sql,[':nomo'=>$nomo])->one();


            // Assign Variable Null to prevent error if it's the first data entry
            if(empty($row)){
                $last_datetime_start = null;
                $last_datetime_stop = null;
                $last_id = null;
                $nama_operator = null;
            }else{
                $last_datetime_start = $row->datetime_start;
                $last_datetime_stop = $row->datetime_stop;
                $last_id = $row->id;
                $nama_operator = $row->nama_operator;
            }

            // Check If Post Value True
            if ($model->load(Yii::$app->request->post())) {

                $model->save();
                $id = $model->id;

                echo "<script>location.href='".Yii::getALias('@ipUrl')."flow-input-gbb%2Ftask&id=";
                echo $id;
                // echo "&operator=";
                // echo $operator;
                echo "';</script>";
                // return $this->redirect(['check-bom-rm',
                //     'id' => $model->id]);
            } else {
                
                return $this->render('create-supply-rm', [
                    'model' => $model,
                    'nomo' => $nomo,
                    'last_datetime_start' => $last_datetime_start,
                    'last_datetime_stop' => $last_datetime_stop,
                    'last_id' => $last_id,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]);
            }
        }else{
            throw new \yii\web\HttpException(401,
          'Jadwal telah di HOLD / CANCEL mohon hubungi Planner');
        }
    }

    public function actionGetLanjutan($nomo)
    {
        $lanjutan = Yii::$app->db->createCommand("SELECT MAX (lanjutan) FROM flow_input_gbb WHERE nomo = :nomo",[':nomo'=>$nomo])->queryOne()['max'] + 1;
        $result = ['status'=>'success','value'=>$lanjutan];
        echo Json::encode($result);
    }

    public function actionTask($id)
    {
        $this->layout = '//main-sidebar-collapse';
        $sql = "
            SELECT *
            FROM flow_input_gbb
            WHERE  id = :id ";

        $row = FlowInputGbb::findBySql($sql,[':id'=>$id])->one(); 

        $sql = "SELECT nomo, nama_bulk,snfg
                FROM scm_planner 
                WHERE nomo = :nomo ";
        $sp = ScmPlanner::findBySql($sql,[':nomo'=>$row->nomo])->one();


        if ($row->siklus == 0){

            $sql = "SELECT *
                FROM log_picking_gbb WHERE nomo = :nomo
                ORDER BY operator ASC
            ";
            $data = LogPickingGbb::findBySql($sql,[':nomo'=>$row->nomo])->all();
            
            $dataProvider= new ArrayDataProvider([
                'allModels'=>$data,
                'pagination'=>false,
            ]);


            $searchModel = new LogPickingGbbSearch();
            // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // // $dataProvider->query->where("nomo='".$nomo."'")->orderBy(['kode_olah'=>SORT_ASC,'id'=>SORT_ASC]);
            // $dataProvider->query->where("nomo='".$nomo."'")
            // ->orderBy("siklus, (COALESCE(substring(kode_olah::text, '^(\d+)'::text)::integer, 99999999)), 
            //             (substring(kode_olah::text, '[a-zA-z_-]+'::text)), 
            //             (COALESCE(substring(kode_olah::text, '(\d+)$'::text)::integer, 0))");
            // $dataProvider->pagination = false;


           
        }else{

            $sql = "SELECT *
                FROM log_picking_gbb WHERE nomo = :nomo and siklus = :siklus
                ORDER BY operator ASC
                ";
            $data = LogPickingGbb::findBySql($sql,[':nomo'=>$row->nomo,':siklus'=>$row->siklus])->all();
            
            $dataProvider= new ArrayDataProvider([
                'allModels'=>$data,
                'pagination'=>false,
            ]);

            $searchModel = new LogTaskPengolahanSearch();
            // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // $dataProvider->query->where("nomo='".$nomo."' and siklus = '".$siklus."' ")->orderBy(['kode_olah'=>SORT_ASC]);
            // $dataProvider->pagination = false;

        }

        $operator = $row->nama_operator;

        return $this->render('task', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'nomo' => $row->nomo,
            'data' => $data,
            'operator' => $operator,
            'siklus' => $row->siklus,
            'fig_id' => $row->id,
            'sp' => $sp,
        ]);
    }


    /**
     * Updates an existing FlowInputGbb model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FlowInputGbb model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FlowInputGbb model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FlowInputGbb the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FlowInputGbb::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
