<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use kartik\mpdf\Pdf;
use phpoffice\phpexcel\Classes\PHPExcel;

use app\models\DeviceMapRm;
use app\models\MasterDataTimbanganRm;
use app\models\FlowInputMo;

class BulkSisaController extends Controller
{

  public $params;

  public function actionIndex()
  {
    $bulksisa = $this->querySelAllBulkSisa();
    $arr_nomo = array();
    $countRejected = 0;
    for ($i=0; $i < count($bulksisa); $i++) {
      date_default_timezone_set('Asia/Jakarta');
      if (!empty($bulksisa[$i]['snfg_baru'] )) {
        $checkJadwal = $this->queryCheckJadwalPlanner($bulksisa[$i]['snfg_baru'], $bulksisa[$i]['kode_bulk']);
        if (!empty($checkJadwal['snfg'])){
          if ($bulksisa[$i]['status_bulksisa'] == 'Waiting List' && $bulksisa[$i]['pic'] == 'in PPC') {
            $update = $this->queryUpdStatusBulkSisa($bulksisa[$i]['nomo'],$bulksisa[$i]['snfg'],'Waiting Check','in QC Bulk',null);
          }
        }
      }
      $flowMO = $this->querySelFlowInputMO($bulksisa[$i]['nomo']);
      $tgl_rejected = date('Y-m-d',strtotime($bulksisa[$i]['datetime_rejected']));
      $now =  date('Y-m-d');
      /*if (strtotime($bulksisa[$i]['datetime_rejected']) <= strtotime('now')) {
      // if ($now == $tgl_rejected) {
        if ($bulksisa[$i]['status'] != 'Rejected' && $bulksisa[$i]['pic'] != 'in WPM') {
          $update = $this->queryUpdStatusBulkSisa($bulksisa[$i]['nomo'],$bulksisa[$i]['snfg'],'Rejected','in WPM',null);
        }
      }else{
        if (strtotime($bulksisa[$i]['datetime_rejected']) > strtotime('now')) {
          if (!empty($bulksisa[$i]['snfg_baru'] )) {
            $checkJadwal = $this->queryCheckJadwalPlanner($bulksisa[$i]['snfg_baru'], $bulksisa[$i]['kode_bulk']);
            if (!empty($checkJadwal['snfg'])){
              if ($bulksisa[$i]['status_bulksisa'] == 'Waiting List' && $bulksisa[$i]['pic'] == 'in PPC') {
                $update = $this->queryUpdStatusBulkSisa($bulksisa[$i]['nomo'],$bulksisa[$i]['snfg'],'Waiting Check','in QC Bulk',null);
              }
            }
          }
        }
      }*/
      // $tgl_timbang = strtotime($bulksisa[$i]['tgl_timbang_bulk_sisa']);
      $tgl_olah = strtotime($flowMO['datetime_start']);
      $curr = time();
      $diff = abs($curr - $tgl_olah);
      $years = floor($diff / (365*60*60*24));
      $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
      if ($bulksisa[$i]['status_bulksisa'] != 'Rejected') {
        if (!in_array($bulksisa[$i]['nomo'],$arr_nomo)) {
          array_push($arr_nomo, $bulksisa[$i]['nomo']);
        }
      }else{
        if (!in_array($bulksisa[$i]['nomo'],$arr_nomo) && $bulksisa[$i]['datetime_rejected'] == date("Y-m-d") && !empty($bulksisa[$i]['snfg_baru'] )) {
          $countRejected++;
        }
      }
    }
    $countRework = $this->queryCountRework();
    $countAgesBulk = $this->queryCountAgesBulk();
    $bulksisa = $this->querySelAllBulkSisa();
    $OS = $_SERVER['HTTP_USER_AGENT'];
    if (strpos($OS,'Android') !== false || strpos($OS,'iPhone') !== false) {
      $this->layout = '//main-sidebar-collapse';
    }
    return $this->render('index',[
      'bulksisa' => $bulksisa,
      'flowMO' => $flowMO,
      'countRejected' => $countRejected,
      'countRework' => $countRework,
      'countAgesBulk' => $countAgesBulk,
    ]);
  }

  public function actionScanNomo()
  {

    if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $frontend_ip = $_SERVER['REMOTE_ADDR'];
    }

    $query = new Query();
    $map_device = $query
    ->from('device_map_rm')
    ->where('device_ip = :device_ip',[':device_ip' => $frontend_ip])
    ->one();

    if ($map_device['device_ip'] != null) {
      $OS = $_SERVER['HTTP_USER_AGENT'];
      if (strpos($OS,'Android') !== false || strpos($OS,'iPhone') !== false) {
        $this->layout = '//main-sidebar-collapse';
      }
      return $this->render('scan-nomo',[]);
    }else{
      echo '<script type="text/javascript">',
      'window.alert("Device ini tidak memiliki hak akses untuk menimbang")
      window.location = "index.php?r=bulk-sisa/"',
      '</script>'
      ;
    }
  }

  public function actionScanValidasi($obj)
  {
    $OS = $_SERVER['HTTP_USER_AGENT'];
    if (strpos($OS,'Android') !== false || strpos($OS,'iPhone') !== false) {
      $this->layout = '//main-sidebar-collapse';
    }
    $jsonObj = json_decode(base64_decode($obj));
    $jadwal = $this->querySelSCMPlannerBySNFG($jsonObj->snfg_baru);
    return $this->render('scan-validasi',[
      'jadwal' => $jadwal
    ]);
  }

  public function actionMeasureMassa($obj)
  {
    $OS = $_SERVER['HTTP_USER_AGENT'];
    if (strpos($OS,'Android') !== false || strpos($OS,'iPhone') !== false) {
      $this->layout = '//main-sidebar-collapse';
    }
    if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $frontend_ip = $_SERVER['REMOTE_ADDR'];
    }

    $query = new Query();
    $map_device = $query->select('*')
    ->from('device_map_rm')
    ->where('device_ip = :device_ip',[':device_ip' => $frontend_ip])
    ->one();
    if ($map_device['device_ip'] != null) {
      $jsonObj = json_decode(base64_decode($obj));
      if (strpos($jsonObj->snfg, '/BS') !== false) {
        $scm_planner = $this->querySelSCMPlannerBySNFG($jsonObj->snfg);
      }else{
        $scm_planner = $this->querySelSCMPlannerByNOMO($jsonObj->nomo);
      }
      $sqll="
      SELECT
          angka_belakang_koma,galat,uom
      FROM master_data_timbangan_rm
      WHERE kode_timbangan = '".$map_device['timbangan']."'";
      $data_timbangan = MasterDataTimbanganRm::findBySql($sqll)->one();
      $bulk_sisa = $this->querySelOneBulkSisa($jsonObj->nomo,$jsonObj->snfg);
      $countStorage = $this->queryCountTotalStorage($bulk_sisa['nomo'],$bulk_sisa['snfg']);
      $sumQty = $this->querySumQty($bulk_sisa['nomo'],$bulk_sisa['snfg']);
      $het = $this->querySelHetItem(explode("/",$bulk_sisa['snfg'])[0]);
      $netto_refil = $this->querySelNettoRefil(explode("/",$bulk_sisa['snfg'])[0],$bulk_sisa['kode_bulk']);
      if ($countStorage > 0) {
        $jsonObj->countStorage = $countStorage;
      }else{
        $jsonObj->countStorage = 1;
      }
      return $this->render('measure-massa',[
        'jsonObj' => $jsonObj,
        'map_device' => $map_device,
        'scm_planner' => $scm_planner,
        'data_timbangan' => $data_timbangan,
        'sumQty' => $sumQty,
        'het' => $het,
        'netto_refil' => $netto_refil,
        'bulk_sisa' => $bulk_sisa
      ]);
    }else{
      echo '<script type="text/javascript">',
      'window.alert("Device ini tidak memiliki hak akses untuk menimbang")
      window.location = "index.php?r=bulk-sisa/scan-nomo"',
      '</script>'
      ;
    }
  }

  public function actionInputMassa($obj)
  {
    $OS = $_SERVER['HTTP_USER_AGENT'];
    if (strpos($OS,'Android') !== false || strpos($OS,'iPhone') !== false) {
      $this->layout = '//main-sidebar-collapse';
    }
    if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $frontend_ip = $_SERVER['REMOTE_ADDR'];
    }

    $sql="
    SELECT
    *
    FROM device_map_rm
    WHERE device_ip = '".$frontend_ip."'";

    $map_device = DeviceMapRm::findBySql($sql)->one();
    if ($map_device['device_ip'] != null) {
      $jsonObj = json_decode(base64_decode($obj));
      if (strpos($jsonObj->snfg, '/BS') !== false) {
        $scm_planner = $this->querySelSCMPlannerBySNFG($jsonObj->snfg);
      }else{
        $scm_planner = $this->querySelSCMPlannerByNOMO($jsonObj->nomo);
      }
      $sqll="
      SELECT
          angka_belakang_koma,galat,uom
      FROM master_data_timbangan_rm
      WHERE kode_timbangan = '".$map_device->timbangan."'";
      $data_timbangan = MasterDataTimbanganRm::findBySql($sqll)->one();
      $bulk_sisa = $this->querySelOneBulkSisa($jsonObj->nomo,$jsonObj->snfg);
      $countStorage = $this->queryCountTotalStorage($bulk_sisa['nomo'],$bulk_sisa['snfg']);
      $sumQty = $this->querySumQty($bulk_sisa['nomo'],$bulk_sisa['snfg']);
      $het = $this->querySelHetItem(explode("/",$bulk_sisa['snfg'])[0]);
      $netto_refil = $this->querySelNettoRefil(explode("/",$bulk_sisa['snfg'])[0],$bulk_sisa['kode_bulk']);
      if ($countStorage > 0) {
        $jsonObj->countStorage = $countStorage;
      }
      return $this->render('input-massa',[
        'jsonObj' => $jsonObj,
        'map_device' => $map_device,
        'scm_planner' => $scm_planner,
        'data_timbangan' => $data_timbangan,
        'sumQty' => $sumQty,
        'het' => $het,
        'netto_refil' => $netto_refil,
        'bulk_sisa' => $bulk_sisa
      ]);
    }else{
      echo '<script type="text/javascript">',
      'window.alert("Device ini tidak memiliki hak akses untuk menimbang")
      window.location = "index.php?r=bulk-sisa/scan-nomo"',
      '</script>'
      ;
    }
  }

  public function actionDetailBulkInfo($obj)
  {
    $OS = $_SERVER['HTTP_USER_AGENT'];
    if (strpos($OS,'Android') !== false || strpos($OS,'iPhone') !== false) {
      $this->layout = '//main-sidebar-collapse';
    }
    $jsonObj = json_decode(base64_decode($obj));
    date_default_timezone_set('Asia/Jakarta');
    if (strpos($jsonObj->snfg, '/BS') !== false) {
      $planner = $this->querySelSCMPlannerBySNFG($jsonObj->snfg);
      $flow_input_snfgkomponen = $this->querySelFlowInputSNFGKomponen($planner['snfg']);
      if (empty($flow_input_snfgkomponen)) {
        $flow_input_snfg = $this->querySelFlowInputSNFGLike($planner['snfg']);
      }
    }else{
      $planner = $this->querySelSCMPlannerByNOMO($jsonObj->nomo);
      $flow_input_snfgkomponen = $this->querySelFlowInputSNFGKomponen($planner['snfg_komponen']);
      if (empty($flow_input_snfgkomponen)) {
        $flow_input_snfg = $this->querySelFlowInputSNFGNotLike($planner['snfg']);
      }
    }
    $listFg = $this->querySelAllFgList();
    $list_bulk_sisa = $this->querySelManyBulkSisa($jsonObj->nomo,$jsonObj->snfg);
    $bulk_sisa = $this->querySelOneBulkSisa($jsonObj->nomo,$jsonObj->snfg);
    $master_data_product = $this->querySelMasterDataProduct($planner['koitem_bulk']);
    $master_data_koli = $this->querySelMasterDataKoli($planner['koitem_fg']);
    $sumQty = $this->querySumQty($bulk_sisa['nomo'],$bulk_sisa['snfg']);
    $flowMO = $this->querySelFlowInputMO($bulk_sisa['nomo']);
    // $tgl_timbang = strtotime($bulk_sisa['tgl_timbang']);
    $tgl_olah = strtotime($flowMO['datetime_start']);
    $now = time();
    $diff = abs($now - $tgl_olah);
    $years = floor($diff / (365*60*60*24));
    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
    $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
    if ($months < 1) {
      $weeks = floor($days/7);
      if ($weeks < 1) {
        if ($days < 1) {
          $bulk_sisa['ages'] = $hours." Jam";
        }else{
          $bulk_sisa['ages'] = $days." Hari";
        }
      }else{
        $bulk_sisa['ages'] = $weeks." Minggu";
      }
    }else{
      $bulk_sisa['ages'] = $months." Bulan";
    }
    date_default_timezone_set('Asia/Jakarta');
    /*if (strtotime($bulk_sisa['datetime_rejected']) <= strtotime('now')) {
    // if ($now == $tgl_rejected) {
      if ($bulk_sisa['status'] != 'Rejected' && $bulk_sisa['pic'] != 'in WPM') {
        /*if ($bulk_sisa['nomo'] == '-') {
          $update = $this->queryUpdStatusBulkBySNFG($bulk_sisa['snfg'],'Rejected','in WPM','Due Date');
        }else{
          $update = $this->queryUpdStatusBulkByNOMO($bulk_sisa['nomo'],'Rejected','in WPM','Due Date');
        }
        $update = $this->queryUpdStatusBulkSisa($bulk_sisa['nomo'],$bulk_sisa['snfg'],'Rejected','in WPM',null);
      }
    }else{
      if (strtotime($bulk_sisa['datetime_rejected']) > strtotime('now')) {
        if (!empty($bulk_sisa['snfg_baru'] )) {
          $checkJadwal = $this->queryCheckJadwalPlanner($bulk_sisa['snfg_baru'], $bulk_sisa['kode_bulk']);
          if (!empty($checkJadwal['snfg'])){
            if ($bulk_sisa['status'] == 'Waiting List' && $bulk_sisa['pic'] != 'in PPC') {
              $update = $this->queryUpdStatusBulkSisa($bulk_sisa['nomo'],$bulk_sisa['snfg'],'Waiting Check','in QC Bulk',null);
            }
          }
        }
      }
    }*/
    $isPrint = True;
    $numAge = (int) filter_var($bulk_sisa['ages'], FILTER_SANITIZE_NUMBER_INT);
    if (strpos($bulk_sisa['ages'],'Bulan') !== false) {
      if ($numAge > 5) {
        $bulk_sisa['flagAging'] = 'true';
      }else{
        $bulk_sisa['flagAging'] = 'false';
      }
    }else if (strpos($bulk_sisa['ages'],'Tahun') !== false) {
      $bulk_sisa['flagAging'] = 'true';
    }else{
      $bulk_sisa['flagAging'] = 'false';
    }
    for ($i=0; $i < count($list_bulk_sisa); $i++) {
      if ($list_bulk_sisa[$i]['is_printed'] == null || $list_bulk_sisa[$i]['is_printed'] == 0) {
        $isPrint = False;
        break;
      }
    }
    if (!empty($flow_input_snfg)) {
      $array = [
          'jsonObj' => $jsonObj,
          'planner' => $planner,
          'flow_input_snfg' => $flow_input_snfg,
          'flow_input_snfgkomponen' => $flow_input_snfgkomponen,
          'listFg' => $listFg,
          'sumQty' => $sumQty,
          'isPrint' => $isPrint,
          'bulk_sisa' => $bulk_sisa,
          'list_bulk_sisa' => $list_bulk_sisa,
          'master_data_product' => $master_data_product,
          'master_data_koli' => $master_data_koli,
      ];
    }else{
      $array = [
          'jsonObj' => $jsonObj,
          'planner' => $planner,
          'flow_input_snfgkomponen' => $flow_input_snfgkomponen,
          'listFg' => $listFg,
          'sumQty' => $sumQty,
          'isPrint' => $isPrint,
          'bulk_sisa' => $bulk_sisa,
          'list_bulk_sisa' => $list_bulk_sisa,
          'master_data_product' => $master_data_product,
          'master_data_koli' => $master_data_koli,
      ];
    }
    if (!empty($bulk_sisa['snfg_baru'] )) {
      $checkJadwal = $this->queryCheckJadwalPlanner($bulk_sisa['snfg_baru'], $bulk_sisa['kode_bulk']);
      $checkQC = $this->queryCheckQC($bulk_sisa['nomo'], $bulk_sisa['snfg_baru']);
      $array['jadwal'] = $checkJadwal;
      $array['qcStatus'] = $checkQC;
      /*if (count($checkQC) > 0) {
        for ($i=0; $i < count($checkQC); $i++) {
          if ($checkQC[$i]['status'] == 'MASUK SPESIFIKASI') {
            $update = $this->queryUpdStatusBulkSisaByStorage($bulk_sisa['nomo'],$bulk_sisa['snfg'],'On Progress','in PRO',($i+1));
          }else if ($checkQC[$i]['status'] == 'PENDING') {
            $update = $this->queryUpdStatusBulkSisaByStorage($bulk_sisa['nomo'],$bulk_sisa['snfg'],'Pending','in QC Bulk',($i+1));
          }else if ($checkQC[$i]['status'] == 'REWORK' && !in_array($bulk_sisa['pic'],['in PPC','in QC Bulk'])) {
            $update = $this->queryUpdStatusBulkSisaByStorage($bulk_sisa['nomo'],$bulk_sisa['snfg'],'Rework','in PPC',($i+1));
          }else{
            $update = $this->queryUpdStatusBulkSisaByStorage($bulk_sisa['nomo'],$bulk_sisa['snfg'],'Rejected','in WPM',($i+1));
          }
        }
      }*/
    }
    $array['het'] = $this->querySelHetItem(explode("/",$bulk_sisa['snfg'])[0]);
    $array['netto_refil'] = $this->querySelNettoRefil(explode("/",$bulk_sisa['snfg'])[0],$bulk_sisa['kode_bulk']);
    $content =  $this->render('detail-info', $array);
    return $content;
  }

  public function actionAddBulk($obj)
  {
    $jsonObj = json_decode(base64_decode($obj));
    $search = $this->querySelOneBulkSisa($jsonObj->nomo,$jsonObj->snfg);
    if (empty($search)) {
      $insert = $this->queryInsBulkSisa($jsonObj);
      if ($insert) {
        return 1;
      }else{
        return 0;
      }
    }else{
      $update = $this->queryUpdBulkSisa($jsonObj);
      if ($update) {
        return 1;
      }else{
        return 0;
      }
    }
  }

  public function actionPrintPdf($obj)
  {
    $jsonObj = json_decode(base64_decode($obj));
    date_default_timezone_set('Asia/Jakarta');
    $date = date('Y-m-d H:i:s');
    $flow_input_mo = $this->querySelFlowInputMO($jsonObj->nomo);
    if (strpos($jsonObj->snfg, '/BS') !== false) {
      $planner = $this->querySelSCMPlannerBySNFG($jsonObj->snfg);
      $flow_input_snfgkomponen = $this->querySelFlowInputSNFGKomponen($planner['snfg_komponen']);
      if (empty($flow_input_snfgkomponen)) {
        $flow_input_snfg = $this->querySelFlowInputSNFGLike($planner['snfg']);
      }
    }else{
      $planner = $this->querySelSCMPlannerByNOMO($jsonObj->nomo);
      $flow_input_snfgkomponen = $this->querySelFlowInputSNFGKomponen($planner['snfg_komponen']);
      if (empty($flow_input_snfgkomponen)) {
        $flow_input_snfg = $this->querySelFlowInputSNFGNotLike($planner['snfg']);
      }
    }
    $bulk_sisa = $this->querySelOneBulkSisaByStorage($jsonObj->nomo,$jsonObj->snfg,$jsonObj->no_storage);
    $qc_bulk_entry_komponen = $this->querySelQcBulkEntryKomponen($jsonObj->nomo);
    $pdf = new Pdf([
    // set to use core fonts only
    'mode' => Pdf::MODE_CORE,
    // A4 paper format
    'format' => Pdf::FORMAT_FOLIO,
    // portrait orientation
    'orientation' => Pdf::ORIENT_LANDSCAPE,
    // stream to browser inline
    'destination' => Pdf::DEST_BROWSER,

    'filename' => 'label_print_timbang_'.$jsonObj->nomo.'_'.date('Y-m-d'),
    // format content from your own css file if needed or use the
    // enhanced bootstrap css built by Krajee for mPDF formatting
    'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
    // any css to be embedded if required
    'cssInline' => '.kv-heading-1{font-size:18px} .data-text{font-size:16px!important; font-weight: bold}',
    // set mPDF properties on the fly
    'options' => ['title' => 'Label Timbang Bulk Sisa'],
    // call mPDF methods on the fly
    'methods' => [
    'SetHeader'=>['Penimbangan Bulk Sisa - PT Paragon Technology & Innovation'],
    'SetFooter'=>['This Label is computer generated, no signature required'],
    ]
    ]);
    if (!empty($flow_input_snfg)) {
      $content =  $this->renderPartial('label-kemas', [
          'jsonObj' => $jsonObj,
          'planner' => $planner,
          'date' => $date,
          'flow_input_mo' => $flow_input_mo,
          'flow_input_snfg' => $flow_input_snfg,
          'flow_input_snfgkomponen' => $flow_input_snfgkomponen,
          'bulk_sisa' => $bulk_sisa,
          'qc_bulk_entry_komponen' => $qc_bulk_entry_komponen,
      ]);
    }else{
      $content =  $this->renderPartial('label-kemas', [
          'jsonObj' => $jsonObj,
          'planner' => $planner,
          'date' => $date,
          'flow_input_mo' => $flow_input_mo,
          'flow_input_snfgkomponen' => $flow_input_snfgkomponen,
          'bulk_sisa' => $bulk_sisa,
          'qc_bulk_entry_komponen' => $qc_bulk_entry_komponen,
      ]);
    }
    $pdf->content = $content;

    /*------------------------------------*/
    Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
    $headers = Yii::$app->response->headers;
    $headers->add('Content-Type', 'application/pdf');
    /*------------------------------------*/

    // return the pdf output as per the destination setting
    return $pdf->render();
  }

  public function actionPrintLabelSample($obj)
  {
    $jsonObj = json_decode(base64_decode($obj));
    $pdf = new Pdf([
    // set to use core fonts only
    'mode' => Pdf::MODE_UTF8,
    // A4 paper format
    // 'format' => Pdf::FORMAT_FOLIO,
    'format' => [30,70],
    'marginLeft' => 1,
    'marginTop' => 1,
    'marginRight' => 1,
    'marginBottom' => 1,
    // portrait orientation
    'orientation' => Pdf::ORIENT_LANDSCAPE,
    // stream to browser inline
    'destination' => Pdf::DEST_BROWSER,

    'filename' => 'label_print_sampling_'.$jsonObj->nomo,
    // format content from your own css file if needed or use the
    // enhanced bootstrap css built by Krajee for mPDF formatting
    'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
    // any css to be embedded if required
    'cssInline' => '.kv-heading-1{font-size:18px} .title-text{font-size: 1rem!important} .data-text{font-size: 0.4rem!important} .content-text{font-size: 0.8rem!important;width:100%;height:100%}',
    // set mPDF properties on the fly
    'options' => ['title' => 'Label Sampling Bulk Sisa'],
    // call mPDF methods on the fly
    /*'methods' => [
      'SetHeader'=>['Penimbangan Bulk Sisa - PT Paragon Technology & Innovation'],
      'SetFooter'=>['This Label is computer generated, no signature required'],
    ]*/
    ]);
    $content =  $this->renderPartial('label-sample', [
        'jsonObj' => $jsonObj
    ]);
    $pdf->content = $content;

    /*------------------------------------*/
    Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
    $headers = Yii::$app->response->headers;
    $headers->add('Content-Type', 'application/pdf');
    /*------------------------------------*/

    // return the pdf output as per the destination setting
    return $pdf->render();
  }

  public function actionUpdateWeight($obj)
  {
    $jsonObj = json_decode(base64_decode($obj));
    $jsonObj->countStorage = $this->queryCountTotalStorage($jsonObj->nomo,$jsonObj->snfg);
    $jsonObj->sumQty = $this->querySumQty($jsonObj->nomo,$jsonObj->snfg);
    if ($jsonObj->countStorage > 0 && $jsonObj->sumQty['sum'] > 0) {
      $add = $this->queryInsBulkSisa($jsonObj);
    }
    $res = $this->queryUpdBulkSisaWeight($jsonObj);
    if ($jsonObj->status == 'Siap Dikemas') {
      // $status = $this->queryUpdStatusBulkByNOMO($jsonObj->nomo,'Siap Dikemas','in SPR',null);
      $update = $this->queryUpdStatusBulkSisa($jsonObj->nomo,$jsonObj->snfg,'Siap Dikemas','in SPR',null);
      $update = $this->queryUpdDateRejected6Month($jsonObj->nomo,$jsonObj->snfg);
    }
    if (!$res) {
      return 1;
    }else{
      return 0;
    }
  }

  public function actionUpdateKonfSpr($obj)
  {
    $jsonObj = json_decode(base64_decode($obj));
    $lastSNFG = $this->querySelIndexLastFg();
    $index = explode('/',$lastSNFG['snfg'])[1][0];
    $jsonObj->index = $index;
    $count = $this->querySelCountFgBSList($jsonObj);
    $countBS = $this->querySelLastCountBSList($jsonObj);
    if ($count != null) {
      $lastItem = explode('/',$count['snfg']);
    }else{
      $lastItem = null;
    }
    if ($countBS != null) {
      $lastItemBS = explode('/',$countBS['snfg_baru']);
    }else{
      $lastItemBS = null;
    }
    if ($jsonObj->demands != '-') {
      if ($lastItem != null) {
        $lastCount = (int) filter_var($lastItem[1], FILTER_SANITIZE_NUMBER_INT);
        $lastCountBS = (int) filter_var($lastItemBS[1], FILTER_SANITIZE_NUMBER_INT);
        if ($lastCount > $lastCountBS) {
          $fixLast = $lastCount;
        }else{
          $fixLast = $lastCountBS;
        }
      }else{
        $fixLast = 0;
      }
      $jsonObj->snfg_baru = $jsonObj->demands.'/BS'.$index.''.str_pad(($fixLast+1), 4, '0', STR_PAD_LEFT);
    }
    $res = $this->queryUpdKonfSPR($jsonObj);
    if (!$res) {
      return 1;
    }else{
      return 0;
    }
  }

  public function actionUpdateJadwal($obj)
  {
    $jsonObj = json_decode(base64_decode($obj));
    $res = $this->queryUpdJadwalPlanner($jsonObj);
    if (!$res) {
      $update = $this->queryUpdStatusBulkBySNFG($jsonObj->snfg,'Rework','in QC Bulk',null);
      return 1;
    }else{
      return 0;
    }
  }

  public function actionDeleteBulkSisa($nomo,$snfg)
  {
    $res = $this->queryDelBulkSisa($nomo,$snfg);
    if (!$res) {
      return 1;
    }else{
      return 0;
    }
  }

  public function actionDownloadPpc($sediaan)
  {
    date_default_timezone_set('Asia/Jakarta');
    /*$file = Yii::getAlias('@dirWeb')."/uploads/template_bulk_sisa_ppc.xls";
    $newfile = Yii::getAlias('@dirWeb')."/uploads/list_bulk_sisa_".date('Y-m-d').".xls";
    if (!copy($file, $newfile)) {
        echo "Failed to copy";
    }*/
    // $bulksisa = $this->querySelAllBulkSisa();
    /*$fp=fopen(Yii::getAlias('@dirWeb')."/uploads/list_bulk_sisa_".date('Y-m-d').".xls","a+"); //use a+ instead of w
    for ($i=0; $i < count($bulksisa); $i++) {
      fputcsv($fp, array('',$bulksisa[$i]['sediaan'], $bulksisa[$i]['streamline'], ' ', ' ', ' ', ' ',' ',' ',' ',$bulksisa[$i]['line_kemas_1'], $bulksisa[$i]['line_kemas_2'],' ',' ',' ',
      $bulksisa[$i]['kode_jadwal'],' ','-',$bulksisa[$i]['snfg_baru'],$bulksisa[$i]['snfg_baru'],$bulksisa[$i]['kode_bulk'],explode('/',$bulksisa[$i]['snfg_baru'])[0],
      $bulksisa[$i]['nama_bulk'],$bulksisa[$i]['nama_fg_baru'],$bulksisa[$i]['qty'],' ',' ',' ',' ',' ',' ',' ',' ',' ',$bulksisa[$i]['no_batch_baru'],' ',' ',' ',' ',' ',' ',' ',' '));
      echo $bulksisa[$i]['sediaan']."<br>";
    }
    fclose($fp);*/
    $spreadsheet = \PHPExcel_IOFactory::createReader('Excel5');
    $objExcel = $spreadsheet->load(Yii::getAlias('@dirWeb')."/uploads/template_bulk_sisa_ppc.xls");

    $objExcel->setActiveSheetIndex(0);
    if ($sediaan == 'all') {
      $arraySediaan = array('L','P','S','V');
    }else if ($sediaan == 'liquid') {
      $arraySediaan = array('L');
    }else if ($sediaan == 'powder') {
      $arraySediaan = array('P');
    }else if ($sediaan == 'semsol') {
      $arraySediaan = array('S');
    }else{
      $arraySediaan = array('V');
    }
    $bulksisa = $this->querySelAllBulkSisaForDownload($sediaan,"in PPC");

    for ($i=0; $i < count($bulksisa); $i++) {
      if (in_array($bulksisa[$i]['sediaan'],$arraySediaan) && $bulksisa[$i]['pic'] == 'in PPC') {
        $master_data_product = $this->querySelMasterDataProduct($bulksisa[$i]['koitem_bulk']);
        $master_data_koli = $this->querySelMasterDataKoli($bulksisa[$i]['koitem_fg']);
        $sumQty = $this->querySumQty($bulksisa[$i]['nomo'],$bulksisa[$i]['snfg']);
        $netto_refil = $this->querySelNettoRefil(explode("/",$bulksisa[$i]['snfg'])[0],$bulksisa[$i]['kode_bulk']);
        $qty_karbox = $master_data_koli['qty'] ?? 0;
        $totalQty = $sumQty['sum'];
        $fg_pcs = floor(($totalQty-0.01*$totalQty)*1000/($netto_refil['child_1_qty']*$qty_karbox))*$qty_karbox;
        $tgl_timbang = strtotime($bulksisa[$i]['tgl_timbang']);
        $now = time();
        $diff = abs($now - $tgl_timbang);
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
        if ($months < 1) {
          $weeks = floor($days/7);
          if ($weeks < 1) {
            if ($days < 1) {
              $bulksisa[$i]['ages'] = $hours." Jam";
            }else{
              $bulksisa[$i]['ages'] = $days." Hari";
            }
          }else{
            $bulksisa[$i]['ages'] = $weeks." Minggu";
          }
        }else{
          $bulksisa[$i]['ages'] = $months." Bulan";
        }
        date_default_timezone_set('Asia/Jakarta');
        $isPrint = True;
        $numAge = (int) filter_var($bulksisa[$i]['ages'], FILTER_SANITIZE_NUMBER_INT);
        if (strpos($bulksisa[$i]['ages'],'Bulan') !== false) {
          if ($numAge > 5) {
            $bulksisa[$i]['flagAging'] = 'true';
          }else{
            $bulksisa[$i]['flagAging'] = 'false';
          }
        }else if (strpos($bulksisa[$i]['ages'],'Tahun') !== false) {
          $bulksisa[$i]['flagAging'] = 'true';
        }else{
          $bulksisa[$i]['flagAging'] = 'false';
        }

        $objExcel->getActiveSheet()->setCellValue('B'.($i+2),$bulksisa[$i]['sediaan']);
        $objExcel->getActiveSheet()->setCellValue('C'.($i+2),$bulksisa[$i]['streamline']);
        $objExcel->getActiveSheet()->setCellValue('K'.($i+2),$bulksisa[$i]['line_kemas_1']);
        $objExcel->getActiveSheet()->setCellValue('L'.($i+2),$bulksisa[$i]['line_kemas_2']);
        $objExcel->getActiveSheet()->setCellValue('P'.($i+2),$bulksisa[$i]['kode_jadwal']);
        $objExcel->getActiveSheet()->setCellValue('R'.($i+2),'-');
        $objExcel->getActiveSheet()->setCellValue('S'.($i+2),$bulksisa[$i]['snfg_baru']);
        $objExcel->getActiveSheet()->setCellValue('T'.($i+2),$bulksisa[$i]['snfg_baru']);
        $objExcel->getActiveSheet()->setCellValue('U'.($i+2),$bulksisa[$i]['kode_bulk']);
        $objExcel->getActiveSheet()->setCellValue('V'.($i+2),explode('/',$bulksisa[$i]['snfg_baru'])[0]);
        $objExcel->getActiveSheet()->setCellValue('W'.($i+2),$bulksisa[$i]['nama_bulk']);
        $objExcel->getActiveSheet()->setCellValue('X'.($i+2),$bulksisa[$i]['nama_fg_baru']);
        $objExcel->getActiveSheet()->setCellValue('Y'.($i+2),$bulksisa[$i]['qty']);
        if ($bulksisa[$i]['sediaan'] == 'P') {
          $objExcel->getActiveSheet()->setCellValue('AD'.($i+2),$fg_pcs);
          $objExcel->getActiveSheet()->setCellValue('AE'.($i+2),$fg_pcs);
        }else{
          $objExcel->getActiveSheet()->setCellValue('AE'.($i+2),$fg_pcs);
        }
        $objExcel->getActiveSheet()->setCellValue('AI'.($i+2),$bulksisa[$i]['no_batch_baru']);
        if ($bulksisa[$i]['flagAging'] == 'true') {
          $objExcel->getActiveSheet()
          ->getStyle('A'.($i+2).':AQ'.($i+2))
          ->getFill()
          ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
          ->getStartColor()->setRGB('FFCFCC');
        }
      }
    }

    $writer = \PHPExcel_IOFactory::createWriter($objExcel, 'Excel5');
    $writer->save(Yii::getAlias('@dirWeb')."/uploads/bulk_sisa_ppc.xls");
    Yii::$app->response->sendFile(Yii::getAlias('@dirWeb')."/uploads/bulk_sisa_ppc.xls");
  }

  public function actionDownloadSpr($sediaan)
  {
    date_default_timezone_set('Asia/Jakarta');
    /*$file = Yii::getAlias('@dirWeb')."/uploads/template_bulk_sisa_ppc.xls";
    $newfile = Yii::getAlias('@dirWeb')."/uploads/list_bulk_sisa_".date('Y-m-d').".xls";
    if (!copy($file, $newfile)) {
        echo "Failed to copy";
    }*/
    /*$fp=fopen(Yii::getAlias('@dirWeb')."/uploads/list_bulk_sisa_".date('Y-m-d').".xls","a+"); //use a+ instead of w
    for ($i=0; $i < count($bulksisa); $i++) {
      fputcsv($fp, array('',$bulksisa[$i]['sediaan'], $bulksisa[$i]['streamline'], ' ', ' ', ' ', ' ',' ',' ',' ',$bulksisa[$i]['line_kemas_1'], $bulksisa[$i]['line_kemas_2'],' ',' ',' ',
      $bulksisa[$i]['kode_jadwal'],' ','-',$bulksisa[$i]['snfg_baru'],$bulksisa[$i]['snfg_baru'],$bulksisa[$i]['kode_bulk'],explode('/',$bulksisa[$i]['snfg_baru'])[0],
      $bulksisa[$i]['nama_bulk'],$bulksisa[$i]['nama_fg_baru'],$bulksisa[$i]['qty'],' ',' ',' ',' ',' ',' ',' ',' ',' ',$bulksisa[$i]['no_batch_baru'],' ',' ',' ',' ',' ',' ',' ',' '));
      echo $bulksisa[$i]['sediaan']."<br>";
    }
    fclose($fp);*/
    $spreadsheet = \PHPExcel_IOFactory::createReader('Excel5');
    $objExcel = $spreadsheet->load(Yii::getAlias('@dirWeb')."/uploads/template_bulk_sisa_spr.xls");

    $objExcel->setActiveSheetIndex(0);
    if ($sediaan == 'all') {
      $arraySediaan = array('L','P','S','V');
    }else if ($sediaan == 'liquid') {
      $arraySediaan = array('L');
    }else if ($sediaan == 'powder') {
      $arraySediaan = array('P');
    }else if ($sediaan == 'semsol') {
      $arraySediaan = array('S');
    }else{
      $arraySediaan = array('V');
    }
    $bulksisa = $this->querySelAllBulkSisaForDownload($sediaan,"in SPR");

    for ($i=0; $i < count($bulksisa); $i++) {
      if (in_array($bulksisa[$i]['sediaan'],$arraySediaan) && $bulksisa[$i]['pic'] == 'in SPR') {
        $master_data_product = $this->querySelMasterDataProduct($bulksisa[$i]['koitem_bulk']);
        $master_data_koli = $this->querySelMasterDataKoli($bulksisa[$i]['koitem_fg']);
        $sumQty = $this->querySumQty($bulksisa[$i]['nomo'],$bulksisa[$i]['snfg']);
        $netto_refil = $this->querySelNettoRefil(explode("/",$bulksisa[$i]['snfg'])[0],$bulksisa[$i]['kode_bulk']);
        $qty_karbox = $master_data_koli['qty'] ?? 0;
        $totalQty = $sumQty['sum'];
        $totalSpace = $netto_refil['child_1_qty']*$qty_karbox;
        if ($totalSpace == 0) {
          $fg_pcs = 0;
        }else{
          $fg_pcs = floor(($totalQty-0.01*$totalQty)*1000/($netto_refil['child_1_qty']*$qty_karbox))*$qty_karbox;
        }
        $fg_pack = ceil(1.05*$fg_pcs);
        $tgl_timbang = strtotime($bulksisa[$i]['tgl_timbang']);
        $now = time();
        $diff = abs($now - $tgl_timbang);
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
        if ($months < 1) {
          $weeks = floor($days/7);
          if ($weeks < 1) {
            if ($days < 1) {
              $bulksisa[$i]['ages'] = $hours." Jam";
            }else{
              $bulksisa[$i]['ages'] = $days." Hari";
            }
          }else{
            $bulksisa[$i]['ages'] = $weeks." Minggu";
          }
        }else{
          $bulksisa[$i]['ages'] = $months." Bulan";
        }
        date_default_timezone_set('Asia/Jakarta');
        $isPrint = True;
        $numAge = (int) filter_var($bulksisa[$i]['ages'], FILTER_SANITIZE_NUMBER_INT);
        if (strpos($bulksisa[$i]['ages'],'Bulan') !== false) {
          if ($numAge > 5) {
            $bulksisa[$i]['flagAging'] = 'true';
          }else{
            $bulksisa[$i]['flagAging'] = 'false';
          }
        }else if (strpos($bulksisa[$i]['ages'],'Tahun') !== false) {
          $bulksisa[$i]['flagAging'] = 'true';
        }else{
          $bulksisa[$i]['flagAging'] = 'false';
        }

        $objExcel->getActiveSheet()->setCellValue('A'.($i+2),'W'.date("W", strtotime($bulksisa[$i]['tgl_timbang'])));
        $objExcel->getActiveSheet()->setCellValue('B'.($i+2),$bulksisa[$i]['sediaan']);
        $objExcel->getActiveSheet()->setCellValue('C'.($i+2),$bulksisa[$i]['zona']);
        $objExcel->getActiveSheet()->setCellValue('D'.($i+2),$bulksisa[$i]['streamline']);
        $objExcel->getActiveSheet()->setCellValue('E'.($i+2),$bulksisa[$i]['kode_bulk']);
        $objExcel->getActiveSheet()->setCellValue('F'.($i+2),$bulksisa[$i]['nomo']);
        $objExcel->getActiveSheet()->setCellValue('G'.($i+2),$bulksisa[$i]['koitem_fg']);
        $objExcel->getActiveSheet()->setCellValue('H'.($i+2),$bulksisa[$i]['snfg']);
        $objExcel->getActiveSheet()->setCellValue('I'.($i+2),$bulksisa[$i]['nama_bulk']);
        $objExcel->getActiveSheet()->setCellValue('J'.($i+2),$bulksisa[$i]['qty']);
        $objExcel->getActiveSheet()->setCellValue('K'.($i+2),$fg_pcs);
        $objExcel->getActiveSheet()->setCellValue('L'.($i+2),$fg_pack);
        $objExcel->getActiveSheet()->setCellValue('M'.($i+2),$bulksisa[$i]['no_batch']);
        $objExcel->getActiveSheet()->setCellValue('N'.($i+2),$bulksisa[$i]['no_batch'].'X');
        if ($bulksisa[$i]['flagAging'] == 'true') {
          $objExcel->getActiveSheet()
          ->getStyle('A'.($i+2).':N'.($i+2))
          ->getFill()
          ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
          ->getStartColor()->setRGB('FFCFCC');
        }
      }
    }

    $writer = \PHPExcel_IOFactory::createWriter($objExcel, 'Excel5');
    $writer->save(Yii::getAlias('@dirWeb')."/uploads/bulk_sisa_spr.xls");
    Yii::$app->response->sendFile(Yii::getAlias('@dirWeb')."/uploads/bulk_sisa_spr.xls");
  }

  public function actionDeleteBulkSisaJadwal($snfg){
    $delJadwal = $this->queryDelBulkSisaJadwal($snfg);
    if (!$delJadwal) {
      $delQC = $this->queryDelBulkSisaQC($snfg);
      if (!$delQC) {
        $updateTime = $this->queryUpdDateRejectedNow($snfg);
        $updateTime = $this->queryUpdStatusBulkBySNFG($snfg,'Rejected','in WPM','Due Date');
        return 1;
      }else{
        return 0;
      }
    }else{
      return 0;
    }
  }

  public function actionRejectBulkSisa($snfg, $ket = 'Due Date'){
      $updateTime = $this->queryUpdStatusRejectAging($snfg,$ket);
      if ($updateTime) {
        return 1;
      }else{
        return 0;
      }
  }

  public function actionResetBulkSisa($nomo,$snfg)
  {
    $res = $this->queryUpdResetBulkSisa($nomo,$snfg);
    if (!$res) {
      return 1;
    }else{
      return 0;
    }
  }

  public function actionUpdatePrintLabel($nomo,$snfg,$no_storage, $ket = null)
  {
    $res = $this->queryUpdPrintLabel($nomo,$snfg,$no_storage,$ket);
    if (!$res) {
      return 1;
    }else{
      return 0;
    }
  }

  public function actionGetDataByNomo($nomo)
  {

    if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $frontend_ip = $_SERVER['REMOTE_ADDR'];
    }

    $query = new Query();
    $map_device = $query
    ->from('device_map_rm')
    ->where('device_ip = :device_ip',[':device_ip' => $frontend_ip])
    ->one();

    if ($map_device['device_ip'] != null) {
      if (strpos($nomo, '/BS') !== false) {
        $checkBS = $this->querySelOneBulkSisaBySNFG($nomo);
        $res = $this->querySelSCMPlannerBySNFG($checkBS['snfg_baru']);
        $flow_input_snfgkomponen = $this->querySelFlowInputSNFGKomponen($res['snfg_komponen']);
        if (empty($flow_input_snfgkomponen)) {
          $flow_input_snfg = $this->querySelFlowInputSNFGLike($res['snfg']);
        }
        $res['nobatch'] = $checkBS['no_batch_baru'];
        $res['nomo'] = $checkBS['nomo'];
        if (!empty($res)) {
            if (!empty($flow_input_snfg) || !empty($flow_input_snfgkomponen)) {
              if (!empty($flow_input_snfgkomponen)) {
                $res['line'] = $flow_input_snfgkomponen['nama_line'];
              }else if(!empty($flow_input_snfg)){
                $res['line'] = $flow_input_snfg['nama_line'];
              }else{
                return 2;
              }
              $search = $this->querySelOneBulkSisaBySNFGOld($res['snfg']);
              if (!empty($search)) {
                $res['nama_operator'] = $search['nama_op'];
                $res['status'] = $search['status'];
              }
              return base64_encode(json_encode($res));
            }else{
              // return 2 kalau nomo belum selesai kemas
              return 2;
            }
        }else{
          // return 0 kalau nomo tidak ada dalam jadwal planner
          return 0;
        }
      }else{
      $flow_input_mo = $this->querySelFlowInputMO($nomo);
        $res = $this->querySelSCMPlannerByNOMO($nomo);
        $flow_input_snfgkomponen = $this->querySelFlowInputSNFGKomponen($res['snfg_komponen']);
        if (empty($flow_input_snfgkomponen)) {
          $flow_input_snfg = $this->querySelFlowInputSNFGNotLike($res['snfg']);
        }
        if (!empty($flow_input_mo)) {
          $res['nobatch'] = $flow_input_mo['nobatch'];
          if (!empty($res)) {
              if (!empty($flow_input_snfg) || !empty($flow_input_snfgkomponen)) {
                if (!empty($flow_input_snfgkomponen)) {
                  $res['line'] = $flow_input_snfgkomponen['nama_line'];
                }else if(!empty($flow_input_snfg)){
                  $res['line'] = $flow_input_snfg['nama_line'];
                }else{
                  return 2;
                }
                $search = $this->querySelOneBulkSisa($res['nomo'],$res['snfg']);
                if (!empty($search)) {
                  $res['nama_operator'] = $search['nama_op'];
                  $res['status'] = $search['status'];
                }
                return base64_encode(json_encode($res));
              }else{
                // return 2 kalau nomo belum selesai kemas
                return 2;
              }
          }else{
            // return 0 kalau nomo tidak ada dalam jadwal planner
            return 0;
          }
        }else{
            // return 1 kalau nomo tidak ada dalam jadwal olah
            return 1;
        }

      }
    }else{
      // return 3 kalau device tidak punya permission
      return 3;
    }
  }

  public function actionUpdateStatusBulk($snfg){
      $res = $this->queryUpdStatus($snfg);
      if ($res) {
        return 1;
      }else{
        return 0;
      }
  }

  /*Kumpulan Query-Query*/
  public function querySelFlowInputMO($nomo){
    $connection = Yii::$app->db;
    $command = $connection->createCommand("

    SELECT *
    FROM flow_input_mo
    WHERE  nomo = :nomo AND posisi = 'PENGOLAHAN'
    ",
    [
      ':nomo' => $nomo
    ]);

    $res = $command->queryOne();
    return $res;
  }
  public function querySelFlowInputSNFGNotLike($snfg){
    $query = new Query();
    $res = $query
    ->from('flow_input_snfg')
    ->where('snfg = :snfg',[':snfg' => $snfg])
    ->andWhere('snfg not like :like',[':like' => '%/BS%'])
    ->orderBy('datetime_write')
    ->one();
    return $res;
  }
  public function querySelFlowInputSNFGLike($snfg){
    $query = new Query();
    $res = $query
    ->from('flow_input_snfg')
    ->where('snfg = :snfg',[':snfg' => $snfg])
    ->andWhere('snfg like :like',[':like' => '%/BS%'])
    ->orderBy('datetime_write')
    ->one();
    return $res;
  }
  public function querySelFlowInputSNFGKomponen($snfg){
    $query = new Query();
    $res = $query
    ->from('flow_input_snfgkomponen')
    ->where('snfg_komponen = :snfg',[':snfg' => $snfg])
    ->one();
    return $res;
  }
  public function querySelSCMPlannerByNOMO($nomo){
    $query = new Query();
    $res = $query
    ->from('scm_planner')
    ->where('nomo = :nomo',[':nomo' => $nomo])
    ->andWhere('snfg not like :like',[':like' => '%/BS%'])
    ->orderBy('timestamp')
    ->one();
    return $res;
  }
  public function querySelSCMPlannerBySNFG($nomo){
    $query = new Query();
    $res = $query
    ->from('scm_planner')
    ->where('snfg = :snfg',[':snfg' => $nomo])
    ->orderBy('timestamp')
    ->one();
    return $res;
  }

  public function querySelOneBulkSisaBySNFGOld($snfg){
    $query = new Query();
    $res = $query
    ->from('log_bulk_sisa')
    ->where('snfg = :snfg',[':snfg' => $snfg])
    ->orderBy('no_storage desc')
    ->one();
    return $res;
  }
  public function querySelOneBulkSisaBySNFG($snfg){
    $query = new Query();
    $res = $query
    ->from('log_bulk_sisa')
    ->where('snfg_BARU = :snfg',[':snfg' => $snfg])
    ->orderBy('no_storage desc')
    ->one();
    return $res;
  }
  public function querySelOneBulkSisa($nomo,$snfg){
    $connection = Yii::$app->db;
    $command = $connection->createCommand("

    SELECT *
    FROM log_bulk_sisa
    WHERE  nomo = :nomo AND snfg = :snfg
    ",
    [
      ':nomo' => $nomo,
      ':snfg' => $snfg
    ]);

    $res = $command->queryOne();
    return $res;
  }
  public function querySelOneBulkSisaByStorage($nomo,$snfg,$no_storage){
    $connection = Yii::$app->db;
    $command = $connection->createCommand("

    SELECT *
    FROM log_bulk_sisa
    WHERE  nomo = :nomo AND no_storage = :no_storage AND snfg = :snfg
    ",
    [
      ':nomo' => $nomo,
      ':snfg' => $snfg,
      ':no_storage' => $no_storage
    ]);

    $res = $command->queryOne();
    return $res;
  }

  public function querySelManyBulkSisa($nomo,$snfg){
    $query = new Query();
    $res = $query
    ->from('log_bulk_sisa')
    ->where('nomo = :nomo',[':nomo' =>  $nomo])
    ->andWhere('snfg = :snfg',[':snfg' =>  $snfg])
    ->orderBy('(case when status = '."'Siap Dikemas'".' then 0 when status = '."'Waiting List'".' then 1 when status = '."'Waiting Check'".' then 2 when status = '."'On Progress'".' then 3 when status = '."'Rework'".' then 4
    when status = '."'Pending'".' then 5 else 6 end) desc')
    ->all();
    return $res;
  }
  public function querySelQcBulkEntryKomponen($nomo){
    $connection = Yii::$app->db;
    $command = $connection->createCommand("

    SELECT *
    FROM qc_bulk_entry_komponen
    WHERE  nomo = :nomo Order By id DESC
    ",
    [
      ':nomo' => $nomo
    ]);

    $res = $command->queryOne();
    return $res;
  }

  public function querySelAllBulkSisaForDownload($sediaan,$pic){
    $connection = Yii::$app->db;
    if ($sediaan == 'all') {
    $command = $connection->createCommand("
      SELECT distinct on (log_bulk_sisa.nomo) log_bulk_sisa.status as status_bulksisa,log_bulk_sisa.tgl_timbang as tgl_timbang_bulk_sisa,log_bulk_sisa.snfg as snfg_awal,*
      FROM (select * from log_bulk_sisa order by (case when status = 'Siap Dikemas' then 0 when status = 'Waiting List' then 1 when status = 'Waiting Check' then 2 when status = 'On Progress' then 3 when status = 'Rework' then 4
      when status = 'Pending' then 5 else 6 end) desc) as log_bulk_sisa
      JOIN scm_planner ON scm_planner.nomo = log_bulk_sisa.nomo AND scm_planner.snfg = log_bulk_sisa.snfg
      WHERE log_bulk_sisa.status <> 'Belum Ditimbang' AND pic = '".$pic."'
      order by log_bulk_sisa.nomo asc, (case when log_bulk_sisa.status = 'Siap Dikemas' then 0 when log_bulk_sisa.status = 'Waiting List' then 1 when log_bulk_sisa.status = 'Waiting Check' then 2 when log_bulk_sisa.status = 'On Progress' then 3
      when log_bulk_sisa.status = 'Rework' then 4 when log_bulk_sisa.status = 'Pending' then 5 else 6 end) asc
      ");
    }else{
      if ($sediaan == 'liquid') {
        $sed = 'L';
      }else if ($sediaan == 'powder') {
        $sed = 'P';
      }else if ($sediaan == 'semsol') {
        $sed = 'S';
      }else{
        $sed = 'V';
      }
      $command = $connection->createCommand("
      SELECT distinct on (log_bulk_sisa.nomo) log_bulk_sisa.status as status_bulksisa,log_bulk_sisa.tgl_timbang as tgl_timbang_bulk_sisa,log_bulk_sisa.snfg as snfg_awal,*
      FROM (select * from log_bulk_sisa order by (case when status = 'Siap Dikemas' then 0 when status = 'Waiting List' then 1 when status = 'Waiting Check' then 2 when status = 'On Progress' then 3 when status = 'Rework' then 4
      when status = 'Pending' then 5 else 6 end) desc) as log_bulk_sisa
      JOIN scm_planner ON scm_planner.nomo = log_bulk_sisa.nomo AND scm_planner.snfg = log_bulk_sisa.snfg
      WHERE log_bulk_sisa.status <> 'Belum Ditimbang' AND pic = '".$pic."' AND sediaan = '".$sed."'
      order by log_bulk_sisa.nomo asc, (case when log_bulk_sisa.status = 'Siap Dikemas' then 0 when log_bulk_sisa.status = 'Waiting List' then 1 when log_bulk_sisa.status = 'Waiting Check' then 2 when log_bulk_sisa.status = 'On Progress' then 3
      when log_bulk_sisa.status = 'Rework' then 4 when log_bulk_sisa.status = 'Pending' then 5 else 6 end) asc
      ");
    }
    $res = $command->queryAll();
    return $res;
  }
  public function querySelAllBulkSisa(){

    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    SELECT distinct on (log_bulk_sisa.nomo) log_bulk_sisa.status as status_bulksisa,log_bulk_sisa.tgl_timbang as tgl_timbang_bulk_sisa,log_bulk_sisa.snfg as snfg_awal,*
    FROM (select * from log_bulk_sisa order by (case when status = 'Siap Dikemas' then 0 when status = 'Waiting List' then 1 when status = 'Waiting Check' then 2 when status = 'On Progress' then 3 when status = 'Rework' then 4
    when status = 'Pending' then 5 else 6 end) desc) as log_bulk_sisa
    JOIN scm_planner ON scm_planner.nomo = log_bulk_sisa.nomo AND scm_planner.snfg = log_bulk_sisa.snfg
    WHERE log_bulk_sisa.status <> 'Belum Ditimbang'
    order by log_bulk_sisa.nomo asc, (case when log_bulk_sisa.status = 'Siap Dikemas' then 0 when log_bulk_sisa.status = 'Waiting List' then 1 when log_bulk_sisa.status = 'Waiting Check' then 2 when log_bulk_sisa.status = 'On Progress' then 3
    when log_bulk_sisa.status = 'Rework' then 4 when log_bulk_sisa.status = 'Pending' then 5 else 6 end) asc
    ");
    $res = $command->queryAll();
    return $res;
  }
  public function querySelAllScmPlanner(){
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    SELECT log_bulk_sisa.status as status_bulksisa,log_bulk_sisa.tgl_timbang as tgl_timbang_bulk_sisa,*
    FROM log_bulk_sisa
    INNER JOIN scm_planner ON scm_planner.nomo = log_bulk_sisa.nomo
    ");
    $res = $command->queryAll();
    return $res;
  }
  public function querySelAllFgList(){
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
      select distinct on (koitem_fg) koitem_fg, nama_fg from scm_planner where
      length(koitem_fg) > 1 AND
      length(nama_fg) > 1 AND
      koitem_fg not ilike '%BULK%' order by koitem_fg asc
    ");
    $res = $command->queryAll();
    return $res;
  }

  public function queryCountTotalStorage($nomo,$snfg){
    $query = new Query();
    $res = $query
    ->from('log_bulk_sisa')
    ->where('nomo = :nomo AND snfg = :snfg',[':nomo' => $nomo, ':snfg' => $snfg])
    ->count();
    return $res;
  }
  public function queryCountRework(){
    date_default_timezone_set('Asia/Jakarta');
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    SELECT count(*)
    FROM (select distinct on (lbs.nomo) * from log_bulk_sisa lbs join qc_bulk_entry_komponen qc on (lbs.nomo = qc.nomo AND lbs.no_storage = qc.no_storage)
  	where snfg_komponen like '%/BS%' AND lbs.status = 'Rework' AND DATE(qc.timestamp) = '".date('Y-m-d')."'
  	order by lbs.nomo asc, qc.nourut desc) as lfb
    ");
    $res = $command->queryAll();
    return $res;
  }
  public function queryCountAgesBulk(){
    $query = new Query();
    $res = $query
    ->from("(select distinct on (nomo) * from log_bulk_sisa where status <> 'Belum Ditimbang' AND is_expired is null or is_expired = 0 order by nomo asc, (case when log_bulk_sisa.status = 'Siap Dikemas' then 0 when log_bulk_sisa.status = 'Waiting List' then 1 when log_bulk_sisa.status = 'Waiting Check' then 2 when log_bulk_sisa.status = 'On Progress' then 3
    when log_bulk_sisa.status = 'Rework' then 4 when log_bulk_sisa.status = 'Pending' then 5 else 6 end) asc) as lfb")
    ->where("tgl_timbang <  CURRENT_DATE - INTERVAL '5 months'")
    ->count();
    return $res;
  }
  public function queryCountRejected(){
    date_default_timezone_set('Asia/Jakarta');
    $query = new Query();
    $res = $query
    ->from("(select distinct on (nomo) * from log_bulk_sisa where snfg_baru is not null AND status = 'Rejected' AND DATE(tgl_timbang) = '".date('Y-m-d')."'
    AND nomo not in (select nomo from log_bulk_sisa where status <> 'Rejected' group by nomo)) as bulk_sisa")
    ->count();
    return $res;
  }

  public function querySumQty($nomo,$snfg){
    $query = new Query();
    $res = $query
    ->select('sum(qty)')
    ->from('log_bulk_sisa')
    ->where('nomo = :nomo AND snfg = :snfg',[':nomo' => $nomo, ':snfg' => $snfg])
    ->groupBy('nomo, snfg')
    ->one();
    return $res;
  }

  public function querySelNettoRefil($snfg,$kode_bulk){
    $query = new Query();
    $res = $query
    ->from('master_data_netto_refill')
    ->where('child = :kode_bulk AND fg = :fg',[':kode_bulk' => $kode_bulk, ':fg' => $snfg])
    ->one();
    return $res;
  }
  public function querySelHetItem($koitem_fg){
    /*$query = new Query();
    $res = $query
    ->select("select 'pt.default_code','pt.korporate_code','pt.name','ppi.fixed_price','ppi.pricelist_name'")
    ->from("'mdm.product_pricelist_item ppi'")
    ->innerJoin('mdm.product_template pt', 'ppi.product_tmpl_id = pt.id')
    ->where('ppi.pricelist_name = :pricelist_name AND ppi.korporate_code = :koitem_fg',[':pricelist_name' => 'Public Pricelist', ':koitem_fg' => $koitem_fg])
    ->one();
    return $res;*/

    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    select pt.default_code,pt.korporate_code,pt.name,ppi.fixed_price,ppi.pricelist_name from mdm.product_pricelist_item ppi
    left join mdm.product_template pt on ppi.product_tmpl_id = pt.id
    where ppi.pricelist_name = 'Public Pricelist' AND korporate_code = '".$koitem_fg."'
    order by fixed_price desc
    ");

    $res = $command->queryOne();
    return $res;
  }
  public function querySelCountFgBSList($obj){
    $query = new Query();
    $res = $query
    ->select('snfg')
    ->from('scm_planner')
    ->where('snfg like :demands',[':demands' => '%'.$obj->demands.'/BS'.$obj->index.'%'])
    ->orderBy('timestamp DESC')
    ->one();
    return $res;
  }
  public function querySelLastCountBSList($obj){
    $query = new Query();
    $res = $query
    ->select('snfg_baru')
    ->from('log_bulk_sisa')
    ->where('snfg_baru like :demands',[':demands' => '%'.$obj->demands.'/BS'.$obj->index.'%'])
    ->orderBy('tgl_timbang DESC')
    ->one();
    return $res;
  }
  public function querySelIndexLastFg(){
    $query = new Query();
    $res = $query
    ->select('snfg')
    ->from('scm_planner')
    ->where('snfg like :slash',[':slash' => '%/%'])
    ->andWhere('snfg not like :bs',[':bs' => '%/BS%'])
    ->andWhere('snfg not like :l',[':l' => '%/L%'])
    ->orderBy('timestamp DESC')
    ->one();
    return $res;
  }
  public function querySelMasterDataProduct($koitem_bulk){
    $query = new Query();
    $res = $query
    ->from('master_data_product')
    ->where('koitem_bulk = :koitem_bulk',[':koitem_bulk' => $koitem_bulk])
    ->one();
    return $res;
  }
  public function querySelMasterDataKoli($koitem_fg){
    $query = new Query();
    $res = $query
    ->from('master_data_koli')
    ->where('koitem = :koitem_fg',[':koitem_fg' => $koitem_fg])
    ->one();
    return $res;
  }

  public function queryCheckJadwalPlanner($snfg_baru, $kode_bulk){
    $query = new Query();
    $res = $query
    ->from('scm_planner')
    ->where('snfg = :snfg_baru',[':snfg_baru' => $snfg_baru])
    ->andWhere('koitem_bulk = :kode_bulk',[':kode_bulk' => $kode_bulk])
    ->one();
    return $res;
  }
  public function queryCheckKemasDone($snfg_baru){
    $query = new Query();
    $res = $query
    ->from('flow_input_snfg')
    ->where('snfg = :snfg_baru',[':snfg_baru' => $snfg_baru])
    ->one();
    return $res;
  }
  public function queryCheckQC($nomo, $snfg){
    $query = new Query();
    $res = $query
    ->select("DISTINCT on(no_storage) *")
    ->from('(Select * from qc_bulk_entry_komponen order by nourut desc) as qc_bulk_entry_komponen')
    ->where('nomo = :nomo AND snfg_komponen = :snfg',[':nomo' => $nomo,':snfg' => $snfg])
    ->orderBy('no_storage ASC')
    ->all();
    return $res;
  }

  public function queryInsBulkSisa($obj){
    date_default_timezone_set('Asia/Jakarta');
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    INSERT INTO log_bulk_sisa (nomo,kode_bulk,snfg,no_batch,nama_op,tgl_timbang,status,zona)
    VALUES (:nomo,:kode_bulk,:snfg, :no_batch,:nama_op,:tgl_timbang,:status,:zona)
    ",
    [
      ':nomo' => $obj->nomo,
      ':snfg' => $obj->snfg,
      ':kode_bulk' => $obj->kode_bulk,
      ':no_batch' => $obj->no_batch,
      ':nama_op' => str_replace(' ', '', $obj->nama_operator),
      ':tgl_timbang' => date("Y-m-d H:i:s"),
      ':status' => 'Belum Ditimbang',
      ':zona' => $obj->zona,
    ]);

    $res = $command->queryOne();
    return $res;
  }

  public function queryUpdPrintLabel($nomo,$snfg,$no_storage,$ket){
    $connection = Yii::$app->db;
    if ($ket == 'isExpired') {
      $command = $connection->createCommand("
      UPDATE log_bulk_sisa SET is_printed = :is_printed, is_expired = :is_expired
      WHERE nomo = :nomo AND no_storage = :no_storage AND snfg = :snfg
      ",
      [
        ':is_printed' => 1,
        ':is_expired' => 1,
        ':nomo' => $nomo,
        ':snfg' => $snfg,
        ':no_storage' => $no_storage,
      ]);
    }else{
      $command = $connection->createCommand("
      UPDATE log_bulk_sisa SET is_printed = :is_printed
      WHERE nomo = :nomo AND no_storage = :no_storage AND snfg = :snfg
      ",
      [
        ':is_printed' => 1,
        ':nomo' => $nomo,
        ':snfg' => $snfg,
        ':no_storage' => $no_storage,
      ]);
    }
    $res = $command->queryOne();
    return $res;
  }
  public function queryUpdBulkSisa($obj){
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    UPDATE log_bulk_sisa SET nama_op = :nama_op
    WHERE nomo = :nomo AND kode_bulk = :kode_bulk AND snfg = :snfg
    ",
    [
      ':nama_op' => $obj->nama_operator,
      ':nomo' => $obj->nomo,
      ':snfg' => $obj->snfg,
      ':kode_bulk' => $obj->kode_bulk,
    ]);

    $res = $command->queryOne();
    return $res;
  }
  public function queryUpdBulkSisaWeight($obj)  {
    date_default_timezone_set('Asia/Jakarta');
    if ($obj->status == 'Siap Dikemas') {
      $pic = 'in SPR';
      $tgl_rejected = date("Y-m-d H:i:s",strtotime('+6 months'));
    }else if ($obj->status == 'Rejected'){
      $pic = 'in WPM';
      $tgl_rejected = date("Y-m-d H:i:s");
    }
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    UPDATE log_bulk_sisa SET qty = :qty, tgl_timbang = :tgl_timbang, datetime_rejected = :tgl_rejected, status = :status, pic = :pic, no_storage = :no_storage,keterangan = :keterangan
    WHERE nomo = :nomo AND snfg = :snfg AND kode_bulk = :kode_bulk AND status = 'Belum Ditimbang'
    ",
    [
      ':qty' => $obj->qty,
      ':pic' => $pic,
      ':status' => $obj->status,
      ':tgl_timbang' => date("Y-m-d H:i:s"),
      ':tgl_rejected' => $tgl_rejected,
      ':nomo' => $obj->nomo,
      ':snfg' => $obj->snfg,
      ':keterangan' => $obj->keterangan,
      ':no_storage' => $this->queryCountTotalStorage($obj->nomo,$obj->snfg),
      ':kode_bulk' => $obj->kode_bulk,
    ]);

    $res = $command->queryOne();
    return $res;
  }
  public function queryUpdKonfSPR($obj){
    $connection = Yii::$app->db;
    if ($obj->konfMPS == 1 && $obj->konfPack == 1 && $obj->demands != '-') {
      $pic = 'in PPC';
      $status = 'Waiting List';
      $command = $connection->createCommand("
      UPDATE log_bulk_sisa SET konf_MPS = :konfMPS, konf_packaging = :konfPack, snfg_baru = :snfg_baru, status = :status, pic = :pic, no_batch_baru = :no_batch_baru, nama_fg_baru = :nama_fg_baru
      WHERE nomo = :nomo
      ",
      [
        ':konfMPS' => $obj->konfMPS,
        ':konfPack' => $obj->konfPack,
        ':snfg_baru' => $obj->snfg_baru,
        ':nama_fg_baru' => $obj->nama_demands,
        ':nomo' => $obj->nomo,
        ':pic' => $pic,
        ':status' => $status,
        ':no_batch_baru' => $obj->no_batch_baru,
      ]);
    }else{
      $command = $connection->createCommand("
      UPDATE log_bulk_sisa SET konf_MPS = :konfMPS, konf_packaging = :konfPack, snfg_baru = :snfg_baru
      WHERE nomo = :nomo
      ",
      [
        ':konfMPS' => $obj->konfMPS,
        ':konfPack' => $obj->konfPack,
        ':snfg_baru' => $obj->demands,
        ':nomo' => $obj->nomo,
      ]);
    }
    $res = $command->queryOne();
    return $res;
  }
  public function queryUpdJadwalPlanner($obj){
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    UPDATE scm_planner SET start = :start, due = :due, week = :week, line_olah_premix = :olah_premix, line_olah = :olah_1, line_olah_2 = :olah_2, line_adjust_olah_1 = :adjust_olah_1,
    line_adjust_olah_2 = :adjust_olah_2, line_kemas_1 = :kemas_1, line_kemas_2 = :kemas_2
    WHERE snfg = :snfg
    ",
    [
      ':start' => $obj->startDate,
      ':due' => $obj->dueDate,
      ':week' => $obj->week,
      ':olah_premix' => $obj->olahPremix,
      ':olah_1' => $obj->olah1,
      ':olah_2' => $obj->olah2,
      ':adjust_olah_1' => $obj->adjustOlah1,
      ':adjust_olah_2' => $obj->adjustOlah2,
      ':kemas_1' => $obj->kemas1,
      ':kemas_2' => $obj->kemas2,
      ':snfg' => $obj->snfg,
    ]);
    $res = $command->queryOne();
  }
  public function queryUpdStatusBulkSisa($nomo,$snfg,$status,$pic,$keterangan){
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    UPDATE log_bulk_sisa SET status = :status, pic = :pic, keterangan = :keterangan
    WHERE nomo = :nomo AND snfg = :snfg
    ",
    [
      ':status' => $status,
      ':pic' => $pic,
      ':nomo' => $nomo,
      ':snfg' => $snfg,
      ':keterangan' => $keterangan,
    ]);

    $res = $command->queryAll();
    return $res;
  }
  public function queryUpdStatusBulkSisaByStorage($nomo,$snfg,$status,$pic,$storage){
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    UPDATE log_bulk_sisa SET status = :status, pic = :pic
    WHERE nomo = :nomo AND snfg = :snfg AND no_storage = :storage
    ",
    [
      ':status' => $status,
      ':pic' => $pic,
      ':nomo' => $nomo,
      ':snfg' => $snfg,
      ':storage' => $storage,
    ]);

    $res = $command->queryAll();
    return $res;
  }
  public function queryUpdStatusBulkByNOMO($nomo,$status,$pic,$keterangan){
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    UPDATE log_bulk_sisa SET status = :status, pic = :pic, keterangan = :keterangan
    WHERE nomo = :nomo
    ",
    [
      ':status' => $status,
      ':pic' => $pic,
      ':nomo' => $nomo,
      ':keterangan' => $keterangan,
    ]);

    $res = $command->queryAll();
    return $res;
  }
  public function queryUpdStatusBulkBySNFG($snfg,$status,$pic,$keterangan){
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    UPDATE log_bulk_sisa SET status = :status, pic = :pic, keterangan = :keterangan
    WHERE snfg_baru = :snfg
    ",
    [
      ':snfg' => $snfg,
      ':status' => $status,
      ':pic' => $pic,
      ':keterangan' => $keterangan,
    ]);

    $res = $command->queryAll();
    return $res;
  }
  public function queryUpdResetBulkSisa($nomo,$snfg){
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    UPDATE log_bulk_sisa SET pic = :pic, status = :status, konf_mps =:konf_mps,
    konf_packaging = :konf_packaging, no_batch_baru = :no_batch_baru, snfg_baru = :snfg_baru,
    nama_fg_baru = :nama_fg_baru
    WHERE nomo = :nomo AND snfg = :snfg
    ",
    [
      ':nomo' => $nomo,
      ':snfg' => $snfg,
      ':pic' => 'in SPR',
      ':status' => 'Siap Dikemas',
      ':konf_mps' => 0,
      ':konf_packaging' => 0,
      ':no_batch_baru' => null,
      ':snfg_baru' => null,
      ':nama_fg_baru' => null,
    ]);

    $res = $command->queryOne();
    return $res;
  }
  public function queryUpdDateRejectedNow($snfg){
    date_default_timezone_set('Asia/Jakarta');
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    UPDATE log_bulk_sisa SET datetime_rejected = :datetime_rejected
    WHERE snfg_baru = :snfg
    ",
    [
      ':datetime_rejected' => date("Y-m-d"),
      ':snfg' => $snfg,
    ]);

    $res = $command->queryOne();
    return $res;
  }
  public function queryUpdDateRejected6Month($nomo,$snfg){
    date_default_timezone_set('Asia/Jakarta');
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    UPDATE log_bulk_sisa SET datetime_rejected = :datetime_rejected
    WHERE nomo = :nomo AND snfg = :snfg
    ",
    [
      ':datetime_rejected' => date("Y-m-d",strtotime('+6 months')),
      ':nomo' => $nomo,
      ':snfg' => $snfg,
    ]);

    $res = $command->queryOne();
    return $res;
  }
  public function queryUpdStatusRejectAging($snfg,$ket){
    date_default_timezone_set('Asia/Jakarta');
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    UPDATE log_bulk_sisa SET status = :status, pic = :pic, keterangan = :keterangan, datetime_rejected = :datetime_rejected
    WHERE snfg = :snfg
    ",
    [
      ':snfg' => $snfg,
      ':status' => 'Rejected',
      ':pic' => 'in WPM',
      ':keterangan' => $ket,
      ':datetime_rejected' => date("Y-m-d"),
    ]);

    $res = $command->queryAll();
    return $res;
  }
  public function queryUpdStatus($snfg){
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    UPDATE log_bulk_sisa SET status = :status, pic = :pic
    WHERE snfg = :snfg
    ",
    [
      ':snfg' => $snfg,
      ':status' => 'Destroyed',
      ':pic' => '-',
    ]);

    $res = $command->queryAll();
    return $res;
  }

  public function queryDelBulkSisa($nomo,$snfg){
     $connection = Yii::$app->db;
     $command = $connection->createCommand("
     DELETE FROM log_bulk_sisa WHERE nomo = :nomo AND snfg = :snfg;
     ",
     [
       ':nomo' => $nomo,
       ':snfg' => $snfg,
     ]);
     $res = $command->queryOne();
     return $res;
  }
  public function queryDelBulkSisaSNFG($snfg){
     $connection = Yii::$app->db;
     $command = $connection->createCommand("
     DELETE FROM log_bulk_sisa WHERE snfg = :snfg;
     ",
     [
       ':snfg' => $snfg,
     ]);
     $res = $command->queryOne();
     return $res;
  }
  public function queryDelBulkSisaJadwal($snfg){
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    DELETE FROM scm_planner WHERE snfg = :snfg;
    ",
    [
      ':snfg' => $snfg,
    ]);
    $res = $command->queryOne();
    return $res;
  }
  public function queryDelBulkSisaQC($snfg){
    $connection = Yii::$app->db;
    $command = $connection->createCommand("
    DELETE FROM qc_bulk_entry_komponen WHERE snfg_komponen = :snfg;
    ",
    [
      ':snfg' => $snfg,
    ]);
    $res = $command->queryOne();
    return $res;
  }
}
