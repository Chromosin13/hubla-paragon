<?php

namespace app\controllers;

use Yii;
use app\models\ScheduleNotHit;
use app\models\LossTree;
use app\models\LossTreeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LossTreeController implements the CRUD actions for LossTree model.
 */
class LossTreeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LossTree models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LossTreeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LossTree model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LossTree model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($snfg)
    {
        $check_losstree = LossTree::find()->where(['snfg'=>$snfg])->one();
        if(!empty($check_losstree)){
            return $this->redirect(['update','id'=>$check_losstree->id]);
        }
        $model = new LossTree();

        $schedule_nh = ScheduleNotHit::find()->where(['snfg'=>$snfg])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['schedule-not-hit/index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'schedule_nh' => $schedule_nh,
            ]);
        }
    }

    /**
     * Updates an existing LossTree model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $schedule_nh = ScheduleNotHit::find()->where(['snfg'=>$model->snfg])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['schedule-not-hit/index']);
            // return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'schedule_nh' => $schedule_nh,

            ]);
        }
    }

    /**
     * Deletes an existing LossTree model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LossTree model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LossTree the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LossTree::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
