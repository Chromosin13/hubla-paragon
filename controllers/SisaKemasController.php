<?php

namespace app\controllers;

use Yii;
use app\models\SisaKemas;
use app\models\FlowInputSnfg;
use app\models\SisaKemasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SisaKemasController implements the CRUD actions for SisaKemas model.
 */
class SisaKemasController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SisaKemas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SisaKemasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }




    /**
     * Displays a single SisaKemas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SisaKemas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SisaKemas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionAddData()
    {

        // Get Posted Data Value
        if ( isset($_POST['snfg'] ))
        {

            $snfg = $_POST['snfg'];

        }



        $flowinputsnfg_array = FlowInputSnfg::findOne([
                'snfg' => $snfg,
        ]);

        if(!empty($flowinputsnfg_array->snfg)){
            return $this->redirect(['add','snfg'=>$snfg]);            
        }else{
            echo 'SNFG Belum Scan Station Kemas';
        }



        // Check Master Data Konfigurasi
        // $konfigurasi = KonfigurasiStandar::findOne([
        //         'koitem' => $koitem,
        // ]);

        // if(empty($konfigurasi->id)){

        //     // echo "BELUM memiliki konfigurasi, Klik OK untuk membuat Konfigurasi";

        //     $id = $this->populate($koitem);

        //     return $this->redirect(['home','koi'=>$id]);
        

        // }else{
        //     // echo "Konfigurasi Ditemukan, Klik OK untuk melihat rincian";
        //     return $this->redirect(['home','koi'=>$id]);
        // }

    }


    public function actionScan()
    {
        $model = new SisaKemas();

        return $this->render('scan', [
            'model' => $model,
        ]);
    }


    public function actionAdd($snfg)
    {
        
        $model = new SisaKemas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['scan']);
        } else {
            return $this->render('add', [
                    'model' => $model,
                    'snfg' => $snfg,
            ]);
        }



        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // } else {
        //     return $this->render('scan', [
        //         'model' => $model,
        //         'snfg' => $snfg,
        //     ]);
        // }
    }

    /**
     * Updates an existing SisaKemas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SisaKemas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SisaKemas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SisaKemas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SisaKemas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
