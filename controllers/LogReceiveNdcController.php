<?php

namespace app\controllers;

use Yii;
use app\models\LogReceiveNdc;
use app\models\LogReceiveNdcSearch;
use app\models\SuratJalan;
use app\models\SuratJalanRincian;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * LogReceiveNdcController implements the CRUD actions for LogReceiveNdc model.
 */
class LogReceiveNdcController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LogReceiveNdc models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogReceiveNdcSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LogReceiveNdc model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LogReceiveNdc model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LogReceiveNdc();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionForceComplete($surat_jalan){
        $update = Yii::$app->db->createCommand("UPDATE log_receive_ndc
                    SET status = 'RECEIVED',
                    log = 'force',
                    timestamp_terima = :now,
                    pic = 'system'
                    WHERE status is null and nosj = :nosj
                    ",
                    [
                        ':now'=> date('Y-m-d H:i:s'),
                        ':nosj'=>$surat_jalan
                    ])->execute();
        if ($update){
            echo 1;
        }else{
            echo 0;
        }
    }

    public function actionTask($nosj,$operator)
    {

        // $sql = "
        //     SELECT
        //         *
        //     FROM
        //         log_receive_ndc
        //     WHERE nosj='".$surat_jalan."'
        //    ";

        // $task = LogReceiveNdc::findBySql($sql)->all();

        $this->layout = '//main-sidebar-collapse';
        date_default_timezone_set('Asia/Jakarta');

        $model = new LogReceiveNdc();

        $searchModel = new LogReceiveNdcSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['nosj'=>$nosj])->orderBy(['id'=>SORT_ASC,'status'=>SORT_ASC]);
        $dataProvider->pagination=false;


        // Get Related Information

        if ($model->load(Yii::$app->request->post())){

            // return $this->redirect(['create-rincian-qr', 'id' => $id]);
            return $this->redirect(['qc-fg-v2/terima-surat-jalan-ndc', 'surat_jalan' => $surat_jalan]);

        } else {

            return $this->render('task', [
                'model' => $model,
                'nosj' => $nosj,
                'operator' => $operator,
                // 'is_received' => $sj->is_received,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

    }

    public function actionScanPalet($nosj,$operator)
    {

        // Get Current Selected Operator
        $sql2="
                SELECT
                     *
                FROM log_receive_ndc
                WHERE nosj = '".$nosj."' ";

        $palet = LogReceiveNdc::findBySql($sql2)->one();

        return $this->render('scan-palet', [
            'nosj' => $nosj,
            'palet' => $palet,
            'operator' => $operator,
        ]);
    }

    public function actionCheckPaletById($id_palet,$nosj)
    {
        $sql="select * from log_receive_ndc where nosj = '".$nosj."' and id_palet='".$id_palet."' ";
        $check= LogReceiveNdc::findBySql($sql)->one();

        $sql="select * from log_receive_ndc where nosj = '".$nosj."' and id_palet='".$id_palet."' and status = 'RECEIVED'";
        $check_scan= LogReceiveNdc::findBySql($sql)->one();

        if (!empty($check) && empty($check_scan)) {
            echo 1;
        } else if (!empty($check) && !empty($check_scan)){
            echo 2;
        } else {
            echo 0;
        }
    }

    public function actionUpdateStatusById($id_palet,$log,$pic)
    {
        date_default_timezone_set('Asia/Jakarta');

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

        UPDATE log_receive_ndc
        SET status = 'RECEIVED', log = :log, pic = :pic, timestamp_terima = :timestamp_terima
        WHERE id_palet = :id_palet;
        ", 
        [':id_palet'=> $id_palet,
         ':log'=>$log,
         ':pic'=>$pic,
         ':timestamp_terima'=>date('Y-m-d h:i A'),
        ]);

        $result = $command->queryAll();

        // echo $row;
        if ($result){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionCheckPaletBySnfg($snfg,$nourut,$nosj)
    {
        $sql="select * from log_receive_ndc where nosj = '".$nosj."' and snfg='".$snfg."' and nourut = ".$nourut." ";
        $check= LogReceiveNdc::findBySql($sql)->one();

        $sql="select * from log_receive_ndc where nosj = '".$nosj."' and snfg='".$snfg."' and nourut = ".$nourut." and status = 'RECEIVED'";
        $check_scan= LogReceiveNdc::findBySql($sql)->one();

        if (!empty($check) && empty($check_scan)) {
            echo 1;
        } else if (!empty($check) && !empty($check_scan)){
            echo 2;
        } else {
            echo 0;
        }
    }

    public function actionUpdateStatusBySnfg($snfg,$nourut,$log,$pic)
    {
        date_default_timezone_set('Asia/Jakarta');

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

        UPDATE log_receive_ndc
        SET status = 'RECEIVED', log = :log, pic = :pic, timestamp_terima = :timestamp_terima
        WHERE snfg = :snfg and nourut = :nourut;
        ", 
        [':snfg'=> $snfg,
         ':nourut'=>$nourut,
         ':log'=>$log,
         ':pic'=>$pic,
         ':timestamp_terima'=>date('Y-m-d h:i A'),
        ]);

        $result = $command->queryAll();

        // echo $row;
        if ($result){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionGetIdPaletByOldLabel($snfg,$nourut){
        $check = LogReceiveNdc::find()->where(['and',['snfg'=>$snfg,'nourut'=>$nourut]])->one();
        if (!empty($check)){
            return Json::encode(['status'=>'success','value'=>$check->id_palet]);
        }
    }

    public function actionFinishReceiving($nosj)
    {
        $sql = "
        SELECT *
        FROM log_receive_ndc
        WHERE  nosj = '".$nosj."' AND status is null";

        $row = LogReceiveNdc::findBySql($sql)->all();

        // $sql2 = "
        // SELECT *
        // FROM flow_input_mo
        // WHERE  nomo = '".$nomo."' AND siklus = '".$siklus."' AND posisi = 'CHECKER'";

        // $fim = FlowInputMo::findBySql($sql2)->one();

        // echo $row;
        if (empty($row)){
            echo 1;
        } else {
            echo 0;
        }
    }


    // Redha Script Integrate to WMS NDC Testing

    public function actionPushReceivingWmsndc(){

        // Import API Configuration File

            require_once Yii::$app->basePath.'/config/api_wmsndc.php';

        // Using Json encoder plugin

        // Get Outstanding Receiving with 'sync_wmsndc' status on 'surat_jalan' table equals to string value 'WAIT'
        
        // Parse Alokasi String, removing prefix "NDC " with split_part


        $sql = "
            SELECT 
                sj.nosj as document_number,
                split_part(sj.alokasi,' ',2) as warehouse
            FROM surat_jalan sj
            WHERE 
                sync_wmsndc ='WAIT'
            ";
        $outstanding_sj = LogReceiveNdc::findBySql($sql)->asArray()->all();

        $json_outstanding_sj = Json::encode($outstanding_sj);




        // Iterate all Outstanding and Call API to Insert

        foreach($outstanding_sj as $sj){

            // Convert to JSON Object
                $obj = Json::encode($sj);


            // Catch if there are anomalies in data

                $sql = "
                    SELECT 
                        count(*) as count
                    FROM  log_receive_ndc l 
                    WHERE l.nosj = :nosj
                    AND ( expired_date is NULL OR odoo_code is NULL )
                        
                    ";
                $outlier_items = LogReceiveNdc::findBySql($sql,[":nosj" => $sj['document_number'] ])->asArray()->one();

                // if there are outlier / anomali data then return error

                if( $outlier_items['count'] > 0){

                    print_r(json_encode(array("message"=>"ERROR for ".$sj['document_number']."! missing Items in Odoo Code / Expired Date")));

                }else{

                    // Proceed to trigger API

                    // Trigger API Call using CURL for Create Receive Document

                        $curl = curl_init();
                        // Prevent Printing Response without our command
                        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
                        curl_setopt_array($curl, array(
                          CURLOPT_URL => $api_url.'?r=api/create-ndc-receive-order',
                          CURLOPT_CUSTOMREQUEST => 'POST',
                          CURLOPT_POSTFIELDS => $obj,
                          CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/json'
                          ),
                        ));

                        $response = curl_exec($curl);
                        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                        curl_close($curl);

                    // END of Create Receive Document 

                    // If Succeed then proceed Create Receive ITEMS
                        if($statusCode==201){
                            
                            // Decode JSON Object to PHP Array
                            $res = Json::decode($response);

                            // Update Surat Jalan to 'SYNCING' which means the state is in progress for synchronizing to WMS

                            $connection = Yii::$app->getDb();
                            $command = $connection->createCommand("

                              UPDATE surat_jalan
                              SET
                                  sync_wmsndc = 'SYNCING'
                              WHERE nosj = '".$sj['document_number']."'
                              ");

                            $result = $command->execute();

                            // If Succeed then Call API to push items to WMS NDC from 'log_receive_ndc' table
                            
                            if($result){

                                // Get Details per Item

                                $sql = "
                                    SELECT 
                                        ".$res['id']." as id,
                                        l.odoo_code as default_code,
                                        l.qty,
                                        l.nobatch,
                                        l.expired_date,
                                        l.nourut as nopalet
                                    FROM surat_jalan sj
                                    LEFT JOIN log_receive_ndc l on sj.nosj = l.nosj
                                    WHERE sj.nosj = '".$sj['document_number']."' 
                                        
                                    ";
                                $receive_items = LogReceiveNdc::findBySql($sql)->asArray()->all();
                                $json_receive_items = Json::encode($receive_items);


                                // Trigger API Call using CURL for Receive Items

                                $curl2 = curl_init();
                                // Prevent Printing Response without our command
                                curl_setopt($curl2,CURLOPT_RETURNTRANSFER,1);
                                curl_setopt_array($curl2, array(
                                  CURLOPT_URL => $api_url.'?r=api/create-ndc-receive-order-item',
                                  CURLOPT_CUSTOMREQUEST => 'POST',
                                  CURLOPT_POSTFIELDS => $json_receive_items,
                                  CURLOPT_HTTPHEADER => array(
                                    'Content-Type: application/json'
                                  ),
                                ));

                                $responseItems = curl_exec($curl2);
                                $statusCodeItems = curl_getinfo($curl2, CURLINFO_HTTP_CODE);

                                curl_close($curl2);

                                // If Succeed update status on 'surat_jalan' to 'DONE' , else if Fail then revert back to Wait

                                if($statusCodeItems==201){

                                    $connection = Yii::$app->getDb();
                                    $command = $connection->createCommand("

                                      UPDATE surat_jalan
                                      SET
                                          sync_wmsndc = 'DONE'
                                      WHERE nosj = '".$sj['document_number']."'
                                      ");

                                    $result3 = $command->execute();

                                    print_r($responseItems);

                                }else{

                                    $connection = Yii::$app->getDb();
                                    $command = $connection->createCommand("

                                      UPDATE surat_jalan
                                      SET
                                          sync_wmsndc = 'WAIT'
                                      WHERE nosj = '".$sj['document_number']."'
                                      ");

                                    $result3 = $command->execute();

                                    print_r($responseItems);

                                }


                            }

                            // Else then revert the 'surat_jalan' sync_wmsndc status to 'WAIT' so it can be executed again in the next run
                            
                            else{

                                $connection = Yii::$app->getDb();
                                $command = $connection->createCommand("

                                  UPDATE surat_jalan
                                  SET
                                      sync_wmsndc = 'WAIT'
                                  WHERE nosj = '".$sj['nosj']."'
                                  ");

                                $result2 = $command->execute();

                            }
                        

                        } 

                    // END OF Create Receive Items

                } // END OF Proceed to trigger API 

            

        } // END Foreach oustanding_sj   

    }


    // END Of Redha Script

    /**
     * Updates an existing LogReceiveNdc model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LogReceiveNdc model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LogReceiveNdc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogReceiveNdc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogReceiveNdc::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
