<?php

namespace app\controllers;

use Yii;
use app\models\FlowCuciMesin;
use app\models\FlowCuciMesinKemas;
use app\models\FlowCuciMesinSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FlowCuciMesinController implements the CRUD actions for FlowCuciMesin model.
 */
class FlowCuciMesinController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FlowCuciMesin models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FlowCuciMesinSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FlowCuciMesin model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FlowCuciMesin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FlowCuciMesin();
        $model2 = new FlowCuciMesinKemas();


        if ($model->load(Yii::$app->request->post()) ) {
            $nama_line = $model->nama_line;

            $sql = "SELECT * FROM flow_cuci_mesin WHERE nama_line = '".$nama_line."' order by datetime_start DESC";
            $row1 = FlowCuciMesin::findBySql($sql)->one();
            if (!empty($row1) && empty($row1->datetime_stop)){
                return $this->redirect(['stop-cuci', 'id' => $row1->id]);
            }else{
                return $this->redirect(['start-cuci', 'nama_line' => $nama_line]);
            }
            // $nomo = $row['nomo'];
            // print_r ($rows);
            // return $this->redirect(['start-cuci-mesin-olah', 'nama_line' => $nama_line]);
        } else if ($model2->load(Yii::$app->request->post()) ) {
            $nama_line = $model2->nama_line;

            $sql = "SELECT * FROM flow_cuci_mesin_kemas WHERE nama_line = '".$nama_line."' order by datetime_start DESC";
            $row1 = FlowCuciMesinKemas::findBySql($sql)->one();
            if (!empty($row1) && empty($row1->datetime_stop)){
                return $this->redirect(['flow-cuci-mesin-kemas/stop-cuci', 'id' => $row1->id]);
            }else{
                return $this->redirect(['flow-cuci-mesin-kemas/start-cuci', 'nama_line' => $nama_line]);
            }
            // $nomo = $row['nomo'];
            // print_r ($rows);
            // return $this->redirect(['start-cuci-mesin-olah', 'nama_line' => $nama_line]);
        }else{
            return $this->render('create', [
                'model2' => $model2,
                'model' => $model,
            ]);
        }


    }

    public function actionCreateBarcode()
    {
        $model = new FlowCuciMesin();


        if ($model->load(Yii::$app->request->post()) ) {
            $nama_line = $model->nama_line;

            $sql = "SELECT * FROM flow_cuci_mesin WHERE nama_line = '".$nama_line."' order by datetime_start DESC";
            $row1 = FlowCuciMesin::findBySql($sql)->one();
            if (!empty($row1) && empty($row1->datetime_stop)){
                return $this->redirect(['stop-cuci', 'id' => $row1->id]);
            }else{
                return $this->redirect(['start-cuci', 'nama_line' => $nama_line]);
            }
            // $nomo = $row['nomo'];
            // print_r ($rows);
            // return $this->redirect(['start-cuci-mesin-olah', 'nama_line' => $nama_line]);
        } else {
            return $this->render('create-barcode', [
                'model' => $model,
            ]);
        }
    }

    //Check sudah start cuci atau belum
    public function actionCheckStatusCuci($nama_line)
    {
        $row = FlowCuciMesin::find()->where(['nama_line'=>$nama_line])->orderBy('datetime_start DESC')->limit(1)->one();
        $max_id = $row['id'];
        $start = $row['datetime_start'];
        $stop = $row['datetime_stop'];
        if (empty($stop)){
            $now = date('Y-m-d H:i:s');
            $diff =strtotime($now)-strtotime($start);
            $hours= floor($diff/(60*60));
            //automatic stop when diff more than 1 hour
            if ($hours > 9){
                $stop = date('Y-m-d H:i:s',strtotime('+9 hours',strtotime($start)));
                // Stop Jadwal
                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("

                UPDATE flow_cuci_mesin
                SET datetime_stop = :current_time,
                WHERE id = :id;
                ",
                [':current_time' => date('Y-m-d h:i A'),
                 ':id'=> $max_id,
                ]);
                // $model->save();
                $result = $command->queryAll();
                // print_r($stop);
            }else{
                return $this->redirect(['stop-cuci', 'id' => $max_id]);
                // print_r('berhasil');
            }
        // new record
        }else{
          return $this->redirect(['start-cuci', 'nama_line' => $nama_line]);
        }
    }

    //Check nama line, sedang dipakai olah atau tidak
    public function actionCheckLine($nama_line)
    {
        $row = Yii::$app->db->createCommand("SELECT * FROM flow_input_mo WHERE nama_line = '".$nama_line."' ORDER BY datetime_start DESC")->queryOne();
        // print_r($data);
        if (!empty($row['datetime_start']) && empty($row['datetime_stop']))
        {
          $data = [];
          $data = ['running'=>1];
          echo json_encode($data);
        }else{
          $data = [];
          $data = ['running'=>0];
          echo json_encode($data);
        }

    }
    //Scan start cuci mesin
    public function actionStartCuci($nama_line)
    {
        $searchModel = new FlowCuciMesinSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['datetime_start'=>SORT_DESC]);

        $model = new FlowCuciMesin();
        if ($model->load(Yii::$app->request->post()) ) {
            $model->save();
            // print_r('<pre>');
            // print_r ($model);
            // print_r('</pre>');
            return $this->redirect(['create']);
        } else {
            return $this->render('_form-start-cuci', [
                'nama_line' => $nama_line,
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    //Scan stop cuci mesin
    public function actionStopCuci($id)
    {
        $searchModel = new FlowCuciMesinSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['datetime_start'=>SORT_DESC]);

        $model = new FlowCuciMesin();
        if ($model->load(Yii::$app->request->post()) ) {

          // Get Posted Value
              $verifikator=Yii::$app->request->post('FlowCuciMesin')['verifikator'];
              if(empty($verifikator)){
                  $verifikator=0;
              }

              $gombalan=Yii::$app->request->post('FlowCuciMesin')['gombalan'];
              if(empty($gombalan)){
                  $gombalan=0;
              }

              $sarung_tangan=Yii::$app->request->post('FlowCuciMesin')['sarung_tangan'];
              if(empty($sarung_tangan)){
                  $sarung_tangan=0;
              }

              $detergen=Yii::$app->request->post('FlowCuciMesin')['detergen'];
              if(empty($detergen)){
                  $detergen=0;
              }

              $parafin=Yii::$app->request->post('FlowCuciMesin')['parafin'];
              if(empty($parafin)){
                  $parafin=0;
              }

              // Stop Jadwal
              $connection = Yii::$app->getDb();
              $command = $connection->createCommand("

              UPDATE flow_cuci_mesin
              SET datetime_stop = :current_time,
                  verifikator = :verifikator,
                  gombalan = :gombalan,
                  sarung_tangan = :sarung_tangan,
                  detergen = :detergen,
                  parafin = :parafin
              WHERE id = :id;
              ",
              [':current_time' => date('Y-m-d h:i A'),
               ':id'=> $id,
               ':verifikator'=>$verifikator,
               'gombalan'=>$gombalan,
               'sarung_tangan'=>$sarung_tangan,
               'detergen'=>$detergen,
               'parafin'=>$parafin
              ]);

              // $model->save();

              $result = $command->queryAll();


            return $this->redirect(['create']);
        } else {
            return $this->render('_form-stop-cuci', [
                // 'nama_line' => $nama_line,
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing FlowCuciMesin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FlowCuciMesin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FlowCuciMesin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FlowCuciMesin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FlowCuciMesin::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
