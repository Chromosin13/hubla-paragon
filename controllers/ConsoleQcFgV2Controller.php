<?php

namespace app\controllers;

use Yii;
use app\models\QcFgV2;
use app\models\ScmPlanner;
use app\models\QcFgV2Search;
use yii\console\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\filters\AccessControl;
use app\components\AccessRule;
use app\models\User;
// use yii\helpers\Json;
/**
 * QcFgV2Controller implements the CRUD actions for QcFgV2 model.
 */
class ConsoleQcFgV2Controller extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create', 'update', 'delete','index','view','terima-karantina','terima-ndc','print-label','print-zebra','scan-karantina','update-karantina'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_QCFG,
                            User::ROLE_KARANTINA,
                            User::ROLE_NDC,
                        ],
                    ],
                    [
                        'actions' => ['terima-karantina','scan-karantina','update-karantina'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_KARANTINA,
                        ],
                    ],
                    [
                        'actions' => ['terima-ndc'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_NDC,
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_QCFG
                        ],
                    ],
                    [
                        'actions' => ['print-label','print-zebra'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_QCFG
                        ],
                    ],
                ],
            ],
        ];
    }



    public function actionConsoleInsertTest(){

        $connection = Yii::$app->getDb();
        date_default_timezone_set('Asia/Jakarta');
        $command = $connection->createCommand("
            INSERT INTO qc_fg_v2_auto_is_done (snfg)
            SELECT  
                    'A' as snfg
            ;");
        $result = $command->queryAll();

    }


    public function actionIsDonePatch(){
        // Log Anomali Is Done
        $connection = Yii::$app->getDb();
        date_default_timezone_set('Asia/Jakarta');
        $command = $connection->createCommand("
            INSERT INTO qc_fg_v2_auto_is_done (snfg,inspektor_qcfg,last_timestamp)
            SELECT 
             snfg,
             inspektor_qcfg,
             last_timestamp
             FROM
            (SELECT
              a.last_timestamp,
              a.snfg,
              a.jumlah_koli,
              a.jumlah_terima_ndc,
              a.is_done,
              case when a.jumlah_koli = a.jumlah_terima_ndc and a.is_done < a.jumlah_koli then 1 else 0 
              end as is_anomali,
              op.inspektor_qcfg as inspektor_qcfg

            FROM
              (SELECT
                max(timestamp_terima_ndc) as last_timestamp,
                snfg,
                count(nourut) as jumlah_koli,
                sum(case when status='SUDAH DITERIMA NDC' then 1 
                else 0 end) as jumlah_terima_ndc,
                sum(coalesce(is_done,0)) as is_done
              FROM qc_fg_v2
              GROUP BY snfg
              HAVING count(nourut)>1
              ) a
              LEFT JOIN (
               SELECT 
                DISTINCT ON (snfg)
                snfg,
                inspektor_qcfg,
                nourut
                FROM qc_fg_v2
                WHERE timestamp_terima_ndc is not null
                ORDER BY snfg, nourut desc
              ) op  on a.snfg = op.snfg
            ) b 
            WHERE is_anomali=1
            and last_timestamp <= current_date- interval '1 week'
            ORDER BY last_timestamp desc
            ;");
        $result = $command->queryAll();

        // Do The Patch on qc_fg_v2 table ( Set Is Done = 1)
        $connection = Yii::$app->getDb();
        date_default_timezone_set('Asia/Jakarta');
        $command = $connection->createCommand("
            UPDATE qc_fg_v2
            SET is_done=1
            WHERE snfg in 
            (SELECT 
                   snfg
                   FROM
                  (SELECT
                    a.last_timestamp,
                    a.snfg,
                    a.jumlah_koli,
                    a.jumlah_terima_ndc,
                    a.is_done,
                    case when a.jumlah_koli = a.jumlah_terima_ndc and a.is_done < a.jumlah_koli then 1 else 0 
                    end as is_anomali,
                    op.inspektor_qcfg as inspektor_qcfg
                  FROM
                    (SELECT
                      max(timestamp_terima_ndc) as last_timestamp,
                      snfg,
                      count(nourut) as jumlah_koli,
                      sum(case when status='SUDAH DITERIMA NDC' then 1 
                      else 0 end) as jumlah_terima_ndc,
                      sum(coalesce(is_done,0)) as is_done
                    FROM qc_fg_v2
                    GROUP BY snfg
                    HAVING count(nourut)>1
                    ) a
                    LEFT JOIN (
                     SELECT 
                      DISTINCT ON (snfg)
                      snfg,
                      inspektor_qcfg,
                      nourut
                      FROM qc_fg_v2
                      WHERE timestamp_terima_ndc is not null
                      ORDER BY snfg, nourut desc
                    ) op  on a.snfg = op.snfg
                  ) b 
                  WHERE is_anomali=1
                  and last_timestamp <= current_date- interval '1 week'
                  ORDER BY last_timestamp desc
              );");
        $result = $command->queryAll();
    }

    /**
     * Finds the QcFgV2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QcFgV2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QcFgV2::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
