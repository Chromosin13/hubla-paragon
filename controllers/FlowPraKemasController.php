<?php

namespace app\controllers;

use Yii;
use app\models\MasterDataNa;
use app\models\JenisProsesPraKemas;
use app\models\ScmPlanner;
use app\models\FlowPraKemas;
use app\models\FlowPraKemasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use yii\helpers\ArrayHelper;

/**
 * FlowPraKemasController implements the CRUD actions for FlowPraKemas model.
 */
class FlowPraKemasController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FlowPraKemas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FlowPraKemasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FlowPraKemas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FlowPraKemas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FlowPraKemas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FlowPraKemas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Render Scan Barcode SNFG Kemas 2 Page
     * @return mixed
     */
    public function actionCheckKemas()
    {
        $model = new FlowPraKemas();

        $searchModel = new FlowPraKemasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("datetime_start is not null and datetime_stop is null");

        $this->layout = '//main-sidebar-collapse';

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['create-kemas', 'snfg' => $model->snfg]);
        } else {
            return $this->render('check-kemas', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionCheckIsLineActive($nama_line)
    {
        date_default_timezone_set('Asia/Jakarta');
        // $date = date("Y-m-d");
        $current_time = date("Y-m-d H:i:s");
        $yesterday_time = date("Y-m-d H:i:s",strtotime("-1 days"));

        $sql2="
        SELECT * FROM flow_pra_kemas
        WHERE nama_line = '".$nama_line."' AND datetime_start > '".$yesterday_time."' AND datetime_start < '".$current_time."' AND datetime_stop is null order by id desc";
        
        $result2 = FlowPraKemas::findBySql($sql2)->all();

        if (!empty($result2)){
            foreach ($result2 as $result_) {
                $array[] = $result_->snfg;
            }    
            echo Json::encode($array);
        } else {
            echo 0;
        }
    }

    public function actionCheckIfSplitLine($snfg)
    {
        date_default_timezone_set('Asia/Jakarta');
        // $date = date("Y-m-d");
        $current_time = date("Y-m-d H:i:s");
        $yesterday_time = date("Y-m-d H:i:s",strtotime("-1 days"));

        $sql="
            SELECT id,nama_line FROM flow_pra_kemas
            WHERE snfg = '".$snfg."' AND datetime_start > '".$yesterday_time."' AND datetime_start < '".$current_time."' AND datetime_stop is null order by id desc
        ";

        $result = FlowPraKemas::findBySql($sql)->all();

        if (!empty($result)){
            foreach ($result as $data) {
                $list[] = array("id"=>$data->id,"nama_line"=>$data->nama_line,"snfg"=>$snfg,"route"=>"to-stop");
            }
            echo Json::encode($list);   
        } else {
            $var = json_decode('[{"route" : "to-create"}]');
            return Json::encode($var);
        }

    }

    public function actionCheckIsSplit($snfg)
    {
        date_default_timezone_set('Asia/Jakarta');

        $sql2="
        SELECT is_split_line FROM flow_pra_kemas
        WHERE snfg = '".$snfg."' order by id desc";
        
        $result2 = FlowPraKemas::findBySql($sql2)->one();

        if (!empty($result2)){
            echo $result2->is_split_line;
        } else {
            echo 99;
        }
    }

    /**
     * Render Page Form for Pra Kemas
     * This will render a page which the results typically comes from barcode scanning result
     * Redirected from actionCheckKemas[/QR/Inline]
     * @param $snfg
     * @return mixed
     */
    public function actionCreateKemas($snfg)
    {

        $mod = new ScmPlanner();

        $result = $mod->periksaStatusSnfg($snfg);

        $a = json_decode($result);

        // print_r($a->status);
        // die;

        if($a->status == "not-found"){
            throw new \yii\web\HttpException(401,
          'Jadwal untuk SNFG tidak ditemukan. Pastikan input sudah benar atau tidak typoo. Jika input sudah benar, pastikan PPC sudah upload jadwal untuk SNFG ini.');
        }

        if($a->status=='UNHOLD')
        {

                //Get Sediaan
                $sediaan = ScmPlanner::find()->where(['snfg'=>$snfg])->one()['sediaan'];


                $model = new FlowPraKemas();

                // Populate Grid Data
                $searchModel = new FlowPraKemasSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->query->where("snfg='".$snfg."' and posisi='PRA_KEMAS'");

                $this->layout = '//main-sidebar-collapse';

                //Check Last State of a SNFG
                $sql="

                SELECT
                    distinct on (snfg)
                    id,
                    snfg,
                    datetime_start,
                    datetime_stop,
                    is_split_line
                FROM flow_pra_kemas
                WHERE snfg = '".$snfg."' and posisi='PRA_KEMAS'
                ORDER BY snfg,id desc
                ";

                $row = FlowPraKemas::findBySql($sql)->one();

                // Assign Variable Null to prevent error if it's the first data entry
                if(empty($row)){
                    $last_datetime_start = null;
                    $last_datetime_stop = null;
                    $last_id = null;
                    $is_split = null;
                }else{
                    $last_datetime_start = $row->datetime_start;
                    $last_datetime_stop = $row->datetime_stop;
                    $last_id = $row->id;
                    if ($row->is_split_line == 1){
                        $is_split = 1;
                    } else {
                        $is_split = 0;
                    }
                }
                


                // Jenis Proses Check Status Jadwal, If Found then Don't Display
                $sql3="
                        SELECT
                            jenis_proses_pra_kemas
                        FROM jenis_proses_pra_kemas
                        WHERE jenis_proses_pra_kemas not in
                        (SELECT jenis_proses FROM status_jadwal WHERE
                        nojadwal='".$snfg."' and posisi='PRA_KEMAS')
                    ";

                $jenis_proses_list = ArrayHelper::map(JenisProsesPraKemas::findBySql($sql3)->all(), 'jenis_proses_pra_kemas','jenis_proses_pra_kemas');

                /******* GET NO SMB (NOMO) FROM ODOO MRP **********/
                $connection8 = Yii::$app->db2;
                $command8 = $connection8->createCommand("

                SELECT
                    name
                FROM mrp_production
                WHERE origin = :snfg AND state = 'confirmed'
                ",
                [':snfg' => $snfg,
                ]);

                $data8 = $command8->queryAll();

                if(empty($data8)){
                    $nosmb = "-";
                } else {
                    $nosmb = $data8[0]['name'];
                }

                //Get NoBatch dari Olah
                $result = $this->actionGenerateBatchPraKemas($snfg);
                $nobatch = $result ['nobatch'];
                $exp_date = $result ['exp_date'];

                // Check If Snfg Last Status is Start, if yes then redirect to actionStopKemas1
                if(!empty($last_datetime_start)&&(empty($last_datetime_stop)) ){
                    return $this->redirect(['stop-kemas','snfg' => $snfg,'last_id'=>$last_id,'pos'=>'pusat']);
                }
                

                /********* GET PRODUCT NAME FROM ODOO MRP *********/
                $connection = Yii::$app->db2;
                $command = $connection->createCommand("

                SELECT
                    pt.description as odoo_code,
                    pt.name
                FROM product_template pt
                WHERE pt.default_code = :koitem
                ",
                [':koitem' => strtok($snfg, '/'),
                ]);

                $data2 = $command->queryAll();

                if (empty($data2)){
                    $connection = Yii::$app->db_varcos;
                    $command = $connection->createCommand("

                    SELECT
                        pt.default_code as odoo_code,
                        pt.name
                    FROM product_template pt
                    WHERE pt.old_koitem = :koitem
                    ",
                    [':koitem' => strtok($snfg, '/'),
                    ]);
                    $data2 = $command->queryAll();                    
                }


                if (empty($data2)){
                    $fg_name = "";
                } else {
                    $fg_name = $data2[0]['name'];
                }
                /**************************************************/


                /******* GET PRODUCT BARCODE FROM ODOO MRP ********/
                $kode_item = strtok($snfg, '/');

                if (strpos($kode_item, 'MY-') !== false) {
                    $sqlj="
                    select *
                    from master_data_barcode_malaysia
                    where koitem_lama = '".$kode_item."' or koitem_baru = '".$kode_item."'
                    ";

                    $mdbm = MasterDataBarcodeMalaysia::findBySql($sqlj)->one();

                    if (empty($mdbm)){
                        $barcode = "";
                    } else {
                        $barcode = $mdbm->barcode_harmonize;
                    }
                } else {

                    $connection3 = Yii::$app->db_paragon;
                    $command3 = $connection3->createCommand("

                    SELECT
                        pt.old_koitem,
                        pp.barcode
                    FROM product_product pp
                    LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id
                    WHERE pt.old_koitem = :koitem
                    ",
                    [':koitem' => strtok($snfg, '/'),
                    ]);

                    $data3 = $command3->queryAll();

                    if (empty($data3)){
                        $barcode = "";
                    } else {
                        $barcode = $data3[0]['barcode'];
                    }
                }

                $master_na = MasterDataNa::find()->where(['koitem'=>strtok($snfg,'/')])->one();

                if (empty($master_na)){
                    $na_number = "";
                }else{
                    $na_number = $master_na->na_number;
                }

                
                /**************************************************/

                // Check If Post Value True
                // if ($model->load(Yii::$app->request->post()) && $model->save()) {
                if ($model->load(Yii::$app->request->post())) {

                    $model->barcode = $model->barcode;
                    $model->fg_name_odoo = $model->fg_name_odoo;
                    $model->nama_line = $model->nama_line;
                    $model->posisi_scan = "pusat";
                    
                    //Check Last State of a Snfg
                    // $sql_batch="

                    // select nobatch
                    // from scm_planner
                    // where snfg = '".$snfg."'
                    // ";

                    // $nobatch_sp = ScmPlanner::findBySql($sql_batch)->one();
                    // $kode_tahun = "";
                    // if (!empty($nobatch_sp)){
                    //     $kode_tahun = substr($nobatch_sp->nobatch,1,1);
                    // } else {
                    //     $kode_tahun = "";
                    // }

                    // if (strpos($snfg, '/BS') !== false and $kode_tahun == "G") {
                    //     $month = date("m",strtotime($model->exp_date));
                    //     if ($month == 1 or $month == 7){
                    //         $model->lakban = 'HIJAU';
                    //     } else if ($month == 2 or $month == 8){
                    //         $model->lakban = 'ORANYE';
                    //     } else if ($month == 3 or $month == 9){
                    //         $model->lakban = 'UNGU';
                    //     } else if ($month == 4 or $month == 10){
                    //         $model->lakban = 'PUTIH';
                    //     } else if ($month == 5 or $month == 11){
                    //         $model->lakban = 'TOSKA';
                    //     } else if ($month == 6 or $month == 12){
                    //         $model->lakban = 'MERAH MUDA';
                    //     }
                    // } else {
                    //     $month = date("m",strtotime($model->exp_date));
                    //     if ($month == 1 or $month == 2){
                    //         $model->lakban = 'HIJAU';
                    //     } else if ($month == 3 or $month == 4){
                    //         $model->lakban = 'ORANYE';
                    //     } else if ($month == 5 or $month == 6){
                    //         $model->lakban = 'UNGU';
                    //     } else if ($month == 7 or $month == 8){
                    //         $model->lakban = 'PUTIH';
                    //     } else if ($month == 9 or $month == 10){
                    //         $model->lakban = 'TOSKA';
                    //     } else if ($month == 11 or $month == 12){
                    //         $model->lakban = 'MERAH MUDA';
                    //     }
                    // }
                    // $model->reason = "-";
                    // $model->lakban_old = $model->lakban;
                    $model->save();
                    $id = $model->id;

                    return $this->redirect(['check-kemas']);
                } else {
                    return $this->render('create-kemas', [
                        'model' => $model,
                        'snfg' => $snfg,
                        'last_datetime_start' => $last_datetime_start,
                        'last_datetime_stop' => $last_datetime_stop,
                        'last_id' => $last_id,
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'na_number' => $na_number,
                        'jenis_proses_list' => $jenis_proses_list,
                        // 'frontend_ip' => $frontend_ip,
                        // 'sbc_ip' => $sbc_ip,
                        // 'line' => $line,
                        'nosmb' => $nosmb,
                        'nobatch' => $nobatch,
                        'exp_date' => $exp_date,
                        'barcode' => $barcode,
                        'fg_name' => $fg_name,
                        // 'keterangan' => $keterangan
                    ]);
                }

        }else {
            throw new \yii\web\HttpException(401,
          'Jadwal telah di HOLD / CANCEL mohon hubungi Planner');
        }
    }

    public function actionStopKemas($snfg,$last_id)
    {
        $model = new FlowPraKemas();
        $jenis_kemas = FlowPraKemas::find()->where(['id'=>$last_id])->one()['jenis_kemas'];
        // Populate Grid Data
        $searchModel = new FlowPraKemasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("snfg='".$snfg."' and posisi='PRA_KEMAS'")->orderby(['datetime_start'=>SORT_DESC]);

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");

        $this->layout = '//main-sidebar-collapse';

        //Get No Batch yg sudah diisi di form start scan
        $sql="

        select nobatch
        from flow_pra_kemas
        where snfg = '".$snfg."' and id = ".$last_id;

        $nobatch_row = FlowPraKemas::findBySql($sql)->one();
        if (empty($nobatch_row)){
            $batch = NULL;
        } else {
            $batch = $nobatch_row->nobatch;
        }

        //Get Data Netto
        $koitem_fg = explode('/',$snfg)[0];
        $komponens = ScmPlanner::find()->where(['snfg'=>$snfg])->all();
        $jumlah_komponen = count($komponens);

        if ($jumlah_komponen > 1){
            $koitem_bulk=[];
            foreach ($komponens as $komponen){
                $koitem_bulk_array[] = "'".$komponen['koitem_bulk']."'";
            }
            $koitem_bulk = implode(',',$koitem_bulk_array);

            $netto_min = Yii::$app->db->createCommand("SELECT SUM(netto_min) FROM master_data_product WHERE koitem_fg = '".$koitem_fg."' AND koitem_bulk != '#N/A' AND koitem_bulk is not null ")->queryScalar();
        }else{
            $data = Yii::$app->db->createCommand("SELECT netto_min FROM master_data_product WHERE koitem_fg = '".$koitem_fg."' AND netto_min is not null ")->queryOne();
            $netto_min = $data['netto_min'];
            if (empty($netto_min)){
                $data = Yii::$app->db->createCommand("SELECT netto_klaim FROM master_data_product WHERE koitem_fg = '".$koitem_fg."' AND uom_klaim is not null AND uom_klaim != 'ml' ")->queryOne();
                $netto_min = $data['netto_klaim'];
            }
        }

        // Check If Post Value True
        if ($model->load(Yii::$app->request->post())){

            // Jenis Proses
                $array_id = FlowPraKemas::find()->where(['id' => $last_id])->one();
                $jenis_proses = $array_id->jenis_kemas;

            
            // Get Posted Value
                $jumlah_realisasi=Yii::$app->request->post('FlowPraKemas')['jumlah_realisasi'];
                if(empty($jumlah_realisasi)){
                    $jumlah_realisasi=0;
                }

                $nobatch=Yii::$app->request->post('FlowPraKemas')['nobatch'];
                if(empty($nobatch)){
                    $nobatch=null;
                }

                $counter=Yii::$app->request->post('FlowPraKemas')['counter'];
                if(empty($counter)){
                    $counter=null;
                }
                
                // Get Staging value
                $staging=Yii::$app->request->post('FlowPraKemas')['staging'];
                // Get Is Done Value
                $is_done=Yii::$app->request->post('FlowPraKemas')['is_done'];

                // $netto_1=Yii::$app->request->post('FlowPraKemas')['netto_1'];
                // if(empty($netto_1)){
                //     $netto_1=0;
                // }

                // $netto_2=Yii::$app->request->post('FlowPraKemas')['netto_2'];
                // if(empty($netto_2)){
                //     $netto_2=0;
                // }

                // $netto_3=Yii::$app->request->post('FlowPraKemas')['netto_3'];
                // if(empty($netto_3)){
                //     $netto_3=0;
                // }

                // $netto_4=Yii::$app->request->post('FlowPraKemas')['netto_4'];
                // if(empty($netto_4)){
                //     $netto_4=0;
                // }

                // $netto_5=Yii::$app->request->post('FlowPraKemas')['netto_5'];
                // if(empty($netto_5)){
                //     $netto_5=0;
                // }

            // Insert Netto & Reject
            // $connection2 = Yii::$app->getDb();
            // $command2 = $connection2->createCommand("

            //     INSERT INTO mpq_monitoring (flow_pra_kemas_id,netto_1,netto_2,netto_3,netto_4,netto_5)
            //     SELECT
            //         :flow_pra_kemas_id as flow_pra_kemas_id,
            //         :netto_1 as netto_1,
            //         :netto_2 as netto_2,
            //         :netto_3 as netto_3,
            //         :netto_4 as netto_4,
            //         :netto_5 as netto_5
            // ",
            // [
            //     ':flow_pra_kemas_id' => $last_id,
            //     ':netto_1' => $netto_1,
            //     ':netto_2' => $netto_2,
            //     ':netto_3' => $netto_3,
            //     ':netto_4' => $netto_4,
            //     ':netto_5' => $netto_5
            // ]);

            // $result = $command2->queryAll();

            // Stop Jadwal
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE flow_pra_kemas
            SET datetime_stop = :current_time,
                datetime_write = :current_time,
                jumlah_realisasi = :jumlah_realisasi,
                counter = :counter,
                nobatch = :nobatch,
                staging = :staging
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i A'),
             ':id'=> $last_id,
             ':jumlah_realisasi'=>$jumlah_realisasi,
             ':counter'=>$counter,
             ':nobatch'=>$nobatch,
             ':staging'=>$staging
            ]);

            $result = $command->queryAll();

            // $this->actionAutoOffSplitLine($snfg);

            // Insert Is Done Value to status_jadwal
            if(!empty($is_done)){

                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("

                INSERT INTO status_jadwal (nojadwal,status,table_name,posisi,jenis_proses)
                SELECT
                    :snfg as nojadwal,
                    1 as status,
                    'flow_pra_kemas' as table_name,
                    'PRA_KEMAS' as posisi,
                    :jenis_proses as jenis_proses
                ON CONFLICT (nojadwal,posisi,jenis_proses)
                DO NOTHING;
                ",
                [':snfg' => $snfg, ':jenis_proses' => $jenis_proses]);

                $result = $command->queryAll();

                // return $this->redirect(['rekonsiliasi-packaging/create', 'snfg'=>$snfg,'flow_pra_kemas_id'=>$last_id, 'pos'=>$pos]);

            }

            // Redirect to Downtime
            return $this->redirect(['downtime/index-pra-kemas', 'id' => $last_id]);

        } else {
            return $this->render('_form-kemas-stop', [
                'model' => $model,
                'jenis_kemas' => $jenis_kemas,
                'snfg' => $snfg,
                'last_id' => $last_id,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'batch' => $batch,
                'netto_min' => $netto_min,
            ]);
        }
    }

    public function actionGenerateBatchPraKemas($snfg)
    {
        date_default_timezone_set('Asia/Jakarta');

        $year_mapping = [2014=>'A',2015=>'B',2016=>'C',2017=>'D',2018=>'E',2019=>'F',2020=>'G',
                        2021=>'H',2022=>'I',2023=>'J',2024=>'K',2025=>'L',2026=>'M',2027=>'N',
                        2028=>'O',2029=>'P',2030=>'Q',2031=>'R',2032=>'S',2033=>'T',2034=>'U',
                        2035=>'V',2036=>'W',2037=>'X',2038=>'Y',2039=>'Z'];
    
        $month_mapping = [1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',6=>'F',7=>'G',
                        8=>'H',9=>'I',10=>'J',11=>'K',12=>'L'];

        $serial_mapping = [0=>'0',1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',
                            6=>'F',7=>'G',8=>'H',9=>'I',10=>'J',11=>'K'];
                            
        //Cek snfg sudah punya nobatch atau belum
        $current_nobatch = FlowPraKemas::find()->where("snfg='".$snfg."' and nobatch is not null")->one()['nobatch'];
        if (empty($current_nobatch)){
            $jadwal_bulk_sisa = ScmPlanner::find()->where("snfg = '".$snfg."' and nobatch is not null and nobatch not in ('-','') ")->all();
            
            //Jika jadwal adalah jadwal bulk sisa
            if (!empty($jadwal_bulk_sisa)){
                $list_date = [];
                foreach($jadwal_bulk_sisa as $data){
                    $m = substr($data['nobatch'],0,1);
                    $y = substr($data['nobatch'],1,1);
                    $d = substr($data['nobatch'],2,2);
                    $s = substr($data['nobatch'],4,1);

                    $list_date[]=$y.$m.$d.$s;
                    
                }
            
                sort($list_date);
                $first_nobatch = $list_date[0];

                $y = substr($first_nobatch,0,1);
                $m = substr($first_nobatch,1,1);
                $d = substr($first_nobatch,2,2);
                $s = substr($first_nobatch,4,1);

                $nobatch=$m.$y.$d.$s;
                
                //Get NOMO tanpa sn -> ex:'/F0001'
                $start_serial = array_search($s,$serial_mapping);
                $snfg_without_serial = explode('/',$snfg)[0];
                for ($serial=$start_serial; $serial<= 10; $serial++)
                {
                    $nobatch = $m.$y.$d.$serial_mapping[$serial];
                    $check = Yii::$app->db->createCommand("SELECT nobatch FROM flow_pra_kemas 
                                                            WHERE snfg LIKE '".$snfg_without_serial."%' AND nobatch = '".$nobatch."'
                                                            ")->queryOne();
                    if (empty($check)){
                        break;
                    }
                }

                
                Print_r('Batch kemas menggunakan bulk sisa');

            }else{//Jika bukan jadwal bulk sisa

                //Get nomo yang terdapat pada satu snfg
                $list_jadwal = ScmPlanner::find()->where(['snfg'=>$snfg])->all();
                $list_komponen = [];
                foreach ($list_jadwal as $jadwal){
                    $list_komponen[] = $jadwal['snfg_komponen'];
                }
                $jumlah_komponen = count($list_komponen);
    
                if ($jumlah_komponen == 1){ //Jika bukan item fg yang memiliki komponen
                    $pengolahan_batch = Yii::$app->db->createCommand("SELECT nobatch FROM flow_input_mo
                                                                        WHERE nomo IN (SELECT nomo FROM scm_planner WHERE snfg = '".$snfg."' and trim(nomo) NOT IN ('','-') and nomo is not null) 
                                                                        AND nobatch is not null 
                                                                        ORDER BY datetime_start ASC LIMIT 1
                                                                        ")->queryOne();
                    //Jika ada proses olahnya 
                    if (!empty($pengolahan_batch)){
                        $nobatch = $pengolahan_batch['nobatch'];
                        $first_record = null;
                        Print_r('Batch kemas FG tanpa komponen');

                    }else{// Jika tidak ada proses olahnya
                        $nobatch = null;
                        $first_record['datetime_start'] = date('Y-m-d H:i:s');
                        Print_r('Batch kemas FG tanpa komponen, tanpa proses olah');

                        $current_year = (int)date('Y',strtotime($first_record['datetime_start']));
                        $current_month = (int)date('m',strtotime($first_record['datetime_start']));
                        $current_date = date('d',strtotime($first_record['datetime_start']));
                        // var_dump ($first_record['datetime_start']);
                        // var_dump ($current_year);
                        
                        //Buat mappping tahun berdasarkan acuan 2017 -> D
                        $y = $year_mapping[$current_year];
                        $m = $month_mapping[$current_month];
                        $d = $current_date;
                        // var_dump ($d);

                        //Get NOMO tanpa sn -> ex:'/F0001'
                        $snfg_without_serial = explode('/',$snfg)[0];
                        for ($serial=1; $serial<= 10; $serial++)
                        {
                            $nobatch = $m.$y.$d.$serial_mapping[$serial];
                            // $check = Yii::$app->db->createCommand("SELECT pb.* FROM pengolahan_batch pb LEFT JOIN
                            //                                 flow_input_mo fim ON pb.pengolahan_id = fim.id
                            //                                 WHERE nomo LIKE '".$nomo_without_serial."%' AND pb.nobatch = '".$nobatch."'
                            //                                  ")->queryOne();
                            $check = Yii::$app->db->createCommand("SELECT * FROM flow_pra_kemas
                                                             WHERE snfg LIKE '".$snfg_without_serial."%' AND nobatch = '".$nobatch."'
                                                              ")->queryOne();
                            if (empty($check)){
                              break;
                            }
                            // print_r('<pre>');
                            // print_r($check);
                            // print_r($serial);
                            // print_r($nobatch);
                        }
                    }
    
                }else{ //Jika FG memiliki komponen
                        $pengolahan_batch = Yii::$app->db->createCommand("SELECT nobatch FROM flow_input_mo
                                                                        WHERE nomo IN (SELECT nomo FROM scm_planner WHERE snfg = '".$snfg."' and trim(nomo) NOT IN ('','-') and nomo is not null) 
                                                                        AND nobatch is not null 
                                                                        ORDER BY datetime_start ASC LIMIT 1
                                                                        ")->queryOne();
                        //Jika ada proses olahnya 
                        if (!empty($pengolahan_batch)){
                            $nobatch = $pengolahan_batch['nobatch'];
                            $first_record = null;
                            // Print_r('Batch kemas FG dengan komponen');

                        }else{// Jika tidak ada proses olahnya
                            $nobatch = null;
                            $first_record['datetime_start'] = date('Y-m-d H:i:s');
                            // Print_r('Batch kemas FG dengan komponen, tanpa record olah');

                            $current_year = (int)date('Y',strtotime($first_record['datetime_start']));
                            $current_month = (int)date('m',strtotime($first_record['datetime_start']));
                            $current_date = date('d',strtotime($first_record['datetime_start']));
                            // var_dump ($first_record['datetime_start']);
                            // var_dump ($current_year);
                            
                            //Buat mappping tahun berdasarkan acuan 2017 -> D
                            $y = $year_mapping[$current_year];
                            $m = $month_mapping[$current_month];
                            $d = $current_date;
                            // var_dump ($d);

                            //Get NOMO tanpa sn -> ex:'/F0001'
                            $snfg_without_serial = explode('/',$snfg)[0];
                            for ($serial=1; $serial<= 10; $serial++)
                            {
                                $nobatch = $m.$y.$d.$serial_mapping[$serial];
                                // $check = Yii::$app->db->createCommand("SELECT pb.* FROM pengolahan_batch pb LEFT JOIN
                                //                                 flow_input_mo fim ON pb.pengolahan_id = fim.id
                                //                                 WHERE nomo LIKE '".$nomo_without_serial."%' AND pb.nobatch = '".$nobatch."'
                                //                                  ")->queryOne();
                                $check = Yii::$app->db->createCommand("SELECT * FROM flow_pra_kemas
                                                                 WHERE snfg LIKE '".$snfg_without_serial."%' AND nobatch = '".$nobatch."'
                                                                  ")->queryOne();
                                if (empty($check)){
                                  break;
                                }
                                // print_r('<pre>');
                                // print_r($check);
                                // print_r($serial);
                                // print_r($nobatch);
                            }
                        }
                        
                }//$jumlah_komponen==1

            }//!empty($bulk_sisa)

        }else{
            $nobatch = $current_nobatch;
            Print_r('Batch kemas sudah ada sebelumnya');
        }//empty($current_nobatch)

        if (!empty($nobatch)){
            $m = substr($nobatch,0,1);
            $y = substr($nobatch,1,1);
            $d = substr($nobatch,2,2);
            $s = substr($nobatch,4,1);
    
            //Get Exp Date
            $year = array_search($y,$year_mapping);
            $month = array_search($m,$month_mapping);
            $snfg_without_serial = explode('/',$snfg)[0];
    
            $sl_fg = Yii::$app->db->createCommand("SELECT sl_fg from master_data_product where koitem_fg = '".$snfg_without_serial."' and sl_fg is not null ")
                    ->queryOne()['sl_fg'];
    
            $first_olah = $year.'-'.$month.'-'.$d;
            if (!empty($sl_fg)){
                $exp_date = date('Y-m-d', strtotime($first_olah. ' + '.$sl_fg.' years'));
            }else{
                $exp_date = null;
            }
            // return $nobatch;
            // print_r ('Year Code: '.$y);
            // print_r ('<br>');
            // print_r ('Year : '.$year);
            // print_r ('<br>');
            // print_r ('Nobatch : '.$nobatch);
            // print_r ('<br>');
            // print_r ('First olah : '.$first_olah);
            // print_r ('<br>');
            // print_r ('sl_fg : '.$sl_fg);
            // print_r ('<br>');
            // print_r ('Exp date : '.$exp_date);
        }else{
            $exp_date = null;
        }

        
        $data = ['nobatch'=>$nobatch,'exp_date'=>$exp_date];
        return $data;
    }



    public function actionGetLanjutanKemas($snfg,$jenis_kemas,$posisi)
    {
        $sql="
        SELECT
            coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM flow_pra_kemas
        WHERE snfg = '".$snfg."'
        and jenis_kemas = '".$jenis_kemas."'
        and posisi = '".$posisi."'
        ";

        $lanjutan= FlowPraKemas::findBySql($sql)->one();
        echo Json::encode($lanjutan);
    }

    /**
     * Deletes an existing FlowPraKemas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FlowPraKemas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FlowPraKemas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FlowPraKemas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
