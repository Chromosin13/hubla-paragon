<?php

namespace app\controllers;

use Yii;
use app\models\Downtime;
use app\models\DowntimeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DowntimeController implements the CRUD actions for Downtime model.
 */
class DowntimeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Downtime models.
     * @return mixed
     */
    // public function actionIndex()
    // {
    //     $searchModel = new DowntimeSearch();
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    //     return $this->render('index', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }

    public function actionIndexPenimbangan($id)
    {
        $searchModel = new DowntimeSearch();
        $searchModel->flow_input_mo_id=$id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index-penimbangan', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'flow_input_mo_id' => $id,
        ]);
    }

    public function actionIndexPengolahan($id)
    {
        $searchModel = new DowntimeSearch();
        $searchModel->flow_input_mo_id=$id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';

        return $this->render('index-pengolahan', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'flow_input_mo_id' => $id,
        ]);
    }

    public function actionIndexKemas2($id,$pos)
    {
        $searchModel = new DowntimeSearch();
        $searchModel->flow_input_snfg_id=$id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';

        return $this->render('index-kemas2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'flow_input_snfg_id' => $id,
            'pos' => $pos
        ]);
    }

    public function actionIndexKemas2Inline($id)
    {
        $searchModel = new DowntimeSearch();
        $searchModel->flow_input_snfg_id=$id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';

        return $this->render('index-kemas2-inline', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'flow_input_snfg_id' => $id,
        ]);
    }

    public function actionIndexPraKemas($id)
    {
        $searchModel = new DowntimeSearch();
        $searchModel->flow_pra_kemas_id=$id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';

        return $this->render('index-pra-kemas', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'flow_pra_kemas_id' => $id,
        ]);
    }

    public function actionIndexKemas1($id)
    {
        $searchModel = new DowntimeSearch();
        $searchModel->flow_input_snfgkomponen_id=$id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';

        return $this->render('index-kemas1', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'flow_input_snfgkomponen_id' => $id,
        ]);
    }

    /**
     * Displays a single Downtime model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Downtime model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Downtime();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreatePenimbangan($id)
    {
        $model = new Downtime();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) 
            {
                echo 1;
            }else{
                echo 0;
            } 
        }else {
            return $this->renderAjax('create-penimbangan', [
                'model' => $model,
                'flow_input_mo_id' => $id,
            ]);
        }
    }


    public function actionCreateKemas2($id)
    {
        $model = new Downtime();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) 
            {
                echo 1;
            }else{
                echo 0;
            } 
        }else {
            return $this->renderAjax('create-kemas2', [
                'model' => $model,
                'flow_input_snfg_id' => $id,
            ]);
        }
    }

    public function actionCreateKemas1($id)
    {
        $model = new Downtime();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) 
            {
                echo 1;
            }else{
                echo 0;
            } 
        }else {
            return $this->renderAjax('create-kemas1', [
                'model' => $model,
                'flow_input_snfgkomponen_id' => $id,
            ]);
        }
    }

    public function actionCreatePraKemas($id)
    {
        $model = new Downtime();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) 
            {
                echo 1;
            }else{
                echo 0;
            } 
        }else {
            return $this->renderAjax('create-pra-kemas', [
                'model' => $model,
                'flow_pra_kemas_id' => $id,
            ]);
        }
    }

    // public function actionCreatePengolahan()
    // {
    //     $model = new Downtime();

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     } else {
    //         return $this->render('create-pengolahan', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    public function actionCreatePengolahan($id)
    {
        $model = new Downtime();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) 
            {
                echo 1;
            }else{
                echo 0;
            } 
        }else {
            return $this->renderAjax('create-pengolahan', [
                'model' => $model,
                'flow_input_mo_id' => $id,
            ]);
        }
    }

    /**
     * Updates an existing Downtime model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Downtime model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        // return $this->redirect(['index']);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Downtime model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Downtime the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Downtime::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
