<?php

namespace app\controllers;

use Yii;
use app\models\Kemas2;
use app\models\Dummy;
use app\models\Kemas2Search;
use app\models\PosisiProsesReworkSb;
use app\models\PosisiProses;
use app\models\PosisiProsesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use app\models\User;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use app\models\Kendala;
use app\models\Model;
use app\models\DimLine;
use app\models\ScmPlanner;

/**
 * Kemas2Controller implements the CRUD actions for Kemas2 model.
 */
class Kemas2Controller extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create', 'update', 'delete','index','view'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_KEMAS
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Kemas2 models.
     * @return mixed
     */
    // public function actionIndex()
    // {
    //     $searchModel = new Kemas2Search();
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    //     return $this->render('index', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }

    /**
     * Displays a single Kemas2 model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Kemas2 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new Kemas2();
    //     $modelsKendala = [new Kendala];

    //     // VAR

    //     // Get State

    //         $state =Yii::$app->request->post('Kemas2')['state'];

    //     // Get Nomesin
            
    //         $namaline=Yii::$app->request->post('Kemas2')['nama_line'];

    //         $nomesin_sql = " 
    //                 select id
    //                 from dim_line 
    //                 where kemas='".$namaline."'
    //                 limit 1 
    //               ";

    //         $nomesin_array = DimLine::findBySql($nomesin_sql)->one();
    //         $nomesin = $nomesin_array['id'];

    //     // Get Koitem

    //         $snfg=Yii::$app->request->post('Kemas2')['snfg'];

    //         $sql_itemfg = " 
    //                 select koitem_fg
    //                 from scm_planner 
    //                 where snfg='".$snfg."'
    //                 limit 1 
    //               ";

    //         $koitem_array = ScmPlanner::findBySql($sql_itemfg)->one();
    //         $koitem = $koitem_array['koitem_fg'];


    //     // Get TPM

    //         $jenis_kemas=Yii::$app->request->post('Kemas2')['jenis_kemas'];

    //         $tpm_sql = " 

    //             SELECT 
    //                 case when leadtime.value is null then 0 
    //                 when leadtime.value=0 then 0 
    //                 else round(mpq.value/leadtime.value,2) end as jumlah_pcs
    //             FROM 
    //                 (SELECT
    //                     coalesce(value,0) as value
    //                 FROM konfigurasi_std_leadtime k
    //                 WHERE k.std_type='P'
    //                 and k.jenis='mpq'
    //                 and k.koitem='".$koitem."'
    //                 LIMIT 1) as mpq
    //             LEFT JOIN 
    //                 (SELECT 
    //                     value
    //                 FROM konfigurasi_std_leadtime k
    //                 LEFT JOIN jenis_jenis_proses_rel j on j.jenis = k.jenis
    //                 WHERE k.std_type='P'
    //                 and j.jenis_proses='".$jenis_kemas."'
    //                 and k.koitem='".$koitem."'
    //                 LIMIT 1) as leadtime
    //             ON 1=1
    //            ";

    //         $tpm_array = ScmPlanner::findBySql($tpm_sql)->one();
    //         $tpm = $tpm_array['jumlah_pcs'];
    //         if(empty($tpm)){
    //             $tpm = 0;
    //         }




    //     if ($model->load(Yii::$app->request->post()) && $model->save() ) 
    //     {

    //         //

    //         $snfg=Yii::$app->request->post('Kemas2')['snfg'];
    //         $jenis_proses=Yii::$app->request->post('Kemas2')['jenis_kemas'];

    //         // Penanda Kemas 2

    //         $j=2;

    //         $connection = Yii::$app->getDb();
    //         date_default_timezone_set('Asia/Jakarta');
    //         $command = $connection->createCommand("
    //             INSERT INTO pit_std_leadtime_jadwal (nojadwal,proses,jenis_proses,std_type,value,time)
    //             SELECT  :snfg as nojadwal,
    //                     'KEMAS 2' as proses,
    //                     :jenis_proses as jenis_proses,
    //                     ksl.std_type,
    //                     ksl.value,
    //                     :last_update as time
    //             FROM (select distinct koitem_fg,koitem_bulk from scm_planner where snfg=:snfg) a 
    //             INNER JOIN (select k.* from konfigurasi_std_leadtime k
    //                          inner join jenis_jenis_proses_rel j on k.jenis = j.jenis 
    //                           where j.jenis_proses=:jenis_proses
    //                         ) ksl ON ksl.koitem = a.koitem_fg and a.koitem_bulk = ksl.koitem_bulk
    //             ON CONFLICT (nojadwal,jenis_proses,std_type)
    //             DO NOTHING;
    //             ", 
    //             [':last_update' => date('Y-m-d h:i:s A'),':snfg'=> $snfg,':jenis_proses'=> $jenis_proses]);
    //         $result = $command->queryAll();

    //         $command2 = $connection->createCommand("
    //             INSERT INTO pit_std_leadtime_jadwal (nojadwal,proses,jenis_proses,std_type,value,time)
    //             SELECT  :snfg as nojadwal,
    //                     'MPQ' as proses,
    //                     'MPQ' as jenis_proses,
    //                     ksl.std_type,
    //                     ksl.value,
    //                     :last_update as time
    //             FROM (select distinct koitem_fg,koitem_bulk from scm_planner where snfg=:snfg) a 
    //             INNER JOIN (select k.* from konfigurasi_std_leadtime k
    //                          where k.jenis='mpq'
    //                         ) ksl ON ksl.koitem = a.koitem_fg and a.koitem_bulk = ksl.koitem_bulk
    //             ON CONFLICT (nojadwal,jenis_proses,std_type)
    //             DO NOTHING;
    //             ", 
    //             [':last_update' => date('Y-m-d h:i:s A'),':snfg'=> $snfg]);
    //         $result2 = $command2->queryAll();

    //         // Tembak ke Counter

    //             if($state=="START"){

    //                 $url="http://10.3.5.104/ptidms/php_action/setproduction.php?no=".$nomesin."&item=".$koitem."&snfg=".$snfg."&tpm=".$tpm."";

    //                 $ch=curl_init();

    //                 curl_setopt($ch,CURLOPT_URL, $url);

    //                 if(curl_errno($ch)){
    //                     die("Could not send request to DMS");

    //                 } else {

    //                     $result=curl_exec($ch);
    //                     curl_close($ch);
    //                     echo $url;

    //                 }
    //             }

    //         //

    //         $modelsKendala = Model::createMultiple(Kendala::classname());
    //         Model::loadMultiple($modelsKendala, Yii::$app->request->post());

    //         // validate all models
    //         $valid = $model->validate();
    //         $valid = Model::validateMultiple($modelsKendala) && $valid;

    //         if ($valid) {
    //             $transaction = \Yii::$app->db->beginTransaction();
    //             try {
    //                 if ($flag = $model->save(false)) {
    //                     foreach ($modelsKendala as $modelKendala) 
    //                     {
    //                         $modelKendala->kemas_2_id = $model->id;
    //                         if (! ($flag = $modelKendala->save(false))) {
    //                             $transaction->rollBack();
    //                             break;
    //                         }
    //                     }
    //                 }
    //                 if ($flag) {
    //                     $transaction->commit();
    //                     if($state=="STOP"){
    //                         // return Yii::$app->runAction('achievement-leadtime-kemas2-direct/view', ['id' => $model->id]);

    //                         return $this->redirect(['planned-down-time/index','i'=>$model->id,'j'=>$j]);
    //                     } else {
    //                         return $this->redirect(['create']);
    //                     }
    //                 }
    //             } catch (Exception $e) {
    //                 $transaction->rollBack();
    //             }
    //         }
    //     } else 

    //     {
    //         return $this->render('create', [
    //             'model' => $model,
    //             'modelsKendala' => (empty($modelsKendala)) ? [new Kendala] : $modelsKendala
    //         ]);
    //     }

    //     // $model = new Kemas2();

    //     // if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //     //     //return $this->redirect(['view', 'id' => $model->id]);
    //     //     return $this->redirect(['create']);
    //     // } else {
    //     //     return $this->render('create', [
    //     //         'model' => $model,
    //     //     ]);
    //     // }
    // }

    public function actionCreateResolusi()
    {
        $model = new Kemas2();
        $modelsKendala = [new Kendala];

        // VAR

        // Get State

            $state =Yii::$app->request->post('Kemas2')['state'];

        // Get Nomesin
            
            $namaline=Yii::$app->request->post('Kemas2')['nama_line'];

            $nomesin_sql = " 
                    select id
                    from dim_line 
                    where kemas='".$namaline."'
                    limit 1 
                  ";

            $nomesin_array = DimLine::findBySql($nomesin_sql)->one();
            $nomesin = $nomesin_array['id'];

        // Get Koitem

            $snfg=Yii::$app->request->post('Kemas2')['snfg'];

            $sql_itemfg = " 
                    select koitem_fg
                    from scm_planner 
                    where snfg='".$snfg."'
                    limit 1 
                  ";

            $koitem_array = ScmPlanner::findBySql($sql_itemfg)->one();
            $koitem = $koitem_array['koitem_fg'];


        // Get TPM

            $jenis_kemas=Yii::$app->request->post('Kemas2')['jenis_kemas'];

            $tpm_sql = " 

                SELECT 
                    case when leadtime.value is null then 0 
                    when leadtime.value=0 then 0 
                    else round(mpq.value/leadtime.value,2) end as jumlah_pcs
                FROM 
                    (SELECT
                        coalesce(value,0) as value
                    FROM konfigurasi_std_leadtime k
                    WHERE k.std_type='P'
                    and k.jenis='mpq'
                    and k.koitem='".$koitem."'
                    LIMIT 1) as mpq
                LEFT JOIN 
                    (SELECT 
                        value
                    FROM konfigurasi_std_leadtime k
                    LEFT JOIN jenis_jenis_proses_rel j on j.jenis = k.jenis
                    WHERE k.std_type='P'
                    and j.jenis_proses='".$jenis_kemas."'
                    and k.koitem='".$koitem."'
                    LIMIT 1) as leadtime
                ON 1=1
               ";

            $tpm_array = ScmPlanner::findBySql($tpm_sql)->one();
            $tpm = $tpm_array['jumlah_pcs'];
            if(empty($tpm)){
                $tpm = 0;
            }




        if ($model->load(Yii::$app->request->post()) && $model->save() ) 
        {

            //

            $snfg=Yii::$app->request->post('Kemas2')['snfg'];
            $jenis_proses=Yii::$app->request->post('Kemas2')['jenis_kemas'];

            // Penanda Kemas 2

            $j=2;

            $connection = Yii::$app->getDb();
            date_default_timezone_set('Asia/Jakarta');
            $command = $connection->createCommand("
                INSERT INTO pit_std_leadtime_jadwal (nojadwal,proses,jenis_proses,std_type,value,time)
                SELECT  :snfg as nojadwal,
                        'KEMAS 2' as proses,
                        :jenis_proses as jenis_proses,
                        ksl.std_type,
                        ksl.value,
                        :last_update as time
                FROM (select distinct koitem_fg,koitem_bulk from scm_planner where snfg=:snfg) a 
                INNER JOIN (select k.* from konfigurasi_std_leadtime k
                             inner join jenis_jenis_proses_rel j on k.jenis = j.jenis 
                              where j.jenis_proses=:jenis_proses
                            ) ksl ON ksl.koitem = a.koitem_fg and a.koitem_bulk = ksl.koitem_bulk
                ON CONFLICT (nojadwal,jenis_proses,std_type)
                DO NOTHING;
                ", 
                [':last_update' => date('Y-m-d h:i:s A'),':snfg'=> $snfg,':jenis_proses'=> $jenis_proses]);
            $result = $command->queryAll();

            $command2 = $connection->createCommand("
                INSERT INTO pit_std_leadtime_jadwal (nojadwal,proses,jenis_proses,std_type,value,time)
                SELECT  :snfg as nojadwal,
                        'MPQ' as proses,
                        'MPQ' as jenis_proses,
                        ksl.std_type,
                        ksl.value,
                        :last_update as time
                FROM (select distinct koitem_fg,koitem_bulk from scm_planner where snfg=:snfg) a 
                INNER JOIN (select k.* from konfigurasi_std_leadtime k
                             where k.jenis='mpq'
                            ) ksl ON ksl.koitem = a.koitem_fg and a.koitem_bulk = ksl.koitem_bulk
                ON CONFLICT (nojadwal,jenis_proses,std_type)
                DO NOTHING;
                ", 
                [':last_update' => date('Y-m-d h:i:s A'),':snfg'=> $snfg]);
            $result2 = $command2->queryAll();

            // Tembak ke Counter

                if($state=="START"){

                    $url="http://10.3.5.104/ptidms/php_action/setproduction.php?no=".$nomesin."&item=".$koitem."&snfg=".$snfg."&tpm=".$tpm."";

                    $ch=curl_init();

                    curl_setopt($ch,CURLOPT_URL, $url);

                    if(curl_errno($ch)){
                        die("Could not send request to DMS");

                    } else {

                        $result=curl_exec($ch);
                        curl_close($ch);
                        echo $url;

                    }
                }

            //

            $modelsKendala = Model::createMultiple(Kendala::classname());
            Model::loadMultiple($modelsKendala, Yii::$app->request->post());

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsKendala) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($modelsKendala as $modelKendala) 
                        {
                            $modelKendala->kemas_2_id = $model->id;
                            if (! ($flag = $modelKendala->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        if($state=="STOP"){
                            // return Yii::$app->runAction('achievement-leadtime-kemas2-direct/view', ['id' => $model->id]);

                            return $this->redirect(['planned-down-time/index','i'=>$model->id,'j'=>$j]);
                        } else {
                            return $this->redirect(['pusat-resolusi/create']);
                        }
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        } else 

        {
            return $this->render('create-resolusi', [
                'model' => $model,
                'modelsKendala' => (empty($modelsKendala)) ? [new Kendala] : $modelsKendala
            ]);
        }

        // $model = new Kemas2();

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     //return $this->redirect(['view', 'id' => $model->id]);
        //     return $this->redirect(['create']);
        // } else {
        //     return $this->render('create', [
        //         'model' => $model,
        //     ]);
        // }
    }

    public function actionCheckReworkKomponen($snfg_komponen)
    {
        $sql=   "
                SELECT * FROM
                posisi_proses_rework_sb
                where snfg_komponen = '".$snfg_komponen."'
                ";
        $posisi= PosisiProsesReworkSb::findBySql($sql)->one();
        //print_r($Kemas2);
        echo Json::encode($posisi);
    }

    public function actionTest()
    {
        echo 'Test Api tronjal tronjol';
    }


    public function actionCheckReworkSnfg($snfg)
    {
        $sql=   "
                SELECT * FROM
                posisi_proses_rework_sb
                where snfg = '".$snfg."'
                ";
        $posisi= PosisiProsesReworkSb::findBySql($sql)->one();
        //print_r($Kemas2);
        echo Json::encode($posisi);
    }

    public function actionCheckBatchSplit($snfg_komponen)
    {
        $sql="


                select a.* from
                (select distinct on (k1.snfg_komponen) 
                    k1.snfg_komponen snfg_komponen
                    ,state
                    ,jenis_kemas
                    ,is_done
                    ,k1.lanjutan_split_batch
                    ,case when jenis_kemas in ('PRESS','FILLING') and k2.lanjutan_split_batch>1 then 1
                          when is_done!=1 or state!='STOP' then 0
                          else -1
                     end last_status
                from kemas_1 k1 
                left join 
                    (select snfg_komponen,
                            max(lanjutan_split_batch) lanjutan_split_batch from  kemas_1
                     where snfg_komponen= '".$snfg_komponen."'
                     group by snfg_komponen
                    )  k2 on k1.snfg_komponen = k2.snfg_komponen
                where k1.snfg_komponen ='".$snfg_komponen."' and    k1.state='STOP'
                order by snfg_komponen,timestamp desc
                ) a
                 ";
        $Kemas2= Kemas2::findBySql($sql)->one();
        //print_r($Kemas2);
        echo Json::encode($Kemas2);
        //print_r(ArrayHelper::getColumn($Kemas2,'lanjutan_split_batch'));
        //print_r($Kemas2);
    }

    public function actionBatchSplitAssignKomponen($snfg_komponen)
    {


        $countPosisiProses = PosisiProsesReworkSb::find()
                        ->where(['snfg_komponen' => $snfg_komponen])
                        ->count();


        $sql = "
                SELECT * FROM
                posisi_proses_rework_sb
                where snfg_komponen = '".$snfg_komponen."'
                ";

        $PosisiProsess = PosisiProsesReworkSb::findBySql($sql)->all();


        if($countPosisiProses > 0)
        {
            foreach($PosisiProsess as $PosisiProses){
                echo "<option value='".$PosisiProses->lanjutan_split_batch."'>".$PosisiProses->lanjutan_split_batch."</options>";
            }

        }
        else{
                echo "<option>1</option>";
        }
    }

    public function actionBatchSplitAssignSnfg($snfg)
    {
        $countPosisiProses = PosisiProsesReworkSb::find()
                        ->where(['snfg' => $snfg])
                        ->count();


        $sql = "
                SELECT * FROM
                posisi_proses_rework_sb
                where snfg = '".$snfg."'
                ";

        $PosisiProsess = PosisiProses::findBySql($sql)->all();


        if($countPosisiProses > 0)
        {
            foreach($PosisiProsess as $PosisiProses){
                echo "<option value='".$PosisiProses->lanjutan_split_batch."'>".$PosisiProses->lanjutan_split_batch."</options>";
            }

        }
        else{
                echo "<option>1</option>";
        }
    }

    public function actionLanjutanSplitBatchKemas2($snfg_komponen,$jenis_kemas)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan_split_batch,0)),0)+1 as lanjutan_split_batch
        FROM 
            (
                SELECT  sp.snfg, 
                        kemas_2.snfg_komponen,
                        kemas_2.jenis_kemas,
                        kemas_2.state,
                        kemas_2.lanjutan,
                        kemas_2.lanjutan_split_batch
                from kemas_2 
                inner join scm_planner sp on sp.snfg_komponen = kemas_2.snfg_komponen
                where   state='STOP'
                        and kemas_2.batch_split=1 
                        and kemas_2.snfg is null 
                        and kemas_2.snfg_komponen is not null 
                        and kemas_2.snfg_komponen='".$snfg_komponen."'
            ) a
        WHERE a.snfg_komponen ='".$snfg_komponen."'
        and a.jenis_kemas = '".$jenis_kemas."'";
        $Kemas2= Kemas2::findBySql($sql)->one();
        echo Json::encode($Kemas2);
    }

    public function actionLanjutanSplitBatchKemas2snfg($snfg,$jenis_kemas)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan_split_batch,0)),0)+1 as lanjutan_split_batch
        FROM 
            (            
                SELECT  kemas_2.snfg, 
                        sp.snfg_komponen,
                        kemas_2.jenis_kemas,
                        kemas_2.state,
                        kemas_2.lanjutan,
                        kemas_2.lanjutan_split_batch
                from kemas_2 
                inner join scm_planner sp on sp.snfg = kemas_2.snfg
                where   state='STOP'
                        and kemas_2.batch_split=1 
                        and kemas_2.snfg is not null  
                        and sp.snfg ='".$snfg."'
            ) a
        WHERE a.snfg ='".$snfg."'
        and a.jenis_kemas = '".$jenis_kemas."'
        ";
        $Kemas2= Kemas2::findBySql($sql)->one();
        echo Json::encode($Kemas2);

    }

     // GEt Last Jenis Olah untuk QC Bulk
    public function actionGetJenisKemasKomponen($snfg_komponen)
    {
        $sql="
        
        SELECT 
            distinct on (T.snfg_komponen_sp) T.snfg,T.jenis_kemas,T.timestamp,snfg_komponen_sp snfg_komponen
        FROM
            (   
                select  
                        p.*,sp.snfg_komponen snfg_komponen_sp 
                from kemas_2 p

                left join scm_planner sp on sp.snfg_komponen = p.snfg_komponen
                
                where p.snfg is null and sp.snfg_komponen='".$snfg_komponen."'
                and p.state='STOP'
                -- and is_done=1
            
            union all 
            
                select 
                        p.*,sp.snfg_komponen snfg_komponen_sp
                from kemas_2 p

                left join scm_planner sp on sp.snfg = p.snfg
                
                where p.snfg_komponen is null and sp.snfg_komponen = '".$snfg_komponen."'
                and p.state='STOP'
                -- and is_done=1

            ) T
        ORDER BY T.snfg_komponen_sp,T.timestamp desc

        ";
        $jenisKemas= Kemas2::findBySql($sql)->one();
        echo Json::encode($jenisKemas);
    }

    // GEt Last Jenis Olah untuk QC Bulk
    public function actionGetJenisKemasSnfg($snfg)
    {
        $sql="
        
        SELECT 
            distinct on (T.snfg_komponen_sp) T.snfg,T.jenis_kemas,T.timestamp,snfg_komponen_sp snfg_komponen
        FROM
            (   
                select  
                        p.*,sp.snfg_komponen snfg_komponen_sp 
                from kemas_2 p

                left join scm_planner sp on sp.snfg_komponen = p.snfg_komponen
                
                where p.snfg is null and sp.snfg='".$snfg."'
                and state='STOP'
                -- and is_done=1
            
            union all 
            
                select 
                        p.*,sp.snfg_komponen snfg_komponen_sp
                from kemas_2 p

                left join scm_planner sp on sp.snfg = p.snfg
                
                where p.snfg_komponen is null and sp.snfg = '".$snfg."'
                and state='STOP'
                -- and is_done=1
            ) T
        ORDER BY T.snfg_komponen_sp,T.timestamp desc

        ";
        $jenisKemas= Kemas2::findBySql($sql)->one();
        echo Json::encode($jenisKemas);
    }

    public function actionPaletKeKomponen($snfg_komponen,$jenis_kemas,$lanjutan_split_batch)
    {
        $sql="
        SELECT coalesce(max(coalesce(palet_ke,0)),0)+1 as palet_ke
        FROM 
            (
                SELECT 
                        sp.snfg, 
                        kemas_2.snfg_komponen, 
                        kemas_2.jenis_kemas, 
                        kemas_2.state, 
                        kemas_2.palet_ke,
                        kemas_2.lanjutan_split_batch
                FROM kemas_2 
                
                INNER JOIN scm_planner sp on sp.snfg_komponen = kemas_2.snfg_komponen
                
                WHERE state='STOP' 
                and kemas_2.snfg is null 
                and kemas_2.snfg_komponen is not null 
                and kemas_2.snfg_komponen='".$snfg_komponen."'
                and kemas_2.palet_flag=1
                and kemas_2.jenis_kemas = '".$jenis_kemas."'
                and kemas_2.lanjutan_split_batch = '".$lanjutan_split_batch."'

                UNION ALL
                
                SELECT  p.snfg,
                        sp.snfg_komponen, 
                        p.jenis_kemas, 
                        p.state, 
                        p.palet_ke,
                        p.lanjutan_split_batch
                FROM kemas_2 p
                INNER JOIN scm_planner sp on sp.snfg=p.snfg
                
                WHERE p.state='STOP' 
                and p.snfg is not null 
                and sp.snfg_komponen='".$snfg_komponen."'
                and p.palet_flag=1
                and p.jenis_kemas = '".$jenis_kemas."'
                and p.lanjutan_split_batch = '".$lanjutan_split_batch."'
            ) a
        ";

        $Kemas2= Kemas2::findBySql($sql)->one();
        echo Json::encode($Kemas2);

    }

    public function actionPaletKeSnfg($snfg,$jenis_kemas,$lanjutan_split_batch)
    {
        $sql="
        SELECT coalesce(max(coalesce(palet_ke,0)),0)+1 as palet_ke
        FROM 
            (
                select  kemas_2.snfg, 
                        sp.snfg_komponen,
                        kemas_2.jenis_kemas,
                        kemas_2.state,
                        kemas_2.palet_ke,
                        kemas_2.lanjutan_split_batch
                from kemas_2 
                inner join scm_planner sp on sp.snfg = kemas_2.snfg
                where state='STOP' and kemas_2.snfg is not null and kemas_2.snfg_komponen is null 
                and kemas_2.snfg='".$snfg."' 
                and kemas_2.palet_flag=1
                and kemas_2.jenis_kemas = '".$jenis_kemas."'
                and kemas_2.lanjutan_split_batch = '".$lanjutan_split_batch."'

                union all
                
                select  sp.snfg,
                        p.snfg_komponen, 
                        p.jenis_kemas, 
                        p.state, 
                        p.palet_ke,
                        p.lanjutan_split_batch 
                from kemas_2 p
                inner join scm_planner sp on sp.snfg_komponen=p.snfg_komponen
                where p.state='STOP' 
                and p.snfg is null 
                and sp.snfg='".$snfg."' 
                and p.palet_flag=1
                and p.jenis_kemas = '".$jenis_kemas."'
                and p.lanjutan_split_batch = '".$lanjutan_split_batch."'

            ) a
        ";

        $Kemas2= Kemas2::findBySql($sql)->one();
        echo Json::encode($Kemas2);

    }



    public function actionLanjutanKemas2($snfg_komponen,$jenis_kemas)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM 
            (
                select sp.snfg, kemas_2.snfg_komponen, kemas_2.jenis_kemas, kemas_2.state, kemas_2.lanjutan
                from kemas_2 
                inner join scm_planner sp on sp.snfg_komponen = kemas_2.snfg_komponen
                where state='STOP' and kemas_2.snfg is null and kemas_2.snfg_komponen is not null 
                and kemas_2.snfg_komponen='".$snfg_komponen."'

                union all
                
                select p.snfg,sp.snfg_komponen, p.jenis_kemas, p.state, p.lanjutan 
                from kemas_2 p
                inner join scm_planner sp on sp.snfg=p.snfg
                where p.state='STOP' and p.snfg is not null and sp.snfg_komponen='".$snfg_komponen."'
            ) a
        WHERE a.snfg_komponen ='".$snfg_komponen."'
        and a.jenis_kemas = '".$jenis_kemas."'
        ";

        $Kemas2= Kemas2::findBySql($sql)->one();
        echo Json::encode($Kemas2);

    }

    public function actionLanjutanKemas2snfg($snfg,$jenis_kemas)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM 
            (
                select kemas_2.snfg, sp.snfg_komponen, kemas_2.jenis_kemas, kemas_2.state, kemas_2.lanjutan
                from kemas_2 
                inner join scm_planner sp on sp.snfg = kemas_2.snfg
                where state='STOP' and kemas_2.snfg is not null and kemas_2.snfg_komponen is null 
                and kemas_2.snfg='".$snfg."'

                union all
                
                select sp.snfg,p.snfg_komponen, p.jenis_kemas, p.state, p.lanjutan 
                from kemas_2 p
                inner join scm_planner sp on sp.snfg_komponen=p.snfg_komponen
                where p.state='STOP' and p.snfg is null and sp.snfg='".$snfg."'
            ) a
        WHERE a.snfg ='".$snfg."'
        and a.jenis_kemas = '".$jenis_kemas."'
        ";

        $Kemas2= Kemas2::findBySql($sql)->one();
        echo Json::encode($Kemas2);

    }

    public function actionLanjutanIstKemas2($snfg_komponen,$jenis_kemas,$lanjutan)
    {
        $sql="
                SELECT coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist
                FROM 
                    (
                        select sp.snfg, kemas_2.snfg_komponen, kemas_2.jenis_kemas, kemas_2.state, kemas_2.lanjutan,kemas_2.lanjutan_ist
                        from kemas_2 
                        inner join scm_planner sp on sp.snfg_komponen = kemas_2.snfg_komponen
                        where state='ISTIRAHAT STOP' and kemas_2.snfg is null and kemas_2.snfg_komponen is not null and kemas_2.snfg_komponen='".$snfg_komponen."'

                        union all
                        
                        select p.snfg,sp.snfg_komponen, p.jenis_kemas, p.state, p.lanjutan, p.lanjutan_ist 
                        from kemas_2 p
                        inner join scm_planner sp on sp.snfg=p.snfg
                        where p.state='ISTIRAHAT STOP' and p.snfg is not null and sp.snfg_komponen='".$snfg_komponen."'
                    ) a
                WHERE a.snfg_komponen ='".$snfg_komponen."'
                and a.jenis_kemas = '".$jenis_kemas."'
                and a.lanjutan = '".$lanjutan."'
                ";
        $Kemas2= Kemas2::findBySql($sql)->one();
        echo Json::encode($Kemas2);
    }

    public function actionLanjutanIstKemas2snfg($snfg,$jenis_kemas,$lanjutan)
    {
        $sql="
                SELECT coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist
                FROM 
                    (
                        select sp.snfg, kemas_2.snfg_komponen, kemas_2.jenis_kemas, kemas_2.state, kemas_2.lanjutan,kemas_2.lanjutan_ist
                        from kemas_2 
                        inner join scm_planner sp on sp.snfg_komponen = kemas_2.snfg_komponen
                        where state='ISTIRAHAT STOP' and kemas_2.snfg is null and kemas_2.snfg_komponen is not null and sp.snfg='".$snfg."'

                        union all
                        
                        select p.snfg,sp.snfg_komponen, p.jenis_kemas, p.state, p.lanjutan, p.lanjutan_ist 
                        from kemas_2 p
                        inner join scm_planner sp on sp.snfg=p.snfg
                        where p.state='ISTIRAHAT STOP' and p.snfg is not null and p.snfg='".$snfg."'
                    ) a
                WHERE a.snfg ='".$snfg."'
                and a.jenis_kemas = '".$jenis_kemas."'
                and a.lanjutan = '".$lanjutan."'
                ";
        $Kemas2= Kemas2::findBySql($sql)->one();
        echo Json::encode($Kemas2);
    }

    /**
     * Updates an existing Kemas2 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $modelsKendala = $model->kendalas;

        if ($model->load(Yii::$app->request->post()) && $model->save()) 
        {
            $oldIDs = ArrayHelper::map($modelsKendala, 'id', 'id');

            $modelsKendala = Model::createMultiple(Kendala::classname(), $modelsKendala);

            Model::loadMultiple($modelsKendala, Yii::$app->request->post());

            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsKendala, 'id', 'id')));

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsKendala) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {

                        if (! empty($deletedIDs)) {
                            Kendala::deleteAll(['id' => $deletedIDs]);
                        }

                        foreach ($modelsKendala as $modelKendala) {
                            $modelKendala->kemas_2_id = $model->id;
                            if (! ($flag = $modelKendala->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    } else {
                        $transaction->rollBack();
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }

            //return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelsKendala' => (empty($modelsKendala)) ? [new Kendala] : $modelsKendala
            ]);
        }

        // $model = $this->findModel($id);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // } else {
        //     return $this->render('update', [
        //         'model' => $model,
        //     ]);
        // }
    }

    /**
     * Deletes an existing Kemas2 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetLogInput($nojadwal)
    {
        $sql="select    
                        waktu,
                        snfg as nojadwal,
                        state,
                        jenis_kemas as jenis_proses,
                        lanjutan,
                        is_done,
                        id 
              from kemas_2 where snfg='".$nojadwal."'";
        $pp= Dummy::findBySql($sql)->all();
        //print_r($pprr);
        echo Json::encode($pp);

    }

    public function actionGetLastResolusi($nojadwal)
    {
        $sql="select    
                        waktu,
                        snfg as nojadwal,
                        state,
                        jenis_kemas as jenis_proses,
                        lanjutan,
                        is_done,
                        id 
              from kemas_2 where snfg='".$nojadwal."'
              ORDER BY waktu desc 
              LIMIT 1
              ";
        $pp= Dummy::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($pp);
    }

    // Change December 4 2017
    public function actionCheckLine($nama_line)
    {
        $sql="
        SELECT nama_line,
                nojadwal as snfg,
                waktu,
                state,
                lanjutan
        FROM line_monitoring_kemas
        WHERE nama_line ='".$nama_line."'
        ";
        $Kemas2= Kemas2::findBySql($sql)->one();
        echo Json::encode($Kemas2);

    }

    /**
     * Finds the Kemas2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kemas2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kemas2::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
