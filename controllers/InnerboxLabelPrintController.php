<?php

namespace app\controllers;

use Yii;
use app\models\InnerboxLabelPrint;
use app\models\InnerboxLabelPrintSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\FlowInputSnfg;
use app\models\WeigherFgMapDevice;
use app\models\ScmPlanner;
use app\models\MasterDataKoli;
use app\models\MasterDataNa;
use yii\helpers\ArrayHelper;

/**
 * InnerboxLabelPrintController implements the CRUD actions for InnerboxLabelPrint model.
 */
class InnerboxLabelPrintController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InnerboxLabelPrint models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InnerboxLabelPrintSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InnerboxLabelPrint model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new InnerboxLabelPrint model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new InnerboxLabelPrint();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing InnerboxLabelPrint model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing InnerboxLabelPrint model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the InnerboxLabelPrint model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InnerboxLabelPrint the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InnerboxLabelPrint::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Render Scan Barcode SNFG Kemas 2 Page
     * @return mixed
     */
    public function actionCheckSnfg()
    {
        $server_name = $_SERVER['SERVER_NAME'];
        if($server_name == 'factory.pti-cosmetics.com'){
             echo "<script>location.href='".Yii::getALias('@ipUrl')."innerbox-label-print%2Fcheck-snfg';</script>";
        }
        $model = new FlowInputSnfg();

        $searchModel = new InnerboxLabelPrintSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query->where("datetime_start is not null and datetime_stop is null");

        $this->layout = '//main-sidebar-collapse'; 

        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];            
        }else{
            $frontend_ip = $_SERVER['REMOTE_ADDR'];
        }


        // Get SNFG Array
        // $sql="
        // SELECT 
        //     distinct snfg as snfg
        // FROM flow_input_snfg
        // WHERE id = ".$id;

        // $snfg_array = FlowInputSnfg::findBySql($sql)->one();

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT 
            *
        FROM weigher_fg_map_device
        WHERE frontend_ip = '".$frontend_ip."'";

        $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

        // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
        if(empty($map_device_array)){
            $sbc_ip = 'Tidak Terdaftar';
        }else{
            $sbc_ip = $map_device_array->sbc_ip;
        }

        date_default_timezone_set('Asia/Jakarta');
        $current_date = date("Y-m-d");
        $btsBawah = $current_date." 00:00:00";
        $btsAtas = $current_date." 23:59:59";
        //$current_time = date('Y-m-d h:i A');

        $sql2="select sum(qty_request) from innerbox_label_print where timestamp>='".$btsBawah."' and timestamp <= '".$btsAtas."'";

        $qty_today = InnerboxLabelPrint::findBySql($sql2)->scalar();

        if (empty($qty_today)){
            $qty_today = 0;
        }

        $sql3="select sum(qty_total) from innerbox_label_print";

        $qty_total = InnerboxLabelPrint::findBySql($sql3)->scalar();

        if (empty($qty_total)){
            $qty_total = 0;
        }

        if ($model->load(Yii::$app->request->post())) {
            $sql="select * from scm_planner where snfg='".$snfg."'";
            $check= ScmPlanner::findBySql($sql)->one();

            $sql2="select * from flow_input_snfg where snfg='".$snfg."'";
            $check2= FlowInputSnfg::findBySql($sql2)->one();

            if (!empty($check)) {
                if (!empty($check2)){
                    return $this->redirect(['create-innerbox-label', 'snfg' => $model->snfg]);
                } else {
                    echo '<script>window.alert("Anda belum melakukan scan start kemas.")</script>';
                }
            } else {
                echo '<script>window.alert("Jadwal SNFG ini tidak ada, dari planner.")</script>';
            }
            
        } else {
            return $this->render('check-snfg', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'frontend_ip' => $frontend_ip,
                'sbc_ip' => $sbc_ip,
                'qty_today' => $qty_today,
                'qty_total' => $qty_total,
            ]);
        }
    }



    /**
     * Render Page Form for Kemas 2 
     * This will render a page which the results typically comes from barcode scanning result
     * Redirected from actionCheckKemas2[/QR/Inline]
     * @param $snfg      
     * @return mixed
     */
    public function actionCreateInnerboxLabel($snfg) 
    {
        $model = new InnerboxLabelPrint();

        $this->layout = '//main-sidebar-collapse';

        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $frontend_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];            
        }else{
            $frontend_ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT 
            *
        FROM weigher_fg_map_device
        WHERE frontend_ip = '".$frontend_ip."'";

        $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

        // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
        if(empty($map_device_array)){
            $sediaan = 'Tidak Terdaftar';
        }else{
            $sediaan = $map_device_array->keterangan;
        }

        /********** GET DATA FROM SCM PLANNER *************/
        $sql1="
        
        SELECT 
            *
        FROM scm_planner
        WHERE snfg = '".$snfg."'
        ORDER BY id desc
        ";

        $scm_planner = ScmPlanner::findBySql($sql1)->one();

        if (empty($scm_planner->odoo_code)){
            $odoo_code = "";
        } else {
            $odoo_code = $scm_planner->odoo_code;
        }
        /**************************************************/


        /********* GET PRODUCT NAME FROM ODOO MRP *********/
        // $connection = Yii::$app->db2;
        // $command = $connection->createCommand("

        // SELECT
        //     pt.default_code,
        //     pt.name
        // FROM product_template pt
        // WHERE pt.default_code = :koitem
        // ", 
        // [':koitem' => $scm_planner->koitem_fg,
        // ]);

        // $data2 = $command->queryAll();

        // if (empty($data2)){
        //     $fg_name = $scm_planner->nama_fg;
        // } else {
        //     $fg_name = $data2[0]["name"];
        // }
        /**************************************************/


        /******* GET PRODUCT BARCODE FROM ODOO MRP ********/
        // $connection3 = Yii::$app->db_paragon;
        // $command3 = $connection3->createCommand("

        // SELECT
        //     pt.old_koitem,
        //     pp.barcode
        // FROM product_product pp
        // LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id
        // WHERE pt.old_koitem = :koitem
        // ", 
        // [':koitem' => $scm_planner->koitem_fg,
        // ]);

        // $data3 = $command3->queryAll();

        // if (empty($data3)){
        //     $barcode = "";
        // } else {
        //     $barcode = $data3[0]['barcode'];
        // }
        /**************************************************/


        /***** GET INNERBOX QUANTITY FROM FLOWREPORT ******/
        $sql4="
        
        SELECT 
            *
        FROM master_data_koli
        WHERE koitem ilike '%".$scm_planner->koitem_fg."%'
        ORDER BY id desc
        ";

        $data4 = MasterDataKoli::findBySql($sql4)->one();

        if (empty($data4)){
            $qty = 0;
        } else {
            $qty = $data4->qty_inside_innerbox;
        }
        /**************************************************/


        /********* GET LINE NAME FROM FLOWREPORT **********/
         $sql5="
                SELECT id,nama_line,nobatch,exp_date,nosmb,barcode,fg_name_odoo
                FROM flow_input_snfg 
                WHERE snfg = '".$snfg."' ORDER BY id DESC
        ";

        $fis = FlowInputSnfg::findBySql($sql5)->one();

        if(empty($fis)){
            $nama_line = "";
            $nobatch = "";
            $exp_date = "";
            $id = 0;
            $barcode = "";
            $fg_name = "";
        }else{
            $nama_line = $fis->nama_line;
            $nobatch = $fis->nobatch;
            $exp_date = $fis->exp_date;
            $id = $fis->id;
            $barcode = $fis->barcode;
            $fg_name = $fis->fg_name_odoo;
        }
        /**************************************************/


        /********* GET NA NUMBER FROM FLOWREPORT **********/
         $sql6="
                SELECT na_number 
                FROM master_data_na 
                WHERE koitem = '".$scm_planner->koitem_fg."'
        ";

        $na_row = MasterDataNa::findBySql($sql6)->one();

        if (empty($na_row)){
            $na_number = "";
        } else {
            $na_number = $na_row->na_number;
        }
        /**************************************************/


        /********* GET OPERATOR FROM FLOWREPORT ***********/
        $sql7="
            SELECT
                 unnest(string_to_array(nama_operator, ',')) as nama_operator
            FROM flow_input_snfg  
            WHERE snfg = '".$snfg."'";

        $list_operator = ArrayHelper::map(FlowInputSnfg::findBySql($sql7)->all(), 'nama_operator','nama_operator');
        /**************************************************/

        // if (strpos($data->nama_fg, 'ardah') !== false) {
        //     $brand = "Wardah";
        // } else if (strpos($planner->nama_fg, 'OVER') !== false) {
        //     $brand = "MAKE OVER";
        // } else if (strpos($planner->nama_fg, 'mina') !== false) {
        //     $brand = "Emina";
        // } else if (strpos($planner->nama_fg, 'utri') !== false) {
        //     $brand = "Putri";
        // } else {
        //     $brand = "";
        // }

        // Check If Post Value True
        if ($model->load(Yii::$app->request->post())) {
            $sql5="select sum(qty_request) from innerbox_label_print where snfg ='".$snfg."'";

            $qty_total = InnerboxLabelPrint::findBySql($sql5)->scalar();

            if (empty($qty_total)){
                $qty_total = 0;
            }

            $model->status = "not-start";
            $model->flow_input_snfg_id = $id;
            $model->odoo_code = $odoo_code;
            $model->na_number = $na_number;
            $model->sediaan = $sediaan;
            $model->barcode = $model->barcode;
            $model->nama_fg = $model->nama_fg;

            $model->qty_total = $qty_total+$model->qty_request;
            if ($model->save()){
                return $this->redirect(['check-snfg']);
            }
        } else {
            return $this->render('create-innerbox-label', [
                'model' => $model,
                'snfg' => $snfg,
                // 'data' => $data,
                // 'data2' => $data2,
                'nama_line' => $nama_line,
                'fg_name' => $fg_name,
                'barcode' => $barcode,
                'qty' => $qty,
                'list_operator' => $list_operator,
                'na_number' => $na_number,
                'kode_odoo' => $odoo_code,
                'nobatch' => $nobatch,
                'exp_date' => $exp_date,
                'sediaan' => $sediaan
                // 'jenis_proses_list' => $jenis_proses_list,
            ]);
        }
    }

    /**
     * Stop Jadwal Kemas 2 Inline Function
     * @param $snfg      
     * @return mixed
     */
    public function actionCheckSnfgExist($snfg)
    {
        $model = new InnerboxLabelPrint();
 
        // Populate Grid Data
        $searchModel = new InnerboxLabelPrintSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';

        $sql="select * from scm_planner where snfg='".$snfg."'";
        $check= ScmPlanner::findBySql($sql)->one();

        $sql2="select * from flow_input_snfg where snfg='".$snfg."'";
        $check2= FlowInputSnfg::findBySql($sql2)->one();

        if (!empty($check)) {
            if (!empty($check2)){
                echo 1;
            } else {
                echo 2;
            }
        } else {
            echo 0;
        }    
    }

}
