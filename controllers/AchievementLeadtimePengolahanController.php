<?php

namespace app\controllers;

use Yii;
use app\models\AchievementLeadtimePengolahan;
use app\models\AchievementLeadtimePengolahanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AchievementLeadtimePengolahanController implements the CRUD actions for AchievementLeadtimePengolahan model.
 */
class AchievementLeadtimePengolahanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AchievementLeadtimePengolahan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        // $searchModel->achievement=127.5;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModel2 = new AchievementLeadtimePengolahanSearch();
        $searchModel2->achievement='>500';
        $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModel2' => $searchModel2,
            'dataProvider2' => $dataProvider2,
        ]);
    }

    public function actionSemisolid()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        // $searchModel->achievement=127.5;
        $searchModel->sediaan='S';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModel2 = new AchievementLeadtimePengolahanSearch();
        $searchModel2->sediaan='S';
        $searchModel2->achievement='>500';
        $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModel2' => $searchModel2,
            'dataProvider2' => $dataProvider2,
        ]);
    }

    public function actionSemisolidOlahpremix()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        $searchModel->sediaan='S';
        $searchModel->jenis_olah='OLAH PREMIX';
        $info = 'Semisolid OLAH PREMIX';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionSemisolidOlah1()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        $searchModel->sediaan='S';
        $searchModel->jenis_olah='OLAH 1';
        $info = 'Semisolid OLAH 1';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionSemisolidOlah2()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        $searchModel->sediaan='S';
        $searchModel->jenis_olah='OLAH 2';
        $info = 'Semisolid OLAH 2';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionSemisolidAdjust()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        $searchModel->sediaan='S';
        $searchModel->jenis_olah='ADJUST';
        $info = 'Semisolid ADJUST';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionPowder()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        // $searchModel->achievement=127.5;
        $searchModel->sediaan='P';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModel2 = new AchievementLeadtimePengolahanSearch();
        $searchModel2->sediaan='P';
        $searchModel2->achievement='>500';
        $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModel2' => $searchModel2,
            'dataProvider2' => $dataProvider2,
        ]);
    }

    public function actionPowderOlahpremix()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        $searchModel->sediaan='P';
        $searchModel->jenis_olah='OLAH PREMIX';
        $info = 'Powder Olah Premix';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionPowderOlah1()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        $searchModel->sediaan='P';
        $searchModel->jenis_olah='OLAH 1';
        $info = 'Powder OLAH 1';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionPowderOlah2()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        $searchModel->sediaan='P';
        $searchModel->jenis_olah='OLAH 2';
        $info = 'Powder OLAH 2';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionPowderAdjust()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        $searchModel->sediaan='P';
        $searchModel->jenis_olah='ADJUST';
        $info = 'Powder ADJUST';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionLiquid()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        // $searchModel->achievement=127.5;
        $searchModel->sediaan='L';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModel2 = new AchievementLeadtimePengolahanSearch();
        $searchModel2->sediaan='L';
        $searchModel2->achievement='>500';
        $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModel2' => $searchModel2,
            'dataProvider2' => $dataProvider2,
        ]);
    }

        public function actionLiquidOlahpremix()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        $searchModel->sediaan='L';
        $searchModel->jenis_olah='OLAH PREMIX';
        $info = 'Liquid OLAH PREMIX';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionLiquidOlah1()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        $searchModel->sediaan='L';
        $searchModel->jenis_olah='OLAH 1';
        $info = 'Liquid OLAH 1';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionLiquidOlah2()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        $searchModel->sediaan='L';
        $searchModel->jenis_olah='OLAH 2';
        $info = 'Liquid OLAH 2';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionLiquidAdjust()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        $searchModel->sediaan='L';
        $searchModel->jenis_olah='ADJUST';
        $info = 'Liquid ADJUST';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'info' => $info,
        ]);
    }

    public function actionIndex2()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDashboard()
    {
        $this->layout = '//main-sidebar-collapse';
        return $this->render('dashboard', [
        ]);
    }

    public function actionRunning()
    {
        $searchModel = new AchievementLeadtimePengolahanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->layout = '//main-sidebar-collapse';
        
        return $this->render('running', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AchievementLeadtimePengolahan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AchievementLeadtimePengolahan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AchievementLeadtimePengolahan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AchievementLeadtimePengolahan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AchievementLeadtimePengolahan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AchievementLeadtimePengolahan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AchievementLeadtimePengolahan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AchievementLeadtimePengolahan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
