<?php

namespace app\controllers;

use Yii;
use app\models\ScmPlanner;
use app\models\ScmPlannerSearch;
use yii\console\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * LogReceiveNdcController implements the CRUD actions for LogReceiveNdc model.
 */
class ConsoleUpdateScheduleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionUpdateData(){
        $check_schedule = ScmPlanner::find()->where("cron_update is null and timestamp > 'now'::timestamp - '1 week'::interval ")->all();
        // $check_schedule = ScmPlanner::find()->where("cron_update is null and nama_fg ilike '%labor%'")->orderBy(['id'=>SORT_DESC])->limit(50)->all();
        $check_odoo_null = ScmPlanner::find()->select(['koitem_fg'])->distinct()->where("(odoo_code is null or odoo_code = '') and timestamp > 'now'::timestamp - '2 months'::interval")->all();

        // print_r($check_log);
        foreach($check_schedule as $schedule){
            //Update nama fg
            $sql = "SELECT name,default_code as odoo_code,korporate_code,ean_13 from product_template where korporate_code = :koitem_fg";
            $detailFg = Yii::$app->db_mdm->createCommand($sql,[':koitem_fg'=>trim($schedule->koitem_fg)])->queryOne();

            if (!empty($detailFg)){
                $nama_fg = $detailFg['name'];
                $update_fg_name = true;

                $sql_update = "UPDATE scm_planner SET nama_fg = :nama_fg, cron_update = 1 WHERE id = :id";
                $update = Yii::$app->db->createCommand($sql_update,
                                                            [
                                                                ':nama_fg'=>$nama_fg,
                                                                ':id'=>$schedule->id,
                                                            ])->execute();

            }
            else{
                $sql_update = "UPDATE scm_planner SET nama_fg = :nama_fg, cron_update = 1 WHERE id = :id";
                $update = Yii::$app->db->createCommand($sql_update,
                                                            [
                                                                ':nama_fg'=>$schedule->nama_fg,
                                                                ':id'=>$schedule->id,
                                                            ])->execute();
            }


            //Update nama bulk
            $sql = "SELECT name FROM mdm.product_template WHERE korporate_code = :koitem_bulk";
            $detailBulk = Yii::$app->db->createCommand($sql,[':koitem_bulk'=>$schedule->koitem_bulk])->queryOne();

            if (!empty($detailBulk))
            {
                $nama_bulk = $detailBulk->name;
                $update_bulk_name = true;
                $sql_update = "UPDATE scm_planner SET nama_bulk = :nama_bulk, cron_update = 1 WHERE id = :id";
                $update = Yii::$app->db->createCommand($sql_update,
                                                            [
                                                                ':nama_bulk'=>$nama_bulk,
                                                                ':id'=>$schedule->id,
                                                            ])->execute();
            }else
            {
                $sql_update = "UPDATE scm_planner SET nama_bulk = :nama_bulk, cron_update = 1 WHERE id = :id";
                $update = Yii::$app->db->createCommand($sql_update,
                                                            [
                                                                ':nama_bulk'=>$schedule->nama_bulk,
                                                                ':id'=>$schedule->id,
                                                            ])->execute();

            }
        }

        //Update odoo code
        foreach($check_odoo_null as $schedule){
            $sql = "SELECT name,default_code as odoo_code,korporate_code,ean_13 from product_template where korporate_code = :koitem_fg";
            $detailFg = Yii::$app->db_mdm->createCommand($sql,[':koitem_fg'=>trim($schedule->koitem_fg)])->queryOne();

            // $sql = "SELECT
            //             pt.default_code as odoo_code,
            //             pt.name
            //         FROM product_template pt
            //         WHERE pt.old_koitem = :koitem_fg";
            // $detailFg = Yii::$app->db_paragon->createCommand($sql,[':koitem_fg'=>trim($schedule->koitem_fg)])->queryOne();

            if (!empty($detailFg)){
                $odoo_code = $detailFg['odoo_code'];
                $update_odoo_code = true;

                $sql_update = "UPDATE scm_planner SET odoo_code = :odoo_code WHERE koitem_fg = :koitem_fg and (odoo_code is null or odoo_code = '')";
                $update = Yii::$app->db->createCommand($sql_update,
                                                            [
                                                                ':odoo_code'=>$odoo_code,
                                                                ':koitem_fg'=>$schedule->koitem_fg,
                                                            ])->execute();
            }
            // else{
            //     $sql_update = "UPDATE scm_planner SET odoo_code = :odoo_code WHERE koitem_fg = :koitem_fg and (odoo_code is null or odoo_code = '')";
            //     $update = Yii::$app->db->createCommand($sql_update,
            //                                             [
            //                                                 ':odoo_code'=>$odoo_code,
            //                                                 ':koitem_fg'=>$schedule->koitem_fg,
            //                                             ])->execute();
            //     // $sql = "SELECT
            //     //             pt.default_code as odoo_code,
            //     //             pt.name
            //     //         FROM product_template pt
            //     //         WHERE pt.old_koitem = :koitem_fg";
            //     // $detailFgVarcos = Yii::$app->db_varcos->createCommand($sql,[':koitem_fg'=>trim($schedule->koitem_fg)])->queryOne();
            //     // if (!empty($detailFgVarcos))
            //     // {
            //     //     $odoo_code = $detailFgVarcos['odoo_code'];
            //     //     $update_odoo_code = true;
            //     // }else{
            //     //     $odoo_code = $schedule->odoo_code;
            //     //     $update_odoo_code = false;
            //     // }

            // }

        }



    }

    /**
     * Finds the LogReceiveNdc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogReceiveNdc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogReceiveNdc::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
