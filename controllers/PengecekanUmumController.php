<?php

namespace app\controllers;

use Yii;
use app\models\PengecekanUmum;
use app\models\PengecekanUmumSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\PengecekanSemsol;
use app\models\PengecekanPowder;
use app\models\PengecekanLiquid;
use app\models\FormScanSnfg;
use app\models\ScmPlanner;
use app\models\FlowInputSnfg;
use app\models\IdentitasPengecekanFg;

/**
 * PengecekanUmumController implements the CRUD actions for PengecekanUmum model.
 */
class PengecekanUmumController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PengecekanUmum models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PengecekanUmumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PengecekanUmum model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $data_pengecekan = PengecekanUmum::find()->where(['id'=>$id])->one();
        $id_identitas = $data_pengecekan->id_identitas_pengecekan_fg;
        $model_fg = IdentitasPengecekanFg::find()->where(['id'=>$id_identitas])->one();
        
        if ($model_fg->sediaan == 'S') {
            return $this->redirect(['view-semsol','id'=>$id]);

        } elseif ($model_fg->sediaan == 'P') {
            return $this->redirect(['view-powder','id'=>$id]);

        } else {            
            return $this->redirect(['view-liquid','id'=>$id]);

        }
        // print_r($model_fg);
    }

    public function actionViewLiquid($id)
    {
        
        $data_pengecekan = PengecekanUmum::find()->where(['id'=>$id])->one();
        $id_identitas = $data_pengecekan->id_identitas_pengecekan_fg;
        $model_fg = IdentitasPengecekanFg::find()->where(['id'=>$id_identitas])->one();
        $model_liquid = PengecekanLiquid::find()->where(['id_pengecekan_umum'=>$id])->one();
        
        return $this->render('view_liquid', 
        [
            'model' => $this->findModel($id),
            'model_fg' => $model_fg,
            'model_liquid' => $model_liquid,
        ]);

        // print_r('<pre>');
        // print_r($sediaan);
        // print_r('<pre>');

    }
    public function actionViewSemsol($id)
    {
        
        $data_pengecekan = PengecekanUmum::find()->where(['id'=>$id])->one();
        $id_identitas = $data_pengecekan->id_identitas_pengecekan_fg;
        $model_fg = IdentitasPengecekanFg::find()->where(['id'=>$id_identitas])->one();
        $model_semsol = PengecekanSemsol::find()->where(['id_pengecekan_umum'=>$id])->one();
        
        return $this->render('view_semsol', 
        [
            'model' => $this->findModel($id),
            'model_fg' => $model_fg,
            'model_semsol' => $model_semsol,
        ]);

        // print_r('<pre>');
        // print_r($sediaan);
        // print_r('<pre>');

    }
    public function actionViewPowder($id)
    {
        
        $data_pengecekan = PengecekanUmum::find()->where(['id'=>$id])->one();
        $id_identitas = $data_pengecekan->id_identitas_pengecekan_fg;
        $model_fg = IdentitasPengecekanFg::find()->where(['id'=>$id_identitas])->one();
        $model_powder = PengecekanPowder::find()->where(['id_pengecekan_umum'=>$id])->one();
        
        return $this->render('view_powder', 
        [
            'model' => $this->findModel($id),
            'model_fg' => $model_fg,
            'model_powder' => $model_powder,
        ]);

        // print_r('<pre>');
        // print_r($sediaan);
        // print_r('<pre>');

    }

    /**
     * Creates a new PengecekanUmum model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($snfg)
    {
        $scm_planner = ScmPlanner::find()->where(['snfg'=>$snfg])->one();
        $model_fg = IdentitasPengecekanFg::find()->where(['snfg'=>$snfg])->one();

        if ($scm_planner->sediaan == 'S') {
            return $this->redirect(['create-semsol','snfg'=>$snfg]);

        } elseif ($scm_planner->sediaan == 'P') {
            return $this->redirect(['create-powder','snfg'=>$snfg]);

        } else {            
            return $this->redirect(['create-liquid','snfg'=>$snfg]);

        }
        // print_r($model_fg);
    }

    public function actionCreatePowder($snfg)
    {
        $this->layout = '//main-sidebar-collapse'; 
        $model = new PengecekanUmum();
        date_default_timezone_set('Asia/Jakarta');
        $start_time = date('Y-m-d H:i:s');

        $kode_item = explode('/',$snfg)[0];

        //Cek apakah item tersebut FG atau Packaging
        $sql = "SELECT pt.default_code, pc.complete_name 
                FROM product_template pt
                JOIN product_category pc on pt.categ_id = pc.id
                WHERE default_code = :kode_item";
        $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();

        if(empty($product_detail)){
            $sql = "SELECT pt.default_code, pc.complete_name 
                FROM varcos.product_template pt
                JOIN varcos.product_category pc on pt.categ_id = pc.id
                WHERE default_code = :kode_item";
            $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();
        }

        if (!empty($product_detail)){
            if (strpos(strtolower($product_detail['complete_name']),'packaging')!==false){
                $category = 'packaging';
            }else{
                $category = 'fg';
            }            
        }else{
            $category = 'null';
        }

        //Cek apakah item tersebut baru mulai cek QC atau bukan
        $model_fg = IdentitasPengecekanFg::find()->where(['snfg'=>$snfg])->one();
        if (empty($model_fg)){
            $model_fg = new IdentitasPengecekanFg();

            $palet=1;
            $siklus=1;

        }else{
            $pengecekan_terakhir = PengecekanUmum::find()->where(['snfg'=>$snfg])->orderby(['id'=>SORT_DESC])->one();
            $siklus_sebelumnya = $pengecekan_terakhir['siklus'];
            $palet_sebelumnya = $pengecekan_terakhir['no_palet'];
            $is_done = $pengecekan_terakhir['is_done'];

            //cek palet dan siklus terakhir, jika siklus ==3 maka posisi pengecekan fg diinsert data baru
            if (empty($is_done) and $siklus_sebelumnya < 3)
            {
                $siklus = $siklus_sebelumnya+1;
                $palet = $palet_sebelumnya;
                
            }else{ //jika siklus sebelumnya adalah 3 maka insert data baru di posisi_pengecekan_fg

                $siklus = 1;
                $palet = $palet_sebelumnya+1;
                
            }
        }
 
        // $model_semsol = new PengecekanSemsol();
        $model_powder = new PengecekanPowder();
        // $model_liquid = new PengecekanLiquid();


        //Check Last State of a SNFG
        $command = Yii::$app->db->createCommand("SELECT sum(aql_per_siklus) FROM pengecekan_umum WHERE snfg ='".$snfg."'");
        $aql_pengecekan = $command->queryOne()['sum'];

        $row3="
        
        SELECT total_aql
        FROM identitas_pengecekan_fg
        WHERE snfg = '".$snfg."'
        ";

        $aql_total = IdentitasPengecekanFg::findBySql($row3)->one();
        $aql_sisa = $aql_total['total_aql'] - $aql_pengecekan;


        $command1 = Yii::$app->db->createCommand("SELECT sum(sampel_retain) FROM pengecekan_umum WHERE snfg ='".$snfg."'");
        $sampel_retain = $command1->queryOne()['sum'];


        $command2 = Yii::$app->db->createCommand("SELECT sum(sampel_mikro) FROM pengecekan_umum WHERE snfg ='".$snfg."'");
        $sampel_mikro = $command2->queryOne()['sum'];

        $sql="
        
        SELECT 
            nama_fg,
            snfg,
            jumlah_pcs,
            sediaan,
            due
        FROM scm_planner
        WHERE snfg = '".$snfg."'
        ";


        $scmplanner = ScmPlanner::findBySql($sql)->one();
        
        $sql2="
        SELECT
            nama_line,
            nobatch
        FROM flow_input_snfg
        WHERE snfg = '".$snfg."' AND nobatch is not null order by datetime_start desc
        ";

        $flowinputsnfg = FlowInputSnfg::findBySql($sql2)->one();

        $sql0="
        SELECT
            sistem_cek,
            extra_proses,
            pengerjaan_extra_proses,
            jumlah_komponen_bulk,
            aql_per_palet,
            jumlah_komponen_penyusun,
            komponen_penyusun_1,
            komponen_penyusun_2,
            komponen_penyusun_3,
            komponen_penyusun_4,
            komponen_penyusun_5,
            komponen_penyusun_6,
            komponen_penyusun_7,
            komponen_penyusun_8
        FROM pengecekan_umum
        WHERE snfg = '".$snfg."' order by jam_mulai desc
        ";

        $pengecekanumum = PengecekanUmum::findBySql($sql0)->one();
                
        // berjalan ketika tombol create ditekan
        if ($model->load(Yii::$app->request->post()) ) 
        {

            $model_fg->load(Yii::$app->request->post());
            $model_fg->save();

            $model->id_identitas_pengecekan_fg = $model_fg->id;
            $model->jam_selesai = date('Y-m-d H:i:s');
            $model->snfg = $model_fg->snfg;
            if ($model->sistem_cek == 'QA System' || $model->siklus==3){
                $model->is_done=1;
            }
            $model->save();

            $model_powder->load(Yii::$app->request->post());
            $model_powder->id_pengecekan_umum = $model->id;
            $model_powder->save(); 

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create_powder', [
                'model' => $model,
                'model_fg' => $model_fg, 
                'model_powder'=> $model_powder,
                'scmplanner' => $scmplanner,
                'flowinputsnfg' => $flowinputsnfg,
                'pengecekanumum' => $pengecekanumum,
                'palet'=>$palet,
                'siklus'=>$siklus,
                'start_time'=>$start_time,
                'aql_sisa' =>$aql_sisa,
                'sampel_mikro' => $sampel_mikro,
                'sampel_retain' => $sampel_retain,
                'item_category' => $category,
            ]);
        }
    }

    public function actionCreateLiquid($snfg)
    {
        $this->layout = '//main-sidebar-collapse'; 
        $model = new PengecekanUmum();
        date_default_timezone_set('Asia/Jakarta');
        $start_time = date('Y-m-d H:i:s');

        $kode_item = explode('/',$snfg)[0];

        //Cek apakah item tersebut FG atau Packaging
        $sql = "SELECT pt.default_code, pc.complete_name 
                FROM product_template pt
                JOIN product_category pc on pt.categ_id = pc.id
                WHERE default_code = :kode_item";
        $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();

        if(empty($product_detail)){
            $sql = "SELECT pt.default_code, pc.complete_name 
                FROM varcos.product_template pt
                JOIN varcos.product_category pc on pt.categ_id = pc.id
                WHERE default_code = :kode_item";
            $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();
        }

        if (!empty($product_detail)){
            if (strpos(strtolower($product_detail['complete_name']),'packaging')!==false){
                $category = 'packaging';
            }else{
                $category = 'fg';
            }            
        }else{
            $category = 'null';
        }
   
        //Cek apakah item tersebut baru mulai cek QC atau bukan
        $model_fg = IdentitasPengecekanFg::find()->where(['snfg'=>$snfg])->one();
        if (empty($model_fg)){//fg baru
            $model_fg = new IdentitasPengecekanFg();

            $palet=1;
            $siklus=1;

        }else{
            $pengecekan_terakhir = PengecekanUmum::find()->where(['snfg'=>$snfg])->orderby(['id'=>SORT_DESC])->one();
            $siklus_sebelumnya = $pengecekan_terakhir['siklus'];
            $palet_sebelumnya = $pengecekan_terakhir['no_palet'];
            $is_done = $pengecekan_terakhir['is_done'];

            //cek palet dan siklus terakhir, jika siklus ==3 maka posisi pengecekan fg diinsert data baru
            if (empty($is_done) and $siklus_sebelumnya < 3)
            {
                $siklus = $siklus_sebelumnya+1;
                $palet = $palet_sebelumnya;
                
            }else{ //jika siklus sebelumnya adalah 3 maka insert data baru di posisi_pengecekan_fg

                $siklus = 1;
                $palet = $palet_sebelumnya+1;
                
            }
        }
 
        $model_liquid = new PengecekanLiquid();


        //Check Last State of a SNFG
        $command = Yii::$app->db->createCommand("SELECT sum(aql_per_siklus) FROM pengecekan_umum WHERE snfg ='".$snfg."'");
        $aql_pengecekan = $command->queryOne()['sum'];

        $row3="
        
        SELECT total_aql
        FROM identitas_pengecekan_fg
        WHERE snfg = '".$snfg."'
        ";

        $aql_total = IdentitasPengecekanFg::findBySql($row3)->one();
        $aql_sisa = $aql_total['total_aql'] - $aql_pengecekan;


        $command1 = Yii::$app->db->createCommand("SELECT sum(sampel_retain) FROM pengecekan_umum WHERE snfg ='".$snfg."'");
        $sampel_retain = $command1->queryOne()['sum'];


        $command2 = Yii::$app->db->createCommand("SELECT sum(sampel_mikro) FROM pengecekan_umum WHERE snfg ='".$snfg."'");
        $sampel_mikro = $command2->queryOne()['sum'];

        $sql="
        
        SELECT 
            nama_fg,
            snfg,
            jumlah_pcs,
            sediaan,
            due
        FROM scm_planner
        WHERE snfg = '".$snfg."'
        ";


        $scmplanner = ScmPlanner::findBySql($sql)->one();
        
        $sql2="
        SELECT
            nama_line,
            nobatch
        FROM flow_input_snfg
        WHERE snfg = '".$snfg."' AND nobatch is not null order by datetime_start desc
        ";

        $flowinputsnfg = FlowInputSnfg::findBySql($sql2)->one();

        $sql0="
        SELECT
            aql_per_palet,
            sistem_cek,
            extra_proses,
            pengerjaan_extra_proses,
            jumlah_komponen_bulk,
            jumlah_komponen_penyusun,
            komponen_penyusun_1,
            komponen_penyusun_2,
            komponen_penyusun_3,
            komponen_penyusun_4,
            komponen_penyusun_5,
            komponen_penyusun_6,
            komponen_penyusun_7,
            komponen_penyusun_8
        FROM pengecekan_umum
        WHERE snfg = '".$snfg."' order by jam_mulai desc
        ";

        $pengecekanumum = PengecekanUmum::findBySql($sql0)->one();
                
        // berjalan ketika tombol create ditekan
        if ($model->load(Yii::$app->request->post()) ) 
        {

            $model_fg->load(Yii::$app->request->post());
            $model_fg->save();

            $model->id_identitas_pengecekan_fg = $model_fg->id;
            $model->jam_selesai = date('Y-m-d H:i:s');
            $model->snfg = $model_fg->snfg;
            if ($model->sistem_cek == 'QA System' || $model->siklus==3){
                $model->is_done=1;
            }
            $model->save();

            $model_liquid->load(Yii::$app->request->post());
            $model_liquid->id_pengecekan_umum = $model->id;
            $model_liquid->save();

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create_liquid', [
                'model' => $model,
                'model_fg' => $model_fg, 
                'model_liquid'=> $model_liquid,
                'scmplanner' => $scmplanner,
                'flowinputsnfg' => $flowinputsnfg,
                'pengecekanumum' => $pengecekanumum,
                'palet'=>$palet,
                'siklus'=>$siklus,
                'start_time'=>$start_time,
                'aql_sisa' =>$aql_sisa,
                'sampel_mikro' => $sampel_mikro,
                'sampel_retain' => $sampel_retain,
                'item_category' => $category,
            ]);
        }
    }

    public function actionCreateSemsol($snfg)
    {
        $this->layout = '//main-sidebar-collapse'; 
        $model = new PengecekanUmum();
        date_default_timezone_set('Asia/Jakarta');
        $start_time = date('Y-m-d H:i:s');
   
        $kode_item = explode('/',$snfg)[0];

        //Cek apakah item tersebut FG atau Packaging
        $sql = "SELECT pt.default_code, pc.complete_name 
                FROM product_template pt
                JOIN product_category pc on pt.categ_id = pc.id
                WHERE default_code = :kode_item";
        $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();

        if(empty($product_detail)){
            $sql = "SELECT pt.default_code, pc.complete_name 
                FROM varcos.product_template pt
                JOIN varcos.product_category pc on pt.categ_id = pc.id
                WHERE default_code = :kode_item";
            $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();
        }

        if (!empty($product_detail)){
            if (strpos(strtolower($product_detail['complete_name']),'packaging')!==false){
                $category = 'packaging';
            }else{
                $category = 'fg';
            }            
        }else{
            $category = 'null';
        }



        //Cek apakah item tersebut baru mulai cek QC atau bukan
        $model_fg = IdentitasPengecekanFg::find()->where(['snfg'=>$snfg])->one();
        if (empty($model_fg)){
            $model_fg = new IdentitasPengecekanFg();

            $palet=1;
            $siklus=1;

        }else{
            $pengecekan_terakhir = PengecekanUmum::find()->where(['snfg'=>$snfg])->orderby(['id'=>SORT_DESC])->one();
            $siklus_sebelumnya = $pengecekan_terakhir['siklus'];
            $palet_sebelumnya = $pengecekan_terakhir['no_palet'];
            $is_done = $pengecekan_terakhir['is_done'];
            
            //cek palet dan siklus terakhir, jika siklus ==3 maka posisi pengecekan fg diinsert data baru
            if (empty($is_done) and $siklus_sebelumnya < 3)
            {
                $siklus = $siklus_sebelumnya+1;
                $palet = $palet_sebelumnya;
                
            }else{ //jika siklus sebelumnya adalah 3 maka insert data baru di posisi_pengecekan_fg

                $siklus = 1;
                $palet = $palet_sebelumnya+1;
            }
        }
 
        $model_semsol = new PengecekanSemsol();

        //Check Last State of a SNFG
        $command = Yii::$app->db->createCommand("SELECT sum(aql_per_siklus) FROM pengecekan_umum WHERE snfg ='".$snfg."'");
        $aql_pengecekan = $command->queryOne()['sum'];

        $row3="
        
        SELECT total_aql
        FROM identitas_pengecekan_fg
        WHERE snfg = '".$snfg."'
        ";

        $aql_total = IdentitasPengecekanFg::findBySql($row3)->one();
        $aql_sisa = $aql_total['total_aql'] - $aql_pengecekan;


        $command1 = Yii::$app->db->createCommand("SELECT sum(sampel_retain) FROM pengecekan_umum WHERE snfg ='".$snfg."'");
        $sampel_retain = $command1->queryOne()['sum'];


        $command2 = Yii::$app->db->createCommand("SELECT sum(sampel_mikro) FROM pengecekan_umum WHERE snfg ='".$snfg."'");
        $sampel_mikro = $command2->queryOne()['sum'];

        $sql="
        
        SELECT 
            nama_fg,
            snfg,
            jumlah_pcs,
            sediaan,
            due
        FROM scm_planner
        WHERE snfg = '".$snfg."'
        ";


        $scmplanner = ScmPlanner::findBySql($sql)->one();
        
        $sql2="
        SELECT
            nama_line,
            nobatch
        FROM flow_input_snfg
        WHERE snfg = '".$snfg."' AND nobatch is not null order by datetime_start desc
        ";

        $flowinputsnfg = FlowInputSnfg::findBySql($sql2)->one();

        $sql0="
        SELECT
            aql_per_palet,
            sistem_cek,
            extra_proses,
            pengerjaan_extra_proses,
            jumlah_komponen_bulk,
            jumlah_komponen_penyusun,
            komponen_penyusun_1,
            komponen_penyusun_2,
            komponen_penyusun_3,
            komponen_penyusun_4,
            komponen_penyusun_5,
            komponen_penyusun_6,
            komponen_penyusun_7,
            komponen_penyusun_8
        FROM pengecekan_umum
        WHERE snfg = '".$snfg."' ORDER by jam_mulai desc
        ";

        $pengecekanumum = PengecekanUmum::findBySql($sql0)->one();
                
        // berjalan ketika tombol create ditekan
        if ($model->load(Yii::$app->request->post()) ) 
        {

            $model_fg->load(Yii::$app->request->post());
            $model_fg->save();

            $model->id_identitas_pengecekan_fg = $model_fg->id;
            $model->jam_selesai = date('Y-m-d H:i:s');
            $model->snfg = $model_fg->snfg;
            $model->save();
            
            $model_semsol->load(Yii::$app->request->post());
            $model_semsol->id_pengecekan_umum = $model->id;
            $model_semsol->save();

            // $check = IdentitasPengecekanFg::find()->where(['snfg'=>$model_fg->snfg])->one();

            if ($model->siklus == 3){
                $connection3 = Yii::$app->getDb();
                $command3 = $connection3->createCommand("
                    UPDATE posisi_pengecekan_fg
                    SET stop = :stop,
                        is_done = 1
                    WHERE snfg = :snfg
                    AND palet = :palet; 
                    ", 
                    [':stop'=> date('Y-m-d h:i:s'),
                    ':snfg'=>$model_fg->snfg,
                    ':palet'=>$model->no_palet,
                    
                ]);

                $result3 = $command3->queryAll();
            }
            
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create_semsol', [
                'model' => $model,
                'model_fg' => $model_fg, 
                'model_semsol'=> $model_semsol,
                'scmplanner' => $scmplanner,
                'flowinputsnfg' => $flowinputsnfg,
                'pengecekanumum' => $pengecekanumum,
                'palet'=>$palet,
                'siklus'=>$siklus,
                'start_time'=>$start_time,
                'aql_sisa' =>$aql_sisa,
                'sampel_mikro' => $sampel_mikro,
                'sampel_retain' => $sampel_retain,
                'item_category' => $category,
            ]);
        }
    }    

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $data_pengecekan = PengecekanUmum::find()->where(['id'=>$id])->one();
        $id_identitas = $data_pengecekan->id_identitas_pengecekan_fg;
        $model_fg = IdentitasPengecekanFg::find()->where(['id'=>$id_identitas])->one();
        
        if ($model_fg->sediaan == 'S') {
            return $this->redirect(['update-semsol','id'=>$id]);

        } elseif ($model_fg->sediaan == 'P') {
            return $this->redirect(['update-powder','id'=>$id]);

        } elseif ($model_fg->sediaan == 'L') {            
            return $this->redirect(['update-liquid','id'=>$id]);

        }else{
            
        }
    }

    public function actionUpdateLiquid($id)
    {
        
        $this->layout = '//main-sidebar-collapse'; 
        $model = $this->findModel($id);
        $data_pengecekan = PengecekanUmum::find()->where(['id'=>$id])->one();
        $id_identitas = $data_pengecekan->id_identitas_pengecekan_fg;
        $model_fg = IdentitasPengecekanFg::find()->where(['id'=>$id_identitas])->one();
        $model_liquid = PengecekanLiquid::find()->where(['id_pengecekan_umum'=>$id])->one();
        $kode_item = explode('/',$model->snfg)[0];


        //Cek apakah item tersebut FG atau Packaging
        $sql = "SELECT pt.default_code, pc.complete_name 
                FROM product_template pt
                JOIN product_category pc on pt.categ_id = pc.id
                WHERE default_code = :kode_item";
        $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();

        if(empty($product_detail)){
            $sql = "SELECT pt.default_code, pc.complete_name 
                FROM varcos.product_template pt
                JOIN varcos.product_category pc on pt.categ_id = pc.id
                WHERE default_code = :kode_item";
            $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();
        }

        if (!empty($product_detail)){
            if (strpos(strtolower($product_detail['complete_name']),'packaging')!==false){
                $category = 'packaging';
            }else{
                $category = 'fg';
            }            
        }else{
            $category = 'null';
        }
        
        if ($model->load(Yii::$app->request->post()) ) 
        {

            $model_fg->load(Yii::$app->request->post());
            $model_fg->save();

            $model_liquid->load(Yii::$app->request->post());
            $model_liquid->save();
            date_default_timezone_set('Asia/Jakarta');
            $model->jam_selesai = date('Y-m-d H:i:s');
            $model->save();
            
            return $this->redirect(['view-liquid', 'id' => $model->id]);
        } else {
            
                return $this->render('update_liquid', [
                    'model' => $model,
                    'model_fg' => $model_fg, 
                    'model_liquid'=> $model_liquid,
                    'item_category'=> $category,
                ]);
        }
    }

    public function actionUpdateSemsol($id)
    {
        
        $this->layout = '//main-sidebar-collapse'; 
        $model = $this->findModel($id);
        $data_pengecekan = PengecekanUmum::find()->where(['id'=>$id])->one();
        $id_identitas = $data_pengecekan->id_identitas_pengecekan_fg;
        $model_fg = IdentitasPengecekanFg::find()->where(['id'=>$id_identitas])->one();
        $model_semsol = PengecekanSemsol::find()->where(['id_pengecekan_umum'=>$id])->one();
        $kode_item = explode('/',$model->snfg)[0];

        //Cek apakah item tersebut FG atau Packaging
        $sql = "SELECT pt.default_code, pc.complete_name 
                FROM product_template pt
                JOIN product_category pc on pt.categ_id = pc.id
                WHERE default_code = :kode_item";
        $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();

        if(empty($product_detail)){
            $sql = "SELECT pt.default_code, pc.complete_name 
                FROM varcos.product_template pt
                JOIN varcos.product_category pc on pt.categ_id = pc.id
                WHERE default_code = :kode_item";
            $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();
        }

        if (!empty($product_detail)){
            if (strpos(strtolower($product_detail['complete_name']),'packaging')!==false){
                $category = 'packaging';
            }else{
                $category = 'fg';
            }            
        }else{
            $category = 'null';
        }
        
        if ($model->load(Yii::$app->request->post()) ) 
        {

            $model_fg->load(Yii::$app->request->post());
            $model_fg->save();

            $model_semsol->load(Yii::$app->request->post());
            $model_semsol->save();
            date_default_timezone_set('Asia/Jakarta');
            $model->jam_selesai = date('Y-m-d H:i:s');
            $model->save();
            
            return $this->redirect(['view-semsol', 'id' => $model->id]);
        } else {
            
                return $this->render('update_semsol', [
                    'model' => $model,
                    'model_fg' => $model_fg, 
                    'model_semsol'=> $model_semsol,
                    'item_category'=> $category,
                ]);
        }
    }

    public function actionUpdatePowder($id)
    {
        
        $this->layout = '//main-sidebar-collapse'; 
        $model = $this->findModel($id);
        $data_pengecekan = PengecekanUmum::find()->where(['id'=>$id])->one();
        $id_identitas = $data_pengecekan->id_identitas_pengecekan_fg;
        $model_fg = IdentitasPengecekanFg::find()->where(['id'=>$id_identitas])->one();
        $model_powder = PengecekanPowder::find()->where(['id_pengecekan_umum'=>$id])->one();
        $kode_item = explode('/',$model->snfg)[0];

        //Cek apakah item tersebut FG atau Packaging
        $sql = "SELECT pt.default_code, pc.complete_name 
                FROM product_template pt
                JOIN product_category pc on pt.categ_id = pc.id
                WHERE default_code = :kode_item";
        $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();

        if(empty($product_detail)){
            $sql = "SELECT pt.default_code, pc.complete_name 
                FROM varcos.product_template pt
                JOIN varcos.product_category pc on pt.categ_id = pc.id
                WHERE default_code = :kode_item";
            $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();
        }

        if (!empty($product_detail)){
            if (strpos(strtolower($product_detail['complete_name']),'packaging')!==false){
                $category = 'packaging';
            }else{
                $category = 'fg';
            }            
        }else{
            $category = 'null';
        }
                
        if ($model->load(Yii::$app->request->post()) ) 
        {

            $model_fg->load(Yii::$app->request->post());
            $model_fg->save();

            $model_powder->load(Yii::$app->request->post());
            $model_powder->save();
            date_default_timezone_set('Asia/Jakarta');
            $model->jam_selesai = date('Y-m-d H:i:s');
            $model->save();
            
            return $this->redirect(['view-powder', 'id' => $model->id]);
        } else {
            
                return $this->render('update_powder', [
                    'model' => $model,
                    'model_fg' => $model_fg, 
                    'model_powder'=> $model_powder,
                    'item_category'=> $category,
                ]);
        }
    }
    
    /**
     * Finds the PengecekanUmum model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PengecekanUmum the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionScanSnfgInspeksi()
    {

        $model = new PengecekanUmum();

        $searchModel = new PengecekanUmumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere("jam_mulai is not null")->orderby(['id'=>SORT_DESC])->all();

        $this->layout = '//main-sidebar-collapse'; 

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['create', 'snfg' => $model->snfg]);
        } else {
            return $this->render('scan-snfg-inspeksi', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionCreateQr()
    {

            $model = new PengecekanUmum();

            $this->layout = '//main-sidebar-collapse';

            return $this->render('create-qr', [
                    'model' => $model,
            ]);
    }

    /**
     * Updates an existing PengecekanUmum model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    protected function findModel($id)
    {
        if (($model = PengecekanUmum::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
