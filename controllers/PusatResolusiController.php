<?php

namespace app\controllers;

use Yii;
use app\models\PusatResolusi;
use app\models\PusatResolusiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
/**
 * PusatResolusiController implements the CRUD actions for PusatResolusi model.
 */
class PusatResolusiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PusatResolusi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PusatResolusiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    // public function actionDirect()
    // {
    //     $searchModel = new PusatResolusiSearch();
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    //     return $this->render('index', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }

    public function actionDirect()
    {
        $model = new PusatResolusi();

        $nojadwal=Yii::$app->request->post('PusatResolusi')['nojadwal'];
        // $nama_line = str_replace(' ','',$nl);
        // $snfg_komponen=Yii::$app->request->post('Kemas1')['snfg_komponen'];      
        // $state =Yii::$app->request->post('Kemas1')['state'];
        // $jenis_proses=Yii::$app->request->post('Kemas1')['jenis_kemas'];
        if ($model->load(Yii::$app->request->post())  && $model->save() ) 
        {
            echo $nojadwal;

        } else{

            echo 'GAGAL';
        }

        

        // $sql="
        // SELECT 
        //     id
        // from penimbangan
        // WHERE nomo ='".$nojadwal."'
        // order by timestamp desc
        // limit 1
        // ";

        // $id= PusatResolusi::findBySql($sql)->one();

        // $a = "&id=9110";        
        // print_r($id);
        //$this->redirect(array('penimbangan/update','id'=>$id->id));

        // echo Json::encode($id);
        // $model = $this->findModel($id);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // } else {
        //     return $this->render('update', [
        //         'model' => $model,
        //     ]);
        // }

    }

    /**
     * Displays a single PusatResolusi model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PusatResolusi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PusatResolusi();

        if ($model->load(Yii::$app->request->post())){

            $kategori=Yii::$app->request->post('PusatResolusi')['kategori'];
            $proses=Yii::$app->request->post('PusatResolusi')['proses'];
            $s_id=Yii::$app->request->post('PusatResolusi')['s_id'];
            
            $model->save();
            
            if($kategori=='SALAH_INPUT'){
                return $this->redirect([$proses.'/update', 'id' => $s_id]);
            }else if($kategori=='LUPA_INPUT'){
                return $this->redirect([$proses.'/create-resolusi']);
            }else {
                return $this->render('create', [
                    'model' => $model
                ]);
            }


        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateNew()
    {
        $model = new PusatResolusi();

        if ($model->load(Yii::$app->request->post())){

            $kategori=Yii::$app->request->post('PusatResolusi')['kategori'];
            $nojadwal=Yii::$app->request->post('PusatResolusi')['nojadwal'];
            $nama_kapten=Yii::$app->request->post('PusatResolusi')['nama_operator'];
            $proses=Yii::$app->request->post('PusatResolusi')['proses'];
            if(in_array(Yii::$app->request->post('PusatResolusi')['proses'],array("penimbangan", "pengolahan"))){
                $jadwal = 'mo';
                $param = 'nomo';
            }else if(Yii::$app->request->post('PusatResolusi')['proses']=='kemas1'){
                $jadwal = 'snfgkomponen';
                $param = 'snfg_komponen';
            }else if(Yii::$app->request->post('PusatResolusi')['proses']=='kemas2'){
                $jadwal = 'snfg';
                $param = 'snfg';
            }

            if(empty(Yii::$app->request->post('PusatResolusi')['s_id'])){
                $s_id = null;
            }else{
                $s_id = Yii::$app->request->post('PusatResolusi')['s_id'];
            }

            $username = Yii::$app->user->identity->username;
            
            // Save Pusat Resolusi Form Data
            $model->save();

            // Insert Into Pusat Resolusi Log
            date_default_timezone_set('Asia/Jakarta');
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand('
                INSERT INTO pusat_resolusi_log ( 
                        proses_id,
                        proses,
                        alter_user,
                        type,
                        alter_captain,
                        "table",
                        alter_timestamp
                )
                SELECT 
                    :proses_id as proses_id,
                    :proses as proses,
                    :alter_user as alter_user,
                    :type as type,
                    :alter_captain as alter_captain,
                    :table as "table",
                    :alter_timestamp as alter_timestamp

                RETURNING id; 
                ', 
                [   ':proses_id'=> $s_id,
                    ':proses'=>$proses,
                    ':table'=>'flow_input_'.$jadwal,
                    ':alter_user'=> $username,
                    ':alter_captain'=>$nama_kapten,
                    ':alter_timestamp'=>date('Y-m-d h:i A'),
                    ':type'=>$kategori
                ]
            );

            $result = $command->queryAll();
            
            // ID Lupa Input
            $lp_id = $result[0]['id'];

            // If succeed then check kategori
            if($result){

                if($kategori=='SALAH_INPUT'){

                    return $this->redirect(['flow-input-'.$jadwal.'/update','id'=>$s_id]);        
                
                }else if($kategori=='LUPA_INPUT'){

                    // Resolve
                    return $this->redirect(['flow-input-'.$jadwal.'/resolve-'.$proses,$param=>$nojadwal,'lp_id'=>$lp_id]);

                }else {
                    return $this->render('create', [
                        'model' => $model
                    ]);
                }

            }

        } else {
            return $this->render('create-new', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Updates an existing PusatResolusi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PusatResolusi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionCheckNomo($nojadwal)
    {
        $sql="select
                     count(*) as id
            from scm_planner where nomo='".$nojadwal."'";
        $planner= PusatResolusi::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($planner);
    }

    public function actionCheckSnfg($nojadwal)
    {
        $sql="select
                     count(*) as id
            from scm_planner where snfg='".$nojadwal."'";
        $planner= PusatResolusi::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($planner);
    }

    public function actionCheckSnfgkomponen($nojadwal)
    {
        $sql="select
                     count(*) as id
            from scm_planner where snfg_komponen='".$nojadwal."'";
        $planner= PusatResolusi::findBySql($sql)->one();
        //print_r($pprr);
        echo Json::encode($planner);
    }

    /**
     * Finds the PusatResolusi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PusatResolusi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PusatResolusi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
