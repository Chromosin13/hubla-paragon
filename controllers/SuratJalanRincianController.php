<?php

namespace app\controllers;

use Yii;
use app\models\SuratJalanRincian;
use app\models\ScmPlanner;
use app\models\FlowInputSnfg;
use app\models\SuratJalanRincianSearch;
use app\models\SuratJalan;
use app\models\QcFgV2;
use app\models\LogReceiveNdc;
use app\models\FlowPraKemas;
use app\models\SuratJalanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use yii\filters\AccessControl;
use app\components\AccessRule;
use Da\QrCode\QrCode;
use app\models\User;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;

/**
 * SuratJalanRincianController implements the CRUD actions for SuratJalanRincian model.
 */
class SuratJalanRincianController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create-rincian-qr','scan-konfirmasi-terima-ndc-qr','konfirmasi-terima-ndc'],
                'rules' => [
                    [
                        'actions' => ['create-rincian-qr','scan-karantina','update-karantina'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_SURAT_JALAN,
                            User::ROLE_KARANTINA,
                            User::ROLE_QCFG,
                        ],
                    ],
                    [
                        'actions' => ['scan-konfirmasi-terima-ndc-qr','konfirmasi-terima-ndc'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_NDC,
                            User::ROLE_KARANTINA,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all SuratJalanRincian models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SuratJalanRincianSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SuratJalanRincian model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionBelumKarantina()
    {
        return $this->render('belum-karantina', [
        ]);
    }
    public function actionAlokasiRdc()
    {
        return $this->render('alokasi-rdc', [
        ]);
    }
    public function actionNdcRdc()
    {
        return $this->render('ndc-rdc', [
        ]);
    }
    public function actionBedaAlokasi()
    {
        return $this->render('beda-alokasi', [
        ]);
    }
    public function actionChangeDownloadStatus($id)
    {
        $update = Yii::$app->db->createCommand("
            UPDATE surat_jalan SET is_downloaded = 1
            WHERE id = ".$id."")->execute();
        return $this->redirect(['qc-fg-v2/halaman-terima-ndc']);
    }
    public function actionRincianItem($id)
    {
        // $searchModel = SuratJalanRincianSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query->andWhere(['surat_jalan_id'=>$id]);

        $rincian = Yii::$app->db->createCommand("
            SELECT DISTINCT ON (id) id, * from
                (select sj.nosj nomor_sj,
                        sj.timestamp tgl_buat_sj,
                        sp.odoo_code,
                        sp.koitem_fg kode_korporat,
                        sjr.snfg,
                        sjr.nama_fg,
                        sjr.qty,
                        sjr.id,
                        fis.nobatch
                        from surat_jalan sj
                        left join surat_jalan_rincian sjr on sj.id = sjr.surat_jalan_id
                        left join scm_planner sp on sjr.snfg = sp.snfg
                        left join flow_input_snfg fis on sjr.snfg = fis.snfg
                        where sj.id = ".$id." and fis.nobatch is not null) as main")->queryAll();
        // print_r('<pre>');
        // print_r($rincian);
        // print_r('</pre>');
        $dataProvider = new ArrayDataProvider([
            'allModels' => $rincian,
            'sort' => [
                'attributes' => ['id'],
            ],
            'pagination' => [
                'pageSize' => 15,
            ],
        ]);
        // print_r($dataProvider);
        return $this->render('rincian-item',[
            'dataProvider'=>$dataProvider,
            'id'=>$id,
        ]);
    }

    public function actionSudahScan($snfg,$nourut)
    {

        // Get Last Sequence of This Day
        $sql = "

                SELECT
                    sj.nosj
                FROM
                    surat_jalan sj
                INNER JOIN surat_jalan_rincian sjr on sjr.surat_jalan_id=sj.id
                WHERE sjr.snfg='".$snfg."' and sjr.nourut=".$nourut."
               ";

        $sj_array = SuratJalan::findBySql($sql)->one();

        return $this->render('sudah-scan', [
            'nosj' => $sj_array['nosj'],
        ]);
    }

    /**
     * Creates a new SuratJalanRincian model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SuratJalanRincian();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionViewRincian($id)
    {
        return $this->redirect(['create-rincian','id'=>$id]);
        // date_default_timezone_set('Asia/Jakarta');
        //
        // $model = new SuratJalanRincian();
        //
        // $searchModel = new SuratJalanRincianSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query->where("surat_jalan_id=".$id."");
        //
        // //Editable Column check, if there is a post data
        // if(Yii::$app->request->post('hasEditable'))
        // {
        //     $operatorId = Yii::$app->request->post('editableKey');
        //     $operator = SuratJalanRincian::findOne($operatorId);
        //
        //     $out = Json::encode(['output'=>'','message'=>'']);
        //     $post = [];
        //     $posted = current($_POST['SuratJalanRincian']);
        //     $post['SuratJalanRincian'] = $posted;
        //     if($operator->load($post))
        //     {
        //         $operator->save();
        //
        //     }
        //     echo $out;
        //     return;
        // }
        //
        // // Get Related Information
        //
        // if ($model->load(Yii::$app->request->post())){
        //
        //     $model->save();
        //
        //     $connection2 = Yii::$app->getDb();
        //     $command2 = $connection2->createCommand("
        //
        //             UPDATE surat_jalan_rincian sjr
        //             SET
        //                 qty=q.qty_karantina,
        //                 nama_fg =q.nama_fg
        //             FROM (select a.*,s.nama_fg from qc_fg_v2 a
        //                   left join (select distinct snfg,nama_fg from scm_planner) s on a.snfg=s.snfg
        //                  )  q
        //             WHERE sjr.snfg = q.snfg  and sjr.nourut = q.nourut and sjr.id = ".$model->id.";
        //             ");
        //
        //             $result2 = $command2->queryAll();
        //
        //     return $this->redirect(['create-rincian', 'id' => $id]);
        //
        // } else {
        //
        //     return $this->render('create-rincian', [
        //         'model' => $model,
        //         'id' => $id,
        //         'searchModel' => $searchModel,
        //         'dataProvider' => $dataProvider,
        //     ]);
        // }
    }


    public function actionCreateRincian($id)
    {

        date_default_timezone_set('Asia/Jakarta');

        $model = new SuratJalanRincian();

        $searchModel = new SuratJalanRincianSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("surat_jalan_id=".$id."");

        //Editable Column check, if there is a post data
        if(Yii::$app->request->post('hasEditable'))
        {
            $operatorId = Yii::$app->request->post('editableKey');
            $operator = SuratJalanRincian::findOne($operatorId);

            $out = Json::encode(['output'=>'','message'=>'']);
            $post = [];
            $posted = current($_POST['SuratJalanRincian']);
            $post['SuratJalanRincian'] = $posted;
            if($operator->load($post))
            {
                $operator->save();

            }
            echo $out;
            return;
        }

        // Get Related Information

        if ($model->load(Yii::$app->request->post())){
            $surjal_rincian = SuratJalanRincian::find()->where(['surat_jalan_id'=>$id])->one();
            $data_palet = QcFgV2::find()->select(['alokasi'])->where(['and',['snfg'=>$model->snfg],['nourut'=>$model->nourut]])->one();

            if (!empty($surjal_rincian)){
                $alokasi_surjal = SuratJalan::find()->where(['id'=>$id])->one()['alokasi'];
                if (empty($alokasi_surjal)){
                    $alokasi_surjal = $data_palet->alokasi;
                }
            }else{
                $update_alokasi_surjal = Yii::$app->db
                                            ->createCommand("
                                                UPDATE surat_jalan 
                                                SET alokasi = :alokasi 
                                                WHERE id = :id;",
                                                [
                                                    ':alokasi' => $data_palet->alokasi,
                                                    ':id' => $id,
                                                ]
                                            )->execute();
                $alokasi_surjal = $data_palet->alokasi;
            }
            
            $sql = "
            SELECT
                id
            FROM
                surat_jalan_rincian
            WHERE snfg='".$model->snfg."'
            and nourut=".$model->nourut." LIMIT 1
            ";

            $exist = SuratJalanRincian::findBySql($sql)->one();

            $sql = "
            SELECT snfg,nourut,id_palet,jumlah_koli,nobatch,qty_karantina,alokasi,timestamp_terima_karantina
            FROM
                qc_fg_v2
            WHERE snfg='".$model->snfg."'
            and nourut=".$model->nourut." LIMIT 1
           ";

            $qcfg = QcFgV2::findBySql($sql)->one();

            $fis = FlowInputSnfg::findBySql("
                        SELECT fg_name_odoo
                        FROM flow_input_snfg
                        WHERE snfg = :snfg 
                        LIMIT 1
                        ",
                        [
                            ':snfg'=>$qcfg->snfg
                        ])->one();

                if(empty($exist->id)){

                  if ( $data_palet->alokasi == $alokasi_surjal || ($data_palet->alokasi != $alokasi_surjal && strpos($data_palet->alokasi,'NDC') !== false) ){

                    if(!empty($qcfg->timestamp_terima_karantina)){
                        $model->snfg = $qcfg->snfg;
                        $model->nourut = $qcfg->nourut;
                        $model->id_palet = $qcfg->id_palet;
                        $model->jumlah_koli = $qcfg->jumlah_koli;
                        $model->nobatch = $qcfg->nobatch;
                        $model->qty = $qcfg->qty_karantina;
                        $model->alokasi = $qcfg->alokasi;
                        $model->nama_fg = $fis->fg_name_odoo;
                        $model->save();
                        
                        // $connection2 = Yii::$app->getDb();
                        // $command2 = $connection2->createCommand("

                        //       UPDATE surat_jalan_rincian sjr
                        //       SET
                        //           alokasi=q.alokasi,
                        //           qty=q.qty_karantina,
                        //           nama_fg =q.nama_fg,
                        //           jumlah_koli =q.jumlah_koli ,
                        //           nobatch =q.nobatch
                        //       FROM (select a.*, f.nama_fg from qc_fg_v2 a
                        //             left join (select distinct snfg,fg_name_odoo as nama_fg from flow_input_snfg) f on a.snfg=f.snfg
                        //            )  q
                        //       WHERE sjr.snfg = q.snfg  and sjr.nourut = q.nourut and sjr.id = ".$model->id.";
                        //       ");

                        // $result2 = $command2->queryAll();

                        return $this->redirect(['create-rincian', 'id' => $id]);

                    }else{
                        return $this->redirect(['belum-karantina']);
                    }

                  }else{
                      return $this->redirect(['beda-alokasi']);
                  }

                }else{
                    return $this->redirect(['sudah-scan','snfg'=>$model->snfg,'nourut'=>$model->nourut]);
                }

        } else {

            return $this->render('create-rincian', [
                'model' => $model,
                'id' => $id,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    // public function actionConfirmSuratJalan($id)
    // {
    //   // This Action is triggered on the Index Page of SuratJalanRincian, when the operator finished adding all the pallets
    //   // To Allocate NOSJ Document Number , ex : 2018-01-01/2
    //   //                    -- coalesce(count(*),0)+1 as id
    //   // Get Last Sequence of This Day
    //   $sql = "
    //             SELECT
    //                 coalesce(max(split_part(nosj,'/',2)::integer),0)+1 as id

    //             FROM
    //                 surat_jalan
    //             WHERE timestamp::date='".date('Y-m-d')."' and nosj!='DRAFT'
    //             LIMIT 1
    //            ";

    //         $seq_sj = SuratJalan::findBySql($sql)->one();

    //  //  Update Null Surat Jalan Document Number refer to the Surat Jalan ID
    //         $connection = Yii::$app->getDb();
    //         $command = $connection->createCommand("

    //           UPDATE surat_jalan
    //           SET
    //               nosj=concat('".date('Y-m-d')."','/',id)
    //           WHERE id = ".$id.";
    //           ");

    //           $result = $command->queryAll();
    //   if($result){
    //     return $this->redirect(Yii::$app->request->referrer);
    //   }

    // }

    public function actionConfirmSuratJalan($id)
    {
        

        //  Update Null Surat Jalan Document Number refer to the Surat Jalan ID
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

          UPDATE surat_jalan
          SET
              nosj=concat('".date('Y-m-d')."','/',id)
          WHERE id = ".$id.";
          ");

        $result = $command->execute();
        $nosj = SuratJalan::find()->where(['id'=>$id])->one()['nosj'];

        $cek_log_receive = LogReceiveNdc::find()->where(['nosj'=>$nosj])->all();
        $list_log_receive = [];
        foreach ($cek_log_receive as $data){
            $list_log_receive[] = $data['id_palet'];
        }


        if($result){
            $list_palet = SuratJalanRincian::find()->where(['surat_jalan_id'=>$id])->orderBy(['id'=>SORT_ASC])->all();
            foreach ($list_palet as $palet){

                $kode_item = strtok($palet->snfg,'/');
                //Cek apakah item tersebut FG atau Packaging
                $sql = "SELECT pt.default_code, pc.complete_name 
                        FROM product_template pt
                        JOIN product_category pc on pt.categ_id = pc.id
                        WHERE default_code = :kode_item";
                $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();

                if(empty($product_detail)){
                    $sql = "SELECT pt.default_code, pc.complete_name 
                        FROM varcos.product_template pt
                        JOIN varcos.product_category pc on pt.categ_id = pc.id
                        WHERE default_code = :kode_item";
                    $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();
                }

                if (!empty($product_detail)){
                    if (strpos(strtolower($product_detail['complete_name']),'packaging')!==false){
                        $category = 'packaging';
                        continue;
                    }else{
                        $category = 'fg';
                    }            
                }else{
                    $category = 'null';
                }

                // Insert into Surat Jalan Rincian
                // Rama Please confirm kenapa adding item via QR / Input Manual ID Palet tidak menginsert ke Surat Jalan Rincian? tetapi hanya update status di qc_fg_v2 ?
                // if (!in_array($palet['id_palet'],$list_log_receive)){
                //     $connection = Yii::$app->getDb();
                //     $command = $connection->createCommand("
                //         INSERT INTO surat_jalan_rincian (surat_jalan_id,snfg,nourut,qty,nama_fg,jumlah_koli,nobatch,alokasi)
                //         SELECT 
                //             :surat_jalan_id as surat_jalan_id,
                //             :snfg as snfg,
                //             :nourut as nourut,
                //             :qty as qty,
                //             :nama_fg as nama_fg,
                //             :jumlah_koli as jumlah_koli,
                //             :nobatch as nobatch,
                //             :alokasi as alokasi
                //         ",
                //         [
                //          ':surat_jalan_id' => $id,
                //          ':snfg' => $palet->snfg,
                //          ':nourut' => $palet->nourut,
                //          ':qty'=>$palet->qty_karantina,
                //          ':nama_fg' => $palet->nama_fg,
                //          ':jumlah_koli' => $palet->jumlah_koli,
                //          ':nobatch' => $palet->nobatch,
                //          ':alokasi' => $palet->alokasi
                //         ]
                //     );

                //     $insert = $command->execute();                        
                // }
                

                //Insert into log receive ndc

                if (!in_array($palet['id_palet'],$list_log_receive)){
                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand("
                        INSERT INTO log_receive_ndc (id_palet,nosj,nama_fg,snfg,nourut,nobatch,qty,odoo_code,expired_date)
                        SELECT  :id_palet as id_palet, 
                                :nosj as nosj, 
                                :nama_fg as nama_fg, 
                                s.snfg as snfg, 
                                :nourut as nourut, 
                                :nobatch as nobatch, 
                                :qty as qty,
                                s.odoo_code as odoo_code,
                                q.exp_date as expired_date
                        
                        FROM (
                            SELECT DISTINCT on (snfg) odoo_code,snfg
                            FROM scm_planner 
                            ORDER BY snfg,id DESC
                        ) s 
                        LEFT JOIN (
                            SELECT exp_date,id_palet,snfg
                            FROM qc_fg_v2 
                        ) q on s.snfg = q.snfg and q.id_palet=:id_palet
                        WHERE s.snfg=:snfg
                        ",
                        [
                         ':id_palet' => $palet->id_palet,
                         ':nosj' => $nosj,
                         ':nama_fg' => $palet->nama_fg,
                         ':snfg' => $palet->snfg,
                         ':nourut' => $palet->nourut,
                         ':nobatch' => $palet->nobatch,
                         ':qty'=>$palet->qty
                        ]
                    );

                    $insert = $command->execute();                        
                }

                
            }

            return $this->redirect(Yii::$app->request->referrer);
        }else{
            Yii::$app->session->setFlash('danger','Gagal konfirmasi surat jalan');
            return $this->redirect(Yii::$app->request->referrer);
        }

    }


    public function actionCreateRincianQr($id)
    {
        $this->layout = '//main-qr-layout';

        date_default_timezone_set('Asia/Jakarta');

        $model = new SuratJalanRincian();

        $searchModel = new SuratJalanRincianSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("surat_jalan_id=".$id."");


        //Editable Column check, if there is a post data
        if(Yii::$app->request->post('hasEditable'))
        {
            $operatorId = Yii::$app->request->post('editableKey');
            $operator = SuratJalanRincian::findOne($operatorId);

            $out = Json::encode(['output'=>'','message'=>'']);
            $post = [];
            $posted = current($_POST['SuratJalanRincian']);
            $post['SuratJalanRincian'] = $posted;
            if($operator->load($post))
            {
                $operator->save();

            }
            echo $out;
            return;
        }
        // Get Related Information

        if ($model->load(Yii::$app->request->post())){
            $surjal_rincian = SuratJalanRincian::find()->where(['surat_jalan_id'=>$id])->one();

            if (!empty($model->id_palet)){
                $data_palet = QcFgV2::find()->select(['alokasi'])->where(['id_palet'=>$model->id_palet])->one();
            }else{
                $data_palet = QcFgV2::find()->select(['alokasi'])->where(['and',['snfg'=>$model->snfg,'nourut'=>$model->nourut]])->one();
            }

            //If surat jalan already has list pallet, get alokasi
            if (!empty($surjal_rincian)){
                $alokasi_surjal = SuratJalan::find()->where(['id'=>$id])->one()['alokasi'];
                if (empty($alokasi_surjal)){
                    $alokasi_surjal = $data_palet->alokasi;
                }
            //If surat jalan doesn't have list pallet, update alokasi based on alokasi in first pallet
            }else{
                $update_alokasi_surjal = Yii::$app->db
                                            ->createCommand("
                                                UPDATE surat_jalan 
                                                SET alokasi = :alokasi 
                                                WHERE id = :id;",
                                                [
                                                    ':alokasi' => $data_palet->alokasi,
                                                    ':id' => $id,
                                                ]
                                            )->execute();
                $alokasi_surjal = $data_palet->alokasi;
            }

            $exist = SuratJalanRincian::findBySql("
                        SELECT id
                        FROM surat_jalan_rincian
                        WHERE id_palet= :id_palet or (snfg = :snfg and nourut = :nourut) 
                        LIMIT 1
                        ",
                        [
                            ':id_palet'=>$model->id_palet,
                            ':snfg'=>$model->snfg,
                            ':nourut'=>$model->nourut
                        ])->one();


            $qcfg = QcFgV2::findBySql("
                        SELECT snfg,nourut,id_palet,jumlah_koli,nobatch,qty_karantina,alokasi,timestamp_terima_karantina
                        FROM qc_fg_v2
                        WHERE id_palet = :id_palet or (snfg=:snfg and nourut= :nourut) LIMIT 1",
                        [
                            ':id_palet'=>$model->id_palet,
                            ':snfg'=>$model->snfg,
                            'nourut'=>$model->nourut
                        ])->one();
            
            $kode_item = strtok($qcfg->snfg,'/');
            //Cek apakah item tersebut FG atau Packaging
            $sql = "SELECT pt.default_code, pc.complete_name 
                    FROM product_template pt
                    JOIN product_category pc on pt.categ_id = pc.id
                    WHERE default_code = :kode_item";
            $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();

            if(empty($product_detail)){
                $sql = "SELECT pt.default_code, pc.complete_name 
                    FROM varcos.product_template pt
                    JOIN varcos.product_category pc on pt.categ_id = pc.id
                    WHERE default_code = :kode_item";
                $product_detail = Yii::$app->db->createCommand($sql,[':kode_item'=>$kode_item])->queryOne();
            }

            if (!empty($product_detail)){
                if (strpos(strtolower($product_detail['complete_name']),'packaging')!==false){
                    $category = 'packaging';
                }else{
                    $category = 'fg';
                }            
            }else{
                $category = 'null';
            }

            if ($category=='packaging'){
                $fis = FlowPraKemas::findBySql("
                            SELECT fg_name_odoo
                            FROM flow_pra_kemas
                            WHERE snfg = :snfg 
                            LIMIT 1
                            ",
                            [
                                ':snfg'=>$qcfg->snfg
                            ])->one();

            }else{
                $fis = FlowInputSnfg::findBySql("
                            SELECT fg_name_odoo
                            FROM flow_input_snfg
                            WHERE snfg = :snfg 
                            LIMIT 1
                            ",
                            [
                                ':snfg'=>$qcfg->snfg
                            ])->one();

            }

            if(empty($exist->id)){

              if ( $data_palet->alokasi == $alokasi_surjal || ($data_palet->alokasi != $alokasi_surjal && strpos($data_palet->alokasi,'NDC') !== false)  ){

                if(!empty($qcfg->timestamp_terima_karantina)){
                    $model->snfg = $qcfg->snfg;
                    $model->nourut = $qcfg->nourut;
                    $model->id_palet = $qcfg->id_palet;
                    $model->jumlah_koli = $qcfg->jumlah_koli;
                    $model->nobatch = $qcfg->nobatch;
                    $model->qty = $qcfg->qty_karantina;
                    $model->alokasi = $qcfg->alokasi;
                    $model->nama_fg = $fis->fg_name_odoo;
                    $model->save();

                    // $connection2 = Yii::$app->getDb();
                    // $command2 = $connection2->createCommand("

                    //       UPDATE surat_jalan_rincian sjr
                    //       SET
                    //           alokasi =q.alokasi,
                    //           qty=q.qty_karantina,
                    //           nama_fg =q.fg_name_odoo,
                    //           jumlah_koli =q.jumlah_koli ,
                    //           nobatch =q.nobatch
                    //       FROM (select a.*, f.fg_name_odoo from qc_fg_v2 a
                    //             left join (select distinct snfg,fg_name_odoo from flow_input_snfg) f on a.snfg=f.snfg
                    //            )  q
                    //       WHERE sjr.snfg = q.snfg  and sjr.nourut = q.nourut and sjr.id = ".$model->id.";
                    //       ");

                    // $result2 = $command2->queryAll();


                    // $item_rdc = QcFgV2::find()->where("snfg = '".$model->snfg."' and alokasi != 'NDC' and alokasi is not null ")->all();
                    // if (!empty($item_rdc)){
                    //     foreach ($item_rdc as $rdc){
                    //         $check_rincian = SuratJalanRincian::find()->where("snfg = '".$rdc['snfg']."' and nourut = ".$rdc['nourut']." ")->one();
                    //         if(empty($check_rincian)){
                    //           $qf = QcFgV2::find()->where(['id'=>$rdc['id']])->one();
                    //           $nama_fg = ScmPlanner::find()->where(['snfg'=>$rdc['snfg']])->one()['nama_fg'];
                    //           $nobatch = FlowInputSnfg::find()->where("snfg = '".$rdc['snfg']."' and nobatch is not null")->one()['nobatch'];

                    //           $insert = Yii::$app->db->createCommand()->insert('surat_jalan_rincian', [
                    //             'surat_jalan_id' => $id,
                    //             'alokasi' => $qc['alokasi'],
                    //             'snfg' => $qc['snfg'],
                    //             'nourut' => $qc['nourut'],
                    //             'qty' => $qc['qty_karantina'],
                    //             'jumlah_koli' => $qc['jumlah_koli'],
                    //             'nama_fg' => $nama_fg,
                    //             'nobatch' => $nobatch,
                    //           ])->execute();

                    //         }
                    //     }
                    // }

                    return $this->redirect(['create-rincian-qr', 'id' => $id]);

                }else{
                    // Yii::warning("Division by zero.");
                    return $this->redirect(['belum-karantina']);
                }

              }else{
                  return $this->redirect(['beda-alokasi']);
              }

            }else{
                return $this->redirect(['sudah-scan','snfg'=>$qcfg->snfg,'nourut'=>$qcfg->nourut]);
            }

          } else {

              return $this->render('create-rincian-qr', [
                  'model' => $model,
                  'id' => $id,
                  'searchModel' => $searchModel,
                  'dataProvider' => $dataProvider,
              ]);
          }
    }


    public function actionScanKonfirmasiTerimaNdcQr()
    {

        $this->layout = '//main-qr-layout';

        return $this->render('scan-konfirmasi-terima-ndc-qr', [
        ]);
    }

    public function actionScanBarcodeTerimaNdc()
    {

        return $this->render('scan-barcode-terima-ndc', [
        ]);
    }


    public function actionKonfirmasiTerimaNdcQr($surat_jalan)
    {

        $nosj = $surat_jalan;

        $sql = "
            SELECT
                id,
                is_received
            FROM
                surat_jalan
            WHERE nosj='".$surat_jalan."'
            LIMIT 1
           ";

        $sj = SuratJalan::findBySql($sql)->one();

        if(empty($sj->id)){
            throw new \yii\web\HttpException(404,'Tidak Ditemukan');
        }

        else{

            $this->layout = '//main-qr-layout';

            date_default_timezone_set('Asia/Jakarta');

            $model = new SuratJalanRincian();

            $searchModel = new SuratJalanRincianSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where("surat_jalan_id=".$sj->id."");


            // Get Related Information

            if ($model->load(Yii::$app->request->post())){

                // return $this->redirect(['create-rincian-qr', 'id' => $id]);
                return $this->redirect(['qc-fg-v2/terima-surat-jalan-ndc', 'surat_jalan' => $surat_jalan]);

            } else {

                return $this->render('terima-ndc-qr', [
                    'model' => $model,
                    'nosj' => $nosj,
                    'is_received' => $sj->is_received,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]);
            }


        }

    }



    public function actionPdf($id) {
        // Your SQL query here
        //$model = new ApTable();
        date_default_timezone_set('Asia/Jakarta');

        $model = new SuratJalanRincian();

           $sql = "SELECT sj.nosj as nosj,sj.timestamp::date as timestamp
                   FROM surat_jalan sj
                   WHERE sj.id=".$id."
                   LIMIT 1
                   ";
           $suratjalan = SuratJalan::findBySql($sql)->one();
           $nosj = $suratjalan->nosj;
           $timestamp = $suratjalan->timestamp;

        $searchModel = new SuratJalanRincianSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProviderRdc = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("surat_jalan_id=".$id." ")->orderBy(['id'=>SORT_ASC]);
        // $dataProviderNdc->query->where("surat_jalan_id=".$id." and (alokasi = 'NDC' or alokasi is null)");
        // $dataProviderRdc->query->where("surat_jalan_id=".$id." and alokasi != 'NDC' and alokasi is not null");
        // $dataProvider->pagination=false;
        // $rincian = Yii::$app->db->createCommand("
        //     SELECT DISTINCT ON (id) * FROM (SELECT sjr.*,fis.nobatch FROM surat_jalan_rincian sjr
        //     LEFT JOIN (select nobatch,snfg from flow_input_snfg where nobatch is not null) fis ON sjr.snfg=fis.snfg
        //     WHERE sjr.surat_jalan_id = ".$id." )main
        //     ")->queryAll();
        //
        // $dataProvider = new ArrayDataProvider([
        //     'allModels' => $rincian,
        //     'sort' => [
        //         'attributes' => ['id'],
        //     ],
        //     'pagination' => false,
        // ]);


        $content =  $this->renderPartial('report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'nosj' => $nosj,
            'timestamp' => $timestamp,
            'id' => $id,
        ]);

        // $content = $this->renderPartial('report', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        //     'nalang' => $nalang,
        //     'tglrencanabayar' => $tglrencanabayar,
        //     'norek' => $norek,
        //     'nama_rekening' => $nama_rekening,
        //     'bank' => $bank,
        //     'bayar_via' => $bayar_via,
        // ]);

        // $content = $this->renderPartial('index', ['model' => $model]);

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_CORE,
        // A4 paper format
        'format' => Pdf::FORMAT_A4,
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT,
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER,
        // your html content input
        'content' => $content,
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting
        'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
        'cssInline' => '.kv-heading-1{font-size:18px}',
        // set mPDF properties on the fly
        'options' => ['title' => 'Surat Jalan - Flowreport - '.$nosj],
        // call mPDF methods on the fly
        'methods' => [
        'SetHeader'=>['Flowreport - PT Paragon Technology & Innovation'],
        'SetFooter'=>['<div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>QC Karantina</th>
              <th>Driver NDC</th>
              <th>Security</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td><br></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>(.......................)</td>
              <td>(.......................)</td>
              <td>(.......................)</td>
            </tr>
            </tbody>
          </table>
        </div> {PAGENO}'],
        ]
        ]);

        /*------------------------------------*/
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        /*------------------------------------*/

        // return the pdf output as per the destination setting
        return $pdf->render();

        // return $content->render();
    }

    /**
     * Updates an existing SuratJalanRincian model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionPdfButton($id)
    {

        return $this->redirect(['pdf', 'id' => $id]);
    }

    /**
     * Deletes an existing SuratJalanRincian model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the SuratJalanRincian model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SuratJalanRincian the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SuratJalanRincian::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
