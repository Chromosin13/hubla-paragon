<?php

namespace app\controllers;

use Yii;
use app\models\KonfigurasiStdLeadtime;
use app\models\KonfigurasiStdLeadtimeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;


/**
 * KonfigurasiStdLeadtimeController implements the CRUD actions for KonfigurasiStdLeadtime model.
 */
class KonfigurasiStdLeadtimeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KonfigurasiStdLeadtime models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KonfigurasiStdLeadtimeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KonfigurasiStdLeadtime model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new KonfigurasiStdLeadtime model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KonfigurasiStdLeadtime();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCheck()
    {
        $model = new KonfigurasiStdLeadtime();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('check', [
                'model' => $model,
            ]);
        }
    }


    public function actionCreateTrigger()
    {
        $model = new KonfigurasiStdLeadtime();
        date_default_timezone_set('Asia/Jakarta');
            $model->last_update = date('Y-m-d h:i:s A');
        // if ($model->load(Yii::$app->request->post(


        //     )) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // } 



        if ($model->load(Yii::$app->request->post())) {

           $koitem=Yii::$app->request->post('KonfigurasiStdLeadtime')['koitem'];
           $koitem_bulk=Yii::$app->request->post('KonfigurasiStdLeadtime')['koitem_bulk'];
           $naitem=Yii::$app->request->post('KonfigurasiStdLeadtime')['naitem'];

            $connection = Yii::$app->getDb();
            date_default_timezone_set('Asia/Jakarta');
            $command = $connection->createCommand("
                INSERT INTO konfigurasi_std_leadtime(koitem,koitem_bulk,naitem,std_type,last_update,jenis,value)
                select 
                    a.koitem,
                    a.koitem_bulk,
                    a.naitem,
                    lp.std_type,
                    :last_update as last_update,
                    lp.jenis,
                    null as value
                from list_proses lp 
                inner join (select 
                                1 as test,
                                :koitem::text as koitem,
                                :koitem_bulk::text as koitem_bulk,
                                :naitem::text as naitem
                            ) a
                on a.test = 1;", 
                [':last_update' => date('Y-m-d h:i:s A'),':koitem'=> $koitem,':koitem_bulk'=> $koitem_bulk,':naitem'=> $naitem]);
            $result = $command->queryAll();
            if($result){
                return $this->redirect(['fill-p', 'koitem' => $koitem]);
            }else{
                echo "FAIL TO INSERT TO DATABASE, PLEASE GO BACK";
            }
            
           // if ($model->save()) {             
           //   return $this->redirect(['view', 'id' => $model->id]);             
           // }  else {
           //   var_dump ($model->getErrors()); die();
           // }
        }

        else {
            return $this->render('create_trigger', [
                'model' => $model,
            ]);
        }
    }



    /**
     * Updates an existing KonfigurasiStdLeadtime model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionCheckKoitem($koitem)
    {
        $sql="  
                select count(koitem) as id,koitem
                from konfigurasi_std_leadtime 
                                where koitem='".$koitem."'
                group by koitem
             ";
        $ch = KonfigurasiStdLeadtime::findBySql($sql)->one();
        echo Json::encode($ch);
    }

    public function actionFillP($koitem)
    {
        $searchModel = new KonfigurasiStdLeadtimeSearch();
        $searchModel->koitem=$koitem;
        $searchModel->std_type='P';
        $type = 'P';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query->where('work_order_id ='.$id.'');

        // $sql="SELECT work_order as work_order  FROM work_order WHERE id=".$id.";";
        // $work_order_number = WorkOrder::findBySql($sql)->one();

        if(Yii::$app->request->post('hasEditable'))
        {
            $operatorId = Yii::$app->request->post('editableKey');
            $operator = KonfigurasiStdLeadtime::findOne($operatorId);
            
            $out = Json::encode(['output'=>'','message'=>'']);
            $post = [];
            $posted = current($_POST['KonfigurasiStdLeadtime']);
            $post['KonfigurasiStdLeadtime'] = $posted;
            if($operator->load($post))
            {
                $operator->save();

            }
            echo $out;
            return;
        }

        return $this->render('fill', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'koitem' => $koitem,
            'type' => $type,
            // 'id' => $id,
            // 'work_order_number' => $work_order_number,
        ]);
    }

    public function actionFillB($koitem)
    {
        $searchModel = new KonfigurasiStdLeadtimeSearch();
        $searchModel->koitem=$koitem;
        $searchModel->std_type='B';
        $type = 'B';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query->where('work_order_id ='.$id.'');

        // $sql="SELECT work_order as work_order  FROM work_order WHERE id=".$id.";";
        // $work_order_number = WorkOrder::findBySql($sql)->one();

        if(Yii::$app->request->post('hasEditable'))
        {
            $operatorId = Yii::$app->request->post('editableKey');
            $operator = KonfigurasiStdLeadtime::findOne($operatorId);
            
            $out = Json::encode(['output'=>'','message'=>'']);
            $post = [];
            $posted = current($_POST['KonfigurasiStdLeadtime']);
            $post['KonfigurasiStdLeadtime'] = $posted;
            if($operator->load($post))
            {
                $operator->save();

            }
            echo $out;
            return;
        }

        return $this->render('fill', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'koitem' => $koitem,
            'type' => $type,
            // 'id' => $id,
            // 'work_order_number' => $work_order_number,
        ]);
    }


    /**
     * Deletes an existing KonfigurasiStdLeadtime model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KonfigurasiStdLeadtime model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KonfigurasiStdLeadtime the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KonfigurasiStdLeadtime::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
