<?php

namespace app\controllers;

use Yii;
use app\models\PosisiProsesScm;
use app\models\PosisiProsesScmSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PosisiProsesScmController implements the CRUD actions for PosisiProsesScm model.
 */
class PosisiProsesScmController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PosisiProsesScm models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PosisiProsesScmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRunning()
    {
        $searchModel = new PosisiProsesScmSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize=7;
        
        $this->layout = '//main-sidebar-collapse';
        $this->layout = "@app/views/layouts/main-login";
        
        return $this->render('running', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRunningS()
    {
        $searchModel = new PosisiProsesScmSearch();
        // $searchModel->sediaan='S';
        
        $delimitdate = date('Y-m-d', strtotime("+1 days"));
        $searchModel->due<$delimitdate;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize=7;
        $dataProvider->query->where("due<='".$delimitdate."' and sediaan = 'S'");

        $this->layout = '//main-sidebar-collapse';
        $this->layout = "@app/views/layouts/main-login";
        
        return $this->render('running-s', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRunningP()
    {
        $searchModel = new PosisiProsesScmSearch();
        // $searchModel->sediaan='P';
        
        $delimitdate = date('Y-m-d', strtotime("+1 days"));
        //$searchModel->start<'2017-08-05';
        

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize=7;
        $dataProvider->query->where("due<='".$delimitdate."' and sediaan = 'P'");
        
        $this->layout = '//main-sidebar-collapse';
        $this->layout = "@app/views/layouts/main-login";
        
        return $this->render('running-p', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRunningL()
    {

        $searchModel = new PosisiProsesScmSearch();
        // $searchModel->sediaan='L';
        
        $delimitdate = date('Y-m-d', strtotime("+1 days"));
        $searchModel->due<$delimitdate;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize=7;
        $dataProvider->query->where("due<='".$delimitdate."' and sediaan = 'L'");
        
        $this->layout = '//main-sidebar-collapse';
        $this->layout = "@app/views/layouts/main-login";
        
        return $this->render('running-l', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PosisiProsesScm model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PosisiProsesScm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PosisiProsesScm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->snfg]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PosisiProsesScm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->snfg]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PosisiProsesScm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PosisiProsesScm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PosisiProsesScm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PosisiProsesScm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
