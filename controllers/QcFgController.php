<?php

namespace app\controllers;

use Yii;
use app\models\QcFg;
use app\models\PosisiProses;
use app\models\QcFgSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use app\models\User;
use yii\filters\AccessControl;
use app\models\Kemas2;
use app\models\Kendala;
use app\models\Model;
use app\models\ArrayHelper;

/**
 * QcFgController implements the CRUD actions for QcFg model.
 */
class QcFgController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create', 'update', 'delete','index','view'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_QCFG
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all QcFg models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QcFgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single QcFg model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new QcFg model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new QcFg();
        $modelsKendala = [new Kendala];

        if ($model->load(Yii::$app->request->post()) && $model->save() ) 
        {

            $modelsKendala = Model::createMultiple(Kendala::classname());
            Model::loadMultiple($modelsKendala, Yii::$app->request->post());

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsKendala) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($modelsKendala as $modelKendala) 
                        {
                            $modelKendala->qc_fg_id = $model->id;
                            if (! ($flag = $modelKendala->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['create']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        } else 

        {
            return $this->render('create', [
                'model' => $model,
                'modelsKendala' => (empty($modelsKendala)) ? [new Kendala] : $modelsKendala
            ]);
        }

        // $model = new QcFg();

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     //return $this->redirect(['view', 'id' => $model->id]);
        //     return $this->redirect(['create']);
        // } else {
        //     return $this->render('create', [
        //         'model' => $model,
        //     ]);
        // }
    }

    /**
     * Updates an existing QcFg model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        
        $model = $this->findModel($id);
        $modelsKendala = $model->kendalas;

        if ($model->load(Yii::$app->request->post()) && $model->save()) 
        {
            $oldIDs = ArrayHelper::map($modelsKendala, 'id', 'id');

            $modelsKendala = Model::createMultiple(Kendala::classname(), $modelsKendala);

            Model::loadMultiple($modelsKendala, Yii::$app->request->post());

            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsKendala, 'id', 'id')));

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsKendala) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {

                        if (! empty($deletedIDs)) {
                            Kendala::deleteAll(['id' => $deletedIDs]);
                        }

                        foreach ($modelsKendala as $modelKendala) {
                            $modelKendala->qc_fg_id = $model->id;
                            if (! ($flag = $modelKendala->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    } else {
                        $transaction->rollBack();
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }

            //return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelsKendala' => (empty($modelsKendala)) ? [new Kendala] : $modelsKendala
            ]);
        }

        // $model = $this->findModel($id);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // } else {
        //     return $this->render('update', [
        //         'model' => $model,
        //     ]);
        // }
    }

    public function actionBatchSplitAssignKomponen($snfg_komponen)
    {


        $countKemas2 = Kemas2::find()
                        ->where(['snfg_komponen' => $snfg_komponen])
                        ->count();


        $sql = "
                SELECT distinct on (lanjutan_split_batch)
                       lanjutan_split_batch,jumlah_realisasi,waktu
                FROM kemas_2
                WHERE snfg_komponen='".$snfg_komponen."' and state='STOP'
                ORDER BY lanjutan_split_batch,timestamp desc
                ";

        $Kemas2s = Kemas2::findBySql($sql)->all();

        if($countKemas2 > 0)
        {   
            echo "<option value='1'>Select BS</options>";
            foreach($Kemas2s as $Kemas2){
                echo "<option value='".$Kemas2->lanjutan_split_batch."'> BS ke-".$Kemas2->lanjutan_split_batch.
                        " , Realisasi : ".$Kemas2->jumlah_realisasi.", Waktu Stop : ".$Kemas2->waktu."</options>";
            }

        }
        else{
                echo "<option>1</option>";
        }
    }

    public function actionBatchSplitAssignSnfg($snfg)
    {
        $countKemas2 = Kemas2::find()
                        ->where(['snfg' => $snfg])
                        ->count();


        $sql = "
                SELECT distinct on (lanjutan_split_batch)
                       lanjutan_split_batch,jumlah_realisasi,waktu
                FROM kemas_2
                WHERE snfg='".$snfg."' and state='STOP'
                ORDER BY lanjutan_split_batch,timestamp desc
                ";

        $Kemas2s = Kemas2::findBySql($sql)->all();


        if($countKemas2 > 0)
        {
            echo "<option value='1'>Select BS</options>";
            foreach($Kemas2s as $Kemas2){
                echo "<option value='".$Kemas2->lanjutan_split_batch."'> BS Ke-".$Kemas2->lanjutan_split_batch.
                        " , Realisasi : ".$Kemas2->jumlah_realisasi.", Waktu Stop : ".$Kemas2->waktu."</options>";
            }

        }
        else{
                echo "<option>1</option>";
        }
    }


    public function actionCheckBatchSplitKomponen($snfg_komponen)
    {
        $sql="
                select distinct on (kemas_2.snfg_komponen) 
                    kemas_2.snfg_komponen
                    ,state
                    ,is_done
                    ,lanjutan_split_batch
                    ,case when k2.maxlsb>1 then 1
                          else -1
                     end last_status
                from kemas_2 
                left join (
                            select  snfg_komponen, 
                                    max(lanjutan_split_batch) maxlsb
                            from kemas_2
                            where snfg_komponen='".$snfg_komponen."'
                            group by snfg_komponen 
                          ) k2 on k2.snfg_komponen = kemas_2.snfg_komponen
                WHERE kemas_2.snfg_komponen ='".$snfg_komponen."'
                order by snfg_komponen,timestamp desc

                ";
        $QcFg= Kemas2::findBySql($sql)->one();
        //print_r($Kemas2);
        echo Json::encode($QcFg);
    }


    public function actionCheckBatchSplitSnfg($snfg)
    {
        $sql="
                select distinct on (kemas_2.snfg) 
                    kemas_2.snfg
                    ,state
                    ,is_done
                    ,lanjutan_split_batch
                    ,case when k2.maxlsb>1 then 1
                          else -1
                     end last_status
                from kemas_2 
                left join (
                            select  snfg, 
                                    max(lanjutan_split_batch) maxlsb
                            from kemas_2
                            where snfg='".$snfg."'
                            group by snfg 
                          ) k2 on k2.snfg = kemas_2.snfg
                WHERE kemas_2.snfg ='".$snfg."'
                order by snfg,timestamp desc
                ";
        $QcFg= Kemas2::findBySql($sql)->one();
        //print_r($Kemas2);
        echo Json::encode($QcFg);
    }

    public function actionPaletAssignKomponen($snfg_komponen,$lanjutan_split_batch)
    {
        $countKemas2 = Kemas2::find()
                        ->where(['snfg_komponen' => $snfg_komponen])
                        ->count();


        $sql = "
                SELECT distinct on (palet_ke)
                    palet_ke,
                    jumlah_realisasi,
                    waktu
                FROM kemas_2
                WHERE snfg_komponen='".$snfg_komponen."' and state='STOP'
                and lanjutan_split_batch='".$lanjutan_split_batch."'
                ";

        $Kemas2s = Kemas2::findBySql($sql)->all();

        if($countKemas2 > 0)
        {
            echo "<option value='Select Palet'>Select Palet</options>";
            foreach($Kemas2s as $Kemas2){
                echo "<option value='".$Kemas2->palet_ke."'> Palet Ke-".
                        $Kemas2->palet_ke." , Realisasi : ".$Kemas2->jumlah_realisasi." Pcs , Waktu Stop : ".$Kemas2->waktu."</options>";
            }

        }
        else{
                echo "<option>1</option>";
        }
    }

    public function actionPaletAssignSnfg($snfg,$lanjutan_split_batch)
    {
        $countKemas2 = Kemas2::find()
                        ->where(['snfg' => $snfg])
                        ->count();


        $sql = "
                SELECT distinct on (palet_ke)
                    palet_ke,
                    jumlah_realisasi,
                    waktu
                FROM kemas_2
                WHERE snfg='".$snfg."' and state='STOP'
                and lanjutan_split_batch='".$lanjutan_split_batch."'
                ORDER BY palet_ke
                ";

        $Kemas2s = Kemas2::findBySql($sql)->all();

        if($countKemas2 > 0)
        {
            echo "<option value='Select Palet'>Select Palet</options>";
            foreach($Kemas2s as $Kemas2){

                echo "<option value='".$Kemas2->palet_ke."'> Palet Ke-".
                        $Kemas2->palet_ke." , Realisasi : ".$Kemas2->jumlah_realisasi." Pcs , Waktu Stop : ".$Kemas2->waktu."</options>";
            }

        }
        else{
                echo "<option>1</option>";
        }
    }



    public function actionGetPaletMaxKomponen($snfg_komponen)
    {
        $sql="

                SELECT 
                    max(coalesce(palet_ke,0)) as palet_ke
                FROM kemas_2
                WHERE snfg_komponen='".$snfg_komponen."'
             ";
        $QcFg= QcFg::findBySql($sql)->one();
        //print_r($Kemas2);
        echo Json::encode($QcFg);
    }

    public function actionGetPaletMaxKomponenSb($snfg_komponen,$lanjutan_split_batch)
    {
        $sql="

                SELECT 
                    max(coalesce(palet_ke,0)) as palet_ke
                FROM kemas_2
                WHERE snfg_komponen='".$snfg_komponen."'
                and lanjutan_split_batch='".$lanjutan_split_batch."'
             ";
        $QcFg= QcFg::findBySql($sql)->one();
        //print_r($Kemas2);
        echo Json::encode($QcFg);
    }


    public function actionGetPaletMaxSnfg($snfg)
    {
        $sql="

                SELECT 
                    max(coalesce(palet_ke,0)) as palet_ke
                FROM kemas_2
                WHERE snfg='".$snfg."'
             ";
        $QcFg= QcFg::findBySql($sql)->one();
        //print_r($Kemas2);
        echo Json::encode($QcFg);
    }

    public function actionGetPaletMaxSnfgSb($snfg,$lanjutan_split_batch)
    {
        $sql="

                SELECT 
                    max(coalesce(palet_ke,0)) as palet_ke
                FROM kemas_2
                WHERE snfg='".$snfg."'
                and lanjutan_split_batch='".$lanjutan_split_batch."'
             ";
        $QcFg= QcFg::findBySql($sql)->one();
        //print_r($Kemas2);
        echo Json::encode($QcFg);
    }



    public function actionLanjutanQcFg($snfg_komponen,$jenis_periksa)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM 
            (
                select sp.snfg, qc_fg.snfg_komponen, qc_fg.jenis_periksa, qc_fg.state, qc_fg.lanjutan
                from qc_fg 
                inner join scm_planner sp on sp.snfg_komponen = qc_fg.snfg_komponen
                where state='STOP' and qc_fg.snfg is null and qc_fg.snfg_komponen is not null 
                and qc_fg.snfg_komponen='".$snfg_komponen."'

                union all
                
                select p.snfg,sp.snfg_komponen, p.jenis_periksa, p.state, p.lanjutan 
                from qc_fg p
                inner join scm_planner sp on sp.snfg=p.snfg
                where p.state='STOP' and p.snfg is not null and sp.snfg_komponen='".$snfg_komponen."'
            ) a
        WHERE a.snfg_komponen ='".$snfg_komponen."'
        and a.jenis_periksa = '".$jenis_periksa."'
        ";

        $QcFg= QcFg::findBySql($sql)->one();
        echo Json::encode($QcFg);

    }

    public function actionLanjutanQcFgsnfg($snfg,$jenis_periksa)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM 
            (
                select qc_fg.snfg, sp.snfg_komponen, qc_fg.jenis_periksa, qc_fg.state, qc_fg.lanjutan
                from qc_fg 
                inner join scm_planner sp on sp.snfg = qc_fg.snfg
                where state='STOP' and qc_fg.snfg is not null and qc_fg.snfg_komponen is null 
                and qc_fg.snfg='".$snfg."'

                union all
                
                select sp.snfg,p.snfg_komponen, p.jenis_periksa, p.state, p.lanjutan 
                from qc_fg p
                inner join scm_planner sp on sp.snfg_komponen=p.snfg_komponen
                where p.state='STOP' and p.snfg is null and sp.snfg='".$snfg."'
            ) a
        WHERE a.snfg ='".$snfg."'
        and a.jenis_periksa = '".$jenis_periksa."'
        ";

        $QcFg= QcFg::findBySql($sql)->one();
        echo Json::encode($QcFg);

    }



    // public function actionLanjutanQcFg($snfg,$jenis_periksa)
    // {
    //     $sql="select coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan from qc_fg where snfg='".$snfg."' and jenis_periksa='".$jenis_periksa."' and state='STOP'";
    //     $Qcfg= QcFg::findBySql($sql)->one();
    //     echo Json::encode($Qcfg);

    // }


    public function actionLanjutanIstQcFg($snfg_komponen,$jenis_periksa,$lanjutan)
    {
        $sql="
                SELECT coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist
                FROM 
                    (
                        select sp.snfg, qc_fg.snfg_komponen, qc_fg.jenis_periksa, qc_fg.state, qc_fg.lanjutan,qc_fg.lanjutan_ist
                        from qc_fg 
                        inner join scm_planner sp on sp.snfg_komponen = qc_fg.snfg_komponen
                        where state='ISTIRAHAT STOP' and qc_fg.snfg is null and qc_fg.snfg_komponen is not null and qc_fg.snfg_komponen='".$snfg_komponen."'

                        union all
                        
                        select p.snfg,sp.snfg_komponen, p.jenis_periksa, p.state, p.lanjutan, p.lanjutan_ist 
                        from qc_fg p
                        inner join scm_planner sp on sp.snfg=p.snfg
                        where p.state='ISTIRAHAT STOP' and p.snfg is not null and sp.snfg_komponen='".$snfg_komponen."'
                    ) a
                WHERE a.snfg_komponen ='".$snfg_komponen."'
                and a.jenis_periksa = '".$jenis_periksa."'
                and a.lanjutan = '".$lanjutan."'
                ";
        $QcFg= QcFg::findBySql($sql)->one();
        echo Json::encode($QcFg);
    }

    public function actionLanjutanIstQcFgsnfg($snfg,$jenis_periksa,$lanjutan)
    {
        $sql="
                SELECT coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist
                FROM 
                    (
                        select sp.snfg, qc_fg.snfg_komponen, qc_fg.jenis_periksa, qc_fg.state, qc_fg.lanjutan,qc_fg.lanjutan_ist
                        from qc_fg 
                        inner join scm_planner sp on sp.snfg_komponen = qc_fg.snfg_komponen
                        where state='ISTIRAHAT STOP' and qc_fg.snfg is null and qc_fg.snfg_komponen is not null and sp.snfg='".$snfg."'

                        union all
                        
                        select p.snfg,sp.snfg_komponen, p.jenis_periksa, p.state, p.lanjutan, p.lanjutan_ist 
                        from qc_fg p
                        inner join scm_planner sp on sp.snfg=p.snfg
                        where p.state='ISTIRAHAT STOP' and p.snfg is not null and p.snfg='".$snfg."'
                    ) a
                WHERE a.snfg ='".$snfg."'
                and a.jenis_periksa = '".$jenis_periksa."'
                and a.lanjutan = '".$lanjutan."'
                ";
        $QcFg= QcFg::findBySql($sql)->one();
        echo Json::encode($QcFg);
    }
    
    // public function actionLanjutanIstQcFg($snfg,$jenis_periksa,$lanjutan)
    // {
    //     $sql="select coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist from qc_fg where snfg='".$snfg."' and jenis_periksa='".$jenis_periksa."' and state='ISTIRAHAT STOP' and lanjutan=".$lanjutan." ";
    //     $QcFg= QcFg::findBySql($sql)->one();
    //     echo Json::encode($QcFg);
    // }

    /**
     * Deletes an existing QcFg model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the QcFg model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QcFg the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QcFg::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
