<?php

namespace app\controllers;

use Yii;
use app\models\BillOfMaterials;
use app\models\BillOfMaterialsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BillOfMaterialsController implements the CRUD actions for BillOfMaterials model.
 */
class BillOfMaterialsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BillOfMaterials models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BillOfMaterialsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all BillOfMaterials models.
     * @return mixed
     */
    public function actionStart()
    {
        $searchModel = new BillOfMaterialsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';

        sleep(3);

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

        INSERT INTO status_jadwal (nojadwal,status,table_name,posisi,jenis_proses)
        SELECT  
            :snfg as nojadwal,
            1 as status,
            'flow_input_snfg' as table_name,
            'KEMAS_2' as posisi,
            :jenis_proses as jenis_proses
        ON CONFLICT (nojadwal,posisi,jenis_proses)
        DO NOTHING;
        ", 
        [':snfg' => $snfg, ':jenis_proses' => $jenis_proses]);

        $result = $command->queryAll();

        $this->redirect(['index',]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BillOfMaterials model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BillOfMaterials model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BillOfMaterials();

        $this->layout = '//main-sidebar-collapse';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BillOfMaterials model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BillOfMaterials model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BillOfMaterials model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BillOfMaterials the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BillOfMaterials::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
