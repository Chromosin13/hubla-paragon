<?php

namespace app\controllers;

use Yii;
use app\models\PenerimaanlogFg;
use app\models\AdminlogFg;
use app\models\NamaOperator;
use app\models\PenerimaanlogFgSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\UploadedFile;
use kartik\mpdf\Pdf;
use yii\helpers\ArrayHelper;
use yii\data\SqlDataProvider;
use app\base\Model;

/**
 * PenerimaanlogFgController implements the CRUD actions for PenerimaanlogFg model.
 */
class PenerimaanlogFgController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PenerimaanlogFg models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model2 = new AdminlogFg();
        $searchModel = new PenerimaanlogFgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
          'model2' => $model2,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PenerimaanlogFg model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PenerimaanlogFg model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PenerimaanlogFg();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateRincian($nopo)
    {
        // $model = $this->findModelPo($no_po);
        // $modelsOperatorPenerimaanlog = $model->operatorPenerimaanlogs;

        $sql2 = "
                SELECT *
                FROM nama_operator
                WHERE nomor_po = '". $nopo ."'
                AND nama is null
                ";
        $data2 = NamaOperator::findBySql($sql2)->one();

        $model = $data2;


        $sql = "
                SELECT *
                FROM penerimaanlog_fg
                WHERE nopo = '". $nopo ."'
                AND jumlah_koli is null
                ";
        $data = PenerimaanlogFg::findBySql($sql)->all();

        // $modelsAdminlog = $model->adminlogs;
        $modelsPenerimaanlogFg = $data;

        // echo '<pre>';
        // print_r($modelPo);
        // echo '</pre>';

        // echo '<pre>';
        // print_r($modelAdmin);
        // echo '</pre>';


        if ($model->load(Yii::$app->request->post())) {

            $oldIDs = ArrayHelper::map($modelsPenerimaanlogFg, 'id', 'id');
            $modelsPenerimaanlogFg = Model::createMultiple(PenerimaanlogFg::classname(), $modelsPenerimaanlogFg);
            Model::loadMultiple($modelsPenerimaanlogFg, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsPenerimaanlogFg, 'id', 'id')));

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsPenerimaanlogFg) && $valid;
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (!empty($deletedIDs)) {
                            PenerimaanlogFg::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($modelsPenerimaanlogFg as $modePenerimaanlogFg) {
                            $modePenerimaanlogFg->operator_id = $model->id;
                            if (! ($flag = $modePenerimaanlogFg->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                    $transaction->commit();
                    return $this->redirect(['index']);
                    }
                }catch(Exception $e) {
                $transaction->rollBack();
                }
            }
        }
        return $this->render('_form-rincian', [
            'model' => $model,
            'nopo' => $nopo,
            'modelsPenerimaanlogFg' => (empty($modelsPenerimaanlogFg)) ? [new PenerimaanlogFg] : $modelsPenerimaanlogFg
        ]);

    }

    public function actionGetPo($nopo,$koitem_fg=false)
    {
        $sql29="
        SELECT
            *
        FROM adminlog_fg
        WHERE nopo = '".$nopo."'
        ";

        if ($koitem_fg) {
            $sql29 .= " and koitem_fg= '".$koitem_fg."'";
        }

        $data= AdminlogFg::findBySql($sql29)->all();
        echo Json::encode($data);
        //print_r($data);
    }

    public function actionCheckPenerimaan()
    {

        if(in_array(Yii::$app->user->identity->group,array("admin","penerimaan"))){
                $model = new PenerimaanlogFg();

                $searchModel = new PenerimaanlogFgSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->sort->defaultOrder = ['datetime_start' => SORT_DESC];
                // $dataProvider->query->where("posisi='PENIMBANGAN' and datetime_start is not null and datetime_stop is null");

                $this->layout = '//main-sidebar-collapse';

                return $this->render('check-penerimaan', [
                        'model' => $model,
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                ]);
        }else{
            return $this->redirect(['site/forbidden']);
        }
    }

    public function actionCheckPenerimaanQr()
    {

        if(in_array(Yii::$app->user->identity->group,array("admin","penerimaan"))){
            $model = new PenerimaanlogFg();

            $searchModel = new PenerimaanlogFgSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // $dataProvider->query->where("posisi='PENIMBANGAN' and datetime_start is not null and datetime_stop is null");

            $this->layout = '//main-sidebar-collapse';

            return $this->render('check-penerimaan-qr', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
            ]);
        }else {
            return $this->redirect(['site/forbidden']);
        }
    }

    public function actionUpdateItem($nopo)
    {

        //$po = NamaOperator::findOne(['nomor_po' => $no_po]);

        // $sql= "
        //     SELECT
        //         *
        //     FROM adminlog
        //     WHERE no_po = '". $no_po ."'
        // ";

        // $data= Adminlog::findBySql($sql)->all();

        $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
                            SELECT max(batch)
                            FROM adminlog_fg
                            WHERE nopo = '".$nopo."'
                            and nopo is not null;
                            "
                        );
            $max_batch = $command->queryAll();

        $sql19="
        SELECT *
        FROM adminlog_fg
        WHERE nopo = '".$nopo."'
        and batch = '".$max_batch[0]['max']."'

        ";

        $data= AdminlogFg::findBySql($sql19)->all();

        $first = true;
        foreach ($data as $d) {
            // print_r("<pre>");
            // print_r($d);
            // print_r("</pre>");

            $po = false;
            if (!$first) {
                $po = NamaOperator::findOne(['nomor_po' => $d->nopo ]);
            }
            $first = false;

            if(!$po){

                $sqlinsert = Yii::$app->db->createCommand(
                            "INSERT INTO nama_operator(nomor_po)
                             VALUES ('".$d->nopo."')"
                            )->execute();
            }
                // if($sqlinsert){
                    $sqlinsert1 = Yii::$app->db->createCommand(
                                "INSERT INTO penerimaanlog_fg(nopo,koitem_fg,nama_fg,supplier,no_surjal, batch)
                                 VALUES (
                                    '".$d->nopo."',
                                    '".$d->koitem_fg."',
                                    '".$d->nama_fg."',
                                    '".$d->supplier."',
                                    '".$d->no_surjal."',
                                    '".$d->batch."'

                                )"
                                )->execute();

                //}
             //print_r($sqlinsert);

        }

        return $this->redirect(['create-rincian', 'nopo' => $nopo]);

        // if($sqlinsert1){
        //     return $this->redirect(['create-update2', 'no_po' => $no_po]);
        // }

    }

    public function actionPdf($id) {
        $model = $this->findModel($id);

        $sql17="
                SELECT pf.nopo, pf.koitem_fg,pf.nama_fg,pf.supplier,pf.penempatan,pf.jumlah_koli_palet,pf.qty_per_koli,pf.qty_koli_receh
                ,pf.batch,pf.jumlah,pf.jumlah_palet,pf.jumlah_koli,pf.jumlah_pcs_palet,pf.koordinat_penempatan,pf.sisa_koli_palet, pf.sisa_pcs_palet, pf.nomor_batch
                ,nop.nama as namaoperator
                from penerimaanlog_fg pf left join nama_operator nop
                on pf.operator_id = nop.id
                where pf.id='".$model->id."'
                limit 1
             ";

        $sql22="
                SELECT namaoperator
                from nama_operator
                where id='".$model->id."'
                limit 1

             ";
        $array_namaoperator = Yii::$app->db->createCommand(
          "SELECT pf.nopo, nop.nama as namaoperator
            from penerimaanlog_fg pf left join nama_operator nop
            on pf.operator_id = nop.id
            where pf.id='".$model->id."'
            limit 1
        ")->queryOne();

        // $namaoperator = PenerimaanlogFg::findBySql($sql17)->one()['namaoperator'];
        // $array_namaoperator = NamaPenerimaan::findBySql($sql22)->one();

        $nopo = $model->nopo;
        $koitem_fg = $model->koitem_fg;
        $jumlah_palet = $model->jumlah_palet;
        $nama_fg = $model->nama_fg;
        $supplier = $model->supplier;
        $penempatan = $model->penempatan;
        $koordinat_penempatan = $model->koordinat_penempatan;
        $jumlah_koli = $model->jumlah_koli;
        $jumlah_koli_palet = $model->jumlah_koli_palet;
        $qty_per_koli = $model->qty_per_koli;
        $batch = $model->batch;
        $jumlah = $model->jumlah;
        $jumlah_pcs_palet = $model->jumlah_pcs_palet;
        $namaoperator = $array_namaoperator['namaoperator'];
        $qty_koli_receh = $model->qty_koli_receh;
        $sisa_koli_palet = $model->sisa_koli_palet;
        $sisa_pcs_palet = $model->sisa_pcs_palet;
        $nomor_batch = $model->nomor_batch;

        // Populate Data to HTML Template
        $content_array = '';

        for ($i=1;$i<=$model->jumlah_palet;$i++) {
            if ($i > 0 && $i < $model->jumlah_palet){

                $content_array .=  $this->renderPartial('label1', [
                    'model' => $model,
                    'nopo' => $nopo,
                    'kode_item' => $koitem_fg,
                    'jumlah_palet' => $jumlah_palet,
                    'nama_item' => $nama_fg,
                    'supplier' => $supplier,
                    'penempatan' => $penempatan,
                    'koordinat_penempatan' => $koordinat_penempatan,
                    'jumlah_koli' => $jumlah_koli,
                    'jumlah_pcs_palet' => $jumlah_koli_palet,
                    'quantity_per_koli' => $qty_per_koli,
                    'no_batch' => $batch,
                    'nomor_batch' => $nomor_batch,
                    'jumlah' => $jumlah,
                    'jumlah_pcs_palet' => $jumlah_pcs_palet,
                    'namaoperator' => $namaoperator,
                    'quantity_koli_receh' => $qty_koli_receh,
                    'array_namaoperator' => $array_namaoperator,
                    'sisa_koli_palet' => $sisa_koli_palet,
                    'sisa_pcs_palet' => $sisa_pcs_palet,
                    'i' => $i,
                ]);
            }

            else if ($model->jumlah_palet == 1 and $model->qty_koli_receh == 0) {
                $content_array .=  $this->renderPartial('label2', [
                    'model' => $model,
                    'kode_item' => $koitem_fg,
                    'jumlah_palet' => $jumlah_palet,
                    'nama_item' => $nama_fg,
                    'supplier' => $supplier,
                    'penempatan' => $penempatan,
                    'koordinat_penempatan' => $koordinat_penempatan,
                    'jumlah_koli' => $jumlah_koli,
                    'jumlah_pcs_palet' => $jumlah_koli_palet,
                    'quantity_per_koli' => $qty_per_koli,
                    'no_batch' => $batch,
                    'nomor_batch' => $nomor_batch,
                    'jumlah' => $jumlah,
                    'jumlah_pcs_palet' => $jumlah_pcs_palet,
                    'namaoperator' => $namaoperator,
                    'quantity_koli_receh' => $qty_koli_receh,
                    'array_namaoperator' => $array_namaoperator,
                    'sisa_koli_palet' => $sisa_koli_palet,
                    'sisa_pcs_palet' => $sisa_pcs_palet,
                    'i' => $i,
                ]);
            }

            else if ($i > 0 && $i < $model->jumlah_palet && $model->qty_koli_receh == 0) {
                $content_array .=  $this->renderPartial('label1', [
                    'model' => $model,
                    'kode_item' => $koitem_fg,
                    'jumlah_palet' => $jumlah_palet,
                    'nama_item' => $nama_fg,
                    'supplier' => $supplier,
                    'penempatan' => $penempatan,
                    'koordinat_penempatan' => $koordinat_penempatan,
                    'jumlah_koli' => $jumlah_koli,
                    'jumlah_pcs_palet' => $jumlah_koli_palet,
                    'quantity_per_koli' => $qty_per_koli,
                    'no_batch' => $batch,
                    'nomor_batch' => $nomor_batch,
                    'jumlah' => $jumlah,
                    'jumlah_pcs_palet' => $jumlah_pcs_palet,
                    'namaoperator' => $namaoperator,
                    'quantity_koli_receh' => $qty_koli_receh,
                    'array_namaoperator' => $array_namaoperator,
                    'sisa_koli_palet' => $sisa_koli_palet,
                    'sisa_pcs_palet' => $sisa_pcs_palet,
                    'i' => $i,
                ]);
            }

            else if ($i > 0 && $i == $model->jumlah_palet && $model->qty_koli_receh == 0) {
                $content_array .=  $this->renderPartial('label3', [
                    'model' => $model,
                    'kode_item' => $koitem_fg,
                    'jumlah_palet' => $jumlah_palet,
                    'nama_item' => $nama_fg,
                    'supplier' => $supplier,
                    'penempatan' => $penempatan,
                    'koordinat_penempatan' => $koordinat_penempatan,
                    'jumlah_koli' => $jumlah_koli,
                    'jumlah_pcs_palet' => $jumlah_koli_palet,
                    'quantity_per_koli' => $qty_per_koli,
                    'no_batch' => $batch,
                    'nomor_batch' => $nomor_batch,
                    'jumlah' => $jumlah,
                    'jumlah_pcs_palet' => $jumlah_pcs_palet,
                    'namaoperator' => $namaoperator,
                    'quantity_koli_receh' => $qty_koli_receh,
                    'array_namaoperator' => $array_namaoperator,
                    'sisa_koli_palet' => $sisa_koli_palet,
                    'sisa_pcs_palet' => $sisa_pcs_palet,
                    'i' => $i,
                ]);
            }

            else if ($i == $model->jumlah_palet && $model->qty_koli_receh != 0) {
                $content_array .=  $this->renderPartial('label5', [
                    'model' => $model,
                    'kode_item' => $koitem_fg,
                    'jumlah_palet' => $jumlah_palet,
                    'nama_item' => $nama_fg,
                    'supplier' => $supplier,
                    'penempatan' => $penempatan,
                    'koordinat_penempatan' => $koordinat_penempatan,
                    'jumlah_koli' => $jumlah_koli,
                    'jumlah_pcs_palet' => $jumlah_koli_palet,
                    'quantity_per_koli' => $qty_per_koli,
                    'no_batch' => $batch,
                    'nomor_batch' => $nomor_batch,
                    'jumlah' => $jumlah,
                    'jumlah_pcs_palet' => $jumlah_pcs_palet,
                    'namaoperator' => $namaoperator,
                    'quantity_koli_receh' => $qty_koli_receh,
                    'array_namaoperator' => $array_namaoperator,
                    'sisa_koli_palet' => $sisa_koli_palet,
                    'sisa_pcs_palet' => $sisa_pcs_palet,
                    'i' => $i,
                ]);
            }

            else{
                $content_array .=  $this->renderPartial('label4', [
                    'model' => $model,
                    'kode_item' => $koitem_fg,
                    'jumlah_palet' => $jumlah_palet,
                    'nama_item' => $nama_fg,
                    'supplier' => $supplier,
                    'penempatan' => $penempatan,
                    'koordinat_penempatan' => $koordinat_penempatan,
                    'jumlah_koli' => $jumlah_koli,
                    'jumlah_pcs_palet' => $jumlah_koli_palet,
                    'quantity_per_koli' => $qty_per_koli,
                    'no_batch' => $batch,
                    'nomor_batch' => $nomor_batch,
                    'jumlah' => $jumlah,
                    'jumlah_pcs_palet' => $jumlah_pcs_palet,
                    'namaoperator' => $namaoperator,
                    'quantity_koli_receh' => $qty_koli_receh,
                    'array_namaoperator' => $array_namaoperator,
                    'sisa_koli_palet' => $sisa_koli_palet,
                    'sisa_pcs_palet' => $sisa_pcs_palet,
                    'i' => $i,
                ]);
            }

        }


        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_UTF8,
        // A4 paper format
        // 'format' => ([180,245]), //'format' => ([170,245]),
        'format' => ([280,345]), //'format' => ([170,245]),
        // portrait orientation
        'orientation' => Pdf::ORIENT_LANDSCAPE,
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER,
        // your html content input
        'content' => $content_array,
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting
        'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
        'cssInline' => '
                    @media all{
                                .font_next{font-family:DoodlePen}table{border-collapse:collapse;width:100%}td{border:1px solid #000}.page-break {display: none;}
                                }
                    @media print{
                                .page-break{display: block;page-break-before: always;}
                                }
                 ',
        // set mPDF properties on the fly
        'options' => [],
        // call mPDF methods on the fly
        // 'methods' => [
        // 'SetHeader'=>['
        //                 <p style="text-align: left;font-size: 10px; font-weight: bold;">
        //                     <img src="../web/images/paragon.jpeg" height="3%"> PT Paragon Technology & Innovation
        //                 </p>'
        //             ],
        // ]
        ]);


        /*------------------------------------*/
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        /*------------------------------------*/
        date_default_timezone_set('Asia/Jakarta');
            $current_time = date("H:i:s");
        $connection3 = Yii::$app->getDb();
                $command3 = $connection3->createCommand("
                                UPDATE operator_penerimaanlog
                                SET datetime_stop = :current_time,
                                    status_print = 'OK'
                                WHERE id=:id;
                                ",
                                [ ':id'=>$id,
                                  ':current_time' => date('Y-m-d h:i A'),
                                ]
                            );
                $result = $command3->queryAll();


        // return the pdf output as per the destination setting
        return $pdf->render();

    }

    /**
     * Updates an existing PenerimaanlogFg model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PenerimaanlogFg model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PenerimaanlogFg model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PenerimaanlogFg the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PenerimaanlogFg::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
