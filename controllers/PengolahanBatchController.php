<?php

namespace app\controllers;

use Yii;
use app\models\FlowInputMo;
use app\models\PengolahanBatch;
use app\models\PengolahanBatchSearch;
use app\models\ScmPlanner;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\helpers\Json;

/**
 * PengolahanBatchController implements the CRUD actions for PengolahanBatch model.
 */
class PengolahanBatchController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PengolahanBatch models.
     * @return mixed
     */
    // public function actionIndex()
    // {
    //     $searchModel = new PengolahanBatchSearch();
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    //     return $this->render('index', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }


    public function actionIndex($id)
    {
        $searchModel = new PengolahanBatchSearch();
        $searchModel->pengolahan_id=$id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->layout = '//main-sidebar-collapse';

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'pengolahan_id' => $id,
        ]);
    }

    /**
     * Displays a single PengolahanBatch model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PengolahanBatch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PengolahanBatch();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionGenerateBatchOlah($nomo)
    {
        date_default_timezone_set('Asia/Jakarta');
        $record_olah = Yii::$app->db->createCommand("SELECT * FROM 
                                                        (SELECT fim.id,fim.nomo,pb.nobatch 
                                                            FROM flow_input_mo fim 
                                                            JOIN pengolahan_batch pb ON fim.id = pb.pengolahan_id 
                                                            WHERE fim.nomo = '".$nomo."'
                                                        ) data 
                                                    WHERE nobatch is not null")->queryOne();
        
        if (empty($record_olah)){

            $koitem_bulk = ScmPlanner::find()->where(['nomo'=>$nomo])->one()['koitem_bulk'];
            $first_record = FlowInputMo::find()->where("nomo = '".$nomo."' and posisi = 'PENGOLAHAN'")->orderBy(['datetime_start'=>SORT_ASC])->one();
            $year_mapping = [1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',6=>'F',7=>'G',
                              8=>'H',9=>'I',10=>'J',11=>'K',12=>'L',13=>'M',14=>'N',
                              15=>'O',16=>'P',17=>'Q',18=>'R',19=>'S',20=>'T',21=>'U',
                              22=>'V',23=>'W',24=>'X',25=>'Y',26=>'Z'];
    
            $month_mapping = [1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',6=>'F',7=>'G',
                              8=>'H',9=>'I',10=>'J',11=>'K',12=>'L'];
    
            $serial_mapping = [0=>'0',1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',
                                6=>'F',7=>'G',8=>'H',9=>'I',10=>'J',11=>'K'];
    
            $current_year = (int)date('Y',strtotime($first_record['datetime_start']));
            $current_month = (int)date('m',strtotime($first_record['datetime_start']));
            $current_date = date('d',strtotime($first_record['datetime_start']));
            // var_dump ($first_record['datetime_start']);
            // var_dump ($current_year);
            
            //Buat mappping tahun berdasarkan acuan 2017 -> D
            $y = $year_mapping[$current_year - 2017 + 4];
    
            $m = $month_mapping[$current_month];
            $d = $current_date;
            // var_dump ($d);
    
            //Get NOMO tanpa sn -> ex:'/F0001'
            $nomo_without_serial = explode('/',$nomo)[0];
            for ($serial=1; $serial<= 10; $serial++)
            {
                $nobatch = $m.$y.$d.$serial_mapping[$serial];
                $check = Yii::$app->db->createCommand("SELECT pb.* FROM pengolahan_batch pb LEFT JOIN
                                                flow_input_mo fim ON pb.pengolahan_id = fim.id
                                                WHERE nomo LIKE '".$nomo_without_serial."%' AND pb.nobatch = '".$nobatch."'
                                                 ")->queryOne();
                if (empty($check)){
                  break;
                }
                // print_r('<pre>');
                // print_r($check);
                // print_r($serial);
                // print_r($nobatch);
            }
        }else{
            $nobatch = $record_olah['nobatch'];
        }
        $value = ['nobatch'=>$nobatch];
        echo Json::encode($value);
    }
    /**
     * Updates an existing PengolahanBatch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $scm_planner = ScmPlanner::find()->where(['snfg_komponen'=>$model->snfg_komponen])->one();
        // Planner menggunakan case yang berbeda2, kadang menggunakan besar lot, kadang menggunakan besar batch sebagai mpq teoritis
        if (!empty($scm_planner->besar_lot) && $scm_planner->besar_lot != 0){
            $mpq_teoritis = $scm_planner->besar_lot;
        }else{
            $mpq_teoritis = $scm_planner->besar_batch;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() ) {
            if($model->save())
            {
                return $this->redirect(Yii::$app->request->referrer);
            }else{
                return $this->redirect(Yii::$app->request->referrer);
            }
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
                'mpq_teoritis' => $mpq_teoritis,
            ]);
        }
    }


    /**
     * Deletes an existing PengolahanBatch model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PengolahanBatch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PengolahanBatch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PengolahanBatch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
