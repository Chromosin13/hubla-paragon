<?php

namespace app\controllers;

use Yii;
use app\models\LogTaskChecker;
use app\models\LogTaskCheckerSearch;
use app\models\FlowInputMo;
use app\models\ScmPlanner;
use app\models\LogPrintQueueChecker;
use app\models\FlowInputMoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\editable\Editable;
use yii\helpers\Json;

/**
 * LogTaskCheckerController implements the CRUD actions for LogTaskChecker model.
 */
class LogTaskCheckerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LogTaskChecker models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogTaskCheckerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all LogTaskChecker models.
     * @return mixed
     */
    public function actionTask($nomo,$siklus)
    {
        // $fim = Yii::$app->db->createCommand("SELECT nomo,nama_operator,siklus FROM flow_input_mo WHERE nomo = '".$nomo."' and siklus = '".$siklus."'")->queryOne();
        // $pic = $fim->nama_operator;

        $searchModel = new LogTaskCheckerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if ($siklus == 0){
            $dataProvider->query->where("nomo='".$nomo."'");
        }else{
            $dataProvider->query->where("nomo='".$nomo."' and siklus=".$siklus."");
        }
        $dataProvider->query->orderBy(['status'=>SORT_DESC]);
        $dataProvider->pagination  = false;


        $this->layout = '//main-sidebar-collapse';

        $sql = "
        SELECT *
        FROM scm_planner
        WHERE  nomo = '".$nomo."' ";

        $sp = ScmPlanner::findBySql($sql)->one();

        $sql = "
        SELECT *
        FROM flow_input_mo
        WHERE  nomo = '".$nomo."' and posisi = 'CHECKER' and siklus=".$siklus."";

        $row = FlowInputMo::findBySql($sql)->one();

        $sql2 = "
        SELECT *
        FROM flow_input_mo
        WHERE  nomo = '".$nomo."' and posisi = 'PENIMBANGAN' order by id desc";

        $row2 = FlowInputMo::findBySql($sql2)->one();

        // if (Yii::$app->request->post('hasEditable')) {
        //     $bomId = Yii::$app->request->post('editableKey');
        //     $model_bom = LogFormulaBreakdown::findOne($bomId);

        //     $out = Json::encode(['output' => $bomId, 'message' => $bomId]);
        //     $post = [];
        //     $posted = current($_POST['LogFormulaBreakdown']);
        //     $post['LogFormulaBreakdown'] = $posted;
        //     if ($model_bom->load($post)) {
        //         $model_bom->save();
        //         // $error = $model_bom->errors;
        //         // $out = Json::encode(['output' => '', 'message' => $error]);
        //     }
        //     // echo $out;
        //     return;
        // }

        return $this->render('task', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'sp' => $sp,
            'nomo' => $nomo,
            'siklus' => $siklus,
            'operator' => $row->nama_operator,
            'fim_id' => $row->id,
            'id_timbang' => $row2->id,
        ]);
    }

    /**
     * Displays a single LogTaskChecker model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LogTaskChecker model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LogTaskChecker();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LogTaskChecker model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LogTaskChecker model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LogTaskChecker model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogTaskChecker the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogTaskChecker::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCheckNomoChecker()
    {
        $model = new FlowInputMo();

        $searchModel = new FlowInputMoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("posisi='CHECKER' and datetime_start is not null and datetime_stop is null");

        $this->layout = '//main-sidebar-collapse';

        return $this->render('check-nomo-checker', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
        // if ($model->load(Yii::$app->request->post())) {
        //     // return $this->redirect(['create-penimbangan', 'nomo' => $model->nomo]);
        // } else {

        // }
    }

    public function actionFormChecker($nomo,$message)
    {

        //$mod = new ScmPlanner();

        //Check Last State of a Nomo
        $sql="

        SELECT
            *
        FROM log_task_checker
        WHERE nomo = '".$nomo."'
        ORDER BY nomo,id desc
        ";

        $task = LogTaskChecker::findBySql($sql)->one();

        if(!empty($task)){

            $model = new FlowInputMo();

            // Populate Grid Data
            $searchModel = new FlowInputMoSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where("nomo='".$nomo."' and posisi='CHECKER'");

            $this->layout = '//main-sidebar-collapse';

            //Check Last State of a Nomo
            $sql="

            SELECT
                distinct on (nomo)
                id,
                nomo,
                datetime_start,
                datetime_stop
            FROM flow_input_mo
            WHERE nomo = '".$nomo."' and posisi='CHECKER'
            ORDER BY nomo,id desc
            ";

            $row = FlowInputMo::findBySql($sql)->one();

            // Assign Variable Null to prevent error if it's the first data entry
            if(empty($row)){
                $last_datetime_start = null;
                $last_datetime_stop = null;
                $last_id = null;
            }else{
                $last_datetime_start = $row->datetime_start;
                $last_datetime_stop = $row->datetime_stop;
                $last_id = $row->id;
            }

            // Jenis Proses Check Status Jadwal, If Found then Don't Display
            // $sql3="
            //         SELECT
            //             jenis_proses_timbang
            //         FROM jenis_proses_timbang
            //         WHERE jenis_proses_timbang not in
            //         (SELECT jenis_proses FROM status_jadwal WHERE
            //         nojadwal='".$nomo."' and posisi='PENIMBANGAN')
            //     ";

            // $jenis_proses_list = ArrayHelper::map(JenisProsesTimbang::findBySql($sql3)->all(), 'jenis_proses_timbang','jenis_proses_timbang');

            //Check Last State of a Nomo
            // $sql3="

            // SELECT
            //     *
            // FROM log_task_checker
            // WHERE nomo = '".$nomo."' and status is NULL
            // ";

            // $undoneTask = LogTaskChecker::findBySql($sql3)->one();


            // Check If Snfg Last Status is Start, if yes then redirect to actionStopPengolahan
            if(!empty($last_datetime_start)&&empty($last_datetime_stop)){
                return $this->redirect(['task','nomo' => $nomo]);
            }

            // Check If Post Value True
            if ($model->load(Yii::$app->request->post())) {
                if ($model->nama_operator == ""){
                    echo "<script>window.alert('Belum ada nama operator!');</script>";
                    // sleep(5);
                    return $this->redirect(['form-checker','nomo' => $nomo,'message' => 'Nama operator wajib diisi!']);
                } else{
                    $model->save();
                    return $this->redirect(['task','nomo' => $nomo, 'siklus'=>$model->siklus]);
                }
            } else {
                return $this->render('form-checker', [
                    'model' => $model,
                    'nomo' => $nomo,
                    'last_datetime_start' => $last_datetime_start,
                    'last_datetime_stop' => $last_datetime_stop,
                    'last_id' => $last_id,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'message' => $message,
                    // 'jenis_proses_list' => $jenis_proses_list,
                ]);
            }
        }else{
            echo "<script>window.alert('Belum ada task untuk Nomo ini!');</script>";
            // sleep(2);
            return $this->redirect(['check-nomo-checker']);
        }



    }

    public function actionCheckKodeBb($id,$kode_bb)
    {
        $sql = "
        SELECT kode_bb
        FROM log_task_checker
        WHERE  id = ".$id;

        $row = LogTaskChecker::findBySql($sql)->all();

        // echo $row;
        if ($row[0]->kode_bb == $kode_bb){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionCheckStatusNomo($nomo)
    {
        $sql = "
        SELECT *
        FROM flow_input_mo
        WHERE  nomo = '".$nomo."' and posisi = 'CHECKER' ORDER BY datetime_start DESC " ;

        $row = FlowInputMo::findBySql($sql)->one();

        if (empty($row)){
            $result = ['status'=>'generate'];
        }else{
            if (empty($row->datetime_stop)){
                $result = ['status'=>'continue','siklus'=>$row->siklus];
            }else{
                $result = ['status'=>'generate'];
            }
        }
        // echo $row;
        // if (empty($row)){
        //     echo 0;
        // } else if (empty($row->datetime_stop)){
        //     echo 1;
        // } else {
        //     echo 2;
        // }

        echo Json::encode($result);

        // return 
    }

    public function actionCheckStatusTask($nomo)
    {
        $sql = "
        SELECT kode_bb
        FROM log_task_checker
        WHERE  nomo = '".$nomo."'";

        $row = LogTaskChecker::findBySql($sql)->all();

        $sql = "
        SELECT kode_bb
        FROM log_task_checker
        WHERE  nomo = '".$nomo."' and status is null ";

        $taskUnchecked = LogTaskChecker::findBySql($sql)->all();

        // $sql = "
        // SELECT nomo,datetime_stop
        // FROM flow_input_mo
        // WHERE  nomo = '".$nomo."' and posisi = 'CHECKER' order by datetime_start desc limit 1 ";

        // $processChecker= LogTaskChecker::findBySql($sql)->one();

        // echo $row;
        if (!empty($row)){
            if(empty($taskUnchecked)){
                echo 2;
            }else{
                echo 1;
            }
        } else {
            echo 0;
        }
    }

    public function actionCancelChecker($nomo,$siklus)
    {
        date_default_timezone_set('Asia/Jakarta');
        if ($siklus == 0){
            $sql = "
            UPDATE log_task_checker 
            SET status = null, pic = null, timestamp_checked = null, log = null
            WHERE  nomo = '".$nomo."' and kode_bb != 'AIR-RO--L' ";

            $row = Yii::$app->db->createCommand($sql)->execute();            
        }else{
            $sql = "
            UPDATE log_task_checker 
            SET status = null, pic = null, timestamp_checked = null, log = null
            WHERE  nomo = '".$nomo."' AND siklus = '".$siklus."' and kode_bb != 'AIR-RO--L'";

            $row = Yii::$app->db->createCommand($sql)->execute();
        }

        $sql2 = "
        DELETE 
        FROM flow_input_mo
        WHERE  nomo = '".$nomo."' AND siklus = '".$siklus."' AND posisi = 'CHECKER'";

        $delete_fim = Yii::$app->db->createCommand($sql2)->execute();

        // echo $row;
        if ($delete_fim>0){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionCheckUnfinishTask($nomo,$siklus){
        date_default_timezone_set('Asia/Jakarta');
        if ($siklus == 0){
            $sql = "
            SELECT *
            FROM log_task_checker
            WHERE  nomo = '".$nomo."' AND status is null and kode_bb != 'AIR-RO--L'";

            $row = LogTaskChecker::findBySql($sql)->all();            
        }else{
            $sql = "
            SELECT *
            FROM log_task_checker
            WHERE  nomo = '".$nomo."' AND siklus = '".$siklus."' AND status is null and kode_bb != 'AIR-RO--L'";

            $row = LogTaskChecker::findBySql($sql)->all();
        }
        if (empty($row)){
            echo 1;
        }else{
            echo 0;
        }
    }

    public function actionFinishChecker($nomo,$siklus)
    {
        date_default_timezone_set('Asia/Jakarta');
        // if ($siklus == 0){
        //     $sql = "
        //     SELECT *
        //     FROM log_task_checker
        //     WHERE  nomo = '".$nomo."' AND status is null";

        //     $row = LogTaskChecker::findBySql($sql)->all();            
        // }else{
        //     $sql = "
        //     SELECT *
        //     FROM log_task_checker
        //     WHERE  nomo = '".$nomo."' AND siklus = '".$siklus."' AND status is null";

        //     $row = LogTaskChecker::findBySql($sql)->all();
        // }


        $sql2 = "
        SELECT *
        FROM flow_input_mo
        WHERE  nomo = '".$nomo."' AND siklus = '".$siklus."' AND posisi = 'CHECKER'";

        $fim = FlowInputMo::findBySql($sql2)->one();

        $sql3 = "
        SELECT *
        FROM scm_planner
        WHERE  nomo = '".$nomo."' ";

        $sp = ScmPlanner::findBySql($sql3)->one();

        // if (empty($row)){
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

        UPDATE flow_input_mo
        SET datetime_stop = :current_time,
            datetime_write = :current_time
        WHERE nomo = :nomo AND posisi = 'CHECKER' AND siklus = :siklus;
        ",
        [':current_time' => date('Y-m-d h:i A'),
         ':nomo'=> $nomo,
         ':siklus'=> $siklus,
        ]);

        $result = $command->queryAll();

        $connection2 = Yii::$app->getDb();
        $command2 = $connection2->createCommand("
            INSERT INTO log_print_queue_checker (nomo,siklus,timestamp_checked,nama_bulk,operator,is_print)
            SELECT :nomo as nomo, :siklus as siklus, :timestamp_checked as timestamp_checked, :nama_bulk as nama_bulk, :operator as operator, :is_print as is_print
            ",
            [':nomo' => $nomo,
             ':siklus' => $siklus,
             ':timestamp_checked' => $fim->datetime_start,
             ':nama_bulk' => $sp->nama_bulk,
             ':operator' => $fim->nama_operator,
             ':is_print' => 'undone',
            ]
        );
        // $insert=$command->execute();
        $result2 = $command2->queryAll();
        

        // print_r($insert);
        // exit();

        $sql3 = "
        SELECT id
        FROM log_print_queue_checker
        WHERE  nomo = '".$nomo."' and siklus = ".$siklus." order by id desc limit 1";

        $print_checker_id = LogPrintQueueChecker::findBySql($sql3)->one();
        // print_r($print_checker_id);
        // exit();            
        LogPrintQueueCheckerController::actionPrintZebra($print_checker_id,true);



        if ($result and $result2){
            echo 1;
        }else{
            echo 0;
        }
        // } else {
            // echo 0;
        // }
    }

    public function actionUpdateStatus($id_bb,$log,$pic)
    {
        date_default_timezone_set('Asia/Jakarta');

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("

        UPDATE log_task_checker
        SET status = 'OK', log = :log, pic = :pic, timestamp_checked = :timestamp_checked
        WHERE id_bb = :id_bb;
        ", 
        [':id_bb'=> $id_bb,
         ':log'=>$log,
         ':pic'=>$pic,
         ':timestamp_checked'=>date('Y-m-d h:i A'),
        ]);

        $result = $command->queryAll();

        // echo $row;
        if ($result){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionScanBb($id,$wo)
    {

        $sql="
            SELECT *
            FROM log_task_checker
            WHERE id = '".$wo."'
            ";

        $task = LogTaskChecker::findBySql($sql)->one();

        // Get Current Selected Operator
        $sql2="
                SELECT
                     *
                FROM flow_input_mo
                WHERE id = ".$id;

        $op = FlowInputMo::findBySql($sql2)->one();

        $operator = $op->nama_operator;

        return $this->render('scan-bb', [
            'task' => $task,
            'op' => $op,
            'operator' => $operator,
            'id'=>$id,
            'wo'=>$wo,
            'nomo'=>$task->nomo,
        ]);
    }

    public function actionScanQr($id)
    {

        // $sql="
        //     SELECT *
        //     FROM log_task_checker
        //     WHERE id = '".$wo."'
        //     ";

        // $task = LogTaskChecker::findBySql($sql)->one();

        // Get Current Selected Operator
        $sql2="
                SELECT
                     *
                FROM flow_input_mo
                WHERE id = ".$id;

        $op = FlowInputMo::findBySql($sql2)->one();

        $operator = $op->nama_operator;
        $siklus = $op->siklus;
        $nomo = $op->nomo;

        return $this->render('scan-qr', [
            // 'task' => $task,
            'op' => $op,
            'operator' => $operator,
            'siklus' => $siklus,
            'nomo' => $nomo,
            'id'=>$id,
            // 'wo'=>$wo,
            // 'nomo'=>$task->nomo,
        ]);
    }

    public function actionCheckBbManual($nomo,$siklus,$kode_internal,$kode_olah,$qty){
        if ($siklus == 0){

            $sql = "
            SELECT id
            FROM log_task_checker
            WHERE  nomo = '".$nomo."' 
                    and kode_internal = '".$kode_internal."'  
                    and kode_olah = '".$kode_olah."'  
                    and qty = ".$qty." 
                    and status is null 
                    ";

            $row = LogTaskChecker::findBySql($sql)->one();                
        } else {
            $sql = "
            SELECT id
            FROM log_task_checker
            WHERE  nomo = '".$nomo."' 
                    and siklus = ".$siklus."  
                    and kode_internal = '".$kode_internal."'  
                    and kode_olah = '".$kode_olah."'  
                    and qty = ".$qty." 
                    and status is null 
                    ";

            $row = LogTaskChecker::findBySql($sql)->one();  
        }

        if (!empty($row)){
            $value = ["status"=>"exist","id_bb"=>$row->id];
        }else{
            $value = ["status"=>"not-exist"];
        }

        echo Json::encode($value);


           
    }

    public function actionUpdateStatusBbManual($id,$log,$pic){
        $sql = "
        UPDATE log_task_checker
        SET status = 'OK',
            log = 'manual-detail',
            pic = '".$pic."',
            timestamp_checked = current_timestamp
        WHERE  id = ".$id." ";

        $row = Yii::$app->db->createCommand($sql)->execute(); 

        if ($row>0){
            echo 1;
        }else{
            echo 0;
        }
    }

    public function actionInputDetailBb($nomo,$siklus,$pic){
        $model = new LogTaskChecker();

        if ($model->load(Yii::$app->request->post()) ) {
            
            return $this->redirect(['task', 'nomo' => $nomo , 'siklus' => $siklus]);
        } else {
            return $this->render('input-detail-bb', [
                'model' => $model,
                'nomo' => $nomo,
                'siklus' => $siklus,
                'pic' => $pic,
            ]);
        }

    }

    // public function actionCheckBb($wo,$kode_bb)
    // {
    //     $sql="select * from log_task_checker where id=".$wo;
    //     $check= LogTaskChecker::findBySql($sql)->one();

    //     if ($check->kode_bb == $kode_bb or $check->kode_internal == $kode_bb) {
    //         echo 1;
    //     } else {
    //         echo 0;
    //     }
    // }

    public function actionCheckBb($id_bb)
    {
        $sql="select * from log_task_checker where id_bb='".$id_bb."' ";
        $check= LogTaskChecker::findBySql($sql)->one();

        $sql="select * from log_task_checker where id_bb='".$id_bb."' and status = 'OK'";
        $check_scan= LogTaskChecker::findBySql($sql)->one();

        if (!empty($check) ) {
            if (empty($check_scan)){
                echo 1;
            }else{
                echo 2;
            }
        } else {
            echo 0;
        }
    }


    public function actionSendDataToPengolahan($nomo,$siklus)
    {
        // $result2 = true;

        if ($siklus == 0){
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

            SELECT 
                *
            FROM log_task_checker
            WHERE nomo = '".$nomo."' ORDER BY kode_olah,id ASC");

            $list_formula = $command->queryAll();

            $connection = Yii::$app->db;
            $command = $connection->createCommand("

            SELECT 
                id_bb
            FROM log_task_pengolahan
            WHERE nomo = '".$nomo."' ORDER BY id ASC");

            $task_pengolahan = $command->queryAll();
            $list_id_bb = [];

            foreach ($task_pengolahan as $task){
                $list_id_bb[] = $task['id_bb'];
            }

            $serial_manual = 1;

            for ($x=0; $x < count($list_formula); $x++)
            {
                if (in_array($list_formula[$x]['id_bb'],$list_id_bb)){
                    $serial_manual += 1;
                    $result2 = 1;
                    continue;
                }
                if ($list_formula[$x]['kode_bb'] == 'AIR-RO--L')
                {
                    $status = "Checked";
                    $pic = "System";
                } else {
                    $status = NULL;
                    $pic = NULL;
                }

                $connection2 = Yii::$app->getDb(); 
                $command2 = $connection2->createCommand("

                INSERT INTO log_task_pengolahan(kode_bb,nama_bb,qty,uom,kode_olah,status,pic,nomo,siklus,id_bb,serial_manual,kode_internal)
                SELECT  :kode_bb as kode_bb, 
                        :nama_bb as nama_bb,
                        :qty as qty, 
                        :uom as uom,
                        :kode_olah as kode_olah,
                        :status as status,
                        :pic as pic,
                        :nomo as nomo,
                        :siklus as siklus,
                        :id_bb as id_bb,
                        :serial_manual as serial_manual,
                        :kode_internal as kode_internal", 
                
                [':kode_bb' => $list_formula[$x]['kode_bb'],
                 ':nama_bb' => $list_formula[$x]['nama_bb'],
                 ':qty' => $list_formula[$x]['qty'],
                 ':uom' => $list_formula[$x]['uom'],
                 ':kode_olah' => $list_formula[$x]['kode_olah'],
                 ':status' => $status,
                 ':pic' => $pic,
                 ':nomo' => $nomo,
                 ':siklus' => $list_formula[$x]['siklus'],
                 ':id_bb' => $list_formula[$x]['id_bb'],
                 ':serial_manual' => $serial_manual,
                 ':kode_internal' => $list_formula[$x]['kode_internal'],
                ]);

                $result2 = $command2->queryAll();
                $serial_manual += 1;
            }
        } else {
            $connection = Yii::$app->db;
            $command = $connection->createCommand("

            SELECT 
                *
            FROM log_task_checker
            WHERE nomo = '".$nomo."' and siklus = '".$siklus."' ORDER BY id ASC");

            $list_formula = $command->queryAll();

            $connection = Yii::$app->db;
            $command = $connection->createCommand("

            SELECT 
                id_bb
            FROM log_task_pengolahan
            WHERE nomo = '".$nomo."' ORDER BY id ASC");

            $task_pengolahan = $command->queryAll();
            $list_id_bb = [];

            foreach ($task_pengolahan as $task){
                $list_id_bb[] = $task['id_bb'];
            }

            for ($x=0; $x < count($list_formula); $x++)
            {
                if (in_array($list_formula[$x]['id_bb'],$list_id_bb)){
                    $result2 = 1;
                    continue;
                }
                
                if ($list_formula[$x]['kode_bb'] == 'AIR-RO--L')
                {
                    $status = "Checked";
                    $pic = "System";
                } else {
                    $status = NULL;
                    $pic = NULL;
                }

                $connection2 = Yii::$app->getDb(); 
                $command2 = $connection2->createCommand("

                INSERT INTO log_task_pengolahan(kode_bb,nama_bb,qty,uom,kode_olah,status,pic,nomo,siklus,id_bb,kode_internal)
                SELECT  :kode_bb as kode_bb, 
                        :nama_bb as nama_bb,
                        :qty as qty, 
                        :uom as uom,
                        :kode_olah as kode_olah,
                        :status as status,
                        :pic as pic,
                        :nomo as nomo,
                        :siklus as siklus,
                        :id_bb as id_bb,
                        :kode_internal as kode_internal", 
                
                [':kode_bb' => $list_formula[$x]['kode_bb'],
                 ':nama_bb' => $list_formula[$x]['nama_bb'],
                 ':qty' => $list_formula[$x]['qty'],
                 ':uom' => $list_formula[$x]['uom'],
                 ':kode_olah' => $list_formula[$x]['kode_olah'],
                 ':status' => $status,
                 ':pic' => $pic,
                 ':nomo' => $nomo,
                 ':siklus' => $list_formula[$x]['siklus'],
                 ':id_bb' => $list_formula[$x]['id_bb'],
                 ':kode_internal' => $list_formula[$x]['kode_internal'],
                ]);

                $result2 = $command2->queryAll();
            }
        }



        if ($result2){
            echo 1;
        } else {
            echo 0;
        }

    }

    public function actionCheckNomoCheckerQr()
    {

            $model = new LogTaskChecker();

            $searchModel = new LogTaskCheckerSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // $dataProvider->query->where("posisi='PENIMBANGAN' and datetime_start is not null and datetime_stop is null");

            $this->layout = '//main-sidebar-collapse';

            return $this->render('check-nomo-checker-qr', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
            // if ($model->load(Yii::$app->request->post())) {
            //     // return $this->redirect(['create-penimbangan', 'kode_unik_formula' => $model->kode_unik_formula]);
            // } else {

            // }
    }
}
