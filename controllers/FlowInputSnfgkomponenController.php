<?php

namespace app\controllers;

use Yii;
use app\models\FlowInputSnfgkomponen;
use app\models\ScmPlanner;
use app\models\MpqMonitoring;
use app\models\JenisProsesKemas1;
use app\models\FlowInputSnfgkomponenSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use yii\helpers\ArrayHelper;
use app\models\WeigherFgMapDevice;
use yii\db\Query;

/**
 * FlowInputSnfgkomponenController implements the CRUD actions for FlowInputSnfgkomponen model.
 */
class FlowInputSnfgkomponenController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FlowInputSnfgkomponen models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FlowInputSnfgkomponenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FlowInputSnfgkomponen model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FlowInputSnfgkomponen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FlowInputSnfgkomponen();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCheckKemas1()
    {
        $model = new FlowInputSnfgkomponen();

        // Populate Grid Data
        $searchModel = new FlowInputSnfgkomponenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("datetime_start is not null and datetime_stop is null");

        $this->layout = '//main-sidebar-collapse';

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['create-kemas1', 'snfg_komponen' => $model->snfg_komponen]);
        } else {
            return $this->render('check-kemas1', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,

            ]);
        }
    }

    /**
     * Render Scan Barcode QR SNFG Kemas 1 Page
     * @return mixed
     */
    public function actionCheckKemas1Tab()
    {

            $frontend_ip = $_SERVER['REMOTE_ADDR'];

            $sql="
            SELECT
                *
            FROM weigher_fg_map_device
            WHERE frontend_ip = '".$frontend_ip."'";

            $map_device_array = WeigherFgMapDevice::findBySql($sql)->one();

            if(empty($map_device_array)){
                $sbc_ip = '10.128.1.161';
                $line = 'Line A';
            }else{
                $sbc_ip = $map_device_array->sbc_ip;
                $line = $map_device_array->line;

            }


            $model = new FlowInputSnfgkomponen();

            $searchModel = new FlowInputSnfgkomponenSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // $dataProvider->query->where("posisi='PENIMBANGAN' and datetime_start is not null and datetime_stop is null");

            $this->layout = '//main-sidebar-collapse';

            return $this->render('check-kemas1-tab', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'frontend_ip' => $frontend_ip,
                    'sbc_ip' => $sbc_ip,
                    'line' => $line,
            ]);
            // if ($model->load(Yii::$app->request->post())) {
            //     // return $this->redirect(['create-penimbangan', 'kode_unik_formula' => $model->kode_unik_formula]);
            // } else {

            // }
    }

    public function actionCreateKemas1($snfg_komponen)
    {

        $mod = new ScmPlanner();

        $result = $mod->periksaStatusKomponen($snfg_komponen);

        if($result=='UNHOLD'){


            $model = new FlowInputSnfgkomponen();

            // Populate Grid Data
            $searchModel = new FlowInputSnfgkomponenSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where("snfg_komponen='".$snfg_komponen."' and posisi='KEMAS_1'");

            $this->layout = '//main-sidebar-collapse';

            //Check Last State of a Nomo
            $sql="

            SELECT
                distinct on (snfg_komponen)
                id,
                snfg_komponen,
                datetime_start,
                datetime_stop
            FROM flow_input_snfgkomponen
            WHERE snfg_komponen = '".$snfg_komponen."' and posisi='KEMAS_1'
            ORDER BY snfg_komponen,id desc
            ";

            $row = FlowInputSnfgkomponen::findBySql($sql)->one();

            // Assign Variable Null to prevent error if it's the first data entry
            if(empty($row)){
                $last_datetime_start = null;
                $last_datetime_stop = null;
                $last_id = null;
            }else{
                $last_datetime_start = $row->datetime_start;
                $last_datetime_stop = $row->datetime_stop;
                $last_id = $row->id;
            }

            //Check Last State of a Nomo
            $sql="

            select nama_line
            from flow_input_snfgkomponen
            where snfg_komponen = '".$snfg_komponen."'
            and posisi='KEMAS_1'
            and lanjutan = (select max(lanjutan) from flow_input_snfgkomponen where snfg_komponen = '".$snfg_komponen."')
            ";

            $nama_line_row = FlowInputSnfgkomponen::findBySql($sql)->one();

            if(empty($nama_line_row)){
                $nama_line = null;
            }else{
                $nama_line = $nama_line_row->nama_line;
            }

            // Jenis Proses Check Status Jadwal, If Found then Don't Display
            $sql3="
                    SELECT
                        jenis_proses_kemas1
                    FROM jenis_proses_kemas1
                    WHERE jenis_proses_kemas1 not in
                    (SELECT jenis_proses FROM status_jadwal WHERE
                    nojadwal='".$snfg_komponen."' and posisi='KEMAS_1')
                ";

            $jenis_proses_list = ArrayHelper::map(JenisProsesKemas1::findBySql($sql3)->all(), 'jenis_proses_kemas1','jenis_proses_kemas1');


            // Check If Snfg Komponen Last Status is Start, if yes then redirect to actionStopKemas1
            if(!empty($last_datetime_start)&&(empty($last_datetime_stop))){
                return $this->redirect(['stop-kemas1','snfg_komponen' => $snfg_komponen,'last_id'=>$last_id]);
            }

            // Check If Post Value True
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                // return $this->redirect(['create-kemas1', 'snfg_komponen' => $snfg_komponen]);
                 return $this->redirect(['check-kemas1']);
            } else {
                return $this->render('create-kemas1', [
                    'model' => $model,
                    'snfg_komponen' => $snfg_komponen,
                    'last_datetime_start' => $last_datetime_start,
                    'last_datetime_stop' => $last_datetime_stop,
                    'last_id' => $last_id,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'nama_line' => $nama_line,
                    'jenis_proses_list' => $jenis_proses_list,
                ]);
            }

        }else{
            throw new \yii\web\HttpException(401,
          'Jadwal telah di HOLD / CANCEL mohon hubungi Planner');
        }

    }


    public function actionResolveKemas1($snfg_komponen,$lp_id)
    {
        $model = new FlowInputSnfgkomponen();

        // Populate Grid Data
        $searchModel = new FlowInputSnfgkomponenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("snfg_komponen='".$snfg_komponen."' and posisi='KEMAS_1'");

        $this->layout = '//main-sidebar-collapse';

        //Check Last State of a Nomo
        $sql="

        SELECT
            distinct on (snfg_komponen)
            id,
            snfg_komponen,
            datetime_start,
            datetime_stop
        FROM flow_input_snfgkomponen
        WHERE snfg_komponen = '".$snfg_komponen."' and posisi='KEMAS_1'
        ORDER BY snfg_komponen,id desc
        ";

        $row = FlowInputSnfgkomponen::findBySql($sql)->one();

        // Assign Variable Null to prevent error if it's the first data entry
        if(empty($row)){
            $last_datetime_start = null;
            $last_datetime_stop = null;
            $last_id = null;
        }else{
            $last_datetime_start = $row->datetime_start;
            $last_datetime_stop = $row->datetime_stop;
            $last_id = $row->id;
        }

        //Check Last State of a Nomo
        $sql="

        select nama_line
        from flow_input_snfgkomponen
        where snfg_komponen = '".$snfg_komponen."'
        and posisi='KEMAS_1'
        and lanjutan = 1
        ";

        $nama_line_row = FlowInputSnfgkomponen::findBySql($sql)->one();

        if(empty($nama_line_row)){
            $nama_line = null;
        }else{
            $nama_line = $nama_line_row->nama_line;
        }


        // Check If Snfg Komponen Last Status is Start, if yes then redirect to actionStopKemas1
        if(!empty($last_datetime_start)&&(empty($last_datetime_stop))){
            return $this->redirect(['stop-kemas1','snfg_komponen' => $snfg_komponen,'last_id'=>$last_id]);
        }

        // Check If Post Value True
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['create-kemas1', 'snfg_komponen' => $snfg_komponen]);
            $connection3 = Yii::$app->getDb();
            $command3 = $connection3->createCommand("
                UPDATE pusat_resolusi_log
                SET proses_id = :id
                WHERE id = :lp_id;
                ",
                [':id'=> $model->id,':lp_id'=>$lp_id]
            );

            $result3 = $command3->queryAll();

             return $this->redirect(['downtime/index-kemas1', 'id' => $model->id]);
        } else {
            return $this->render('resolve-kemas1', [
                'model' => $model,
                'snfg_komponen' => $snfg_komponen,
                'last_datetime_start' => $last_datetime_start,
                'last_datetime_stop' => $last_datetime_stop,
                'last_id' => $last_id,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'nama_line' => $nama_line,
            ]);
        }
    }


public function actionStopKemas1($snfg_komponen,$last_id)
    {
        $model = new FlowInputSnfgkomponen();

        // Populate Grid Data
        $searchModel = new FlowInputSnfgkomponenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("snfg_komponen='".$snfg_komponen."' and posisi='KEMAS_1'");

        date_default_timezone_set('Asia/Jakarta');
        $current_time = date("H:i:s");

        $this->layout = '//main-sidebar-collapse';

        // Jenis Proses
            $array_id = FlowInputSnfgkomponen::find()->where(['id' => $last_id])->one();
            $jenis_proses = $array_id->jenis_kemas;

        // Check If Post Value True
        if ($model->load(Yii::$app->request->post())){


            // Get Posted Value
                $jumlah_realisasi=Yii::$app->request->post('FlowInputSnfgkomponen')['jumlah_realisasi'];
                if(empty($jumlah_realisasi)){
                    $jumlah_realisasi=null;
                }
                $counter=Yii::$app->request->post('FlowInputSnfgkomponen')['counter'];
                if(empty($counter)){
                    $counter=null;
                }

                // Get Staging value
                $staging=Yii::$app->request->post('FlowInputSnfgkomponen')['staging'];
                if(empty($staging)){
                    $staging=null;
                }

                // Get Is Done Value
                $is_done=Yii::$app->request->post('FlowInputSnfgkomponen')['is_done'];


                // Get MPQ Monitoring Values

                // $sisa_bulk=Yii::$app->request->post('FlowInputSnfgkomponen')['sisa_bulk'];
                // if(empty($sisa_bulk)){
                //     $sisa_bulk=0;
                // }

                // $fg_retur_layak_pakai=Yii::$app->request->post('FlowInputSnfgkomponen')['fg_retur_layak_pakai'];
                // if(empty($fg_retur_layak_pakai)){
                //     $fg_retur_layak_pakai=0;
                // }

                // $fg_retur_reject=Yii::$app->request->post('FlowInputSnfgkomponen')['fg_retur_reject'];
                // if(empty($fg_retur_reject)){
                //     $fg_retur_reject=0;
                // }

                $netto_1=Yii::$app->request->post('FlowInputSnfgkomponen')['netto_1'];
                if(empty($netto_1)){
                    $netto_1=0;
                }

                $netto_2=Yii::$app->request->post('FlowInputSnfgkomponen')['netto_2'];
                if(empty($netto_2)){
                    $netto_2=0;
                }

                $netto_3=Yii::$app->request->post('FlowInputSnfgkomponen')['netto_3'];
                if(empty($netto_3)){
                    $netto_3=0;
                }

                $netto_4=Yii::$app->request->post('FlowInputSnfgkomponen')['netto_4'];
                if(empty($netto_4)){
                    $netto_4=0;
                }

                $netto_5=Yii::$app->request->post('FlowInputSnfgkomponen')['netto_5'];
                if(empty($netto_5)){
                    $netto_5=0;
                }


            // Insert Netto
            $connection2 = Yii::$app->getDb();
            $command2 = $connection2->createCommand("

                INSERT INTO mpq_monitoring (flow_input_snfgkomponen_id,netto_1,netto_2,netto_3,netto_4,netto_5)
                SELECT
                    :flow_input_snfgkomponen_id as flow_input_snfgkomponen_id,
                    :netto_1 as netto_1,
                    :netto_2 as netto_2,
                    :netto_3 as netto_3,
                    :netto_4 as netto_4,
                    :netto_5 as netto_5
            ",
            [
                ':flow_input_snfgkomponen_id' => $last_id,
                ':netto_1' => $netto_1,
                ':netto_2' => $netto_2,
                ':netto_3' => $netto_3,
                ':netto_4' => $netto_4,
                ':netto_5' => $netto_5
            ]);

            // $model->save();

            $result = $command2->queryAll();


            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("

            UPDATE flow_input_snfgkomponen
            SET datetime_stop = :current_time,
                datetime_write = :current_time,
                jumlah_realisasi = :jumlah_realisasi,
                counter = :counter,
                staging = :staging
            WHERE id = :id;
            ",
            [':current_time' => date('Y-m-d h:i A'),
             ':id'=> $last_id,
             ':jumlah_realisasi'=>$jumlah_realisasi,
             ':counter'=>$counter,
             'staging'=>$staging
            ]);

            // $model->save();

            $result = $command->queryAll();

            // Insert Is Done Value to status_jadwal
            if(!empty($is_done)){

                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("

                INSERT INTO status_jadwal (nojadwal,status,table_name,posisi,jenis_proses)
                SELECT
                    :snfg_komponen as nojadwal,
                    1 as status,
                    'flow_input_snfgkomponen' as table_name,
                    'KEMAS_1' as posisi,
                    :jenis_proses as jenis_proses
                ON CONFLICT (nojadwal,posisi,jenis_proses)
                DO NOTHING;
                ",
                [':snfg_komponen' => $snfg_komponen, ':jenis_proses' => $jenis_proses]);

                $result = $command->queryAll();

                // // Insert MPQ Monitoring
                // $connection2 = Yii::$app->getDb();
                // $command2 = $connection2->createCommand("

                //     INSERT INTO mpq_monitoring (flow_input_snfgkomponen_id,netto_1,netto_2,netto_3,netto_4,netto_5)
                //     SELECT
                //         -- :sisa_bulk as sisa_bulk,
                //         -- :fg_retur_layak_pakai as fg_retur_layak_pakai,
                //         -- :fg_retur_reject as fg_retur_reject,
                //         :flow_input_snfgkomponen_id as flow_input_snfgkomponen_id,
                //         :netto_1 as netto_1,
                //         :netto_2 as netto_2,
                //         :netto_3 as netto_3,
                //         :netto_4 as netto_4,
                //         :netto_5 as netto_5
                // ",
                // [
                //     // ':sisa_bulk' => $sisa_bulk,
                //     // ':fg_retur_layak_pakai' => $fg_retur_layak_pakai,
                //     // ':fg_retur_reject' => $fg_retur_reject,
                //     ':flow_input_snfgkomponen_id' => $last_id,
                //     ':netto_1' => $netto_1,
                //     ':netto_2' => $netto_2,
                //     ':netto_3' => $netto_3,
                //     ':netto_4' => $netto_4,
                //     ':netto_5' => $netto_5
                // ]);

                // // $model->save();

                // $result = $command2->queryAll();
                return $this->redirect(['stop-isdone', 'snfg_komponen'=>$snfg_komponen,'last_id'=>$last_id]);

            }

            // Redirect to Downtime
            return $this->redirect(['downtime/index-kemas1', 'id' => $last_id]);
            // return $this->redirect(['create-kemas1', 'snfg_komponen' => $snfg_komponen]);
             // return $this->redirect(['check-kemas1']);
        } else {
            return $this->render('_form-kemas1-stop', [
                'model' => $model,
                'jenis_kemas' => $jenis_proses,
                'snfg_komponen' => $snfg_komponen,
                'last_id' => $last_id,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,

            ]);
        }
    }

    public function actionGetSisaBulk($snfg_komponen){
      $scmp = ScmPlanner::find()->where(['snfg_komponen'=>$snfg_komponen])->one();
      $bulk_sisa = $this->queryGetBulkSisa($scmp->nomo);
      if ($bulk_sisa) {
        if ($bulk_sisa['qty'] == null) {
          return 0;
        }else{
          return $bulk_sisa['qty'];
        }
      }else{
        return 0;
      }
    }

    public function queryGetBulkSisa($nomo){
      $query = new Query();
      $res = $query
      ->from('log_bulk_sisa')
      ->where('nomo = :nomo',[':nomo' => $nomo])
      ->one();
      return $res;
    }

    public function actionStopIsdone($snfg_komponen,$last_id){

        $model = MpqMonitoring::find()->where(['flow_input_snfgkomponen_id'=>$last_id])->one();

        $this->layout = '//main-sidebar-collapse';

        // Check If Post Value True
        if ($model->load(Yii::$app->request->post())){

            // Get Posted Value
            $sisa_bulk=Yii::$app->request->post('MpqMonitoring')['sisa_bulk'];
            if(empty($sisa_bulk)){
                $sisa_bulk=0;
            }

            $fg_retur_layak_pakai=Yii::$app->request->post('MpqMonitoring')['fg_retur_layak_pakai'];
            if(empty($fg_retur_layak_pakai)){
                $fg_retur_layak_pakai=0;
            }

            $fg_retur_reject=Yii::$app->request->post('MpqMonitoring')['fg_retur_reject'];
            if(empty($fg_retur_reject)){
                $fg_retur_reject=0;
            }

            // Update MPQ Monitoring
            $connection2 = Yii::$app->getDb();

            $command2= $connection2->createCommand()
                                    ->update('mpq_monitoring',[
                                                'sisa_bulk'=>$sisa_bulk,
                                                'fg_retur_reject'=>$fg_retur_reject,
                                                'fg_retur_layak_pakai'=>$fg_retur_layak_pakai
                                            ],
                                            ['flow_input_snfgkomponen_id' => $last_id])
                                    ->execute();

            // Redirect to Downtime
            return $this->redirect(['downtime/index-kemas1', 'id' => $last_id]);
        } else {
            return $this->render('_form-stop-isdone', [
                'model' => $model,
                'snfg_komponen' => $snfg_komponen,
                'last_id' => $last_id,

            ]);
        }

    }


    public function actionGetLogInputKemas1($nojadwal)
    {
        $sql="select
                        datetime_write,
                        snfg_komponen,
                        jenis_kemas,
                        lanjutan,
                        nama_operator,
                        id
              from flow_input_snfgkomponen where snfg_komponen='".$nojadwal."'
              and posisi='KEMAS_1'";
        $pp= FlowInputSnfgkomponen::findBySql($sql)->all();
        // print_r($pprr);
        echo Json::encode($pp);

    }

    // public function actionStopKemas1($last_id)
    // {

    //     date_default_timezone_set('Asia/Jakarta');
    //     $current_time = date("H:i:s");

    //     $connection = Yii::$app->getDb();
    //     $command = $connection->createCommand("

    //         UPDATE flow_input_snfgkomponen
    //         SET datetime_stop = :current_time,
    //             datetime_write = :current_time
    //         WHERE id = :id;
    //         ",
    //         [':current_time' => date('Y-m-d h:i A'),':id'=> $last_id]);

    //     $result = $command->queryAll();

    //     // Redirect to Downtime
    //     return $this->redirect(['downtime/index-kemas1', 'id' => $last_id]);

    //     // Redirect to Referrer
    //     // return $this->redirect(Yii::$app->request->referrer);

    // }

    public function actionGetLanjutanKemas1($snfg_komponen,$jenis_kemas,$posisi)
    {
        $sql="
        SELECT
            coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM flow_input_snfgkomponen
        WHERE snfg_komponen = '".$snfg_komponen."'
        and jenis_kemas = '".$jenis_kemas."'
        and posisi = '".$posisi."'
        ";

        $lanjutan= FlowInputSnfgkomponen::findBySql($sql)->one();
        echo Json::encode($lanjutan);
    }



    /**
     * Updates an existing FlowInputSnfgkomponen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FlowInputSnfgkomponen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FlowInputSnfgkomponen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FlowInputSnfgkomponen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FlowInputSnfgkomponen::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
