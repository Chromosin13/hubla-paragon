<?php

namespace app\controllers;

use Yii;
use app\models\ScheduleNotHit;
use app\models\ScheduleNotHitSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ScheduleNotHitController implements the CRUD actions for ScheduleNotHit model.
 */
class ScheduleNotHitController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ScheduleNotHit models.
     * @return mixed
     */
    public function actionIndex($sediaan=null)
    {
        $current_date = date('Y-m-d');
        $yesterday = date('Y-m-d',strtotime("-1 days"));
        $this->layout = '//main-sidebar-collapse';
        $searchModel = new ScheduleNotHitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if(!empty($sediaan)){
            $dataProvider->query->andWhere("sediaan ilike '%".$sediaan."%' ");
        }
        $dataProvider->query->andWhere(['schedule_due'=>$yesterday]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'current_date' => $current_date,
            'yesterday' => $yesterday,
            'sediaan' => $sediaan,
        ]);
    }

    /**
     * Displays a single ScheduleNotHit model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ScheduleNotHit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ScheduleNotHit();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ScheduleNotHit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ScheduleNotHit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ScheduleNotHit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScheduleNotHit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ScheduleNotHit::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
