<?php

namespace app\controllers;

use Yii;
use app\models\Kemas2;
use app\models\Kemas2Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\components\AccessRule;
use app\models\User;
use yii\filters\AccessControl;
/**
 * Kemas2Controller implements the CRUD actions for Kemas2 model.
 */
class Kemas2Controller extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['create', 'update', 'delete','index','view'],
                'rules' => [
                    [
                        'actions' => ['create','index','update','view'],
                        'allow' => true,
                        // Allow users, moderators and admins to create
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_KEMAS
                        ],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        // Allow admins to delete
                        'roles' => [
                            User::ROLE_ADMIN
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Kemas2 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Kemas2Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Kemas2 model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Kemas2 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Kemas2();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['create']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionLanjutanSplitBatchKemas2($snfg_komponen,$jenis_kemas)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan_split_batch,0)),0)+1 as lanjutan_split_batch
        FROM 
            (
                SELECT  sp.snfg, 
                        kemas_1.snfg_komponen,
                        kemas_1.jenis_kemas,
                        kemas_1.state,
                        kemas_1.lanjutan,
                        kemas_1.lanjutan_split_batch
                from kemas_1 
                inner join scm_planner sp on sp.snfg_komponen = kemas_1.snfg_komponen
                where   state='STOP'
                        and kemas_1.batch_split=1 
                        and kemas_1.snfg is null 
                        and kemas_1.snfg_komponen is not null 
                        and kemas_1.snfg_komponen='".$snfg_komponen."'
                        
                UNION ALL

                SELECT  sp.snfg, 
                        kemas_2.snfg_komponen,
                        kemas_2.jenis_kemas,
                        kemas_2.state,
                        kemas_2.lanjutan,
                        kemas_2.lanjutan_split_batch
                from kemas_2 
                inner join scm_planner sp on sp.snfg_komponen = kemas_2.snfg_komponen
                where   state='STOP'
                        and kemas_2.batch_split=1 
                        and kemas_2.snfg is null 
                        and kemas_2.snfg_komponen is not null 
                        and kemas_2.snfg_komponen='".$snfg_komponen."'

                UNION ALL

                SELECT  p.snfg,
                        sp.snfg_komponen,
                        p.jenis_kemas,
                        p.state,
                        p.lanjutan,
                        p.lanjutan_split_batch               
                from kemas_2 p
                inner join scm_planner sp on sp.snfg=p.snfg
                where   p.state='STOP' 
                        and p.batch_split=1
                        and p.snfg is not null 
                        and sp.snfg_komponen='".$snfg_komponen."'
            ) a
        WHERE a.snfg_komponen ='".$snfg_komponen."'
        and a.jenis_kemas = '".$jenis_kemas."'";
        $Kemas2= Kemas2::findBySql($sql)->one();
        echo Json::encode($Kemas2);
    }

    public function actionLanjutanSplitBatchKemas2snfg($snfg,$jenis_kemas)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan_split_batch,0)),0)+1 as lanjutan_split_batch
        FROM 
            (            
                SELECT  sp.snfg, 
                        kemas_1.snfg_komponen,
                        kemas_1.jenis_kemas,
                        kemas_1.state,
                        kemas_1.lanjutan,
                        kemas_1.lanjutan_split_batch
                from kemas_1 
                inner join scm_planner sp on sp.snfg_komponen = kemas_1.snfg_komponen
                where   state='STOP'
                        and kemas_1.batch_split=1 
                        and kemas_1.snfg is null 
                        and kemas_1.snfg_komponen is not null 
                        and sp.snfg ='".$snfg."'
                        
                UNION ALL

                SELECT  sp.snfg, 
                        kemas_2.snfg_komponen,
                        kemas_2.jenis_kemas,
                        kemas_2.state,
                        kemas_2.lanjutan,
                        kemas_2.lanjutan_split_batch
                from kemas_2 
                inner join scm_planner sp on sp.snfg_komponen = kemas_2.snfg_komponen
                where   state='STOP'
                        and kemas_2.batch_split=1 
                        and kemas_2.snfg is null 
                        and kemas_2.snfg_komponen is not null 
                        and sp.snfg='".$snfg."'

                UNION ALL

                SELECT  p.snfg,
                        sp.snfg_komponen,
                        p.jenis_kemas,
                        p.state,
                        p.lanjutan,
                        p.lanjutan_split_batch               
                from kemas_2 p
                inner join scm_planner sp on sp.snfg=p.snfg
                where   p.state='STOP' 
                        and p.batch_split=1
                        and p.snfg is not null 
                        and p.snfg='".$snfg."'
            ) a
        WHERE a.snfg ='".$snfg."'
        and a.jenis_kemas = '".$jenis_kemas."'
        ";
        $Kemas2= Kemas2::findBySql($sql)->one();
        echo Json::encode($Kemas2);

    }


    public function actionLanjutanKemas2($snfg_komponen,$jenis_kemas)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM 
            (
                select sp.snfg, kemas_2.snfg_komponen, kemas_2.jenis_kemas, kemas_2.state, kemas_2.lanjutan
                from kemas_2 
                inner join scm_planner sp on sp.snfg_komponen = kemas_2.snfg_komponen
                where state='STOP' and kemas_2.snfg is null and kemas_2.snfg_komponen is not null 
                and kemas_2.snfg_komponen='".$snfg_komponen."'

                union all
                
                select p.snfg,sp.snfg_komponen, p.jenis_kemas, p.state, p.lanjutan 
                from kemas_2 p
                inner join scm_planner sp on sp.snfg=p.snfg
                where p.state='STOP' and p.snfg is not null and sp.snfg_komponen='".$snfg_komponen."'
            ) a
        WHERE a.snfg_komponen ='".$snfg_komponen."'
        and a.jenis_kemas = '".$jenis_kemas."'
        ";

        $Kemas2= Kemas2::findBySql($sql)->one();
        echo Json::encode($Kemas2);

    }

    public function actionLanjutanKemas2snfg($snfg,$jenis_kemas)
    {
        $sql="
        SELECT coalesce(max(coalesce(lanjutan,0)),0)+1 as lanjutan
        FROM 
            (
                select kemas_2.snfg, sp.snfg_komponen, kemas_2.jenis_kemas, kemas_2.state, kemas_2.lanjutan
                from kemas_2 
                inner join scm_planner sp on sp.snfg = kemas_2.snfg
                where state='STOP' and kemas_2.snfg is not null and kemas_2.snfg_komponen is null 
                and kemas_2.snfg='".$snfg."'

                union all
                
                select sp.snfg,p.snfg_komponen, p.jenis_kemas, p.state, p.lanjutan 
                from kemas_2 p
                inner join scm_planner sp on sp.snfg_komponen=p.snfg_komponen
                where p.state='STOP' and p.snfg is null and sp.snfg='".$snfg."'
            ) a
        WHERE a.snfg ='".$snfg."'
        and a.jenis_kemas = '".$jenis_kemas."'
        ";

        $Kemas2= Kemas2::findBySql($sql)->one();
        echo Json::encode($Kemas2);

    }

    public function actionLanjutanIstKemas2($snfg_komponen,$jenis_kemas,$lanjutan)
    {
        $sql="
                SELECT coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist
                FROM 
                    (
                        select sp.snfg, kemas_2.snfg_komponen, kemas_2.jenis_kemas, kemas_2.state, kemas_2.lanjutan,kemas_2.lanjutan_ist
                        from kemas_2 
                        inner join scm_planner sp on sp.snfg_komponen = kemas_2.snfg_komponen
                        where state='ISTIRAHAT STOP' and kemas_2.snfg is null and kemas_2.snfg_komponen is not null and kemas_2.snfg_komponen='".$snfg_komponen."'

                        union all
                        
                        select p.snfg,sp.snfg_komponen, p.jenis_kemas, p.state, p.lanjutan, p.lanjutan_ist 
                        from kemas_2 p
                        inner join scm_planner sp on sp.snfg=p.snfg
                        where p.state='ISTIRAHAT STOP' and p.snfg is not null and sp.snfg_komponen='".$snfg_komponen."'
                    ) a
                WHERE a.snfg_komponen ='".$snfg_komponen."'
                and a.jenis_kemas = '".$jenis_kemas."'
                and a.lanjutan = '".$lanjutan."'
                ";
        $Kemas2= Kemas2::findBySql($sql)->one();
        echo Json::encode($Kemas2);
    }

    public function actionLanjutanIstKemas2snfg($snfg,$jenis_kemas,$lanjutan)
    {
        $sql="
                SELECT coalesce(max(coalesce(lanjutan_ist,0)),0)+1 as lanjutan_ist
                FROM 
                    (
                        select sp.snfg, kemas_2.snfg_komponen, kemas_2.jenis_kemas, kemas_2.state, kemas_2.lanjutan,kemas_2.lanjutan_ist
                        from kemas_2 
                        inner join scm_planner sp on sp.snfg_komponen = kemas_2.snfg_komponen
                        where state='ISTIRAHAT STOP' and kemas_2.snfg is null and kemas_2.snfg_komponen is not null and sp.snfg='".$snfg."'

                        union all
                        
                        select p.snfg,sp.snfg_komponen, p.jenis_kemas, p.state, p.lanjutan, p.lanjutan_ist 
                        from kemas_2 p
                        inner join scm_planner sp on sp.snfg=p.snfg
                        where p.state='ISTIRAHAT STOP' and p.snfg is not null and p.snfg='".$snfg."'
                    ) a
                WHERE a.snfg ='".$snfg."'
                and a.jenis_kemas = '".$jenis_kemas."'
                and a.lanjutan = '".$lanjutan."'
                ";
        $Kemas2= Kemas2::findBySql($sql)->one();
        echo Json::encode($Kemas2);
    }

    /**
     * Updates an existing Kemas2 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Kemas2 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Kemas2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kemas2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kemas2::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
