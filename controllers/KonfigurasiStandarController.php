<?php

namespace app\controllers;

use Yii;
use app\models\KonfigurasiStandar;
use app\models\MasterDataLine;
use app\models\UniqueKoitem;
use app\models\ScmPlanner;
use app\models\ListProses;
use app\models\KonfigurasiStandarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * KonfigurasiStandarController implements the CRUD actions for KonfigurasiStandar model.
 */
class KonfigurasiStandarController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KonfigurasiStandar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KonfigurasiStandarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KonfigurasiStandar model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new KonfigurasiStandar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KonfigurasiStandar();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionPostadd()
    {
        
      
        // Get Variables
        $koitem = $_POST['koitem'];
        $konfig_standar_koitem = KonfigurasiStandar::findOne([
                'koitem' => $koitem,
        ]);
        $koitem_id = $konfig_standar_koitem->id;
        $mpqs = $_POST['mpq'];
        $jumlah_operators = $_POST['jumlah_operator'];
        // $lines = $_POST['line'];


        // Get Bulks

            $sql="  
                    SELECT distinct
                        koitem_bulk
                    FROM scm_planner
                    WHERE koitem_fg='".$koitem."'
                 ";
            $bulks = ScmPlanner::findBySql($sql)->all();


        // List Proses Bulk

        foreach($bulks as $bulk){
            $sql2="  
                SELECT 
                     ".$koitem_id." as ks_id,
                    '".$bulk->koitem_bulk."' as kode,
                    jenis,
                    proses
                FROM list_proses
                WHERE jenis='BULK';
             ";
            $list_proses_bulk = ListProses::findBySql($sql2)->all();  

            foreach($list_proses_bulk as $lp_bulk){
                $ks_id = $lp_bulk->ks_id;
                $kode = $lp_bulk->kode;
                $jenis = $lp_bulk->jenis;
                $proses = $lp_bulk->proses;


                foreach($mpqs as $mpq){

                    foreach($jumlah_operators as $jumlah_operator){

                        // echo "MPQ : ".$mpq." JO : ".$jumlah_operator;

                        $connection2 = Yii::$app->getDb();
                        $command2 = $connection2->createCommand("

                            INSERT INTO konfigurasi_standar_item (ks_id,kode,jenis,proses,mpq,jumlah_operator)
                            VALUES (
                                :ks_id,
                                :kode,
                                :jenis,
                                :proses,
                                :mpq,
                                :jumlah_operator
                            )
                            RETURNING id;
                            ", 
                            [   ':ks_id' => $ks_id,
                                ':kode' => $kode,
                                ':jenis' => $jenis,
                                ':proses' => $proses,    
                                ':mpq'=>$mpq,
                                ':jumlah_operator'=>$jumlah_operator
                            ]);

                        $result2 = $command2->queryAll();
                    }
                }
            }

        }

        //  List FG
            $sql2="  
                SELECT 
                     ".$koitem_id." as ks_id,
                    '".$koitem."' as kode,
                    jenis,
                    proses
                FROM list_proses
                WHERE jenis='FG';
             ";
            $list_proses_fg = ListProses::findBySql($sql2)->all();  

            foreach($list_proses_fg as $lp_fg){
            // print_r($list_proses_bulk);
                $ks_id = $lp_fg->ks_id;
                $kode = $lp_fg->kode;
                $jenis = $lp_fg->jenis;
                $proses = $lp_fg->proses;

                // print_r($ks_id);

                foreach($mpqs as $mpq){

                    foreach($jumlah_operators as $jumlah_operator){

                        // echo "MPQ : ".$mpq." JO : ".$jumlah_operator;

                        $connection2 = Yii::$app->getDb();
                        $command2 = $connection2->createCommand("

                            INSERT INTO konfigurasi_standar_item (ks_id,kode,jenis,proses,mpq,jumlah_operator)
                            VALUES (
                                :ks_id,
                                :kode,
                                :jenis,
                                :proses,
                                :mpq,
                                :jumlah_operator
                            )
                            RETURNING id;
                            ", 
                            [   ':ks_id' => $ks_id,
                                ':kode' => $kode,
                                ':jenis' => $jenis,
                                ':proses' => $proses,    
                                ':mpq'=>$mpq,
                                ':jumlah_operator'=>$jumlah_operator
                            ]);

                        $result2 = $command2->


                        queryAll();
                    }
                }
            }

        echo $koitem_id;
    }

    public function actionGetProses()
    {

        $jenis_item = $_POST['data'];

        $sql="  
                    SELECT 
                        proses
                    FROM list_proses
                    WHERE jenis='".$jenis_item."'
                 ";
        $listproses = ListProses::findBySql($sql)->all();

        print_r(Json::encode($listproses));

    }

    public function actionPosttable()
    {

         // Get Variables
            $koitem = $_POST['koitem'];

        // Get Koitem ID
            $konfig_standar_koitem = KonfigurasiStandar::findOne([
                'koitem' => $koitem,
            ]);
            $koitem_id = $konfig_standar_koitem->id;
            $data_array = $_POST['data'];

        // Array Konfigurasi
            $array_konfigurasi = json_decode($data_array);


        // Get Bulks

            if($array_konfigurasi[0]->jenis_item=='BULK'){
                
                $sql="  
                    SELECT distinct
                        koitem_bulk
                    FROM scm_planner
                    WHERE koitem_fg='".$koitem."'
                 ";
                $bulks = ScmPlanner::findBySql($sql)->all();


                foreach($array_konfigurasi as $array){
                        $proses = $array->jenis_proses;
                        $jenis = $array->jenis_item;
                        $mpq = $array->mpq;
                        $nourut = $array->nourut;
                        $jumlah_operator = $array->jumlah_operator;
                        $line = $array->line;
                        $zona = $array->zona;
                        $print_type = $array->print_type;
                        $output = $array->output;
                        $kode_streamline = $array->kode_streamline;
                        $standar_leadtime = $array->standar_leadtime;
                        $pdt = $array->pdt;

            
                        
                        foreach($bulks as $bulk){

                                // print_r($bulk->koitem_bulk);
                                $connection2 = Yii::$app->getDb();
                                $command2 = $connection2->createCommand("
                                    INSERT INTO konfigurasi_standar_item (ks_id,kode,jenis,proses,mpq,jumlah_operator,line_id,zona,output,print_flag,kode_streamline,std_leadtime,pdt,nourut)
                                    SELECT
                                        ".$koitem_id." as ks_id,
                                        '".$bulk->koitem_bulk."' as kode,
                                        '".$jenis."' as jenis,
                                        '".$proses."' as proses,
                                        ".$mpq." as mpq,
                                        ".$jumlah_operator." as jumlah_operator,
                                        ".$line." as line_id,
                                        '".$zona."' as zona,
                                        ".$output." as output,
                                        '".$print_type."' as print_flag,
                                        '".$kode_streamline."' as kode_streamline,
                                        ".$standar_leadtime." as std_leadtime,
                                        ".$pdt." as pdt,
                                        ".$nourut." as nourut
                                ");
                                $result2 = $command2->queryAll();  
                                
                                               
                        }

                    
                }

                
                echo $koitem_id;
            }
            
    }

    public function actionPostdata()
    {
        // Get Variables
            $koitem = $_POST['koitem'];

        // Get Koitem ID
            $konfig_standar_koitem = KonfigurasiStandar::findOne([
                'koitem' => $koitem,
            ]);
            $koitem_id = $konfig_standar_koitem->id;
            $data_array = $_POST['data'];

        // Array Konfigurasi
            $array_konfigurasi = json_decode($data_array);

        // Get Bulks

            $sql="  
                    SELECT distinct
                        koitem_bulk
                    FROM scm_planner
                    WHERE koitem_fg='".$koitem."'
                 ";
            $bulks = ScmPlanner::findBySql($sql)->all();


        //  Input Data
        foreach($array_konfigurasi as $array){
            $mpq = $array->mpq;
            $jumlah_operator = $array->jumlah_operator;
            $line = $array->line;
            $zona = $array->zona;
            $print_flag = $array->print_flag;
            $output = $array->output;
            $kode_streamline = $array->kode_streamline;


            // Iterate Insert Bulk
            foreach($bulks as $bulk){
                $sql2="  
                    SELECT 
                         ".$koitem_id." as ks_id,
                        '".$bulk->koitem_bulk."' as kode,
                        jenis,
                        proses
                    FROM list_proses
                    WHERE jenis='BULK';
                 ";
                $list_proses_bulk = ListProses::findBySql($sql2)->all();

                foreach($list_proses_bulk as $lp_bulk){
                    $ks_id = $lp_bulk->ks_id;
                    $kode = $lp_bulk->kode;
                    $jenis = $lp_bulk->jenis;
                    $proses = $lp_bulk->proses;

                    $connection2 = Yii::$app->getDb();
                    $command2 = $connection2->createCommand("
                        INSERT INTO konfigurasi_standar_item 
                        (ks_id,kode,jenis,proses,mpq,jumlah_operator,line_id,zona,print_flag,output,kode_streamline)
                        VALUES (
                            :ks_id,
                            :kode,
                            :jenis,
                            :proses,
                            :mpq,
                            :jumlah_operator,
                            :line,
                            :zona,
                            :print_flag,
                            :output,
                            :kode_streamline
                        )
                        RETURNING id;
                        ", 
                        [   ':ks_id' => $ks_id,
                            ':kode' => $kode,
                            ':jenis' => $jenis,
                            ':proses' => $proses,
                            ':mpq'=>$mpq,
                            ':jumlah_operator'=>$jumlah_operator,
                            ':line'=>$line,
                            ':zona'=>$zona,
                            ':print_flag'=>$print_flag,
                            ':output'=>$output,
                            ':kode_streamline'=>$kode_streamline
                        ]);

                    $result2 = $command2->queryAll();
                }
            }


            //  Iterate Insert FG
            $sql2="  
                SELECT 
                     ".$koitem_id." as ks_id,
                    '".$koitem."' as kode,
                    jenis,
                    proses
                FROM list_proses
                WHERE jenis='FG';
             ";
            $list_proses_fg = ListProses::findBySql($sql2)->all();  

            foreach($list_proses_fg as $lp_fg){
                // print_r($list_proses_bulk);
                $ks_id = $lp_fg->ks_id;
                $kode = $lp_fg->kode;
                $jenis = $lp_fg->jenis;
                $proses = $lp_fg->proses;
                // print_r($ks_id);

                $connection2 = Yii::$app->getDb();
                $command2 = $connection2->createCommand("

                    INSERT INTO konfigurasi_standar_item
                    (ks_id,kode,jenis,proses,mpq,jumlah_operator,line_id,zona,print_flag,output,kode_streamline)
                    VALUES (
                            :ks_id,
                            :kode,
                            :jenis,
                            :proses,
                            :mpq,
                            :jumlah_operator,
                            :line,
                            :zona,
                            :print_flag,
                            :output,
                            :kode_streamline
                    )
                    RETURNING id;
                    ", 
                    [   ':ks_id' => $ks_id,
                        ':kode' => $kode,
                        ':jenis' => $jenis,
                        ':proses' => $proses,
                        ':mpq'=>$mpq,
                        ':jumlah_operator'=>$jumlah_operator,
                        ':line'=>$line,
                        ':zona'=>$zona,
                        ':print_flag'=>$print_flag,
                        ':output'=>$output,
                        ':kode_streamline'=>$kode_streamline
                    ]);

                $result2 = $command2->queryAll();
            }

        }

        echo $koitem_id;

    }

    public function actionAddKoitem()
    {

        // Get Posted Data Value
        if ( isset($_POST['koitem'] ))
        {

            $koitem = $_POST['koitem'];

        }


        // Check Master Data Konfigurasi
        $konfigurasi = KonfigurasiStandar::findOne([
                'koitem' => $koitem,
        ]);


        if(empty($konfigurasi->id)){

            // echo "BELUM memiliki konfigurasi, Klik OK untuk membuat Konfigurasi";
            $id = $this->populate($koitem);

            return $this->redirect(['home','koi'=>$id]);
        

        }else{
            // echo "Konfigurasi Ditemukan, Klik OK untuk melihat rincian";
            return $this->redirect(['home','koi'=>$konfigurasi->id]);
        }

    }


    public function actionValidateKoitem()
    {

        // Get Posted Data Value
        if ( isset($_POST['koitem'] ))
        {

            $koitem = $_POST['koitem'];

        }


        // Check Master Data Koitem Based On Distinct Value of Koitem in scm_planner
        $exist_in_master = UniqueKoitem::findOne([
                'koitem' => $koitem,
        ]);

        // Check Master Data Konfigurasi
        $konfigurasi = KonfigurasiStandar::findOne([
                'koitem' => $koitem,
        ]);

        // Logic Routing
        if(empty($exist_in_master->koitem)){
            echo "Data Tidak Terdapat pada Master Data, Hubungi Planner";
        }else if(!empty($exist_in_master->koitem)){
            if(empty($konfigurasi->id)){

                // echo "BELUM memiliki konfigurasi, Klik OK untuk membuat Konfigurasi";

                $this->populate($koitem);

                return $this->redirect(['home','koitem'=>$koitem]);
                

                // if($this->populate($koitem)){
                //     return $this->redirect(['index']);
                // }
                // return $this->redirect(['index']);

                // $this->populate(['koitem' => $koitem]);
                // print_r($koitem);
                // $this->populate();
                
                // KonfigurasiStandar::populate($koitem);
                
                // return $this->redirect(['populate','koitem'=>$koitem]);

            }else{
                // echo "Konfigurasi Ditemukan, Klik OK untuk melihat rincian";
                return $this->redirect(['home','koitem'=>$koitem]);
            }
        }
    }

    public function actionCheck()
    {
        $model = new KonfigurasiStandar();


         $sql="  
                SELECT *
                FROM konfigurasi_standar_planner_stats
             ";
        $arr_konfigurasi_standar_planner_stats = KonfigurasiStandar::findBySql($sql)->one();


        if($model->load(Yii::$app->request->post())){

            $koitem=Yii::$app->request->post('KonfigurasiStandar')['koitem'];

            $konfigurasi = KonfigurasiStandar::findOne([
                'koitem' => $koitem,
            ]);

            print_r($konfigurasi->id);

        } else {
            return $this->render('check', [
                'model' => $model,
                'arr_konfigurasi_standar_planner_stats' => $arr_konfigurasi_standar_planner_stats,
            ]);
        }


        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // } else {
        //     return $this->render('check', [
        //         'model' => $model,
        //     ]);
        // }
    }

    public function actionExecute()
    {
        $koitem = '123';
        $this->populate($koitem);

    }


    public function actionAdd($koi)
    {


        // Check Master Data Konfigurasi
        $koitem_array = KonfigurasiStandar::findOne([
                'id' => $koi,
        ]);

        $koitem = $koitem_array->koitem;

        // Get Sediaan
        $sql="
                SELECT
                    sediaan            
                FROM scm_planner
                WHERE koitem_fg='".$koitem."'
                LIMIT 1
            ";

        // Query Result to Array
        $sediaan_obj = ScmPlanner::findBySql($sql)->one();
        $sediaan = $sediaan_obj->sediaan;

        $sql3="
                SELECT
                    id,nama_line            
                FROM master_data_line
                WHERE sediaan='".$sediaan."'
                ORDER BY id
            ";

        // Query Result to Array
        $list_nama_line = ArrayHelper::map(MasterDataLine::findBySql($sql3)->all(), 'id','nama_line');


        return $this->render('add', [
                // 'model' => $model,
            'koitem' => $koitem,
            'list_nama_line' => $list_nama_line,
        ]);
    }

    public function actionGetLine(){

        $sql3="
                SELECT
                    id,nama_line           
                FROM master_data_line
                WHERE nama_line ilike '%".$_GET['term']."%'
                ORDER BY id
            ";

        // $sql3="
        //         SELECT
        //             id,nama_line           
        //         FROM master_data_line
        //         ORDER BY id
        //     ";

        // Query Result to Array
        $list_nama_line = MasterDataLine::findBySql($sql3)->all();

        print_r(Json::encode($list_nama_line));
    }

    // Home Page of a specific koitem (after scanning/input on the check page)
    public function actionHome($koi)
    {

        $konfig_standar_koitem = KonfigurasiStandar::findOne([
                'id' => $koi,
            ]);
        $koitem=$konfig_standar_koitem->koitem;


        // Get the numbers of FG and Bulk for view purpose
        $sql="  
                SELECT distinct
                    sum(case when jenis='BULK' then 1 else 0 end) as jumlah_bulk,
                    sum(case when jenis='FG' then 1 else 0 end) as jumlah_fg,
                    count(*) as jumlah_konfigurasi
                FROM konfigurasi_standar_item
                WHERE ks_id=".$koi."
             ";
        $arr_jumlah_konfigurasi = KonfigurasiStandar::findBySql($sql)->one();

        return $this->render('home', [
                'koi' => $koi,
                'koitem' => $koitem,
                'arr_jumlah_konfigurasi' => $arr_jumlah_konfigurasi,
                // 'model' => $model,
        ]);
    }

    private function Populate($koitem)
    {

        $connection2 = Yii::$app->getDb();
        $command2 = $connection2->createCommand("

            INSERT INTO konfigurasi_standar (koitem)
            VALUES (
                :koitem
            )
            RETURNING id;
            ", 
            [':koitem' => $koitem]);

        $result2 = $command2->queryAll();
        return $result2[0]['id'];
    }

    public function actionGenerateTemplate()
    {

        return $this->render('home', [
                // 'model' => $model,
        ]);
    }

    /**
     * Updates an existing KonfigurasiStandar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing KonfigurasiStandar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KonfigurasiStandar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KonfigurasiStandar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KonfigurasiStandar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
