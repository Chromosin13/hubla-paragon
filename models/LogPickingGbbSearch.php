<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogPickingGbb;

/**
 * LogPickingGbbSearch represents the model behind the search form about `app\models\LogPickingGbb`.
 */
class LogPickingGbbSearch extends LogPickingGbb
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'siklus'], 'integer'],
            [['timestamp', 'datetime_start', 'datetime_stop', 'nomo', 'nama_line', 'mo_odoo', 'kode_bb', 'kode_internal', 'nama_bb', 'kode_olah', 'satuan', 'operator', 'koitem_fg', 'jenis_bb'], 'safe'],
            [['weight'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogPickingGbb::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'timestamp' => $this->timestamp,
            'datetime_start' => $this->datetime_start,
            'datetime_stop' => $this->datetime_stop,
            'siklus' => $this->siklus,
            'weight' => $this->weight,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'mo_odoo', $this->mo_odoo])
            ->andFilterWhere(['like', 'kode_bb', $this->kode_bb])
            ->andFilterWhere(['like', 'kode_internal', $this->kode_internal])
            ->andFilterWhere(['like', 'nama_bb', $this->nama_bb])
            ->andFilterWhere(['like', 'kode_olah', $this->kode_olah])
            ->andFilterWhere(['like', 'satuan', $this->satuan])
            ->andFilterWhere(['like', 'operator', $this->operator])
            ->andFilterWhere(['like', 'koitem_fg', $this->koitem_fg])
            ->andFilterWhere(['like', 'jenis_bb', $this->jenis_bb]);

        return $dataProvider;
    }
}
