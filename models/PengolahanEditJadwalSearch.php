<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PengolahanEditJadwal;

/**
 * PengolahanEditJadwalSearch represents the model behind the search form of `app\models\PengolahanEditJadwal`.
 */
class PengolahanEditJadwalSearch extends PengolahanEditJadwal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'shift_plan_start_olah', 'shift_plan_end_olah', 'start_id'], 'integer'],
            [['nomo', 'snfg_komponen', 'start', 'stop', 'jenis_olah', 'nama_line', 'nama_operator', 'plan_start_olah', 'plan_end_olah'], 'safe'],
            [['lanjutan'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengolahanEditJadwal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lanjutan' => $this->lanjutan,
            'start' => $this->start,
            'stop' => $this->stop,
            'plan_start_olah' => $this->plan_start_olah,
            'plan_end_olah' => $this->plan_end_olah,
            'shift_plan_start_olah' => $this->shift_plan_start_olah,
            'shift_plan_end_olah' => $this->shift_plan_end_olah,
            'start_id' => $this->start_id,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'jenis_olah', $this->jenis_olah])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator]);

        return $dataProvider;
    }
}
