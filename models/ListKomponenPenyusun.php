<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "list_komponen_penyusun".
 *
 * @property integer $id
 * @property string $komponen
 */
class ListKomponenPenyusun extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'list_komponen_penyusun';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['komponen'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'komponen' => 'Komponen',
        ];
    }
}
