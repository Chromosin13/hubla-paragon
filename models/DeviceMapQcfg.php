<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "device_map_qcfg".
 *
 * @property integer $id
 * @property string $sbc_ip
 * @property string $frontend_ip
 * @property string $keterangan
 */
class DeviceMapQcfg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'device_map_qcfg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sbc_ip', 'frontend_ip', 'keterangan'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sbc_ip' => 'Sbc Ip',
            'frontend_ip' => 'Frontend Ip',
            'keterangan' => 'Keterangan',
        ];
    }
}
