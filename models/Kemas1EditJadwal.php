<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kemas1_edit_jadwal".
 *
 * @property integer $id
 * @property string $snfg_komponen
 * @property string $lanjutan
 * @property string $start
 * @property string $stop
 * @property string $jenis_kemas
 * @property string $nama_line
 * @property string $nama_operator
 * @property string $jumlah_realisasi
 * @property integer $is_done
 * @property integer $start_id
 * @property integer $stop_id
 */
class Kemas1EditJadwal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function primaryKey()
    {
        return ['id'];
    }
    
    public static function tableName()
    {
        return 'kemas1_edit_jadwal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_done', 'start_id', 'stop_id'], 'integer'],
            [['snfg_komponen', 'jenis_kemas', 'nama_line', 'nama_operator'], 'string'],
            [['lanjutan', 'jumlah_realisasi'], 'number'],
            [['start', 'stop'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg_komponen' => 'Snfg Komponen',
            'lanjutan' => 'Lanjutan',
            'start' => 'Start',
            'stop' => 'Stop',
            'jenis_kemas' => 'Jenis Kemas',
            'nama_line' => 'Nama Line',
            'nama_operator' => 'Nama Operator',
            'jumlah_realisasi' => 'Jumlah Realisasi',
            'is_done' => 'Is Done',
            'start_id' => 'Start ID',
            'stop_id' => 'Stop ID',
        ];
    }
}
