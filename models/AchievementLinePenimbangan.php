<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "achievement_line_penimbangan".
 *
 * @property integer $row_number
 * @property string $tanggal
 * @property string $nama_line
 * @property string $snfg
 * @property string $snfg_komponen
 * @property string $nama_item
 * @property string $leadtime
 * @property string $leadtime_std
 * @property string $status_leadtime
 * @property string $plan_start_timbang
 * @property string $status_plan
 * @property string $posisi
 */
class AchievementLinePenimbangan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['row_number'];
    }
    
    public static function tableName()
    {
        return 'achievement_line_penimbangan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row_number'], 'integer'],
            [['tanggal', 'plan_start_timbang'], 'safe'],
            [['nama_line', 'snfg', 'snfg_komponen', 'nama_item', 'status_leadtime', 'status_plan', 'posisi'], 'string'],
            [['leadtime', 'leadtime_std'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'row_number' => 'Row Number',
            'tanggal' => 'Tanggal',
            'nama_line' => 'Nama Line',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'nama_item' => 'Nama Item',
            'leadtime' => 'Leadtime',
            'leadtime_std' => 'Leadtime Std',
            'status_leadtime' => 'Status Leadtime',
            'plan_start_timbang' => 'Plan Start Timbang',
            'status_plan' => 'Status Plan',
            'posisi' => 'Posisi',
        ];
    }
}
