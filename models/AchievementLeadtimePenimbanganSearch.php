<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AchievementLeadtimePenimbangan;

/**
 * AchievementLeadtimePenimbanganSearch represents the model behind the search form of `app\models\AchievementLeadtimePenimbangan`.
 */
class AchievementLeadtimePenimbanganSearch extends AchievementLeadtimePenimbangan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nama_line', 'nomo', 'koitem_bulk', 'nama_bulk', 'nama_fg', 'jenis_penimbangan', 'nama_operator', 'tanggal_stop','real_leadtime', 'standard_leadtime', 'achievement','sediaan'], 'safe'],
            // [[], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AchievementLeadtimePenimbangan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterCompare('achievement',$this->achievement,'=');
        $query->andFilterCompare('real_leadtime',$this->real_leadtime,'=');
        $query->andFilterCompare('standard_leadtime',$this->standard_leadtime,'=');
        $query->andFilterCompare('tanggal_stop',$this->tanggal_stop,'=');


        // $query->andFilterWhere([
        //     'id' => $this->id,
        //     'real_leadtime' => $this->real_leadtime,
        //     'standard_leadtime' => $this->standard_leadtime,
        //     // 'achievement' => $this->achievement,
        //     'tanggal_stop' => $this->tanggal_stop,
        // ]);

        $query->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'koitem_bulk', $this->koitem_bulk])
            ->andFilterWhere(['like', 'nama_bulk', $this->nama_bulk])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'sediaan', $this->sediaan])
            ->andFilterWhere(['like', 'jenis_penimbangan', $this->jenis_penimbangan])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator]);

        return $dataProvider;
    }
}
