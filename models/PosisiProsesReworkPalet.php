<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posisi_proses_rework_palet".
 *
 * @property integer $id
 * @property string $snfg_komponen
 * @property string $snfg
 * @property string $posisi
 * @property string $timestamp
 * @property string $status
 * @property string $palet_ke
 */
class PosisiProsesReworkPalet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posisi_proses_rework_palet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['snfg_komponen', 'snfg', 'posisi', 'status'], 'string'],
            [['timestamp'], 'safe'],
            [['palet_ke'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg_komponen' => 'Snfg Komponen',
            'snfg' => 'Snfg',
            'posisi' => 'Posisi',
            'timestamp' => 'Timestamp',
            'status' => 'Status',
            'palet_ke' => 'Palet Ke',
        ];
    }
}
