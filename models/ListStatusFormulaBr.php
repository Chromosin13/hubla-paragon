<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "list_status_formula_br".
 *
 * @property integer $id
 * @property string $status
 */
class ListStatusFormulaBr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'list_status_formula_br';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
        ];
    }
}
