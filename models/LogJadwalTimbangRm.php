<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_jadwal_timbang_rm".
 *
 * @property integer $id
 * @property string $nomo
 * @property string $nama_fg
 * @property string $qty_batch
 * @property string $scheduled_start
 * @property integer $formula_reference
 * @property integer $week
 * @property string $lokasi
 * @property string $status
 * @property string $timestamp
 * @property string $nama_line
 */
class LogJadwalTimbangRm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_jadwal_timbang_rm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['formula_reference', 'nomo', 'nama_fg', 'lokasi', 'status', 'nama_line'], 'string'],
            [['qty_batch'], 'number'],
            [['scheduled_start', 'timestamp'], 'safe'],
            [['week'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomo' => 'Nomo',
            'nama_fg' => 'Nama Fg',
            'qty_batch' => 'Qty Batch',
            'scheduled_start' => 'Scheduled Start',
            'formula_reference' => 'Formula Reference',
            'week' => 'Week',
            'lokasi' => 'Lokasi',
            'status' => 'Status',
            'timestamp' => 'Timestamp',
            'nama_line' => 'Nama Line',
        ];
    }
}
