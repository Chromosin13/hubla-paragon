<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "weigher_fg_map_device".
 *
 * @property integer $id
 * @property string $frontend_ip
 * @property string $sbc_ip
 * @property string $line
 * @property string $posisi
 * @property string $keterangan
 */
class WeigherFgMapDevice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'weigher_fg_map_device';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['frontend_ip', 'sbc_ip', 'line', 'posisi', 'keterangan', 'sbc_user'], 'string'],
            [['is_counter'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'frontend_ip' => 'IP Tablet',
            'sbc_ip' => 'IP Panel IDW (Raspberry Pi)',
            'line' => 'Nama Line',
            'posisi' => 'Posisi',
            'keterangan' => 'Keterangan',
            'sbc_user' => 'Sbc User',
            'is_counter' => 'Is Using Counter Innerbox'
        ];
    }
}
