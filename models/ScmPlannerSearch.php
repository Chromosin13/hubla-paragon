<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ScmPlanner;

/**
 * ScmPlannerSearch represents the model behind the search form about `app\models\ScmPlanner`.
 */
class ScmPlannerSearch extends ScmPlanner
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'week'], 'integer'],
            [['sediaan', 'streamline', 'line_timbang', 'line_olah_premix', 'line_olah', 'line_olah_2', 'line_adjust_olah_1', 'line_adjust_olah_2', 'line_press', 'line_kemas_1', 'line_kemas_2', 'start', 'due', 'kode_jadwal', 'nors', 'nomo', 'snfg', 'snfg_komponen', 'koitem_bulk', 'koitem_fg', 'nama_bulk', 'nama_fg','tglpermintaan', 'no_formula_br', 'keterangan', 'kategori_sop', 'kategori_detail', 'nobatch', 'status', 'line_terima', 'alokasi','lot_ke','line_end','npd','odoo_code'], 'safe'],
            [['leadtime', 'besar_batch', 'besar_lot', 'jumlah_press', 'jumlah_pcs','mpq_olah_kunci','jumlah_supply_packaging'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ScmPlanner::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'week' => $this->week,
            'start' => $this->start,
            'due' => $this->due,
            'leadtime' => $this->leadtime,
            'besar_batch' => $this->besar_batch,
            'besar_lot' => $this->besar_lot,
            'tglpermintaan' => $this->tglpermintaan,
            'jumlah_press' => $this->jumlah_press,
            'jumlah_pcs' => $this->jumlah_pcs,
            'mpq_olah_kunci' => $this->mpq_olah_kunci,
            'jumlah_supply_packaging' => $this->jumlah_supply_packaging,
        ]);

        $query->andFilterWhere(['ilike', 'sediaan', $this->sediaan])
            ->andFilterWhere(['ilike', 'streamline', $this->streamline])
            ->andFilterWhere(['ilike', 'line_timbang', $this->line_timbang])
            ->andFilterWhere(['ilike', 'line_olah_premix', $this->line_olah_premix])
            ->andFilterWhere(['ilike', 'line_olah', $this->line_olah])
            ->andFilterWhere(['ilike', 'line_olah_2', $this->line_olah_2])
            ->andFilterWhere(['ilike', 'line_adjust_olah_1', $this->line_adjust_olah_1])
            ->andFilterWhere(['ilike', 'line_adjust_olah_2', $this->line_adjust_olah_2])
            ->andFilterWhere(['ilike', 'line_press', $this->line_press])
            ->andFilterWhere(['ilike', 'line_kemas_1', $this->line_kemas_1])
            ->andFilterWhere(['ilike', 'line_kemas_2', $this->line_kemas_2])
            ->andFilterWhere(['ilike', 'kode_jadwal', $this->kode_jadwal])
            ->andFilterWhere(['ilike', 'nors', $this->nors])
            ->andFilterWhere(['ilike', 'nomo', $this->nomo])
            ->andFilterWhere(['ilike', 'snfg', $this->snfg])
            ->andFilterWhere(['ilike', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['ilike', 'koitem_bulk', $this->koitem_bulk])
            ->andFilterWhere(['ilike', 'koitem_fg', $this->koitem_fg])
            ->andFilterWhere(['ilike', 'nama_bulk', $this->nama_bulk])
            ->andFilterWhere(['ilike', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['ilike', 'no_formula_br', $this->no_formula_br])
            ->andFilterWhere(['ilike', 'keterangan', $this->keterangan])
            ->andFilterWhere(['ilike', 'kategori_sop', $this->kategori_sop])
            ->andFilterWhere(['ilike', 'kategori_detail', $this->kategori_detail])
            ->andFilterWhere(['ilike', 'nobatch', $this->nobatch])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'line_terima', $this->line_terima])
            ->andFilterWhere(['ilike', 'alokasi', $this->alokasi])
            ->andFilterWhere(['ilike', 'lot_ke', $this->lot_ke])
            ->andFilterWhere(['ilike', 'npd', $this->npd])
            ->andFilterWhere(['ilike', 'line_end', $this->line_end])
            ->andFilterWhere(['ilike', 'odoo_code', $this->odoo_code]);

        return $dataProvider;
    }
}
