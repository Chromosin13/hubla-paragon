<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flow_cuci_mesin".
 *
 * @property integer $id
 * @property string $nama_line
 * @property string $nama_bulk
 * @property string $nobatch
 * @property string $nomo
 * @property string $operator
 * @property string $jenis_cuci
 * @property string $verifikator
 * @property string $gombalan
 * @property integer $sarung_tangan
 * @property string $detergen
 * @property string $parafin
 */
class FlowCuciMesin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flow_cuci_mesin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_line', 'nama_bulk', 'nobatch', 'nomo', 'operator', 'jenis_cuci', 'verifikator'], 'string'],
            [['gombalan', 'detergen', 'parafin','alkohol'], 'number'],
            [['sarung_tangan','pelarut_id'], 'integer'],
            [['datetime_start','datetime_stop'], 'safe'],
            [['nobatch','nama_line','jenis_cuci', 'operator'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_line' => 'Nama Line',
            'nama_bulk' => 'Nama Bulk',
            'nobatch' => 'Nobatch',
            'nomo' => 'Nomo',
            'operator' => 'Operator',
            'jenis_cuci' => 'Jenis Cuci',
            'verifikator' => 'Verifikator',
            'gombalan' => 'Gombalan',
            'sarung_tangan' => 'Sarung Tangan',
            'detergen' => 'Detergen',
            'parafin' => 'Parafin',
            'datetime_start' => 'Start',
            'datetime_stop' => 'Stop',
            'alkohol' => 'Alkohol',
            'pelarut_id' => 'Pelarut ID',
        ];
    }
}
