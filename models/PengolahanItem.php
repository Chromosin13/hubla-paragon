<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pengolahan_item".
 *
 * @property integer $id
 * @property string $nobatch
 * @property string $besar_batch_real
 * @property integer $pengolahan_id
 *
 * @property Pengolahan $pengolahan
 */
class PengolahanItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengolahan_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nobatch'], 'string'],
            [['snfg_komponen'], 'string'],
            [['besar_batch_real'], 'number'],
            [['pengolahan_id'], 'integer'],
            [['pengolahan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pengolahan::className(), 'targetAttribute' => ['pengolahan_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nobatch' => 'Nobatch',
            'SNFG Komponen' => 'SNFG Komponen',
            'besar_batch_real' => 'Besar Batch Real',
            'pengolahan_id' => 'Pengolahan ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengolahan()
    {
        return $this->hasOne(Pengolahan::className(), ['id' => 'pengolahan_id']);
    }
}
