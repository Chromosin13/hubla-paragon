<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "array_odoo".
 *
 * @property integer $id
 * @property string $product_id
 * @property string $product_uom_qty
 * @property string $qty_done
 * @property integer $lanjutan
 */
class ArrayOdoo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'array_odoo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id'], 'string'],
            [['product_uom_qty', 'qty_done'], 'number'],
            [['lanjutan'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'product_uom_qty' => 'Product Uom Qty',
            'qty_done' => 'Qty Done',
            'lanjutan' => 'Lanjutan',
        ];
    }
}
