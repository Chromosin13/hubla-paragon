<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_proses_kemas1".
 *
 * @property integer $id
 * @property string $jenis_proses_kemas1
 */
class JenisProsesKemas1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_proses_kemas1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_proses_kemas1'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_proses_kemas1' => 'Jenis Proses Kemas1',
        ];
    }
}
