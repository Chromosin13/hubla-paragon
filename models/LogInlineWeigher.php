<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_inline_weigher".
 *
 * @property integer $id
 * @property integer $flow_input_snfg_id
 * @property string $event
 * @property string $timestamp
 * @property string $nama_line
 * @property string $status
 */
class LogInlineWeigher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_inline_weigher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['flow_input_snfg_id'], 'integer'],
            [['event', 'nama_line', 'status'], 'string'],
            [['timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'flow_input_snfg_id' => 'Flow Input Snfg ID',
            'event' => 'Event',
            'timestamp' => 'Timestamp',
            'nama_line' => 'Nama Line',
            'status' => 'Status',
        ];
    }
}
