<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogMasterBr;
use yii\helpers\Json;
use yii\data\SqlDataProvider;

/**
 * LogMasterBrSearch represents the model behind the search form about `app\models\LogMasterBr`.
 */
class LogMasterBrSearch extends LogMasterBr
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'formula_reference'], 'integer'],
            [['kode_bulk', 'nama_bulk', 'timestamp', 'last_user', 'sediaan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogMasterBr::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'formula_reference' => $this->formula_reference,
            'timestamp' => $this->timestamp,
        ]);

        $query->andFilterWhere(['like', 'kode_bulk', $this->kode_bulk])
            ->andFilterWhere(['like', 'nama_bulk', $this->nama_bulk])
            ->andFilterWhere(['like', 'last_user', $this->last_user])
            ->andFilterWhere(['like', 'sediaan', $this->sediaan]);

        return $dataProvider;
    }



    // public function getTimestamp($kode_bulk,$reference)
    // {
    //     $sql = "
    //         SELECT timestamp
    //         FROM log_master_br
    //         WHERE kode_bulk = '".$kode_bulk."' AND formula_reference = '".$reference."'
    //     ";

    //     $log_data = LogMasterBr::findBySql($sql)->one();

    //     if (!empty($log_data)){
    //         echo $log_data->timestamp;
    //     } else {
    //         echo 0;
    //     }
    // }
}
