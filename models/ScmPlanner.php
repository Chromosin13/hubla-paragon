<?php

namespace app\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "scm_planner".
 *
 * @property integer $id
 * @property integer $week
 * @property string $sediaan
 * @property string $streamline
 * @property string $line_timbang
 * @property string $line_olah_premix
 * @property string $line_olah
 * @property string $line_olah_2
 * @property string $line_adjust_olah_1
 * @property string $line_adjust_olah_2
 * @property string $line_press
 * @property string $line_kemas_1
 * @property string $line_kemas_2
 * @property string $start
 * @property string $due
 * @property string $leadtime
 * @property string $kode_jadwal
 * @property string $nors
 * @property string $nomo
 * @property string $snfg
 * @property string $snfg_komponen
 * @property string $koitem_bulk
 * @property string $koitem_fg
 * @property string $nama_bulk
 * @property string $nama_fg
 * @property string $besar_batch
 * @property string $besar_lot
 * @property string $lot_ke
 * @property string $tglpermintaan
 * @property string $no_formula_br
 * @property string $jumlah_press
 * @property string $jumlah_pcs
 * @property string $keterangan
 * @property string $kategori_sop
 * @property string $kategori_detail
 * @property string $nobatch
 * @property string $status
 * @property string $line_terima
 * @property string $alokasi
 */
class ScmPlanner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scm_planner';
    }

    /**
     * @inheritdoc
     */
    public $tanggal_start;
    public $tanggal_stop;
    public $status_jadwal_api;


    public function rules()
    {
        return [
            [['week','cron_update'], 'integer'],
            [['sediaan', 'streamline', 'line_timbang', 'line_olah_premix', 'line_olah', 'line_olah_2', 'line_adjust_olah_1', 'line_adjust_olah_2', 'line_press', 'line_kemas_1', 'line_kemas_2', 'kode_jadwal', 'nors', 'nomo', 'snfg', 'snfg_komponen', 'koitem_bulk', 'koitem_fg', 'nama_bulk', 'nama_fg','no_formula_br', 'keterangan', 'kategori_sop', 'kategori_detail', 'nobatch', 'status', 'line_terima', 'alokasi','lot_ke','line_end','npd','odoo_code'], 'string'],
            [['start', 'due', 'tglpermintaan','tgl_start','tgl_stop','timestamp'], 'safe'],
            [['leadtime', 'besar_batch', 'besar_lot', 'jumlah_press', 'jumlah_pcs','mpq_olah_kunci','jumlah_supply_packaging'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'week' => 'Week',
            'cron_update' => 'Cron Update',
            'sediaan' => 'Sediaan',
            'streamline' => 'Streamline',
            'line_timbang' => 'Line Timbang',
            'line_olah_premix' => 'Line Olah Premix',
            'line_olah' => 'Line Olah',
            'line_olah_2' => 'Line Olah 2',
            'line_adjust_olah_1' => 'Line Adjust Olah 1',
            'line_adjust_olah_2' => 'Line Adjust Olah 2',
            'line_press' => 'Line Press',
            'line_kemas_1' => 'Line Kemas 1',
            'line_kemas_2' => 'Line Kemas 2',
            'start' => 'Start',
            'due' => 'Due',
            'leadtime' => 'Leadtime',
            'kode_jadwal' => 'Kode Jadwal',
            'nors' => 'Nors',
            'nomo' => 'Nomo',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'koitem_bulk' => 'Koitem Bulk',
            'koitem_fg' => 'Koitem Fg',
            'nama_bulk' => 'Nama Bulk',
            'nama_fg' => 'Nama Fg',
            'besar_batch' => 'Besar Batch',
            'besar_lot' => 'Besar Lot',
            'lot_ke' => 'Lot Ke',
            'tglpermintaan' => 'Tglpermintaan',
            'no_formula_br' => 'No Formula Br',
            'jumlah_press' => 'Jumlah Press',
            'jumlah_pcs' => 'Jumlah Pcs',
            'keterangan' => 'Keterangan',
            'kategori_sop' => 'Kategori Sop',
            'kategori_detail' => 'Kategori Detail',
            'nobatch' => 'Nobatch',
            'status' => 'Status',
            'line_terima' => 'Line Terima',
            'alokasi' => 'Alokasi',
            'npd' => 'NPD',
            'line_end' => 'Line End',
            'mpq_olah_kunci' => 'MPQ Olah Kunci (Kg)',
            'jumlah_supply_packaging' => 'Jumlah Supply Packaging',
            'tanggal_start' => 'Dari (Tanggal Penimbangan)',
            'tanggal_stop' => 'Sampai (Tanggal Penimbangan)',
            'timestamp' => 'Waktu Upload',
            'odoo_code' => 'Odoo Code',
        ];
    }

    public function periksaStatusNomo($nomo)
    {
        $sql="select 
                  nomo,status
              from scm_planner 
              where nomo='".$nomo."'
              order by id desc";
        $planner= ScmPlanner::findBySql($sql)->one();
        return $planner->status;
    }


    public function periksaStatusKomponen($snfg_komponen)
    {
        $sql="select 
                  snfg_komponen,status
              from scm_planner 
              where snfg_komponen='".$snfg_komponen."'
              order by id desc";
        $planner= ScmPlanner::findBySql($sql)->one();
        return $planner->status;
    }

    public function periksaStatusSnfg($snfg)
    {
        $sql="select 
                  snfg,status
              from scm_planner 
              where snfg='".$snfg."'
              order by id desc";
        $planner= ScmPlanner::findBySql($sql)->one();
        if (!empty($planner)){
            return Json::encode(["status" => $planner->status]);
        } else {
            return Json::encode(["status" => "not-found"]);
        }
    }

    public function http_request($url){
        // persiapkan curl
        $ch = curl_init(); 

        // set url 
        curl_setopt($ch, CURLOPT_URL, $url);
        
        // set user agent    
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        // return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        // $output contains the output string 
        $output = curl_exec($ch); 

        // tutup curl 
        curl_close($ch);      

        // mengembalikan hasil curl
        return $output;
    }
}
