<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "planner_distinct_snfg".
 *
 * @property string $snfg
 * @property string $status
 */
class PlannerDistinctSnfg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function primaryKey()
    {
        return ['snfg'];
    }

    public static function tableName()
    {
        return 'planner_distinct_snfg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'status'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'snfg' => 'Snfg',
            'status' => 'Status',
        ];
    }
}
