<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_penimbangan_rm_split".
 *
 * @property integer $id
 * @property integer $log_penimbangan_rm_id
 * @property string $kode_bb
 * @property string $no_batch
 * @property string $realisasi
 * @property string $satuan_realisasi
 * @property integer $urutan
 *
 * @property LogPenimbanganRm $logPenimbanganRm
 */
class LogPenimbanganRmSplit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_penimbangan_rm_split';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['log_penimbangan_rm_id', 'urutan'], 'integer'],
            [['kode_bb', 'no_batch', 'satuan_realisasi', 'kode_internal', 'log'], 'string'],
            [['realisasi'], 'number'],
            [['log_penimbangan_rm_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogPenimbanganRm::className(), 'targetAttribute' => ['log_penimbangan_rm_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'log_penimbangan_rm_id' => 'Log Penimbangan Rm ID',
            'kode_bb' => 'Kode Bb',
            'no_batch' => 'No Batch',
            'realisasi' => 'Realisasi',
            'satuan_realisasi' => 'Satuan Realisasi',
            'urutan' => 'Urutan',
            'kode_internal' => 'Kode Internal',
            'log' => 'Log'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogPenimbanganRm()
    {
        return $this->hasOne(LogPenimbanganRm::className(), ['id' => 'log_penimbangan_rm_id']);
    }
}
