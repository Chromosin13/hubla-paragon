<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hit_achievement".
 *
 * @property integer $id
 * @property string $sediaan
 * @property string $nomo
 * @property string $snfg
 * @property string $nama_fg
 * @property string $due
 * @property string $finish_qcfg
 * @property integer $hit
 */
class HitAchievement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hit_achievement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'hit'], 'integer'],
            [['sediaan', 'nomo', 'snfg', 'nama_fg'], 'string'],
            [['due', 'finish_qcfg'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sediaan' => 'Sediaan',
            'nomo' => 'Nomo',
            'snfg' => 'Snfg',
            'nama_fg' => 'Nama Fg',
            'due' => 'Due',
            'finish_qcfg' => 'Finish Qcfg',
            'hit' => 'Hit',
        ];
    }
}
