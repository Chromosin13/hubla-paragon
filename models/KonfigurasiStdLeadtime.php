<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "konfigurasi_std_leadtime".
 *
 * @property integer $id
 * @property string $koitem
 * @property string $koitem_bulk
 * @property string $naitem
 * @property string $std_type
 * @property string $last_update
 * @property string $jenis
 * @property string $value
 */
class KonfigurasiStdLeadtime extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'konfigurasi_std_leadtime';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['koitem', 'koitem_bulk', 'naitem', 'std_type', 'jenis'], 'string'],
            [['last_update'], 'safe'],
            [['value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'koitem' => 'Koitem',
            'koitem_bulk' => 'Koitem Bulk',
            'naitem' => 'Naitem',
            'std_type' => 'Std Type',
            'last_update' => 'Last Update',
            'jenis' => 'Jenis',
            'value' => 'Value',
        ];
    }
}
