<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "line_monitoring_olah".
 *
 * @property string $proses
 * @property string $nama_line
 * @property string $nomo
 * @property string $waktu
 * @property string $state
 * @property string $lanjutan
 * @property string $jenis_proses
 */
class LineMonitoringOlah extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'line_monitoring_olah';
    }

    public static function primaryKey()
    {
        return ['nama_line'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proses', 'nama_line', 'nomo', 'state', 'jenis_proses'], 'string'],
            [['waktu'], 'safe'],
            [['lanjutan'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'proses' => 'Proses',
            'nama_line' => 'Nama Line',
            'nomo' => 'Nomo',
            'waktu' => 'Waktu',
            'state' => 'State',
            'lanjutan' => 'Lanjutan',
            'jenis_proses' => 'Jenis Proses',
        ];
    }
}
