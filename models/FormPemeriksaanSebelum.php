<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_pemeriksaan_sebelum".
 *
 * @property integer $id
 * @property integer $flow_input_mo_id
 * @property string $date
 * @property string $checker
 * @property integer $is_bersih
 * @property string $perbaikan_bersih
 * @property string $comment_bersih
 * @property string $pic_bersih
 * @property integer $is_lengkap
 * @property string $perbaikan_lengkap
 * @property string $comment_lengkap
 * @property string $pic_lengkap
 * @property integer $is_helper
 * @property string $perbaikan_helper
 * @property string $comment_helper
 * @property string $pic_helper
 * @property integer $is_debu
 * @property string $perbaikan_debu
 * @property string $comment_debu
 * @property string $pic_debu
 * @property integer $is_rusak
 * @property string $perbaikan_rusak
 * @property string $comment_rusak
 * @property string $pic_rusak
 * @property integer $is_kadaluarsa
 * @property string $perbaikan_kadaluarsa
 * @property string $comment_kadaluarsa
 * @property string $pic_kadaluarsa
 * @property integer $is_release
 * @property string $perbaikan_release
 * @property string $comment_release
 * @property string $pic_release
 * @property integer $is_cukup
 * @property string $perbaikan_cukup
 * @property string $comment_cukup
 * @property string $pic_cukup
 */
class FormPemeriksaanSebelum extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_pemeriksaan_sebelum';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['flow_input_mo_id', 'is_bersih', 'is_lengkap', 'is_helper', 'is_debu', 'is_rusak', 'is_kadaluarsa', 'is_release', 'is_cukup'], 'integer'],
            [['date'], 'safe'],
            [['checker', 'perbaikan_bersih', 'comment_bersih', 'pic_bersih', 'perbaikan_lengkap', 'comment_lengkap', 'pic_lengkap', 'perbaikan_helper', 'comment_helper', 'pic_helper', 'perbaikan_debu', 'comment_debu', 'pic_debu', 'perbaikan_rusak', 'comment_rusak', 'pic_rusak', 'perbaikan_kadaluarsa', 'comment_kadaluarsa', 'pic_kadaluarsa', 'perbaikan_release', 'comment_release', 'pic_release', 'perbaikan_cukup', 'comment_cukup', 'pic_cukup'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'flow_input_mo_id' => 'Flow Input Mo ID',
            'date' => 'Date',
            'checker' => 'Checker',
            'is_bersih' => 'Is Bersih',
            'perbaikan_bersih' => 'Perbaikan Bersih',
            'comment_bersih' => 'Comment Bersih',
            'pic_bersih' => 'Pic Bersih',
            'is_lengkap' => 'Is Lengkap',
            'perbaikan_lengkap' => 'Perbaikan Lengkap',
            'comment_lengkap' => 'Comment Lengkap',
            'pic_lengkap' => 'Pic Lengkap',
            'is_helper' => 'Is Helper',
            'perbaikan_helper' => 'Perbaikan Helper',
            'comment_helper' => 'Comment Helper',
            'pic_helper' => 'Pic Helper',
            'is_debu' => 'Is Debu',
            'perbaikan_debu' => 'Perbaikan Debu',
            'comment_debu' => 'Comment Debu',
            'pic_debu' => 'Pic Debu',
            'is_rusak' => 'Is Rusak',
            'perbaikan_rusak' => 'Perbaikan Rusak',
            'comment_rusak' => 'Comment Rusak',
            'pic_rusak' => 'Pic Rusak',
            'is_kadaluarsa' => 'Is Kadaluarsa',
            'perbaikan_kadaluarsa' => 'Perbaikan Kadaluarsa',
            'comment_kadaluarsa' => 'Comment Kadaluarsa',
            'pic_kadaluarsa' => 'Pic Kadaluarsa',
            'is_release' => 'Is Release',
            'perbaikan_release' => 'Perbaikan Release',
            'comment_release' => 'Comment Release',
            'pic_release' => 'Pic Release',
            'is_cukup' => 'Is Cukup',
            'perbaikan_cukup' => 'Perbaikan Cukup',
            'comment_cukup' => 'Comment Cukup',
            'pic_cukup' => 'Pic Cukup',
        ];
    }
}
