<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_verified_br".
 *
 * @property integer $id
 * @property string $nomo
 * @property string $timestamp
 * @property string $status
 * @property string $pic
 */
class LogVerifiedBr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_verified_br';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'status', 'pic','nama_line'], 'string'],
            [['timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomo' => 'Nomo',
            'timestamp' => 'Timestamp',
            'status' => 'Status',
            'pic' => 'Pic',
            'nama_line' => 'Nama Line',
        ];
    }
}
