<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "barcode_malaysia_printer".
 *
 * @property integer $id
 * @property string $timestamp
 * @property string $snfg
 * @property string $koitem_fg
 * @property string $nama_fg
 * @property string $barcode
 * @property integer $qty_request
 * @property integer $qty_total_per_snfg
 * @property string $status
 * @property string $lokasi
 */
class BarcodeMalaysiaPrinter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'barcode_malaysia_printer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['timestamp'], 'safe'],
            [['snfg', 'koitem_fg', 'nama_fg', 'barcode', 'status','lokasi'], 'string'],
            [['qty_request','barcode'], 'required'],
            [['qty_request'], 'integer', 'min' => 1, 'max' => 200],
            [['qty_total_per_snfg'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'timestamp' => 'Timestamp',
            'snfg' => 'Snfg',
            'koitem_fg' => 'Koitem Fg',
            'nama_fg' => 'Nama Fg',
            'barcode' => 'Barcode',
            'qty_request' => 'Qty Request',
            'qty_total_per_snfg' => 'Qty Total Per Snfg',
            'status' => 'Status',
            'lokasi' => 'Lokasi',
        ];
    }
}
