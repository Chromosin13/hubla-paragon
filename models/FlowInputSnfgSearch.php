<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FlowInputSnfg;

/**
 * FlowInputSnfgSearch represents the model behind the search form of `app\models\FlowInputSnfg`.
 */
class FlowInputSnfgSearch extends FlowInputSnfg
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['posisi', 'datetime_start', 'datetime_stop', 'datetime_write', 'nama_operator', 'nama_line', 'jenis_kemas', 'snfg', 'nobatch','exp_date'], 'safe'],
            [['lanjutan', 'is_done', 'counter', 'id'], 'integer'],
            [['jumlah_realisasi'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FlowInputSnfg::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lanjutan' => $this->lanjutan,
            'datetime_start' => $this->datetime_start,
            'datetime_stop' => $this->datetime_stop,
            'datetime_write' => $this->datetime_write,
            'is_done' => $this->is_done,
            'counter' => $this->counter,
            'jumlah_realisasi' => $this->jumlah_realisasi,
            'id' => $this->id,
            'exp_date' => $this->exp_date,
        ]);

        $query->andFilterWhere(['like', 'posisi', $this->posisi])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'jenis_kemas', $this->jenis_kemas])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'nobatch', $this->nobatch]);

        return $dataProvider;
    }
}
