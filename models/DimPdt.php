<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dim_planned_down_time".
 *
 * @property integer $id
 * @property string $keterangan
 */
class DimPdt extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dim_planned_down_time';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keterangan'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keterangan' => 'Keterangan',
        ];
    }
}
