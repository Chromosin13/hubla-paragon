<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogPenimbanganRm;

/**
 * LogPenimbanganRmSearch represents the model behind the search form about `app\models\LogPenimbanganRm`.
 */
class LogPenimbanganRmSearch extends LogPenimbanganRm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'no_revisi', 'siklus', 'cycle_time', 'operator'], 'integer'],
            [['nomo', 'no_formula', 'naitem_fg', 'datetime_start', 'datetime_stop', 'datetime_write', 'kode_bb', 'nama_bb', 'kode_olah', 'satuan', 'sediaan', 'timbangan', 'no_timbangan', 'satuan_realisasi', 'status'], 'safe'],
            [['weight', 'realisasi'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogPenimbanganRm::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'no_revisi' => $this->no_revisi,
            'datetime_start' => $this->datetime_start,
            'datetime_stop' => $this->datetime_stop,
            'datetime_write' => $this->datetime_write,
            'siklus' => $this->siklus,
            'weight' => $this->weight,
            'cycle_time' => $this->cycle_time,
            'operator' => $this->operator,
            'realisasi' => $this->realisasi,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'no_formula', $this->no_formula])
            ->andFilterWhere(['like', 'naitem_fg', $this->naitem_fg])
            ->andFilterWhere(['like', 'kode_bb', $this->kode_bb])
            ->andFilterWhere(['like', 'nama_bb', $this->nama_bb])
            ->andFilterWhere(['like', 'kode_olah', $this->kode_olah])
            ->andFilterWhere(['like', 'satuan', $this->satuan])
            ->andFilterWhere(['like', 'sediaan', $this->sediaan])
            ->andFilterWhere(['like', 'timbangan', $this->timbangan])
            ->andFilterWhere(['like', 'no_timbangan', $this->no_timbangan])
            ->andFilterWhere(['like', 'satuan_realisasi', $this->satuan_realisasi])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
