<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AchievementLinePenimbangan1;

/**
 * AchievementLinePenimbangan1Search represents the model behind the search form about `app\models\AchievementLinePenimbangan1`.
 */
class AchievementLinePenimbangan1Search extends AchievementLinePenimbangan1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row_number'], 'integer'],
            [['snfg_komponen', 'snfg', 'nama_fg', 'jenis_penimbangan', 'nama_line', 'tanggal_start', 'tanggal_stop', 'status_leadtime', 'plan_start_timbang', 'status_ontime'], 'safe'],
            [['is_done', 'leadtime', 'std_leadtime'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AchievementLinePenimbangan1::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'row_number' => $this->row_number,
            'tanggal_start' => $this->tanggal_start,
            'tanggal_stop' => $this->tanggal_stop,
            'is_done' => $this->is_done,
            'leadtime' => $this->leadtime,
            'std_leadtime' => $this->std_leadtime,
            'plan_start_timbang' => $this->plan_start_timbang,
        ]);

        $query->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'jenis_penimbangan', $this->jenis_penimbangan])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'status_leadtime', $this->status_leadtime])
            ->andFilterWhere(['like', 'status_ontime', $this->status_ontime]);

        return $dataProvider;
    }
}
