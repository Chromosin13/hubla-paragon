<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "innerbox_label_print".
 *
 * @property integer $id
 * @property string $snfg
 * @property string $timestamp
 * @property string $no_batch
 * @property string $exp_date
 * @property string $nama_line
 * @property integer $qty_request
 * @property integer $qty_total
 * @property string $nama_fg
 * @property string $barcode
 * @property integer $innerbox_qty
 * @property string $status
 * @property string $operator
 * @property integer $flow_input_snfg_id
 * @property string $na_number
 * @property string $odoo_code
 * @property string $sediaan
 */
class InnerboxLabelPrint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'innerbox_label_print';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'no_batch', 'nama_line', 'nama_fg', 'status', 'operator', 'na_number', 'odoo_code', 'sediaan'], 'string'],
            [['timestamp', 'exp_date'], 'safe'],
            [['qty_total', 'innerbox_qty', 'flow_input_snfg_id'], 'integer'],
            [['qty_request'], 'integer', 'min' => 1, 'max' => 300],
            [['barcode'], 'string', 'min' => 13, 'max' => 13],
            [['qty_request','operator','exp_date', 'innerbox_qty', 'barcode'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'timestamp' => 'Timestamp',
            'no_batch' => 'No Batch',
            'exp_date' => 'Exp Date',
            'nama_line' => 'Nama Line',
            'qty_request' => 'Qty Request',
            'qty_total' => 'Qty Total',
            'nama_fg' => 'Nama Fg',
            'barcode' => 'Barcode',
            'innerbox_qty' => 'Qty per Innerbox',
            'status' => 'Status',
            'operator' => 'Operator',
            'flow_input_snfg_id' => 'Flow Input Snfg ID',
            'na_number' => 'Na Number',
            'odoo_code' => 'Odoo Code',
            'sediaan' => 'Sediaan',
        ];
    }
}
