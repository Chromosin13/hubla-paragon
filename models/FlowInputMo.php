<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flow_input_mo".
 *
 * @property string $nomo
 * @property string $posisi
 * @property integer $lanjutan
 * @property string $datetime_start
 * @property string $datetime_stop
 * @property string $datetime_write
 * @property string $nama_operator
 * @property string $nama_line
 * @property string $jenis_penimbangan
 * @property integer $shift_plan_start_olah
 * @property integer $shift_plan_end_olah
 * @property string $plan_start_olah
 * @property string $plan_end_olah
 * @property string $turun_bulk_start
 * @property string $turun_bulk_stop
 * @property string $adjust_start
 * @property string $adjust_stop
 * @property string $ember_start
 * @property string $ember_stop
 * @property string $nobatch
 * @property integer $is_done
 * @property integer $id
 */
class FlowInputMo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public function getDurasiMenit(){
        $datetime1 = strtotime($this->datetime_start);
        $datetime2 = strtotime($this->datetime_stop);

        $secs = $datetime2 - $datetime1;// == <seconds between the two times>
        $minutes = $secs / 60;

        return $minutes;

    }


    public function getDurasiMenitbersih(){
        $datetime1 = strtotime($this->datetime_start);
        $datetime2 = strtotime($this->datetime_stop);

        $secs = $datetime2 - $datetime1;// == <seconds between the two times>
        $minutes = $secs / 60;

        $sql="
        
        SELECT 
            sum(coalesce(EXTRACT(EPOCH FROM waktu_stop-waktu_start)/60::integer,0)) as id 
        FROM downtime
        WHERE flow_input_mo_id = ".$this->id;

        $row = FlowInputMo::findBySql($sql)->one();

        $minutes_bersih = $minutes-$row->id; 


        return $minutes_bersih;

    }

    public function getDowntimeFilled(){
        

        $sql="
        
        SELECT 
            count(*) as id
        FROM downtime
        WHERE flow_input_mo_id = ".$this->id;

        $row = FlowInputMo::findBySql($sql)->one();

        if(empty($row->id)){
            return 'False';
        }else{
            return 'True';
        }

    }

    public static function tableName()
    {
        return 'flow_input_mo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'posisi', 'nama_operator', 'nama_line', 'jenis_penimbangan', 'no_formula', 'nobatch'], 'string'],
            [['lanjutan', 'shift_plan_start_olah', 'shift_plan_end_olah', 'is_done', 'no_revisi', 'siklus','is_split_line'], 'integer'],
            [['datetime_start', 'datetime_stop', 'datetime_write', 'plan_start_olah', 'plan_end_olah', 'turun_bulk_start', 'turun_bulk_stop', 'adjust_start', 'adjust_stop', 'ember_start', 'ember_stop','staging'], 'safe'],
            [['jenis_penimbangan','nama_line','lanjutan'], 'required'],
            [['switch_line'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nomo' => 'Nomo',
            'posisi' => 'Posisi',
            'lanjutan' => 'Lanjutan',
            'datetime_start' => 'Datetime Start',
            'datetime_stop' => 'Datetime Stop',
            'datetime_write' => 'Datetime Write',
            'nama_operator' => 'Nama Operator',
            'nama_line' => 'Nama Line',
            'jenis_penimbangan' => 'Jenis Penimbangan',
            'shift_plan_start_olah' => 'Shift Plan Start Olah',
            'shift_plan_end_olah' => 'Shift Plan End Olah',
            'plan_start_olah' => 'Plan Start Olah',
            'plan_end_olah' => 'Plan End Olah',
            'turun_bulk_start' => 'Turun Bulk Start',
            'turun_bulk_stop' => 'Turun Bulk Stop',
            'adjust_start' => 'Adjust Start',
            'adjust_stop' => 'Adjust Stop',
            'ember_start' => 'Ember Start',
            'ember_stop' => 'Ember Stop',
            'is_done' => 'Is Done',
            'id' => 'ID',
            'durasiMenit' => 'Gross Leadtime',
            'durasiMenitbersih' => 'Net Leadtime',
            'downtimeFilled' => 'Downtime?',
            'staging' => 'Staging',
            'no_formula' => 'No Formula',
            'no_revisi' => 'No Revisi',
            'siklus' => 'Siklus',
            'nobatch' => 'Nobatch',
        ];
    }
}
