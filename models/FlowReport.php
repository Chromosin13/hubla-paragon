<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flow_report".
 *
 * @property integer $id
 * @property string $nosmb
 * @property string $nobatch
 * @property string $snfg
 * @property string $nadagang
 * @property string $process_position
 */
class FlowReport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flow_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nosmb', 'nobatch', 'snfg', 'nadagang', 'process_position'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nosmb' => 'Nosmb',
            'nobatch' => 'Nobatch',
            'snfg' => 'Snfg',
            'nadagang' => 'Nadagang',
            'process_position' => 'Process Position',
        ];
    }
}
