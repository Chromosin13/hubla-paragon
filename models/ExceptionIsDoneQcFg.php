<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "exception_is_done_qc_fg".
 *
 * @property integer $id
 * @property string $keterangan
 * @property string $timestamp
 * @property string $pic
 * @property string $snfg
 */
class ExceptionIsDoneQcFg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exception_is_done_qc_fg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keterangan', 'pic', 'snfg'], 'string'],
            [['timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keterangan' => 'Keterangan',
            'timestamp' => 'Timestamp',
            'pic' => 'Pic',
            'snfg' => 'Snfg',
        ];
    }
}
