<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DashboardLeadtimePengolahan;
use kartik\daterange\DateRangeBehavior;

/**
 * DashboardLeadtimePengolahanSearch represents the model behind the search form of `app\models\DashboardLeadtimePengolahan`.
 */
class DashboardLeadtimePengolahanSearch extends DashboardLeadtimePengolahan
{
    /**
     * @inheritdoc
     */

    public $createTimeRange;
    public $createTimeStart;
    public $createTimeEnd;


    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'upload_date',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
            ]
        ];
    }


    public function rules()
    {
        return [
            [['nama_fg', 'nomo', 'posisi', 'start', 'stop', 'sum_leadtime_raw', 'sum_leadtime_bruto', 'jenis_proses', 'sediaan', 'sum_downtime', 'sum_adjust', 'sum_leadtime_net', 'keterangan_downtime', 'keterangan_adjust', 'snfg', 'snfg_komponen','upload_date','besar_batch_real','nama_line','status','time_release'], 'safe'],
            [['id','week'], 'integer'],
            [['upload_date'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DashboardLeadtimePengolahan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'start' => $this->start,
            'stop' => $this->stop,
            //'upload_date' => $this->upload_date,
            'id' => $this->id,
            'week' => $this->week,
            'besar_batch_real' => $this->besar_batch_real,
        ]);

        // Set Date Variable Start Stop
        $start = date("Y-m-d", $this->createTimeStart);
        $stop = date("Y-m-d", $this->createTimeEnd);

        $query->andFilterWhere(['ilike', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['ilike', 'nomo', $this->nomo])
            ->andFilterWhere(['ilike', 'posisi', $this->posisi])
            ->andFilterWhere(['ilike', 'sum_leadtime_raw', $this->sum_leadtime_raw])
            ->andFilterWhere(['ilike', 'sum_leadtime_bruto', $this->sum_leadtime_bruto])
            ->andFilterWhere(['ilike', 'jenis_proses', $this->jenis_proses])
            ->andFilterWhere(['ilike', 'sediaan', $this->sediaan])
            ->andFilterWhere(['ilike', 'sum_downtime', $this->sum_downtime])
            ->andFilterWhere(['ilike', 'sum_adjust', $this->sum_adjust])
            ->andFilterWhere(['ilike', 'sum_leadtime_net', $this->sum_leadtime_net])
            ->andFilterWhere(['ilike', 'keterangan_downtime', $this->keterangan_downtime])
            ->andFilterWhere(['ilike', 'keterangan_adjust', $this->keterangan_adjust])
            ->andFilterWhere(['ilike', 'snfg', $this->snfg])
            ->andFilterWhere(['ilike', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['ilike', 'nama_line', $this->nama_line])
            // ->andFilterWhere(['>=', 'upload_date', $start])
            //   ->andFilterWhere(['<=', 'upload_date', $stop]);
            ->andWhere(['between', 'upload_date', $start, $stop]);


        return $dataProvider;
    }
}
