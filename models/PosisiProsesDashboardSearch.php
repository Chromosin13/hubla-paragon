<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PosisiProsesDashboard;

/**
 * PosisiProsesDashboardSearch represents the model behind the search form of `app\models\PosisiProsesDashboard`.
 */
class PosisiProsesDashboardSearch extends PosisiProsesDashboard
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'snfg', 'snfg_komponen', 'posisi', 'jenis_proses', 'datetime_write', 'start', 'due', 'nama_fg', 'process_status', 'is_done','nobatch'], 'safe'],
            [['lanjutan'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PosisiProsesDashboard::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lanjutan' => $this->lanjutan,
            'datetime_write' => $this->datetime_write,
            'start' => $this->start,
            'due' => $this->due,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'posisi', $this->posisi])
            ->andFilterWhere(['like', 'jenis_proses', $this->jenis_proses])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'process_status', $this->process_status])
            ->andFilterWhere(['like', 'is_done', $this->is_done])
            ->andFilterWhere(['like', 'nobatch', $this->nobatch]);

        return $dataProvider;
    }
}
