<?php

namespace app\models;

use Yii;
use DateTime;

/**
 * This is the model class for table "pengolahan".
 *
 * @property integer $id
 * @property string $snfg
 * @property string $nobatch
 * @property string $nama_line
 * @property string $jumlah_operator
 * @property string $nama_operator
 * @property string $operator_2
 * @property string $waktu
 * @property string $state
 * @property string $lanjutan
 */

    

class Pengolahan extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengolahan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'nobatch', 'nama_line', 'nama_operator', 'operator_2', 'state','jenis_olah','nomo','snfg_komponen','edit_nama'], 'string'],
            [['jumlah_operator', 'lanjutan','besar_batch_real','lanjutan_ist','is_done','palet_ke','shift_plan_start_olah','shift_plan_end_olah'], 'number'],
            [['waktu','timestamp','plan_start_olah','plan_end_olah','turun_bulk_start','turun_bulk_stop','ember_start','ember_stop','adjust_start','adjust_stop','edit_time'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'nobatch' => 'Nobatch',
            'nama_line' => 'Nama Line',
            'jumlah_operator' => 'Jumlah Operator',
            'nama_operator' => 'Nama Operator',
            'operator_2' => 'Operator 2',
            'waktu' => 'Waktu',
            'state' => 'State',
            'jenis_olah' => 'Jenis Olah',
            'lanjutan' => 'Lanjutan',
            'besar_batch_real' => 'Besar Batch Real',
            'snfg_komponen' => 'Snfg Komponen',
            'timestamp' => 'Timestamp',
            'lanjutan_ist' => 'Lanjutan Istirahat',
            'plan_end_olah' => 'Plan End Olah',
            'plan_start_olah' => 'Plan Start Olah',
            'is_done' => 'Is Done',
            'nomo' => 'Manufacturing Number',
            'palet_ke' => 'Palet Ke',
            'shift_plan_end_olah' => 'Shift Plan End Olah',
            'shift_plan_start_olah' => 'Shift Plan Start Olah',
            'turun_bulk_start' => 'Start Turun Bulk',
            'turun_bulk_stop' => 'Stop Turun Bulk',
            'ember_start' => 'Waktu Awal Pengemberan',
            'ember_stop' => 'Waktu Akhir Pengemberan',
            'adjust_start' => 'Waktu Awal Adjust',
            'adjust_stop' => 'Waktu Akhir Adjust',
        ];
    }

    public function getPengolahanItems()
    {
        return $this->hasMany(PengolahanItem::className(), ['pengolahan_id' => 'id']);
    }

    public function getKendalas()
    {
        return $this->hasMany(Kendala::className(), ['pengolahan_id' => 'id']);
    }

    public function getFormatwaktu()
    {
        return DateTime::createFromFormat('Y-m-d H:i:s.u', $this->waktu)->format('Y-m-d H:i:s');
    }

}
