<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "press".
 *
 * @property integer $id
 * @property string $snfg
 * @property string $jumlah_plan
 * @property string $jumlah_realisasi
 * @property string $nama_line
 * @property string $jumlah_operator
 * @property string $operator_1
 * @property string $operator_2
 * @property string $waktu
 * @property string $state
 * @property integer $lanjutan
 */
class Press extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'press';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'nama_line', 'state'], 'string'],
            [['jumlah_plan', 'jumlah_realisasi', 'jumlah_operator', 'operator_1', 'operator_2'], 'number'],
            [['waktu'], 'safe'],
            [['lanjutan'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'jumlah_plan' => 'Jumlah Plan',
            'jumlah_realisasi' => 'Jumlah Realisasi',
            'nama_line' => 'Nama Line',
            'jumlah_operator' => 'Jumlah Operator',
            'operator_1' => 'Operator 1',
            'operator_2' => 'Operator 2',
            'waktu' => 'Waktu',
            'state' => 'State',
            'lanjutan' => 'Lanjutan',
        ];
    }
}
