<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alokasi_kirim".
 *
 * @property integer $id
 * @property string $tujuan
 * @property string $nama
 */
class AlokasiKirim extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'alokasi_kirim';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tujuan', 'nama'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tujuan' => 'Tujuan',
            'nama' => 'Nama',
        ];
    }
}
