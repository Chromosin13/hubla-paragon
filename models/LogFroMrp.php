<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_fro_mrp".
 *
 * @property integer $id
 * @property string $nomo
 * @property string $no_smb
 * @property string $status
 * @property string $status_jadwal_odoo
 * @property string $last_updated
 */
class LogFroMrp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_fro_mrp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'no_smb', 'status', 'status_jadwal_odoo'], 'string'],
            [['last_updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomo' => 'Nomo',
            'no_smb' => 'No Smb',
            'status' => 'Status',
            'status_jadwal_odoo' => 'Status Jadwal Odoo',
            'last_updated' => 'Last Updated',
        ];
    }
}
