<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monitoring_line_kemas".
 *
 * @property string $sediaan
 * @property string $nama_line
 * @property string $nama_fg
 * @property string $nojadwal
 * @property string $datetime_write
 * @property string $jenis_proses
 * @property string $status
 * @property integer $id
 */
class MonitoringLineKemas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'monitoring_line_kemas';
    }


    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sediaan', 'nama_line', 'nama_fg', 'nojadwal', 'jenis_proses', 'status'], 'string'],
            [['datetime_write'], 'safe'],
            [['id','current_jadwal_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sediaan' => 'Sediaan',
            'nama_line' => 'Nama Line',
            'nama_fg' => 'Nama Fg',
            'nojadwal' => 'Nojadwal',
            'datetime_write' => 'Datetime Write',
            'jenis_proses' => 'Jenis Proses',
            'status' => 'Status',
            'id' => 'ID',
            'current_jadwal_id' => 'Current Jadwal ID',
        ];
    }
}
