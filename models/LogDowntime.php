<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_downtime".
 *
 * @property string $snfg
 * @property string $snfg_komponen
 * @property string $nomo
 * @property string $jenis_proses
 * @property integer $lanjutan
 * @property string $interval
 * @property double $interval_dec
 * @property string $waktu_start
 * @property string $waktu_stop
 * @property string $jenis
 * @property string $keterangan
 * @property integer $uid
 */
class LogDowntime extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_downtime';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'snfg_komponen', 'nomo', 'jenis_proses', 'interval', 'jenis', 'keterangan'], 'string'],
            [['lanjutan', 'uid'], 'integer'],
            [['interval_dec'], 'number'],
            [['waktu_start', 'waktu_stop'], 'safe'],
        ];
    }

    public static function primaryKey()
    {
        return ['uid'];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'nomo' => 'Nomo',
            'jenis_proses' => 'Jenis Proses',
            'lanjutan' => 'Lanjutan',
            'interval' => 'Interval',
            'interval_dec' => 'Interval Dec',
            'waktu_start' => 'Waktu Start',
            'waktu_stop' => 'Waktu Stop',
            'jenis' => 'Jenis',
            'keterangan' => 'Keterangan',
            'uid' => 'Uid',
        ];
    }
}
