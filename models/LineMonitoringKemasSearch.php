<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LineMonitoringKemas;

/**
 * LineMonitoringKemasSearch represents the model behind the search form of `app\models\LineMonitoringKemas`.
 */
class LineMonitoringKemasSearch extends LineMonitoringKemas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proses', 'nama_line', 'nojadwal', 'waktu', 'state', 'jenis_proses', 'tipe_jadwal'], 'safe'],
            [['lanjutan'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LineMonitoringKemas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'waktu' => $this->waktu,
            'lanjutan' => $this->lanjutan,
        ]);

        $query->andFilterWhere(['like', 'proses', $this->proses])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nojadwal', $this->nojadwal])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'jenis_proses', $this->jenis_proses])
            ->andFilterWhere(['like', 'tipe_jadwal', $this->tipe_jadwal]);

        return $dataProvider;
    }
}
