<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "iot_dashboard_weigher".
 *
 * @property string $snfg
 * @property string $nama_line
 * @property string $nama_operator
 * @property string $nama_operator_real
 * @property string $jenis_kemas
 * @property string $nobatch
 * @property integer $is_done
 * @property integer $is_done_wo
 * @property string $write_time
 * @property integer $flow_input_snfg_id
 * @property integer $jumlah_wo
 * @property integer $pass
 * @property integer $less
 * @property integer $over
 * @property integer $rft
 * @property string $error_rate
 * @property string $success_rate
 * @property integer $uid
 */
class IotDashboardWeigher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'iot_dashboard_weigher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'nama_line', 'nama_operator', 'nama_operator_real', 'jenis_kemas', 'nobatch'], 'string'],
            [['is_done', 'is_done_wo', 'flow_input_snfg_id', 'jumlah_wo', 'pass', 'less', 'over', 'rft', 'uid'], 'integer'],
            [['write_time'], 'safe'],
            [['error_rate', 'success_rate'], 'number'],
        ];
    }

    public static function primaryKey()
    {
        return ['uid'];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'snfg' => 'Snfg',
            'nama_line' => 'Nama Line',
            'nama_operator' => 'Nama Operator',
            'nama_operator_real' => 'Nama Operator Real',
            'jenis_kemas' => 'Jenis Kemas',
            'nobatch' => 'Nobatch',
            'is_done' => 'Is Done',
            'is_done_wo' => 'Is Done Wo',
            'write_time' => 'Write Time',
            'flow_input_snfg_id' => 'Flow Input Snfg ID',
            'jumlah_wo' => 'Jumlah Wo',
            'pass' => 'Pass',
            'less' => 'Less',
            'over' => 'Over',
            'rft' => 'Rft',
            'error_rate' => 'Error Rate',
            'success_rate' => 'Success Rate',
            'uid' => 'Uid',
        ];
    }
}
