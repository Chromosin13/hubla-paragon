<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "list_proses".
 *
 * @property integer $id
 * @property string $jenis
 * @property string $proses
 * @property string $posisi
 */
class ListProses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $ks_id;
    public $kode;

    public static function tableName()
    {
        return 'list_proses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis', 'proses', 'posisi'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis' => 'Jenis',
            'proses' => 'Proses',
            'posisi' => 'Posisi',
        ];
    }
}
