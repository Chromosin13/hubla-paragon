<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inventory_label_print".
 *
 * @property integer $id
 * @property string $device_code
 * @property string $zpl
 * @property string $status
 */
class InventoryLabelPrint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inventory_label_print';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['device_code', 'zpl', 'status'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_code' => 'Device Code',
            'zpl' => 'Zpl',
            'status' => 'Status',
        ];
    }
}
