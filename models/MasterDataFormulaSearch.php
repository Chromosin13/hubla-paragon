<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MasterDataFormula;

/**
 * MasterDataFormulaSearch represents the model behind the search form about `app\models\MasterDataFormula`.
 */
class MasterDataFormulaSearch extends MasterDataFormula
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'jml_revisi'], 'integer'],
            [['no_formula', 'koitem_fg', 'naitem_fg', 'kode_bulk', 'nama_bulk'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterDataFormula::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'jml_revisi' => $this->jml_revisi,
        ]);

        $query->andFilterWhere(['like', 'no_formula', $this->no_formula])
            ->andFilterWhere(['like', 'koitem_fg', $this->koitem_fg])
            ->andFilterWhere(['like', 'naitem_fg', $this->naitem_fg])
            ->andFilterWhere(['like', 'kode_bulk', $this->kode_bulk])
            ->andFilterWhere(['like', 'nama_bulk', $this->nama_bulk]);

        return $dataProvider;
    }
}
