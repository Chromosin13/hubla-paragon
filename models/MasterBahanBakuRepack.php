<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_bahan_baku_repack".
 *
 * @property integer $id
 * @property string $kode_internal
 * @property string $nama_bb
 * @property string $netto_repack
 * @property string $galat
 */
class MasterBahanBakuRepack extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_bahan_baku_repack';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_internal', 'nama_bb'], 'string'],
            [['netto_repack', 'galat'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode_internal' => 'Kode Internal',
            'nama_bb' => 'Nama Bahan Baku',
            'netto_repack' => 'Netto Repack (kg)',
            'galat' => 'Galat (kg)',
        ];
    }
}
