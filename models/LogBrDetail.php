<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_br_detail".
 *
 * @property integer $id
 * @property string $kode_bulk
 * @property string $formula_reference
 * @property string $qty_batch
 * @property string $status_formula
 * @property string $brand
 * @property string $no_dokumen
 * @property string $no_revisi
 * @property string $sediaan
 * @property string $koitem_bulk
 * @property string $status_jurnal
 * @property integer $is_lock
 */
class LogBrDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_br_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_bulk', 'formula_reference', 'status_formula', 'brand', 'no_dokumen', 'no_revisi', 'sediaan', 'status_jurnal','nama_bulk','no_smb','bentuk_sediaan'], 'string'],
            [['qty_batch'], 'number'],
            [['is_lock','is_can_send'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode_bulk' => 'Kode Bulk',
            'formula_reference' => 'Formula Reference',
            'qty_batch' => 'Qty Batch',
            'status_formula' => 'Status Formula',
            'brand' => 'Brand',
            'no_dokumen' => 'No Dokumen',
            'no_revisi' => 'No Revisi',
            'sediaan' => 'Sediaan',
            'status_jurnal' => 'Status Jurnal',
            'is_lock' => 'Is Lock',
            'bentuk_sediaan' => 'Bentuk Sediaan',
        ];
    }
}
