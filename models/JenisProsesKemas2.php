<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_proses_kemas2".
 *
 * @property integer $id
 * @property integer $is_print
 * @property string $jenis_proses_kemas2
 */
class JenisProsesKemas2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_proses_kemas2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_proses_kemas2'], 'string'],
            [['is_print'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_proses_kemas2' => 'Jenis Proses Kemas2',
            'is_print' => 'Is Print',
        ];
    }
}
