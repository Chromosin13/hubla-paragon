<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MasterDataNetto;

/**
 * MasterDataNettoSearch represents the model behind the search form about `app\models\MasterDataNetto`.
 */
class MasterDataNettoSearch extends MasterDataNetto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'isi_koli'], 'integer'],
            [['koitem', 'nama_item', 'timestamp'], 'safe'],
            [['netto_min', 'netto_max'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterDataNetto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'isi_koli' => $this->isi_koli,
            'netto_min' => $this->netto_min,
            'netto_max' => $this->netto_max,
            'timestamp' => $this->timestamp,
        ]);

        $query->andFilterWhere(['like', 'koitem', $this->koitem])
            ->andFilterWhere(['like', 'nama_item', $this->nama_item]);

        return $dataProvider;
    }
}
