<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogReceiveNdc;

/**
 * LogReceiveNdcSearch represents the model behind the search form about `app\models\LogReceiveNdc`.
 */
class LogReceiveNdcSearch extends LogReceiveNdc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nourut'], 'integer'],
            [['nosj', 'id_palet', 'snfg', 'nobatch', 'nama_fg', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogReceiveNdc::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'nourut' => $this->nourut,
        ]);

        $query->andFilterWhere(['like', 'nosj', $this->nosj])
            ->andFilterWhere(['like', 'id_palet', $this->id_palet])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'nobatch', $this->nobatch])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
