<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "achievement_kemas2".
 *
 * @property integer $row_number
 * @property string $nomo
 * @property string $snfg
 * @property string $snfg_komponen
 * @property string $nama_fg
 * @property string $tanggal_stop
 * @property string $tanggal_start
 * @property string $nama_line
 * @property string $sediaan
 * @property string $jenis_kemas
 * @property string $leadtime
 * @property string $std_leadtime
 * @property string $status_leadtime
 * @property string $achievement_rate
 * @property string $koitem_bulk
 * @property string $koitem_fg
 * @property string $nama_bulk
 */
class AchievementKemas2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    public static function primaryKey()
    {
        return ['row_number'];
    }
    public static function tableName()
    {
        return 'achievement_kemas2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row_number'], 'integer'],
            [['nomo', 'snfg', 'snfg_komponen', 'nama_fg', 'nama_line', 'sediaan', 'jenis_kemas', 'status_leadtime', 'koitem_bulk', 'koitem_fg', 'nama_bulk'], 'string'],
            [['tanggal_stop', 'tanggal_start'], 'safe'],
            [['leadtime', 'std_leadtime', 'achievement_rate'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'row_number' => 'Row Number',
            'nomo' => 'Nomo',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'nama_fg' => 'Nama Fg',
            'tanggal_stop' => 'Tanggal Stop',
            'tanggal_start' => 'Tanggal Start',
            'nama_line' => 'Nama Line',
            'sediaan' => 'Sediaan',
            'jenis_kemas' => 'Jenis Kemas',
            'leadtime' => 'Leadtime',
            'std_leadtime' => 'Std Leadtime',
            'status_leadtime' => 'Status Leadtime',
            'achievement_rate' => 'Achievement Rate',
            'koitem_bulk' => 'Koitem Bulk',
            'koitem_fg' => 'Koitem Fg',
            'nama_bulk' => 'Nama Bulk',
        ];
    }
}
