<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BillOfMaterials;

/**
 * BillOfMaterialsSearch represents the model behind the search form about `app\models\BillOfMaterials`.
 */
class BillOfMaterialsSearch extends BillOfMaterials
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'no_revisi', 'siklus', 'cycle_time', 'operator'], 'integer'],
            [['no_formula', 'koitem_fg', 'kode_bb', 'kode_olah', 'nama_bb', 'satuan', 'sediaan', 'timbangan', 'no_timbangan'], 'safe'],
            [['weight'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BillOfMaterials::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'no_revisi' => $this->no_revisi,
            'siklus' => $this->siklus,
            'weight' => $this->weight,
            'cycle_time' => $this->cycle_time,
            'operator' => $this->operator,
        ]);

        $query->andFilterWhere(['like', 'no_formula', $this->no_formula])
            ->andFilterWhere(['like', 'koitem_fg', $this->koitem_fg])
            ->andFilterWhere(['like', 'kode_bb', $this->kode_bb])
            ->andFilterWhere(['like', 'kode_olah', $this->kode_olah])
            ->andFilterWhere(['like', 'nama_bb', $this->nama_bb])
            ->andFilterWhere(['like', 'satuan', $this->satuan])
            ->andFilterWhere(['like', 'sediaan', $this->sediaan])
            ->andFilterWhere(['like', 'timbangan', $this->timbangan])
            ->andFilterWhere(['like', 'no_timbangan', $this->no_timbangan]);

        return $dataProvider;
    }
}
