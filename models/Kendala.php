<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kendala".
 *
 * @property integer $id
 * @property string $keterangan
 * @property string $start
 * @property string $stop
 * @property integer $penimbangan_id
 * @property integer $pengolahan_id
 * @property integer $qc_bulk_id
 * @property integer $kemas_1_id
 * @property integer $kemas_2_id
 * @property integer $qc_fg_id
 *
 * @property Kemas1 $kemas1
 * @property Kemas2 $kemas2
 * @property Pengolahan $pengolahan
 * @property Penimbangan $penimbangan
 * @property QcBulk $qcBulk
 * @property QcFg $qcFg
 */
class Kendala extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kendala';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keterangan'], 'string'],
            [['exception'], 'string'],
            [['start', 'stop'], 'safe'],
            [['penimbangan_id', 'pengolahan_id', 'qc_bulk_id', 'kemas_1_id', 'kemas_2_id', 'qc_fg_id'], 'integer'],
            [['kemas_1_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kemas1::className(), 'targetAttribute' => ['kemas_1_id' => 'id']],
            [['kemas_2_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kemas2::className(), 'targetAttribute' => ['kemas_2_id' => 'id']],
            [['pengolahan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pengolahan::className(), 'targetAttribute' => ['pengolahan_id' => 'id']],
            [['penimbangan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Penimbangan::className(), 'targetAttribute' => ['penimbangan_id' => 'id']],
            [['qc_bulk_id'], 'exist', 'skipOnError' => true, 'targetClass' => QcBulk::className(), 'targetAttribute' => ['qc_bulk_id' => 'id']],
            [['qc_fg_id'], 'exist', 'skipOnError' => true, 'targetClass' => QcFg::className(), 'targetAttribute' => ['qc_fg_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keterangan' => 'Keterangan',
            'start' => 'Durasi',
            'stop' => 'Stop',
            'penimbangan_id' => 'Penimbangan ID',
            'pengolahan_id' => 'Pengolahan ID',
            'qc_bulk_id' => 'Qc Bulk ID',
            'kemas_1_id' => 'Kemas 1 ID',
            'kemas_2_id' => 'Kemas 2 ID',
            'qc_fg_id' => 'Qc Fg ID',
            'exception' => 'Exception',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKemas1()
    {
        return $this->hasOne(Kemas1::className(), ['id' => 'kemas_1_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKemas2()
    {
        return $this->hasOne(Kemas2::className(), ['id' => 'kemas_2_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengolahan()
    {
        return $this->hasOne(Pengolahan::className(), ['id' => 'pengolahan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenimbangan()
    {
        return $this->hasOne(Penimbangan::className(), ['id' => 'penimbangan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQcBulk()
    {
        return $this->hasOne(QcBulk::className(), ['id' => 'qc_bulk_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQcFg()
    {
        return $this->hasOne(QcFg::className(), ['id' => 'qc_fg_id']);
    }
}
