<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogApprovalBr;

/**
 * LogApprovalBrSearch represents the model behind the search form about `app\models\LogApprovalBr`.
 */
class LogApprovalBrSearch extends LogApprovalBr
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nomo', 'creator', 'status_creator', 'timestamp_creator', 'reviewer', 'status_reviewer', 'timestamp_reviewer', 'approver', 'status_approver', 'timestamp_approver', 'status', 'kode_bulk', 'formula_reference', 'current_pic', 'current_status', 'reject_note_reviewer', 'reject_note_approver','nama_bulk'], 'safe'],
            [['qty_batch'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogApprovalBr::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'timestamp_creator' => $this->timestamp_creator,
            'timestamp_reviewer' => $this->timestamp_reviewer,
            'timestamp_approver' => $this->timestamp_approver,
            'qty_batch' => $this->qty_batch,
        ]);

        $query->andFilterWhere(['ilike', 'log_approval_br.nomo', $this->nomo])
            ->andFilterWhere(['ilike', 'log_approval_br.nama_bulk', $this->nama_bulk])
            ->andFilterWhere(['ilike', 'log_approval_br.creator', $this->creator])
            ->andFilterWhere(['ilike', 'log_approval_br.status_creator', $this->status_creator])
            ->andFilterWhere(['ilike', 'log_approval_br.reviewer', $this->reviewer])
            ->andFilterWhere(['ilike', 'log_approval_br.status_reviewer', $this->status_reviewer])
            ->andFilterWhere(['ilike', 'log_approval_br.approver', $this->approver])
            ->andFilterWhere(['ilike', 'log_approval_br.status_approver', $this->status_approver])
            ->andFilterWhere(['ilike', 'log_approval_br.status', $this->status])
            ->andFilterWhere(['ilike', 'log_approval_br.kode_bulk', $this->kode_bulk])
            ->andFilterWhere(['ilike', 'log_approval_br.formula_reference', $this->formula_reference])
            ->andFilterWhere(['ilike', 'log_approval_br.current_pic', $this->current_pic])
            ->andFilterWhere(['ilike', 'log_approval_br.current_status', $this->current_status])
            ->andFilterWhere(['ilike', 'log_approval_br.reject_note_reviewer', $this->reject_note_reviewer])
            ->andFilterWhere(['ilike', 'log_approval_br.reject_note_approver', $this->reject_note_approver]);

        return $dataProvider;
    }
}
