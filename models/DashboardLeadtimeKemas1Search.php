<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DashboardLeadtimeKemas1;
use kartik\daterange\DateRangeBehavior;

/**
 * DashboardLeadtimeKemas1Search represents the model behind the search form about `app\models\DashboardLeadtimeKemas1`.
 */
class DashboardLeadtimeKemas1Search extends DashboardLeadtimeKemas1
{
    /**
     * @inheritdoc
     */


    public $createTimeRange;
    public $createTimeStart;
    public $createTimeEnd;

    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'upload_date',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
            ]
        ];
    }

    public function rules()
    {
        return [
            [['nama_fg', 'snfg_komponen', 'posisi', 'nama_line', 'start', 'stop', 'sum_leadtime_raw', 'sum_leadtime_bruto', 'jenis_proses', 'sediaan', 'sum_downtime', 'sum_adjust', 'sum_leadtime_net', 'keterangan_downtime', 'keterangan_adjust', 'snfg', 'nomo', 'upload_date'], 'safe'],
            [['id', 'week'], 'integer'],
            [['upload_date'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }



    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DashboardLeadtimeKemas1::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'start' => $this->start,
            'stop' => $this->stop,
            'id' => $this->id,
            // 'upload_date' => $this->upload_date,
            'week' => $this->week,
        ]);

        // Set Date Variable Start Stop
        $start = date("Y-m-d", $this->createTimeStart);
        $stop = date("Y-m-d", $this->createTimeEnd);

        $query->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'posisi', $this->posisi])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'sum_leadtime_raw', $this->sum_leadtime_raw])
            ->andFilterWhere(['like', 'sum_leadtime_bruto', $this->sum_leadtime_bruto])
            ->andFilterWhere(['like', 'jenis_proses', $this->jenis_proses])
            ->andFilterWhere(['like', 'sediaan', $this->sediaan])
            ->andFilterWhere(['like', 'sum_downtime', $this->sum_downtime])
            ->andFilterWhere(['like', 'sum_adjust', $this->sum_adjust])
            ->andFilterWhere(['like', 'sum_leadtime_net', $this->sum_leadtime_net])
            ->andFilterWhere(['like', 'keterangan_downtime', $this->keterangan_downtime])
            ->andFilterWhere(['like', 'keterangan_adjust', $this->keterangan_adjust])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andWhere(['between', 'upload_date', $start, $stop]);

        return $dataProvider;
    }
}
