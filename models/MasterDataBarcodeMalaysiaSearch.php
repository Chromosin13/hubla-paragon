<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MasterDataBarcodeMalaysia;

/**
 * MasterDataBarcodeMalaysiaSearch represents the model behind the search form about `app\models\MasterDataBarcodeMalaysia`.
 */
class MasterDataBarcodeMalaysiaSearch extends MasterDataBarcodeMalaysia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['koitem_lama', 'koitem_baru', 'nama_fg', 'barcode_lama', 'barcode_harmonize'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterDataBarcodeMalaysia::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'koitem_lama', $this->koitem_lama])
            ->andFilterWhere(['like', 'koitem_baru', $this->koitem_baru])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'barcode_lama', $this->barcode_lama])
            ->andFilterWhere(['like', 'barcode_harmonize', $this->barcode_harmonize]);

        return $dataProvider;
    }
}
