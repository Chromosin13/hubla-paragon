<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sampling_label".
 *
 * @property integer $id
 * @property string $barcode
 * @property string $odoo_code
 * @property integer $qty
 * @property string $expired
 * @property string $nobatch
 */
class SamplingLabel extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sampling_label';
    }

    public $code;
    public $upload;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['upload'], 'file', 'maxFiles'=>5],
            [['barcode', 'odoo_code', 'nobatch','data','image','image_url'], 'string'],
            [['qty'], 'integer'],
            [['expired'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'barcode' => 'Barcode',
            'odoo_code' => 'Odoo Code',
            'qty' => 'Qty',
            'expired' => 'Expired',
            'nobatch' => 'Nobatch',
            'data' => 'Data QR',
        ];
    }
}
