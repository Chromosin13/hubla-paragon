<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AdminlogFg;

/**
 * AdminlogSearch represents the model behind the search form about `app\models\AdminlogFg`.
 */
class AdminlogFgSearch extends AdminlogFg
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'admin_id', 'status_print'], 'integer'],
            [['nopo', 'no_surjal', 'koitem_fg', 'nama_fg', 'supplier', 'datetime_start', 'datetime_stop', 'datetime_write', 'batch', 'tgl_plan_kirim', 'tgl_po', 'tgl_kirim'], 'safe'],
            [['qty_demand', 'price_unit', 'qty_kirim'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdminlogFg::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'datetime_start' => $this->datetime_start,
            'datetime_stop' => $this->datetime_stop,
            'datetime_write' => $this->datetime_write,
            'admin_id' => $this->admin_id,
            'status_print' => $this->status_print,
            'tgl_plan_kirim' => $this->tgl_plan_kirim,
            'qty_demand' => $this->qty_demand,
            'price_unit' => $this->price_unit,
            'qty_kirim' => $this->qty_kirim,
            'tgl_po' => $this->tgl_po,
            'tgl_kirim' => $this->tgl_kirim,
        ]);

        $query->andFilterWhere(['like', 'nopo', $this->nopo])
            ->andFilterWhere(['like', 'no_surjal', $this->no_surjal])
            ->andFilterWhere(['like', 'koitem_fg', $this->koitem_fg])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'supplier', $this->supplier])
            ->andFilterWhere(['like', 'batch', $this->batch]);

        return $dataProvider;
    }
}
