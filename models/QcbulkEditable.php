<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "qcbulk_editable".
 *
 * @property integer $id
 * @property string $nomo
 * @property string $snfg
 * @property string $snfg_komponen
 * @property string $ph
 * @property string $viskositas
 * @property string $berat_jenis
 * @property string $kadar
 * @property string $warna
 * @property string $bau
 * @property string $performance
 * @property string $bentuk
 * @property string $mikro
 * @property string $kejernihan
 * @property string $status
 * @property string $waktu
 * @property string $state
 * @property string $posisi
 * @property string $jenis_periksa
 * @property string $timestamp
 */
class QcbulkEditable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qcbulk_editable';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nomo', 'snfg', 'snfg_komponen', 'ph', 'viskositas', 'berat_jenis', 'kadar', 'warna', 'bau', 'performance', 'bentuk', 'mikro', 'kejernihan', 'status', 'state', 'posisi', 'jenis_periksa'], 'string'],
            [['waktu', 'timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomo' => 'Nomo',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'ph' => 'Ph',
            'viskositas' => 'Viskositas',
            'berat_jenis' => 'Berat Jenis',
            'kadar' => 'Kadar',
            'warna' => 'Warna',
            'bau' => 'Bau',
            'performance' => 'Performance',
            'bentuk' => 'Bentuk',
            'mikro' => 'Mikro',
            'kejernihan' => 'Kejernihan',
            'status' => 'Status',
            'waktu' => 'Waktu',
            'state' => 'State',
            'posisi' => 'Posisi',
            'jenis_periksa' => 'Jenis Periksa',
            'timestamp' => 'Timestamp',
        ];
    }
}
