<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posisi_pengecekan_fg".
 *
 * @property integer $id
 * @property string $snfg
 * @property string $palet
 * @property string $start
 * @property string $stop
 */
class PosisiPengecekanFg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posisi_pengecekan_fg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'palet'], 'string'],
            [['start', 'stop'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'palet' => 'Palet',
            'start' => 'Start',
            'stop' => 'Stop',
        ];
    }
}
