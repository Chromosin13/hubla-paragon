<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kemas_2".
 *
 * @property string $snfg
 * @property string $jumlah_plan
 * @property string $jumlah_realisasi
 * @property string $nama_line
 * @property string $nama_operator
 * @property string $waktu
 * @property string $state
 * @property string $posisi
 * @property integer $id
 */
class Kemas2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kemas_2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'nama_line', 'nama_operator', 'state','jenis_kemas','nobatch'], 'string'],
            [['jumlah_plan', 'jumlah_realisasi','lanjutan','jumlah_operator'], 'number'],
            [['waktu','timestamp'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'jumlah_plan' => 'Jumlah Plan',
            'jumlah_realisasi' => 'Jumlah Realisasi',
            'nama_line' => 'Nama Line',
            'nama_operator' => 'Nama Operator',
            'waktu' => 'Waktu',
            'state' => 'State',
            'jenis_kemas' => 'Jenis Kemas',
            'lanjutan' => 'Lanjutan',
            'jumlah_operator' => 'Jumlah Operator',
            'timestamp' => 'Timestamp',
            'nobatch' =>'No Batch',
        ];
    }
}
