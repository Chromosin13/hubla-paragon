<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ScmPlanner;

/**
 * ScmPlannerSearch represents the model behind the search form about `app\models\ScmPlanner`.
 */
class ScmPlannerSearch extends ScmPlanner
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['sediaan', 'streamline', 'line_timbang', 'line_olah', 'line_olah_2', 'line_press', 'line_kemas_1', 'line_kemas_2', 'start', 'due', 'kode_jadwal', 'snfg', 'koitem_bulk', 'koitem_fg', 'nama_bulk', 'nama_fg', 'tglpermintaan', 'keterangan', 'kategori_sop', 'kategori_detail','status','timestamp','snfg_komponen','nobatch','line_olah_premix','line_adjust_olah_1','line_adjust_olah_2','line_ayak','line_press_filling_packing','line_filling','line_filling_packing_inline','line_flame_packing_inline','line_packing','snfg_induk','kode_sl','alokasi'], 'safe'],
            [['leadtime', 'besar_batch', 'besar_lot', 'lot', 'jumlah','jumlah_press','week'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ScmPlanner::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'start' => $this->start,
            'due' => $this->due,
            'leadtime' => $this->leadtime,
            'besar_batch' => $this->besar_batch,
            'besar_lot' => $this->besar_lot,
            'lot' => $this->lot,
            'tglpermintaan' => $this->tglpermintaan,
            'jumlah' => $this->jumlah,
            'timestamp' => $this->timestamp,
            'week' => $this->week,
            'jumlah_press' => $this->jumlah_press,
        ]);
        $query->andFilterWhere(['like', 'sediaan', $this->sediaan])
            ->andFilterWhere(['like', 'streamline', $this->streamline])
            ->andFilterWhere(['like', 'line_timbang', $this->line_timbang])
            ->andFilterWhere(['like', 'line_olah', $this->line_olah])
            ->andFilterWhere(['like', 'line_olah_2', $this->line_olah_2])
            ->andFilterWhere(['like', 'line_press', $this->line_press])
            ->andFilterWhere(['like', 'line_kemas_1', $this->line_kemas_1])
            ->andFilterWhere(['like', 'line_kemas_2', $this->line_kemas_2])
            ->andFilterWhere(['like', 'kode_jadwal', $this->kode_jadwal])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'koitem_bulk', $this->koitem_bulk])
            ->andFilterWhere(['like', 'koitem_fg', $this->koitem_fg])
            ->andFilterWhere(['like', 'nama_bulk', $this->nama_bulk])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'kategori_sop', $this->kategori_sop])
            ->andFilterWhere(['like', 'kategori_detail', $this->kategori_detail])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'nobatch', $this->nobatch])
            ->andFilterWhere(['like', 'line_olah_premix', $this->line_olah_premix])
            ->andFilterWhere(['like', 'line_adjust_olah_1', $this->line_adjust_olah_1])
            ->andFilterWhere(['like', 'line_adjust_olah_2', $this->line_adjust_olah_2])
            ->andFilterWhere(['like', 'line_ayak', $this->line_ayak])
            ->andFilterWhere(['like', 'line_press_filling_packing', $this->line_press_filling_packing])
            ->andFilterWhere(['like', 'line_filling', $this->line_filling])
            ->andFilterWhere(['like', 'line_filling_packing_inline', $this->line_filling_packing_inline])
            ->andFilterWhere(['like', 'line_flame_packing_inline', $this->line_flame_packing_inline])
            ->andFilterWhere(['like', 'line_packing', $this->line_packing])
            ->andFilterWhere(['like', 'snfg_induk', $this->snfg_induk])
            ->andFilterWhere(['like', 'kode_sl', $this->kode_sl])
            ->andFilterWhere(['like', 'alokasi', $this->alokasi]);

        return $dataProvider;
    }
}
