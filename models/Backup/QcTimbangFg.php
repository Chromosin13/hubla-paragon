<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "qc_timbang_fg".
 *
 * @property integer $id
 * @property string $snfg
 * @property string $palet_ke
 * @property string $tanggal
 * @property string $nama_inspektor
 * @property string $waktu
 * @property string $state
 * @property string $qty
 * @property string $status
 * @property integer $periksa
 */
class QcTimbangFg extends \yii\db\ActiveRecord
{
    /*
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qc_timbang_fg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'nama_inspektor', 'state', 'status','nama_line','jenis_periksa'], 'string'],
            [['palet_ke', 'qty','jumlah_inspektor','lanjutan'], 'number'],
            [['tanggal', 'waktu'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'palet_ke' => 'Palet Ke-',
            'tanggal' => 'Tanggal',
            'nama_inspektor' => 'Nama Inspektor',
            'waktu' => 'Waktu',
            'state' => 'State',
            'qty' => 'Qty',
            'status' => 'Status',
            'jenis_periksa' => 'Jenis Periksa',
            'jumlah_inspektor' => 'Jumlah Inspektor',
            'nama_line' => 'Nama Line',
            'lanjutan' => 'Lanjutan',
        ];
    }
}
