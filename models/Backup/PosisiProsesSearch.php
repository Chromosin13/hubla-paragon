<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PosisiProses;

/**
 * PosisiProsesSearch represents the model behind the search form about `app\models\PosisiProses`.
 */
class PosisiProsesSearch extends PosisiProses
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'posisi', 'status', 'start', 'due','timestamp','ontime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PosisiProses::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'start' => $this->start,
            'due' => $this->due,
            'timestamp' => $this->timestamp,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'posisi', $this->posisi])
            ->andFilterWhere(['like', 'ontime', $this->ontime])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
