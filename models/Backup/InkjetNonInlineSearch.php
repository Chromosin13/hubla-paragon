<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\InkjetNonInline;

/**
 * InkjetNonInlineSearch represents the model behind the search form about `app\models\InkjetNonInline`.
 */
class InkjetNonInlineSearch extends InkjetNonInline
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['snfg', 'inkjet_sticker_bottom_nama_line', 'inkjet_sticker_bottom_operator_1', 'inkjet_sticker_bottom_operator_2', 'inkjet_sticker_bottom_operator_3', 'inkjet_sticker_bottom_waktu', 'inkjet_sticker_bottom_state', 'inkjet_dus_satuan_nama_line', 'inkjet_dus_satuan_operator_1', 'inkjet_dus_satuan_operator_2', 'inkjet_dus_satuan_operator_3', 'inkjet_dus_satuan_waktu', 'inkjet_dus_satuan_state', 'inkjet_dus_12_nama_line', 'inkjet_dus_12_operator_1', 'inkjet_dus_12_operator_2', 'inkjet_dus_12_operator_3', 'inkjet_dus_12_waktu', 'inkjet_dus_12_state', 'inkjet_packaging_primer_nama_line', 'inkjet_packaging_primer_operator_1', 'inkjet_packaging_primer_operator_2', 'inkjet_packaging_primer_operator_3', 'inkjet_packaging_primer_waktu', 'inkjet_packaging_primer_state'], 'safe'],
            [['inkjet_sticker_bottom_jumlah_plan', 'inkjet_sticker_bottom_jumlah_realisasi', 'inkjet_sticker_bottom_jumlah_operator', 'inkjet_dus_satuan_jumlah_operator', 'inkjet_dus_12_jumlah_plan', 'inkjet_dus_12_jumlah_realisasi', 'inkjet_dus_12_jumlah_operator', 'inkjet_packaging_primer_jumlah_plan', 'inkjet_packaging_primer_jumlah_realisasi', 'inkjet_packaging_primer_jumlah_operator', 'inkjet_dus_satuan_jumlah_plan', 'inkjet_dus_satuan_jumlah_realisasi'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InkjetNonInline::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'inkjet_sticker_bottom_jumlah_plan' => $this->inkjet_sticker_bottom_jumlah_plan,
            'inkjet_sticker_bottom_jumlah_realisasi' => $this->inkjet_sticker_bottom_jumlah_realisasi,
            'inkjet_sticker_bottom_jumlah_operator' => $this->inkjet_sticker_bottom_jumlah_operator,
            'inkjet_sticker_bottom_waktu' => $this->inkjet_sticker_bottom_waktu,
            'inkjet_dus_satuan_jumlah_operator' => $this->inkjet_dus_satuan_jumlah_operator,
            'inkjet_dus_satuan_waktu' => $this->inkjet_dus_satuan_waktu,
            'inkjet_dus_12_jumlah_plan' => $this->inkjet_dus_12_jumlah_plan,
            'inkjet_dus_12_jumlah_realisasi' => $this->inkjet_dus_12_jumlah_realisasi,
            'inkjet_dus_12_jumlah_operator' => $this->inkjet_dus_12_jumlah_operator,
            'inkjet_dus_12_waktu' => $this->inkjet_dus_12_waktu,
            'inkjet_packaging_primer_jumlah_plan' => $this->inkjet_packaging_primer_jumlah_plan,
            'inkjet_packaging_primer_jumlah_realisasi' => $this->inkjet_packaging_primer_jumlah_realisasi,
            'inkjet_packaging_primer_jumlah_operator' => $this->inkjet_packaging_primer_jumlah_operator,
            'inkjet_packaging_primer_waktu' => $this->inkjet_packaging_primer_waktu,
            'inkjet_dus_satuan_jumlah_plan' => $this->inkjet_dus_satuan_jumlah_plan,
            'inkjet_dus_satuan_jumlah_realisasi' => $this->inkjet_dus_satuan_jumlah_realisasi,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'inkjet_sticker_bottom_nama_line', $this->inkjet_sticker_bottom_nama_line])
            ->andFilterWhere(['like', 'inkjet_sticker_bottom_operator_1', $this->inkjet_sticker_bottom_operator_1])
            ->andFilterWhere(['like', 'inkjet_sticker_bottom_operator_2', $this->inkjet_sticker_bottom_operator_2])
            ->andFilterWhere(['like', 'inkjet_sticker_bottom_operator_3', $this->inkjet_sticker_bottom_operator_3])
            ->andFilterWhere(['like', 'inkjet_sticker_bottom_state', $this->inkjet_sticker_bottom_state])
            ->andFilterWhere(['like', 'inkjet_dus_satuan_nama_line', $this->inkjet_dus_satuan_nama_line])
            ->andFilterWhere(['like', 'inkjet_dus_satuan_operator_1', $this->inkjet_dus_satuan_operator_1])
            ->andFilterWhere(['like', 'inkjet_dus_satuan_operator_2', $this->inkjet_dus_satuan_operator_2])
            ->andFilterWhere(['like', 'inkjet_dus_satuan_operator_3', $this->inkjet_dus_satuan_operator_3])
            ->andFilterWhere(['like', 'inkjet_dus_satuan_state', $this->inkjet_dus_satuan_state])
            ->andFilterWhere(['like', 'inkjet_dus_12_nama_line', $this->inkjet_dus_12_nama_line])
            ->andFilterWhere(['like', 'inkjet_dus_12_operator_1', $this->inkjet_dus_12_operator_1])
            ->andFilterWhere(['like', 'inkjet_dus_12_operator_2', $this->inkjet_dus_12_operator_2])
            ->andFilterWhere(['like', 'inkjet_dus_12_operator_3', $this->inkjet_dus_12_operator_3])
            ->andFilterWhere(['like', 'inkjet_dus_12_state', $this->inkjet_dus_12_state])
            ->andFilterWhere(['like', 'inkjet_packaging_primer_nama_line', $this->inkjet_packaging_primer_nama_line])
            ->andFilterWhere(['like', 'inkjet_packaging_primer_operator_1', $this->inkjet_packaging_primer_operator_1])
            ->andFilterWhere(['like', 'inkjet_packaging_primer_operator_2', $this->inkjet_packaging_primer_operator_2])
            ->andFilterWhere(['like', 'inkjet_packaging_primer_operator_3', $this->inkjet_packaging_primer_operator_3])
            ->andFilterWhere(['like', 'inkjet_packaging_primer_state', $this->inkjet_packaging_primer_state]);

        return $dataProvider;
    }
}
