<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kemas1;

/**
 * Kemas1Search represents the model behind the search form about `app\models\Kemas1`.
 */
class Kemas1Search extends Kemas1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['snfg', 'nama_line', 'nama_operator', 'waktu', 'state','jenis_kemas','snfg_komponen','timestamp'], 'safe'],
            [['jumlah_plan', 'jumlah_realisasi','lanjutan','jumlah_operator'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kemas1::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'jumlah_plan' => $this->jumlah_plan,
            'jumlah_realisasi' => $this->jumlah_realisasi,
            'jumlah_operator' => $this->jumlah_operator,
            'lanjutan' => $this->lanjutan,
            'waktu' => $this->waktu,
            'timestamp' => $this->timestamp,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator])
            ->andFilterWhere(['like', 'jenis_kemas', $this->jenis_kemas])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'state', $this->state]);

        return $dataProvider;
    }
}
