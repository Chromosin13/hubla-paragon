<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inkjet".
 *
 * @property integer $id
 * @property string $jenis_inkjet
 * @property string $lanjutan
 * @property string $nama_line
 * @property string $jumlah_operator
 * @property string $jumlah_realisasi
 * @property string $nama_operator
 */
class Inkjet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inkjet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_inkjet', 'nama_line', 'nama_operator','snfg','state','posisi','keterangan','snfg_komponen'], 'string'],
            [['lanjutan', 'jumlah_operator', 'jumlah_realisasi','jumlah_reject'], 'number'],
            [['waktu','timestamp'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_inkjet' => 'Jenis Inkjet',
            'lanjutan' => 'Lanjutan',
            'nama_line' => 'Nama Line',
            'jumlah_operator' => 'Jumlah Operator',
            'jumlah_realisasi' => 'Jumlah Realisasi',
            'nama_operator' => 'Nama Operator',
            'snfg'=> 'SNFG',
            'state'=>'State',
            'posisi'=>'Posisi',
            'jumlah_reject' => 'Jumlah Reject',
            'timestamp' => 'Timestamp',
            'snfg_komponen' => 'SNFG Komponen',
            'keterangan' =>'Keterangan',
            'waktu' => 'Waktu',
        ];
    }
}
