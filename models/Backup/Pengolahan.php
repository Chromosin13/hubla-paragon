<?php

namespace app\models;

use Yii;
/**
 * This is the model class for table "pengolahan".
 *
 * @property integer $id
 * @property string $snfg
 * @property string $nobatch
 * @property string $nama_line
 * @property string $jumlah_operator
 * @property string $nama_operator
 * @property string $operator_2
 * @property string $waktu
 * @property string $state
 * @property string $lanjutan
 */
class Pengolahan extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengolahan';
    }

    public function getItems()
    {
        return $this->hasMany(Item::className(),['pengolahan_id'=>'id']);
    }
 
    public function setItems($value)
    {
        $this->loadRelated('items', $value);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'nobatch', 'nama_line', 'nama_operator', 'operator_2', 'state','jenis_olah','snfg_komponen'], 'string'],
            [['jumlah_operator', 'lanjutan','besar_batch_real'], 'number'],
            [['waktu','timestamp'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'nobatch' => 'Nobatch',
            'nama_line' => 'Nama Line',
            'jumlah_operator' => 'Jumlah Operator',
            'nama_operator' => 'Nama Operator',
            'operator_2' => 'Operator 2',
            'waktu' => 'Waktu',
            'state' => 'State',
            'jenis_olah' => 'Jenis Olah',
            'lanjutan' => 'Lanjutan',
            'besar_batch_real' => 'Besar Batch Real',
            'snfg_komponen' => 'Snfg Komponen',
            'timestamp' => 'Timestamp',
        ];
    }
}

class Item extends \yii\db\ActiveRecord
{

    public function getPengolahan()
    {
        return $this->hasOne(Pengolahan::className(),['id'=>'pengolahan_id']);
    }
}
