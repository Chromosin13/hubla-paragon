<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Inkjet;

/**
 * InkjetSearch represents the model behind the search form about `app\models\Inkjet`.
 */
class InkjetSearch extends Inkjet
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['jenis_inkjet', 'nama_line', 'nama_operator','snfg','state','posisi','waktu','timestamp','snfg_komponen','keterangan'], 'safe'],
            [['lanjutan', 'jumlah_operator', 'jumlah_realisasi','jumlah_reject'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Inkjet::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lanjutan' => $this->lanjutan,
            'jumlah_operator' => $this->jumlah_operator,
            'jumlah_realisasi' => $this->jumlah_realisasi,
            'waktu' => $this->waktu,
            'timestamp' => $this->timestamp,
            'jumlah_reject'=> $this->jumlah_reject,
        ]);

        $query->andFilterWhere(['like', 'jenis_inkjet', $this->jenis_inkjet])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'posisi', $this->state])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator]);

        return $dataProvider;
    }
}
