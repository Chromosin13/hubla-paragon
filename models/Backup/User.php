<?php

namespace app\models;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
//class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    const ROLE_ADMIN = 10;
    const ROLE_USER = 20;
    const ROLE_PLANNER = 30;
    const ROLE_PENIMBANGAN = 40;
    const ROLE_PENGOLAHAN = 50;
    const ROLE_QCBULK = 60;
    const ROLE_INKJET = 70;
    const ROLE_KEMAS = 80;
    const ROLE_QCFG = 90;
    const ROLE_QCTIMBANGFG = 100;
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $role;

    private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'flowreportadmin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
            'role' => 10,
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
            'role' => 20,
        ],
        '102' => [
            'id' => '102',
            'username' => 'planner',
            'password' => 'planner',
            'authKey' => 'test102key',
            'accessToken' => '102-token',
            'role' => 30,
        ],
        '103' => [
            'id' => '103',
            'username' => 'penimbangan',
            'password' => 'penimbangan',
            'authKey' => 'test103key',
            'accessToken' => '103-token',
            'role' => 40,
        ],
        '104' => [
            'id' => '104',
            'username' => 'pengolahan',
            'password' => 'pengolahan',
            'authKey' => 'test104key',
            'accessToken' => '104-token',
            'role' => 50,
        ],
        '105' => [
            'id' => '105',
            'username' => 'qcbulk',
            'password' => 'qcbulk',
            'authKey' => 'test105key',
            'accessToken' => '105-token',
            'role' => 60,
        ],
        '106' => [
            'id' => '106',
            'username' => 'inkjet',
            'password' => 'inkjet',
            'authKey' => 'test106key',
            'accessToken' => '106-token',
            'role' => 70,
        ],
        '107' => [
            'id' => '107',
            'username' => 'kemas',
            'password' => 'kemas',
            'authKey' => 'test107key',
            'accessToken' => '107-token',
            'role' => 80,
        ],
        '108' => [
            'id' => '108',
            'username' => 'qcfg',
            'password' => 'qcfg',
            'authKey' => 'test108key',
            'accessToken' => '108-token',
            'role' => 90,
        ],
        '109' => [
            'id' => '109',
            'username' => 'qctimbangfg',
            'password' => 'qctimbangfg',
            'authKey' => 'test109key',
            'accessToken' => '109-token',
            'role' => 100,
        ],
    ];

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
