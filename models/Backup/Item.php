<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pengolahan_item".
 *
 * @property string $nobatch
 * @property string $besar_batch_real
 * @property string $pengolahan_id
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengolahan_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nobatch'], 'string'],
            [['besar_batch_real', 'pengolahan_id'], 'number'],
            [['pengolahan_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nobatch' => 'Nobatch',
            'besar_batch_real' => 'Besar Batch Real',
            'pengolahan_id' => 'Pengolahan ID',
        ];
    }
}
