<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pengolahan;

/**
 * PengolahanSearch represents the model behind the search form about `app\models\Pengolahan`.
 */
class PengolahanSearch extends Pengolahan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['snfg', 'nobatch', 'nama_line', 'nama_operator', 'operator_2', 'waktu', 'state','jenis_olah','snfg_komponen','timestamp'], 'safe'],
            [['jumlah_operator', 'lanjutan','besar_batch_real'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pengolahan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'jumlah_operator' => $this->jumlah_operator,
            'waktu' => $this->waktu,
            'timestamp' => $this->timestamp,
            'lanjutan' => $this->lanjutan,
            'besar_batch_real' => $this->besar_batch_real,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'nobatch', $this->nobatch])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            //->andFilterWhere(['like', 'nama_operator', $this->nama_operator])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator])
            ->andFilterWhere(['like', 'operator_2', $this->operator_2])
            ->andFilterWhere(['like', 'jenis_olah', $this->jenis_olah])
            //->andFilterWhere(['like', 'state', $this->state])
            ->andWhere('state LIKE "STOP"');

        return $dataProvider;
    }
}
