<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inkjet_non_inline".
 *
 * @property integer $id
 * @property string $snfg
 * @property string $inkjet_sticker_bottom_jumlah_plan
 * @property string $inkjet_sticker_bottom_jumlah_realisasi
 * @property string $inkjet_sticker_bottom_nama_line
 * @property string $inkjet_sticker_bottom_jumlah_operator
 * @property string $inkjet_sticker_bottom_operator_1
 * @property string $inkjet_sticker_bottom_operator_2
 * @property string $inkjet_sticker_bottom_operator_3
 * @property string $inkjet_sticker_bottom_waktu
 * @property string $inkjet_sticker_bottom_state
 * @property string $inkjet_dus_satuan_nama_line
 * @property string $inkjet_dus_satuan_jumlah_operator
 * @property string $inkjet_dus_satuan_operator_1
 * @property string $inkjet_dus_satuan_operator_2
 * @property string $inkjet_dus_satuan_operator_3
 * @property string $inkjet_dus_satuan_waktu
 * @property string $inkjet_dus_satuan_state
 * @property string $inkjet_dus_12_jumlah_plan
 * @property string $inkjet_dus_12_jumlah_realisasi
 * @property string $inkjet_dus_12_nama_line
 * @property string $inkjet_dus_12_jumlah_operator
 * @property string $inkjet_dus_12_operator_1
 * @property string $inkjet_dus_12_operator_2
 * @property string $inkjet_dus_12_operator_3
 * @property string $inkjet_dus_12_waktu
 * @property string $inkjet_dus_12_state
 * @property string $inkjet_packaging_primer_jumlah_plan
 * @property string $inkjet_packaging_primer_jumlah_realisasi
 * @property string $inkjet_packaging_primer_nama_line
 * @property string $inkjet_packaging_primer_jumlah_operator
 * @property string $inkjet_packaging_primer_operator_1
 * @property string $inkjet_packaging_primer_operator_2
 * @property string $inkjet_packaging_primer_operator_3
 * @property string $inkjet_packaging_primer_waktu
 * @property string $inkjet_packaging_primer_state
 * @property string $inkjet_dus_satuan_jumlah_plan
 * @property string $inkjet_dus_satuan_jumlah_realisasi
 */
class InkjetNonInline extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inkjet_non_inline';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'inkjet_sticker_bottom_nama_line', 'inkjet_sticker_bottom_operator_1', 'inkjet_sticker_bottom_operator_2', 'inkjet_sticker_bottom_operator_3', 'inkjet_sticker_bottom_state', 'inkjet_dus_satuan_nama_line', 'inkjet_dus_satuan_operator_1', 'inkjet_dus_satuan_operator_2', 'inkjet_dus_satuan_operator_3', 'inkjet_dus_satuan_state', 'inkjet_dus_12_nama_line', 'inkjet_dus_12_operator_1', 'inkjet_dus_12_operator_2', 'inkjet_dus_12_operator_3', 'inkjet_dus_12_state', 'inkjet_packaging_primer_nama_line', 'inkjet_packaging_primer_operator_1', 'inkjet_packaging_primer_operator_2', 'inkjet_packaging_primer_operator_3', 'inkjet_packaging_primer_state'], 'string'],
            [['inkjet_sticker_bottom_jumlah_plan', 'inkjet_sticker_bottom_jumlah_realisasi', 'inkjet_sticker_bottom_jumlah_operator', 'inkjet_dus_satuan_jumlah_operator', 'inkjet_dus_12_jumlah_plan', 'inkjet_dus_12_jumlah_realisasi', 'inkjet_dus_12_jumlah_operator', 'inkjet_packaging_primer_jumlah_plan', 'inkjet_packaging_primer_jumlah_realisasi', 'inkjet_packaging_primer_jumlah_operator', 'inkjet_dus_satuan_jumlah_plan', 'inkjet_dus_satuan_jumlah_realisasi'], 'number'],
            [['inkjet_sticker_bottom_waktu', 'inkjet_dus_satuan_waktu', 'inkjet_dus_12_waktu', 'inkjet_packaging_primer_waktu'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'inkjet_sticker_bottom_jumlah_plan' => 'Inkjet Sticker Bottom Jumlah Plan',
            'inkjet_sticker_bottom_jumlah_realisasi' => 'Inkjet Sticker Bottom Jumlah Realisasi',
            'inkjet_sticker_bottom_nama_line' => 'Inkjet Sticker Bottom Nama Line',
            'inkjet_sticker_bottom_jumlah_operator' => 'Inkjet Sticker Bottom Jumlah Operator',
            'inkjet_sticker_bottom_operator_1' => 'Inkjet Sticker Bottom Operator 1',
            'inkjet_sticker_bottom_operator_2' => 'Inkjet Sticker Bottom Operator 2',
            'inkjet_sticker_bottom_operator_3' => 'Inkjet Sticker Bottom Operator 3',
            'inkjet_sticker_bottom_waktu' => 'Inkjet Sticker Bottom Waktu',
            'inkjet_sticker_bottom_state' => 'Inkjet Sticker Bottom State',
            'inkjet_dus_satuan_nama_line' => 'Inkjet Dus Satuan Nama Line',
            'inkjet_dus_satuan_jumlah_operator' => 'Inkjet Dus Satuan Jumlah Operator',
            'inkjet_dus_satuan_operator_1' => 'Inkjet Dus Satuan Operator 1',
            'inkjet_dus_satuan_operator_2' => 'Inkjet Dus Satuan Operator 2',
            'inkjet_dus_satuan_operator_3' => 'Inkjet Dus Satuan Operator 3',
            'inkjet_dus_satuan_waktu' => 'Inkjet Dus Satuan Waktu',
            'inkjet_dus_satuan_state' => 'Inkjet Dus Satuan State',
            'inkjet_dus_12_jumlah_plan' => 'Inkjet Dus 12 Jumlah Plan',
            'inkjet_dus_12_jumlah_realisasi' => 'Inkjet Dus 12 Jumlah Realisasi',
            'inkjet_dus_12_nama_line' => 'Inkjet Dus 12 Nama Line',
            'inkjet_dus_12_jumlah_operator' => 'Inkjet Dus 12 Jumlah Operator',
            'inkjet_dus_12_operator_1' => 'Inkjet Dus 12 Operator 1',
            'inkjet_dus_12_operator_2' => 'Inkjet Dus 12 Operator 2',
            'inkjet_dus_12_operator_3' => 'Inkjet Dus 12 Operator 3',
            'inkjet_dus_12_waktu' => 'Inkjet Dus 12 Waktu',
            'inkjet_dus_12_state' => 'Inkjet Dus 12 State',
            'inkjet_packaging_primer_jumlah_plan' => 'Inkjet Packaging Primer Jumlah Plan',
            'inkjet_packaging_primer_jumlah_realisasi' => 'Inkjet Packaging Primer Jumlah Realisasi',
            'inkjet_packaging_primer_nama_line' => 'Inkjet Packaging Primer Nama Line',
            'inkjet_packaging_primer_jumlah_operator' => 'Inkjet Packaging Primer Jumlah Operator',
            'inkjet_packaging_primer_operator_1' => 'Inkjet Packaging Primer Operator 1',
            'inkjet_packaging_primer_operator_2' => 'Inkjet Packaging Primer Operator 2',
            'inkjet_packaging_primer_operator_3' => 'Inkjet Packaging Primer Operator 3',
            'inkjet_packaging_primer_waktu' => 'Inkjet Packaging Primer Waktu',
            'inkjet_packaging_primer_state' => 'Inkjet Packaging Primer State',
            'inkjet_dus_satuan_jumlah_plan' => 'Inkjet Dus Satuan Jumlah Plan',
            'inkjet_dus_satuan_jumlah_realisasi' => 'Inkjet Dus Satuan Jumlah Realisasi',
        ];
    }
}
