<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Press;

/**
 * PressSearch represents the model behind the search form about `app\models\Press`.
 */
class PressSearch extends Press
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lanjutan'], 'integer'],
            [['snfg', 'nama_line', 'waktu', 'state'], 'safe'],
            [['jumlah_plan', 'jumlah_realisasi', 'jumlah_operator', 'operator_1', 'operator_2'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Press::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'jumlah_plan' => $this->jumlah_plan,
            'jumlah_realisasi' => $this->jumlah_realisasi,
            'jumlah_operator' => $this->jumlah_operator,
            'operator_1' => $this->operator_1,
            'operator_2' => $this->operator_2,
            'waktu' => $this->waktu,
            'lanjutan' => $this->lanjutan,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'state', $this->state]);

        return $dataProvider;
    }
}
