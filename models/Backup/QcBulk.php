<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "qc_bulk".
 *
 * @property integer $id
 * @property string $snfg
 * @property string $ph
 * @property string $viskositas
 * @property string $berat_jenis
 * @property string $kadar
 * @property string $warna
 * @property string $bau
 * @property string $performance
 * @property string $bentuk
 * @property string $mikro
 * @property string $kejernihan
 * @property string $status
 * @property string $jumlah_operator
 * @property string $nama_qc
 * @property string $waktu
 * @property string $state
 */
class QcBulk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qc_bulk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'ph', 'viskositas', 'berat_jenis', 'kadar', 'warna', 'bau', 'performance', 'bentuk', 'mikro', 'kejernihan', 'status', 'nama_qc', 'state','jenis_periksa','nama_line','snfg_komponen'], 'string'],
            [['jumlah_operator','lanjutan'], 'number'],
            [['waktu','timestamp'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'ph' => 'Ph',
            'viskositas' => 'Viskositas',
            'berat_jenis' => 'Berat Jenis',
            'kadar' => 'Kadar',
            'warna' => 'Warna',
            'bau' => 'Bau',
            'performance' => 'Performance',
            'bentuk' => 'Bentuk',
            'mikro' => 'Mikro',
            'kejernihan' => 'Kejernihan',
            'status' => 'Status',
            'jumlah_operator' => 'Jumlah Operator',
            'nama_qc' => 'Nama Qc',
            'waktu' => 'Waktu',
            'state' => 'State',
            'jenis_periksa' => 'Jenis Periksa',
            'nama_line' => 'Nama Line',
            'lanjutan' => 'Lanjutan',
            'timestamp' => 'Timestamp',
            'snfg_komponen' => 'SNFG Komponen',
        ];
    }
}
