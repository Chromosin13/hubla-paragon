<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "scm_planner".
 *
 * @property integer $id
 * @property string $sediaan
 * @property string $streamline
 * @property string $line_timbang
 * @property string $line_olah
 * @property string $line_olah_2
 * @property string $line_press
 * @property string $line_kemas_1
 * @property string $line_kemas_2
 * @property string $start
 * @property string $due
 * @property string $leadtime
 * @property string $kode_jadwal
 * @property string $snfg
 * @property string $koitem_bulk
 * @property string $koitem_fg
 * @property string $nama_bulk
 * @property string $nama_fg
 * @property string $besar_batch
 * @property string $besar_lot
 * @property string $lot
 * @property string $tglpermintaan
 * @property string $jumlah
 * @property string $keterangan
 * @property string $kategori_sop
 * @property string $kategori_detail
 */
class ScmPlanner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scm_planner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sediaan', 'streamline', 'line_timbang', 'line_olah', 'line_olah_2', 'line_press', 'line_kemas_1', 'line_kemas_2', 'kode_jadwal', 'snfg', 'koitem_bulk', 'koitem_fg', 'nama_bulk', 'nama_fg', 'keterangan', 'kategori_sop', 'kategori_detail','status','snfg_komponen','nobatch','line_olah_premix','line_adjust_olah_1','line_adjust_olah_2','line_ayak','line_press_filling_packing','line_filling','line_filling_packing_inline','line_flame_packing_inline','line_packing','snfg_induk','kode_sl','alokasi'], 'string'],
            [['start', 'due', 'tglpermintaan','timestamp'], 'safe'],
            [['leadtime', 'besar_batch', 'besar_lot', 'lot', 'jumlah','week','jumlah_press'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sediaan' => 'Sediaan',
            'streamline' => 'Streamline',
            'line_timbang' => 'Line Timbang',
            'line_olah' => 'Line Olah',
            'line_olah_2' => 'Line Olah 2',
            'line_press' => 'Line Press',
            'line_kemas_1' => 'Line Kemas 1',
            'line_kemas_2' => 'Line Kemas 2',
            'start' => 'Start',
            'due' => 'Due',
            'leadtime' => 'Leadtime',
            'kode_jadwal' => 'Kode Jadwal',
            'snfg' => 'Snfg',
            'koitem_bulk' => 'Koitem Bulk',
            'koitem_fg' => 'Koitem Fg',
            'nama_bulk' => 'Nama Bulk',
            'nama_fg' => 'Nama Fg',
            'besar_batch' => 'Besar Batch',
            'besar_lot' => 'Besar Lot',
            'lot' => 'Lot',
            'tglpermintaan' => 'Tglpermintaan',
            'jumlah' => 'Jumlah',
            'keterangan' => 'Keterangan',
            'kategori_sop' => 'Kategori Sop',
            'kategori_detail' => 'Kategori Detail',
            'status' => 'Status',
            'timestamp' => 'Timestamp',
            'snfg_komponen' => 'Snfg Komponen',
            'nobatch' => 'No Batch',
            'week' => 'Week',
            'line_olah_premix' => 'Line Olah Premix',
            'line_adjust_olah_1' => 'Line Adjust Olah 1',
            'line_adjust_olah_2' => 'Line Adjust Olah 2',
            'line_ayak' => 'Line Ayak',
            'line_press_filling_packing' => 'Line Press Filling Packing',
            'line_filling'=>'Line Filling',
            'line_filling_packing_inline' => 'Line Filling Packing Inline',
            'line_flame_packing_inline' => 'Line Flame Packing Inline',
            'line_packing' => 'Line Packing',
            'snfg_induk' => 'SNFG Induk',
            'kode_sl' => 'Kode SL',
            'jumlah_press' => 'Jumlah Press',
            'alokasi' => 'Alokasi',
        ];
    }
}
