<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posisi_proses".
 *
 * @property string $snfg
 * @property string $posisi
 * @property string $start
 * @property string $due
 */
class PosisiProses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function primaryKey()
    {
        return ['snfg'];
    }

    public static function tableName()
    {
        return 'posisi_proses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'posisi','status','ontime'], 'string'],
            [['start', 'due','timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'snfg' => 'Snfg',
            'posisi' => 'Posisi',
            'status' => 'Status',
            'start' => 'Start',
            'due' => 'Due',
            'ontime' => 'ontime',
            'timestamp' => 'Timestamp',
        ];
    }
}
