<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "daily_consumption_default".
 *
 * @property string $scheduled_start
 * @property string $name
 * @property string $scheduled_quantity
 * @property string $type
 * @property string $reference
 * @property string $bom_qty
 * @property string $bom_uom
 * @property string $component_code
 * @property string $component
 * @property string $component_qty
 * @property string $component_uom
 * @property string $total_component_qty
 * @property string $total_component_uom
 */
class DailyConsumptionDefault extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daily_consumption_default';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_warehouse');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['scheduled_start'], 'safe'],
            [['name', 'type', 'reference', 'bom_uom', 'component_code', 'component', 'component_uom', 'total_component_uom', 'nomo', 'fg_name', 'location', 'week'], 'string'],
            [['scheduled_quantity', 'bom_qty', 'component_qty', 'total_component_qty'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'scheduled_start' => 'Scheduled Start',
            'name' => 'Name',
            'scheduled_quantity' => 'Qty Batch',
            'type' => 'Type',
            'reference' => 'Formula Reference',
            'bom_qty' => 'Bom Qty',
            'bom_uom' => 'Bom Uom',
            'component_code' => 'Component Code',
            'component' => 'Component',
            'component_qty' => 'Component Qty',
            'component_uom' => 'Component Uom',
            'total_component_qty' => 'Total Component Qty',
            'total_component_uom' => 'Total Component Uom',
            'nomo' => 'Nomo',
            'fg_name' => 'Nama FG',
            'location' => 'Lokasi',
            'week' => 'Week'
        ];
    }
}
