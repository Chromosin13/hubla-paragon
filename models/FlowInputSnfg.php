<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flow_input_snfg".
 *
 * @property string $posisi
 * @property integer $lanjutan
 * @property string $datetime_start
 * @property string $datetime_stop
 * @property string $datetime_write
 * @property string $nama_operator
 * @property string $nama_line
 * @property string $jenis_kemas
 * @property integer $is_done
 * @property integer $counter
 * @property string $snfg
 * @property string $jumlah_realisasi
 * @property string $nobatch
 * @property integer $id
 */
class FlowInputSnfg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flow_input_snfg';
    }

    public $pack_date;
    //public $exp_date;
    public $koli;
    public $nama_fg;
    public $status_tare;


    public $sisa_bulk;
    public $fg_retur_layak_pakai;
    public $fg_retur_reject;
    public $netto_min;
    public $netto_1;
    public $netto_2;
    public $netto_3;
    public $netto_4;
    public $netto_5;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['posisi', 'nama_operator', 'jenis_kemas', 'snfg', 'nosmb','fg_name_odoo', 'posisi_scan', 'lakban', 'reason', 'lakban_old'], 'string'],
            [['sisa_bulk','fg_retur_reject','fg_retur_layak_pakai','netto_1','netto_2','netto_3','netto_4','netto_5'], 'number'],
            [['lanjutan', 'is_done', 'counter', 'is_split_line'], 'integer'],
            [['datetime_start', 'datetime_stop', 'datetime_write','staging','exp_date', 'tgl_olah'], 'safe'],
            [['jumlah_realisasi'], 'number'],
            [['nobatch'], 'string', 'min' => 3, 'max' => 15],
            [['barcode'], 'string', 'min' => 13, 'max' => 13],
            [['jenis_kemas','nama_line','lanjutan','nama_operator','nobatch','nosmb','fg_name_odoo','barcode', 'reason'], 'required'],

            // ['nobatch', 'string', 'length' => [5]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'posisi' => 'Posisi',
            'lanjutan' => 'Lanjutan',
            'datetime_start' => 'Datetime Start',
            'datetime_stop' => 'Datetime Stop',
            'datetime_write' => 'Datetime Write',
            'nama_operator' => 'Nama Operator',
            'nama_line' => 'Nama Line',
            'jenis_kemas' => 'Jenis Kemas',
            'is_done' => 'Is Done',
            'counter' => 'Counter',
            'snfg' => 'Snfg',
            'jumlah_realisasi' => 'Jumlah Realisasi',
            'nobatch' => 'Nobatch',
            'id' => 'ID',
            'staging' => 'Staging / Floating',
            'exp_date' => 'Expired Date',
            'nosmb' => 'SMB',
            'barcode' => 'Kode Barcode',
            'fg_name_odoo' => 'Nama FG',
            'is_split_line' => 'Is Split Line',
            'posisi_scan' => 'Posisi Scan',
            'tgl_olah' => 'Tanggal Olah',
            'lakban' => 'Lakban Baru (Exception)',
            'lakban_old' => 'Lakban Default (hasil generate sistem)',
            'reason' => 'Alasan mengubah warna lakban'
        ];
    }
}
