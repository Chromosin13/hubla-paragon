<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NotDoneQcFg;

/**
 * NotDoneQcFgSearch represents the model behind the search form about `app\models\NotDoneQcFg`.
 */
class NotDoneQcFgSearch extends NotDoneQcFg
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_palet', 'last_update', 'due', 'hari_ini', 'snfg', 'nama_fg', 'nama_bulk'], 'safe'],
            [['jumlah_palet_diterima_ndc', 'jumlah_palet', 'margin'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NotDoneQcFg::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'jumlah_palet_diterima_ndc' => $this->jumlah_palet_diterima_ndc,
            'jumlah_palet' => $this->jumlah_palet,
            'last_update' => $this->last_update,
            'due' => $this->due,
            'hari_ini' => $this->hari_ini,
            'margin' => $this->margin,
        ]);

        $query->andFilterWhere(['ilike', 'status_palet', $this->status_palet])
            ->andFilterWhere(['ilike', 'snfg', $this->snfg])
            ->andFilterWhere(['ilike', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['ilike', 'nama_bulk', $this->nama_bulk]);

        return $dataProvider;
    }
}
