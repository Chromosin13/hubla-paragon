<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogJadwalTimbangRm;

/**
 * LogJadwalTimbangRmSearch represents the model behind the search form about `app\models\LogJadwalTimbangRm`.
 */
class LogJadwalTimbangRmSearch extends LogJadwalTimbangRm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'formula_reference', 'week'], 'integer'],
            [['nomo', 'nama_fg', 'scheduled_start', 'lokasi', 'status', 'timestamp', 'nama_line'], 'safe'],
            [['qty_batch'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$week,$week2,$week3)
    {
        $query = LogJadwalTimbangRm::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'qty_batch' => $this->qty_batch,
            'scheduled_start' => $this->scheduled_start,
            'formula_reference' => $this->formula_reference,
            'timestamp' => $this->timestamp,
        ]);

        $query->orWhere(['week' => $week2])->orWhere(['week' => $week])->orWhere(['week' => $week3])->orderBy(['id' => SORT_ASC]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'lokasi', $this->lokasi])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line]);

        // ->orWhere(['week' => 9])->orWhere(['week' => 10])->orderBy(['id' => SORT_ASC])

        return $dataProvider;

        // $searchModel = new LogJadwalTimbangRmSearch();

        // $query = LogJadwalTimbangRm::find()->orWhere(['week' => $week2])->orWhere(['week' => $week])->orWhere(['week' => $week3])->orderBy(['id' => SORT_ASC]);

        // $dataProvider = new ActiveDataProvider([
        //     'query' => $query,
        //     'pagination' => [
        //         'pageSize' => 20,
        //     ],
        //     'sort' => [
        //         'defaultOrder' => [
        //             // 'id' => SORT_ASC,
        //             //'scheduled_start' => SORT_ASC,
        //             'week' => SORT_ASC, 
        //         ]
        //     ],
        // ]);
    }
}
