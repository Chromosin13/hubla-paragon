<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogVerifiedBr;

/**
 * LogVerifiedBrSearch represents the model behind the search form about `app\models\LogVerifiedBr`.
 */
class LogVerifiedBrSearch extends LogVerifiedBr
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nomo', 'timestamp', 'status', 'pic'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogVerifiedBr::find()->select('nomo')->distinct();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'sort'=> ['defaultOrder' => ['timestamp' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'timestamp' => $this->timestamp,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo]);
            // ->andFilterWhere(['like', 'status', $this->status])
            // ->andFilterWhere(['like', 'pic', $this->pic]);

        return $dataProvider;
    }

    public function search2($params,$nomo)
    {
        $query = LogVerifiedBr::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['timestamp' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'nomo' => $nomo,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'pic', $this->pic]);

        return $dataProvider;
    }
}
