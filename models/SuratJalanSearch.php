<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SuratJalan;

/**
 * SuratJalanSearch represents the model behind the search form of `app\models\SuratJalan`.
 */
class SuratJalanSearch extends SuratJalan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','is_received'], 'integer'],
            [['nosj', 'timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SuratJalan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['timestamp'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_received' => $this->is_received,
            'timestamp' => $this->timestamp,
        ]);

        $query->andFilterWhere(['like', 'nosj', $this->nosj]);

        return $dataProvider;
    }
}
