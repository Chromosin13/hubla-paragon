<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alokasi_planner".
 *
 * @property string $sediaan
 * @property string $koitem_baru
 * @property string $naitem
 * @property string $brand
 * @property string $week_plot
 * @property string $nors
 * @property string $week_rs
 * @property integer $alokasi
 * @property string $plot
 * @property integer $row_number
 */
class AlokasiPlanner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
     public static function primaryKey()
     {
         return ['row_number'];
     }

    public static function tableName()
    {
        return 'alokasi_planner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sediaan', 'koitem_baru', 'naitem', 'brand', 'nors','week_plot'], 'string'],
            [['week_rs', 'plot'], 'number'],
            [['alokasi', 'row_number'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sediaan' => 'Sediaan',
            'koitem_baru' => 'Koitem Baru',
            'naitem' => 'Naitem',
            'brand' => 'Brand',
            'week_plot' => 'Week Plot',
            'nors' => 'Nors',
            'week_rs' => 'Week Rs',
            'alokasi' => 'Alokasi',
            'plot' => 'Plot',
            'row_number' => 'Row Number',
        ];
    }
}
