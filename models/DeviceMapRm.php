<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "device_map_rm".
 *
 * @property integer $id
 * @property string $device_ip
 * @property string $nama_line
 * @property integer $operator
 * @property string $keterangan
 * @property string $mac_address
 * @property integer $weigher_connection
 * @property string $raspi_ip
 */
class DeviceMapRm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'device_map_rm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['device_ip', 'operator', 'weigher_connection','raspi_ip'], 'required'],
            [['device_ip', 'nama_line', 'keterangan', 'mac_address', 'raspi_ip'], 'string'],
            [['operator', 'weigher_connection'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_ip' => 'Tablet IP*',
            'nama_line' => 'Nama Line',
            'operator' => 'Operator*',
            'keterangan' => 'Keterangan',
            'mac_address' => 'MAC Address',
            'weigher_connection' => 'Weigher Connection*',
            'raspi_ip' => 'Raspi IP*',
        ];
    }
}
