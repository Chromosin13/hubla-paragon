<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pusat_resolusi_log".
 *
 * @property integer $id
 * @property integer $proses_id
 * @property string $proses
 * @property string $alter_user
 * @property string $alter_name
 * @property string $type
 * @property string $alter_captain
 * @property string $table
 * @property string $alter_timestamp
 */
class PusatResolusiLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pusat_resolusi_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proses_id'], 'integer'],
            [['proses', 'alter_user', 'alter_name', 'type', 'alter_captain', 'table'], 'string'],
            [['alter_timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proses_id' => 'Proses ID',
            'proses' => 'Proses',
            'alter_user' => 'Alter User',
            'alter_name' => 'Alter Name',
            'type' => 'Type',
            'alter_captain' => 'Alter Captain',
            'table' => 'Table',
            'alter_timestamp' => 'Alter Timestamp',
        ];
    }
}
