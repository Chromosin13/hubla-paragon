<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\InnerboxLabelPrint;

/**
 * InnerboxLabelPrintSearch represents the model behind the search form about `app\models\InnerboxLabelPrint`.
 */
class InnerboxLabelPrintSearch extends InnerboxLabelPrint
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'qty_request', 'qty_total', 'innerbox_qty', 'flow_input_snfg_id'], 'integer'],
            [['snfg', 'timestamp', 'no_batch', 'exp_date', 'nama_line', 'nama_fg', 'barcode', 'status', 'operator', 'na_number', 'odoo_code', 'sediaan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InnerboxLabelPrint::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'timestamp' => $this->timestamp,
            'exp_date' => $this->exp_date,
            'qty_request' => $this->qty_request,
            'qty_total' => $this->qty_total,
            'innerbox_qty' => $this->innerbox_qty,
            'flow_input_snfg_id' => $this->flow_input_snfg_id,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'no_batch', $this->no_batch])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'barcode', $this->barcode])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'operator', $this->operator])
            ->andFilterWhere(['like', 'na_number', $this->na_number])
            ->andFilterWhere(['like', 'odoo_code', $this->odoo_code])
            ->andFilterWhere(['like', 'sediaan', $this->sediaan]);

        return $dataProvider;
    }
}
