<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "penimbangan_edit_jadwal".
 *
 * @property integer $id
 * @property string $nomo
 * @property string $snfg_komponen
 * @property string $lanjutan
 * @property string $start
 * @property string $stop
 * @property string $jenis_penimbangan
 * @property string $nama_line
 * @property string $nama_operator
 * @property integer $start_id
 */
class PenimbanganEditJadwal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['id'];
    }


    public static function tableName()
    {
        return 'penimbangan_edit_jadwal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'start_id'], 'integer'],
            [['nomo', 'snfg_komponen', 'jenis_penimbangan', 'nama_line', 'nama_operator'], 'string'],
            [['lanjutan'], 'number'],
            [['start', 'stop'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomo' => 'Nomo',
            'snfg_komponen' => 'Snfg Komponen',
            'lanjutan' => 'Lanjutan',
            'start' => 'Start',
            'stop' => 'Stop',
            'jenis_penimbangan' => 'Jenis Penimbangan',
            'nama_line' => 'Nama Line',
            'nama_operator' => 'Nama Operator',
            'start_id' => 'Start ID',
        ];
    }
}
