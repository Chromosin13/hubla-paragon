<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\QcFg;

/**
 * QcFgSearch represents the model behind the search form about `app\models\QcFg`.
 */
class QcFgSearch extends QcFg
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['snfg', 'tanggal', 'nama_inspektor', 'state', 'aql', 'filling_kesesuaian_bulk', 'filling_kesesuaian_packaging_primer', 'filling_netto', 'filling_seal', 'filling_leakage', 'filling_warna_olesan', 'filling_warna_performance', 'filling_uji_ayun', 'filling_uji_oles', 'filling_uji_ketrok', 'filling_drop_test', 'filling_rub_test', 'filling_identitas_packaging_primer', 'filling_identitas_stc_bottom', 'packing_kesesuaian_packaging_sekunder', 'packing_identitas_unit_box', 'packing_identitas_inner_box', 'packing_performance_segel', 'packing_posisi_packing', 'paletting_identitas_karton_box', 'status', 'waktu','jenis_periksa','nama_line','snfg_komponen','jenis_kemas'], 'safe'],
            [['packing_qty_inner_box', 'paletting_qty_karton_box', 'retain_sample','lanjutan','jumlah_inspektor','lanjutan_ist','is_done','lanjutan_split_batch','qty','qty_sample','qty_reject'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QcFg::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tanggal' => $this->tanggal,
            'packing_qty_inner_box' => $this->packing_qty_inner_box,
            'paletting_qty_karton_box' => $this->paletting_qty_karton_box,
            'waktu' => $this->waktu,
            'retain_sample' =>$this->retain_sample,
            'lanjutan' =>$this->lanjutan,
            'lanjutan_ist' =>$this->lanjutan_ist,
            'jumlah_inspektor' =>$this->jumlah_inspektor,
            'is_done' =>$this->is_done,
            'lanjutan_split_batch'=>$this->lanjutan_split_batch,
            'qty'=>$this->qty,
            'qty_sample'=>$this->qty_sample,
            'qty_reject'=>$this->qty_reject,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'nama_inspektor', $this->nama_inspektor])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'aql', $this->aql])
            ->andFilterWhere(['like', 'filling_kesesuaian_bulk', $this->filling_kesesuaian_bulk])
            ->andFilterWhere(['like', 'filling_kesesuaian_packaging_primer', $this->filling_kesesuaian_packaging_primer])
            ->andFilterWhere(['like', 'filling_netto', $this->filling_netto])
            ->andFilterWhere(['like', 'filling_seal', $this->filling_seal])
            ->andFilterWhere(['like', 'filling_leakage', $this->filling_leakage])
            ->andFilterWhere(['like', 'filling_warna_olesan', $this->filling_warna_olesan])
            ->andFilterWhere(['like', 'filling_warna_performance', $this->filling_warna_performance])
            ->andFilterWhere(['like', 'filling_uji_ayun', $this->filling_uji_ayun])
            ->andFilterWhere(['like', 'filling_uji_oles', $this->filling_uji_oles])
            ->andFilterWhere(['like', 'filling_uji_ketrok', $this->filling_uji_ketrok])
            ->andFilterWhere(['like', 'filling_drop_test', $this->filling_drop_test])
            ->andFilterWhere(['like', 'filling_rub_test', $this->filling_rub_test])
            ->andFilterWhere(['like', 'filling_identitas_packaging_primer', $this->filling_identitas_packaging_primer])
            ->andFilterWhere(['like', 'filling_identitas_stc_bottom', $this->filling_identitas_stc_bottom])
            ->andFilterWhere(['like', 'packing_kesesuaian_packaging_sekunder', $this->packing_kesesuaian_packaging_sekunder])
            ->andFilterWhere(['like', 'packing_identitas_unit_box', $this->packing_identitas_unit_box])
            ->andFilterWhere(['like', 'packing_identitas_inner_box', $this->packing_identitas_inner_box])
            ->andFilterWhere(['like', 'packing_performance_segel', $this->packing_performance_segel])
            ->andFilterWhere(['like', 'packing_posisi_packing', $this->packing_posisi_packing])
            ->andFilterWhere(['like', 'paletting_identitas_karton_box', $this->paletting_identitas_karton_box])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'jenis_periksa', $this->jenis_periksa])
            ->andFilterWhere(['like', 'jenis_kemas', $this->jenis_kemas])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen]);


        return $dataProvider;
    }
}
