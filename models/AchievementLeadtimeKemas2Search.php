<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AchievementLeadtimeKemas2;

/**
 * AchievementLeadtimeKemas2Search represents the model behind the search form of `app\models\AchievementLeadtimeKemas2`.
 */
class AchievementLeadtimeKemas2Search extends AchievementLeadtimeKemas2
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'snfg', 'snfg_komponen', 'jenis_kemas', 'nama_line', 'nama_operator', 'tanggal', 'tanggal_stop', 'koitem_bulk', 'koitem_fg', 'nama_fg', 'nama_bulk', 'kendala','sediaan'], 'safe'],
            [['lanjutan', 'delta_minute', 'delta_hour', 'delta_minute_net', 'delta_hour_net', 'lanjutan_split_batch', 'jumlah_realisasi', 'jam_per_pcs', 'mpq', 'achievement'], 'number'],
            [['is_done', 'id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AchievementLeadtimeKemas2::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lanjutan' => $this->lanjutan,
            'tanggal' => $this->tanggal,
            'tanggal_stop' => $this->tanggal_stop,
            'delta_minute' => $this->delta_minute,
            'delta_hour' => $this->delta_hour,
            'is_done' => $this->is_done,
            'delta_minute_net' => $this->delta_minute_net,
            'delta_hour_net' => $this->delta_hour_net,
            'lanjutan_split_batch' => $this->lanjutan_split_batch,
            'jumlah_realisasi' => $this->jumlah_realisasi,
            'jam_per_pcs' => $this->jam_per_pcs,
            'mpq' => $this->mpq,
            'achievement' => $this->achievement,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'jenis_kemas', $this->jenis_kemas])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator])
            ->andFilterWhere(['like', 'koitem_bulk', $this->koitem_bulk])
            ->andFilterWhere(['like', 'koitem_fg', $this->koitem_fg])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'nama_bulk', $this->nama_bulk])
            ->andFilterWhere(['like', 'sediaan', $this->sediaan])
            ->andFilterWhere(['like', 'kendala', $this->kendala]);

        return $dataProvider;
    }
}
