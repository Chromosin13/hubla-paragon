<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "surat_jalan_rincian".
 *
 * @property integer $id
 * @property integer $surat_jalan_id
 * @property string $snfg
 * @property integer $nourut
 * @property string $qty
 * @property string $nosj
 *
 * @property SuratJalan $suratJalan
 */
class SuratJalanRincian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'surat_jalan_rincian';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['surat_jalan_id'], 'required'],
            [['surat_jalan_id', 'nourut','jumlah_koli'], 'integer'],
            [['snfg', 'nosj','nama_fg','nobatch','id_palet'], 'string'],
            [['qty'], 'number'],
            [['surat_jalan_id'], 'exist', 'skipOnError' => true, 'targetClass' => SuratJalan::className(), 'targetAttribute' => ['surat_jalan_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surat_jalan_id' => 'Surat Jalan ID',
            'snfg' => 'SNFG',
            'nourut' => 'Nomor Urut',
            'qty' => 'Quantity',
            'nosj' => 'Nomor SJ',
            'nama_fg' => 'Nama FG',
            'jumlah_koli' => 'Jumlah Koli',
            'nobatch' => 'No Batch',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuratJalan()
    {
        return $this->hasOne(SuratJalan::className(), ['id' => 'surat_jalan_id']);
    }
}
