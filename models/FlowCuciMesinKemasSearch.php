<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FlowCuciMesinKemas;

/**
 * FlowCuciMesinKemasSearch represents the model behind the search form about `app\models\FlowCuciMesinKemas`.
 */
class FlowCuciMesinKemasSearch extends FlowCuciMesinKemas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_line', 'nama_fg', 'nobatch', 'snfg', 'operator', 'jenis_cuci', 'verifikator', 'datetime_start', 'datetime_stop'], 'safe'],
            [['gombalan', 'detergen', 'parafin'], 'number'],
            [['sarung_tangan', 'id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FlowCuciMesinKemas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'gombalan' => $this->gombalan,
            'sarung_tangan' => $this->sarung_tangan,
            'detergen' => $this->detergen,
            'parafin' => $this->parafin,
            'datetime_start' => $this->datetime_start,
            'datetime_stop' => $this->datetime_stop,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'nobatch', $this->nobatch])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'operator', $this->operator])
            ->andFilterWhere(['like', 'jenis_cuci', $this->jenis_cuci])
            ->andFilterWhere(['like', 'verifikator', $this->verifikator]);

        return $dataProvider;
    }
}
