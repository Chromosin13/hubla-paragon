<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogJadwalTimbangRmNew;

/**
 * LogJadwalTimbangRmNewSearch represents the model behind the search form about `app\models\LogJadwalTimbangRmNew`.
 */
class LogJadwalTimbangRmNewSearch extends LogJadwalTimbangRmNew
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'nama_fg', 'scheduled_start', 'formula_reference', 'week', 'lokasi', 'nama_line','status'], 'safe'],
            [['qty_batch'], 'number'],
            [['custom_order'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogJadwalTimbangRmNew::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'qty_batch' => $this->qty_batch,
            'week' => $this->week,
            'custom_order' => $this->custom_order,
            'scheduled_start' => $this->scheduled_start,
        ]);

        $query->andFilterWhere(['ilike', 'nomo', $this->nomo])
            ->andFilterWhere(['ilike', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['ilike', 'formula_reference', $this->formula_reference])
            ->andFilterWhere(['ilike', 'lokasi', $this->lokasi])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'nama_line', $this->nama_line]);

        return $dataProvider;
    }
}
