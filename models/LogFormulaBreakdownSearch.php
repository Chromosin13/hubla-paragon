<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogFormulaBreakdown;

/**
 * LogFormulaBreakdownSearch represents the model behind the search form about `app\models\LogFormulaBreakdown`.
 */
class LogFormulaBreakdownSearch extends LogFormulaBreakdown
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'formula_reference', 'is_split', 'operator', 'siklus'], 'integer'],
            [['kode_bulk', 'nama_fg', 'lokasi', 'scheduled_start', 'week', 'kode_bb', 'uom', 'timbangan', 'nama_line', 'nama_bb', 'timestamp', 'is_sync', 'kode_internal', 'kode_olah', 'action'], 'safe'],
            [['qty'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogFormulaBreakdown::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'formula_reference' => $this->formula_reference,
            'scheduled_start' => $this->scheduled_start,
            'qty' => $this->qty,
            'is_split' => $this->is_split,
            'operator' => $this->operator,
            'siklus' => $this->siklus,
            'timestamp' => $this->timestamp,
        ]);

        $query->orderBy([
            // 'kode_bb' => SORT_ASC,
            // 'uom' => SORT_ASC,
            // 'qty' => SORT_DESC, 
            'serial_manual' => SORT_ASC,     
        ]);

        $query->andFilterWhere(['like', 'kode_bulk', $this->kode_bulk])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'lokasi', $this->lokasi])
            ->andFilterWhere(['like', 'week', $this->week])
            ->andFilterWhere(['like', 'kode_bb', $this->kode_bb])
            ->andFilterWhere(['like', 'kode_internal', $this->kode_internal])
            ->andFilterWhere(['like', 'kode_olah', $this->kode_olah])
            ->andFilterWhere(['like', 'uom', $this->uom])
            ->andFilterWhere(['like', 'timbangan', $this->timbangan])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nama_bb', $this->nama_bb])
            ->andFilterWhere(['like', 'action', $this->action]);

        return $dataProvider;
    }

    public function searchBr($params,$kode_bulk,$reference,$qty_batch)
    {
        $query = LogFormulaBreakdown::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'kode_bulk' => $kode_bulk,
            'formula_reference' => $reference,
            'qty_batch' => $qty_batch,
        ]);

        // $query->andFilterWhere(['like', 'kode_bulk', $this->kode_bulk])
        //     ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
        //     ->andFilterWhere(['like', 'lokasi', $this->lokasi])
        //     ->andFilterWhere(['like', 'week', $this->week])
        //     ->andFilterWhere(['like', 'kode_bb', $this->kode_bb])
        //     ->andFilterWhere(['like', 'uom', $this->uom])
        //     ->andFilterWhere(['like', 'timbangan', $this->timbangan])
        //     ->andFilterWhere(['like', 'nama_line', $this->nama_line])
        //     ->andFilterWhere(['like', 'nama_bb', $this->nama_bb]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function master_br($params)
    {
        $query = LogFormulaBreakdown::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'formula_reference' => $this->formula_reference,
            'scheduled_start' => $this->scheduled_start,
            'qty' => $this->qty,
            'is_split' => $this->is_split,
            'operator' => $this->operator,
            'siklus' => $this->siklus,
            'timestamp' => $this->timestamp,
        ]);

        $query->select(['kode_bulk', 'formula_reference','nama_fg'])->distinct();

        $query->orderBy('kode_bulk asc','formula_reference asc');

        //$query->orWhere(['week' => $week-1])->orWhere(['week' => $week])->orWhere(['week' => $week+1])->orderBy(['id' => SORT_ASC]);

        $query->andFilterWhere(['like', 'kode_bulk', $this->kode_bulk])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'lokasi', $this->lokasi])
            ->andFilterWhere(['like', 'week', $this->week])
            ->andFilterWhere(['like', 'kode_bb', $this->kode_bb])
            ->andFilterWhere(['like', 'uom', $this->uom])
            ->andFilterWhere(['like', 'timbangan', $this->timbangan])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nama_bb', $this->nama_bb]);

        // ->orWhere(['week' => 9])->orWhere(['week' => 10])->orderBy(['id' => SORT_ASC])

        return $dataProvider;

        // $searchModel = new LogJadwalTimbangRmSearch();

        // $query = LogJadwalTimbangRm::find()->orWhere(['week' => $week2])->orWhere(['week' => $week])->orWhere(['week' => $week3])->orderBy(['id' => SORT_ASC]);

        // $dataProvider = new ActiveDataProvider([
        //     'query' => $query,
        //     'pagination' => [
        //         'pageSize' => 20,
        //     ],
        //     'sort' => [
        //         'defaultOrder' => [
        //             // 'id' => SORT_ASC,
        //             //'scheduled_start' => SORT_ASC,
        //             'week' => SORT_ASC, 
        //         ]
        //     ],
        // ]);
    }
}
