<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "line_monitoring_kemas".
 *
 * @property string $proses
 * @property string $nama_line
 * @property string $nojadwal
 * @property string $waktu
 * @property string $state
 * @property string $lanjutan
 * @property string $jenis_proses
 * @property string $tipe_jadwal
 */
class LineMonitoringKemas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'line_monitoring_kemas';
    }

    public static function primaryKey()
    {
        return ['nama_line'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proses', 'nama_line', 'nojadwal', 'state', 'jenis_proses', 'tipe_jadwal'], 'string'],
            [['waktu'], 'safe'],
            [['lanjutan'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'proses' => 'Proses',
            'nama_line' => 'Nama Line',
            'nojadwal' => 'Nojadwal',
            'waktu' => 'Waktu',
            'state' => 'State',
            'lanjutan' => 'Lanjutan',
            'jenis_proses' => 'Jenis Proses',
            'tipe_jadwal' => 'Tipe Jadwal',
        ];
    }
}
