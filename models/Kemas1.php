<?php

namespace app\models;

use Yii;
use DateTime;
/**
 * This is the model class for table "kemas_1".
 *
 * @property integer $id
 * @property string $snfg
 * @property string $jumlah_plan
 * @property string $jumlah_realisasi
 * @property string $nama_line
 * @property string $nama_operator
 * @property string $waktu
 * @property string $state
 */
class Kemas1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kemas_1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'nama_line', 'nama_operator', 'state','jenis_kemas','snfg_komponen','edit_nama'], 'string'],
            [['jumlah_plan', 'jumlah_realisasi','lanjutan','jumlah_operator','lanjutan_ist','lanjutan_split_batch','is_done','batch_split','palet_ke','counter'], 'number'],
            [['waktu','timestamp','edit_time'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'jumlah_plan' => 'Jumlah Plan',
            'jumlah_realisasi' => 'Jumlah Realisasi',
            'nama_line' => 'Nama Line',
            'nama_operator' => 'Nama Operator',
            'waktu' => 'Waktu',
            'state' => 'State',
            'jenis_kemas' => 'Jenis Kemas',
            'lanjutan' => 'Lanjutan',
            'jumlah_operator' => 'Jumlah Operator',
            'timestamp' => 'Timestamp',
            'snfg_komponen' => 'SNFG Komponen',
            'lanjutan_ist' => 'Lanjutan Istirahat',
            'is_done' => 'Is Done',
            'batch_split' => 'Batch Split',
            'lanjutan_split_batch' => 'Lanjutan Split Batch',
            'palet_ke' => 'Palet Ke',
            'counter' => 'Counter',
        ];
    }

    public function getKendalas()
    {
        return $this->hasMany(Kendala::className(), ['kemas_1_id' => 'id']);
    }

    public function getFormatwaktu()
    {
        return DateTime::createFromFormat('Y-m-d H:i:s.u', $this->waktu)->format('Y-m-d H:i:s');
    }

}
