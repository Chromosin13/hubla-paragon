<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "achievement_leadtime_penimbangan".
 *
 * @property integer $id
 * @property string $nama_line
 * @property string $nomo
 * @property string $koitem_bulk
 * @property string $nama_bulk
 * @property string $nama_fg
 * @property string $jenis_penimbangan
 * @property string $real_leadtime
 * @property string $standard_leadtime
 * @property string $nama_operator
 * @property string $achievement
 * @property string $tanggal_stop
 */
class AchievementLeadtimePenimbangan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    public static function primaryKey()
    {
        return ['id'];
    }

    public static function tableName()
    {
        return 'achievement_leadtime_penimbangan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nama_line', 'nomo', 'koitem_bulk', 'sediaan', 'nama_bulk', 'nama_fg', 'jenis_penimbangan', 'nama_operator'], 'string'],
            [['real_leadtime', 'standard_leadtime', 'achievement'], 'number'],
            [['tanggal_stop'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_line' => 'Nama Line',
            'nomo' => 'Nomo',
            'koitem_bulk' => 'Koitem Bulk',
            'nama_bulk' => 'Nama Bulk',
            'nama_fg' => 'Nama Fg',
            'jenis_penimbangan' => 'Jenis Penimbangan',
            'real_leadtime' => 'Real Leadtime',
            'standard_leadtime' => 'Standard Leadtime',
            'nama_operator' => 'Nama Operator',
            'achievement' => 'Achievement',
            'tanggal_stop' => 'Tanggal Stop',
            'sediaan' => 'Sediaan',
        ];
    }
}
