<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "penempatan".
 *
 * @property integer $id
 * @property string $penempatan
 * @property string $is_active
 */
class Penempatan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'penempatan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['penempatan', 'is_active'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'penempatan' => 'Penempatan',
            'is_active' => 'Is Active',
        ];
    }
}
