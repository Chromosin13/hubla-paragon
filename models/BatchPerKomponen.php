<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "batch_per_komponen".
 *
 * @property string $nobatch
 * @property string $snfg_komponen
 * @property string $besar_batch_real
 */
class BatchPerKomponen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    // public static function primaryKey()
    // {
    //     return ['snfg_komponen'];
    // }
    
    public static function tableName()
    {
        return 'batch_per_komponen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nobatch', 'snfg_komponen'], 'string'],
            [['besar_batch_real'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nobatch' => 'Nobatch',
            'snfg_komponen' => 'Snfg Komponen',
            'besar_batch_real' => 'Besar Batch Real',
        ];
    }
}
