<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "qc_bulk_entry".
 *
 * @property integer $id
 * @property integer $nourut
 * @property string $nomo
 * @property string $status
 * @property string $timestamp
 * @property string $nama_inspektor
 */
class QcBulkEntry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qc_bulk_entry';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nourut'], 'integer'],
            [['nomo', 'status', 'nama_inspektor','jenis_adjust','keterangan_adjust'], 'string'],
            [['timestamp','created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nourut' => 'Nourut',
            'nomo' => 'Nomo',
            'status' => 'Status',
            'timestamp' => 'Timestamp',
            'nama_inspektor' => 'Nama Inspektor',
            'created_at' => 'Created At',
            'jenis_adjust' => 'Jenis Adjust',
            'keterangan_adjust' => 'Keterangan Adjust',
        ];
    }
}
