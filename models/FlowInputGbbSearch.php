<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FlowInputGbb;

/**
 * FlowInputGbbSearch represents the model behind the search form about `app\models\FlowInputGbb`.
 */
class FlowInputGbbSearch extends FlowInputGbb
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lanjutan', 'siklus'], 'integer'],
            [['datetime_start', 'datetime_stop', 'nomo', 'nama_operator'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FlowInputGbb::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'datetime_start' => $this->datetime_start,
            'datetime_stop' => $this->datetime_stop,
            'lanjutan' => $this->lanjutan,
            'siklus' => $this->siklus,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator]);

        return $dataProvider;
    }
}
