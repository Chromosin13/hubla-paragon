<?php

namespace app\models;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
//class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    const ROLE_ADMIN = 10;
    const ROLE_USER = 20;
    const ROLE_PLANNER = 30;
    const ROLE_PENIMBANGAN = 40;
    const ROLE_PENGOLAHAN = 50;
    const ROLE_QCBULK = 60;
    const ROLE_ENGINEER = 70;
    const ROLE_KEMAS = 80;
    const ROLE_QCFG = 90;
    const ROLE_NDC = 91;
    const ROLE_KARANTINA = 92;
    const ROLE_QCTIMBANGFG = 100;
    const ROLE_LEADER_PENIMBANGAN = 110;
    const ROLE_LEADER_PENGOLAHAN = 120;
    const ROLE_SURAT_JALAN = 111;
    const ROLE_IPC = 130;
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $name;
    public $role;

    private static $users = [
        '114' => [
            'id' => '114',
            'username' => 'arief.ramadhani',
            'authKey' => 'test113key',
            'accessToken' => '113-token',
            'role' => 10,
            'group' => 'admin',
        ],
        '99' => [
            'id' => '99',
            'username' => 'sampler',
            'password' => 'sampler',
            'authKey' => 'test99key',
            'accessToken' => '99-token',
            'role' => 20,
        ],
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => '1q2w3e4r',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
            'role' => 10,
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
            'role' => 20,
        ],
        '102' => [
            'id' => '102',
            'username' => 'planner',
            'password' => 'planner',
            'authKey' => 'test102key',
            'accessToken' => '102-token',
            'role' => 30,
        ],
        '103' => [
            'id' => '103',
            'username' => 'penimbangan',
            'password' => 'penimbangan',
            'authKey' => 'test103key',
            'accessToken' => '103-token',
            'role' => 40,
        ],
        '104' => [
            'id' => '104',
            'username' => 'pengolahan',
            'password' => 'pengolahan',
            'authKey' => 'test104key',
            'accessToken' => '104-token',
            'role' => 50,
        ],
        '105' => [
            'id' => '105',
            'username' => 'qcbulk',
            'password' => 'qcbulk',
            'authKey' => 'test105key',
            'accessToken' => '105-token',
            'role' => 60,
        ],
        '106' => [
            'id' => '106',
            'username' => 'engineer',
            'password' => 'engineer',
            'authKey' => 'test106key',
            'accessToken' => '106-token',
            'role' => 70,
        ],
        '107' => [
            'id' => '107',
            'username' => 'kemas',
            'password' => 'kemas',
            'authKey' => 'test107key',
            'accessToken' => '107-token',
            'role' => 80,
        ],
        '108' => [
            'id' => '108',
            'username' => 'qcfg',
            'password' => 'qcfg',
            'authKey' => 'test108key',
            'accessToken' => '108-token',
            'role' => 90,
        ],
        '109' => [
            'id' => '109',
            'username' => 'qctimbangfg',
            'password' => 'qctimbangfg',
            'authKey' => 'test109key',
            'accessToken' => '109-token',
            'role' => 100,
        ],
        '110' => [
            'id' => '110',
            'username' => 'leaderpenimbangan',
            'password' => 'leaderpenimbangan',
            'authKey' => 'test110key',
            'accessToken' => '110-token',
            'role' => 110,
        ],
        '111' => [
            'id' => '111',
            'username' => 'leaderpengolahan',
            'password' => 'leaderpengolahan',
            'authKey' => 'test111key',
            'accessToken' => '111-token',
            'role' => 120,
        ],
        '112' => [
            'id' => '112',
            'username' => 'karantina',
            'password' => 'karantina',
            'authKey' => 'test112key',
            'accessToken' => '112-token',
            'role' => 92,
        ],
        '113' => [
            'id' => '113',
            'username' => 'ndc',
            'password' => 'ndc',
            'authKey' => 'test113key',
            'accessToken' => '113-token',
            'role' => 91,
        ],
        '114' => [
            'id' => '114',
            'username' => 'surjal',
            'password' => 'surjal',
            'authKey' => 'test114key',
            'accessToken' => '114-token',
            'role' => 111,
        ],
        '115' => [
            'id' => '115',
            'username' => 'scm',
            'password' => 'scm',
            'authKey' => 'test115key',
            'accessToken' => '115-token',
            'role' => 115,
        ],
        '116' => [
            'id' => '116',
            'username' => 'viewer',
            'password' => 'viewer',
            'authKey' => 'test116key',
            'accessToken' => '116-token',
            'role' => 116,
        ],
        '117' => [
            'id' => '117',
            'username' => 'produksi',
            'password' => 'produksi',
            'authKey' => 'test117key',
            'accessToken' => '117-token',
            'role' => 117,
        ],
        '118' => [
            'id' => '118',
            'username' => 'siti.djulaeha',
            'password' => 'adminbr',
            'authKey' => 'br-J2',
            'accessToken' => '118-token',
            'role' => 'pe',
        ],
        '119' => [
            'id' => '119',
            'username' => 'dewi.swahyuni',
            'password' => 'adminbr',
            'authKey' => 'Dewi Sri Wahyuni',
            'accessToken' => '119-token',
            'role' => 'pe-reviewer',
        ],
        '120' => [
            'id' => '120',
            'username' => 'muhammad.ghazi',
            'password' => 'adminbr',
            'authKey' => 'Muhammad Ghazi',
            'accessToken' => '120-token',
            'role' => 'pe-approver',
        ],
        '121' => [
            'id' => '121',
            'username' => 'dela.viantika',
            'password' => 'adminbr',
            'authKey' => 'Dela Viantika',
            'accessToken' => '121-token',
            'role' => 'pe',
        ],
        '122' => [
            'id' => '122',
            'username' => 'pratiwi.cahyaningsih',
            'password' => 'adminbr',
            'authKey' => 'Pratiwi Cahyaningsih',
            'accessToken' => '122-token',
            'role' => 'pe',
        ],
        '123' => [
            'id' => '123',
            'username' => 'indah.etikas',
            'password' => 'adminbr',
            'authKey' => 'br-J1',
            'accessToken' => '123-token',
            'role' => 'pe',
        ],
        '124' => [
            'id' => '124',
            'username' => 'anisa.masrucha',
            'password' => 'adminbr',
            'authKey' => 'br-J1',
            'accessToken' => '124-token',
            'role' => 'pe',
        ],
        '125' => [
            'id' => '125',
            'username' => 'dwi.jayanti',
            'password' => 'adminbr',
            'authKey' => 'Dwi Jayanti',
            'accessToken' => '125-token',
            'role' => 'pe-creator',
        ],
        '126' => [
            'id' => '126',
            'username' => 'station',
            'password' => 'station',
            'authKey' => 'test126key',
            'accessToken' => '126-token',
            'role' => 126,
        ],
        '127' => [
            'id' => '127',
            'username' => 'febryani.rozalin',
            'password' => 'adminbr',
            'authKey' => 'br-J1',
            'accessToken' => '127-token',
            'role' => 'pe',
        ],
        '128' => [
            'id' => '128',
            'username' => 'ahmad.furqanh',
            'password' => 'adminbr',
            'authKey' => 'br-J1',
            'accessToken' => '128-token',
            'role' => 'pe',
        ],
        '129' => [
            'id' => '129',
            'username' => 'sita.saputri',
            'password' => 'adminbr',
            'authKey' => 'Sita Swadesti Asnan Putri',
            'accessToken' => '129-token',
            'role' => 'pe-approver',
        ],
        '130' => [
            'id' => '130',
            'username' => 'ayu.dlwidianingrum',
            'password' => 'adminbr',
            'authKey' => 'Ayu Dwi Lestari Widianingrum',
            'accessToken' => '130-token',
            'role' => 'pe-approver',
        ],
        '131' => [
            'id' => '131',
            'username' => 'rachma.kusumawati',
            'password' => 'adminbr',
            'authKey' => 'Rachma Kusumawati',
            'accessToken' => '131-token',
            'role' => 'pe-creator',
        ],
        '132' => [
            'id' => '132',
            'username' => 'nurfadlah.musaropah',
            'password' => 'adminbr',
            'authKey' => 'Nurfadlah Musaropah',
            'accessToken' => '132-token',
            'role' => 'pe-creator',
        ],
        '133' => [
            'id' => '133',
            'username' => 'mela.endriani',
            'password' => 'adminbr',
            'authKey' => 'Mela Endriani',
            'accessToken' => '133-token',
            'role' => 'pe-reviewer',
        ],
        '134' => [
            'id' => '134',
            'username' => 'admin-malaysia',
            'password' => 'admin-malaysia',
            'authKey' => '',
            'accessToken' => '134-token',
            'role' => 'malaysia',
        ],
        '135' => [
            'id' => '135',
            'username' => 'printer-malaysia',
            'password' => 'printer-malaysia',
            'authKey' => '',
            'accessToken' => '135-token',
            'role' => 'malaysia',
        ],
        '136' => [
            'id' => '136',
            'username' => 'febryani.rozalin',
            'password' => 'adminbr',
            'authKey' => 'br-J2',
            'accessToken' => '136-token',
            'role' => 'pe',
        ],
        '137' => [
            'id' => '137',
            'username' => 'packeng',
            'password' => 'packeng',
            'authKey' => '',
            'accessToken' => '137-token',
            'role' => 'packeng',
        ],

        '138' => [
            'id' => '138',
            'username' => 'jit',
            'password' => 'jit',
            'authKey' => '',
            'accessToken' => '138-token',
            'role' => 92,
        ],

        '139' => [
            'id' => '139',
            'username' => 'ika.eastuti',
            'password' => 'adminbr',
            'authKey' => 'Ika Eli Astuti',
            'accessToken' => '139-token',
            'role' => 'pe-creator-reviewer',
        ],
        '140' => [
            'id' => '140',
            'username' => 'afriani.lestari',
            'password' => 'adminbr',
            'authKey' => 'Afriani Lestari',
            'accessToken' => '140-token',
            'role' => 'pe-creator-reviewer',
        ],
        '142' => [
            'id' => '142',
            'username' => 'lovana.astriana',
            'password' => 'adminbr',
            'authKey' => 'Lovana Astriana',
            'accessToken' => '138-token',
            'role' => 'pe-creator-reviewer',
        ],
        '143' => [
            'id' => '143',
            'username' => 'kireina.artanti',
            'password' => 'adminbr',
            'authKey' => 'Kireina Artanti',
            'accessToken' => '143-token',
            'role' => 'pe-reviewer',
            ],
        '144' => [
            'id' => '144',
            'username' => 'hilmi.halif',
            'password' => 'adminbr',
            'authKey' => 'Hilmi Haidar Alif',
            'accessToken' => '144-token',
            'role' => 'pe-creator-reviewer',
            ],
        '145' => [
            'id' => '145',
            'username' => 'harits.majiid',
            'password' => 'adminbr',
            'authKey' => 'Harits Majiid',
            'accessToken' => '145-token',
            'role' => 'pe-approver',
            ],
        '146' => [
            'id' => '146',
            'username' => 'alinda.hputri',
            'password' => 'adminbr',
            'authKey' => 'Alinda Herdiana Putri',
            'accessToken' => '146-token',
            'role' => 'pe-reviewer',
            ],
        '147' => [
            'id' => '147',
            'username' => 'nurul.masyithah',
            'password' => 'ptiuser1234',
            'authKey' => 'test147key',
            'accessToken' => '147-token',
            'role' => 90,
        ],
        '148' => [
            'id' => '148',
            'username' => 'melin.aputri',
            'password' => 'adminbr',
            'authKey' => 'Melin Alyuni Putri',
            'accessToken' => '148-token',
            'role' => 'pe-creator-reviewer',
        ],
        '149' => [
            'id' => '149',
            'username' => 'eka.eastuti',
            'password' => 'adminbr',
            'authKey' => 'br-J1',
            'accessToken' => '149-token',
            'role' => 'pe',
        ],
        '150' => [
            'id' => '150',
            'username' => 'akun.mex',
            'password' => 'mex1234',
            'authKey' => 'mex',
            'accessToken' => '150-token',
            'role' => 'mex',
        ],
        '151' => [
            'id' => '151',
            'username' => 'inspektor',
            'password' => 'inspektor',
            'authKey' => '',
            'accessToken' => '151-token',
            'role' => 'inspektor',
        ],
        '152' => [
            'id' => '152',
            'username' => 'umu.kmrd',
            'password' => 'adminbr',
            'authKey' => 'Umu Kulsum Mrd',
            'accessToken' => '152-token',
            'role' => 'pe-reviewer',
        ],
        '153' => [
            'id' => '153',
            'username' => 'dian.marlina',
            'password' => 'adminbr',
            'authKey' => 'Dian Marlina',
            'accessToken' => '153-token',
            'role' => 'pe-reviewer',
        ],
        '154' => [
            'id' => '154',
            'username' => 'ika.lirunnisa',
            'password' => 'adminbr',
            'authKey' => 'Ika Luthfi Irunnisa',
            'accessToken' => '154-token',
            'role' => 'pe-reviewer',
        ],
        '155' => [
            'id' => '155',
            'username' => 'annisa.agustina',
            'password' => 'adminbr',
            'authKey' => 'Annisa Agustina',
            'accessToken' => '155-token',
            'role' => 'pe-creator-reviewer',
        ],
        '156' => [
            'id' => '156',
            'username' => 'm.aramarullah',
            'password' => 'adminbr',
            'authKey' => 'M Azkia Rifqi Amarullah',
            'accessToken' => '156-token',
            'role' => 'pe-approver',
        ],
        '157' => [
            'id' => '157',
            'username' => 'checker',
            'password' => 'checker',
            'authKey' => 'checker',
            'accessToken' => '157-token',
            'role' => 'checker',
        ],
        '158' => [
            'id' => '158',
            'username' => 'dwi.ffebriyani',
            'password' => 'adminbr',
            'authKey' => 'Dwi Fitri Febriyani',
            'accessToken' => '158-token',
            'role' => 'pe-reviewer',
        ],
        '159' => [
            'id' => '159',
            'username' => 'ipc',
            'password' => 'ipc',
            'authKey' => 'ipc',
            'accessToken' => '159-token',
            'role' => 'ipc',
        ],
        '160' => [
            'id' => '160',
            'username' => 'adminbr',
            'password' => '1q2w3e4r',
            'authKey' => 'br-J1',
            'accessToken' => '160-token',
            'role' => 'pe-creator-reviewer-approver',
        ],
        '161' => [
            'id' => '161',
            'username' => 'spr',
            'password' => 'spr',
            'authKey' => 'spr',
            'accessToken' => '161-token',
            'role' => 'bulk_sisa',
        ],
        '162' => [
            'id' => '162',
            'username' => 'bulksisa',
            'password' => 'bulksisa',
            'authKey' => 'bs-J1',
            'accessToken' => '162-token',
            'role' => 'bulk_sisa',
        ],
        '163' => [
          'id' => '163',
          'username' => 'wpm',
          'password' => 'wpm',
          'authKey' => 'wpm',
          'accessToken' => '163-token',
          'role' => 'bulk_sisa',
        ],

        '164' => [
          'id' => '164',
          'username' => 'gbb',
          'password' => 'gbb',
          'authKey' => 'gbb',
          'accessToken' => '164-token',
          'role' => 'gbb',
        ],
        '165' => [
          'id' => '165',
          'username' => 'amelia.safitri',
          'password' => 'adminbr',
          'authKey' => 'Amelia Safitri',
          'accessToken' => '165-token',
          'role' => 'pe-approver',
        ],
        '166' => [
          'id' => '166',
          'username' => 'yacinta.chapsari',
          'password' => 'adminbr',
          'authKey' => 'Yacinta Christi Hapsari',
          'accessToken' => '166-token',
          'role' => 'pe-approver',
        ],
        '167' => [
          'id' => '167',
          'username' => 'aulia.kwardani',
          'password' => 'adminbr',
          'authKey' => 'Aulia Kusuma Wardani',
          'accessToken' => '167-token',
          'role' => 'pe-creator-reviewer',
        ],
        '168' => [
          'id' => '168',
          'username' => 'mauriel.mkurnia',
          'password' => 'adminbr',
          'authKey' => 'Mauriel Marsha Kurnia',
          'accessToken' => '168-token',
          'role' => 'pe-creator-reviewer',
        ],
        '169' => [
          'id' => '169',
          'username' => 'desi.ariyanti',
          'password' => 'adminbr',
          'authKey' => 'Desi Ariyanti',
          'accessToken' => '169-token',
          'role' => 'pe-creator-reviewer',
        ],

    ];


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    public function getRole()
    {
        return $this->role;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
