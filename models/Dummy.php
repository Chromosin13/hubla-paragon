<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dummy".
 *
 * @property integer $id
 * @property string $snfg
 * @property string $nama_line
 * @property string $jumlah_operator
 * @property string $nama_operator
 * @property string $nama_operator_2
 * @property string $waktu
 * @property string $state
 * @property string $lanjutan
 * @property string $posisi
 * @property string $snfg_komponen
 * @property string $jenis_proses
 * @property string $timestamp
 * @property string $lanjutan_ist
 * @property integer $is_done
 * @property string $nomo
 * @property integer $lanjutan_split_batch
 * @property integer $palet_ke
 * @property string $nojadwal
 */
class Dummy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dummy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'is_done', 'lanjutan_split_batch', 'palet_ke'], 'integer'],
            [['snfg', 'nama_line', 'nama_operator', 'nama_operator_2', 'state', 'posisi', 'snfg_komponen', 'jenis_proses', 'nomo', 'nojadwal'], 'string'],
            [['jumlah_operator', 'lanjutan', 'lanjutan_ist'], 'number'],
            [['waktu', 'timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'nama_line' => 'Nama Line',
            'jumlah_operator' => 'Jumlah Operator',
            'nama_operator' => 'Nama Operator',
            'nama_operator_2' => 'Nama Operator 2',
            'waktu' => 'Waktu',
            'state' => 'State',
            'lanjutan' => 'Lanjutan',
            'posisi' => 'Posisi',
            'snfg_komponen' => 'Snfg Komponen',
            'jenis_proses' => 'Jenis Proses',
            'timestamp' => 'Timestamp',
            'lanjutan_ist' => 'Lanjutan Ist',
            'is_done' => 'Is Done',
            'nomo' => 'Nomo',
            'lanjutan_split_batch' => 'Lanjutan Split Batch',
            'palet_ke' => 'Palet Ke',
            'nojadwal' => 'Nojadwal',
        ];
    }
}
