<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kehadiran".
 *
 * @property integer $id
 * @property string $nomor_unik
 * @property string $nama
 * @property string $kehadiran
 */
class Kehadiran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kehadiran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomor_unik', 'nama', 'kehadiran'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomor_unik' => 'Nomor Unik',
            'nama' => 'Nama',
            'kehadiran' => 'Kehadiran',
        ];
    }
}
