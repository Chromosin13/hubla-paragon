<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AllprocessLeadtime;

/**
 * AllprocessLeadtimeSearch represents the model behind the search form about `app\models\AllprocessLeadtime`.
 */
class AllprocessLeadtimeSearch extends AllprocessLeadtime
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row_number'], 'integer'],
            [['posisi', 'snfg', 'snfg_komponen', 'nama_line', 'nama', 'tanggal'], 'safe'],
            [['sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AllprocessLeadtime::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'row_number' => $this->row_number,
            'tanggal' => $this->tanggal,
            'sum' => $this->sum,
        ]);

        $query->andFilterWhere(['like', 'posisi', $this->posisi])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nama', $this->nama]);

        return $dataProvider;
    }
}
