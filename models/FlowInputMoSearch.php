<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FlowInputMo;

/**
 * FlowInputMoSearch represents the model behind the search form of `app\models\FlowInputMo`.
 */
class FlowInputMoSearch extends FlowInputMo
{
    /**
     * @inheritdoc
     */


    /* your calculated attribute */
    public $durasiMenit;
    public $downtimeFilled;
    public $durasiMenitbersih;



    public function rules()
    {
        return [
            [['nomo', 'posisi', 'datetime_start', 'datetime_stop', 'datetime_write', 'nama_operator', 'nama_line', 'jenis_penimbangan', 'plan_start_olah', 'plan_end_olah', 'turun_bulk_start', 'turun_bulk_stop', 'adjust_start', 'adjust_stop', 'ember_start', 'ember_stop'], 'safe'],
            [['lanjutan', 'shift_plan_start_olah', 'shift_plan_end_olah', 'is_done', 'id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FlowInputMo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lanjutan' => $this->lanjutan,
            'datetime_start' => $this->datetime_start,
            'datetime_stop' => $this->datetime_stop,
            'datetime_write' => $this->datetime_write,
            'shift_plan_start_olah' => $this->shift_plan_start_olah,
            'shift_plan_end_olah' => $this->shift_plan_end_olah,
            'plan_start_olah' => $this->plan_start_olah,
            'plan_end_olah' => $this->plan_end_olah,
            'turun_bulk_start' => $this->turun_bulk_start,
            'turun_bulk_stop' => $this->turun_bulk_stop,
            'adjust_start' => $this->adjust_start,
            'adjust_stop' => $this->adjust_stop,
            'ember_start' => $this->ember_start,
            'ember_stop' => $this->ember_stop,
            'is_done' => $this->is_done,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'posisi', $this->posisi])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'jenis_penimbangan', $this->jenis_penimbangan]);

        return $dataProvider;
    }
}
