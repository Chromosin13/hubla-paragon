<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MasterBahanBakuRepack;

/**
 * MasterBahanBakuRepackSearch represents the model behind the search form about `app\models\MasterBahanBakuRepack`.
 */
class MasterBahanBakuRepackSearch extends MasterBahanBakuRepack
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['kode_internal', 'nama_bb'], 'safe'],
            [['netto_repack', 'galat'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterBahanBakuRepack::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'netto_repack' => $this->netto_repack,
            'galat' => $this->galat,
        ]);

        $query->andFilterWhere(['ilike', 'kode_internal', $this->kode_internal])
            ->andFilterWhere(['ilike', 'nama_bb', $this->nama_bb]);

        return $dataProvider;
    }
}
