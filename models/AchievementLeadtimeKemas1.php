<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "achievement_leadtime_kemas1".
 *
 * @property string $nomo
 * @property string $snfg_komponen
 * @property string $snfg
 * @property string $jenis_kemas
 * @property string $lanjutan
 * @property string $nama_line
 * @property string $nama_operator
 * @property string $tanggal
 * @property string $tanggal_stop
 * @property string $delta_minute
 * @property string $delta_hour
 * @property string $koitem_bulk
 * @property string $koitem_fg
 * @property string $nama_fg
 * @property string $nama_bulk
 * @property integer $is_done
 * @property string $kendala
 * @property string $delta_minute_net
 * @property string $delta_hour_net
 * @property string $jumlah_realisasi
 * @property string $jam_per_pcs
 * @property string $mpq
 * @property string $press
 * @property string $filling
 * @property string $achievement
 * @property integer $id
 */
class AchievementLeadtimeKemas1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    // public static function primaryKey()
    // {
    //     return ['id'];
    // }
    
    public static function primaryKey()
    {
        return ['stop_id'];
    }

    public static function tableName()
    {
        return 'achievement_leadtime_kemas1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'snfg_komponen', 'snfg', 'jenis_kemas', 'nama_line', 'nama_operator', 'koitem_bulk', 'koitem_fg', 'nama_fg', 'nama_bulk', 'kendala'], 'string'],
            [['lanjutan', 'delta_minute', 'delta_hour', 'delta_minute_net', 'delta_hour_net', 'jumlah_realisasi', 'jam_per_pcs', 'mpq', 'press', 'filling', 'achievement'], 'number'],
            [['tanggal', 'tanggal_stop'], 'safe'],
            [['is_done', 'id','start_id','stop_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nomo' => 'Nomo',
            'snfg_komponen' => 'Snfg Komponen',
            'snfg' => 'Snfg',
            'jenis_kemas' => 'Jenis Kemas',
            'lanjutan' => 'Lanjutan',
            'nama_line' => 'Nama Line',
            'nama_operator' => 'Nama Operator',
            'tanggal' => 'Tanggal',
            'tanggal_stop' => 'Tanggal Stop',
            'delta_minute' => 'Delta Minute',
            'delta_hour' => 'Delta Hour',
            'koitem_bulk' => 'Koitem Bulk',
            'koitem_fg' => 'Koitem Fg',
            'nama_fg' => 'Nama Fg',
            'nama_bulk' => 'Nama Bulk',
            'is_done' => 'Is Done',
            'kendala' => 'Kendala',
            'delta_minute_net' => 'Delta Minute Net',
            'delta_hour_net' => 'Delta Hour Net',
            'jumlah_realisasi' => 'Jumlah Realisasi',
            'jam_per_pcs' => 'Jam Per Pcs',
            'mpq' => 'Mpq',
            'press' => 'Press',
            'filling' => 'Filling',
            'achievement' => 'Achievement',
            'id' => 'ID',
            'start_id' => 'Start ID',
            'stop_id' => 'Stop ID',
        ];
    }
}
