<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "konfigurasi_standar_item".
 *
 * @property integer $id
 * @property integer $ks_id
 * @property string $kode
 * @property string $nama
 * @property string $jenis
 * @property string $proses
 * @property string $mpq
 * @property integer $jumlah_operator
 * @property integer $line_id
 * @property string $zona
 * @property string $output
 * @property string $print_flag
 * @property string $kode_streamline
 * @property string $std_leadtime
 * @property string $pdt
 * @property string $last_update
 *
 * @property KonfigurasiStandar $ks
 */
class KonfigurasiStandarItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'konfigurasi_standar_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ks_id', 'jumlah_operator', 'line_id','nourut'], 'integer'],
            [['kode', 'nama', 'jenis', 'proses', 'zona', 'print_flag', 'kode_streamline'], 'string'],
            [['mpq', 'output', 'std_leadtime', 'pdt'], 'number'],
            [['last_update'], 'safe'],
            [['ks_id'], 'exist', 'skipOnError' => true, 'targetClass' => KonfigurasiStandar::className(), 'targetAttribute' => ['ks_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ks_id' => 'Ks ID',
            'kode' => 'Kode',
            'nama' => 'Nama',
            'jenis' => 'Jenis',
            'proses' => 'Proses',
            'mpq' => 'Mpq',
            'jumlah_operator' => 'Jumlah Operator',
            'line_id' => 'Line ID',
            'zona' => 'Zona',
            'output' => 'Output',
            'print_flag' => 'Print Flag',
            'kode_streamline' => 'Kode Streamline',
            'std_leadtime' => 'Std Leadtime',
            'pdt' => 'Pdt',
            'last_update' => 'Last Update',
            'nourut' => 'Nourut',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKs()
    {
        return $this->hasOne(KonfigurasiStandar::className(), ['id' => 'ks_id']);
    }
}
