<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posisi_proses_npd".
 *
 * @property string $nomo
 * @property string $snfg
 * @property string $snfg_komponen
 * @property string $nama_fg
 * @property string $posisi
 * @property string $start
 * @property string $due
 * @property string $status
 * @property string $timestamp
 * @property string $ontime
 * @property string $lanjutan_split_batch
 * @property string $delta
 * @property string $time
 */
class PosisiProsesNpd extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posisi_proses_npd';
    }


    public static function primaryKey()
    {
        return ['snfg_komponen'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'snfg', 'snfg_komponen', 'nama_fg', 'posisi', 'status', 'ontime'], 'string'],
            [['start', 'due', 'timestamp', 'time'], 'safe'],
            [['lanjutan_split_batch', 'delta'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nomo' => 'Nomo',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'nama_fg' => 'Nama Fg',
            'posisi' => 'Posisi',
            'start' => 'Start',
            'due' => 'Due',
            'status' => 'Status',
            'timestamp' => 'Timestamp',
            'ontime' => 'Ontime',
            'lanjutan_split_batch' => 'Lanjutan Split Batch',
            'delta' => 'Delta',
            'time' => 'Time',
        ];
    }
}
