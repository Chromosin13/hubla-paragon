<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_data_barcode_malaysia".
 *
 * @property integer $id
 * @property string $koitem_lama
 * @property string $koitem_baru
 * @property string $nama_fg
 * @property string $barcode_lama
 * @property string $barcode_harmonize
 */
class MasterDataBarcodeMalaysia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_data_barcode_malaysia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['koitem_lama', 'koitem_baru', 'barcode_lama', 'barcode_harmonize'], 'required'],
            [['koitem_lama', 'koitem_baru', 'nama_fg', 'barcode_lama', 'barcode_harmonize'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'koitem_lama' => 'Koitem Lama',
            'koitem_baru' => 'Koitem Baru',
            'nama_fg' => 'Nama Fg',
            'barcode_lama' => 'Barcode Lama',
            'barcode_harmonize' => 'Barcode Harmonize',
        ];
    }
}
