<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unique_koitem".
 *
 * @property string $koitem
 */
class UniqueKoitem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function primaryKey()
    {
        return ['koitem'];
    }

    public static function tableName()
    {
        return 'unique_koitem';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['koitem'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'koitem' => 'Koitem',
        ];
    }
}
