<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogPrintQueueChecker;

/**
 * LogPrintQueueCheckerSearch represents the model behind the search form about `app\models\LogPrintQueueChecker`.
 */
class LogPrintQueueCheckerSearch extends LogPrintQueueChecker
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'siklus'], 'integer'],
            [['timestamp', 'nomo', 'operator', 'is_print', 'timestamp_checked', 'nama_bulk'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogPrintQueueChecker::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'timestamp' => $this->timestamp,
            'siklus' => $this->siklus,
            'timestamp_checked' => $this->timestamp_checked,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'operator', $this->operator])
            ->andFilterWhere(['like', 'is_print', $this->is_print])
            ->andFilterWhere(['like', 'nama_bulk', $this->nama_bulk]);

        return $dataProvider;
    }
}
