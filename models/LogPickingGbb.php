<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_picking_gbb".
 *
 * @property integer $id
 * @property string $timestamp
 * @property string $datetime_start
 * @property string $datetime_stop
 * @property string $nomo
 * @property string $nama_line
 * @property string $mo_odoo
 * @property string $kode_bb
 * @property string $kode_internal
 * @property string $nama_bb
 * @property string $kode_olah
 * @property integer $siklus
 * @property string $weight
 * @property string $satuan
 * @property string $operator
 * @property string $koitem_fg
 * @property string $jenis_bb
 */
class LogPickingGbb extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_picking_gbb';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['timestamp', 'datetime_start', 'datetime_stop'], 'safe'],
            [['nomo', 'nama_line', 'mo_odoo', 'kode_bb', 'kode_internal', 'nama_bb', 'kode_olah', 'satuan', 'operator', 'koitem_fg', 'jenis_bb','status'], 'string'],
            [['siklus'], 'integer'],
            [['weight'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'timestamp' => 'Timestamp',
            'datetime_start' => 'Datetime Start',
            'datetime_stop' => 'Datetime Stop',
            'nomo' => 'Nomo',
            'nama_line' => 'Nama Line',
            'mo_odoo' => 'Mo Odoo',
            'kode_bb' => 'Kode Bb',
            'kode_internal' => 'Kode Internal',
            'nama_bb' => 'Nama Bb',
            'kode_olah' => 'Kode Olah',
            'siklus' => 'Siklus',
            'status' => 'Status',
            'weight' => 'Weight',
            'satuan' => 'Satuan',
            'operator' => 'Operator',
            'koitem_fg' => 'Koitem Fg',
            'jenis_bb' => 'Jenis Bb',
        ];
    }
}
