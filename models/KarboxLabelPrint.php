<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "karbox_label_print".
 *
 * @property integer $id
 * @property string $snfg
 * @property string $koitem_fg
 * @property string $nama_fg
 * @property string $nosmb
 * @property string $barcode
 * @property string $nobatch
 * @property string $na_number
 * @property string $operator
 * @property string $exp_date
 * @property integer $qty
 * @property string $status
 * @property integer $qty_request
 * @property integer $qty_total_per_snfg
 * @property string $nama_line
 * @property string $timestamp
 * @property string $odoo_code
 * @property integer $flow_input_snfg_id
 * @property string $sediaan
 */
class KarboxLabelPrint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'karbox_label_print';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'koitem_fg', 'nama_fg', 'nosmb', 'nobatch', 'na_number', 'status', 'nama_line', 'odoo_code', 'sediaan', 'lakban'], 'string'],
            [['exp_date', 'timestamp'], 'safe'],
            [['qty', 'qty_total_per_snfg', 'flow_input_snfg_id'], 'integer'],
            [['qty_request'], 'integer', 'min' => 1, 'max' => 200],
            [['barcode'], 'string', 'min' => 13, 'max' => 13],
            [['qty_request','operator', 'exp_date', 'nosmb', 'qty', 'barcode'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'koitem_fg' => 'Koitem Fg',
            'nama_fg' => 'Nama Fg',
            'nosmb' => 'Nosmb',
            'barcode' => 'Barcode',
            'nobatch' => 'Nobatch',
            'na_number' => 'Na Number',
            'operator' => 'Operator',
            'exp_date' => 'Exp Date',
            'qty' => 'Qty per Karbox',
            'status' => 'Status',
            'qty_request' => 'Qty Request',
            'qty_total_per_snfg' => 'Qty Total Per Snfg',
            'nama_line' => 'Nama Line',
            'timestamp' => 'Timestamp',
            'odoo_code' => 'Odoo Code',
            'flow_input_snfg_id' => 'Flow Input Snfg ID',
            'sediaan' => 'Sediaan',
        ];
    }
}
