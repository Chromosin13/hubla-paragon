<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IdentitasProduk;

/**
 * IdentitasProdukSearch represents the model behind the search form about `app\models\IdentitasProduk`.
 */
class IdentitasProdukSearch extends IdentitasProduk
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama_produk', 'batch_produk', 'kode_sl', 'no_notifikasi', 'barcode_produk', 'exp_date', 'no_mo', 'line_kemas', 'due_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IdentitasProduk::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'exp_date' => $this->exp_date,
            'due_date' => $this->due_date,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'nama_produk', $this->nama_produk])
            ->andFilterWhere(['like', 'batch_produk', $this->batch_produk])
            ->andFilterWhere(['like', 'kode_sl', $this->kode_sl])
            ->andFilterWhere(['like', 'no_notifikasi', $this->no_notifikasi])
            ->andFilterWhere(['like', 'barcode_produk', $this->barcode_produk])
            ->andFilterWhere(['like', 'no_mo', $this->no_mo])
            ->andFilterWhere(['like', 'line_kemas', $this->line_kemas]);

        return $dataProvider;
    }
}
