<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\QcbulkRft;

/**
 * QcbulkRftSearch represents the model behind the search form about `app\models\QcbulkRft`.
 */
class QcbulkRftSearch extends QcbulkRft
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row_number', 'start_id', 'stop_id', 'is_done'], 'integer'],
            [['sediaan', 'snfg', 'snfg_komponen', 'nama_fg', 'jenis_periksa', 'nama_qc', 'ph', 'viskositas', 'berat_jenis', 'kadar', 'warna', 'bau', 'performance', 'bentuk', 'mikro', 'kejernihan', 'status', 'status_rft','tanggal_stop'], 'safe'],
            [['lanjutan', 'jumlah_operator'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QcbulkRft::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'row_number' => $this->row_number,
            'start_id' => $this->start_id,
            'lanjutan' => $this->lanjutan,
            'jumlah_operator' => $this->jumlah_operator,
            'stop_id' => $this->stop_id,
            'is_done' => $this->is_done,
            'tanggal_stop' => $this->tanggal_stop,
        ]);

        $query->andFilterWhere(['like', 'sediaan', $this->sediaan])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'jenis_periksa', $this->jenis_periksa])
            ->andFilterWhere(['like', 'nama_qc', $this->nama_qc])
            ->andFilterWhere(['like', 'ph', $this->ph])
            ->andFilterWhere(['like', 'viskositas', $this->viskositas])
            ->andFilterWhere(['like', 'berat_jenis', $this->berat_jenis])
            ->andFilterWhere(['like', 'kadar', $this->kadar])
            ->andFilterWhere(['like', 'warna', $this->warna])
            ->andFilterWhere(['like', 'bau', $this->bau])
            ->andFilterWhere(['like', 'performance', $this->performance])
            ->andFilterWhere(['like', 'bentuk', $this->bentuk])
            ->andFilterWhere(['like', 'mikro', $this->mikro])
            ->andFilterWhere(['like', 'kejernihan', $this->kejernihan])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'status_rft', $this->status_rft]);

        return $dataProvider;
    }
}
