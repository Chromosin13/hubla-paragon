<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_proses_olah".
 *
 * @property integer $id
 * @property string $jenis_proses_olah
 */
class JenisProsesOlah extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_proses_olah';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_proses_olah'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_proses_olah' => 'Jenis Proses Olah',
        ];
    }
}
