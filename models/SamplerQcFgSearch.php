<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SamplerQcFg;

/**
 * SamplerQcFgSearch represents the model behind the search form about `app\models\SamplerQcFg`.
 */
class SamplerQcFgSearch extends SamplerQcFg
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'jumlah_kedatangan_batch', 'aql_kedatangan', 'pengambilan_sampel', 'sampler_id', 'jumlah_palet'], 'integer'],
            [['temuan_defect', 'kategori_aql', 'datetime_start', 'datetime_stop', 'datetime_write', 'nopo', 'no_surjal', 'koitem_fg', 'nama_fg', 'supplier', 'batch', 'penempatan', 'koordinat_penempatan', 'status_analis', 'priority', 'kategori', 'date_penerimaanlog', 'sop_npd', 'flag_analis'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SamplerQcFg::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'jumlah_kedatangan_batch' => $this->jumlah_kedatangan_batch,
            'aql_kedatangan' => $this->aql_kedatangan,
            'pengambilan_sampel' => $this->pengambilan_sampel,
            'datetime_start' => $this->datetime_start,
            'datetime_stop' => $this->datetime_stop,
            'datetime_write' => $this->datetime_write,
            'sampler_id' => $this->sampler_id,
            'date_penerimaanlog' => $this->date_penerimaanlog,
            'jumlah_palet' => $this->jumlah_palet,
        ]);

        $query->andFilterWhere(['like', 'temuan_defect', $this->temuan_defect])
            ->andFilterWhere(['like', 'kategori_aql', $this->kategori_aql])
            ->andFilterWhere(['like', 'nopo', $this->nopo])
            ->andFilterWhere(['like', 'no_surjal', $this->no_surjal])
            ->andFilterWhere(['like', 'koitem_fg', $this->koitem_fg])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'supplier', $this->supplier])
            ->andFilterWhere(['like', 'batch', $this->batch])
            ->andFilterWhere(['like', 'penempatan', $this->penempatan])
            ->andFilterWhere(['like', 'koordinat_penempatan', $this->koordinat_penempatan])
            ->andFilterWhere(['like', 'status_analis', $this->status_analis])
            ->andFilterWhere(['like', 'priority', $this->priority])
            ->andFilterWhere(['like', 'kategori', $this->kategori])
            ->andFilterWhere(['like', 'sop_npd', $this->sop_npd])
            ->andFilterWhere(['like', 'flag_analis', $this->flag_analis]);

        return $dataProvider;
    }
}
