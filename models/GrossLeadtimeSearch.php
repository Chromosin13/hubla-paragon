<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GrossLeadtime;

/**
 * GrossLeadtimeSearch represents the model behind the search form of `app\models\GrossLeadtime`.
 */
class GrossLeadtimeSearch extends GrossLeadtime
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nama_fg', 'snfg', 'tanggal', 'tanggal_stop'], 'safe'],
            [['leadtime_kotor_jam', 'leadtime_kotor_hari'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GrossLeadtime::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tanggal' => $this->tanggal,
            'tanggal_stop' => $this->tanggal_stop,
            'leadtime_kotor_jam' => $this->leadtime_kotor_jam,
            'leadtime_kotor_hari' => $this->leadtime_kotor_hari,
        ]);

        $query->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'snfg', $this->snfg]);

        return $dataProvider;
    }
}
