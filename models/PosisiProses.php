<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posisi_proses".
 *
 * @property string $snfg
 * @property string $posisi
 * @property string $start
 * @property string $due
 */
class PosisiProses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function primaryKey()
    {
        return ['snfg_komponen'];
    }

    public static function tableName()
    {
        return 'posisi_proses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg','snfg_komponen','posisi','status','ontime','nomo','nama_fg'], 'string'],
            [['start', 'due','lanjutan_split_batch','timestamp','delta','time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nomo' => 'Nomor MO',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'SNFG Komponen',
            'posisi' => 'Posisi',
            'status' => 'Status',
            'start' => 'Start',
            'due' => 'Due',
            'ontime' => 'ontime',
            'timestamp' => 'Timestamp',
            'nama_fg' => 'Nama FG',
            'lanjutan_split_batch' => 'SB',
            'delta' => 'Delta',
            'time' => 'Time',
        ];
    }
}
