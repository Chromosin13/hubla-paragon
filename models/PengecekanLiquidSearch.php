<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PengecekanLiquid;

/**
 * PengecekanLiquidSearch represents the model behind the search form about `app\models\PengecekanLiquid`.
 */
class PengecekanLiquidSearch extends PengecekanLiquid
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['uji_ketahanan_seal', 'hasil_seal', 'kondisi_fisik', 'kes_warna_dusat', 'kes_identitas_dusat', 'kes_warna_paper', 'kes_identitas_paper', 'uji_torsi_cap', 'uji_kekencangan_cap', 'hasil_shrink_wrap'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengecekanLiquid::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'uji_ketahanan_seal', $this->uji_ketahanan_seal])
            // ->andFilterWhere(['like', 'hasil_seal', $this->hasil_seal])
            // ->andFilterWhere(['like', 'kondisi_fisik', $this->kondisi_fisik])
            // ->andFilterWhere(['like', 'kes_warna_dusat', $this->kes_warna_dusat])
            // ->andFilterWhere(['like', 'kes_identitas_dusat', $this->kes_identitas_dusat])
            // ->andFilterWhere(['like', 'kes_warna_paper', $this->kes_warna_paper])
            // ->andFilterWhere(['like', 'kes_identitas_paper', $this->kes_identitas_paper])
            // ->andFilterWhere(['like', 'uji_torsi_cap', $this->uji_torsi_cap])
            // ->andFilterWhere(['like', 'uji_kekencangan_cap', $this->uji_kekencangan_cap])
            // ->andFilterWhere(['like', 'hasil_shrink_wrap', $this->hasil_shrink_wrap]);
        ;

        return $dataProvider;
    }
}
