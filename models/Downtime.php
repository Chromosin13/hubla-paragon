<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "downtime".
 *
 * @property string $flow_input_mo_id
 * @property string $jenis
 * @property string $posisi
 * @property string $waktu_start
 * @property string $waktu_stop
 * @property integer $id
 */
class Downtime extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'downtime';
    }


    public function getDurasiMenit() {

        $datetime1 = strtotime($this->waktu_start);
        $datetime2 = strtotime($this->waktu_stop);

        $secs = $datetime2 - $datetime1;// == <seconds between the two times>
        $minutes = $secs / 60;

        return $minutes;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis', 'posisi','keterangan'], 'string'],
            [['flow_input_mo_id','flow_input_snfg_id','flow_input_snfgkomponen_id','flow_pra_kemas_id'],'integer'],
            [['waktu_start', 'waktu_stop','bahan_baku_adjust'], 'safe'],
            [['jenis','keterangan','waktu_start','waktu_stop'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'flow_input_mo_id' => 'Flow Input Mo ID',
            'flow_input_snfg_id' => 'Flow Input Snfg ID',
            'flow_input_snfgkomponen_id' => 'Flow Input Snfg Komponen ID',
            'flow_pra_kemas_id' => 'Flow Pra Kemas ID',
            'jenis' => 'Jenis',
            'posisi' => 'Posisi',
            'waktu_start' => 'Waktu Start',
            'waktu_stop' => 'Waktu Stop',
            'id' => 'ID',
            'keterangan' => 'Keterangan',
            'bahan_baku_adjust' => 'Bahan Baku Adjust',
            'durasiMenit' => Yii::t('app','Durasi Menit'),
        ];
    }
}
