<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "komponen_powder".
 *
 * @property integer $id
 * @property string $nama_komponen
 */
class KomponenPowder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'komponen_powder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_komponen'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_komponen' => 'Nama Komponen',
        ];
    }
}
