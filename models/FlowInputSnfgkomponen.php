<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flow_input_snfgkomponen".
 *
 * @property string $posisi
 * @property integer $lanjutan
 * @property string $datetime_start
 * @property string $datetime_stop
 * @property string $datetime_write
 * @property string $nama_operator
 * @property string $nama_line
 * @property string $jenis_kemas
 * @property integer $is_done
 * @property integer $counter
 * @property string $snfg_komponen
 * @property string $jumlah_realisasi
 * @property string $nobatch
 * @property integer $id
 */
class FlowInputSnfgkomponen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flow_input_snfgkomponen';
    }

    public $sisa_bulk;
    public $fg_retur_layak_pakai;
    public $fg_retur_reject;
    public $netto_1;
    public $netto_2;
    public $netto_3;
    public $netto_4;
    public $netto_5;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['posisi', 'nama_operator', 'nama_line', 'jenis_kemas', 'snfg_komponen', 'nobatch'], 'string'],
            [['sisa_bulk','fg_retur_reject','fg_retur_layak_pakai','netto_1','netto_2','netto_3','netto_4','netto_5'], 'number'],
            [['lanjutan', 'is_done', 'counter'], 'integer'],
            [['datetime_start', 'datetime_stop', 'datetime_write','staging'], 'safe'],
            [['jumlah_realisasi'], 'number'],
            [['jenis_kemas','nama_line','lanjutan','nama_operator'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'posisi' => 'Posisi',
            'lanjutan' => 'Lanjutan',
            'datetime_start' => 'Datetime Start',
            'datetime_stop' => 'Datetime Stop',
            'datetime_write' => 'Datetime Write',
            'nama_operator' => 'Nama Operator',
            'nama_line' => 'Nama Line',
            'jenis_kemas' => 'Jenis Kemas',
            'is_done' => 'Is Done',
            'counter' => 'Counter',
            'snfg_komponen' => 'Snfg Komponen',
            'jumlah_realisasi' => 'Jumlah Realisasi',
            'nobatch' => 'Nobatch',
            'id' => 'ID',
            'staging' => 'Staging',
        ];
    }
}
