<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dim_line".
 *
 * @property string $sediaan
 * @property string $penimbangan
 * @property string $pengolahan
 * @property string $kemas
 * @property integer $id
 */
class DimLine extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dim_line';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sediaan', 'penimbangan', 'pengolahan', 'kemas'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sediaan' => 'Sediaan',
            'penimbangan' => 'Penimbangan',
            'pengolahan' => 'Pengolahan',
            'kemas' => 'Kemas',
            'id' => 'ID',
        ];
    }
}
