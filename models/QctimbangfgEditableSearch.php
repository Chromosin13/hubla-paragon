<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\QctimbangfgEditable;

/**
 * QctimbangfgEditableSearch represents the model behind the search form about `app\models\QctimbangfgEditable`.
 */
class QctimbangfgEditableSearch extends QctimbangfgEditable
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lanjutan_split_batch'], 'integer'],
            [['snfg', 'snfg_komponen', 'tanggal', 'nama_inspektor', 'waktu', 'state', 'status', 'posisi', 'nama_line', 'jenis_periksa', 'timestamp'], 'safe'],
            [['palet_ke', 'qty', 'jumlah_inspektor', 'lanjutan', 'lanjutan_ist'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QctimbangfgEditable::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lanjutan_split_batch' => $this->lanjutan_split_batch,
            'palet_ke' => $this->palet_ke,
            'tanggal' => $this->tanggal,
            'waktu' => $this->waktu,
            'qty' => $this->qty,
            'jumlah_inspektor' => $this->jumlah_inspektor,
            'lanjutan' => $this->lanjutan,
            'timestamp' => $this->timestamp,
            'lanjutan_ist' => $this->lanjutan_ist,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'nama_inspektor', $this->nama_inspektor])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'posisi', $this->posisi])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'jenis_periksa', $this->jenis_periksa]);

        return $dataProvider;
    }
}
