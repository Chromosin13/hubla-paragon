<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogDowntime;

/**
 * LogDowntimeSearch represents the model behind the search form about `app\models\LogDowntime`.
 */
class LogDowntimeSearch extends LogDowntime
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'snfg_komponen', 'nomo', 'jenis_proses', 'interval', 'waktu_start', 'waktu_stop', 'jenis', 'keterangan'], 'safe'],
            [['lanjutan', 'uid'], 'integer'],
            [['interval_dec'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogDowntime::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lanjutan' => $this->lanjutan,
            'interval_dec' => $this->interval_dec,
            'waktu_start' => $this->waktu_start,
            'waktu_stop' => $this->waktu_stop,
            'uid' => $this->uid,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'jenis_proses', $this->jenis_proses])
            ->andFilterWhere(['like', 'interval', $this->interval])
            ->andFilterWhere(['like', 'jenis', $this->jenis])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
