<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DailyConsumptionDefault;

/**
 * SupplierConfirmationSearch represents the model behind the search form about `app\models\SupplierConfirmation`.
 */
class DailyConsumptionDefaultSearch extends DailyConsumptionDefault
{
    // /**
    //  * @return \yii\db\Connection the database connection used by this AR class.
    //  */
    // public static function getDb()
    // {
    //     return Yii::$app->get('db_warehouse');
    // }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['scheduled_start'], 'safe'],
            [['name', 'type', 'reference', 'bom_uom', 'component_code', 'component', 'component_uom', 'total_component_uom', 'nomo', 'fg_name', 'location', 'week'], 'string'],
            [['scheduled_quantity', 'bom_qty', 'component_qty', 'total_component_qty'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DailyConsumptionDefault::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'nomo' => $this->nomo,
            'fg_name' => $this->fg_name,
            'scheduled_quantity' => $this->scheduled_quantity,
            'scheduled_start' => $this->scheduled_start,
            'reference' => $this->reference,
            'week' => $this->week,
            'location' => $this->location,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'fg_name', $this->fg_name])
            ->andFilterWhere(['like', 'scheduled_quantity', $this->scheduled_quantity])
            ->andFilterWhere(['like', 'scheduled_start', $this->scheduled_start])
            ->andFilterWhere(['like', 'reference', $this->reference])
            ->andFilterWhere(['like', 'week', $this->week])
            ->andFilterWhere(['like', 'location', $this->location]);

        return $dataProvider;
    }

    public function searchJadwalLiquid($params)
    {
        $query = DailyConsumptionDefault::find();

        date_default_timezone_set('Asia/Jakarta');
        $ddate = date("Y-m-d");
        $duedt = explode("-", $ddate);
        $date  = mktime(0, 0, 0, $duedt[1], $duedt[2], $duedt[0]);
        $week  = (int)date('W', $date);
        $week_before  = $week-1;
        $week_after  = $week+1;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'scheduled_start' => SORT_ASC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'location' => "J2",
        ]);

        $query->andWhere('
            week >= :week_before
        ')->andWhere('
            week <= :week_after
        ')
        ->params([
            'week_before' => $week_before,
            'week_after' => $week_after,
        ])

        return $dataProvider;
    }

    public function searchJadwalPowder($params)
    {
        $query = DailyConsumptionDefault::find();

        date_default_timezone_set('Asia/Jakarta');
        $ddate = date("Y-m-d");
        $duedt = explode("-", $ddate);
        $date  = mktime(0, 0, 0, $duedt[1], $duedt[2], $duedt[0]);
        $week  = (int)date('W', $date);
        $week_before  = $week-1;
        $week_after  = $week+1;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'scheduled_start' => SORT_ASC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'location' => "J1",
        ]);

        $query->andWhere('
            week >= :week_before
        ')->andWhere('
            week <= :week_after
        ')
        ->params([
            'week_before' => $week_before,
            'week_after' => $week_after,
        ])

        return $dataProvider;
    }
}