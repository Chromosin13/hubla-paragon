<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PosisiProsesScm;

/**
 * PosisiProsesScmSearch represents the model behind the search form of `app\models\PosisiProsesScm`.
 */
class PosisiProsesScmSearch extends PosisiProsesScm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['koitem_fg', 'nama_fg', 'snfg', 'start', 'due', 'posisi', 'status', 'time', 'hit_status','sediaan'], 'safe'],
            [['jumlah_pcs', 'lanjutan_split_batch', 'jumlah_aktual'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PosisiProsesScm::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'start' => $this->start,
            'due' => $this->due,
            'jumlah_pcs' => $this->jumlah_pcs,
            'lanjutan_split_batch' => $this->lanjutan_split_batch,
            'time' => $this->time,
            'jumlah_aktual' => $this->jumlah_aktual,
        ]);

        $query->andFilterWhere(['ilike', 'koitem_fg', $this->koitem_fg])
            ->andFilterWhere(['ilike', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['ilike', 'snfg', $this->snfg])
            ->andFilterWhere(['ilike', 'posisi', $this->posisi])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'sediaan', $this->sediaan])
            ->andFilterWhere(['ilike', 'hit_status', $this->hit_status]);

        return $dataProvider;
    }
}
