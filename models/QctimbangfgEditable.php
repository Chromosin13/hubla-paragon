<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "qctimbangfg_editable".
 *
 * @property integer $id
 * @property string $snfg
 * @property string $snfg_komponen
 * @property integer $lanjutan_split_batch
 * @property string $palet_ke
 * @property string $tanggal
 * @property string $nama_inspektor
 * @property string $waktu
 * @property string $state
 * @property string $qty
 * @property string $status
 * @property string $posisi
 * @property string $jumlah_inspektor
 * @property string $nama_line
 * @property string $jenis_periksa
 * @property string $lanjutan
 * @property string $timestamp
 * @property string $lanjutan_ist
 */
class QctimbangfgEditable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qctimbangfg_editable';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lanjutan_split_batch'], 'integer'],
            [['snfg', 'snfg_komponen', 'nama_inspektor', 'state', 'status', 'posisi', 'nama_line', 'jenis_periksa'], 'string'],
            [['palet_ke', 'qty', 'jumlah_inspektor', 'lanjutan', 'lanjutan_ist'], 'number'],
            [['tanggal', 'waktu', 'timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'lanjutan_split_batch' => 'Lanjutan Split Batch',
            'palet_ke' => 'Palet Ke',
            'tanggal' => 'Tanggal',
            'nama_inspektor' => 'Nama Inspektor',
            'waktu' => 'Waktu',
            'state' => 'State',
            'qty' => 'Qty',
            'status' => 'Status',
            'posisi' => 'Posisi',
            'jumlah_inspektor' => 'Jumlah Inspektor',
            'nama_line' => 'Nama Line',
            'jenis_periksa' => 'Jenis Periksa',
            'lanjutan' => 'Lanjutan',
            'timestamp' => 'Timestamp',
            'lanjutan_ist' => 'Lanjutan Ist',
        ];
    }
}
