<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SuratJalanRincian;

/**
 * SuratJalanRincianSearch represents the model behind the search form of `app\models\SuratJalanRincian`.
 */
class SuratJalanRincianSearch extends SuratJalanRincian
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'surat_jalan_id', 'nourut','jumlah_koli'], 'integer'],
            [['snfg', 'nosj','nobatch'], 'safe'],
            [['qty'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SuratJalanRincian::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'surat_jalan_id' => $this->surat_jalan_id,
            'nourut' => $this->nourut,
            'jumlah_koli' => $this->jumlah_koli,
            'qty' => $this->qty,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'nobatch', $this->nobatch])
            ->andFilterWhere(['like', 'nosj', $this->nosj]);

        return $dataProvider;
    }
}
