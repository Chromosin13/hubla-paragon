<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monitoring_line_penimbangan".
 *
 * @property string $sediaan
 * @property string $nama_line
 * @property string $nama_fg
 * @property string $nomo
 * @property string $datetime_write
 * @property string $jenis_proses
 * @property string $status
 * @property integer $id
 */
class MonitoringLinePenimbangan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'monitoring_line_penimbangan';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sediaan', 'nama_line', 'nama_fg', 'nomo', 'jenis_proses', 'status'], 'string'],
            [['datetime_write'], 'safe'],
            [['id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sediaan' => 'Sediaan',
            'nama_line' => 'Nama Line',
            'nama_fg' => 'Nama Fg',
            'nomo' => 'Nomo',
            'datetime_write' => 'Datetime Write',
            'jenis_proses' => 'Jenis Proses',
            'status' => 'Status',
            'id' => 'ID',
        ];
    }
}
