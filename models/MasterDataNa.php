<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_data_na".
 *
 * @property integer $id
 * @property string $koitem
 * @property string $na_number
 */
class MasterDataNa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_data_na';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['koitem', 'nama_item', 'updated_by', 'na_number'], 'string'],
            [['timestamp'], 'safe'],
            [['koitem', 'na_number'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'koitem' => 'Kode Item',
            'na_number' => 'Nomor NA',
            'nama_item' => 'Nama Item',
            'timestamp' => 'Timestamp',
            'updated_by' => 'Updated By'
        ];
    }
}
