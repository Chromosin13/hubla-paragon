<?php

namespace app\models;

use yii\base\Model;

class ViTimeStamp extends Model {
    private $db;

    public function __construct() {
        $this->db = \Yii::$app->db_vi;
    }

    public function addData($param) {
        $this->counter = $this->counter + 1;
        date_default_timezone_set('Asia/Jakarta');
        return $this->db->createCommand()->insert("{{%time_stamp}}", [
            'kode_sku' => $param['kode_sku'],
            'jalur_kemas' => $param['jalur_kemas'],
            'time_stamp' => date("Y-m-d H:i:s"),
            'jumlah_defect_sekunder' => random_int(0,5),
            ])->execute();
    }

    public function addInspectionResult($param1, $param2) {
        return $this->db->createCommand()->insert("spk_time_stamp", [
            'snfg' => $param1['snfg'],
            'time_stamp' => $param2['time_stamp'],
            'inspeksi_sekunder_ke' => $param2['inspeksi_sekunder_ke'],
            'id_schedule' => $param2['id_schedule'],
            'is_stop' => filter_var($param2['is_stop'], FILTER_VALIDATE_BOOLEAN)
            ])->execute();
    }
    
    public function getProductTimeStampBy($column, $value) {
        
        $query = 'SELECT * FROM {{%info}} WHERE ' . $column . '=:' . $column;
        
        return $this->db->createCommand($query)
            ->bindValue($column, $value)
            ->queryAll();
    }
    
    public function getAllTimeStampProduct() {
        $query = 'SELECT * FROM {{%info}}';

        return $this->db->createCommand($query)
            ->queryAll();
    }

    public function getCurrentHistoryData($param) {
        $query = 'SELECT * 
                    FROM spk_time_stamp
                    WHERE id_schedule=:id
                    ORDER BY time_stamp
                    ';

        return $this->db->createCommand($query)
            ->bindValue('id', $param['id'])
            ->queryAll  ();
    }

    public function getPeriodicInspectionResult($param) {
        $query = "SELECT * 
                    FROM (
                        SELECT *
                        FROM (
                            SELECT 
                                id_schedule,
                                time_only_stamp,
                                periodic_time_stamp
                            FROM (
                                SELECT 
                                    id_schedule,
                                    to_char(
                                        generate_series(
                                            MIN(time_stamp), 
                                            MAX(time_stamp), 
                                            INTERVAL '". $param['period'] . "'
                                        ), 
                                        'HH24:MI'
                                    ) AS time_only_stamp,
                    
                                    generate_series(
                                        MIN(time_stamp), 
                                        MAX(time_stamp), 
                                        INTERVAL '". $param['period'] . "'
                                    ) AS periodic_time_stamp
                    
                                FROM spk_time_stamp
                                WHERE id_schedule=:id_schedule
                                GROUP BY 1
                            ) AS t1
                    
                            INNER JOIN (
                                SELECT *
                                FROM flow_input_snfg
                                WHERE id=:id
                            ) AS t2
                            ON
                                t1.id_schedule = t2.id
                                AND t1.periodic_time_stamp > t2.datetime_start
                        ) AS j1
                    ) grid
                    
                    CROSS JOIN LATERAL (
                        SELECT 
                            jenis_defect,
                            COUNT(id) AS kumulatif_periodik_defect_sekunder,
                            MAX(inspeksi_sekunder_ke) AS counter_inspeksi
                        FROM spk_time_stamp
                        WHERE 
                            is_stop=false
                            AND id_schedule = grid.id_schedule
                            AND time_stamp >= grid.periodic_time_stamp
                            AND time_stamp < grid.periodic_time_stamp + INTERVAL '". $param['period'] . "'
                        GROUP BY jenis_defect
                    ) AS periodic
                    ORDER BY grid.periodic_time_stamp ASC
                    ";
                
        return $this->db->createCommand($query)
            ->bindValues([
                'id_schedule' => $param['id'],
                'id' => $param['id']
            ])
            ->queryAll();
    }

    public function getHistoryData($param) {
        $query = 'SELECT * FROM spk_time_stamp
                     WHERE id_schedule=:id
                     ORDER BY time_stamp
                    ';

        return $this->db->createCommand($query)
        ->bindValue(':id', $param['id'])     
        ->queryAll();
    }

}