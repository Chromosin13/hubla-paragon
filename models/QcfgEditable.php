<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "qcfg_editable".
 *
 * @property integer $id
 * @property string $snfg
 * @property string $snfg_komponen
 * @property integer $lanjutan_split_batch
 * @property string $jenis_periksa
 * @property string $status
 * @property string $state
 * @property string $waktu
 * @property string $posisi
 * @property string $timestamp
 * @property string $aql
 * @property string $filling_kesesuaian_bulk
 * @property string $filling_kesesuaian_packaging_primer
 * @property string $filling_netto
 * @property string $filling_seal
 * @property string $filling_leakage
 * @property string $filling_warna_olesan
 * @property string $filling_warna_performance
 * @property string $filling_uji_ayun
 * @property string $filling_uji_oles
 * @property string $filling_uji_ketrok
 * @property string $filling_drop_test
 * @property string $filling_rub_test
 * @property string $filling_identitas_packaging_primer
 * @property string $filling_identitas_stc_bottom
 * @property string $packing_kesesuaian_packaging_sekunder
 * @property string $packing_qty_inner_box
 * @property string $packing_identitas_unit_box
 * @property string $packing_identitas_inner_box
 * @property string $packing_performance_segel
 * @property string $packing_posisi_packing
 * @property string $paletting_qty_karton_box
 * @property string $paletting_identitas_karton_box
 * @property string $retain_sample
 */
class QcfgEditable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qcfg_editable';
    }

      public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lanjutan_split_batch'], 'integer'],
            [['snfg', 'snfg_komponen', 'jenis_periksa', 'status', 'state', 'posisi', 'aql', 'filling_kesesuaian_bulk', 'filling_kesesuaian_packaging_primer', 'filling_netto', 'filling_seal', 'filling_leakage', 'filling_warna_olesan', 'filling_warna_performance', 'filling_uji_ayun', 'filling_uji_oles', 'filling_uji_ketrok', 'filling_drop_test', 'filling_rub_test', 'filling_identitas_packaging_primer', 'filling_identitas_stc_bottom', 'packing_kesesuaian_packaging_sekunder', 'packing_identitas_unit_box', 'packing_identitas_inner_box', 'packing_performance_segel', 'packing_posisi_packing', 'paletting_identitas_karton_box'], 'string'],
            [['waktu', 'timestamp'], 'safe'],
            [['packing_qty_inner_box', 'paletting_qty_karton_box', 'retain_sample','qty'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'lanjutan_split_batch' => 'Lanjutan Split Batch',
            'jenis_periksa' => 'Jenis Periksa',
            'status' => 'Status',
            'state' => 'State',
            'waktu' => 'Waktu',
            'posisi' => 'Posisi',
            'timestamp' => 'Timestamp',
            'aql' => 'Aql',
            'filling_kesesuaian_bulk' => 'Filling Kesesuaian Bulk',
            'filling_kesesuaian_packaging_primer' => 'Filling Kesesuaian Packaging Primer',
            'filling_netto' => 'Filling Netto',
            'filling_seal' => 'Filling Seal',
            'filling_leakage' => 'Filling Leakage',
            'filling_warna_olesan' => 'Filling Warna Olesan',
            'filling_warna_performance' => 'Filling Warna Performance',
            'filling_uji_ayun' => 'Filling Uji Ayun',
            'filling_uji_oles' => 'Filling Uji Oles',
            'filling_uji_ketrok' => 'Filling Uji Ketrok',
            'filling_drop_test' => 'Filling Drop Test',
            'filling_rub_test' => 'Filling Rub Test',
            'filling_identitas_packaging_primer' => 'Filling Identitas Packaging Primer',
            'filling_identitas_stc_bottom' => 'Filling Identitas Stc Bottom',
            'packing_kesesuaian_packaging_sekunder' => 'Packing Kesesuaian Packaging Sekunder',
            'packing_qty_inner_box' => 'Packing Qty Inner Box',
            'packing_identitas_unit_box' => 'Packing Identitas Unit Box',
            'packing_identitas_inner_box' => 'Packing Identitas Inner Box',
            'packing_performance_segel' => 'Packing Performance Segel',
            'packing_posisi_packing' => 'Packing Posisi Packing',
            'paletting_qty_karton_box' => 'Paletting Qty Karton Box',
            'paletting_identitas_karton_box' => 'Paletting Identitas Karton Box',
            'retain_sample' => 'Retain Sample',
            'qty' => 'Quantity',
        ];
    }
}
