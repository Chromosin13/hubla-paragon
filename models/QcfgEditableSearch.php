<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\QcfgEditable;

/**
 * QcfgEditableSearch represents the model behind the search form about `app\models\QcfgEditable`.
 */
class QcfgEditableSearch extends QcfgEditable
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lanjutan_split_batch'], 'integer'],
            [['snfg', 'snfg_komponen', 'jenis_periksa', 'status', 'state', 'waktu', 'posisi', 'timestamp', 'aql', 'filling_kesesuaian_bulk', 'filling_kesesuaian_packaging_primer', 'filling_netto', 'filling_seal', 'filling_leakage', 'filling_warna_olesan', 'filling_warna_performance', 'filling_uji_ayun', 'filling_uji_oles', 'filling_uji_ketrok', 'filling_drop_test', 'filling_rub_test', 'filling_identitas_packaging_primer', 'filling_identitas_stc_bottom', 'packing_kesesuaian_packaging_sekunder', 'packing_identitas_unit_box', 'packing_identitas_inner_box', 'packing_performance_segel', 'packing_posisi_packing', 'paletting_identitas_karton_box'], 'safe'],
            [['packing_qty_inner_box', 'paletting_qty_karton_box', 'retain_sample', 'qty'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QcfgEditable::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lanjutan_split_batch' => $this->lanjutan_split_batch,
            'waktu' => $this->waktu,
            'timestamp' => $this->timestamp,
            'packing_qty_inner_box' => $this->packing_qty_inner_box,
            'paletting_qty_karton_box' => $this->paletting_qty_karton_box,
            'retain_sample' => $this->retain_sample,
            'qty' => $this->qty,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'jenis_periksa', $this->jenis_periksa])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'posisi', $this->posisi])
            ->andFilterWhere(['like', 'aql', $this->aql])
            ->andFilterWhere(['like', 'filling_kesesuaian_bulk', $this->filling_kesesuaian_bulk])
            ->andFilterWhere(['like', 'filling_kesesuaian_packaging_primer', $this->filling_kesesuaian_packaging_primer])
            ->andFilterWhere(['like', 'filling_netto', $this->filling_netto])
            ->andFilterWhere(['like', 'filling_seal', $this->filling_seal])
            ->andFilterWhere(['like', 'filling_leakage', $this->filling_leakage])
            ->andFilterWhere(['like', 'filling_warna_olesan', $this->filling_warna_olesan])
            ->andFilterWhere(['like', 'filling_warna_performance', $this->filling_warna_performance])
            ->andFilterWhere(['like', 'filling_uji_ayun', $this->filling_uji_ayun])
            ->andFilterWhere(['like', 'filling_uji_oles', $this->filling_uji_oles])
            ->andFilterWhere(['like', 'filling_uji_ketrok', $this->filling_uji_ketrok])
            ->andFilterWhere(['like', 'filling_drop_test', $this->filling_drop_test])
            ->andFilterWhere(['like', 'filling_rub_test', $this->filling_rub_test])
            ->andFilterWhere(['like', 'filling_identitas_packaging_primer', $this->filling_identitas_packaging_primer])
            ->andFilterWhere(['like', 'filling_identitas_stc_bottom', $this->filling_identitas_stc_bottom])
            ->andFilterWhere(['like', 'packing_kesesuaian_packaging_sekunder', $this->packing_kesesuaian_packaging_sekunder])
            ->andFilterWhere(['like', 'packing_identitas_unit_box', $this->packing_identitas_unit_box])
            ->andFilterWhere(['like', 'packing_identitas_inner_box', $this->packing_identitas_inner_box])
            ->andFilterWhere(['like', 'packing_performance_segel', $this->packing_performance_segel])
            ->andFilterWhere(['like', 'packing_posisi_packing', $this->packing_posisi_packing])
            ->andFilterWhere(['like', 'paletting_identitas_karton_box', $this->paletting_identitas_karton_box]);

        return $dataProvider;
    }
}
