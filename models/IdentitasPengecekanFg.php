<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "identitas_pengecekan_fg".
 *
 * @property integer $id
 * @property string $nama_fg
 * @property string $keterangan
 * @property string $batch_fg
 * @property string $serial_number
 * @property string $no_mo
 * @property string $exp_date
 * @property string $due
 * @property string $plant
 * @property string $line_kemas
 * @property integer $jumlah_mpq
 * @property integer $total_aql
 * @property string $kode_sl
 * @property string $no_notifikasi
 * @property string $nama_produk_sebelumnya
 * @property string $batch_produk_sebelumnya
 * @property string $air_bilasan_akhir
 * @property string $verifikasi_trial_filling
 * @property string $label_bersih_bulk
 * @property string $label_bersih_mesin
 */
class IdentitasPengecekanFg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'identitas_pengecekan_fg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg','nama_fg', 'keterangan', 'batch_fg', 'no_mo', 'plant', 'line_kemas', 'no_notifikasi', 'nama_produk_sebelumnya', 'batch_produk_sebelumnya', 'label_bersih_mesin','line_clearance','sediaan'], 'string'],
            [['exp_date', 'due'], 'safe'],
            [['jumlah_mpq', 'total_aql','jumlah_palet','id_pengecekan_umum'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => '1.1 SNFG',
            'sediaan' => 'Sediaan',
            'nama_fg' => '1.2 Nama Finished Goods',
            'keterangan' => '1.3 Keterangan',
            'batch_fg' => '1.4 Batch FG',
            // 'serial_number' => 'Serial Number',
            'no_mo' => '1.5 No MO',
            'exp_date' => '1.6 Exp Date (yyyy/mm/dd)',
            'due' => '1.7 Due Date (yyyy/mm/dd)',
            'plant' => '1.8 Plant',
            'line_kemas' => '1.9 Line Kemas',
            'jumlah_mpq' => '1.16 Jumlah MPQ (pcs)',
            'total_aql' => '1.19 Total AQL (pcs)',
            // 'kode_sl' => 'Kode SL',
            'no_notifikasi' => '1.10 No. Notifikasi (NA Number)',
            'nama_produk_sebelumnya' => '1.11 Nama Produk Sebelumnya',
            'batch_produk_sebelumnya' => '1.12 Batch Produk Sebelumnya',
            'label_bersih_mesin' => '1.14 Label Bersih Mesin',
            'jumlah_palet' => '1.18 Jumlah Palet',
            'id_pengecekan_umum' => 'Id Pengecekan Umum',
            'line_clearance' => '1.15 Line Clearance',
        ];
    }
}
