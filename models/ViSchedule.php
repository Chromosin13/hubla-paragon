<?php

namespace app\models;

use yii\base\Model;

class ViSchedule extends Model {
    const SCENARIO_CREATE = 'create';
    
    private $db;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'flow_input_snfg';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_sku', 'jalur_kemas'], 'required'],
            [['start_time', 'end_time'], 'safe'],
            [['jumlah_defect_sekunder'], 'default', 'value' => null],
            [['jumlah_defect_sekunder'], 'integer'],
            [['kode_sku'], 'string', 'max' => 20],
            [['jalur_kemas', 'status_inspeksi'], 'string', 'max' => 10],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'SNFG',
            'fg_name_odoo' => 'Jalur Kemas',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'status_inspeksi' => 'Status Inspeksi',
            'jumlah_defect_sekunder' => 'Jumlah Defect Sekunder',
        ];
    }
    
    public function __construct() {
        $this->db = \Yii::$app->db_vi;
    }
    
    public function getDoneSchedule() {
        $query_dep = 'SELECT
                            *,
                            ROUND((jumlah_defect_sekunder/inspeksi_sekunder_ke::DECIMAL)*100, 2) AS defect_rate
                        FROM (
                            SELECT
                                t1.id,
                                snfg,
                                fg_name_odoo,
                                nama_line,
                                datetime_diff,
                                datetime_start,
                                datetime_stop
                            FROM (
                                SELECT
                                    id,
                                    datetime_stop - datetime_start AS datetime_diff
                                FROM flow_input_snfg
                                WHERE
                                    datetime_stop IS NOT NULL
                                GROUP BY 1
                            ) AS t1

                            INNER JOIN (
                                SELECT * FROM flow_input_snfg
                            ) AS t2
                            ON
                                t1.id = t2.id
                                AND t1.datetime_diff >:datetime_diff
                        ) AS j1

                        INNER JOIN (
                            SELECT 
                                id_schedule,
                                COUNT(id_schedule)-1 AS jumlah_defect_sekunder,
                                MAX(inspeksi_sekunder_ke) AS inspeksi_sekunder_ke
                            FROM spk_time_stamp
                            GROUP BY id_schedule
                        ) AS t3
                        ON j1.id = t3.id_schedule
                        ';

        return $this->db->createCommand($query_dep)
            ->bindValue('datetime_diff', '00:10:00')
            ->queryAll();
    }

    public function getAllProductScheduleApi() {
        $query = 'SELECT * FROM flow_input_snfg';

        $products = $this->db->createCommand($query)
                        ->queryAll();
        return $products;
    }

    public function addRecord($param) {
        date_default_timezone_set('Asia/Jakarta');
        return $this->db->createCommand()->insert("{{%schedule}}", [
            'kode_snfg' => $param['kode_snfg'],
            'kode_sku' => $param['kode_sku'],
            'jalur_kemas' => $param['jalur_kemas'],
            'start_time' => date("Y-m-d H:i:s"),
            'status_inspeksi' => 'start'
        ])->execute();
    }

    public function updateRecord($param) {
        date_default_timezone_set('Asia/Jakarta');
        return $this->db->createCommand()->update("{{%schedule}}", [
            'end_time' => date("Y-m-d H:i:s"),
            'jumlah_defect_sekunder' => $param['jumlah_defect_sekunder'],
            'status_inspeksi' => 'end'
        ],[
            'id' => $param['id'],
        ])->execute();
    }

    public function deleteNull($param) {
        return $this->db->createCommand()->delete('{{%schedule}}', 'id!=:id', [
            'id' => $param,
        ])->execute();
    }

    public function getProductScheduleBy($column, $value) {
        
        $query = 'SELECT * 
                    FROM flow_input_snfg
                    WHERE ' . $column . '=:' . $column
                    ;
        
        $products = $this->db->createCommand($query)
            ->bindValue($column, $value)
            ->queryOne();

        return $products;
    }

    public function getLastId() {
        $query = "SELECT MAX(ID) FROM {{%schedule}}";

        return $this->db->createCommand($query)
            ->queryOne();
    }

    public function getProductScheduleNull($param) {
        $query = "SELECT * FROM {{%schedule}}
                    WHERE end_time IS NULL
                    AND jalur_kemas=:jalur_kemas
                    ";

        return $this->db->createCommand($query)
            ->bindValue('jalur_kemas', $param)
            ->queryOne();
    }

    public function getProductScheduleNullAll() {
        $query = "SELECT * FROM {{%schedule}}
                    WHERE end_time IS NULL
                    ";

        return $this->db->createCommand($query)
            ->queryAll();
    }

    public function getRunningSchedule($param) {
        $query = "SELECT *
                    FROM flow_input_snfg
                    WHERE
                        id=(
                            SELECT MAX(id)
                            FROM flow_input_snfg
                            WHERE
                                datetime_stop IS NULL
                                AND nama_line=:nama_line
                        )
                    ";

        return $this->db->createCommand($query)
                    ->bindValue('nama_line', $param)
                    ->queryOne();
    }


    public function getItemCode($param) {
        $query="SELECT *,
                    SPLIT_PART(snfg, '/', 1)
                AS koitem
                FROM flow_input_snfg
                WHERE id=:id
                LIMIT 1
                ";
        return $this->db->createCommand($query)
            ->bindValue('id', $param)
            ->queryOne();
    }

    public function getUser() {
        $query = 'SELECT * FROM user_slp ORDER BY id';
        return $this->db->createCommand($query)
            ->queryAll();
    }
}