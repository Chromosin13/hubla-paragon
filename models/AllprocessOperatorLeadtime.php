<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "allprocess_operator_leadtime".
 *
 * @property integer $row_number
 * @property string $posisi
 * @property string $snfg
 * @property string $snfg_komponen
 * @property string $jenis_proses
 * @property string $lanjutan
 * @property string $nama
 * @property string $tanggal
 * @property string $leadtime
 */
class AllprocessOperatorLeadtime extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['row_number'];
    }

    public static function tableName()
    {
        return 'allprocess_operator_leadtime';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row_number'], 'integer'],
            [['posisi', 'snfg', 'snfg_komponen', 'jenis_proses', 'nama'], 'string'],
            [['lanjutan', 'leadtime'], 'number'],
            [['tanggal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'row_number' => 'Row Number',
            'posisi' => 'Posisi',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'jenis_proses' => 'Jenis Proses',
            'lanjutan' => 'Lanjutan',
            'nama' => 'Nama',
            'tanggal' => 'Tanggal',
            'leadtime' => 'Leadtime',
        ];
    }
}
