<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "status_jadwal".
 *
 * @property integer $id
 * @property string $nojadwal
 * @property integer $status
 * @property string $table
 * @property string $posisi
 * @property string $timestamp
 */
class StatusJadwal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status_jadwal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nojadwal', 'table', 'posisi'], 'string'],
            [['status'], 'integer'],
            [['timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nojadwal' => 'Nojadwal',
            'status' => 'Status',
            'table' => 'Table',
            'posisi' => 'Posisi',
            'timestamp' => 'Timestamp',
        ];
    }
}
