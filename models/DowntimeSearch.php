<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Downtime;

/**
 * DowntimeSearch represents the model behind the search form of `app\models\Downtime`.
 */
class DowntimeSearch extends Downtime
{
    /**
     * @inheritdoc
     */

    /* your calculated attribute */
    public $durasiMenit;


    public function rules()
    {
        return [
            [['jenis', 'posisi', 'waktu_start', 'waktu_stop','durasiMenit','bahan_baku_adjust'], 'safe'],
            [['id','flow_input_mo_id','flow_input_snfg_id','flow_input_snfgkomponen_id','flow_pra_kemas_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Downtime::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'waktu_start' => $this->waktu_start,
            'waktu_stop' => $this->waktu_stop,
            'bahan_baku_adjust' => $this->bahan_baku_adjust,
            'id' => $this->id,
            'flow_input_mo_id' => $this->flow_input_mo_id,
            'flow_input_snfgkomponen_id' => $this->flow_input_snfgkomponen_id,
            'flow_input_snfg_id' => $this->flow_input_snfg_id,
            'flow_pra_kemas_id' => $this->flow_pra_kemas_id,
        ]);

        $query->andFilterWhere(['like', 'jenis', $this->jenis])
            ->andFilterWhere(['like', 'posisi', $this->posisi]);

        return $dataProvider;
    }
}
