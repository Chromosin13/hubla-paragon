<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IdentitasPengecekanFg;

/**
 * IdentitasPengecekanFgSearch represents the model behind the search form about `app\models\IdentitasPengecekanFg`.
 */
class IdentitasPengecekanFgSearch extends IdentitasPengecekanFg
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'jumlah_mpq', 'total_aql'], 'integer'],
            [['nama_fg', 'keterangan', 'batch_fg', 'serial_number', 'no_mo', 'exp_date', 'due_date', 'plant', 'line_kemas', 'kode_sl', 'no_notifikasi', 'nama_produk_sebelumnya', 'batch_produk_sebelumnya', 'air_bilasan_akhir', 'verifikasi_trial_filling', 'label_bersih_bulk', 'label_bersih_mesin'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IdentitasPengecekanFg::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'exp_date' => $this->exp_date,
            'due_date' => $this->due_date,
            'jumlah_mpq' => $this->jumlah_mpq,
            'total_aql' => $this->total_aql,
        ]);

        $query->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'batch_fg', $this->batch_fg])
            ->andFilterWhere(['like', 'serial_number', $this->serial_number])
            ->andFilterWhere(['like', 'no_mo', $this->no_mo])
            ->andFilterWhere(['like', 'plant', $this->plant])
            ->andFilterWhere(['like', 'line_kemas', $this->line_kemas])
            ->andFilterWhere(['like', 'kode_sl', $this->kode_sl])
            ->andFilterWhere(['like', 'no_notifikasi', $this->no_notifikasi])
            ->andFilterWhere(['like', 'nama_produk_sebelumnya', $this->nama_produk_sebelumnya])
            ->andFilterWhere(['like', 'batch_produk_sebelumnya', $this->batch_produk_sebelumnya])
            ->andFilterWhere(['like', 'air_bilasan_akhir', $this->air_bilasan_akhir])
            ->andFilterWhere(['like', 'verifikasi_trial_filling', $this->verifikasi_trial_filling])
            ->andFilterWhere(['like', 'label_bersih_bulk', $this->label_bersih_bulk])
            ->andFilterWhere(['like', 'label_bersih_mesin', $this->label_bersih_mesin]);

        return $dataProvider;
    }
}
