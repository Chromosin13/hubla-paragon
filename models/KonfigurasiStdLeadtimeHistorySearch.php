<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KonfigurasiStdLeadtimeHistory;

/**
 * KonfigurasiStdLeadtimeHistorySearch represents the model behind the search form of `app\models\KonfigurasiStdLeadtimeHistory`.
 */
class KonfigurasiStdLeadtimeHistorySearch extends KonfigurasiStdLeadtimeHistory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'uid'], 'integer'],
            [['koitem', 'koitem_bulk', 'naitem', 'std_type', 'last_update', 'jenis'], 'safe'],
            [['value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KonfigurasiStdLeadtimeHistory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'last_update' => $this->last_update,
            'value' => $this->value,
            'uid' => $this->uid,
        ]);

        $query->andFilterWhere(['like', 'koitem', $this->koitem])
            ->andFilterWhere(['like', 'koitem_bulk', $this->koitem_bulk])
            ->andFilterWhere(['like', 'naitem', $this->naitem])
            ->andFilterWhere(['like', 'std_type', $this->std_type])
            ->andFilterWhere(['like', 'jenis', $this->jenis]);

        return $dataProvider;
    }
}
