<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pengolahan_batch".
 *
 * @property string $nobatch
 * @property string $besar_batch_real
 * @property integer $pengolahan_id
 * @property string $snfg_komponen
 * @property integer $id
 *
 * @property FlowInputMo $pengolahan
 */
class PengolahanBatch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengolahan_batch';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nobatch', 'besar_batch_real'], 'required'],
            [['nobatch', 'snfg_komponen'], 'string'],
            [['besar_batch_real','besar_batch_teoritis','sisa_bulk'], 'number'],
            [['pengolahan_id'], 'integer'],
            [['pengolahan_id'], 'exist', 'skipOnError' => true, 'targetClass' => FlowInputMo::className(), 'targetAttribute' => ['pengolahan_id' => 'id']],
            ['besar_batch_real','checkRealisasi'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nobatch' => 'Nomor Batch',
            'besar_batch_real' => 'Besar Batch Real (Kg)',
            'pengolahan_id' => 'Pengolahan ID',
            'snfg_komponen' => 'SNFG Komponen',
            'besar_batch_teoritis' => 'Standar MPQ Olah Fix (Kg)',
            'sisa_bulk' => 'Sisa Bulk',
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengolahan()
    {
        return $this->hasOne(FlowInputMo::className(), ['id' => 'pengolahan_id']);
    }

    public function checkRealisasi($attribute,$params,$validator)
    {
        if ($this->$attribute > 1.5*$this->besar_batch_teoritis || $this->$attribute< 0.5*$this->besar_batch_teoritis) {
            $validator->addError($this,$attribute, 'The country must be either "USA" or "Indonesia".');
        }
        // if (true){
          // $this->addError('besar_batch_real','Berhasil');
          // $validator->addError($this, $attribute, 'Berhasil');
          // return false;
        // }
    }
}
