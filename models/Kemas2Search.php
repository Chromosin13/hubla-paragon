<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kemas2;

/**
 * Kemas2Search represents the model behind the search form about `app\models\Kemas2`.
 */
class Kemas2Search extends Kemas2
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['snfg', 'nama_line', 'nama_operator', 'waktu', 'state','jenis_kemas','snfg_komponen','timestamp','nobatch'], 'safe'],
            [['jumlah_plan', 'jumlah_realisasi','lanjutan','jumlah_operator','lanjutan_ist','is_done','batch_split','lanjutan_split_batch','last_status','counter'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kemas2::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'jumlah_plan' => $this->jumlah_plan,
            'jumlah_realisasi' => $this->jumlah_realisasi,
            'jumlah_operator' => $this->jumlah_operator,
            'lanjutan' => $this->lanjutan,
            'waktu' => $this->waktu,
            'timestamp' => $this->timestamp,
            'lanjutan_ist'=> $this->lanjutan_ist,
            'is_done'=> $this->is_done,
            'batch_split' => $this->batch_split,
            'lanjutan_split_batch' => $this->lanjutan_split_batch,
            'last_status' => $this->last_status,
            'counter' => $this->counter,

        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator])
            ->andFilterWhere(['like', 'jenis_kemas', $this->jenis_kemas])
            ->andFilterWhere(['like', 'nobatch', $this->nobatch])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'state', $this->state]);
            
        return $dataProvider;
    }
}
