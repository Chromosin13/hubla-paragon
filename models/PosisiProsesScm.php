<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posisi_proses_scm".
 *
 * @property string $koitem_fg
 * @property string $nama_fg
 * @property string $snfg
 * @property string $start
 * @property string $due
 * @property string $posisi
 * @property integer $jumlah_pcs
 * @property string $status
 * @property integer $lanjutan_split_batch
 * @property string $time
 * @property integer $jumlah_aktual
 * @property string $hit_status
 */
class PosisiProsesScm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function primaryKey()
    {
        return ['snfg'];
    }


    public static function tableName()
    {
        return 'posisi_proses_scm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['koitem_fg', 'nama_fg', 'snfg', 'posisi', 'status', 'hit_status','sediaan'], 'string'],
            [['start', 'due', 'time'], 'safe'],
            [['jumlah_pcs', 'lanjutan_split_batch', 'jumlah_aktual'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'koitem_fg' => 'Koitem Fg',
            'nama_fg' => 'Nama Fg',
            'snfg' => 'Snfg',
            'start' => 'Start',
            'due' => 'Due',
            'posisi' => 'Posisi',
            'jumlah_pcs' => 'Jumlah Pcs',
            'status' => 'Status',
            'lanjutan_split_batch' => 'Lanjutan Split Batch',
            'time' => 'Time',
            'jumlah_aktual' => 'Jumlah Aktual',
            'hit_status' => 'Hit Status',
            'sediaan' => 'Sediaan',
        ];
    }
}
