<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_data_formula".
 *
 * @property integer $id
 * @property string $no_formula
 * @property integer $jml_revisi
 * @property string $koitem_fg
 * @property string $naitem_fg
 * @property string $kode_bulk
 * @property string $nama_bulk
 */
class MasterDataFormula extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_data_formula';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_formula', 'koitem_fg', 'naitem_fg', 'kode_bulk', 'nama_bulk'], 'string'],
            [['jml_revisi'], 'integer'],
        ];
    }

    /**
     * @inheritdoc 
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_formula' => 'No Formula',
            'jml_revisi' => 'Jml Revisi',
            'koitem_fg' => 'Koitem Fg',
            'naitem_fg' => 'Naitem Fg',
            'kode_bulk' => 'Kode Bulk',
            'nama_bulk' => 'Nama Bulk',
        ];
    }

    // Check the formula number and revision number. Is it Exist? 
    public function isFormulaExist($no_formula,$no_revisi)
    {
        $sql="select count(distinct b.no_formula)
              from master_data_formula as a inner join bill_of_materials as b
              on a.no_formula='".$no_formula."' and a.no_formula=b.no_formula and b.no_revisi = ".$no_revisi."";
        $planner= MasterDataFormula::findBySql($sql)->scalar();
        return $planner;
    }
}
