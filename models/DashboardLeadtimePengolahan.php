<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dashboard_leadtime_pengolahan".
 *
 * @property string $nama_fg
 * @property string $nomo
 * @property string $posisi
 * @property string $start
 * @property string $stop
 * @property string $sum_leadtime_raw
 * @property string $sum_leadtime_bruto
 * @property string $jenis_proses
 * @property string $sediaan
 * @property string $sum_downtime
 * @property string $sum_adjust
 * @property string $sum_leadtime_net
 * @property string $keterangan_downtime
 * @property string $keterangan_adjust
 * @property string $snfg
 * @property string $snfg_komponen
 * @property integer $id
 */
class DashboardLeadtimePengolahan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dashboard_leadtime_pengolahan';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_fg', 'nomo', 'posisi', 'sum_leadtime_raw', 'sum_leadtime_bruto', 'jenis_proses', 'sediaan', 'sum_downtime', 'sum_adjust', 'sum_leadtime_net', 'keterangan_downtime', 'keterangan_adjust', 'snfg', 'snfg_komponen','nama_line','status','time_release'], 'string'],
            [['start', 'stop','upload_date','besar_batch_real'], 'safe'],
            [['id','week'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nama_fg' => 'Nama Fg',
            'nomo' => 'Nomo',
            'posisi' => 'Posisi',
            'start' => 'Start',
            'stop' => 'Stop',
            'sum_leadtime_raw' => 'Sum Leadtime Raw',
            'sum_leadtime_bruto' => 'Sum Leadtime Bruto',
            'jenis_proses' => 'Jenis Proses',
            'sediaan' => 'Sediaan',
            'sum_downtime' => 'Sum Downtime',
            'sum_adjust' => 'Sum Adjust',
            'sum_leadtime_net' => 'Sum Leadtime Net',
            'keterangan_downtime' => 'Keterangan Downtime',
            'keterangan_adjust' => 'Keterangan Adjust',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'id' => 'ID',
            'week' => 'Week',
            'upload_date' => 'Upload Date',
            'nama_line' => 'Nama Line',
            'besar_batch_real' => 'Besar Batch Real (Kg)',
            'status' => 'Status QC',
            'time_release' => 'Time Release',
        ];
    }
}
