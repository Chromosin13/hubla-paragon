<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AchievementLeadtimePengolahan;

/**
 * AchievementLeadtimePengolahanSearch represents the model behind the search form of `app\models\AchievementLeadtimePengolahan`.
 */
class AchievementLeadtimePengolahanSearch extends AchievementLeadtimePengolahan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['id'], 'integer'],
            [['nama_line', 'nomo', 'koitem_bulk', 'nama_bulk', 'nama_fg', 'jenis_olah', 'nama_operator', 'tanggal_stop', 'plan_start_olah', 'plan_end_olah','id','achievement','sediaan'], 'safe'],
            [['real_leadtime', 'standard_leadtime'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AchievementLeadtimePengolahan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterCompare('achievement',$this->achievement
            ,'=');
        $query->andFilterCompare('id',$this->id
            ,'=');
        $query->andFilterCompare('real_leadtime',$this->real_leadtime
            ,'=');
        $query->andFilterCompare('DATE(tanggal_stop)',$this->tanggal_stop
            ,'=');
        $query->andFilterCompare('DATE(plan_start_olah)',$this->plan_start_olah
            ,'=');
        $query->andFilterCompare('DATE(plan_end_olah)',$this->plan_end_olah
            ,'=');
        $query->andFilterCompare('standard_leadtime',$this->standard_leadtime
            ,'=');

        // $query->andFilterWhere([
        //     'id' => $this->id,
        //     'real_leadtime' => $this->real_leadtime,
        //     'standard_leadtime' => $this->standard_leadtime,    
        //     'tanggal_stop' => $this->tanggal_stop,
        //     'plan_start_olah' => $this->plan_start_olah,
        //     'plan_end_olah' => $this->plan_end_olah,
        // ]);

        $query->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'koitem_bulk', $this->koitem_bulk])
            ->andFilterWhere(['like', 'nama_bulk', $this->nama_bulk])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'jenis_olah', $this->jenis_olah])
            ->andFilterWhere(['like', 'sediaan', $this->sediaan])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator]);

        return $dataProvider;
    }
}
