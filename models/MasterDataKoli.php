<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_data_koli".
 *
 * @property integer $id
 * @property string $koitem
 * @property string $naitem
 * @property integer $qty
 * @property integer $inner_box
 * @property string $sediaan
 * @property integer $qty_inside_innerbox
 * @property integer $qty_innerbox_per_karbox
 */
class MasterDataKoli extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_data_koli';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['koitem', 'naitem', 'sediaan', 'updated_by'], 'string'],
            [['timestamp'], 'safe'],
            [['qty', 'inner_box', 'qty_inside_innerbox', 'qty_innerbox_per_karbox'], 'integer'],
            [['koitem', 'qty', 'inner_box', 'qty_inside_innerbox'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'koitem' => 'Kode Item',
            'naitem' => 'Nama Item',
            'qty' => 'Quantity per Karbox (Pcs)',
            'inner_box' => 'Is Inner Box?',
            'sediaan' => 'Sediaan',
            'qty_inside_innerbox' => 'Quantity per Innerbox (Pcs)',
            'qty_innerbox_per_karbox' => 'Qty Innerbox Per Karbox',
            'timestamp' => 'Timestamp',
            'updated_by' => 'Updated By'
        ];
    }
}
