<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_data_netto".
 *
 * @property integer $id
 * @property string $koitem
 * @property string $nama_item
 * @property integer $isi_koli
 * @property string $netto_min
 * @property string $netto_max
 * @property string $timestamp
 */
class MasterDataNetto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_data_netto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['koitem', 'nama_item'], 'string'],
            [['isi_koli','pengali_netto'], 'integer'],
            [['netto_min', 'netto_max'], 'number'],
            [['timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'koitem' => 'Koitem',
            'nama_item' => 'Nama Item',
            'isi_koli' => 'Isi Koli',
            'netto_min' => 'Netto Min',
            'netto_max' => 'Netto Max',
            'timestamp' => 'Timestamp',
            'pengali_netto' => 'Pengali Netto',
        ];
    }
}
