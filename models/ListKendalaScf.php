<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "list_kendala_scf".
 *
 * @property integer $id
 * @property string $level_1
 * @property string $level_2
 * @property string $level_3
 */
class ListKendalaScf extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'list_kendala_scf';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level_1', 'level_2', 'level_3'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'level_1' => 'Level 1',
            'level_2' => 'Level 2',
            'level_3' => 'Level 3',
        ];
    }
}
