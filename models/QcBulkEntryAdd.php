<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "qc_bulk_entry_add".
 *
 * @property integer $id
 * @property string $penambahan_bb_adjust
 * @property string $nomo
 */
class QcBulkEntryAdd extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qc_bulk_entry_add';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['penambahan_bb_adjust'], 'number'],
            [['nomo'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'penambahan_bb_adjust' => 'Penambahan Bb Adjust',
            'nomo' => 'Nomo',
        ];
    }
}
