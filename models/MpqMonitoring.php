<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mpq_monitoring".
 *
 * @property string $sisa_bulk
 * @property integer $flow_input_snfg_id
 * @property string $fg_retur_layak_pakai
 * @property string $fg_retur_reject
 * @property string $netto_1
 * @property string $netto_2
 * @property string $netto_3
 * @property string $netto_4
 * @property string $netto_5
 * @property integer $id
 * @property integer $flow_input_snfgkomponen_id
 * @property string $create_time
 */
class MpqMonitoring extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mpq_monitoring';
    }
    
    public $sisa_bulk_standar;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sisa_bulk','fg_retur_layak_pakai', 'fg_retur_reject', 'netto_1', 'netto_2', 'netto_3', 'netto_4', 'netto_5'], 'number'],
            [['sisa_bulk','fg_retur_layak_pakai', 'fg_retur_reject'], 'required'],
            [['flow_input_snfg_id', 'flow_input_snfgkomponen_id'], 'integer'],
            [['create_time'], 'safe'],
            // [['sisa_bulk'],'validateComma'],
        ];
    }

    public function validateComma($attribute)
    {
        $comma = explode(',',$this->attribute);

        if(length($comma)>1){
          $this->addError($attribute, 'Gunakan titik untuk memisahkan desimal');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sisa_bulk' => 'Sisa Bulk',
            'sisa_bulk_standar' => 'Sisa Bulk Standar',
            'flow_input_snfg_id' => 'Flow Input Snfg ID',
            'fg_retur_layak_pakai' => 'Fg Retur Layak Pakai',
            'fg_retur_reject' => 'Fg Retur Reject',
            'netto_1' => 'Netto 1',
            'netto_2' => 'Netto 2',
            'netto_3' => 'Netto 3',
            'netto_4' => 'Netto 4',
            'netto_5' => 'Netto 5',
            'id' => 'ID',
            'flow_input_snfgkomponen_id' => 'Flow Input Snfgkomponen ID',
            'create_time' => 'Create Time',
        ];
    }
}
