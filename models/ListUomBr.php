<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "list_uom_br".
 *
 * @property integer $id
 * @property string $uom
 */
class ListUomBr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'list_uom_br';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uom'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uom' => 'Uom',
        ];
    }
}
