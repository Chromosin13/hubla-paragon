<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrackingSo;

/**
 * TrackingSoSearch represents the model behind the search form of `app\models\TrackingSo`.
 */
class TrackingSoSearch extends TrackingSo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'nobatch', 'sediaan', 'streamline', 'start', 'due', 'nama_fg', 'tglpermintaan', 'timestamp_complete', 'status', 'status_ndc', 'timestamp','status_koli_karantina','status_terima_karantina','first_terima_karantina','latest_terima_karantina'], 'safe'],
            [['week'], 'integer'],
            [['leadtime', 'mpq', 'mpq_supply', 'qty_in_process', 'qty_karantina', 'qty_ndc', 'qty_total','qty_sample','qty_reject','qty_ndc_release','qty_ndc_pending_mikro'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrackingSo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'week' => $this->week,
            'start' => $this->start,
            'due' => $this->due,
            'leadtime' => $this->leadtime,
            'tglpermintaan' => $this->tglpermintaan,
            'mpq' => $this->mpq,
            'mpq_supply' => $this->mpq_supply,
            'qty_in_process' => $this->qty_in_process,
            'qty_karantina' => $this->qty_karantina,
            'qty_ndc' => $this->qty_ndc,
            'timestamp_complete' => $this->timestamp_complete,
            'qty_total' => $this->qty_total,
            'qty_sample' => $this->qty_sample,
            'qty_reject' => $this->qty_reject,
            'timestamp' => $this->timestamp,
            'qty_ndc_release' => $this->qty_ndc_release,
            'qty_ndc_pending_mikro' => $this->qty_ndc_pending_mikro,
            'first_terima_karantina' => $this->first_terima_karantina,
            'latest_terima_karantina' => $this->latest_terima_karantina
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'nobatch', $this->nobatch])
            ->andFilterWhere(['like', 'sediaan', $this->sediaan])
            ->andFilterWhere(['like', 'streamline', $this->streamline])
            ->andFilterWhere(['like', 'status_ndc', $this->status_ndc])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'status_koli_karantina', $this->status_koli_karantina])
            ->andFilterWhere(['like', 'status_terima_karantina', $this->status_terima_karantina]);

        return $dataProvider;
    }
}
