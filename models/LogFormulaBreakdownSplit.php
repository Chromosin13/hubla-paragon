<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_formula_breakdown_split".
 *
 * @property integer $id
 * @property integer $log_formula_breakdown_id
 * @property string $kode_bulk
 * @property string $qty
 * @property string $uom
 * @property string $timestamp
 * @property integer $formula_reference
 * @property string $siklus
 * @property integer $operator
 * @property string $timbangan
 */
class LogFormulaBreakdownSplit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_formula_breakdown_split';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['log_formula_breakdown_id', 'operator'], 'integer'],
            [['kode_bulk', 'uom', 'siklus', 'formula_reference', 'timbangan', 'kode_olah', 'keterangan'], 'string'],
            [['qty'], 'number'],
            [['timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'log_formula_breakdown_id' => 'Log Formula Breakdown ID',
            'kode_bulk' => 'Kode Bulk',
            'qty' => 'Qty',
            'uom' => 'Uom',
            'timestamp' => 'Timestamp',
            'formula_reference' => 'Formula Reference',
            'siklus' => 'Siklus',
            'operator' => 'Operator',
            'kode_olah' => 'Kode Olah',
            'timbangan' => 'Timbangan',
            'keterangan' => 'Keterangan'
        ];
    }
}
