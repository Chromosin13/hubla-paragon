<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_proses_timbang".
 *
 * @property integer $id
 * @property string $jenis_proses_timbang
 */
class JenisProsesTimbang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_proses_timbang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_proses_timbang'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_proses_timbang' => 'Jenis Proses Timbang',
        ];
    }
}
