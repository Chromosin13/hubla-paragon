<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "planner_distinct_nomo".
 *
 * @property string $nomo
 * @property string $status
 */
class PlannerDistinctNomo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function primaryKey()
    {
        return ['nomo'];
    }

    public static function tableName()
    {
        return 'planner_distinct_nomo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'status'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nomo' => 'Nomo',
            'status' => 'Status',
        ];
    }
}
