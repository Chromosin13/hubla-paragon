<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flow_pra_kemas".
 *
 * @property string $posisi
 * @property integer $lanjutan
 * @property string $datetime_start
 * @property string $datetime_stop
 * @property string $datetime_write
 * @property string $nama_operator
 * @property string $nama_line
 * @property string $jenis_kemas
 * @property integer $is_done
 * @property integer $counter
 * @property string $snfg
 * @property string $jumlah_realisasi
 * @property string $nobatch
 * @property boolean $staging
 * @property string $exp_date
 * @property string $nosmb
 * @property string $barcode
 * @property string $fg_name_odoo
 * @property string $posisi_scan
 * @property integer $id
 */
class FlowPraKemas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flow_pra_kemas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['posisi', 'nama_operator', 'nama_line', 'jenis_kemas', 'snfg', 'nobatch', 'nosmb', 'barcode', 'fg_name_odoo', 'posisi_scan','na_number'], 'string'],
            [['lanjutan', 'is_done', 'counter','is_split_line'], 'integer'],
            [['datetime_start', 'datetime_stop', 'datetime_write', 'exp_date'], 'safe'],
            [['jumlah_realisasi'], 'number'],
            [['staging'], 'boolean'],
            [['jenis_kemas','nama_line','lanjutan','nama_operator','nobatch','exp_date','fg_name_odoo'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'posisi' => 'Posisi',
            'lanjutan' => 'Lanjutan',
            'datetime_start' => 'Datetime Start',
            'datetime_stop' => 'Datetime Stop',
            'datetime_write' => 'Datetime Write',
            'nama_operator' => 'Nama Operator',
            'nama_line' => 'Nama Line',
            'jenis_kemas' => 'Jenis Kemas',
            'is_done' => 'Is Done',
            'counter' => 'Counter',
            'snfg' => 'Snfg',
            'jumlah_realisasi' => 'Jumlah Realisasi',
            'nobatch' => 'Nobatch',
            'staging' => 'Staging',
            'exp_date' => 'Exp Date',
            'na_number' => 'NA Number',
            'nosmb' => 'Nosmb',
            'barcode' => 'Barcode',
            'fg_name_odoo' => 'Fg Name Odoo',
            'posisi_scan' => 'Posisi Scan',
            'id' => 'ID',
        ];
    }
}
