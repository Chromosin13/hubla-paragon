<?php

namespace app\models;

use yii\base\Model;
use yii\db\Query;

class ViInfo extends Model {
    private $db;

    public function __construct() {
        $this->db = \Yii::$app->db_vi;
    }
    
    public function getProductInfoBy($column, $value) {
        
        $query = 'SELECT * FROM {{%info}} WHERE ' . $column . '=:' . $column;
        $query_dep = 'SELECT * 
                        FROM scm_planner 
                        WHERE ' . $column . '=:' . $column;
        
        return $this->db->createCommand($query_dep)
            ->bindValue($column, $value)
            ->queryOne();
    }

    public function getAllProductCodeInfo() {
        $query = 'SELECT 
                        DISTINCT koitem_fg,
                        nama_fg
                    FROM scm_planner
                    WHERE line_kemas_2=:line_kemas_2
                    ORDER BY koitem_fg ASC
                    ';

        return $this->db->createCommand($query)
            ->bindValue('line_kemas_2', 'TUP06')
            ->queryAll();
    }

    public function validateSku($sku) {
        $query = 'SELECT *
                    FROM scm_planner
                    WHERE
                        koitem_fg=:koitem_fg
                        
                    ORDER BY id ASC
                    LIMIT 10';

        return $this->db->createCommand($query)
            ->bindValue('koitem_fg', $sku)
            ->queryOne();
    }
}