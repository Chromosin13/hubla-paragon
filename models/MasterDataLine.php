<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_data_line".
 *
 * @property integer $id
 * @property string $nama_line
 * @property string $sediaan
 * @property string $posisi
 */
class MasterDataLine extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_data_line';
    }


    public $text;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_line', 'sediaan', 'posisi'], 'string'],
            [['sediaan'],'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_line' => 'Nama Line',
            'sediaan' => 'Sediaan',
            'posisi' => 'Posisi',
        ];
    }
}
