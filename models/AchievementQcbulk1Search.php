<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AchievementQcbulk1;

/**
 * AchievementQcbulk1Search represents the model behind the search form about `app\models\AchievementQcbulk1`.
 */
class AchievementQcbulk1Search extends AchievementQcbulk1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row_number', 'rft'], 'integer'],
            [['tanggal_stop', 'tanggal_start', 'nama_line', 'sediaan', 'nomo', 'snfg', 'snfg_komponen', 'nama_fg', 'jenis_periksa', 'status_leadtime', 'koitem_bulk', 'koitem_fg', 'nama_bulk', 'status'], 'safe'],
            [['leadtime', 'std_leadtime', 'achievement_rate', 'is_done'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AchievementQcbulk1::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'row_number' => $this->row_number,
            'tanggal_stop' => $this->tanggal_stop,
            'tanggal_start' => $this->tanggal_start,
            'leadtime' => $this->leadtime,
            'std_leadtime' => $this->std_leadtime,
            'achievement_rate' => $this->achievement_rate,
            'rft' => $this->rft,
            'is_done' => $this->is_done,
        ]);

        $query->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'sediaan', $this->sediaan])
            ->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'jenis_periksa', $this->jenis_periksa])
            ->andFilterWhere(['like', 'status_leadtime', $this->status_leadtime])
            ->andFilterWhere(['like', 'koitem_bulk', $this->koitem_bulk])
            ->andFilterWhere(['like', 'koitem_fg', $this->koitem_fg])
            ->andFilterWhere(['like', 'nama_bulk', $this->nama_bulk])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
