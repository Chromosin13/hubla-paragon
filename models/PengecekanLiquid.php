<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pengecekan_liquid".
 *
 * @property integer $id
 * @property string $uji_ketahanan_seal
 * @property string $hasil_seal
 * @property string $kondisi_fisik
 * @property string $kes_warna_dusat
 * @property string $kes_identitas_dusat
 * @property string $kes_warna_paper
 * @property string $kes_identitas_paper
 * @property string $uji_torsi_cap
 * @property string $uji_kekencangan_cap
 * @property string $hasil_shrink_wrap
 */
class PengecekanLiquid extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengecekan_liquid';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengecekan_umum','sample_retain_ep','sample_mikro_ep'], 'integer'],
            // [['exp_date_ep'], 'save'],
            // [['uji_ketahanan_seal', 'hasil_seal', 'kes_warna_dusat', 'kes_identitas_dusat', 'kes_warna_paper', 'kes_identitas_paper', 'uji_torsi_cap', 'uji_kekencangan_cap', 'hasil_shrink_wrap','tube_dedusting','strainer','bad_tube_ejector','product_sensing','bottle_dedusting','auto_capper','plug_checking','pot_dedusting'], 'string'],
            [['tube_dedusting','strainer','bad_tube_ejector','product_sensing','bottle_dedusting','auto_capper','plug_checking','pot_dedusting','uji_ketahanan_seal', 'hasil_seal', 'uji_torsi_cap','alasan_tube_dedusting','alasan_strainer','alasan_bad_tube_ejector','alasan_product_sensing','alasan_bottle_dedusting','alasan_auto_capper','alasan_plug_checking','alasan_pot_dedusting','hasil_uji_seal','line_proses_blister','hasil_blister','temperature_mesin','time','voltage','heating_time','pressing_time','line_kemas_shrink','hasil_shrink','netto_ep','fg_per_karbox_ep','fg_per_palet_ep','warna_lakban_ep','jml_komponen_penyusun_ep','penyusun_1_ep','penyusun_2_ep','no_batch_ep','no_barcode_ep','kualitas_print_ep','inisial_inspektor_ep','hasil_investigasi_ep','penanganan_ketidaksesuaian_ep','detail_penanganan_ketidaksesuaian_ep','metode_rework_ep','status_siklus_ep','status_produk_ep','temuan_penyusun_ep_1_ep','temuan_penyusun_ep_2_ep','exp_date_ep'], 'string'],
            //[['uji_ketahanan_seal', 'hasil_seal', 'uji_torsi_cap'], 'integer'],

            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pengecekan_umum' => 'ID Pengecekan Umum',
            'uji_ketahanan_seal' => '6.1 Uji Ketahanan Seal (pcs)',
            'hasil_seal' => '6.3 Uji Visual Hasil Seal (pcs)',
            // 'kes_warna_dusat' => 'Kesesuaian Warna Dusat',
            // 'kes_identitas_dusat' => 'Kesesuaian Identitas Dusat',
            // 'kes_warna_paper' => 'Kesesuaian Warna Paper',
            // 'kes_identitas_paper' => 'Kesesuaian Identitas Paper',
            'uji_torsi_cap' => '6.4 Uji Torsi Cap (pcs)',
            // 'uji_kekencangan_cap' => 'Uji Kekencangan Cap',
            // 'hasil_shrink_wrap' => 'Hasil Shrink Wrap',
            'tube_dedusting' => '4.1 Tube Dedusting',
            'strainer' => '4.2 Strainer',
            'bad_tube_ejector' => '4.3 Bad Tube Ejector',
            'product_sensing' => '4.4 Product Sensing',
            'bottle_dedusting' => '4.5 Bottle Dedusting',
            'auto_capper' => '4.6 Capping / Semi Auto Capper (Capping Mobile)',
            'plug_checking' => '4.7 Plug Checking',
            'pot_dedusting' => '4.8 Pot Dedusting',
            'alasan_tube_dedusting' => '4.1.1 Alasan tidak aktif & PIC Mesin (DMS)',
            'alasan_strainer' => '4.2.1 Alasan tidak aktif & PIC Mesin (DMS)',
            'alasan_bad_tube_ejector' => '4.3.1 Alasan tidak aktif & PIC Mesin (DMS)',
            'alasan_product_sensing' => '4.4.1 Alasan tidak aktif & PIC Mesin (DMS)',
            'alasan_bottle_dedusting' => '4.5.1 Alasan tidak aktif & PIC Mesin (DMS)',
            'alasan_auto_capper' => '4.6.1 Alasan tidak aktif & PIC Mesin (DMS)',
            'alasan_plug_checking' => '4.7.1 Alasan tidak aktif & PIC Mesin (DMS)',
            'alasan_pot_dedusting' => '4.8.1 Alasan tidak aktif & PIC Mesin (DMS)',
            'hasil_uji_seal' => '6.2 Pengukuran Hasil Uji Seal (dengan mesin TLT)',
            'line_proses_blister' => '12.1 Line Kemas Proses Blister',
            'hasil_blister' => '12.2 Kualitas Hasil Blister (pcs)',
            'temperature_mesin' => '12.3 Temperatur Mesin Blister (BLP 01)',
            'time' => '12.4 Time (BLP 01)',
            'voltage' => '12.5 Voltage (BLP 02)',
            'heating_time' => '12.6 Heating Time (BLP 02)',
            'pressing_time' => '12.7 Pressing Time (BLP 02)',
            'line_kemas_shrink' => '12.1 Line Kemas Proses Shrink Wrap',
            'hasil_shrink' => '12.2 Kualitas Hasil Shrink Wrap (pcs)
',
            'netto_ep' => '13.1 Netto (gram)
',
            'fg_per_karbox_ep' => '13.2 Jumlah FG per karbox (pcs)',
            'fg_per_palet_ep' => '13.3 Jumlah FG per pallete (pcs)',
            'warna_lakban_ep' => '13.4 Warna Lakban / Label Barcode di Innerbox dan Karbox ',
            'jml_komponen_penyusun_ep' => '13.7 Jumlah Komponen Penyusun',
            'penyusun_1_ep' => '13.8 Komponen Penyusun 1 (pcs)',
            'penyusun_2_ep' => '13.9 Komponen Penyusun 2 (pcs)',
            'no_batch_ep' => '13.11 No. Batch di Packaging',
            'no_barcode_ep' => '13.12 Kesesuaian Nomor Barcode Produk',
            'kualitas_print_ep' => '13.13 Kualitas Print Label Karbox',
            'inisial_inspektor_ep' => '13.14 Inisial Inspektor QLT FG',
            'hasil_investigasi_ep' => '13.15 Hasil Investigasi Ketidaksesuaian',
            'penanganan_ketidaksesuaian_ep' => '13.16 Penanganan Ketidaksesuaian FG',
            'detail_penanganan_ketidaksesuaian_ep' => '13.17 Detail Penanganan Ketidaksesuaian',
            'metode_rework_ep' => '13.18 Penjelasan Metode Rework',
            'status_siklus_ep' => '13.19 Status Siklus Ekstra Proses',
            'status_produk_ep' => '13.20 Status Produk Ekstra Proses',
            'sample_retain_ep' => '13.5 Sampel Retain (pcs)',
            'sample_mikro_ep' => '13.6 Sampel Mikro (pcs)',
            'exp_date_ep' => '13.10 Exp Date di Packaging',
            'temuan_penyusun_ep_1_ep' => '13.8.1 Temuan Penyusun 1',
            'temuan_penyusun_ep_2_ep' => '13.9.1 Temuan Penyusun 2',
        ];
    }
}
