<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "konfigurasi_standar".
 *
 * @property string $koitem
 * @property string $last_update
 * @property integer $id
 *
 * @property KonfigurasiStandarItem[] $konfigurasiStandarItems
 */
class KonfigurasiStandar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */



    public $text;
    public $jumlah_konfigurasi;
    public $jumlah_fg;
    public $jumlah_bulk;
    public $jumlah_unfilled_standard;
    public $jumlah_filled_standard;
    public $jumlah_koitem_fg;

    
    public static function tableName()
    {
        return 'konfigurasi_standar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['koitem'], 'string'],
            [['last_update'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'koitem' => 'Koitem',
            'last_update' => 'Last Update',
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKonfigurasiStandarItems()
    {
        return $this->hasMany(KonfigurasiStandarItem::className(), ['ks_id' => 'id']);
    }
}
