<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "odoo_mc_log".
 *
 * @property integer $id
 * @property integer $lanjutan
 * @property integer $bom_id
 * @property string $write_date
 * @property string $qty
 */
class OdooMcLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'odoo_mc_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lanjutan', 'bom_id'], 'integer'],
            [['write_date','nomor_mo'], 'safe'],
            [['qty'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lanjutan' => 'Lanjutan',
            'bom_id' => 'Bom ID',
            'write_date' => 'Write Date',
            'qty' => 'Qty',
        ];
    }
}
