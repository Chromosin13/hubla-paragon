<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "allprocess_leadtime".
 *
 * @property integer $row_number
 * @property string $posisi
 * @property string $snfg
 * @property string $snfg_komponen
 * @property string $nama_line
 * @property string $nama
 * @property string $tanggal
 * @property string $sum
 */
class AllprocessLeadtime extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['row_number'];
    }
    
    public static function tableName()
    {
        return 'allprocess_leadtime';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row_number'], 'integer'],
            [['posisi', 'snfg', 'snfg_komponen', 'nama_line', 'nama'], 'string'],
            [['tanggal'], 'safe'],
            [['sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'row_number' => 'Row Number',
            'posisi' => 'Posisi',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'nama_line' => 'Nama Line',
            'nama' => 'Nama',
            'tanggal' => 'Tanggal',
            'sum' => 'Sum',
        ];
    }
}
