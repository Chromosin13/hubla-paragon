<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_penimbangan_rm".
 *
 * @property integer $id
 * @property string $nomo
 * @property string $no_formula
 * @property integer $no_revisi
 * @property string $naitem_fg
 * @property string $datetime_start
 * @property string $datetime_stop
 * @property string $datetime_write
 * @property string $kode_bb
 * @property string $nama_bb
 * @property string $kode_olah
 * @property integer $siklus
 * @property string $weight
 * @property string $satuan
 * @property string $sediaan
 * @property integer $cycle_time
 * @property string $timbangan
 * @property integer $operator
 * @property string $no_timbangan
 * @property string $realisasi
 * @property string $satuan_realisasi
 * @property string $status
 * @property string $koitem_fg
 * @property string $nobatch
 * @property integer $is_split
 *
 * @property LogPenimbanganRmSplit[] $logPenimbanganRmSplits
 */
class LogPenimbanganRm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_penimbangan_rm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'no_formula', 'naitem_fg', 'kode_bb', 'nama_bb', 'kode_olah', 'satuan', 'sediaan', 'timbangan', 'no_timbangan', 'satuan_realisasi', 'status', 'koitem_fg', 'nobatch', 'kode_internal', 'log', 'nama_line','id_bb','mo_odoo','log_realisasi'], 'string'],

            [['no_revisi', 'siklus', 'cycle_time', 'operator', 'is_split', 'is_repack'], 'integer'],
            [['datetime_start', 'datetime_stop', 'datetime_write'], 'safe'],
            [['weight', 'realisasi'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_bb' => 'ID BB',
            'nomo' => 'Nomo',
            'no_formula' => 'No Formula',
            'no_revisi' => 'No Revisi',
            'naitem_fg' => 'Naitem Fg',
            'datetime_start' => 'Datetime Start',
            'datetime_stop' => 'Datetime Stop',
            'datetime_write' => 'Datetime Write',
            'kode_bb' => 'Kode Bb',
            'nama_bb' => 'Nama Bb',
            'nama_line' => 'Nama Line',
            'kode_olah' => 'Kode Olah',
            'siklus' => 'Siklus',
            'weight' => 'Weight',
            'satuan' => 'Satuan',
            'sediaan' => 'Sediaan',
            'cycle_time' => 'Cycle Time',
            'timbangan' => 'Timbangan',
            'operator' => 'Operator',
            'no_timbangan' => 'No Timbangan',
            'realisasi' => 'Realisasi',
            'satuan_realisasi' => 'Satuan Realisasi',
            'status' => 'Status',
            'koitem_fg' => 'Koitem Fg',
            'nobatch' => 'Nobatch',
            'is_split' => 'Is Split',
            'kode_internal' => 'Kode Internal',
            'log' => 'Log',
            'nama_line' => 'Nama Line',
            'mo_odoo' => 'MO Odoo',
            'is_repack' => 'Is Repack'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogPenimbanganRmSplits()
    {
        return $this->hasMany(LogPenimbanganRmSplit::className(), ['log_penimbangan_rm_id' => 'id']);
    }
}
