<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "adminlog".
 *
 * @property integer $id
 * @property string $nopo
 * @property string $no_surjal
 * @property string $koitem_pack
 * @property string $nama_pack
 * @property string $supplier
 * @property string $datetime_start
 * @property string $datetime_stop
 * @property string $datetime_write
 * @property string $batch
 * @property integer $admin_id
 * @property integer $status_print
 * @property string $tgl_plan_kirim
 * @property string $qty_demand
 * @property string $price_unit
 * @property string $qty_kirim
 * @property string $tgl_po
 * @property string $tgl_kirim
 */
class AdminlogFg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adminlog_fg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nopo', 'no_surjal', 'koitem_fg', 'nama_fg', 'supplier', 'batch', 'status_print'], 'string'],
            [['datetime_start', 'datetime_stop', 'datetime_write', 'tgl_plan_kirim', 'tgl_po', 'tgl_kirim'], 'safe'],
            [['admin_id','sml_id'], 'integer'],
            [['qty_demand', 'price_unit', 'qty_kirim'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nopo' => 'Nopo',
            'no_surjal' => 'No Surjal',
            'koitem_fg' => 'Koitem FG',
            'nama_fg' => 'Nama FG',
            'supplier' => 'Supplier',
            'datetime_start' => 'Datetime Start',
            'datetime_stop' => 'Datetime Stop',
            'datetime_write' => 'Datetime Write',
            'batch' => 'Batch',
            'admin_id' => 'Admin ID',
            'status_print' => 'Status Print',
            'tgl_plan_kirim' => 'Tgl Plan Kirim',
            'qty_demand' => 'Qty Demand',
            'price_unit' => 'Price Unit',
            'qty_kirim' => 'Qty Kirim',
            'tgl_po' => 'Tgl Po',
            'tgl_kirim' => 'Tgl Kirim',
        ];
    }
}
