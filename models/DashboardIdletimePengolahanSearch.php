<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DashboardIdletimePengolahan;

/**
 * DashboardIdletimePengolahanSearch represents the model behind the search form about `app\models\DashboardIdletimePengolahan`.
 */
class DashboardIdletimePengolahanSearch extends DashboardIdletimePengolahan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'snfg', 'snfg_komponen', 'nama_fg', 'sediaan', 'upload_date', 'idle_timbang_olah', 'idle_olahpremix_olah1', 'idle_olah1_olah2', 'idle_olah2_qcbulk', 'timbang_max', 'olah_pertama', 'olah_premix_min', 'olah_premix_max', 'olah_1_min', 'olah_1_max', 'olah_2_min', 'olah_2_max', 'qcbulk_min'], 'safe'],
            [['week', 'id'], 'integer'],
            [['idle_timbang_olah_jam', 'idle_olahpremix_olah1_jam', 'idle_olah1_olah2_jam', 'idle_olah2_qcbulk_jam'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DashboardIdletimePengolahan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'upload_date' => $this->upload_date,
            'week' => $this->week,
            'idle_timbang_olah_jam' => $this->idle_timbang_olah_jam,
            'idle_olahpremix_olah1_jam' => $this->idle_olahpremix_olah1_jam,
            'idle_olah1_olah2_jam' => $this->idle_olah1_olah2_jam,
            'idle_olah2_qcbulk_jam' => $this->idle_olah2_qcbulk_jam,
            'timbang_max' => $this->timbang_max,
            'olah_pertama' => $this->olah_pertama,
            'olah_premix_min' => $this->olah_premix_min,
            'olah_premix_max' => $this->olah_premix_max,
            'olah_1_min' => $this->olah_1_min,
            'olah_1_max' => $this->olah_1_max,
            'olah_2_min' => $this->olah_2_min,
            'olah_2_max' => $this->olah_2_max,
            'qcbulk_min' => $this->qcbulk_min,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'sediaan', $this->sediaan])
            ->andFilterWhere(['like', 'idle_timbang_olah', $this->idle_timbang_olah])
            ->andFilterWhere(['like', 'idle_olahpremix_olah1', $this->idle_olahpremix_olah1])
            ->andFilterWhere(['like', 'idle_olah1_olah2', $this->idle_olah1_olah2])
            ->andFilterWhere(['like', 'idle_olah2_qcbulk', $this->idle_olah2_qcbulk]);

        return $dataProvider;
    }
}
