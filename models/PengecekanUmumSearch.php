<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PengecekanUmum;
use app\models\IdentitasPengecekanFg;

/**
 * PengecekanUmumSearch represents the model behind the search form about `app\models\PengecekanUmum`.
 */
class PengecekanUmumSearch extends PengecekanUmum
{
    /**
     * @inheritdoc
     */

    // public $nama_fg;

    public function rules()
    {
        return [
            [[ 'jam_mulai','jam_selesai'], 'safe'],
            [[ 'aql_per_palet', 'aql_per_siklus', 'jumlah_sampel_trial', 'sampel_retain', 'sampel_mikro', 'id','no_palet','siklus'], 'integer'],
            [[ 'snfg','status_produk','status_siklus','status_hasil_cek_qa','inspector_fg','catatan_pengecekan','sistem_cek','koli_qa','pengerjaan_extra_proses','extra_proses'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengecekanUmum::find();
        // $query2 = IdentitasPengecekanFg::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // $snfgs = Yii::$app->db->createCommand("SELECT snfg FROM identitas_pengecekan_fg WHERE nama_fg ilike '%".$this->nama_fg."%' ")->queryAll();
        // $snfg_str = "";
        // foreach ($snfgs as $snfg) {
        //     $snfg_str += "'".$snfg."',";
        // }
        // $snfg_string = implode()

        // grid filtering conditions
        $query->andFilterWhere([

            'snfg' => $this->snfg,
            'catatan_pengecekan' => $this->catatan_pengecekan,
            'sistem_cek' => $this->sistem_cek,
            'koli_qa' => $this->koli_qa,
            'extra_proses' => $this->extra_proses,
            'pengerjaan_extra_proses' => $this->pengerjaan_extra_proses,
            'inspector_fg' => $this->inspector_fg,
            'status_siklus' => $this->status_siklus,
            'status_hasil_cek_qa' => $this->status_hasil_cek_qa,
            'status_produk' => $this->status_produk,
            'aql_per_palet' => $this->aql_per_palet,
            'aql_per_siklus' => $this->aql_per_siklus,
            'jam_mulai' => $this->jam_mulai,
            'jam_selesai' => $this->jam_selesai,
            'kesesuaian_exp_primer' => $this->kesesuaian_exp_primer,
            'kesesuaian_exp_sekunder' => $this->kesesuaian_exp_sekunder,
            'kesesuaian_exp_karbox' => $this->kesesuaian_exp_karbox,
            'jumlah_sampel_trial' => $this->jumlah_sampel_trial,
            'sampel_retain' => $this->sampel_retain,
            'sampel_mikro' => $this->sampel_mikro,
            'id' => $this->id,
        ]);

        $query
            ->andFilterWhere(['ilike', 'snfg', $this->snfg])
            ->andFilterWhere(['ilike', 'inspector_fg', $this->inspector_fg])
            ->andFilterWhere(['ilike', 'catatan_pengecekan', $this->catatan_pengecekan])
            ->andFilterWhere(['ilike', 'status_hasil_cek_qa', $this->status_hasil_cek_qa])
            ->andFilterWhere(['ilike', 'status_siklus', $this->status_siklus])
            ->andFilterWhere(['ilike', 'status_produk', $this->status_produk])
            ->andFilterWhere(['ilike', 'visual_performance_bulk', $this->visual_performance_bulk])
            ->andFilterWhere(['ilike', 'fisik_packaging', $this->fisik_packaging])
            ->andFilterWhere(['ilike', 'fungsional_packaging', $this->fungsional_packaging])
            ->andFilterWhere(['ilike', 'kesesuaian_warna_packaging', $this->kesesuaian_warna_packaging])
            ->andFilterWhere(['ilike', 'kesesuaian_identitas_packaging', $this->kesesuaian_identitas_packaging])
            ->andFilterWhere(['ilike', 'performance_segel', $this->performance_segel])
            ->andFilterWhere(['ilike', 'kesesuaian_batch_primer', $this->kesesuaian_batch_primer])
            ->andFilterWhere(['ilike', 'kesesuaian_batch_sekunder', $this->kesesuaian_batch_sekunder])
            ->andFilterWhere(['ilike', 'kesesuaian_batch_karbox', $this->kesesuaian_batch_karbox])
            ->andFilterWhere(['ilike', 'kesesuaian_barcode_produk', $this->kesesuaian_barcode_produk])
            ->andFilterWhere(['ilike', 'kualitas_barcode_karbox', $this->kualitas_barcode_karbox])
            ->andFilterWhere(['ilike', 'netto', $this->netto])
            ->andFilterWhere(['ilike', 'jumlah_per_karbox', $this->jumlah_per_karbox])
            ->andFilterWhere(['ilike', 'kesesuaian_warna_lakban', $this->kesesuaian_warna_lakban])
            ->andFilterWhere(['ilike', 'inspector_fg', $this->inspector_fg])
            ->andFilterWhere(['ilike', 'investigasi_ketidaksesuaian', $this->investigasi_ketidaksesuaian])
            ->andFilterWhere(['ilike', 'penanganan_ketidaksesuaian', $this->penanganan_ketidaksesuaian])
            ->andFilterWhere(['ilike', 'status_produk', $this->status_produk]);

        return $dataProvider;
    }
}
