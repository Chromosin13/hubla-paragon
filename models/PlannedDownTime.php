<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "planned_down_time".
 *
 * @property integer $id
 * @property string $keterangan
 * @property string $durasi
 * @property integer $kemas_id
 * @property integer $jenis
 */
class PlannedDownTime extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'planned_down_time';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keterangan'], 'string'],
            [['durasi'], 'safe'],
            [['kemas_id', 'jenis'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keterangan' => 'Keterangan',
            'durasi' => 'Durasi',
            'kemas_id' => 'Kemas ID',
            'jenis' => 'Jenis',
        ];
    }
}
