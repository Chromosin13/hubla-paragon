<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tracking_so".
 *
 * @property string $snfg
 * @property integer $week
 * @property string $sediaan
 * @property string $streamline
 * @property string $start
 * @property string $due
 * @property string $leadtime
 * @property string $nama_fg
 * @property string $tglpermintaan
 * @property string $mpq
 * @property string $qty_in_process
 * @property string $qty_karantina
 * @property string $qty_ndc
 * @property string $timestamp_complete
 * @property string $qty_total
 * @property string $status
 * @property string $timestamp
 */
class TrackingSo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['snfg'];
    }

    public static function tableName()
    {
        return 'tracking_so';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'sediaan', 'streamline', 'nama_fg', 'status', 'status_ndc','koitem_fg','status_koli_karantina','status_terima_karantina','nobatch'], 'string'],
            [['week'], 'integer'],
            [['start', 'due', 'tglpermintaan', 'timestamp_complete', 'timestamp','first_terima_karantina','latest_terima_karantina'], 'safe'],
            [['leadtime', 'mpq', 'mpq_supply', 'qty_in_process', 'qty_karantina', 'qty_ndc', 'qty_total','qty_sample','qty_reject','qty_ndc_release','qty_ndc_pending_mikro'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'snfg' => 'Snfg',
            'nobatch' => 'No Batch',
            'week' => 'Week',
            'sediaan' => 'Sediaan',
            'streamline' => 'Streamline',
            'start' => 'Start',
            'due' => 'Due',
            'leadtime' => 'Leadtime',
            'nama_fg' => 'Nama Fg',
            'tglpermintaan' => 'Tglpermintaan',
            'mpq' => 'Mpq',
            'mpq_supply' => 'Mpq Supply',
            'qty_in_process' => 'Qty In Process',
            'qty_karantina' => 'Qty Karantina',
            'qty_ndc' => 'Qty Ndc',
            'timestamp_complete' => 'Timestamp Complete',
            'qty_total' => 'Qty Total',
            'status' => 'Status',
            'status_ndc' => 'Status NDC',
            'timestamp' => 'Timestamp',
            'qty_sample' => 'Qty Sample',
            'qty_reject' => 'Qty Reject',
            'status_koli_karantina' => 'status_koli_karantina',
            'status_terima_karantina' => 'status_terima_karantina',
            'first_terima_karantina' => 'first_terima_karantina',
            'latest_terima_karantina' => 'latest_terima_karantina'
        ];
    }
}
