<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ScheduleNotHit;

/**
 * ScheduleNotHitSearch represents the model behind the search form about `app\models\ScheduleNotHit`.
 */
class ScheduleNotHitSearch extends ScheduleNotHit
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_done'], 'integer'],
            [['snfg', 'start', 'schedule_due', 'koitem_fg', 'nama_fg', 'timestamp_check', 'remark', 'level_1', 'level_2', 'level_3', 'corrective_action', 'preventive_action', 'pic', 'due'], 'safe'],
            [['jumlah_pcs', 'qty_release', 'qty_nh'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ScheduleNotHit::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'start' => $this->start,
            'schedule_due' => $this->schedule_due,
            'jumlah_pcs' => $this->jumlah_pcs,
            'qty_release' => $this->qty_release,
            'qty_nh' => $this->qty_nh,
            'is_done' => $this->is_done,
            'timestamp_check' => $this->timestamp_check,
            'due' => $this->due,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'sediaan', $this->sediaan])
            ->andFilterWhere(['like', 'koitem_fg', $this->koitem_fg])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'level_1', $this->level_1])
            ->andFilterWhere(['like', 'level_2', $this->level_2])
            ->andFilterWhere(['like', 'level_3', $this->level_3])
            ->andFilterWhere(['like', 'corrective_action', $this->corrective_action])
            ->andFilterWhere(['like', 'preventive_action', $this->preventive_action])
            ->andFilterWhere(['like', 'pic', $this->pic]);

        return $dataProvider;
    }
}
