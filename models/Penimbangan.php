<?php

namespace app\models;

use Yii;
use DateTime;

/**
 * This is the model class for table "penimbangan".
 *
 * @property integer $id
 * @property string $snfg
 * @property string $nama_line
 * @property string $jumlah_operator
 * @property string $nama_operator
 * @property string $nama_operator_2
 * @property string $waktu
 * @property string $state
 * @property string $lanjutan
 */
class Penimbangan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'penimbangan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'nama_line', 'nama_operator', 'nama_operator_2','nomo', 'state','snfg_komponen','jenis_penimbangan','edit_nama'], 'string'],
            [['jumlah_operator', 'lanjutan','lanjutan_ist','is_done','palet_ke'], 'number'],
            [['waktu','timestamp','edit_time'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'nama_line' => 'Nama Line',
            'jumlah_operator' => 'Jumlah Operator',
            'nama_operator' => 'Nama Operator',
            'nama_operator_2' => 'Nama Operator 2',
            'waktu' => 'Waktu',
            'state' => 'State',
            'lanjutan' => 'Lanjutan',
            'snfg_komponen' => 'Snfg Komponen',
            'timestamp' => 'Timestamp',
            'jenis_penimbangan' => 'Jenis Penimbangan',
            'lanjutan_ist' => 'Lanjutan Istirahat',
            'is_done' => 'Is Done',
            'nomo' => 'Manufacturing Number',
            'palet_ke' => 'Palet Ke',
        ];
    }

    public function getKendalas()
    {
        return $this->hasMany(Kendala::className(), ['penimbangan_id' => 'id']);
    }

    public function getFormatwaktu()
    {
        return DateTime::createFromFormat('Y-m-d H:i:s.u', $this->waktu)->format('Y-m-d H:i:s');
    }

}
