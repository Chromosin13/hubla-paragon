<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_fro_mrp_detail".
 *
 * @property integer $id
 * @property string $nomo
 * @property string $kode_bb
 * @property string $nobatch
 * @property string $realisasi
 * @property string $satuan_realisasi
 * @property integer $is_split
 * @property string $log
 * @property string $no_smb
 * @property string $log_odoo
 * @property string $keterangan
 */
class LogFroMrpDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_fro_mrp_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'kode_bb', 'nobatch', 'satuan_realisasi', 'log', 'no_smb', 'log_odoo', 'keterangan'], 'string'],
            [['realisasi'], 'number'],
            [['last_updated'], 'safe'],
            [['is_split'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomo' => 'Nomo',
            'kode_bb' => 'Kode Bb',
            'nobatch' => 'Nobatch',
            'realisasi' => 'Realisasi',
            'satuan_realisasi' => 'Satuan Realisasi',
            'is_split' => 'Is Split',
            'log' => 'Log',
            'no_smb' => 'No Smb',
            'log_odoo' => 'Log Odoo',
            'keterangan' => 'Keterangan',
        ];
    }
}
