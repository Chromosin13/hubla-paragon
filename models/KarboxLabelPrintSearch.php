<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KarboxLabelPrint;

/**
 * KarboxLabelPrintSearch represents the model behind the search form about `app\models\KarboxLabelPrint`.
 */
class KarboxLabelPrintSearch extends KarboxLabelPrint
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'qty', 'qty_request', 'qty_total_per_snfg', 'flow_input_snfg_id'], 'integer'],
            [['snfg', 'koitem_fg', 'nama_fg', 'nosmb', 'barcode', 'nobatch', 'na_number', 'operator', 'exp_date', 'status', 'nama_line', 'timestamp', 'odoo_code', 'sediaan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KarboxLabelPrint::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'exp_date' => $this->exp_date,
            'qty' => $this->qty,
            'qty_request' => $this->qty_request,
            'qty_total_per_snfg' => $this->qty_total_per_snfg,
            'timestamp' => $this->timestamp,
            'flow_input_snfg_id' => $this->flow_input_snfg_id,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'koitem_fg', $this->koitem_fg])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'nosmb', $this->nosmb])
            ->andFilterWhere(['like', 'barcode', $this->barcode])
            ->andFilterWhere(['like', 'nobatch', $this->nobatch])
            ->andFilterWhere(['like', 'na_number', $this->na_number])
            ->andFilterWhere(['like', 'operator', $this->operator])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'odoo_code', $this->odoo_code])
            ->andFilterWhere(['like', 'sediaan', $this->sediaan]);

        return $dataProvider;
    }
}
