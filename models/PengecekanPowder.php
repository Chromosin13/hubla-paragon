<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pengecekan_powder".
 *
 * @property integer $id
 * @property integer $id_pengecekan_umum
 * @property string $netto
 * @property string $drop_test
 * @property string $rub_test
 * @property string $pengecekan_warna
 */
class PengecekanPowder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengecekan_powder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengecekan_umum'], 'integer'],
            [['uji_jatuh', 'uji_oles', 'pengecekan_warna'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pengecekan_umum' => 'Id Pengecekan Umum',
            'uji_jatuh' => '6.1 Uji Jatuh (pcs)',
            'uji_oles' => '6.2 Uji Oles (pcs)',
            'pengecekan_warna' => '6.3 Pengecekan Warna (pcs)',
        ];
    }
}
