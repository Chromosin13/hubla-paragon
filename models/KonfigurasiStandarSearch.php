<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KonfigurasiStandar;

/**
 * KonfigurasiStandarSearch represents the model behind the search form about `app\models\KonfigurasiStandar`.
 */
class KonfigurasiStandarSearch extends KonfigurasiStandar
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['koitem', 'last_update'], 'safe'],
            [['id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KonfigurasiStandar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'last_update' => $this->last_update,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'koitem', $this->koitem]);

        return $dataProvider;
    }
}
