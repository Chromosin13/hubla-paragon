<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tracking_batch_olah".
 *
 * @property integer $id_pengolahan
 * @property string $nama_fg
 * @property string $snfg
 * @property string $nomo
 * @property string $scan_start
 * @property string $nobatch
 * @property string $nama_operator
 */
class TrackingBatchOlah extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tracking_batch_olah';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengolahan'], 'integer'],
            [['nama_fg', 'nama_bulk', 'snfg', 'nomo', 'nobatch', 'nama_operator','sediaan'], 'string'],
            [['scan_start'], 'safe'],
        ];
    }

    public static function primaryKey()
    {
        return ['id_pengolahan'];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pengolahan' => 'Id Pengolahan',
            'nama_fg' => 'Nama Fg',
            'nama_bulk' => 'Nama Bulk',
            'sediaan' => 'Sediaan',
            'snfg' => 'Snfg',
            'nomo' => 'Nomo',
            'scan_start' => 'Scan Start',
            'nobatch' => 'Nobatch',
            'nama_operator' => 'Nama Operator',
        ];
    }
}
