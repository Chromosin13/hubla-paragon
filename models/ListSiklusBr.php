<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "list_siklus_br".
 *
 * @property integer $id
 * @property integer $siklus
 * @property string $sediaan
 */
class ListSiklusBr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'list_siklus_br';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['siklus'], 'integer'],
            [['sediaan'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'siklus' => 'Siklus',
            'sediaan' => 'Sediaan',
        ];
    }
}
