<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kode_action_penimbangan_rm".
 *
 * @property integer $id
 * @property string $kode
 * @property string $keterangan
 */
class KodeActionPenimbanganRm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kode_action_penimbangan_rm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode', 'keterangan'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode' => 'Kode',
            'keterangan' => 'Keterangan',
        ];
    }
}
