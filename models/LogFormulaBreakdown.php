<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_formula_breakdown".
 *
 * @property integer $id
 * @property string $kode_bulk
 * @property string $nama_fg
 * @property integer $formula_reference
 * @property string $lokasi
 * @property string $scheduled_start
 * @property string $week
 * @property string $kode_bb
 * @property string $qty
 * @property string $uom
 * @property string $timbangan
 * @property string $nama_line
 * @property string $nama_bb
 * @property integer $is_split
 * @property integer $operator
 * @property string $siklus
 * @property string $timestamp
 * @property string $qty_batch
 * @property string $kode_olah
 * @property string $action
 * @property string $keterangan
 * @property string $no_dokumen
 * @property integer $no_revisi
 * @property string $kode_internal
 * @property integer $is_repack
 * @property string $status_formula
 * @property integer $urutan
 * @property string $serial_manual
 * @property string $version
 * @property string $cycle_time_std
 * @property string $cycle_time_real
 * @property integer $is_lock
 */
class LogFormulaBreakdown extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_formula_breakdown';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_bulk', 'nama_fg', 'lokasi', 'week', 'kode_bb', 'uom', 'timbangan', 'nama_line', 'nama_bb', 'siklus', 'kode_olah', 'action', 'keterangan', 'no_dokumen', 'kode_internal', 'status_formula', 'version'], 'string'],
            [['formula_reference', 'is_split', 'operator', 'no_revisi', 'is_repack', 'urutan', 'is_lock'], 'integer'],
            [['scheduled_start', 'timestamp'], 'safe'],
            [['qty', 'qty_batch', 'serial_manual', 'cycle_time_std', 'cycle_time_real'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode_bulk' => 'Kode Bulk',
            'nama_fg' => 'Nama Fg',
            'formula_reference' => 'Formula Reference',
            'lokasi' => 'Lokasi',
            'scheduled_start' => 'Scheduled Start',
            'week' => 'Week',
            'kode_bb' => 'Kode Bb',
            'qty' => 'Qty',
            'uom' => 'Uom',
            'timbangan' => 'Timbangan',
            'nama_line' => 'Nama Line',
            'nama_bb' => 'Nama Bb',
            'is_split' => 'Is Split',
            'operator' => 'Operator',
            'siklus' => 'Siklus',
            'timestamp' => 'Timestamp',
            'qty_batch' => 'Qty Batch',
            'kode_olah' => 'Kode Olah',
            'action' => 'Action',
            'keterangan' => 'Keterangan',
            'no_dokumen' => 'No Dokumen',
            'no_revisi' => 'No Revisi',
            'kode_internal' => 'Kode Internal',
            'is_repack' => 'Is Repack',
            'status_formula' => 'Status Formula',
            'urutan' => 'Urutan',
            'serial_manual' => 'Serial Manual',
            'version' => 'Version',
            'cycle_time_std' => 'Cycle Time Std',
            'cycle_time_real' => 'Cycle Time Real',
            'is_lock' => 'Is Lock',
        ];
    }

    public function get_decimal($timbangan)
    {
        $sql = "
        SELECT angka_belakang_koma
        FROM master_data_timbangan_rm
        WHERE  kode_timbangan = '".$timbangan."'";

        $row = MasterDataTimbanganRm::findBySql($sql)->one();

        if (!empty($row)){
            // echo count($row);
            return $row->angka_belakang_koma;
        } else {
            return 6;
        }

    }

    public function get_current_pic($nomo)
    {
        $sql = "
        SELECT current_pic
        FROM log_approval_br
        WHERE  nomo = '".$nomo."'";

        $row = LogApprovalBr::findBySql($sql)->one();

        if (!empty($row)){
            // echo count($row);
            return $row->current_pic;
        } else {
            return NULL;
        }

    }

    public function get_current_status($nomo)
    {
        $sql = "
        SELECT current_status
        FROM log_approval_br
        WHERE  nomo = '".$nomo."'";

        $row = LogApprovalBr::findBySql($sql)->one();

        if (!empty($row)){
            // echo count($row);
            return $row->current_status;
        } else {
            return NULL;
        }

    }
}
