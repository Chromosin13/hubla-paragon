<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sampler_qc_fg".
 *
 * @property integer $id
 * @property integer $jumlah_kedatangan_batch
 * @property integer $aql_kedatangan
 * @property integer $pengambilan_sampel
 * @property string $temuan_defect
 * @property string $kategori_aql
 * @property string $datetime_start
 * @property string $datetime_stop
 * @property string $datetime_write
 * @property string $nopo
 * @property string $no_surjal
 * @property string $koitem_fg
 * @property string $nama_fg
 * @property string $supplier
 * @property string $batch
 * @property string $penempatan
 * @property integer $sampler_id
 * @property string $koordinat_penempatan
 * @property string $status_analis
 * @property string $priority
 * @property string $kategori
 * @property string $date_penerimaanlog
 * @property string $sop_npd
 * @property integer $jumlah_palet
 * @property string $flag_analis
 */
class SamplerQcFg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sampler_qc_fg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jumlah_kedatangan_batch', 'aql_kedatangan', 'pengambilan_sampel', 'sampler_id', 'jumlah_palet'], 'integer'],
            [['temuan_defect', 'kategori_aql', 'nopo', 'no_surjal', 'koitem_fg', 'nama_fg', 'supplier', 'batch', 'penempatan', 'koordinat_penempatan', 'status_analis', 'priority', 'kategori', 'sop_npd', 'flag_analis'], 'string'],
            [['datetime_start', 'datetime_stop', 'datetime_write', 'date_penerimaanlog'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jumlah_kedatangan_batch' => 'Jumlah Kedatangan Batch',
            'aql_kedatangan' => 'Aql Kedatangan',
            'pengambilan_sampel' => 'Pengambilan Sampel',
            'temuan_defect' => 'Temuan Defect',
            'kategori_aql' => 'Kategori Aql',
            'datetime_start' => 'Datetime Start',
            'datetime_stop' => 'Datetime Stop',
            'datetime_write' => 'Datetime Write',
            'nopo' => 'Nopo',
            'no_surjal' => 'No Surjal',
            'koitem_fg' => 'Koitem Fg',
            'nama_fg' => 'Nama Fg',
            'supplier' => 'Supplier',
            'batch' => 'Batch',
            'penempatan' => 'Penempatan',
            'sampler_id' => 'Sampler ID',
            'koordinat_penempatan' => 'Koordinat Penempatan',
            'status_analis' => 'Status Analis',
            'priority' => 'Priority',
            'kategori' => 'Kategori',
            'date_penerimaanlog' => 'Date Penerimaanlog',
            'sop_npd' => 'Sop Npd',
            'jumlah_palet' => 'Jumlah Palet',
            'flag_analis' => 'Flag Analis',
        ];
    }
}
