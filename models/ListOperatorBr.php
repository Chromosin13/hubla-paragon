<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "list_operator_br".
 *
 * @property integer $id
 * @property integer $operator
 * @property integer $value
 * @property string $sediaan
 */
class ListOperatorBr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'list_operator_br';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['operator'], 'integer'],
            [['sediaan'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'operator' => 'Operator',
            'sediaan' => 'Sediaan',
        ];
    }
}
