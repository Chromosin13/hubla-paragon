<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gross_leadtime".
 *
 * @property integer $id
 * @property string $nama_fg
 * @property string $snfg
 * @property string $tanggal
 * @property string $tanggal_stop
 * @property string $leadtime_kotor_jam
 * @property string $leadtime_kotor_hari
 */
class GrossLeadtime extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gross_leadtime';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_fg', 'snfg'], 'string'],
            [['tanggal', 'tanggal_stop'], 'safe'],
            [['leadtime_kotor_jam', 'leadtime_kotor_hari'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_fg' => 'Nama Fg',
            'snfg' => 'Snfg',
            'tanggal' => 'Tanggal',
            'tanggal_stop' => 'Tanggal Stop',
            'leadtime_kotor_jam' => 'Leadtime Kotor Jam',
            'leadtime_kotor_hari' => 'Leadtime Kotor Hari',
        ];
    }
}
