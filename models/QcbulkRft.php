<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "qcbulk_rft".
 *
 * @property integer $row_number
 * @property integer $start_id
 * @property string $sediaan
 * @property string $snfg
 * @property string $snfg_komponen
 * @property string $nama_fg
 * @property string $lanjutan
 * @property string $jenis_periksa
 * @property string $jumlah_operator
 * @property string $nama_qc
 * @property integer $stop_id
 * @property string $ph
 * @property string $viskositas
 * @property string $berat_jenis
 * @property string $kadar
 * @property string $warna
 * @property string $bau
 * @property string $performance
 * @property string $bentuk
 * @property string $mikro
 * @property string $kejernihan
 * @property string $status
 * @property integer $is_done
 * @property string $status_rft
 */
class QcbulkRft extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function primaryKey()
    {
        return ['row_number'];
    }
    
    public static function tableName()
    {
        return 'qcbulk_rft';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row_number', 'start_id', 'stop_id', 'is_done'], 'integer'],
            [['sediaan', 'snfg', 'snfg_komponen', 'nama_fg', 'jenis_periksa', 'nama_qc', 'ph', 'viskositas', 'berat_jenis', 'kadar', 'warna', 'bau', 'performance', 'bentuk', 'mikro', 'kejernihan', 'status', 'status_rft'], 'string'],
            [['tanggal_stop'], 'safe'],
            [['lanjutan', 'jumlah_operator'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'row_number' => 'Row Number',
            'start_id' => 'Start ID',
            'sediaan' => 'Sediaan',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'nama_fg' => 'Nama Fg',
            'lanjutan' => 'Lanjutan',
            'jenis_periksa' => 'Jenis Periksa',
            'jumlah_operator' => 'Jumlah Operator',
            'nama_qc' => 'Nama Qc',
            'stop_id' => 'Stop ID',
            'ph' => 'Ph',
            'viskositas' => 'Viskositas',
            'berat_jenis' => 'Berat Jenis',
            'kadar' => 'Kadar',
            'warna' => 'Warna',
            'bau' => 'Bau',
            'performance' => 'Performance',
            'bentuk' => 'Bentuk',
            'mikro' => 'Mikro',
            'kejernihan' => 'Kejernihan',
            'status' => 'Status',
            'is_done' => 'Is Done',
            'status_rft' => 'Status Rft',
            'tanggal_stop' => 'Tanggal Stop',
        ];
    }
}
