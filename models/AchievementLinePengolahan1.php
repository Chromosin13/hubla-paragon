<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "achievement_line_pengolahan_1".
 *
 * @property integer $row_number
 * @property string $tanggal_stop
 * @property string $tanggal_start
 * @property string $nama_line
 * @property string $sediaan
 * @property string $snfg
 * @property string $snfg_komponen
 * @property string $nama_fg
 * @property string $jenis_olah
 * @property string $leadtime
 * @property string $std_leadtime
 * @property string $status_leadtime
 * @property string $plan_start_olah
 * @property string $plan_end_olah
 * @property string $status_ontime_olah
 * @property string $status_ontime_kemas
 * @property string $is_done
 */
class AchievementLinePengolahan1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function primaryKey()
    {
        return ['row_number'];
    }

    
    public static function tableName()
    {
        return 'achievement_line_pengolahan_1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row_number'], 'integer'],
            [['tanggal_stop', 'tanggal_start', 'plan_start_olah', 'plan_end_olah'], 'safe'],
            [['nama_line', 'sediaan', 'snfg', 'snfg_komponen', 'nama_fg', 'jenis_olah', 'status_leadtime', 'status_ontime_olah', 'status_ontime_kemas'], 'string'],
            [['delta_hour_net', 'std_leadtime', 'is_done'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'row_number' => 'Row Number',
            'tanggal_stop' => 'Tanggal Stop',
            'tanggal_start' => 'Tanggal Start',
            'nama_line' => 'Nama Line',
            'sediaan' => 'Sediaan',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'nama_fg' => 'Nama Fg',
            'jenis_olah' => 'Jenis Olah',
            'delta_hour_net' => 'Delta Hour Net',
            'std_leadtime' => 'Std Leadtime',
            'status_leadtime' => 'Status Leadtime',
            'plan_start_olah' => 'Plan Start Olah',
            'plan_end_olah' => 'Plan End Olah',
            'status_ontime_olah' => 'Status Ontime Olah',
            'status_ontime_kemas' => 'Status Ontime Kemas',
            'is_done' => 'Is Done',
        ];
    }
}
