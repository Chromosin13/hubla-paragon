<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "list_bentuk_sediaan".
 *
 * @property integer $id
 * @property string $nama
 */
class ListBentukSediaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'list_bentuk_sediaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
        ];
    }
}
