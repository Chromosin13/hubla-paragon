<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OverallLeadtime;

/**
 * OverallLeadtimeSearch represents the model behind the search form of `app\models\OverallLeadtime`.
 */
class OverallLeadtimeSearch extends OverallLeadtime
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nomo', 'snfg', 'snfg_komponen', 'nama_fg'], 'safe'],
            [['timbang_jadwal_baru', 'timbang_rework', 'olah_olah_premix', 'olah_olah_1', 'olah_olah_2', 'olah_adjust', 'olah_rework', 'qcbulk_jadwal_baru', 'qcbulk_rework', 'kemas1_press', 'kemas1_filling', 'qcfg_jadwal_baru', 'qcfg_rework', 'qcfg_adjust', 'qcfg_pending', 'qcfg_lanjutan', 'kemas2_packing', 'kemas2_press_filling_packing', 'kemas2_filling_packing_inline', 'kemas2_flame_packing_inline'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OverallLeadtime::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'timbang_jadwal_baru' => $this->timbang_jadwal_baru,
            'timbang_rework' => $this->timbang_rework,
            'olah_olah_premix' => $this->olah_olah_premix,
            'olah_olah_1' => $this->olah_olah_1,
            'olah_olah_2' => $this->olah_olah_2,
            'olah_adjust' => $this->olah_adjust,
            'olah_rework' => $this->olah_rework,
            'qcbulk_jadwal_baru' => $this->qcbulk_jadwal_baru,
            'qcbulk_rework' => $this->qcbulk_rework,
            'kemas1_press' => $this->kemas1_press,
            'kemas1_filling' => $this->kemas1_filling,
            'qcfg_jadwal_baru' => $this->qcfg_jadwal_baru,
            'qcfg_rework' => $this->qcfg_rework,
            'qcfg_adjust' => $this->qcfg_adjust,
            'qcfg_pending' => $this->qcfg_pending,
            'qcfg_lanjutan' => $this->qcfg_lanjutan,
            'kemas2_packing' => $this->kemas2_packing,
            'kemas2_press_filling_packing' => $this->kemas2_press_filling_packing,
            'kemas2_filling_packing_inline' => $this->kemas2_filling_packing_inline,
            'kemas2_flame_packing_inline' => $this->kemas2_flame_packing_inline,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg]);

        return $dataProvider;
    }
}
