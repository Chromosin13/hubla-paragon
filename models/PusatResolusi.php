<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pusat_resolusi".
 *
 * @property integer $id
 * @property string $proses
 * @property string $nojadwal
 * @property string $permasalahan
 * @property string $nama_operator
 * @property string $status
 */
class PusatResolusi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pusat_resolusi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proses', 'nojadwal', 'permasalahan', 'nama_operator', 'status','nama_leader'], 'string'],
            [['s_id'], 'integer'],
            [['kategori','proses'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Nomor Ticket',
            'proses' => 'Proses',
            'nama_leader' => 'Leader',
            'nojadwal' => 'Nomor Jadwal',
            'permasalahan' => 'Keterangan',
            'nama_operator' => 'Nama Kapten',
            'status' => 'Status',
            'kategori' => 'Kategori',
            's_id' => 'Reference'
        ];
    }
}
