<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PenimbanganEditJadwal;

/**
 * PenimbanganEditJadwalSearch represents the model behind the search form of `app\models\PenimbanganEditJadwal`.
 */
class PenimbanganEditJadwalSearch extends PenimbanganEditJadwal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'start_id'], 'integer'],
            [['nomo', 'snfg_komponen', 'start', 'stop', 'jenis_penimbangan', 'nama_line', 'nama_operator'], 'safe'],
            [['lanjutan'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PenimbanganEditJadwal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lanjutan' => $this->lanjutan,
            'start' => $this->start,
            'stop' => $this->stop,
            'start_id' => $this->start_id,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'jenis_penimbangan', $this->jenis_penimbangan])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator]);

        return $dataProvider;
    }
}
