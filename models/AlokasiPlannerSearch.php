<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AlokasiPlanner;

/**
 * AlokasiPlannerSearch represents the model behind the search form about `app\models\AlokasiPlanner`.
 */
class AlokasiPlannerSearch extends AlokasiPlanner
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sediaan', 'koitem_baru', 'naitem', 'brand', 'nors','week_plot'], 'safe'],
            [['week_rs', 'plot'], 'number'],
            [['alokasi', 'row_number'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AlokasiPlanner::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'week_plot' => $this->week_plot,
            'week_rs' => $this->week_rs,
            'alokasi' => $this->alokasi,
            'plot' => $this->plot,
            'row_number' => $this->row_number,
        ]);

        $query->andFilterWhere(['like', 'sediaan', $this->sediaan])
            ->andFilterWhere(['like', 'koitem_baru', $this->koitem_baru])
            ->andFilterWhere(['like', 'naitem', $this->naitem])
            ->andFilterWhere(['like', 'brand', $this->brand])
            ->andFilterWhere(['like', 'nors', $this->nors]);

        return $dataProvider;
    }
}
