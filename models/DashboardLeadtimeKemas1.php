<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dashboard_leadtime_kemas1".
 *
 * @property string $nama_fg
 * @property string $snfg_komponen
 * @property string $posisi
 * @property string $start
 * @property string $stop
 * @property string $sum_leadtime_raw
 * @property string $sum_leadtime_bruto
 * @property string $jenis_proses
 * @property string $sediaan
 * @property string $sum_downtime
 * @property string $sum_adjust
 * @property string $sum_leadtime_net
 * @property string $keterangan_downtime
 * @property string $keterangan_adjust
 * @property string $snfg
 * @property string $nomo
 * @property integer $id
 * @property string $upload_date
 * @property integer $week
 */
class DashboardLeadtimeKemas1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dashboard_leadtime_kemas1';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_fg', 'snfg_komponen', 'posisi', 'nama_line', 'sum_leadtime_raw', 'sum_leadtime_bruto', 'jenis_proses', 'sediaan', 'sum_downtime', 'sum_adjust', 'sum_leadtime_net', 'keterangan_downtime', 'keterangan_adjust', 'snfg', 'nomo'], 'string'],
            [['start', 'stop', 'upload_date'], 'safe'],
            [['id', 'week'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nama_fg' => 'Nama Fg',
            'snfg_komponen' => 'Snfg Komponen',
            'posisi' => 'Posisi',
            'nama_line' => 'Nama Line',
            'start' => 'Start',
            'stop' => 'Stop',
            'sum_leadtime_raw' => 'Sum Leadtime Raw',
            'sum_leadtime_bruto' => 'Sum Leadtime Bruto',
            'jenis_proses' => 'Jenis Proses',
            'sediaan' => 'Sediaan',
            'sum_downtime' => 'Sum Downtime',
            'sum_adjust' => 'Sum Adjust',
            'sum_leadtime_net' => 'Sum Leadtime Net',
            'keterangan_downtime' => 'Keterangan Downtime',
            'keterangan_adjust' => 'Keterangan Adjust',
            'snfg' => 'Snfg',
            'nomo' => 'Nomo',
            'id' => 'ID',
            'upload_date' => 'Upload Date',
            'week' => 'Week',
        ];
    }
}
