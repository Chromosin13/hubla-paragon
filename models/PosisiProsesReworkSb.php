<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posisi_proses_rework_sb".
 *
 * @property integer $id
 * @property string $snfg_komponen
 * @property string $snfg
 * @property string $posisi
 * @property string $timestamp
 * @property string $status
 * @property integer $lanjutan_split_batch
 */
class PosisiProsesReworkSb extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posisi_proses_rework_sb';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lanjutan_split_batch'], 'integer'],
            [['snfg_komponen', 'snfg', 'posisi', 'status'], 'string'],
            [['timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg_komponen' => 'Snfg Komponen',
            'snfg' => 'Snfg',
            'posisi' => 'Posisi',
            'timestamp' => 'Timestamp',
            'status' => 'Status',
            'lanjutan_split_batch' => 'Lanjutan Split Batch',
        ];
    }
}
