<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\QcBulkEntryKomponen;

/**
 * QcBulkEntryKomponenSearch represents the model behind the search form of `app\models\QcBulkEntryKomponen`.
 */
class QcBulkEntryKomponenSearch extends QcBulkEntryKomponen
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nourut', 'id'], 'integer'],
            [['nomo', 'status', 'timestamp', 'nama_inspektor', 'created_at', 'snfg_komponen'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QcBulkEntryKomponen::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['nourut'=>SORT_ASC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'nourut' => $this->nourut,
            'timestamp' => $this->timestamp,
            'created_at' => $this->created_at,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'nama_inspektor', $this->nama_inspektor])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen]);

        return $dataProvider;
    }
}
