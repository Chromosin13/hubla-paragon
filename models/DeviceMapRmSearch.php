<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DeviceMapRm;

/**
 * DeviceMapRmSearch represents the model behind the search form about `app\models\DeviceMapRm`.
 */
class DeviceMapRmSearch extends DeviceMapRm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'operator'], 'integer'],
            [['device_ip', 'raspi_ip', 'nama_line', 'keterangan', 'mac_address'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DeviceMapRm::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'operator' => $this->operator,
        ]);

        $query->andFilterWhere(['like', 'device_ip', $this->device_ip])
            ->andFilterWhere(['like', 'raspi_ip', $this->raspi_ip])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'mac_address', $this->mac_address]);

        return $dataProvider;
    }
}
