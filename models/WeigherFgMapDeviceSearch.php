<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\WeigherFgMapDevice;

/**
 * WeigherFgMapDeviceSearch represents the model behind the search form about `app\models\WeigherFgMapDevice`.
 */
class WeigherFgMapDeviceSearch extends WeigherFgMapDevice
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['frontend_ip', 'sbc_ip', 'line', 'posisi', 'keterangan', 'username'], 'safe'],
            [['is_counter'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WeigherFgMapDevice::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'frontend_ip', $this->frontend_ip])
            ->andFilterWhere(['like', 'sbc_user', $this->sbc_user])
            ->andFilterWhere(['like', 'sbc_ip', $this->sbc_ip])
            ->andFilterWhere(['like', 'line', $this->line])
            ->andFilterWhere(['like', 'posisi', $this->posisi])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
