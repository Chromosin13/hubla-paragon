<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrackingJit;

/**
 * TrackingJitSearch represents the model behind the search form about `app\models\TrackingJit`.
 */
class TrackingJitSearch extends TrackingJit
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'no_palet'], 'integer'],
            [['nama_fg', 'snfg', 'status', 'last_updated', 'timestamp_check', 'timestamp_terima_karantina', 'timestamp_siap_kirim'], 'safe'],
            [['qty'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrackingJit::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'no_palet' => $this->no_palet,
            'qty' => $this->qty,
            'last_updated' => $this->last_updated,
            'timestamp_check' => $this->timestamp_check,
            'timestamp_terima_karantina' => $this->timestamp_terima_karantina,
            'timestamp_siap_kirim' => $this->timestamp_siap_kirim,
        ]);

        $query->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
