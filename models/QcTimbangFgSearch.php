<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\QcTimbangFg;

/**
 * QcTimbangFgSearch represents the model behind the search form about `app\models\QcTimbangFg`.
 */
class QcTimbangFgSearch extends QcTimbangFg
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['snfg', 'tanggal', 'nama_inspektor', 'waktu', 'state', 'status','nama_line','jenis_periksa','snfg_komponen'], 'safe'],
            [['palet_ke', 'qty','lanjutan','jumlah_inspektor','lanjutan_ist','is_done','lanjutan_split_batch'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QcTimbangFg::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'palet_ke' => $this->palet_ke,
            'tanggal' => $this->tanggal,
            'waktu' => $this->waktu,
            'qty' => $this->qty,
            'lanjutan' => $this->lanjutan,
            'jumlah_inspektor' => $this->jumlah_inspektor,
            'lanjutan_ist' => $this->lanjutan_ist,
            'is_done' => $this->is_done,
            'lanjutan_split_batch'=> $this->lanjutan_split_batch,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'nama_inspektor', $this->nama_inspektor])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like','snfg_komponen',$this->snfg_komponen])
            ->andFilterWhere(['like', 'jenis_periksa', $this->jenis_periksa]);

        return $dataProvider;
    }
}
