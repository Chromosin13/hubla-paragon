<?php

namespace app\models;

use Yii;
use app\models\LogPenimbanganRm;
use app\models\DeviceMapRm;

/**
 * This is the model class for table "log_penimbangan_rm_operator".
 *
 * @property integer $id
 * @property integer $flow_input_mo_id
 * @property string $nama_operator
 * @property string $timestamp
 * @property integer $operator
 */
class LogPenimbanganRmOperator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_penimbangan_rm_operator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['flow_input_mo_id', 'operator'], 'integer'],
            [['nama_operator'], 'string'],
            [['timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'flow_input_mo_id' => 'Flow Input Mo ID',
            'nama_operator' => 'Nama Operator',
            'timestamp' => 'Timestamp',
            'operator' => 'Operator',
        ];
    }

    /* Check if the clicked operator already used by other
     *
     *
    */
    public function checkSelectedOperator($id,$nama_operator,$operator)
    {

        // Get Original IP Address from direct IP / DNS access
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];            
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        // Get Mapping Line - Frontend IP Array
        $sql="
        SELECT 
            *
        FROM device_map_rm
        WHERE device_ip = '".$ip."' ";

        $map_device_array = DeviceMapRm::findBySql($sql)->one();

        // Assign Nama Line from Array into a string variable, if not exist use default hardcoded mapping.
        // $operator = $map_device_array->operator;

        // Get Mapping Line - Frontend IP Array
        $sql2="
        SELECT * 
        FROM (
            SELECT DISTINCT ON (operator) * 
            FROM log_penimbangan_rm_operator 
            WHERE flow_input_mo_id = :flow_input_mo_id 
            ORDER BY operator,id DESC) a 
        WHERE nama_operator ilike :nama_operator ";


        $isSameName = LogPenimbanganRmOperator::findBySql($sql2,[':flow_input_mo_id'=>$id,':nama_operator'=>'%'.$nama_operator.'%'])->one();

        //selected in same device = -1, action delete op name
        //selected in other device = -2, block
        //not selected in any device = 0, insert operator name
        //update operator name = 1, update op name
        //max operator reached = 2, block

        //Jika nama op sudah pernah dipilih sebelumnya, cek terlebih dahulu dipilih di tab mana
        if (!empty($isSameName)){

            //Jika nama operator sudah di pilih di tab yang sama
            if ($isSameName['operator'] == $operator){
                //delete nama op yg dipilih
                return -1;

            
            //JIka nama op sudah dipilih di tab lain
            }else{
                //block pilih operator, nama op sudah dipilih di tab lain, munculkan notifikasi
                return -2;

            }

        //Jika operator belum dipilih di tab manapun
        }else{

            //Cek apakah di tab sendiri nama op yang di assign sudah ada 2 orang atau belum
            $sql3= "SELECT * 
                        FROM log_penimbangan_rm_operator 
                        WHERE operator = :operator and flow_input_mo_id = :flow_input_mo_id 
                        ORDER BY id DESC";

            $check_current_tab = LogPenimbanganRmOperator::findBySql($sql3,[':operator'=>$operator,':flow_input_mo_id'=>$id])->one();

            if (!empty($check_current_tab)){
    
                $list_operator_current = explode(',',$check_current_tab->nama_operator);
    
                //Jika nama op yang dipilih sudah lebih atau sama dengan 2, block pilih nama op, munculkan notif
                if (count($list_operator_current)>=2){
                    //block pilih nama op
                    return 2;
                
                }else if (count($list_operator_current==1)){
                    //insert data baru, nama op lama + nama op baru
                    return 1;

                }else{
                    //insert data baru, nama op baru
                    return 0;
                }
            
            
            }else{
                //insert data baru, nama op baru
                return 0;
            
            }
        }

    }
}
