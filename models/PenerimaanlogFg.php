<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "penerimaanlog_fg".
 *
 * @property integer $id
 * @property integer $jumlah_koli
 * @property integer $qty_per_koli
 * @property integer $qty_koli_receh
 * @property integer $jumlah
 * @property integer $jumlah_koli_palet
 * @property integer $jumlah_palet
 * @property string $datetime_start
 * @property string $datetime_stop
 * @property integer $adminlog_fg_id
 * @property integer $jumlah_pcs_palet
 * @property integer $koordinat_penempatan
 * @property integer $operator_id
 * @property string $penempatan
 * @property integer $sisa_koli_palet
 * @property integer $sisa_pcs_palet
 * @property integer $status_print
 * @property integer $flag_sampler
 * @property string $nopo
 * @property string $no_surjal
 * @property string $koitem_fg
 * @property string $nama_fg
 * @property string $supplier
 * @property string $batch
 */
class PenerimaanlogFg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'penerimaanlog_fg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          [['jumlah_koli', 'qty_per_koli', 'qty_koli_receh', 'jumlah', 'jumlah_koli_palet', 'jumlah_palet', 'adminlog_fg_id', 'jumlah_pcs_palet', 'koordinat_penempatan', 'operator_id', 'sisa_koli_palet', 'sisa_pcs_palet', 'status_print'], 'integer'],
            [['jumlah_koli', 'qty_per_koli', 'qty_koli_receh', 'jumlah', 'jumlah_koli_palet', 'jumlah_palet', 'jumlah_pcs_palet', 'koordinat_penempatan', 'sisa_koli_palet', 'sisa_pcs_palet'], 'required'],
            [['datetime_start', 'datetime_stop'], 'safe'],
            [['penempatan', 'nopo', 'no_surjal', 'koitem_fg', 'nama_fg', 'supplier', 'batch', 'nomor_batch', 'flag_sampler'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jumlah_koli' => 'Jumlah Koli',
            'qty_per_koli' => 'Qty Per Koli',
            'qty_koli_receh' => 'Qty Koli Receh',
            'jumlah' => 'Jumlah',
            'jumlah_koli_palet' => 'Jumlah Koli Palet',
            'jumlah_palet' => 'Jumlah Palet',
            'datetime_start' => 'Datetime Start',
            'datetime_stop' => 'Datetime Stop',
            'adminlog_fg_id' => 'Adminlog Fg ID',
            'jumlah_pcs_palet' => 'Jumlah Pcs Palet',
            'koordinat_penempatan' => 'Koordinat Penempatan',
            'operator_id' => 'Operator ID',
            'penempatan' => 'Penempatan',
            'sisa_koli_palet' => 'Sisa Koli Palet',
            'sisa_pcs_palet' => 'Sisa Pcs Palet',
            'status_print' => 'Status Print',
            'flag_sampler' => 'Flag Sampler',
            'nopo' => 'Nopo',
            'no_surjal' => 'No Surjal',
            'koitem_fg' => 'Koitem Fg',
            'nama_fg' => 'Nama Fg',
            'supplier' => 'Supplier',
            'batch' => 'Batch',
        ];
    }
}
