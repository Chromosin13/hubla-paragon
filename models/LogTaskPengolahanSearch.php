<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogTaskPengolahan;

/**
 * LogTaskPengolahanSearch represents the model behind the search form about `app\models\LogTaskPengolahan`.
 */
class LogTaskPengolahanSearch extends LogTaskPengolahan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['kode_bb', 'nama_bb', 'uom', 'kode_olah', 'status', 'pic', 'timestamp_awal', 'timestamp_checked', 'nomo', 'log', 'kode_internal'], 'safe'],
            [['qty'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogTaskPengolahan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->orderBy([
            // 'kode_bb' => SORT_ASC,
            // 'uom' => SORT_ASC,
            // 'qty' => SORT_DESC, 
            'id' => SORT_ASC,     
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'qty' => $this->qty,
            'timestamp_awal' => $this->timestamp_awal,
            'timestamp_checked' => $this->timestamp_checked,
        ]);

        $query->andFilterWhere(['like', 'kode_bb', $this->kode_bb])
            ->andFilterWhere(['like', 'nama_bb', $this->nama_bb])
            ->andFilterWhere(['like', 'uom', $this->uom])
            ->andFilterWhere(['like', 'kode_olah', $this->kode_olah])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'pic', $this->pic])
            ->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'log', $this->log])
            ->andFilterWhere(['like', 'kode_internal', $this->kode_internal]);

        return $dataProvider;
    }
}
