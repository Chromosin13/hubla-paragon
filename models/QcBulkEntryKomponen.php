<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "qc_bulk_entry_komponen".
 *
 * @property integer $nourut
 * @property string $nomo
 * @property string $status
 * @property string $timestamp
 * @property string $nama_inspektor
 * @property string $created_at
 * @property string $snfg_komponen
 * @property integer $id
 */
class QcBulkEntryKomponen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $besar_batch_real;
    public $storage;
    public $savelife;
    public $unit;

    public static function tableName()
    {
        return 'qc_bulk_entry_komponen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_inspektor','savelife','snfg_komponen','storage'],'required'],
            [['nourut','storage','no_storage','savelife'], 'integer'],
            [['nomo', 'status', 'snfg_komponen', 'keterangan','nobatch'], 'string'],
            [['timestamp', 'created_at', 'savelife_storage'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'keterangan' => 'Keterangan',
            'nourut' => 'Nourut',
            'nomo' => 'Nomo',
            'status' => 'Status',
            'timestamp' => 'Timestamp',
            'nama_inspektor' => 'Nama Inspektor',
            'created_at' => 'Created At',
            'snfg_komponen' => 'Snfg Komponen',
            'id' => 'ID',
            'nobatch' => 'No Batch',
            'no_storage' => 'No Storage',
        ];
    }
}
