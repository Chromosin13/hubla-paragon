<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "device_map_qcbulk".
 *
 * @property integer $id
 * @property string $sbc_ip
 * @property string $frontend_ip
 * @property string $keterangan
 * @property string $sbc_user
 * @property string $posisi
 * @property string $mac
 */
class DeviceMapQcbulk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'device_map_qcbulk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sbc_ip', 'frontend_ip', 'keterangan', 'sbc_user', 'posisi', 'mac'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sbc_ip' => 'Sbc Ip',
            'frontend_ip' => 'Frontend Ip',
            'keterangan' => 'Keterangan',
            'sbc_user' => 'Sbc User',
            'posisi' => 'Posisi',
            'mac' => 'Mac',
        ];
    }
}
