<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PenerimaanlogFg;

/**
 * PenerimaanlogFgSearch represents the model behind the search form about `app\models\PenerimaanlogFg`.
 */
class PenerimaanlogFgSearch extends PenerimaanlogFg
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'jumlah_koli', 'qty_per_koli', 'qty_koli_receh', 'jumlah', 'jumlah_koli_palet', 'jumlah_palet', 'adminlog_fg_id', 'jumlah_pcs_palet', 'koordinat_penempatan', 'operator_id', 'sisa_koli_palet', 'sisa_pcs_palet', 'status_print', 'flag_sampler'], 'integer'],
            [['datetime_start', 'datetime_stop', 'penempatan', 'nopo', 'no_surjal', 'koitem_fg', 'nama_fg', 'supplier', 'batch'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PenerimaanlogFg::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'jumlah_koli' => $this->jumlah_koli,
            'qty_per_koli' => $this->qty_per_koli,
            'qty_koli_receh' => $this->qty_koli_receh,
            'jumlah' => $this->jumlah,
            'jumlah_koli_palet' => $this->jumlah_koli_palet,
            'jumlah_palet' => $this->jumlah_palet,
            'datetime_start' => $this->datetime_start,
            'datetime_stop' => $this->datetime_stop,
            'adminlog_fg_id' => $this->adminlog_fg_id,
            'jumlah_pcs_palet' => $this->jumlah_pcs_palet,
            'koordinat_penempatan' => $this->koordinat_penempatan,
            'operator_id' => $this->operator_id,
            'sisa_koli_palet' => $this->sisa_koli_palet,
            'sisa_pcs_palet' => $this->sisa_pcs_palet,
            'status_print' => $this->status_print,
            'flag_sampler' => $this->flag_sampler,
        ]);

        $query->andFilterWhere(['like', 'penempatan', $this->penempatan])
            ->andFilterWhere(['like', 'nopo', $this->nopo])
            ->andFilterWhere(['like', 'no_surjal', $this->no_surjal])
            ->andFilterWhere(['like', 'koitem_fg', $this->koitem_fg])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'supplier', $this->supplier])
            ->andFilterWhere(['like', 'batch', $this->batch]);

        return $dataProvider;
    }
}
