<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flow_input_gbb".
 *
 * @property integer $id
 * @property string $datetime_start
 * @property string $datetime_stop
 * @property string $nomo
 * @property integer $lanjutan
 * @property string $nama_operator
 * @property integer $siklus
 */
class FlowInputGbb extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flow_input_gbb';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['datetime_start', 'datetime_stop'], 'safe'],
            [['nomo', 'nama_operator'], 'string'],
            [['lanjutan', 'siklus'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'datetime_start' => 'Datetime Start',
            'datetime_stop' => 'Datetime Stop',
            'nomo' => 'Nomo',
            'lanjutan' => 'Lanjutan',
            'nama_operator' => 'Nama Operator',
            'siklus' => 'Siklus',
        ];
    }
}
