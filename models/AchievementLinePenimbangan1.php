<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "achievement_line_penimbangan_1".
 *
 * @property integer $row_number
 * @property string $snfg_komponen
 * @property string $snfg
 * @property string $nama_fg
 * @property string $jenis_penimbangan
 * @property string $nama_line
 * @property string $tanggal_start
 * @property string $tanggal_stop
 * @property string $is_done
 * @property string $leadtime
 * @property string $std_leadtime
 * @property string $status_leadtime
 * @property string $plan_start_timbang
 * @property string $status_ontime
 */
class AchievementLinePenimbangan1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function primaryKey()
    {
        return ['row_number'];
    }
    
    public static function tableName()
    {
        return 'achievement_line_penimbangan_1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row_number'], 'integer'],
            [['snfg_komponen', 'snfg', 'nama_fg', 'jenis_penimbangan', 'nama_line', 'status_leadtime', 'status_ontime'], 'string'],
            [['tanggal_start', 'tanggal_stop', 'plan_start_timbang'], 'safe'],
            [['is_done', 'leadtime', 'std_leadtime'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'row_number' => 'Row Number',
            'snfg_komponen' => 'Snfg Komponen',
            'snfg' => 'Snfg',
            'nama_fg' => 'Nama Fg',
            'jenis_penimbangan' => 'Jenis Penimbangan',
            'nama_line' => 'Nama Line',
            'tanggal_start' => 'Tanggal Start',
            'tanggal_stop' => 'Tanggal Stop',
            'is_done' => 'Is Done',
            'leadtime' => 'Leadtime',
            'std_leadtime' => 'Std Leadtime',
            'status_leadtime' => 'Status Leadtime',
            'plan_start_timbang' => 'Plan Start Timbang',
            'status_ontime' => 'Status Ontime',
        ];
    }
}
