<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_task_checker".
 *
 * @property integer $id
 * @property string $kode_bb
 * @property string $nama_bb
 * @property string $qty
 * @property string $uom
 * @property string $kode_olah
 * @property string $status
 * @property string $pic
 * @property string $timestamp
 */
class LogTaskChecker extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_task_checker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_bb', 'nama_bb', 'uom', 'kode_olah', 'status', 'pic', 'nomo', 'log','id_bb'], 'string'],
            [['siklus'], 'integer'],
            [['qty'], 'number'],
            [['timestamp_awal', 'timestamp_checked'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode_bb' => 'Kode Bb',
            'nama_bb' => 'Nama Bb',
            'qty' => 'Qty',
            'uom' => 'Uom',
            'kode_olah' => 'Kode Olah',
            'status' => 'Status',
            'pic' => 'Pic',
            'timestamp_awal' => 'Timestamp',
            'timestamp_checked' => 'Timestamp Checked',
            'nomo' => 'Nomo',
            'log' => 'Scan Kode Item',
            'id_bb' => 'ID BB'
        ];
    }
}
