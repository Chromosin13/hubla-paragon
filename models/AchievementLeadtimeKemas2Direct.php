<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "achievement_leadtime_kemas2_direct".
 *
 * @property string $nomo
 * @property string $snfg
 * @property string $snfg_komponen
 * @property string $jenis_kemas
 * @property string $lanjutan
 * @property string $nama_line
 * @property string $nama_operator
 * @property string $tanggal
 * @property string $tanggal_stop
 * @property string $delta_minute
 * @property string $delta_hour
 * @property string $koitem_bulk
 * @property string $koitem_fg
 * @property string $nama_fg
 * @property string $nama_bulk
 * @property integer $is_done
 * @property string $delta_minute_net
 * @property string $delta_hour_net
 * @property string $lanjutan_split_batch
 * @property string $jumlah_realisasi
 * @property string $jam_per_pcs
 * @property string $mpq
 * @property string $achievement
 * @property integer $id
 * @property integer $start_id
 * @property integer $stop_id
 */
class AchievementLeadtimeKemas2Direct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'achievement_leadtime_kemas2_direct';
    }

    public static function primaryKey()
    {
        return ['stop_id'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'snfg', 'snfg_komponen', 'jenis_kemas', 'nama_line', 'nama_operator', 'koitem_bulk', 'koitem_fg', 'nama_fg', 'nama_bulk'], 'string'],
            [['lanjutan', 'delta_minute', 'delta_hour', 'delta_minute_net', 'delta_hour_net', 'lanjutan_split_batch', 'jumlah_realisasi', 'jam_per_pcs', 'mpq', 'achievement'], 'number'],
            [['tanggal', 'tanggal_stop'], 'safe'],
            [['is_done', 'id', 'start_id', 'stop_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nomo' => 'Nomo',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'jenis_kemas' => 'Jenis Kemas',
            'lanjutan' => 'Lanjutan',
            'nama_line' => 'Nama Line',
            'nama_operator' => 'Nama Operator',
            'tanggal' => 'Tanggal',
            'tanggal_stop' => 'Tanggal Stop',
            'delta_minute' => 'Delta Minute',
            'delta_hour' => 'Delta Hour',
            'koitem_bulk' => 'Koitem Bulk',
            'koitem_fg' => 'Koitem Fg',
            'nama_fg' => 'Nama Fg',
            'nama_bulk' => 'Nama Bulk',
            'is_done' => 'Is Done',
            'delta_minute_net' => 'Delta Minute Net',
            'delta_hour_net' => 'Delta Hour Net',
            'lanjutan_split_batch' => 'Lanjutan Split Batch',
            'jumlah_realisasi' => 'Jumlah Realisasi',
            'jam_per_pcs' => 'Jam Per Pcs',
            'mpq' => 'Mpq',
            'achievement' => 'Achievement',
            'id' => 'ID',
            'start_id' => 'Start ID',
            'stop_id' => 'Stop ID',
        ];
    }
}
