<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MasterDataKoli;

/**
 * MasterDataKoliSerach represents the model behind the search form about `app\models\MasterDataKoli`.
 */
class MasterDataKoliSerach extends MasterDataKoli
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'qty', 'inner_box', 'qty_inside_innerbox', 'qty_innerbox_per_karbox'], 'integer'],
            [['koitem', 'naitem', 'sediaan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterDataKoli::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'qty' => $this->qty,
            'inner_box' => $this->inner_box,
            'qty_inside_innerbox' => $this->qty_inside_innerbox,
            'qty_innerbox_per_karbox' => $this->qty_innerbox_per_karbox,
        ]);

        $query->andFilterWhere(['like', 'koitem', $this->koitem])
            ->andFilterWhere(['like', 'naitem', $this->naitem])
            ->andFilterWhere(['like', 'sediaan', $this->sediaan]);

        return $dataProvider;
    }
}
