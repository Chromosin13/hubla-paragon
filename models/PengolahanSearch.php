<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pengolahan;

/**
 * PengolahanSearch represents the model behind the search form about `app\models\Pengolahan`.
 */
class PengolahanSearch extends Pengolahan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['snfg', 'nobatch', 'nama_line', 'nama_operator', 'operator_2','nomo', 'waktu', 'state','jenis_olah','snfg_komponen','timestamp','plan_end_olah','plan_start_olah','turun_bulk_start','turun_bulk_stop','ember_start','ember_stop','adjust_start','adjust_stop'], 'safe'],
            [['jumlah_operator', 'lanjutan','besar_batch_real','lanjutan_ist','is_done','shift_plan_end_olah','shift_plan_start_olah'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pengolahan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'jumlah_operator' => $this->jumlah_operator,
            'waktu' => $this->waktu,
            'timestamp' => $this->timestamp,
            'lanjutan' => $this->lanjutan,
            'plan_start_olah'=> $this->plan_start_olah,
            'plan_end_olah'=> $this->plan_end_olah,
            'shift_plan_start_olah'=> $this->shift_plan_start_olah,
            'shift_plan_end_olah'=> $this->shift_plan_end_olah,
            'besar_batch_real' => $this->besar_batch_real,
            'lanjutan_ist' => $this->lanjutan_ist,
            'is_done' => $this->is_done,
            'nomo' => $this->nomo,
            'turun_bulk_start' => $this->turun_bulk_start,
            'turun_bulk_stop' => $this->turun_bulk_stop,
            'ember_start' => $this->ember_start,
            'ember_stop' => $this->ember_stop,
            'adjust_start' => $this->adjust_start,
            'adjust_stop' => $this->adjust_stop,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'nobatch', $this->nobatch])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator])
            ->andFilterWhere(['like', 'operator_2', $this->operator_2])
            ->andFilterWhere(['like', 'jenis_olah', $this->jenis_olah])
            ->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'state', $this->state]);

        return $dataProvider;
    }
}
