<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_grn".
 *
 * @property string $nomor_po
 * @property string $supplier
 * @property string $tgl_po
 * @property string $tgl_plan_kirim
 * @property string $kode_item
 * @property string $nama_dagang
 * @property string $nama_pabrik
 * @property string $qty_demand
 * @property string $price_unit
 * @property string $category
 * @property string $tgl_kirim
 * @property string $qty_kirim
 * @property string $nomor_grn
 * @property string $state
 * @property integer $stock_picking_id
 * @property integer $stock_move_id
 * @property integer $sml_id
 */
class DataGrn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_grn';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db3');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomor_po', 'supplier', 'kode_item', 'nama_dagang', 'nama_pabrik', 'category', 'nomor_grn', 'state'], 'string'],
            [['tgl_po', 'tgl_plan_kirim', 'tgl_kirim'], 'safe'],
            [['qty_demand', 'price_unit', 'qty_kirim'], 'number'],
            [['stock_picking_id', 'stock_move_id', 'sml_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nomor_po' => 'Nomor Po',
            'supplier' => 'Supplier',
            'tgl_po' => 'Tgl Po',
            'tgl_plan_kirim' => 'Tgl Plan Kirim',
            'kode_item' => 'Kode Item',
            'nama_dagang' => 'Nama Dagang',
            'nama_pabrik' => 'Nama Pabrik',
            'qty_demand' => 'Qty Demand',
            'price_unit' => 'Price Unit',
            'category' => 'Category',
            'tgl_kirim' => 'Tgl Kirim',
            'qty_kirim' => 'Qty Kirim',
            'nomor_grn' => 'Nomor Grn',
            'state' => 'State',
            'stock_picking_id' => 'Stock Picking ID',
            'stock_move_id' => 'Stock Move ID',
            'sml_id' => 'Sml ID',
        ];
    }
}
