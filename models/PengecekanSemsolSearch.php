<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PengecekanSemsol;

/**
 * PengecekanSemsolSearch represents the model behind the search form about `app\models\PengecekanSemsol`.
 */
class PengecekanSemsolSearch extends PengecekanSemsol
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pengecekan_umum'], 'integer'],
            [['uji_ayun', 'uji_ulir', 'uji_ketrok', 'uji_oles', 'uji_sweating', 'pengecekan_warna'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengecekanSemsol::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_pengecekan_umum' => $this->id_pengecekan_umum,
        ]);

        $query->andFilterWhere(['like', 'uji_ayun', $this->uji_ayun])
            ->andFilterWhere(['like', 'uji_ulir', $this->uji_ulir])
            ->andFilterWhere(['like', 'uji_ketrok', $this->uji_ketrok])
            ->andFilterWhere(['like', 'uji_oles', $this->uji_oles])
            ->andFilterWhere(['like', 'uji_sweating', $this->uji_sweating])
            ->andFilterWhere(['like', 'pengecekan_warna', $this->pengecekan_warna]);

        return $dataProvider;
    }
}
