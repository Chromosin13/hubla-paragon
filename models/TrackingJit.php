<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tracking_jit".
 *
 * @property integer $id
 * @property string $nama_fg
 * @property string $snfg
 * @property integer $no_palet
 * @property string $qty
 * @property string $status
 * @property string $last_updated
 * @property string $timestamp_check
 * @property string $timestamp_terima_karantina
 * @property string $timestamp_siap_kirim
 */
class TrackingJit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
      return ['id'];
    }
    public static function tableName()
    {
        return 'tracking_jit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'no_palet'], 'integer'],
            [['nama_fg', 'snfg', 'status','koitem_fg','alokasi'], 'string'],
            [['qty'], 'number'],
            [['last_updated', 'timestamp_check', 'timestamp_terima_karantina', 'timestamp_siap_kirim'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'koitem_fg' => 'Koitem Fg',
            'nama_fg' => 'Nama Fg',
            'snfg' => 'Snfg',
            'no_palet' => 'No Palet',
            'qty' => 'Qty',
            'status' => 'Status',
            'last_updated' => 'Last Updated',
            'timestamp_check' => 'Timestamp Check',
            'timestamp_terima_karantina' => 'Timestamp Terima Karantina',
            'timestamp_siap_kirim' => 'Timestamp Siap Kirim',
        ];
    }
}
