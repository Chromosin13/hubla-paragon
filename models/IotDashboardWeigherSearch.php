<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IotDashboardWeigher;

/**
 * IotDashboardWeigherSearch represents the model behind the search form about `app\models\IotDashboardWeigher`.
 */
class IotDashboardWeigherSearch extends IotDashboardWeigher
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'nama_line', 'nama_operator', 'nama_operator_real', 'jenis_kemas', 'nobatch', 'write_time'], 'safe'],
            [['is_done', 'is_done_wo', 'flow_input_snfg_id', 'jumlah_wo', 'pass', 'less', 'over', 'rft', 'uid'], 'integer'],
            [['error_rate', 'success_rate'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IotDashboardWeigher::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'is_done' => $this->is_done,
            'is_done_wo' => $this->is_done_wo,
            'write_time' => $this->write_time,
            'flow_input_snfg_id' => $this->flow_input_snfg_id,
            'jumlah_wo' => $this->jumlah_wo,
            'pass' => $this->pass,
            'less' => $this->less,
            'over' => $this->over,
            'rft' => $this->rft,
            'error_rate' => $this->error_rate,
            'success_rate' => $this->success_rate,
            'uid' => $this->uid,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator])
            ->andFilterWhere(['like', 'nama_operator_real', $this->nama_operator_real])
            ->andFilterWhere(['like', 'jenis_kemas', $this->jenis_kemas])
            ->andFilterWhere(['like', 'nobatch', $this->nobatch]);

        return $dataProvider;
    }
}
