<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dashboard_idletime_pengolahan".
 *
 * @property string $nomo
 * @property string $snfg
 * @property string $snfg_komponen
 * @property string $nama_fg
 * @property string $sediaan
 * @property string $upload_date
 * @property integer $week
 * @property string $idle_timbang_olah
 * @property string $idle_olahpremix_olah1
 * @property string $idle_olah1_olah2
 * @property string $idle_olah2_qcbulk
 * @property string $idle_timbang_olah_jam
 * @property string $idle_olahpremix_olah1_jam
 * @property string $idle_olah1_olah2_jam
 * @property string $idle_olah2_qcbulk_jam
 * @property string $timbang_max
 * @property string $olah_pertama
 * @property string $olah_premix_min
 * @property string $olah_premix_max
 * @property string $olah_1_min
 * @property string $olah_1_max
 * @property string $olah_2_min
 * @property string $olah_2_max
 * @property string $qcbulk_min
 * @property integer $id
 */
class DashboardIdletimePengolahan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dashboard_idletime_pengolahan';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'snfg', 'snfg_komponen', 'nama_fg', 'sediaan', 'idle_timbang_olah', 'idle_olahpremix_olah1', 'idle_olah1_olah2', 'idle_olah2_qcbulk'], 'string'],
            [['upload_date', 'timbang_max', 'olah_pertama', 'olah_premix_min', 'olah_premix_max', 'olah_1_min', 'olah_1_max', 'olah_2_min', 'olah_2_max', 'qcbulk_min'], 'safe'],
            [['week', 'id'], 'integer'],
            [['idle_timbang_olah_jam', 'idle_olahpremix_olah1_jam', 'idle_olah1_olah2_jam', 'idle_olah2_qcbulk_jam'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nomo' => 'Nomo',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'nama_fg' => 'Nama Fg',
            'sediaan' => 'Sediaan',
            'upload_date' => 'Upload Date',
            'week' => 'Week',
            'idle_timbang_olah' => 'Idle Timbang Olah',
            'idle_olahpremix_olah1' => 'Idle Olahpremix Olah1',
            'idle_olah1_olah2' => 'Idle Olah1 Olah2',
            'idle_olah2_qcbulk' => 'Idle Olah2 Qcbulk',
            'idle_timbang_olah_jam' => 'Idle Timbang Olah Jam',
            'idle_olahpremix_olah1_jam' => 'Idle Olahpremix Olah1 Jam',
            'idle_olah1_olah2_jam' => 'Idle Olah1 Olah2 Jam',
            'idle_olah2_qcbulk_jam' => 'Idle Olah2 Qcbulk Jam',
            'timbang_max' => 'Timbang Max',
            'olah_pertama' => 'Olah Pertama',
            'olah_premix_min' => 'Olah Premix Min',
            'olah_premix_max' => 'Olah Premix Max',
            'olah_1_min' => 'Olah 1 Min',
            'olah_1_max' => 'Olah 1 Max',
            'olah_2_min' => 'Olah 2 Min',
            'olah_2_max' => 'Olah 2 Max',
            'qcbulk_min' => 'Qcbulk Min',
            'id' => 'ID',
        ];
    }
}
