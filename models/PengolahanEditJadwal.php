<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pengolahan_edit_jadwal".
 *
 * @property integer $id
 * @property string $nomo
 * @property string $snfg_komponen
 * @property string $lanjutan
 * @property string $start
 * @property string $stop
 * @property string $jenis_olah
 * @property string $nama_line
 * @property string $nama_operator
 * @property string $plan_start_olah
 * @property string $plan_end_olah
 * @property integer $shift_plan_start_olah
 * @property integer $shift_plan_end_olah
 * @property integer $start_id
 */
class PengolahanEditJadwal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function primaryKey()
    {
        return ['id'];
    }

    public static function tableName()
    {
        return 'pengolahan_edit_jadwal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'shift_plan_start_olah', 'shift_plan_end_olah', 'start_id'], 'integer'],
            [['nomo', 'snfg_komponen', 'jenis_olah', 'nama_line', 'nama_operator'], 'string'],
            [['lanjutan'], 'number'],
            [['start', 'stop', 'plan_start_olah', 'plan_end_olah'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomo' => 'Nomo',
            'snfg_komponen' => 'Snfg Komponen',
            'lanjutan' => 'Lanjutan',
            'start' => 'Start',
            'stop' => 'Stop',
            'jenis_olah' => 'Jenis Olah',
            'nama_line' => 'Nama Line',
            'nama_operator' => 'Nama Operator',
            'plan_start_olah' => 'Plan Start Olah',
            'plan_end_olah' => 'Plan End Olah',
            'shift_plan_start_olah' => 'Shift Plan Start Olah',
            'shift_plan_end_olah' => 'Shift Plan End Olah',
            'start_id' => 'Start ID',
        ];
    }
}
