<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dim_kendala".
 *
 * @property string $sediaan
 * @property string $penimbangan
 * @property string $pengolahan
 * @property string $kemas
 */
class DimKendala extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dim_kendala';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sediaan', 'penimbangan', 'pengolahan', 'kemas'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sediaan' => 'Sediaan',
            'penimbangan' => 'Penimbangan',
            'pengolahan' => 'Pengolahan',
            'kemas' => 'Kemas',
        ];
    }
}
