<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PengolahanItem;

/**
 * PengolahanItemSearch represents the model behind the search form about `app\models\PengolahanItem`.
 */
class PengolahanItemSearch extends PengolahanItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pengolahan_id'], 'integer'],
            [['nobatch','snfg_komponen'], 'safe'],
            [['besar_batch_real'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengolahanItem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'besar_batch_real' => $this->besar_batch_real,
            'pengolahan_id' => $this->pengolahan_id,
        ]);

        $query->andFilterWhere(['like', 'nobatch', $this->nobatch]);
        $query->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen]);

        return $dataProvider;
    }
}
