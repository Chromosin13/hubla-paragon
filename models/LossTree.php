<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "loss_tree".
 *
 * @property integer $id
 * @property string $snfg
 * @property integer $jumlah
 * @property integer $qty_release
 * @property integer $qty_not_hit
 * @property string $remark
 * @property string $level_1
 * @property string $level_2
 * @property string $level_3
 * @property string $corrective_action
 * @property string $preventive_action
 * @property string $pic
 * @property string $due
 */
class LossTree extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loss_tree';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'remark', 'level_1', 'level_2', 'level_3', 'corrective_action', 'preventive_action', 'pic'], 'string'],
            [['jumlah', 'qty_release', 'qty_not_hit'], 'integer'],
            [['remark', 'level_1', 'level_2','level_3','corrective_action','preventive_action','due','pic'], 'required'],
            [['due'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'jumlah' => 'Jumlah',
            'qty_release' => 'Qty Release',
            'qty_not_hit' => 'Qty Not Hit',
            'remark' => 'Remark',
            'level_1' => 'Level 1',
            'level_2' => 'Level 2',
            'level_3' => 'Level 3',
            'corrective_action' => 'Corrective Action',
            'preventive_action' => 'Preventive Action',
            'pic' => 'Pic',
            'due' => 'Due',
        ];
    }
}
