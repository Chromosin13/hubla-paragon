<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "overall_leadtime".
 *
 * @property integer $id
 * @property string $nomo
 * @property string $snfg
 * @property string $snfg_komponen
 * @property string $nama_fg
 * @property string $timbang_jadwal_baru
 * @property string $timbang_rework
 * @property string $olah_olah_premix
 * @property string $olah_olah_1
 * @property string $olah_olah_2
 * @property string $olah_adjust
 * @property string $olah_rework
 * @property string $qcbulk_jadwal_baru
 * @property string $qcbulk_rework
 * @property string $kemas1_press
 * @property string $kemas1_filling
 * @property string $qcfg_jadwal_baru
 * @property string $qcfg_rework
 * @property string $qcfg_adjust
 * @property string $qcfg_pending
 * @property string $qcfg_lanjutan
 * @property string $kemas2_packing
 * @property string $kemas2_press_filling_packing
 * @property string $kemas2_filling_packing_inline
 * @property string $kemas2_flame_packing_inline
 */
class OverallLeadtime extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'overall_leadtime';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'snfg', 'snfg_komponen', 'nama_fg'], 'string'],
            [['timbang_jadwal_baru', 'timbang_rework', 'olah_olah_premix', 'olah_olah_1', 'olah_olah_2', 'olah_adjust', 'olah_rework', 'qcbulk_jadwal_baru', 'qcbulk_rework', 'kemas1_press', 'kemas1_filling', 'qcfg_jadwal_baru', 'qcfg_rework', 'qcfg_adjust', 'qcfg_pending', 'qcfg_lanjutan', 'kemas2_packing', 'kemas2_press_filling_packing', 'kemas2_filling_packing_inline', 'kemas2_flame_packing_inline'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomo' => 'Nomo',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'nama_fg' => 'Nama Fg',
            'timbang_jadwal_baru' => 'Timbang Jadwal Baru',
            'timbang_rework' => 'Timbang Rework',
            'olah_olah_premix' => 'Olah Olah Premix',
            'olah_olah_1' => 'Olah Olah 1',
            'olah_olah_2' => 'Olah Olah 2',
            'olah_adjust' => 'Olah Adjust',
            'olah_rework' => 'Olah Rework',
            'qcbulk_jadwal_baru' => 'Qcbulk Jadwal Baru',
            'qcbulk_rework' => 'Qcbulk Rework',
            'kemas1_press' => 'Kemas1 Press',
            'kemas1_filling' => 'Kemas1 Filling',
            'qcfg_jadwal_baru' => 'Qcfg Jadwal Baru',
            'qcfg_rework' => 'Qcfg Rework',
            'qcfg_adjust' => 'Qcfg Adjust',
            'qcfg_pending' => 'Qcfg Pending',
            'qcfg_lanjutan' => 'Qcfg Lanjutan',
            'kemas2_packing' => 'Kemas2 Packing',
            'kemas2_press_filling_packing' => 'Kemas2 Press Filling Packing',
            'kemas2_filling_packing_inline' => 'Kemas2 Filling Packing Inline',
            'kemas2_flame_packing_inline' => 'Kemas2 Flame Packing Inline',
        ];
    }
}
