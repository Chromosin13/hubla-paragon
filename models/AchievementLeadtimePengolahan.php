<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "achievement_leadtime_pengolahan".
 *
 * @property integer $id
 * @property string $nama_line
 * @property string $nomo
 * @property string $koitem_bulk
 * @property string $nama_bulk
 * @property string $nama_fg
 * @property string $jenis_olah
 * @property string $real_leadtime
 * @property string $standard_leadtime
 * @property string $nama_operator
 * @property string $achievement
 * @property string $tanggal_stop
 * @property string $plan_start_olah
 * @property string $plan_end_olah
 */
class AchievementLeadtimePengolahan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function primaryKey()
    {
        return ['id'];
    }
    
    public static function tableName()
    {
        return 'achievement_leadtime_pengolahan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['id'], 'integer'],
            [['nama_line', 'nomo', 'koitem_bulk', 'nama_bulk', 'nama_fg', 'jenis_olah', 'nama_operator'], 'string'],
            [['real_leadtime', 'standard_leadtime'], 'number'],
            [['id','tanggal_stop', 'plan_start_olah', 'plan_end_olah','achievement'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_line' => 'Nama Line',
            'nomo' => 'Nomo',
            'koitem_bulk' => 'Koitem Bulk',
            'nama_bulk' => 'Nama Bulk',
            'nama_fg' => 'Nama Fg',
            'jenis_olah' => 'Jenis Olah',
            'real_leadtime' => 'Real Leadtime',
            'standard_leadtime' => 'Standard Leadtime',
            'nama_operator' => 'Nama Operator',
            'achievement' => 'Achievement',
            'tanggal_stop' => 'Tanggal Stop',
            'plan_start_olah' => 'Plan Start Olah',
            'plan_end_olah' => 'Plan End Olah',
        ];
    }
}
