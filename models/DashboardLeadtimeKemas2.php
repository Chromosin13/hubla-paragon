<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dashboard_leadtime_kemas2".
 *
 * @property string $nama_fg
 * @property string $snfg
 * @property string $posisi
 * @property string $start
 * @property string $stop
 * @property string $sum_leadtime_raw
 * @property string $sum_leadtime_bruto
 * @property string $jenis_proses
 * @property string $sediaan
 * @property string $sum_downtime
 * @property string $sum_adjust
 * @property string $sum_leadtime_net
 * @property string $keterangan_downtime
 * @property string $keterangan_adjust
 * @property string $nomo
 * @property string $snfg_komponen
 * @property string $upload_date
 * @property integer $week
 * @property integer $id
 */
class DashboardLeadtimeKemas2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dashboard_leadtime_kemas2';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_fg', 'snfg', 'posisi', 'nama_line', 'sum_leadtime_raw', 'sum_leadtime_bruto', 'jenis_proses', 'sediaan', 'sum_downtime', 'sum_adjust', 'sum_leadtime_net', 'keterangan_downtime', 'keterangan_adjust', 'nomo', 'snfg_komponen'], 'string'],
            [['start', 'stop', 'upload_date'], 'safe'],
            [['week', 'id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nama_fg' => 'Nama Fg',
            'snfg' => 'Snfg',
            'posisi' => 'Posisi',
            'nama_line' => 'Nama Line',
            'start' => 'Start',
            'stop' => 'Stop',
            'sum_leadtime_raw' => 'Sum Leadtime Raw',
            'sum_leadtime_bruto' => 'Sum Leadtime Bruto',
            'jenis_proses' => 'Jenis Proses',
            'sediaan' => 'Sediaan',
            'sum_downtime' => 'Sum Downtime',
            'sum_adjust' => 'Sum Adjust',
            'sum_leadtime_net' => 'Sum Leadtime Net',
            'keterangan_downtime' => 'Keterangan Downtime',
            'keterangan_adjust' => 'Keterangan Adjust',
            'nomo' => 'Nomo',
            'snfg_komponen' => 'Snfg Komponen',
            'upload_date' => 'Upload Date',
            'week' => 'Week',
            'id' => 'ID',
        ];
    }
}
