<?php

namespace app\models;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

use Yii;

/**
 * This is the model class for table "pengecekan_umum".
 *
 * @property string $nama_produk
 * @property string $batch_produk
 * @property string $kode_sl
 * @property integer $no_notifikasi
 * @property integer $barcode_produk
 * @property string $exp_date
 * @property integer $no_mo
 * @property string $line_kemas
 * @property string $due_date
 * @property integer $jumlah_mpq
 * @property integer $total_aql
 * @property integer $jumlah_palet
 * @property integer $aql_per_palet
 * @property integer $aql_per_siklus
 * @property string $jam_mulai
 * @property string $jam_selesai
 * @property string $visual_performance_bulk
 * @property string $fisik_packaging
 * @property string $fungsional_packaging
 * @property string $kesesuaian_warna_packaging
 * @property string $kesesuaian_identitas_packaging
 * @property string $performance_segel
 * @property string $kesesuaian_batch_primer
 * @property string $kesesuaian_exp_primer
 * @property string $kesesuaian_batch_sekunder
 * @property string $kesesuaian_exp_sekunder
 * @property string $kesesuaian_batch_karbox
 * @property string $kesesuaian_exp_karbox
 * @property string $kesesuaian_notifikasi_produk
 * @property string $kesesuaian_barcode_produk
 * @property string $kualitas_barcode_karbox
 * @property string $kelengkapan_komponen
 * @property string $kesesuaian_kelengkapan_komponen
 * @property string $netto
 * @property string $jumlah_per_karbox
 * @property string $kesesuaian_warna_lakban
 * @property integer $jumlah_sampel_trial
 * @property integer $sampel_retain
 * @property integer $sampel_mikro
 * @property string $inspector_fg
 * @property string $investigasi_ketidaksesuaian
 * @property string $penanganan_ketidaksesuaian
 * @property string $approval_ketidaksesuaian
 * @property string $status_produk
 * @property string $label_bersih_bulk
 * @property string $label_bersih_mesin
 * @property string $plant
 * @property string $keterangan
 * @property string $nama_produk_sebelumnya
 * @property string $batch_produk_sebelumnya
 * @property string $air_bilasan_akhir
 * @property string $verifikasi_trial_filling
 * @property integer $id
 * @property string $serial_number
 */
class PengecekanUmum extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengecekan_umum';
    }

    public $sediaan;

    public $sampel_retain_yang_sudah_diambil;

    public $sampel_mikro_yang_sudah_diambil;

    public $aql_sisa;

    public $sisa_aql_palet;

    // public $sampel_retain;

    // public $sampel_mikro;

    public $sisa_aql_yang_harus_dicek;

 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'kesesuaian_batch_primer', 'kesesuaian_batch_sekunder', 'kesesuaian_batch_karbox', 'kesesuaian_barcode_produk', 'kualitas_barcode_karbox', 'netto', 'jumlah_per_karbox', 'kesesuaian_warna_lakban', 'inspector_fg', 'investigasi_ketidaksesuaian', 'penanganan_ketidaksesuaian', 'metode_rework', 'status_produk', 'sediaan', 'checkweigher','inline_weigher','batch_innerbox','jumlah_per_palet','snfg','komponen_penyusun_1','komponen_penyusun_2','komponen_penyusun_3','komponen_penyusun_4','komponen_penyusun_5','komponen_penyusun_6','komponen_penyusun_7','komponen_penyusun_8','tipe_checkweigher','tipe_inline_weigher','indicator_lamp','temuan_penyusun_1','temuan_penyusun_2','temuan_penyusun_3','temuan_penyusun_4','temuan_penyusun_5','temuan_penyusun_6','temuan_penyusun_7','temuan_penyusun_8','status_siklus','visual_performance_bulk', 'fisik_packaging', 'fungsional_packaging', 'kesesuaian_warna_packaging', 'kesesuaian_identitas_packaging', 'performance_segel','uji_kekencangan_cap','sistem_cek','proses_kerja','sistem_3s','proses_produksi','tekanan_udara','rejection_timing','hasil_uji_bulk','hasil_uji_dusat','hasil_uji_komponen','metode_tare','visual_performance_bulk_2','visual_performance_bulk_3','visual_performance_bulk_4','visual_performance_bulk_5','visual_performance_bulk_6','visual_performance_bulk_7','visual_performance_bulk_8','catatan_pengecekan','indicator_lamp_inline','extra_proses','pengerjaan_extra_proses','tipe_extra_proses','detail_penanganan','status_hasil_cek_qa','kesesuaian_na_produk','alasan_checkweigher','alasan_inline_weigher'], 'string'],
            // [['nama_produk','keterangan','batch_produk','serial_number','no_mo','plant','line_kemas','jumlah_mpq','total_aql','kode_sl','no_notifikasi','nama_produk_sebelumnya','batch_produk_sebelumnya'],'required'],
            // [['jam_mulai','jam_selesai'],'required'],

            [[  'aql_per_palet', 'aql_per_siklus', 'jumlah_sampel_trial', 'sampel_retain', 'sampel_mikro','siklus','no_palet','id_identitas_pengecekan_fg','jumlah_komponen_penyusun','koli_qa','jumlah_komponen_bulk'], 'integer'],

            [[ 'kesesuaian_exp_primer', 'kesesuaian_exp_sekunder', 'kesesuaian_exp_karbox','jam_mulai', 'jam_selesai','exp_innerbox'], 'safe'],

            // [[ 'kesesuaian_batch_primer', 'kesesuaian_batch_sekunder', 'kesesuaian_batch_karbox', 'kesesuaian_barcode_produk', 'kualitas_barcode_karbox', 'netto', 'jumlah_per_karbox', 'kesesuaian_warna_lakban', 'inspector_fg', 'investigasi_ketidaksesuaian', 'penanganan_ketidaksesuaian', 'metode_rework', 'status_produk', 'sediaan', 'kesesuaian_exp_primer', 'kesesuaian_exp_sekunder', 'kesesuaian_exp_karbox','checkweigher','inline_weigher','batch_innerbox','exp_innerbox','jumlah_per_palet','snfg','tipe_checkweigher','tipe_inline_weigher','indicator_lamp'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

            'aql_per_palet' => '1.20 AQL per Pallete / Trolley (pcs)',
            'aql_per_siklus' => '1.21 AQL per Siklus (pcs)',
            'jam_mulai' => '1.13 Mulai',
            'jam_selesai' => 'Selesai',
            'visual_performance_bulk' => '5.2.1 Visual Performance Bulk (pcs)',
            'fisik_packaging' => '5.6 Kondisi Fisik Packaging (pcs)',
            'fungsional_packaging' => '5.4 Performa Fungsional Packaging (pcs)',
            'kesesuaian_warna_packaging' => '5.5 Kesesuaian Warna Packaging (pcs)',
            'kesesuaian_identitas_packaging' => '5.3 Kesesuaian Identitas Packaging (pcs)',
            'performance_segel' => '5.7 Performa Segel / Shrink Wrap (pcs)',
            'kesesuaian_batch_primer' => '7.1 No. Batch di Pack. Primer',
            'kesesuaian_exp_primer' => '7.2 Exp Date di Pack. Primer',
            'kesesuaian_batch_sekunder' => '7.3 No. Batch di Pack. Sekunder',
            'kesesuaian_exp_sekunder' => '7.4 Exp Date di Pack. Sekunder',
            'kesesuaian_batch_karbox' => '7.7 No. Batch Karbox',
            'kesesuaian_exp_karbox' => '7.8 Exp Date Karbox',
            'kesesuaian_barcode_produk' => '7.9 Kesesuaian Nomor Barcode Produk',
            'kualitas_barcode_karbox' => '7.11 Kualitas Print Label Barcode Karbox',
            'netto' => '9.1 Netto Produk (gram)',
            'jumlah_per_karbox' => '9.2 Jumlah FG per Karbox (pcs)',
            'kesesuaian_warna_lakban' => '9.5 Warna Lakban / Label Barcode di Innerbox dan Karbox ',
            'jumlah_sampel_trial' => '9.4 Jumlah Sample Trial Destruktif (pcs)',
            'sampel_retain' => '9.6 Sampel Retain (pcs)',
            'sampel_mikro' => '9.7 Sampel Mikro (pcs)',
            'inspector_fg' => '10.1 Inspektor QFG',
            'investigasi_ketidaksesuaian' => '10.5 Hasil Investigasi Ketidaksesuaian',
            'penanganan_ketidaksesuaian' => '10.6 Penanganan Ketidaksesuaian FG',
            'detail_penanganan' => '10.7 Detail Penanganan Ketidaksesuaian',
            'metode_rework' => '10.8 Penjelasan Metode Rework',
            'status_produk' => '10.4 Status Produk',
            'id' => 'ID',
            'siklus' => '1.23 Siklus',
            'no_palet' => '1.22 No Palet',
            'id_identitas_pengecekan_fg' => 'Id Identitas Fg',
            'checkweigher' => '3.1 Penggunaan Checkweigher',
            'inline_weigher' => '3.9 Penggunaan Inline Weigher',
            'batch_innerbox' => '7.5 No. Batch Innerbox',
            'exp_innerbox' => '7.6 Exp Date Innerbox',
            'jumlah_per_palet' => '9.3 Jumlah FG per Palet (pcs)',
            'snfg'=>'SNFG',
            'jumlah_komponen_penyusun'=> '8.1 Jumlah Komponen Penyusun',
            'komponen_penyusun_1'=>'8.2.1 Komponen Penyusun 1',
            'komponen_penyusun_2'=>'8.2.2 Komponen Penyusun 2',
            'komponen_penyusun_3'=>'8.2.3 Komponen Penyusun 3',
            'komponen_penyusun_4'=>'8.2.4 Komponen Penyusun 4',
            'komponen_penyusun_5'=>'8.2.5 Komponen Penyusun 5',
            'komponen_penyusun_6'=>'8.2.6 Komponen Penyusun 6',
            'komponen_penyusun_7'=>'8.2.7 Komponen Penyusun 7',
            'komponen_penyusun_8'=>'8.2.8 Komponen Penyusun 8',
            'uji_kekencangan_cap'=>'5.8 Uji Manual Kekencangan Cap (pcs)',
            'tipe_checkweigher' => '3.2 Tipe Checkweigher',
            'tipe_inline_weigher' => '3.10 Tipe Inline Weigher',
            'indicator_lamp' => '3.3 Kondisi Indicator Lamp Checkweigher',
            'temuan_penyusun_1' => '8.2.1.1 Jumlah Temuan Komponen Penyusun 1',
            'temuan_penyusun_2' => '8.2.2.1 Jumlah Temuan Komponen Penyusun 2',
            'temuan_penyusun_3' => '8.2.3.1 Jumlah Temuan Komponen Penyusun 3',
            'temuan_penyusun_4' => '8.2.4.1 Jumlah Temuan Komponen Penyusun 4',
            'temuan_penyusun_5' => '8.2.5.1 Jumlah Temuan Komponen Penyusun 5',
            'temuan_penyusun_6' => '8.2.6.1 Jumlah Temuan Komponen Penyusun 6',
            'temuan_penyusun_7' => '8.2.7.1 Jumlah Temuan Komponen Penyusun 7',
            'temuan_penyusun_8' => '8.2.8.1 Jumlah Temuan Komponen Penyusun 8',
            'status_siklus' => '10.3 Status Siklus',
            'kesesuaian_na_produk' => '7.10 Kesesuaian NA Produk',
            'sistem_cek' => '1.17 Sistem Cek FG',
            'proses_kerja' => '2.1 Apakah proses kerja operator sesuai standard ?',
            'sistem_3s' => '2.2 Apakah operator produksi melaksanakan sistem 3S ?',
            'proses_produksi' => '2.3 Apakah proses produksi berjalan secara one-piece flow atau "menggunung" ?',
            'tekanan_udara' => '3.4 Tekanan Udara Checkweigher',
            'rejection_timing' => '3.5 Rejection timing FG oleh Checkweigher',
            'hasil_uji_bulk' => '3.6 Hasil Uji FG Bulk Kosong',
            'hasil_uji_dusat' => '3.7 Hasil Uji FG Dusat Kosong',
            'hasil_uji_komponen' => '3.8 Hasil Uji FG komponen Tidak Lengkap',
            'metode_tare' => '3.12 Metode Tare Inline Weigher',
            'visual_performance_bulk_2' => '5.2.2 Visual Performance Bulk 2 (pcs)',
            'visual_performance_bulk_3' => '5.2.3 Visual Performance Bulk 3 (pcs)',
            'visual_performance_bulk_4' => '5.2.4 Visual Performance Bulk 4 (pcs)',
            'visual_performance_bulk_5' => '5.2.5 Visual Performance Bulk 5 (pcs)',
            'visual_performance_bulk_6' => '5.2.6 Visual Performance Bulk 6 (pcs)',
            'visual_performance_bulk_7' => '5.2.7 Visual Performance Bulk 7 (pcs)',
            'visual_performance_bulk_8' => '5.2.8 Visual Performance Bulk 8 (pcs)',
            'catatan_pengecekan' => '10.2 Catatan Pengecekan',
            'indicator_lamp_inline' => '3.11 Kondisi Indicator Lamp Inline Weigher',
            'koli_qa' => '1.24 Koli QA ke -',
            'jumlah_komponen_bulk' => '5.1 Jumlah Komponen Bulk dalam FG',
            'extra_proses' => '11.1 Kebutuhan EP',
            'pengerjaan_extra_proses' => '11.2 Progres EP',
            'tipe_extra_proses' => '11.3 Tipe Ekstra Proses',
            'status_hasil_cek_qa' => '10.3 Status Pallet dari Hasil Cek Koli QA',
            'alasan_checkweigher' => '3.1.1 Alasan Checkweigher tidak digunakan & PIC line kemas (DMS)',
            'alasan_inline_weigher' => '3.9.1 Alasan Inline Weigher tidak digunakan & PIC line kemas (DMS)',

        ];
    }
}
