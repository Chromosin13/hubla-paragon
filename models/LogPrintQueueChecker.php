<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_print_queue_checker".
 *
 * @property integer $id
 * @property string $timestamp
 * @property string $nomo
 * @property string $operator
 * @property string $is_print
 * @property integer $siklus
 * @property string $timestamp_checked
 * @property string $nama_bulk
 */
class LogPrintQueueChecker extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_print_queue_checker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['timestamp', 'timestamp_checked'], 'safe'],
            [['nomo', 'operator', 'is_print', 'nama_bulk'], 'string'],
            [['siklus'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'timestamp' => 'Timestamp',
            'nomo' => 'Nomo',
            'operator' => 'Operator',
            'is_print' => 'Is Print',
            'siklus' => 'Siklus',
            'timestamp_checked' => 'Timestamp Checked',
            'nama_bulk' => 'Nama Bulk',
        ];
    }
}
