<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "not_done_qc_fg".
 *
 * @property string $status_palet
 * @property integer $jumlah_palet_diterima_ndc
 * @property integer $jumlah_palet
 * @property string $last_update
 * @property string $due
 * @property string $hari_ini
 * @property string $snfg
 * @property string $nama_fg
 * @property string $nama_bulk
 * @property integer $margin
 */
class NotDoneQcFg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'not_done_qc_fg';
    }

    public static function primaryKey()
    {
        return ['snfg'];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_palet', 'snfg', 'nama_fg', 'nama_bulk'], 'string'],
            [['jumlah_palet_diterima_ndc', 'jumlah_palet', 'margin'], 'integer'],
            [['last_update', 'due', 'hari_ini'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status_palet' => 'Status Palet',
            'jumlah_palet_diterima_ndc' => 'Jumlah Palet Diterima Ndc',
            'jumlah_palet' => 'Jumlah Palet',
            'last_update' => 'Last Update',
            'due' => 'Due',
            'hari_ini' => 'Hari Ini',
            'snfg' => 'Snfg',
            'nama_fg' => 'Nama Fg',
            'nama_bulk' => 'Nama Bulk',
            'margin' => 'Margin',
        ];
    }
}
