<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flow_cuci_mesin_kemas".
 *
 * @property string $nama_line
 * @property string $nama_fg
 * @property string $nobatch
 * @property string $snfg
 * @property string $operator
 * @property string $jenis_cuci
 * @property string $verifikator
 * @property string $gombalan
 * @property integer $sarung_tangan
 * @property string $detergen
 * @property string $parafin
 * @property string $datetime_start
 * @property string $datetime_stop
 * @property integer $id
 */
class FlowCuciMesinKemas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flow_cuci_mesin_kemas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_line', 'nama_fg', 'nobatch', 'snfg', 'operator', 'jenis_cuci', 'verifikator'], 'string'],
            [['gombalan', 'detergen', 'parafin','alkohol'], 'number'],
            [['sarung_tangan','pelarut_id'], 'integer'],
            [['datetime_start', 'datetime_stop'], 'safe'],
            [['nobatch','nama_line','jenis_cuci','operator'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nama_line' => 'Nama Line',
            'nama_fg' => 'Nama FG',
            'nobatch' => 'Nobatch',
            'snfg' => 'Snfg',
            'operator' => 'Operator',
            'jenis_cuci' => 'Jenis Cuci',
            'verifikator' => 'Verifikator',
            'gombalan' => 'Gombalan',
            'sarung_tangan' => 'Sarung Tangan',
            'detergen' => 'Detergen',
            'parafin' => 'Parafin',
            'datetime_start' => 'Datetime Start',
            'datetime_stop' => 'Datetime Stop',
            'id' => 'ID',
            'alkohol' => 'Alkohol',
            'pelarut_id' => 'Pelarut ID',
        ];
    }
}
