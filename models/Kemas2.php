<?php

namespace app\models;

use Yii;
use DateTime;
/**
 * This is the model class for table "kemas_2".
 *
 * @property string $snfg
 * @property string $jumlah_plan
 * @property string $jumlah_realisasi
 * @property string $nama_line
 * @property string $nama_operator
 * @property string $waktu
 * @property string $state
 * @property string $posisi
 * @property integer $id
 */
class Kemas2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kemas_2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'nama_line', 'nama_operator', 'state','jenis_kemas','nobatch','snfg_komponen','edit_nama'], 'string'],
            [['jumlah_plan', 'jumlah_realisasi','lanjutan','jumlah_operator','lanjutan_ist','is_done','batch_split','lanjutan_split_batch','last_status','palet_ke','palet_flag','counter'], 'number'],
            [['waktu','timestamp','edit_time'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'jumlah_plan' => 'Jumlah Plan',
            'jumlah_realisasi' => 'Jumlah Realisasi',
            'nama_line' => 'Nama Line',
            'nama_operator' => 'Nama Operator',
            'waktu' => 'Waktu',
            'state' => 'State',
            'jenis_kemas' => 'Jenis Kemas',
            'lanjutan' => 'Lanjutan',
            'jumlah_operator' => 'Jumlah Operator',
            'timestamp' => 'Timestamp',
            'nobatch' =>'No Batch',
            'lanjutan_ist'=>'Lanjutan Istirahat',
            'is_done'=>'Is Done',
            'batch_split' =>'Batch Split',
            'snfg_komponen' => 'SNFG Komponen',
            'lanjutan_split_batch' => 'Lanjutan Split Batch',
            'last_status' => 'Last Status',
            'palet_ke' => 'Palet Ke',
            'palet_flag' => 'Palet Flag',
            'counter' => 'Counter',

        ];
    }

    public function getKendalas()
    {
        return $this->hasMany(Kendala::className(), ['kemas_2_id' => 'id']);
    }

    public function getFormatwaktu()
    {
        return DateTime::createFromFormat('Y-m-d H:i:s.u', $this->waktu)->format('Y-m-d H:i:s');
    }

}
