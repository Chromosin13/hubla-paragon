<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AchievementLinePenimbangan;

/**
 * AchievementLinePenimbanganSearch represents the model behind the search form about `app\models\AchievementLinePenimbangan`.
 */
class AchievementLinePenimbanganSearch extends AchievementLinePenimbangan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row_number'], 'integer'],
            [['tanggal', 'nama_line', 'snfg', 'snfg_komponen', 'nama_item', 'status_leadtime', 'plan_start_timbang', 'status_plan', 'posisi'], 'safe'],
            [['leadtime', 'leadtime_std'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AchievementLinePenimbangan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'row_number' => $this->row_number,
            'tanggal' => $this->tanggal,
            'leadtime' => $this->leadtime,
            'leadtime_std' => $this->leadtime_std,
            'plan_start_timbang' => $this->plan_start_timbang,
        ]);

        $query->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'nama_item', $this->nama_item])
            ->andFilterWhere(['like', 'status_leadtime', $this->status_leadtime])
            ->andFilterWhere(['like', 'status_plan', $this->status_plan])
            ->andFilterWhere(['like', 'posisi', $this->posisi]);

        return $dataProvider;
    }
}
