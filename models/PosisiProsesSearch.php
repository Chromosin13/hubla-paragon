<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PosisiProses;

/**
 * PosisiProsesSearch represents the model behind the search form about `app\models\PosisiProses`.
 */
class PosisiProsesSearch extends PosisiProses
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo','snfg','snfg_komponen', 'posisi','nama_fg', 'status', 'start', 'due','time','timestamp','lanjutan_split_batch','ontime','delta'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PosisiProses::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        // $query->andFilterWhere([
        //     'DATE(start)' => $this->start,
        //     'DATE(due)' => $this->due,
        //     'DATE(timestamp)' => $this->timestamp,
        // ]);

        $query->andFilterCompare('DATE(start)',$this->start,'=');
        $query->andFilterCompare('DATE(due)',$this->due,'=');
        $query->andFilterCompare('DATE(timestamp)',$this->timestamp,'=');
        $query->andFilterCompare('DATE(time)',$this->timestamp,'=');
        $query->andFilterCompare('lanjutan_split_batch',$this->lanjutan_split_batch
            ,'=');





        $query
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'posisi', $this->posisi])
            ->andFilterWhere(['like', 'ontime', $this->ontime])
            ->andFilterWhere(['like', 'status', $this->status])
            // ->andFilterWhere(['like', 'lanjutan_split_batch', $this->lanjutan_split_batch])
            ->andFilterWhere(['like', 'delta', $this->delta]);

        return $dataProvider;
    }
}
