<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\QcbulkEditable;

/**
 * QcbulkEditableSearch represents the model behind the search form about `app\models\QcbulkEditable`.
 */
class QcbulkEditableSearch extends QcbulkEditable
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nomo', 'snfg', 'snfg_komponen', 'ph', 'viskositas', 'berat_jenis', 'kadar', 'warna', 'bau', 'performance', 'bentuk', 'mikro', 'kejernihan', 'status', 'waktu', 'state', 'posisi', 'jenis_periksa', 'timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QcbulkEditable::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'waktu' => $this->waktu,
            'timestamp' => $this->timestamp,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'ph', $this->ph])
            ->andFilterWhere(['like', 'viskositas', $this->viskositas])
            ->andFilterWhere(['like', 'berat_jenis', $this->berat_jenis])
            ->andFilterWhere(['like', 'kadar', $this->kadar])
            ->andFilterWhere(['like', 'warna', $this->warna])
            ->andFilterWhere(['like', 'bau', $this->bau])
            ->andFilterWhere(['like', 'performance', $this->performance])
            ->andFilterWhere(['like', 'bentuk', $this->bentuk])
            ->andFilterWhere(['like', 'mikro', $this->mikro])
            ->andFilterWhere(['like', 'kejernihan', $this->kejernihan])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'posisi', $this->posisi])
            ->andFilterWhere(['like', 'jenis_periksa', $this->jenis_periksa]);

        return $dataProvider;
    }
}
