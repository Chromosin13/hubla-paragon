<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "list_reviewer".
 *
 * @property integer $id
 * @property string $sediaan
 * @property string $reviewer
 */
class ListReviewer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'list_reviewer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sediaan', 'reviewer', 'username'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sediaan' => 'Sediaan',
            'reviewer' => 'Reviewer',
            'username' => 'Username'
        ];
    }
}
