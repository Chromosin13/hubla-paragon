<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posisi_proses_raw".
 *
 * @property string $snfg
 * @property string $snfg_komponen
 * @property string $posisi
 * @property string $start
 * @property string $due
 * @property string $state
 * @property string $timestamp
 * @property string $jenis_proses
 * @property string $lanjutan
 * @property string $is_done
 * @property string $ontime
 */
class PosisiProsesRaw extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posisi_proses_raw';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'snfg_komponen', 'posisi', 'state', 'jenis_proses', 'is_done', 'ontime'], 'string'],
            [['start', 'due', 'timestamp'], 'safe'],
            [['lanjutan'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'posisi' => 'Posisi',
            'start' => 'Start',
            'due' => 'Due',
            'state' => 'State',
            'timestamp' => 'Timestamp',
            'jenis_proses' => 'Jenis Proses',
            'lanjutan' => 'Lanjutan',
            'is_done' => 'Is Done',
            'ontime' => 'Ontime',
        ];
    }
}
