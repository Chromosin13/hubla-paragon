<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AchievementLinePengolahan1;

/**
 * AchievementLinePengolahan1Search represents the model behind the search form about `app\models\AchievementLinePengolahan1`.
 */
class AchievementLinePengolahan1Search extends AchievementLinePengolahan1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row_number'], 'integer'],
            [['tanggal_stop', 'tanggal_start', 'nama_line', 'sediaan', 'snfg', 'snfg_komponen', 'nama_fg', 'jenis_olah', 'status_leadtime', 'plan_start_olah', 'plan_end_olah', 'status_ontime_olah', 'status_ontime_kemas'], 'safe'],
            [['delta_hour_net', 'std_leadtime', 'is_done'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AchievementLinePengolahan1::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'row_number' => $this->row_number,
            'tanggal_stop' => $this->tanggal_stop,
            'tanggal_start' => $this->tanggal_start,
            'delta_hour_net' => $this->delta_hour_net,
            'std_leadtime' => $this->std_leadtime,
            'plan_start_olah' => $this->plan_start_olah,
            'plan_end_olah' => $this->plan_end_olah,
            'is_done' => $this->is_done,
        ]);

        $query->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'sediaan', $this->sediaan])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'jenis_olah', $this->jenis_olah])
            ->andFilterWhere(['like', 'status_leadtime', $this->status_leadtime])
            ->andFilterWhere(['like', 'status_ontime_olah', $this->status_ontime_olah])
            ->andFilterWhere(['like', 'status_ontime_kemas', $this->status_ontime_kemas]);

        return $dataProvider;
    }
}
