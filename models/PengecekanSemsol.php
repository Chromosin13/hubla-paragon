<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pengecekan_semsol".
 *
 * @property integer $id
 * @property string $uji_ayun
 * @property string $uji_ulir
 * @property string $uji_ketrok
 * @property string $uji_oles
 * @property string $uji_sweating
 * @property string $pengecekan_warna
 * @property integer $id_pengecekan_umum
 */
class PengecekanSemsol extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengecekan_semsol';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengecekan_umum'], 'integer'],
            [['uji_ayun', 'uji_ulir', 'uji_ketrok', 'uji_oles', 'uji_kepatahan','pengecekan_warna','uji_torsi_cap'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uji_ayun' => '6.1 Uji Ayun (pcs)',
            'uji_kepatahan' => '6.2 Uji Kepatahan Menggunakan Bandul (pcs)',
            'uji_ulir' => '6.3 Uji Ulir (pcs)',
            'uji_ketrok' => '6.4 Uji Ketrok (pcs)',
            'uji_oles' => '6.5 Uji Oles (pcs)',
            
            // 'uji_sweating' => 'Uji Sweating (pcs)',
            'pengecekan_warna' => '6.6 Pengecekan Warna (pcs)',
            'uji_torsi_cap' => '6.7 Uji Torsi Cap (pcs)',
            'id_pengecekan_umum' => 'Id Pengecekan Umum',
        ];
    }
}
