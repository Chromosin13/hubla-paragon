<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_proses_pra_kemas".
 *
 * @property string $jenis_proses_pra_kemas
 * @property integer $is_print
 * @property string $sediaan
 * @property integer $id
 */
class JenisProsesPraKemas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_proses_pra_kemas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_proses_pra_kemas', 'sediaan'], 'string'],
            [['is_print'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'jenis_proses_pra_kemas' => 'Jenis Proses Pra Kemas',
            'is_print' => 'Is Print',
            'sediaan' => 'Sediaan',
            'id' => 'ID',
        ];
    }
}
