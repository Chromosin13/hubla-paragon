<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_task_pengolahan".
 *
 * @property integer $id
 * @property string $kode_bb
 * @property string $nama_bb
 * @property string $qty
 * @property string $uom
 * @property string $kode_olah
 * @property string $status
 * @property string $pic
 * @property string $timestamp_awal
 * @property string $timestamp_checked
 * @property string $nomo
 * @property string $log
 * @property string $kode_internal
 */
class LogTaskPengolahan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_task_pengolahan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_bb', 'nama_bb', 'uom', 'kode_olah', 'status', 'pic', 'nomo', 'log', 'kode_internal','id_bb'], 'string'],
            [['siklus','serial_manual'], 'integer'],
            [['qty'], 'number'],
            [['timestamp_awal', 'timestamp_checked'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode_bb' => 'Kode Bb',
            'nama_bb' => 'Nama Bb',
            'qty' => 'Qty',
            'uom' => 'Uom',
            'kode_olah' => 'Kode Olah',
            'status' => 'Status',
            'pic' => 'Pic',
            'timestamp_awal' => 'Timestamp Awal',
            'timestamp_checked' => 'Timestamp Checked',
            'nomo' => 'Nomo',
            'log' => 'Log',
            'kode_internal' => 'Kode Internal',
        ];
    }
}
