<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_data_timbangan_rm".
 *
 * @property integer $id
 * @property string $sediaan
 * @property string $line
 * @property string $kode_timbangan
 * @property string $type
 */
class MasterDataTimbanganRm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_data_timbangan_rm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sediaan', 'line', 'kode_timbangan', 'type','uom'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sediaan' => 'Sediaan',
            'line' => 'Line',
            'kode_timbangan' => 'Kode Timbangan',
            'type' => 'Type',
            'uom' => 'Uom',
        ];
    }
}
