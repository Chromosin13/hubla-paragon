<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_approval_br".
 *
 * @property integer $id
 * @property string $nomo
 * @property string $creator
 * @property string $status_creator
 * @property string $timestamp_creator
 * @property string $reviewer
 * @property string $status_reviewer
 * @property string $timestamp_reviewer
 * @property string $approver
 * @property string $status_approver
 * @property string $timestamp_approver
 * @property string $status
 * @property string $kode_bulk
 * @property string $formula_reference
 * @property string $qty_batch
 * @property string $current_pic
 * @property string $current_status
 * @property string $reject_note_reviewer
 * @property string $reject_note_approver
 */
class LogApprovalBr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_approval_br';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'creator', 'status_creator', 'reviewer', 'status_reviewer', 'approver', 'status_approver', 'status', 'kode_bulk', 'formula_reference', 'current_pic', 'current_status', 'reject_note_reviewer', 'reject_note_approver','nama_bulk'], 'string'],
            [['timestamp_creator', 'timestamp_reviewer', 'timestamp_approver'], 'safe'],
            [['qty_batch'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomo' => 'Nomo',
            'creator' => 'Creator',
            'status_creator' => 'Status Creator',
            'timestamp_creator' => 'Timestamp Creator',
            'reviewer' => 'Reviewer',
            'status_reviewer' => 'Status Reviewer',
            'timestamp_reviewer' => 'Timestamp Reviewer',
            'approver' => 'Approver',
            'status_approver' => 'Status Approver',
            'timestamp_approver' => 'Timestamp Approver',
            'status' => 'Status',
            'kode_bulk' => 'Kode Bulk',
            'formula_reference' => 'Formula Reference',
            'qty_batch' => 'Qty Batch',
            'current_pic' => 'Current Pic',
            'current_status' => 'Current Status',
            'reject_note_reviewer' => 'Reject Note Reviewer',
            'reject_note_approver' => 'Reject Note Approver',
        ];
    }
}
