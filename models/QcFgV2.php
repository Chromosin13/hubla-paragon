<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "qc_fg_v2".
 *
 * @property integer $id
 * @property string $snfg
 * @property string $qty
 * @property string $qty_sample
 * @property string $timestamp
 */
class QcFgV2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qc_fg_v2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['snfg','status','inspektor_qcfg','inspektor_karantina','status_check','alokasi','nobatch','id_palet','odoo_code'], 'string'],
            [['qty', 'qty_sample','qty_karantina','is_done','qty_reject','nourut','jumlah_koli'], 'number'],
            [['qty', 'qty_sample','qty_reject','jumlah_koli','alokasi'], 'required'],
            [['timestamp','timestamp_kirim','timestamp_terima_karantina','timestamp_terima_ndc','timestamp_check','exp_date','timestamp_surjal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alokasi' => 'Alokasi FG',
            'snfg' => 'Snfg',
            'nobatch' => 'Nobatch',
            'qty' => 'Qty',
            'qty_sample' => 'Qty Sample',
            'timestamp' => 'Timestamp',
            'timestamp_kirim' => 'Timestamp Kirim',
            'timestamp_terima_karantina' => 'Timestamp Terima Karantina',
            'qty_karantina' => 'Quantity Karantina',
            'is_done' => 'Is Done',
            'qty_reject' => 'Qty Reject',
            'nourut' => 'Nomor Urut',
            'inspektor_qcfg' => 'Inspektor QC FG',
            'inspektor_karantina' => 'Inspektor Karantina',
            'status_check' => 'Status Pengecekan',
            'timestamp_check' => 'Timestamp Check',
            'jumlah_koli' => 'Jumlah Koli',
            'odoo_code' => 'Odoo Code',
            'id_palet' => 'ID Palet',

        ];
    }
}
