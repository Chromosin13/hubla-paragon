<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PlannedDownTime;

/**
 * PlannedDownTimeSearch represents the model behind the search form of `app\models\PlannedDownTime`.
 */
class PlannedDownTimeSearch extends PlannedDownTime
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'kemas_id', 'jenis'], 'integer'],
            [['keterangan', 'durasi'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PlannedDownTime::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'durasi' => $this->durasi,
            'kemas_id' => $this->kemas_id,
            'jenis' => $this->jenis,
        ]);

        $query->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
