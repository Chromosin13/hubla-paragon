<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "list_approver".
 *
 * @property integer $id
 * @property string $sediaan
 * @property string $approver
 * @property string $username
 */
class ListApprover extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'list_approver';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sediaan', 'approver', 'username'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sediaan' => 'Sediaan',
            'approver' => 'Approver',
            'username' => 'Username',
        ];
    }
}
