<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "surat_jalan".
 *
 * @property integer $id
 * @property string $nosj
 * @property string $timestamp
 *
 * @property SuratJalanRincian[] $suratJalanRincians
 */
class SuratJalan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'surat_jalan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_received','is_downloaded'], 'integer'],
            [['nosj','alokasi'], 'string'],
            [['timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nosj' => 'Nomor SJ',
            'is_received' => 'Diterima?',
            'is_downloaded' => 'Didownload?',
            'timestamp' => 'Tanggal Buat',
            'alokasi' => 'Alokasi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuratJalanRincians()
    {
        return $this->hasMany(SuratJalanRincian::className(), ['surat_jalan_id' => 'id']);
    }
}
