<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PosisiProsesHalf;

/**
 * PosisiProsesHalfSearch represents the model behind the search form of `app\models\PosisiProsesHalf`.
 */
class PosisiProsesHalfSearch extends PosisiProsesHalf
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'snfg', 'snfg_komponen', 'nama_fg', 'posisi', 'start', 'due', 'status', 'timestamp', 'ontime', 'time'], 'safe'],
            [['lanjutan_split_batch', 'delta'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PosisiProsesHalf::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'start' => $this->start,
            'due' => $this->due,
            'timestamp' => $this->timestamp,
            'lanjutan_split_batch' => $this->lanjutan_split_batch,
            'delta' => $this->delta,
            'time' => $this->time,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'posisi', $this->posisi])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'ontime', $this->ontime]);

        return $dataProvider;
    }
}
