<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "identitas_produk".
 *
 * @property string $id
 * @property string $nama_produk
 * @property string $batch_produk
 * @property string $kode_sl
 * @property string $no_notifikasi
 * @property string $barcode_produk
 * @property string $exp_date
 * @property string $no_mo
 * @property string $line_kemas
 * @property string $due_date
 */
class IdentitasProduk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'identitas_produk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_produk', 'batch_produk', 'kode_sl', 'no_notifikasi', 'barcode_produk', 'no_mo', 'line_kemas'], 'string'],
            [['exp_date', 'due_date'], 'safe'],
            [['id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_produk' => 'Nama Produk',
            'batch_produk' => 'Batch Produk',
            'kode_sl' => 'Kode Sl',
            'no_notifikasi' => 'No Notifikasi',
            'barcode_produk' => 'Barcode Produk',
            'exp_date' => 'Exp Date',
            'no_mo' => 'No Mo',
            'line_kemas' => 'Line Kemas',
            'due_date' => 'Due Date',
        ];
    }
}
