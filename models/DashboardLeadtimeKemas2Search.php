<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DashboardLeadtimeKemas2;
use kartik\daterange\DateRangeBehavior;
/**
 * DashboardLeadtimeKemas2Search represents the model behind the search form about `app\models\DashboardLeadtimeKemas2`.
 */
class DashboardLeadtimeKemas2Search extends DashboardLeadtimeKemas2
{
    /**
     * @inheritdoc
     */


    public $createTimeRange;
    public $createTimeStart;
    public $createTimeEnd;

    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'upload_date',
                'dateStartAttribute' => 'createTimeStart',
                'dateEndAttribute' => 'createTimeEnd',
            ]
        ];
    }

    public function rules()
    {
        return [
            [['nama_fg', 'snfg', 'posisi', 'nama_line', 'start', 'stop', 'sum_leadtime_raw', 'sum_leadtime_bruto', 'jenis_proses', 'sediaan', 'sum_downtime', 'sum_adjust', 'sum_leadtime_net', 'keterangan_downtime', 'keterangan_adjust', 'nomo', 'snfg_komponen', 'upload_date'], 'safe'],
            [['week', 'id'], 'integer'],
            [['upload_date'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DashboardLeadtimeKemas2::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'start' => $this->start,
            'stop' => $this->stop,
            // 'upload_date' => $this->upload_date,
            'week' => $this->week,
            'id' => $this->id,
        ]);

        $start = date("Y-m-d", $this->createTimeStart);
        $stop = date("Y-m-d", $this->createTimeEnd);

        $query->andFilterWhere(['ilike', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['ilike', 'snfg', $this->snfg])
            ->andFilterWhere(['ilike', 'posisi', $this->posisi])
            ->andFilterWhere(['ilike', 'nama_line', $this->nama_line])
            ->andFilterWhere(['ilike', 'sum_leadtime_raw', $this->sum_leadtime_raw])
            ->andFilterWhere(['ilike', 'sum_leadtime_bruto', $this->sum_leadtime_bruto])
            ->andFilterWhere(['ilike', 'jenis_proses', $this->jenis_proses])
            ->andFilterWhere(['ilike', 'sediaan', $this->sediaan])
            ->andFilterWhere(['ilike', 'sum_downtime', $this->sum_downtime])
            ->andFilterWhere(['ilike', 'sum_adjust', $this->sum_adjust])
            ->andFilterWhere(['ilike', 'sum_leadtime_net', $this->sum_leadtime_net])
            ->andFilterWhere(['ilike', 'keterangan_downtime', $this->keterangan_downtime])
            ->andFilterWhere(['ilike', 'keterangan_adjust', $this->keterangan_adjust])
            ->andFilterWhere(['ilike', 'nomo', $this->nomo])
            ->andFilterWhere(['ilike', 'snfg_komponen', $this->snfg_komponen])
            ->andWhere(['between', 'upload_date', $start, $stop]);

        return $dataProvider;
    }
}
