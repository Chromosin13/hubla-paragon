<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BarcodeMalaysiaPrinter;

/**
 * BarcodeMalaysiaPrinterSearch represents the model behind the search form about `app\models\BarcodeMalaysiaPrinter`.
 */
class BarcodeMalaysiaPrinterSearch extends BarcodeMalaysiaPrinter
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'qty_request', 'qty_total_per_snfg'], 'integer'],
            [['timestamp', 'snfg', 'koitem_fg', 'nama_fg', 'barcode', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BarcodeMalaysiaPrinter::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'timestamp' => $this->timestamp,
            'qty_request' => $this->qty_request,
            'qty_total_per_snfg' => $this->qty_total_per_snfg,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'koitem_fg', $this->koitem_fg])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'barcode', $this->barcode])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
