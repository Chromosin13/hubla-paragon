<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posisi_proses_dashboard".
 *
 * @property string $nomo
 * @property string $snfg
 * @property string $snfg_komponen
 * @property string $posisi
 * @property string $jenis_proses
 * @property string $lanjutan
 * @property string $datetime_write
 * @property string $start
 * @property string $due
 * @property string $nama_fg
 * @property string $process_status
 * @property string $is_done
 */
class PosisiProsesDashboard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function primaryKey()
    {
        return ['snfg_komponen'];
    }

    public static function tableName()
    {
        return 'posisi_proses_dashboard';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'snfg', 'snfg_komponen', 'posisi', 'jenis_proses', 'nama_fg', 'process_status', 'is_done','koitem_fg', 'nobatch'], 'string'],
            [['lanjutan'], 'number'],
            [['datetime_write', 'start', 'due'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nomo' => 'Nomo',
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'posisi' => 'Posisi',
            'jenis_proses' => 'Jenis Proses',
            'lanjutan' => 'Lanjutan',
            'datetime_write' => 'Datetime Write',
            'start' => 'Start',
            'due' => 'Due',
            'nama_fg' => 'Nama Fg',
            'process_status' => 'Process Status',
            'is_done' => 'Is Done',
            'koitem_fg' => 'Koitem FG',
            'nobatch' => 'No Batch'
        ];
    }
}
