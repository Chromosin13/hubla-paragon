<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kendala;

/**
 * KendalaSearch represents the model behind the search form about `app\models\Kendala`.
 */
class KendalaSearch extends Kendala
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'penimbangan_id', 'pengolahan_id', 'qc_bulk_id', 'kemas_1_id', 'kemas_2_id', 'qc_fg_id'], 'integer'],
            [['keterangan', 'start', 'stop'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kendala::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'start' => $this->start,
            'stop' => $this->stop,
            'penimbangan_id' => $this->penimbangan_id,
            'pengolahan_id' => $this->pengolahan_id,
            'qc_bulk_id' => $this->qc_bulk_id,
            'kemas_1_id' => $this->kemas_1_id,
            'kemas_2_id' => $this->kemas_2_id,
            'qc_fg_id' => $this->qc_fg_id,
        ]);

        $query->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
