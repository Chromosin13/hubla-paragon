<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MonitoringLinePenimbangan;

/**
 * MonitoringLinePenimbanganSearch represents the model behind the search form about `app\models\MonitoringLinePenimbangan`.
 */
class MonitoringLinePenimbanganSearch extends MonitoringLinePenimbangan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sediaan', 'nama_line', 'nama_fg', 'nomo', 'datetime_write', 'jenis_proses', 'status'], 'safe'],
            [['id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MonitoringLinePenimbangan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'datetime_write' => $this->datetime_write,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'sediaan', $this->sediaan])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nama_fg', $this->nama_fg])
            ->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'jenis_proses', $this->jenis_proses])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
