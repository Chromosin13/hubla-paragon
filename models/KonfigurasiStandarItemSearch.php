<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KonfigurasiStandarItem;

/**
 * KonfigurasiStandarItemSearch represents the model behind the search form about `app\models\KonfigurasiStandarItem`.
 */
class KonfigurasiStandarItemSearch extends KonfigurasiStandarItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ks_id', 'jumlah_operator', 'line_id','nourut'], 'integer'],
            [['kode', 'nama', 'jenis', 'proses', 'zona', 'print_flag', 'kode_streamline', 'last_update'], 'safe'],
            [['mpq', 'output', 'std_leadtime', 'pdt'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KonfigurasiStandarItem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ks_id' => $this->ks_id,
            'mpq' => $this->mpq,
            'jumlah_operator' => $this->jumlah_operator,
            'line_id' => $this->line_id,
            'output' => $this->output,
            'std_leadtime' => $this->std_leadtime,
            'pdt' => $this->pdt,
            'last_update' => $this->last_update,
            'nourut' => $this->nourut,
        ]);

        $query->andFilterWhere(['like', 'kode', $this->kode])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'jenis', $this->jenis])
            ->andFilterWhere(['like', 'proses', $this->proses])
            ->andFilterWhere(['like', 'zona', $this->zona])
            ->andFilterWhere(['like', 'print_flag', $this->print_flag])
            ->andFilterWhere(['like', 'kode_streamline', $this->kode_streamline]);

        return $dataProvider;
    }
}
