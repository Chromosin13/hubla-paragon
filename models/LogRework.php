<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_rework".
 *
 * @property string $snfg
 * @property string $snfg_komponen
 * @property string $nomo
 * @property string $posisi
 * @property string $datetime_start
 * @property string $datetime_stop
 * @property string $datetime_write
 * @property string $nama_operator
 * @property integer $lanjutan
 * @property string $jenis_proses
 * @property string $nama_line
 * @property integer $id
 * @property integer $uid
 */
class LogRework extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_rework';
    }

    public static function primaryKey()
    {
        return ['uid'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['snfg', 'snfg_komponen', 'nomo', 'posisi', 'nama_operator', 'jenis_proses', 'nama_line'], 'string'],
            [['datetime_start', 'datetime_stop', 'datetime_write'], 'safe'],
            [['lanjutan', 'id', 'uid'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'snfg' => 'Snfg',
            'snfg_komponen' => 'Snfg Komponen',
            'nomo' => 'Nomo',
            'posisi' => 'Posisi',
            'datetime_start' => 'Datetime Start',
            'datetime_stop' => 'Datetime Stop',
            'datetime_write' => 'Datetime Write',
            'nama_operator' => 'Nama Operator',
            'lanjutan' => 'Lanjutan',
            'jenis_proses' => 'Jenis Proses',
            'nama_line' => 'Nama Line',
            'id' => 'ID',
            'uid' => 'Uid',
        ];
    }
}
