<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rekonsiliasi_packaging".
 *
 * @property integer $id
 * @property integer $flow_input_snfg_id
 * @property string $nama_pack
 * @property string $nobatch
 * @property integer $supply
 * @property integer $rs_unb
 * @property integer $sisa_unb
 * @property integer $ok_unb
 * @property integer $rs_pro
 * @property integer $rm_pro
 * @property integer $rp_pro
 * @property integer $sisa_pro
 * @property integer $rqc
 * @property integer $total
 * @property integer $hilang
 */
class RekonsiliasiPackaging extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rekonsiliasi_packaging';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['flow_input_snfg_id', 'qty_supply','qty_realisasi','hasil_kemas', 'rs_unb', 'sisa_unb', 'ok_unb', 'rs_pro', 'rm_pro', 'rp_pro', 'sisa_pro', 'rqc', 'total', 'hilang','qty_supply_tambahan'], 'integer'],
            [['qty_mo','qty_component'], 'number'],
            [['nama_pack', 'nobatch','snfg','uom'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'flow_input_snfg_id' => 'Flow Input Snfg ID',
            'koitem_pack' => 'Koitem Packaging',
            'nama_pack' => 'Nama Packaging',
            'nobatch' => 'Nobatch',
            'qty_supply' => 'Qty Supply',
            'qty_supply_tambahan' => 'Qty Supply Tambahan',
            'qty_mo' => 'Qty MO',
            'qty_component' => 'Qty Component',
            'rs_unb' => 'Rs Unb',
            'sisa_unb' => 'Sisa Unb',
            'ok_unb' => 'Ok Unb',
            'rs_pro' => 'Reject Supply',
            'rm_pro' => 'Reject Mesin',
            'rp_pro' => 'Reject Proses',
            'sisa_pro' => 'Kembalian OK',
            'rqc' => 'Rqc',
            'total' => 'Total',
            'hilang' => 'Hilang',
            'snfg' => 'SNFG',
            'uom' => 'Satuan',
            'qty_realisasi' => 'Qty Realisasi',
            // 'qty_sample_qc' => 'Qty Sample Qc',
            'hasil_kemas' => 'Hasil Kemas',
        ];
    }
}
