<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kemas1EditJadwal;

/**
 * Kemas1EditJadwalSearch represents the model behind the search form of `app\models\Kemas1EditJadwal`.
 */
class Kemas1EditJadwalSearch extends Kemas1EditJadwal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_done', 'start_id', 'stop_id'], 'integer'],
            [['snfg_komponen', 'start', 'stop', 'jenis_kemas', 'nama_line', 'nama_operator'], 'safe'],
            [['lanjutan', 'jumlah_realisasi'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kemas1EditJadwal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lanjutan' => $this->lanjutan,
            'start' => $this->start,
            'stop' => $this->stop,
            'jumlah_realisasi' => $this->jumlah_realisasi,
            'is_done' => $this->is_done,
            'start_id' => $this->start_id,
            'stop_id' => $this->stop_id,
        ]);

        $query->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'jenis_kemas', $this->jenis_kemas])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator]);

        return $dataProvider;
    }
}
