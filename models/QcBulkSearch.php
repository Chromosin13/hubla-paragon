<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\QcBulk;

/**
 * QcBulkSearch represents the model behind the search form about `app\models\QcBulk`.
 */
class QcBulkSearch extends QcBulk
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nomo','snfg', 'ph', 'viskositas', 'berat_jenis', 'kadar', 'warna', 'bau', 'performance', 'bentuk', 'mikro', 'kejernihan', 'status', 'nama_qc', 'waktu', 'state','jenis_periksa','jenis_olah','nama_line','timestamp','snfg_komponen'], 'safe'],
            [['jumlah_operator','lanjutan','lanjutan_ist','is_done'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QcBulk::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'jumlah_operator' => $this->jumlah_operator,
            'waktu' => $this->waktu,
            'timestamp' => $this->timestamp,
            'lanjutan' => $this->lanjutan,
            'lanjutan_ist' => $this->lanjutan_ist,
            'is_done'=> $this->is_done,
        ]);

        $query
            ->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'ph', $this->ph])
            ->andFilterWhere(['like', 'viskositas', $this->viskositas])
            ->andFilterWhere(['like', 'berat_jenis', $this->berat_jenis])
            ->andFilterWhere(['like', 'kadar', $this->kadar])
            ->andFilterWhere(['like', 'warna', $this->warna])
            ->andFilterWhere(['like', 'bau', $this->bau])
            ->andFilterWhere(['like', 'performance', $this->performance])
            ->andFilterWhere(['like', 'bentuk', $this->bentuk])
            ->andFilterWhere(['like', 'mikro', $this->mikro])
            ->andFilterWhere(['like', 'kejernihan', $this->kejernihan])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'nama_qc', $this->nama_qc])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'jenis_periksa', $this->jenis_periksa])
            ->andFilterWhere(['like', 'jenis_periksa', $this->jenis_olah])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen]);

        return $dataProvider;
    }
}
