<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_data_sediaan".
 *
 * @property integer $id
 * @property string $sediaan
 */
class MasterDataSediaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_data_sediaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sediaan'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sediaan' => 'Sediaan',
        ];
    }
}
