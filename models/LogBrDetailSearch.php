<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogBrDetail;

/**
 * LogBrDetailSearch represents the model behind the search form about `app\models\LogBrDetail`.
 */
class LogBrDetailSearch extends LogBrDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['kode_bulk', 'formula_reference', 'status_formula', 'brand'], 'safe'],
            [['qty_batch'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogBrDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'qty_batch' => $this->qty_batch,
        ]);

        $query->andFilterWhere(['like', 'kode_bulk', $this->kode_bulk])
            ->andFilterWhere(['like', 'formula_reference', $this->formula_reference])
            ->andFilterWhere(['like', 'status_formula', $this->status_formula])
            ->andFilterWhere(['like', 'brand', $this->brand]);

        return $dataProvider;
    }
}
