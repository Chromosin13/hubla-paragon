<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_receive_ndc".
 *
 * @property integer $id
 * @property string $nosj
 * @property string $id_palet
 * @property string $snfg
 * @property integer $nourut
 * @property string $nobatch
 * @property string $nama_fg
 * @property string $status
 */
class LogReceiveNdc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $count;
    
    public static function tableName()
    {
        return 'log_receive_ndc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nosj', 'id_palet', 'snfg', 'nobatch', 'nama_fg', 'status','pic','log'], 'string'],
            [['nourut','qty'], 'integer'],
            [['timestamp_terima'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nosj' => 'Nosj',
            'id_palet' => 'Id Palet',
            'snfg' => 'Snfg',
            'nourut' => 'Nourut',
            'nobatch' => 'Nobatch',
            'nama_fg' => 'Nama Fg',
            'status' => 'Status',
            'qty' => 'Qty',
            'pic' => 'Pic',
            'log' => 'Log',
        ];
    }
}
