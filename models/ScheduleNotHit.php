<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "schedule_not_hit".
 *
 * @property integer $id
 * @property string $snfg
 * @property string $start
 * @property string $schedule_due
 * @property string $koitem_fg
 * @property string $nama_fg
 * @property string $jumlah_pcs
 * @property string $qty
 * @property integer $is_done
 * @property string $timestamp_check
 * @property string $remark
 * @property string $level_1
 * @property string $level_2
 * @property string $level_3
 * @property string $corrective_action
 * @property string $preventive_action
 * @property string $pic
 * @property string $due
 */
class ScheduleNotHit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schedule_not_hit';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_done'], 'integer'],
            [['snfg', 'koitem_fg', 'nama_fg', 'remark', 'level_1', 'level_2', 'level_3', 'corrective_action', 'preventive_action', 'pic','sediaan'], 'string'],
            [['start', 'schedule_due', 'timestamp_check', 'due'], 'safe'],
            [['jumlah_pcs', 'qty_release','qty_nh'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'snfg' => 'Snfg',
            'start' => 'Start',
            'schedule_due' => 'Schedule Due',
            'koitem_fg' => 'Koitem Fg',
            'nama_fg' => 'Nama Fg',
            'jumlah_pcs' => 'Jumlah Pcs',
            'qty_release' => 'Qty Release',
            'qty_nh' => 'Qty Not Hit',
            'is_done' => 'Is Done',
            'timestamp_check' => 'Timestamp Check',
            'remark' => 'Remark',
            'level_1' => 'Level 1',
            'level_2' => 'Level 2',
            'level_3' => 'Level 3',
            'corrective_action' => 'Corrective Action',
            'preventive_action' => 'Preventive Action',
            'pic' => 'Pic',
            'due' => 'Due',
        ];
    }
}
