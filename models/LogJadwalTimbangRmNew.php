<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_jadwal_timbang_rm_new".
 *
 * @property string $nomo
 * @property string $nama_fg
 * @property string $qty_batch
 * @property string $scheduled_start
 * @property string $formula_reference
 * @property string $week
 * @property string $lokasi
 * @property string $nama_line
 */
class LogJadwalTimbangRmNew extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_jadwal_timbang_rm_new';
    }

    public static function primaryKey()
    {
        return ['nomo'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomo', 'nama_fg', 'formula_reference', 'week', 'lokasi', 'nama_line', 'status'], 'string'],
            [['qty_batch'], 'number'],
            [['custom_order'], 'integer'],
            [['scheduled_start'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nomo' => 'Nomo',
            'nama_fg' => 'Nama Fg',
            'qty_batch' => 'Qty Batch',
            'scheduled_start' => 'Scheduled Start',
            'formula_reference' => 'Formula Reference',
            'week' => 'Week',
            'lokasi' => 'Lokasi',
            'status' => 'Status',
            'nama_line' => 'Nama Line',
        ];
    }
}
