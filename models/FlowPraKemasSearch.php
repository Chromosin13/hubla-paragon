<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FlowPraKemas;

/**
 * FlowPraKemasSearch represents the model behind the search form about `app\models\FlowPraKemas`.
 */
class FlowPraKemasSearch extends FlowPraKemas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['posisi', 'datetime_start', 'datetime_stop', 'datetime_write', 'nama_operator', 'nama_line', 'jenis_kemas', 'snfg', 'nobatch', 'exp_date', 'nosmb', 'barcode', 'fg_name_odoo', 'posisi_scan'], 'safe'],
            [['lanjutan', 'is_done', 'counter', 'id'], 'integer'],
            [['jumlah_realisasi'], 'number'],
            [['staging'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FlowPraKemas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lanjutan' => $this->lanjutan,
            'datetime_start' => $this->datetime_start,
            'datetime_stop' => $this->datetime_stop,
            'datetime_write' => $this->datetime_write,
            'is_done' => $this->is_done,
            'counter' => $this->counter,
            'jumlah_realisasi' => $this->jumlah_realisasi,
            'staging' => $this->staging,
            'exp_date' => $this->exp_date,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'posisi', $this->posisi])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'jenis_kemas', $this->jenis_kemas])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'nobatch', $this->nobatch])
            ->andFilterWhere(['like', 'nosmb', $this->nosmb])
            ->andFilterWhere(['like', 'barcode', $this->barcode])
            ->andFilterWhere(['like', 'fg_name_odoo', $this->fg_name_odoo])
            ->andFilterWhere(['like', 'posisi_scan', $this->posisi_scan]);

        return $dataProvider;
    }
}
