<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sisa_kemas".
 *
 * @property integer $id
 * @property string $sisa_bulk
 * @property string $sisa_packaging
 * @property string $packaging_reject
 * @property string $finish_good_reject
 * @property string $snfg
 */
class SisaKemas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sisa_kemas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sisa_bulk', 'sisa_packaging', 'packaging_reject', 'finish_good_reject'], 'number'],
            [['snfg'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sisa_bulk' => 'Sisa Bulk',
            'sisa_packaging' => 'Sisa Packaging',
            'packaging_reject' => 'Packaging Reject',
            'finish_good_reject' => 'Finish Good Reject',
            'snfg' => 'Snfg',
        ];
    }
}
