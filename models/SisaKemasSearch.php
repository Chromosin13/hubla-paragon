<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SisaKemas;

/**
 * SisaKemasSearch represents the model behind the search form about `app\models\SisaKemas`.
 */
class SisaKemasSearch extends SisaKemas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['sisa_bulk', 'sisa_packaging', 'packaging_reject', 'finish_good_reject'], 'number'],
            [['snfg'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SisaKemas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sisa_bulk' => $this->sisa_bulk,
            'sisa_packaging' => $this->sisa_packaging,
            'packaging_reject' => $this->packaging_reject,
            'finish_good_reject' => $this->finish_good_reject,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg]);

        return $dataProvider;
    }
}
