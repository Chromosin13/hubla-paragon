<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nama_admin".
 *
 * @property integer $id
 * @property string $nama
 * @property string $nomor_po
 */
class NamaAdmin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nama_admin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'nomor_po'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'nomor_po' => 'Nomor Po',
        ];
    }
}
