<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\QcFgV2;

/**
 * QcFgV2Search represents the model behind the search form of `app\models\QcFgV2`.
 */
class QcFgV2Search extends QcFgV2
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','nourut'], 'integer'],
            [['snfg','status', 'timestamp','timestamp_kirim','timestamp_terima_karantina','timestamp_terima_ndc','inspektor_qcfg','inspektor_karantina'], 'safe'],
            [['qty', 'qty_sample','qty_karantina','is_done','jumlah_koli'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QcFgV2::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['nourut'=>SORT_ASC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'nourut' => $this->nourut,
            'is_done' => $this->is_done,
            'qty' => $this->qty,
            'qty_karantina' => $this->qty_karantina,
            'qty_sample' => $this->qty_sample,
            'timestamp' => $this->timestamp,
            'timestamp_kirim' => $this->timestamp_kirim,
            'timestamp_check' => $this->timestamp_check,
            'jumlah_koli' => $this->jumlah_koli,
            'timestamp_terima_karantina' => $this->timestamp_terima_karantina,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
        ->andFilterWhere(['like', 'status', $this->status])
        ->andFilterWhere(['like', 'inspektor_qcfg', $this->inspektor_qcfg])
        ->andFilterWhere(['like', 'status_check', $this->status_check])
        ->andFilterWhere(['like', 'inspektor_karantina', $this->inspektor_karantina]);

        return $dataProvider;
    }
}
