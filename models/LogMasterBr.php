<?php

namespace app\models;

use Yii;
use yii\helpers\Json;
use yii\data\SqlDataProvider;

/**
 * This is the model class for table "log_master_br".
 *
 * @property integer $id
 * @property string $kode_bulk
 * @property integer $formula_reference
 * @property string $nama_bulk
 * @property string $timestamp
 * @property string $last_user
 */
class LogMasterBr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_master_br';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_bulk', 'nama_bulk', 'last_user'], 'string'],
            [['formula_reference'], 'integer'],
            [['timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode_bulk' => 'Kode Bulk',
            'formula_reference' => 'Formula Reference',
            'nama_bulk' => 'Nama Bulk',
            'timestamp' => 'Timestamp',
            'last_user' => 'Last User',
        ];
    }
}
