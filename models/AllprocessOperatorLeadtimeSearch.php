<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AllprocessOperatorLeadtime;

/**
 * AllprocessOperatorLeadtimeSearch represents the model behind the search form about `app\models\AllprocessOperatorLeadtime`.
 */
class AllprocessOperatorLeadtimeSearch extends AllprocessOperatorLeadtime
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row_number'], 'integer'],
            [['posisi', 'snfg', 'snfg_komponen', 'jenis_proses', 'nama', 'tanggal'], 'safe'],
            [['lanjutan', 'leadtime'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AllprocessOperatorLeadtime::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'row_number' => $this->row_number,
            'lanjutan' => $this->lanjutan,
            'tanggal' => $this->tanggal,
            'leadtime' => $this->leadtime,
        ]);

        $query->andFilterWhere(['like', 'posisi', $this->posisi])
            ->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'jenis_proses', $this->jenis_proses])
            ->andFilterWhere(['like', 'nama', $this->nama]);

        return $dataProvider;
    }
}
