<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_penimbangan_rm_print_queue".
 *
 * @property integer $id
 * @property integer $flow_input_mo_id
 * @property string $nomo
 * @property string $kode_bb
 * @property string $nama_bb
 * @property string $weight
 * @property string $pack_date
 * @property string $timestamp
 * @property string $timbangan
 * @property string $nama_operator
 * @property string $satuan
 * @property string $no_batch
 * @property string $kode_olah
 */
class LogPenimbanganRmPrintQueue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_penimbangan_rm_print_queue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['flow_input_mo_id'], 'integer'],
            [['nomo', 'kode_bb', 'nama_bb', 'timbangan', 'nama_operator', 'satuan', 'no_batch', 'kode_olah'], 'string'],
            [['weight'], 'number'],
            [['pack_date', 'timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'flow_input_mo_id' => 'Flow Input Mo ID',
            'nomo' => 'Nomo',
            'kode_bb' => 'Kode Bb',
            'nama_bb' => 'Nama Bb',
            'weight' => 'Weight',
            'pack_date' => 'Pack Date',
            'timestamp' => 'Timestamp',
            'timbangan' => 'Timbangan',
            'nama_operator' => 'Nama Operator',
            'satuan' => 'Satuan',
            'no_batch' => 'No Batch',
            'kode_olah' => 'Kode Olah',
        ];
    }
}
