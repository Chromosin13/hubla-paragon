<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Penimbangan;

/**
 * PenimbanganSearch represents the model behind the search form about `app\models\Penimbangan`.
 */
class PenimbanganSearch extends Penimbangan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['snfg', 'nama_line', 'nama_operator', 'nama_operator_2', 'waktu', 'state','snfg_komponen','jenis_penimbangan','timestamp','nomo'], 'safe'],
            [['jumlah_operator', 'lanjutan','lanjutan_ist','is_done'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Penimbangan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'jumlah_operator' => $this->jumlah_operator,
            'waktu' => $this->waktu,
            'lanjutan' => $this->lanjutan,
            'timestamp' => $this->timestamp,
            'lanjutan_ist' => $this->lanjutan_ist,
            'is_done'=> $this->is_done,
            'nomo' => $this->nomo,
        ]);

        $query->andFilterWhere(['like', 'snfg', $this->snfg])
            ->andFilterWhere(['like', 'snfg_komponen', $this->snfg_komponen])
            ->andFilterWhere(['like', 'jenis_penimbangan', $this->jenis_penimbangan])
            ->andFilterWhere(['like', 'nama_line', $this->nama_line])
            ->andFilterWhere(['like', 'nama_operator', $this->nama_operator])
            ->andFilterWhere(['like', 'nama_operator_2', $this->nama_operator_2])
            ->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'state', $this->state]);

        return $dataProvider;
    }
}
