<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bill_of_materials".
 *
 * @property integer $id
 * @property string $no_formula
 * @property string $koitem_fg
 * @property integer $no_revisi
 * @property string $kode_bb
 * @property string $kode_olah
 * @property string $nama_bb
 * @property integer $siklus
 * @property string $weight
 * @property string $satuan
 * @property string $sediaan
 * @property integer $cycle_time
 * @property string $timbangan
 * @property integer $operator
 * @property string $no_timbangan
 */
class BillOfMaterials extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill_of_materials';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_formula', 'koitem_fg', 'no_revisi', 'kode_bb', 'nama_bb', 'siklus', 'weight', 'satuan', 'sediaan', 'cycle_time', 'timbangan', 'operator', 'no_timbangan'], 'required'],
            [['no_formula', 'koitem_fg', 'kode_bb', 'kode_olah', 'nama_bb', 'satuan', 'sediaan', 'timbangan', 'no_timbangan', 'status'], 'string'],
            [['no_revisi', 'siklus', 'cycle_time', 'operator'], 'integer'],
            [['weight'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_formula' => 'No Formula',
            'koitem_fg' => 'Koitem Fg',
            'no_revisi' => 'No Revisi',
            'kode_bb' => 'Kode Bb',
            'kode_olah' => 'Kode Olah',
            'nama_bb' => 'Nama Bb',
            'siklus' => 'Siklus',
            'weight' => 'Weight',
            'satuan' => 'Satuan',
            'sediaan' => 'Sediaan',
            'cycle_time' => 'Cycle Time',
            'timbangan' => 'Timbangan',
            'operator' => 'Operator',
            'no_timbangan' => 'No Timbangan',
            'status' => 'Status'
        ];
    }
}
