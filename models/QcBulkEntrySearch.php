<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\QcBulkEntry;

/**
 * QcBulkEntrySearch represents the model behind the search form of `app\models\QcBulkEntry`.
 */
class QcBulkEntrySearch extends QcBulkEntry
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nourut'], 'integer'],
            [['nomo', 'status', 'timestamp', 'nama_inspektor','created_at','jenis_adjust','keterangan_adjust'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QcBulkEntry::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['nourut'=>SORT_ASC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'nourut' => $this->nourut,
            'timestamp' => $this->timestamp,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'nomo', $this->nomo])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'jenis_adjust', $this->jenis_adjust])
            ->andFilterWhere(['like', 'keterangan_adjust', $this->keterangan_adjust])
            ->andFilterWhere(['like', 'nama_inspektor', $this->nama_inspektor]);

        return $dataProvider;
    }
}
