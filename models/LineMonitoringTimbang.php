<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "line_monitoring_timbang".
 *
 * @property string $proses
 * @property string $nama_line
 * @property string $nomo
 * @property string $waktu
 * @property string $state
 * @property string $lanjutan
 * @property string $jenis_proses
 */
class LineMonitoringTimbang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'line_monitoring_timbang';
    }

    public static function primaryKey()
    {
        return ['nama_line'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proses', 'nama_line', 'nomo', 'state', 'jenis_proses'], 'string'],
            [['waktu'], 'safe'],
            [['lanjutan'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'proses' => 'Proses',
            'nama_line' => 'Nama Line',
            'nomo' => 'Nomo',
            'waktu' => 'Waktu',
            'state' => 'State',
            'lanjutan' => 'Lanjutan',
            'jenis_proses' => 'Jenis Proses',
        ];
    }
}
