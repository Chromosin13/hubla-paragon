<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RekonsiliasiPackaging;

/**
 * RekonsiliasiPackagingSearch represents the model behind the search form about `app\models\RekonsiliasiPackaging`.
 */
class RekonsiliasiPackagingSearch extends RekonsiliasiPackaging
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'flow_input_snfg_id', 'qty_supply', 'rs_unb', 'sisa_unb', 'ok_unb', 'rs_pro', 'rm_pro', 'rp_pro', 'sisa_pro', 'rqc', 'total', 'hilang'], 'integer'],
            [['nama_pack', 'nobatch'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RekonsiliasiPackaging::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'flow_input_snfg_id' => $this->flow_input_snfg_id,
            'qty_supply' => $this->qty_supply,
            'rs_unb' => $this->rs_unb,
            'sisa_unb' => $this->sisa_unb,
            'ok_unb' => $this->ok_unb,
            'rs_pro' => $this->rs_pro,
            'rm_pro' => $this->rm_pro,
            'rp_pro' => $this->rp_pro,
            'sisa_pro' => $this->sisa_pro,
            'rqc' => $this->rqc,
            'total' => $this->total,
            'hilang' => $this->hilang,
        ]);

        $query->andFilterWhere(['like', 'nama_pack', $this->nama_pack])
            ->andFilterWhere(['like', 'nobatch', $this->nobatch]);

        return $dataProvider;
    }
}
