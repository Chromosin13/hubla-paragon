<?php 
namespace app\models;

use yii\db\ActiveRecord;

class Karyawan extends ActiveRecord
{
	public function rules()
	{
		return [
			[['nama','email','bagian'],'required'],
			['email','email'],
		];
	}

	public function dataBagian()
	{
		return [
			'logistic' => 'Logistic',
			'quality' => 'Quality',
			'mex' => 'MEX',
			'ndc' => 'NDC',
			'rnd' => 'RnD',
			'it'  => 'IT'
		];

	}
}

?>