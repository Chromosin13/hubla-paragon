<?php
namespace console\controllers;
use yii\console\Controller;
 
Class SendDataToMrpController extends Controller
{
    public function actionMake(){
        // root of directory yii2
        // /var/www/html/<yii2>
        $rootyii = realpath(dirname(__FILE__).'/../../');
 
        // create file <jam:menit:detik>.txt
        $filename = date('H:i:s') . '.txt';
        $folder = $rootyii.'/cronjob/'.$filename;
        $f = fopen($folder, 'w');
        $fw = fwrite($f, 'now : ' . $filename);
        fclose($f);
    }

    public function actionSend(){

        print_r('expression');
        exit();
        $sql = "SELECT * FROM log_fro_mrp WHERE status = 'ready' ";

        $list_nomo = Yii::$app->db->createCommand($sql)->queryAll();

        if (!empty($list_nomo))
        {
            foreach($list_nomo as $list)
            {
                $sql = "SELECT lpr.no_smb,lprs.kode_bb,lprs.kode_internal,lprs.realisasi,lprs.satuan_realisasi,lprs.no_batch as nobatch,lprs.log, 1 as is_split 
                    from log_penimbangan_rm_split lprs
                    join log_penimbangan_rm lpr on lprs.log_penimbangan_rm_id = lpr.id
                    where lpr.nomo = '".$list['nomo']."'
                    union
                    select no_smb, kode_bb,kode_internal,realisasi,satuan_realisasi, nobatch, log, 0 as is_split from log_penimbangan_rm where nomo = '".$list['nomo']."'
                    and is_split = 0";

                $sql = " SELECT data.nomo,
                            data.no_smb,
                            data.kode_bb,
                            data.kode_internal,
                            data2.realisasi,
                            data.satuan_realisasi,
                            data.nobatch,
                            data.log,
                            data.is_split
                           FROM ( SELECT DISTINCT ON (a.nomo, a.kode_bb, a.nobatch) a.nomo,
                                    a.no_smb,
                                    a.kode_bb,
                                    a.kode_internal,
                                    a.realisasi,
                                    a.satuan_realisasi,
                                    a.nobatch,
                                    a.log,
                                    a.is_split
                                   FROM ( SELECT lpr.nomo,
                                            lpr.no_smb,
                                            lprs.kode_bb,
                                            lprs.kode_internal,
                                            lprs.realisasi,
                                            lprs.satuan_realisasi,
                                            lprs.no_batch AS nobatch,
                                            lprs.log,
                                            1 AS is_split
                                           FROM log_penimbangan_rm_split lprs
                                             JOIN log_penimbangan_rm lpr ON lprs.log_penimbangan_rm_id = lpr.id
                                        UNION
                                         SELECT log_penimbangan_rm.nomo,
                                            log_penimbangan_rm.no_smb,
                                            log_penimbangan_rm.kode_bb,
                                            log_penimbangan_rm.kode_internal,
                                            log_penimbangan_rm.realisasi,
                                            log_penimbangan_rm.satuan_realisasi,
                                            log_penimbangan_rm.nobatch,
                                            log_penimbangan_rm.log,
                                            0 AS is_split
                                           FROM log_penimbangan_rm
                                          WHERE log_penimbangan_rm.is_split = 0) a) data
                             LEFT JOIN ( SELECT b.nomo,
                                    b.no_smb,
                                    b.kode_bb,
                                    sum(b.realisasi) AS realisasi,
                                    b.satuan_realisasi,
                                    b.nobatch
                                   FROM ( SELECT lpr.nomo,
                                            lpr.no_smb,
                                            lprs.kode_bb,
                                            lprs.realisasi,
                                            lprs.satuan_realisasi,
                                            lprs.no_batch AS nobatch
                                           FROM log_penimbangan_rm_split lprs
                                             JOIN log_penimbangan_rm lpr ON lprs.log_penimbangan_rm_id = lpr.id
                                        UNION
                                         SELECT log_penimbangan_rm.nomo,
                                            log_penimbangan_rm.no_smb,
                                            log_penimbangan_rm.kode_bb,
                                            log_penimbangan_rm.realisasi,
                                            log_penimbangan_rm.satuan_realisasi,
                                            log_penimbangan_rm.nobatch
                                           FROM log_penimbangan_rm
                                          WHERE log_penimbangan_rm.is_split = 0) b
                                  GROUP BY b.nomo, b.no_smb, b.kode_bb, b.satuan_realisasi, b.nobatch) data2 ON data.nomo::text = data2.nomo::text AND data.kode_bb::text = data2.kode_bb::text AND data.nobatch::text = data2.nobatch::text
                          WHERE data.nomo::text = :nomo and data.kode_bb != 'AIR-RO--L'";
                // $sql = "SELECT realisasi,satuan_realisasi,kode_internal,kode_bb,log,no_smb,nomo,nobatch FROM log_penimbangan_rm WHERE nomo = '".$nomo."' and kode_bb != 'AIR-RO--L' ";
                $rincian_bb = Yii::$app->db->createCommand($sql,[':nomo'=>$nomo])->queryAll();

                // print_r('<pre>');
                // print_r($rincian_bb);
                $item_json = json_encode($rincian_bb);
                $create_json_file = file_put_contents("update_stock_ndc/data_rmw.json", $item_json);
                if ($create_json_file)
                {
                    // echo "JSON file created successfully...";
                    // require (Yii::getAlias('@webroot').'/update_stock_ndc/mrp_fro.php');
                }else{
                    // echo "Oops! Error creating json file...";
                    // return $this->redirect(['qc-fg-v2/halaman-terima-ndc', 'e'=>'json-fail']);
                }

            }
        }

    }
}