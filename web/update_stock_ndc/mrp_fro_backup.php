<?php
require_once('ripcord/ripcord.php');

// $connection=pg_connect("host=localhost port=5432 dbname=flowreport_development user=postgres password=ptidotnet")  or die ("Could not connect to server\n"); ;

// $username = 'admin';
// $password = '1712sib';
// $url = 'http://10.3.181.241:13169';
// // $db = 'odoomrp20210215';
// $db = 'mrp27082021';


$connection=pg_connect("host=localhost port=5432 dbname=flowreport user=postgres password=ptidotnet");

$username = 'support-fro';
$password = '1234567890';
$url = 'https://odoomrp.pti-cosmetics.com';
$db = 'db_odoomrp_2';

// $nomo = $_POST['nomo'];
// $nomo = $_GET['nomo'];
$url_auth = $url . '/xmlrpc/2/common';
$url_exec = $url . '/xmlrpc/2/object';
$common = ripcord::client("$url/xmlrpc/2/common");
$uid = $common->authenticate($db, $username, $password, array());
$models = ripcord::client($url_exec);

//GET DATA FROM JSON FILE
$json_content = file_get_contents("/var/www/html/flowreport/web/update_stock_ndc/data_rmw.json");
$array_content = json_decode($json_content);

date_default_timezone_set('Asia/Jakarta');

$data_rmw = [];
foreach ($array_content as $object){
    $obj_arr = (array) $object;
    $data_rmw[] = $obj_arr;
}
// print_r('<pre>');
// print_r($data_rmw);
// print_r('</pre>');

//GET DATA FROM LOG FRO MRP DETAIL
// $rincian = pg_query_params($connection, "SELECT * FROM log_fro_mrp_detail WHERE nomo = $1",[$nomo]);
// print_r('<pre>');
// print_r($rincian);
// exit();

foreach ($data_rmw as $bb){
    //DATA FROM UI SEBELUMNYA
    $id = $bb['id'];
    $kode_bb = $bb['kode_bb'];
    $nomo = $bb['nomo'];
    // $kode_internal = $bb['kode_internal'];
    $nobatch = $bb['nobatch'];
    $no_smb = $bb['no_smb'];
    $realisasi = $bb['realisasi'];
    $satuan_realisasi = $bb['satuan_realisasi'];
    if ($bb['log']=='scan'){
        $log = true;    
    }else{
        $log = false;
    }

    if (empty($nobatch) || empty($realisasi)){
        // print_r('<pre>');
        // print_r($kode_bb." : SKIP, DATA FRO KOSONG");
        $current_time = date('Y-m-d H:i:s',time());
        $update_log = pg_query_params(" UPDATE log_fro_mrp_detail SET timestamp = $1, log_odoo = 'skip', keterangan = 'Data FRO kosong' WHERE id = $2",[$current_time,$id]);
        continue;
    }


    // $kode_bb = 'SILI-2005-DC-K';
    // $kode_internal = '';
    // $nobatch = 'GBMJTK6/FJ30305';
    // $no_smb = 'INT/210025333';
    // $realisasi = 100;
    // $satuan_realisasi = 'g';
    // $log = false;        

    $uom = $models->execute_kw($db, $uid, $password, 'uom.uom', 'search_read',
                array(array(array('name', '=', $satuan_realisasi))),
                array('fields' =>array('id')));
    if (empty($uom)){
        // print_r('<pre>');
        // print_r($kode_bb." : SKIP, UOM NOT FOUND");

        //Log uom ID Not Found
        $current_time = date('Y-m-d H:i:s',time());
        $update_log = pg_query_params(" UPDATE log_fro_mrp_detail SET timestamp = $1, log_odoo = 'skip', keterangan = 'Satuan tidak ditemukan' WHERE id = $2",[$current_time,$id]);
        continue;
    }
    $uom_id = $uom[0]['id'];

    // print_r('uom_fro :');
    // print_r($uom_id);
    // print_r('<br>');

    // Get product
    $cek_product = $models->execute_kw($db, $uid, $password, 'product.product', 'search_read',
                array(array(array('default_code', '=', $kode_bb))),
                array('fields' =>array('id','product_tmpl_id')));

    if (empty($cek_product)){
        // print_r('<pre>');
        // print_r($kode_bb." : SKIP PRODUCT NOT FOUND");

        //Log Product Not Found
        $current_time = date('Y-m-d H:i:s',time());
        $update_log = pg_query_params(" UPDATE log_fro_mrp_detail SET timestamp = $1, log_odoo = 'skip', keterangan = 'Produk tidak ditemukan' WHERE id = $2",[$current_time,$id]);
        continue;
    }

    // print_r('<pre>');
    // print_r($cek_product);
    // exit();

    $product_id = $cek_product[0]['id'];
    $product_tmpl_id = $cek_product[0]['product_tmpl_id'][0];

    $cek_product_antara = $models->execute_kw($db, $uid, $password, 'product.template', 'search_read',
                array(array(array('id', '=', $product_tmpl_id))),
                array('fields' =>array('id','categ_id')));

    // print_r('<pre>');
    // print_r($cek_product_antara);
    // exit();

    $product_category = $cek_product_antara[0]['categ_id'][1];

    if (strpos(strtolower($product_category),'antara')!==false){
        // print_r('<pre>');
        // print_r($kode_bb." : SKIP PRODUCT ANTARA");
        //Product Antara
        $current_time = date('Y-m-d H:i:s',time());
        $update_log = pg_query_params(" UPDATE log_fro_mrp_detail SET timestamp = $1, log_odoo = 'skip', keterangan = 'Produk antara' WHERE id = $2",[$current_time,$id]);
        continue;
    }



    //Jika kode_bb tidak ditemukan di product.product lalu gimana

    $lot = $models->execute_kw($db, $uid, $password, 'stock.production.lot', 'search_read',
        array(array(array('product_id', '=', $product_id),array('name','=',$nobatch))),
        array('fields' =>array('id')));

    //Jika lot odoo ditemukan maka cek dulu apakah sama dengan reserved
    if (!empty($lot)){
        $lot_id = $lot[0]['id']; 
        $sml = $models->execute_kw($db, $uid, $password, 'stock.move.line', 'search_read',
            array(array(array('product_id', '=', $product_id),array('reference','=',$no_smb),array('lot_id','=',$lot_id))),
            array('fields' =>array('id','product_uom_id')));
        // print_r('uom_fro :');
        // print_r($uom_id);
        // print_r('<br>');
        // print_r($kode_bb);
        // print_r('sml');
        // print_r($sml);
        // print_r('<br>');

        //Jika batch sama dengan yang di reserved maka update data
        if(!empty($sml)){
            // print_r('<pre>');
            // print_r($kode_bb." : BATCH SAMA, UPDATE DATA");


            if ($sml[0]['product_uom_id'][0]==3 and $uom_id==4){
                // print_r('Convert g to kg');

                $realisasi = $realisasi/1000;
            }else if ($sml[0]['product_uom_id'][0]==4 and $uom_id==3){
                // print_r('Convert kg to g');

                $realisasi = $realisasi*1000;
            }else{

            }

            // print_r('Realisasi : ');
            // print_r($realisasi);
            // print_r('<br>');

            //Write data
            $update = array(
                        'lot_fro'=>$nobatch,
                        'qty_done'=>$realisasi,
                        'product_uom_id'=>$uom_id,
                        'scan'=>$log
                         );
            //Uncomment ini
            $update_sml = $models->execute_kw($db, $uid, $password, 'stock.move.line', 'write',array(array($sml[0]['id']),$update));

            if ($update_sml == 1 ){
                print_r('success');
                $current_time = date('Y-m-d H:i:s',time());
                $update_log = pg_query_params(" UPDATE log_fro_mrp_detail SET timestamp = $1, log_odoo = 'update', keterangan = 'Batch Odoo & FRO match' WHERE id = $2",[$current_time,$id]);
            }else{
                print_r('failed');
            }

        //Jika batch berbeda dengan reserved maka create data
            // Insert data baru berisi 
                // Kode Item, 
                // Location Destination, 
                // no. SMB, 
                // picking_id, 
                // company_id, 
                // Lot FRO, 
                // qty Done, 
                // UOM, 
                // Log scan
        }else{
            
            $stock_picking = $models->execute_kw($db, $uid, $password, 'stock.picking', 'search',
                array(array(array('name', '=', $no_smb))),
                array('limit'=>1));
            if (!empty($stock_picking)){
                $picking_id = $stock_picking[0];

            }else{
                // print_r('<pre>');
                // print_r($kode_bb." : SKIP STOCK PICKING NOT FOUND");

                $current_time = date('Y-m-d H:i:s',time());
                $update_log = pg_query_params(" UPDATE log_fro_mrp_detail SET timestamp = $1, log_odoo = 'skip', keterangan = 'Stock picking tidak ditemukan' WHERE id = $2",[$current_time,$id]);

                continue;
            }
            // print_r('picking_id : ');
            // print_r($picking_id);

            $smb = $models->execute_kw($db, $uid, $password, 'stock.picking', 'read', array($picking_id), array('fields'=>array('move_line_ids','location_id','location_dest_id')));
            // print_r('<pre>');
            // print_r($smb);

            $location_id = $smb[0]['location_id'][0];
            $location_dest_id = $smb[0]['location_dest_id'][0];
            $move_line_ids = $smb[0]['move_line_ids'];
        
            //Jika SML tidak ditemukan cek log di fro, scan atau input manual 

            //Jika scan ($log == true), maka create data baru dengan nobatch reserve di odoo menjadi sama dengan FRO, 
            //Jika input manual ($log == false) create data baru dengan nobatch reserve null
            
            if ($log){
                // print_r('<pre>');
                // print_r($kode_bb." : CREATE, RESERVE BATCH SAME WITH FRO BATCH");

                $new_sml = array('product_id'=>$product_id,
                            'location_id'=>$location_id,
                            'location_dest_id'=>$location_dest_id,
                            'lot_id'=>$lot_id,
                            'lot_fro'=>$nobatch,
                            'qty_done'=>$realisasi,
                            'product_uom_id'=>$uom_id,
                            'reference'=>$no_smb,
                            'scan'=>$log,
                            'picking_id'=>$picking_id,
                            'company_id'=>1 );

                //Uncomment disini
                $new_picking_id = $models->execute_kw($db, $uid, $password,'stock.move.line', 'create', 
                                    array($new_sml));

                $move_line_ids[] =$new_picking_id;
                
                $update_sml = $models->execute_kw($db, $uid, $password, 'stock.move.line', 'write', array(array($picking_id), array('move_line_ids'=>$move_line_ids)));            
                //Sampai sini

                $current_time = date('Y-m-d H:i:s',time());
                $update_log = pg_query_params(" UPDATE log_fro_mrp_detail SET timestamp = $1, log_odoo = 'create', keterangan = 'Batch tidak sama dengan reserve, Log FRO adalah scan' WHERE id = $2",[$current_time,$id]);

                // if ($update_sml == 1 ){
                //     print_r('success');
                //     $current_time = date('Y-m-d H:i:s',time());
                //     $update_log = pg_query_params(" UPDATE log_fro_mrp_detail SET timestamp = $1, log_odoo = 'create', keterangan = 'Batch tidak sama dengan reserve, Log FRO adalah scan' WHERE id = $2",[$current_time,$id]);
                // }else{
                //     print_r('failed');
                // }

            }else{
                // print_r('<pre>');
                // print_r($kode_bb." : CREATE, RESERVE BATCH NULL");

                $new_sml = array('product_id'=>$product_id,
                            'location_id'=>$location_id,
                            'location_dest_id'=>$location_dest_id,
                            'lot_fro'=>$nobatch,
                            'qty_done'=>$realisasi,
                            'product_uom_id'=>$uom_id,
                            'reference'=>$no_smb,
                            'scan'=>$log,
                            'picking_id'=>$picking_id,
                            'company_id'=>1 );
                

                //Uncomment disini
                $new_picking_id = $models->execute_kw($db, $uid, $password,'stock.move.line', 'create', 
                                    array($new_sml));

                $move_line_ids[] =$new_picking_id;
                
                $update_sml = $models->execute_kw($db, $uid, $password, 'stock.move.line', 'write', array(array($picking_id), array('move_line_ids'=>$move_line_ids)));            
                //Sampai sini 
                $current_time = date('Y-m-d H:i:s',time());
                $update_log = pg_query_params(" UPDATE log_fro_mrp_detail SET timestamp = $1, log_odoo = 'create', keterangan = 'Batch tidak sama dengan reserve, Log FRO adalah manual' WHERE id = $2",[$current_time,$id]);

                // if ($update_sml == 1 ){
                //     print_r('success');
                //     $current_time = date('Y-m-d H:i:s',time());
                //     $update_log = pg_query_params(" UPDATE log_fro_mrp_detail SET timestamp = $1, log_odoo = 'create', keterangan = 'Batch tidak sama dengan reserve, Log FRO adalah manual' WHERE id = $2",[$current_time,$id]);
                // }else{
                //     print_r('failed');
                // }          
                
            }

            // print_r('<pre>');
            // print_r($kode_bb." : UPDATE, BATCH BEDA, BATCH ADA DI ODOO");
            
            



            // print_r('new_picking_id');
            // print_r($new_picking_id);
            // print_r('<br>');

            // print_r($move_line_ids);
            // print_r('<br>');
            // print_r('insert');

            // exit();
        }
    //Jika lot odoo tidak ditemukan maka create data baru
        // Insert data baru berisi 
                // Kode Item, 
                // Location Destination, 
                // no. SMB, 
                // picking_id, 
                // company_id, 
                // Lot FRO, 
                // qty Done, 
                // UOM, 
                // Log scan
    }else{
        $stock_picking = $models->execute_kw($db, $uid, $password, 'stock.picking', 'search',
            array(array(array('name', '=', $no_smb))),
            array('limit'=>1));
        if (!empty($stock_picking)){
            $picking_id = $stock_picking[0];
            // print_r('<pre>');
            // print_r('Picking ID : ');
            // print_r($picking_id);

        }else{
            // print_r('<pre>');
            // print_r($kode_bb." : SKIP, STOCK PICKING NOT FOUND");
            $current_time = date('Y-m-d H:i:s',time());
            $update_log = pg_query_params(" UPDATE log_fro_mrp_detail SET timestamp = $1, log_odoo = 'skip', keterangan = 'Stock picking tidak ditemukan' WHERE id = $2",[$current_time,$id]);
            continue;
        }
        // print_r('picking_id : ');
        // print_r($picking_id);

        $smb = $models->execute_kw($db, $uid, $password, 'stock.picking', 'read', array($picking_id), array('fields'=>array('move_line_ids','location_id','location_dest_id')));
        // print_r('<pre>');
        // print_r($smb);

        $location_id = $smb[0]['location_id'][0];
        $location_dest_id = $smb[0]['location_dest_id'][0];
        $move_line_ids = $smb[0]['move_line_ids'];

        $new_sml = array('product_id'=>$product_id,
                    'location_id'=>$location_id,
                    'location_dest_id'=>$location_dest_id,
                    'lot_fro'=>$nobatch,
                    'qty_done'=>$realisasi,
                    'product_uom_id'=>$uom_id,
                    'reference'=>$no_smb,
                    'scan'=>$log,
                    'picking_id'=>$picking_id,
                    'company_id'=>1 );

        // print_r('<pre>');
        // print_r($kode_bb." : CREATE, BATCH TIDAK ADA DI ODOO");

        //Uncomment disini
        $new_picking_id = $models->execute_kw($db, $uid, $password,'stock.move.line', 'create', 
                            array($new_sml));

        $move_line_ids[] =$new_picking_id;
        
        $update_sml = $models->execute_kw($db, $uid, $password, 'stock.move.line', 'write', array(array($picking_id), array('move_line_ids'=>$move_line_ids)));     

        // print_r('<pre>');       
        // print_r('PICKING ID : ');       
        // print_r($picking_id);       
        // print_r($update_sml);       
        //Sampai sini
        $current_time = date('Y-m-d H:i:s',time());
        $update_log = pg_query_params(" UPDATE log_fro_mrp_detail SET timestamp = $1, log_odoo = 'create', keterangan = 'Batch FRO tidak ada di Odoo' WHERE id = $2",[$current_time,$id]);
        
        // if ($update_sml == 1 ){
        //     print_r('success');
        //     $current_time = date('Y-m-d H:i:s',time());
        //     $update_log = pg_query_params(" UPDATE log_fro_mrp_detail SET timestamp = $1, log_odoo = 'create', keterangan = 'Batch FRO tidak ada di Odoo' WHERE id = $2",[$current_time,$id]);
        // }else{
        //     print_r('failed');
        // }
        // print_r('new_picking_id');
        // print_r($new_picking_id);
        // print_r('<br>');

        // print_r($move_line_ids);
        // print_r('<br>');
        // print_r('insert');

        // exit();
    }


    // print_r('lot');
    // print_r($lot_id);
    // print_r('<br>');


}

$current_time = date('Y-m-d H:i:s',time());
$update_log = pg_query_params(" UPDATE log_fro_mrp SET last_updated = $1, status = 'done' WHERE nomo = $2",[$current_time,$nomo]);

exit();


        
    