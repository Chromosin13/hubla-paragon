<?php
// Ripcord can be cloned from https://github.com/poef/ripcord
require_once('ripcord/ripcord.php');

// Login information
$url = 'http://10.3.0.62:8069';
$url_auth = $url . '/xmlrp/common';
$url_exec = $url . '/xmlrpc/object';

$db = 'testing_new';
$username = 'admin';
$password = '1';

//$url = 'https://odoo.pti-cosmetics.com';
//$url_auth = $url . '/xmlrp/common';
//$url_exec = $url . '/xmlrpc/object';
//
//$db = 'paragon';
//$username = 'admin';
//$password = 'c4ca4238a0';

// Login
$common = ripcord::client("$url/xmlrpc/common");
//$common = ripcord::client($url_auth);
$uid = $common->authenticate($db, $username, $password, array());

//print("<p>Your current user id is '${uid}'</p>");

$models = ripcord::client($url_exec);

include "config.php";
$sql = "SELECT so.id as id,
               so.partner_id,
               so.no_sales_order,
               p.dc,
               p.sales_id,
               so.write_date,
               p.partner_name,
               p.sales_id
        FROM sales_order so
        INNER JOIN partner p on p.partner_id = so.partner_id
        WHERE so.status_odoo = 'new'
       ";

$exec = pg_query($sql);

while ($row = pg_fetch_array($exec)){

    $query = pg_query(
        "SELECT sod.product_id, sod.qty_sales, p.name
                       FROM sales_order_detail sod
                       INNER JOIN product p on p.id = sod.product_id
                       WHERE sales_order_id = ".$row['id']." AND qty_sales != 0"
    );

    while ($data = pg_fetch_array($query)) {

//    $produk = [];
//    foreach ($data as $item){
//        array_push(
//            $produk, array(
//                0,false, array
//                ('product_id' => $item['product_id'], 'product_uom_qty' => $item['qty_sales']))
//        );
//        echo "product_id = ".$item['product_id']." qty = ". $item['qty_sales']."<br>";
//    }

        $id = $models->execute_kw($db, $uid, $password,
            'sale.order', 'create',
            array(array(
                'partner_id' => $row['partner_id'],
                'order_method' => 'Salesperson',
                'client_order_ref' => $row['no_sales_order'],
                'dc_id' => $row['dc'],
                'partner_invoice_id' => $row['partner_id'],
                'partner_shipping_id' => $row['partner_id'],
                'warehouse_id' => $row['dc'],
                'sales_id' => $row['sales_id'],
                'allow_sales' => true,
                'allow_finance' => false,
                'order_line' => [array(
                    0, false, array(
                        'product_id' => $data['product_id'],
                        'product_uom' => 21,
                        'name' => $data['name'],
                        'product_uom_qty' => $data['qty_sales'])
                )]
            )));

        print_r($id);

//    $id = $models->execute_kw($db, $uid, $password,
//        'sale.order', 'create',
//        array(array(
//            'partner_id'=>72447,
//            'order_method'=> 1,
//            'client_order_ref' => 'PO/72447/000079',
//            'dc_id' => 12,
//            'partner_invoice_id' => 72447,
//            'partner_shipping_id' => 72447,
//            'warehouse_id' => 12,
//            'sales_id' => 215,
//            'allow_sales' => true,
//            'allow_finance' => false,
//            'order_line'=> [array(
//                                0,false, array(
//                                    'product_id' => 1957,
//                                    'product_uom' => 21,
//                                    'name' => 'Wardah C-Defense Face Mist 55 ml (pc(s))',
//                                    'product_uom_qty' => 12)
//                                )]
//        )));
//
//    print_r($id);
    }
}


//$customer = $models->execute_kw(
//    $db,
//    $uid,
//    $password,
//    'sale.order',
//    'search_read', // Note the different function here, so we don't have to search AND read
//    array( // Search domain
//        array(
//            array('id', '=', 316414), // Query condition
//        ),
//        array('fields'=> array('id', 'origin', 'name'))
//    ));


