$('#masterdatanetto-koitem').change(function(){
    var koitem = $('#masterdatanetto-koitem').val();
    $.get('index.php?r=master-data-netto/get-netto-before',{koitem:koitem},function(data){
    	var data = $.parseJSON(data);
    	$('#masterdatanetto-nama_item').attr('value', data.nama_item);
    	$('#masterdatanetto-isi_koli').attr('value', data.isi_koli);
    	$('#masterdatanetto-netto_min').attr('value', data.netto_min);
    	$('#masterdatanetto-netto_max').attr('value', data.netto_max);
    });
});

$('[id^="mulai"]').hide();

//$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
      $('input[id^="adminlog-"][id$="-no_batch"]').each(function() {
        var d = new Date();
        //var value = d.getFullYear().toString().slice(-2) + (d.getMonth() + 1).toString() + d.getDate().toString();
        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = d.getFullYear().toString().slice(-2) +
        (month<10 ? '0' : '') + month +
        (day<10 ? '0' : '') + day;
        $(this).val(output);
      });


      $('input[name^="Adminlog["]').change(function() {

        var prefix = $(this).attr('id').split("-");
        prefix.splice(-1, 1);
        var sup_id = '#' + prefix.join('-') + '-supplier';
        var item_code_id = '#' + prefix.join('-') + '-kode_item';
        var item_name_id = '#' + prefix.join('-') + '-nama_item';
        var no_po = $(this).val();

          $.get('index.php?r=adminlog/get-po',{ no_po : no_po }, function(data){
            var data = $.parseJSON(data);


              // $(item_code_id).empty().append($('<option>', {
              //     value:'',
              //     text:'Select...'
              // }));

              $.each(data, function(idx, value) {
                $(item_code_id).append($('<option>', {
                  value: value['kode_item'],
                  text: value['kode_item']
                }));
              });

          });
      });

    $('input[name^="Adminlog["]').change(function() {
      var prefix = $(this).attr('id').split("-");
      prefix.splice(-1, 1);
      var sup_id = '#' + prefix.join('-') + '-supplier';
      var item_code_id = '#' + prefix.join('-') + '-kode_item';
      var item_name_id = '#' + prefix.join('-') + '-nama_item';
      var no_po = $(this).val();
      $(item_code_id).change(function(){

        var kode_item = $(this).val();
          $.get('index.php?r=adminlog/get-po',{ no_po: no_po, kode_item : kode_item }, function(data){
            var data = $.parseJSON(data);

            $.each(data, function(idx, value) {
              $(sup_id).val(value['supplier']);
              $(item_name_id).val(value['nama_pabrik']);
            });
          });
      });
    });

        // $('input[name^="Adminlog["]').change(function() {

        //   if ($(this).attr('name').endsWith('no_batch]')) {

        //     var prefix = $(this).attr('id').split("-");
        //     // prefix.splice(-1, 1);
        //     // var batch_id = '#' + prefix.join('-') + '-no_batch';
        //     // var no_batch = $(this).val();

        //     $(batch_id).val(value['no_batch']);
        //   }
        // });

//});



// $(".dynamicform_wrapper").on('afterInsert', function(e, item) {
//     var datePickers = $(this).find('[data-krajee-kvdatepicker]');
//     datePickers.each(function(index, el) {
//         $(this).parent().removeData().kvDatepicker('remove');
//         $(this).parent().kvDatepicker(eval($(this).attr('data-krajee-kvdatepicker')));
//     });
// });

    // $('select[name^="Adminlog["]').change(function() {
    //         var prefix = $(this).attr('id').split("-");
    //         prefix.splice(-1, 1);
    //         var sup_id = '#' + prefix.join('-') + '-supplier';
    //         var item_code_id = '#' + prefix.join('-') + '-kode_item';
    //         var item_name_id = '#' + prefix.join('-') + '-nama_item';
    //         var item_batch_id = '#' + prefix.join('-') + '-no_batch';
    //         var no_po = $(this).val();

    //         $(item_batch_id).datepicker({
    //                 yearRange: '-71:+0',
    //                 changeMonth: true,
    //                 changeYear: true,
    //                 showMonthAfterYear: true,
    //                 dateFormat: 'dd-mm-yy',
    //                 minDate:"-71Y",
    //                 maxDate:"+0Y"
    //             });
    //

    //         $(item_batch_id).keydown(function (event) {
    //             event.preventDefault();
    //         })
    // });

$(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
    console.log("beforeInsert");
});

$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    console.log("afterInsert");
});

$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
    if (! confirm("Are you sure you want to delete this item?")) {
        return false;
    }
    return true;
});

$(".dynamicform_wrapper").on("afterDelete", function(e) {
    console.log("Deleted item!");
});

$(".dynamicform_wrapper").on("limitReached", function(e, item) {
    alert("Limit reached");
});


jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title-adminlog").each(function(index) {
        jQuery(this).html("Adminlog: " + (index + 1))
    });
});

jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title-adminlog").each(function(index) {
        jQuery(this).html("Adminlog: " + (index + 1))
    });
});
