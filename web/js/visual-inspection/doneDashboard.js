import * as API from './modules/debugHelperAPI.js';
// import * as Event from "./debugHelperEvents.js";

let inspection_chart;
let tracibility_table;
const ip_address = '192.168.43.43';
// const ip_address = 'localhost';

const sekunder = document.getElementById('jumlah_defect_sekunder_disp');
const total = document.getElementById('jumlah_inspeksi_disp');

let data = {
    url: `http://${ip_address}:8070/fro-v2-visdat_system/web/index.php?r=vi-api%2Fapi-post-inspection-result/`,
    post_data: {
        id: $('#id-inspection').val(),
        period: '10 minutes'
    },
    chart: {
        filter: ['periodic_time_stamp', 'kumulatif_periodik_defect_sekunder'],
    },
    table: {
        filter: ['time_only_stamp', 'kumulatif_periodik_defect_sekunder', 'counter_inspeksi'],
    }

};
let config = {
    chart: {
        type: 'line',
        data: {
            responsive: true,
            maintainAspectRation: true,
            
            labels: data.chart.labels,
            datasets: [{
                label: 'Defect Kemasan Sekunder',
                data: data.chart.values,
                
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 3,
                
                pointRadius: 7,
                pointHoverRadius: 12,
            }]
        },
        options: {
            responsive: true,
            showTooltips: true,
            interaction: {
                intersect: false,
                axis: 'x',
                mode: 'nearest'
            },
            scales: {
                x: {
                    ticks: {
                        callback: function (value) {
                            let x_point = this.getLabelForValue(value);
                            return x_point.substr(11,5);
                        },
                    },
                },
                y: {
                    beginAtZero: true
                }
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        title: (data_point) => {
                            console.log(data_point);
                            return `Defect sekunder pada ${data_point[0].label}`;
                        },
                        label: (data_point) => {
                            const tooltip_point = ` ${data_point.raw}`;
                            return tooltip_point;
                        }
                    }
                }
            }
        }
    },
    table: {
        "dom": 'Blfrtip',
        "responsive": true,
        // serverSide: true,
        // destroy: true,
        "language": {
            "emptyTable": ['Tidak ada Ketidaksesuaian Kemasan Sekunder terhadap Jadwal Kemas ini']
        },
        "ajax": {
            'type': 'POST',
            'url': data.url,
            'data': function(d) {
                d.id = $('#id-inspection').val(),
                d.period = data.post_data.period 
            },
            dataSrc: received_data => {
                return DONE.processTable(received_data, config);
            },
        },
        "columns": [
            {"data": "periodic_time_stamp"},
            {"data": "kumulatif_periodik_defect_sekunder"},
            {"data": "counter_inspeksi"},
            {"data": "jenis_defect"},
        ],
        "buttons": [
            {
                extend: "collection",
                text: "Export Table",
                className: 'btn btn-primary',
                buttons:[
                    {
                        extend: 'copy',
                        title: `inspect_result_${$('#snfg-inspection').val()}`
                    },
                    {
                        extend: 'excel',
                        title: `inspect_result_${$('#snfg-inspection').val()}`
                    },
                    {
                        extend: 'csv',
                        title: `inspect_result_${$('#snfg-inspection').val()}`
                    },
                    {
                        extend: 'pdf',
                        title: `inspect_result_${$('#snfg-inspection').val()}`
                    },
                    {
                        extend: 'print',
                        title: `inspect_result_${$('#snfg-inspection').val()}`
                    }
                ],
                exportOptions: {
                    columns: ':visible'
                },
            },
            {
                extend: 'colvis',
                text: "Pilih Kolom",
                className: 'btn btn-primary',
                columns: ':not(.noVis)'
            },
        ],
    },
};

const DONE = (() => {
    const reducer = (previous_value, current_value) => previous_value + current_value;

    function processChart(data, setup) {
        const chart_data = API.filteredData(data, setup.chart.filter);
        // console.log(chart_data);

        config.chart.data.labels = chart_data.periodic_time_stamp;
        // console.log(chart_data.kumulatif_periodik_defect_sekunder.reduce(reducer));
        $('#jumlah-defect-sekunder-disp').text(chart_data.kumulatif_periodik_defect_sekunder.reduce(reducer));
        config.chart.data.datasets[0].data = chart_data.kumulatif_periodik_defect_sekunder;
        inspection_chart.update();
    }

    function processTable(data, config) {
        // const table_data = API.filteredData(data, config.table.filter);
        const table_data = API.cleanData(data);
        // console.log(table_data);
        // console.log(table_data[table_data.length-1].counter_inspeksi);
        $('#jumlah-inspeksi-disp').text(table_data[table_data.length-1].counter_inspeksi);
        // console.log(table_data);
        return table_data;
    }

    async function process(config) {
        const fetched_data = await API.fetchPostData(config.url, config.post_data);
        processChart(fetched_data, config);
    }

    return {
        process, processTable
    }
})();

const chart_period_buttons = document.querySelectorAll('.set-period');
Array.from(chart_period_buttons).forEach(element => {
    element.addEventListener('click', () => {
        data.post_data.period = element.name;
        DONE.process(data);
        
        config.table.ajax.data.period = element.name;
        console.log(config.table);
        tracibility_table.ajax.reload(null, false);

        $("#dropdown-menu").text(element.innerText);
    })
});

const chart_type_buttons = document.querySelectorAll('.chart-type');
Array.from(chart_type_buttons).forEach(element => {
    element.addEventListener('click', () => {
        config.chart.type = element.name;
        // DONE.processGraph(api_url.api_post_inspection_result, post_data, chart);
        inspection_chart.update();
        tracibility_table.ajax.reload(null,false);
    })
});

$(document).ready(() => {
    const ctx = document.getElementById("current-chart").getContext("2d");
    inspection_chart = new Chart(ctx, config.chart);
    tracibility_table = $('#tracibility-table').DataTable(config.table);
    
    console.log(data.post_data);
    tracibility_table.ajax.reload(null, false);
    DONE.process(data);
})



