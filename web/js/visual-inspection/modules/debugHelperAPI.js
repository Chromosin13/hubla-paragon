function cleanData(data) {
    return data.filter(data_point => !Object.values(data_point).includes(null));
};

function mapData(array_object, keys) {
    let result = {};
    keys.forEach(key => {
        result[key] = array_object.map(obj => obj[key]);
    });
    return result
}

function filteredData(data, keys) {
    let result = cleanData(data);
    result = mapData(result, keys);
    return result;
}

async function fetchPostData(url, post_data) {
    let result;
    try {
        const response = await fetch(url, {
            mode: "cors",
            method: "POST",
            body: JSON.stringify(post_data),
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json; charset=UTF-8'
            },
        }).then(response => {
            result = response.json();
        });
    } catch (error) {
        result = error;
    }
    return result;
}


export {filteredData, fetchPostData, cleanData, mapData};
