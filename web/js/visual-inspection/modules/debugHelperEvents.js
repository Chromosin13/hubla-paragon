function chartType(component, type) {
    Array.from(component).forEach(element => {
        element.addEventListener('click', () => {
            type = element.name;
            DONE.processGraph(api_url.api_post_inspection_result, post_data, chart);
            inspection_chart.update();
            tracibility_table.ajax.reload(null,false);
        })
    });
}

export {
    chartType,
    setPeriod
};