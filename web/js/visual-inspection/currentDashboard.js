import * as API from './modules/debugHelperAPI.js';
// import * as Event from "./debugHelperEvents.js";

let inspection_chart;
let tracibility_table;
const ip_address = '192.168.43.43';
// const ip_address = 'localhost';

// const sekunder = document.getElementById('jumlah_defect_sekunder_disp');
// const total = document.getElementById('jumlah_inspeksi_disp');

let data = {
    url: `http://${ip_address}:8070/fro-v2-visdat_system/web/index.php?r=vi-api%2Fapi-post-inspection-result/`,
    post_data: {
        id: $('#id-inspection').val(),
        period: '10 minutes'
    },
    chart: {
        filter: ['periodic_time_stamp', 'kumulatif_periodik_defect_sekunder'],
    },
    table: {
        filter: ['time_only_stamp', 'kumulatif_periodik_defect_sekunder', 'counter_inspeksi', 'jenis_defect'],
    }

};
let config = {
    chart: {
        type: 'line',
        data: {
            responsive: true,
            maintainAspectRation: true,
            
            labels: data.chart.labels,
            datasets: [{
                label: 'Defect Kemasan Sekunder',
                data: data.chart.values,
                
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 3,
                pointRadius: 7,
                pointHoverRadius: 12,
            }]
        },
        options: {
            responsive: true,
            showTooltips: true,
            interaction: {
                intersect: false,
                axis: 'x',
                mode: 'nearest'
            },
            scales: {
                x: {
                    ticks: {
                        callback: function (value) {
                            let x_point = this.getLabelForValue(value);
                            return x_point.substr(11,5);
                        },
                    },
                },
                y: {
                    beginAtZero: true
                }
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        title: (data_point) => {
                            return `Defect sekunder pada ${data_point[0].label}`;
                        },
                        label: (data_point) => {
                            const tooltip_point = ` ${data_point.raw}`;
                            return tooltip_point;
                        }
                    }
                }
            }
        }
    },
    table: {
        "dom": 'Blfrtip',
        "responsive": true,
        // serverSide: true,
        // destroy: true,
        "language": {
            "emptyTable": ['Tidak ada Ketidaksesuaian Kemasan Sekunder terhadap Jadwal Kemas ini']
        },
        "ajax": {
            'type': 'POST',
            'url': `http://${ip_address}:8070/fro-v2-visdat_system/web/index.php?r=vi-api%2Fapi-post-inspection-result/`,
            'data': function(d) {
                d.id = $('#id-inspection').val();
                d.period = data.post_data.period 
            },
            dataSrc: received_data => {
                console.log(CURRENT.processTable(received_data, config));
                return CURRENT.processTable(received_data, config);
            },
        },
        "columns": [
            {"data": "periodic_time_stamp"},
            {"data": "kumulatif_periodik_defect_sekunder"},
            {"data": "counter_inspeksi"},
            {"data": "jenis_defect"},
        ],
        "buttons": [
            {
                extend: "collection",
                text: "Download",
                className: 'btn btn-primary',
                buttons:[
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ],
                exportOptions: {
                    columns: ':visible'
                },
            },
            {
                extend: 'colvis',
                text: "Pilih Kolom",
                className: 'btn btn-primary',
                columns: ':not(.noVis)'
            },
        ],
    },
};
let inspection_result = {
    all_inspection: 0,
    defect_dusat: 0,
    defect_tube: 0,
}

const CURRENT = (() => {
    const reducer = (previous_value, current_value) => previous_value + current_value;
    
    function processChart(data, setup) {
        const chart_data = API.filteredData(data, setup.chart.filter);
        
        config.chart.data.labels = chart_data.periodic_time_stamp;
        inspection_result.defect_dusat = chart_data.kumulatif_periodik_defect_sekunder.reduce(reducer);
        config.chart.data.datasets[0].data = chart_data.kumulatif_periodik_defect_sekunder;
        
        inspection_chart.update();
    }
    
    function processTable(data, config) {
        // const table_data = API.filteredData(data, config.table.filter);
        const table_data = API.cleanData(data);
        console.log(table_data);
        inspection_result.all_inspection = table_data[table_data.length-1].counter_inspeksi;
        return table_data;
    }
    
    async function process(config) {
        const fetched_data = await API.fetchPostData(config.url, config.post_data);
        
        processChart(fetched_data, config);
        
        $('#jumlah-defect-sekunder-disp').text(inspection_result.defect_dusat);
        $('#jumlah-inspeksi-disp').text(inspection_result.all_inspection);
    }
    
    return {
        process, processTable
    }
})();

const chart_period_buttons = document.querySelectorAll('.set-period');
Array.from(chart_period_buttons).forEach(element => {
    element.addEventListener('click', () => {
        console.log(typeof(element.name));
        data.post_data.period = element.name;

        config.table.ajax.data.period = element.name;

        $("#dropdown-menu").text(element.innerText);
    })
});

const chart_type_buttons = document.querySelectorAll('.chart-type');
Array.from(chart_type_buttons).forEach(element => {
    element.addEventListener('click', () => {
        config.chart.type = element.name;
        inspection_chart.update();
        // tracibility_table.ajax.reload(null,false);
    })
});

const tracibility_options_buttons = document.querySelectorAll('.tracibility-options');
Array.from(tracibility_options_buttons).forEach(element => {
    element.addEventListener('click', () => {
        console.log(element.innerText);
        if (element.innerText == 'All') {
            config.table.ajax.data.period = element.value;
        } else if (element.innerText == 'Periodic Data') {
            config.table.ajax.data.period = data.post_data.period;
        }
        console.log(config.table.ajax);
        // tracibility_table.ajax.reload(null, false);
    })
})

$(document).ready(() => {
    const ctx = document.getElementById("current-chart").getContext("2d");
    inspection_chart = new Chart(ctx, config.chart);
    tracibility_table = $('#tracibility-table').DataTable(config.table);
    
    setInterval(() => {
        if (data.post_data.period != '2 second') {
            CURRENT.process(data);
        }
        // console.log(tracibility_table);
        tracibility_table.ajax.reload(null, false);
    }, 2000);
})



