import * as API from "./modules/debugHelperAPI.js";

let done_table;
let current_table;
const ip_address = '192.168.43.43';
// const ip_address = 'localhost';

let table = {
    done: {
        url: `http://${ip_address}:8070/fro-v2-visdat_system/web/index.php?r=vi-api/api-get-all-historic-schedule`,
        // url: `/index.php?r=vi-api/api-get-all-historic-schedule`,
        filter: [
            'snfg', 
            'nama_line', 
            'datetime_start', 
            'datetime_stop',
            'jumlah_defect_sekunder',
            'inspeksi_sekunder_ke'
        ]
    },
    current: {
        url: `http://${ip_address}:8070/fro-v2-visdat_system/web/index.php?r=vi-api/api-get-running-schedule&line=`,
        // url: `http://${ip_address}:8070/fro-v2-visdat_system/web/index.php?r=vi-api/api-get-running-schedule&line=`,
        post_data: {temp_id: 0},
        line: [
            'TUP06', 'TUP07',
            'TUP08', 'TUP09'
        ],
        filter: [
            'id',
            'snfg',
            'fg_name_odoo',
            'nama_line',
            'datetime_start'
        ],
        data: []
    }
}

const table_config = {
    done: {
        dom: 'Bfrtip',    
        responsive: true,
        columnDefs: [
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 10001, targets: 4 },
            { responsivePriority: 2, targets: -2 }
        ],
        language: {
            "emptyTable": ['Tidak Hasil Jadwal Kemas Hasil Inspeksi']
        },
        ajax: {
            'type': 'GET',
            'url': table.done.url,
            'dataSrc': received_data => {
                console.log(received_data);
                return received_data;
            },
        },
        columns: [
            {"data": "snfg"},
            {"data": "fg_name_odoo"},
            {"data": "nama_line"},
            {"data": "datetime_start"},
            {"data": "datetime_stop"},
            {"data": "jumlah_defect_sekunder"},
            {"data": "inspeksi_sekunder_ke"},
            {"data": "defect_rate"},

            {
                "data": function (data, type, row, meta) {
                    return `<a type="button" class="btn btn-primary" href="http://${ip_address}:8070/fro-v2-visdat_system/web/index.php?r=vi-dashboard%2Finspect&param1=done&param2=${data.id}">View</a>`;
                }
            }
        ],
        buttons: [
            {
                extend: 'collection',
                text: 'Export Table',
                className: 'btn-primary',
                buttons:[
                    'copy',
                    'csv',
                    'pdf',
                    'print'
                ],
                exportOptions: {
                    columns: ':visible'
                },
            },
            {
                extend: 'colvis',
                text: "Pilih Kolom",
                className: 'btn-primary',
                columns: ':not(.noVis)'
            },
        ],
    },
    current: {
        dom: 'Bfrtip',    
        responsive: true,
        columnDefs: [
            { responsivePriority: 5 },
            { responsivePriority: 4 },
            { responsivePriority: 3 },
            { responsivePriority: 2 },
            { responsivePriority: 1 }
        ],
        language: {
            emptyTable: ['Tidak Hasil Jadwal Kemas Hasil Inspeksi']
        },
        columns: [
            {data: "snfg"},
            {data: "fg_name_odoo"},
            {data: "nama_line"},
            {data: "datetime_start"},

            {
                data: function (data, type, row, meta) {
                    console.log(data);
                    return `<a type="button" class="btn btn-primary" href="http://${ip_address}:8070/fro-v2-visdat_system/web/index.php?r=vi-dashboard%2Finspect&param1=current&param2=${data.id}">View</a>`;
                }
            },
        ],
        buttons: [
            {
                extend: "collection",
                text: "Export Table",
                className: 'btn-primary',
                buttons:[
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ],
                exportOptions: {
                    columns: ':visible'
                },
            },
            {
                extend: 'colvis',
                text: "Pilih Kolom",
                className: 'btn-primary',
                columns: ':not(.noVis)'
            },
        ],
    }
}

const NAVIGATION = (() => {  
    function filterData(obj, keys) {
        let result = {};
        keys.forEach(key => {
            result[key] = obj[key];
        })
        
        return result;
    }
    
    function mapData(data, filter) {
        let result = [];
        data.forEach(element => {
            result.push(filterData(element.schedule, filter));
        });
        return result;
    }

    function cleanData(data) {
        return data.filter((data_point) => data_point.temp_id !== 0);
    }
    
    async function tableData (config) {
        let datas = [];
        for (let i = 0; i < config.current.line.length; i++) {
            const current_url = config.current.url + config.current.line[i];
            const data = await API.fetchPostData(current_url, config.current.post_data);
            console.log(data);
            datas.push(data);
        }
        datas = cleanData(datas);
        console.log(datas);
        datas = mapData(datas, config.current.filter);
        console.log(datas);

        current_table.clear();
        current_table.rows.add(datas);
        current_table.draw();

        done_table.ajax.reload(null, false);

    };

    return {
        tableData,
    }
})();

(async() => {
    await NAVIGATION.tableData(table);
})();

$(document).ready(() => {
    done_table = $('#historic-table').DataTable(table_config.done);
    current_table = $('#current-table').DataTable(table_config.current);
    setInterval(async (event) => {
        await NAVIGATION.tableData(table);
    }, 5000);
});