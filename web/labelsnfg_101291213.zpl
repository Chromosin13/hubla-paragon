
            ^FX Begin setup
            ^XA
            ~TA120
            ~JSN
            ^LT0
            ^MNW
            ^MTT
            ^PON
            ^PMN
            ^LH0,0
            ^JMA
            ^PR4,4
            ~SD25
            ^JUS
            ^LRN
            ^CI0
            ^MD0
            ^CI13,196,36
            ^XZ
            ^FX End setup

            ^FX Begin label format
            ^XA
            ^MMT
            ^LL0264
            ^PW583
            ^LS0

            ^FX Line Vertical
            ^FO400,20
            ^GB0,190,2^FS

            ^FX Line Vertical
            ^FO150,210
            ^GB0,45,2^FS

            ^FX Line Vertical
            ^FO260,210
            ^GB0,140,2^FS

            ^FX Line Vertical
            ^FO340,255
            ^GB0,50,2^FS

            ^FX Line Vertical
            ^FO420,210
            ^GB0,140,2^FS

            ^FX Line Horizontal First
            ^FO20,70
            ^GB380,0,2^FS

            ^FX Line Horizontal
            ^FO20,160
            ^GB380,0,2^FS

            ^FX Line Horizontal
            ^FO20,210
            ^GB560,0,2^FS

            ^FX Line Horizontal
            ^FO20,255
            ^GB560,0,2^FS

            ^FX Line Horizontal
            ^FO260,305
            ^GB160,0,2^FS

            ^FX Nama Status
            ^FO20,15
            ^ARN,16
            ^FX Field block 507 dots wide, 2 lines max
            ^FB400,2,,C,
            ^FDRELEASE^FS

            ^FX Keterangan Pending
            ^FO20,45
            ^ALN,20,20
            ^FX Field block 507 dots wide, 2 lines max
            ^FB400,2,,C,
            ^FD ^FS

            ^FX QR kode BB
            ^FO415,5,0
            ^BQN,2,5,H,7
            ^FDMA,B-BSLP-EM/G0108@2@30@1080@02083^FS
       
            ^FX Nama Item Label
            ^FO30,78
            ^ADN5,5
            ^FDNama Item^FS

            ^FX Nama Item Value
            ^FO20,100
            ^A0N10,20
            ^FX Field block 507 dots wide, 3 lines max
            ^FB380,3,,C,
            ^FDEmina Bright Stuff Loose Powder 55 g^FS

            ^FX SNFG Label
            ^FO20,167
            ^ABN5,5
            ^FD SNFG^FS

            ^FX SNFG Value
            ^FO38,185
            ^ADN10,20
            ^FX Field block 507 dots wide, 2 lines max
            ^FB350,1,,C,
            ^FDB-BSLP-EM/G0108^FS

            ^FX Qty Koli Label
            ^FO265,260
            ^ABN5,5
            ^FDQty.Koli^FS

            ^FX Qty Koli Value
            ^FO265,280
            ^AQN10,10
            ^FX Field block 507 dots wide, 1 lines max
            ^FB70,1,,C,
            ^FD30^FS

           ^FX Inspektor QC Label
            ^FO270,215
            ^ABN5,5
            ^FDQCFG^FS

            ^FX Inspektor QC Value
            ^FO270,233
            ^A0N5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB140,1,,C,
            ^FDIYNG^FS

            ^FX Kode Odoo Label
            ^FO160,215
            ^ABN5,5
            ^FDKode Odoo^FS

            ^FX Kode Odoo Value
            ^FO160,233
            ^ACN5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB100,1,,C,
            ^FD02083^FS

            ^FX Exp Date Label
            ^FO430,215
            ^ABN5,5
            ^FDExp.Date^FS

            ^FX Exp Date Value
            ^FO430,233
            ^ACN5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB150,1,,C,
            ^FD2024-05-25^FS

            ^FX No.Urut Label
            ^FO345,260
            ^ABN5,5
            ^FDNo.Urut^FS

            ^FX No.Urut Value
            ^FO343,280
            ^AJN5,20
            ^FX Field block 507 dots wide, 1 lines max
            ^FB70,1,,C,
            ^FD2^FS

            ^FX No.Batch Label
            ^FO20,215
            ^ABN5,5
            ^FD No. Batch^FS

            ^FX No.Batch Value
            ^FO28,232
            ^ACN10,20
            ^FX Field block 507 dots wide, 2 lines max
            ^FB110,1,,C,
            ^FDEH25A^FS

           ^FX Qty per palet Title
           ^FO430,260
           ^ABN5,5
           ^FX Field block 507 dots wide, 3 lines max
           ^FB150,1,,C
           ^FDQty per Palet^FS

           ^FX Qty per Palet human readable
           ^FO425,285
           ^APN,75,75
           ^FX Field block 507 dots wide, 3 lines max
           ^FB160,1,,C
           ^FD1080^FS

            ^FX Keterangan Label
            ^FO30,263
            ^ABN5,5
            ^FDKeterangan^FS

            ^FX Keterangan Value
            ^FO30,282
            ^ACN10,10
            ^FX Field block 507 dots wide, 2 lines max
            ^FB230,2,,L,
            ^FD ^FS                     

            ^FX Alokasi Label
            ^FO270,310
            ^ABN5,5
            ^FDAlokasi^FS

            ^FX Alokasi Value
            ^FO270,325
            ^ACN10,19
            ^FX Field block 507 dots wide, 1 lines max
            ^FB150,2,,C,
            ^FDNDC^FS

            ^FX Print quantity
            ^PQ1,0,1,Y
            ^FX End label format
            ^XZ
            