#### [04/09/2019-CR-MES : Tracking SO Overdue Status & Details of Rework]

#### Date of Creation : 4th October 2019

##### Commit : 69599128

##### Developer : 
|Name | Division |
|:--|:--|
|Redha Hari W | IT |

##### Stakeholders :
|Name | Division |
|:--|:--|
|Indita Ramasaphira | SCM|
|Mety Dora Nuramalia | SCM|

##### Current State :

Tracking SO in MES Plaftorm without Overdue or Rework information Per SNFG

##### Desired State :

Tracking SO in MES Platform with Overdue or Rework Details information Per SNFG

##### Technical Implementation :

1. Database Layer

Create View for Karantina Finish Good Summary


|Parameter | Name |
|:--|:--|
|Database | flowreport |
|View | summary_karantina_qc_fg_v2 |

```sql
SELECT
  	snfg,
  	min(coalesce(timestamp_terima_karantina,null)) as first_terima_karantina,
  	max(coalesce(timestamp_terima_karantina,null)) as latest_terima_karantina,
  	sum(case when timestamp_terima_karantina is not null then 1 else 0 
  	end) as count_terima_karantina,
  	sum(coalesce(is_done,0)) as count_koli_is_done,
  	count(id) as jumlah_koli,
	case when sum(coalesce(is_done,0)) = count(id) then 1 else 0 end as snfg_is_done 
FROM
	  qc_fg_v2
GROUP BY snfg 
 ```
 Output
|column | description |
|:--|:--|
|snfg | schedule number serial number finish good|
|first_terima_karantina | waktu penerimaan koli pertama pada proses karantina|
|latest_terima_karantina | waktu penerimaan koli paling akhir pada proses karantina|
|count_terima_karantina | jumlah berapa koli yang diterima di karantina|
|count_koli_is_done | jumlah berapa koli yang sudah is done|
|jumlah_koli | jumlah koli total|
|snfg_is_done | schedule number serial number finish good|

Change Tracking SO Materialized View Query
```sql
SELECT 
        c.snfg,
        c.week,
        c.sediaan,
        c.streamline,
        c.start,
        c.due,
        c.leadtime,
        c.nama_fg,
        c.tglpermintaan,
        c.mpq,
        c.qty_in_process,
        c.qty_karantina,
        c.qty_ndc,
        c.timestamp_complete,
        c.qty_total,
        c.status,
        c."timestamp",
        split_part(c.snfg::text, '/'::text, 1) AS koitem_fg,
        c.status_ndc,
        c.qty_sample,
        c.qty_reject,
        c.qty_ndc_release,
        c.qty_ndc_pending_mikro,
        case when count_koli_is_done=jumlah_koli then 'Complete Koli' 
             when count_koli_is_done>0 and count_koli_is_done<jumlah_koli then 'Partial Koli'
             when coalesce(count_koli_is_done,0)=0 then 'Koli Belum Diterima Karantina'
        end as status_koli_karantina,
        case when skq.first_terima_karantina::date<=c.due then 'Ontime' else 'Overdue'
        end as status_terima_karantina,
        skq.first_terima_karantina,
        skq.latest_terima_karantina
FROM ( SELECT b.snfg,
            b.week,
            b.sediaan,
            b.streamline,
            b.start,
            b.due,
            b.leadtime,
            b.nama_fg,
            b.tglpermintaan,
            b.mpq,
            b.qty_in_process,
            b.qty_karantina,
            b.qty_ndc,
            b.qty_ndc_release,
            b.qty_ndc_pending_mikro,
            b.timestamp_complete,
            b.qty_in_process + b.qty_karantina + b.qty_ndc AS qty_total,
                CASE
                    WHEN b.timestamp_complete IS NOT NULL THEN 'Complete QCFG'::text
                    ELSE 'In Progress'::text
                END AS status,
                CASE
                    WHEN b.timestamp_complete IS NULL THEN b."timestamp"
                    ELSE b.timestamp_complete
                END AS "timestamp",
                CASE
                    WHEN b.qty_karantina <= 0::numeric AND b.qty_ndc_release > 0::numeric THEN 'Complete NDC'::text
                    WHEN b.qty_karantina > 0::numeric AND b.qty_ndc_release > 0::numeric THEN 'Partial NDC'::text
                    ELSE 'In Progress'::text
                END AS status_ndc,
            b.qty_sample,
            b.qty_reject
           FROM ( SELECT a.snfg,
                    a.week,
                    a.sediaan,
                    a.streamline,
                    a.start,
                    a.due,
                    a.leadtime,
                    a.nama_fg,
                    a.tglpermintaan,
                    a.mpq,
                        CASE
                            WHEN q.snfg IS NOT NULL AND COALESCE(q.is_done, 0::bigint) = 0 THEN
                            CASE
                                WHEN (a.mpq - COALESCE(q.qty_karantina, 0::numeric) - COALESCE(q.qty_ndc, 0::numeric)) < 0::numeric THEN 0::numeric
                                ELSE a.mpq - COALESCE(q.qty_karantina, 0::numeric) - COALESCE(q.qty_ndc, 0::numeric)
                            END
                            WHEN q.snfg IS NOT NULL AND COALESCE(q.is_done, 0::bigint) > 0 THEN 0::numeric
                            WHEN COALESCE(q.snfg, ''::character varying)::text = ''::text THEN a.mpq
                            ELSE NULL::numeric
                        END AS qty_in_process,
                    COALESCE(q.qty_karantina, 0::numeric) AS qty_karantina,
                    COALESCE(q.qty_ndc, 0::numeric) AS qty_ndc,
                    COALESCE(q.qty_ndc_release, 0::numeric) AS qty_ndc_release,
                    COALESCE(q.qty_ndc_pending_mikro, 0::numeric) AS qty_ndc_pending_mikro,
                        CASE
                            WHEN COALESCE(q.is_done, 0::bigint) > 0 THEN q."timestamp"
                            ELSE NULL::timestamp without time zone
                        END AS timestamp_complete,
                    a."timestamp",
                    q.qty_sample,
                    q.qty_reject
                   FROM ( SELECT DISTINCT ON (scm_planner.snfg) scm_planner.snfg,
                            scm_planner.week,
                            scm_planner.sediaan,
                            scm_planner.streamline,
                            scm_planner.start,
                            scm_planner.due,
                            scm_planner.leadtime,
                            scm_planner.nama_fg,
                            scm_planner.tglpermintaan,
                            scm_planner.jumlah_pcs AS mpq,
                            scm_planner."timestamp"
                           FROM scm_planner
                          WHERE scm_planner."timestamp" > ('now'::text::date - '60 days'::interval) AND scm_planner.line_end::text = 'QC FG'::text AND (COALESCE(scm_planner.status, ''::character varying)::text <> ALL (ARRAY['HOLD'::text, 'CANCEL'::text]))
                          ORDER BY scm_planner.snfg, (COALESCE(scm_planner.jumlah_pcs, 0::numeric)) DESC) a
                     LEFT JOIN ( SELECT qc_fg_v2.snfg,
                            sum(
                                CASE
                                    WHEN qc_fg_v2.status::text = ANY (ARRAY['PROSES KARANTINA'::character varying::text, 'SIAP KIRIM KE NDC'::character varying::text]) THEN qc_fg_v2.qty_karantina
                                    ELSE 0::numeric
                                END) AS qty_karantina,
                            sum(
                                CASE
                                    WHEN qc_fg_v2.status::text = 'SUDAH DITERIMA NDC'::text THEN qc_fg_v2.qty_karantina
                                    ELSE 0::numeric
                                END) AS qty_ndc,
                            sum(
                                CASE
                                    WHEN qc_fg_v2.status::text = 'SUDAH DITERIMA NDC'::text AND qc_fg_v2.status_check::text = 'RELEASE'::text THEN qc_fg_v2.qty_karantina
                                    ELSE 0::numeric
                                END) AS qty_ndc_release,
                            sum(
                                CASE
                                    WHEN qc_fg_v2.status::text = 'SUDAH DITERIMA NDC'::text AND (qc_fg_v2.status_check::text = ANY (ARRAY['PENDING'::character varying::text, 'CEK MIKRO'::character varying::text])) THEN qc_fg_v2.qty_karantina
                                    ELSE 0::numeric
                                END) AS qty_ndc_pending_mikro,
                            sum(COALESCE(qc_fg_v2.is_done, 0)) AS is_done,
                            max(qc_fg_v2."timestamp") AS "timestamp",
                            sum(COALESCE(qc_fg_v2.qty_sample, 0::numeric)) AS qty_sample,
                            sum(COALESCE(qc_fg_v2.qty_reject, 0::numeric)) AS qty_reject
                           FROM qc_fg_v2
                          GROUP BY qc_fg_v2.snfg) q ON a.snfg::text = q.snfg::text) b
     ) c
LEFT JOIN summary_karantina_qc_fg_v2 skq on skq.snfg = c.snfg
ORDER BY c."timestamp" DESC;
```

2. Web App Layer

Add new columns on model, searchmodel, and index :
- models/TrackingSo
- models/TrackingSoSearch
- views/tracking-so/index.php


Details of changes can be tracked on flowreport_production master branch with commit number 69599128

##### Deployment :
Pull from commit number #69599128
5th October 2019 12:54 by Redha



