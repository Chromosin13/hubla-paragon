<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests/codeception');

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
               // 'admin' => [
               //      'class' => 'mdm\admin\Module',
               //  ],
               'gridview' =>  [
                    'class' => '\kartik\grid\Module'
                    // enter optional module parameters below - only if you need to  
                    // use your own export download action or custom translation 
                    // message source
                    // 'downloadAction' => 'gridview/export/download',
                    // 'i18n' => []
                ]
    ],
    'components' => [
        // 'authManager' => [
        //     'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\DbManager'
        // ],
        // 'session' => [
        //     'class' => '\yii\web\Session',
        //     // 'name' => 'production',
        // ],
        // 'request' => [
        //     // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
        //     'cookieValidationKey' => 'ptidotnet',
        // ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'app\models\User',
            // 'identityClass' => 'app\models\User',
            // 'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            // 'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'db2' => require(__DIR__ . '/db2.php'),
        'db_warehouse' => require(__DIR__ . '/db_warehouse.php'),
        'db_varcos' => require(__DIR__ . '/db_varcos.php'),
        'db_paragon' => require(__DIR__ . '/db_paragon.php'),
        'db_mdm' => require(__DIR__ . '/db_mdm.php'),
        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => '_all-skins',
                ],
            ],
        ],
        // 'assetManager' => [
        //     'bundles' => [
        //         'dmstr\web\AdminLteAsset2' => [
        //             'skin' => 'skin-green',
        //         ],
        //     ],
        // ],
        // 'assetManager' => [
        //     'bundles' => [
        //         'dmstr\web\AdminLteAsset' => [
        //             'skin' => 'skin-green',
        //         ],
        //     ],
        // ],
        /*'view' => [
         'theme' => [
             'pathMap' => [
                '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
             ],
            ],
        ],*/
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
        /*
        'urlManager' => [
        'class' => 'yii\web\UrlManager',
        // Disable index.php
        'showScriptName' => false,
        // Disable r= routes
        'enablePrettyUrl' => true,
        'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
        ),
        ],*/
    ],
    // 'as access' => [
    //     'class' => 'mdm\admin\components\AccessControl',
    //     'allowActions' => [
    //         'site/*',
    //         'admin/*',
    //         'some-controller/some-action',
    //         // The actions listed here will be allowed to everyone including guests.
    //         // So, 'admin/*' should not appear here in the production, of course.
    //         // But in the earlier stages of your development, you may probably want to
    //         // add a lot of actions here until you finally completed setting up rbac,
    //         // otherwise you may not even take a first step.
    //     ]
    // ],
    'params' => $params,
    // 'as beforeRequest' => [  //if guest user access site so, redirect to login page.
    //     'class' => 'yii\filters\AccessControl',
    //     'rules' => [
    //         [
    //             'actions' => ['login', 'error'],
    //             'allow' => true,
    //         ],
    //         [
    //             'allow' => true,
    //             'roles' => ['@'],
    //         ],
    //         ],
    // ],

    /*'as access' => [
        'class' => \yii\filters\AccessControl::className(),//AccessControl::className(),
        'rules' => [
            [
                'actions' => ['login', 'error'],
                'allow' => true,
            ],
            [
                'actions' => ['logout', 'index','dashboard'], // add all actions to take guest to login page
                'allow' => true,
                'roles' => ['@'],
            ],
        ],
    ],
    */
];


// $config = [
//     'id' => 'basic-console',
//     'basePath' => dirname(__DIR__),
//     'bootstrap' => ['log'],
//     'modules' => [
//                'gridview' =>  [
//                     'class' => '\kartik\grid\Module'
//                     // enter optional module parameters below - only if you need to  
//                     // use your own export download action or custom translation 
//                     // message source
//                     // 'downloadAction' => 'gridview/export/download',
//                     // 'i18n' => []
//                 ]
//     ],
//     'controllerNamespace' => 'app\commands',
//     'components' => [
//         'authManager' => [
//             'class' => 'yii\rbac\DbManager',
//         ],
//         'cache' => [
//             'class' => 'yii\caching\FileCache',
//         ],
//         'log' => [
//             'targets' => [
//                 [
//                     'class' => 'yii\log\FileTarget',
//                     'levels' => ['error', 'warning'],
//                 ],
//             ],
//         ],
//         'db' => $db,
//     ],
//     'params' => $params,
//     /*
//     'controllerMap' => [
//         'fixture' => [ // Fixture generation command line.
//             'class' => 'yii\faker\FixtureController',
//         ],
//     ],
//     */
// ];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
