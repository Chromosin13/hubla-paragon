<?php

use yii\web\JsonParser;

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
               // 'admin' => [
               //      'class' => 'mdm\admin\Module',
               //  ],
               'gridview' =>  [
                    'class' => '\kartik\grid\Module'
                    // enter optional module parameters below - only if you need to
                    // use your own export download action or custom translation
                    // message source
                    // 'downloadAction' => 'gridview/export/download',
                    // 'i18n' => []
                ]
    ],
    'components' => [
        // 'authManager' => [
        //     'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\DbManager'
        // ],
        'session' => [
            'class' => '\yii\web\Session',
            'name' => 'production',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'ptidotnet',
            'enableCsrfValidation' => false,
            // 'enableCsrfCookie' => false,
            'parsers' => [
                // 'application/json' => 'yii\web\JsonParser',
                'application/json' => JsonParser::class,
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db_vi' => require(__DIR__ . '/db_vi.php'),
        'db' => require(__DIR__ . '/db.php'),
        'db2' => require(__DIR__ . '/db2.php'),
        // 'db3' => require(__DIR__ . '/db3.php'),
        'db_warehouse' => require(__DIR__ . '/db_warehouse.php'),
        'db_varcos' => require(__DIR__ . '/db_varcos.php'),
        'db_paragon' => require(__DIR__ . '/db_paragon.php'),
        // 'db_mdm' => require(__DIR__ . '/db_mdm.php'),
        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => '_all-skins',
                ],
            ],
        ],

        // 'assetManager' => [
        //     'bundles' => [
        //         'dmstr\web\AdminLteAsset2' => [
        //             'skin' => 'skin-green',
        //         ],
        //     ],
        // ],
        // 'assetManager' => [
        //     'bundles' => [
        //         'dmstr\web\AdminLteAsset' => [
        //             'skin' => 'skin-green',
        //         ],
        //     ],
        // ],

        // 'view' => [
        //  'theme' => [
        //      'pathMap' => [
        //         '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
        //      ],
        //     ],
        // ],
        
        // 'urlManager' => [
        //     'enablePrettyUrl' => false,
        //     'enableStrictParsing' => true,
        //     'showScriptName' => false,
        //     'rules' => [
        //         ['class' => 'yii\rest\UrlRule', 'controller' => ['vi-debug']],
        //     ],
        // ],
        
        /*
        'urlManager' => [
        'class' => 'yii\web\UrlManager',
        // Disable index.php
        'showScriptName' => false,
        // Disable r= routes
        'enablePrettyUrl' => true,
        'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
        ),
        ],*/
    ],
    // 'as access' => [
    //     'class' => 'mdm\admin\components\AccessControl',
    //     'allowActions' => [
    //         'site/*',
    //         'admin/*',
    //         'some-controller/some-action',
    //         // The actions listed here will be allowed to everyone including guests.
    //         // So, 'admin/*' should not appear here in the production, of course.
    //         // But in the earlier stages of your development, you may probably want to
    //         // add a lot of actions here until you finally completed setting up rbac,
    //         // otherwise you may not even take a first step.
    //     ]
    // ],
    'params' => $params,
    'as beforeRequest' => [  //if guest user access site so, redirect to login page.
        'class' => 'yii\filters\AccessControl',
        'rules' => [
            [
                'actions' => [
                    'login',
                    'error',
                    'api-post-debug',
                    'api-get-debug',
                    'api-post-inspection-result',
                    'api-get-running-schedule',
                    'api-get-check-db-connection',
                    'api-get-all-historic-schedule',
                    'api-validate-sku',
                    'api-post-trace-defect'
                ],
                'allow' => true,
            ],
            [
                'allow' => true,
                'roles' => ['@'],
            ],
        ],
    ],

    /*'as access' => [
        'class' => \yii\filters\AccessControl::className(),//AccessControl::className(),
        'rules' => [
            [
                'actions' => ['login', 'error'],
                'allow' => true,
            ],
            [
                'actions' => ['logout', 'index','dashboard'], // add all actions to take guest to login page
                'allow' => true,
                'roles' => ['@'],
            ],
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    // $config['bootstrap'][] = 'debug';
    // $config['modules']['debug'] = [
    //     'class' => 'yii\debug\Module',
    // ];
    // $config['modules']['debug']['allowedIPs'] = ['*'];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
    $config['modules']['gii']['allowedIPs'] = ['*'];
}

return $config;
