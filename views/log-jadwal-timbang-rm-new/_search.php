<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogJadwalTimbangRmNewSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-jadwal-timbang-rm-new-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'nomo') ?>

    <?= $form->field($model, 'nama_fg') ?>

    <?= $form->field($model, 'qty_batch') ?>

    <?= $form->field($model, 'scheduled_start') ?>

    <?= $form->field($model, 'formula_reference') ?>

    <?php // echo $form->field($model, 'week') ?>

    <?php // echo $form->field($model, 'lokasi') ?>

    <?php // echo $form->field($model, 'nama_line') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
