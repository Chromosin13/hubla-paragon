<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\LogJadwalTimbangRmNewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Jadwal Timbang Rm News';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-jadwal-timbang-rm-new-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Log Jadwal Timbang Rm New', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nomo',
            'nama_fg',
            'qty_batch',
            'scheduled_start',
            'formula_reference',
            // 'week',
            // 'lokasi',
            // 'nama_line',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
