<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LogJadwalTimbangRmNew */

$this->title = $model->nomo;
$this->params['breadcrumbs'][] = ['label' => 'Log Jadwal Timbang Rm News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-jadwal-timbang-rm-new-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->nomo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->nomo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nomo',
            'nama_fg',
            'qty_batch',
            'scheduled_start',
            'formula_reference',
            'week',
            'lokasi',
            'nama_line',
        ],
    ]) ?>

</div>
