<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LogJadwalTimbangRmNew */

$this->title = 'Create Log Jadwal Timbang Rm New';
$this->params['breadcrumbs'][] = ['label' => 'Log Jadwal Timbang Rm News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-jadwal-timbang-rm-new-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
