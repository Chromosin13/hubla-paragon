<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogJadwalTimbangRmNew */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-jadwal-timbang-rm-new-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomo')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'qty_batch')->textInput() ?>

    <?= $form->field($model, 'scheduled_start')->textInput() ?>

    <?= $form->field($model, 'formula_reference')->textInput() ?>

    <?= $form->field($model, 'week')->textInput() ?>

    <?= $form->field($model, 'lokasi')->textInput() ?>

    <?= $form->field($model, 'nama_line')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
