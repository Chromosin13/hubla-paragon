<?php
/* @var $this yii\web\View */
?>

<style media="screen">

  .container{
    width: 100%;
    padding: 3vh 3vh
  }

  .content-head{
    background: #3C8DBC;
    border-radius: 24px;
    color: white;
  }

  .content-head-title{
    font-size: 4vh;
    font-weight: 600;
  }

  .content-head-desc{
    font-size: 3vh;
    margin-top: 1vh
  }

  .project-logo{
    border: 0.6vh solid white;
    width: 10vh;
    height: 10vh;
    position: absolute;
    left: 50%;
    margin-top: -2.5vh;
  }

  .content-body{
    background: white;
    margin-top: 6vh;
    border-radius: 24px
  }

  .table{
    width: 50%;
  }

  table tr td{
    border:none!important
  }

  .content-body-title{
    font-size: 6vh;
  }

  .output{
    color:#FFD91B
  }

  .input{
    color:#3C8DBC;
  }

  .content-body-input{
    background: #FFD91B!important;
    border-radius: 24px;
    width: 100%
  }

  .content-body-output{
    background: #3C8DBC!important;
    border-radius: 24px;
    width: 100%
  }

  .content-body-background-input{
    background:white!important;
    border-radius: 24px;
  }

  .content-body-background-output{
    background:white!important;
    border-radius: 24px;
  }

  .content-body-value{
    font-size: 7vh;
    color: black;
    font-weight: 900;
    padding: 0%!important;
    margin: 0%!important
  }

  .content-body-desc{
    font-size:3vh;
    color: black;
    font-weight: 600;
    padding: 0%!important;
    margin: 0%!important
  }

  .content-body-input tr td .content-body-value{
    padding: 0%!important;
    margin: 0%!important
  }

  #sediaan{
    background: #DDD;
    border: 3px solid white;
    color: #3C8DBC;
    padding: 1vh;
    border-radius: 14px;
    font-weight: bold;
  }

  #sediaan:hover{
    cursor: pointer;
  }

</style>

<script type="text/javascript">
  function resize_to_fit() {
    var fontsize = $('.content-body-background-input .content-body-value').css('font-size');
    $('.content-body-background-input .content-body-value').css('fontSize', parseFloat(fontsize) - 1);

    if ($('.content-body-background-input .content-body-value').height() >= $('.content-body-background-input').height()) {
      resize_to_fit();
    }
  }

  function changeSediaan(){
    var sediaan = document.getElementById('sediaan').value;
    window.location = 'index.php?r=live-dashboard/index&sediaan='+sediaan;
  }
  window.onload = function(){
    resize_to_fit();
    document.getElementById('sediaan').value = '<?= $sediaan; ?>';
    setTimeout(function(){
       window.location.reload(1);
    }, 300000);
  }
</script>

<div class="container content-head">
  <div class="row">
    <div class="col" style="padding-left: 4vh">
      <div class="">
        <span class="content-head-title">Live Dashboard KFG</span>
      </div>
      <div class="content-head-desc">
        <span>Current Date: <?= $data['date'] ?></span>
      </div>
      <div class="content-head-desc">
        <span>Current Week:  <?= $data['week']?></span>
      </div>
      <div class="content-head-desc">
        <span>Sediaan:
          <select class="" name="sediaan" id="sediaan" onchange="changeSediaan()">
            <option value="" style="display:none"></option>
            <option value="liquid">Liquid</option>
            <option value="powder">Powder</option>
            <option value="semsol">Semi Solid</option>
            <option value="varcos">Varcos</option>
          </select>
        </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <img class="img-circle project-logo" src="../web/images/proses_icons/live-dashboard.svg" alt="LiveDashboardIcon">
    </div>
  </div>
</div>

<div class="container content-body">
  <div class="table-responsive">
    <table class="table" style="width:100%">
      <tr>
        <td>
          <div class="table-responsive-sm">
            <table class="table content-body-input">
              <tr>
                <td colspan="2" style="text-align:center;">
                  <span class="content-body-title input"><b>DAILY INPUT</b></span>
                </td>
              </tr>
              <tr>
                <td style="text-align:center;width:50%">
                  <div class="content-body-background-input" style="margin-left:2vh!important;margin-bottom:2vh!important">
                    <div class="">
                      <span class="content-body-value"><?= str_replace(',','.',number_format($res['qty_palet_input_daily'])) ?? '-' ?></span>
                    </div>
                    <div class="">
                      <span class="content-body-desc">PALET / TROLI</span>
                    </div>
                  </div>
                </td>
                <td style="text-align:center;width:50%">
                  <div class="content-body-background-input" style="margin-right:2vh!important;margin-bottom:2vh!important">
                    <div class="">
                      <span class="content-body-value"><?= str_replace(',','.',number_format($res['qty_input_daily'])) ?? '-' ?></span>
                    </div>
                    <div class="">
                      <span class="content-body-desc">PCS</span>
                    </div>
                  </div>
                </td>
              </tr>
            </table>
          </div>
        </td>
        <td>
          <div class="table-responsive-sm">
            <table class="table content-body-output">
              <tr>
                <td colspan="2" style="text-align:center;">
                  <span class="content-body-title output"><b>DAILY OUTPUT</b></span>
                </td>
              </tr>
              <tr>
                <td style="text-align:center;width:50%">
                  <div class="content-body-background-output" style="margin-left:2vh!important;margin-bottom:2vh!important">
                    <div class="">
                      <span class="content-body-value"><?= str_replace(',','.',number_format($res['qty_palet_output_daily'])) ?? '-' ?></span>
                    </div>
                    <div class="">
                      <span class="content-body-desc">PALET / TROLI</span>
                    </div>
                  </div>
                </td>
                <td style="text-align:center;width:50%">
                  <div class="content-body-background-input" style="margin-right:2vh!important;margin-bottom:2vh!important">
                    <div class="">
                      <span class="content-body-value"><?= str_replace(',','.',number_format($res['qty_output_daily'])) ?? '-' ?></span>
                    </div>
                    <div class="">
                      <span class="content-body-desc">PCS</span>
                    </div>
                  </div>
                </td>
              </tr>
            </table>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <div class="table-responsive-sm">
            <table class="table content-body-input">
              <tr>
                <td colspan="2" style="text-align:center;">
                  <span class="content-body-title input"><b>WEEKLY INPUT</b></span>
                </td>
              </tr>
              <tr>
                <td style="text-align:center;width:50%">
                  <div class="content-body-background-input" style="margin-left:2vh!important;margin-bottom:2vh!important">
                    <div class="">
                      <span class="content-body-value"><?= str_replace(',','.',number_format($res['qty_palet_input_weekly'])) ?? '-' ?></span>
                    </div>
                    <div class="">
                      <span class="content-body-desc">PALET / TROLI</span>
                    </div>
                  </div>
                </td>
                <td style="text-align:center;width:50%">
                  <div class="content-body-background-input" style="margin-right:2vh!important;margin-bottom:2vh!important">
                    <div class="">
                      <span class="content-body-value"><?= str_replace(',','.',number_format($res['qty_input_weekly'])) ?? '-' ?></span>
                    </div>
                    <div class="">
                      <span class="content-body-desc">PCS</span>
                    </div>
                  </div>
                </td>
              </tr>
            </table>
          </div>
        </td>
        <td>
          <div class="table-responsive-sm">
            <table class="table content-body-output">
              <tr>
                <td colspan="2" style="text-align:center;">
                  <span class="content-body-title output"><b>WEEKLY OUTPUT</b></span>
                </td>
              </tr>
              <tr>
                <td style="text-align:center;width:50%">
                  <div class="content-body-background-output" style="margin-left:2vh!important;margin-bottom:2vh!important">
                    <div class="">
                      <span class="content-body-value"><?= str_replace(',','.',number_format($res['qty_palet_output_weekly'])) ?? '-' ?></span>
                    </div>
                    <div class="">
                      <span class="content-body-desc">PALET / TROLI</span>
                    </div>
                  </div>
                </td>
                <td style="text-align:center;width:50%">
                  <div class="content-body-background-input" style="margin-right:2vh!important;margin-bottom:2vh!important">
                    <div class="">
                      <span class="content-body-value"><?= str_replace(',','.',number_format($res['qty_output_weekly'])) ?? '-' ?></span>
                    </div>
                    <div class="">
                      <span class="content-body-desc">PCS</span>
                    </div>
                  </div>
                </td>
              </tr>
            </table>
          </div>
        </td>
      </tr>
    </table>
  </div>
</div>
