<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\QcbulkEditable */

$this->title = 'Create Qcbulk Editable';
$this->params['breadcrumbs'][] = ['label' => 'Qcbulk Editables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qcbulk-editable-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
