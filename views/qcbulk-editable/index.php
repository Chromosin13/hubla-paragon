<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QcbulkEditableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Qcbulk Editables';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qcbulk-editable-index"> 

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

     <?php 

        $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
                    'id',
                    'nomo',
                    'snfg',
                    'snfg_komponen',
                    'ph',
                    'viskositas',
                    'berat_jenis',
                    'kadar',
                    'warna',
                    'bau',
                    'performance',
                    'bentuk',
                    'mikro',
                    'kejernihan',
                    'status',
                    'waktu',
                    'state',
                    'posisi',
                    'jenis_periksa',
                    'timestamp',
        ['class' => 'yii\grid\ActionColumn'],
        ];

        echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_PDF => false,
                                ExportMenu::FORMAT_HTML => false
                          ]        
    ]);?>

    <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'primary', 'heading'=>'Check Data Planner'],
            'columns'=>[
                ['class' => 'kartik\grid\ActionColumn','template' => '{update}',],
                ['class'=>'kartik\grid\SerialColumn'],
                'id',
                'nomo',
                'snfg',
                'snfg_komponen',
                [
                        'class'=>'kartik\grid\EditableColumn',
                        'attribute'=>'ph',
                ],
                [
                        'class'=>'kartik\grid\EditableColumn',
                        'attribute'=>'viskositas',
                ],
                [
                        'class'=>'kartik\grid\EditableColumn',
                        'attribute'=>'berat_jenis',
                ],
                [
                        'class'=>'kartik\grid\EditableColumn',
                        'attribute'=>'kadar',
                ],
                [
                        'class'=>'kartik\grid\EditableColumn',
                        'attribute'=>'warna',
                ],
                [
                        'class'=>'kartik\grid\EditableColumn',
                        'attribute'=>'bau',
                ],
                [
                        'class'=>'kartik\grid\EditableColumn',
                        'attribute'=>'performance',
                ],
                [
                        'class'=>'kartik\grid\EditableColumn',
                        'attribute'=>'bentuk',
                ],
                [
                        'class'=>'kartik\grid\EditableColumn',
                        'attribute'=>'mikro',
                ],
                [
                        'class'=>'kartik\grid\EditableColumn',
                        'attribute'=>'kejernihan',
                ],
                [
                        'class'=>'kartik\grid\EditableColumn',
                        'attribute'=>'status',
                ],
                // 'status',
                'waktu',
                'state',
                'posisi',
                'jenis_periksa',
                'timestamp',
             ],
        ]);
    
    ?>

</div>
