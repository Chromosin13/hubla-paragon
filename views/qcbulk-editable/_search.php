<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\QcbulkEditableSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qcbulk-editable-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nomo') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'snfg_komponen') ?>

    <?= $form->field($model, 'ph') ?>

    <?php // echo $form->field($model, 'viskositas') ?>

    <?php // echo $form->field($model, 'berat_jenis') ?>

    <?php // echo $form->field($model, 'kadar') ?>

    <?php // echo $form->field($model, 'warna') ?>

    <?php // echo $form->field($model, 'bau') ?>

    <?php // echo $form->field($model, 'performance') ?>

    <?php // echo $form->field($model, 'bentuk') ?>

    <?php // echo $form->field($model, 'mikro') ?>

    <?php // echo $form->field($model, 'kejernihan') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'waktu') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'posisi') ?>

    <?php // echo $form->field($model, 'jenis_periksa') ?>

    <?php // echo $form->field($model, 'timestamp') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
