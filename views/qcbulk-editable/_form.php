<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\QcbulkEditable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qcbulk-editable-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jenis_periksa')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'nomo')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'snfg')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'snfg_komponen')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'ph')->textInput() ?>

    <?= $form->field($model, 'viskositas')->textInput() ?>

    <?= $form->field($model, 'berat_jenis')->textInput() ?>

    <?= $form->field($model, 'kadar')->textInput() ?>

    <?= $form->field($model, 'warna')->textInput() ?>

    <?= $form->field($model, 'bau')->textInput() ?>

    <?= $form->field($model, 'performance')->textInput() ?>

    <?= $form->field($model, 'bentuk')->textInput() ?>

    <?= $form->field($model, 'mikro')->textInput() ?>

    <?= $form->field($model, 'kejernihan')->textInput() ?>

    <?= $form->field($model, 'status')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'waktu')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'timestamp')->textInput(['readonly' => 'true']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
