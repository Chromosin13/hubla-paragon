<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\IdentitasPengecekanFg */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Identitas Pengecekan Fgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="identitas-pengecekan-fg-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nama_fg',
            'keterangan',
            'batch_fg',
            'serial_number',
            'no_mo',
            'exp_date',
            'due_date',
            'plant',
            'line_kemas',
            'jumlah_mpq',
            'total_aql',
            'kode_sl',
            'no_notifikasi',
            'nama_produk_sebelumnya',
            'batch_produk_sebelumnya',
            'air_bilasan_akhir',
            'verifikasi_trial_filling',
            'label_bersih_bulk',
            'label_bersih_mesin',
        ],
    ]) ?>

</div>
