<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IdentitasPengecekanFg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="identitas-pengecekan-fg-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'keterangan')->textInput() ?>

    <?= $form->field($model, 'batch_fg')->textInput() ?>

    <?= $form->field($model, 'serial_number')->textInput() ?>

    <?= $form->field($model, 'no_mo')->textInput() ?>

    <?= $form->field($model, 'exp_date')->textInput() ?>

    <?= $form->field($model, 'due_date')->textInput() ?>

    <?= $form->field($model, 'plant')->textInput() ?>

    <?= $form->field($model, 'line_kemas')->textInput() ?>

    <?= $form->field($model, 'jumlah_mpq')->textInput() ?>

    <?= $form->field($model, 'total_aql')->textInput() ?>

    <?= $form->field($model, 'kode_sl')->textInput() ?>

    <?= $form->field($model, 'no_notifikasi')->textInput() ?>

    <?= $form->field($model, 'nama_produk_sebelumnya')->textInput() ?>

    <?= $form->field($model, 'batch_produk_sebelumnya')->textInput() ?>

    <?= $form->field($model, 'air_bilasan_akhir')->textInput() ?>

    <?= $form->field($model, 'verifikasi_trial_filling')->textInput() ?>

    <?= $form->field($model, 'label_bersih_bulk')->textInput() ?>

    <?= $form->field($model, 'label_bersih_mesin')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
