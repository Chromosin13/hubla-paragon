<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IdentitasPengecekanFgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Identitas Pengecekan Fgs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="identitas-pengecekan-fg-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Identitas Pengecekan Fg', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nama_fg',
            'keterangan',
            'batch_fg',
            'serial_number',
            // 'no_mo',
            // 'exp_date',
            // 'due_date',
            // 'plant',
            // 'line_kemas',
            // 'jumlah_mpq',
            // 'total_aql',
            // 'kode_sl',
            // 'no_notifikasi',
            // 'nama_produk_sebelumnya',
            // 'batch_produk_sebelumnya',
            // 'air_bilasan_akhir',
            // 'verifikasi_trial_filling',
            // 'label_bersih_bulk',
            // 'label_bersih_mesin',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
