<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\IdentitasPengecekanFg */

$this->title = 'Create Identitas Pengecekan Fg';
$this->params['breadcrumbs'][] = ['label' => 'Identitas Pengecekan Fgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="identitas-pengecekan-fg-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
