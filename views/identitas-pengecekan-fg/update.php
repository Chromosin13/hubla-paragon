<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IdentitasPengecekanFg */

$this->title = 'Update Identitas Pengecekan Fg: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Identitas Pengecekan Fgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="identitas-pengecekan-fg-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
