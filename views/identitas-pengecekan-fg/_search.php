<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IdentitasPengecekanFgSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="identitas-pengecekan-fg-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nama_fg') ?>

    <?= $form->field($model, 'keterangan') ?>

    <?= $form->field($model, 'batch_fg') ?>

    <?= $form->field($model, 'serial_number') ?>

    <?php // echo $form->field($model, 'no_mo') ?>

    <?php // echo $form->field($model, 'exp_date') ?>

    <?php // echo $form->field($model, 'due_date') ?>

    <?php // echo $form->field($model, 'plant') ?>

    <?php // echo $form->field($model, 'line_kemas') ?>

    <?php // echo $form->field($model, 'jumlah_mpq') ?>

    <?php // echo $form->field($model, 'total_aql') ?>

    <?php // echo $form->field($model, 'kode_sl') ?>

    <?php // echo $form->field($model, 'no_notifikasi') ?>

    <?php // echo $form->field($model, 'nama_produk_sebelumnya') ?>

    <?php // echo $form->field($model, 'batch_produk_sebelumnya') ?>

    <?php // echo $form->field($model, 'air_bilasan_akhir') ?>

    <?php // echo $form->field($model, 'verifikasi_trial_filling') ?>

    <?php // echo $form->field($model, 'label_bersih_bulk') ?>

    <?php // echo $form->field($model, 'label_bersih_mesin') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
