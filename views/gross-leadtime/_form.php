<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GrossLeadtime */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gross-leadtime-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'tanggal')->textInput() ?>

    <?= $form->field($model, 'tanggal_stop')->textInput() ?>

    <?= $form->field($model, 'leadtime_kotor_jam')->textInput() ?>

    <?= $form->field($model, 'leadtime_kotor_hari')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
