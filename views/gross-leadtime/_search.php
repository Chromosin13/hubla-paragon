<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GrossLeadtimeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gross-leadtime-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nama_fg') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'tanggal') ?>

    <?= $form->field($model, 'tanggal_stop') ?>

    <?php // echo $form->field($model, 'leadtime_kotor_jam') ?>

    <?php // echo $form->field($model, 'leadtime_kotor_hari') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
