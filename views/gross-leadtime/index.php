<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;
use app\models\GrossLeadtime;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GrossLeadtimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gross Leadtimes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gross-leadtime-index">


    <?php 

        $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
            'nama_fg',
            'snfg',
            'tanggal',
            'tanggal_stop',
            'leadtime_kotor_jam',
            'leadtime_kotor_hari',
        ['class' => 'yii\grid\ActionColumn'],
        ];

        echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
  
    ]);?>

    <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'responsiveWrap' => false,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'success', 'heading'=>'Overall Leadtime'],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                [   
                    'attribute' => 'nama_fg',
                    'format'=> 'raw',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(GrossLeadtime::find()->orderBy('nama_fg')->asArray()->all(), 'nama_fg', 'nama_fg'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'SKU'],
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>1,
                ],  
                [   
                    'attribute' => 'snfg',
                    'format'=> 'raw',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>2,
                ],
                [   
                    'attribute' => 'tanggal',
                    'format'=> 'raw',
                ],
                [   
                    'attribute' => 'tanggal_stop',
                    'format'=> 'raw',
                ],
                [   
                    'attribute' => 'leadtime_kotor_jam',
                    'format'=> 'raw',
                ],
                [   
                    'attribute' => 'leadtime_kotor_hari',
                    'format'=> 'raw',
                ],


                        // 'palet_ke',
             ],
            
        ]);
    
    ?>
</div>
