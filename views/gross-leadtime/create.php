<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GrossLeadtime */

$this->title = 'Create Gross Leadtime';
$this->params['breadcrumbs'][] = ['label' => 'Gross Leadtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gross-leadtime-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
