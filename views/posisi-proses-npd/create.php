<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PosisiProsesNpd */

$this->title = 'Create Posisi Proses Npd';
$this->params['breadcrumbs'][] = ['label' => 'Posisi Proses Npds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posisi-proses-npd-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
