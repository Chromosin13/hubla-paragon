<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PosisiProsesNpd */

$this->title = 'Update Posisi Proses Npd: ' . $model->snfg_komponen;
$this->params['breadcrumbs'][] = ['label' => 'Posisi Proses Npds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->snfg_komponen, 'url' => ['view', 'id' => $model->snfg_komponen]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="posisi-proses-npd-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
