<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>


                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
                      <h3 class="widget-user-username"><b>Pra Kemas</b></h3>
                      <h5 class="widget-user-desc">Input Data</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/business-partnership.png" alt="User Avatar">
                    </div>
                  
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">

                            <div class="zoom">
                              <?=Html::a('SCAN WITH QR', ['flow-pra-kemas/check-kemas-tab'], ['class' => 'btn btn-success btn-block']);?>
                            </div>
                            <p></p>

                              <div class="box-body">
                                  <div class="flow-pra-kemas-form">

                                    <div class="form-group field-flowprakemas-snfg has-success">
                                    <label class="control-label" for="flowprakemas-snfg">Snfg</label>
                                    <input type="text" id="flowprakemas-snfg" class="form-control" name="FloPraKemas[snfg]" aria-invalid="false">
                                    <br>
                                    <button id="checkbtn" class="btn btn-info btn-block" style="width:50%; margin:0 auto;">CHECK</button>

                                    </div>
                                  </div>
                              </div>
                        </div>
                      <!-- /.row -->
                      </div>
                    </div>


                  <!-- List Data -->

                   <div class="box box-widget widget-user">

                      <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                                    IN PROGRESS
                      </h4>

                      <div class="box-footer">

                        <div class="row">
                          <div class="box-body">
                               
                                  <div class="flow-pra-kemas-form">



                                     <?= GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'columns' => [
                                            
                                            'snfg',
                                            'lanjutan',
                                            'posisi',
                                            'datetime_start',
                                            'datetime_stop',
                                            'nama_line',
                                            'jenis_kemas',
                                            'nama_operator',
                                        ],
                                    ]); ?>


                                  </div>
                          </div>
                        </div>
                        <!-- /.row -->
                      </div>
                  </div>

<div class="modal fade"  id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="modaltitle">Pilih Line yang mau di stop</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="isi_modal" class="modal-body">
          
          <b class="text-left" style="font-size:1em;">Nama line</b> :  <p id="isi_snfg"></p>
          <br>
          
          <!-- Jika belum selesai, dan dilanjut di shift berikutnya, klik Belum All Batch. -->
      </div>
      <div class="modal-footer">
        <button id="cancel" type="button" class="btn btn-warning pull-left" data-dismiss="modal">Close</button>
        <!-- <button id="notdone" type="button" class="btn btn-secondary"></button>
        <button id="done" type="button" class="btn btn-primary">Sudah All Batch</button> -->
      </div>
    </div>
  </div>
</div>


<?php
$script = <<< JS

  // AutoFocus SNFG Field

  document.getElementById("flowprakemas-snfg").focus();

  // Check SNFG Validity

    $('#checkbtn').click(function(){  

      var snfg = $('#flowprakemas-snfg').val().trim();
      
      if(snfg){
        $.get('index.php?r=flow-pra-kemas/check-if-split-line',{ snfg : snfg },function(data){ 
            a = JSON.parse(data);
            //console.log(a[0]['route']);
            if (a[0]['route'] == 'to-create') {
              window.location = "index.php?r=flow-pra-kemas/create-kemas&snfg="+snfg;
            } else {
              $.get('index.php?r=flow-pra-kemas/check-is-split',{ snfg : snfg },function(data){
                if (data == 1){
                  document.getElementById("isi_snfg").innerHTML = "";
                  a.forEach(myFunction);
                  $('#modalConfirm').appendTo('body').modal('show');
                } else {
                  window.location = "index.php?r=flow-pra-kemas/create-kemas&snfg="+snfg;
                }
              });
            }
          });
      } else {
        alert("Isi field SNFG terlebih dahulu!.");
      }
      
  });

  $("#flowprakemas-snfg").on('keyup', function (e) {
    if (e.key === 'Enter' || e.keyCode === 13) {
      var snfg = $('#flowprakemas-snfg').val().trim();
      
      if(snfg){
         $.get('index.php?r=flow-pra-kemas/check-if-split-line',{ snfg : snfg },function(data){ 
            a = JSON.parse(data);
            //console.log(a[0]['route']);
            if (a[0]['route'] == 'to-create') {
              window.location = "index.php?r=flow-pra-kemas/create-kemas&snfg="+snfg;
            } else {
              $.get('index.php?r=flow-pra-kemas/check-is-split',{ snfg : snfg },function(data){
                if (data == 1){
                  document.getElementById("isi_snfg").innerHTML = "";
                  a.forEach(myFunction);
                  $('#modalConfirm').appendTo('body').modal('show');
                } else {
                  window.location = "index.php?r=flow-pra-kemas/create-kemas&snfg="+snfg;
                }
              });
            }
          });
      } else {
        alert("Isi field SNFG terlebih dahulu!.");
      }
    }
});

function myFunction(item, index) {
    var snfg = $('#flowprakemas-snfg').val().trim();
    document.getElementById("isi_snfg").innerHTML += `<button class='btn btn-info btn-block' onclick='window.location="index.php?r=flow-pra-kemas/stop-work-order&snfg=`+snfg+`&last_id=`+item['id']+`&pos=inline"' style='width:50%; margin:0 auto;'>`+item['nama_line']+`</button><br>`;
  }


  // Check SNFG Validity

  // $('#flowprakemas-snfg').change(function(){  

  //     var snfg = $('#flowprakemas-snfg').val().trim();
      
  //     if(snfg){
  //       window.location = "index.php?r=flow-pra-kemas/create-kemas&snfg="+snfg;
  //     }else{
  //       alert('Nomor Jadwal kosong!');
  //     }
      
  // });

  // $('#flowprakemas-snfg').change(function(){  

  //     var snfg = $('#flowprakemas-snfg').val();


  //     $.get('index.php?r=scm-planner/check-snfg',{ snfg : snfg },function(data){
  //         var data = $.parseJSON(data);
  //         if(data.id>=1){
            
  //           window.location = "index.php?r=flow-pra-kemas/create-kemas&snfg="+snfg;
             
  //         }

  //         else{

  //           alert('Nomor SNFG Tidak Ditemukan di Database; Silahkan mengontak Planner/Leader');
  //         }
      
  //     });
  // });



JS;
$this->registerJs($script);
?>