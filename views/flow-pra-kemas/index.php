<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FlowPraKemasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Flow Pra Kemas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flow-pra-kemas-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Flow Pra Kemas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'posisi',
            'lanjutan',
            'datetime_start',
            'datetime_stop',
            'datetime_write',
            // 'nama_operator',
            // 'nama_line',
            // 'jenis_kemas',
            // 'is_done',
            // 'counter',
            // 'snfg',
            // 'jumlah_realisasi',
            // 'nobatch',
            // 'staging:boolean',
            // 'exp_date',
            // 'nosmb',
            // 'barcode',
            // 'fg_name_odoo',
            // 'posisi_scan',
            // 'id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
