<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */


?>
<div class="flow-pra-kemas-create">




    <?php 

    	echo $this->render('_form-kemas', [
		        'model' => $model,
		        'snfg' => $snfg,
			    'searchModel' => $searchModel,
			    'dataProvider' => $dataProvider,
			    'na_number' => $na_number,
			    'jenis_proses_list' => $jenis_proses_list,
		        // 'frontend_ip' => $frontend_ip,
		        // 'sbc_ip' => $sbc_ip,
		        // 'line' => $line,
		        'nosmb' => $nosmb,
		        'nobatch' => $nobatch,
		        'exp_date' => $exp_date,
		        'barcode' => $barcode,
                'fg_name' => $fg_name,
                // 'keterangan' => $keterangan
		]);

     ?>

</div>
