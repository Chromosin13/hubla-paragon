<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowPraKemas;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\FlowPraKemas */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
#modaltitle{
  display:inline-block;
}
.modal-body{
  font-weight:500;
  font-size:24px;
}
.modal-content{
  vertical-align:middle;
  margin-top: 25%;
  background-color : #f66257;
  text-align:center;

}

</style>




                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
                      <h3 class="widget-user-username"><b>PRA KEMAS/b></h3>
                      <h5 class="widget-user-desc">Work Order</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/scale.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">

                            <div class="box-body">

                                <div class="flow-pra-kemas-form">


                                    <?php $form = ActiveForm::begin(); ?>

                                    <div class="row">
                                        <div class="col-md-4">
                                          <?= $form->field($model, 'jumlah_realisasi')->textInput() ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-4">
                                          <?= $form->field($model, 'nobatch')->textInput(['value'=>$batch]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-4">
                                          <?= $form->field($model, 'counter')->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>
                                    
                                    <div class="row" >
                                        <div class="col-md-12">
                                          <?= $form->field($model, 'staging')->checkbox(); ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-12" id="div-hide">
                                          <?= $form->field($model, 'is_done')->checkbox(['id'=>'check-alert']); ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>

                                
                                    <div class="col-md-6 col-md-offset-5">

                                        <a id='buttonStop' class = 'btn btn-danger'>Stop Jadwal</a>
                                        <?php echo Html::submitButton('Stop Jadwal',['hidden'=>'hidden','id'=>'stop-jadwal','class'=>'btn btn-danger'], ['stop-kemas', 'snfg'=>$snfg,'last_id'=>$last_id]) ?>

                                    </div>

                                    <?php ActiveForm::end(); ?>



                                </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>


<div class="box box-widget widget-user">
    <div class="widget-user-header bg-blue">
                      <h3 class="widget-user-username"><b>Rincian</b></h3>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // ['class' => 'kartik\grid\SerialColumn'],
            'lanjutan',
            'snfg',
            'posisi',
            'datetime_start',
            'datetime_stop',
            // 'datetime_write',
            // 'nama_operator',
            'nama_line',
            'jenis_kemas',
            'nama_operator',
            'jumlah_realisasi',
            // 'shift_plan_start_olah',
            // 'shift_plan_end_olah',
            // 'plan_start_olah',
            // 'plan_end_olah',
            // 'turun_bulk_start',
            // 'turun_bulk_stop',
            // 'adjust_start',
            // 'adjust_stop',
            // 'ember_start',
            // 'ember_stop',
            // 'is_done',
            // 'id',

            // ['class' => 'kartik\grid\ActionColumn'],
        ],
    ]); ?>

</div>

<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
  Launch demo modal
</button> -->

    <div class="modal fade"  id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h2 class="modal-title" id="modaltitle">Confirm</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              Apakah proses <?php echo $jenis_kemas;?> sudah selesai All Batch / Is Done?<br>
              <!-- Jika belum selesai, dan dilanjut di shift berikutnya, klik Belum All Batch. -->
          </div>
          <div class="modal-footer">
            <button id="cancel" type="button" class="btn btn-warning pull-left" data-dismiss="modal">Cancel</button>
            <button id="notdone" type="button" class="btn btn-secondary">Belum All Batch</button>
            <button id="done" type="button" class="btn btn-primary">Sudah All Batch</button>
          </div>
        </div>
      </div>
    </div>


<?php
$script = <<< JS
    $('#div-hide').hide();
    $('#stop-jadwal').hide();

    $('#form-sisa-kemas').hide();

    // $("#check-alert").on("click", function() {
    //     if ($('input[type="checkbox"]').is(':checked')) {
    //         krajeeDialog.alert("!!! PERHATIAN !!! <div> Anda mengaktifkan pilihan ISDONE. <div> Pastikan batch benar-benar sudah selesai, atau SCAN SELANJUTNYA TIDAK BISA DILAKUKAN");
    //
    //         $('#form-sisa-kemas').fadeIn("slow");
    //
    //         return true;
    //
    //     }else{
    //         $('#form-sisa-kemas').hide();
    //         return true;
    //
    //     }
    // });

    $("#notdone").on("click", function(){
        $('#check-alert').prop("checked", false);
        $('#stop-jadwal').trigger('click');
    });

    $("#done").on("click", function(){
        $('#check-alert').prop("checked", true);
        $('#stop-jadwal').trigger('click');
    });

    $("#stop-jadwal").on("click", function validation(){
      return true;
    });
    
    $("#buttonStop").on("click", function validation(){
        // console.log("berhasil");
        // var sb = document.getElementById('sb').value;
        // var netto_min = "$netto_min";
        // var netto1 = document.getElementById('netto-1').value;
        // var netto2 = document.getElementById('netto-2').value;
        // var netto3 = document.getElementById('netto-3').value;
        // var netto4 = document.getElementById('netto-4').value;
        // var netto5 = document.getElementById('netto-5').value;

        // if (netto1==''||netto2==''||netto3==''||netto4==''||netto5==''){
        //   alert('Data harus diisi semua');
        //   return false;
        // }else{
        $('#modalConfirm').appendTo('body').modal('show');
          
          // // Jika field netto sudah terisi semua;
          // if (netto_min == null || netto_min == ''){
          //   //Jika netto_min bernilai null, validasi tidak dilakukan
          //   $('#modalConfirm').appendTo('body').modal('show');
          // }else{
          //   //Jika netto_min tidak null
          //   if (netto1 < netto_min + 40*netto_min/100 && netto1 > netto_min - 40*netto_min/100){
          //     if (netto2 < netto_min + 40*netto_min/100 && netto2 > netto_min - 40*netto_min/100){
          //       if (netto3 < netto_min + 40*netto_min/100 && netto3 > netto_min - 40*netto_min/100){
          //         if (netto4 < netto_min + 40*netto_min/100 && netto4 > netto_min - 40*netto_min/100){
          //           if (netto5 < netto_min + 40*netto_min/100 && netto5 > netto_min - 40*netto_min/100){
          //             $('#modalConfirm').appendTo('body').modal('show');
          //           }else{
          //             alert ('Netto 5 tidak sesuai standar netto');
          //             return false;
          //           }
          //         }else{
          //           alert ('Netto 4 tidak sesuai standar netto');
          //           return false;
          //         }
          //       }else{
          //         alert ('Netto 3 tidak sesuai standar netto');
          //         return false;
          //       }
          //     }else{
          //       alert ('Netto 2 tidak sesuai standar netto');
          //       return false;
          //     }
          //   }else{
          //     alert ('Netto 1 tidak sesuai standar netto');
          //     return false;
          //   }
          // }
        // }
    });



JS;
$this->registerJs($script);
?>
