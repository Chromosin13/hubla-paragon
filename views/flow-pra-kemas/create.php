<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowPraKemas */

$this->title = 'Create Flow Pra Kemas';
$this->params['breadcrumbs'][] = ['label' => 'Flow Pra Kemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flow-pra-kemas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
