<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FrowPraKemas;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\Downtime;
use app\models\MasterDataLine;
use app\models\DowntimeSearch;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\FrowPraKemas */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
#modaltitle{
  display:inline-block;
}
.modal-body{
  font-weight:500;
  font-size:18px;
}
.modal-content{
  vertical-align:middle;
  margin-top: 25%;
  background-color : #f66257;
  /*text-align:center;*/
}
</style>

                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
                      <h3 class="widget-user-username"><b>PRA KEMAS</b></h3>
                      <h5 class="widget-user-desc">Work Order</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">




                                <div class="flow-pra-kemas-form">

                                    <?php $form = ActiveForm::begin(); ?>


                                    <div class="row">
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'snfg')->textInput(['readOnly'=>true,'value'=>$snfg]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'posisi')->textInput(['readOnly'=>true,'value'=>'PRA_KEMAS']) ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-6">
                                          <?php

                                                echo $form->field($model, 'jenis_kemas')
                                                ->dropDownList(
                                                    $jenis_proses_list,           // Flat array ('id'=>'label')
                                                    ['prompt'=>'Select Options']    // options
                                                )->label('Jenis Kemas');

                                            ?>
                                          <!-- /input-group -->
                                        </div>
                                        <!-- /.col-lg-6 -->
                                        <div class="col-md-3">
                                          <?php

                                                if(empty($nama_line)){
                                                    echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(MasterDataLine::find()->where("sediaan not like '%off%' and posisi = 'KEMAS' ")->orderBy(['nama_line'=>SORT_ASC])->all()
                                                        ,'nama_line','nama_line'),
                                                        'options' => ['placeholder' => 'Select Line','id'=>'clubs'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]);
                                                }else{
                                                    echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(MasterDataLine::find()->where("sediaan not like '%off%' and posisi = 'KEMAS' ")->orderBy(['nama_line'=>SORT_ASC])->all()
                                                        ,'nama_line','nama_line'),
                                                        'options' => ['placeholder' => 'Select Line', 'value' => $nama_line ,'id'=>'clubs'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]);
                                                }
                                            ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div id="lanjutan">
                                        <div class="col-md-3">
                                          <?= $form->field($model, 'lanjutan')->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        </div>
                                    </div>


                                    <label class="control-label">Nama Operator</label>
                                    <?= $form->field($model, 'nama_operator')->widget(TagsinputWidget::classname(), [
                                                        'clientOptions' => [
                                                            'trimValue' => true,
                                                            'allowDuplicates' => false,
                                                            'maxChars'=> 4,
                                                            'minChars'=> 4,
                                                        ]
                                                    ])->label(false)?>

                                    <br></br>

                                    <?php

                                        if($fg_name==""){
                                            echo $form->field($model, 'fg_name_odoo')->textInput(['placeholder'=>'Data tidak ditemukan, silahkan isi manual']);
                                        } else {
                                            echo $form->field($model, 'fg_name_odoo')->textInput(['value'=>$fg_name]);
                                        }
                                    ?>
                                    
                                    <div id = "na-number">
                                        <?php

                                            if($na_number=="-"){
                                                echo $form->field($model, 'na_number')->textInput(['required'=>true,'placeholder'=>'Data tidak ditemukan, silahkan isi manual']);
                                            } else {
                                                echo $form->field($model, 'na_number')->textInput(['required'=>true,'value'=>$na_number]);
                                            }
                                        ?>
                                        
                                    </div>

                                     <?php

                                        if($nobatch=="-"){
                                            echo $form->field($model, 'nobatch')->textInput(['placeholder'=>'Data tidak ditemukan, silahkan isi manual']);
                                        } else {
                                            echo $form->field($model, 'nobatch')->textInput(['value'=>$nobatch]);
                                        }
                                    ?>

                                    <?=
                                        DatePicker::widget([
                                        'model' => $model,
                                        'form' => $form,
                                        'name' => 'exp_date',
                                        'attribute' => 'exp_date',
                                        //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                                        'options' => ['value'=>$exp_date,'placeholder' => 'Pilih Exp Date'],
                                        'pluginOptions' => [
                                            'format' => 'yyyy-M-dd',
                                            'todayHighlight' => true
                                            ]
                                        ]);
                                    ?>

                                    <p id='warning' style='color:red; visibility: hidden'>Expired date salah, terhitung hanya 1 tahun. Mohon koreksi.</p>


                                    <div class="row">

                                        <div class="col-md-2 col-md-offset-5">
                                            <?php 
                                                echo Html::submitButton($model->isNewRecord ? 'Start' : 'Update', ['id'=>'btn-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
                                            ?>
                                        </div>
                                        <div id="hide-submit">

                                            
                                            <?php 
                                                echo Html::Button('Start', ['id'=>'btn-check','class' => 'btn btn-success']);
                                            ?>
                                            
                                        </div>
                                    </div>

                                    <?php ActiveForm::end(); ?>

                                </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>


<div class="box box-widget widget-user">
    <div class="widget-user-header bg-blue">
                      <h3 class="widget-user-username"><b>Rincian</b></h3>
    </div>
    <?php 
    // echo GridView::widget([
    //     'dataProvider' => $dataProvider,
    //     // 'filterModel' => $searchModel,
    //     'columns' => [
    //         [
    //             'class' => 'kartik\grid\ExpandRowColumn',
    //             'value' => function($model,$key,$index,$column){
    //                 return GridView::ROW_COLLAPSED;
    //             },
    //             'detail' => function($model,$key,$index,$column){

    //                 $searchModel = new DowntimeSearch();

    //                 $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    //                 $dataProvider->query->where("flow_input_snfg_id=".$model->id);

    //                 return Yii::$app->controller->renderPartial('_expand-downtime-kemas2', [
    //                     'searchModel' => $searchModel,
    //                     'dataProvider' => $dataProvider,
    //                     'flow_input_snfg_id' => $model->id,
    //                 ]);
    //             },
    //         ],
    //         'lanjutan',
    //         'snfg',
    //         'posisi',
    //         'datetime_start',
    //         'datetime_stop',
    //         'nama_line',
    //         'jenis_kemas',
    //         'nama_operator',
    //         'jumlah_realisasi',
    //     ],
    // ]); 
    ?>

</div>


<?php

    Modal::begin([
            'header'=>'<h2 style="text-align:center; font-weight:bold;">Line Kemas Sedang Digunakan</h2>',
            'id' => 'modalWarning',
            'size' => 'modal-lg',
        ]);

    echo "<div id='modalContentWarning'></div>";

    Modal::end();

?>





<?php
$script = <<< JS

    $('#lanjutan').hide();
    $('#hide-submit').hide();

    var snfg = $('#flowprakemas-snfg').val();

    var posisi = $('#flowprakemas-posisi').val();

    // $.post("index.php?r=master-data-line/get-line-kemas-snfg&snfg="+$('#flowprakemas-snfg').val(), function (data){
    //     $("select#flowprakemas-nama_line").html(data);
    // });

    // $('#btn-check').on('click',function(){

    //     var snfg = $('#flowprakemas-snfg').val();
    //     var posisi = $('#flowprakemas-posisi').val();
    //     var jenis_kemas = $('#flowprakemas-jenis_kemas').val();
    //     var nama_line = $('#flowprakemas-nama_line').val();
    //     var lanjutan = $('#flowprakemas-lanjutan').val();
    //     var nama_operator = $('#flowprakemas-nama_operator').val();
    //     var fg_name_odoo = $('#flowprakemas-fg_name_odoo').val();
    //     var nobatch = $('#flowprakemas-nobatch').val();
    //     var exp_date = $('#flowprakemas-exp_date').val();
    //     var nosmb = $('#flowprakemas-nosmb').val();
    //     var test = null;

    //     if (snfg){
    //         if (posisi){
    //             if (jenis_kemas){
    //                 if (nama_line){
    //                     if (lanjutan){
    //                         if (nama_operator){
    //                             if (fg_name_odoo){
    //                                 if (nobatch){
    //                                     if (exp_date){
    //                                         if (nosmb){
    //                                             $.get('index.php?r=flow-pra-kemas/check-availability-line',{ snfg : snfg , nama_line : nama_line }, function(data){
    //                                                 if (data == 1){
    //                                                     $('#btn-submit').trigger('click');                                                        
    //                                                 }else{
    //                                                     // alert('Line ini sedang digunakan, tidak bisa dipilih');
    //                                                     var value = 'index.php?r=flow-pra-kemas/line-not-available&nama_line='+nama_line;
    //                                                     console.log(value);
    //                                                     $('#modalWarning').modal('show')
    //                                                       // find id
    //                                                       .find('#modalContentWarning')
    //                                                       // load id
    //                                                       .load(value);
    //                                                     // console.log(id);
    //                                                 }

    //                                             });
    //                                         }else{ alert('No SMB Kosong'); }
    //                                     }else{ alert('Exp Date Kosong'); }
    //                                 }else{ alert('Nomor Batch Kosong'); }
    //                             }else{ alert('Nama Item Kosong'); }
    //                         }else{ alert('Nama Operator Kosong'); }
    //                     }else{ alert('Lanjutan Kosong'); }
    //                 }else{ alert('Nama Line Kosong'); }
    //             }else{ alert('Jenis Kemas Kosong'); }
    //         }else{ alert('Posisi Proses Kosong'); }
    //     }else{ alert('SNFG Kosong'); }

    // }); 


    $('#flowprakemas-jenis_kemas').change(function(){

        var jenis_kemas = $('#flowprakemas-jenis_kemas').val();
        var na_number = "$na_number";

        $.get('index.php?r=flow-pra-kemas/get-lanjutan-kemas',{ snfg : snfg , jenis_kemas : jenis_kemas , posisi : posisi  }, function(data){
            var data = $.parseJSON(data);
            $('#flowprakemas-lanjutan').attr('value',data.lanjutan);

        });

        $('#lanjutan').fadeIn();

        if (jenis_kemas != 'INKJET'){
            $('#na-number').hide();
            $('#flowprakemas-na_number').attr('value','-');
        }else{
            $('#na-number').show();
            $('#flowprakemas-na_number').attr('value',na_number);
        }

    });

    document.getElementById('flowprakemas-nobatch').addEventListener('input', function (e) {
      //e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1,').trim();
        e.target.value = e.target.value.toUpperCase();
        // if (e.target.value.length % 5 == 0){
        //     //e.target.value = e.target.value.replace(/(.{4})/g, '$1').replace(/[ ,]+/g, ",");
        //     e.target.value = e.target.value.replace(/.$/,",");
        // }

    });

    // $('input[id^="flowprakemas-exp_date"]').on("change",function() {
    //   var date_exp = $(this).val();
    //   var exp_date = new Date(date_exp);

    //   var current_date = new Date();

    //   exp_year = exp_date.getFullYear();
    //   current_year = current_date.getFullYear();

    //   const diffTime = Math.abs(exp_date - current_date);
    //   const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

    //   // var line = document.getElementById("select2-chosen-1").innerHTML;
    //   // console.log(tes);

    //   if ((diffDays)<=365){
    //     $( "#btn-submit" ).prop( "disabled", true );
    //     $("#warning").css("visibility", "visible");
    //   } else {
    //     $( "#btn-submit" ).prop( "disabled", false );
    //     $("#warning").css("visibility", "hidden");
    //   }
    //   // console.log(exp_date.getFullYear());
    //   // console.log(current_date.getFullYear());

    // });

    $('[id^="clubs"]').on("change",function() {
        var line = document.getElementById("select2-chosen-1").innerHTML;
        //console.log(line);

        $.get('index.php?r=flow-pra-kemas/check-is-line-active',{ nama_line : line},function(data){ 
            var data = $.parseJSON(data);

            if (data != 0){
                var value = 'index.php?r=flow-pra-kemas/line-not-available&nama_line='+line;

                $('#modalWarning').modal('show')
                    .find('#modalContentWarning')
                    .load(value);
                // alert("Masih ada jadwal berjalan di line yang anda pilih ("+line+"). Pastikan anda tidak salah pilih line. Atau lakukan scan stop dahulu di line tersebut");

                document.getElementById("select2-chosen-1").innerHTML = "";
                document.getElementById("clubs").value = "";
            }
        });

    });


JS;
$this->registerJs($script);
?>
