<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FlowPraKemas */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Flow Pra Kemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flow-pra-kemas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'posisi',
            'lanjutan',
            'datetime_start',
            'datetime_stop',
            'datetime_write',
            'nama_operator',
            'nama_line',
            'jenis_kemas',
            'is_done',
            'counter',
            'snfg',
            'jumlah_realisasi',
            'nobatch',
            'staging:boolean',
            'exp_date',
            'nosmb',
            'barcode',
            'fg_name_odoo',
            'posisi_scan',
            'id',
        ],
    ]) ?>

</div>
