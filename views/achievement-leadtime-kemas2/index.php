<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;
use app\models\AchievementLeadtimeKemas2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AchievementLeadtimeKemas2Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

if (isset($info)) {
    $this->title = 'Achievement Leadtime Kemas 2 '.$info;
} else {
    $this->title = 'Achievement Leadtime Kemas 2 ';
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-leadtime-kemas2-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        <?= Html::a('Create Achievement Leadtime Kemas2', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->

    <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
             'responsiveWrap' => false,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'success', 'heading'=>'Achievement Line Kemas 2'],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                
                [
                    'attribute'=>'nama_line', 
                    'width'=>'210px',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(AchievementLeadtimeKemas2::find()->orderBy('nama_line')->asArray()->all(), 'nama_line', 'nama_line'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Line'],
                    'group'=>true,  // enable grouping
                    'groupHeader'=>function ($model, $key, $index, $widget) {
                        return [
                            'mergeColumns'=>[[1,4]], // columns to merge in summary
                            'content'=>[             // content to show in each summary cell
                                1=>'Summary (' . $model->nama_fg . ')',
                                //7=>GridView::F_SUM,
                                // 8=>GridView::F_AVG,
                                // 9=>' hrs',
                                //9=>GridView::F_SUM,
                            ],
                            'contentFormats'=>[      // content reformatting for each summary cell
                                //7=>['format'=>'number', 'decimals'=>2],
                                8=>['format'=>'number', 'decimals'=>1],
                                //8=>['append'=>' hrs'],
                                //9=>['format'=>'number', 'decimals'=>1],
                            ],
                            'contentOptions'=>[      // content html attributes for each summary cell
                                1=>['style'=>'font-variant:medium-caps;font-size: 20px'],
                                // 7=>['style'=>'text-align:right'],
                                8=>['style'=>'text-align:right'],
                                // 9=>['style'=>'text-align:right'],
                            ],
                            // html attributes for group summary row
                            'options'=>['class'=>'success','style'=>'font-weight:bold;']
                        ];
                    }
                ],
                [
                    'attribute'=>'nama_fg', 
                    'width'=>'210px',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(AchievementLeadtimeKemas2::find()->orderBy('nama_fg')->asArray()->all(), 'nama_fg', 'nama_fg'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Nama Finished Good'],
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>1,
                ],
                [

                    'attribute'=>'snfg', 
                    'width'=>'210px',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(AchievementLeadtimeKemas2::find()->orderBy('nomo')->asArray()->all(), 'nomo', 'nomo'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Nomor MO'],
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>2,
                ],
                'nama_bulk',
                [
                    'attribute'=>'achievement', 
                    'label' => 'Achievement',
                    'width'=>'210px',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>3,
                    'format' => 'raw',
                    // 'headerOptions' => ['style' => 'text-align: center;'],
                    'contentOptions' => function($model){
                    // Pewarnaan untuk Posisi
                                            if($model->achievement==null)
                                            {
                                                return ['style'=>'font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                                            }            
                                            else if($model->achievement < 70)
                                            {
                                                return ['style'=>'font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                                            }
                                            else if($model->achievement >= 70 && $model->achievement <90)
                                            {
                                                return ['style'=>'font-weight:bold;width: 150px;font-size: 18px','class'=>'warning'];
                                            }
                                            else if($model->achievement >=90)
                                            {
                                                return ['style'=>'font-weight:bold;width: 150px;font-size: 18px','class'=>'success'];
                                            }

                    },
                ],
                // [
                //     'attribute'=>'tanggal_stop', 
                //     'width'=>'210px',
                //     'group'=>true,  // enable grouping
                //     'subGroupOf'=>4,
                // ],
                // 'jenis_kemas',
                // 'lanjutan',
                // 'nama_operator',
                // 'tanggal',
                // 'koitem_bulk',
                // 'koitem_fg',
                // 'is_done',
                // 'delta_minute_net',
                // 'delta_hour_net',
                // 'lanjutan_split_batch',
                // 'jumlah_realisasi',
                // 'jam_per_pcs',
                // 'mpq',
             ],
        ]);
    
        ?>

</div>
