<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLeadtimeKemas2 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Achievement Leadtime Kemas2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-leadtime-kemas2-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nomo',
            'snfg',
            'snfg_komponen',
            'jenis_kemas',
            'lanjutan',
            'nama_line',
            'nama_operator',
            'tanggal',
            'tanggal_stop',
            'delta_minute',
            'delta_hour',
            'koitem_bulk',
            'koitem_fg',
            'nama_fg',
            'nama_bulk',
            'is_done',
            'kendala',
            'delta_minute_net',
            'delta_hour_net',
            'lanjutan_split_batch',
            'jumlah_realisasi',
            'jam_per_pcs',
            'mpq',
            'achievement',
            'id',
        ],
    ]) ?>

</div>
