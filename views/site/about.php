<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <p>
        Flowreport is an open source in-house production tracking system developed using open source assets. Released under the terms of BSD License. 
        <br \>
        <br \>
        Flowreport components :
		<br \>
        <br \>
        1. Yii2 PHP Framework : <code>https://github.com/yiisoft/yii2/blob/master/</code><p \>
        2. AdminLTE2 Assets Bundle : <code>https://github.com/dmstr/yii2-adminlte-asset ; https://github.com/almasaeed2010/AdminLTE</code><p \>
        3. Airbnb-Superset : <code>https://github.com/airbnb/superset/</code><p \>
        4. Kartik Yii2 Extensions : <code>https://github.com/kartik-v/</code><p \>
        5. PostgreSQL Database<p \>
        <br \>
        <br \>
        Team : 
		<br \>
        <br \>
        1. Redha Hari (IT) - Full-Stack Developer<p />
        2. Arief Ramadhani (IT) - Developer<p />
        3. Kukuh Damareza (MEX) - Product Owner<p />

    </p>
</div>
