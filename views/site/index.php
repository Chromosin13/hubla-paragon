<?php
use yii\helpers\Html;
use app\models\User;
/* @var $this yii\web\View */

// $this->title = 'PT Paragon Technology & Innovation';
?>

<div class="site-index">



<?php //echo Yii::$app->user->identity->role; ?>




<?php

if(in_array(Yii::$app->user->identity->username,['ndc','jit'])){


} else if(in_array(Yii::$app->user->identity->username,['karantina','ndc','jit'])){
  echo '
  <div class="foo">
    <span class="letter" data-letter="F">F</span>
    <span class="letter" data-letter="L">L</span>
    <span class="letter" data-letter="O">O</span>
    <span class="letter" data-letter="W">W</span>
    <span class="letter" data-letter=".">.</span>
  </div>
  <br>
  <br>
  <br>
  <div style="margin-left:30%;">
          <div class="col-sm-3">
              <div class="zoom">
                                      <div class="image">
                                      <figure><a href="index.php?r=live-dashboard"><img class="img" src="../web/images/proses_icons/live-dashboard.svg" style="width:70%; margin-left:15%"></a>
                                        <p></p>
                                        <figcaption style="text-align:center"><b>LIVE DASHBOARD KFG</b></figcaption>
                                      </figure>
                                      </div>
              </div>
          </div>

          </div>

      <div class="body-content">
    ';

} else if (in_array(Yii::$app->user->identity->username,['sampler'])) {
  echo '
  <div class="foo">
    <span class="letter" data-letter="F">F</span>
    <span class="letter" data-letter="L">L</span>
    <span class="letter" data-letter="O">O</span>
    <span class="letter" data-letter="W">W</span>
    <span class="letter" data-letter=".">.</span>
  </div>
  <br>
  <br>
  <br>
  <div style="margin-left:30%;">
          <div class="col-sm-3">
              <div class="zoom">
                                      <div class="image">
                                      <figure><a href="index.php?r=sampling-label/scan-qr"><img class="img" src="../web/images/scanner.png" style="width:70%; margin-left:15%"></a>
                                        <p></p>
                                        <figcaption style="text-align:center"><b>SAMPLING LABEL</b></figcaption>
                                      </figure>
                                      </div>
              </div>
          </div>

          </div>

      <div class="body-content">
    ';

} else if (in_array(Yii::$app->user->identity->username,['inspektor'])){
echo '
<div class="foo">
  <span class="letter" data-letter="F">F</span>
  <span class="letter" data-letter="L">L</span>
  <span class="letter" data-letter="O">O</span>
  <span class="letter" data-letter="W">W</span>
  <span class="letter" data-letter=".">.</span>
</div>
<br>
<br>

        <div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=pengecekan-umum/scan-snfg-inspeksi"><img class="img" src="../web/images/clipboard-icon.png" alt="Trulli" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>INSPEKSI FG</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>



    <div class="body-content">
  ';
} else if (strpos(Yii::$app->user->identity->role, 'pe-') !== false) {
echo '
<div class="foo">
  <span class="letter" data-letter="F">F</span>
  <span class="letter" data-letter="L">L</span>
  <span class="letter" data-letter="O">O</span>
  <span class="letter" data-letter="W">W</span>
  <span class="letter" data-letter=".">.</span>
</div>
<br>
<br>
        <div class="row">';
        if (strpos(Yii::$app->user->identity->role, 'pe-creator') !== false or strpos(Yii::$app->user->identity->role, 'pe-creator-reviewer') !== false){
          echo '<div class="col-sm-4">
              <div class="zoom" style="width:50%">
                                      <div class="image" style="text-align:center">
                                      <figure><a href="index.php?r=log-formula-breakdown/verification-history"><img class="img" src="../web/images/proses_icons/folder.png" style="width:50%"></a>
                                        <p></p>
                                        <figcaption><b>VERIFICATION HISTORY</b></figcaption>
                                      </figure>
                                      </div>
              </div>
          </div>
          <div class="col-sm-4">
              <div class="zoom" style="width:50%">
                                      <div class="image" style="text-align:center">
                                      <figure><a href="index.php?r=log-formula-breakdown/schedule-list"><img class="img" src="../web/images/proses_icons/edit.png" alt="Trulli" style="width:50%"></a>
                                        <p></p>
                                        <figcaption><b>VERIFIKASI JADWAL</b></figcaption>
                                      </figure>
                                      </div>
              </div>
          </div>';
        }
        if (strpos(Yii::$app->user->identity->role, 'pe-reviewer') !== false or strpos(Yii::$app->user->identity->role, 'pe-creator-reviewer') !== false){
          echo '<div class="col-sm-4">
              <div class="zoom" style="width:50%">
                                  <div class="image" style="text-align:center">
                                  <figure><a href="index.php?r=log-formula-breakdown/list-schedule-reviewer"><img class="img" src="../web/images/proses_icons/correct.png" alt="Trulli" style="width:50%"></a>
                                    <p></p>
                                    <figcaption><b>REVIEWER</b></figcaption>
                                  </figure>
                                  </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="zoom" style="width:50%">
                                  <div class="image" style="text-align:center">
                                  <figure><a href="index.php?r=master-bahan-baku-repack/index"><img class="img" src="../web/images/raw-material.png" alt="Trulli" style="width:50%"></a>
                                    <p></p>
                                    <figcaption><b>MASTER DATA REPACK</b></figcaption>
                                  </figure>
                                  </div>
              </div>
            </div>';
        }
        if (strpos(Yii::$app->user->identity->role, 'pe-approver') !== false){
          echo '<div class="col-sm-4">
              <div class="zoom" style="width:50%">
                                  <div class="image" style="text-align:center">
                                  <figure><a href="index.php?r=log-formula-breakdown/list-schedule-approver"><img class="img" src="../web/images/proses_icons/correct.png" alt="Trulli" style="width:50%"></a>
                                    <p></p>
                                    <figcaption><b>APPROVER</b></figcaption>
                                  </figure>
                                  </div>
              </div>
          </div>';
        }
        if (strpos(Yii::$app->user->identity->role, 'pe-creator-reviewer-approver') !== false){
          echo '<div class="col-sm-4">
              <div class="zoom" style="width:50%">
                                  <div class="image" style="text-align:center">
                                  <figure><a href="index.php?r=log-formula-breakdown/list-schedule-approver"><img class="img" src="../web/images/proses_icons/correct.png" alt="Trulli" style="width:50%"></a>
                                    <p></p>
                                    <figcaption><b>APPROVER</b></figcaption>
                                  </figure>
                                  </div>
              </div>
          </div>';
        }

      echo '</div>


    <div class="body-content">
  ';
} else if (in_array(Yii::$app->user->identity->role,['checker'])){
echo '
<div class="foo">
  <span class="letter" data-letter="F">F</span>
  <span class="letter" data-letter="L">L</span>
  <span class="letter" data-letter="O">O</span>
  <span class="letter" data-letter="W">W</span>
  <span class="letter" data-letter=".">.</span>
</div>
<br>
<br>
        <div class="col-sm-4">
            <div class="zoom" style="width:50%">
                                    <div class="image" style="text-align:center">
                                    <figure><a href="index.php?r=log-task-checker/check-nomo-checker"><img class="img" src="../web/images/proses_icons/edit.png" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>SCAN NOMO</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>


    <div class="body-content">
  ';
} else if (in_array(Yii::$app->user->identity->username,['station'])) {
  echo '
  <div class="foo">
    <span class="letter" data-letter="L">L</span>
    <span class="letter" data-letter="A">A</span>
    <span class="letter" data-letter="B">B</span>
    <span class="letter" data-letter="E">E</span>
    <span class="letter" data-letter="L">L</span>
    <span class="letter" data-letter=" "> </span>
    <span class="letter" data-letter="P">P</span>
    <span class="letter" data-letter="R">R</span>
    <span class="letter" data-letter="I">I</span>
    <span class="letter" data-letter="N">N</span>
    <span class="letter" data-letter="T">T</span>
    <span class="letter" data-letter="E">E</span>
    <span class="letter" data-letter="R">R</span>
  </div>
  <br>
  <br>
  <br>
  <div style="margin-left:30%;">
          <div class="col-sm-3">
              <div class="zoom">
                                      <div class="image">
                                      <figure><a href="index.php?r=karbox-label-print/check-snfg"><img class="img" src="../web/images/box.png" style="width:70%; margin-left:15%"></a>
                                        <p></p>
                                        <figcaption style="text-align:center"><b>LABEL KARBOX</b></figcaption>
                                      </figure>
                                      </div>
              </div>
          </div>
          <div class="col-sm-3">
              <div class="zoom">
                                      <div class="image">
                                      <figure><a href="index.php?r=innerbox-label-print/check-snfg"><img class="img" src="../web/images/box(1).png" alt="Trulli" style="width:70%; margin-left:15%"></a>
                                        <p></p>
                                        <figcaption style="text-align:center"><b>LABEL INNERBOX</b></figcaption>
                                      </figure>
                                      </div>
              </div>
          </div>
          </div>

      <div class="body-content">
    ';

} else if (in_array(Yii::$app->user->identity->role,['malaysia'])) {
  echo '
  <div class="foo">
    <span class="letter" data-letter="L">L</span>
    <span class="letter" data-letter="A">A</span>
    <span class="letter" data-letter="B">B</span>
    <span class="letter" data-letter="E">E</span>
    <span class="letter" data-letter="L">L</span>
    <span class="letter" data-letter=" "> </span>
    <span class="letter" data-letter="P">P</span>
    <span class="letter" data-letter="R">R</span>
    <span class="letter" data-letter="I">I</span>
    <span class="letter" data-letter="N">N</span>
    <span class="letter" data-letter="T">T</span>
    <span class="letter" data-letter="E">E</span>
    <span class="letter" data-letter="R">R</span>
  </div>
  <br>
  <br>
  <br>
    ';

} else if (in_array(Yii::$app->user->identity->username,['spr'])){
echo '
<div class="foo">
  <span class="letter" data-letter="F">F</span>
  <span class="letter" data-letter="L">L</span>
  <span class="letter" data-letter="O">O</span>
  <span class="letter" data-letter="W">W</span>
  <span class="letter" data-letter=".">.</span>
</div>
<br>
<br>
        <div class="col-sm-4">
            <div class="zoom" style="width:50%">
                                    <div class="image" style="text-align:center">
                                    <figure><a href="index.php?r=bulk-sisa/"><img class="img" src="../web/images/proses_icons/recycle.svg" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>BULK SISA</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>


    <div class="body-content">
  ';
} else if (in_array(Yii::$app->user->identity->username,['wpm'])){
echo '
<div class="foo">
  <span class="letter" data-letter="F">F</span>
  <span class="letter" data-letter="L">L</span>
  <span class="letter" data-letter="O">O</span>
  <span class="letter" data-letter="W">W</span>
  <span class="letter" data-letter=".">.</span>
</div>
<br>
<br>
        <div class="col-sm-4">
            <div class="zoom" style="width:50%">
                                    <div class="image" style="text-align:center">
                                    <figure><a href="index.php?r=bulk-sisa/"><img class="img" src="../web/images/proses_icons/recycle.svg" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>BULK SISA</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>


    <div class="body-content">
  ';
} else if (in_array(Yii::$app->user->identity->username,['gbb'])){
echo '
<div class="foo">
  <span class="letter" data-letter="F">F</span>
  <span class="letter" data-letter="L">L</span>
  <span class="letter" data-letter="O">O</span>
  <span class="letter" data-letter="W">W</span>
  <span class="letter" data-letter=".">.</span>
</div>
<br>
<br>
        <div class="col-sm-4">
            <div class="zoom" style="width:50%">
                                    <div class="image" style="text-align:center">
                                    <figure><a href="index.php?r=flow-input-gbb/check-flow-gbb"><img class="img" src="../web/images/trolley.png" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>Supply RM</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>


    <div class="body-content">
  ';
} else if(in_array(Yii::$app->user->identity->username,['kemas'])){
echo '
<div style="text-align:center"><img class="img" src="../web/images/prodigy-2.png" style="width:60%"></div>


    <div class= "row" style="justify-content : center;">
        <div class="col-sm-2">
            <div class="zoom">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=flow-pra-kemas/check-kemas"><img class="img" src="../web/images/proses_icons/packing.png" alt="Trulli" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>PRA KEMAS</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=flow-input-snfgkomponen/check-kemas1"><img class="img" src="../web/images/proses_icons/packing.png" alt="Trulli" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>KEMAS 1</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=flow-input-snfg/check-kemas2"><img class="img" src="../web/images/kemas-2.png" alt="Trulli" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>KEMAS 2</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=pengecekan-umum/scan-snfg-inspeksi"><img class="img" src="../web/images/clipboard-icon.png" alt="Trulli" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>INSPEKSI FG</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>

    </div>

<br><br>
    <div class="row">
        <div class="col-sm-2">
            <div class="zoom">
            </div>
        </div>
        
        <div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=qc-fg-v2/create"><img class="img" src="../web/images/proses_icons/qcfg.png" alt="Trulli" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>QC FG</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=flow-cuci-mesin/create"><img class="img" src="../web/images/water.png" alt="Trulli" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>CUCI MESIN</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>';
        if (in_array(Yii::$app->user->identity->username,["kemas",'planner','qcbulk'])) {
          echo '
          <div class="col-sm-2">
              <div class="zoom">
                                      <div class="image">
                                        <figure><a href="index.php?r=bulk-sisa/"><img class="img" src="../web/images/proses_icons/recycle.svg" style="width:50%"></a>
                                          <p></p>
                                          <figcaption><b>BULK SISA</b></figcaption>
                                        </figure>
                                      </div>
              </div>
          </div>';
        }
    echo '<div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=schedule-not-hit/index"><img class="img" src="../web/images/schedule-not-hit.png" alt="Trulli" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>Loss Tree</b></figcaption>
                                    </figure>
                                    </div>
            </div>
          </div>
      </div>

    <div class="body-content">
  ';
}else{
echo '
<div style="text-align:center"><img class="img" src="../web/images/prodigy-2.png" style="width:60%"></div>


    <div class= "row">
        <div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=flow-input-mo/check-penimbangan"><img class="img" src="../web/images/proses_icons/weighing.png" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>WEIGHING</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=flow-input-mo/check-pengolahan"><img class="img" src="../web/images/proses_icons/mixer.png" alt="Trulli" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>MIXING</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="zoom">
                                <div class="image">
                                <figure><a href="index.php?r=qc-bulk-entry%2Fcreate"><img class="img" src="../web/images/proses_icons/list.png" alt="Trulli" style="width:50%"></a>
                                  <p></p>
                                  <figcaption><b>QC BULK</b></figcaption>
                                </figure>
                                </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=flow-pra-kemas/check-kemas"><img class="img" src="../web/images/proses_icons/packing.png" alt="Trulli" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>PRA KEMAS</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=flow-input-snfgkomponen/check-kemas1"><img class="img" src="../web/images/proses_icons/packing.png" alt="Trulli" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>KEMAS 1</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=flow-input-snfg/check-kemas2"><img class="img" src="../web/images/kemas-2.png" alt="Trulli" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>KEMAS 2</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>

    </div>

<br><br>
    <div class="row">
        <div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=pengecekan-umum/scan-snfg-inspeksi"><img class="img" src="../web/images/clipboard-icon.png" alt="Trulli" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>INSPEKSI FG</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=qc-fg-v2/create"><img class="img" src="../web/images/proses_icons/qcfg.png" alt="Trulli" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>QC FG</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=flow-cuci-mesin/create"><img class="img" src="../web/images/water.png" alt="Trulli" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>CUCI MESIN</b></figcaption>
                                    </figure>
                                    </div>
            </div>
        </div>';
        if (in_array(Yii::$app->user->identity->username,["kemas",'planner','qcbulk'])) {
          echo '
          <div class="col-sm-2">
              <div class="zoom">
                                      <div class="image">
                                        <figure><a href="index.php?r=bulk-sisa/"><img class="img" src="../web/images/proses_icons/recycle.svg" style="width:50%"></a>
                                          <p></p>
                                          <figcaption><b>BULK SISA</b></figcaption>
                                        </figure>
                                      </div>
              </div>
          </div>';
        }
    echo '
    <div class="col-sm-2">
            <div class="zoom">
                                    <div class="image">
                                    <figure><a href="index.php?r=schedule-not-hit/index"><img class="img" src="../web/images/schedule-not-hit.png" alt="Trulli" style="width:50%"></a>
                                      <p></p>
                                      <figcaption><b>Loss Tree</b></figcaption>
                                    </figure>
                                    </div>
            </div>
          </div>
        </div>

    <div class="body-content">
  ';
}
?>


<!--         <div class="row">
            <div class="col-lg-4">
                <h2>Powered By Yii 2 Framework</h2>

                <p>.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Theme by AdminLTE 2</h2>

                <p>.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Open Source</h2>

                <p>MIT License.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div> -->

    </div>
</div>
<?php
$script = <<< JS
$(document).ready(function() {
  document.cookie = "OS="+platform.os.family;
})
JS;
$this->registerJs($script);
?>
