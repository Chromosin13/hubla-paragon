<?php
use Yii;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div class="row">
	<div class="col-sm-6">
		<?php
		if(Yii::$app->session->hasFlash('success')){
			echo "<div class = 'alert alert-success'>".Yii::$app->session->getFlash('success')."</div>";
		} 
		?>
		<?php 
		$form = ActiveForm::begin([
			'method' => 'post',
			'action' => Url::to(['/site/karyawan']),
		]);
		?>
		<?= $form->field($model,'nama')->textInput() ?>

		<?= $form->field($model,'bagian')->dropDownlist($model->dataBagian(),[
			'class' => 'form_control','prompt'=>'Pilih Bagian']) ?>

		<?= $form->field($model,'email')->textInput() ?>

		<div class="form_group">
			<?= Html::submitButton('Simpan',['class' => 'btn btn-success']) ?>
		</div>
		<?php
		ActiveForm::end(); 
		?>
	</div>
	
</div>