<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FlowCuciMesin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="flow-cuci-mesin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_line')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'nobatch')->textInput() ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'operator')->textInput() ?>

    <?= $form->field($model, 'jenis_cuci')->textInput() ?>

    <?= $form->field($model, 'verifikator')->textInput() ?>

    <?= $form->field($model, 'gombalan')->textInput() ?>

    <?= $form->field($model, 'sarung_tangan')->textInput() ?>

    <?= $form->field($model, 'detergen')->textInput() ?>

    <?= $form->field($model, 'parafin')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
