<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FlowCuciMesinSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="flow-cuci-mesin-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nama_line') ?>

    <?= $form->field($model, 'nama_fg') ?>

    <?= $form->field($model, 'nobatch') ?>

    <?= $form->field($model, 'snfg') ?>

    <?php // echo $form->field($model, 'operator') ?>

    <?php // echo $form->field($model, 'jenis_cuci') ?>

    <?php // echo $form->field($model, 'verifikator') ?>

    <?php // echo $form->field($model, 'gombalan') ?>

    <?php // echo $form->field($model, 'sarung_tangan') ?>

    <?php // echo $form->field($model, 'detergen') ?>

    <?php // echo $form->field($model, 'parafin') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
