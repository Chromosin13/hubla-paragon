<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\QcBulkEntry;
use kartik\widgets\ActiveField;
use faryshta\widgets\JqueryTagsInput;
use wbraganca\tagsinput\TagsinputWidget;
use yii\bootstrap\Modal;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\QcBulkEntry */
/* @var $form yii\widgets\ActiveForm */
?>

    <?php $form = ActiveForm::begin(); ?>

    <?php
        Modal::begin([
            'header'=>'<h4>Update Model</h4>',
            'id'=>'update-modal',
            'size'=>'modal-lg'
        ]);

        echo "<div id='updateModalContent'></div>";

        Modal::end();
    ?>
    <div id="hidden-field">
        <?= $form->field($model, 'nomo')->textInput(['value' => $nomo,'readOnly'=>true]) ?>
        <?php

        echo  $form->field($model, 'nourut')->textInput(['value' => $nourut,'readOnly'=>true])
        ?>
    </div>


    <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-green">
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/zoom-in-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username"><b>QC BULK ENTRY MO</b></h2>
              <h5 class="widget-user-desc"><?=$nomo?> - <?=$nama_fg?></h5>
            </div>
                <p></p>
                <!-- Form Nama Inspektor -->
                <div id="ni-body" class="box-body">
                        <?= $form->field($model, 'nama_inspektor')->widget(TagsinputWidget::classname(), [
                            'clientOptions' => [
                                'trimValue' => true,
                                'allowDuplicates' => false,
                                'maxChars'=> 4,
                                'minChars'=> 4,
                            ]
                        ]) ?>

                        <div id="add" class="zoom"><?= Html::submitButton($model->isNewRecord ? 'ADD' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?></div>
                </div>

    </div>





    <div id="form-add" class="form-group">
        <?=Html::a('HOME', ['qc-bulk-entry/create'], ['class' => 'btn btn-primary']);?>
    </div>




    <?php ActiveForm::end(); ?>

</div>

<!-- Gridview -->
<div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'kartik\grid\SerialColumn'],
            'nourut',
            [
                'class' => 'kartik\grid\ActionColumn',
                // 'options'=>['class'=>'action-column'],
                'template' => '{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                        $btn = Html::button('<img class="img-circle" src="../web/images/adjust.png" alt="Terima" width="21" height="21">',[
                            'value'=>Url::to('index.php?r=qc-bulk-entry/update&id='.$key),
                            // Yii::$app->urlManager->createUrl('qc-bulk-entry/update'), //<---- here is where you define the action that handles the ajax request
                            'class'=>'update-modal-click grid-action',
                            'data-toggle'=>'tooltip',
                            'data-placement'=>'bottom',
                            'title'=>'Update'
                        ]);
                        return $btn;
                    }
                ],
                'visibleButtons' => [
                    'update' => function($model,$key,$index){
                                if($model->status=="ADJUST"){
                                    return true;
                                } else {
                                    return false;
                                }
                    },
                ],
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{cek-homogenitas} {adjust} {release} {release-uncomformity} {pending1} {pending2} {rework} {parallel-mikro} {pending-mikro}',
                'contentOptions' => ['style' => 'min-width:150px;'],
                'buttons' => [
                    'cek-homogenitas' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/cek-homogenitas.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'CEK HOMOGENITAS',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Cek Homogenitas?'),
                            ]
                        );
                    },
                    'adjust' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/adjust.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'ADJUST',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Adjust?'),
                            ]
                        );
                    },
                    'release' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/release.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'RELEASE',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'RELEASE?'),
                            ]
                        );
                    },
                    'release-uncomformity' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/release-uncomformity.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'RELEASE UNCOMFORMITY',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Release Uncomformity?'),
                            ]
                        );
                    },
                    'pending1' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/pending-1.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'PENDING 1',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Pending 1?'),
                            ]
                        );
                    },
                    'pending2' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/pending-2.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'PENDING 2',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Pending 2?'),
                            ]
                        );
                    },
                    'rework' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/rework.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'REWORK',
                            ]
                        );
                    },
                    'parallel-mikro' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/parallel-mikro.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'PARALLEL MIKRO',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Parallel Mikro?'),
                            ]
                        );
                    },
                    'pending-mikro' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/pending-mikro.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'PENDING MIKRO',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Pending Mikro?'),
                            ]
                        );
                    },
                ],
                'visibleButtons' => [
                    'cek-homogenitas' => function($model,$key,$index){
                                if(empty($model->status)){
                                    return true;
                                } else {
                                    return false;
                                }
                    },
                    'adjust' => function($model,$key,$index){
                                if(empty($model->status)){
                                    return true;
                                } else {
                                    return false;
                                }
                    },
                    'release' => function($model,$key,$index){
                                if(empty($model->status)){
                                    return true;
                                } else {
                                    return false;
                                }
                    },
                    'release-uncomformity' => function($model,$key,$index){
                                if(empty($model->status)){
                                    return true;
                                } else {
                                    return false;
                                }
                    },
                    'pending1' => function($model,$key,$index){
                                if(empty($model->status)){
                                    return true;
                                } else {
                                    return false;
                                }
                    },
                    'pending2' => function($model,$key,$index){
                                if(empty($model->status)){
                                    return true;
                                } else {
                                    return false;
                                }
                    },
                    'rework' => function($model,$key,$index){
                                if(empty($model->status)){
                                    return true;
                                } else {
                                    return false;
                                }
                    },
                    'parallel-mikro' => function($model,$key,$index){
                                if(empty($model->status)){
                                    return true;
                                } else {
                                    return false;
                                }
                    },
                    'pending-mikro' => function($model,$key,$index){
                                if(empty($model->status)){
                                    return true;
                                } else {
                                    return false;
                                }
                    },
                ],
            ],
            'nomo',
            [
                    'attribute' => 'status',
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'contentOptions' => function($model){
                             if($model->status == 'ADJUST')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color: #A9A9A9'];
                                                }
                            else if($model->status == 'PENDING 1')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color: #FFFF00'];
                                                }
                            else if($model->status == 'PENDING 2')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color: #FF7F50'];
                                                }
                            else if($model->status == 'REWORK')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color: #B22222'];
                                                }
                            else if($model->status == 'RELEASE')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color:     #228B22'];
                                                }
                            else if($model->status == 'CEK MIKRO')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color:     #000080'];
                                                }
                            else if($model->status == 'PARALLEL MIKRO')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color:     #00ffff'];
                                                }
                            else if($model->status == 'PENDING MIKRO')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color:     #ff00ff'];
                                                }
                            else if($model->status == 'RELEASE UNCOMFORMITY')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color:     #7CFC00'];
                                                }
                            else if($model->status == 'CEK HOMOGENITAS')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color:     #cc6699'];
                                                }
                            else {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px','class'=>'default'];
                            }
                    },
            ],
            'created_at',
            'timestamp',
            [
                    'class'=>'kartik\grid\EditableColumn',
                    'attribute'=>'nama_inspektor',
                    'label' => 'Nama Inspektor',
                    'editableOptions' => [
                        'asPopover' => false,
                    ],
            ],
            'jenis_adjust',
            'keterangan_adjust',
            ['class' => 'kartik\grid\ActionColumn',
             'template' => '{delete}'],


        ],
    ]); ?>

<br \>
</div>





<div id="next">
        <?=Html::a('NEXT', ['qc-bulk-entry-komponen/create-rincian', 'nomo' => $nomo], ['class'=>'btn btn-success btn-block']) ;?>
</div>



<?php
$script = <<< JS


// Hide Create new Field

    $('#hidden-field').hide();
    $('#next').hide();


// Get Value of Nomo

    var nomo = $('#qcbulkentry-nomo').val();

    $.get('index.php?r=qc-bulk-entry/check-release',{ nomo : nomo },function(data){
            var data = $.parseJSON(data);
            var nomo = $('#qcbulkentry-nomo').val();
            if(data.id){
                $('#add').hide();
                $('#next').fadeIn("slow");
            }else{
                 $('#add').fadeIn("slow");
                 $('#next').hide();
            }

    });


$(function () {
    $('.update-modal-click').click(function () {
        $('#update-modal')
            .modal('show')
            .find('#updateModalContent')
            .load($(this).attr('value'));
    });
});

JS;
$this->registerJs($script);
?>
