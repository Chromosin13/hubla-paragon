<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FlowCuciMesin */

$this->title = 'Update Flow Cuci Mesin: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Flow Cuci Mesins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="flow-cuci-mesin-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
