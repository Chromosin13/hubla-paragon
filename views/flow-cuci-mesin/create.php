<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\QcBulkEntry */

// $this->title = 'Create Qc Bulk Entry';
// $this->params['breadcrumbs'][] = ['label' => 'Qc Bulk Entries', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="flow-cuci-mesin-create">

<div>
    <div class="box box-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-blue-gradient" style = "background-color:#ACECD5">
        <h3 class="widget-user-username"><b>Cuci Mesin OLAH</b></h3>
        <h5 class="widget-user-desc">Work Order</h5>
      </div>
      <div class="widget-user-image">
        <img class="img-circle" src="../web/images/proses_icons/file.png" alt="User Avatar">
      </div>

      <div class="box-footer">
        <div class="row">
          <div class="box-body">
          <div class="box-body">
            <div class="zoom">
              <?=Html::a('SCAN WITH BARCODE', ['flow-cuci-mesin/create-barcode'], ['class' => 'btn bg-maroon btn-block']);?>
                <!-- <?=Html::a('SCAN WITH QR', ['flow-cuci-mesin/create-tab'], ['class' => 'btn bg-navy btn-block']);?> -->
            </div>
            <p></p>
              <div class="box-body">
                 <?= $this->render('_form', [
                      'model' => $model,
                  ]) ?>

              </div><!-- /.box-body -->
            </div><!-- /.box -->
            </div><!-- /.box -->
          </div>
      </div>
    </div>
</div>

<div>
    <div class="box box-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-aqua" style = "background-color:#ACECD5">
        <h3 class="widget-user-username"><b>Cuci Mesin KEMAS</b></h3>
        <h5 class="widget-user-desc">Work Order</h5>
      </div>
      <div class="widget-user-image">
        <img class="img-circle" src="../web/images/proses_icons/file.png" alt="User Avatar">
      </div>

      <div class="box-footer">
        <div class="row">
          <div class="box-body">
          <div class="box-body">
            <div class="zoom">
              <?=Html::a('SCAN WITH BARCODE', ['flow-cuci-mesin-kemas/create-barcode'], ['class' => 'btn bg-maroon btn-block']);?>
                <!-- <?=Html::a('SCAN WITH QR', ['flow-cuci-mesin/create-tab'], ['class' => 'btn bg-navy btn-block']);?> -->
            </div>
            <p></p>
              <div class="box-body">
                 <?= $this->render('_form-kemas', [
                      'model' => $model2,
                  ]) ?>

              </div><!-- /.box-body -->
            </div><!-- /.box -->
            </div><!-- /.box -->
          </div>
      </div>
    </div>
</div>

</div>
