<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IotDashboardWeigherSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="iot-dashboard-weigher-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'nama_line') ?>

    <?= $form->field($model, 'nama_operator') ?>

    <?= $form->field($model, 'nama_operator_real') ?>

    <?= $form->field($model, 'jenis_kemas') ?>

    <?php // echo $form->field($model, 'nobatch') ?>

    <?php // echo $form->field($model, 'is_done') ?>

    <?php // echo $form->field($model, 'is_done_wo') ?>

    <?php // echo $form->field($model, 'write_time') ?>

    <?php // echo $form->field($model, 'flow_input_snfg_id') ?>

    <?php // echo $form->field($model, 'jumlah_wo') ?>

    <?php // echo $form->field($model, 'pass') ?>

    <?php // echo $form->field($model, 'less') ?>

    <?php // echo $form->field($model, 'over') ?>

    <?php // echo $form->field($model, 'rft') ?>

    <?php // echo $form->field($model, 'error_rate') ?>

    <?php // echo $form->field($model, 'success_rate') ?>

    <?php // echo $form->field($model, 'uid') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
