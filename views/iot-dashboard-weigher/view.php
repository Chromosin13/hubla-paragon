<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\IotDashboardWeigher */

$this->title = $model->uid;
$this->params['breadcrumbs'][] = ['label' => 'Iot Dashboard Weighers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iot-dashboard-weigher-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->uid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->uid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'snfg',
            'nama_line',
            'nama_operator',
            'nama_operator_real',
            'jenis_kemas',
            'nobatch',
            'is_done',
            'is_done_wo',
            'write_time',
            'flow_input_snfg_id',
            'jumlah_wo',
            'pass',
            'less',
            'over',
            'rft',
            'error_rate',
            'success_rate',
            'uid',
        ],
    ]) ?>

</div>
