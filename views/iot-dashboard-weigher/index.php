<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IotDashboardWeigherSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>



<?php 

foreach($data as $dat){
        // print_r($dat->nama_operator);

        echo '
        <div class="col-xl-3 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
              <div class="card-body">
                <img class="card-img-iotline" src="./images/line/IWK.jpg" alt="Card image cap">
                <div class="row">
                  <div class="col">
                    <h3 class="card-title text-uppercase mb-0">'.$dat->nama_line.'</h5>
                    <h5 class="card-title text-uppercase mb-0">'.$dat->snfg.'</h5>
                    <h5 class="card-title text-uppercase mb-0 text-muted"> Batch : '.$dat->nobatch.'</h5>
                    <h5 class="card-title text-uppercase mb-0 text-muted"> Operator : '.$dat->nama_operator_real.'</h5>
                    <p></p>
                    <h5 class="card-title text-uppercase mb-0"> Jumlah Check : '.$dat->jumlah_wo.' Pcs</h5>
                    <h5 class="card-title text-uppercase mb-0 text-success"> Success Rate : '.$dat->success_rate.' %</h5>
                    <h5 class="card-title text-uppercase mb-0 text-danger"> Error Rate : '.$dat->error_rate.' %</h5>
                    <h5 class="card-title text-uppercase mb-0 text-muted"> RFT : '.$dat->rft.' Pcs</h5>


                  </div>
                  <div class="col-auto">';                    
                        if($dat->is_done_wo==1){
                            echo '<div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                      <i class="fas fa-stop"></i>';            
                        }else{
                            echo '<div class="icon icon-shape bg-success text-white rounded-circle shadow">
                                      <i class="fas fa-play"></i>';
                        }
    
        echo   '
                    </div>
                  </div>
                </div>

                <p></p>

                <div class="row">
                        <div class="col-md-4 col-xs-3">
                            <button type="button" class="btn btn-success btn-sm btn-block">P '.$dat->pass.' Pcs</button>
                        </div>
                        <div class="col-md-4 col-xs-3">
                            <button type="button" class="btn btn-warning btn-sm btn-block">L '.$dat->less.' Pcs</button>
                        </div>
                        <div class="col-md-4 col-xs-3">
                            <button type="button" class="btn btn-danger btn-sm btn-block">O '.$dat->over.' Pcs</button>  
                        </div>
                </div>

                <p></p>

                <a href="index.php?r=flow-input-snfg/weigher-fg-preview&id='.$dat->flow_input_snfg_id.'" class="btn btn-primary btn-sm btn-block" role="button">Details</a>

                <p></p>
                <div class="row">
                    <div class="col">
                      <h5 class="card-title text-muted mb-0">Last Update '.$dat->write_time.'</h5>
                    </div>
                </div>


              </div>

            </div>
          </div>

        ';

    }

?>


<?php
$script = <<< JS


JS;
$this->registerJs($script);
?>