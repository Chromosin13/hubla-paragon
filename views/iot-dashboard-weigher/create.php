<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\IotDashboardWeigher */

$this->title = 'Create Iot Dashboard Weigher';
$this->params['breadcrumbs'][] = ['label' => 'Iot Dashboard Weighers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iot-dashboard-weigher-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
