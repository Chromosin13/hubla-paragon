<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IotDashboardWeigher */

$this->title = 'Update Iot Dashboard Weigher: ' . $model->uid;
$this->params['breadcrumbs'][] = ['label' => 'Iot Dashboard Weighers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uid, 'url' => ['view', 'id' => $model->uid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="iot-dashboard-weigher-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
