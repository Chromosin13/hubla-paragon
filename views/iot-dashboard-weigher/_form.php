<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IotDashboardWeigher */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="iot-dashboard-weigher-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'nama_line')->textInput() ?>

    <?= $form->field($model, 'nama_operator')->textInput() ?>

    <?= $form->field($model, 'nama_operator_real')->textInput() ?>

    <?= $form->field($model, 'jenis_kemas')->textInput() ?>

    <?= $form->field($model, 'nobatch')->textInput() ?>

    <?= $form->field($model, 'is_done')->textInput() ?>

    <?= $form->field($model, 'is_done_wo')->textInput() ?>

    <?= $form->field($model, 'write_time')->textInput() ?>

    <?= $form->field($model, 'flow_input_snfg_id')->textInput() ?>

    <?= $form->field($model, 'jumlah_wo')->textInput() ?>

    <?= $form->field($model, 'pass')->passwordInput() ?>

    <?= $form->field($model, 'less')->textInput() ?>

    <?= $form->field($model, 'over')->textInput() ?>

    <?= $form->field($model, 'rft')->textInput() ?>

    <?= $form->field($model, 'error_rate')->textInput() ?>

    <?= $form->field($model, 'success_rate')->textInput() ?>

    <?= $form->field($model, 'uid')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
