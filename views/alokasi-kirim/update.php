<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AlokasiKirim */

$this->title = 'Update Alokasi Kirim: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Alokasi Kirims', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alokasi-kirim-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
