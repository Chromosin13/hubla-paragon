<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AlokasiKirim */

$this->title = 'Create Alokasi Kirim';
$this->params['breadcrumbs'][] = ['label' => 'Alokasi Kirims', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alokasi-kirim-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
