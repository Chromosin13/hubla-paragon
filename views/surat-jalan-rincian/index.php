<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Da\QrCode\QrCode;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SuratJalanRincianSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Surat Jalan Rincians';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surat-jalan-rincian-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Surat Jalan Rincian', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'surat_jalan_id',
            'snfg',
            'nourut',
            'qty',
            // 'nosj',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
