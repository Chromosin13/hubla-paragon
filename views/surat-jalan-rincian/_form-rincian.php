<?php

use app\models\SuratJalan;
use app\models\SuratJalanRincian;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\models\QcFgV2;

/* @var $this yii\web\View */
/* @var $model app\models\SuratJalanRincian */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $sql="
                select
                	nosj
                from surat_jalan
                where id=".$id."
             ";

        $data = SuratJalan::findBySql($sql)->one();

?>

<?php $form = ActiveForm::begin(); ?>
        <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <?php

                if(strpos($data->nosj,'DRAFT')!==false){
                    echo '<div class="widget-user-header bg-yellow">';
                }else{
                    echo '<div class="widget-user-header bg-green">';
                }
            ?>
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/clipboard-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username"><?=$data->nosj?></h2>
              <h5 class="widget-user-desc">Surat Jalan</h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <p \>
                <li><a href="#">Surat Jalan ID <span class="pull-right "><?= $form->field($model, 'surat_jalan_id')->textInput(['style'=>'width:400px;height:30px','value'=>$id,'readOnly'=>true])->label(false) ?></span></a>
                <br \>
                <br \>
                </li>

                <br \>
                <p \>
                <li><a href="#">SNFG <span class="pull-right "><?php

                                    echo $form->field($model, 'snfg')->widget(Select2::classname(), [
                                        'data' => ArrayHelper::map(QcFgV2::find()->where("status = 'SIAP KIRIM KE NDC' and timestamp_check >= ('now'::text::date - '3 months'::interval)")->all()
                                        ,'snfg','snfg'),
                                        'options' => ['placeholder' => 'Select SNFG','style'=>'width:400px;height:30px'],
                                        'pluginOptions' => [
                                            'allowClear' => true

                                        ],
                                    ])->label(false);

                                ?></span></a>
                <br \>
                <br \>
                </li>

                <br \>

                <div id="form-nourut">
	                <li><a href="#">Nomor Urut<span class="pull-right">
	                <?php echo $form->field($model, 'nourut')->widget(Select2::classname(), [
	                                        'data' => ArrayHelper::map(SuratJalanRincian::find()->all()
	                                        ,'nourut','nourut'),
	                                        'options' => ['placeholder' => 'Select Nourut'],
	                                        'pluginOptions' => [
	                                            'allowClear' => true
	                                        ],
	                                    ]);
	                ?></span></a>
	                <br \>
	                <br \>
	                </li>
                </div>

                <br \>


               <!--  <li><a href="#">Completed Projects <span class="pull-right badge bg-green">12</span></a></li>
                <li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li> -->
              </ul>
            </div>
          </div>


    <div id="form-add" class="form-group">


        <span class="pull-right">


        <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>


		</span>

        <?=Html::a('Home', ['surat-jalan/index'], ['class' => 'btn btn-primary']);?>
    </div>


    <?php ActiveForm::end(); ?>


    <?php
    if(strpos($data->nosj,'DRAFT')!==false){
        echo
        GridView::widget([
        'dataProvider' => $dataProvider,
        'responsiveWrap' => false,
        'floatHeader'=>'true',
        'floatHeaderOptions' => ['position' => 'absolute',],
        'columns' => [
            'nama_fg',
            [
                  'attribute' => 'snfg',
                  'label'=>'SNFG',
                  'contentOptions' => function($model){
                      return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                  },
            ],
            [
                  'attribute' => 'nourut',
                  'label'=>'PALET',
                  'contentOptions' => function($model){
                      return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                  },
            ],
            [
                  'attribute' => 'qty',
                  'label'=>'QTY',
                  'contentOptions' => function($model){
                      return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                  },
            ],
            [
                  'attribute' => 'alokasi',
                  'label'=>'Alokasi',
                  'contentOptions' => function($model){
                      return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                  },
            ],
            [
                  'class'=>'kartik\grid\EditableColumn',
                  'attribute' => 'jumlah_koli',
                  'label'=>'Jumlah Koli',
                  'contentOptions' => function($model){
                      return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                  },
                  'editableOptions' => [
                      'asPopover' => false,
                  ],
            ],
            [
                'class'=>'kartik\grid\EditableColumn',
                'attribute'=>'nobatch',
                'label' => 'No Batch',
                'contentOptions' => function($model){
                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];},
                'editableOptions' => [
                    'asPopover' => false,
                ],
            ],
            [
              'class' => 'kartik\grid\ActionColumn', 'template' => '{delete}',
            ],
          ],
        ]);

      }else{

        echo
          GridView::widget([
          'dataProvider' => $dataProvider,
          'responsiveWrap' => false,
          'floatHeader'=>'true',
          'floatHeaderOptions' => ['position' => 'absolute',],
          'columns' => [
              'nama_fg',
              [
                    'attribute' => 'snfg',
                    'label'=>'SNFG',
                    'contentOptions' => function($model){
                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                    },
              ],
              [
                    'attribute' => 'nourut',
                    'label'=>'PALET',
                    'contentOptions' => function($model){
                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                    },
              ],
              [
                    'attribute' => 'qty',
                    'label'=>'QTY',
                    'contentOptions' => function($model){
                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                    },
              ],
              [
                    'attribute' => 'alokasi',
                    'label'=>'Alokasi',
                    'contentOptions' => function($model){
                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                    },
              ],
              [
                    'class'=>'kartik\grid\EditableColumn',
                    'attribute' => 'jumlah_koli',
                    'label'=>'Jumlah Koli',
                    'contentOptions' => function($model){
                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                    },
                    'editableOptions' => [
                        'asPopover' => false,
                    ],
              ],
              [
                  'class'=>'kartik\grid\EditableColumn',
                  'attribute'=>'nobatch',
                  'label' => 'No Batch',
                  'contentOptions' => function($model){
                      return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];},
                  'editableOptions' => [
                      'asPopover' => false,
                  ],
              ],
            ],
          ]);
      }
    ?>


</div><br \>
    <div id="form-confirm">

        <?php

            if(strpos($data->nosj,'DRAFT')!==false){
               echo Html::a('Confirm', ['surat-jalan-rincian/confirm-surat-jalan','id'=>$id], ['class' => 'btn btn-warning btn-block']);
            }
        ?>

    </div>

    <?php

        if(strpos($data->nosj,'DRAFT')===false){
            echo Html::a('PDF Print', ['surat-jalan-rincian/pdf','id'=> $id], ['class' => 'btn btn-danger btn-block']);
        }
    ?>

    <p></p>

    <div id="form-add" class="form-group">


      <?=Html::a('Home', ['surat-jalan/index'], ['class' => 'btn btn-primary btn-block']);?>


    </div>


<?php
$script = <<< JS

$('#form-nourut').hide();

$('#suratjalanrincian-snfg').change(function(){
    var snfg = $(this).val();



    $.post("index.php?r=qc-fg-v2/get-nourut-karantina&snfg="+$('#suratjalanrincian-snfg').val(), function (data){
            $("select#suratjalanrincian-nourut").html(data);

    });

    $('#form-nourut').fadeIn();

});

JS;
$this->registerJs($script);
?>
