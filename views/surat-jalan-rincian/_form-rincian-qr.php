<?php

use app\models\SuratJalan;
use app\models\SuratJalanRincian;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\models\QcFgV2;
use app\models\AlokasiKirim;


/* @var $this yii\web\View */
/* @var $model app\models\SuratJalanRincian */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- CSS For Video Selector -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="ZXing for JS">

  <title>ZXing TypeScript | Decoding from camera stream</title>

  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">
</head>




<?php $sql="
                select
                  nosj
                from surat_jalan
                where id=".$id."
             ";

        $data = SuratJalan::findBySql($sql)->one();

?>

    <div class="box box-widget widget-user-2">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <?php

            if($data->nosj=="DRAFT"){
                echo '<div class="widget-user-header bg-yellow">';
            }else{
                echo '<div class="widget-user-header bg-green">';
            }
        ?>
      <div class="widget-user-image">
        <img class="img-circle" src="../web/images/clipboard-icon.png" alt="User Avatar">
      </div>
      <!-- /.widget-user-image -->
      <h2 class="widget-user-username"><?=$data->nosj?></h2>
      <h5 class="widget-user-desc">Surat Jalan</h5>

    </div>

<p></p>


<!-- Javascript -->
  <!-- Calling ZXing API CDN -->
  <!-- <script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script> -->
  <script type="text/javascript" src="zxing.js"></script>

  <!-- If NO SJ DRAFT Then Show the QR Scanner -->
  <?php

  if(strpos($data->nosj,'DRAFT')!==false) : ?>
      <div class="box-body" id= "scanner">
          <div class="box-body">
            <div id="sourceSelectPanel" style="display:none">
              <label for="sourceSelect">Switch Camera</label>
              <select id="sourceSelect" style="max-width:400px">x
              </select>
            </div>
            <div>
              <video id="video" width="330" height="300" style="border: 0.5px solid gray"></video>
            </div>
          </div>
      </div>
      <div class="col-sm-12">

      <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-body">
              <div class="box-group bg-blue-gradient" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                
                <div class="panel box box-primary">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false">
                        Input Manual ID PALET (Jika kamera tidak aktif)
                      </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false">
                    <div class="box-body">
                      <div class="col-md-6 column" style="margin-left:0px; color: black; margin-top:0px;">
                        <label for="id_manual">ID Palet</label>
                        <input type="text" autocomplete="off" name="id_manual" id="id_manual" style="color: black;" >
                      </div>
                      <button onclick="check()" style="width:50%; display: block; margin-left: auto; margin-right: auto;">Check</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

      <script type="text/javascript">
          
                let selectedDeviceId;
                const codeReader = new ZXing.BrowserMultiFormatReader()
                console.log('ZXing code reader initialized')
                codeReader.getVideoInputDevices()
                  .then((videoInputDevices) => {
                    const sourceSelect = document.getElementById('sourceSelect')
                    // selectedDeviceId = videoInputDevices[1].deviceId
                    if (videoInputDevices.length >= 1) {
                      videoInputDevices.forEach((element) => {
                        const sourceOption = document.createElement('option')
                        sourceOption.text = element.label
                        sourceOption.value = element.deviceId
                        sourceSelect.appendChild(sourceOption)
                      })
          
                      selectedDeviceId = videoInputDevices[videoInputDevices.length-1].deviceId


                      // When Changing Camera, Reset Scanner and Execute Canvas
                      sourceSelect.onchange = () => {
                        selectedDeviceId = sourceSelect.value;

                        codeReader.reset()
                        codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {

                          if (result) {
                            var input = result.text;
                            var fields = input.split('@');

                            if (fields.length > 1){
                              var snfg= fields[0];
                              var nourut = fields[1];
                              var jumlah_koli = fields[2];

                              $('#suratjalanrincian-snfg').attr('value',snfg);
                              $('#suratjalanrincian-nourut').attr('value',nourut);
                              $('#suratjalanrincian-jumlah_koli').attr('value',jumlah_koli);

                            //if qr content is new format
                            }else{
                              var id_palet ='';
                              var nourut = 0;
                              var fields_new = input.split('|');
                              // var id_palet = fields_new[I];
                              fields_new.forEach(function(content,index){
                                if (content.substring(0,2)=='I:'){
                                  id_palet  = content.substring(2);
                                } else if (content.substring(0,2)=='N:'){
                                  nourut  = content.substring(2);
                                }else{

                                }
                              });
                              $('#suratjalanrincian-id_palet').attr('value',id_palet);
                              $('#suratjalanrincian-nourut').attr('value',nourut);
                              // console.log(id_palet);
                              // console.log(' - '+nourut);
                              // console.log(code);
                            }


                            document.getElementById('formSubmit').click();
                          }
                          if (err && !(err instanceof ZXing.NotFoundException)) {
                            console.error(err)
                            document.getElementById('result').textContent = err
                          }

                        }) // on scanned
                      }; // onchange

                      const sourceSelectPanel = document.getElementById('sourceSelectPanel')
                      sourceSelectPanel.style.display = 'block'
                    }

                    // Initialize Execute Canvas when Page first loaded
                    codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
                      if (result) {
                        var cam_obj = document.getElementById("scanner");
                        cam_obj.remove();

                        var input = result.text;
                        var fields = input.split('@');

                        
                        if (fields.length > 1){
                          var snfg= fields[0];
                          var nourut = fields[1];
                          var jumlah_koli = fields[2];

                          $('#suratjalanrincian-snfg').attr('value',snfg);
                          $('#suratjalanrincian-nourut').attr('value',nourut);
                          $('#suratjalanrincian-jumlah_koli').attr('value',jumlah_koli);

                        //if qr content is new format
                        }else{
                          var id_palet ='';
                          var nourut = 0;
                          var fields_new = input.split('|');
                          // var id_palet = fields_new[I];
                          fields_new.forEach(function(content,index){
                            if (content.substring(0,2)=='I:'){
                              id_palet  = content.substring(2);
                            } else if (content.substring(0,2)=='N:'){
                              nourut  = content.substring(2);
                            }else{

                            }
                          });
                          $('#suratjalanrincian-id_palet').attr('value',id_palet);
                          $('#suratjalanrincian-nourut').attr('value',nourut);
                          // console.log(id_palet);
                          // console.log(' - '+nourut);
                          // console.log(code);
                        }

                        document.getElementById('formSubmit').click();
                      }
                      if (err && !(err instanceof ZXing.NotFoundException)) {
                        console.error(err)
                        document.getElementById('result').textContent = err
                      }
                    }) // on scanned


                  })
                  .catch((err) => {
                    console.error(err)
                  })
            </script>
        </div>
  <?php endif ; ?>


<?php $form = ActiveForm::begin(); ?>


<div id="hide-this">

    <?= $form->field($model, 'surat_jalan_id')->textInput(['style'=>'width:400px;height:30px','value'=>$id,'readOnly'=>true])->label(false) ?>
    <?= $form->field($model, 'snfg')->textInput()->label(false) ?>
    <?= $form->field($model, 'nourut')->textInput()->label(false) ?>
    <?= $form->field($model, 'jumlah_koli')->textInput()->label(false) ?>
    <?= $form->field($model, 'id_palet')->textInput()->label(false) ?>
        <span class="pull-right">


        <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['id'=>'formSubmit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>


        </span>


</div>





<?php ActiveForm::end(); ?>




</div><br \>

    <?php

      if(strpos($data->nosj,'DRAFT')!==false){
        echo
        GridView::widget([
        'dataProvider' => $dataProvider,
        'responsiveWrap' => false,
        'floatHeader'=>'true',
        'floatHeaderOptions' => ['position' => 'absolute',],
        'columns' => [
            'nama_fg',
            [
                  'attribute' => 'snfg',
                  'label'=>'SNFG',
                  'contentOptions' => function($model){
                      return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                  },
            ],
            [
                  'attribute' => 'nourut',
                  'label'=>'PALET',
                  'contentOptions' => function($model){
                      return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                  },
            ],
            [
                  'attribute' => 'qty',
                  'label'=>'QTY',
                  'contentOptions' => function($model){
                      return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                  },
            ],
            [
                  'class'=>'kartik\grid\EditableColumn',
                  'attribute' => 'alokasi',
                  'label'=>'Alokasi',
                  'contentOptions' => function($model){
                      return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                  },
                  'editableOptions' => [
                        'asPopover' => false,
                        'inputType' => 'dropDownList',
                        'data' => ArrayHelper::map(AlokasiKirim::find()->orderBy(['tujuan'=>SORT_ASC])->all()
                                ,'tujuan','nama'),

                    ],
            ],
            [
                  'class'=>'kartik\grid\EditableColumn',
                  'attribute' => 'jumlah_koli',
                  'label'=>'Jumlah Koli',
                  'contentOptions' => function($model){
                      return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                  },
                  'editableOptions' => [
                      'asPopover' => false,
                  ],
            ],
            [
                'class'=>'kartik\grid\EditableColumn',
                'attribute'=>'nobatch',
                'label' => 'No Batch',
                'contentOptions' => function($model){
                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];},
                'editableOptions' => [
                    'asPopover' => false,
                ],
            ],
            [
              'class' => 'kartik\grid\ActionColumn', 'template' => '{delete}',
            ],
          ],
        ]);

      }else{

        echo
          GridView::widget([
          'dataProvider' => $dataProvider,
          'responsiveWrap' => false,
          'floatHeader'=>'true',
          'floatHeaderOptions' => ['position' => 'absolute',],
          'columns' => [
              'nama_fg',
              [
                    'attribute' => 'snfg',
                    'label'=>'SNFG',
                    'contentOptions' => function($model){
                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                    },
              ],
              [
                    'attribute' => 'nourut',
                    'label'=>'PALET',
                    'contentOptions' => function($model){
                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                    },
              ],
              [
                    'attribute' => 'qty',
                    'label'=>'QTY',
                    'contentOptions' => function($model){
                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                    },
              ],
              [
                    'attribute' => 'alokasi',
                    'label'=>'Alokasi',
                    'contentOptions' => function($model){
                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                    },
              ],
              [
                    'class'=>'kartik\grid\EditableColumn',
                    'attribute' => 'jumlah_koli',
                    'label'=>'Jumlah Koli',
                    'contentOptions' => function($model){
                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                    },
                    'editableOptions' => [
                        'asPopover' => false,
                    ],
              ],
              [
                  'class'=>'kartik\grid\EditableColumn',
                  'attribute'=>'nobatch',
                  'label' => 'No Batch',
                  'contentOptions' => function($model){
                      return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];},
                  'editableOptions' => [
                      'asPopover' => false,
                  ],
              ],
            ],
          ]);

      }



    ?>

    <div>
      <?php

          if(strpos($data->nosj,'DRAFT')===false){
              echo Html::a('PDF Print', ['surat-jalan-rincian/pdf','id'=> $id], ['class' => 'btn btn-danger btn-block']);
          }
      ?>
    </div>

    <div id="form-confirm">

        <?php

            if(strpos($data->nosj,'DRAFT')!==false){
               echo Html::a('Confirm', ['surat-jalan-rincian/confirm-surat-jalan','id'=>$id], ['class' => 'btn btn-warning btn-block']);
            }
        ?>

    </div>

    <p></p>

    <div id="form-add" class="form-group">


      <?=Html::a('Home', ['surat-jalan/index'], ['class' => 'btn btn-primary btn-block']);?>


    </div>

<script>


  function check(){
    var id_palet = $('#id_manual').val();
    if (id_palet != ''){
      $.getJSON("index.php?r=qc-fg-v2/check-id-palet",{id_palet : id_palet}, function(data){
        if (data.status=='exist'){
          $('#suratjalanrincian-id_palet').attr('value',id_palet);
          $('#suratjalanrincian-snfg').attr('value',data.snfg);
          $('#suratjalanrincian-nourut').attr('value',data.nourut);
          document.getElementById('formSubmit').click();

          // window.location = 'index.php?r=qc-fg-v2/update-karantina&id_palet='+id_palet;
        }else{
          window.alert('ID Palet tidak ditemukan !');
        }

      });
    }else{
      window.alert('ID Palet kosong!');
    }
  }
</script>

<?php
$script = <<< JS

$('#form-nourut').hide();

$('#hide-this').hide();

$('#suratjalanrincian-snfg').change(function(){
    var snfg = $(this).val();



    $.post("index.php?r=qc-fg-v2/get-nourut-karantina&snfg="+$('#suratjalanrincian-snfg').val(), function (data){
            $("select#suratjalanrincian-nourut").html(data);

    });

    $('#form-nourut').fadeIn();

});

JS;
$this->registerJs($script);
?>
