<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SuratJalanRincian */

?>
<div class="surat-jalan-rincian-view">

    Oops, Item dalam satu surat jalan harus memiliki alokasi yang sama!!

    <p>
        <?= \yii\helpers\Html::a( 'Back', Yii::$app->request->referrer); ?>
    </p>

</div>
