<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SuratJalanRincian */

$this->title = 'Update Surat Jalan Rincian: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Surat Jalan Rincians', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="surat-jalan-rincian-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
