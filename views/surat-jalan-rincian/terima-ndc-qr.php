<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SuratJalanRincian */

?>
<div class="surat-jalan-rincian-create">


    <?= $this->render('_form-terima-ndc-qr', [
        'model' => $model,
        'nosj' => $nosj,
        'is_received' => $is_received,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
