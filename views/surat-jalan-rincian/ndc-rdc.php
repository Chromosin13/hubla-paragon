<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SuratJalanRincian */

?>
<div class="surat-jalan-rincian-view">

    Oops, Item alokasi NDC tidak bisa digabung dengan alokasi Non NDC!!

    <p>
        <?= \yii\helpers\Html::a( 'Back', Yii::$app->request->referrer); ?>
    </p>

</div>
