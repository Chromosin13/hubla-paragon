<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SuratJalanRincian */

$this->title = 'Create Surat Jalan Rincian';
$this->params['breadcrumbs'][] = ['label' => 'Surat Jalan Rincians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surat-jalan-rincian-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
