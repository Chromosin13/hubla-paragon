<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Da\QrCode\QrCode;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SuratJalanRincianSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rincian Item';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surat-jalan-rincian-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php

        $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
                'id',
                'nomor_sj',
                'tgl_buat_sj',
                'odoo_code',
                'kode_korporat',
                'snfg',
                'nama_fg',
                'nobatch',
                'qty',
        // ['class' => 'yii\grid\ActionColumn'],
        ];

        echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_PDF => false,
                                ExportMenu::FORMAT_HTML => false
                          ]
    ]);?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nomor_sj',
            'tgl_buat_sj',
            'odoo_code',
            'kode_korporat',
            'snfg',
            'nama_fg',
            'nobatch',
            'qty',
            // 'nosj',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <div id="home">
        <?=Html::a('HOME', ['surat-jalan-rincian/change-download-status', 'id'=>$id], ['class'=>'btn btn-success btn-block']) ;?>
    </div>
</div>
