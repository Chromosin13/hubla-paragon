<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;
use app\models\OverallLeadtime;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OverallLeadtimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Overall Leadtimes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="overall-leadtime-index">

   <!--  <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php 

        $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
            'nomo',
            'snfg',
            'snfg_komponen',
            'nama_fg',
            'timbang_jadwal_baru',
            'timbang_rework',
            'olah_olah_premix',
            'olah_olah_1',
            'olah_olah_2',
            'olah_adjust',
            'olah_rework',
            'qcbulk_jadwal_baru',
            'qcbulk_rework',
            'kemas1_press',
            'kemas1_filling',
            'kemas2_packing',
            'kemas2_press_filling_packing',
            'kemas2_filling_packing_inline',
            'kemas2_flame_packing_inline',
            'qcfg_jadwal_baru',
            'qcfg_rework',
            'qcfg_adjust',
            'qcfg_pending',
            'qcfg_lanjutan',
        ['class' => 'yii\grid\ActionColumn'],
        ];

        echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
  
    ]);?>

    <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'responsiveWrap' => false,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'success', 'heading'=>'Gross Leadtime'],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                [
                    'attribute'=>'snfg', 
                    'width'=>'210px',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(OverallLeadtime::find()->orderBy('snfg')->asArray()->all(), 'snfg', 'snfg'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'SKU'],
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 220px;font-size: 18px;font-family: Verdana','class'=>'info'],
                    'group'=>true,  // enable grouping
                    //'subGroupOf'=>1,
                ],
                  [
                    'attribute'=>'nomo',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(OverallLeadtime::find()->orderBy('nomo')->asArray()->all(), 'nomo', 'nomo'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'No MO'],
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>2,
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 220px;font-size: 20px','class'=>'info'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                ],
                [
                    'attribute' => 'snfg_komponen',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(OverallLeadtime::find()->orderBy('snfg_komponen')->asArray()->all(), 'snfg_komponen', 'snfg_komponen'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Komponen'],
                    'label' => 'Komponen',
                    'format' => 'raw',
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 200px;font-size: 20px','class'=>'info'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>3,      
                ],
                [
                    'attribute' => 'nama_fg',
                    'label' => 'Nama FG',
                    'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px'
                    ],
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>4,
                ],
                [   
                    'attribute' => 'timbang_jadwal_baru',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>5,
                ],  
                [   
                    'attribute' => 'timbang_rework',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>6,
                ],
                [   
                    'attribute' => 'olah_olah_premix',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>7,
                ],
                [   
                    'attribute' => 'olah_olah_1',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>8,
                ],
                [   
                    'attribute' => 'olah_olah_2',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>9,
                ],
                [   
                    'attribute' => 'olah_adjust',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>10,
                ],
                [   
                    'attribute' => 'olah_rework',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>11,
                ],
                [   
                    'attribute' => 'qcbulk_jadwal_baru',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>12,
                ],
                [   
                    'attribute' => 'qcbulk_rework',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>13,
                ],
                [   
                    'attribute' => 'kemas1_press',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>14,
                ],
                [   
                    'attribute' => 'kemas1_filling',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>15,
                ],
                [   
                    'attribute' => 'kemas2_packing',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>16,
                ],
                [   
                    'attribute' => 'kemas2_press_filling_packing',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>17,
                ],
                [   
                    'attribute' => 'kemas2_filling_packing_inline',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>18,
                ],
                [   
                    'attribute' => 'kemas2_flame_packing_inline',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>19,
                ],
                [   
                    'attribute' => 'qcfg_jadwal_baru',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>20,
                ],
                [   
                    'attribute' => 'qcfg_rework',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>21,
                ],
                [   
                    'attribute' => 'qcfg_adjust',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>22,
                ],
                [   
                    'attribute' => 'qcfg_pending',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>23,
                ],
                [   
                    'attribute' => 'qcfg_lanjutan',
                    'format'=> 'raw',
                    // 'group'=>true,  // enable grouping
                    //'subGroupOf'=>24,
                ],  

                        // 'palet_ke',
             ],
            
        ]);
    
    ?>
</div>
