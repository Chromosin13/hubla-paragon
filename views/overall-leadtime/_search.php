<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OverallLeadtimeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="overall-leadtime-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nomo') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'snfg_komponen') ?>

    <?= $form->field($model, 'nama_fg') ?>

    <?php // echo $form->field($model, 'timbang_jadwal_baru') ?>

    <?php // echo $form->field($model, 'timbang_rework') ?>

    <?php // echo $form->field($model, 'olah_olah_premix') ?>

    <?php // echo $form->field($model, 'olah_olah_1') ?>

    <?php // echo $form->field($model, 'olah_olah_2') ?>

    <?php // echo $form->field($model, 'olah_adjust') ?>

    <?php // echo $form->field($model, 'olah_rework') ?>

    <?php // echo $form->field($model, 'qcbulk_jadwal_baru') ?>

    <?php // echo $form->field($model, 'qcbulk_rework') ?>

    <?php // echo $form->field($model, 'kemas1_press') ?>

    <?php // echo $form->field($model, 'kemas1_filling') ?>

    <?php // echo $form->field($model, 'qcfg_jadwal_baru') ?>

    <?php // echo $form->field($model, 'qcfg_rework') ?>

    <?php // echo $form->field($model, 'qcfg_adjust') ?>

    <?php // echo $form->field($model, 'qcfg_pending') ?>

    <?php // echo $form->field($model, 'qcfg_lanjutan') ?>

    <?php // echo $form->field($model, 'kemas2_packing') ?>

    <?php // echo $form->field($model, 'kemas2_press_filling_packing') ?>

    <?php // echo $form->field($model, 'kemas2_filling_packing_inline') ?>

    <?php // echo $form->field($model, 'kemas2_flame_packing_inline') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
