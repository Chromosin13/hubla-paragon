<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OverallLeadtime */

$this->title = 'Create Overall Leadtime';
$this->params['breadcrumbs'][] = ['label' => 'Overall Leadtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="overall-leadtime-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
