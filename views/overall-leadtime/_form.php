<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OverallLeadtime */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="overall-leadtime-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomo')->textInput() ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'snfg_komponen')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'timbang_jadwal_baru')->textInput() ?>

    <?= $form->field($model, 'timbang_rework')->textInput() ?>

    <?= $form->field($model, 'olah_olah_premix')->textInput() ?>

    <?= $form->field($model, 'olah_olah_1')->textInput() ?>

    <?= $form->field($model, 'olah_olah_2')->textInput() ?>

    <?= $form->field($model, 'olah_adjust')->textInput() ?>

    <?= $form->field($model, 'olah_rework')->textInput() ?>

    <?= $form->field($model, 'qcbulk_jadwal_baru')->textInput() ?>

    <?= $form->field($model, 'qcbulk_rework')->textInput() ?>

    <?= $form->field($model, 'kemas1_press')->textInput() ?>

    <?= $form->field($model, 'kemas1_filling')->textInput() ?>

    <?= $form->field($model, 'qcfg_jadwal_baru')->textInput() ?>

    <?= $form->field($model, 'qcfg_rework')->textInput() ?>

    <?= $form->field($model, 'qcfg_adjust')->textInput() ?>

    <?= $form->field($model, 'qcfg_pending')->textInput() ?>

    <?= $form->field($model, 'qcfg_lanjutan')->textInput() ?>

    <?= $form->field($model, 'kemas2_packing')->textInput() ?>

    <?= $form->field($model, 'kemas2_press_filling_packing')->textInput() ?>

    <?= $form->field($model, 'kemas2_filling_packing_inline')->textInput() ?>

    <?= $form->field($model, 'kemas2_flame_packing_inline')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
