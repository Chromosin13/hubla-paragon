<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OverallLeadtime */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Overall Leadtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="overall-leadtime-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nomo',
            'snfg',
            'snfg_komponen',
            'nama_fg',
            'timbang_jadwal_baru',
            'timbang_rework',
            'olah_olah_premix',
            'olah_olah_1',
            'olah_olah_2',
            'olah_adjust',
            'olah_rework',
            'qcbulk_jadwal_baru',
            'qcbulk_rework',
            'kemas1_press',
            'kemas1_filling',
            'qcfg_jadwal_baru',
            'qcfg_rework',
            'qcfg_adjust',
            'qcfg_pending',
            'qcfg_lanjutan',
            'kemas2_packing',
            'kemas2_press_filling_packing',
            'kemas2_filling_packing_inline',
            'kemas2_flame_packing_inline',
        ],
    ]) ?>

</div>
