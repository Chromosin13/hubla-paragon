<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\QcBulkEntryKomponen;
use app\models\ScmPlanner;
use kartik\widgets\ActiveField;
use faryshta\widgets\JqueryTagsInput;
use wbraganca\tagsinput\TagsinputWidget;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\QcBulkEntry */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    @media screen and (max-width: 1000px) {
      custom-action-column {
        max-width:580px;
      }
    }
    @media screen and (min-width: 1001px) {
      custom-action-column {
        min-width:580px;
      }
    }
</style>

<script type="text/javascript">

  function changeJenis(){
    var jenis_bulk = document.getElementById('jenis_bulk').value;
    var urlString = window.location.href;
    var newUrl = urlString.split("&jenis")[0]
    window.location = newUrl+'&jenis='+jenis_bulk;
  }
  window.onload = function(){
    var param = '<?= $_GET['jenis'] ?? '' ?>';
    if (param == 'sisa') {
      document.getElementById('jenis_bulk').value = 'sisa';
    }else{
      document.getElementById('jenis_bulk').value = 'utuh';
    }
    var url = new URL(window.location.href);
    var obj = url.searchParams.get("obj");
    if (obj) {
      jsonObj = JSON.parse(atob(obj));
      document.getElementById('qcbulkentrykomponen-snfg_komponen').value = jsonObj.snfg_baru
      document.getElementById('qcbulkentrykomponen-snfg_komponen').readOnly = true
      document.getElementById('qcbulkentrykomponen-nobatch').value = jsonObj.no_batch_baru
      document.getElementById('qcbulkentrykomponen-nobatch').readOnly = true
      document.getElementById('qcbulkentrykomponen-storage').value = jsonObj.totalStorage
      document.getElementById('qcbulkentrykomponen-storage').readOnly = true
    }
  }
</script>

<div class="qc-bulk-entry-komponen-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-widget widget-user">


            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-yellow">
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/zoom-in-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username"><b>QC BULK ENTRY KOMPONEN</b></h2>
              <h5 class="widget-user-username"><?=$nomo?> - <?=$nama_bulk?></h5>
            </div>
            <p></p>
            <div class="box-body">
                <?php
                if (isset($_GET['obj'])) {
                  echo $form->field($model, 'snfg_komponen');
                }else{
                  $sql = "
                          SELECT snfg_komponen
                          FROM scm_planner
                          WHERE nomo='".$nomo."'
                          ";

                  // $QcFgs = ScmPlanner::findBySql($sql)->all();

                  echo $form->field($model, 'snfg_komponen')->dropDownList(
                  ArrayHelper::map(ScmPlanner::findBySql($sql)->all()
                  ,'snfg_komponen','snfg_komponen')
                  ,['prompt'=>'Select Komponen']);
                }
                ?>

                <div class="row">
                    <div class="col-md-2">
                        <?= $form->field($model, 'nobatch')->textInput(['value'=>$nobatch])?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($model, 'storage')->textInput(['type'=>'integer'])->label('Jumlah Storage')?>
                    </div>
                    <div class="col-md-1">
                        <?= $form->field($model, 'savelife')->textInput(['type'=>'integer'])->label('SL Storage')?>
                    </div>

                    <div class="col-md-1">
                        <?= $form->field($model, 'unit')->dropDownList(['days' => 'days', 'months' => 'months'])->label('Unit'); ?>
                    </div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'keterangan')->textInput()->label('Keterangan')?>
                    </div>
                </div>
                <?= $form->field($model, 'nama_inspektor')->widget(TagsinputWidget::classname(), [
                    'clientOptions' => [
                        'trimValue' => true,
                        'allowDuplicates' => false,
                        'maxChars'=> 4,
                        'minChars'=> 4,
                    ]
                ])?>
            </div>
    </div>


    <div id="hidden-field">
        <?= $form->field($model, 'nomo')->textInput(['value' => $nomo,'readOnly'=>true]) ?>

        <?= $form->field($model, 'nourut')->textInput(['readOnly'=>true]) ?>

    </div>


    </div>


    <div id="form-add" class="form-group">
        <span id="add" class="pull-right"><?= Html::submitButton($model->isNewRecord ? 'ADD' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?></span>
        <?=Html::a('HOME', ['qc-bulk-entry/create'], ['class' => 'btn btn-primary']);?>

    </div>

    <?php ActiveForm::end(); ?>

</div>



<!-- Gridview -->
<div>
  <span>Jenis Bulk:
    <select class="" name="jenis_bulk" id="jenis_bulk" onchange="changeJenis()">
      <option value="" style="display:none"></option>
      <option value="utuh">Bulk Utuh</option>
      <option value="sisa">Bulk Sisa</option>
    </select>
  </span>
    <?=Html::beginForm(['qc-bulk-entry-komponen/print-zebra-multiple'],'post');?> <!-- routing for multiple print -->
    <?php
      if (isset($_GET['jenis'])) {
        if ($_GET['jenis'] == 'sisa') {
          $dataProvider = $dataProviderSisa;
        }else{
          $dataProvider = $dataProviderUtuh;
        }
      }else{
        $dataProvider = $dataProviderUtuh;
      }
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        // 'showPageSummary' => true,
        // 'pjax' => true,
        // 'striped' => true,
        // 'hover' => true,
        // 'panel' => ['type' => 'primary', 'heading' => 'Grid Grouping Example'],
        // 'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        'columns' => [
            // 'nourut',

            // ['class' => 'kartik\grid\SerialColumn'],

            [
                'class' => 'yii\grid\CheckboxColumn',
                'checkboxOptions'=>function($model,$key,$index,$widget){return['value'=>$model['id']];}
                // you may configure additional properties here
            ],

            [
                    'class' => 'kartik\grid\ActionColumn',
                    'header' => 'Print Label',
                    'template' => '{print-label} {print-zebra}',
                    'contentOptions' => ['style' => 'max-width:40px;'],
                    'buttons' => [

                        'print-label' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/printer-icon.png" alt="Terima" width="30" height="30">',
                                $url,
                                [
                                    'title' => 'Print Label?',
                                ]
                            );
                        },

                        'print-zebra' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/cmyk-icon.png" alt="Terima" width="30" height="30">',
                                $url,
                                [
                                    'title' => 'Print Label?',
                                ]
                            );
                        },


                    ],
                    'visibleButtons' => [
                        'print-label' => function($model,$key,$index){
                                    if(!empty($model->status)){
                                        return true;
                                    } else {
                                        return false;
                                    }
                        },
                        'print-zebra' => function($model,$key,$index){
                                    if(!empty($model->status)){
                                        return true;
                                    } else {
                                        return false;
                                    }
                        },
                    ],

            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{masuk-spesifikasi} {uncomformity1} {uncomformity2} {pending} {cek-homogenitas} {adjust} {parallel-mikro} {pending-mikro} {tidak-masuk-spesifikasi} {rework} {reject}',
                'contentOptions' => ['style' => 'min-width:180px;'],
                'buttons' => [
                    'masuk-spesifikasi' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/masuk_spesifikasi.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'MASUK SPESIFIKASI',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Masuk Spesifikasi?'),
                            ]
                        );
                    },
                    'uncomformity1' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/uncomformity_1.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'UNCOMFORMITY 1',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Release Uncomformity 1?'),
                            ]
                        );
                    },
                    'uncomformity2' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/uncomformity_2.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'UNCOMFORMITY 2',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Release Uncomformity 2?'),
                            ]
                        );
                    },
                    'pending' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/pending.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'PENDING',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Pending 1?'),
                            ]
                        );
                    },
                    'cek-homogenitas' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/cek-homogenitas.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'CEK HOMOGENITAS',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Anda Yakin Memilih CEK HOMOGENITAS?'),
                            ]
                        );
                    },
                    'adjust' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/adjust.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'ADJUST',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Adjust?'),
                            ]
                        );
                    },
                    'ok-filling' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/adjust.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'OK FILLING',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Ok Filling?'),
                            ]
                        );
                    },
                    // 'release' => function ($url,$model) {
                    //     return Html::a(
                    //         '<img class="img-circle" src="../web/images/release.png" alt="Terima" width="42" height="42">',
                    //         $url,
                    //         [
                    //             'title' => 'RELEASE',
                    //             'data-pjax' => '0',
                    //             'data-confirm' => Yii::t('app', 'RELEASE?'),
                    //         ]
                    //     );
                    // },

                    'parallel-mikro' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/parallel-mikro.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'PARALLEL MIKRO',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Parallel Mikro?'),
                            ]
                        );
                    },
                    'pending-mikro' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/pending-mikro.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'PENDING MIKRO',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Pending Mikro?'),
                            ]
                        );
                    },
                    'tidak-masuk-spesifikasi' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/tidak_masuk_spesifikasi.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'TIDAK MASUK SPESIFIKASI',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Tidak Masuk Spesifikasi?'),
                            ]
                        );
                    },

                    // 'pending2' => function ($url,$model) {
                    //     return Html::a(
                    //         '<img class="img-circle" src="../web/images/pending-2.png" alt="Terima" width="42" height="42">',
                    //         $url,
                    //         [
                    //             'title' => 'PENDING 2',
                    //             'data-pjax' => '0',
                    //             'data-confirm' => Yii::t('app', 'Pending 2?'),
                    //         ]
                    //     );
                    // },
                    'rework' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/rework.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'REWORK',
                            ]
                        );
                    },

                    'reject' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/reject.png" alt="Terima" width="42" height="42">',
                            $url,
                            [
                                'title' => 'REJECT',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Reject?'),
                            ]
                        );
                    },
                ],
                'visibleButtons' => [

                    'ok-filling' => function($model,$key,$index){
                        if (!strpos($model->snfg_komponen,'/BS') !== false) {
                          if(empty($model->status)){
                              return true;
                          } else {
                              return false;
                          }
                        }
                    },
                    // 'release' => function($model,$key,$index){
                    //             if(empty($model->status)){
                    //                 return true;
                    //             } else {
                    //                 return false;
                    //             }
                    // },
                    'masuk-spesifikasi' => function($model,$key,$index){
                                if (!strpos($model->snfg_komponen,'/BS') !== false) {
                                  if(empty($model->status)){
                                      return true;
                                  } else {
                                      return false;
                                  }
                                }else{
                                  if(empty($model->status)){
                                      return true;
                                  } else {
                                      return false;
                                  }
                                }
                    },
                    'uncomformity1' => function($model,$key,$index){
                      if (!strpos($model->snfg_komponen,'/BS') !== false) {
                        if(empty($model->status)){
                            return true;
                        } else {
                            return false;
                        }
                      }
                    },
                    'uncomformity2' => function($model,$key,$index){
                      if (!strpos($model->snfg_komponen,'/BS') !== false) {
                        if(empty($model->status)){
                            return true;
                        } else {
                            return false;
                        }
                      }
                    },

                    'pending' => function($model,$key,$index){
                      if (!strpos($model->snfg_komponen,'/BS') !== false) {
                        if(empty($model->status)){
                            return true;
                        } else {
                            return false;
                        }
                      }else{
                        if(empty($model->status)){
                            return true;
                        } else {
                            return false;
                        }
                      }
                    },
                    'cek-homogenitas' => function($model,$key,$index){
                      if (!strpos($model->snfg_komponen,'/BS') !== false) {
                        if(empty($model->status)){
                            return true;
                        } else {
                            return false;
                        }
                      }
                    },
                    'adjust' => function($model,$key,$index){
                      if (!strpos($model->snfg_komponen,'/BS') !== false) {
                        if(empty($model->status)){
                            return true;
                        } else {
                            return false;
                        }
                      }
                    },
                    // 'pending2' => function($model,$key,$index){
                    //             if(empty($model->status)){
                    //                 return true;
                    //             } else {
                    //                 return false;
                    //             }
                    // },

                    'parallel-mikro' => function($model,$key,$index){
                      if (!strpos($model->snfg_komponen,'/BS') !== false) {
                        if(empty($model->status)){
                            return true;
                        } else {
                            return false;
                        }
                      }
                    },
                    'pending-mikro' => function($model,$key,$index){
                      if (!strpos($model->snfg_komponen,'/BS') !== false) {
                        if(empty($model->status)){
                            return true;
                        } else {
                            return false;
                        }
                      }
                    },

                    'tidak-masuk-spesifikasi' => function($model,$key,$index){
                      if (!strpos($model->snfg_komponen,'/BS') !== false) {
                        if(empty($model->status)){
                            return true;
                        } else {
                            return false;
                        }
                      }
                    },
                    'rework' => function($model,$key,$index){
                      if (!strpos($model->snfg_komponen,'/BS') !== false) {
                        if(empty($model->status)){
                            return true;
                        } else {
                            return false;
                        }
                      }else{
                        if(empty($model->status)){
                            return true;
                        } else {
                            return false;
                        }
                      }
                    },
                    'reject' => function($model,$key,$index){
                      if (!strpos($model->snfg_komponen,'/BS') !== false) {
                        if(empty($model->status)){
                            return true;
                        } else {
                            return false;
                        }
                      }else{
                        if(empty($model->status)){
                            return true;
                        } else {
                            return false;
                        }
                      }
                    },
                ],
            ],
            // 'nomo',
            // 'snfg_komponen',
            [
                'attribute' => 'snfg_komponen',
                'width' => '200px',
                'value' => function ($model, $key, $index, $widget) {
                    return $model->snfg_komponen;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(QcBulkEntryKomponen::find()->orderBy('snfg_komponen')->asArray()->all(), 'snfg_komponen', 'snfg_komponen'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Any supplier'],
                'group' => true,  // enable grouping
            ],
            // [
            //     'attribute' => 'No Storage',
            //     'width' => '50px',
            //     'value' => 'nourut',
            // ],
            [
                'attribute' => 'No Storage',
                'width' => '30px',
                'value' => function ($model, $key, $index, $widget) {


                    if (strpos($model->snfg_komponen,'/BS') !== false) {
                      $jumlah_storage = (new yii\db\Query())
                          ->select(['no_storage',])
                          ->from('qc_bulk_entry_komponen')
                          ->where("nomo = '".$model->nomo."'")
                          ->andWhere("snfg_komponen like '%/BS%'")
                          ->distinct()
                          ->count();
                    }else{
                      $jumlah_storage = (new yii\db\Query())
                        ->select(['no_storage',])
                        ->from('qc_bulk_entry_komponen')
                        ->where("nomo = '".$model->nomo."'")
                        ->andWhere("snfg_komponen not like '%/BS%'")
                        ->distinct()
                        ->count();
                  }
                  return $model->no_storage.' / '.$jumlah_storage;
                },
                'group' => true,  // enable grouping
            ],

            'nourut',

            [
                    'attribute' => 'status',
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'contentOptions' => function($model){
                             if($model->status == 'ADJUST')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color: #A9A9A9'];
                                                }
                            else if($model->status == 'OK FILLING')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color: #71EEB8'];
                                                }
                            else if($model->status == 'PENDING')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color: #FFFF00'];
                                                }
                            // else if($model->status == 'PENDING 2')
                            //                     {
                            //                         return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color: #FF7F50'];
                            //                     }
                            else if($model->status == 'REWORK')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color: #B22222'];
                                                }
                            // else if($model->status == 'RELEASE')
                            //                     {
                            //                         return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color:     #228B22'];
                            //                     }
                            else if($model->status == 'CEK MIKRO')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color:     #000080'];
                                                }
                            else if($model->status == 'PARALLEL MIKRO')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color:     #00ffff'];
                                                }
                            else if($model->status == 'PENDING MIKRO')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color:     #ff00ff'];
                                                }
                            else if($model->status == 'UNCOMFORMITY 1')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color:     #7CFC00'];
                                                }
                            else if($model->status == 'UNCOMFORMITY 2')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color:     #47B881'];
                                                }
                            else if($model->status == 'MASUK SPESIFIKASI')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color:     #9900CC'];
                                                }
                            else if($model->status == 'TIDAK MASUK SPESIFIKASI')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color:     #CD3700'];
                                                }
                            else if($model->status == 'CEK HOMOGENITAS')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color:     #cc6699'];
                                                }
                            else if($model->status == 'REJECT')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 18px;background-color: black'];
                                                }
                            else {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: black;font-size: 18px','class'=>'default'];
                            }
                    },
            ],
            // 'created_at',
            'timestamp',
            [
                'attribute' => 'SL Storage',
                'width' => '200px',
                'value' => function ($model, $key, $index, $widget) {
                    return substr($model->savelife_storage, 0,10);
                },
                'group' => false,  // enable grouping
            ],
            [
                    'class'=>'kartik\grid\EditableColumn',
                    'attribute'=>'nobatch',
                    'label' => 'No Batch',
                    'editableOptions' => [
                        'asPopover' => false,
                    ],
            ],
            [
                    'class'=>'kartik\grid\EditableColumn',
                    'attribute'=>'keterangan',
                    'label' => 'Keterangan',
                    'editableOptions' => [
                        'asPopover' => false,
                    ],
            ],
            [
                    'class'=>'kartik\grid\EditableColumn',
                    'attribute'=>'nama_inspektor',
                    'label' => 'Nama Inspektor',
                    'editableOptions' => [
                        'asPopover' => false,
                    ],
            ],
            // [
            //     'class' => 'kartik\grid\ActionColumn',
            //     'template' => '{update}{delete}',
            // ],

            [
                'class' => 'kartik\grid\ActionColumn',
                'header' => 'Actions',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => '{insert}{delete}',
                'buttons' => [
                    'insert' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'insert'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'delete'),
                                    'data-confirm' => Yii::t('yii', 'Are you sure to delete this item?'),
                                    'data-method' => 'POST',
                        ]);
                    }

                ],
            ],

        ],
    ]); ?>

    <?=Html::submitButton('PRINT ALL', ['class' => 'btn btn-warning',]);?>
    <?= Html::endForm();?>

<br \>

<!-- <?=Html::a('ISI PENAMBAHAN BAHAN BAKU ADJUST', ['qc-bulk-entry-add/create','nomo'=>$nomo], ['class' => 'btn btn-danger btn-block' ]);?> -->



<?php
$script = <<< JS


// Hide Create new Field

  var url = new URL(window.location.href);
  var obj = url.searchParams.get("obj");
    $('#hidden-field').hide();
    $('#add').hide();
    if (obj) {
      $('#add').fadeIn("slow");
    }


// Get Value of Nomo

    var nomo = $('#qcbulkentrykomponen-nomo').val();

    // $.get('index.php?r=qc-bulk-entry-komponen/check-release',{ nomo : nomo },function(data){
    //         var data = $.parseJSON(data);
    //         if(!data.id){
    //              $('#add').fadeIn("slow");
    //         }else{
    //              $('#add').hide();

    //         }

    // });

// Populate SNFG Komponen

    // $.post("index.php?r=qc-bulk-entry-komponen/get-komponen&nomo="+nomo, function (data){
    //     $("select#qcbulkentrykomponen-snfg_komponen").html(data);

    // });

    $('#qcbulkentrykomponen-snfg_komponen').change(function(){

        var snfg_komponen = $('#qcbulkentrykomponen-snfg_komponen').val();

        if(snfg_komponen=="Select Komponen"){
            $('#add').hide();
        }else{
            $('#add').fadeIn("slow");
        }

        $.get('index.php?r=qc-bulk-entry-komponen/get-nourut',{ snfg_komponen : snfg_komponen },function(data){
            var data = $.parseJSON(data);

            $('#qcbulkentrykomponen-nourut').attr('value',data.nourut);

        });

    });

// $("#delete_id").on("click", function() {
//     krajeeDialog.confirm("Are you really sure you want to do this?", function (result) {
//         if (result) {
//             return true;
//         } else {
//             return false;
//         }
//     });
// });

JS;
$this->registerJs($script);
?>
