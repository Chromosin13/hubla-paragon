<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\ActiveField;
use kartik\dialog\Dialog;
use kartik\checkbox\CheckboxX;
/* @var $this yii\web\View */
/* @var $model app\models\qcbulkentrykomponen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qc-bulk-entry-komponen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomo')->textInput() ?>

    <div id="create-form" class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <div id="baseurl" value="<?php echo Yii::$app->homeUrl; ?>">

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

// Hide Create new Field

    $('#create-form').hide();
// Get Base Url
var baseurl = $('#baseurl').val();

$('#qcbulkentrykomponen-nomo').change(function(){  

    var nomo = $('#qcbulkentrykomponen-nomo').val();


    $.get('index.php?r=qc-bulk-entry-komponen/check-nomo',{ nomo : nomo },function(data){
        var data = $.parseJSON(data);
        if(data.id==0){
            $('#create-form').show();
            alert("Jadwal Belum Ada, silahkan membuat Jadwal");
        }

        else if(data.id>0){

          alert('Jadwal Ditemukan, Halaman akan diteruskan');
          
          $('#create-form').hide();
          
          var nomo = $('#qcbulkentrykomponen-nomo').val();

          window.location = baseurl+"?r=qc-bulk-entry-komponen/create-rincian&nomo="+nomo;
          
          
        }
    
    });
});


JS;
$this->registerJs($script);
?>
