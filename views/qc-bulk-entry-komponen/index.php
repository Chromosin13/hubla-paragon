<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QcBulkEntryKomponenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Qc Bulk Entry Komponens';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qc-bulk-entry-komponen-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Qc Bulk Entry Komponen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nourut',
            'nomo',
            'status',
            'timestamp',
            'nama_inspektor',
            // 'created_at',
            // 'snfg_komponen',
            // 'id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
