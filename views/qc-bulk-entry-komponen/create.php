<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\QcBulkEntryKomponen */

$this->title = 'Create Qc Bulk Entry Komponen';
$this->params['breadcrumbs'][] = ['label' => 'Qc Bulk Entry Komponens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qc-bulk-entry-komponen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
