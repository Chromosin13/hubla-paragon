<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MonitoringLinePenimbangan */

$this->title = 'Create Monitoring Line Penimbangan';
$this->params['breadcrumbs'][] = ['label' => 'Monitoring Line Penimbangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monitoring-line-penimbangan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
