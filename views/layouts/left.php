<?php

use app\models\User;

?>


<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/flowreport/web/images/proses_icons/wave.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?=Yii::$app->user->identity->username?></p>


                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
<!--         <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <?php


            // View Sampler
            if(Yii::$app->user->identity->username=='sampler'){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [

                        ['label' => 'Menu', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Sampling Label',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Scan QR', 'icon' => 'fa fa-circle-o', 'url' => ['sampling-label/scan-qr'],],

                                ],
                            ],

                        ],
                    ]
                );

            }
            // View Penimbangan
            if(Yii::$app->user->identity->username=='penimbangan'){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Pusat Resolusi',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                ],
                            ],
                        ['label' => 'Reports', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Tracking',
                                'icon' => 'fa fa-safari',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Tracking Posisi Proses', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-dashboard/index']],
                                    ['label' => 'Tracking SO', 'icon' => 'fa fa-compass', 'url' => ['tracking-so/index']],
                                ],
                            ],
                            ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                        ['label' => 'Input v2', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Trial Flow Report 2 Input Data',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Penimbangan New', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-mo/check-penimbangan'],],
                                    ['label' => 'Penimbangan RM', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-mo/check-penimbangan-rm'],],

                                ],
                            ],

                        ],
                    ]
                );

            }
            // View Checker
            if(Yii::$app->user->identity->username=='checker'){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],

                             [
                                'label' => 'Pusat Resolusi',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                ],
                            ],
                        ['label' => 'Input v2', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Trial Flow Report 2 Input Data',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Scan NOMO', 'icon' => 'fa fa-circle-o', 'url' => ['log-task-checker/check-nomo-checker'],],
                                    ['label' => 'Log Print', 'icon' => 'fa fa-circle-o', 'url' => ['log-print-queue-checker/index-print'],],

                                ],
                            ],

                        ],
                    ]
                );

            }
            // View Pengolahan
            else if(in_array(Yii::$app->user->identity->username,['pengolahan','ipc'])){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],

                             [
                                'label' => 'Pusat Resolusi',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                ],
                            ],
                        ['label' => 'Reports', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Tracking',
                                'icon' => 'fa fa-safari',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Tracking Posisi Proses', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-dashboard/index']],
                                    ['label' => 'Tracking SO', 'icon' => 'fa fa-compass', 'url' => ['tracking-so/index']],
                                    ['label' => 'Batch Olah', 'icon' => 'fa fa-compass', 'url' => ['tracking-batch-olah/index']],
                                ],
                            ],
                            ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                        ['label' => 'Input v2', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Trial Flow Report 2 Input Data',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Pengolahan New', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-mo/check-pengolahan'],],
                                    ['label' => 'Pengolahan Tablet', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-mo/check-pengolahan-new'],],
                                    ['label' => 'Cuci Mesin', 'icon' => 'fa fa-circle-o', 'url' => ['flow-cuci-mesin/create'],],

                                ],
                            ],

                        ],
                    ]
                );



            }
            // View QC Bulk
            else if(Yii::$app->user->identity->username=='qcbulk'){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],

                             [
                                'label' => 'Pusat Resolusi',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                ],
                            ],
                        ['label' => 'Reports', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Tracking',
                                'icon' => 'fa fa-safari',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Tracking Posisi Proses', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-dashboard/index']],
                                    ['label' => 'Tracking SO', 'icon' => 'fa fa-compass', 'url' => ['tracking-so/index']],
                                ],
                            ],
                            ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                        ['label' => 'Input v2', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Trial Flow Report 2 Input Data',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                     ['label' => 'QC Bulk New', 'icon' => 'fa fa-circle-o', 'url' => ['qc-bulk-entry/create'],],
                                     ['label' => 'Bulk Sisa', 'icon' => 'fa fa-circle-o', 'url' => ['bulk-sisa/index'],],
                                ],
                            ],

                        ],
                    ]
                );



            }
            // View Kemas
            else if(Yii::$app->user->identity->username=='kemas'){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],

                             [
                                'label' => 'Pusat Resolusi',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                ],
                            ],
                        ['label' => 'Reports', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Tracking',
                                'icon' => 'fa fa-safari',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Tracking Posisi Proses', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-dashboard/index']],
                                    ['label' => 'Tracking SO', 'icon' => 'fa fa-compass', 'url' => ['tracking-so/index']],
                                    ['label' => 'Log Rework', 'icon' => 'fa fa-compass', 'url' => ['log-rework/index']],
                                    ['label' => 'Log Downtime', 'icon' => 'fa fa-compass', 'url' => ['log-downtime/index']],
                                ],
                            ],
                            ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                        ['label' => 'Input v2', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Trial Flow Report 2 Input Data',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Pra Kemas', 'icon' => 'fa fa-circle-o', 'url' => ['flow-pra-kemas/check-kemas'],],
                                    ['label' => 'Kemas Primer', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-snfgkomponen/check-kemas1'],],
                                    ['label' => 'Kemas Sekunder (Pusat)', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-snfg/check-kemas2'],],
                                    ['label' => 'Kemas Sekunder (Inline)', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-snfg/check-kemas2-inline'],],
                                    // ['label' => 'Sisa Kemas [UNRELEASED]', 'icon' => 'fa fa-circle-o', 'url' => ['#'],],
                                    ['label' => 'Sisa Kemas [NEW]', 'icon' => 'fa fa-circle-o', 'url' => ['sisa-kemas/scan'],],
                                    ['label' => 'Cuci Mesin', 'icon' => 'fa fa-circle-o', 'url' => ['flow-cuci-mesin/create'],],
                                    ['label' => 'Bulk Sisa', 'icon' => 'fa fa-circle-o', 'url' => ['bulk-sisa/index'],],
                                    ['label' => 'Loss Tree', 'icon' => 'fa fa-circle-o', 'url' => ['schedule-not-hit/index'],],
                                    

                                ],
                            ],
                        ['label' => 'Print Label', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Print Label',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Label Karbox (QR)', 'icon' => 'fa fa-circle-o', 'url' => ['karbox-label-print/check-snfg'],],
                                    ['label' => 'Label Innerbox (QR)', 'icon' => 'fa fa-circle-o', 'url' => ['innerbox-label-print/check-snfg'],],
                                    ['label' => 'Label Barcode Malaysia', 'icon' => 'fa fa-circle-o', 'url' => ['master-data-barcode-malaysia/check-snfg'],],
                                ],
                            ],

                        ],

                    ]
                );



            }
            // View Karantina Timbang
            else if(Yii::$app->user->identity->username=='karantina'){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],

                             [
                                'label' => 'Pusat Resolusi',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                ],
                            ],

                            ['label' => 'Reports', 'options' => ['class' => 'header']],
                                [
                                    'label' => 'Tracking',
                                    'icon' => 'fa fa-safari',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Tracking JIT', 'icon' => 'fa fa-compass', 'url' => ['tracking-jit/index']],
                                    ],
                                ],
                                [
                                    'label' => 'Dashboard',
                                    'icon' => 'fa fa-safari',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Live Dashboard KFG', 'icon' => 'fa fa-compass', 'url' => ['live-dashboard/index']],
                                    ],
                                ],
                            ['label' => 'Input v2', 'options' => ['class' => 'header']],
                                [
                                    'label' => 'Input',
                                    'icon' => 'fa fa-keyboard-o',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'QC Timbang FG', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/create'],],
                                        ['label' => 'QC Timbang FG QR', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/scan-karantina'],],
                                        ['label' => 'Surat Jalan', 'icon' => 'fa fa-circle-o', 'url' => ['surat-jalan/index'],],
                                    ],
                                ],


                        ],
                    ]
                );
            }
            // View Surat Jalan
            else if(Yii::$app->user->identity->username=='surjal'){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],

                             [
                                'label' => 'Pusat Resolusi',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                ],
                            ],
                        ['label' => 'Input v2', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Input',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Surat Jalan', 'icon' => 'fa fa-circle-o', 'url' => ['surat-jalan/index'],],
                                ],
                            ],

                        ],
                    ]
                );
            }
            // View Surat Jalan
            else if(Yii::$app->user->identity->username=='ndc'){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],

                                 [
                                    'label' => 'Pusat Resolusi',
                                    'icon' => 'fa fa-users',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                    ],
                                ],
                            ['label' => 'Reports', 'options' => ['class' => 'header']],
                                [
                                    'label' => 'Tracking',
                                    'icon' => 'fa fa-safari',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Tracking Posisi Proses', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-dashboard/index']],
                                        ['label' => 'Tracking SO', 'icon' => 'fa fa-compass', 'url' => ['tracking-so/index']],
                                        ['label' => 'Tracking JIT', 'icon' => 'fa fa-compass', 'url' => ['tracking-jit/index']],
                                    ],
                                ],

                            ['label' => 'Input v2', 'options' => ['class' => 'header']],
                                [
                                    'label' => 'Input',
                                    'icon' => 'fa fa-keyboard-o',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Scan Terima NDC', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/halaman-terima-ndc'],],
                                        ['label' => 'QC FG V2', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/create'],],
                                    ],
                                ],

                        ],
                    ]
                );
            }
            //view surat jalan
            else if(Yii::$app->user->identity->username=='jit'){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],

                             [
                                'label' => 'Pusat Resolusi',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                ],
                            ],
                            ['label' => 'Master Data', 'options' => ['class' => 'header']],
                                [
                                    'label' => 'Master Data Alokasi JIT',
                                    'icon' => 'fa fa-keyboard-o',
                                    'url' => ['alokasi-kirim/index'],

                                ],
                            ['label' => 'Reports', 'options' => ['class' => 'header']],
                                [
                                    'label' => 'Tracking',
                                    'icon' => 'fa fa-safari',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Tracking JIT', 'icon' => 'fa fa-compass', 'url' => ['tracking-jit/index']],
                                    ],
                                ],
                            ['label' => 'Input v2', 'options' => ['class' => 'header']],
                                [
                                    'label' => 'Input',
                                    'icon' => 'fa fa-keyboard-o',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'QC Timbang FG', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/create'],],
                                        ['label' => 'QC Timbang FG QR', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/scan-karantina'],],
                                        ['label' => 'Surat Jalan', 'icon' => 'fa fa-circle-o', 'url' => ['surat-jalan/index'],],
                                        ['label' => 'Scan Terima NDC', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/halaman-terima-ndc'],],
                                    ],
                                ],

                        ],
                    ]
                );
            }
             // View Surat Jalan
            else if(Yii::$app->user->identity->username=='engineer'){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                        ['label' => 'Master Data', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Master Data Line',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Master Data Line', 'icon' => 'fa fa-circle-o', 'url' => ['master-data-line/index'],],
                                ],
                            ],

                        ],
                    ]
                );
            }

            // View QCFG
            else if(in_array(Yii::$app->user->identity->username, ['qcfg']) || Yii::$app->user->identity->role == 90){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],

                             [
                                'label' => 'Pusat Resolusi',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                    ['label' => 'Ubah Warna Lakban', 'icon' => 'fa fa-handshake-o', 'url' => ['flow-input-snfg/check-snfg-lakban']],
                                ],
                            ],
                        ['label' => 'Reports', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Tracking',
                                'icon' => 'fa fa-safari',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Tracking Posisi Proses', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-dashboard/index']],
                                    ['label' => 'Tracking SO', 'icon' => 'fa fa-compass', 'url' => ['tracking-so/index']],
                                ],
                            ],
                            [
                                'label' => 'Visual Inspection',
                                'icon' => 'fa fa-columns',
                                'url' => ['vi-navigation/index'],
                                'visible' => Yii::$app->user->identity->username = 'qcfg'
                            ],
                            ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                        ['label' => 'Input v2', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Trial Flow Report 2 Input Data',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'QC FG & NDC', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/create'],],
                                    ['label' => 'QC Timbang FG', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/create'],],
                                    ['label' => 'QC Timbang FG QR', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/scan-karantina'],],
                                    ['label' => 'Surat Jalan', 'icon' => 'fa fa-circle-o', 'url' => ['surat-jalan/index'],],
                                    ['label' => 'Inspeksi FG', 'icon' => 'fa fa-circle-o', 'url' => ['pengecekan-umum/scan-snfg-inspeksi'],],
                                ],
                            ],
                        ['label' => 'MASTER DATA', 'options' => ['class' => 'header']],
                            [
                                'label' => 'INPUT MASTER DATA',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    [
                                        'label' => 'Master Data NA',
                                        'icon' => 'fa fa-cog',
                                        'url' => ['master-data-na/index'],
                                        'visible' => Yii::$app->user->identity->username = 'qcfg'
                                    ],
                                    [
                                        'label' => 'Master Data Qty Karbox & Innerbox',
                                        'icon' => 'fa fa-cog',
                                        'url' => ['master-data-koli/index'],
                                        'visible' => Yii::$app->user->identity->username = 'admin'
                                    ],
                                ],
                            ],
                        // ['label' => 'Visual Inspection System', 'options' => ['class' => 'header']],
                            // [
                            //     'label' => 'Navigation',
                            //     'icon' => 'fa fa-columns',
                            //     'url' => ['vi-navigation/index'],
                            //     'visible' => Yii::$app->user->identity->username = 'qcfg'
                            // ],
                        ],
                    ]
                );



            }// View Inspektor
            else if((in_array(Yii::$app->user->identity->username, ['inspektor']))){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],

                             [
                                'label' => 'Pusat Resolusi',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                ],
                            ],
                        ['label' => 'Reports', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Tracking',
                                'icon' => 'fa fa-safari',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Tracking Posisi Proses', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-dashboard/index']],
                                    ['label' => 'Tracking SO', 'icon' => 'fa fa-compass', 'url' => ['tracking-so/index']],
                                ],
                            ],
                            ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                        ['label' => 'Input v2', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Flow Report 2 Input Data',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Inspeksi FG', 'icon' => 'fa fa-circle-o', 'url' => ['pengecekan-umum/scan-snfg-inspeksi'],],
                                ],
                            ],

                        ],
                    ]
                );



            }else if(Yii::$app->user->identity->username=='admin'){


                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],

                             [
                                'label' => 'Pusat Resolusi',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Input Keluhan', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create']],
                                    ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                    ['label' => 'List Resolusi', 'icon' => 'fa fa-drivers-license', 'url' => ['pusat-resolusi/index']],
                                ],
                            ],
                            ['label' => 'Reports', 'options' => ['class' => 'header']],
                                [
                                    'label' => 'Tracking',
                                    'icon' => 'fa fa-safari',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Tracking Posisi Proses', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-dashboard/index']],
                                        ['label' => 'Tracking SO', 'icon' => 'fa fa-compass', 'url' => ['tracking-so/index']],
                                        ['label' => 'Log Rework', 'icon' => 'fa fa-compass', 'url' => ['log-rework/index']],
                                        ['label' => 'Log Downtime', 'icon' => 'fa fa-compass', 'url' => ['log-downtime/index']],
                                    ],
                                ],
                                [
                                    'label' => 'Dashboard Leadtime',
                                    'icon' => 'fa fa-safari',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Leadtime Penimbangan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-penimbangan/index']],
                                        ['label' => 'Leadtime Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-pengolahan/index']],
                                        ['label' => 'Idletime Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-idletime-pengolahan/index']],
                                        ['label' => 'Leadtime Kemas Primer', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-kemas1/index']],
                                        ['label' => 'Leadtime Kemas Sekunder', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-kemas2/index']],
                                        ['label' => 'Live Dashboard KFG', 'icon' => 'fa fa-compass', 'url' => ['live-dashboard/index']],
                                    ],
                                ],
                                [
                                    'label' => 'Line Monitoring',
                                    'icon' => 'fa fa-cogs',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Monitoring Line Penimbangan', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-penimbangan/index']],
                                        ['label' => 'Monitoring Line Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-pengolahan/index']],
                                        ['label' => 'Monitoring Line Kemas', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-kemas/index']],
                                    ],
                                ],

                            ['label' => 'Adm. Flow Report Planner', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/index'],],
                            ['label' => 'Input', 'options' => ['class' => 'header'], 'visible' => Yii::$app->user->identity->username != 'engineer'],
                                // Konfigurasi Leadtime

                                [
                                    'label' => 'Konfigurasi Leadtime (Old)',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['konfigurasi-std-leadtime/check'],
                                    'visible' => Yii::$app->user->identity->username != 'engineer'
                                ],
                                [
                                    'label' => 'Master Data Line',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-line/index'],
                                    'visible' => Yii::$app->user->identity->username = 'admin'
                                ],
                                [
                                    'label' => 'Master Data Sediaan',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-sediaan/index'],
                                    'visible' => Yii::$app->user->identity->username = 'admin'
                                ],
                                [
                                    'label' => 'Master Data Netto',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-netto/index'],
                                    'visible' => Yii::$app->user->identity->username = 'admin'
                                ],
                                [
                                    'label' => 'Master Data NA',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-na/index'],
                                    'visible' => Yii::$app->user->identity->username = 'admin'
                                ],
                                [
                                    'label' => 'Master Data Qty Karbox & Innerbox',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-koli/index'],
                                    'visible' => Yii::$app->user->identity->username = 'admin'
                                ],

                                ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                            ['label' => 'Input v2', 'options' => ['class' => 'header']],
                                [
                                    'label' => 'Flow Report V2 Input Data',
                                    'icon' => 'fa fa-keyboard-o',
                                    'url' => '#',
                                    'items' => [
                                        // Manage IoT Devices
                                        [ 'label' => 'IoT Management', 'icon' => 'fa fa-circle-o', 'url' => '#',
                                          'items' => [
                                              ['label' => 'Weigher FG Mapping Test', 'icon' => 'fa fa-circle-o', 'url' => ['weigher-fg-map-device/index'],]
                                              ,
                                              ['label' => 'Weigher Dashboard', 'icon' => 'fa fa-circle-o', 'url' => ['iot-dashboard-weigher/dashboard'],],
                                              ['label' => 'Print Inventory Label', 'icon' => 'fa fa-circle-o', 'url' => ['inventory-label-print/index-print'],],
                                          ],

                                        ],
                                        [ 'label' => 'SCM', 'icon' => 'fa fa-circle-o', 'url' => '#',
                                          'items' => [
                                              ['label' => 'Generate SPK-PDF Jadwal', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/generate-spk'],],
                                              ['label' => 'Reposisi Generator', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/create-reposisi'],],
                                          ],

                                        ],
                                        [ 'label' => 'Planner', 'icon' => 'fa fa-circle-o', 'url' => '#',
                                          'items' => [
                                              ['label' => 'Flow Report | Planner', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/create'],],
                                              ['label' => 'Auto Plan | Planner ', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/create-autoplan'],],
                                          ],

                                        ],
                                        ['label' => 'Penimbangan New', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-mo/check-penimbangan'],],
                                        ['label' => 'Pengolahan New', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-mo/check-pengolahan'],],
                                        ['label' => 'QC Bulk New', 'icon' => 'fa fa-circle-o', 'url' => ['qc-bulk-entry/create'],],
                                        // ['label' => 'QC Bulk', 'icon' => 'fa fa-circle-o', 'url' => ['qc-bulk/create'],],
                                        // ['label' => 'Inkjet', 'icon' => 'fa fa-circle-o', 'url' => ['inkjet/create'],],
                                        ['label' => 'Kemas Primer', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-snfgkomponen/check-kemas1'],],
                                        ['label' => 'Kemas Sekunder (Pusat)', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-snfg/check-kemas2'],],
                                        ['label' => 'Kemas Sekunder (Inline)', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-snfg/check-kemas2-inline'],],
                                        // ['label' => 'Sisa Kemas [UNRELEASED]', 'icon' => 'fa fa-circle-o', 'url' => ['#'],],
                                        ['label' => 'Sisa Kemas [NEW]', 'icon' => 'fa fa-circle-o', 'url' => ['sisa-kemas/scan'],],
                                        ['label' => 'Inspeksi FG', 'icon' => 'fa fa-circle-o', 'url' => ['pengecekan-umum/scan-snfg-inspeksi'],],

                                        // ['label' => 'QC FG', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg/create'],],
                                        ['label' => 'QC FG & NDC', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/create'],],
                                        ['label' => 'QC Timbang FG', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/create'],],
                                        ['label' => 'QC Timbang FG QR', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/scan-karantina'],],
                                        ['label' => 'Loss Tree', 'icon' => 'fa fa-circle-o', 'url' => ['schedule-not-hit/index'],],
                                        ['label' => 'Surat Jalan', 'icon' => 'fa fa-circle-o', 'url' => ['surat-jalan/index'],],
                                        ['label' => 'Scan Terima NDC', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/halaman-terima-ndc'],],
                                        ['label' => 'Cuci Mesin', 'icon' => 'fa fa-circle-o', 'url' => ['flow-cuci-mesin/create'],],

                                    ],
                                ],

                            //
                            ['label' => 'Konfigurasi Standar', 'options' => ['class' => 'header']],
                                [
                                    'label' => 'Konfigurasi Standar',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['konfigurasi-standar/check'],
                                ],

                            // ['label' => 'FG Import/Makloon', 'options' => ['class' => 'header']],
                            //     [
                            //         'label' => 'Flow Input FG Makloon',
                            //         'icon' => 'fa fa-keyboard-o',
                            //         'url' => '#',
                            //         'items' => [
                            //             ['label' => 'Adminlog', 'icon' => 'fa fa-circle-o', 'url' => ['adminlog-fg/index'],],
                            //             ['label' => 'Penerimaan', 'icon' => 'fa fa-circle-o', 'url' => ['penerimaanlog-fg/index'],],
                            //             ['label' => 'SamplerQC', 'icon' => 'fa fa-circle-o', 'url' => ['sampler-qc-fg/index'],],
                            //         ],
                            //     ],
                        ],


                    ]
                );
            }else if(Yii::$app->user->identity->username=='produksi'){


                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],

                             [
                                'label' => 'Pusat Resolusi',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Input Keluhan', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create']],
                                    ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                    ['label' => 'List Resolusi', 'icon' => 'fa fa-drivers-license', 'url' => ['pusat-resolusi/index']],
                                ],
                            ],
                            ['label' => 'Reports', 'options' => ['class' => 'header']],
                                [
                                    'label' => 'Tracking',
                                    'icon' => 'fa fa-safari',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Tracking Posisi Proses', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-dashboard/index']],
                                        ['label' => 'Tracking SO', 'icon' => 'fa fa-compass', 'url' => ['tracking-so/index']],
                                        ['label' => 'Log Rework', 'icon' => 'fa fa-compass', 'url' => ['log-rework/index']],
                                        ['label' => 'Log Downtime', 'icon' => 'fa fa-compass', 'url' => ['log-downtime/index']],
                                    ],
                                ],
                                [
                                    'label' => 'Dashboard Leadtime',
                                    'icon' => 'fa fa-safari',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Leadtime Penimbangan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-penimbangan/index']],
                                        ['label' => 'Leadtime Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-pengolahan/index']],
                                        ['label' => 'Idletime Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-idletime-pengolahan/index']],
                                        ['label' => 'Leadtime Kemas Primer', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-kemas1/index']],
                                        ['label' => 'Leadtime Kemas Sekunder', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-kemas2/index']],
                                    ],
                                ],
                                [
                                    'label' => 'Line Monitoring',
                                    'icon' => 'fa fa-cogs',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Monitoring Line Penimbangan', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-penimbangan/index']],
                                        ['label' => 'Monitoring Line Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-pengolahan/index']],
                                        ['label' => 'Monitoring Line Kemas', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-kemas/index']],
                                    ],
                                ],

                            // ['label' => 'Adm. Flow Report Planner', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/index'],],
                            ['label' => 'Input', 'options' => ['class' => 'header'], 'visible' => Yii::$app->user->identity->username != 'engineer'],
                                // Konfigurasi Leadtime

                                [
                                    'label' => 'Konfigurasi Leadtime (Old)',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['konfigurasi-std-leadtime/check'],
                                    'visible' => Yii::$app->user->identity->username != 'engineer'
                                ],
                                [
                                    'label' => 'Master Data Line',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-line/index'],
                                    'visible' => Yii::$app->user->identity->username = 'admin'
                                ],
                                [
                                    'label' => 'Master Data Sediaan',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-sediaan/index'],
                                    'visible' => Yii::$app->user->identity->username = 'admin'
                                ],
                                [
                                    'label' => 'Master Data Netto',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-netto/index'],
                                    'visible' => Yii::$app->user->identity->username = 'admin'
                                ],

                                ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                            // ['label' => 'Input v2', 'options' => ['class' => 'header']],
                            //     [
                            //         'label' => 'Flow Report V2 Input Data',
                            //         'icon' => 'fa fa-keyboard-o',
                            //         'url' => '#',
                            //         'items' => [
                            //             // Manage IoT Devices
                            //             [ 'label' => 'IoT Management', 'icon' => 'fa fa-circle-o', 'url' => '#',
                            //               'items' => [
                            //                   ['label' => 'Weigher FG Mapping Test', 'icon' => 'fa fa-circle-o', 'url' => ['weigher-fg-map-device/index'],]
                            //                   ,
                            //                   ['label' => 'Weigher Dashboard', 'icon' => 'fa fa-circle-o', 'url' => ['iot-dashboard-weigher/dashboard'],],
                            //               ],

                            //             ],
                            //             [ 'label' => 'SCM', 'icon' => 'fa fa-circle-o', 'url' => '#',
                            //               'items' => [
                            //                   ['label' => 'Generate SPK-PDF Jadwal', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/generate-spk'],],
                            //                   ['label' => 'Reposisi Generator', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/create-reposisi'],],
                            //               ],

                            //             ],
                            //             [ 'label' => 'Planner', 'icon' => 'fa fa-circle-o', 'url' => '#',
                            //               'items' => [
                            //                   ['label' => 'Flow Report | Planner', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/create'],],
                            //                   ['label' => 'Auto Plan | Planner ', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/create-autoplan'],],
                            //               ],

                            //             ],
                            //             ['label' => 'Penimbangan New', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-mo/check-penimbangan'],],
                            //             ['label' => 'Pengolahan New', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-mo/check-pengolahan'],],
                            //             ['label' => 'QC Bulk New', 'icon' => 'fa fa-circle-o', 'url' => ['qc-bulk-entry/create'],],
                            //             // ['label' => 'QC Bulk', 'icon' => 'fa fa-circle-o', 'url' => ['qc-bulk/create'],],
                            //             // ['label' => 'Inkjet', 'icon' => 'fa fa-circle-o', 'url' => ['inkjet/create'],],
                            //             ['label' => 'Kemas Primer', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-snfgkomponen/check-kemas1'],],
                            //             ['label' => 'Kemas Sekunder (Pusat)', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-snfg/check-kemas2'],],
                            //             ['label' => 'Kemas Sekunder (Inline)', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-snfg/check-kemas2-inline'],],
                            //             // ['label' => 'Sisa Kemas [UNRELEASED]', 'icon' => 'fa fa-circle-o', 'url' => ['#'],],
                            //             ['label' => 'Sisa Kemas [NEW]', 'icon' => 'fa fa-circle-o', 'url' => ['sisa-kemas/scan'],],

                            //             // ['label' => 'QC FG', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg/create'],],
                            //             ['label' => 'QC FG & NDC', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/create'],],
                            //             ['label' => 'Surat Jalan', 'icon' => 'fa fa-circle-o', 'url' => ['surat-jalan/index'],],

                            //         ],
                            //     ],

                            //
                            ['label' => 'Konfigurasi Standar', 'options' => ['class' => 'header']],
                                [
                                    'label' => 'Konfigurasi Standar',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['konfigurasi-standar/check'],
                                ],
                        ],


                    ]
                );
            }else if(Yii::$app->user->identity->username=='planner'){


                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],

                             [
                                'label' => 'Pusat Resolusi',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Input Keluhan', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create']],
                                    ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                    ['label' => 'List Resolusi', 'icon' => 'fa fa-drivers-license', 'url' => ['pusat-resolusi/index']],
                                ],
                            ],
                        ['label' => 'Reports', 'options' => ['class' => 'header']],
                            // [
                            //     'label' => 'Process Position',
                            //     'icon' => 'fa fa-safari',
                            //     'url' => '#',
                            //     'items' => [
                            //         ['label' => 'Runnning Process Position', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses/index']],
                            //         ['label' => 'Process Position View', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses/admin']],
                            //         ['label' => 'Process Position Setengah Jadi', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-half/index']],
                            //         ['label' => 'Process Position NPD', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-npd/index']],
                            //         ['label' => 'Process Position SCM', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-scm/index']],
                            //         ['label' => 'PP All SCM Dash.', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-scm/running']],
                            //         ['label' => 'Process Position SCM Dash.', 'icon' => 'fa fa-compass', 'url' => '#',
                            //           'items'=> [
                            //               ['label' => 'PP SCM Dash. S', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-scm/running-s']],
                            //               ['label' => 'PP SCM Dash. P', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-scm/running-p']],
                            //               ['label' => 'PP SCM Dash. L', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-scm/running-l']],
                            //           ],

                            //         ],
                            //     ],
                            // ],
                            [
                                'label' => 'Tracking',
                                'icon' => 'fa fa-safari',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Tracking Posisi Proses', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-dashboard/index']],
                                    ['label' => 'Tracking SO', 'icon' => 'fa fa-compass', 'url' => ['tracking-so/index']],
                                    ['label' => 'Log Rework', 'icon' => 'fa fa-compass', 'url' => ['log-rework/index']],
                                ],
                            ],
                            [
                                'label' => 'Dashboard Leadtime',
                                'icon' => 'fa fa-safari',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Leadtime Penimbangan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-penimbangan/index']],
                                    ['label' => 'Leadtime Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-pengolahan/index']],
                                    ['label' => 'Idletime Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-idletime-pengolahan/index']],
                                    ['label' => 'Leadtime Kemas Primer', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-kemas1/index']],
                                    ['label' => 'Leadtime Kemas Sekunder', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-kemas2/index']],
                                ],
                            ],
                            [
                                'label' => 'Line Monitoring',
                                'icon' => 'fa fa-cogs',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Monitoring Line Penimbangan', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-penimbangan/index']],
                                    ['label' => 'Monitoring Line Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-pengolahan/index']],
                                    ['label' => 'Monitoring Line Kemas', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-kemas/index']],
                                ],
                            ],


                        ['label' => 'Adm. Flow Report Planner', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/index'],],
                        ['label' => 'Input', 'options' => ['class' => 'header'], 'visible' => Yii::$app->user->identity->username != 'engineer'],
                            // Konfigurasi Leadtime

                            [
                                'label' => 'Konfigurasi Leadtime',
                                'icon' => 'fa fa-cog',
                                'url' => ['konfigurasi-std-leadtime/check'],
                                'visible' => Yii::$app->user->identity->username != 'engineer'
                            ],
                            [
                                'label' => 'Master Data Line',
                                'icon' => 'fa fa-cog',
                                'url' => ['master-data-line/index'],
                                'visible' => Yii::$app->user->identity->username = 'admin'
                            ],
                            [
                                'label' => 'Master Data Sediaan',
                                'icon' => 'fa fa-cog',
                                'url' => ['master-data-sediaan/index'],
                                'visible' => Yii::$app->user->identity->username = 'admin'
                            ],
                            [
                                    'label' => 'Master Data Netto',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-netto/index'],
                                    'visible' => Yii::$app->user->identity->username = 'admin'
                            ],



                            ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                            // Input Data
                            // [
                            //     'label' => 'Flow Report Input Data',
                            //     'icon' => 'fa fa-keyboard-o',
                            //     'url' => '#',
                            //     'visible' => Yii::$app->user->identity->username != 'engineer',
                            //     'items' => [
                            //         [ 'label' => 'SCM', 'icon' => 'fa fa-circle-o', 'url' => '#',
                            //           'items' => [
                            //               ['label' => 'Reposisi Generator', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/create-reposisi'],],
                            //           ],

                            //         ],
                            //         [ 'label' => 'Planner', 'icon' => 'fa fa-circle-o', 'url' => '#',
                            //           'items' => [
                            //               ['label' => 'Flow Report | Planner', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/create'],],
                            //               ['label' => 'Auto Plan | Planner ', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/create-autoplan'],],
                            //           ],

                            //         ],
                            //         ['label' => 'Penimbangan', 'icon' => 'fa fa-circle-o', 'url' => ['penimbangan/create'],],
                            //         ['label' => 'Pengolahan', 'icon' => 'fa fa-circle-o', 'url' => ['pengolahan/create'],],
                            //         ['label' => 'QC Bulk New', 'icon' => 'fa fa-circle-o', 'url' => ['qc-bulk-entry/create'],],
                            //         // ['label' => 'QC Bulk', 'icon' => 'fa fa-circle-o', 'url' => ['qc-bulk/create'],],
                            //         // ['label' => 'Inkjet', 'icon' => 'fa fa-circle-o', 'url' => ['inkjet/create'],],
                            //         ['label' => 'Kemas Primer', 'icon' => 'fa fa-circle-o', 'url' => ['kemas1/create'],],
                            //         ['label' => 'Kemas Sekunder', 'icon' => 'fa fa-circle-o', 'url' => ['kemas2/create'],],
                            //         // ['label' => 'QC FG', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg/create'],],
                            //         ['label' => 'QC FG & NDC', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/create'],],
                            //         ['label' => 'Surat Jalan', 'icon' => 'fa fa-circle-o', 'url' => ['surat-jalan/index'],],

                            //     ],
                            // ],
                        ['label' => 'Input v2', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Flow Report V2 Input Data',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    // Manage IoT Devices
                                    [ 'label' => 'IoT Management', 'icon' => 'fa fa-circle-o', 'url' => '#',
                                      'items' => [
                                          ['label' => 'Weigher FG Mapping', 'icon' => 'fa fa-circle-o', 'url' => ['weigher-fg-map-device/index'],],
                                          ['label' => 'Weigher Dashboard', 'icon' => 'fa fa-circle-o', 'url' => ['iot-dashboard-weigher/dashboard'],],

                                      ],


                                    ],
                                    [ 'label' => 'SCM', 'icon' => 'fa fa-circle-o', 'url' => '#',
                                      'items' => [
                                          ['label' => 'Generate SPK-PDF Jadwal', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/generate-spk'],],
                                          ['label' => 'Reposisi Generator', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/create-reposisi'],],
                                      ],

                                    ],
                                    [ 'label' => 'Planner', 'icon' => 'fa fa-circle-o', 'url' => '#',
                                      'items' => [
                                          ['label' => 'Flow Report | Planner', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/create'],],
                                          ['label' => 'Auto Plan | Planner ', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/create-autoplan'],],
                                      ],

                                    ],
                                    ['label' => 'Penimbangan New', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-mo/check-penimbangan'],],
                                    ['label' => 'Pengolahan New', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-mo/check-pengolahan'],],
                                    ['label' => 'QC Bulk New', 'icon' => 'fa fa-circle-o', 'url' => ['qc-bulk-entry/create'],],
                                    // ['label' => 'QC Bulk', 'icon' => 'fa fa-circle-o', 'url' => ['qc-bulk/create'],],
                                    // ['label' => 'Inkjet', 'icon' => 'fa fa-circle-o', 'url' => ['inkjet/create'],],
                                    ['label' => 'Kemas Primer', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-snfgkomponen/check-kemas1'],],
                                    ['label' => 'Kemas Sekunder (Pusat)', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-snfg/check-kemas2'],],
                                    ['label' => 'Kemas Sekunder (Inline)', 'icon' => 'fa fa-circle-o', 'url' => ['flow-input-snfg/check-kemas2-inline'],],
                                    // ['label' => 'QC FG', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg/create'],],
                                    ['label' => 'QC FG & NDC', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg-v2/create'],],
                                    ['label' => 'Surat Jalan', 'icon' => 'fa fa-circle-o', 'url' => ['surat-jalan/index'],],
                                    ['label' => 'Bulk Sisa', 'icon' => 'fa fa-circle-o', 'url' => ['bulk-sisa/index'],],

                                ],
                            ],

                        ],
                    ]
                );
            }else if(Yii::$app->user->identity->username=='spr'){


                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],

                             [
                                'label' => 'Pusat Resolusi',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Input Keluhan', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create']],
                                    ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                    ['label' => 'List Resolusi', 'icon' => 'fa fa-drivers-license', 'url' => ['pusat-resolusi/index']],
                                ],
                            ],
                        ['label' => 'Reports', 'options' => ['class' => 'header']],
                            // [
                            //     'label' => 'Process Position',
                            //     'icon' => 'fa fa-safari',
                            //     'url' => '#',
                            //     'items' => [
                            //         ['label' => 'Runnning Process Position', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses/index']],
                            //         ['label' => 'Process Position View', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses/admin']],
                            //         ['label' => 'Process Position Setengah Jadi', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-half/index']],
                            //         ['label' => 'Process Position NPD', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-npd/index']],
                            //         ['label' => 'Process Position SCM', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-scm/index']],
                            //         ['label' => 'PP All SCM Dash.', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-scm/running']],
                            //         ['label' => 'Process Position SCM Dash.', 'icon' => 'fa fa-compass', 'url' => '#',
                            //           'items'=> [
                            //               ['label' => 'PP SCM Dash. S', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-scm/running-s']],
                            //               ['label' => 'PP SCM Dash. P', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-scm/running-p']],
                            //               ['label' => 'PP SCM Dash. L', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-scm/running-l']],
                            //           ],

                            //         ],
                            //     ],
                            // ],
                            [
                                'label' => 'Tracking',
                                'icon' => 'fa fa-safari',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Tracking Posisi Proses', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-dashboard/index']],
                                    ['label' => 'Tracking SO', 'icon' => 'fa fa-compass', 'url' => ['tracking-so/index']],
                                    ['label' => 'Log Rework', 'icon' => 'fa fa-compass', 'url' => ['log-rework/index']],
                                ],
                            ],
                            [
                                'label' => 'Dashboard Leadtime',
                                'icon' => 'fa fa-safari',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Leadtime Penimbangan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-penimbangan/index']],
                                    ['label' => 'Leadtime Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-pengolahan/index']],
                                    ['label' => 'Idletime Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-idletime-pengolahan/index']],
                                    ['label' => 'Leadtime Kemas Primer', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-kemas1/index']],
                                    ['label' => 'Leadtime Kemas Sekunder', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-kemas2/index']],
                                ],
                            ],
                            [
                                'label' => 'Line Monitoring',
                                'icon' => 'fa fa-cogs',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Monitoring Line Penimbangan', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-penimbangan/index']],
                                    ['label' => 'Monitoring Line Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-pengolahan/index']],
                                    ['label' => 'Monitoring Line Kemas', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-kemas/index']],
                                ],
                            ],
                        ['label' => 'Input v2', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Trial Flow Report 2 Input Data',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Bulk Sisa', 'icon' => 'fa fa-circle-o', 'url' => ['bulk-sisa/index'],],

                                ],
                            ],

                        ],
                    ]
                );
            }else if(Yii::$app->user->identity->username=='wpm'){


                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            ['label' => 'Input v2', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Trial Flow Report 2 Input Data',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Bulk Sisa', 'icon' => 'fa fa-circle-o', 'url' => ['bulk-sisa/index'],],

                                ],
                            ],

                        ],
                    ]
                );
            }else if(Yii::$app->user->identity->username=='viewer'){


                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            [
                                'label' => 'Tracking',
                                'icon' => 'fa fa-safari',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Tracking Posisi Proses', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-dashboard/index']],
                                    ['label' => 'Tracking SO', 'icon' => 'fa fa-compass', 'url' => ['tracking-so/index']],
                                ],
                            ],
                            [
                                'label' => 'Dashboard Leadtime',
                                'icon' => 'fa fa-safari',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Leadtime Penimbangan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-penimbangan/index']],
                                    ['label' => 'Leadtime Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-pengolahan/index']],
                                    ['label' => 'Idletime Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-idletime-pengolahan/index']],
                                    ['label' => 'Leadtime Kemas Primer', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-kemas1/index']],
                                    ['label' => 'Leadtime Kemas Sekunder', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-kemas2/index']],
                                ],
                            ],
                            [
                                'label' => 'Line Monitoring',
                                'icon' => 'fa fa-cogs',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Monitoring Line Penimbangan', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-penimbangan/index']],
                                    ['label' => 'Monitoring Line Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-pengolahan/index']],
                                    ['label' => 'Monitoring Line Kemas', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-kemas/index']],
                                ],
                            ],
                            [
                                'label' => 'Master Data Line',
                                'icon' => 'fa fa-cog',
                                'url' => ['master-data-line/index'],
                                'visible' => Yii::$app->user->identity->username = 'admin'
                            ],

                            ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                        ['label' => 'Input v2', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Flow Report V2 Input Data',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    // Manage IoT Devices
                                    [ 'label' => 'IoT Management', 'icon' => 'fa fa-circle-o', 'url' => '#',
                                      'items' => [
                                          ['label' => 'Weigher FG Mapping', 'icon' => 'fa fa-circle-o', 'url' => ['weigher-fg-map-device/index'],],
                                          ['label' => 'Weigher Dashboard', 'icon' => 'fa fa-circle-o', 'url' => ['iot-dashboard-weigher/dashboard'],],

                                      ],


                                    ],

                                ],
                            ],

                        ],
                    ]
                );
            }else if(Yii::$app->user->identity->username=='scm'){


                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            [
                                'label' => 'Tracking',
                                'icon' => 'fa fa-safari',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Tracking Posisi Proses', 'icon' => 'fa fa-compass', 'url' => ['posisi-proses-dashboard/index']],
                                    ['label' => 'Tracking SO', 'icon' => 'fa fa-compass', 'url' => ['tracking-so/index']],
                                    ['label' => 'Log Rework', 'icon' => 'fa fa-compass', 'url' => ['log-rework/index']],
                                    ['label' => 'Log Downtime', 'icon' => 'fa fa-compass', 'url' => ['log-downtime/index']],
                                ],
                            ],
                            [
                                'label' => 'Dashboard Leadtime',
                                'icon' => 'fa fa-safari',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Leadtime Penimbangan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-penimbangan/index']],
                                    ['label' => 'Leadtime Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-pengolahan/index']],
                                    ['label' => 'Idletime Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['dashboard-idletime-pengolahan/index']],
                                    ['label' => 'Leadtime Kemas Primer', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-kemas1/index']],
                                    ['label' => 'Leadtime Kemas Sekunder', 'icon' => 'fa fa-compass', 'url' => ['dashboard-leadtime-kemas2/index']],
                                ],
                            ],
                            [
                                'label' => 'Line Monitoring',
                                'icon' => 'fa fa-cogs',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Monitoring Line Penimbangan', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-penimbangan/index']],
                                    ['label' => 'Monitoring Line Pengolahan', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-pengolahan/index']],
                                    ['label' => 'Monitoring Line Kemas', 'icon' => 'fa fa-compass', 'url' => ['monitoring-line-kemas/index']],
                                ],
                            ],
                            [
                                'label' => 'Master Data Line',
                                'icon' => 'fa fa-cog',
                                'url' => ['master-data-line/index'],
                                'visible' => Yii::$app->user->identity->username = 'admin'
                            ],

                            ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                        ['label' => 'Input v2', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Flow Report V2 Input Data',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    // Manage IoT Devices
                                    [ 'label' => 'IoT Management', 'icon' => 'fa fa-circle-o', 'url' => '#',
                                      'items' => [
                                          ['label' => 'Weigher FG Mapping', 'icon' => 'fa fa-circle-o', 'url' => ['weigher-fg-map-device/index'],],                                      ],

                                    ],

                                ],
                            ],

                        ],
                    ]
                );
            } else if(Yii::$app->user->identity->role=='pe'){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],

                             [
                                'label' => 'Pusat Resolusi',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                ],
                            ],
                        ['label' => 'Input Batch Record', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Batch Record',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Master Batch Record', 'icon' => 'fa fa-keyboard-o', 'url' => ['log-formula-breakdown/master-br']],
                                    ['label' => 'Verifikasi Jadwal', 'icon' => 'fa fa-keyboard-o', 'url' => ['log-formula-breakdown/schedule-list']],
                                    ['label' => 'Edit Data (Penimbangan)', 'icon' => 'fa fa-keyboard-o', 'url' => ['tracking-so/index']],
                                ],
                            ],
                            // ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                        ],
                    ]
                );
            } else if(Yii::$app->user->identity->username=='station'){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                                [
                                    'label' => 'Input', 'options' => ['class' => 'header'], 'visible' => Yii::$app->user->identity->username != 'engineer'],
                                [
                                    'label' => 'LABEL KARBOX',
                                    'icon' => 'fa fa-keyboard-o',
                                    'url' => ['karbox-label-print/check-snfg']
                                ],
                                [
                                    'label' => 'LABEL INNERBOX',
                                    'icon' => 'fa fa-keyboard-o',
                                    'url' => ['innerbox-label-print/check-snfg']
                                ],
                        ]
                    ]
                );

            }
            else if(Yii::$app->user->identity->username=='adminlog'){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Menu', 'options' => ['class' => 'header']],

                             [
                                'label' => 'Pusat Resolusi',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Trial Input Keluhan v2', 'icon' => 'fa fa-handshake-o', 'url' => ['pusat-resolusi/create-new']],
                                ],
                            ],
                        ['label' => 'FG Import/Makloon', 'options' => ['class' => 'header']],
                            [
                                'label' => 'Flow Input FRO Makloon',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Adminlog', 'icon' => 'fa fa-circle-o', 'url' => ['adminlog-fg/index'],],
                                    ['label' => 'Penerimaan', 'icon' => 'fa fa-circle-o', 'url' => ['penerimaanlog-fg/index'],],
                                ],
                            ],

                        ],
                    ]
                );
            } else if(Yii::$app->user->identity->username=='admin-malaysia'){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            ['label' => 'MALAYSIA', 'options' => ['class' => 'header']],
                                // Konfigurasi Leadtime
                                [
                                    'label' => 'Master Data Barcode',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-barcode-malaysia/index'],
                                    'visible' => Yii::$app->user->identity->username = 'admin-malaysia'
                                ],
                                [
                                    'label' => 'Print Barcode Malaysia',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-barcode-malaysia/check-snfg'],
                                    'visible' => Yii::$app->user->identity->username = 'admin-malaysia'
                                ],

                                ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest]

                        ]

                    ]
                );
            } else if(Yii::$app->user->identity->username=='printer-malaysia'){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            ['label' => 'MALAYSIA', 'options' => ['class' => 'header']],
                                // Konfigurasi Leadtime
                                [
                                    'label' => 'Print Barcode Malaysia',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-barcode-malaysia/check-snfg'],
                                    'visible' => Yii::$app->user->identity->username = 'printer-malaysia'
                                ],

                                ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest]

                        ]

                    ]
                );
            } else if(Yii::$app->user->identity->username=='akun.mex'){


                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            // ['label' => 'Gii', 'url' => ['gii'], 'visible' => Yii::$app->user->identity->username == 'admin'],
                            ['label' => 'Input', 'options' => ['class' => 'header'], 'visible' => Yii::$app->user->identity->username != 'engineer'],
                                // Konfigurasi Leadtime
                                [
                                    'label' => 'Master Data NA',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-na/index'],
                                    // 'visible' => Yii::$app->user->identity->username = 'admin'
                                ],
                                [
                                    'label' => 'Master Data Line',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-line/index'],
                                    // 'visible' => Yii::$app->user->identity->username = 'admin'
                                ],
                                [
                                    'label' => 'Master Data Sediaan',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-sediaan/index'],
                                    // 'visible' => Yii::$app->user->identity->username = 'admin'
                                ],
                                [
                                    'label' => 'Master Data Qty Karbox & Innerbox',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-koli/index'],
                                    // 'visible' => Yii::$app->user->identity->username = 'admin'
                                ],
                                [
                                    'label' => 'Print Inventory Label',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['inventory-label-print/index-print'],
                                    // 'visible' => Yii::$app->user->identity->username = 'admin'
                                ],

                                ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                            ['label' => 'Input v2', 'options' => ['class' => 'header']],
                                [
                                    'label' => 'Flow Report V2 Input Data',
                                    'icon' => 'fa fa-keyboard-o',
                                    'url' => '#',
                                    'items' => [
                                        // Manage IoT Devices
                                        [ 'label' => 'IoT Management', 'icon' => 'fa fa-circle-o', 'url' => '#',
                                          'items' => [
                                              ['label' => 'Weigher FG Mapping Test', 'icon' => 'fa fa-circle-o', 'url' => ['weigher-fg-map-device/index'],]
                                              ,
                                          ],

                                        ],
                                    ],
                                ],



                            //

                            // ['label' => 'FG Import/Makloon', 'options' => ['class' => 'header']],
                            //     [
                            //         'label' => 'Flow Input FG Makloon',
                            //         'icon' => 'fa fa-keyboard-o',
                            //         'url' => '#',
                            //         'items' => [
                            //             ['label' => 'Adminlog', 'icon' => 'fa fa-circle-o', 'url' => ['adminlog-fg/index'],],
                            //             ['label' => 'Penerimaan', 'icon' => 'fa fa-circle-o', 'url' => ['penerimaanlog-fg/index'],],
                            //             ['label' => 'SamplerQC', 'icon' => 'fa fa-circle-o', 'url' => ['sampler-qc-fg/index'],],
                            //         ],
                            //     ],
                        ],


                    ]
                );
            } else if(Yii::$app->user->identity->username == 'packeng'){
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                        ['label' => 'MASTER DATA', 'options' => ['class' => 'header']],
                            [
                                'label' => 'INPUT MASTER DATA',
                                'icon' => 'fa fa-keyboard-o',
                                'url' => '#',
                                'items' => [
                                    [
                                    'label' => 'Master Data NA',
                                    'icon' => 'fa fa-cog',
                                    'url' => ['master-data-na/index'],
                                    'visible' => Yii::$app->user->identity->username = 'qcfg'
                                    ],
                                    [
                                        'label' => 'Master Data Qty Karbox & Innerbox',
                                        'icon' => 'fa fa-cog',
                                        'url' => ['master-data-koli/index'],
                                        'visible' => Yii::$app->user->identity->username = 'qcfg'
                                    ],

                                ],
                            ],

                        ],
                    ]
                );



            }

        ?>

    </section>

</aside>
