<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php if (isset($this->blocks['content-header'])) { ?>
            <h1><?= $this->blocks['content-header'] ?></h1>
        <?php } 
            else { ?>
            <h1>
                <?php
                if ($this->title !== null) 
                {
                    echo \yii\helpers\Html::encode($this->title);
                } 
                else 
                {
                    //  Disabling Mini Title accross breadcrumbs

                    // echo \yii\helpers\Inflector::camel2words(
                    //     \yii\helpers\Inflector::id2camel($this->context->module->id)
                    // );
                    // echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                } ?>
            </h1>
        <?php } ?>

        <?=
        Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?>
    </section>

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <a href="index.php?r=site/about"><b>Flowreport </b></a> 2.0 
    </div>
    <strong>V2.0.2016-2018.</strong> Developed by IS Paragon Technology & Innovation
</footer>

<!-- Control Sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class='control-sidebar-bg'></div>