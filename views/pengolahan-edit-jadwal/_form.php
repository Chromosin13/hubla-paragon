<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
use yii\helpers\ArrayHelper;
use faryshta\widgets\JqueryTagsInput;
use app\models\PengolahanEditJadwal;
use yii\widgets\Pjax; 
use kartik\date\DatePicker;
use kartik\widgets\DateTimePicker;
/* @var $this yii\web\View */
/* @var $model app\models\PengolahanEditJadwal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengolahan-edit-jadwal-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'id')->textInput() ?> -->

    <?= $form->field($model, 'nomo')->textInput(['disabled'=>'true']) ?>

    <?= $form->field($model, 'snfg_komponen')->textInput(['disabled'=>'true']) ?>

    <?= $form->field($model, 'lanjutan')->textInput(['disabled'=>'true']) ?>

    <?= $form->field($model, 'start')->textInput(['disabled'=>'true']) ?>

    <?= $form->field($model, 'stop')->textInput(['disabled'=>'true']) ?>

    <?= $form->field($model, 'jenis_olah')->textInput(['disabled'=>'true']) ?>

    <?= $form->field($model, 'nama_line')->dropDownList(
                            ArrayHelper::map(PengolahanEditJadwal::find()->all()
                            ,'nama_line','nama_line')
                            ,['prompt'=>'Select Line']

    );?>

    <?php echo $form->field($model, 'nama_operator', ['hintType' => ActiveField::HINT_SPECIAL, 'addon' => ['prepend' => ['content'=>'<i class="fa fa-barcode"></i>']]
                            ])->widget(JqueryTagsInput::className([]))->hint(' <b>Scan</b> : Klik pada isian dibawah kemudian Scan Barcode pada Nametag<p \> <b>Manual</b> : isi dengan Format 4 Huruf Kapital, Contoh : "ABCD" lalu diikuti dengan menekan Enter ');
    ?>

    <?= 
                                DatePicker::widget([
                                'model' => $model,
                                'form' => $form,
                                'name' => 'plan_start_olah',
                                'attribute' => 'plan_start_olah', 
                                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                                'options' => ['placeholder' => 'Select issue date ...'],
                                'pluginOptions' => [
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true
                                    ]
                                ]);
                            ?>

    <?= 
                                DatePicker::widget([
                                'model' => $model,
                                'form' => $form,
                                'name' => 'plan_end_olah',
                                'attribute' => 'plan_end_olah', 
                                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                                'options' => ['placeholder' => 'Select issue date ...'],
                                'pluginOptions' => [
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true
                                    ]
                                ]);
                            ?>

    <?= $form->field($model, 'shift_plan_start_olah')->dropDownList([1 => 'Shift 1',2 => 'Shift 2'],['prompt'=>'Select Shift']); 
    ?>

    <?= $form->field($model, 'shift_plan_end_olah')->dropDownList([1 => 'Shift 1',2 => 'Shift 2'],['prompt'=>'Select Shift']); 
    ?>

    <?= $form->field($model, 'start_id')->textInput(['disabled'=>'true']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

var lanjutan = $('#pengolahaneditjadwal-lanjutan').val();
if(lanjutan>1){
    document.getElementById("pengolahaneditjadwal-plan_start_olah").disabled = true
    document.getElementById("pengolahaneditjadwal-plan_end_olah").disabled = true
    document.getElementById("pengolahaneditjadwal-shift_plan_start_olah").disabled = true
    document.getElementById("pengolahaneditjadwal-shift_plan_end_olah").disabled = true;
}


// Assign Nama Line
var nomo = $('#pengolahaneditjadwal-nomo').val();
var snfg_komponen = $('#pengolahaneditjadwal-snfg_komponen').val();
if(nomo!="" && snfg_komponen==""){
    $.post("index.php?r=scm-planner/get-line-olah-nomo-edit&nomo="+$('#pengolahaneditjadwal-nomo').val()+"&nama_line="+$('#pengolahaneditjadwal-nama_line').val(), function (data){
        $("select#pengolahaneditjadwal-nama_line").html(data);
    });
} else if(nomo=="" && snfg_komponen!=""){
    $.post("index.php?r=scm-planner/get-line-olah-komponen-edit&nomo="+$('#pengolahaneditjadwal-snfg_komponen').val()+"&nama_line="+$('#pengolahaneditjadwal-nama_line').val(), function (data){
        $("select#pengolahaneditjadwal-nama_line").html(data);
    });
}

JS;
$this->registerJs($script);
?>