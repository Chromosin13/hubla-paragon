<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PengolahanEditJadwal */

$this->title = 'Create Pengolahan Edit Jadwal';
$this->params['breadcrumbs'][] = ['label' => 'Pengolahan Edit Jadwals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengolahan-edit-jadwal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
