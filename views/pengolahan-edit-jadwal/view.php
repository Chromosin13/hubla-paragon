<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PengolahanEditJadwal */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pengolahan Edit Jadwals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengolahan-edit-jadwal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nomo',
            'snfg_komponen',
            'lanjutan',
            'start',
            'stop',
            'jenis_olah',
            'nama_line',
            'nama_operator',
            'plan_start_olah',
            'plan_end_olah',
            'shift_plan_start_olah',
            'shift_plan_end_olah',
            'start_id',
        ],
    ]) ?>

</div>
