<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DeviceMapQcfg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="device-map-qcfg-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sbc_ip')->textInput() ?>

    <?= $form->field($model, 'frontend_ip')->textInput() ?>

    <?= $form->field($model, 'keterangan')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
