<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DeviceMapQcfg */

$this->title = 'Create Device Map Qcfg';
$this->params['breadcrumbs'][] = ['label' => 'Device Map Qcfgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="device-map-qcfg-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
