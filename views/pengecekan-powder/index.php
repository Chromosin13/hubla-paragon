<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PengecekanPowderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengecekan Powders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengecekan-powder-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pengecekan Powder', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_pengecekan_umum',
            'netto',
            'drop_test',
            'rub_test',
            // 'pengecekan_warna',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
