<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PengecekanPowder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengecekan-powder-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_pengecekan_umum')->textInput() ?>

    <?= $form->field($model, 'netto')->textInput() ?>

    <?= $form->field($model, 'drop_test')->textInput() ?>

    <?= $form->field($model, 'rub_test')->textInput() ?>

    <?= $form->field($model, 'pengecekan_warna')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
