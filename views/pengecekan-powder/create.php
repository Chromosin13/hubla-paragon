<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PengecekanPowder */

$this->title = 'Create Pengecekan Powder';
$this->params['breadcrumbs'][] = ['label' => 'Pengecekan Powders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengecekan-powder-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
