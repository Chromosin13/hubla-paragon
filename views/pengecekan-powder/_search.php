<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PengecekanPowderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengecekan-powder-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_pengecekan_umum') ?>

    <?= $form->field($model, 'netto') ?>

    <?= $form->field($model, 'drop_test') ?>

    <?= $form->field($model, 'rub_test') ?>

    <?php // echo $form->field($model, 'pengecekan_warna') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
