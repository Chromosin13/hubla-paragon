<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LineMonitoringOlah */

$this->title = 'Create Line Monitoring Olah';
$this->params['breadcrumbs'][] = ['label' => 'Line Monitoring Olahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="line-monitoring-olah-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
