<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SamplingLabel */

// $this->title = 'Create Sampling Label';
// $this->params['breadcrumbs'][] = ['label' => 'Sampling Labels', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="sampling-label-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-confirm', [
      'barcode' => $barcode,
      'odoo_code' => $odoo_code,
      'nobatch' => $nobatch,
      'qty' => $qty,
      'expired' => $expired,
      'data' => $data,
        'model' => $model,
    ]) ?>

</div>
