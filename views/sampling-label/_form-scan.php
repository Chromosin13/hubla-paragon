<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>


                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
                      <h3 class="widget-user-username"><b>Sampling Label</b></h3>
                      <h5 class="widget-user-desc">Scan QR</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/business-partnership.png" alt="User Avatar">
                    </div>

                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">

                            <div class="zoom">
                              <?=Html::a('SCAN WITH CAMERA', ['sampling-label/scan-qr-camera'], ['class' => 'btn btn-success btn-block']);?>
                            </div>
                            <p></p>

                              <div class="box-body">
                                  <div class="sampling-label-form">

                                    <div class="form-group field-samplinglabel-code has-success">
                                    <label class="control-label" for="samplinglabel-code">Code</label>
                                    <input type="text" id="samplinglabel-code" class="form-control" name="samplinglabel[snfg]" aria-invalid="false">

                                    </div>
                                  </div>
                              </div>
                        </div>
                      <!-- /.row -->
                      </div>
                    </div>


<?php
$script = <<< JS

  // AutoFocus SNFG Field

  document.getElementById("samplinglabel-code").focus();
  $("#samplinglabel-code").keypress(function(e) {
    //Enter key
    if (e.which == 13) {
      return false;
    }
  });


  // Check SNFG Validity

  $('#samplinglabel-code').change(function(){

      var code = $('#samplinglabel-code').val().trim();

      if(code){
        window.location = "index.php?r=sampling-label/create&data="+code;
      }else{
        alert('Kode kosong!');
      }

  });

  // $('#samplinglabel-code').change(function(){

  //     var snfg = $('#samplinglabel-code').val();


  //     $.get('index.php?r=scm-planner/check-snfg',{ snfg : snfg },function(data){
  //         var data = $.parseJSON(data);
  //         if(data.id>=1){

  //           window.location = "index.php?r=sampling-label/create-kemas2&snfg="+snfg;

  //         }

  //         else{

  //           alert('Nomor SNFG Tidak Ditemukan di Database; Silahkan mengontak Planner/Leader');
  //         }

  //     });
  // });



JS;
$this->registerJs($script);
?>
