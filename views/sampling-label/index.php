<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SamplingLabelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sampling Labels';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sampling-label-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sampling Label', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'barcode',
            'odoo_code',
            'qty',
            'expired',
            // 'nobatch',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
