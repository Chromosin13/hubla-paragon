<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;

/* @var $this yii\web\View */
/* @var $model app\models\SamplingLabel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
        <div class="col-md-12">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-blue">
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/clock.gif" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username"><b>Sampling Label</b></h3>
              <h5 class="widget-user-desc"></h5>
            </div>

            <br>
            <?php
    	    	    // echo Html::img('../profile_image/profile_'.$kode_personil.'.png', ['class' => 'pull-left img-responsive', 'align'=>'center']);
    	      ?>
            <br>

            <div class="row">
        <div class="col-md-12">
          <!-- Box Comment -->
          <div class="box box-widget">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">

                <div class="col-md-12">
                  <div class="box box-warning box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title">Confirm</h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div>
                      <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                              [
                                'label'=>'Data QR',
                                'value'=>$data,
                              ],
                                [
                                  'label'=>'Barcode',
                                  'value'=>$barcode,
                                ],
                                [
                                  'label'=>'Odoo Code',
                                  'value'=>$odoo_code
                                ],
                                [
                                   'label' => 'No Batch',
                                   'value' => $nobatch
                                ],
                                [
                                   'label' => 'Quantity',
                                   'value' => $qty
                                ],
                                [
                                   'label' => 'Expired Date',
                                   'value' => $expired
                                ],

                                // 'kendaraan_umum' ,
                                // 'aktifitas_luar_rumah' ,
                                // 'keluarga_sakit' ,
                                // 'keluarga_bepergian',

                            ],
                        ]) ?>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
                </div>
                <!-- /.col -->
              </div>

              <div class="sampling-label-form">


                  <?php $form = ActiveForm::begin(); ?>

                  <div class="row" id="hide">
                      <div class="col-md-4">
                        <?= $form->field($model, 'data')->textInput(['value'=>$data]) ?>
                        <?= $form->field($model, 'barcode')->textInput(['value'=>$barcode]) ?>
                        <?= $form->field($model, 'odoo_code')->textInput(['value'=>$odoo_code]) ?>
                        <?= $form->field($model, 'qty')->textInput(['value'=>$qty]) ?>
                        <?= $form->field($model, 'expired')->textInput(['value'=>$expired]) ?>
                        <?= $form->field($model, 'nobatch')->textInput(['value'=>$nobatch]) ?>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                          <?= $form->field($model, 'upload[]')->fileInput(['multiple'=>true])->label('Upload (max 5 images)') ?>
                        <!-- /input-group -->
                      </div>
                  </div>

                  <div class="form-group">
                      <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                  </div>

                  <?php ActiveForm::end(); ?>



              </div>
              <!-- <img class="img-responsive pad" src="../dist/img/photo2.png" alt="Photo"> -->

              <!-- <p>I took this photo this morning. What do you guys think?</p> -->
              <!-- <button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button> -->
              <!-- <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button> -->
              <!-- <span class="pull-right text-muted">127 likes - 3 comments</span> -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

          </div>
          <!-- /.widget-user -->
        </div>
        <!-- /.col -->
      </div>

<?php
$script = <<< JS
  $('#hide').hide();
    // AutoFocus SNFG Field
    document.getElementById("mobilitaasparagonian-snfg").focus();
    $("#koordinatstok-qr_palet").keypress(function(e) {
      //Enter key
      if (e.which == 13) {
        return false;
      }
    });


    // Check SNFG Validity

    $('#mobilitaasparagonian-snfg').change(function(){

        var snfg = $('#mobilitaasparagonian-snfg').val().trim();

        if(snfg){
          window.location = "index.php?r=mobilitaas-paragonian/create-kemas2&snfg="+snfg;
        }else{
          alert('Nomor Jadwal kosong!');
        }

    });

    // $('#mobilitaasparagonian-snfg').change(function(){

    //     var snfg = $('#mobilitaasparagonian-snfg').val();


    //     $.get('index.php?r=scm-planner/check-snfg',{ snfg : snfg },function(data){
    //         var data = $.parseJSON(data);
    //         if(data.id>=1){

    //           window.location = "index.php?r=mobilitaas-paragonian/create-kemas2&snfg="+snfg;

    //         }

    //         else{

    //           alert('Nomor SNFG Tidak Ditemukan di Database; Silahkan mengontak Planner/Leader');
    //         }

    //     });
    // });



JS;
$this->registerJs($script);
?>
