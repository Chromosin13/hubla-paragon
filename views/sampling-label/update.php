<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SamplingLabel */

$this->title = 'Update Sampling Label: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sampling Labels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sampling-label-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
