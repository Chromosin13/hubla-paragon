<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LineMonitoringTimbang */

$this->title = 'Update Line Monitoring Timbang: ' . $model->nama_line;
$this->params['breadcrumbs'][] = ['label' => 'Line Monitoring Timbangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama_line, 'url' => ['view', 'id' => $model->nama_line]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="line-monitoring-timbang-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
