<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LineMonitoringTimbang */

$this->title = 'Create Line Monitoring Timbang';
$this->params['breadcrumbs'][] = ['label' => 'Line Monitoring Timbangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="line-monitoring-timbang-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
