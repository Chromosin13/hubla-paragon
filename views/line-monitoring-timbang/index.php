<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\LineMonitoringTimbang;
/* @var $this yii\web\View */
/* @var $searchModel app\models\LineMonitoringTimbangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Line Monitoring Timbangs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="line-monitoring-timbang-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'proses',
            [
                'attribute'=>'nama_line',
                'filter'=>ArrayHelper::map(LineMonitoringTimbang::find()->asArray()->all(), 'nama_line', 'nama_line'),
            ],
            'nomo',
            'waktu',
            [
                    'attribute' => 'state',
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'contentOptions' => function($model){
                             if($model->state == 'STOP')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                                                }
                            else if($model->state == 'START') 
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'success'];
                                                }
                            else                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'default'];
                                                }
                    },
            ],
            'lanjutan',
            'jenis_proses',
        ],
    ]); ?>
</div>
