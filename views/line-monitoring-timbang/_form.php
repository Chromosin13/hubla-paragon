<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LineMonitoringTimbang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="line-monitoring-timbang-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'proses')->textInput() ?>

    <?= $form->field($model, 'nama_line')->textInput() ?>

    <?= $form->field($model, 'nomo')->textInput() ?>

    <?= $form->field($model, 'waktu')->textInput() ?>

    <?= $form->field($model, 'state')->textInput() ?>

    <?= $form->field($model, 'lanjutan')->textInput() ?>

    <?= $form->field($model, 'jenis_proses')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
