<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogPickingGbbSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-picking-gbb-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'timestamp') ?>

    <?= $form->field($model, 'datetime_start') ?>

    <?= $form->field($model, 'datetime_stop') ?>

    <?= $form->field($model, 'nomo') ?>

    <?php // echo $form->field($model, 'nama_line') ?>

    <?php // echo $form->field($model, 'mo_odoo') ?>

    <?php // echo $form->field($model, 'kode_bb') ?>

    <?php // echo $form->field($model, 'kode_internal') ?>

    <?php // echo $form->field($model, 'nama_bb') ?>

    <?php // echo $form->field($model, 'kode_olah') ?>

    <?php // echo $form->field($model, 'siklus') ?>

    <?php // echo $form->field($model, 'weight') ?>

    <?php // echo $form->field($model, 'satuan') ?>

    <?php // echo $form->field($model, 'operator') ?>

    <?php // echo $form->field($model, 'koitem_fg') ?>

    <?php // echo $form->field($model, 'jenis_bb') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
