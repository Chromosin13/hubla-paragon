<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LogPickingGbb */

$this->title = 'Create Log Picking Gbb';
$this->params['breadcrumbs'][] = ['label' => 'Log Picking Gbbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-picking-gbb-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
