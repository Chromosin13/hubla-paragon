<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LogPickingGbb */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Log Picking Gbbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-picking-gbb-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'timestamp',
            'datetime_start',
            'datetime_stop',
            'nomo',
            'nama_line',
            'mo_odoo',
            'kode_bb',
            'kode_internal',
            'nama_bb',
            'kode_olah',
            'siklus',
            'weight',
            'satuan',
            'operator',
            'koitem_fg',
            'jenis_bb',
        ],
    ]) ?>

</div>
