<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\LogPickingGbbSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Picking Gbbs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-picking-gbb-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Log Picking Gbb', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'timestamp',
            'datetime_start',
            'datetime_stop',
            'nomo',
            // 'nama_line',
            // 'mo_odoo',
            // 'kode_bb',
            // 'kode_internal',
            // 'nama_bb',
            // 'kode_olah',
            // 'siklus',
            // 'weight',
            // 'satuan',
            // 'operator',
            // 'koitem_fg',
            // 'jenis_bb',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
