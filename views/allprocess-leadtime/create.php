<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AllprocessLeadtime */

$this->title = 'Create Allprocess Leadtime';
$this->params['breadcrumbs'][] = ['label' => 'Allprocess Leadtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="allprocess-leadtime-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
