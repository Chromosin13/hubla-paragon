<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AllprocessLeadtime */

$this->title = 'Update Allprocess Leadtime: ' . $model->row_number;
$this->params['breadcrumbs'][] = ['label' => 'Allprocess Leadtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->row_number, 'url' => ['view', 'id' => $model->row_number]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="allprocess-leadtime-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
