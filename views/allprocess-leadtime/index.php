<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AllprocessLeadtimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Process Leadtimes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="allprocess-leadtime-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Allprocess Leadtime', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 
        $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
                'row_number',
                'posisi:ntext',
                'snfg',
                'snfg_komponen',
                'nama_line',
                'nama',
                'tanggal',
                'sum',
        ['class' => 'yii\grid\ActionColumn'],
        ];

        echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_PDF => false,
                                ExportMenu::FORMAT_HTML => false
                          ]        
    ]);?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'row_number',
            'posisi:ntext',
            'snfg',
            'snfg_komponen',
            'nama_line',
            'nama',
            'tanggal',
            'sum',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>



    

</div>
