
<?php
$script = <<< JS
$('input').keydown( function (event) { //event==Keyevent
    if(event.which == 13) {
        var inputs = $(this).closest('form').find(':input:visible');
        inputs.eq( inputs.index(this)+ 1 ).focus();
        event.preventDefault(); //Disable standard Enterkey action
    }
    // event.preventDefault(); <- Disable all keys  action
});
JS;
$this->registerJs($script);
?>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Kemas2 */

// $this->title = 'Kemas2';
// $this->params['breadcrumbs'][] = ['label' => 'Kemas2s', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="kemas2-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsKendala' => $modelsKendala,
    ]) ?>

</div>
