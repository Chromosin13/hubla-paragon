<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
use faryshta\widgets\JqueryTagsInput;
use yii\grid\GridView;
use kartik\checkbox\CheckboxX;
use app\models\Kemas1;
use app\models\PosisiProses;
use app\models\ScmPlanner;
use app\models\PosisiProsesReworkSb;
use app\models\PosisiProsesReworkPalet;
use yii\helpers\ArrayHelper;
use app\models\Kemas2;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\web\JsExpression;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\DateTimePicker;


/* @var $this yii\web\View */
/* @var $model app\models\kemas2 */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="kemas2-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <!-- Left Side -->
    <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-blue">
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username"><b>KEMAS 2</b></h2>
              <h5 class="widget-user-desc">INPUT</h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                
                <!-- Content Begins Here  -->

                    <!-- Isi SNFG -->
                    <div id="form_snfg" class="box-header with-border">
                            <?= $form->field($model, 'snfg', ['hintType' => ActiveField::HINT_SPECIAL, 
                                                              'addon' => [
                                                                            'append' => [
                                                                                'content'=>'<i class="fa fa-barcode"></i>'
                                                                            ],
                                                                            'prepend' => [
                                                                                'content' => Html::button('Tekan Setelah Scan',['class'=>'btn btn-primary','id'=>'afterScnBtnSnfg']),
                                                                                'asButton' => true,
                                                                            ]
                                                             ]
                            ])->textInput(['placeholder'=>'Klik Disini, Scan pada Barcode SNFG yang tertera di SPK'])->hint('PASTIKAN CAPS LOCK TIDAK AKTIF!');
                            ?>
                    </div>

                    <!-- Planner Info -->
                    <div id="planner-information">
                        <div class="box-header with-border">
                                
                                <h2 class="box-title"> <b>Planner Information</b></h2>
                                <br \>
                        </div>


                        <div class="box-header with-border">
                            <table id="table-snfg" class="table table-bordered">
                                <tbody><tr>
                                  <th>NOMOR MO</th>
                                  <th>STREAMLINE</th>
                                  <th>START</th>
                                  <th>DUE</th>
                                  <th>BESAR BATCH</th>
                                  <th>BESAR LOT</th>
                                  <th>LOT KE</th>
                                  <th>KODE JADWAL</th>
                                  <th>STATUS</th>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                    <!-- Last Position -->
                    <div id="last-position">

                            <div class="box-header with-border">
                                    
                                    <h2 class="box-title"> <b>Last Position</b></h2>
                                    <br \>
                            </div>


                            <div class="box-header with-border">


                                <table id="table-last" class="table table-bordered">
                                    <tbody><tr>
                                      <th style="width: 120px">SNFG Komponen</th>
                                      <th style="width: 50px">POSISI</th>
                                      <th>STATE</th>
                                      <th>JENIS PROSES</th>
                                      <th>LANJUTAN</th>
                                      <th style="width: 40px">IS DONE</th>
                                    </tr>
                                  </tbody>
                              </table>

                            </div>

                    </div>

                    <!-- Form Start Stop -->

                    <div class="box-header with-border" id="form_start_stop">
                            <a class="btn btn-success" id="start-button">
                                        <i class="fa fa-play"></i> Start
                            </a>

                            <a class="btn btn-danger" id="stop-button">
                                        <i class="fa fa-pause"></i> Stop
                            </a>
                            <p \>
                            <p \>
                            
                            <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>
                            
                            
                    </div>


                    <!-- Form Input Jadwal -->


                        <div class="box-header with-border">

                            <?= $form->field($model, 'edit_nama')->textInput(['readOnly'=>true,'value'=>Yii::$app->user->identity->username]) ?>

                            <?php
                                
                                date_default_timezone_set('Asia/Jakarta');

                                echo $form->field($model, 'edit_time')->textInput(['readonly' => 'true','value'=>date("Y-m-d H:i:s")])
                            ?>

                            <?php 
                                date_default_timezone_set('Asia/Jakarta');

                                echo '<label class="control-label">Waktu</label>';
                                echo DateTimePicker::widget([
                                    'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                                    'model' => $model,
                                    'attribute' => 'waktu',
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'yyyy-mm-dd hh:ii:ss',
                                        'endDate' => date("Y-m-d H:i:s")
                                    ]
                                ]);
                            ?>
                        </div>

                    <div class="box-header with-border" id="form_jenis">

                        <div id="lsb_validator">

                            <div id="kemas2-jenis_kemas_00">
                            <?= $form->field($model, 'jenis_kemas')->dropDownList(['PACKING' => 'PACKING' ,'PRESS+FILLING+PACKING' => 'PRESS+FILLING+PACKING', 'FILLING-PACKING_INLINE' => 'FILLING-PACKING_INLINE','FLAME-PACKING_INLINE' => 'FLAME-PACKING_INLINE', 'REWORK' => 'REWORK' ],['prompt'=>'Select Option']);
                            ?>
                            </div>

                            <div id="kemas2-jenis_kemas_0">
                            <?= $form->field($model, 'jenis_kemas')->textInput(['id'=>'kemas2-jenis_kemas_1', 'disabled'=>'true' , 'readonly' =>'true']);
                            ?>
                            </div>


                        </div>

                    </div>


                <!-- Content Ends Here -->

              </ul>
            </div>


    </div>




            <!-- Palet Autoincrement -->
            <!-- <div id='palet_ke_form'>
            <?= $form->field($model, 'palet_ke')->textInput(['readonly' => 'true']) ?>
            </div> -->

    <div id="form_entry_start_stop">


            <div class="box box-widget widget-user-2" id="kemas2-start">

                    <div class="widget-user-header bg-green">
                      <div class="widget-user-image">
                        <img class="img-circle" src="../web/images/arrow-up-icon.png" alt="User Avatar">
                      </div>
                      <!-- /.widget-user-image -->
                      <h2 class="widget-user-username">Start</h2>
                      <h5 class="widget-user-desc">Entry</h5>
                    </div>

                    <div class="box-footer no-padding">
                      <ul class="nav nav-stacked">
                            
                            <!-- Content Begins Here -->
                            <div class="box-header with-border">

                                <?php

                                    echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                                        'data' => ArrayHelper::map(Kemas2::find()->all()
                                        ,'nama_line','nama_line'),
                                        'options' => ['placeholder' => 'Select Line'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);

                                    // echo $form->field($model, 'nama_line')->dropDownList(
                                    // ArrayHelper::map(Kemas2::find()->all()
                                    // ,'nama_line','nama_line')
                                    // ,['prompt'=>'Select Line']);

                                ?>

                                <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>

                                <div id="kemas2-lanjutan_split_batch_start-form">
                                <?php 
                                    //echo $form->field($model, 'lanjutan_split_batch')->textInput(['id'=>'kemas2-lanjutan_split_batch_start','readonly' => 'true']) ?>
                                </div>
                                
                                <!-- Nama Operator -->
                                <?php 

                                    echo $form->field($model, 'nama_operator', ['hintType' => ActiveField::HINT_SPECIAL,'addon' => ['prepend' => ['content'=>'<i class="fa fa-barcode"></i>']]
                                        ])->widget(TagsinputWidget::classname(), [
                                        'clientOptions' => [
                                            'trimValue' => true,
                                            'allowDuplicates' => false,
                                            'maxChars'=> 4,
                                        ]
                                    ])->hint(' <b>Scan</b> : Klik pada isian dibawah kemudian Scan Barcode pada Nametag<p \> <b>Manual</b> : isi dengan Format 4 Huruf Kapital, Contoh : "ABCD" lalu diikuti dengan menekan Enter ');

                                ?>

                            </div>
                        

                      </ul>
                    </div>

            </div>

            <div class="box box-widget widget-user-2" id="kemas2-stop">

                    <div class="widget-user-header bg-red">
                      <div class="widget-user-image">
                        <img class="img-circle" src="../web/images/arrow-down-icon.png" alt="User Avatar">
                      </div>
                      <!-- /.widget-user-image -->
                      <h2 class="widget-user-username">Stop</h2>
                      <h5 class="widget-user-desc">Entry</h5>
                    </div>

                    <div class="box-footer no-padding">
                      <ul class="nav nav-stacked">
                            
                            <!-- Content Begins Here -->
                            <div class="box-header with-border">
                                <?= $form->field($model, 'lanjutan')->textInput(['id'=>'kemas2-lanjutan_stop','readonly' => 'true']) ?>

                                <div id="kemas2-lanjutan_split_batch_stop-form">
                                <?php 
                                    // echo $form->field($model, 'lanjutan_split_batch')->textInput(['id'=>'kemas2-lanjutan_split_batch_stop','readonly' => 'true']) ?>
                                </div>

                                <?= $form->field($model, 'nobatch')->textInput() ?>

                                <?= $form->field($model, 'jumlah_realisasi')->textInput() ?>

                                <?= $form->field($model, 'counter')->textInput(['readonly'=>true]) ?>


                                <label>
                                  <input type="checkbox" id="is_kendala">
                                  Terdapat Kendala ?
                                </label>

                                <div id="kendala-form">
                                    <?php DynamicFormWidget::begin([
                                        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                        'widgetBody' => '.container-items', // required: css class selector
                                        'widgetItem' => '.item', // required: css class
                                        'limit' => 3, // the maximum times, an element can be cloned (default 999)
                                        'min' => 1, // 0 or 1 (default 1)
                                        'insertButton' => '.add-item', // css class
                                        'deleteButton' => '.remove-item', // css class
                                        'model' => $modelsKendala[0],
                                        'formId' => 'dynamic-form',
                                        'formFields' => [
                                            'keterangan',
                                            'start',
                                            'stop',
                                        ],
                                    ]); ?>

                                    <div class="container-items"><!-- widgetContainer -->
                                        <?php foreach ($modelsKendala as $i => $modelKendala): ?>
                                        <div class="item panel panel-default"><!-- widgetBody -->
                                            <div class="panel-heading">
                                                <h3 class="panel-title pull-left">Kendala</h3>
                                                <div class="pull-right">
                                                    <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="panel-body">
                                                <?php
                                                    // necessary for update action.
                                                    if (! $modelKendala->isNewRecord) {
                                                        echo Html::activeHiddenInput($modelKendala, "[{$i}]id");
                                                    }
                                                ?>

                                                <div class="row">
                                                    <div class="col-sm-3">


                                                        <?= $form->field($modelKendala, "[{$i}]keterangan")->widget(Select2::classname(), [
                                                                            'data' => ArrayHelper::map(Kendala::find()->all()
                                                                    ,'keterangan','keterangan'),
                                                                            'options' => ['placeholder' => 'Select Kendala'],
                                                                            'pluginOptions' => [
                                                                                // 'tags' => true,
                                                                                'allowClear' => true
                                                                            ],
                                                                            ]);
                                                        ?>

                                                    </div>
                                                    <div class="col-sm-3">
                                                        <?= $form->field($modelKendala, "[{$i}]start")->widget(TimePicker::classname(), [
                                                                        'options' => ['placeholder' => 'Start'],
                                                                        'pluginOptions' => [
                                                                                'minuteStep' => 5,
                                                                                'autoclose'=>true,
                                                                                'showMeridian' => false,
                                                                                'defaultTime' => '00:00'
                                                                        ]
                                                        ]); ?>
                                                    </div>
                                                </div><!-- .row -->
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                        </div>
                                        <?php DynamicFormWidget::end(); ?>
                                </div>

                                <p \>

                                <div id="is_done-form">
                                <?php
                                    echo '<label class="cbx-label" for="is_done">Is Done?  </label>';
                                    echo CheckboxX::widget([
                                        'model' => $model,
                                        'attribute' => 'is_done',
                                        'pluginOptions' => [
                                            'threeState' => false,
                                            'size' => 'sm'
                                        ]
                                    ]);
                                ?>
                                </div>

                                <div id="batch_split-form">
                                <?php
                                    // echo '<label class="cbx-label" for="batch_split">Split Batch?</label>';
                                    // echo CheckboxX::widget([
                                    //     'model' => $model,
                                    //     'attribute' => 'batch_split',
                                    //     'pluginOptions' => [
                                    //         'threeState' => false,
                                    //         'size' => 'sm'
                                    //     ]
                                    // ]);
                                ?>
                                </div>

                            </div>
                      </ul>
                    </div>

            </div>

<!--             <div class="box" id="kemas2-stop">

            </div> -->

            <div class="form-group" id="create-button">
                
                <?php echo Dialog::widget([
                        'libName' => 'krajeeDialogCust', // a custom lib name
                        'options' => [  // customized BootstrapDialog options
                            'size' => Dialog::SIZE_LARGE, // large dialog text
                            'type' => Dialog::TYPE_WARNING, // bootstrap contextual color
                            'title' => 'Konfirmasi Input',
                            'buttons' => [
                                [
                                    
                                    'id' => 'btn-ok',
                                    'icon' => 'glyphicon glyphicon-check',
                                    'label' => 'Sudah Benar, Submit!',
                                    'cssClass' => 'btn-primary',
                                    'action' => new JsExpression("function() {
                                        $('#submitButton').submit();            
                                        }")
                                    // Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
                                ],
                            ]
                        ]
                    ]);
                ?>

                <button type="button" id="btn-custom" class="btn btn-info">Validate</button>
                <button id="submitButton" type="submit" class="btn btn-success">Creates</button>
                <!-- <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?> -->
            </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS



    

// Hide Submit Button untuk Konfirmasi Input
$('#submitButton').hide();


// Inisialisasi Form Hide

// Hide and Disable DynamicFormWidget for Kendala Model //

    $('#kendala-form').hide();
        document.getElementById("kendala-0-keterangan").disabled = true;
        document.getElementById("kendala-0-start").disabled = true;
    // Original
    $('#kemas2-start').hide();
    $('#kemas2-stop').hide();
    $('#start-button').hide();
    $('#stop-button').hide(); 
    $('#form_start_stop').hide(); 
    $('#form_jenis').hide(); 
    $('#form_entry_start_stop').hide();
    $('#planner-information').hide();
    $('#last-position').hide();


// After Klik Tombol Tekan Scan
    // Klik Scan SNFG

    $('#afterScnBtnSnfg').click(function(){
        var nomo = $('#kemas2-snfg').val();
        

        // Get Data From Counter
        $.get('http://10.5.4.203/ptidms/php_action/test_get_redha.php',{ snfg : nomo },function(data){
                var data = $.parseJSON(data);
                var qtyaktual = data.qtyaktual;
                alert(qtyaktual);
                
                $('#kemas2-counter').attr('value',data.qtyaktual);

        });

        if(nomo==""){
            $('#form_start_stop').hide();    
        }
        else {
            $('#form_start_stop').fadeIn("slow"); 
        }
    });    


// After Klik Start Stop Button
    $('#start-button').click(function(){
        $('#form_jenis').fadeIn("slow");
    });

    $('#stop-button').click(function(){
        $('#form_jenis').fadeIn("slow");
        $('#form_entry_start_stop').fadeIn("slow");
    });

// After Select Jenis Kemas
    $('#kemas2-jenis_kemas').change(function(){
        var jenis_kemas = $('#kemas2-jenis_kemas').val();
        if(jenis_kemas==""){
            $('#form_entry_start_stop').hide(); 
        } else {
            $('#form_entry_start_stop').fadeIn("slow");
            var state = $('#kemas2-state').val();
            if(state=="START"){
                $('#create-button').hide();
                $('#kemas2-nama_line').change(function(){
                    var nama_line = $('#kemas2-nama_line').val();

                    $.get('index.php?r=kemas2/check-line',{ nama_line : nama_line },function(data){
                    var data = $.parseJSON(data);
                        
                        if(nama_line=="Select Line"){
                            $('#create-button').hide();
                        }else if(data.state=="START"){
                            alert('Mesin ini belum di STOP oleh nomor jadwal '+data.snfg);
                            // $('#create-button').hide();
                            $('#create-button').fadeIn("slow");
                        }else{
                            $('#create-button').fadeIn("slow");
                        }

                    });
                });
            }
      
        }
    });

// Validate Button

$("#btn-custom").on("click", function() {
    var snfg = $('#kemas2-snfg').val();
    var state = $('#kemas2-state').val();
    var jenis_kemas = $('#kemas2-jenis_kemas').val();
    var jenis_kemas_1 = $('#kemas2-jenis_kemas_1').val();
    var lanjutan = $('#kemas2-lanjutan').val();
    var nama_line = $('#kemas2-nama_line').val();
    var nama_operator =  $('#kemas2-nama_operator').val();
    var jumlah_realisasi = $('#kemas2-jumlah_realisasi').val();   
    var is_done = $('#kemas2-is_done').val();
    // var lanjutan_split_batch = $('#kemas2-lanjutan_split_batch_start').val();
    // var lanjutan_split_batch_stop = $('#kemas2-lanjutan_split_batch_stop').val();
    var nobatch = $('#kemas2-nobatch').val();

    if(lanjutan==null){
        alert('Penarikan data Lanjutan Gagal, Mohon refresh halaman dan scan ulang jadwal');
    }else if(state=="START"){
        krajeeDialogCust.dialog('<b>SNFG</b><p>' + '<font color="blue"><i>'+(snfg)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Kemas</b><p>' + '<font color="blue"><i>'+(jenis_kemas)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>' +
                                '<b>Nama Line</b><p>' + '<font color="blue"><i>'+(nama_line)+'</i></font><p>' +  
                                '<b>Nama Operator</b><p>' + '<font color="blue"><i>'+(nama_operator)+'</i></font><p>',function(result) {}
        );
    }else if(state=="STOP"&&is_done==1){
    
        krajeeDialogCust.dialog('<b>SNFG</b><p>' + '<font color="blue"><i>'+(snfg)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Kemas</b><p>' + '<font color="blue"><i>'+(jenis_kemas_1)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>'+
                                '<b>Nomor Batch</b><p>' + '<font color="blue"><i>'+(nobatch)+'</i></font><p>' +
                                '<b>Jumlah Realisasi</b><p>' + '<font color="blue"><i>'+(jumlah_realisasi)+'</i></font><p>' + 
                                '<b><font color="blue"><i>Jadwal Sudah Selesai (Tidak Lanjut Shift)</i></font></b><p>',function(result) {}
        );

    }else if(state=="STOP"&&is_done==0){

        krajeeDialogCust.dialog('<b>SNFG</b><p>' + '<font color="blue"><i>'+(snfg)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Kemas</b><p>' + '<font color="blue"><i>'+(jenis_kemas_1)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>'+
                                '<b>Nomor Batch</b><p>' + '<font color="blue"><i>'+(nobatch)+'</i></font><p>' +
                                '<b>Jumlah Realisasi</b><p>' + '<font color="blue"><i>'+(jumlah_realisasi)+'</i></font><p>' +
                                '<b><font color="blue"><i>Jadwal Akan Dilanjutkan ke Shift Berikutnya (Belum Done)</i></font></b><p>' ,function(result) {}
        );

    }

});


$('#is_kendala').change(function(){

    if(document.getElementById('is_kendala').checked){
            $('#kendala-form').fadeIn("slow");

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);

                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = false;
                    document.getElementById("kendala-"+(i)+"-start").disabled = false;
        
                    i++;
                }

    }else{
            $('#kendala-form').hide();

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);
                    
                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = true;
                    document.getElementById("kendala-"+(i)+"-start").disabled = true;  

                    i++;
                }

    } 
 });


$('#kemas2-snfg').change(function(){
    var snfg = $(this).val();

    $.post("index.php?r=scm-planner/get-line-kemas-snfg&snfg="+$('#kemas2-snfg').val(), function (data){
        $("select#kemas2-nama_line").html(data);
    });

    // Assign Get Kendala Kemas
    $.post("index.php?r=scm-planner/get-kendala-kemas-snfg&snfg="+$('#kemas2-snfg').val(), function (data){
            $("select#kendala-0-keterangan").html(data);
    });

    $.get('index.php?r=scm-planner/get-planner-snfg-raw',{ snfg : snfg },function(data){
        var json = $.parseJSON(data);
        var tr;
        for (var i = 0; i < json.length; i++) {
            tr = $('<tr/>');
            tr.append("<td><span class='badge bg-grey'>" + json[i].nomo + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].streamline + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].start + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].due + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].besar_batch + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].besar_lot + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].lot_ke + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].kode_jadwal + "</span></td>");
            tr.append("<td><span class='badge bg-green'>" + json[i].status + "</span></td>");
            $('#table-snfg').append(tr);
        }
        $('#planner-information').fadeIn("slow");
    });

    $.get('index.php?r=scm-planner/get-pprr',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data);
        //alert(data);
        $('#ppr-snfg_komponen').attr('value',data.snfg_komponen);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });


    $.get('index.php?r=scm-planner/get-posisiprosesraw',{ snfg : snfg },function(data){
        var json = $.parseJSON(data);
        var tr;
        for (var i = 0; i < json.length; i++) {
            tr = $('<tr/>');
            tr.append("<td><span class='badge bg-grey'>" + json[i].snfg_komponen + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].posisi + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].state + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].jenis_proses + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].lanjutan + "</span></td>");
            if(json[i].is_done=="Done"){
                tr.append("<td><span class='badge bg-green'>" + json[i].is_done + "</span></td>");
            }else if(json[i].is_done=="Not Done"){
                tr.append("<td><span class='badge bg-red'>" + json[i].is_done + "</span></td>");
            }
            $('#table-last').append(tr);
        }
        $('#last-position').fadeIn("slow");

    });


    $.get('index.php?r=scm-planner/get-planner-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);

        if(data.status==null){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#create-button').hide();
            alert('Not Found');
        }

        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#start-button').hide();
            $('#stop-button').hide();

            $('#create-button').hide();
            alert('Status ditetapkan sebagai HOLD/PAUSE dari Planner')
        }
        else{
                
            $.get('index.php?r=kemas2/get-last-resolusi',{ nojadwal : snfg },function(data){
                var data = $.parseJSON(data);
                console.log(data);
                if(data===null){
                    $('#start-button').fadeIn("slow");
                    $('#stop-button').hide();

                    $('#start-button').click(function(){
                        var start = "START";
                        $('#kemas2-state').attr('value',start);
                        $('#kemas2-start').fadeIn("slow");
                        $('#kemas2-stop').hide();
                        
                        $('#kemas2-jenis_kemas').change(function(){

                            var jenis_kemas = $('#kemas2-jenis_kemas').val();
                            var snfg = $('#kemas2-snfg').val();

                                $.get('index.php?r=kemas2/lanjutan-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);

                                    $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);

                                });

                            });
                        });
                }else if(data.state=="START"){
                    $('#start-button').hide();
                    $('#stop-button').fadeIn("slow");

                    $('#kemas2-jenis_kemas_00').hide();
                    document.getElementById("kemas2-jenis_kemas").disabled = true;
                    $('#kemas2-jenis_kemas_0').fadeIn("slow");
                    document.getElementById("kemas2-jenis_kemas_1").disabled = false;
                    $('#kemas2-jenis_kemas_1').attr('value',data.jenis_proses);

                    // Assign Lanjutan


                    $('#stop-button').click(function(){
                        var stop = "STOP";
                        $('#kemas2-state').attr('value',stop);
                        $('#kemas2-stop').fadeIn("slow");
                        $('#kemas2-start').hide();
                        var jenis_kemas = $('#kemas2-jenis_kemas_1').val();
                        var snfg = $('#kemas2-snfg').val();

                            $.get('index.php?r=kemas2/lanjutan-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                                var data = $.parseJSON(data);
                                //alert(data.sediaan);
                                $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);

                                // Disable Post Value Lanjutan Istirahat

                            });

                    });


                    // EO Lanjutan

                }else if(data.state=="STOP"){
                    $('#start-button').fadeIn("slow");
                    $('#stop-button').hide();

                    $('#start-button').click(function(){
                        var start = "START";
                        $('#kemas2-state').attr('value',start);
                        $('#kemas2-start').fadeIn("slow");
                        $('#kemas2-stop').hide();
                        
                        $('#kemas2-jenis_kemas').change(function(){

                            var jenis_kemas = $('#kemas2-jenis_kemas').val();
                            var snfg = $('#kemas2-snfg').val();

                                $.get('index.php?r=kemas2/lanjutan-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);

                                    $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);

                                });

                            });
                        });

                }

            });
        }
    });


});



$(function() {

    
    // DynamicForm override JS , Script Fix for non-working kartik date/timepicker afterInsert 
    // DynamicForm override JS , Fix for select2 kendala list

    // ID untuk penomoran nested dynamic form
    var id = 0;


    // Setelah Insert Button

    $(".dynamicform_wrapper").on('afterInsert', function(e, item) {
        var dateTimePickers = $(this).find('[data-krajee-kvtimepicker]');
        dateTimePickers.each(function(index, el) {
            $(this).parent().removeData().kvTimepicker('remove');
            $(this).parent().kvTimepicker(eval($(this).attr('data-krajee-kvtimepicker')));
        });
        

        // Override Fix for select2 kendala list 

        id += 1;
        console.log(id);


        $.post("index.php?r=scm-planner/get-kendala-kemas-snfg&snfg="+$('#kemas2-snfg').val(), function (data){

                $("select#kendala-"+(id)+"-keterangan").html(data);

        });


    });

    // Setelah Delete Button

    $(".dynamicform_wrapper").on('afterDelete', function(e, item) {
        
        // Override Fix for select2 kendala list 

        id -= 1;
        console.log(id);

    });


    // Jika Mencapai Batas Maximum
    $(".dynamicform_wrapper").on('limitReached', function(e, item) {
        alert('Maximum 3');
    });


    $('#kemas2-jenis_kemas_0').hide();                                  //  Hide Jenis Proses Dropdown
    document.getElementById("kemas2-jenis_kemas_1").disabled = true;    //  Disable Jenis Proses auto-assign


    // $('#lsb_validator').hide();
    // $('#start-button').click(function(){
    //         var start = "START";
    //         $('#kemas2-state').attr('value',start);
    //         $('#kemas2-start').fadeIn("slow");
    //         $('#kemas2-stop').hide();
    //         $('#kemas2-jenis_kemas').change(function(){
    //             var jenis_kemas = $('#kemas2-jenis_kemas').val();
    //             var snfg = $('#kemas2-snfg').val();

    //             // assign lanjutan

    //             $.get('index.php?r=kemas2/lanjutan-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
    //                 var data = $.parseJSON(data);
    //                 //alert(data.sediaan);
    //                 $('#kemas2-lanjutan').attr('value',data.lanjutan);
    //                 $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
    //             });

    //         });
    //     });


    // $('#stop-button').click(function(){
    //         var stop = "STOP";
    //         $('#kemas2-state').attr('value',stop);
    //         $('#kemas2-stop').fadeIn("slow");
    //         $('#kemas2-start').hide();
    //         $('#kemas2-jenis_kemas').change(function(){
    //             var jenis_kemas = $('#kemas2-jenis_kemas').val();
    //             var snfg = $('#kemas2-snfg').val();

    //             // Assign Lanjutan
    //             $.get('index.php?r=kemas2/lanjutan-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
    //                 var data = $.parseJSON(data);
    //                 //alert(data.sediaan);

    //                 $('#kemas2-lanjutan').attr('value',data.lanjutan);
    //                 $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);

    //             });

                

    //         });
    // });



});



JS;
$this->registerJs($script);
?>
