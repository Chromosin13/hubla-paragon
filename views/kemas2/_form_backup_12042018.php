<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
use faryshta\widgets\JqueryTagsInput;
use yii\grid\GridView;
use kartik\checkbox\CheckboxX;
use app\models\Kemas1;
use app\models\PosisiProses;
use app\models\ScmPlanner;
use app\models\PosisiProsesReworkSb;
use app\models\PosisiProsesReworkPalet;
use yii\helpers\ArrayHelper;
use app\models\Kemas2;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\kemas2 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kemas2-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <div class="info-box bg-aqua">
        <span class="info-box-icon"><i class="fa fa-dropbox"></i></span>

        <div class="info-box-content">
          <b><div class="col-md-3 col-sm-6 col-xs-12" id="nama_fg_results"></div></b>
          <!-- <div id="nama_fg_results"></div> -->
          <span class="info-box-number"><div id="snfg_results"></div><div id="snfg_komponen_results"></div></span>

          <div class="progress">
            <div class="progress-bar" style="width: 100%"></div>
          </div>
              <span class="progress-description">
              <div class="col-md-3 col-sm-6 col-xs-12" id="streamline_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="start_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="due_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="besar_batch_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="besar_lot_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="lot_ke_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="kode_jadwal_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="jumlah_results"></div>
              </span>
        </div>
        <!-- /.info-box-content -->
    </div>

    <div class="box">

            <div class="box-header with-border">        
                    <h4><b>Pilih Jenis Scan</b></h4>            
                    <?=
                            Html::button('<b>Per-SNFG</b>',['class'=>'btn btn-success', 'id' => 'btn_snfg','style' => 'width: 120px; border-radius: 5px;']);
                    ?>
<!--                     <?=
                            Html::button('<b>Per-Komponen</b>',['class'=>'btn btn-primary', 'id' => 'btn_snfg_komponen', 'style' => 'width: 120px; border-radius: 5px;']);
                    ?> -->
            </div>



            <div style="width:50%; float: left; display: inline-block;" class="box-header with-border">
                    
                    <h2 class="box-title"> <b>Input dan Informasi SNFG dari Planner</b></h2>
                    <br \>
                    <br \>

                    <div id="form_snfg">
                        <?= $form->field($model, 'snfg', ['hintType' => ActiveField::HINT_SPECIAL, 
                                                          'addon' => [
                                                                        'append' => [
                                                                            'content'=>'<i class="fa fa-barcode"></i>'
                                                                        ],
                                                                        'prepend' => [
                                                                            'content' => Html::button('Tekan Setelah Scan',['class'=>'btn btn-primary','id'=>'afterScnBtnSnfg']),
                                                                            'asButton' => true
                                                                        ]
                                                         ]
                        ])->textInput(['placeholder'=>'Klik Disini, Scan pada Barcode SNFG yang tertera di SPK'])->hint('PASTIKAN CAPS LOCK TIDAK AKTIF!');
                        ?>
                    </div>

                    <div id="form_snfg_komponen">
                        <?= $form->field($model, 'snfg_komponen',  ['hintType' => ActiveField::HINT_SPECIAL, 
                                                                    'addon' => [
                                                                                'append' => [
                                                                                    'content'=>'<i class="fa fa-barcode"></i>'
                                                                                ],
                                                                                'prepend' => [
                                                                                    'content' => Html::button('Tekan Setelah Scan',['class'=>'btn btn-primary','id'=>'afterScnBtnKomponen']),
                                                                                    'asButton' => true
                                                                                ]
                                                            ]
                        ])->textInput(['disabled'=>'true','placeholder'=>'Klik Disini, Scan pada Barcode SNFG Komponen yang tertera di SPK'])->hint('Klik Pada Isian dibawah ini, kemudian arahkan scan pada barcode Komponen yang tertera di SPK');
                        ?>
                    </div>

                    <div id="form_start_stop">
                        <a class="btn btn-success" id="start-button">
                                    <i class="fa fa-play"></i> Start
                        </a>

                        <a class="btn btn-danger" id="stop-button">
                                    <i class="fa fa-pause"></i> Stop
                        </a>
                        <p \>
                        <p \>
                        
                        <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>
                        
                        
                    </div>


            </div>
            <div style="width:50%; display: inline-block;" class="box-header with-border">
                <h2 class="box-title"><b>Posisi Terakhir</b></h2>
                    <br \>
                    <br \>
                    <b>SNFG Komponen</b>
                    <input type="text" class="form-control" id="ppr-snfg_komponen" placeholder="" disabled>
                    <b>Posisi</b>
                    <input type="text" class="form-control" id="ppr-posisi" placeholder="" disabled>
                    <b>State</b>
                    <input type="text" class="form-control" id="ppr-state" placeholder="" disabled>
                    <b>Jenis Proses</b>
                    <input type="text" class="form-control" id="ppr-jenis-proses" placeholder="" disabled>
                    <b>Lanjutan</b>
                    <input type="text" class="form-control" id="ppr-lanjutan" placeholder="" disabled>
                    <b>Is Done</b>
                    <input type="text" class="form-control" id="ppr-isdone" placeholder="" disabled>
                    <br \>
                    <br \>
            </div>
    </div>


    <div id="form_jenis">

        <div id="lsb_validator">

            <div id="kemas2-jenis_kemas_00">
            <?= $form->field($model, 'jenis_kemas')->dropDownList(['PACKING' => 'PACKING' ,'PRESS+FILLING+PACKING' => 'PRESS+FILLING+PACKING', 'FILLING-PACKING_INLINE' => 'FILLING-PACKING_INLINE','FLAME-PACKING_INLINE' => 'FLAME-PACKING_INLINE', 'REWORK' => 'REWORK' ],['prompt'=>'Select Option']);
            ?>
            </div>

            <div id="kemas2-jenis_kemas_0">
            <?= $form->field($model, 'jenis_kemas')->textInput(['id'=>'kemas2-jenis_kemas_1', 'disabled'=>'true' , 'readonly' =>'true']);
            ?>
            </div>


        </div>

    </div>

            <!--     
                <div id='lanjutan_split_batch_form'>
                <?= $form->field($model, 'lanjutan_split_batch')->dropDownList(
                                    ArrayHelper::map(PosisiProsesReworkSb::find()->all()
                                    ,'lanjutan_split_batch','lanjutan_split_batch')
                                    ,['prompt'=>'Select Batch Split']

                );?>
            </div> 
            -->

            <!-- Palet Dropdown -->
            <!-- <div id='dropdown-palet_ke'>
                        <?= $form->field($model, 'palet_ke')->dropDownList(
                                ArrayHelper::map(PosisiProsesReworkPalet::find()->all()
                                ,'palet_ke','palet_ke')
                                ,['id'=>'kemas2-dropdown-palet_ke','prompt'=>'Select Palet Ke']


                        );?>
            </div> -->

            <!-- Palet Autoincrement -->
            <!-- <div id='palet_ke_form'>
            <?= $form->field($model, 'palet_ke')->textInput(['readonly' => 'true']) ?>
            </div> -->

    <div id="form_entry_start_stop">
            <div class="box" id="kemas2-start">
                    <div class="box-header with-border">
                      <h2 class="box-title">Start Entry</h2>

                        <?= $form->field($model, 'nama_line')->dropDownList(
                            ArrayHelper::map(Kemas2::find()->all()
                            ,'nama_line','nama_line')
                            ,['prompt'=>'Select Line']

                        );?>

                        <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>

                        <div id="kemas2-lanjutan_split_batch_start-form">
                        <?= $form->field($model, 'lanjutan_split_batch')->textInput(['id'=>'kemas2-lanjutan_split_batch_start','readonly' => 'true']) ?>
                        </div>
                        
                        <?php echo $form->field($model, 'nama_operator', ['hintType' => ActiveField::HINT_SPECIAL, 'addon' => ['prepend' => ['content'=>'<i class="fa fa-barcode"></i>']]
                                ])->widget(JqueryTagsInput::className([]))->hint(' <b>Scan</b> : Klik pada isian dibawah kemudian Scan Barcode pada Nametag<p \> <b>Manual</b> : isi dengan Format 4 Huruf Kapital, Contoh : "ABCD" lalu diikuti dengan menekan Enter ');
                        ?>

                    </div>
            </div>

            <div class="box" id="kemas2-stop">
                    <div class="box-header with-border">
                      <h2 class="box-title">Stop Entry</h2>
                        <?= $form->field($model, 'lanjutan')->textInput(['id'=>'kemas2-lanjutan_stop','readonly' => 'true']) ?>

                        <div id="kemas2-lanjutan_split_batch_stop-form">
                        <?= $form->field($model, 'lanjutan_split_batch')->textInput(['id'=>'kemas2-lanjutan_split_batch_stop','readonly' => 'true']) ?>
                        </div>

                        <?= $form->field($model, 'nobatch')->textInput() ?>

                        <?= $form->field($model, 'jumlah_realisasi')->textInput() ?>

                        <?= $form->field($model, 'counter')->textInput(['readonly'=>true]) ?>


                        <label>
                          <input type="checkbox" id="is_kendala">
                          Terdapat Kendala ?
                        </label>

                        <div id="kendala-form">
                            <?php DynamicFormWidget::begin([
                                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                'widgetBody' => '.container-items', // required: css class selector
                                'widgetItem' => '.item', // required: css class
                                'limit' => 3, // the maximum times, an element can be cloned (default 999)
                                'min' => 1, // 0 or 1 (default 1)
                                'insertButton' => '.add-item', // css class
                                'deleteButton' => '.remove-item', // css class
                                'model' => $modelsKendala[0],
                                'formId' => 'dynamic-form',
                                'formFields' => [
                                    'keterangan',
                                    'start',
                                    'stop',
                                ],
                            ]); ?>

                            <div class="container-items"><!-- widgetContainer -->
                                <?php foreach ($modelsKendala as $i => $modelKendala): ?>
                                <div class="item panel panel-default"><!-- widgetBody -->
                                    <div class="panel-heading">
                                        <h3 class="panel-title pull-left">Kendala</h3>
                                        <div class="pull-right">
                                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body">
                                        <?php
                                            // necessary for update action.
                                            if (! $modelKendala->isNewRecord) {
                                                echo Html::activeHiddenInput($modelKendala, "[{$i}]id");
                                            }
                                        ?>

                                        <div class="row">
                                            <div class="col-sm-3">


                                                <?= $form->field($modelKendala, "[{$i}]keterangan")->widget(Select2::classname(), [
                                                                    'data' => ArrayHelper::map(Kendala::find()->all()
                                                            ,'keterangan','keterangan'),
                                                                    'options' => ['placeholder' => 'Select Kendala'],
                                                                    'pluginOptions' => [
                                                                        // 'tags' => true,
                                                                        'allowClear' => true
                                                                    ],
                                                                    ]);
                                                ?>

                                            </div>
                                            <div class="col-sm-3">
                                                <?= $form->field($modelKendala, "[{$i}]start")->widget(TimePicker::classname(), [
                                                                'options' => ['placeholder' => 'Start'],
                                                                'pluginOptions' => [
                                                                        'minuteStep' => 5,
                                                                        'autoclose'=>true,
                                                                        'showMeridian' => false,
                                                                        'defaultTime' => '00:00'
                                                                ]
                                                ]); ?>
                                            </div>
                                        </div><!-- .row -->
                                    </div>
                                </div>
                                <?php endforeach; ?>
                                </div>
                                <?php DynamicFormWidget::end(); ?>
                        </div>

                        <p \>

                        <div id="is_done-form">
                        <?php
                            echo '<label class="cbx-label" for="is_done">Is Done?  </label>';
                            echo CheckboxX::widget([
                                'model' => $model,
                                'attribute' => 'is_done',
                                'pluginOptions' => [
                                    'threeState' => false,
                                    'size' => 'sm'
                                ]
                            ]);
                        ?>
                        </div>

                        <div id="batch_split-form">
                        <?php
                            echo '<label class="cbx-label" for="batch_split">Split Batch?</label>';
                            echo CheckboxX::widget([
                                'model' => $model,
                                'attribute' => 'batch_split',
                                'pluginOptions' => [
                                    'threeState' => false,
                                    'size' => 'sm'
                                ]
                            ]);
                        ?>
                        </div>

        <!--                 <div id="palet_ke-form">
                        <?php
                            echo '<label class="cbx-label" for="palet_flag">Tambah Palet?</label>';
                            echo CheckboxX::widget([
                                'model' => $model,
                                'attribute' => 'palet_flag',
                                'pluginOptions' => [
                                    'threeState' => false,
                                    'size' => 'sm'
                                ]
                            ]);
                        ?>
                        </div> -->

                    </div>
            </div>

            <div class="box" id="kemas2-istirahat-start">
                    <div class="box-header with-border">
                      <h2 class="box-title">Istirahat Start Entry</h2>
                         <?= $form->field($model, 'lanjutan')->textInput(['id'=>'kemas2-lanjutan_istirahat_start','readonly' => 'true']) ?>

                         <div id="kemas2-lanjutan_split_batch_istirahat_start-form">
                         <?= $form->field($model, 'lanjutan_split_batch')->textInput(['id'=>'kemas2-lanjutan_split_batch_istirahat_start','readonly' => 'true']) ?>
                         </div>

                         <?= $form->field($model, 'lanjutan_ist')->textInput(['id'=>'kemas2-lanjutan-ist_istirahat_start','readonly' => 'true']) ?>
                    </div>
            </div>

            <div class="box" id="kemas2-istirahat-stop">
                    <div class="box-header with-border">
                      <h2 class="box-title">Istirahat Stop Entry</h2>
                         <?= $form->field($model, 'lanjutan')->textInput(['id'=>'kemas2-lanjutan_istirahat_stop','readonly' => 'true']) ?>

                         <div id="kemas2-lanjutan_split_batch_istirahat_stop-form">
                         <?= $form->field($model, 'lanjutan_split_batch')->textInput(['id'=>'kemas2-lanjutan_split_batch_istirahat_stop','readonly' => 'true']) ?>
                         </div>

                         <?= $form->field($model, 'lanjutan_ist')->textInput(['id'=>'kemas2-lanjutan-ist_istirahat_stop','readonly' => 'true']) ?>
                    </div>
            </div>

            <div class="form-group" id="create-button">
                
                <?php echo Dialog::widget([
                        'libName' => 'krajeeDialogCust', // a custom lib name
                        'options' => [  // customized BootstrapDialog options
                            'size' => Dialog::SIZE_LARGE, // large dialog text
                            'type' => Dialog::TYPE_WARNING, // bootstrap contextual color
                            'title' => 'Konfirmasi Input',
                            'buttons' => [
                                [
                                    
                                    'id' => 'btn-ok',
                                    'icon' => 'glyphicon glyphicon-check',
                                    'label' => 'Sudah Benar, Submit!',
                                    'cssClass' => 'btn-primary',
                                    'action' => new JsExpression("function() {
                                        $('#submitButton').submit();            
                                        }")
                                    // Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
                                ],
                            ]
                        ]
                    ]);
                ?>

                <button type="button" id="btn-custom" class="btn btn-info">Validate</button>
                <button id="submitButton" type="submit" class="btn btn-success">Creates</button>
                <!-- <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?> -->
            </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

// Hide Submit Button untuk Konfirmasi Input
$('#submitButton').hide();


// Inisialisasi Form Hide
    $('#start-button').hide();
    $('#stop-button').hide(); 
    $('#form_start_stop').hide(); 
    $('#form_jenis').hide(); 
    $('#form_entry_start_stop').hide();

// After Klik Tombol Tekan Scan
    // Klik Scan SNFG

    $('#afterScnBtnSnfg').click(function(){
        var nomo = $('#kemas2-snfg').val();

        $.get('http://10.5.4.203/ptidms/php_action/test_get_redha.php',{ snfg : nomo },function(data){
                var data = $.parseJSON(data);
                var qtyaktual = data.qtyaktual;
                alert(qtyaktual);
                
                $('#kemas2-counter').attr('value',data.qtyaktual);

        });

        if(nomo==""){
            $('#form_start_stop').hide();    
        }
        else {
            $('#form_start_stop').show(); 
        }
    });    

    // $('#afterScnBtnSnfg').click(function(){
    //     $('#form_start_stop').show(); 
    // });


    // Klik Scan Komponen
    $('#afterScnBtnKomponen').click(function(){
        $('#form_start_stop').show();  
    });

// After Klik Start Stop Button
    $('#start-button').click(function(){
        $('#form_jenis').show();
    });

    $('#stop-button').click(function(){
        $('#form_jenis').show();
        $('#form_entry_start_stop').show();
    });

// After Select Jenis Kemas
    $('#kemas2-jenis_kemas').change(function(){
        var jenis_kemas = $('#kemas2-jenis_kemas').val();
        if(jenis_kemas==""){
            $('#form_entry_start_stop').hide(); 
        } else {
            $('#form_entry_start_stop').show();
            var state = $('#kemas2-state').val();
            if(state=="START"){
                $('#create-button').hide();
                $('#kemas2-nama_line').change(function(){
                    var nama_line = $('#kemas2-nama_line').val();
                    if(nama_line=="Select Line"){
                        $('#create-button').hide();
                    }else{
                        $('#create-button').show();
                    }
                });
            }
      
        }
    });

// Validate Button

$("#btn-custom").on("click", function() {
    var snfg = $('#kemas2-snfg').val();
    var state = $('#kemas2-state').val();
    var jenis_kemas = $('#kemas2-jenis_kemas').val();
    var jenis_kemas_1 = $('#kemas2-jenis_kemas_1').val();
    var lanjutan = $('#kemas2-lanjutan').val();
    var nama_line = $('#kemas2-nama_line').val();
    var nama_operator =  $('#kemas2-nama_operator').val();
    var jumlah_realisasi = $('#kemas2-jumlah_realisasi').val();   
    var is_done = $('#kemas2-is_done').val();
    var lanjutan_split_batch = $('#kemas2-lanjutan_split_batch_start').val();
    var lanjutan_split_batch_stop = $('#kemas2-lanjutan_split_batch_stop').val();
    var nobatch = $('#kemas2-nobatch').val();

    if(lanjutan==null||lanjutan_split_batch==null||lanjutan_split_batch_stop==null){
        alert('Penarikan data Lanjutan Gagal, Mohon refresh halaman dan scan ulang jadwal');
    }else if(state=="START"){
        krajeeDialogCust.dialog('<b>SNFG</b><p>' + '<font color="blue"><i>'+(snfg)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Kemas</b><p>' + '<font color="blue"><i>'+(jenis_kemas)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>' +
                                '<b>Nama Line</b><p>' + '<font color="blue"><i>'+(nama_line)+'</i></font><p>' +  
                                '<b>Nama Operator</b><p>' + '<font color="blue"><i>'+(nama_operator)+'</i></font><p>',function(result) {}
        );
    }else if(state=="STOP"&&is_done==1){
    
        krajeeDialogCust.dialog('<b>SNFG</b><p>' + '<font color="blue"><i>'+(snfg)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Kemas</b><p>' + '<font color="blue"><i>'+(jenis_kemas_1)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>'+
                                '<b>Nomor Batch</b><p>' + '<font color="blue"><i>'+(nobatch)+'</i></font><p>' +
                                '<b>Jumlah Realisasi</b><p>' + '<font color="blue"><i>'+(jumlah_realisasi)+'</i></font><p>' + 
                                '<b><font color="blue"><i>Jadwal Sudah Selesai (Tidak Lanjut Shift)</i></font></b><p>',function(result) {}
        );

    }else if(state=="STOP"&&is_done==0&&lanjutan_split_batch==1){

        krajeeDialogCust.dialog('<b>SNFG</b><p>' + '<font color="blue"><i>'+(snfg)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Kemas</b><p>' + '<font color="blue"><i>'+(jenis_kemas_1)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>'+
                                '<b>Nomor Batch</b><p>' + '<font color="blue"><i>'+(nobatch)+'</i></font><p>' +
                                '<b>Jumlah Realisasi</b><p>' + '<font color="blue"><i>'+(jumlah_realisasi)+'</i></font><p>' +
                                '<b><font color="blue"><i>Jadwal Akan Dilanjutkan ke Shift Berikutnya (Belum Done) dan Terdapat Batch Split!</i></font></b><p>' ,function(result) {}
        );

    }else if(state=="STOP"&&is_done==0&&lanjutan_split_batch!=1){

        krajeeDialogCust.dialog('<b>SNFG</b><p>' + '<font color="blue"><i>'+(snfg)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Kemas</b><p>' + '<font color="blue"><i>'+(jenis_kemas_1)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>'+
                                '<b>Nomor Batch</b><p>' + '<font color="blue"><i>'+(nobatch)+'</i></font><p>' +
                                '<b>Jumlah Realisasi</b><p>' + '<font color="blue"><i>'+(jumlah_realisasi)+'</i></font><p>' +
                                '<b><font color="blue"><i>Jadwal Akan Dilanjutkan ke Shift Berikutnya (Belum Done)</i></font></b><p>' ,function(result) {}
        );

    }

});



$('#form_snfg_komponen').hide(); 
$('#form_snfg').hide(); 

$('#btn_snfg').click(function(){
    document.getElementById("kemas2-snfg_komponen").disabled = true
    document.getElementById("kemas2-snfg").disabled = false;
    $('#form_snfg_komponen').hide(); 
    $('#form_snfg').show(); 
});

$('#btn_snfg_komponen').click(function(){
    document.getElementById("kemas2-snfg_komponen").disabled = false
    document.getElementById("kemas2-snfg").disabled = true;
    $('#form_snfg_komponen').show();
    $('#form_snfg').hide();
});


$('#is_kendala').change(function(){

    if(document.getElementById('is_kendala').checked){
            $('#kendala-form').show();

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);

                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = false;
                    document.getElementById("kendala-"+(i)+"-start").disabled = false;
        
                    i++;
                }

    }else{
            $('#kendala-form').hide();

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);
                    
                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = true;
                    document.getElementById("kendala-"+(i)+"-start").disabled = true;  

                    i++;
                }

    } 
 });


$('#kemas2-is_done').change(function(){
    var data = $(this).val();;
    if(data==1){
        $('#batch_split-form').hide();
    }
    else if(data==0){
        $('#batch_split-form').show();
    }
});

$('#kemas2-batch_split').change(function(){
    var data = $(this).val();;
    if(data==1){
        $('#is_done-form').hide();
    }
    else if(data==0){
        $('#is_done-form').show();
    }
});


// $(function() {
//     // $('#dropdown-palet_ke').hide();
//     // document.getElementById("kemas2-dropdown-palet_ke").disabled = true;
//     $('#lanjutan_split_batch_form').hide();
//     document.getElementById("kemas2-lanjutan_split_batch").disabled = true;
// });


// $('#per_snfg_komponen').change(function(){
//     var per_snfg_komponen = document.getElementById("kemas2-snfg_komponen").disabled;
//     if(per_snfg_komponen){
//         document.getElementById("kemas2-snfg_komponen").disabled = false
//         document.getElementById("kemas2-snfg").disabled = true;
//     }else{
//         document.getElementById("kemas2-snfg_komponen").disabled = true
//         document.getElementById("kemas2-snfg").disabled = false;
//     }
// });

$('#kemas2-snfg').change(function(){
    var snfg = $(this).val();

    $.post("index.php?r=scm-planner/get-line-kemas-snfg&snfg="+$('#kemas2-snfg').val(), function (data){
        $("select#kemas2-nama_line").html(data);
    });

    $.post("index.php?r=kemas-2/batch-split-assign-snfg&snfg="+$(this).val(), function (data){
            $("select#kemas2-lanjutan_split_batch").html(data);
    });

    // $.post("index.php?r=qc-timbang-fg/palet-assign-snfg&snfg="+$(this).val(), function (data){
    //         $("select#kemas2-dropdown-palet_ke").html(data);
    // });

    // Assign Get Kendala Kemas
    $.post("index.php?r=scm-planner/get-kendala-kemas-snfg&snfg="+$('#kemas2-snfg').val(), function (data){
            $("select#kendala-0-keterangan").html(data);
    });

    $.get('index.php?r=kemas2/check-rework-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        if(data == null){
                $('#kemas2-jenis_kemas').change(function(){
                    var jenis_kemas = $(this).val();
                    if(jenis_kemas=='REWORK'){
                        alert('case : rework forbidden to start karena ga ada rework');
                        $('#create-button').hide();
                    }else{
                        alert('case : karena nggak milih rework dan ga ada rework ya lanjut, ga ngapa2in, otomatis counting sb nya');
                        $('#create-button').show();
                    }
                });
        } else {

                $('#kemas2-jenis_kemas').change(function(){
                    var jenis_kemas = $(this).val();
                    if(jenis_kemas=='REWORK'){

                        alert('case : rework dan harus pilih nomor reworknya');

                        /// SHOW//

                            // Palet Show DropDown List
                            // $('#dropdown-palet_ke').show();
                            // document.getElementById("kemas2-dropdown-palet_ke").disabled = false;


                            // Show Kemas 2 Split Batch Dropdown List
                            $('#lanjutan_split_batch_form').show();
                            document.getElementById("kemas2-lanjutan_split_batch").disabled = false;

                        /// HIDE //////

                        // HIDE Lanjutan Split Batch Automatis di Form Start , Stop, Ist Start, Ist Stop


                        // $('#palet_ke_form').hide();
                        // document.getElementById("kemas2-palet_ke").disabled = true;

                        $('#kemas2-lanjutan_split_batch_start-form').hide();
                        document.getElementById("kemas2-lanjutan_split_batch_start").disabled = true;

                        $('#kemas2-lanjutan_split_batch_stop-form').hide();
                        document.getElementById("kemas2-lanjutan_split_batch_stop").disabled = true;

                        $('#kemas2-lanjutan_split_batch_istirahat_start-form').hide();
                        document.getElementById("kemas2-lanjutan_split_batch_istirahat_start").disabled = true;

                        $('#kemas2-lanjutan_split_batch_istirahat_stop-form').hide();
                        document.getElementById("kemas2-lanjutan_split_batch_istirahat_stop").disabled = true;

                    }else{
                        alert('case : nggak milih jenis kemas rework jadi lanjut otomatis counting sb nya');


                    /// HIDE

                            // Palet Show DropDown List
                            // $('#dropdown-palet_ke').hide();
                            // document.getElementById("kemas2-dropdown-palet_ke").disabled = true;


                            // Show Kemas 2 Split Batch Dropdown List
                            $('#lanjutan_split_batch_form').hide();
                            document.getElementById("kemas2-lanjutan_split_batch").disabled = true;


                    // SHOW

                        // $('#palet_ke_form').show();
                        // document.getElementById("kemas2-palet_ke").disabled = false;

                        $('#kemas2-lanjutan_split_batch_start-form').show();
                        document.getElementById("kemas2-lanjutan_split_batch_start").disabled = false;

                        $('#kemas2-lanjutan_split_batch_stop-form').show();
                        document.getElementById("kemas2-lanjutan_split_batch_stop").disabled = false;

                    }

                });
        }

    });

    $.get('index.php?r=scm-planner/get-planner-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        $('#kemas2-snfg_komponen').attr('value',data.snfg_komponen);
        $('#nomo').attr('value',data.nomo);


        $('#streamline_results').html('STREAMLINE : '+data.streamline);
        $('#start_results').html('START : '+ data.start);
        $('#due_results').html('DUE : '+ data.due);
        $('#nama_bulk_results').html(data.nama_bulk);
        $('#nama_fg_results').html(data.nama_fg);
        $('#besar_batch_results').html('BESAR BATCH : '+ data.besar_batch);
        $('#besar_lot_results').html('BESAR LOT : '+ data.besar_lot);
        $('#lot_ke_results').html('LOT KE : '+ data.lot); ;
        $('#kode_jadwal_results').html('KODE JADWAL : '+ data.kode_jadwal);
        $('#jumlah_results').html('JUMLAH : '+ data.jumlah);


    });
    $.get('index.php?r=pengolahan/get-pengolahan',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#no-batch').attr('value',data.nobatch);
        $('#besar-batch-real').attr('value',data.besar_batch_real);
    });
    $.get('index.php?r=scm-planner/get-pprr',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data);
        //alert(data);
        $('#ppr-snfg_komponen').attr('value',data.snfg_komponen);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });
    $.get('index.php?r=scm-planner/get-planner-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);

        if(data.status==null){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Not Found');
        }

        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#start-button').hide();
            $('#stop-button').hide();

            $('#create-button').hide();
            alert('Status ditetapkan sebagai HOLD/PAUSE dari Planner')
        }
        else{
                $.get('index.php?r=scm-planner/get-last-induk',{ snfg : snfg },function(data){
                    var data = $.parseJSON(data);
                    if(data.posisi=="KEMAS 2" && data.state=="START"){
                        $('#start-button').hide();
                        $('#stop-button').show();

                        $('#kemas2-jenis_kemas_00').hide();
                        document.getElementById("kemas2-jenis_kemas").disabled = true;
                        $('#kemas2-jenis_kemas_0').show();
                        document.getElementById("kemas2-jenis_kemas_1").disabled = false;
                        $('#kemas2-jenis_kemas_1').attr('value',data.jenis_proses);

                        // Assign Lanjutan


                        $('#stop-button').click(function(){

                            var jenis_kemas = $('#kemas2-jenis_kemas_1').val();
                            var snfg = $('#kemas2-snfg').val();

                                $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                                        var data = $.parseJSON(data);
                                        $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);
                                        // fetch data lanjutan split batch

                                        var lanjutan_split_batch = $('#kemas2-lanjutan_split_batch_start').val();

                                        // inject to get palet

                                        $.get('index.php?r=kemas2/palet-ke-snfg',{ snfg : snfg, jenis_kemas : jenis_kemas, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                            var data = $.parseJSON(data);
                                            // $('#kemas2-palet_ke').attr('value',data.palet_ke);
                                        });
                                });



                                $.get('index.php?r=kemas2/lanjutan-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);

                                    // Disable Post Value Lanjutan Istirahat

                                    document.getElementById("kemas2-lanjutan-ist_istirahat_start").disabled = true;
                                    document.getElementById("kemas2-lanjutan-ist_istirahat_stop").disabled = true;

                                });

                        });


                        // EO Lanjutan


                    }else if(data.posisi=="KEMAS 2" && data.state=="STOP"){
                        $('#start-button').show();
                        $('#stop-button').hide();

                        $('#start-button').click(function(){

                            $('#kemas2-jenis_kemas').change(function(){

                                var jenis_kemas = $('#kemas2-jenis_kemas').val();
                                var snfg = $('#kemas2-snfg').val();

                                    $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                                            var data = $.parseJSON(data);

                                            $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                                            $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);

                                            // fetch data lanjutan split batch

                                            var lanjutan_split_batch = $('#kemas2-lanjutan_split_batch_start').val();

                                            // inject to get palet

                                            $.get('index.php?r=kemas2/palet-ke-snfg',{ snfg : snfg, jenis_kemas : jenis_kemas, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                                var data = $.parseJSON(data);
                                                // $('#kemas2-palet_ke').attr('value',data.palet_ke);
                                            });
                                            //
                                    });

                                    $.get('index.php?r=kemas2/lanjutan-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.sediaan);

                                        $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                        $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);


                                        });
                                });
                            });

                    }else if(data.posisi!="KEMAS 2" && data!=null){
                        $('#start-button').show();
                        $('#stop-button').hide();
                        alert('Jadwal Baru');
                
                    }else {
                        $('#start-button').hide();
                        $('#stop-button').hide();

                        alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                    }
                });
        }
    });
});



// everything that relates to the change of snfg komponen value
$('#kemas2-snfg_komponen').change(function(){
    var snfg_komponen = $(this).val();

    // Assign Get Kendala Kemas
    $.post("index.php?r=scm-planner/get-kendala-kemas-komponen&snfg_komponen="+$('#kemas2-snfg_komponen').val(), function (data){
            $("select#kendala-0-keterangan").html(data);
    });


    // Assign Nama Line

    $.post("index.php?r=scm-planner/get-line-kemas-komponen&snfg_komponen="+$('#kemas2-snfg_komponen').val(), function (data){
        $("select#kemas2-nama_line").html(data);
    });

    // assign dropdown data for lanjutan_split_batch

    $.post("index.php?r=kemas-2/batch-split-assign-komponen&snfg_komponen="+$(this).val(), function (data){
            $("select#kemas2-lanjutan_split_batch").html(data);
    });

    // $.post("index.php?r=qc-timbang-fg/palet-assign-komponen&snfg_komponen="+$(this).val(), function (data){
    //         $("select#kemas2-dropdown-palet_ke").html(data);
    // });

    $.get('index.php?r=kemas2/check-rework-komponen',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        if(data == null){
            $('#kemas2-jenis_kemas').change(function(){
                    var jenis_kemas = $(this).val();
                    if(jenis_kemas=='REWORK'){
                        alert('case : rework forbidden to start karena ga ada rework');
                        $('#create-button').hide();
                    }else{
                        alert('case : karena nggak milih rework dan ga ada rework ya lanjut, ga ngapa2in, otomatis counting sb nya');
                        $('#create-button').show();
                    }
                });
        } else {

            $('#kemas2-jenis_kemas').change(function(){
                    var jenis_kemas = $(this).val();
                    if(jenis_kemas=='REWORK'){
                        /// SHOW//

                            // Palet Show DropDown List
                            // $('#dropdown-palet_ke').show();
                            // document.getElementById("kemas2-dropdown-palet_ke").disabled = false;


                            // Show Kemas 2 Split Batch Dropdown List
                            $('#lanjutan_split_batch_form').show();
                            document.getElementById("kemas2-lanjutan_split_batch").disabled = false;



                        // HIDE Lanjutan Split Batch Automatis di Form Start , Stop, Ist Start, Ist Stop

                        // $('#palet_ke_form').hide();
                        // document.getElementById("kemas2-palet_ke").disabled = true;


                        $('#kemas2-lanjutan_split_batch_start-form').hide();
                        document.getElementById("kemas2-lanjutan_split_batch_start").disabled = true;

                        $('#kemas2-lanjutan_split_batch_stop-form').hide();
                        document.getElementById("kemas2-lanjutan_split_batch_stop").disabled = true;

                        $('#kemas2-lanjutan_split_batch_istirahat_start-form').hide();
                        document.getElementById("kemas2-lanjutan_split_batch_istirahat_start").disabled = true;

                        $('#kemas2-lanjutan_split_batch_istirahat_stop-form').hide();
                        document.getElementById("kemas2-lanjutan_split_batch_istirahat_stop").disabled = true;

                    }else{
                        alert('case : nggak milih jenis kemas rework jadi lanjut otomatis counting sb nya');
                        /// HIDE

                            // Palet Show DropDown List
                            // $('#dropdown-palet_ke').hide();
                            // document.getElementById("kemas2-dropdown-palet_ke").disabled = true;


                            // Show Kemas 2 Split Batch Dropdown List
                            $('#lanjutan_split_batch_form').hide();
                            document.getElementById("kemas2-lanjutan_split_batch").disabled = true;


                        // SHOW

                        // $('#palet_ke_form').show();
                        // document.getElementById("kemas2-palet_ke").disabled = false;

                        $('#kemas2-lanjutan_split_batch_start-form').show();
                        document.getElementById("kemas2-lanjutan_split_batch_start").disabled = false;

                        $('#kemas2-lanjutan_split_batch_stop-form').show();
                        document.getElementById("kemas2-lanjutan_split_batch_stop").disabled = false;

                        $('#kemas2-lanjutan_split_batch_istirahat_start-form').show();
                        document.getElementById("kemas2-lanjutan_split_batch_istirahat_start").disabled = false;

                        $('#kemas2-lanjutan_split_batch_istirahat_stop-form').show();
                        document.getElementById("kemas2-lanjutan_split_batch_istirahat_stop").disabled = false;
                    }

                });

        }

    });

    $.get('index.php?r=kemas-2/check-batch-split',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        if (data == null){
           alert('Komponen Tidak terdapat di Proses Kemas 1');
        } else if(data.last_status==1){
            // $.get('index.php?r=kemas-2/get-batch-split-list',{ snfg_komponen : snfg_komponen },function(data){
            //     var data = $.parseJSON(data);

            // });



        } else if(data.last_status==0){
            alert('Proses pada Kemas 1 belum selesai')
            $('#start-button').hide();
            $('#stop-button').hide();
            $('#istirahat-start-button').hide();
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
        } else {
        }
    });
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        $('#kemas2-snfg').attr('value',data.snfg);
        $('#nomo').attr('value',data.nomo);

        $('#streamline_results').html('STREAMLINE : '+data.streamline);
        $('#start_results').html('START : '+ data.start);
        $('#due_results').html('DUE : '+ data.due);
        $('#nama_bulk_results').html(data.nama_bulk);
        $('#nama_fg_results').html(data.nama_fg);
        $('#besar_batch_results').html('BESAR BATCH : '+ data.besar_batch);
        $('#besar_lot_results').html('BESAR LOT : '+ data.besar_lot);
        $('#lot_ke_results').html('LOT KE : '+ data.lot); ;
        $('#kode_jadwal_results').html('KODE JADWAL : '+ data.kode_jadwal);
        $('#jumlah_results').html('JUMLAH : '+ data.jumlah);

    });
    $.get('index.php?r=scm-planner/get-ppr',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        $('#ppr-snfg_komponen').attr('value',data.snfg_komponen);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#start-button').hide();
            $('#stop-button').hide();
            $('#create-button').hide();
            alert('Terdapat SNFG Komponen yang masih HOLD/PAUSE dari Planner')
        }
        else{
                $.get('index.php?r=scm-planner/get-last',{ snfg_komponen : snfg_komponen },function(data){
                    var data = $.parseJSON(data);
                    if(data.posisi=="KEMAS 2" && data.state=="START"){
                        $('#start-button').hide();
                        $('#stop-button').show();


                        $('#kemas2-jenis_kemas_00').hide();
                        document.getElementById("kemas2-jenis_kemas").disabled = true;
                        $('#kemas2-jenis_kemas_0').show();
                        document.getElementById("kemas2-jenis_kemas_1").disabled = false;
                        $('#kemas2-jenis_kemas_1').attr('value',data.jenis_proses);

                        // Assign Lanjutan



                        $('#stop-button').click(function(){

                            var jenis_kemas = $('#kemas2-jenis_kemas_1').val();
                            var snfg_komponen = $('#kemas2-snfg_komponen').val();

                                $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                        var data = $.parseJSON(data);
                                        $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);

                                        var lanjutan_split_batch = $('#kemas2-lanjutan_split_batch_start').val();

                                        $.get('index.php?r=kemas2/palet-ke-komponen',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                                var data = $.parseJSON(data);
                                                // $('#kemas2-palet_ke').attr('value',data.palet_ke);

                                        });


                                });

                                $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);

                                    // Disable Post Value Lanjutan Istirahat

                                    document.getElementById("kemas2-lanjutan-ist_istirahat_start").disabled = true;
                                    document.getElementById("kemas2-lanjutan-ist_istirahat_stop").disabled = true;

                                });

                        });


                        // EO Lanjutan


                    }else if(data.posisi=="KEMAS 2" && data.state=="STOP"){
                        $('#start-button').show();
                        $('#stop-button').hide();

                        $('#start-button').click(function(){

                            $('#kemas2-jenis_kemas').change(function(){

                                var jenis_kemas = $('#kemas2-jenis_kemas').val();
                                var snfg_komponen = $('#kemas2-snfg_komponen').val();

                                    $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.sediaan);

                                        $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                        $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);

                                        });
                                });
                            });
                    }else if(data.posisi!="KEMAS 2" && data!=null){
                        $('#start-button').show();
                        $('#stop-button').hide();
                        alert('Jadwal Baru');
                    }else {
                        $('#start-button').show();
                        $('#stop-button').show();
                        alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                    }
                });
        }
    });
});



$(function() {

    // Hide and Disable DynamicFormWidget for Kendala Model //

        $('#kendala-form').hide();
            document.getElementById("kendala-0-keterangan").disabled = true;
            document.getElementById("kendala-0-start").disabled = true;


    // DynamicForm override JS , Script Fix for non-working kartik date/timepicker afterInsert 
    // DynamicForm override JS , Fix for select2 kendala list

        // ID untuk penomoran nested dynamic form
        var id = 0;


        // Setelah Insert Button

        $(".dynamicform_wrapper").on('afterInsert', function(e, item) {
            var dateTimePickers = $(this).find('[data-krajee-kvtimepicker]');
            dateTimePickers.each(function(index, el) {
                $(this).parent().removeData().kvTimepicker('remove');
                $(this).parent().kvTimepicker(eval($(this).attr('data-krajee-kvtimepicker')));
            });
            

            // Override Fix for select2 kendala list 

            id += 1;
            console.log(id);


            $.post("index.php?r=scm-planner/get-kendala-kemas-komponen&snfg_komponen="+$('#kemas2-snfg_komponen').val(), function (data){

                    $("select#kendala-"+(id)+"-keterangan").html(data);

            });

            $.post("index.php?r=scm-planner/get-kendala-kemas-snfg&snfg="+$('#kemas2-snfg').val(), function (data){

                    $("select#kendala-"+(id)+"-keterangan").html(data);

            });


        });

        // Setelah Delete Button

        $(".dynamicform_wrapper").on('afterDelete', function(e, item) {
            
            // Override Fix for select2 kendala list 

            id -= 1;
            console.log(id);

        });


        // Jika Mencapai Batas Maximum
        $(".dynamicform_wrapper").on('limitReached', function(e, item) {
            alert('Maximum 3');
        });


    $('#kemas2-jenis_kemas_0').hide();                                  //  Hide Jenis Proses Dropdown
    document.getElementById("kemas2-jenis_kemas_1").disabled = true;    //  Disable Jenis Proses auto-assign

    // Original
    $('#kemas2-start').hide();
    $('#kemas2-stop').hide();
    $('#kemas2-istirahat-start').hide();
    $('#kemas2-istirahat-stop').hide();
    $('#lsb_validator').hide();
    $('#start-button').click(function(){
            var start = "START";
            $('#kemas2-state').attr('value',start);
                    $('#kemas2-start').show();
                    $('#kemas2-stop').hide();
                    $('#kemas2-istirahat-start').hide();
                    $('#kemas2-istirahat-stop').hide();
                    $('#lsb_validator').show();
            $('#kemas2-jenis_kemas').change(function(){
                var jenis_kemas = $('#kemas2-jenis_kemas').val();
                var snfg = $('#kemas2-snfg').val();
                var snfg_komponen = $('#kemas2-snfg_komponen').val();
                var per_snfg_komponen = document.getElementById("kemas2-snfg_komponen").disabled;
                if(per_snfg_komponen){
                    alert('Per SNFG');
                    $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);
                            // fetch data lanjutan split batch

                            var lanjutan_split_batch = $('#kemas2-lanjutan_split_batch_start').val();

                            // inject to get palet

                            $.get('index.php?r=kemas2/palet-ke-snfg',{ snfg : snfg, jenis_kemas : jenis_kemas, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                var data = $.parseJSON(data);
                                // $('#kemas2-palet_ke').attr('value',data.palet_ke);
                            });
                            //
                    });

                    // assign lanjutan

                    $.get('index.php?r=kemas2/lanjutan-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                        var data = $.parseJSON(data);
                        //alert(data.sediaan);
                        $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                        $('#kemas2-lanjutan').attr('value',data.lanjutan);
                        $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                        $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                    });

                }else {

                    alert('Per Komponen')
                    $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                    });

                    $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);

                            var lanjutan_split_batch = $('#kemas2-lanjutan_split_batch_start').val();

                            $.get('index.php?r=kemas2/palet-ke-komponen',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                    var data = $.parseJSON(data);
                                    // $('#kemas2-palet_ke').attr('value',data.palet_ke);

                            });


                    });
                }
            });
        });

    $('#istirahat-start-button').click(function(){
            var ist_start = "ISTIRAHAT START";
            $('#kemas2-state').attr('value',ist_start);
                    $('#kemas2-istirahat-start').show();
                    $('#kemas2-stop').hide();
                    $('#kemas2-start').hide();
                    $('#kemas2-istirahat-stop').hide();
                    $('#lsb_validator').show();
            $('#kemas2-jenis_kemas').change(function(){
                var jenis_kemas = $('#kemas2-jenis_kemas').val();
                var snfg = $('#kemas2-snfg').val();
                var snfg_komponen = $('#kemas2-snfg_komponen').val();
                var per_snfg_komponen = document.getElementById("kemas2-snfg_komponen").disabled;
                if(per_snfg_komponen){

                    alert('NAMBAH LANJUTAN PER SNFG')

                    $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);
                            // fetch data lanjutan split batch

                            var lanjutan_split_batch = $('#kemas2-lanjutan_split_batch_start').val();

                            // inject to get palet

                            $.get('index.php?r=kemas2/palet-ke-snfg',{ snfg : snfg, jenis_kemas : jenis_kemas, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                var data = $.parseJSON(data);
                                // $('#kemas2-palet_ke').attr('value',data.palet_ke);
                            });
                            //
                    });

                    $.get('index.php?r=kemas2/lanjutan-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                            var lanjutan = $('#kemas2-lanjutan_istirahat_start').val();
                            $.get('index.php?r=kemas2/lanjutan-ist-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas, lanjutan : lanjutan },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.lanjutan_ist);
                                    $('#kemas2-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                    $('#kemas2-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                            });
                    });

                }else{
                    alert('NAMBAH LANJUTAN PER SNFG KOMPONEN')

                    $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);

                            var lanjutan_split_batch = $('#kemas2-lanjutan_split_batch_start').val();

                            $.get('index.php?r=kemas2/palet-ke-komponen',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                    var data = $.parseJSON(data);
                                    // $('#kemas2-palet_ke').attr('value',data.palet_ke);

                            });


                    });

                    $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                            var lanjutan = $('#kemas2-lanjutan_istirahat_start').val();
                                $.get('index.php?r=kemas2/lanjutan-ist-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas, lanjutan : lanjutan },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.lanjutan_ist);
                                        $('#kemas2-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                        $('#kemas2-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                });

                    });

                }
            });

        });

    $('#istirahat-stop-button').click(function(){
            var ist_stop = "ISTIRAHAT STOP";
            $('#kemas2-state').attr('value',ist_stop);
                    $('#kemas2-istirahat-stop').show();
                    $('#kemas2-stop').hide();
                    $('#kemas2-istirahat-start').hide();
                    $('#kemas2-start').hide();
                    $('#lsb_validator').show();
            $('#kemas2-jenis_kemas').change(function(){
                var jenis_kemas = $('#kemas2-jenis_kemas').val();
                var snfg = $('#kemas2-snfg').val();
                var snfg_komponen = $('#kemas2-snfg_komponen').val();
                var per_snfg_komponen = document.getElementById("kemas2-snfg_komponen").disabled;
                if(per_snfg_komponen){

                    $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);
                            // fetch data lanjutan split batch

                            var lanjutan_split_batch = $('#kemas2-lanjutan_split_batch_start').val();

                            // inject to get palet

                            $.get('index.php?r=kemas2/palet-ke-snfg',{ snfg : snfg, jenis_kemas : jenis_kemas, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                var data = $.parseJSON(data);
                                // $('#kemas2-palet_ke').attr('value',data.palet_ke);
                            });
                            //
                    });

                    $.get('index.php?r=kemas2/lanjutan-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                            $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                            var lanjutan = $('#kemas2-lanjutan_istirahat_start').val();
                                            $.get('index.php?r=kemas2/lanjutan-ist-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas, lanjutan : lanjutan },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.lanjutan_ist);
                                                    $('#kemas2-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                    $('#kemas2-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                            });
                    });

                }else{

                    alert('Per Komponen');

                    $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);

                            var lanjutan_split_batch = $('#kemas2-lanjutan_split_batch_start').val();

                            $.get('index.php?r=kemas2/palet-ke-komponen',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                    var data = $.parseJSON(data);
                                    // $('#kemas2-palet_ke').attr('value',data.palet_ke);

                            });


                    });

                    $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                            var lanjutan = $('#kemas2-lanjutan_istirahat_start').val();
                                $.get('index.php?r=kemas2/lanjutan-ist-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas, lanjutan : lanjutan },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.lanjutan_ist);
                                        $('#kemas2-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                        $('#kemas2-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                });

                    });

                }
            });
        });

    $('#stop-button').click(function(){
             var stop = "STOP";
             $('#kemas2-state').attr('value',stop);
                    $('#kemas2-stop').show();
                    $('#kemas2-start').hide();
                    $('#kemas2-istirahat-start').hide();
                    $('#kemas2-istirahat-stop').hide();
                    $('#lsb_validator').show();
            $('#kemas2-jenis_kemas').change(function(){
                var jenis_kemas = $('#kemas2-jenis_kemas').val();
                var snfg = $('#kemas2-snfg').val();
                var snfg_komponen = $('#kemas2-snfg_komponen').val();
                var per_snfg_komponen = document.getElementById("kemas2-snfg_komponen").disabled;
                if(per_snfg_komponen){

                    $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);
                            // fetch data lanjutan split batch

                            var lanjutan_split_batch = $('#kemas2-lanjutan_split_batch_start').val();

                            // inject to get palet

                            $.get('index.php?r=kemas2/palet-ke-snfg',{ snfg : snfg, jenis_kemas : jenis_kemas, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                var data = $.parseJSON(data);
                                // $('#kemas2-palet_ke').attr('value',data.palet_ke);
                            });
                            //
                    });

                    $.get('index.php?r=kemas2/lanjutan-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                            $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);

                    });

                }else{


                    alert('Per Komponen')
                    $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                    });

                    $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);

                            var lanjutan_split_batch = $('#kemas2-lanjutan_split_batch_start').val();

                            $.get('index.php?r=kemas2/palet-ke-komponen',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                    var data = $.parseJSON(data);
                                    // $('#kemas2-palet_ke').attr('value',data.palet_ke);

                            });


                    });
                }

            });
        });
});


JS;
$this->registerJs($script);
?>
