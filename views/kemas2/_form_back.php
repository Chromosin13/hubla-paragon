<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use faryshta\widgets\JqueryTagsInput;
use yii\grid\GridView;
use kartik\checkbox\CheckboxX;
use app\models\Kemas1;
use app\models\PosisiProses;
use app\models\PosisiProsesReworkSb;
use app\models\PosisiProsesReworkPalet;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\kemas2 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kemas2-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box">

            <div class="box-header with-border">
            
                    <label>
                      <input type="checkbox" id="per_snfg_komponen">
                      Insert Per SNFG Komponen ?
                    </label>
                </div>



            <div class="box-header with-border">
              <h2 class="box-title">Input dan Informasi SNFG dari Planner</h2>
                <?= $form->field($model, 'snfg')->textInput() ?>

                <?= $form->field($model, 'snfg_komponen')->textInput(['disabled'=>true]) ?>

                
                <b>Streamline</b>
                <input type="text" class="form-control" id="streamline" disabled>
                <b>Start</b>
                <input type="text" class="form-control" id="start" placeholder="" disabled>
                <b>Due</b>
                <input type="text" class="form-control" id="due" placeholder="" disabled>
                <b>Nama Bulk</b>
                <input type="text" class="form-control" id="nama-bulk" placeholder="" disabled>
                <b>Nama FG</b>
                <input type="text" class="form-control" id="nama-fg" placeholder="" disabled>
                <b>Besar Batch</b>
                <input type="text" class="form-control" id="besar-batch" placeholder="" disabled>
                <b>Besar per lot (Kg)</b>
                <input type="text" class="form-control" id="besar-lot" placeholder="" disabled>
                <b>Lot ke-</b>
                <input type="text" class="form-control" id="lot" placeholder="" disabled>
                <b>Kode Jadwal</b>
                <input type="text" class="form-control" id="kode-jadwal" placeholder="" disabled>
                <b>No Batch</b>
                <input type="text" class="form-control" id="no-batch" placeholder="" disabled>
                <b>Besar Batch Real</b>
                <input type="text" class="form-control" id="besar-batch-real" placeholder="" disabled>
                <b>Jumlah Pc(s)</b>
                <input type="text" class="form-control" id="jumlah" placeholder="" disabled>
            </div>
            <div class="box-header with-border">
                <h2 class="box-title">Posisi Terakhir</h2><br>
                    <b>SNFG Komponen</b>
                    <input type="text" class="form-control" id="ppr-snfg_komponen" placeholder="" disabled>
                    <b>Posisi</b>
                    <input type="text" class="form-control" id="ppr-posisi" placeholder="" disabled>
                    <b>State</b>
                    <input type="text" class="form-control" id="ppr-state" placeholder="" disabled>
                    <b>Jenis Proses</b>
                    <input type="text" class="form-control" id="ppr-jenis-proses" placeholder="" disabled>
                    <b>Lanjutan</b>
                    <input type="text" class="form-control" id="ppr-lanjutan" placeholder="" disabled>
                    <b>Is Done</b>
                    <input type="text" class="form-control" id="ppr-isdone" placeholder="" disabled>
                </div>
    </div>


    <!-- Button untuk change event START, STOP, ISTIRAHAT START, ISTIRAHAT STOP -->

    <a class="btn btn-app" id="start-button">
                <i class="fa fa-play"></i> Start
    </a>

    <a class="btn btn-app" id="stop-button">
                <i class="fa fa-pause"></i> Stop
    </a>

    <a class="btn btn-app" id="istirahat-start-button">
                <i class="fa fa-play"></i> Istirahat Start
    </a>

    <a class="btn btn-app" id="istirahat-stop-button">
                <i class="fa fa-pause"></i> Istirahat Stop
    </a>

    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>

    <div id="lsb_validator">
    <?= $form->field($model, 'jenis_kemas')->dropDownList(['PACKING' => 'PACKING' ,'PRESS+FILLING+PACKING' => 'PRESS+FILLING+PACKING', 'FILLING-PACKING_INLINE' => 'FILLING-PACKING_INLINE','FLAME-PACKING_INLINE' => 'FLAME-PACKING_INLINE', 'REWORK' => 'REWORK' ],['prompt'=>'Select Option']); ?>

    </div>

    <div id='lanjutan_split_batch_form'>
    <?= $form->field($model, 'lanjutan_split_batch')->dropDownList(
                        ArrayHelper::map(PosisiProsesReworkSb::find()->all()
                        ,'lanjutan_split_batch','lanjutan_split_batch')
                        ,['prompt'=>'Select Batch Split']

    );?> 
    </div>

    Palet Dropdown
    <div id='dropdown-palet_ke'>
                <?= $form->field($model, 'palet_ke')->dropDownList(
                        ArrayHelper::map(PosisiProsesReworkPalet::find()->all()
                        ,'palet_ke','palet_ke')
                        ,['id'=>'kemas2-dropdown-palet_ke','prompt'=>'Select Palet Ke']
                        

                );?> 
    </div>

    Palet Autoincrement
    <div id='palet_ke_form'>
    <?= $form->field($model, 'palet_ke')->textInput(['readonly' => 'true']) ?>
    </div>

    <div class="box" id="kemas2-start">
            <div class="box-header with-border">
              <h2 class="box-title">Start Entry</h2>

                <?= $form->field($model, 'nama_line')->textInput() ?>

                <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>

                <div id="kemas2-lanjutan_split_batch_start-form">
                <?= $form->field($model, 'lanjutan_split_batch')->textInput(['id'=>'kemas2-lanjutan_split_batch_start','readonly' => 'true']) ?>
                </div>

                <?= $form->field($model, 'jumlah_operator')->textInput() ?>

                <?php echo $form->field($model, 'nama_operator')->widget(JqueryTagsInput::className([]));
                ?>
                
            </div>
    </div>

    <div class="box" id="kemas2-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Stop Entry</h2>
                <?= $form->field($model, 'lanjutan')->textInput(['id'=>'kemas2-lanjutan_stop','readonly' => 'true']) ?>

                <div id="kemas2-lanjutan_split_batch_stop-form">
                <?= $form->field($model, 'lanjutan_split_batch')->textInput(['id'=>'kemas2-lanjutan_split_batch_stop','readonly' => 'true']) ?>
                </div>

                <?= $form->field($model, 'nobatch')->textInput() ?>

                <?= $form->field($model, 'jumlah_realisasi')->textInput() ?>

                <div id="is_done-form">
                <?php
                    echo '<label class="cbx-label" for="is_done">Is Done?  </label>';
                    echo CheckboxX::widget([
                        'model' => $model,
                        'attribute' => 'is_done',
                        'pluginOptions' => [
                            'threeState' => false,
                            'size' => 'sm'
                        ]
                    ]); 
                ?>
                </div>

                <div id="batch_split-form">
                <?php
                    echo '<label class="cbx-label" for="batch_split">Split Batch?</label>';
                    echo CheckboxX::widget([
                        'model' => $model,
                        'attribute' => 'batch_split',
                        'pluginOptions' => [
                            'threeState' => false,
                            'size' => 'sm'
                        ]
                    ]); 
                ?>
                </div>

                <div id="palet_ke-form">
                <?php
                    echo '<label class="cbx-label" for="palet_flag">Tambah Palet?</label>';
                    echo CheckboxX::widget([
                        'model' => $model,
                        'attribute' => 'palet_flag',
                        'pluginOptions' => [
                            'threeState' => false,
                            'size' => 'sm'
                        ]
                    ]); 
                ?>
                </div>

            </div>
    </div>

    <div class="box" id="kemas2-istirahat-start">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Start Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'kemas2-lanjutan_istirahat_start','readonly' => 'true']) ?> 

                 <div id="kemas2-lanjutan_split_batch_istirahat_start-form">
                 <?= $form->field($model, 'lanjutan_split_batch')->textInput(['id'=>'kemas2-lanjutan_split_batch_istirahat_start','readonly' => 'true']) ?>
                 </div>

                 <?= $form->field($model, 'lanjutan_ist')->textInput(['id'=>'kemas2-lanjutan-ist_istirahat_start','readonly' => 'true']) ?> 
            </div>
    </div>

    <div class="box" id="kemas2-istirahat-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Stop Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'kemas2-lanjutan_istirahat_stop','readonly' => 'true']) ?>
                 
                 <div id="kemas2-lanjutan_split_batch_istirahat_stop-form">
                 <?= $form->field($model, 'lanjutan_split_batch')->textInput(['id'=>'kemas2-lanjutan_split_batch_istirahat_stop','readonly' => 'true']) ?>
                 </div>

                 <?= $form->field($model, 'lanjutan_ist')->textInput(['id'=>'kemas2-lanjutan-ist_istirahat_stop','readonly' => 'true']) ?>
            </div>
    </div>    

    <div class="form-group" id="create-button">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

$('#kemas2-is_done').change(function(){
    var data = $(this).val();;
    if(data==1){
        $('#batch_split-form').hide();
    }
    else if(data==0){
        $('#batch_split-form').show();   
    }
});

$('#kemas2-batch_split').change(function(){
    var data = $(this).val();;
    if(data==1){
        $('#is_done-form').hide();
    }
    else if(data==0){
        $('#is_done-form').show();   
    }
});


$(function() {
    $('#dropdown-palet_ke').hide();
    document.getElementById("kemas2-dropdown-palet_ke").disabled = true;
    $('#lanjutan_split_batch_form').hide();
    document.getElementById("kemas2-lanjutan_split_batch").disabled = true;
});


$('#per_snfg_komponen').change(function(){
    var per_snfg_komponen = document.getElementById("kemas2-snfg_komponen").disabled;
    if(per_snfg_komponen){
        document.getElementById("kemas2-snfg_komponen").disabled = false
        document.getElementById("kemas2-snfg").disabled = true;
    }else{
        document.getElementById("kemas2-snfg_komponen").disabled = true
        document.getElementById("kemas2-snfg").disabled = false;
    } 
});

$('#kemas2-snfg').change(function(){
    var snfg = $(this).val();
    
    $.post("index.php?r=kemas-2/batch-split-assign-snfg&snfg="+$(this).val(), function (data){
            $("select#kemas2-lanjutan_split_batch").html(data);
    });

    $.post("index.php?r=qc-timbang-fg/palet-assign-snfg&snfg="+$(this).val(), function (data){
            $("select#kemas2-dropdown-palet_ke").html(data);
    });

    $.get('index.php?r=kemas2/check-rework-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        if(data == null){
                $('#kemas2-jenis_kemas').change(function(){
                    var jenis_kemas = $(this).val();
                    if(jenis_kemas=='REWORK'){ 
                        alert('case : rework forbidden to start karena ga ada rework');                       
                        $('#create-button').hide(); 
                    }else{
                        alert('case : karena nggak milih rework dan ga ada rework ya lanjut, ga ngapa2in, otomatis counting sb nya');  
                        $('#create-button').show(); 
                    }
                });
        } else {
            
                $('#kemas2-jenis_kemas').change(function(){
                    var jenis_kemas = $(this).val();
                    if(jenis_kemas=='REWORK'){

                        alert('case : rework dan harus pilih nomor reworknya');  
                        
                        /// SHOW//

                            // Palet Show DropDown List
                            $('#dropdown-palet_ke').show();
                            document.getElementById("kemas2-dropdown-palet_ke").disabled = false;


                            // Show Kemas 2 Split Batch Dropdown List
                            $('#lanjutan_split_batch_form').show();
                            document.getElementById("kemas2-lanjutan_split_batch").disabled = false;  

                        /// HIDE //////

                        // HIDE Lanjutan Split Batch Automatis di Form Start , Stop, Ist Start, Ist Stop


                        $('#palet_ke_form').hide();
                        document.getElementById("kemas2-palet_ke").disabled = true;

                        $('#kemas2-lanjutan_split_batch_start-form').hide();
                        document.getElementById("kemas2-lanjutan_split_batch_start").disabled = true;
                        
                        $('#kemas2-lanjutan_split_batch_stop-form').hide();
                        document.getElementById("kemas2-lanjutan_split_batch_stop").disabled = true;
                        
                        $('#kemas2-lanjutan_split_batch_istirahat_start-form').hide();
                        document.getElementById("kemas2-lanjutan_split_batch_istirahat_start").disabled = true;

                        $('#kemas2-lanjutan_split_batch_istirahat_stop-form').hide();
                        document.getElementById("kemas2-lanjutan_split_batch_istirahat_stop").disabled = true;

                    }else{
                        alert('case : nggak milih jenis kemas rework jadi lanjut otomatis counting sb nya');
                       

                    /// HIDE

                            // Palet Show DropDown List
                            $('#dropdown-palet_ke').hide();
                            document.getElementById("kemas2-dropdown-palet_ke").disabled = true;


                            // Show Kemas 2 Split Batch Dropdown List
                            $('#lanjutan_split_batch_form').hide();
                            document.getElementById("kemas2-lanjutan_split_batch").disabled = true;


                    // SHOW

                        $('#palet_ke_form').show();
                        document.getElementById("kemas2-palet_ke").disabled = false;

                        $('#kemas2-lanjutan_split_batch_start-form').show();
                        document.getElementById("kemas2-lanjutan_split_batch_start").disabled = false;
                        
                        $('#kemas2-lanjutan_split_batch_stop-form').show();
                        document.getElementById("kemas2-lanjutan_split_batch_stop").disabled = false;
                        
                        $('#kemas2-lanjutan_split_batch_istirahat_start-form').show();
                        document.getElementById("kemas2-lanjutan_split_batch_istirahat_start").disabled = false;

                        $('#kemas2-lanjutan_split_batch_istirahat_stop-form').show();
                        document.getElementById("kemas2-lanjutan_split_batch_istirahat_stop").disabled = false;
                    }

                });
        }

    });

    $.get('index.php?r=scm-planner/get-planner-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        $('#kemas2-snfg_komponen').attr('value',data.snfg_komponen);
        $('#nomo').attr('value',data.nomo);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
        $('#besar-batch').attr('value',data.besar_batch);
        $('#besar-lot').attr('value',data.besar_lot);
        $('#lot').attr('value',data.lot);
        $('#kode-jadwal').attr('value',data.kode_jadwal);
        $('#jumlah').attr('value',data.jumlah);
    });
    $.get('index.php?r=pengolahan/get-pengolahan',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#no-batch').attr('value',data.nobatch);
        $('#besar-batch-real').attr('value',data.besar_batch_real);
    });
    $.get('index.php?r=scm-planner/get-pprr',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data);
        //alert(data);
        $('#ppr-snfg_komponen').attr('value',data.snfg_komponen);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });
    $.get('index.php?r=scm-planner/get-planner-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Status ditetapkan sebagai HOLD/PAUSE dari Planner')
        }
        else{
                $.get('index.php?r=scm-planner/get-last-induk',{ snfg : snfg },function(data){
                    var data = $.parseJSON(data);
                    if(data.posisi=="KEMAS 2" && data.state=="START"){
                        $('#start-button').hide();
                        $('#stop-button').show(); 
                        $('#istirahat-start-button').show(); 
                        $('#istirahat-stop-button').hide(); 
                    }else if(data.posisi=="KEMAS 2" && data.state=="ISTIRAHAT START"){
                        $('#start-button').hide();
                        $('#stop-button').hide(); 
                        $('#istirahat-start-button').hide(); 
                        $('#istirahat-stop-button').show(); 
                    }else if(data.posisi=="KEMAS 2" && data.state=="ISTIRAHAT STOP"){
                        $('#start-button').hide();
                        $('#stop-button').show(); 
                        $('#istirahat-start-button').show(); 
                        $('#istirahat-stop-button').hide(); 
                    }else if(data.posisi=="KEMAS 2" && data.state=="STOP"){
                        $('#start-button').show();
                        $('#stop-button').hide(); 
                        $('#istirahat-start-button').hide(); 
                        $('#istirahat-stop-button').hide(); 
                    }else {
                        $('#start-button').show();
                        $('#stop-button').show(); 
                        $('#istirahat-start-button').show(); 
                        $('#istirahat-stop-button').show();
                        alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                    }
                });
        }
    });
});



// everything that relates to the change of snfg komponen value
$('#kemas2-snfg_komponen').change(function(){
    var snfg_komponen = $(this).val();
    
    // assign dropdown data for lanjutan_split_batch
   
    $.post("index.php?r=kemas-2/batch-split-assign-komponen&snfg_komponen="+$(this).val(), function (data){
            $("select#kemas2-lanjutan_split_batch").html(data);
    });

    $.post("index.php?r=qc-timbang-fg/palet-assign-komponen&snfg_komponen="+$(this).val(), function (data){
            $("select#kemas2-dropdown-palet_ke").html(data);
    });
   
    $.get('index.php?r=kemas2/check-rework-komponen',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        if(data == null){
            $('#kemas2-jenis_kemas').change(function(){
                    var jenis_kemas = $(this).val();
                    if(jenis_kemas=='REWORK'){ 
                        alert('case : rework forbidden to start karena ga ada rework');                       
                        $('#create-button').hide(); 
                    }else{
                        alert('case : karena nggak milih rework dan ga ada rework ya lanjut, ga ngapa2in, otomatis counting sb nya');  
                        $('#create-button').show(); 
                    }
                });
        } else {
            
            $('#kemas2-jenis_kemas').change(function(){
                    var jenis_kemas = $(this).val();
                    if(jenis_kemas=='REWORK'){
                        /// SHOW//

                            // Palet Show DropDown List
                            $('#dropdown-palet_ke').show();
                            document.getElementById("kemas2-dropdown-palet_ke").disabled = false;


                            // Show Kemas 2 Split Batch Dropdown List
                            $('#lanjutan_split_batch_form').show();
                            document.getElementById("kemas2-lanjutan_split_batch").disabled = false; 



                        // HIDE Lanjutan Split Batch Automatis di Form Start , Stop, Ist Start, Ist Stop

                        $('#palet_ke_form').hide();
                        document.getElementById("kemas2-palet_ke").disabled = true;


                        $('#kemas2-lanjutan_split_batch_start-form').hide();
                        document.getElementById("kemas2-lanjutan_split_batch_start").disabled = true;
                        
                        $('#kemas2-lanjutan_split_batch_stop-form').hide();
                        document.getElementById("kemas2-lanjutan_split_batch_stop").disabled = true;
                        
                        $('#kemas2-lanjutan_split_batch_istirahat_start-form').hide();
                        document.getElementById("kemas2-lanjutan_split_batch_istirahat_start").disabled = true;

                        $('#kemas2-lanjutan_split_batch_istirahat_stop-form').hide();
                        document.getElementById("kemas2-lanjutan_split_batch_istirahat_stop").disabled = true;

                    }else{
                        alert('case : nggak milih jenis kemas rework jadi lanjut otomatis counting sb nya'); 
                        /// HIDE

                            // Palet Show DropDown List
                            $('#dropdown-palet_ke').hide();
                            document.getElementById("kemas2-dropdown-palet_ke").disabled = true;


                            // Show Kemas 2 Split Batch Dropdown List
                            $('#lanjutan_split_batch_form').hide();
                            document.getElementById("kemas2-lanjutan_split_batch").disabled = true;


                        // SHOW

                        $('#palet_ke_form').show();
                        document.getElementById("kemas2-palet_ke").disabled = false;

                        $('#kemas2-lanjutan_split_batch_start-form').show();
                        document.getElementById("kemas2-lanjutan_split_batch_start").disabled = false;
                        
                        $('#kemas2-lanjutan_split_batch_stop-form').show();
                        document.getElementById("kemas2-lanjutan_split_batch_stop").disabled = false;
                        
                        $('#kemas2-lanjutan_split_batch_istirahat_start-form').show();
                        document.getElementById("kemas2-lanjutan_split_batch_istirahat_start").disabled = false;

                        $('#kemas2-lanjutan_split_batch_istirahat_stop-form').show();
                        document.getElementById("kemas2-lanjutan_split_batch_istirahat_stop").disabled = false;
                    }

                });

        }

    });

    $.get('index.php?r=kemas-2/check-batch-split',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        if (data == null){
           alert('Komponen Tidak terdapat di Proses Kemas 1'); 
        } else if(data.last_status==1){
            // $.get('index.php?r=kemas-2/get-batch-split-list',{ snfg_komponen : snfg_komponen },function(data){
            //     var data = $.parseJSON(data);

            // });

            alert('Terjadi Batch Split pada Kemas 1, Mohon Pilih Nomor Lanjutan Batch Split')
            $('#kemas2-lanjutan_split_batch').show();


        } else if(data.last_status==0){
            alert('Proses pada Kemas 1 belum selesai')
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
        } else {
        }
    });
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        $('#kemas2-snfg').attr('value',data.snfg);
        $('#nomo').attr('value',data.nomo);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
        $('#besar-batch').attr('value',data.besar_batch);
        $('#besar-lot').attr('value',data.besar_lot);
        $('#lot').attr('value',data.lot);
        $('#kode-jadwal').attr('value',data.kode_jadwal);
        $('#jumlah').attr('value',data.jumlah);
    });
    $.get('index.php?r=scm-planner/get-ppr',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        $('#ppr-snfg_komponen').attr('value',data.snfg_komponen);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Terdapat SNFG Komponen yang masih HOLD/PAUSE dari Planner')
        }
        else{
                $.get('index.php?r=scm-planner/get-last',{ snfg_komponen : snfg_komponen },function(data){
                    var data = $.parseJSON(data);
                    if(data.posisi=="KEMAS 2" && data.state=="START"){
                        $('#start-button').hide();
                        $('#stop-button').show(); 
                        $('#istirahat-start-button').show(); 
                        $('#istirahat-stop-button').hide(); 
                    }else if(data.posisi=="KEMAS 2" && data.state=="ISTIRAHAT START"){
                        $('#start-button').hide();
                        $('#stop-button').hide(); 
                        $('#istirahat-start-button').hide(); 
                        $('#istirahat-stop-button').show(); 
                    }else if(data.posisi=="KEMAS 2" && data.state=="ISTIRAHAT STOP"){
                        $('#start-button').hide();
                        $('#stop-button').show(); 
                        $('#istirahat-start-button').show(); 
                        $('#istirahat-stop-button').hide(); 
                    }else if(data.posisi=="KEMAS 2" && data.state=="STOP"){
                        $('#start-button').show();
                        $('#stop-button').hide(); 
                        $('#istirahat-start-button').hide(); 
                        $('#istirahat-stop-button').hide(); 
                    }else {
                        $('#start-button').show();
                        $('#stop-button').show(); 
                        $('#istirahat-start-button').show(); 
                        $('#istirahat-stop-button').show();
                        alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                    }
                });
        }
    });
});



$(function() {

    // Original
    $('#kemas2-start').hide();
    $('#kemas2-stop').hide(); 
    $('#kemas2-istirahat-start').hide(); 
    $('#kemas2-istirahat-stop').hide(); 
    $('#lsb_validator').hide();
    $('#start-button').click(function(){
            var start = "START";
            $('#kemas2-state').attr('value',start);
                    $('#kemas2-start').show();
                    $('#kemas2-stop').hide(); 
                    $('#kemas2-istirahat-start').hide(); 
                    $('#kemas2-istirahat-stop').hide();
                    $('#lsb_validator').show();
            $('#kemas2-jenis_kemas').change(function(){
                var jenis_kemas = $('#kemas2-jenis_kemas').val();
                var snfg = $('#kemas2-snfg').val();
                var snfg_komponen = $('#kemas2-snfg_komponen').val();
                var per_snfg_komponen = document.getElementById("kemas2-snfg_komponen").disabled;
                if(per_snfg_komponen){

                    $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);
                            // fetch data lanjutan split batch

                            var lanjutan_split_batch = $('#kemas2-lanjutan_split_batch_start').val();
                            
                            // inject to get palet 
                            
                            $.get('index.php?r=kemas2/palet-ke-snfg',{ snfg : snfg, jenis_kemas : jenis_kemas, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                var data = $.parseJSON(data);
                                $('#kemas2-palet_ke').attr('value',data.palet_ke);
                            });
                            //                            
                        });


                    // assign lanjutan

                    $.get('index.php?r=kemas2/lanjutan-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                        var data = $.parseJSON(data);
                        //alert(data.sediaan);
                        $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                        $('#kemas2-lanjutan').attr('value',data.lanjutan);
                        $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                        $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                    });

                }else {
                
                        $.get('index.php?r=kemas-2/check-batch-split',{ snfg_komponen : snfg_komponen },function(data){
                            var data = $.parseJSON(data);
                            if(data == null || data.last_status!=1) {
                                alert('batch split mandiri kemas2')
                                $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                                        var data = $.parseJSON(data);
                                                        //alert(data.sediaan);
                                                        $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                                        $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                                        $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                                                        $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                                    });

                                $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                        var data = $.parseJSON(data);
                                        $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);
                                        
                                        var lanjutan_split_batch = $('#kemas2-lanjutan_split_batch_start').val();
                        
                                        $.get('index.php?r=kemas2/palet-ke-komponen',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                                var data = $.parseJSON(data);
                                                $('#kemas2-palet_ke').attr('value',data.palet_ke);
                                            
                                        });


                                });

                            }  else if(data.last_status==1){
                                alert('batch split dari kemas 1')
                                $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                    var data = $.parseJSON(data);

                                    document.getElementById("kemas2-lanjutan_split_batch_start").disabled = true;
                                    document.getElementById("kemas2-lanjutan_split_batch_stop").disabled = true;
                                    document.getElementById("kemas2-lanjutan_split_batch_istirahat_start").disabled = true;
                                    document.getElementById("kemas2-lanjutan_split_batch_istirahat_stop").disabled = true;                                 
                                    $('#kemas2-lanjutan_split_batch_start-form').hide();
                                    $('#kemas2-lanjutan_split_batch_stop-form').hide();
                                    $('#kemas2-lanjutan_split_batch_istirahat_start-form').hide();
                                    $('#kemas2-lanjutan_split_batch_istirahat_stop-form').hide();
                                    

                                    $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                    });

                            } 
                        });    
                } 
            });                     
        });

    $('#istirahat-start-button').click(function(){
            var ist_start = "ISTIRAHAT START";
            $('#kemas2-state').attr('value',ist_start);
                    $('#kemas2-istirahat-start').show(); 
                    $('#kemas2-stop').hide(); 
                    $('#kemas2-start').hide(); 
                    $('#kemas2-istirahat-stop').hide();
                    $('#lsb_validator').show();
            $('#kemas2-jenis_kemas').change(function(){
                var jenis_kemas = $('#kemas2-jenis_kemas').val();
                var snfg = $('#kemas2-snfg').val();
                var snfg_komponen = $('#kemas2-snfg_komponen').val();
                var per_snfg_komponen = document.getElementById("kemas2-snfg_komponen").disabled;
                if(per_snfg_komponen){ 

                    alert('NAMBAH LANJUTAN PER SNFG')   

                    $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);
                        });   



                    $.get('index.php?r=kemas2/palet-ke-snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                        var data = $.parseJSON(data);
                        $('#kemas2-palet_ke').attr('value',data.palet_ke);
                    });

                    $.get('index.php?r=kemas2/lanjutan-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                            $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                            var lanjutan = $('#kemas2-lanjutan_istirahat_start').val();
                                            $.get('index.php?r=kemas2/lanjutan-ist-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas, lanjutan : lanjutan },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.lanjutan_ist);
                                                    $('#kemas2-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                    $('#kemas2-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                            });
                        });
                                     
                }else{
                    alert('NAMBAH LANJUTAN PER SNFG KOMPONEN') 

                    $.get('index.php?r=kemas2/palet-ke-komponen',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas2-palet_ke').attr('value',data.palet_ke);
                        });

                    $.get('index.php?r=kemas-2/check-batch-split',{ snfg_komponen : snfg_komponen },function(data){
                            var data = $.parseJSON(data);
                            if(data == null || data.last_status!=1) {
                                alert('batch split mandiri kemas2')
                                $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.sediaan);
                                        $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                        $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                        $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                                        $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                        var lanjutan = $('#kemas2-lanjutan_istirahat_start').val();
                                            $.get('index.php?r=kemas2/lanjutan-ist-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas, lanjutan : lanjutan },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.lanjutan_ist);
                                                    $('#kemas2-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                    $('#kemas2-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                            });

                                    });
                                $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                        var data = $.parseJSON(data);
                                        $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);
                                    });

                            }

                            else if(data.last_status==1){
                                alert('batch split dari kemas 1')
                                $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                    var data = $.parseJSON(data);

                                    document.getElementById("kemas2-lanjutan_split_batch_start").disabled = true;
                                    document.getElementById("kemas2-lanjutan_split_batch_stop").disabled = true;
                                    document.getElementById("kemas2-lanjutan_split_batch_istirahat_start").disabled = true;
                                    document.getElementById("kemas2-lanjutan_split_batch_istirahat_stop").disabled = true;                                 
                                    $('#kemas2-lanjutan_split_batch_start-form').hide();
                                    $('#kemas2-lanjutan_split_batch_stop-form').hide();
                                    $('#kemas2-lanjutan_split_batch_istirahat_start-form').hide();
                                    $('#kemas2-lanjutan_split_batch_istirahat_stop-form').hide();
                                    

                                    $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                    var lanjutan = $('#kemas2-lanjutan_istirahat_start').val();
                                            $.get('index.php?r=kemas2/lanjutan-ist-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas, lanjutan : lanjutan },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.lanjutan_ist);
                                                    $('#kemas2-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                    $('#kemas2-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                            });
                                    });

                            } 
                        });    
                } 
            });                     

        });

    $('#istirahat-stop-button').click(function(){
            var ist_stop = "ISTIRAHAT STOP";
            $('#kemas2-state').attr('value',ist_stop);
                    $('#kemas2-istirahat-stop').show();
                    $('#kemas2-stop').hide(); 
                    $('#kemas2-istirahat-start').hide(); 
                    $('#kemas2-start').hide();
                    $('#lsb_validator').show(); 
            $('#kemas2-jenis_kemas').change(function(){
                var jenis_kemas = $('#kemas2-jenis_kemas').val();
                var snfg = $('#kemas2-snfg').val();
                var snfg_komponen = $('#kemas2-snfg_komponen').val();
                var per_snfg_komponen = document.getElementById("kemas2-snfg_komponen").disabled;
                if(per_snfg_komponen){    

                    $.get('index.php?r=kemas2/palet-ke-snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                        var data = $.parseJSON(data);
                        $('#kemas2-palet_ke').attr('value',data.palet_ke);
                    });

                    $.get('index.php?r=kemas2/lanjutan-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                            $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                            var lanjutan = $('#kemas2-lanjutan_istirahat_start').val();
                                            $.get('index.php?r=kemas2/lanjutan-ist-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas, lanjutan : lanjutan },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.lanjutan_ist);
                                                    $('#kemas2-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                    $('#kemas2-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                            });
                                        });
                    $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);
                        });                                        
                }else{

                    $.get('index.php?r=kemas2/palet-ke-komponen',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas2-palet_ke').attr('value',data.palet_ke);
                        });

                    $.get('index.php?r=kemas-2/check-batch-split',{ snfg_komponen : snfg_komponen },function(data){
                            var data = $.parseJSON(data);
                            if(data == null || data.last_status !=1) {
                                alert('batch split mandiri kemas2')
                                $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.sediaan);
                                        $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                        $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                        $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                                        $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                        var lanjutan = $('#kemas2-lanjutan_istirahat_start').val();
                                            $.get('index.php?r=kemas2/lanjutan-ist-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas, lanjutan : lanjutan },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.lanjutan_ist);
                                                    $('#kemas2-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                    $('#kemas2-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                            });

                                    });
                                $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                        var data = $.parseJSON(data);
                                        $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);
                                    });

                            }
                            else if(data.last_status==1){
                                alert('batch split dari kemas 1')
                                $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                    var data = $.parseJSON(data);

                                    document.getElementById("kemas2-lanjutan_split_batch_start").disabled = true;
                                    document.getElementById("kemas2-lanjutan_split_batch_stop").disabled = true;
                                    document.getElementById("kemas2-lanjutan_split_batch_istirahat_start").disabled = true;
                                    document.getElementById("kemas2-lanjutan_split_batch_istirahat_stop").disabled = true;                                 
                                    $('#kemas2-lanjutan_split_batch_start-form').hide();
                                    $('#kemas2-lanjutan_split_batch_stop-form').hide();
                                    $('#kemas2-lanjutan_split_batch_istirahat_start-form').hide();
                                    $('#kemas2-lanjutan_split_batch_istirahat_stop-form').hide();
                                    

                                    $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                    var lanjutan = $('#kemas2-lanjutan_istirahat_start').val();
                                            $.get('index.php?r=kemas2/lanjutan-ist-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas, lanjutan : lanjutan },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.lanjutan_ist);
                                                    $('#kemas2-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                    $('#kemas2-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                            });
                                    });

                            } 
                        });    
                } 
            });                         
        });

    $('#stop-button').click(function(){
             var stop = "STOP";
             $('#kemas2-state').attr('value',stop);
                    $('#kemas2-stop').show(); 
                    $('#kemas2-start').hide(); 
                    $('#kemas2-istirahat-start').hide(); 
                    $('#kemas2-istirahat-stop').hide();
                    $('#lsb_validator').show();
            $('#kemas2-jenis_kemas').change(function(){
                var jenis_kemas = $('#kemas2-jenis_kemas').val();
                var snfg = $('#kemas2-snfg').val();
                var snfg_komponen = $('#kemas2-snfg_komponen').val();
                var per_snfg_komponen = document.getElementById("kemas2-snfg_komponen").disabled;
                if(per_snfg_komponen){

                    $.get('index.php?r=kemas2/palet-ke-snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                        var data = $.parseJSON(data);
                        $('#kemas2-palet_ke').attr('value',data.palet_ke);
                    });

                    $.get('index.php?r=kemas2/lanjutan-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                            $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                        });
                    $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2snfg',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                            $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);
                        });                                        
                }else{

                        $.get('index.php?r=kemas2/palet-ke-komponen',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas2-palet_ke').attr('value',data.palet_ke);
                        });

                        $.get('index.php?r=kemas-2/check-batch-split',{ snfg_komponen : snfg_komponen },function(data){
                            var data = $.parseJSON(data);
                            if (data == null || data.last_status!=1) {
                                alert('batch split mandiri kemas2')
                                $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                                        var data = $.parseJSON(data);
                                                        //alert(data.sediaan);
                                                        $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                                        $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                                        $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                                                        $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                                    });
                                $.get('index.php?r=kemas2/lanjutan-split-batch-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                        var data = $.parseJSON(data);
                                        $('#kemas2-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_start').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                                        $('#kemas2-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);
                                                    });

                            } else if(data.last_status==1){
                                alert('batch split dari kemas 1')
                                $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                    var data = $.parseJSON(data);

                                    document.getElementById("kemas2-lanjutan_split_batch_start").disabled = true;
                                    document.getElementById("kemas2-lanjutan_split_batch_stop").disabled = true;
                                    document.getElementById("kemas2-lanjutan_split_batch_istirahat_start").disabled = true;
                                    document.getElementById("kemas2-lanjutan_split_batch_istirahat_stop").disabled = true;                                 
                                    $('#kemas2-lanjutan_split_batch_start-form').hide();
                                    $('#kemas2-lanjutan_split_batch_stop-form').hide();
                                    $('#kemas2-lanjutan_split_batch_istirahat_start-form').hide();
                                    $('#kemas2-lanjutan_split_batch_istirahat_stop-form').hide();
                                    

                                    $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                    });

                            } 
                        });  
                } 
            }); 
        });                
});
    
JS;
$this->registerJs($script);
?>