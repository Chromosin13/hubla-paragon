<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
use faryshta\widgets\JqueryTagsInput;
use yii\grid\GridView;
use kartik\checkbox\CheckboxX;
use app\models\Kemas1;
use app\models\PosisiProses;
use app\models\ScmPlanner;
use app\models\PosisiProsesReworkSb;
use app\models\PosisiProsesReworkPalet;
use yii\helpers\ArrayHelper;
use app\models\Kemas2;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\web\JsExpression;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\DateTimePicker;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\kemas2 */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="kemas2-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <!-- Left Side -->
    <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-blue">
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username"><b>KEMAS 2</b></h2>
              <h5 class="widget-user-desc">EDIT JADWAL</h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                
                <!-- Content Begins Here  -->

                    <!-- Isi SNFG -->
                    <div id="form_snfg" class="box-header with-border">
                            <?= $form->field($model, 'snfg', ['hintType' => ActiveField::HINT_SPECIAL, 
                                                              'addon' => [
                                                                            'append' => [
                                                                                'content'=>'<i class="fa fa-barcode"></i>'
                                                                            ]
                                                             ]
                            ])->textInput(['readOnly'=>true]);
                            ?>

                            <?= $form->field($model, 'state')->textInput(['readOnly' => true ]);
                            ?>

                            <?= $form->field($model, 'edit_nama')->textInput(['readOnly'=>true,'value'=>Yii::$app->user->identity->username]) ?>

                            <?php
                                
                                date_default_timezone_set('Asia/Jakarta');

                                echo $form->field($model, 'edit_time')->textInput(['readonly' => 'true','value'=>date("Y-m-d H:i:s")])
                            ?>

                            <?php 
                                echo '<label class="control-label">Waktu</label>';
                                echo DateTimePicker::widget([
                                    'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                                    'model' => $model,
                                    'attribute' => 'formatwaktu',
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'yyyy-mm-dd hh:ii:ss',
                                        'endDate' => $model->formatwaktu
                                    ]
                                ]);
                            ?>
                    </div>


                    <div class="box-header with-border" id="form_jenis">

                        <div id="lsb_validator">

                            <div id="kemas2-jenis_kemas_00">
                            <?php 
                                // echo $form->field($model, 'jenis_kemas')->dropDownList(['PACKING' => 'PACKING' ,'PRESS+FILLING+PACKING' => 'PRESS+FILLING+PACKING', 'FILLING-PACKING_INLINE' => 'FILLING-PACKING_INLINE','FLAME-PACKING_INLINE' => 'FLAME-PACKING_INLINE', 'REWORK' => 'REWORK' ],['prompt'=>'Select Option']);
                            ?>

                            <?= $form->field($model, 'jenis_kemas')->textInput(['readOnly' => true ],['prompt'=>'Select Option']);
                            ?>
                            </div>


                        </div>

                    </div>


                <!-- Content Ends Here -->

              </ul>
            </div>


    </div>




            <!-- Palet Autoincrement -->
            <!-- <div id='palet_ke_form'>
            <?= $form->field($model, 'palet_ke')->textInput(['readonly' => 'true']) ?>
            </div> -->

    <div id="form_entry_start_stop">


            <div class="box box-widget widget-user-2" id="kemas2-start">

                    <div class="widget-user-header bg-green">
                      <div class="widget-user-image">
                        <img class="img-circle" src="../web/images/arrow-up-icon.png" alt="User Avatar">
                      </div>
                      <!-- /.widget-user-image -->
                      <h2 class="widget-user-username">Start</h2>
                      <h5 class="widget-user-desc">Entry</h5>
                    </div>

                    <div class="box-footer no-padding">
                      <ul class="nav nav-stacked">
                            
                            <!-- Content Begins Here -->
                            <div class="box-header with-border">

                                <?php

                                    echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                                        'data' => ArrayHelper::map(Kemas2::find()->all()
                                        ,'nama_line','nama_line'),
                                        'options' => ['placeholder' => 'Select Line'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);

                                ?>

                                <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>
                                
                                <!-- Nama Operator -->
                                <?php 

                                    echo $form->field($model, 'nama_operator', ['hintType' => ActiveField::HINT_SPECIAL,'addon' => ['prepend' => ['content'=>'<i class="fa fa-barcode"></i>']]
                                        ])->widget(TagsinputWidget::classname(), [
                                        'clientOptions' => [
                                            'trimValue' => true,
                                            'allowDuplicates' => false,
                                            'maxChars'=> 4,
                                        ]
                                    ])->hint(' <b>Scan</b> : Klik pada isian dibawah kemudian Scan Barcode pada Nametag<p \> <b>Manual</b> : isi dengan Format 4 Huruf Kapital, Contoh : "ABCD" lalu diikuti dengan menekan Enter ');

                                ?>

                            </div>
                        

                      </ul>
                    </div>

            </div>

            <div class="box box-widget widget-user-2" id="kemas2-stop">

                    <div class="widget-user-header bg-red">
                      <div class="widget-user-image">
                        <img class="img-circle" src="../web/images/arrow-down-icon.png" alt="User Avatar">
                      </div>
                      <!-- /.widget-user-image -->
                      <h2 class="widget-user-username">Stop</h2>
                      <h5 class="widget-user-desc">Entry</h5>
                    </div>

                    <div class="box-footer no-padding">
                      <ul class="nav nav-stacked">
                            
                            <!-- Content Begins Here -->
                            <div class="box-header with-border">
                                <?= $form->field($model, 'lanjutan')->textInput(['id'=>'kemas2-lanjutan_stop','readonly' => 'true']) ?>

                                <div id="kemas2-lanjutan_split_batch_stop-form">
                                <?php 
                                    // echo $form->field($model, 'lanjutan_split_batch')->textInput(['id'=>'kemas2-lanjutan_split_batch_stop','readonly' => 'true']) ?>
                                </div>

                                <?= $form->field($model, 'nobatch')->textInput() ?>

                                <?= $form->field($model, 'jumlah_realisasi')->textInput() ?>

                                <?= $form->field($model, 'counter')->textInput(['readonly'=>true]) ?>


                                <label>
                                  <input type="checkbox" id="is_kendala">
                                  Terdapat Kendala ?
                                </label>

                                <div id="kendala-form">
                                    <?php DynamicFormWidget::begin([
                                        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                        'widgetBody' => '.container-items', // required: css class selector
                                        'widgetItem' => '.item', // required: css class
                                        'limit' => 3, // the maximum times, an element can be cloned (default 999)
                                        'min' => 1, // 0 or 1 (default 1)
                                        'insertButton' => '.add-item', // css class
                                        'deleteButton' => '.remove-item', // css class
                                        'model' => $modelsKendala[0],
                                        'formId' => 'dynamic-form',
                                        'formFields' => [
                                            'keterangan',
                                            'start',
                                            'stop',
                                        ],
                                    ]); ?>

                                    <div class="container-items"><!-- widgetContainer -->
                                        <?php foreach ($modelsKendala as $i => $modelKendala): ?>
                                        <div class="item panel panel-default"><!-- widgetBody -->
                                            <div class="panel-heading">
                                                <h3 class="panel-title pull-left">Kendala</h3>
                                                <div class="pull-right">
                                                    <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="panel-body">
                                                <?php
                                                    // necessary for update action.
                                                    if (! $modelKendala->isNewRecord) {
                                                        echo Html::activeHiddenInput($modelKendala, "[{$i}]id");
                                                    }
                                                ?>

                                                <div class="row">
                                                    <div class="col-sm-3">


                                                        <?= $form->field($modelKendala, "[{$i}]keterangan")->widget(Select2::classname(), [
                                                                            'data' => ArrayHelper::map(Kendala::find()->all()
                                                                    ,'keterangan','keterangan'),
                                                                            'options' => ['placeholder' => 'Select Kendala'],
                                                                            'pluginOptions' => [
                                                                                // 'tags' => true,
                                                                                'allowClear' => true
                                                                            ],
                                                                            ]);
                                                        ?>

                                                    </div>
                                                    <div class="col-sm-3">
                                                        <?= $form->field($modelKendala, "[{$i}]start")->widget(TimePicker::classname(), [
                                                                        'options' => ['placeholder' => 'Start'],
                                                                        'pluginOptions' => [
                                                                                'minuteStep' => 5,
                                                                                'autoclose'=>true,
                                                                                'showMeridian' => false,
                                                                                'defaultTime' => '00:00'
                                                                        ]
                                                        ]); ?>
                                                    </div>
                                                </div><!-- .row -->
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                        </div>
                                        <?php DynamicFormWidget::end(); ?>
                                </div>

                                <p \>

                                <div id="is_done-form">
                                <?php
                                    echo '<label class="cbx-label" for="is_done">Is Done?  </label>';
                                    echo CheckboxX::widget([
                                        'model' => $model,
                                        'attribute' => 'is_done',
                                        'pluginOptions' => [
                                            'threeState' => false,
                                            'size' => 'sm'
                                        ]
                                    ]);
                                ?>
                                </div>


                            </div>
                      </ul>
                    </div>

            </div>

<!--             <div class="box" id="kemas2-stop">

            </div> -->

            <div class="form-group" id="create-button">
                


                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?> 
            </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS


$(function() {
    var state = document.getElementById("kemas2-state").value;
     if(state=="START"){
         $('#kemas2-stop').hide();
         $('#kemas2-start').show();
     }else{
         $('#kemas2-start').hide();
         $('#kemas2-stop').show();
     }
});
    




// Inisialisasi Form Hide

// Hide and Disable DynamicFormWidget for Kendala Model //


// After Klik Tombol Tekan Scan
    // Klik Scan SNFG



// After Select Jenis Kemas
    $('#kemas2-jenis_kemas').change(function(){
        var jenis_kemas = $('#kemas2-jenis_kemas').val();
        if(jenis_kemas==""){
            $('#form_entry_start_stop').hide(); 
        } else {
            $('#form_entry_start_stop').fadeIn("slow");
            var state = $('#kemas2-state').val();
            if(state=="START"){
                $('#create-button').hide();
                $('#kemas2-nama_line').change(function(){
                    var nama_line = $('#kemas2-nama_line').val();

                    $.get('index.php?r=kemas2/check-line',{ nama_line : nama_line },function(data){
                    var data = $.parseJSON(data);
                        
                        if(nama_line=="Select Line"){
                            $('#create-button').hide();
                        }else if(data.state=="START"){
                            alert('Mesin ini belum di STOP oleh nomor jadwal '+data.snfg);
                            // $('#create-button').hide();
                            $('#create-button').fadeIn("slow");
                        }else{
                            $('#create-button').fadeIn("slow");
                        }

                    });
                });
            }
      
        }
    });



$('#is_kendala').change(function(){

    if(document.getElementById('is_kendala').checked){
            $('#kendala-form').fadeIn("slow");

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);

                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = false;
                    document.getElementById("kendala-"+(i)+"-start").disabled = false;
        
                    i++;
                }

    }else{
            $('#kendala-form').hide();

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);
                    
                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = true;
                    document.getElementById("kendala-"+(i)+"-start").disabled = true;  

                    i++;
                }

    } 
 });




$(function() {

    
    // DynamicForm override JS , Script Fix for non-working kartik date/timepicker afterInsert 
    // DynamicForm override JS , Fix for select2 kendala list

    // ID untuk penomoran nested dynamic form
    var id = 0;


    // Setelah Insert Button

    $(".dynamicform_wrapper").on('afterInsert', function(e, item) {
        var dateTimePickers = $(this).find('[data-krajee-kvtimepicker]');
        dateTimePickers.each(function(index, el) {
            $(this).parent().removeData().kvTimepicker('remove');
            $(this).parent().kvTimepicker(eval($(this).attr('data-krajee-kvtimepicker')));
        });
        

        // Override Fix for select2 kendala list 

        id += 1;
        console.log(id);


        $.post("index.php?r=scm-planner/get-kendala-kemas-snfg&snfg="+$('#kemas2-snfg').val(), function (data){

                $("select#kendala-"+(id)+"-keterangan").html(data);

        });


    });

    // Setelah Delete Button

    $(".dynamicform_wrapper").on('afterDelete', function(e, item) {
        
        // Override Fix for select2 kendala list 

        id -= 1;
        console.log(id);

    });


    // Jika Mencapai Batas Maximum
    $(".dynamicform_wrapper").on('limitReached', function(e, item) {
        alert('Maximum 3');
    });




});



JS;
$this->registerJs($script);
?>
