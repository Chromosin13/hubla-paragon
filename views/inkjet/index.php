<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InkjetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inkjets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inkjet-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Inkjet', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'snfg',
            'jenis_inkjet',
            'lanjutan',
            'nama_line',
            'jumlah_operator',
            'jumlah_realisasi',
            'state',
            'nama_operator',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
