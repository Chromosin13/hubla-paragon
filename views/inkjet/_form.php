<?php

use yii\helpers\Html;
use kartik\widgets\ActiveField;
use kartik\form\ActiveForm;
use faryshta\widgets\JqueryTagsInput;
use kartik\checkbox\CheckboxX;
use wbraganca\tagsinput\TagsinputWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Inkjet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inkjet-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="info-box bg-aqua">
        <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

        <div class="info-box-content">
          <b><div class="col-md-3 col-sm-6 col-xs-12" id="nama_bulk_results"></div></b>
          <div id="nama_fg_results"></div>
          <span class="info-box-number"><div id="snfg_results"></div><div id="snfg_komponen_results"></div></span>

          <div class="progress">
            <div class="progress-bar" style="width: 100%"></div>
          </div>
              <span class="progress-description">
              <div class="col-md-3 col-sm-6 col-xs-12" id="streamline_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="start_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="due_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="besar_batch_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="kode_jadwal_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="jumlah_results"></div>
              </span>
        </div>
        <!-- /.info-box-content -->
    </div>

    <div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Input dan Informasi SNFG dari Planner</h2>
                <?= $form->field($model, 'snfg')->textInput() ?>
<!--                 <b>Streamline</b>
                <input type="text" class="form-control" id="streamline" disabled>
                <b>Start</b>
                <input type="text" class="form-control" id="start" placeholder="" disabled>
                <b>Due</b>
                <input type="text" class="form-control" id="due" placeholder="" disabled>
                <b>Nama Bulk</b>
                <input type="text" class="form-control" id="nama-bulk" placeholder="" disabled>
                <b>Nama FG</b>
                <input type="text" class="form-control" id="nama-fg" placeholder="" disabled>
                <b>Besar Batch</b>
                <input type="text" class="form-control" id="besar-batch" placeholder="" disabled>
                <b>Kode Jadwal</b>
                <input type="text" class="form-control" id="kode-jadwal" placeholder="" disabled>
                <b>Jumlah Pc(s)</b>
                <input type="text" class="form-control" id="jumlah" placeholder="" disabled> -->
            </div>
            <div class="box-header with-border">
                <h2 class="box-title">Posisi Terakhir</h2><br>
                    <b>Posisi</b>
                    <input type="text" class="form-control" id="ppr-posisi" placeholder="" disabled>
                    <b>State</b>
                    <input type="text" class="form-control" id="ppr-state" placeholder="" disabled>
                    <b>Jenis Proses</b>
                    <input type="text" class="form-control" id="ppr-jenis-proses" placeholder="" disabled>
                    <b>Lanjutan</b>
                    <input type="text" class="form-control" id="ppr-lanjutan" placeholder="" disabled>
                     <b>Is Done</b>
                    <input type="text" class="form-control" id="ppr-isdone" placeholder="" disabled>
                </div>
    </div>


    <!-- Button untuk change event START, STOP, ISTIRAHAT START, ISTIRAHAT STOP -->

    <a class="btn btn-app" id="start-button">
                <i class="fa fa-play"></i> Start
    </a>

    <a class="btn btn-app" id="stop-button">
                <i class="fa fa-pause"></i> Stop
    </a>

<!--     <a class="btn btn-app" id="istirahat-start-button">
                <i class="fa fa-play"></i> Istirahat Start
    </a>

    <a class="btn btn-app" id="istirahat-stop-button">
                <i class="fa fa-pause"></i> Istirahat Stop
    </a>
 -->
    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'jenis_inkjet')->dropDownList(['INKJET PRODUCT' => 'INKJET PRODUCT','INKJET STICKER PRODUCT' => 'INKJET STICKER PRODUCT','INKJET DUS SATUAN' => 'INKJET DUS SATUAN','INKJET STICKER BOTTOM' => 'INKJET STICKER BOTTOM','INKJET DUS 12' => 'INKJET DUS 12','INKJET DAICHI' => 'INKJET DAICHI'],['prompt'=>'Select Option']); ?>

    <div class="box" id="inkjet-start">
            <div class="box-header with-border">
              <h2 class="box-title">Start Entry</h2>

                <?= $form->field($model, 'lanjutan')->textInput(['id'=>'inkjet-lanjutan','readonly' => 'true']) ?>

                <?= $form->field($model, 'nama_line')->textInput() ?>

                <?= $form->field($model, 'jumlah_operator')->textInput() ?>

                                <!-- Nama Operator -->
                                <?php 

                                    echo $form->field($model, 'nama_operator', ['hintType' => ActiveField::HINT_SPECIAL,'addon' => ['prepend' => ['content'=>'<i class="fa fa-barcode"></i>']]
                                        ])->widget(TagsinputWidget::classname(), [
                                        'clientOptions' => [
                                            'trimValue' => true,
                                            'allowDuplicates' => false,
                                            'maxChars'=> 4,
                                        ]
                                    ])->hint(' <b>Scan</b> : Klik pada isian dibawah kemudian Scan Barcode pada Nametag<p \> <b>Manual</b> : isi dengan Format 4 Huruf Kapital, Contoh : "ABCD" lalu diikuti dengan menekan Enter ');

                                ?>
                
            </div>
    </div>

    <div class="box" id="inkjet-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Stop Entry</h2>
                <?= $form->field($model, 'jumlah_realisasi')->textInput() ?>

                <?= $form->field($model, 'jumlah_reject')->textInput() ?>

                <?= $form->field($model, 'keterangan')->textInput() ?>

                <?= $form->field($model, 'lanjutan')->textInput(['id'=>'inkjet-lanjutan_stop','readonly' => 'true']) ?>

                <?php
                    echo '<label class="cbx-label" for="is_done">Is Done?</label>';
                    echo CheckboxX::widget([
                        'model' => $model,
                        'attribute' => 'is_done',
                        'pluginOptions' => [
                            'threeState' => false,
                            'size' => 'lg'
                        ]
                    ]); 
                ?>
            </div>
    </div>

    <div class="box" id="inkjet-istirahat-start">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Start Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'inkjet-lanjutan_istirahat_start','readonly' => 'true']) ?>
                 <?= $form->field($model, 'lanjutan_ist')->textInput(['id'=>'inkjet-lanjutan-ist_istirahat_start','readonly' => 'true']) ?> 
            </div>
    </div>

    <div class="box" id="inkjet-istirahat-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Stop Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'inkjet-lanjutan_istirahat_stop','readonly' => 'true']) ?>
                 <?= $form->field($model, 'lanjutan_ist')->textInput(['id'=>'inkjet-lanjutan-ist_istirahat_stop','readonly' => 'true']) ?>
            </div>
    </div>    
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
$('#inkjet-snfg').change(function(){
    var snfg = $(this).val();
    $.get('index.php?r=scm-planner/get-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        //$('#inkjet-snfg').attr('value',data.snfg);

        // $('#streamline').attr('value',data.streamline);
        // $('#start').attr('value',data.start);
        // $('#due').attr('value',data.due);
        // $('#nama-bulk').attr('value',data.nama_bulk);
        // $('#nama-fg').attr('value',data.nama_fg);
        // $('#besar-batch').attr('value',data.besar_batch);
        // $('#kode-jadwal').attr('value',data.kode_jadwal);
        // $('#jumlah').attr('value',data.jumlah);

        $('#streamline_results').html('STREAMLINE : '+data.streamline);
        $('#start_results').html('START : '+ data.start);
        $('#due_results').html('DUE : '+ data.due);
        $('#nama_bulk_results').html(data.nama_bulk);
        $('#nama_fg_results').html(data.nama_fg);
        $('#besar_batch_results').html('BESAR BATCH : '+ data.besar_batch);
        $('#besar_lot_results').html('BESAR LOT : '+ data.besar_lot);
        $('#lot_ke_results').html('LOT KE : '+ data.lot); ;
        $('#kode_jadwal_results').html('KODE JADWAL : '+ data.kode_jadwal); 
        $('#jumlah_results').html('JUMLAH : '+ data.jumlah);

    });
    $.get('index.php?r=scm-planner/get-pprr',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });
});

$(function() {
    $('#inkjet-start').hide();
    $('#inkjet-stop').hide(); 
    $('#inkjet-istirahat-start').hide(); 
    $('#inkjet-istirahat-stop').hide(); 
    $('#start-button').click(function(){
            var start = "START";
            $('#inkjet-state').attr('value',start);
                    $('#inkjet-start').show();
                    $('#inkjet-stop').hide(); 
                    $('#inkjet-istirahat-start').hide(); 
                    $('#inkjet-istirahat-stop').hide();
            $('#inkjet-jenis_inkjet').change(function(){
                var jenis_inkjet = $('#inkjet-jenis_inkjet').val();
                var snfg = $('#inkjet-snfg').val();    
                $.get('index.php?r=inkjet/lanjutan-inkjet',{ snfg : snfg, jenis_inkjet : jenis_inkjet },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#inkjet-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan_stop').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                     
        });

    $('#istirahat-start-button').click(function(){
            var ist_start = "ISTIRAHAT START";
            $('#inkjet-state').attr('value',ist_start);
                    $('#inkjet-istirahat-start').show(); 
                    $('#inkjet-stop').hide(); 
                    $('#inkjet-start').hide(); 
                    $('#inkjet-istirahat-stop').hide(); 
            $('#inkjet-jenis_inkjet').change(function(){
                var jenis_inkjet = $('#inkjet-jenis_inkjet').val();
                var snfg = $('#inkjet-snfg').val();    
                $.get('index.php?r=inkjet/lanjutan-inkjet',{ snfg : snfg, jenis_inkjet : jenis_inkjet },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#inkjet-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan_stop').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                            var lanjutan = $('#inkjet-lanjutan_istirahat_start').val();
                                $.get('index.php?r=inkjet/lanjutan-ist-inkjet',{ snfg : snfg, jenis_inkjet : jenis_inkjet, lanjutan : lanjutan },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.lanjutan_ist);
                                        $('#inkjet-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                        $('#inkjet-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                }); 
                        });
            });                     
        });

    $('#istirahat-stop-button').click(function(){
            var ist_stop = "ISTIRAHAT STOP";
            $('#inkjet-state').attr('value',ist_stop);
                    $('#inkjet-istirahat-stop').show();
                    $('#inkjet-stop').hide(); 
                    $('#inkjet-istirahat-start').hide(); 
                    $('#inkjet-start').hide(); 
            $('#inkjet-jenis_inkjet').change(function(){
                var jenis_inkjet = $('#inkjet-jenis_inkjet').val();
                var snfg = $('#inkjet-snfg').val();    
                $.get('index.php?r=inkjet/lanjutan-inkjet',{ snfg : snfg, jenis_inkjet : jenis_inkjet },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#inkjet-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan_stop').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                            var lanjutan = $('#inkjet-lanjutan_istirahat_stop').val();
                                $.get('index.php?r=inkjet/lanjutan-ist-inkjet',{ snfg : snfg, jenis_inkjet : jenis_inkjet, lanjutan : lanjutan},function(data){
                                var data = $.parseJSON(data);
                                $('#inkjet-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                $('#inkjet-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                            }); 
                        });
            });                         
        });

    $('#stop-button').click(function(){
             var stop = "STOP";
             $('#inkjet-state').attr('value',stop);
                    $('#inkjet-stop').show(); 
                    $('#inkjet-start').hide(); 
                    $('#inkjet-istirahat-start').hide(); 
                    $('#inkjet-istirahat-stop').hide();    
            });
            $('#inkjet-jenis_inkjet').change(function(){
                var jenis_inkjet = $('#inkjet-jenis_inkjet').val();
                var snfg = $('#inkjet-snfg').val();    
                $.get('index.php?r=inkjet/lanjutan-inkjet',{ snfg : snfg, jenis_inkjet : jenis_inkjet },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#inkjet-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan_stop').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });               
    });

JS;
$this->registerJs($script);
?>