<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FlowInputSnfgkomponenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Flow Input Snfgkomponens';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flow-input-snfgkomponen-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Flow Input Snfgkomponen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'posisi',
            'lanjutan',
            'datetime_start',
            'datetime_stop',
            'datetime_write',
            // 'nama_operator',
            // 'nama_line',
            // 'jenis_kemas',
            // 'is_done',
            // 'counter',
            // 'snfg_komponen',
            // 'jumlah_realisasi',
            // 'nobatch',
            // 'id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
