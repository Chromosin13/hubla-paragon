<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>





                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
                      <h3 class="widget-user-username"><b>Kemas 1</b></h3>
                      <h5 class="widget-user-desc">Input Data</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/business-partnership.png" alt="User Avatar">
                    </div>


                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                          <!-- <div class="zoom">
                            <?=Html::a('SCAN WITH QR', ['flow-input-snfgkomponen/check-kemas1-tab'], ['class' => 'btn btn-success btn-block']);?>
                          </div>
                          <p></p> -->
                            <div class="box-body">
                                <div class="flow-input-snfg-form">

                                    <div class="form-group field-flowinputsnfgkomponen-snfg_komponen has-success">
                                    <label class="control-label" for="flowinputsnfgkomponen-snfg_komponen">Snfg Komponen</label>
                                    <input type="text" id="flowinputsnfgkomponen-snfg_komponen" class="form-control" name="FlowInputSnfgkomponen[snfg_komponen]" aria-invalid="false">

                                    <div class="help-block"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>

                  <!-- List Data -->

                   <div class="box box-widget widget-user">

                      <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                                    IN PROGRESS
                      </h4>

                      <div class="box-footer">

                        <div class="row">
                          <div class="box-body">
                               
                                  <div class="flow-input-snfg-form">



                                     <?= GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'columns' => [
                                            
                                            'snfg_komponen',
                                            'lanjutan',
                                            'posisi',
                                            'datetime_start',
                                            'datetime_stop',
                                            'nama_line',
                                            'jenis_kemas',
                                            'nama_operator',
                                        ],
                                    ]); ?>


                                  </div>
                          </div>
                        </div>
                        <!-- /.row -->
                      </div>
                  </div>




<?php
$script = <<< JS

// AutoFocus SNFG Komponen Field

document.getElementById("flowinputsnfgkomponen-snfg_komponen").focus();


// Check SNFG Komponen Validity


$('#flowinputsnfgkomponen-snfg_komponen').change(function(){  

    var snfg_komponen = $('#flowinputsnfgkomponen-snfg_komponen').val().trim();
    
    if(snfg_komponen){
      window.location = "index.php?r=flow-input-snfgkomponen/create-kemas1&snfg_komponen="+snfg_komponen;
    }else{
      alert('Nomor Jadwal kosong!');
    }
    
});


// $('#flowinputsnfgkomponen-snfg_komponen').change(function(){  

//     var snfg_komponen = $('#flowinputsnfgkomponen-snfg_komponen').val();


//     $.get('index.php?r=scm-planner/check-snfg-komponen',{ snfg_komponen : snfg_komponen },function(data){
//         var data = $.parseJSON(data);
//         if(data.id>=1){
          
//           window.location = "index.php?r=flow-input-snfgkomponen/create-kemas1&snfg_komponen="+snfg_komponen;
           
//         }

//         else{

//           alert('Nomor SNFG Komponen Tidak Ditemukan di Database; Silahkan mengontak Planner/Leader');
//         }
    
//     });
// });


JS;
$this->registerJs($script);
?>