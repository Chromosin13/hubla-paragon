<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfgkomponen */

$this->title = 'Create Flow Input Snfgkomponen';
$this->params['breadcrumbs'][] = ['label' => 'Flow Input Snfgkomponens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flow-input-snfgkomponen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
