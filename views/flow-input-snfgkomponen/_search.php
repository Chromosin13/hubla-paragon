<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfgkomponenSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="flow-input-snfgkomponen-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'posisi') ?>

    <?= $form->field($model, 'lanjutan') ?>

    <?= $form->field($model, 'datetime_start') ?>

    <?= $form->field($model, 'datetime_stop') ?>

    <?= $form->field($model, 'datetime_write') ?>

    <?php // echo $form->field($model, 'nama_operator') ?>

    <?php // echo $form->field($model, 'nama_line') ?>

    <?php // echo $form->field($model, 'jenis_kemas') ?>

    <?php // echo $form->field($model, 'is_done') ?>

    <?php // echo $form->field($model, 'counter') ?>

    <?php // echo $form->field($model, 'snfg_komponen') ?>

    <?php // echo $form->field($model, 'jumlah_realisasi') ?>

    <?php // echo $form->field($model, 'nobatch') ?>

    <?php // echo $form->field($model, 'id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
