<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputSnfgkomponen;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\Downtime;
use app\models\MasterDataLine;
use app\models\DowntimeSearch;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */
/* @var $form yii\widgets\ActiveForm */
?>



                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
                      <h3 class="widget-user-username"><b>Kemas 1 / Packing</b></h3>
                      <h5 class="widget-user-desc">Work Order</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">




                                <div class="flow-input-snfg-form">

                                    <?php $form = ActiveForm::begin(); ?>


                                    <div class="row">
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'snfg_komponen')->textInput(['readOnly'=>true,'value'=>$snfg_komponen]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'posisi')->textInput(['readOnly'=>true,'value'=>'KEMAS_1']) ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-6">
                                          <?php
                                                echo $form->field($model, 'jenis_kemas')
                                                ->dropDownList(
                                                    $jenis_proses_list,           // Flat array ('id'=>'label')
                                                    ['prompt'=>'Select Options']    // options
                                                )->label('Jenis Kemas');
                                          ?>
                                          <!-- /input-group -->
                                        </div>
                                        <!-- /.col-lg-6 -->
                                        <div class="col-md-3">
                                          <?php

                                                // if(empty($nama_line)){
                                                //     echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                                                //         'data' => ArrayHelper::map(FlowInputSnfgkomponen::find()->all()
                                                //         ,'nama_line','nama_line'),
                                                //         'options' => ['placeholder' => 'Select Line'],
                                                //         'pluginOptions' => [
                                                //             'allowClear' => true
                                                //         ],
                                                //     ]);
                                                // }else{
                                                //     echo $form->field($model, 'nama_line')->textInput(['readOnly'=>true,'value'=>$nama_line]);
                                                // }
                                                if(empty($nama_line)){
                                                    echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(MasterDataLine::find()->where("sediaan not like '%off%' and posisi ='KEMAS'")->orderBy(['sediaan'=>SORT_ASC])->all(),
                                                        'nama_line','nama_line'),
                                                        'options' => ['placeholder' => 'Select Line'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]);
                                                }else{
                                                    echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(MasterDataLine::find()->where("sediaan not like '%off%' and posisi ='KEMAS' ")->orderBy(['sediaan'=>SORT_ASC])->all(),
                                                        'nama_line','nama_line'),
                                                        'options' => ['placeholder' => 'Select Line', 'value' => $nama_line],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]);
                                                }
                                            ?>
                                          <!-- input group -->
                                        </div>
                                        <div id="lanjutan">
                                        <div class="col-md-3">
                                          <?= $form->field($model, 'lanjutan')->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        </div>
                                    </div>


                                    <label class="control-label">Nama Operator</label>
                                    <?= $form->field($model, 'nama_operator')->widget(TagsinputWidget::classname(), [
                                                        'clientOptions' => [
                                                            'trimValue' => true,
                                                            'allowDuplicates' => false,
                                                            'maxChars'=> 4,
                                                            'minChars'=> 4,
                                                        ]
                                                    ])->label(false)?>

                                    <br></br>

                                    <div class="row">

                                        <div class="col-md-2 col-md-offset-5">

                                            <?php

                                                echo Html::submitButton($model->isNewRecord ? 'Start' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
                                            ?>


                                        </div>
                                    </div>

                                    <?php ActiveForm::end(); ?>

                                </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>


<div class="box box-widget widget-user">
    <div class="widget-user-header bg-blue">
                      <h3 class="widget-user-username"><b>Rincian</b></h3>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'value' => function($model,$key,$index,$column){
                    return GridView::ROW_COLLAPSED;
                },
                'detail' => function($model,$key,$index,$column){

                    $searchModel = new DowntimeSearch();

                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                    $dataProvider->query->where("flow_input_snfgkomponen_id=".$model->id);

                    return Yii::$app->controller->renderPartial('_expand-downtime-kemas1', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'flow_input_snfgkomponen_id' => $model->id,
                    ]);
                },
            ],
            'lanjutan',
            'snfg_komponen',
            'posisi',
            'datetime_start',
            'datetime_stop',
            // 'datetime_write',

            'nama_line',
            'jenis_kemas',
            'nama_operator',
            // 'durasiMenit',
            // 'durasiMenitbersih',
            // 'downtimeFilled',
        ],
    ]); ?>

</div>


<?php
$script = <<< JS

    $('#lanjutan').hide();

    var snfg_komponen = $('#flowinputsnfgkomponen-snfg_komponen').val();

    var posisi = $('#flowinputsnfgkomponen-posisi').val();

    // $.post("index.php?r=scm-planner/get-line-kemas-komponen&snfg_komponen="+$('#flowinputsnfgkomponen-snfg_komponen').val(), function (data){
    //     $("select#flowinputsnfgkomponen-nama_line").html(data);
    // });
    // $.post("index.php?r=master-data-line/get-line-kemas-komponen&snfg_komponen="+$('#flowinputsnfgkomponen-snfg_komponen').val(), function (data){
    //     $("select#flowinputsnfgkomponen-nama_line").html(data);
    // });


    $('#flowinputsnfgkomponen-jenis_kemas').change(function(){

        var jenis_kemas = $('#flowinputsnfgkomponen-jenis_kemas').val();

        $.get('index.php?r=flow-input-snfgkomponen/get-lanjutan-kemas1',{ snfg_komponen : snfg_komponen , jenis_kemas : jenis_kemas , posisi : posisi  }, function(data){
            var data = $.parseJSON(data);
            $('#flowinputsnfgkomponen-lanjutan').attr('value',data.lanjutan);

        });

        $('#lanjutan').fadeIn();

    });



JS;
$this->registerJs($script);
?>
