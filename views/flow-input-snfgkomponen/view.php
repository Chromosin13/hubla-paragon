<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfgkomponen */


?>
<div style="padding: 20px 30px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;">
    <a href=# style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">Data Berhasil di Update</a>
</div>
<br></br>
<div class="flow-input-snfgkomponen-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'posisi',
            'lanjutan',
            'datetime_start',
            'datetime_stop',
            'datetime_write',
            'nama_operator',
            'nama_line',
            'jenis_kemas',
            'is_done',
            'counter',
            'snfg_komponen',
            'jumlah_realisasi',
            'nobatch',
            'id',
        ],
    ]) ?>

    <p>
        <?= Html::a('Home', ['pusat-resolusi/create-new'], ['class' => 'btn btn-primary']) ?>
    </p>

</div>
