<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputSnfgkomponen;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile("@web/js/main.js");


?>

<style>

  .swal-button--ok,.swal-button--confirm{
    background-color: #33995E;
    color:white;
    width: 100%
  }

  .qty-bs{
    font-weight: bold;
    font-size: 4rem!important;
  }

  .swal-button--batal{
    background-color: transparent;
    color: #333;
    width: 100%;
    padding: 2px;
    font-size: 1.2rem;
  }

  .swal-button--cancel{
    width: 100%
  }

  .swal-button--ok:hover,.swal-button--confirm:hover{
    background-color: #40BF75!important;
  }

  .swal-button--batal:hover{
    background-color: transparent!important;
  }

  .swal-text, .swal-footer{
    text-align: center;
  }

  .custom-modals > .swal-footer > .swal-button-container{
    width: 100%!important
  }

  .default-modals > .swal-footer > .swal-button-container{
    width: 46%!important
  }

  #modaltitle{
    display:inline-block;
  }
  .modal-body{
    font-weight:500;
    font-size:26px;
  }
  .modal-content{
    vertical-align:middle;
    margin-top: 25%;
    background-color : #f66257;
    text-align:center;
  }

  /* SPINNER LOADING */
  .sk-circle {
    margin: 100px auto;
    width: 100px;
    height: 100px;
    position: relative;
  }
  .sk-circle .sk-child {
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
  }
  .sk-circle .sk-child:before {
    content: '';
    display: block;
    margin: 0 auto;
    width: 15%;
    height: 15%;
    background-color: #333;
    border-radius: 100%;
    -webkit-animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
            animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
  }
  .sk-circle .sk-circle2 {
    -webkit-transform: rotate(30deg);
        -ms-transform: rotate(30deg);
            transform: rotate(30deg); }
  .sk-circle .sk-circle3 {
    -webkit-transform: rotate(60deg);
        -ms-transform: rotate(60deg);
            transform: rotate(60deg); }
  .sk-circle .sk-circle4 {
    -webkit-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
            transform: rotate(90deg); }
  .sk-circle .sk-circle5 {
    -webkit-transform: rotate(120deg);
        -ms-transform: rotate(120deg);
            transform: rotate(120deg); }
  .sk-circle .sk-circle6 {
    -webkit-transform: rotate(150deg);
        -ms-transform: rotate(150deg);
            transform: rotate(150deg); }
  .sk-circle .sk-circle7 {
    -webkit-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
            transform: rotate(180deg); }
  .sk-circle .sk-circle8 {
    -webkit-transform: rotate(210deg);
        -ms-transform: rotate(210deg);
            transform: rotate(210deg); }
  .sk-circle .sk-circle9 {
    -webkit-transform: rotate(240deg);
        -ms-transform: rotate(240deg);
            transform: rotate(240deg); }
  .sk-circle .sk-circle10 {
    -webkit-transform: rotate(270deg);
        -ms-transform: rotate(270deg);
            transform: rotate(270deg); }
  .sk-circle .sk-circle11 {
    -webkit-transform: rotate(300deg);
        -ms-transform: rotate(300deg);
            transform: rotate(300deg); }
  .sk-circle .sk-circle12 {
    -webkit-transform: rotate(330deg);
        -ms-transform: rotate(330deg);
            transform: rotate(330deg); }
  .sk-circle .sk-circle2:before {
    -webkit-animation-delay: -1.1s;
            animation-delay: -1.1s; }
  .sk-circle .sk-circle3:before {
    -webkit-animation-delay: -1s;
            animation-delay: -1s; }
  .sk-circle .sk-circle4:before {
    -webkit-animation-delay: -0.9s;
            animation-delay: -0.9s; }
  .sk-circle .sk-circle5:before {
    -webkit-animation-delay: -0.8s;
            animation-delay: -0.8s; }
  .sk-circle .sk-circle6:before {
    -webkit-animation-delay: -0.7s;
            animation-delay: -0.7s; }
  .sk-circle .sk-circle7:before {
    -webkit-animation-delay: -0.6s;
            animation-delay: -0.6s; }
  .sk-circle .sk-circle8:before {
    -webkit-animation-delay: -0.5s;
            animation-delay: -0.5s; }
  .sk-circle .sk-circle9:before {
    -webkit-animation-delay: -0.4s;
            animation-delay: -0.4s; }
  .sk-circle .sk-circle10:before {
    -webkit-animation-delay: -0.3s;
            animation-delay: -0.3s; }
  .sk-circle .sk-circle11:before {
    -webkit-animation-delay: -0.2s;
            animation-delay: -0.2s; }
  .sk-circle .sk-circle12:before {
    -webkit-animation-delay: -0.1s;
            animation-delay: -0.1s; }

  @-webkit-keyframes sk-circleBounceDelay {
    0%, 80%, 100% {
      -webkit-transform: scale(0);
              transform: scale(0);
    } 40% {
      -webkit-transform: scale(1);
              transform: scale(1);
    }
  }

  @keyframes sk-circleBounceDelay {
    0%, 80%, 100% {
      -webkit-transform: scale(0);
              transform: scale(0);
    } 40% {
      -webkit-transform: scale(1);
              transform: scale(1);
    }
  }


</style>

<div id="spinner" style="visibility: hidden; position:fixed; margin-left: auto;
  margin-right: auto; left:50%; top:30%; z-index:1000;">
  <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
  </div>
</div>

                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
                      <h3 class="widget-user-username"><b>Stop Kemas 1 - IsDone</b></h3>
                      <h5 class="widget-user-desc">Work Order</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/scale.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">


                                <div class="flow-input-mo-form">

                                    <?php $form = ActiveForm::begin(); ?>

                                    <div class="flow-input-snfgkomponen-form", id = 'form-sisa-kemas'>

                                        <div class="row">
                                            <div class="col-md-4" hidden>
                                              <?= $form->field($model, 'sisa_bulk')->textInput(['required'=>'required','id'=>'sb'])->label('Sisa Bulk (kg)',['class'=>'label-class']) ?>
                                            </div>
                                            <div class="col-md-6">
                                              <?= $form->field($model, 'fg_retur_layak_pakai')->textInput(['required'=>'required','id'=>'fg-layak'])->label('FG Retur Layak Pakai (pcs)',['class'=>'label-class']) ?>
                                            </div>

                                            <div class="col-md-6">
                                              <?= $form->field($model, 'fg_retur_reject')->textInput(['required'=>'required','id'=>'fg-reject'])->label('FG Retur Reject (pcs)',['class'=>'label-class']) ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-md-offset-5" hidden>

                                        <!-- <a id='buttonStop' class = 'btn btn-danger'>Stop Jadwal</a> -->
                                        <?= Html::submitButton('NEXT',['hidden'=>'hidden','id'=>'stop-jadwal','class'=>'btn btn-success']) ?>

                                    </div>
                                    <div class="form-group col-md-12 text-center">
                                        <?= Html::button('NEXT', ['id'=>'next-dummy','class' => 'btn btn-success','style'=>'width:30%;margin-top:1rem']) ?>
                                    </div>

                                    <?php ActiveForm::end(); ?>


                                </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>

<script type="text/javascript">
  function alertEmptyBulkSisa(){
    swal({
      title: "Peringatan",
      text: "Bulk sisa belum ditimbang. Silahkan timbang bulk sisa terlebih dahulu pada station bulk sisa.",
      icon: "warning",
      className: "custom-modals",
      dangerMode: true,
      closeOnClickOutside:false,
      allowOutsideClick:false,
      buttons: {
        ok : {
          text: 'Okay',
          value:true
        },
        batal : {
          text: 'Tidak ada bulk sisa',
          value:false
        },
      },
    })
    .then((res) => {
      if (!res) {
        swal({
          title: "Konfirmasi Kembali",
          text: "Apakah anda yakin tidak ada sisa bulk pada item ini?",
          icon: "warning",
          className: "default-modals",
          dangerMode: false,
          allowOutsideClick:false,
          closeOnClickOutside:false,
          buttons: true
        })
        .then((res) => {
          if (res) {
            $('#sb').val(0)
            $('#stop-jadwal').click();
          }
        })
      }
    })
  }
</script>
<?php
$script = <<< JS

    $('#next-dummy').on('click',function(){
      document.getElementsByTagName('body')[0].style.opacity = "0.5";
      document.getElementById("spinner").style.visibility = "visible";
      document.getElementsByTagName('body')[0].disabled = true;
      var snfg_komponen = "$snfg_komponen";
      var sisa_bulk = $('#sb').val();
      var fg_retur_layak_pakai = $('#fg-layak').val();
      var fg_retur_reject = $('#fg-reject').val();
      $.get('index.php?r=flow-input-snfgkomponen/get-sisa-bulk',{ snfg_komponen: snfg_komponen },function(result){
        if (fg_retur_layak_pakai == null || fg_retur_layak_pakai == '') {
          document.getElementsByTagName("body")[0].style.opacity = "1";
          document.getElementById("spinner").style.visibility = "hidden";
          document.getElementsByTagName('body')[0].disabled = false;
          customAlert('warning','Form Belum Lengkap','Silahkan lengkapi field FG Retur Layak Pakai (Pcs) terlebih dahulu.');
        }else{
          if (fg_retur_reject == null || fg_retur_reject == '') {
            document.getElementsByTagName('body')[0].style.opacity = "1";
            document.getElementById("spinner").style.visibility = "hidden";
            document.getElementsByTagName('body')[0].disabled = false;
            customAlert('warning','Form Belum Lengkap','Silahkan lengkapi field FG Retur Reject (Pcs) terlebih dahulu.');
          }else{
            document.getElementsByTagName('body')[0].style.opacity = "1";
            document.getElementById("spinner").style.visibility = "hidden";
            document.getElementsByTagName('body')[0].disabled = false;
            if (result == 0) {
              alertEmptyBulkSisa();
            }else{
              var span = document.createElement("span");
              // span.innerHTML = "Berat sisa bulk pada FRO Bulk Sisa sebesar:<br><span class='qty-bs'>"+parseInt(totalSisaBulk)+" Kg</span><br>Apakah berat sisa bulk sudah sesuai dengan berat yang sebenarnya?";
              span.innerHTML = "Berat sisa bulk pada FRO Bulk Sisa sebesar:<br><br><span class='qty-bs'>"+parseInt(result)+" Kg</span>";
              swal({
                title: "Informasi",
                content:span,
                className:'custom-modals',
                icon: "info",
                html:true,
                closeOnClickOutside:false,
                allowOutsideClick:false,
                buttons: {
                  ok : {
                    text: 'Okay',
                    value:false
                  },
                },
              })
              .then((res) => {
                if (!res) {
                  $('#sb').val(result)
                  $('#stop-jadwal').click();
                }
              })
            }
          }
        }
      })

    })


JS;
$this->registerJs($script);
?>
