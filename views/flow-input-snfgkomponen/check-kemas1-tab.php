<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowAnalyst */

?>
<div class="flow-checkkemas1-tab">

    <?= $this->render('_form-check-kemas1-tab', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'frontend_ip' => $frontend_ip,
        'sbc_ip' => $sbc_ip,
        'line' => $line,
    ]) ?>

</div>
