<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputSnfgkomponen;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\Downtime;
use app\models\DowntimeSearch;
use kartik\widgets\DateTimePicker;
/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */
/* @var $form yii\widgets\ActiveForm */
?>


                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
                      <h3 class="widget-user-username"><b>Kemas 1 / Packing</b></h3>
                      <h5 class="widget-user-desc">Work Order</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">

                                


                                <div class="flow-input-snfg-form">

                                    <?php $form = ActiveForm::begin(); ?>

                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'snfg_komponen')->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'posisi')->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>
                                     <div class="row">
                                        <div class="col-md-6">
                                        <?=
                                            $form->field($model, 'datetime_start')->widget(DateTimePicker::classname(), [
                                                'options' => ['placeholder' => 'Enter event time ...',
                                                              'readOnly' => true
                                                             ],
                                                    'pluginOptions' => [
                                                    'autoclose' => true,
                                                    'showMeridian' => false,
                                                    'minuteStep' => 5,
                                                    
                                                ]
                                            ]);

                                        ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-6">
                                        <?=
                                            $form->field($model, 'datetime_stop')->widget(DateTimePicker::classname(), [
                                                'options' => ['placeholder' => 'Enter event time ...',
                                                              'readOnly' => true
                                                             ],
                                                    'pluginOptions' => [
                                                    'autoclose' => true,
                                                    'showMeridian' => false,
                                                    'minuteStep' => 5,
                                                    
                                                ]
                                            ]);

                                        ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'jenis_kemas')->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        <!-- /.col-lg-6 -->
                                        <div class="col-md-3">
                                          <?php
                                                echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                                                    'data' => ArrayHelper::map(FlowInputSnfgkomponen::find()->all()
                                                    ,'nama_line','nama_line'),
                                                    'options' => ['placeholder' => 'Select Line'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]);
                                            ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div id="lanjutan">
                                        <div class="col-md-3">
                                          <?= $form->field($model, 'lanjutan')->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        </div>
                                    </div>
 

                                        <?= $form->field($model, 'nama_operator')->textInput(['readOnly'=>true]) ?>
                                        <?= $form->field($model, 'jumlah_realisasi')->textInput(['readOnly'=>true]) ?>
                                        <?= $form->field($model, 'counter')->textInput(['readOnly'=>true]) ?>


                                    <br></br>

                                    <div class="row">

                                        <div class="col-md-2 col-md-offset-5">

                                            <?= Html::submitButton($model->isNewRecord ? 'Start' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                            
                                        </div>
                                    </div>

                                    <?php ActiveForm::end(); ?>

                                </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>



<?php
$script = <<< JS

    $('#lanjutan').hide();

    var snfg_komponen = $('#flowinputsnfgkomponen-snfg_komponen').val();
    
    var posisi = $('#flowinputsnfgkomponen-posisi').val();

    $.post("index.php?r=scm-planner/get-line-kemas-komponen&snfg_komponen="+$('#flowinputsnfgkomponen-snfg_komponen').val(), function (data){
        $("select#flowinputsnfgkomponen-nama_line").html(data);
    });




JS;
$this->registerJs($script);
?>