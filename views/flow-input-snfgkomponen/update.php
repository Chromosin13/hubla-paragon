<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfgkomponen */

?>
<div class="flow-input-snfgkomponen-update">
<div style="padding: 20px 30px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;">
	<a href=# style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">Gunakan Sebijaknya; Pengubahan Data Akan Direkam dan menjadi bahan Evaluasi untuk Supervisor, Leader, dan Operator</a>
</div>

    <?= $this->render('_form-edit-kemas1', [
        'model' => $model,
    ]) ?>

</div>
