<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use faryshta\widgets\JqueryTagsInput;

/* @var $this yii\web\View */
/* @var $model app\models\Inkjet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inkjet-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Input dan Informasi SNFG dari Planner</h2>
                <?= $form->field($model, 'snfg_komponen')->textInput() ?>

                <?= $form->field($model, 'snfg')->textInput(['readonly' => 'true']) ?>
                <b>Streamline</b>
                <input type="text" class="form-control" id="streamline" disabled>
                <b>Start</b>
                <input type="text" class="form-control" id="start" placeholder="" disabled>
                <b>Due</b>
                <input type="text" class="form-control" id="due" placeholder="" disabled>
                <b>Nama Bulk</b>
                <input type="text" class="form-control" id="nama-bulk" placeholder="" disabled>
                <b>Nama FG</b>
                <input type="text" class="form-control" id="nama-fg" placeholder="" disabled>
                <b>Besar Batch</b>
                <input type="text" class="form-control" id="besar-batch" placeholder="" disabled>
                <b>Kode Jadwal</b>
                <input type="text" class="form-control" id="kode-jadwal" placeholder="" disabled>
                <b>Jumlah Pc(s)</b>
                <input type="text" class="form-control" id="jumlah" placeholder="" disabled>
            </div>
    </div>


    <!-- Button untuk change event START, STOP, ISTIRAHAT START, ISTIRAHAT STOP -->

    <a class="btn btn-app" id="start-button">
                <i class="fa fa-play"></i> Start
    </a>

    <a class="btn btn-app" id="stop-button">
                <i class="fa fa-pause"></i> Stop
    </a>

    <a class="btn btn-app" id="istirahat-start-button">
                <i class="fa fa-play"></i> Istirahat Start
    </a>

    <a class="btn btn-app" id="istirahat-stop-button">
                <i class="fa fa-pause"></i> Istirahat Stop
    </a>

    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'jenis_inkjet')->dropDownList(['INKJET PRODUCT' => 'INKJET PRODUCT','INKJET STICKER PRODUCT' => 'INKJET STICKER PRODUCT','INKJET DUS SATUAN' => 'INKJET DUS SATUAN','INKJET STICKER BOTTOM' => 'INKJET STICKER BOTTOM','INKJET DUS 12' => 'INKJET DUS 12','INKJET DAICHI' => 'INKJET DAICHI'],['prompt'=>'Select Option']); ?>

    <div class="box" id="inkjet-start">
            <div class="box-header with-border">
              <h2 class="box-title">Start Entry</h2>

                <?= $form->field($model, 'lanjutan')->textInput(['id'=>'inkjet-lanjutan','readonly' => 'true']) ?>

                <?= $form->field($model, 'nama_line')->textInput() ?>

                <?= $form->field($model, 'jumlah_operator')->textInput() ?>

                <?php echo $form->field($model, 'nama_operator')->widget(JqueryTagsInput::className([]));
                ?>
                
            </div>
    </div>

    <div class="box" id="inkjet-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Stop Entry</h2>
                <?= $form->field($model, 'jumlah_realisasi')->textInput() ?>

                <?= $form->field($model, 'jumlah_reject')->textInput() ?>

                <?= $form->field($model, 'keterangan')->textInput() ?>

                <?= $form->field($model, 'lanjutan')->textInput(['id'=>'inkjet-lanjutan_stop','readonly' => 'true']) ?>
            </div>
    </div>

    <div class="box" id="inkjet-istirahat-start">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Start Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'inkjet-lanjutan_istirahat_start','readonly' => 'true']) ?> 
            </div>
    </div>

    <div class="box" id="inkjet-istirahat-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Stop Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'inkjet-lanjutan_istirahat_stop','readonly' => 'true']) ?>
            </div>
    </div>    
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
$('#inkjet-snfg_komponen').change(function(){
    var snfg_komponen = $(this).val();
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#inkjet-snfg').attr('value',data.snfg);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
        $('#besar-batch').attr('value',data.besar_batch);
        $('#kode-jadwal').attr('value',data.kode_jadwal);
        $('#jumlah').attr('value',data.jumlah);
    });
});

$(function() {
    $('#inkjet-start').hide();
    $('#inkjet-stop').hide(); 
    $('#inkjet-istirahat-start').hide(); 
    $('#inkjet-istirahat-stop').hide(); 
    $('#start-button').click(function(){
            var start = "START";
            $('#inkjet-state').attr('value',start);
                    $('#inkjet-start').show();
                    $('#inkjet-stop').hide(); 
                    $('#inkjet-istirahat-start').hide(); 
                    $('#inkjet-istirahat-stop').hide();
            $('#inkjet-jenis_inkjet').change(function(){
                var jenis_inkjet = $('#inkjet-jenis_inkjet').val();
                var snfg_komponen = $('#inkjet-snfg_komponen').val();    
                $.get('index.php?r=inkjet/lanjutan-inkjet',{ snfg_komponen : snfg_komponen, jenis_inkjet : jenis_inkjet },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#inkjet-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan_stop').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                     
        });

    $('#istirahat-start-button').click(function(){
            var ist_start = "ISTIRAHAT START";
            $('#inkjet-state').attr('value',ist_start);
                    $('#inkjet-istirahat-start').show(); 
                    $('#inkjet-stop').hide(); 
                    $('#inkjet-start').hide(); 
                    $('#inkjet-istirahat-stop').hide(); 
            $('#inkjet-jenis_inkjet').change(function(){
                var jenis_inkjet = $('#inkjet-jenis_inkjet').val();
                var snfg_komponen = $('#inkjet-snfg_komponen').val();    
                $.get('index.php?r=inkjet/lanjutan-inkjet',{ snfg_komponen : snfg_komponen, jenis_inkjet : jenis_inkjet },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#inkjet-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan_stop').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                     
        });

    $('#istirahat-stop-button').click(function(){
            var ist_stop = "ISTIRAHAT STOP";
            $('#inkjet-state').attr('value',ist_stop);
                    $('#inkjet-istirahat-stop').show();
                    $('#inkjet-stop').hide(); 
                    $('#inkjet-istirahat-start').hide(); 
                    $('#inkjet-start').hide(); 
            $('#inkjet-jenis_inkjet').change(function(){
                var jenis_inkjet = $('#inkjet-jenis_inkjet').val();
                var snfg_komponen = $('#inkjet-snfg_komponen').val();    
                $.get('index.php?r=inkjet/lanjutan-inkjet',{ snfg_komponen : snfg_komponen, jenis_inkjet : jenis_inkjet },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#inkjet-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan_stop').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                         
        });

    $('#stop-button').click(function(){
             var stop = "STOP";
             $('#inkjet-state').attr('value',stop);
                    $('#inkjet-stop').show(); 
                    $('#inkjet-start').hide(); 
                    $('#inkjet-istirahat-start').hide(); 
                    $('#inkjet-istirahat-stop').hide();    
            });
            $('#inkjet-jenis_inkjet').change(function(){
                var jenis_inkjet = $('#inkjet-jenis_inkjet').val();
                var snfg_komponen = $('#inkjet-snfg_komponen').val();    
                $.get('index.php?r=inkjet/lanjutan-inkjet',{ snfg_komponen : snfg_komponen, jenis_inkjet : jenis_inkjet },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#inkjet-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan_stop').attr('value',data.lanjutan);
                            $('#inkjet-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });               
    });

// $(function() {
//     $('#inkjet-start').hide();
//     $('#inkjet-stop').hide(); 
//     $('#inkjet-istirahat-start').hide(); 
//     $('#inkjet-istirahat-stop').hide(); 
//     $('#start-button').click(function(){
//             var snfg = $('#inkjet-snfg').val();
//                             $.get('index.php?r=inkjet/lanjutan-inkjet',{ snfg : snfg },function(data){
//                                 var data = $.parseJSON(data);
//                                 //alert(data.sediaan);
//                                 $('#inkjet-lanjutan_istirahat_start').attr('value',data.lanjutan);
//                                 $('#inkjet-lanjutan').attr('value',data.lanjutan);
//                                 $('#inkjet-lanjutan_stop').attr('value',data.lanjutan);
//                                 $('#inkjet-lanjutan_istirahat_stop').attr('value',data.lanjutan);
//                             });
//             var start = "START";
//             $('#inkjet-state').attr('value',start);
//                     $('#inkjet-start').show();
//                     $('#inkjet-stop').hide(); 
//                     $('#inkjet-istirahat-start').hide(); 
//                     $('#inkjet-istirahat-stop').hide();                       
//         });

//     $('#istirahat-start-button').click(function(){
//             var snfg = $('#inkjet-snfg').val();
//                             $.get('index.php?r=inkjet/lanjutan-inkjet',{ snfg : snfg },function(data){
//                                 var data = $.parseJSON(data);
//                                 //alert(data.sediaan);
//                                 $('#inkjet-lanjutan_istirahat_start').attr('value',data.lanjutan);
//                                 $('#inkjet-lanjutan').attr('value',data.lanjutan);
//                                 $('#inkjet-lanjutan_stop').attr('value',data.lanjutan);
//                                 $('#inkjet-lanjutan_istirahat_stop').attr('value',data.lanjutan);
//                             });
//             var ist_start = "ISTIRAHAT START";
//             $('#inkjet-state').attr('value',ist_start);
//                     $('#inkjet-istirahat-start').show(); 
//                     $('#inkjet-stop').hide(); 
//                     $('#inkjet-start').hide(); 
//                     $('#inkjet-istirahat-stop').hide(); 
//         });

//     $('#istirahat-stop-button').click(function(){
//             var snfg = $('#inkjet-snfg').val();
//                             $.get('index.php?r=inkjet/lanjutan-inkjet',{ snfg : snfg },function(data){
//                                 var data = $.parseJSON(data);
//                                 //alert(data.sediaan);
//                                 $('#inkjet-lanjutan_istirahat_start').attr('value',data.lanjutan);
//                                 $('#inkjet-lanjutan').attr('value',data.lanjutan);
//                                 $('#inkjet-lanjutan_stop').attr('value',data.lanjutan);
//                                 $('#inkjet-lanjutan_istirahat_stop').attr('value',data.lanjutan);
//                             });
//             var ist_stop = "ISTIRAHAT STOP";
//             $('#inkjet-state').attr('value',ist_stop);
//                     $('#inkjet-istirahat-stop').show();
//                     $('#inkjet-stop').hide(); 
//                     $('#inkjet-istirahat-start').hide(); 
//                     $('#inkjet-start').hide(); 
//         });

//     $('#stop-button').click(function(){
//             var snfg = $('#inkjet-snfg').val();
//                             $.get('index.php?r=inkjet/lanjutan-inkjet',{ snfg : snfg },function(data){
//                                 var data = $.parseJSON(data);
//                                 //alert(data.sediaan);
//                                 $('#inkjet-lanjutan_istirahat_start').attr('value',data.lanjutan);
//                                 $('#inkjet-lanjutan').attr('value',data.lanjutan);
//                                 $('#inkjet-lanjutan_stop').attr('value',data.lanjutan);
//                                 $('#inkjet-lanjutan_istirahat_stop').attr('value',data.lanjutan);
//                             });
//             var stop = "STOP";
//             $('#inkjet-state').attr('value',stop);
//                     $('#inkjet-stop').show(); 
//                     $('#inkjet-start').hide(); 
//                     $('#inkjet-istirahat-start').hide(); 
//                     $('#inkjet-istirahat-stop').hide();    
//         });

//     });


JS;
$this->registerJs($script);
?>