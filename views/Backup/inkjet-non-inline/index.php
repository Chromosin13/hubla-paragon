

<?php

use yii\helpers\Html;
use yii\grid\GridView;
//use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InkjetNonInlineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inkjet Non Inlines';
$this->params['breadcrumbs'][] = $this->title;
?>



<!-- <div class="element-style"> -->
    <p>
        <?= Html::a('Create Inkjet Non Inline', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<div class="inkjet-non-inline-index" style="overflow-x:auto;height:1000px;">

    <?=     GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'snfg',
                'inkjet_sticker_bottom_jumlah_plan',
                'inkjet_sticker_bottom_jumlah_realisasi',
                'inkjet_sticker_bottom_nama_line',
                'inkjet_sticker_bottom_jumlah_operator',
                'inkjet_sticker_bottom_operator_1',
                'inkjet_sticker_bottom_operator_2',
                'inkjet_sticker_bottom_operator_3',
                'inkjet_sticker_bottom_waktu',
                'inkjet_sticker_bottom_state',
                'inkjet_dus_satuan_nama_line',
                'inkjet_dus_satuan_jumlah_operator',
                'inkjet_dus_satuan_operator_1',
                'inkjet_dus_satuan_operator_2',
                'inkjet_dus_satuan_operator_3',
                'inkjet_dus_satuan_waktu',
                'inkjet_dus_satuan_state',
                'inkjet_dus_12_jumlah_plan',
                'inkjet_dus_12_jumlah_realisasi',
                'inkjet_dus_12_nama_line',
                'inkjet_dus_12_jumlah_operator',
                'inkjet_dus_12_operator_1',
                'inkjet_dus_12_operator_2',
                'inkjet_dus_12_operator_3',
                'inkjet_dus_12_waktu',
                'inkjet_dus_12_state',
                'inkjet_packaging_primer_jumlah_plan',
                'inkjet_packaging_primer_jumlah_realisasi',
                'inkjet_packaging_primer_nama_line',
                'inkjet_packaging_primer_jumlah_operator',
                'inkjet_packaging_primer_operator_1',
                'inkjet_packaging_primer_operator_2',
                'inkjet_packaging_primer_operator_3',
                'inkjet_packaging_primer_waktu',
                'inkjet_packaging_primer_state',
                'inkjet_dus_satuan_jumlah_plan',
                'inkjet_dus_satuan_jumlah_realisasi',
                
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); 
    ?>
</div>


