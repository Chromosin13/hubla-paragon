<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\qcbulk;
use faryshta\widgets\JqueryTagsInput;

/* @var $this yii\web\View */
/* @var $model app\models\QcBulk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qcbulk-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Input dan Informasi SNFG dari Planner</h2>
                <?= $form->field($model, 'snfg_komponen')->textInput() ?>

                <?= $form->field($model, 'snfg')->textInput(['readonly' => 'true']) ?>
                <b>Streamline</b>
                <b>Streamline</b>
                <input type="text" class="form-control" id="streamline" disabled>
                <b>Start</b>
                <input type="text" class="form-control" id="start" placeholder="" disabled>
                <b>Due</b>
                <input type="text" class="form-control" id="due" placeholder="" disabled>
                <b>Nama Bulk</b>
                <input type="text" class="form-control" id="nama-bulk" placeholder="" disabled>
                <b>Nama FG</b>
                <input type="text" class="form-control" id="nama-fg" placeholder="" disabled>
                <b>Besar Batch</b>
                <input type="text" class="form-control" id="besar-batch" placeholder="" disabled>
                <b>Besar per lot (Kg)</b>
                <input type="text" class="form-control" id="besar-lot" placeholder="" disabled>
                <b>Lot ke-</b>
                <input type="text" class="form-control" id="lot" placeholder="" disabled>
                <b>Kode Jadwal</b>
                <input type="text" class="form-control" id="kode-jadwal" placeholder="" disabled>
                <b>No Batch</b>
                <input type="text" class="form-control" id="no-batch" placeholder="" disabled>
                <b>Besar Batch Real</b>
                <input type="text" class="form-control" id="besar-batch-real" placeholder="" disabled>
            </div>
    </div>


    <!-- Button untuk change event START, STOP, ISTIRAHAT START, ISTIRAHAT STOP -->

    <a class="btn btn-app" id="start-button">
                <i class="fa fa-play"></i> Start
    </a>

    <a class="btn btn-app" id="stop-button">
                <i class="fa fa-pause"></i> Stop
    </a>

    <a class="btn btn-app" id="istirahat-start-button">
                <i class="fa fa-play"></i> Istirahat Start
    </a>

    <a class="btn btn-app" id="istirahat-stop-button">
                <i class="fa fa-pause"></i> Istirahat Stop
    </a>

    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'jenis_periksa')->dropDownList(['JADWAL BARU' => 'JADWAL BARU','REWORK' => 'REWORK'],['prompt'=>'Select Option']); ?>

    <div class="box" id="qcbulk-start">
            <div class="box-header with-border">
              <h2 class="box-title">Start Entry</h2>

                <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>

                <!-- <?= $form->field($model, 'nama_line')->textInput() ?> -->

                <?= $form->field($model, 'jumlah_operator')->textInput() ?>

                <?php echo $form->field($model, 'nama_qc')->widget(JqueryTagsInput::className([]));
                ?>
                
            </div>
    </div>

    <div class="box" id="qcbulk-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Stop Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'qcbulk-lanjutan_stop','readonly' => 'true']) ?> 

                 <?= $form->field($model, 'ph')->textInput() ?>

                <?= $form->field($model, 'viskositas')->textInput() ?>

                <?= $form->field($model, 'berat_jenis')->textInput() ?>

                <?= $form->field($model, 'kadar')->textInput() ?>

                <?= $form->field($model, 'warna')->textInput() ?>

                <?= $form->field($model, 'bau')->textInput() ?>

                <?= $form->field($model, 'performance')->textInput() ?>

                <?= $form->field($model, 'bentuk')->textInput() ?>

                <?= $form->field($model, 'mikro')->textInput() ?>

                <?= $form->field($model, 'kejernihan')->textInput() ?>

                <?= $form->field($model, 'status')->dropDownList(['RELEASE' => 'RELEASE','REWORK' => 'REWORK','RELEASE_UNCOMFORMITY' => 'RELEASE_UNCOMFORMITY','REJECT' => 'REJECT','PENDING_1' => 'PENDING_1','PENDING_2' => 'PENDING_2','ADJUST' => 'ADJUST','PENDING_MIKRO' => 'PENDING_MIKRO'],['prompt'=>'Select Option']); ?>
            </div>
    </div>

    <div class="box" id="qcbulk-istirahat-start">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Start Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'qcbulk-lanjutan_istirahat_start','readonly' => 'true']) ?> 
            </div>
    </div>

    <div class="box" id="qcbulk-istirahat-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Stop Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'qcbulk-lanjutan_istirahat_stop','readonly' => 'true']) ?>
            </div>
    </div>    

    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
$('#qcbulk-snfg_komponen').change(function(){
    var snfg_komponen = $(this).val();
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#qcbulk-snfg').attr('value',data.snfg);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
        $('#besar-batch').attr('value',data.besar_batch);
        $('#besar-lot').attr('value',data.besar_lot);
        $('#lot').attr('value',data.lot);
        $('#kode-jadwal').attr('value',data.kode_jadwal);
    });
    $.get('index.php?r=pengolahan/get-pengolahan-komponen',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#no-batch').attr('value',data.nobatch);
        $('#besar-batch-real').attr('value',data.besar_batch_real);
    });
});


$(function() {
    $('#qcbulk-start').hide();
    $('#qcbulk-stop').hide(); 
    $('#qcbulk-istirahat-start').hide(); 
    $('#qcbulk-istirahat-stop').hide(); 
    $('#start-button').click(function(){
            var start = "START";
            $('#qcbulk-state').attr('value',start);
                    $('#qcbulk-start').show();
                    $('#qcbulk-stop').hide(); 
                    $('#qcbulk-istirahat-start').hide(); 
                    $('#qcbulk-istirahat-stop').hide();
            $('#qcbulk-jenis_periksa').change(function(){
                var jenis_periksa = $('#qcbulk-jenis_periksa').val();
                var snfg_komponen = $('#qcbulk-snfg_komponen').val();    
                $.get('index.php?r=qc-bulk/lanjutan-qc-bulk',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#qcbulk-lanjutan').attr('value',data.lanjutan);
                            $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
                            $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                     
        });

    $('#istirahat-start-button').click(function(){
            var ist_start = "ISTIRAHAT START";
            $('#qcbulk-state').attr('value',ist_start);
                    $('#qcbulk-istirahat-start').show(); 
                    $('#qcbulk-stop').hide(); 
                    $('#qcbulk-start').hide(); 
                    $('#qcbulk-istirahat-stop').hide(); 
            $('#qcbulk-jenis_periksa').change(function(){
                var jenis_periksa = $('#qcbulk-jenis_periksa').val();
                var snfg_komponen = $('#qcbulk-snfg_komponen').val();    
                $.get('index.php?r=qc-bulk/lanjutan-qc-bulk',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#qcbulk-lanjutan').attr('value',data.lanjutan);
                            $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
                            $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                     
        });

    $('#istirahat-stop-button').click(function(){
            var ist_stop = "ISTIRAHAT STOP";
            $('#qcbulk-state').attr('value',ist_stop);
                    $('#qcbulk-istirahat-stop').show();
                    $('#qcbulk-stop').hide(); 
                    $('#qcbulk-istirahat-start').hide(); 
                    $('#qcbulk-start').hide(); 
            $('#qcbulk-jenis_periksa').change(function(){
                var jenis_periksa = $('#qcbulk-jenis_periksa').val();
                var snfg_komponen = $('#qcbulk-snfg_komponen').val();    
                $.get('index.php?r=qc-bulk/lanjutan-qc-bulk',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#qcbulk-lanjutan').attr('value',data.lanjutan);
                            $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
                            $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                         
        });

    $('#stop-button').click(function(){
             var stop = "STOP";
             $('#qcbulk-state').attr('value',stop);
                    $('#qcbulk-stop').show(); 
                    $('#qcbulk-start').hide(); 
                    $('#qcbulk-istirahat-start').hide(); 
                    $('#qcbulk-istirahat-stop').hide();    
            });
            $('#qcbulk-jenis_periksa').change(function(){
                var jenis_periksa = $('#qcbulk-jenis_periksa').val();
                var snfg_komponen = $('#qcbulk-snfg_komponen').val();    
                $.get('index.php?r=qc-bulk/lanjutan-qc-bulk',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#qcbulk-lanjutan').attr('value',data.lanjutan);
                            $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
                            $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });               
    });

// $(function() {
//     $('#qcbulk-start').hide();
//     $('#qcbulk-stop').hide(); 
//     $('#qcbulk-istirahat-start').hide(); 
//     $('#qcbulk-istirahat-stop').hide(); 
//     $('#start-button').click(function(){
//             var snfg = $('#qcbulk-snfg').val();
//                             $.get('index.php?r=qc-bulk/lanjutan-qc-bulk',{ snfg : snfg },function(data){
//                                 var data = $.parseJSON(data);
//                                 //alert(data.sediaan);
//                                 $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
//                                 $('#qcbulk-lanjutan').attr('value',data.lanjutan);
//                                 $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
//                                 $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);
//                             });
//             var start = "START";
//             $('#qcbulk-state').attr('value',start);
//                     $('#qcbulk-start').show();
//                     $('#qcbulk-stop').hide(); 
//                     $('#qcbulk-istirahat-start').hide(); 
//                     $('#qcbulk-istirahat-stop').hide();                       
//         });

//     $('#istirahat-start-button').click(function(){
//             var snfg = $('#qcbulk-snfg').val();
//                             $.get('index.php?r=qc-bulk/lanjutan-qc-bulk',{ snfg : snfg },function(data){
//                                 var data = $.parseJSON(data);
//                                 //alert(data.sediaan);
//                                 $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
//                                 $('#qcbulk-lanjutan').attr('value',data.lanjutan);
//                                 $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
//                                 $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);
//                             });
//             var ist_start = "ISTIRAHAT START";
//             $('#qcbulk-state').attr('value',ist_start);
//                     $('#qcbulk-istirahat-start').show(); 
//                     $('#qcbulk-stop').hide(); 
//                     $('#qcbulk-start').hide(); 
//                     $('#qcbulk-istirahat-stop').hide(); 
//         });

//     $('#istirahat-stop-button').click(function(){
//             var snfg = $('#qcbulk-snfg').val();
//                             $.get('index.php?r=qc-bulk/lanjutan-qc-bulk',{ snfg : snfg },function(data){
//                                 var data = $.parseJSON(data);
//                                 //alert(data.sediaan);
//                                 $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
//                                 $('#qcbulk-lanjutan').attr('value',data.lanjutan);
//                                 $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
//                                 $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);
//                             });
//             var ist_stop = "ISTIRAHAT STOP";
//             $('#qcbulk-state').attr('value',ist_stop);
//                     $('#qcbulk-istirahat-stop').show();
//                     $('#qcbulk-stop').hide(); 
//                     $('#qcbulk-istirahat-start').hide(); 
//                     $('#qcbulk-start').hide(); 
//         });

//     $('#stop-button').click(function(){
//             var snfg = $('#qcbulk-snfg').val();
//                             $.get('index.php?r=qc-bulk/lanjutan-qc-bulk',{ snfg : snfg },function(data){
//                                 var data = $.parseJSON(data);
//                                 //alert(data.sediaan);
//                                 $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
//                                 $('#qcbulk-lanjutan').attr('value',data.lanjutan);
//                                 $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
//                                 $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);
//                             });
//             var stop = "STOP";
//             $('#qcbulk-state').attr('value',stop);
//                     $('#qcbulk-stop').show(); 
//                     $('#qcbulk-start').hide(); 
//                     $('#qcbulk-istirahat-start').hide(); 
//                     $('#qcbulk-istirahat-stop').hide();    
//         });

//     });

JS;
$this->registerJs($script);
?>