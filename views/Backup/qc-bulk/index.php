<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QcFgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'QC Bulk';
$this->params['breadcrumbs'][] = $this->title;
?>

    <p>
        <?= Html::a('Input QC Bulk', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<div class="qc-bulk-index"  style="overflow-x:auto;height:1000px;" >

    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'snfg',
            'ph',
            'viskositas',
            'berat_jenis',
            'kadar',
            'warna',
            'bau',
            'performance',
            'bentuk',
            'mikro',
            'kejernihan',
            'status',
            'jumlah_operator',
            'nama_qc',
            'waktu',
            'jenis_periksa',
            // 'nama_line',
            'lanjutan',
            'state',
            'timestamp',
            'snfg_komponen',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>