<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?=Yii::$app->user->identity->username?></p>


                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                    ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
                    [
                        'label' => 'Same tools',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'fa fa-circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'fa fa-circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                    ['label' => 'Running Process Positon', 'icon' => 'fa fa-file-code-o', 'url' => ['posisi-proses/index']],
                    ['label' => 'Dashboard Panel', 'icon' => 'fa fa-dashboard', 'url' => ['posisi-proses/dashboard']],
                    //['label' => 'Flow Report', 'icon' => 'fa fa-dashboard', 'url' => ['session_destroy();'']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Flow Report Input Data',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Planner', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/create'],],
                            ['label' => 'Penimbangan', 'icon' => 'fa fa-circle-o', 'url' => ['penimbangan/create'],],
                            ['label' => 'Pengolahan', 'icon' => 'fa fa-circle-o', 'url' => ['pengolahan/create'],],
                            ['label' => 'QC Bulk', 'icon' => 'fa fa-circle-o', 'url' => ['qc-bulk/create'],],
                            ['label' => 'Inkjet', 'icon' => 'fa fa-circle-o', 'url' => ['inkjet/create'],],
                            ['label' => 'Kemas 1', 'icon' => 'fa fa-circle-o', 'url' => ['kemas1/create'],],
                            ['label' => 'Kemas 2', 'icon' => 'fa fa-circle-o', 'url' => ['kemas2/create'],],
                            ['label' => 'QC FG', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg/create'],],
                            ['label' => 'QC Timbang FG', 'icon' => 'fa fa-circle-o', 'url' => ['qc-timbang-fg/create'],],
                        ],
                    ],
                    [
                        'label' => 'Flow Report Check Data',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Planner', 'icon' => 'fa fa-circle-o', 'url' => ['scm-planner/index'],],
                            ['label' => 'Penimbangan', 'icon' => 'fa fa-circle-o', 'url' => ['penimbangan/index'],],
                            ['label' => 'Pengolahan', 'icon' => 'fa fa-circle-o', 'url' => ['pengolahan/index'],],
                            ['label' => 'QC Bulk', 'icon' => 'fa fa-circle-o', 'url' => ['qc-bulk/index'],],
                            ['label' => 'Inkjet', 'icon' => 'fa fa-circle-o', 'url' => ['inkjet/index'],],
                            ['label' => 'Kemas 1', 'icon' => 'fa fa-circle-o', 'url' => ['kemas1/index'],],
                            ['label' => 'Kemas 2', 'icon' => 'fa fa-circle-o', 'url' => ['kemas2/index'],],
                            ['label' => 'QC FG', 'icon' => 'fa fa-circle-o', 'url' => ['qc-fg/index'],],
                            ['label' => 'QC Timbang FG', 'icon' => 'fa fa-circle-o', 'url' => ['qc-timbang-fg/index'],],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
