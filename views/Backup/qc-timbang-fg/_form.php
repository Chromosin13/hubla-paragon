<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use faryshta\widgets\JqueryTagsInput;

/* @var $this yii\web\View */
/* @var $model app\models\QcTimbangFg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qc-timbang-fg-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Input dan Informasi SNFG dari Planner</h2>
                <?= $form->field($model, 'snfg')->textInput() ?>
                <b>Streamline</b>
                <input type="text" class="form-control" id="streamline" disabled>
                <b>Start</b>
                <input type="text" class="form-control" id="start" placeholder="" disabled>
                <b>Due</b>
                <input type="text" class="form-control" id="due" placeholder="" disabled>
                <b>Nama Bulk</b>
                <input type="text" class="form-control" id="nama-bulk" placeholder="" disabled>
                <b>Nama FG</b>
                <input type="text" class="form-control" id="nama-fg" placeholder="" disabled>
                <b>No Batch</b>
                <input type="text" class="form-control" id="no-batch" placeholder="" disabled>
            </div>
    </div>


    <a class="btn btn-app" id="start-button">
                <i class="fa fa-play"></i> Start
    </a>

    <a class="btn btn-app" id="stop-button">
                <i class="fa fa-pause"></i> Stop
    </a>

    <a class="btn btn-app" id="istirahat-start-button">
                <i class="fa fa-play"></i> Istirahat Start
    </a>

    <a class="btn btn-app" id="istirahat-stop-button">
                <i class="fa fa-pause"></i> Istirahat Stop
    </a>

    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'jenis_periksa')->dropDownList(['JADWAL BARU' => 'JADWAL BARU','REWORK' => 'REWORK'],['prompt'=>'Select Option']); ?>


    <div class="box" id="qctimbangfg-start">
            <div class="box-header with-border">
              <h2 class="box-title">Start Entry</h2>

                
                <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>

                <?= $form->field($model, 'palet_ke')->textInput() ?>

                <!-- <?= $form->field($model, 'nama_line')->textInput() ?> -->

                <?= $form->field($model, 'jumlah_inspektor')->textInput() ?>

                <?php echo $form->field($model, 'nama_inspektor')->widget(JqueryTagsInput::className([]));
                ?>
                
            </div>
    </div>

    <div class="box" id="qctimbangfg-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Stop Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'qctimbangfg-lanjutan_stop','readonly' => 'true']) ?> 

                 <?= $form->field($model, 'qty')->textInput() ?>

                <?= $form->field($model, 'status')->dropDownList(['RELEASE' => 'RELEASE','REWORK' => 'REWORK','RECOUNT' => 'RECOUNT','CEK_MIKRO' => 'CEK_MIKRO'],['prompt'=>'Select Option']); ?>
            </div>
    </div>

    <div class="box" id="qctimbangfg-istirahat-start">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Start Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'qctimbangfg-lanjutan_istirahat_start','readonly' => 'true']) ?> 
            </div>
    </div>

    <div class="box" id="qctimbangfg-istirahat-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Stop Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'qctimbangfg-lanjutan_istirahat_stop','readonly' => 'true']) ?>
            </div>
    </div>    


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
$('#qctimbangfg-snfg').change(function(){
    var snfg = $(this).val();
    $.get('index.php?r=scm-planner/get-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
    });
    $.get('index.php?r=pengolahan/get-pengolahan',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#no-batch').attr('value',data.nobatch);
    });
});


$(function() {
    $('#qctimbangfg-start').hide();
    $('#qctimbangfg-stop').hide(); 
    $('#qctimbangfg-istirahat-start').hide(); 
    $('#qctimbangfg-istirahat-stop').hide(); 
    $('#start-button').click(function(){
            var start = "START";
            $('#qctimbangfg-state').attr('value',start);
                    $('#qctimbangfg-start').show();
                    $('#qctimbangfg-stop').hide(); 
                    $('#qctimbangfg-istirahat-start').hide(); 
                    $('#qctimbangfg-istirahat-stop').hide();
            $('#qctimbangfg-jenis_periksa').change(function(){
                var jenis_periksa = $('#qctimbangfg-jenis_periksa').val();
                var snfg = $('#qctimbangfg-snfg').val();    
                $.get('index.php?r=qc-timbang-fg/lanjutan-qc-timbang-fg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#qctimbangfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#qctimbangfg-lanjutan').attr('value',data.lanjutan);
                            $('#qctimbangfg-lanjutan_stop').attr('value',data.lanjutan);
                            $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                     
        });

    $('#istirahat-start-button').click(function(){
            var ist_start = "ISTIRAHAT START";
            $('#qctimbangfg-state').attr('value',ist_start);
                    $('#qctimbangfg-istirahat-start').show(); 
                    $('#qctimbangfg-stop').hide(); 
                    $('#qctimbangfg-start').hide(); 
                    $('#qctimbangfg-istirahat-stop').hide(); 
            $('#qctimbangfg-jenis_periksa').change(function(){
                var jenis_periksa = $('#qctimbangfg-jenis_periksa').val();
                var snfg = $('#qctimbangfg-snfg').val();    
                $.get('index.php?r=qc-timbang-fg/lanjutan-qc-timbang-fg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#qctimbangfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#qctimbangfg-lanjutan').attr('value',data.lanjutan);
                            $('#qctimbangfg-lanjutan_stop').attr('value',data.lanjutan);
                            $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                     
        });

    $('#istirahat-stop-button').click(function(){
            var ist_stop = "ISTIRAHAT STOP";
            $('#qctimbangfg-state').attr('value',ist_stop);
                    $('#qctimbangfg-istirahat-stop').show();
                    $('#qctimbangfg-stop').hide(); 
                    $('#qctimbangfg-istirahat-start').hide(); 
                    $('#qctimbangfg-start').hide(); 
            $('#qctimbangfg-jenis_periksa').change(function(){
                var jenis_periksa = $('#qctimbangfg-jenis_periksa').val();
                var snfg = $('#qctimbangfg-snfg').val();    
                $.get('index.php?r=qc-timbang-fg/lanjutan-qc-timbang-fg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#qctimbangfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#qctimbangfg-lanjutan').attr('value',data.lanjutan);
                            $('#qctimbangfg-lanjutan_stop').attr('value',data.lanjutan);
                            $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                         
        });

    $('#stop-button').click(function(){
             var stop = "STOP";
             $('#qctimbangfg-state').attr('value',stop);
                    $('#qctimbangfg-stop').show(); 
                    $('#qctimbangfg-start').hide(); 
                    $('#qctimbangfg-istirahat-start').hide(); 
                    $('#qctimbangfg-istirahat-stop').hide();    
            });
            $('#qctimbangfg-jenis_periksa').change(function(){
                var jenis_periksa = $('#qctimbangfg-jenis_periksa').val();
                var snfg = $('#qctimbangfg-snfg').val();    
                $.get('index.php?r=qc-timbang-fg/lanjutan-qc-timbang-fg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#qctimbangfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#qctimbangfg-lanjutan').attr('value',data.lanjutan);
                            $('#qctimbangfg-lanjutan_stop').attr('value',data.lanjutan);
                            $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });               
    });


// $('#qctimbangfg-lanjutan').change(function(){
//      var lanjutan = $(this).val();
//         $('#qctimbangfg-lanjutan').attr('value',lanjutan);
//         $('#qctimbangfg-lanjutan_stop').attr('value',lanjutan);
//         $('#qctimbangfg-lanjutan_istirahat_start').attr('value',lanjutan);
//         $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',lanjutan);
//  });

// $('#qctimbangfg-lanjutan_stop').change(function(){
//      var lanjutan = $(this).val();
//         $('#qctimbangfg-lanjutan').attr('value',lanjutan);
//         $('#qctimbangfg-lanjutan_stop').attr('value',lanjutan);
//         $('#qctimbangfg-lanjutan_istirahat_start').attr('value',lanjutan);
//         $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',lanjutan);
//  });

// $('#qctimbangfg-lanjutan_istirahat_start').change(function(){
//      var lanjutan = $(this).val();
//         $('#qctimbangfg-lanjutan').attr('value',lanjutan);
//         $('#qctimbangfg-lanjutan_stop').attr('value',lanjutan);
//         $('#qctimbangfg-lanjutan_istirahat_start').attr('value',lanjutan);
//         $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',lanjutan);
//  });

// $('#qctimbangfg-lanjutan_istirahat_stop').change(function(){
//      var lanjutan = $(this).val();
//         $('#qctimbangfg-lanjutan').attr('value',lanjutan);
//         $('#qctimbangfg-lanjutan_stop').attr('value',lanjutan);
//         $('#qctimbangfg-lanjutan_istirahat_start').attr('value',lanjutan);
//         $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',lanjutan);
//  });



// $(function() {
//     $('#qctimbangfg-start').hide();
//     $('#qctimbangfg-stop').hide(); 
//     $('#qctimbangfg-istirahat-start').hide(); 
//     $('#qctimbangfg-istirahat-stop').hide(); 
//     $('#start-button').click(function(){
//             var snfg = $('#qctimbangfg-snfg').val();
//                             $.get('index.php?r=qc-timbang-fg/lanjutan-qc-timbang-fg',{ snfg : snfg },function(data){
//                                 var data = $.parseJSON(data);
//                                 //alert(data.sediaan);
//                                 $('#qctimbangfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
//                                 $('#qctimbangfg-lanjutan').attr('value',data.lanjutan);
//                                 $('#qctimbangfg-lanjutan_stop').attr('value',data.lanjutan);
//                                 $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
//                             });
//             var start = "START";
//             $('#qctimbangfg-state').attr('value',start);
//                     $('#qctimbangfg-start').show();
//                     $('#qctimbangfg-stop').hide(); 
//                     $('#qctimbangfg-istirahat-start').hide(); 
//                     $('#qctimbangfg-istirahat-stop').hide();                       
//         });

//     $('#istirahat-start-button').click(function(){
//             var snfg = $('#qctimbangfg-snfg').val();
//                             $.get('index.php?r=qc-timbang-fg/lanjutan-qc-timbang-fg',{ snfg : snfg },function(data){
//                                 var data = $.parseJSON(data);
//                                 //alert(data.sediaan);
//                                 $('#qctimbangfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
//                                 $('#qctimbangfg-lanjutan').attr('value',data.lanjutan);
//                                 $('#qctimbangfg-lanjutan_stop').attr('value',data.lanjutan);
//                                 $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
//                             });
//             var ist_start = "ISTIRAHAT START";
//             $('#qctimbangfg-state').attr('value',ist_start);
//                     $('#qctimbangfg-istirahat-start').show(); 
//                     $('#qctimbangfg-stop').hide(); 
//                     $('#qctimbangfg-start').hide(); 
//                     $('#qctimbangfg-istirahat-stop').hide(); 
//         });

//     $('#istirahat-stop-button').click(function(){
//             var snfg = $('#qctimbangfg-snfg').val();
//                             $.get('index.php?r=qc-timbang-fg/lanjutan-qc-timbang-fg',{ snfg : snfg },function(data){
//                                 var data = $.parseJSON(data);
//                                 //alert(data.sediaan);
//                                 $('#qctimbangfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
//                                 $('#qctimbangfg-lanjutan').attr('value',data.lanjutan);
//                                 $('#qctimbangfg-lanjutan_stop').attr('value',data.lanjutan);
//                                 $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
//                             });
//             var ist_stop = "ISTIRAHAT STOP";
//             $('#qctimbangfg-state').attr('value',ist_stop);
//                     $('#qctimbangfg-istirahat-stop').show();
//                     $('#qctimbangfg-stop').hide(); 
//                     $('#qctimbangfg-istirahat-start').hide(); 
//                     $('#qctimbangfg-start').hide(); 
//         });

//     $('#stop-button').click(function(){
//             var snfg = $('#qctimbangfg-snfg').val();
//                             $.get('index.php?r=qc-timbang-fg/lanjutan-qc-timbang-fg',{ snfg : snfg },function(data){
//                                 var data = $.parseJSON(data);
//                                 //alert(data.sediaan);
//                                 $('#qctimbangfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
//                                 $('#qctimbangfg-lanjutan').attr('value',data.lanjutan);
//                                 $('#qctimbangfg-lanjutan_stop').attr('value',data.lanjutan);
//                                 $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
//                             });
//             var stop = "STOP";
//             $('#qctimbangfg-state').attr('value',stop);
//                     $('#qctimbangfg-stop').show(); 
//                     $('#qctimbangfg-start').hide(); 
//                     $('#qctimbangfg-istirahat-start').hide(); 
//                     $('#qctimbangfg-istirahat-stop').hide();    
//         });

//     });

JS;
$this->registerJs($script);
?>