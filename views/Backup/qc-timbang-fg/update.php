<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\QcTimbangFg */

$this->title = 'Update Qc Timbang Fg: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Qc Timbang Fgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="qc-timbang-fg-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
