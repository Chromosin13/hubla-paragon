<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ScmPlanner;
use yii\widgets\Pjax; 
// Menggunakan Pjax untuk Autorefresh
use yii\helpers\Url;
use faryshta\widgets\JqueryTagsInput;
/* @var $this yii\web\View */
/* @var $model app\models\Penimbangan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="penimbangan-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Input dan Informasi SNFG dari Planner</h2>
                <?= $form->field($model, 'snfg_komponen')->textInput() ?>

                <?= $form->field($model, 'snfg')->textInput(['readonly' => 'true']) ?>
                <b>Streamline</b>
                <input type="text" class="form-control" id="streamline" disabled>
                <b>Start</b>
                <input type="text" class="form-control" id="start" placeholder="" disabled>
                <b>Due</b>
                <input type="text" class="form-control" id="due" placeholder="" disabled>
                <b>Nama Bulk</b>
                <input type="text" class="form-control" id="nama-bulk" placeholder="" disabled>
                <b>Nama FG</b>
                <input type="text" class="form-control" id="nama-fg" placeholder="" disabled>
                <b>Besar Batch</b>
                <input type="text" class="form-control" id="besar-batch" placeholder="" disabled>
                <b>Kode Jadwal</b>
                <input type="text" class="form-control" id="kode-jadwal" placeholder="" disabled>
            </div>
    </div>

    <!-- Button untuk change event START, STOP, ISTIRAHAT START, ISTIRAHAT STOP -->

    <a class="btn btn-app" id="start-button">
                <i class="fa fa-play"></i> Start
    </a>

    <a class="btn btn-app" id="stop-button">
                <i class="fa fa-pause"></i> Stop
    </a>

    <a class="btn btn-app" id="istirahat-start-button">
                <i class="fa fa-play"></i> Istirahat Start
    </a>

    <a class="btn btn-app" id="istirahat-stop-button">
                <i class="fa fa-pause"></i> Istirahat Stop
    </a>


    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>

    <!-- <?= $form->field($model, 'state')->textInput(['readonly' => $model->isNewRecord]) ?> -->
    <?= $form->field($model, 'jenis_penimbangan')->dropDownList(['JADWAL BARU' => 'JADWAL BARU','REWORK' => 'REWORK'],['prompt'=>'Select Option']); ?>

    <div class="box" id="penimbangan-start">
            <div class="box-header with-border">
              <h2 class="box-title">Start Entry</h2>

                <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>

                <?= $form->field($model, 'nama_line')->textInput() ?>

                <?= $form->field($model, 'jumlah_operator')->textInput() ?>

                <?php echo $form->field($model, 'nama_operator')->widget(JqueryTagsInput::className([]));
                ?>
                
            </div>
    </div>

    <div class="box" id="penimbangan-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Stop Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'penimbangan-lanjutan_stop','readonly' => 'true']) ?> 
            </div>
    </div>

    <div class="box" id="penimbangan-istirahat-start">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Start Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'penimbangan-lanjutan_istirahat_start','readonly' => 'true']) ?>
            </div>
    </div>

    <div class="box" id="penimbangan-istirahat-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Stop Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'penimbangan-lanjutan_istirahat_stop','readonly' => 'true']) ?>
            </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
$('#penimbangan-snfg_komponen').change(function(){
    var snfg_komponen = $(this).val();
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#penimbangan-snfg').attr('value',data.snfg);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
        $('#besar-batch').attr('value',data.besar_batch);
        $('#kode-jadwal').attr('value',data.kode_jadwal);

    });
});

$(function() {
    $('#penimbangan-start').hide();
    $('#penimbangan-stop').hide(); 
    $('#penimbangan-istirahat-start').hide(); 
    $('#penimbangan-istirahat-stop').hide(); 
    $('#start-button').click(function(){
            var start = "START";
            $('#penimbangan-state').attr('value',start);
                    $('#penimbangan-start').show();
                    $('#penimbangan-stop').hide(); 
                    $('#penimbangan-istirahat-start').hide(); 
                    $('#penimbangan-istirahat-stop').hide();   
            $('#penimbangan-jenis_penimbangan').change(function(){
                var jenis_penimbangan = $('#penimbangan-jenis_penimbangan').val();
                var snfg_komponen = $('#penimbangan-snfg_komponen').val(); 
                    $.get('index.php?r=penimbangan/get-penimbangan',{ snfg_komponen : snfg_komponen, jenis_penimbangan : jenis_penimbangan },function(data){
                        var data = $.parseJSON(data);
                        //alert(data.sediaan);
                        $('#penimbangan-lanjutan_istirahat_start').attr('value',data.lanjutan);
                        $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                        $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);
                        $('#penimbangan-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                    }); 
            });                               
        });

    $('#istirahat-start-button').click(function(){
            var ist_start = "ISTIRAHAT START";
            $('#penimbangan-state').attr('value',ist_start);
                     $('#penimbangan-istirahat-start').show(); 
                    $('#penimbangan-stop').hide(); 
                    $('#penimbangan-start').hide(); 
                    $('#penimbangan-istirahat-stop').hide(); 
             $('#penimbangan-jenis_penimbangan').change(function(){
                var jenis_penimbangan = $('#penimbangan-jenis_penimbangan').val();
                var snfg_komponen = $('#penimbangan-snfg_komponen').val(); 
                    $.get('index.php?r=penimbangan/get-penimbangan',{ snfg_komponen : snfg_komponen, jenis_penimbangan : jenis_penimbangan },function(data){
                        var data = $.parseJSON(data);
                        //alert(data.sediaan);
                        $('#penimbangan-lanjutan_istirahat_start').attr('value',data.lanjutan);
                        $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                        $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);
                        $('#penimbangan-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                    }); 
            });          
        });

    $('#istirahat-stop-button').click(function(){
            var ist_stop = "ISTIRAHAT STOP";
            $('#penimbangan-state').attr('value',ist_stop);
                    $('#penimbangan-istirahat-stop').show();
                    $('#penimbangan-stop').hide(); 
                    $('#penimbangan-istirahat-start').hide(); 
                    $('#penimbangan-start').hide(); 
             $('#penimbangan-jenis_penimbangan').change(function(){
                var jenis_penimbangan = $('#penimbangan-jenis_penimbangan').val();
                var snfg_komponen = $('#penimbangan-snfg_komponen').val(); 
                    $.get('index.php?r=penimbangan/get-penimbangan',{ snfg_komponen : snfg_komponen, jenis_penimbangan : jenis_penimbangan },function(data){
                        var data = $.parseJSON(data);
                        //alert(data.sediaan);
                        $('#penimbangan-lanjutan_istirahat_start').attr('value',data.lanjutan);
                        $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                        $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);
                        $('#penimbangan-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                    }); 
            });          
        });

    $('#stop-button').click(function(){
            var stop = "STOP";
            $('#penimbangan-state').attr('value',stop);
                    $('#penimbangan-stop').show(); 
                    $('#penimbangan-start').hide(); 
                    $('#penimbangan-istirahat-start').hide(); 
                    $('#penimbangan-istirahat-stop').hide();
             $('#penimbangan-jenis_penimbangan').change(function(){
                var jenis_penimbangan = $('#penimbangan-jenis_penimbangan').val();
                var snfg_komponen = $('#penimbangan-snfg_komponen').val(); 
                    $.get('index.php?r=penimbangan/get-penimbangan',{ snfg_komponen : snfg_komponen, jenis_penimbangan : jenis_penimbangan },function(data){
                        var data = $.parseJSON(data);
                        //alert(data.sediaan);
                        $('#penimbangan-lanjutan_istirahat_start').attr('value',data.lanjutan);
                        $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                        $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);
                        $('#penimbangan-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                    }); 
            });              
        });

    });

JS;
$this->registerJs($script);
?>