<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PenimbanganSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="penimbangan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'nama_line') ?>

    <?= $form->field($model, 'jumlah_operator') ?>

    <?= $form->field($model, 'nama_operator_1') ?>

    <?php echo $form->field($model, 'nama_operator_2') ?>

    <?php echo $form->field($model, 'waktu') ?>

    <?php echo $form->field($model, 'state') ?>

    <?php echo $form->field($model, 'lanjutan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
