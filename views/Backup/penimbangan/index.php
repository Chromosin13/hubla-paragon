<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PenimbanganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penimbangan';
$this->params['breadcrumbs'][] = $this->title;
?>
    <p>
        <?= Html::a('Input Penimbangan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<div class="penimbangan-index">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'snfg',
            'snfg_komponen',
            'nama_line',
            'jumlah_operator',
            'nama_operator',
            'waktu',
            'state',
            'lanjutan',
            'jenis_penimbangan',
            'timestamp',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
