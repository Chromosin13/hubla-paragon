<?php
$script = <<< JS
$('input').keydown( function (event) { //event==Keyevent
    if(event.which == 13) {
        var inputs = $(this).closest('form').find(':input:visible');
        inputs.eq( inputs.index(this)+ 1 ).focus();
        event.preventDefault(); //Disable standard Enterkey action
    }
    // event.preventDefault(); <- Disable all keys  action
});
JS;
$this->registerJs($script);
?>


<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pengolahan */

$this->title = 'Create Pengolahan';
$this->params['breadcrumbs'][] = ['label' => 'Pengolahans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengolahan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
