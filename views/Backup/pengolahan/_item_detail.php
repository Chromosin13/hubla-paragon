
<?php

use faryshta\widgets\JqueryTagsInput;
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ScmPlanner;
use yii\widgets\Pjax; 
// Menggunakan Pjax untuk Autorefresh
use yii\helpers\Url;
use unclead\widgets\MultipleInput;
use unclead\widgets\MultipleInputColumn;
//use unclead\widgets\TabularInput;
use yii\base\Model;
use mdm\widgets\TabularInput;
use mdm\widgets\GridInput;
use app\models\Pengolahan;
use app\models\PengolahanSearch;

/* @var $this yii\web\View */
/* @var $model app\models\Pengolahan */
/* @var $form yii\widgets\ActiveForm */
?>

<td class="col-lg-2">
    <?= Html::activeTextInput($model, "[$key]pengolahan_id", ['class'=>'form-control','required' => true]) ?>
</td>
<td class="col-lg-2">
    <?= Html::activeTextInput($model, "[$key]nobatch", ['class'=>'form-control','required' => true]) ?>
</td>
<td class="col-lg-2">
    <?= Html::activeTextInput($model, "[$key]besar_batch_real", ['class'=>'form-control','required' => true]) ?>
</td>
<td  class="col-lg-1" style="text-align: center">
    <a data-action="delete" title="Delete" href="#"><span class="glyphicon glyphicon-trash"></span></a>
</td>