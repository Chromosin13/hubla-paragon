<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use unclead\widgets\MultipleInput;
use unclead\widgets\MultipleInputColumn;
use unclead\widgets\TabularInput;

/* @var $this yii\web\View */
/* @var $model app\models\FlowReport */
/* @var $form yii\widgets\ActiveForm */
?>


 <div class="flow-report-form">

    

     <?php $form = ActiveForm::begin();?>

    <!-- <?= $form->field($model, 'nosmb')->widget(MultipleInput::className(), [
        'limit' => 4,
     ]);
    ?> -->
    <?php
    echo $form->field($model, 'nosmb')->widget(MultipleInput::className(), [
        'limit'             => 6,
        'allowEmptyList'    => true,
        'enableGuessTitle'  => true,
        'min'               => 1, // should be at least 2 rows
        'addButtonPosition' => MultipleInput::POS_HEADER // show add button in the header
    ])
    ->label(false);
    ?>

    <?= Html::submitButton('Create', ['class' => 'btn btn-success']);?>
    <?php ActiveForm::end();?> 

</div>
