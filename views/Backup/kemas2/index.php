<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Kemas2Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kemas 2';
$this->params['breadcrumbs'][] = $this->title;
?>
    <p>
        <?= Html::a('Input Kemas 2', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<div class="kemas2-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'snfg',
            'jumlah_plan',
            'jumlah_realisasi',
            'nama_line',
            'nama_operator',
            'waktu',
            'state',
            'id',
            'nobatch',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
