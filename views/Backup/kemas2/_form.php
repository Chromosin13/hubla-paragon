<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use faryshta\widgets\JqueryTagsInput;

/* @var $this yii\web\View */
/* @var $model app\models\kemas2 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kemas2-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Input dan Informasi SNFG dari Planner</h2>
                <?= $form->field($model, 'snfg')->textInput() ?>
                <b>Streamline</b>
                <input type="text" class="form-control" id="streamline" disabled>
                <b>Start</b>
                <input type="text" class="form-control" id="start" placeholder="" disabled>
                <b>Due</b>
                <input type="text" class="form-control" id="due" placeholder="" disabled>
                <b>Nama Bulk</b>
                <input type="text" class="form-control" id="nama-bulk" placeholder="" disabled>
                <b>Nama FG</b>
                <input type="text" class="form-control" id="nama-fg" placeholder="" disabled>
                <b>Besar Batch</b>
                <input type="text" class="form-control" id="besar-batch" placeholder="" disabled>
                <b>Besar per lot (Kg)</b>
                <input type="text" class="form-control" id="besar-lot" placeholder="" disabled>
                <b>Lot ke-</b>
                <input type="text" class="form-control" id="lot" placeholder="" disabled>
                <b>Kode Jadwal</b>
                <input type="text" class="form-control" id="kode-jadwal" placeholder="" disabled>
                <b>No Batch</b>
                <input type="text" class="form-control" id="no-batch" placeholder="" disabled>
                <b>Besar Batch Real</b>
                <input type="text" class="form-control" id="besar-batch-real" placeholder="" disabled>
                <b>Jumlah Pc(s)</b>
                <input type="text" class="form-control" id="jumlah" placeholder="" disabled>
            </div>
    </div>


    <!-- Button untuk change event START, STOP, ISTIRAHAT START, ISTIRAHAT STOP -->

    <a class="btn btn-app" id="start-button">
                <i class="fa fa-play"></i> Start
    </a>

    <a class="btn btn-app" id="stop-button">
                <i class="fa fa-pause"></i> Stop
    </a>

    <a class="btn btn-app" id="istirahat-start-button">
                <i class="fa fa-play"></i> Istirahat Start
    </a>

    <a class="btn btn-app" id="istirahat-stop-button">
                <i class="fa fa-pause"></i> Istirahat Stop
    </a>

    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>

    <div class="box" id="kemas2-start">
            <div class="box-header with-border">
              <h2 class="box-title">Start Entry</h2>

                <?= $form->field($model, 'jenis_kemas')->dropDownList(['PACKING' => 'PACKING'],['prompt'=>'Select Option']); ?>

                <?= $form->field($model, 'nama_line')->textInput() ?>

                <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>

                <?= $form->field($model, 'jumlah_operator')->textInput() ?>

                <?php echo $form->field($model, 'nama_operator')->widget(JqueryTagsInput::className([]));
                ?>
                
            </div>
    </div>

    <div class="box" id="kemas2-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Stop Entry</h2>
                <?= $form->field($model, 'lanjutan')->textInput(['id'=>'kemas2-lanjutan_stop','readonly' => 'true']) ?>

                <?= $form->field($model, 'nobatch')->textInput() ?>

                <?= $form->field($model, 'jumlah_realisasi')->textInput() ?>

            </div>
    </div>

    <div class="box" id="kemas2-istirahat-start">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Start Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'kemas2-lanjutan_istirahat_start','readonly' => 'true']) ?> 
            </div>
    </div>

    <div class="box" id="kemas2-istirahat-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Stop Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'kemas2-lanjutan_istirahat_stop','readonly' => 'true']) ?>
            </div>
    </div>    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
$('#kemas2-snfg').change(function(){
    var snfg = $(this).val();
    $.get('index.php?r=scm-planner/get-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
        $('#besar-batch').attr('value',data.besar_batch);
        $('#besar-lot').attr('value',data.besar_lot);
        $('#lot').attr('value',data.lot);
        $('#kode-jadwal').attr('value',data.kode_jadwal);
        $('#jumlah').attr('value',data.jumlah);
    });
    $.get('index.php?r=pengolahan/get-pengolahan',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#no-batch').attr('value',data.nobatch);
        $('#besar-batch-real').attr('value',data.besar_batch_real);
    });
});



$(function() {
    $('#kemas2-start').hide();
    $('#kemas2-stop').hide(); 
    $('#kemas2-istirahat-start').hide(); 
    $('#kemas2-istirahat-stop').hide(); 
    $('#start-button').click(function(){
            var start = "START";
            $('#kemas2-state').attr('value',start);
                    $('#kemas2-start').show();
                    $('#kemas2-stop').hide(); 
                    $('#kemas2-istirahat-start').hide(); 
                    $('#kemas2-istirahat-stop').hide();
            $('#kemas2-jenis_kemas').change(function(){
                var jenis_kemas = $('#kemas2-jenis_kemas').val();
                var snfg = $('#kemas2-snfg').val();    
                $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                     
        });

    $('#istirahat-start-button').click(function(){
            var ist_start = "ISTIRAHAT START";
            $('#kemas2-state').attr('value',ist_start);
                    $('#kemas2-istirahat-start').show(); 
                    $('#kemas2-stop').hide(); 
                    $('#kemas2-start').hide(); 
                    $('#kemas2-istirahat-stop').hide(); 
            $('#kemas2-jenis_kemas').change(function(){
                var jenis_kemas = $('#kemas2-jenis_kemas').val();
                var snfg = $('#kemas2-snfg').val();    
                $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                     
        });

    $('#istirahat-stop-button').click(function(){
            var ist_stop = "ISTIRAHAT STOP";
            $('#kemas2-state').attr('value',ist_stop);
                    $('#kemas2-istirahat-stop').show();
                    $('#kemas2-stop').hide(); 
                    $('#kemas2-istirahat-start').hide(); 
                    $('#kemas2-start').hide(); 
            $('#kemas2-jenis_kemas').change(function(){
                var jenis_kemas = $('#kemas2-jenis_kemas').val();
                var snfg = $('#kemas2-snfg').val();    
                $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                         
        });

    $('#stop-button').click(function(){
             var stop = "STOP";
             $('#kemas2-state').attr('value',stop);
                    $('#kemas2-stop').show(); 
                    $('#kemas2-start').hide(); 
                    $('#kemas2-istirahat-start').hide(); 
                    $('#kemas2-istirahat-stop').hide();    
            });
            $('#kemas2-jenis_kemas').change(function(){
                var jenis_kemas = $('#kemas2-jenis_kemas').val();
                var snfg = $('#kemas2-snfg').val();    
                $.get('index.php?r=kemas2/lanjutan-kemas2',{ snfg : snfg, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#kemas2-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_stop').attr('value',data.lanjutan);
                            $('#kemas2-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });               
    });
    
JS;
$this->registerJs($script);
?>