<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScmPlannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Supply Chain Management - Planner';
$this->params['breadcrumbs'][] = $this->title;
?>

    <p>
        <?= Html::a('Input Planner Data', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


<div class="scm-planner-index" style="overflow-x:auto;height:1000px;">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'snfg',
            'snfg_komponen',
            'sediaan',
            'streamline',
            'line_timbang',
            'line_olah',
            'line_olah_2',
            'line_press',
            'line_kemas_1',
            'line_kemas_2',
            'start',
            'due',
            'leadtime',
            'kode_jadwal',
            'koitem_bulk',
            'koitem_fg',
            'nama_bulk',
            'nama_fg',
            'besar_batch',
            'besar_lot',
            'lot',
            'tglpermintaan',
            'jumlah',
            'keterangan',
            'kategori_sop',
            'kategori_detail',
            'nobatch',
            'status',
            'line_olah_premix',
            'line_adjust_olah_1',
            'line_adjust_olah_2',
            'line_ayak',
            'line_press_filling_packing',
            'line_filling',
            'line_filling_packing_inline',
            'line_flame_packing_inline',
            'line_packing',
            'snfg_induk',
            'kode_sl',
            'alokasi',
            'jumlah_press',
            'week',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
