<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\data\SqlDataProvider;
/* @var $this yii\web\View */
/* @var $model app\models\ScmPlanner */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="scm-planner-form">

    

    <div class="col-md-4">
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Form Planner</h3>

            <?= 
                DatePicker::widget([
                'model' => $model,
                'form' => $form,
                'name' => 'start',
                'attribute' => 'start', 
                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                'options' => ['placeholder' => 'Select issue date ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                    ]
                ]);
            ?>

            <?= 
                DatePicker::widget([
                'model' => $model,
                'form' => $form,
                'name' => 'due',
                'attribute' => 'due', 
                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                'options' => ['placeholder' => 'Select issue date ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                    ]
                ]);
            ?>
            <?= 
                DatePicker::widget([
                'model' => $model,
                'form' => $form,
                'name' => 'tglpermintaan',
                'attribute' => 'tglpermintaan', 
                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                'options' => ['placeholder' => 'Select issue date ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                    ]
                ]);
            ?>

            <?= $form->field($model, 'sediaan')->textInput() ?>

            <?= $form->field($model, 'streamline')->textInput() ?>

            <?= $form->field($model, 'line_timbang')->textInput() ?>

            <?= $form->field($model, 'line_olah')->textInput() ?>

            <?= $form->field($model, 'line_olah_2')->textInput() ?>

            <?= $form->field($model, 'line_press')->textInput() ?>

            <?= $form->field($model, 'line_kemas_1')->textInput() ?>

            <?= $form->field($model, 'line_kemas_2')->textInput() ?>

            <?= $form->field($model, 'leadtime')->textInput() ?>

            <?= $form->field($model, 'kode_jadwal')->textInput() ?>
    
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Form Planner</h3>

            <?= $form->field($model, 'week')->textInput() ?>

            <?= $form->field($model, 'line_olah_premix')->textInput() ?>

            <?= $form->field($model, 'line_adjust_olah_1')->textInput() ?>

            <?= $form->field($model, 'line_adjust_olah_2')->textInput() ?>

            <?= $form->field($model, 'line_ayak')->textInput() ?>

            <?= $form->field($model, 'line_press_filling_packing')->textInput() ?>

            <?= $form->field($model, 'line_filling')->textInput() ?>

            <?= $form->field($model, 'line_filling_packing_inline')->textInput() ?>

            <?= $form->field($model, 'line_flame_packing_inline')->textInput() ?>

            <?= $form->field($model, 'line_packing')->textInput() ?>
    
            <?= $form->field($model, 'snfg_induk')->textInput() ?>
    
            <?= $form->field($model, 'kode_sl')->textInput() ?>
    
            <?= $form->field($model, 'jumlah_press')->textInput() ?>
    
            <?= $form->field($model, 'alokasi')->textInput() ?>
    
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Form Planner</h3>
                <?= $form->field($model, 'snfg_komponen')->textInput(['readonly'=>'true']) ?>

                <?= $form->field($model, 'snfg')->textInput(['readonly'=>'true']) ?>

                <?= $form->field($model, 'koitem_bulk')->textInput() ?>

                <?= $form->field($model, 'koitem_fg')->textInput() ?>

                <?= $form->field($model, 'nama_bulk')->textInput() ?>

                <?= $form->field($model, 'nama_fg')->textInput() ?>

                <?= $form->field($model, 'besar_batch')->textInput() ?>

                <?= $form->field($model, 'besar_lot')->textInput() ?>

                <?= $form->field($model, 'lot')->textInput() ?>

                <?= $form->field($model, 'jumlah')->textInput() ?>

                <?= $form->field($model, 'keterangan')->textInput() ?>

                <?= $form->field($model, 'kategori_sop')->textInput() ?>

                <?= $form->field($model, 'kategori_detail')->textInput() ?>

                <?= $form->field($model, 'nobatch')->textInput() ?>

                <?php
                $lokasi_status = Yii::$app->db->createCommand("
                    SELECT posisi from posisi_proses_byid where snfg='".$model->snfg_komponen."'
                    ")->queryScalar();
                
                ?>
                <b>Lokasi Barang Terakhir</b>
                <input type="text" class="form-control" id="lokasi_status" value="<?php echo $lokasi_status ?>" disabled>
                
                <?php
                
                                                        if($model->status == 'HOLD') 
                                                        {
                                                            echo $form->field($model, 'status')->dropDownList(['HOLD'=>'HOLD','UNHOLD' => 'UNHOLD'],['prompt'=>'Select Option']);
                                                        }else if($model->status == 'UNHOLD')
                                                        {
                                                            echo $form->field($model, 'status')->dropDownList(['UNHOLD'=>'UNHOLD','PAUSE' => 'PAUSE'],['prompt'=>'Select Option']);
                                                        }else if($model->status == 'PAUSE' && $lokasi_status == 'PLANNER')
                                                        {
                                                             echo $form->field($model, 'status')->dropDownList(['PAUSE'=>'PAUSE','UNHOLD' => 'UNHOLD'],['prompt'=>'Select Option']);
                                                        }else if($model->status == 'PAUSE' && $lokasi_status != 'PLANNER')
                                                        {
                                                             echo $form->field($model, 'status')->dropDownList(['PAUSE'=>'PAUSE','CONTINUE' => 'CONTINUE'],['prompt'=>'Select Option']);
                                                        }else if($model->status == 'CONTINUE')
                                                        {
                                                             echo $form->field($model, 'status')->dropDownList(['CONTINUE' => 'CONTINUE','PAUSE'=>'PAUSE'],['prompt'=>'Select Option']);
                                                        }
                ?>

               <!--  <?= $form->field($model, 'status')->dropDownList(['PAUSE' => 'PAUSE','CONTINUE' => 'CONTINUE','UNHOLD' => 'UNHOLD'],['prompt'=>'Select Option']); ?> -->

                <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

</div>



<?php
$script = <<< JS
$('.datepicker').datepicker({
    format: 'mm/dd/yyyy',
    startDate: '-3d'
});

JS;
$this->registerJs($script);
?>