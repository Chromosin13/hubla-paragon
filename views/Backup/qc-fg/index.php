<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QcFgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'QC Finish Good';
$this->params['breadcrumbs'][] = $this->title;
?>

    <p>
        <?= Html::a('Input QC FG', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<div class="qc-fg-index"  style="overflow-x:auto;height:1000px;" >

    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'snfg',
            'nama_inspektor',
            'state',
            'aql',
            'filling_kesesuaian_bulk',
            'filling_kesesuaian_packaging_primer',
            'filling_netto',
            'filling_seal',
            'filling_leakage',
            'filling_warna_olesan',
            'filling_warna_performance',
            'filling_uji_ayun',
            'filling_uji_oles',
            'filling_uji_ketrok',
            'filling_drop_test',
            'filling_rub_test',
            'filling_identitas_packaging_primer',
            'filling_identitas_stc_bottom',
            'packing_kesesuaian_packaging_sekunder',
            'packing_qty_inner_box',
            'packing_identitas_unit_box',
            'packing_identitas_inner_box',
            'packing_performance_segel',
            'packing_posisi_packing',
            'paletting_qty_karton_box',
            'paletting_identitas_karton_box',
            'status',
            'waktu',
            'jumlah_inspektor',
            'jenis_periksa',
            'lanjutan',
            'nama_line',
            'retain_sample',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
