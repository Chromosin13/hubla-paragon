<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use faryshta\widgets\JqueryTagsInput;

/* @var $this yii\web\View */
/* @var $model app\models\QcFg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qc-fg-form">

    <?php $form = ActiveForm::begin(); ?>

     <div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Input dan Informasi SNFG dari Planner</h2>
                <?= $form->field($model, 'snfg')->textInput() ?>
                <b>Streamline</b>
                <input type="text" class="form-control" id="streamline" disabled>
                <b>Start</b>
                <input type="text" class="form-control" id="start" placeholder="" disabled>
                <b>Due</b>
                <input type="text" class="form-control" id="due" placeholder="" disabled>
                <b>Nama Bulk</b>
                <input type="text" class="form-control" id="nama-bulk" placeholder="" disabled>
                <b>Nama FG</b>
                <input type="text" class="form-control" id="nama-fg" placeholder="" disabled>
                <b>No Batch</b>
                <input type="text" class="form-control" id="no-batch" placeholder="" disabled>
                <b>Jumlah Pc(s)</b>
                <input type="text" class="form-control" id="jumlah" placeholder="" disabled>
            </div>
    </div>


    <a class="btn btn-app" id="start-button">
                <i class="fa fa-play"></i> Start
    </a>

    <a class="btn btn-app" id="stop-button">
                <i class="fa fa-pause"></i> Stop
    </a>

    <a class="btn btn-app" id="istirahat-start-button">
                <i class="fa fa-play"></i> Istirahat Start
    </a>

    <a class="btn btn-app" id="istirahat-stop-button">
                <i class="fa fa-pause"></i> Istirahat Stop
    </a>

    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'jenis_periksa')->dropDownList(['JADWAL BARU' => 'JADWAL BARU','REWORK' => 'REWORK','ADJUST' => 'ADJUST','PENDING' => 'PENDING'],['prompt'=>'Select Option']); ?>

    <div class="box" id="qcfg-start">
            <div class="box-header with-border">
              <h2 class="box-title">Start Entry</h2>

                

                <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>

                <!-- <?= $form->field($model, 'nama_line')->textInput() ?> -->

                <?= $form->field($model, 'jumlah_inspektor')->textInput() ?>

                <?php echo $form->field($model, 'nama_inspektor')->widget(JqueryTagsInput::className([]));
                ?>
                
            </div>
    </div>

    <div class="box" id="qcfg-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Stop Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'qcfg-lanjutan_stop','readonly' => 'true']) ?> 

                 <?= $form->field($model, 'aql')->textInput() ?>

                <?= $form->field($model, 'filling_kesesuaian_bulk')->textInput() ?>

                <?= $form->field($model, 'filling_kesesuaian_packaging_primer')->textInput() ?>

                <?= $form->field($model, 'filling_netto')->textInput() ?>

                <?= $form->field($model, 'filling_seal')->textInput() ?>

                <?= $form->field($model, 'filling_leakage')->textInput() ?>

                <?= $form->field($model, 'filling_warna_olesan')->textInput() ?>

                <?= $form->field($model, 'filling_warna_performance')->textInput() ?>

                <?= $form->field($model, 'filling_uji_ayun')->textInput() ?>

                <?= $form->field($model, 'filling_uji_oles')->textInput() ?>

                <?= $form->field($model, 'filling_uji_ketrok')->textInput() ?>

                <?= $form->field($model, 'filling_drop_test')->textInput() ?>

                <?= $form->field($model, 'filling_rub_test')->textInput() ?>

                <?= $form->field($model, 'filling_identitas_packaging_primer')->textInput() ?>

                <?= $form->field($model, 'filling_identitas_stc_bottom')->textInput() ?>

                <?= $form->field($model, 'packing_kesesuaian_packaging_sekunder')->textInput() ?>

                <?= $form->field($model, 'packing_qty_inner_box')->textInput() ?>

                <?= $form->field($model, 'packing_identitas_unit_box')->textInput() ?>

                <?= $form->field($model, 'packing_identitas_inner_box')->textInput() ?>

                <?= $form->field($model, 'packing_performance_segel')->textInput() ?>

                <?= $form->field($model, 'packing_posisi_packing')->textInput() ?>

                <?= $form->field($model, 'paletting_qty_karton_box')->textInput() ?>

                <?= $form->field($model, 'paletting_identitas_karton_box')->textInput() ?>

                <?= $form->field($model, 'retain_sample')->textInput() ?>

                <?= $form->field($model, 'status')->dropDownList(['RELEASE' => 'RELEASE','REWORK' => 'REWORK','CEK_MIKRO' => 'CEK_MIKRO','PENDING' => 'PENDING','REJECT' => 'REJECT'],['prompt'=>'Select Option']); ?>
            </div>
    </div>

    <div class="box" id="qcfg-istirahat-start">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Start Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'qcfg-lanjutan_istirahat_start','readonly' => 'true']) ?> 
            </div>
    </div>

    <div class="box" id="qcfg-istirahat-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Stop Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'qcfg-lanjutan_istirahat_stop','readonly' => 'true']) ?>
            </div>
    </div>    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
$('#qcfg-snfg').change(function(){
    var snfg = $(this).val();
    $.get('index.php?r=scm-planner/get-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
        $('#jumlah').attr('value',data.jumlah);
    });
    $.get('index.php?r=pengolahan/get-pengolahan',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#no-batch').attr('value',data.nobatch);
    });
});

// $('#qcfg-lanjutan').change(function(){
//      var lanjutan = $(this).val();
//         $('#qcfg-lanjutan').attr('value',lanjutan);
//         $('#qcfg-lanjutan_stop').attr('value',lanjutan);
//         $('#qcfg-lanjutan_istirahat_start').attr('value',lanjutan);
//         $('#qcfg-lanjutan_istirahat_stop').attr('value',lanjutan);
//  });

// $('#qcfg-lanjutan_stop').change(function(){
//      var lanjutan = $(this).val();
//         $('#qcfg-lanjutan').attr('value',lanjutan);
//         $('#qcfg-lanjutan_stop').attr('value',lanjutan);
//         $('#qcfg-lanjutan_istirahat_start').attr('value',lanjutan);
//         $('#qcfg-lanjutan_istirahat_stop').attr('value',lanjutan);
//  });

// $('#qcfg-lanjutan_istirahat_start').change(function(){
//      var lanjutan = $(this).val();
//         $('#qcfg-lanjutan').attr('value',lanjutan);
//         $('#qcfg-lanjutan_stop').attr('value',lanjutan);
//         $('#qcfg-lanjutan_istirahat_start').attr('value',lanjutan);
//         $('#qcfg-lanjutan_istirahat_stop').attr('value',lanjutan);
//  });

// $('#qcfg-lanjutan_istirahat_stop').change(function(){
//      var lanjutan = $(this).val();
//         $('#qcfg-lanjutan').attr('value',lanjutan);
//         $('#qcfg-lanjutan_stop').attr('value',lanjutan);
//         $('#qcfg-lanjutan_istirahat_start').attr('value',lanjutan);
//         $('#qcfg-lanjutan_istirahat_stop').attr('value',lanjutan);
//  });



$(function() {
    $('#qcfg-start').hide();
    $('#qcfg-stop').hide(); 
    $('#qcfg-istirahat-start').hide(); 
    $('#qcfg-istirahat-stop').hide(); 
    $('#start-button').click(function(){
            var start = "START";
            $('#qcfg-state').attr('value',start);
                    $('#qcfg-start').show();
                    $('#qcfg-stop').hide(); 
                    $('#qcfg-istirahat-start').hide(); 
                    $('#qcfg-istirahat-stop').hide();
            $('#qcfg-jenis_periksa').change(function(){
                var jenis_periksa = $('#qcfg-jenis_periksa').val();
                var snfg = $('#qcfg-snfg').val();    
                $.get('index.php?r=qc-fg/lanjutan-qc-fg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#qcfg-lanjutan').attr('value',data.lanjutan);
                            $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                            $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                     
        });

    $('#istirahat-start-button').click(function(){
            var ist_start = "ISTIRAHAT START";
            $('#qcfg-state').attr('value',ist_start);
                    $('#qcfg-istirahat-start').show(); 
                    $('#qcfg-stop').hide(); 
                    $('#qcfg-start').hide(); 
                    $('#qcfg-istirahat-stop').hide(); 
            $('#qcfg-jenis_periksa').change(function(){
                var jenis_periksa = $('#qcfg-jenis_periksa').val();
                var snfg = $('#qcfg-snfg').val();    
                $.get('index.php?r=qc-fg/lanjutan-qc-fg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#qcfg-lanjutan').attr('value',data.lanjutan);
                            $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                            $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                     
        });

    $('#istirahat-stop-button').click(function(){
            var ist_stop = "ISTIRAHAT STOP";
            $('#qcfg-state').attr('value',ist_stop);
                    $('#qcfg-istirahat-stop').show();
                    $('#qcfg-stop').hide(); 
                    $('#qcfg-istirahat-start').hide(); 
                    $('#qcfg-start').hide(); 
            $('#qcfg-jenis_periksa').change(function(){
                var jenis_periksa = $('#qcfg-jenis_periksa').val();
                var snfg = $('#qcfg-snfg').val();    
                $.get('index.php?r=qc-fg/lanjutan-qc-fg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#qcfg-lanjutan').attr('value',data.lanjutan);
                            $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                            $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                         
        });

    $('#stop-button').click(function(){
             var stop = "STOP";
             $('#qcfg-state').attr('value',stop);
                    $('#qcfg-stop').show(); 
                    $('#qcfg-start').hide(); 
                    $('#qcfg-istirahat-start').hide(); 
                    $('#qcfg-istirahat-stop').hide();    
            });
            $('#qcfg-jenis_periksa').change(function(){
                var jenis_periksa = $('#qcfg-jenis_periksa').val();
                var snfg = $('#qcfg-snfg').val();    
                $.get('index.php?r=qc-fg/lanjutan-qc-fg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#qcfg-lanjutan').attr('value',data.lanjutan);
                            $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                            $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });               
    });
    
JS;
$this->registerJs($script);
?>