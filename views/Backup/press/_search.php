<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PressSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="press-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'jumlah_plan') ?>

    <?= $form->field($model, 'jumlah_realisasi') ?>

    <?= $form->field($model, 'nama_line') ?>

    <?php echo $form->field($model, 'jumlah_operator') ?>

    <?php echo $form->field($model, 'operator_1') ?>

    <?php echo $form->field($model, 'operator_2') ?>

    <?php echo $form->field($model, 'waktu') ?>

    <?php echo $form->field($model, 'state') ?>

    <?php echo $form->field($model, 'lanjutan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
