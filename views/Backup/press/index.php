<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Press';
$this->params['breadcrumbs'][] = $this->title;
?>

    <p>
        <?= Html::a('Input Press Data', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<div class="press-index">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'snfg',
            'jumlah_plan',
            'jumlah_realisasi',
            'nama_line',
            'jumlah_operator',
            'operator_1',
            'operator_2',
            'waktu',
            'state',
            'lanjutan',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
