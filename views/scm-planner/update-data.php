<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ScmPlanner */

// $this->title = 'Update Scm Planner: ' . ' ' . $model->id;
// $this->params['breadcrumbs'][] = ['label' => 'Scm Planners', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = 'Update';
?>
<div class="scm-planner-update">

  

    <?= $this->render('_update-data', [
        // 'model' => $model,
        'id' => $id,
    ]) ?>

</div>
