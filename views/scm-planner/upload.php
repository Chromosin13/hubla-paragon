<?= \kato\DropZone::widget([
       'options' => [
           'url'=> 'index.php?r=scm-planner/upload',
           'maxFilesize' => '10',
       ],
       'clientEvents' => [
           'complete' => "function(file){console.log(file)}",
           'removedfile' => "function(file){alert(file.name + ' is removed')}"
       ],
   ]);
?>
