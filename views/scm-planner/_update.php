<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\data\SqlDataProvider;
/* @var $this yii\web\View */
/* @var $model app\models\ScmPlanner */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<?php
                $lokasi_status = Yii::$app->db->createCommand("
                    SELECT posisi from posisi_proses_byid where snfg_komponen='".$model->snfg_komponen."'
                    ")->queryScalar();
?>

<div class="scm-planner-form">

    

    <div class="col-md-4">
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Form Planner</h3>

            <?= 
                DatePicker::widget([
                'model' => $model,
                'form' => $form,
                'name' => 'start',
                'attribute' => 'start', 
                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                'options' => ['placeholder' => 'Select issue date ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                    ]
                ]);
            ?>

            <?= 
                DatePicker::widget([
                'model' => $model,
                'form' => $form,
                'name' => 'due',
                'attribute' => 'due', 
                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                'options' => ['placeholder' => 'Select issue date ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                    ]
                ]);
            ?>
            <?= 
                DatePicker::widget([
                'model' => $model,
                'form' => $form,
                'name' => 'tglpermintaan',
                'attribute' => 'tglpermintaan', 
                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                'options' => ['placeholder' => 'Select issue date ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                    ]
                ]);
            ?>

            <?= $form->field($model, 'week')->textInput() ?>

            <?= $form->field($model, 'sediaan')->textInput() ?>

            <?= $form->field($model, 'streamline')->textInput() ?>

            <?= $form->field($model, 'line_timbang')->textInput() ?>

            <?= $form->field($model, 'line_olah_premix')->textInput() ?>

            <?= $form->field($model, 'line_olah')->textInput() ?>

            <?= $form->field($model, 'line_olah_2')->textInput() ?>

            <?= $form->field($model, 'line_adjust_olah_1')->textInput() ?>

            <?= $form->field($model, 'line_adjust_olah_2')->textInput() ?>

            <?= $form->field($model, 'line_press')->textInput() ?>
    
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Form Planner</h3>

            <?= $form->field($model, 'line_kemas_1')->textInput() ?>

            <?= $form->field($model, 'line_kemas_2')->textInput() ?>

            <?= $form->field($model, 'leadtime')->textInput() ?>
          
            <?= $form->field($model, 'kode_jadwal')->textInput() ?>

            <?= $form->field($model, 'nors')->textInput() ?>

            <?= $form->field($model, 'nomo')->textInput(['readOnly'=>true]) ?>

            <?= $form->field($model, 'snfg')->textInput(['readOnly'=>true]) ?>

            <?= $form->field($model, 'snfg_komponen')->textInput(['readOnly'=>true]) ?>

            <?= $form->field($model, 'koitem_bulk')->textInput(['readOnly'=>true]) ?>

            <?= $form->field($model, 'koitem_fg')->textInput(['readOnly'=>true]) ?>

            <?= $form->field($model, 'nama_bulk')->textInput(['readOnly'=>true]) ?>

            <?= $form->field($model, 'nama_fg')->textInput(['readOnly'=>true]) ?>

            <?= $form->field($model, 'besar_batch')->textInput() ?>
    
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Form Planner</h3>
                <?= $form->field($model, 'besar_lot')->textInput() ?>

                <?= $form->field($model, 'lot_ke')->textInput() ?>

                <?= $form->field($model, 'no_formula_br')->textInput() ?>

                <?= $form->field($model, 'jumlah_press')->textInput() ?>
                
                <?= $form->field($model, 'jumlah_pcs')->textInput() ?>

                <?= $form->field($model, 'keterangan')->textInput() ?>

                <?= $form->field($model, 'kategori_sop')->textInput() ?>

                <?= $form->field($model, 'kategori_detail')->textInput() ?>

                <?= $form->field($model, 'nobatch')->textInput() ?>

                <?= $form->field($model, 'line_end')->dropDownList(['QC FG' => 'QC FG','KEMAS 2' => 'KEMAS 2','KEMAS 1' => 'KEMAS 1'],['prompt'=>'Select Option']); ?>

                <?php

                    switch ($model->status) {
                        case "UNHOLD":
                            echo $form->field($model, 'status')->dropDownList(['UNHOLD'=>'UNHOLD','HOLD' => 'HOLD','CANCEL' => 'CANCEL']);
                            break;
                        case "HOLD":
                            echo $form->field($model, 'status')->dropDownList(['HOLD'=>'HOLD','UNHOLD' => 'UNHOLD','CANCEL' => 'CANCEL']);
                            break;
                        case "CANCEL":
                            echo $form->field($model, 'status')->dropDownList(['CANCEL'=>'CANCEL','UNHOLD' => 'UNHOLD','HOLD' => 'HOLD']);
                            break;
                    }
                ?>

                 <?= $form->field($model, 'line_terima')->textInput() ?>

                <?= $form->field($model, 'alokasi')->textInput() ?>

                <?= $form->field($model, 'odoo_code')->textInput() ?>
                <br>

               <!--  <?= $form->field($model, 'status')->dropDownList(['PAUSE' => 'PAUSE','CONTINUE' => 'CONTINUE','UNHOLD' => 'UNHOLD'],['prompt'=>'Select Option']); ?> -->

                <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

</div>



<?php
$script = <<< JS
$('.datepicker').datepicker({
    format: 'mm/dd/yyyy',
    startDate: '-3d'
});

JS;
$this->registerJs($script);
?>