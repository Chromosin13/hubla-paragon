<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScmPlannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Supply Chain Management - Planner';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php 
// $items = [
//     array("id" => 1, "name" => "Cyrus", "email" => "risus@consequatdolorvitae.org"),
//     array("id" => 2, "name" => "Justin", "email" => "ac.facilisis.facilisis@at.ca"),
//     array("id" => 3, "name" => "Mason", "email" => "in.cursus.et@arcuacorci.ca"),
//     array("id" => 4, "name" => "Fulton", "email" => "a@faucibusorciluctus.edu"),
//     array("id" => 5, "name" => "Neville", "email" => "eleifend@consequatlectus.com"),
//     array("id" => 6, "name" => "Jasper", "email" => "lectus.justo@miAliquam.com"),
//     array("id" => 7, "name" => "Neville", "email" => "Morbi.non.sapien@dapibusquam.org"),
//     array("id" => 8, "name" => "Neville", "email" => "condimentum.eget@egestas.edu"),
//     array("id" => 9, "name" => "Ronan", "email" => "orci.adipiscing@interdumligulaeu.com"),
//     array("id" => 10, "name" => "Raphael", "email" => "nec.tempus@commodohendrerit.co.uk"),
// ];;


// function filter($item) {
//     $mailfilter = Yii::$app->request->getQueryParam('filteremail', '');
//     if (strlen($mailfilter) > 0) {
//         if (strpos($item['email'], $mailfilter) != false) {
//             return true;
//         } else {
//             return false;
//         }
//     } else {
//         return true;
//     }
// }

$searchAttributes = ['id', 'name', 'email'];
$searchModel = [];
$searchColumns = [];

foreach ($searchAttributes as $searchAttribute) {
    $filterName = 'filter' . $searchAttribute;
    $filterValue = Yii::$app->request->getQueryParam($filterName, '');
    $searchModel[$searchAttribute] = $filterValue;
    $searchColumns[] = [
        'attribute' => $searchAttribute,
        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
        'value' => $searchAttribute,
    ];
    $items = array_filter($items, function($item) use (&$filterValue, &$searchAttribute) {
        return strlen($filterValue) > 0 ? stripos('/^' . strtolower($item[$searchAttribute]) . '/', strtolower($filterValue)) : true;
    });
}

$dataProvider = new \yii\data\ArrayDataProvider([
        // 'key'=>'id',
        'allModels' => $items,
        'sort' => [
            'attributes' =>  $searchAttributes,
        ],
]);

echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge(
            $searchColumns, [
        ['class' => 'yii\grid\ActionColumn']
            ]
        )
]);

?>