<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ScmPlanner */
/* @var $form yii\widgets\ActiveForm */
?>

<?= Html::a('PRINT JADWAL n-Jadwal Terakhir', ['print-jadwal'], ['class'=>'btn btn-primary btn-block']) ?>

<?= Html::a('PRINT JADWAL RANGE TANGGAL PENIMBANGAN DAN SEDIAAN', ['print-jadwal-date-range'], ['class'=>'btn btn-warning btn-block']) ?>


<?php
$script = <<< JS




JS;
$this->registerJs($script);
?>