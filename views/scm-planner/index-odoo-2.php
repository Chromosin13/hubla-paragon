<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;
use kartik\export\ExportMenu;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScmPlannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $nomor_mo." State : ".$mo_state." Write Date : ".$mo_writedate;
$this->params['breadcrumbs'][] = $this->title;
?>

<?php 


if($mo_state=="confirmed"){
   echo Html::a('START PRODUCE', ['scm-planner/produce','nomo'=>$nomor_mo], ['class' => 'btn btn-success btn-block']);
}


// $bom = [
//     array("id" => 1, "name" => "Cyrus", "email" => "risus@consequatdolorvitae.org"),
//     array("id" => 2, "name" => "Justin", "email" => "ac.facilisis.facilisis@at.ca"),
//     array("id" => 3, "name" => "Mason", "email" => "in.cursus.et@arcuacorci.ca"),
//     array("id" => 4, "name" => "Fulton", "email" => "a@faucibusorciluctus.edu"),
//     array("id" => 5, "name" => "Neville", "email" => "eleifend@consequatlectus.com"),
//     array("id" => 6, "name" => "Jasper", "email" => "lectus.justo@miAliquam.com"),
//     array("id" => 7, "name" => "Neville", "email" => "Morbi.non.sapien@dapibusquam.org"),
//     array("id" => 8, "name" => "Neville", "email" => "condimentum.eget@egestas.edu"),
//     array("id" => 9, "name" => "Ronan", "email" => "orci.adipiscing@interdumligulaeu.com"),
//     array("id" => 10, "name" => "Raphael", "email" => "nec.tempus@commodohendrerit.co.uk"),
// ];;


// function filter($item) {
//     $mailfilter = Yii::$app->request->getQueryParam('filteremail', '');
//     if (strlen($mailfilter) > 0) {
//         if (strpos($item['email'], $mailfilter) != false) {
//             return true;
//         } else {
//             return false;
//         }
//     } else {
//         return true;
//     }
// }

$searchAttributes = ['id','product_id','product_uom_qty','qty_done','write_date'];
$searchModel = [];
$searchColumns = [];

foreach ($searchAttributes as $searchAttribute) {
    $filterName = 'filter' . $searchAttribute;
    $filterValue = Yii::$app->request->getQueryParam($filterName, '');
    $searchModel[$searchAttribute] = $filterValue;
    $searchColumns[] = [
        'attribute' => $searchAttribute,
        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
        'value' => $searchAttribute,
    ];
    $bom = array_filter($bom, function($item) use (&$filterValue, &$searchAttribute) {
        return strlen($filterValue) > 0 ? stripos('/^' . strtolower($item[$searchAttribute]) . '/', strtolower($filterValue)) : true;
    });
}

$dataProvider = new \yii\data\ArrayDataProvider([
        'key'=>'id',
        'allModels' => $bom,
        'sort' => [
            'attributes' =>  $searchAttributes,
        ],
]);


        Modal::begin([
            'header'=>'<h4>Update Model</h4>',
            'id'=>'update-modal',
            'size'=>'modal-info modal-lg',
            'footer'=>'

                <button type="button" class="btn btn-outline btn-block">Save changes</button>',
            
        ]);

        echo "<div id='updateModalContent'>";

        echo '<div class="modal-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label"><b>Standard Quantity</b></label>

                  <div class="col-sm-10">
                    <input  type="text" 
                            class="form-control 
                            input-lg" 
                            placeholder="';
                            echo $bom[0]['product_uom_qty'];
                            echo '" 
                            disabled=""
                    >
                  </div>
                </div>
               
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Actual Quantity</label>

                  <div class="col-sm-10">
                    <input class="form-control input-lg" type="number" id="inputPassword3" placeholder="Password">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> JADWAL SELESAI
                      </label>
                    </div>
                  </div>
                </div>
              </div>';



        echo '</div><br></br><br></br><br></br>';


        Modal::end();


Pjax::begin(['id'=>'downtimeGrid']);

if($mo_state=='progress'){


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'floatHeader'=>'true',
        'floatHeaderOptions' => ['position' => 'absolute',],
        'showPageSummary'=>true,
        'pjax'=>true,
        'striped'=>true, // yii2 modal load page with parameter
        'hover'=>true,
        'panel'=>['type'=>'success', 'heading'=>'List'],
        'toolbar' => [
        ],
        'rowOptions'   => function ($bom, $key, $index, $grid) {
            if($bom['write_date'] ){
            
                return [
                        'value'=>Url::to('index.php?r=scm-planner/update-data&id='.$bom['id']),
                        'class' => 'success',
                        'data-toggle' => 'tooltip',
                        'data-id' => $bom['id'],
                        // 'id'=>'popupModal'
                        
                ];
            
            }
        },

        'columns' => array_merge(
            $searchColumns, 
            // [   
            //     'class' => 'kartik\grid\ActionColumn',
            //     // 'options'=>['class'=>'action-column'],
            //     'template' => '{test}',
            //     'buttons'=>[
            //         'test' => function($url,$model,$key){
            //             $btn = Html::button('<img class="img-circle" src="../web/images/adjust.png" alt="Terima" width="21" height="21">',[
            //                 'value'=>Url::to('index.php?r=scm-planner/update-data&id='.$key),
            //                 // Yii::$app->urlManager->createUrl('qc-bulk-entry/update'), //<---- here is where you define the action that handles the ajax request
            //                 'class'=>'update-modal-click grid-action',
            //                 'data-toggle'=>'tooltip',
            //                 'data-placement'=>'bottom',
            //                 'title'=>'Update'
            //             ]);
            //             return $btn;
            //         }
            //     ],
            // ],
            [
                ['class' => 'kartik\grid\ActionColumn']
            ]
        ),

    ]);    
}else{


     

        echo GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'floatHeader'=>'true',
        'floatHeaderOptions' => ['position' => 'absolute',],
        'showPageSummary'=>true,
        'pjax'=>true,
        'striped'=>true,
        'hover'=>true,
        'panel'=>['type'=>'warning', 'heading'=>'List'],
        'toolbar' => [
        ],
        'rowOptions'   => function ($bom, $key, $index, $grid) {
            return ['class' => 'warning','data-id' => $bom['id']];
        },
        'columns' => array_merge(
            $searchColumns, [
        ['class' => 'kartik\grid\ActionColumn']
            ]
        ),

        ]);  
    
}



?>

<?php
$this->registerJs("

var mo_state = '$mo_state';


// if(mo_state=='progress'){
//     $('td').click(function (e) {
//         var id = $(this).closest('tr').data('id');
//         alert(id);
       
//        // $('#popupModal').click(function(e) {

//          e.preventDefault();
//          $('#modal').modal('show')
//          .find('#modalContent')
//          .load($(this).attr('value'));
//        return false;
//        // });


//         // alert(id);
//         // if(e.target == this)
//         //     location.href = '" . Url::to(['scm-planner/update']) . "&id=' + id;
//     });


//     // $(function() {
//     //    $('#popupModal').click(function(e) {
//     //      var id = $(this).closest('tr').data('id');
//     //      e.preventDefault();
//     //      $('#modal').modal('show').find('.modal-content')
//     //      .load($(this).attr('href'));
//     //      location.href = '" . Url::to(['scm-planner/update-data']) . "&id=' + id;
//     //    });
//     // });
// }



if(mo_state=='progress'){
    // $('td').click(function (e) {

    $('td').click(function (e) {

        var id = $(this).closest('tr').data('id');
        alert(id);
        e.preventDefault();

        // var value = $(this).closest('tr').attr('value');
        // alert(value);

         $('#update-modal').modal('show')
         .find('#updateModalContent')
         .load($(this).attr('value'));
    });
}

");

?>
