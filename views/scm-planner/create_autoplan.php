<?php
$script = <<< JS
$('input').keydown( function (event) { //event==Keyevent
    if(event.which == 13) {
        var inputs = $(this).closest('form').find(':input:visible');
        inputs.eq( inputs.index(this)+ 1 ).focus();
        event.preventDefault(); //Disable standard Enterkey action
    }
    // event.preventDefault(); <- Disable all keys  action
});
JS;
$this->registerJs($script);
?>


<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ScmPlanner */

$this->title = 'Input Master Data SCM Requirement Plot Scheduler';
// $this->params['breadcrumbs'][] = ['label' => 'Scm Planners', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="callout callout-success">
  <h4>Notice</h4>

  <p> Upload file pada "Drop files here to upload" kemudian Tekan Generate</p>
</div>

<div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Upload Excel (.xls) SCM Planner</h2>
              <h6>Nama File harus <b>planner_master_data.xls</b> </h6>

              <?= Html::a('Download Template File', ['/scm-planner/download-autoplan'], ['class'=>'btn btn-primary grid-button']) ?>
              <br></br>
              <?= \kato\DropZone::widget([
                     'options' => [
                         'url'=> 'index.php?r=scm-planner/upload',
                         'maxFilesize' => '10',
                     ],
                     'clientEvents' => [
                         'complete' => "function(file){console.log(file)}",
                          'complete' => "function(){
                                                     $('#submit-excel').show();
                                                   }",
                         'removedfile' => "function(file){alert(file.name + ' is removed')}"
                     ],
                 ]);
              ?>


            <br />

            <div class="form-group" id="submit-excel">
                       <?= Html::a('Generate', ['/scm-planner/execute'], ['class'=>'btn btn-primary grid-button']) ?>
            </div>

            </div>
</div>

<!-- <div class="scm-planner-create" >

    <div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Manual Input Form</h2>

                  <br></br>
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>


            </div>
    </div>
</div> -->

<?php
$script = <<< JS
$(function() {
    $('#submit-excel').hide();
});
JS;
$this->registerJs($script);
?>
