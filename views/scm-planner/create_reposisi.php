<?php
$script = <<< JS
$('input').keydown( function (event) { //event==Keyevent
    if(event.which == 13) {
        var inputs = $(this).closest('form').find(':input:visible');
        inputs.eq( inputs.index(this)+ 1 ).focus();
        event.preventDefault(); //Disable standard Enterkey action
    }
    // event.preventDefault(); <- Disable all keys  action
});
JS;
$this->registerJs($script);
?>


<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ScmPlanner */

$this->title = 'Input Reposisi';
// $this->params['breadcrumbs'][] = ['label' => 'Scm Planners', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="callout callout-success">
  <h4>Notice</h4>

  <p> Upload file pada "Drop files here to upload" kemudian Tekan Generate</p>
</div>


<a href='https://docs.google.com/document/d/1BDBSybZRceWsIlYvq-mVqDgJtUGT3mUCSpIpOQMFR0A/edit?usp=sharing' target='_blank' class='btn btn-info'>USER GUIDE</a>

<br></br>
<div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Upload Kebutuhan Reposisi Excel (.xlsx)</h2>
              <h6>Nama File harus <b>kebutuhan_reposisi.xlsx</b> dan <b>po_reposisi.csv</b>   </h6>

              <?= Html::a('Download Template File Kebutuhan', ['/scm-planner/download-kebutuhan'], ['class'=>'btn btn-primary grid-button']) ?>
              <?= Html::a('Download Template File PO', ['/scm-planner/download-po'], ['class'=>'btn btn-primary grid-button']) ?>
              <br></br>
              <?= \kato\DropZone::widget([
                     'options' => [
                         'url'=> 'index.php?r=scm-planner/upload-kebutuhan',
                         'maxFilesize' => '10',
                     ],
                     'clientEvents' => [
                         'complete' => "function(file){console.log(file)}",
                          'complete' => "function(){
                                                     $('#submit-excel').show();
                                                   }",
                         'removedfile' => "function(file){alert(file.name + ' is removed')}"
                     ],
                 ]);
              ?>


            <br />

            <div class="form-group" id="submit-excel">
                       <?= Html::a('Generate and Download', ['/scm-planner/execute-reposisi'], ['class'=>'btn btn-primary grid-button']) ?>
            </div>

            </div>
</div>


<?php
$script = <<< JS
$(function() {
    $('#submit-excel').hide();
});
JS;
$this->registerJs($script);
?>
