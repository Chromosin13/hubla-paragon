<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ScmPlanner */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<style media="screen">
  .swal-button-container{
    text-align: center!important;
    flex-shrink: 5;
    flex-grow:4;
    width: 100%
  }

  .swal-footer{
    text-align: center;
    display: flex;
  }

  .swal-text{
    text-align: center;
  }

  .swal-button--confirm{
    background-color: #4CAF50;
  }

  .swal-button:hover{
    background-color: #aaa!important
  }


  .swal-button{
    vertical-align: middle!important;
    padding: 1em 1em!important;
    text-transform: capitalize;
    width: 100%
  }
</style>

<div class="scm-planner-form">


    <div class="col-md-4">
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Form Planner</h3>


            <?=
                DatePicker::widget([
                'model' => $model,
                'form' => $form,
                'name' => 'start',
                'attribute' => 'start',
                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                'options' => ['placeholder' => 'Select issue date ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                    ]
                ]);
            ?>

            <?=
                DatePicker::widget([
                'model' => $model,
                'form' => $form,
                'name' => 'due',
                'attribute' => 'due',
                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                'options' => ['placeholder' => 'Select issue date ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                    ]
                ]);
            ?>
            <?=
                DatePicker::widget([
                'model' => $model,
                'form' => $form,
                'name' => 'tglpermintaan',
                'attribute' => 'tglpermintaan',
                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                'options' => ['placeholder' => 'Select issue date ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                    ]
                ]);
            ?>

            <?= $form->field($model, 'week')->textInput() ?>

            <?= $form->field($model, 'sediaan')->textInput() ?>

            <?= $form->field($model, 'streamline')->textInput() ?>

            <?= $form->field($model, 'line_timbang')->textInput() ?>

            <?= $form->field($model, 'line_olah_premix')->textInput() ?>

            <?= $form->field($model, 'line_olah')->textInput() ?>

            <?= $form->field($model, 'line_olah_2')->textInput() ?>

            <?= $form->field($model, 'line_adjust_olah_1')->textInput() ?>

            <?= $form->field($model, 'line_adjust_olah_2')->textInput() ?>

            <?= $form->field($model, 'line_press')->textInput() ?>

            <?= $form->field($model, 'line_kemas_1')->textInput() ?>

            <?= $form->field($model, 'line_kemas_2')->textInput() ?>

            <?= $form->field($model, 'leadtime')->textInput() ?>

            <?= $form->field($model, 'kode_jadwal')->textInput() ?>

            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Form Planner</h3>

            <?= $form->field($model, 'nors')->textInput() ?>

            <?= $form->field($model, 'nomo')->textInput() ?>

            <?= $form->field($model, 'snfg')->textInput() ?>

            <?= $form->field($model, 'snfg_komponen')->textInput() ?>

            <?= $form->field($model, 'koitem_bulk')->textInput() ?>

            <?= $form->field($model, 'koitem_fg')->textInput() ?>

            <?= $form->field($model, 'nama_bulk')->textInput() ?>

            <?= $form->field($model, 'nama_fg')->textInput() ?>

            <?= $form->field($model, 'besar_batch')->textInput() ?>

            <?= $form->field($model, 'besar_lot')->textInput() ?>

            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Form Planner</h3>


                <?= $form->field($model, 'lot_ke')->textInput() ?>

                <?= $form->field($model, 'no_formula_br')->textInput() ?>

                <?= $form->field($model, 'jumlah_press')->textInput() ?>

                <?= $form->field($model, 'jumlah_pcs')->textInput() ?>

                <?= $form->field($model, 'keterangan')->textInput() ?>

                <?= $form->field($model, 'kategori_sop')->textInput() ?>

                <?= $form->field($model, 'kategori_detail')->textInput() ?>

                <?= $form->field($model, 'nobatch')->textInput() ?>

                <?= $form->field($model, 'status')->dropDownList(['HOLD' => 'HOLD','UNHOLD' => 'UNHOLD'],['prompt'=>'Select Option']); ?>

                 <?= $form->field($model, 'line_terima')->textInput() ?>

                <?= $form->field($model, 'alokasi')->textInput() ?>


                <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

</div>



<?php
$script = <<< JS

  document.getElementsByClassName('btn btn-success')[0].onclick = function(e){
    e.preventDefault();
    swal({
      title: "Peringatan",
      text: "Apakah anda yakin ingin create jadwal ini?",
      icon: "warning",
      allowOutsideClick: false,
      closeOnClickOutside: false,
      buttons: {
        cancel: {
          text: "Batal",
          value: false,
          visible: true,
          closeModal: true
        },
        confirm: {
          text: "Buat Sekarang",
          value: true,
          visible: true,
          className: "text-full-width",
          closeModal: true
        }
      },
    })
    .then((res) => {
      if (res) {
        document.getElementById('w4').submit();
      }
    })
  }

JS;
$this->registerJs($script);
?>
