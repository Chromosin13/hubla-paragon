<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;
use kartik\export\ExportMenu;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScmPlannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

                <div class="box box-widget widget-user">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                    
                    <?php if($mo_state=='progress'){ echo '<div class="widget-user-header bg-green-gradient">';}else{ echo '<div class="widget-user-header bg-yellow-gradient">'; }?>

                      <h3 class="widget-user-username"><b><?php echo $nomor_mo ?></b></h3>
                      <h5 class="widget-user-desc"><?php if($mo_state=='progress'){ echo 'IN PROGRESS : '.$mo_writedate;}else{ echo $mo_state; }?></h5>
                <?php 
                        if($mo_state=="confirmed"){
                           echo Html::a('START PRODUCE', ['scm-planner/produce','nomo'=>$nomor_mo], ['class' => 'btn btn-outline btn-warning btn-block']);
                        }
                ?>

                </div>
                    <!--<div class="widget-user-image">
                            <img class="img-circle" src="../web/images/cmyk-icon.png" alt="User Avatar">
                        </div> -->

                    <?php 





                        $searchAttributes = ['id','product_id','product_uom_qty','qty_done','lanjutan','qty','nomor_mo'];
                        $searchModel = [];
                        $searchColumns = [];

                        foreach ($searchAttributes as $searchAttribute) {
                            $filterName = 'filter' . $searchAttribute;
                            $filterValue = Yii::$app->request->getQueryParam($filterName, '');
                            $searchModel[$searchAttribute] = $filterValue;
                            $searchColumns[] = [
                                'attribute' => $searchAttribute,
                                'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                                'value' => $searchAttribute,
                            ];
                            $bom = array_filter($bom, function($item) use (&$filterValue, &$searchAttribute) {
                                return strlen($filterValue) > 0 ? stripos('/^' . strtolower($item[$searchAttribute]) . '/', strtolower($filterValue)) : true;
                            });
                        }

                        $dataProvider = new \yii\data\ArrayDataProvider([
                                'key'=>'id',
                                'allModels' => $bom,
                                'sort' => [
                                    'attributes' =>  $searchAttributes,
                                ],
                        ]);


                                Modal::begin([
                                    'header'=>'<h4>INPUT MATERIAL CONSUMPTION</h4>',
                                    'id'=>'update-modal',
                                    'size'=>'modal-success modal-lg',
                                    
                                ]);

                                echo "<div id='updateModalContent'></div>";

                                Modal::end();

                                if($mo_state=='progress'){


                                    echo GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        // 'filterModel' => $searchModel,
                                        'responsiveWrap' => false,
                                        'floatHeader'=>'true',
                                        'floatHeaderOptions' => ['position' => 'absolute',],
                                        // 'showPageSummary'=>true,
                                        'pjax'=>true,
                                        'striped'=>true, // yii2 modal load page with parameter
                                        'condensed'=>true, 
                                        'hover'=>true,
                                        'panel'=>['type'=>'default', 'heading'=>'List', 'footer' => false, 'beforeOptions'=>['class'=>'grid_panel_remove'],],
                                        'toolbar'=> false,
                                        'rowOptions'   => function ($bom, $key, $index, $grid) {
                                            if($bom['lanjutan'] ){
                                                if($bom['lanjutan']%2==0){ 
                                                    return [
                                                            'value'=>Url::to('index.php?r=scm-planner/update-data&id='.$bom['id'].'&nomo='.$bom['nomor_mo']),
                                                            'class' => 'info',
                                                            'data-toggle' => 'tooltip',
                                                            'data-id' => $bom['id'],
                                                            // 'id'=>'popupModal'
                                                            
                                                    ];
                                                }else{
                                                    return [
                                                            'value'=>Url::to('index.php?r=scm-planner/update-data&id='.$bom['id'].'&nomo='.$bom['nomor_mo']),
                                                            'class' => 'success',
                                                            'data-toggle' => 'tooltip',
                                                            'data-id' => $bom['id'],
                                                            // 'id'=>'popupModal'
                                                            
                                                    ];
                                                }
                                            
                                            }else{
                                                return [
                                                            'value'=>Url::to('index.php?r=scm-planner/update-data&id='.$bom['id'].'&nomo='.$bom['nomor_mo']),
                                                            'class' => 'default',
                                                            'data-toggle' => 'tooltip',
                                                            'data-id' => $bom['id'],
                                                            // 'id'=>'popupModal'
                                                            
                                                    ];
                                            }
                                        },

                                        'columns' => array_merge(
                                            $searchColumns, 
                                            [
                                                // ['class' => 'kartik\grid\ActionColumn']
                                            ]
                                        ),

                                    ]);    
                                }else{


                                     

                                        echo GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        // 'filterModel' => $searchModel,
                                        'responsiveWrap' => false,
                                        'floatHeader'=>'true',
                                        'floatHeaderOptions' => ['position' => 'absolute',],
                                        // 'showPageSummary'=>true,
                                        'pjax'=>true,
                                        'striped'=>true,
                                        'condensed'=>true, 
                                        // 'hover'=>true,
                                        'panel'=>['type'=>'default', 'heading'=>'List', 'footer' => false, 'beforeOptions'=>['class'=>'grid_panel_remove'],],
                                        'toolbar' => [
                                        ],
                                        'rowOptions'   => function ($bom, $key, $index, $grid) {
                                            return ['class' => 'warning','data-id' => $bom['id']];
                                        },
                                        'columns' => array_merge(
                                            $searchColumns, [
                                            ]
                                        ),

                                        ]);  
                                    
                                }



                                ?>



<div class="zoom">
    <?php 
            if($mo_state=="progress"){
               echo Html::a('MARK DONE', ['scm-planner/mark-done','nomo'=>$nomor_mo], ['class' => 'btn btn-danger btn-block']);
            }
    ?>
</div>                            

<?php
$this->registerJs("

var mo_state = '$mo_state';

if(mo_state=='progress'){
    $('td').click(function (e) {

        e.preventDefault();

         $('#update-modal').modal('show')
         .find('#updateModalContent')
         .load($(this).closest('tr').attr('value'))
        
        $('#update-modal').on('shown.bs.modal', function () {
          $('#qty_done').focus()
        })
    
    });
}

");

?>
