<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use Da\QrCode\QrCode;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApTableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<?php


            // $qrCode = (new QrCode($model->nomo))
            //     ->setSize(75)
            //     ->setMargin(3)
            //     ->useForegroundColor(0, 0, 0);
            // $qrCode->writeFile('/var/www/html/flowreport/web/qr_suratjalan/nomo.png');

            // $qrCode = (new QrCode($model->snfg))
            //     ->setSize(75)
            //     ->setMargin(3)
            //     ->useForegroundColor(0, 0, 0);
            // $qrCode->writeFile('/var/www/html/flowreport/web/qr_suratjalan/snfg.png');

            // $qrCode = (new QrCode($model->snfg_komponen))
            //     ->setSize(75)
            //     ->setMargin(3)
            //     ->useForegroundColor(0, 0, 0);
            // $qrCode->writeFile('/var/www/html/flowreport/web/qr_suratjalan/snfg_komponen.png');
?>



<div class="ap-table-index">
<section class="invoice">
      <!-- title row -->

                  <?php
                    // echo '<img src="../web/images/products/'.$model->koitem_fg;
                    // echo '.jpg" height="100%">';
                    ?>




  <div class="boxed">
        <div class="col-xs-2">
          <?php
                $filename = '/var/www/html/flowreport/web/images/products/'.$model->koitem_fg.'.jpg';

                if (!file_exists($filename)) {
                    echo '<img src="../web/images/no_image_available';
                    echo '.jpeg" sytle="display: block;margin-left: auto;margin-right: auto;">';
                    // exit("Your file doesn't exist");
                }else{

                    echo '<img src="../web/images/products/'.$model->koitem_fg;
                    echo '.jpg" sytle="display: block;margin-left: auto;margin-right: auto;">';
                }
          ?>
        </div>
        <div class="col-xs-8">
          <div style="text-align: left;font-size: 10px;">
           <b> Nama FG </b>
           <p style="text-align: left;font-size: 10px;">
            <?php echo $model->nama_fg;?>
           </p>
           <b> Nama Bulk </b>
           <p style="text-align: left;font-size: 10px;">
            <?php echo $model->nama_bulk;?>
           </p>
           <div class="row">
            <div class="col-xs-4">
                <b> Kode Odoo </b>
                <p style="text-align: left;font-size: 10px;">
                  <?php echo $model->odoo_code;?>
                </p>
              </div>
              <div class="col-xs-4">
                <b> NA Number </b>
                <p style="text-align: left;font-size: 10px;">
                  <?php echo $na_number;?>
                </p>
              </div>
            </div>
          </div>
        </div>
  </div>
  <div class="boxed">
      <table style="text-align: center;font-size: 8px;">
            <tr>
              <th><b>Week</th>
              <th><b>Streamline</th>
              <th><b>Mesin Olah Premix</th>
              <th><b>Mesin Olah 1</th>
              <th><b>Mesin Olah 2</th>
              <th><b>Mesin Adjust 1</th>
              <th><b>Mesin Adjust 2</th>
              <th><b>Mesin Kemas 1</th>
              <th><b>Mesin Kemas 2</th>
            </tr>
            <tr>
              <td><?php echo $model->week;?></td>

              <td><?php echo $model->streamline;?></td>

              <td><?php echo $model->line_olah_premix;?></td>

              <td><?php echo $model->line_olah;?></td>

              <td><?php echo $model->line_olah_2;?></td>

              <td><?php echo $model->line_adjust_olah_1;?></td>

              <td><?php echo $model->line_adjust_olah_2;?></td>

              <td><?php echo $model->line_kemas_1;?></td>

              <td><?php echo $model->line_kemas_2;?></td>
            </tr>

      </table>
      <table style="text-align: center;font-size: 8px;">
           <tr>
              <th><b>Start</th>
              <th><b>Due</th>
              <th><b>Kode Jadwal</th>
              <th><b>Besar Batch (Kg)</th>
              <th><b>Besar Lot (Kg)</th>
              <th><b>MPQ Olah Kunci (Kg)</th>
              <th><b>Lot Ke</th>
              <th><b>Jumlah Pcs</th>
              <th><b>Jumlah Supply Packaging</th>
            </tr>

            <tr>
              <td><?php echo $model->start;?></td>
              <td><?php echo $model->due;?></td>
              <td><?php echo $model->kode_jadwal;?></td>
              <td><?php echo $model->besar_batch;?></td>
              <td><?php echo $model->besar_lot;?></td>
              <td><?php echo $model->mpq_olah_kunci;?></td>
              <td><?php echo $model->lot_ke;?></td>
              <td><?php echo $model->jumlah_pcs;?></td>
              <td><?php echo $model->jumlah_supply_packaging;?></td>
            </tr>

      </table>
      <table style="text-align: center;font-size: 8px;">
           <tr>
             <!--  <th><b>Nama FG</th>
              <th><b>Nama Bulk</th> -->
              <th><b>Due End Olah</th>
              <th><b>Line Yang Menerima</th>
              <th><b>Keterangan</th>
              <th><b>Nomor Batch</th>
              <th><b>Berat Riil</th>
              <th><b>Seq</th>
            </tr>

            <tr>
              <!-- <td><?php echo $model->nama_fg;?></td> -->
              <!-- <td><?php echo $model->nama_bulk;?></td> -->
              <td></td>
              <td><?php echo $model->line_terima;?></td>
              <td><?php echo $model->keterangan;?></td>
              <td height="25px" width="60px"><?php echo $model->nobatch;?></td>
              <td height="25px" width="65px"></td>
              <td><?php echo $model->id;?></td>
            </tr>

      </table>

  </div>

            <i class="fa fa-globe"></i>

        <!-- /.col -->
      <!-- info row -->
      <div class="boxed">
        <div class="col-xs-2">
          <?php
            if(!empty($model->nomo && $model->nomo != '-')){
              $qrCode = (new QrCode($model->nomo))
                  ->setSize(75)
                  ->setMargin(3)
                  ->useForegroundColor(0, 0, 0);
              $qrCode->writeFile('/var/www/html/flowreport/web/qr_suratjalan/'. preg_replace('/[^A-Za-z0-9\-]/', '', $model->nomo).'.png');
            }

          ?>

          <?php

            if(!empty($model->nomo) && $model->nomo != '-'){
              echo '<img src="../web/qr_suratjalan/';
              echo preg_replace('/[^A-Za-z0-9\-]/', '', $model->nomo);
              echo '.png" >';
            }

          ?>

        </div>
        <div class="col-xs-8">
             <b>NOMO</b> <?php echo $model->nomo;?>
             <p></p>
             <?php
              if(!empty($model->nomo && $model->nomo != '-')){
                $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
                echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($model->nomo, $generator::TYPE_CODE_128,2,60)) . '">';
              }

            ?>
        </div>
      </div>
      <div class="boxed">

        <div class="col-xs-8">
          <b>SNFG</b> <?php echo $model->snfg;?>
          <p></p>
          <?php

              if(!empty($model->snfg && $model->snfg != '-')){
                $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
                echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($model->snfg, $generator::TYPE_CODE_128,2,60)) . '" width="100%">';

              }
              ?>
        </div>
        <div class="col-xs-2">
          <?php
            if(!empty($model->snfg) && $model->snfg != '-'){
              $qrCode = (new QrCode($model->snfg))
                ->setSize(75)
                ->setMargin(3)
                ->useForegroundColor(0, 0, 0);
              $qrCode->writeFile('/var/www/html/flowreport/web/qr_suratjalan/'. preg_replace('/[^A-Za-z0-9\-]/', '', $model->snfg).'.png');
            }
          ?>

            <?php
            if(!empty($model->snfg && $model->snfg != '-')){
              echo '<img src="../web/qr_suratjalan/';
              echo preg_replace('/[^A-Za-z0-9\-]/', '', $model->snfg);
              echo '.png" >';
            }
            ?>
        </div>
      </div>
      <div class="boxed">
          <div class="col-xs-2">
            <?php
            if(!empty($model->snfg_komponen && $model->snfg_komponen != '-')){
              $qrCode = (new QrCode($model->snfg_komponen))
                ->setSize(75)
                ->setMargin(3)
                ->useForegroundColor(0, 0, 0);
              $qrCode->writeFile('/var/www/html/flowreport/web/qr_suratjalan/'. preg_replace('/[^A-Za-z0-9\-]/', '', $model->snfg_komponen).'.png');
            }
            ?>
            <?php
            if(!empty($model->snfg_komponen && $model->snfg_komponen != '-')){
              echo '<img src="../web/qr_suratjalan/';
              echo preg_replace('/[^A-Za-z0-9\-]/', '', $model->snfg_komponen);
              echo '.png" >';
            }
            ?>
          </div>
          <div class="col-xs-8">
            <b>KOMPONEN</b> <?php echo $model->snfg_komponen;?>
            <p></p>
            <?php
            if(!empty($model->snfg_komponen && $model->snfg_komponen != '-')){
                $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
                echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($model->snfg_komponen, $generator::TYPE_CODE_128,2,60)) . '" width="100%">';
            }
            ?>
          </div>

      </div>
      <p style="text-align: left;font-size: 8px;">
                   FF.PPC.0001 - Dokumen ini digenerate oleh sistem.
              </p>
      <?php
          // echo $compare_id;
          // echo '===';
          // echo $last_id;

        if($compare_id==$last_id){
          // echo $compare_id;
          // echo '===';
          // echo $last_id;
          // echo '===';
          // echo $compare_id-1;
          // echo '<pagebreak />';
        }else{
          echo '<pagebreak />';
        }
      ?>




      <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
      </div>
<br>
</section>
    <!-- <h1>Pembayaran</h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


</div>
