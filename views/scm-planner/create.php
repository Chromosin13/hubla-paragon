<?php
$script = <<< JS
$('input').keydown( function (event) { //event==Keyevent
    if(event.which == 13) {
        var inputs = $(this).closest('form').find(':input:visible');
        inputs.eq( inputs.index(this)+ 1 ).focus();
        event.preventDefault(); //Disable standard Enterkey action
    }
    // event.preventDefault(); <- Disable all keys  action
});
JS;
$this->registerJs($script);
?>


<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\ScmPlanner */

$this->title = 'Input Data SCM Planner';
$this->params['breadcrumbs'][] = ['label' => 'Scm Planners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="callout callout-info">
  <h4>Notice</h4>

  <p>Jika <b>Jadwal</b> yang diupload oleh user mengandung duplikat pada <b>SNFG Komponen</b>, maka sistem akan melakukan replace jadwal tersebut dengan jadwal yang baru saja di upload </p>
</div>

<div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Upload Excel (.xls) SCM Planner</h2>
              <h6>Nama File harus <i>planner.xls</i> dan ukuran dibawah <b>2 MB</b> </h6>
              <?= Html::a('Download Template File', ['/scm-planner/download'], ['class'=>'btn btn-primary grid-button']) ?>
              <h6>Quick Tutorial : <br>
              1. <b>Download Template File</b><br>
              2. Buka dan Edit menggunakan Aplikasi Office (Libre/OpenOffice) <i>planner.xls</i><br>
              3. Pastikan yang memiliki input angka diisi hanya angka saja!<br>
              4. Format Tanggal YYYY-MM-DD ,cth : 2016-01-01<br>
              5. Upload file tersebut dengan nama file <i>planner.xls</i>, setelah selesai upload pada box dibawah<br>
              6. Akan muncul tombol <b>Submit</b> untuk menginject data ke dalam database</h6>
                    <?= \kato\DropZone::widget([
                           'options' => [
                               'url'=> 'index.php?r=scm-planner/upload',
                               'maxFilesize' => '10',
                           ],
                           'clientEvents' => [
                               'complete' => "function(file){console.log(file)}",
                                'complete' => "function(){
                                                           $('#submit-excel').show();
                                                         }",
                               'removedfile' => "function(file){alert(file.name + ' is removed')}"
                           ],
                       ]);
                    ?>
            <br></br>


            <div class="form-group" id="submit-excel">
                       <?= Html::a('Submit', ['/scm-planner/import-excel'], ['class'=>'btn btn-primary grid-button']) ?>
            </div>

            </div>
</div>

    <?php
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'options' => ['style' => 'font-size:12px;'],
            'filterModel'=>$searchModel,
            'responsiveWrap' => false,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'condensed'=>true,
            'panel'=>['type'=>'default', 'heading'=>'List Jadwal', 'beforeOptions'=>['class'=>'grid_panel_remove'],],
            'toolbar'=> false,
            'columns'=>[
                'id',
                'timestamp',
                'nomo',
                'snfg',
                'snfg_komponen',
                'week',
                'status',
                'kode_jadwal',
                'nors',
                'koitem_bulk',
                'koitem_fg',
                'odoo_code',
                'nama_bulk',
                'nama_fg',
                'sediaan',
                'streamline',
                'line_timbang',
                'line_olah_premix',
                'line_olah',
                'line_olah_2',
                'line_adjust_olah_1',
                'line_adjust_olah_2',
                'line_press',
                'line_kemas_1',
                'line_kemas_2',
                'start',
                'due',
                'leadtime',
                'besar_batch',
                'besar_lot',
                'lot_ke',
                'tglpermintaan',
                'no_formula_br',
                'jumlah_press',
                'jumlah_pcs',
                'keterangan',
                'kategori_sop',
                'kategori_detail',
                'nobatch',
                'line_terima',
                'alokasi' ,
                'npd',
                'line_end',
             ],
        ]);

    ?>

<div class="scm-planner-create"  id="form-input-manual">

    <div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Manual Input Form</h2>

                  <br></br>
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>


            </div>
    </div>
</div>

<?php
$script = <<< JS
$(function() {
    $('#submit-excel').hide();
    var currUrl = window.location.href;
    var url = new URL(currUrl);
    var obj = url.searchParams.get("obj");
    if (obj) {
      var scrollDiv = document.getElementById('form-input-manual').offsetTop;
      window.scrollTo({ top: scrollDiv, behavior: 'smooth'});
      var jsonObj = JSON.parse(atob(obj))
      document.getElementById('scmplanner-sediaan').value = jsonObj.sediaan;
      document.getElementById('scmplanner-sediaan').setAttribute('readonly', true);
      document.getElementById('scmplanner-nomo').value = '-';
      document.getElementById('scmplanner-nomo').setAttribute('readonly', true);
      document.getElementById('scmplanner-snfg').value = jsonObj.snfg_baru;
      document.getElementById('scmplanner-snfg').setAttribute('readonly', true);
      document.getElementById('scmplanner-snfg_komponen').value = jsonObj.snfg_baru;
      document.getElementById('scmplanner-snfg_komponen').setAttribute('readonly', true);
      document.getElementById('scmplanner-koitem_bulk').value = jsonObj.koitem_bulk;
      document.getElementById('scmplanner-koitem_bulk').setAttribute('readonly', true);
      document.getElementById('scmplanner-koitem_fg').value = jsonObj.snfg_baru.split('/')[0];
      document.getElementById('scmplanner-koitem_fg').setAttribute('readonly', true);
      document.getElementById('scmplanner-nama_bulk').value = jsonObj.nama_bulk;
      document.getElementById('scmplanner-nama_bulk').setAttribute('readonly', true);
      document.getElementById('scmplanner-nama_fg').value = jsonObj.nama_fg;
      document.getElementById('scmplanner-nama_fg').setAttribute('readonly', true);
      document.getElementById('scmplanner-besar_batch').value = jsonObj.berat;
      document.getElementById('scmplanner-besar_batch').setAttribute('readonly', true);
      document.getElementById('scmplanner-jumlah_pcs').value = jsonObj.jumlah_pcs;
      document.getElementById('scmplanner-jumlah_pcs').setAttribute('readonly', true);
      document.getElementById('scmplanner-nobatch').value = jsonObj.no_batch_baru;
      document.getElementById('scmplanner-nobatch').setAttribute('readonly', true);
      document.getElementById('scmplanner-streamline').value = jsonObj.streamline;
      document.getElementById('scmplanner-line_kemas_1').value = jsonObj.line_kemas_1;
      document.getElementById('scmplanner-line_kemas_2').value = jsonObj.line_kemas_2;
      document.getElementById('scmplanner-kode_jadwal').value = jsonObj.kode_jadwal;
      console.log(document.getElementById('scmplanner-koitem_fg').value);
    }else{
      console.log("Kosong");
    }
});
JS;
$this->registerJs($script);
?>
