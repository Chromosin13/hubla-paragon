<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ScmPlanner */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="scm-planner-form">



        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>

            <?= $form->field($model, 'id')->textInput()->label('Masukkan Angka Jumlah Jadwal Terakhir (Misalkan : 20 Jadwal Terakhir , isi dengan 20)') ?>
    
            </div>


        </div>
</div>

                <div id="hide-this" class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Generate' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary']) ?>
                </div>

    <?php ActiveForm::end(); ?>

<?php
$script = <<< JS

$(document).keypress(
  function(event){
    if (event.which == '13') {
      event.preventDefault();
    }
});

$('#hide-this').hide();
$('#scmplanner-id').change(function(){

    var jumlah = $('#scmplanner-id').val();

    if(jumlah>25){
        alert('Jumlah Jadwal Cukup Besar (>25)');
    }if(jumlah>=50){
        alert('Jumlah Jadwal yang Besar dapat menimbulkan Crash, Hubungi Administrator (Planner)');
        $('#hide-this').hide();
    }else{
        $('#hide-this').fadeIn();
    }
});


JS;
$this->registerJs($script);
?>