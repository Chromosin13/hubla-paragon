<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ScmPlanner */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Scm Planners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scm-planner-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'week',
                'sediaan',
                'streamline',
                'line_timbang',
                'line_olah_premix',
                'line_olah',
                'line_olah_2',
                'line_adjust_olah_1',
                'line_adjust_olah_2',
                'line_press',
                'line_kemas_1',
                'line_kemas_2',
                'start',
                'due',
                'leadtime',
                'kode_jadwal',
                'nors',
                'nomo',
                'snfg',
                'snfg_komponen',
                'koitem_bulk',
                'koitem_fg',
                'nama_bulk',
                'nama_fg',
                'besar_batch',
                'besar_lot',
                'lot_ke',
                'tglpermintaan',
                'no_formula_br',
                'jumlah_press',
                'jumlah_pcs',
                'keterangan',
                'kategori_sop',
                'kategori_detail',
                'nobatch',
                'status',
                'line_terima',
                'alokasi' ,
        ],
    ]) ?>

</div>
