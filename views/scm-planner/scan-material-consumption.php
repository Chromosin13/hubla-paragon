<?php

use app\models\SuratJalan;
use app\models\SuratJalanRincian;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\models\QcFgV2;

/* @var $this yii\web\View */
/* @var $model app\models\SuratJalanRincian */
/* @var $form yii\widgets\ActiveForm */
?>

<?php

$this->registerJsFile('js/instascan.min.js', ['position' => \yii\web\View::POS_HEAD, 'type' => 'text/javascript']);

?>

        <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <?php 
                    echo '<div class="widget-user-header bg-green">';
                
            ?>
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/clipboard-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username">Scan MO</h2>
              <h5 class="widget-user-desc">Material Consumption</h5>
            </div>
<p></p>

<?php 



    echo '<div class="col-sm-12">';
    echo  '<video id="preview"></video>';
    echo '<script type="text/javascript">';
    echo "   let scanner = new Instascan.Scanner({ video: document.getElementById('preview'),
        mirror: false // prevents the video to be mirrored
        });
        scanner.addListener('scan', function (content) {
          window.location = 'index.php?r=scm-planner/get-array-odoo&nomo='+content;
        });
        Instascan.Camera.getCameras().then(function (cameras) {
          if (cameras.length > 0) {
            // scanner.start(cameras[0]);
            if(cameras[1]){ scanner.start(cameras[1]); } else { scanner.start(cameras[0]); }
          } else {
            console.error('No cameras found.');
          }
        }).catch(function (e) {
          console.error(e);
        });
      </script>
    </div>";

?>





<?php
$script = <<< JS



JS;
$this->registerJs($script);
?>