<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScmPlannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Supply Chain Management - Planner';
$this->params['breadcrumbs'][] = $this->title;
?>

    <p>
        <?= Html::a('Input Planner Data', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


<div class="scm-planner-index">

     <?php 

        $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
                'week',
                'start',
                'due',
                'status',
                'kode_jadwal',
                'nors',
                'nomo',
                'snfg',
                'snfg_komponen',
                'koitem_bulk',
                'koitem_fg',
                'nama_bulk',
                'nama_fg',
                'sediaan',
                'streamline',
                'line_timbang',
                'line_olah_premix',
                'line_olah',
                'line_olah_2',
                'line_adjust_olah_1',
                'line_adjust_olah_2',
                'line_press',
                'line_kemas_1',
                'line_kemas_2',                
                'leadtime',
                'besar_batch',
                'besar_lot',
                'lot_ke',
                'tglpermintaan',
                'no_formula_br',
                'jumlah_press',
                'jumlah_pcs',
                'keterangan',
                'kategori_sop',
                'kategori_detail',
                'nobatch',
                'line_terima',
                'alokasi' ,
                'npd',
                'line_end',
                'odoo_code',
        ['class' => 'yii\grid\ActionColumn'],
        ];

        echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_PDF => false,
                                ExportMenu::FORMAT_HTML => false
                          ]        
    ]);?>
    <p></p>
    <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'options' => ['style' => 'font-size:12px;'],
            'filterModel'=>$searchModel,
            'responsiveWrap' => false,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'condensed'=>true,
            'panel'=>['type'=>'default', 'heading'=>'List Jadwal', 'beforeOptions'=>['class'=>'grid_panel_remove'],],
            'toolbar'=> false,
            'columns'=>[
                // [
                //     'class'=>'kartik\grid\CheckboxColumn',
                //     'headerOptions'=>['class'=>'kartik-sheet-style'],
                // ],
                // Prevent SCM from Deleting Jadwals
                ['class' => 'kartik\grid\ActionColumn', 'template' => '{update}'],
                // ['class'=>'kartik\grid\SerialColumn'],
                'id',
                'timestamp',
                'nomo',
                'snfg',
                'snfg_komponen',
                'week',
                'status',
                'kode_jadwal',
                'nors',
                'koitem_bulk',
                'koitem_fg',
                'nama_bulk',
                'nama_fg',
                'sediaan',
                'streamline',
                'line_timbang',
                'line_olah_premix',
                'line_olah',
                'line_olah_2',
                'line_adjust_olah_1',
                'line_adjust_olah_2',
                'line_press',
                'line_kemas_1',
                'line_kemas_2',
                'start',
                'due',
                'leadtime',
                'besar_batch',
                'besar_lot',
                'lot_ke',
                'tglpermintaan',
                'no_formula_br',
                'jumlah_press',
                'jumlah_pcs',
                'keterangan',
                'kategori_sop',
                'kategori_detail',
                'nobatch',
                'line_terima',
                'alokasi' ,
                'npd',
                'line_end',
                'odoo_code',
             ],
        ]);
    
    ?>

</div>
