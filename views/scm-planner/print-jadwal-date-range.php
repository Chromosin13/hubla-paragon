<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\ScmPlanner */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="scm-planner-form">



        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>

              <?php 

                        echo $form->field($model, 'sediaan')->widget(Select2::classname(), [
                            'data' => $list_sediaan,
                            'options' => ['placeholder' => 'Select Sediaan'],
                            // 'pluginOptions' => [
                            //     'templateResult' => new JsExpression('format'),
                            //     'escapeMarkup' => $escape,
                            //     'allowClear' => true,
                            // ],
                        ])->label('Sediaan');
                        

                    ?>
             <?= 
                DatePicker::widget([
                'model' => $model,
                'form' => $form,
                'name' => 'tanggal_start',
                'attribute' => 'tanggal_start', 
                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                'options' => ['placeholder' => 'Tanggal Awal ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                    ]
                ]);
            ?>

            <?= 
                DatePicker::widget([
                'model' => $model,
                'form' => $form,
                'name' => 'tanggal_stop',
                'attribute' => 'tanggal_stop', 
                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                'options' => ['placeholder' => 'Tanggal Akhir ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                    ]
                ]);
            ?>



    
            </div>


        </div>
</div>

                <div id="hide-this" class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Generate' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary']) ?>
                </div>

    <?php ActiveForm::end(); ?>

<?php
$script = <<< JS

$(document).keypress(
  function(event){
    if (event.which == '13') {
      event.preventDefault();
    }
});

$('#hide-this').hide();
$('#scmplanner-tanggal_stop').change(function(){
    var tanggal_start = $('#scmplanner-tanggal_start').val();
    var tanggal_stop = $('#scmplanner-tanggal_stop').val(); 
    var sediaan = $('#scmplanner-sediaan').val();

    if(tanggal_start===""){
        alert('Tanggal Start Belum Diisi');
        $('#hide-this').hide();
    }else{

        if(tanggal_start>tanggal_stop){
            alert('Tanggal Start harus <= Tanggal Stop');
            $('#hide-this').hide();
        }else if(sediaan){
            $('#hide-this').fadeIn();            
        }else{
            alert('Sediaan Harus Diisi');
            $('#hide-this').hide();
        }        
    }
});

$('#scmplanner-tanggal_start').change(function(){
    var tanggal_stop = $('#scmplanner-tanggal_stop').val();
    var tanggal_start = $('#scmplanner-tanggal_start').val();
    var sediaan = $('#scmplanner-sediaan').val(); 
    
    if(tanggal_stop&&tanggal_start&&sediaan){
        $('#hide-this').fadeIn();
    }
});


JS;
$this->registerJs($script);
?>