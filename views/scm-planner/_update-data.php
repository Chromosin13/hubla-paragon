<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\data\SqlDataProvider;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $bom app\models\ScmPlanner */
/* @var $form yii\widgets\ActiveForm */
?>




<div class="scm-planner-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- product -->
    <div class="form-group">
    <label class="control-label" for="bom-product_id">Bill Of Material</label>
    <?php echo '<input type="text" id="bom-product_id" class="form-control input-lg" placeholder="';
    // echo $bom['product_uom_qty'];
    echo $bom[0]['product_id'];
    echo '"
    disabled="">'; ?>

    <div class="help-block"></div>
    </div>

    <!-- standar -->
    <div class="form-group">
    <label class="control-label" for="bom-product_uom_qty">Standard Quantity</label>
    <?php echo '<input type="text" id="bom-product_uom_qty" class="form-control input-lg" placeholder="';
    // echo $bom['product_uom_qty'];
    echo $bom[0]['product_uom_qty'];
    echo '"
    disabled="">'; ?>

    <div class="help-block"></div>
    </div>

    <div id="hide-this">
        <!-- Lanjutan -->
        <div class="form-group">
        <label class="control-label" for="bom-lanjutan">Lanjutan</label>
        <?php echo '<input type="text" id="bom-lanjutan" name="lanjutan" class="form-control input-lg" value="';
        // echo $bom['product_uom_qty'];
        echo $bom[0]['lanjutan'];
        echo '">'; ?>
        </div>

        <!-- Qty Sebelumnya -->
        <div class="form-group">
        <label class="control-label" for="bom-qty">Qty Sebelumnya</label>
        <?php echo '<input type="text" id="bom-qty" name="qty" class="form-control input-lg" value="';
        // echo $bom['product_uom_qty'];
        echo $bom[0]['qty'];
        echo '">'; ?>

        <div class="help-block"></div>
        </div>
    </div>

    <!-- isian -->
    <div class="form-group">
    <label class="control-label" for="bom-qty_done">(+) Quantity</label>
    <?php echo '<input type="number" step="0.00001" id="qty_done" name="actual_qty" class="form-control input-lg" placeholder="';
    // echo $bom['product_uom_qty'];
    // echo $bom[0]['qty'];
    echo 'Isi dengan jumlah yang di proses pada shift ini';
    echo '">'; ?>

    <div class="help-block"></div>
    </div>

    <div class="zoom">
        <button type="submit" class="btn btn-outline btn-block">UPDATE</button>
    </div>
    <?php ActiveForm::end(); ?>


</div>



<?php
$script = <<< JS

// AutoFocus Nomo Field

// document.getElementById("qty_done").focus();

$('#hide-this').hide();


JS;
$this->registerJs($script);
?>




