<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScmPlannerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="scm-planner-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'sediaan') ?>

    <?= $form->field($model, 'streamline') ?>

    <?= $form->field($model, 'line_timbang') ?>

    <?= $form->field($model, 'line_olah') ?>

    <?php echo $form->field($model, 'line_olah_2') ?>

    <?php echo $form->field($model, 'line_press') ?>

    <?php echo $form->field($model, 'line_kemas_1') ?>

    <?php echo $form->field($model, 'line_kemas_2') ?>

    <?php echo $form->field($model, 'start') ?>

    <?php echo $form->field($model, 'due') ?>

    <?php echo $form->field($model, 'leadtime') ?>

    <?php echo $form->field($model, 'kode_jadwal') ?>

    <?php echo $form->field($model, 'snfg') ?>

    <?php echo $form->field($model, 'koitem_bulk') ?>

    <?php echo $form->field($model, 'koitem_fg') ?>

    <?php echo $form->field($model, 'nama_bulk') ?>

    <?php echo $form->field($model, 'nama_fg') ?>

    <?php echo $form->field($model, 'besar_batch') ?>

    <?php echo $form->field($model, 'besar_lot') ?>

    <?php echo $form->field($model, 'lot') ?>

    <?php echo $form->field($model, 'tglpermintaan') ?>

    <?php echo $form->field($model, 'jumlah') ?>

    <?php echo $form->field($model, 'keterangan') ?>

    <?php echo $form->field($model, 'kategori_sop') ?>

    <?php echo $form->field($model, 'kategori_detail') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
