<?php
/* @var $this yii\web\View */
if (class_exists('backend\assets\AppAsset')) {
    backend\assets\AppAsset::register($this);
} else {
    app\assets\AppAsset::register($this);
}

dmstr\web\AdminLteAsset::register($this);
?>
<style media="screen">
  @import url('https://fonts.googleapis.com/css2?family=Creepster&display=swap');
  @import url('https://fonts.googleapis.com/css2?family=Eater&display=swap');
  @import url('https://fonts.googleapis.com/css2?family=Nosifer&display=swap');
  html,body{
    width: 100%!important;
    height: 100%!important;
  }
  body{
    background: url("../web/images/halloween.jpeg")!important;
    background-color: black!important;
    background-size: cover!important;
    background-repeat: no-repeat!important;
  }

  body > .input{
    position: absolute;
    top: 50%;
    width: 100%;
    transform: translateY(-50%);
  }

  .swal-button--confirm{
    display: none!important
  }

  .welcome{
    font-family: 'Nosifer', cursive;
    font-size: 8vh;
    color:#CC0019;
    width: 100%;
  }

  ::placeholder{
    color:white
  }

  .profile-pic{
    width: 40vh;
    height: 40vh;
    object-fit: cover;
    margin-top: 3vh;
    margin-bottom: 3vh;
    border: 5px solid black;
  }

  .modal-content{
    background-color: transparent;
    position: absolute!important;
    top: 50vh!important;
    width: 100%!important;
    transform: translateY(-50%)!important;
    padding: 0;
    margin: 0;
    text-align: center;
  }

  .data{
    font-family: 'Creepster', cursive;
    font-size: 7vh!important
  }

  .modal-dialog{
    width: 100%
  }

  input{
    background: transparent;
    display: block;
    width: 40%;
    font-size: 2rem;
    padding: 2rem;
    border:0.4rem solid white;
    border-radius: 2rem;
    color:white;
    font-family: 'Creepster', cursive;
  }

  .modal-content > *{
    color:#CC0019;
  }
</style>
<script type="text/javascript">
    function customAlert(type,title,message){
      swal({
        closeOnClickOutside: false,
        title: title,
        text: message,
        icon: type,
        backdrop:true,
        button: "Mengerti!",
      });
    }

    function isFilled(){
        var findId = document.getElementById('find-field').value;
        console.log(findId);
        if (findId) {
          $("#btn-find").click();
        }else{
          customAlert('warning','Gagal','Silahkan scan QR terlebih dahulu')
        }
    }

    function onErrorLoad(){
      document.getElementById('profile-pic').src = "../web/images/dracula.png";
    }
</script>
<div class="input">

    <center>
        <input id="find-field" type="text" name="" placeholder="Find Someone!" value="" autofocus onchange="isFilled()">
        <button id="btn-find" type="button" class="btn btn-primary" style="display:none">Large modal</button>
    </center>

</div>

<div class="modal fade bs-example-modal-lg" id="modal-form" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <h1 class="welcome">WELCOME ABOARD</h1>
        <img id="profile-pic" class="profile-pic" src="../web/images/dracula.png" alt="" onerror="onErrorLoad()">
        <!-- <h1 id="profile-id" class="data"></h1> -->
        <h1 id="profile-name" class="data"></h1>
        <h1 id="profile-dept" class="data"></h1>
        <h1 id="profile-status" class="data"></h1>
    </div>
  </div>
</div>

<?php
$script = <<< JS
  $(document).ready(function() {

    $('#modal-form').on('hidden.bs.modal', function (e) {
      $(this)
        .find("img")
           .attr('src','../web/images/dracula.png')
           .end();
    })

    $("#find-field").keyup(function(event) {
        if (event.keyCode === 13) {
            var id_user = document.getElementById('find-field').value;
            if (!id_user) {
              customAlert('warning','Gagal','Silahkan scan QR terlebih dahulu')
              document.getElementById("find-field").focus();
              setTimeout(function() {
                document.getElementsByClassName("swal-button--confirm")[0].click();
              }, 2000);
            }
        }
    });

    $("#btn-find").click(function(){
      var id_user = document.getElementById('find-field').value;
      $.getJSON('index.php?r=welcome-page/get-data-by-id&id='+id_user,function(data){
        if (data) {
          $.getJSON('index.php?r=welcome-page/upd-timestamp&id='+id_user,function(update){
            if (update == 1) {
              console.log(data);
              $('#modal-form').modal('show');
              document.getElementById('profile-pic').src = "../web/images/profile/"+data.id+".JPG";
              // document.getElementById('profile-id').innerHTML = data.id;
              document.getElementById('profile-name').innerHTML = data.nama_lengkap;
              document.getElementById('profile-dept').innerHTML = data.departemen ?? '-';
              document.getElementById('profile-status').innerHTML = data.status ?? '-';
              document.getElementById('find-field').style.display = 'none';
              setTimeout(function() {
                $('#modal-form').modal('hide');
                document.getElementById('find-field').style.display = 'block';
                $("#find-field").val(null);
                document.getElementById("find-field").focus();
              }, 7000);
            }else{
              customAlert('warning','Gagal','Terjadi kesalahan pada sistem!')
              $("#find-field").val(null);
              document.getElementById('profile-pic').src = "../web/images/dracula.png";
              document.getElementById("find-field").focus();
              setTimeout(function() {
                document.getElementsByClassName("swal-button--confirm")[0].click();
              }, 2000);
            }
          })
        }else{
          customAlert('warning','Gagal','ID tidak dapat ditemukan!')
          $("#find-field").val(null);
          document.getElementById('profile-pic').src = "../web/images/dracula.png";
          document.getElementById("find-field").focus();
          setTimeout(function() {
            document.getElementsByClassName("swal-button--confirm")[0].click();
          }, 2000);
        }
      })
    })
  })
JS;
$this->registerJs($script);
?>
