<?php
/* @var $this yii\web\View */
if (class_exists('backend\assets\AppAsset')) {
    backend\assets\AppAsset::register($this);
} else {
    app\assets\AppAsset::register($this);
}

dmstr\web\AdminLteAsset::register($this);
?>
<style media="screen">

  @import url('https://fonts.googleapis.com/css2?family=Nosifer&display=swap');
  @import url('https://fonts.googleapis.com/css2?family=Creepster&display=swap');
  @import url('https://fonts.googleapis.com/css2?family=Jolly+Lodger&display=swap');

  html,body, .contain, .contain > * , .title > *{
    background-color: black;
    height: 100%;
  }

  .btn-reset{
  }

  .title > *{
    font-family: 'Nosifer', cursive!important;
    font-size: 7vh;
    color:#CC0019;
    width: 100%;
    margin-bottom: 2rem;
  }

  .total{
    font-family: 'Creepster', cursive;
    font-size: 9vh!important;
    color:#CC0019;
  }

  .profile-pic{
    /* width: 100%!important;
    height: 100%!important; */
      width: 100%!important;
      height: 17vh!important;
    object-fit: cover;
    /* border:1px solid red; */
  }

  .grid-container{
    display: grid;
    grid-template-columns:repeat(15, 1fr);
    grid-auto-rows: 1fr;
    border:1px solid red
  }

  .grid-container-inner{
    display: grid;
    grid-template-columns:2fr 1fr;
    width: 100%;
    height: 100%!important
  }

  .grid-container-inner-row{
    display: grid;
    grid-template-columns:1fr;
    padding: 2rem;
    grid-row-gap: 2rem;
    width: 100%;
    height: 100%!important
    border:1px solid red
  }

  .btn{
    font-weight: bold;
    font-size: 2vh;
  }

  .bg-warning{
    background-color: #FFCC00;
  }

  .bg-success{
    background-color: #40BF75;
  }

  .grid-item{
    border:1px solid red;
    /* padding: 2vh; */
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .grid-item-last{
    grid-column-start: 4;
    grid-column-end: 16;
  }

  .swal-footer{
    /* display: grid;
    grid-template-columns: repeat(auto-fit,1fr); */
    display: flex;
    align-items: stretch;
  }

  .swal-button-container{
    display: inline-flex;
    flex: 1;
  }

  .swal-button{
    width: 100%;
  }

  .swal-button--confirm{
    background-color: #40BF75;
  }

  .swal-button--confirm:hover{
    background-color: #33995E!important;
  }

  .modal-content{
    position: absolute!important;
    top: 50vh!important;
    width: 100%!important;
    background-color: transparent;
    transform: translateY(-55%)!important;
    padding: 0;
    margin: 0;
    text-align: center;
  }

  .modal-text-1{
    position: absolute;
    width: 100%;
    text-align: center;
    top: 18%;
    padding: 0;
    margin: 0;
    color:#32cd32;
    font-size: 9vh;
    font-family: 'Jolly Lodger', cursive;
  }

  .modal-text-2{
    position: absolute;
    bottom: 15%;
    width: 100%;
    text-align: center;
    padding: 0;
    margin: 0;
    color:#CC0019;
    font-size: 9vh;
    font-family: 'Jolly Lodger', cursive;
  }

  .modal-img{
    width: 80%;
    padding: 0;
    margin: 0;
  }

</style>
<script type="text/javascript">

    window.setInterval(function(){
      loadData();
    }, 1000);

    function loadData(){
      $.ajax({
        type: 'get',
        url: 'index.php?r=welcome-page/get-all-peserta',
        data: {
        },
        success: function (response) {
          // We get the element having id of display_info and put the response inside it
          var jsonObj = JSON.parse(atob(response))
          var grid_element = document.getElementById('grid-container');
          for (var i = 0; i < jsonObj.length; i++) {
            if (jsonObj[i].timestamp == null || jsonObj[i].timestamp == '') {
              if (!document.getElementById('id-grid-item-'+i)) {
                var gridChild = document.createElement("span");
                gridChild.className = 'profile-pic grid-item';
                gridChild.id = 'id-grid-item-'+i;
                gridChild.innerHTML = '<img src="../web/images/dracula.png" class="" style="width:50%;height:auto">'
                grid_element.appendChild(gridChild)
              }
            }else{
              if ((!document.getElementById('profile-pic-'+i)) && (!document.getElementById('id-grid-item-'+i))) {
                var gridChild = document.createElement("img");
                gridChild.className = 'profile-pic grid-item';
                gridChild.id = 'profile-pic-'+i;
                gridChild.alt = jsonObj[i].nama_lengkap;
                gridChild.onerror = function(){
                  this.src = "../web/images/dracula.png"
                }
                grid_element.appendChild(gridChild)
                gridChild.src = '../web/images/profile/'+jsonObj[i].id+'.JPG';
              }else{
                if (document.getElementById('id-grid-item-'+i)) {
                  console.log('true');
                  var spanChild = document.getElementById('id-grid-item-'+i)
                  var index = Array.prototype.indexOf.call(spanChild.parentNode.children, spanChild);
                  var gridChild = document.createElement("img");
                  gridChild.className = 'profile-pic grid-item';
                  gridChild.id = 'profile-pic-'+i;
                  gridChild.alt = jsonObj[i].nama_lengkap;
                  gridChild.onerror = function(){
                    this.src = "../web/images/dracula.png"
                  }
                  grid_element.insertBefore(gridChild, grid_element.children[index]);
                  grid_element.removeChild(spanChild);
                  gridChild.src = '../web/images/profile/'+jsonObj[i].id+'.JPG';
                }
              }
            }
          }
          if (!document.getElementById('grid-item-last')) {
            var grid_item_last = document.createElement("div");
            grid_item_last.className = 'grid-item-last';
            grid_item_last.id = 'grid-item-last';
            grid_item_last.innerHTML = "<div class='grid-container-inner'><span class='grid-item total' id='total-attendees'></span>"+
            "<div class='grid-container-inner-row'><button class='btn btn-reset bg-warning' id='btn-reset' onclick='resetAttendance()'>Reset Attendance</button>"+
            "<button class='btn btn-reset bg-success' id='btn-finish' data-toggle='modal' data-target='#modal-form'>Close Attendance</button></div></div>";
            grid_element.appendChild(grid_item_last)
          }
          $.ajax({
            type: 'get',
            url: 'index.php?r=welcome-page/count-attendees',
            data: {
            },
            success: function (response) {
              if (response == 0) {
                response = '-'
              }
              document.getElementById('total-attendees').innerHTML = 'Total Attendees: '+response
            }
          });
        }
      });
    }

    function customAlert(type,title,message){
      swal({
        closeOnClickOutside: false,
        title: title,
        text: message,
        icon: type,
        backdrop:true,
        button: "Mengerti!",
      });
    }

    function resetAttendance(){
      swal({
        html:true,
        title: "Confirmation",
        text: "Are you sure want to reset current attendances?",
        icon: "warning",
        buttons: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
      })
      .then((res) => {
        if (res) {
          $.ajax({
            type: 'get',
            url: 'index.php?r=welcome-page/reset-attendance',
            data: {
            },
            success: function (response) {
              if (response) {
                swal({
                  title: "Success",
                  text: "Reset attendances is successful.",
                  icon: "success",
                })
                .then((res) => {
                  location.reload();
                })
              }else{
                customAlert("error",'Error',"Something went wrong.")
              }
            }
          });
        }
      })
    }
</script>

<div class="contain">
    <div class="title text-center">
        <h1>List of Attendees</h1>
    </div>
    <div class="grid-container" id="grid-container">
    </div>
</div>
<div class="modal fade bs-example-modal-lg" id="modal-form" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <img class="modal-img" src="../web/images/city-ghost.png" alt="">
        <!-- <h1 class="modal-text-1">COMPLETED</h1> -->
        <h1 class="modal-text-2">COMPLETED<br>LET'S START HUNT THE WASTE!</h1>
    </div>
  </div>
</div>

<?php
$script = <<< JS
  $(document).ready(function() {

  })
JS;
$this->registerJs($script);
?>
