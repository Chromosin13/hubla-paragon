<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KonfigurasiStandarItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Konfigurasi Standar Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="konfigurasi-standar-item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Konfigurasi Standar Item', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ks_id',
            'kode',
            'nama',
            'jenis',
            'proses',
            'mpq',
            'jumlah_operator',
            'line_id',
            'zona',
            'output',
            'print_flag',
            'kode_streamline',
            'std_leadtime',
            'pdt',
            'last_update',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
