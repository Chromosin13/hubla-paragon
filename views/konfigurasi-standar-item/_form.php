<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KonfigurasiStandarItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="konfigurasi-standar-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ks_id')->textInput() ?>

    <?= $form->field($model, 'kode')->textInput() ?>

    <?= $form->field($model, 'nama')->textInput() ?>

    <?= $form->field($model, 'jenis')->textInput() ?>

    <?= $form->field($model, 'proses')->textInput() ?>

    <?= $form->field($model, 'mpq')->textInput() ?>

    <?= $form->field($model, 'jumlah_operator')->textInput() ?>

    <?= $form->field($model, 'line_id')->textInput() ?>

    <?= $form->field($model, 'zona')->textInput() ?>

    <?= $form->field($model, 'output')->textInput() ?>

    <?= $form->field($model, 'print_flag')->textInput() ?>

    <?= $form->field($model, 'kode_streamline')->textInput() ?>

    <?= $form->field($model, 'std_leadtime')->textInput() ?>

    <?= $form->field($model, 'pdt')->textInput() ?>

    <?= $form->field($model, 'last_update')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
