<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KonfigurasiStandarItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="konfigurasi-standar-item-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ks_id') ?>

    <?= $form->field($model, 'kode') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'jenis') ?>

    <?php // echo $form->field($model, 'proses') ?>

    <?php // echo $form->field($model, 'mpq') ?>

    <?php // echo $form->field($model, 'jumlah_operator') ?>

    <?php // echo $form->field($model, 'line_id') ?>

    <?php // echo $form->field($model, 'zona') ?>

    <?php // echo $form->field($model, 'output') ?>

    <?php // echo $form->field($model, 'print_flag') ?>

    <?php // echo $form->field($model, 'kode_streamline') ?>

    <?php // echo $form->field($model, 'std_leadtime') ?>

    <?php // echo $form->field($model, 'pdt') ?>

    <?php // echo $form->field($model, 'last_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
