<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KonfigurasiStandarItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="konfigurasi-standar-item-index">

    <div class="box box-widget widget-user-2">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-red">
      <div class="widget-user-image">
        <img class="img-circle" src="images/gears.png" alt="User Avatar">
      </div>
      <!-- /.widget-user-image -->
      <h2 class="widget-user-username"><?=$koitem?></h2>
      <h5 class="widget-user-desc"><button class="btn btn-danger" onclick="goBack()">Go Back</button></h5>
    </div>
    <div class="box-footer">
            <?= GridView::widget([
                'dataProvider'=>$dataProvider,
                'filterModel'=>$searchModel,
                'responsiveWrap' => false,
                'floatHeader'=>'true',
                'floatHeaderOptions' => ['position' => 'absolute',],
                'showPageSummary'=>true,
                'pjax'=>true,
                'striped'=>true,
                'hover'=>true,
                'condensed'=>true,
                'panel'=>['type'=>'danger', 'heading'=>'Konfigurasi'],
                'formatter' => [
                    'class' => 'yii\i18n\Formatter',
                    'timeZone' => 'Asia/Jakarta'
                ],
                'toolbar' => [
                ],
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn'],

                    'id',
                    'kode',
                    'nama',
                    'jenis',
                    'proses',
                    'mpq',
                    'jumlah_operator',
                    'line_id',
                    'zona',
                    'output',
                    'print_flag',
                    'kode_streamline',
                    'std_leadtime',
                    'pdt',
                    'last_update',

                    ['class' => 'kartik\grid\ActionColumn'],
                ],
            ]); ?>
    </div>
  </div>

    <p></p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


</div>

<script>
function goBack() {
  window.history.back();
}
</script>