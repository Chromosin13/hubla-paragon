<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KonfigurasiStandarItem */

$this->title = 'Create Konfigurasi Standar Item';
$this->params['breadcrumbs'][] = ['label' => 'Konfigurasi Standar Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="konfigurasi-standar-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
