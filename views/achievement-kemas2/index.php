<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AchievementKemas2Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Achievement Kemas2s';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-kemas2-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box-body">
                <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">

                         <?php

                          $dataASTD= Yii::$app->db->createCommand(
                                          "select
                                              round((count(case when status_leadtime='ACHIEVED' then 1 end)::numeric/count(status_leadtime)::numeric)*100,1)::numeric as astd,
                                              tanggal_start
                                          from achievement_kemas2
                                          where tanggal_start between current_date - interval '7 day' and current_date and jenis_kemas='PACKING'
                                          group by tanggal_start
                                          order by tanggal_start asc
                          ")->queryAll();


                          echo Highcharts::widget([
                                                    'scripts' => [
                                                        'modules/exporting',
                                                        'themes/grid-light',
                                                    ],
                                                   'options' => [
                                                      'title' => ['text' => 'ASTD PACKING'],
                                                      'xAxis' => [
                                                         'categories' => new SeriesDataHelper($dataASTD,['tanggal_start']),
                                                      ],
                                                      'yAxis' => [
                                                         'title' => ['text' => '(%) Achievement Standar Pengolahan ASTD']
                                                      ],
                                                       'plotOptions' => [
                                                                        'line' => [
                                                                            'dataLabels' => [
                                                                                                'enabled' => true
                                                                                            ],
                                                                            'enableMouseTracking' => true
                                                                            ]
                                                                        ],
                                                      'series' => [
                                                         ['name' => 'Proses', 'data' => 
                                                         new SeriesDataHelper($dataASTD,['astd:float']),
                                                         ]
                                                      ]
                                                   ]
                                                ]);

                      ?>



                    </div>

                                        <div class="col-md-6">

                         <?php

                          $dataASTD= Yii::$app->db->createCommand(
                                          "select
                                              round((count(case when status_leadtime='ACHIEVED' then 1 end)::numeric/count(status_leadtime)::numeric)*100,1)::numeric as astd,
                                              tanggal_start
                                          from achievement_kemas2
                                          where tanggal_start between current_date - interval '7 day' and current_date and jenis_kemas='PRESS+FILLING+PACKING'
                                          group by tanggal_start
                                          order by tanggal_start asc
                          ")->queryAll();


                          echo Highcharts::widget([
                                                    'scripts' => [
                                                        'modules/exporting',
                                                        'themes/grid-light',
                                                    ],
                                                   'options' => [
                                                      'title' => ['text' => 'ASTD PRESS+FILLING+PACKING'],
                                                      'xAxis' => [
                                                         'categories' => new SeriesDataHelper($dataASTD,['tanggal_start']),
                                                      ],
                                                      'yAxis' => [
                                                         'title' => ['text' => '(%) Achievement Standar Pengolahan ASTD']
                                                      ],
                                                       'plotOptions' => [
                                                                        'line' => [
                                                                            'dataLabels' => [
                                                                                                'enabled' => true
                                                                                            ],
                                                                            'enableMouseTracking' => true
                                                                            ]
                                                                        ],
                                                      'series' => [
                                                         ['name' => 'Proses', 'data' => 
                                                         new SeriesDataHelper($dataASTD,['astd:float']),
                                                         ]
                                                      ]
                                                   ]
                                                ]);

                      ?>



                    </div>
                                        <div class="col-md-6">

                         <?php

                          $dataASTD= Yii::$app->db->createCommand(
                                          "select
                                              round((count(case when status_leadtime='ACHIEVED' then 1 end)::numeric/count(status_leadtime)::numeric)*100,1)::numeric as astd,
                                              tanggal_start
                                          from achievement_kemas2
                                          where tanggal_start between current_date - interval '7 day' and current_date and jenis_kemas='FILLING-PACKING_INLINE'
                                          group by tanggal_start
                                          order by tanggal_start asc
                          ")->queryAll();


                          echo Highcharts::widget([
                                                    'scripts' => [
                                                        'modules/exporting',
                                                        'themes/grid-light',
                                                    ],
                                                   'options' => [
                                                      'title' => ['text' => 'ASTD FILLING-PACKING_INLINE'],
                                                      'xAxis' => [
                                                         'categories' => new SeriesDataHelper($dataASTD,['tanggal_start']),
                                                      ],
                                                      'yAxis' => [
                                                         'title' => ['text' => '(%) Achievement Standar Pengolahan ASTD']
                                                      ],
                                                       'plotOptions' => [
                                                                        'line' => [
                                                                            'dataLabels' => [
                                                                                                'enabled' => true
                                                                                            ],
                                                                            'enableMouseTracking' => true
                                                                            ]
                                                                        ],
                                                      'series' => [
                                                         ['name' => 'Proses', 'data' => 
                                                         new SeriesDataHelper($dataASTD,['astd:float']),
                                                         ]
                                                      ]
                                                   ]
                                                ]);

                      ?>



                    </div>
                                        <div class="col-md-6">

                         <?php

                          $dataASTD= Yii::$app->db->createCommand(
                                          "select
                                              round((count(case when status_leadtime='ACHIEVED' then 1 end)::numeric/count(status_leadtime)::numeric)*100,1)::numeric as astd,
                                              tanggal_start
                                          from achievement_kemas2
                                          where tanggal_start between current_date - interval '7 day' and current_date and jenis_kemas='FLAME-PACKING_INLINE'
                                          group by tanggal_start
                                          order by tanggal_start asc
                          ")->queryAll();


                          echo Highcharts::widget([
                                                    'scripts' => [
                                                        'modules/exporting',
                                                        'themes/grid-light',
                                                    ],
                                                   'options' => [
                                                      'title' => ['text' => 'ASTD FLAME-PACKING_INLINE'],
                                                      'xAxis' => [
                                                         'categories' => new SeriesDataHelper($dataASTD,['tanggal_start']),
                                                      ],
                                                      'yAxis' => [
                                                         'title' => ['text' => '(%) Achievement Standar Pengolahan ASTD']
                                                      ],
                                                       'plotOptions' => [
                                                                        'line' => [
                                                                            'dataLabels' => [
                                                                                                'enabled' => true
                                                                                            ],
                                                                            'enableMouseTracking' => true
                                                                            ]
                                                                        ],
                                                      'series' => [
                                                         ['name' => 'Proses', 'data' => 
                                                         new SeriesDataHelper($dataASTD,['astd:float']),
                                                         ]
                                                      ]
                                                   ]
                                                ]);

                      ?>



                    </div>
                    
                </div>
                </div>
    </div>


     <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'primary', 'heading'=>'Achievement Line Kemas 1'],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                    'row_number',
                    'nomo',
                    'snfg',
                    'snfg_komponen',
                    'nama_fg',
                    'tanggal_stop',
                    'tanggal_start',
                    'nama_line',
                    'sediaan',
                    'jenis_kemas',
                    'leadtime',
                    'std_leadtime',
                    'status_leadtime:ntext',
                    'achievement_rate',
                    'koitem_bulk',
                    'koitem_fg',
                    'nama_bulk',
                ],
        ]);
    
    ?>


</div>
