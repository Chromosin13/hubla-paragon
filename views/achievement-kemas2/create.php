<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AchievementKemas2 */

$this->title = 'Create Achievement Kemas2';
$this->params['breadcrumbs'][] = ['label' => 'Achievement Kemas2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-kemas2-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
