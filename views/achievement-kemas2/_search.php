<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementKemas2Search */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="achievement-kemas2-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'row_number') ?>

    <?= $form->field($model, 'nomo') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'snfg_komponen') ?>

    <?= $form->field($model, 'nama_fg') ?>

    <?php // echo $form->field($model, 'tanggal_stop') ?>

    <?php // echo $form->field($model, 'tanggal_start') ?>

    <?php // echo $form->field($model, 'nama_line') ?>

    <?php // echo $form->field($model, 'sediaan') ?>

    <?php // echo $form->field($model, 'jenis_kemas') ?>

    <?php // echo $form->field($model, 'leadtime') ?>

    <?php // echo $form->field($model, 'std_leadtime') ?>

    <?php // echo $form->field($model, 'status_leadtime') ?>

    <?php // echo $form->field($model, 'achievement_rate') ?>

    <?php // echo $form->field($model, 'koitem_bulk') ?>

    <?php // echo $form->field($model, 'koitem_fg') ?>

    <?php // echo $form->field($model, 'nama_bulk') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
