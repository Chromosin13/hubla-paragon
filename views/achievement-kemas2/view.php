<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementKemas2 */

$this->title = $model->row_number;
$this->params['breadcrumbs'][] = ['label' => 'Achievement Kemas2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-kemas2-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->row_number], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->row_number], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'row_number',
            'nomo',
            'snfg',
            'snfg_komponen',
            'nama_fg',
            'tanggal_stop',
            'tanggal_start',
            'nama_line',
            'sediaan',
            'jenis_kemas',
            'leadtime',
            'std_leadtime',
            'status_leadtime:ntext',
            'achievement_rate',
            'koitem_bulk',
            'koitem_fg',
            'nama_bulk',
        ],
    ]) ?>

</div>
