<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementKemas2 */

$this->title = 'Update Achievement Kemas2: ' . $model->row_number;
$this->params['breadcrumbs'][] = ['label' => 'Achievement Kemas2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->row_number, 'url' => ['view', 'id' => $model->row_number]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="achievement-kemas2-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
