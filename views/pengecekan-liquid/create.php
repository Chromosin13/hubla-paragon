<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PengecekanLiquid */

$this->title = 'Create Pengecekan Liquid';
$this->params['breadcrumbs'][] = ['label' => 'Pengecekan Liquids', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengecekan-liquid-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
