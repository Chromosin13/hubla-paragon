<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PengecekanLiquidSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengecekan Liquids';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengecekan-liquid-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pengecekan Liquid', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'uji_ketahanan_seal',
            'hasil_seal',
            'kondisi_fisik',
            'kes_warna_dusat',
            // 'kes_identitas_dusat',
            // 'kes_warna_paper',
            // 'kes_identitas_paper',
            // 'uji_torsi_cap',
            // 'uji_kekencangan_cap',
            // 'hasil_shrink_wrap',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
