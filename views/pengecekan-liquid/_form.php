<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PengecekanLiquid */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengecekan-liquid-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'uji_ketahanan_seal')->textInput() ?>

    <?= $form->field($model, 'hasil_seal')->textInput() ?>

    <?= $form->field($model, 'kondisi_fisik')->textInput() ?>

    <?= $form->field($model, 'kes_warna_dusat')->textInput() ?>

    <?= $form->field($model, 'kes_identitas_dusat')->textInput() ?>

    <?= $form->field($model, 'kes_warna_paper')->textInput() ?>

    <?= $form->field($model, 'kes_identitas_paper')->textInput() ?>

    <?= $form->field($model, 'uji_torsi_cap')->textInput() ?>

    <?= $form->field($model, 'uji_kekencangan_cap')->textInput() ?>

    <?= $form->field($model, 'hasil_shrink_wrap')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
