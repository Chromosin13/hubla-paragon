<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PengecekanLiquid */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pengecekan Liquids', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengecekan-liquid-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'uji_ketahanan_seal',
            'hasil_seal',
            'kondisi_fisik',
            'kes_warna_dusat',
            'kes_identitas_dusat',
            'kes_warna_paper',
            'kes_identitas_paper',
            'uji_torsi_cap',
            'uji_kekencangan_cap',
            'hasil_shrink_wrap',
        ],
    ]) ?>

</div>
