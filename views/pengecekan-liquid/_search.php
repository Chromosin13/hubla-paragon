<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PengecekanLiquidSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengecekan-liquid-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'uji_ketahanan_seal') ?>

    <?= $form->field($model, 'hasil_seal') ?>

    <?= $form->field($model, 'kondisi_fisik') ?>

    <?= $form->field($model, 'kes_warna_dusat') ?>

    <?php // echo $form->field($model, 'kes_identitas_dusat') ?>

    <?php // echo $form->field($model, 'kes_warna_paper') ?>

    <?php // echo $form->field($model, 'kes_identitas_paper') ?>

    <?php // echo $form->field($model, 'uji_torsi_cap') ?>

    <?php // echo $form->field($model, 'uji_kekencangan_cap') ?>

    <?php // echo $form->field($model, 'hasil_shrink_wrap') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
