<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KonfigurasiStdLeadtimeHistory */

$this->title = 'Update Konfigurasi Std Leadtime History: ' . $model->uid;
$this->params['breadcrumbs'][] = ['label' => 'Konfigurasi Std Leadtime Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uid, 'url' => ['view', 'id' => $model->uid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="konfigurasi-std-leadtime-history-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
