<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KonfigurasiStdLeadtimeHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Konfigurasi Std Leadtime Histories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="konfigurasi-std-leadtime-history-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'koitem',
            'koitem_bulk',
            'naitem',
            'std_type',
            'last_update',
            'jenis',
            'value',
            'uid',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
