<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KonfigurasiStdLeadtimeHistory */

$this->title = 'Create Konfigurasi Std Leadtime History';
$this->params['breadcrumbs'][] = ['label' => 'Konfigurasi Std Leadtime Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="konfigurasi-std-leadtime-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
