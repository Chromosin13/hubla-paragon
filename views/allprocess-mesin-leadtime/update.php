<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AllprocessMesinLeadtime */

$this->title = 'Update Allprocess Mesin Leadtime: ' . $model->row_number;
$this->params['breadcrumbs'][] = ['label' => 'Allprocess Mesin Leadtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->row_number, 'url' => ['view', 'id' => $model->row_number]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="allprocess-mesin-leadtime-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
