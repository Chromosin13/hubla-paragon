<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AllprocessMesinLeadtime */

$this->title = 'Create Allprocess Mesin Leadtime';
$this->params['breadcrumbs'][] = ['label' => 'Allprocess Mesin Leadtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="allprocess-mesin-leadtime-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
