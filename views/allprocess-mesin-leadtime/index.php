<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AllprocessMesinLeadtimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Allprocess Mesin Leadtimes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="allprocess-mesin-leadtime-index">

  

    
    <!-- <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'row_number',
            'posisi:ntext',
            'snfg',
            'snfg_komponen',
            'nama_line',
            'sum',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?> -->

    <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'primary', 'heading'=>'All Process Mesin Leadtime'],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                [
                    'attribute'=>'nama_line', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                ],
                [

                    'attribute'=>'posisi', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>1,
                ],
                [
                    'attribute'=>'snfg', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>2,
                ],
                [
                    'attribute'=>'snfg_komponen', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>3,
                ],
                [
                    'attribute'=>'sum', 
                    'width'=>'50px',
                    'pageSummary'=>true,
                ],
             ],
        ]);
    
    ?>

      <?php 
        $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
                'row_number',
                'posisi:ntext',
                'snfg',
                'snfg_komponen',
                'nama_line',
                'sum',
        ['class' => 'yii\grid\ActionColumn'],
        ];

        echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_PDF => false,
                                ExportMenu::FORMAT_HTML => false
                          ]        
    ]);?>
</div>
