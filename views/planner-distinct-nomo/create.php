<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PlannerDistinctNomo */

$this->title = 'Create Planner Distinct Nomo';
$this->params['breadcrumbs'][] = ['label' => 'Planner Distinct Nomos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="planner-distinct-nomo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
