<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PlannerDistinctNomo */

$this->title = 'Update Planner Distinct Nomo: ' . $model->nomo;
$this->params['breadcrumbs'][] = ['label' => 'Planner Distinct Nomos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nomo, 'url' => ['view', 'id' => $model->nomo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="planner-distinct-nomo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
