<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLeadtimeKemas2Direct */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="achievement-leadtime-kemas2-direct-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomo')->textInput() ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'snfg_komponen')->textInput() ?>

    <?= $form->field($model, 'jenis_kemas')->textInput() ?>

    <?= $form->field($model, 'lanjutan')->textInput() ?>

    <?= $form->field($model, 'nama_line')->textInput() ?>

    <?= $form->field($model, 'nama_operator')->textInput() ?>

    <?= $form->field($model, 'tanggal')->textInput() ?>

    <?= $form->field($model, 'tanggal_stop')->textInput() ?>

    <?= $form->field($model, 'delta_minute')->textInput() ?>

    <?= $form->field($model, 'delta_hour')->textInput() ?>

    <?= $form->field($model, 'koitem_bulk')->textInput() ?>

    <?= $form->field($model, 'koitem_fg')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'nama_bulk')->textInput() ?>

    <?= $form->field($model, 'is_done')->textInput() ?>

    <?= $form->field($model, 'delta_minute_net')->textInput() ?>

    <?= $form->field($model, 'delta_hour_net')->textInput() ?>

    <?= $form->field($model, 'lanjutan_split_batch')->textInput() ?>

    <?= $form->field($model, 'jumlah_realisasi')->textInput() ?>

    <?= $form->field($model, 'jam_per_pcs')->textInput() ?>

    <?= $form->field($model, 'mpq')->textInput() ?>

    <?= $form->field($model, 'achievement')->textInput() ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'start_id')->textInput() ?>

    <?= $form->field($model, 'stop_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
