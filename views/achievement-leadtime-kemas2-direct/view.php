<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLeadtimeKemas2Direct */

// $this->title = $model->stop_id;
// $this->params['breadcrumbs'][] = ['label' => 'Achievement Leadtime Kemas2 Directs', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-leadtime-kemas2-direct-view">
<?php

$script = <<< JS
$(document).ready(function() {
    // setInterval(function(){ $("#refreshButton").click(); }, 5000);


    setTimeout(function () { location.href = "http://10.3.5.102/flowreport/web/index.php?r=kemas2/create"; }, 6000);

});
JS;
$this->registerJs($script);
?>
<!--
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->stop_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->stop_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p> -->

        <div class="callout callout-info" id="batchsplit">
          <h4>Achievement</h4>

          <p>Halaman akan berganti dalam 6 detik</p>
          <p>Jika Achievement <i>(Not Set)</i> berarti data master standar leadtime belum lengkap</p>
    </div>
<?php
if($model->achievement >= 100)
                                            {
                                                echo '<div class="callout callout-success" id="batchsplit">
                                                              <h4> <i class="fa fa-smile-o" aria-hidden="true"> </i> Selamat! Performa anda sangat baik! Achievement anda '.$model->achievement.' % </h4>
                                                        </div>';
                                            }
                                            else if($model->achievement < 100 && $model->achievement >= 80)
                                            {
                                                echo '<div class="callout callout-info" id="batchsplit">
                                                              <h4> <i class="fa fa-meh-o" aria-hidden="true"> </i> Performa anda cukup baik. Achievement anda '.$model->achievement.' % . Anda bisa lebih baik lagi</h4>
                                                        </div>';
                                            }
                                            else if($model->achievement < 80)
                                            {
                                                echo '<div class="callout callout-danger" id="batchsplit">
                                                              <h4> <i class="fa fa-frown-o" aria-hidden="true"> </i> Performa anda dibawah standar. Achievement anda '.$model->achievement.' % . Tetap Semangat dan Tingkatkan Kinerja anda</h4>
                                                        </div>';
                                            }
?>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'snfg',
            'nama_line',
            'nama_fg',
            'jumlah_realisasi',
            // 'id',
            // 'start_id',
            // 'stop_id',
        ],
    ]) ?>

</div>
