<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AchievementLeadtimeKemas2DirectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Achievement Leadtime Kemas2 Directs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-leadtime-kemas2-direct-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Achievement Leadtime Kemas2 Direct', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nomo',
            'snfg',
            'snfg_komponen',
            'jenis_kemas',
            'lanjutan',
            // 'nama_line',
            // 'nama_operator',
            // 'tanggal',
            // 'tanggal_stop',
            // 'delta_minute',
            // 'delta_hour',
            // 'koitem_bulk',
            // 'koitem_fg',
            // 'nama_fg',
            // 'nama_bulk',
            // 'is_done',
            // 'delta_minute_net',
            // 'delta_hour_net',
            // 'lanjutan_split_batch',
            // 'jumlah_realisasi',
            // 'jam_per_pcs',
            // 'mpq',
            // 'achievement',
            // 'id',
            // 'start_id',
            // 'stop_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
