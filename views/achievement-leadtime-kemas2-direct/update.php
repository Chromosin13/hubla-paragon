<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLeadtimeKemas2Direct */

$this->title = 'Update Achievement Leadtime Kemas2 Direct: ' . $model->stop_id;
$this->params['breadcrumbs'][] = ['label' => 'Achievement Leadtime Kemas2 Directs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->stop_id, 'url' => ['view', 'id' => $model->stop_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="achievement-leadtime-kemas2-direct-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
