<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AchievementLeadtimeKemas2Direct */

$this->title = 'Create Achievement Leadtime Kemas2 Direct';
$this->params['breadcrumbs'][] = ['label' => 'Achievement Leadtime Kemas2 Directs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-leadtime-kemas2-direct-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
