<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLeadtimeKemas2DirectSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="achievement-leadtime-kemas2-direct-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'nomo') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'snfg_komponen') ?>

    <?= $form->field($model, 'jenis_kemas') ?>

    <?= $form->field($model, 'lanjutan') ?>

    <?php // echo $form->field($model, 'nama_line') ?>

    <?php // echo $form->field($model, 'nama_operator') ?>

    <?php // echo $form->field($model, 'tanggal') ?>

    <?php // echo $form->field($model, 'tanggal_stop') ?>

    <?php // echo $form->field($model, 'delta_minute') ?>

    <?php // echo $form->field($model, 'delta_hour') ?>

    <?php // echo $form->field($model, 'koitem_bulk') ?>

    <?php // echo $form->field($model, 'koitem_fg') ?>

    <?php // echo $form->field($model, 'nama_fg') ?>

    <?php // echo $form->field($model, 'nama_bulk') ?>

    <?php // echo $form->field($model, 'is_done') ?>

    <?php // echo $form->field($model, 'delta_minute_net') ?>

    <?php // echo $form->field($model, 'delta_hour_net') ?>

    <?php // echo $form->field($model, 'lanjutan_split_batch') ?>

    <?php // echo $form->field($model, 'jumlah_realisasi') ?>

    <?php // echo $form->field($model, 'jam_per_pcs') ?>

    <?php // echo $form->field($model, 'mpq') ?>

    <?php // echo $form->field($model, 'achievement') ?>

    <?php // echo $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'start_id') ?>

    <?php // echo $form->field($model, 'stop_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
