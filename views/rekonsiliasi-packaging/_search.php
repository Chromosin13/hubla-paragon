<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RekonsiliasiPackagingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rekonsiliasi-packaging-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'flow_input_snfg_id') ?>

    <?= $form->field($model, 'nama_pack') ?>

    <?= $form->field($model, 'nobatch') ?>

    <?= $form->field($model, 'supply') ?>

    <?php // echo $form->field($model, 'rs_unb') ?>

    <?php // echo $form->field($model, 'sisa_unb') ?>

    <?php // echo $form->field($model, 'ok_unb') ?>

    <?php // echo $form->field($model, 'rs_pro') ?>

    <?php // echo $form->field($model, 'rm_pro') ?>

    <?php // echo $form->field($model, 'rp_pro') ?>

    <?php // echo $form->field($model, 'sisa_pro') ?>

    <?php // echo $form->field($model, 'rqc') ?>

    <?php // echo $form->field($model, 'total') ?>

    <?php // echo $form->field($model, 'hilang') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
