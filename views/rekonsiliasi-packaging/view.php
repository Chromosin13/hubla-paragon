<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RekonsiliasiPackaging */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rekonsiliasi Packagings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rekonsiliasi-packaging-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'flow_input_snfg_id',
            'nama_pack',
            'nobatch',
            'supply',
            'rs_unb',
            'sisa_unb',
            'ok_unb',
            'rs_pro',
            'rm_pro',
            'rp_pro',
            'sisa_pro',
            'rqc',
            'total',
            'hilang',
        ],
    ]) ?>

</div>
