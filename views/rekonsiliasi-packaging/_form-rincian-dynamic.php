<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\db\Query;
/* @var $this yii\web\View */
/* @var $searchModel app\models\RekonsiliasiPackagingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
//
// $this->title = 'Form Kemas Is Done';
// $this->params['breadcrumbs'][] = $this->title;
?>

<style>
  .swal-button--ok,.swal-button--confirm,.swal-button--sudah{
    background-color: #33995E;
    color:white;
    width: 100%
  }

  .qty-bs{
    font-weight: bold;
    font-size: 4rem!important;
  }

  .swal-button--belum{
    background-color: #DDD;
    color:black;
    width: 100%
  }

  .item-nomo{
    border: 4px solid #CC0019;
    color: #CC0019;
    border-radius: 8px;
    display: block;
    padding: 1rem;
    width: 100%;
    margin-top: 1rem;
    font-weight: bold;
  }

  .swal-button--batal{
    background-color: transparent;
    color: #333;
    width: 100%;
    padding: 2px;
    font-size: 1.2rem;
  }

  .swal-button--cancel{
    width: 100%
  }

  .swal-button--ok:hover,.swal-button--confirm:hover,.swal-button--sudah:hover{
    background-color: #40BF75!important;
  }

  .swal-button--batal:hover{
    background-color: transparent!important;
  }

  .swal-button--belum:hover{
    background-color: #EEE!important;
  }

  .swal-text, .swal-footer{
    text-align: center;
  }

  .custom-modals > .swal-footer > .swal-button-container{
    width: 100%!important
  }

  .default-modals > .swal-footer > .swal-button-container{
    width: 46%!important
  }
  .tooltip-inner {
       max-width: 250px;
       /* If max-width does not work, try using width instead */
       width: 250px;
       font-size: 18px;
       background-color: #424242;
       opacity :1;
  }
  /* SPINNER LOADING */
  .sk-circle {
    margin: 100px auto;
    width: 100px;
    height: 100px;
    position: relative;
  }
  .sk-circle .sk-child {
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
  }
  .sk-circle .sk-child:before {
    content: '';
    display: block;
    margin: 0 auto;
    width: 15%;
    height: 15%;
    background-color: #333;
    border-radius: 100%;
    -webkit-animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
            animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
  }
  .sk-circle .sk-circle2 {
    -webkit-transform: rotate(30deg);
        -ms-transform: rotate(30deg);
            transform: rotate(30deg); }
  .sk-circle .sk-circle3 {
    -webkit-transform: rotate(60deg);
        -ms-transform: rotate(60deg);
            transform: rotate(60deg); }
  .sk-circle .sk-circle4 {
    -webkit-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
            transform: rotate(90deg); }
  .sk-circle .sk-circle5 {
    -webkit-transform: rotate(120deg);
        -ms-transform: rotate(120deg);
            transform: rotate(120deg); }
  .sk-circle .sk-circle6 {
    -webkit-transform: rotate(150deg);
        -ms-transform: rotate(150deg);
            transform: rotate(150deg); }
  .sk-circle .sk-circle7 {
    -webkit-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
            transform: rotate(180deg); }
  .sk-circle .sk-circle8 {
    -webkit-transform: rotate(210deg);
        -ms-transform: rotate(210deg);
            transform: rotate(210deg); }
  .sk-circle .sk-circle9 {
    -webkit-transform: rotate(240deg);
        -ms-transform: rotate(240deg);
            transform: rotate(240deg); }
  .sk-circle .sk-circle10 {
    -webkit-transform: rotate(270deg);
        -ms-transform: rotate(270deg);
            transform: rotate(270deg); }
  .sk-circle .sk-circle11 {
    -webkit-transform: rotate(300deg);
        -ms-transform: rotate(300deg);
            transform: rotate(300deg); }
  .sk-circle .sk-circle12 {
    -webkit-transform: rotate(330deg);
        -ms-transform: rotate(330deg);
            transform: rotate(330deg); }
  .sk-circle .sk-circle2:before {
    -webkit-animation-delay: -1.1s;
            animation-delay: -1.1s; }
  .sk-circle .sk-circle3:before {
    -webkit-animation-delay: -1s;
            animation-delay: -1s; }
  .sk-circle .sk-circle4:before {
    -webkit-animation-delay: -0.9s;
            animation-delay: -0.9s; }
  .sk-circle .sk-circle5:before {
    -webkit-animation-delay: -0.8s;
            animation-delay: -0.8s; }
  .sk-circle .sk-circle6:before {
    -webkit-animation-delay: -0.7s;
            animation-delay: -0.7s; }
  .sk-circle .sk-circle7:before {
    -webkit-animation-delay: -0.6s;
            animation-delay: -0.6s; }
  .sk-circle .sk-circle8:before {
    -webkit-animation-delay: -0.5s;
            animation-delay: -0.5s; }
  .sk-circle .sk-circle9:before {
    -webkit-animation-delay: -0.4s;
            animation-delay: -0.4s; }
  .sk-circle .sk-circle10:before {
    -webkit-animation-delay: -0.3s;
            animation-delay: -0.3s; }
  .sk-circle .sk-circle11:before {
    -webkit-animation-delay: -0.2s;
            animation-delay: -0.2s; }
  .sk-circle .sk-circle12:before {
    -webkit-animation-delay: -0.1s;
            animation-delay: -0.1s; }

  @-webkit-keyframes sk-circleBounceDelay {
    0%, 80%, 100% {
      -webkit-transform: scale(0);
              transform: scale(0);
    } 40% {
      -webkit-transform: scale(1);
              transform: scale(1);
    }
  }

  @keyframes sk-circleBounceDelay {
    0%, 80%, 100% {
      -webkit-transform: scale(0);
              transform: scale(0);
    } 40% {
      -webkit-transform: scale(1);
              transform: scale(1);
    }
  }
</style>

<?php
  $sisa_bulk_standar = $sisa_bulk_standar + 0.3 * $sisa_bulk_standar;
?>



<div class="box box-widget widget-user">
    <div class="widget-user-header bg-aqua">
      <h3 class="widget-user-username"><b>Form Kemas Is Done</b></h3>
      <h3 class="widget-user-username"><b><?= $modelsRekonsiliasiPackaging[0]['snfg'];?></b></h3>
      <h5 class="widget-user-desc"></h5>
      <!-- <button class="'btn btn-danger" style="height: 34px; border-radius: 5px;" onclick="goBack()">Go Back</button> -->
    </div>
    <div class="widget-user-image">
      <!-- <img class="img-circle" src="../web/images/scale.png" alt="User Avatar"> -->
    </div>
    <br></br>
  <div class="box-body">

    <div class="mpq-monitoring-form">

        <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

            <div class="box box-body">
              <?= $form->field($model, 'sisa_bulk_standar',['options' => ['hidden'=> true] ])->textInput(['readOnly'=>true,'value'=>$sisa_bulk_standar])->label('Sisa Bulk Standar (kg)')?>
              <?= $form->field($model, 'flow_input_snfg_id')->textInput(['readOnly'=>true,'value'=>$model->flow_input_snfg_id])?>
              <?= $form->field($model, 'sisa_bulk',['options' => ['hidden'=> true] ])->textInput()->label('Sisa Bulk (kg)')?>
              <?= $form->field($model, 'fg_retur_layak_pakai')->textInput()->label('FG Retur Layak Pakai (pcs)')?>
              <?= $form->field($model, 'fg_retur_reject')->textInput()->label('FG Retur Reject (pcs)')?>
            </div>
            <!-- <div class="form-group" id="div-submit">
              <div class="mpq-monitoring-form">
                <?php // echo Html::submitButton($model->isNewRecord ? 'Submit' : 'Submit', ['id'=>'next','hidden'=>'hidden','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div> -->

    </div>

        <?php DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 20, // the maximum times, an element can be added (default 999)
            'min' => 0, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $modelsRekonsiliasiPackaging[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'snfg',
                'nama_pack',
                'qty_supply',
                'qty_component',
                'qty_mo',
                'rs_pro',
                'rm_pro',
                'rp_pro',
                'sisa_pro',
                'qty_realisasi',

            ],
        ]); ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <i class="glyphicon glyphicon-envelope"></i> <span style="font-size:20px;"><?php echo "Kembalian Packaging  "; ?></span>
                    <!-- <button type="button" class="add-item btn btn-success btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> ADD</button> -->
                </h4>
            </div>
            <div class="panel-body">
                <div class="container-items"><!-- widgetBody -->
                <?php foreach ($modelsRekonsiliasiPackaging as $i => $modelRekonsiliasiPackaging): ?>
                    <div class="item panel panel-default"><!-- widgetItem -->
                        <div class="panel-heading">
                            <h3 style =" font-size:20px;" class="panel-title pull-left"><?php echo $modelRekonsiliasiPackaging->nama_pack; ?></h3>
                            <div class="pull-right">
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                                // necessary for update action.
                                if (! $modelRekonsiliasiPackaging->isNewRecord) {
                                    echo Html::activeHiddenInput($modelRekonsiliasiPackaging, "[{$i}]id");
                                }
                            ?>

                                        <div class="row">

                                          <div id="dua">
                                            <div class="col-md-4">
                                              <?= $form->field($modelRekonsiliasiPackaging, "[{$i}]koitem_pack")->textInput(['readOnly'=>true]) ?>
                                            </div>
                                          </div>

                                          <div id="dua">
                                            <div class="col-md-4">
                                              <?= $form->field($modelRekonsiliasiPackaging, "[{$i}]nama_pack")->textInput(['readOnly'=>true]) ?>
                                            </div>
                                          </div>

                                          <div id="dua">
                                            <div class="col-md-2">
                                              <?= $form->field($modelRekonsiliasiPackaging, "[{$i}]qty_supply")->textInput(['readOnly'=>true]) ?>
                                              <!-- /input-group -->
                                            </div>
                                          </div>

                                          <div id="dua">
                                            <div class="col-md-2">
                                              <?= $form->field($modelRekonsiliasiPackaging, "[{$i}]qty_realisasi")->textInput(['readOnly'=>true]) ?>
                                              <!-- /input-group -->
                                            </div>
                                          </div>

                                          <div id="dua">
                                            <div class="col-md-2">
                                              <?= $form->field($modelRekonsiliasiPackaging, "[{$i}]qty_supply_tambahan")->textInput(['required'=>'required','class' => 'bg-transparent form-control form-control-alternative', 'style' => 'padding : 5px; border-radius: 5px; border:1px solid blue']) ?>
                                              <!-- /input-group -->
                                            </div>
                                          </div>

                                        </div>

                                        <div class="row">

                                          <div class="col-md-2">
                                              <?php
                                                  echo $form->field($modelRekonsiliasiPackaging, "[{$i}]rs_pro",['template' => '{label}  <i class="fa fa-info-circle text-danger info_modal" data-toggle="tooltip" data-placement="top" title="Packaging reject dari supplier yang masuk ke line produksi dan belum di filling"></i>{input}{hint}{error}'])
                                                            ->textInput(['required'=>'required','class' => 'bg-transparent form-control form-control-alternative', 'style' => 'padding : 5px; border-radius: 5px; border:1px solid blue'])

                                              ?>
                                          </div>
                                          <div class="col-md-2">
                                              <?= $form->field($modelRekonsiliasiPackaging, "[{$i}]rm_pro",['template' => '{label}  <i class="fa fa-info-circle text-danger info_modal" data-toggle="tooltip" data-placement="top" title="Reject yang disebabkan oleh mesin seperti set up, atau mesin trouble dll"></i>{input}{hint}{error}'])
                                              ->textInput(['required'=>'required','class' => 'bg-transparent form-control form-control-alternative', 'style' => 'padding : 5px; border-radius: 5px; border:1px solid blue']) ?>
                                          </div>
                                          <div class="col-md-2">
                                              <?= $form->field($modelRekonsiliasiPackaging, "[{$i}]rp_pro",['template' => '{label}  <i class="fa fa-info-circle text-danger info_modal" data-toggle="tooltip" data-placement="top" title="Semua reject packaging yang sudah difilling, di luar reject mesin"></i>{input}{hint}{error}'])
                                              ->textInput(['required'=>'required','class' => 'bg-transparent form-control form-control-alternative', 'style' => 'padding : 5px; border-radius: 5px; border:1px solid blue']) ?>
                                          </div>
                                          <div class="col-md-3">
                                              <?= $form->field($modelRekonsiliasiPackaging, "[{$i}]sisa_pro")->textInput(['required'=>'required','class' => 'bg-transparent form-control form-control-alternative', 'style' => 'padding : 5px; border-radius: 5px; border:1px solid blue']) ?>
                                          </div>
                                          <div class="col-md-3">
                                              <?= $form->field($modelRekonsiliasiPackaging, "[{$i}]hilang")->textInput(['readOnly'=>true,'required'=>'required','class' => 'bg-transparent form-control form-control-alternative', 'style' => 'padding : 5px; border-radius: 5px; border:1px solid blue']) ?>
                                          </div>
                                        </div>

                              </div>
                          </div>
                      <?php endforeach; ?>
                      </div>
                  </div>
              </div><!-- .panel -->
              <?php DynamicFormWidget::end(); ?>

        <div class="form-group" id="div-submit">
            <?php echo Html::submitButton($modelRekonsiliasiPackaging->isNewRecord ? 'NEXT' : 'NEXT', ['id'=>'next', 'class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary']) ?>
        </div>

        <div class="form-group">
            <?= Html::button('NEXT', ['id'=>'next-dummy','class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
  </div>
</div>
<div id="spinner" style="visibility: hidden; position:fixed; margin-left: auto;
  margin-right: auto; left:50%; top:30%; z-index:1000;">
  <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
  </div>
</div>
<script type="text/javascript">
  function alertEmptyBulkSisa(belumDitimbang,totalSisaBulk){
    var res = '';
    for (var i = 0; i < belumDitimbang.length; i++) {
      var content = "<span class='item-nomo' id='nomo-"+i+"'>"+belumDitimbang[i]+"</span>";
      res = res.concat(content);
    }
    var span = document.createElement("span");
    span.innerHTML = "Bulk sisa belum ditimbang. Silahkan timbang bulk sisa terlebih dahulu pada station bulk sisa. <br>Berikut adalah list itemnya:<br>"+res;
    swal({
      title: "Peringatan",
      content:span,
      icon: "warning",
      html:true,
      className: "custom-modals",
      dangerMode: true,
      closeOnClickOutside:false,
      allowOutsideClick:false,
      buttons: {
        ok : {
          text: 'Okay',
          value:true
        },
        batal : {
          text: 'Tidak ada bulk sisa',
          value:false
        },
      },
    })
    .then((res) => {
      if (!res) {
        swal({
          title: "Konfirmasi Kembali",
          text: "Apakah anda yakin tidak ada sisa bulk pada item ini?",
          icon: "warning",
          className: "default-modals",
          dangerMode: false,
          allowOutsideClick:false,
          closeOnClickOutside:false,
          buttons: true
        })
        .then((res) => {
          if (res) {
            $('#mpqmonitoring-sisa_bulk').val(totalSisaBulk)
            $('#next').click();
          }
        })
      }
    })
  }
</script>
<?php
$script= <<<JS

$('[id^="mulai"]').hide();
$("#div-submit").hide();

/*$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
        $('select[name^="RekonsiliasiPackaging["]').change(function() {

            var prefix = $(this).attr('id').split("-");

            prefix.splice(-1, 1);
            var snfg_id = '#' + prefix.join('-') + '-snfg';
            var rs_pro_id = '#' + prefix.join('-') + '-rs_pro';
            var rp_pro_id = '#' + prefix.join('-') + '-rp_pro';
            var rm_pro_id = '#' + prefix.join('-') + '-rm_pro';
            var supply_id = '#' + prefix.join('-') + '-supply';
            var hilang_id = '#' + prefix.join('-') + '-hilang';
            var sisa_pro_id = '#' + prefix.join('-') + '-sisa_pro';
            var no_po = $(this).val();

              $.get('index.php?r=operator-penerimaanlog/get-po',{ no_po : no_po }, function(data){
                var data = $.parseJSON(data);

                  // $(item_code_id).empty().append($('<option>', {
                  //     value:'',
                  //     text:'Select...'
                  // }));

                  $.each(data, function(idx, value) {
                    $(item_code_id).append($('<option>', {
                      value: value['kode_item'],
                      text: value['kode_item']
                    }));
                  });

              });
          });

          $('select[name^="RekonsiliasiPackaging["]').change(function() {
            var prefix = $(this).attr('id').split("-");
            prefix.splice(-1, 1);
            var snfg_id = '#' + prefix.join('-') + '-snfg';
            var rs_pro_id = '#' + prefix.join('-') + '-rs_pro';
            var rp_pro_id = '#' + prefix.join('-') + '-rp_pro';
            var rm_pro_id = '#' + prefix.join('-') + '-rm_pro';
            var supply_id = '#' + prefix.join('-') + '-supply';
            var hilang_id = '#' + prefix.join('-') + '-hilang';
            var sisa_pro_id = '#' + prefix.join('-') + '-sisa_pro';
            var snfg = $(this).val();
            $(item_code_id).change(function(){

              var kode_item = $(this).val();
                $.get('index.php?r=operator-penerimaanlog/get-po',{ no_po: no_po, kode_item : kode_item }, function(data){
                  var data = $.parseJSON(data);

                  $.each(data, function(idx, value) {
                    $(sup_id).val(value['supplier']);
                    $(item_name_id).val(value['nama_item']);
                    $(surjal_id).val(value['no_surjal']);
                    $(batch_id).val(value['no_batch']);
                  });
                });
              });
    });

  $('#mpqmonitoring-sisa_bulk').on('change',function(){
      var sisa_bulk = Number($('#mpqmonitoring-sisa_bulk').val());
      var standar = Number($('#mpqmonitoring-sisa_bulk_standar').val());

      if (sisa_bulk > standar){
        alert('Sisa Bulk melebihi batas normal yang ditentukan, mohon periksa kembali');
      }
  });*/

  $('#next-dummy').on('click',function(){
    document.getElementsByTagName('body')[0].style.opacity = "0.5";
    document.getElementById("spinner").style.visibility = "visible";
    document.getElementsByTagName('body')[0].disabled = true;
    var snfg = "$snfg";
    var sisa_bulk = $('#mpqmonitoring-sisa_bulk').val();
    var fg_retur_layak_pakai = $('#mpqmonitoring-fg_retur_layak_pakai').val();
    var fg_retur_reject = $('#mpqmonitoring-fg_retur_reject').val();

    if (fg_retur_layak_pakai == null || fg_retur_layak_pakai == '') {
      document.getElementsByTagName("body")[0].style.opacity = "1";
      document.getElementById("spinner").style.visibility = "hidden";
      document.getElementsByTagName('body')[0].disabled = false;
      customAlert('warning','Form Belum Lengkap','Silahkan lengkapi field FG Retur Layak Pakai (Pcs) terlebih dahulu.');
    }else{
      if (fg_retur_reject == null || fg_retur_reject == '') {
        document.getElementsByTagName('body')[0].style.opacity = "1";
        document.getElementById("spinner").style.visibility = "hidden";
        document.getElementsByTagName('body')[0].disabled = false;
        customAlert('warning','Form Belum Lengkap','Silahkan lengkapi field FG Retur Reject (Pcs) terlebih dahulu.');
      }else{
        $.get('index.php?r=flow-input-snfg/get-sisa-bulk',{ snfg: snfg },function(result){
          document.getElementsByTagName('body')[0].style.opacity = "1";
          document.getElementById("spinner").style.visibility = "hidden";
          document.getElementsByTagName('body')[0].disabled = false;
          var json = JSON.parse(atob(result));
          var belumDitimbang = [];
          var totalSisaBulk = 0;
          for (var i = 0; i < json.length; i++) {
            console.log(json[i].nomo+" - "+json[i].qty);
            if (json[i].qty == 0) {
              belumDitimbang.push(json[i].nomo)
            }else{
              totalSisaBulk += json[i].qty;
            }
          }
          if (belumDitimbang.length > 0) {
            alertEmptyBulkSisa(belumDitimbang,totalSisaBulk);
          }else{
            var span = document.createElement("span");
            // span.innerHTML = "Berat sisa bulk pada FRO Bulk Sisa sebesar:<br><span class='qty-bs'>"+parseInt(totalSisaBulk)+" Kg</span><br>Apakah berat sisa bulk sudah sesuai dengan berat yang sebenarnya?";
            span.innerHTML = "Berat sisa bulk pada FRO Bulk Sisa sebesar:<br><br><span class='qty-bs'>"+parseInt(totalSisaBulk)+" Kg</span>";
            swal({
              title: "Informasi",
              content:span,
              className:'custom-modals',
              icon: "info",
              html:true,
              closeOnClickOutside:false,
              allowOutsideClick:false,
              buttons: {
                ok : {
                  text: 'Okay',
                  value:false
                },
              },
            })
            .then((res) => {
              if (!res) {
                $('#mpqmonitoring-sisa_bulk').val(totalSisaBulk)
                $('#next').click();
              }
            })
          }
          /*if (result == 0) {
            document.getElementsByTagName('body')[0].style.opacity = "1";
            document.getElementById("spinner").style.visibility = "hidden";
            document.getElementsByTagName('body')[0].disabled = false;
            alertEmptyBulkSisa();
          }else if(result == -1){
            document.getElementsByTagName('body')[0].style.opacity = "1";
            document.getElementById("spinner").style.visibility = "hidden";
            document.getElementsByTagName('body')[0].disabled = false;
            alertEmptyBulkSisa();
          }else{
            document.getElementsByTagName('body')[0].style.opacity = "1";
            document.getElementById("spinner").style.visibility = "hidden";
            document.getElementsByTagName('body')[0].disabled = false;
            $('#sb').val(result)
            $('#stop-jadwal').click();
          }*/
        })
      }
    }

    /*if (sisa_bulk > standar){
      alert('Sisa Bulk melebihi batas normal yang ditentukan, mohon periksa kembali');
    }else{
      $('#next').trigger('click');
    }*/
    // $('#next').trigger('click');

  })

  $('input[name^="RekonsiliasiPackaging["]').keyup(function(){
    var prefix = $(this).attr('id').split("-");
    prefix.splice(-1, 1);
    var snfg_id = '#' + prefix.join('-') + '-snfg';
    var qty_component_id = '#' + prefix.join('-') + '-qty_component';
    var qty_supply_id = '#' + prefix.join('-') + '-qty_supply';
    var qty_supply_tambahan_id = '#' + prefix.join('-') + '-qty_supply_tambahan';
    var qty_realisasi_id = '#' + prefix.join('-') + '-qty_realisasi';
    var qty_mo_id = '#' + prefix.join('-') + '-qty_mo';
    var rs_pro_id = '#' + prefix.join('-') + '-rs_pro';
    var rp_pro_id = '#' + prefix.join('-') + '-rp_pro';
    var rm_pro_id = '#' + prefix.join('-') + '-rm_pro';
    var hilang_id = '#' + prefix.join('-') + '-hilang';
    var sisa_pro_id = '#' + prefix.join('-') + '-sisa_pro';
    var snfg = $(this).val();
      var rs = parseFloat($(rs_pro_id).val())||0;
      var rm = parseFloat($(rm_pro_id).val())||0;
      var rp = parseFloat($(rp_pro_id).val())||0;
      var sisa = parseFloat($(sisa_pro_id).val())||0;
      var hilang = parseFloat($(hilang_id).val())||0;
      var qty_supply = parseFloat($(qty_supply_id).val())||0;
      var qty_supply_tambahan = parseFloat($(qty_supply_tambahan_id).val())||0;
      var qty_realisasi = parseFloat($(qty_realisasi_id).val())||0;
      var qty_component = parseFloat($(qty_component_id).val())||0;
      var qty_mo = parseFloat($(qty_mo_id).val())||0;
      // var x = parseFloat($(quantity_koli_receh_id).val())==0;
      // var y = parseFloat($(quantity_koli_receh_id).val())>0;

      $(hilang_id).val(qty_supply+qty_supply_tambahan-qty_realisasi-rs-rp-rm-sisa);

  });


$(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
    console.log("beforeInsert");
});

$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    console.log("afterInsert");
});

$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
    if (! confirm("Are you sure you want to delete this item?")) {
        return false;
    }
    return true;
});

$(".dynamicform_wrapper").on("afterDelete", function(e) {
    console.log("Deleted item!");
});

$(".dynamicform_wrapper").on("limitReached", function(e, item) {
    alert("Limit reached");
});


jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title-adminlog").each(function(index) {
        jQuery(this).html("Adminlog: " + (index + 1))
    });
});

jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title-adminlog").each(function(index) {
        jQuery(this).html("Adminlog: " + (index + 1))
    });
});

JS;
$this->registerJs($script);
?>
