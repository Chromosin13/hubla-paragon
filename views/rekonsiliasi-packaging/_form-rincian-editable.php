<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\RekonsiliasiPackagingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
//
$this->title = 'Form Kemas Is Done';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rekonsiliasi-packaging-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="box box-body">
      <?= $form->field($model_mm, 'flow_input_snfg_id')->textInput(['readOnly'=>true,'value'=>$model->flow_input_snfg_id])?>
      <?= $form->field($model_mm, 'sisa_bulk')->textInput()->label('Sisa Bulk (kg)')?>
      <?= $form->field($model_mm, 'fg_retur_reject')->textInput()->label('FG Retur Reject (pcs)')?>
      <?= $form->field($model_mm, 'fg_retur_layak_pakai')->textInput()->label('FG Retur Layak Pakai (pcs)')?>
    </div>
    <div class="form-group" id="div-submit">
        <?php echo Html::submitButton($model_mm->isNewRecord ? 'Submit' : 'Submit', ['id'=>'next','hidden'=>'hidden','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>



</div>

<div class="rekonsiliasi-packaging-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <!-- <?= Html::a('Create Rekonsiliasi Packaging', ['create'], ['class' => 'btn btn-success']) ?> -->
    </p>

<?php Pjax::begin(); ?>
    <?php
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],

        // 'id',
        // 'flow_input_snfg_id',
        'nama_pack',
        // 'nobatch',
        'qty_supply',
        'uom',
        [
            'class'=>'kartik\grid\EditableColumn',
            'attribute'=>'rs_pro',
            'editableOptions' => [
                'asPopover' => false,
            ],
        ],
        [
            'class'=>'kartik\grid\EditableColumn',
            'attribute'=>'rm_pro',
            'editableOptions' => [
                'asPopover' => false,
            ],
        ],
        [
            'class'=>'kartik\grid\EditableColumn',
            'attribute'=>'rp_pro',
            'editableOptions' => [
                'asPopover' => false,
            ],
        ],
        [
            'class'=>'kartik\grid\EditableColumn',
            'attribute'=>'sisa_pro',
            'editableOptions' => [
                'asPopover' => false,
            ],
        ],
        // 'hilang',

        // ['class' => 'yii\grid\ActionColumn'],
    ];

    echo GridView::widget([
    // 'id' => 'kv-grid-demo',
    'dataProvider' => $dataProvider,
    // 'filterModel' => $searchModel,
    'columns' => $gridColumns, // check the configuration for grid columns by clicking button above
    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
    // 'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    // 'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'pjax' => true, // pjax is set to always true for this demo
    // set your toolbar
    // 'toolbar' =>  [
    //     [
    //         'content' =>
    //             Html::button('<i class="fas fa-plus"></i>', [
    //                 'class' => 'btn btn-success',
    //                 'title' => Yii::t('kvgrid', 'Add Book'),
    //                 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");'
    //             ]) . ' '.
    //             Html::a('<i class="fas fa-redo"></i>', ['grid-demo'], [
    //                 'class' => 'btn btn-outline-secondary',
    //                 'title'=>Yii::t('kvgrid', 'Reset Grid'),
    //                 'data-pjax' => 0,
    //             ]),
    //         'options' => ['class' => 'btn-group mr-2']
    //     ],
    //     '{export}',
    //     '{toggleData}',
    // ],
    // 'toggleDataContainer' => ['class' => 'btn-group mr-2'],
    // set export properties
    'export' => false,
    // parameters from the demo form
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    // 'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<i class="fas fa-book"></i> Rekonsiliasi Packaging',
    ],
    'persistResize' => false,
    'toggleDataOptions' => ['minCount' => 10],
    // 'exportConfig' => $exportConfig,
    // 'itemLabelSingle' => 'book',
    // 'itemLabelPlural' => 'books'
]);?>



<?php Pjax::end(); ?>
<div class="form-group">
    <?= Html::button('Next', ['id'=>'next-dummy','class' => 'btn btn-success']) ?>
</div>

</div>

<?php
    $this->registerJs('
        setInterval(function(){
             $.pjax.reload({container:"#listbahanbaku"});
        }, 5000);', \yii\web\VIEW::POS_HEAD);
?>

<?php
$script= <<<JS
$('#div-submit').hide();



$('#next-dummy').on('click',function(){
  $('#next').trigger('click');
})

JS;
$this->registerJs($script);
?>
