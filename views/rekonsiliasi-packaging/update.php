<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RekonsiliasiPackaging */

$this->title = 'Update Rekonsiliasi Packaging: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rekonsiliasi Packagings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rekonsiliasi-packaging-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
