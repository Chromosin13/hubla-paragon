<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\RekonsiliasiPackagingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rekonsiliasi Packagings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rekonsiliasi-packaging-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Rekonsiliasi Packaging', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'flow_input_snfg_id',
            'nama_pack',
            'nobatch',
            'supply',
            // 'rs_unb',
            // 'sisa_unb',
            // 'ok_unb',
            // 'rs_pro',
            // 'rm_pro',
            // 'rp_pro',
            // 'sisa_pro',
            // 'rqc',
            // 'total',
            // 'hilang',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
