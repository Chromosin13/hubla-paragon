<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RekonsiliasiPackaging */

// $this->title = 'Create Rekonsiliasi Packaging';
// $this->params['breadcrumbs'][] = ['label' => 'Rekonsiliasi Packagings', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="rekonsiliasi-packaging-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-rincian-dynamic', [
        'sisa_bulk_standar' => $sisa_bulk_standar,
        'snfg' => $snfg,
        'model' => $model,
        'modelsRekonsiliasiPackaging' => $modelsRekonsiliasiPackaging
        // 'searchModel' => $searchModel,
        // 'dataProvider' => $dataProvider,
    ]) ?>

</div>
