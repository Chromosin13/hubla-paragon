<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RekonsiliasiPackaging */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rekonsiliasi-packaging-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'flow_input_snfg_id')->textInput(['readOnly'=>true]) ?>

    <?= $form->field($model, 'nama_pack')->textInput(['readOnly'=>true]) ?>

    <!-- <?= $form->field($model, 'nobatch')->textInput() ?> -->

    <?= $form->field($model, 'qty_supply')->textInput(['readOnly'=>true]) ?>
    <?= $form->field($model, 'uom')->textInput(['readOnly'=>true]) ?>

    <!-- <?= $form->field($model, 'rs_unb')->textInput() ?> -->

    <!-- <?= $form->field($model, 'sisa_unb')->textInput() ?> -->

    <!-- <?= $form->field($model, 'ok_unb')->textInput() ?> -->

    <?= $form->field($model, 'rs_pro')->textInput() ?>

    <?= $form->field($model, 'rm_pro')->textInput() ?>

    <?= $form->field($model, 'rp_pro')->textInput() ?>

    <?= $form->field($model, 'sisa_pro')->textInput() ?>

    <!-- <?= $form->field($model, 'rqc')->textInput() ?> -->

    <!-- <?= $form->field($model, 'total')->textInput() ?> -->

    <?= $form->field($model, 'hilang')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
