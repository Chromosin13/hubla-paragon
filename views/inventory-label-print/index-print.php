<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\filters\VerbFilter;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\DataParagonian */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-paragonian-form">
  <div class="row">
        <div class="col-md-12">
          <div class="box box-warning box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Warning !!!</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              Jika banyak data yang ingin di print pisahkan setiap kode menggunakan koma
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>


    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'device_code')->textArea(['rows'=>6])->label('Insert device code you want to print (separate by comma)') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Print' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< JS
$(function() {
    $('#submit-excel').hide();
});
JS;
$this->registerJs($script);
?>
