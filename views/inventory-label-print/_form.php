<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InventoryLabelPrint */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inventory-label-print-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'device_code')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'zpl')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
