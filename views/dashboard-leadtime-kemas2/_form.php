<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DashboardLeadtimeKemas2 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dashboard-leadtime-kemas2-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'posisi')->textInput() ?>

    <?= $form->field($model, 'start')->textInput() ?>

    <?= $form->field($model, 'stop')->textInput() ?>

    <?= $form->field($model, 'sum_leadtime_raw')->textInput() ?>

    <?= $form->field($model, 'sum_leadtime_bruto')->textInput() ?>

    <?= $form->field($model, 'jenis_proses')->textInput() ?>

    <?= $form->field($model, 'sediaan')->textInput() ?>

    <?= $form->field($model, 'sum_downtime')->textInput() ?>

    <?= $form->field($model, 'sum_adjust')->textInput() ?>

    <?= $form->field($model, 'sum_leadtime_net')->textInput() ?>

    <?= $form->field($model, 'keterangan_downtime')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'keterangan_adjust')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'nomo')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'snfg_komponen')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'upload_date')->textInput() ?>

    <?= $form->field($model, 'week')->textInput() ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
