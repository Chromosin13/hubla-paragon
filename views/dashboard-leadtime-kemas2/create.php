<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DashboardLeadtimeKemas2 */

$this->title = 'Create Dashboard Leadtime Kemas2';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Leadtime Kemas2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dashboard-leadtime-kemas2-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
