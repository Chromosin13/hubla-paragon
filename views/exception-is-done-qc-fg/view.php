<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ExceptionIsDoneQcFg */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Exception Is Done Qc Fgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exception-is-done-qc-fg-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'keterangan',
            'timestamp',
            'pic',
            'snfg',
        ],
    ]) ?>

</div>
