<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ExceptionIsDoneQcFg */

?>
<div class="exception-is-done-qc-fg-create">


    <?= $this->render('_form', [
        'snfg' => $snfg,
        'model' => $model,
    ]) ?>

</div>
