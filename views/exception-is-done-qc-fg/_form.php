<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ExceptionIsDoneQcFg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="exception-is-done-qc-fg-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'snfg')->textInput(['readOnly'=>true,'value'=>$snfg]) ?>

    <?= $form->field($model, 'pic')->textInput(['readOnly'=>true,'value'=>Yii::$app->user->identity->username]) ?>

    <?= $form->field($model, 'keterangan')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'SUBMIT & SET IS DONE' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-danger btn-block' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
