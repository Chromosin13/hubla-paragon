<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ExceptionIsDoneQcFg */

$this->title = 'Update Exception Is Done Qc Fg: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Exception Is Done Qc Fgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="exception-is-done-qc-fg-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
