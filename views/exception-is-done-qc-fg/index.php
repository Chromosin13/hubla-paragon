<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ExceptionIsDoneQcFgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Exception Is Done Qc Fgs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exception-is-done-qc-fg-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Exception Is Done Qc Fg', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'keterangan',
            'timestamp',
            'pic',
            'snfg',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
