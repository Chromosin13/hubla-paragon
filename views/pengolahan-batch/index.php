<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax; 
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PengolahanBatchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengolahan Batch';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengolahan-batch-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
        Modal::begin([
            'header'=>'<h4>Isi Informasi Batch</h4>',
            'id'=>'update-modal',
            'size'=>'modal-lg'
        ]);

        echo "<div id='updateModalContent'></div>";

        Modal::end();
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            [   
                'class' => 'kartik\grid\ActionColumn',
                // 'options'=>['class'=>'action-column'],
                'template' => '{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                        $btn = Html::button('<img class="img-circle" src="../web/images/cmyk-icon.png" alt="Terima" width="21" height="21">',[
                            'value'=>Url::to('index.php?r=pengolahan-batch/update&id='.$key),
                            // Yii::$app->urlManager->createUrl('qc-bulk-entry/update'), //<---- here is where you define the action that handles the ajax request
                            'class'=>'update-modal-click grid-action',
                            'data-toggle'=>'tooltip',
                            'data-placement'=>'bottom',
                            'title'=>'Update'
                        ]);
                        return $btn;
                    }
                ],
            ],
            'snfg_komponen',
            'nobatch',
            'besar_batch_real',
            
        ],
    ]); ?>
</div>

<?= Html::a('Home', ['flow-input-mo/check-pengolahan'], ['class'=>'btn btn-primary']) ?>

<?php
$script = <<< JS


$(function () {
    $('.update-modal-click').click(function () {
        $('#update-modal')
            .modal('show')
            .find('#updateModalContent')
            .load($(this).attr('value'));
    });
});

JS;
$this->registerJs($script);
?>