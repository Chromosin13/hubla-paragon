<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PengolahanBatch */

$this->title = 'Update Pengolahan Batch: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pengolahan Batches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pengolahan-batch-update">

    <?= $this->render('_form', [
        'model' => $model,
        'mpq_teoritis' => $mpq_teoritis,
    ]) ?>

</div>
