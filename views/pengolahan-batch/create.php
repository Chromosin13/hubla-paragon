<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PengolahanBatch */

$this->title = 'Create Pengolahan Batch';
$this->params['breadcrumbs'][] = ['label' => 'Pengolahan Batches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengolahan-batch-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
