<?php

use app\models\FlowInputMo;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\PengolahanBatch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengolahan-batch-form">

<?php $nomo = FlowInputMo::find()->where(['id'=>$model->pengolahan_id])->one()['nomo']; ?>

    <?php $form = ActiveForm::begin([
      'enableAjaxValidation'=>true,
      // 'validationUrl'=>Url::toRoute('pengolahan-batch/validation'),
    ]); ?>

    <?= $form->field($model, 'pengolahan_id')->textInput(['readOnly'=>true]) ?>

    <?= $form->field($model, 'snfg_komponen')->textInput(['readOnly'=>true]) ?>

    <?= $form->field($model, 'besar_batch_teoritis')->textInput(['readOnly'=>true,'value'=>$mpq_teoritis]) ?>

    <?= $form->field($model, 'nobatch')->textInput(['readOnly'=>true]) ?>

    <?= $form->field($model, 'besar_batch_real')->textInput() ?>

    <!-- <?= $form->field($model, 'sisa_bulk')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['id'=>'updatebutton','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script= <<<JS
$('#updatebutton').on('click',function(){
  var real = $('#pengolahanbatch-besar_batch_real').val();
    var teoritis = $('#pengolahanbatch-besar_batch_teoritis').val();
    if(real>1.5*teoritis){
        alert('Realisasi melebihi standar yang ditentukan');
        return false;
    }
    else if(real<0.5*teoritis){
        alert('Realisasi kurang dari standar yang ditentukan');
        return false;
    }else{
        return true;
    };

});

var nomo = "$nomo";
$.get('index.php?r=flow-input-mo/read-batch-olah',{ nomo : nomo }, function(data){
    var data = $.parseJSON(data);
    $('#pengolahanbatch-nobatch').attr('value',data.nobatch);

});
JS;
$this->registerJs($script);
 ?>
