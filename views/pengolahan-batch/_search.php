<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PengolahanBatchSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengolahan-batch-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'nobatch') ?>

    <?= $form->field($model, 'besar_batch_real') ?>

    <?= $form->field($model, 'pengolahan_id') ?>

    <?= $form->field($model, 'snfg_komponen') ?>

    <?= $form->field($model, 'id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
