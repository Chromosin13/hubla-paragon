<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use app\models\LogDowntime;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogDowntimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="callout callout-success">
  <h4>Log Downtime</h4>

  <p> Data Realtime </p>
  <p> Gabungan Downtime dari seluruh jadwal yang melalui seluruh proses</p>

</div>

<p></p>
Download All Data Here
    <?php 

        $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],
            'snfg',
            'snfg_komponen',
            'nomo',
            'jenis_proses',
            'lanjutan',
            'interval',
            'interval_dec',
            'waktu_start',
            'waktu_stop',
            'jenis',
            'keterangan',
            'uid',
        ['class' => 'kartik\grid\ActionColumn'],
        ];

        echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
  
    ]);?>
<p></p>  



<div class="log-downtime-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'danger', 'heading'=>'Log Downtime'],
            'formatter' => [
                'class' => 'yii\i18n\Formatter',
                'timeZone' => 'Asia/Jakarta'
            ],
            'toolbar' => [
            ],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'snfg',
            'snfg_komponen',
            'nomo',
            'jenis_proses',
            'lanjutan',
            'interval',
            'interval_dec',
            'waktu_start',
            'waktu_stop',
            'jenis',
            'keterangan',
            'uid',
        ],
    ]); ?>
</div>
