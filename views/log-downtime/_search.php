<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogDowntimeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-downtime-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'snfg_komponen') ?>

    <?= $form->field($model, 'nomo') ?>

    <?= $form->field($model, 'jenis_proses') ?>

    <?= $form->field($model, 'lanjutan') ?>

    <?php // echo $form->field($model, 'interval') ?>

    <?php // echo $form->field($model, 'interval_dec') ?>

    <?php // echo $form->field($model, 'waktu_start') ?>

    <?php // echo $form->field($model, 'waktu_stop') ?>

    <?php // echo $form->field($model, 'jenis') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'uid') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
