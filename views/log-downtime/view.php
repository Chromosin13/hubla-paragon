<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LogDowntime */

$this->title = $model->uid;
$this->params['breadcrumbs'][] = ['label' => 'Log Downtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-downtime-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->uid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->uid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'snfg',
            'snfg_komponen',
            'nomo',
            'jenis_proses',
            'lanjutan',
            'interval',
            'interval_dec',
            'waktu_start',
            'waktu_stop',
            'jenis',
            'keterangan',
            'uid',
        ],
    ]) ?>

</div>
