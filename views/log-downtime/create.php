<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LogDowntime */

$this->title = 'Create Log Downtime';
$this->params['breadcrumbs'][] = ['label' => 'Log Downtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-downtime-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
