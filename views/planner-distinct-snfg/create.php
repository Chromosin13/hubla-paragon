<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PlannerDistinctSnfg */

$this->title = 'Create Planner Distinct Snfg';
$this->params['breadcrumbs'][] = ['label' => 'Planner Distinct Snfgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="planner-distinct-snfg-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
