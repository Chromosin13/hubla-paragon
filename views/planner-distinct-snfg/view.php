<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PlannerDistinctSnfg */

$this->title = $model->snfg;
$this->params['breadcrumbs'][] = ['label' => 'Planner Distinct Snfgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="planner-distinct-snfg-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->snfg], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->snfg], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'snfg',
            'status',
        ],
    ]) ?>

</div>
