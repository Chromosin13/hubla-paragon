<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PlannerDistinctSnfg */

$this->title = 'Update Planner Distinct Snfg: ' . $model->snfg;
$this->params['breadcrumbs'][] = ['label' => 'Planner Distinct Snfgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->snfg, 'url' => ['view', 'id' => $model->snfg]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="planner-distinct-snfg-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
