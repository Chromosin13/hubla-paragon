<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowAnalyst;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- CSS For Video Selector -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="ZXing for JS">

  <title>ZXing TypeScript | Decoding from camera stream</title>

  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
  href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
  href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
  href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">

<style>
  .column {
    float: left;
    width: 50%;
  }

  .accordion {

    color: white;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 80%;
    transition: 0.4s;
  }

  .active, .accordion:hover {
    background-color: #ccc;
  }

  .accordion:after {
    content: '\002B';
    color: white;
    font-weight: bold;
    float: right;
    margin-left: 5px;
  }

  .active:after {
    content: "\2212";
    color: white;
  }

  .panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
  }

  .box{
    text-align: center;
  }

  .header{
    padding :1px;
    text-align: center;
  }

  .title-text{
    font-size: 28px;
    vertical-align: middle;
    font-weight: 600
  }

  .widget-user-header{
    text-align: center;
    margin-left: auto;
    margin-right: auto;
    height: 30%!important
  }

  .bg-green-old{
    background-color: #4CAF50;
    color: white;
    border: none;
  }

  video{
    height: auto!important;
    border: 2px solid #555;
    display: block;
    margin: 0 auto;
    margin-top: 2%;
    margin-bottom: 2%;
    object-fit: fill;
  }

  .sub-title-qr{
    margin-top: 2%!important;
    font-size: 2.2rem;
    font-weight: 600;
    color: #555
  }

  .sub-desc-qr{
    font-size: 1.8rem;
    color: #555;
    font-weight: 500;
  }

  .field-result{
    display: none;
  }

  .accordion{
    text-align: center;
  }

  input:disabled{
    font-weight: 600;
    background-color: #EEE
  }

  .swal-button-container{
    text-align: center!important;
    display: inline-block;
  }

  .swal-button--confirm{
    background-color: #4CAF50
  }

  .swal-button:hover{
    background-color: #aaa!important
  }


  .swal-button{
    vertical-align: middle!important;
    padding: 0px!important;
    width: 10rem!important;
    text-transform: capitalize;
  }

  .swal-footer{
    text-align: center;
  }

  .bootstrap-tagsinput{
    min-height: 3.8rem;
    text-align: left;
  }

  .badge-info{
    margin-right: 1%!important;
    text-transform: uppercase!important;
    background-color: #4CAF50
  }

  .bootstrap-tagsinput input{
    position: absolute!important;
    padding: 0px!important;
    margin-top: -0.5%!important;
    margin-left: 1%!important;
    -moz-box-shadow: none!important;
    border: none!important;
    outline: none!important;
    -webkit-box-shadow: none!important;
  }

  .btn-back{
    float: left;
    margin: 1%;
  }

  .btn-back:hover{
    cursor: pointer;
  }

  .text-back{
    color: #888;
    font-size: 2rem;
    font-weight: 600;
  }

  .manual-layout span{
    font-size: 2rem
  }

  table tr td{
    border: none!important;
    text-align: center;
  }


</style>

<script type="text/javascript" src="zxing.js"></script>
<!-- Code -->
<script type="text/javascript">
  var zonaList =
  [
    ['LIQUID A','TUP01'],['LIQUID A','TUP02'],['LIQUID A','TUP03'],['LIQUID A','TUP04'],
    ['LIQUID A','TUP05'],['LIQUID A','TUP06'],['LIQUID A','TUP07'],['LIQUID A','TUP08'],
    ['LIQUID A','TUP09'],['LIQUID B','JAP01'],['LIQUID B','BLP01'],['LIQUID B','BLP02'],
    ['LIQUID B','BOP01'],['LIQUID B','BOP02'],['LIQUID B','BOP03'],['LIQUID B','BOP04'],
    ['LIQUID B','BOP05'],['LIQUID B','BOP06'],['LIQUID B','EMP02'],['LIQUID B','EMP05'],
    ['LIQUID B','LQP04'],['LIQUID C','JAP03'],['LIQUID C','BOP08'],['LIQUID C','BOP09'],
    ['LIQUID C','BOP10'],['LIQUID C','CUP01'],['LIQUID C','EMP01'],['LIQUID C','EMP03'],
    ['LIQUID C','EMP07'],['LIQUID C','EMP15'],['LIQUID C','EMP16'],['LIQUID C','EMP17'],
    ['LIQUID C','OSP01'],

    ['POWDER A','LPP01'],['POWDER A','LPP02'],['POWDER A','LPP03'],['POWDER A','PNP01'],
    ['POWDER A','PNP03'],['POWDER A','PNP04'],['POWDER A','PNP01MANUAL'],['POWDER A','PNP04MANUAL'],
    ['POWDER B','PCP01'],['POWDER B','PCP02'],['POWDER B','PCP03'],['POWDER B','PCP04'],
    ['POWDER B','PCP05'],['POWDER B','PEP01'],['POWDER B','PEP02'],['POWDER B','PEP03'],
    ['POWDER B','PEP04'],['POWDER B','PEP05'],['POWDER B','PEP06'],

    ['BOTTLE LINE - VARCOS','VSN01'],['BOTTLE LINE - VARCOS','VSN02'],['BOTTLE LINE - VARCOS','VSN03'],['BOTTLE LINE - VARCOS','VMN01'],['BOTTLE LINE - VARCOS','VMN02'],
    ['BOTTLE LINE - SEMISOLID','BTP02'],['BOTTLE LINE - SEMISOLID','BTP04'],['BOTTLE LINE - SEMISOLID','LIP11'],
    ['BOTTLE LINE - SEMISOLID','LIP18'],['BOTTLE LINE - SEMISOLID','LRP01'],

    ['JAR & FOUNDATION - SEMISOLID','FOP03'],['JAR & FOUNDATION - SEMISOLID','JRP02'],['JAR & FOUNDATION - SEMISOLID','SAP01'],
    ['JAR & FOUNDATION - SEMISOLID','SBP01'],['LIPSTICK LINE - SEMISOLID','ALP03'],['LIPSTICK LINE - SEMISOLID','LIP06'],
    ['LIPSTICK LINE - SEMISOLID','LIP08'],['LIPSTICK LINE - SEMISOLID','LIP10'],['LIPSTICK LINE - SEMISOLID','LIP12'],
    ['LIPSTICK LINE - SEMISOLID','LIP14'],['LIPSTICK LINE - SEMISOLID','LIP16'], ['LIPSTICK LINE - SEMISOLID','SMALLBATCH']
  ]

  var domain_ip = 'http://10.3.5.102/flowreport/web/';
  // var domain_ip = 'http://10.3.5.102/flowreport_dev/web/';
  // var domain_ip = '';

  // QR Code Scanner
  let selectedDeviceId;
  const codeReader = new ZXing.BrowserMultiFormatReader()
  console.log('ZXing code reader initialized')
  codeReader.getVideoInputDevices()
  .then((videoInputDevices) => {
    const sourceSelect = document.getElementById('sourceSelect')
    // selectedDeviceId = videoInputDevices[1].deviceId
    if (videoInputDevices.length >= 1) {
      videoInputDevices.forEach((element) => {
        const sourceOption = document.createElement('option')
        sourceOption.text = element.label
        sourceOption.value = element.deviceId
        sourceSelect.appendChild(sourceOption)
      })
      const sourceSelectPanel = document.getElementById('sourceSelectPanel')
      sourceSelectPanel.style.display = 'block'
    }

    // Initialize Execute Canvas when Page first loaded
    codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
      if (result) {
        var nomo = result.text
        check(nomo)
      }
    })
  })
  .catch((err) => {
    console.error(err)
  })

  function addBulk() {
    var nama_op = document.getElementById("nama_operator").value.toLocaleUpperCase();
    if (document.getElementById('zona_kemas').value == '-') {
      customAlert('warning','SCAN GAGAL!','Zona kemas tidak ditemukan, Silahkan hubungi Leader!')
    }else{
      if (nama_op != "") {
        var object = {
          "nomo" :document.getElementById("nomo_bulk_sisa").value,
          "snfg" :document.getElementById("snfg").value,
          "nama_operator" : nama_op,
          "kode_bulk":document.getElementById("kode_bulk").value,
          "line_kemas":document.getElementById("line_kemas").value,
          "no_batch":document.getElementById("no_batch").value,
          "zona":document.getElementById("zona_kemas").value,
          "listZona":zonaList,
        }
        $.get('index.php?r=bulk-sisa/add-bulk',{ obj : btoa(JSON.stringify(object)) },function(result){
          if (result) {
            swal({
              title: "Konfirmasi Kembali",
              text: "Apakah anda yakin bulk yang akan diproses sudah benar?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((res) => {
              if (res) {
                window.location = domain_ip+'index.php?r=bulk-sisa/measure-massa&obj='+btoa(JSON.stringify(object))
              } else {
                swal("Proses dibatalkan!", {
                  icon: "error",
                });
              }
            });
          }else{
            customAlert("danger","Simpan Gagal","Terjadi kesalahan pada sistem saat menyimpan jadwal")
          }
        });
      }else{
        customAlert("warning","Form Belum Lengkap","Silahkan lengkapi Nama Operator terlebih dahulu")
      }
    }
  }

  // Function untuk cek nomo ke db
  function check(nomo){
    if ((nomo != "")){
      if (nomo[0] == 'M' && nomo[1] == 'O' && nomo[2] == '-') {
        $.get('index.php?r=bulk-sisa/get-data-by-nomo',{ nomo : nomo },function(result){
          if (result == 0) {
            customAlert('warning','SCAN GAGAL!','NOMO tidak tersedia pada jadwal planner')
            document.getElementsByClassName('field-result')[0].style.display = "none"
          }else if (result == 1){
            customAlert('warning','SCAN GAGAL!','NOMO belum masuk tahap olah')
            document.getElementsByClassName('field-result')[0].style.display = "none"
          }else if (result == 2){
            customAlert('warning','SCAN GAGAL!','NOMO belum masuk tahap kemas')
            document.getElementsByClassName('field-result')[0].style.display = "none"
          }else if(result == 3){
            customAlert('warning','SCAN GAGAL!','Device ini tidak memiliki hak scan')
            document.getElementsByClassName('field-result')[0].style.display = "none"
          }else{
            codeReader.reset();
            document.getElementsByClassName('scan-layout')[0].style.display = 'none'
            var jsonData = JSON.parse(atob(result))
            if (jsonData['nama_operator'] == null) {
              var zona_kemas = '-'
              for (var i = 0; i < zonaList.length; i++) {
                if (zonaList[i][1] == jsonData['line']) {
                  zona_kemas = zonaList[i][0]
                  break;
                }
              }
              document.getElementById('nomo_bulk_sisa').value = jsonData['nomo']
              document.getElementById('nama_fg').value = jsonData['nama_fg'].replace('�','')
              document.getElementById('snfg').value = jsonData['snfg']
              document.getElementById('nama_bulk').value = jsonData['nama_bulk'].replace('�',''),
              document.getElementById('kode_bulk').value = jsonData['koitem_bulk']
              document.getElementById('no_batch').value = jsonData['nobatch']
              document.getElementById('line_kemas').value = jsonData['line']
              document.getElementById('zona_kemas').value = zona_kemas
              document.getElementsByClassName('field-result')[0].style.display = "block"
            }else{
              if (jsonData['status'] == 'Belum Ditimbang') {
                var zona_kemas = '-'
                for (var i = 0; i < zonaList.length; i++) {
                  if (zonaList[i][1] == jsonData['line']) {
                    zona_kemas = zonaList[i][0]
                    break;
                  }
                }
                document.getElementById('nomo_bulk_sisa').value = jsonData['nomo']
                document.getElementById('nama_fg').value = jsonData['nama_fg'].replace('�','')
                document.getElementById('snfg').value = jsonData['snfg']
                document.getElementById('nama_bulk').value = jsonData['nama_bulk'].replace('�',''),
                document.getElementById('kode_bulk').value = jsonData['koitem_bulk']
                document.getElementById('no_batch').value = jsonData['nobatch']
                document.getElementById('line_kemas').value = jsonData['line']
                document.getElementById('zona_kemas').value = zona_kemas
                document.getElementsByClassName('field-result')[0].style.display = "block"
                if (jsonData['nama_operator']!="") {
                  var operator = jsonData['nama_operator'].split(",")
                  for (var i = 0; i < operator.length; i++) {
                    $(document).ready(function() {
                      $('#nama_operator').tagsinput('add', operator[i]);
                      $('#nama_operator').tagsinput({
                        maxChars:4,
                        trimValue:true,
                        confirmKeys:[13,32,188]
                      });
                    });
                  }
                }
              }else{
                window.location = 'index.php?r=bulk-sisa/detail-bulk-info&obj='+result
              }
            }
          }
        });

        // Syntax dibawah hanya kebutuhan check
        // window.location = "index.php?r=bulk-sisa/get-data-by-nomo&nomo="+nomo
      }else{
        if (nomo.includes('/BS')) {
          $.get('index.php?r=bulk-sisa/get-data-by-nomo',{ nomo : nomo },function(result){
            if (result == 0) {
              customAlert('warning','SCAN GAGAL!','NOMO tidak tersedia pada jadwal planner')
              document.getElementsByClassName('field-result')[0].style.display = "none"
            }else if (result == 1){
              customAlert('warning','SCAN GAGAL!','NOMO belum masuk tahap olah')
              document.getElementsByClassName('field-result')[0].style.display = "none"
            }else if (result == 2){
              customAlert('warning','SCAN GAGAL!','NOMO belum masuk tahap kemas')
              document.getElementsByClassName('field-result')[0].style.display = "none"
            }else if(result == 3){
              customAlert('warning','SCAN GAGAL!','Device ini tidak memiliki hak scan')
              document.getElementsByClassName('field-result')[0].style.display = "none"
            }else{
              var jsonData = JSON.parse(atob(result))
              if (jsonData.nomo != '-') {
                codeReader.reset();
                document.getElementsByClassName('scan-layout')[0].style.display = 'none'
                if (jsonData['nama_operator'] == null) {
                  var zona_kemas = '-'
                  for (var i = 0; i < zonaList.length; i++) {
                    if (zonaList[i][1] == jsonData['line']) {
                      zona_kemas = zonaList[i][0]
                      break;
                    }
                  }
                  document.getElementById('nomo_bulk_sisa').value = jsonData['nomo']
                  document.getElementById('nama_fg').value = jsonData['nama_fg'].replace('�','')
                  document.getElementById('snfg').value = jsonData['snfg']
                  document.getElementById('nama_bulk').value = jsonData['nama_bulk'].replace('�',''),
                  document.getElementById('kode_bulk').value = jsonData['koitem_bulk']
                  document.getElementById('no_batch').value = jsonData['nobatch']
                  document.getElementById('line_kemas').value = jsonData['line']
                  document.getElementById('zona_kemas').value = zona_kemas
                  document.getElementsByClassName('field-result')[0].style.display = "block"
                }else{
                  if (jsonData['status'] == 'Belum Ditimbang') {
                    var zona_kemas = '-'
                    for (var i = 0; i < zonaList.length; i++) {
                      if (zonaList[i][1] == jsonData['line']) {
                        zona_kemas = zonaList[i][0]
                        break;
                      }
                    }
                    document.getElementById('nomo_bulk_sisa').value = jsonData['nomo']
                    document.getElementById('nama_fg').value = jsonData['nama_fg'].replace('�','')
                    document.getElementById('snfg').value = jsonData['snfg']
                    document.getElementById('nama_bulk').value = jsonData['nama_bulk'].replace('�',''),
                    document.getElementById('kode_bulk').value = jsonData['koitem_bulk']
                    document.getElementById('no_batch').value = jsonData['nobatch']
                    document.getElementById('line_kemas').value = jsonData['line']
                    document.getElementById('zona_kemas').value = zona_kemas
                    document.getElementsByClassName('field-result')[0].style.display = "block"
                    if (jsonData['nama_operator']!="") {
                      var operator = jsonData['nama_operator'].split(",")
                      for (var i = 0; i < operator.length; i++) {
                        $(document).ready(function() {
                          $('#nama_operator').tagsinput('add', operator[i]);
                          $('#nama_operator').tagsinput({
                            maxChars:4,
                            trimValue:true,
                            confirmKeys:[13,32,188]
                          });
                        });
                      }
                    }
                  }else{
                    window.location = 'index.php?r=bulk-sisa/detail-bulk-info&obj='+result
                  }
                }
              }else{
                document.getElementsByClassName('field-result')[0].style.display = "none";
                customAlert('info',"SCAN GAGAL!","Bulk Sisa ini belum menggunakan sistem, sehingga NOMO tidak dapat ditemukan. Silahkan proses secara manual!")
              }
            }
          });
        }else{
          document.getElementsByClassName('field-result')[0].style.display = "none";
          customAlert('warning',"SCAN GAGAL!","Pastikan anda scan NOMO pada SPK")
        }
      }
    } else {
      customAlert("warning","Form Belum Lengkap","Silahkan lengkapi NOMO Bulk Sisa terlebih dahulu")
      document.getElementsByClassName('field-result')[0].style.display = "none"
    }
  }

  // Function custom alert
  function customAlert(type,title,message){
    swal({
      closeOnClickOutside: false,
      title: title,
      text: message,
      icon: type,
      backdrop:true,
      button: "Mengerti!",
    });
  }

  function partition(str, n) {
    var res = [];
    var i, l;
    for(i = 0, l = str.length; i < l; i += n) {
      res.push(str.substr(i, n));
    }
    return res;
  };

</script>

</head>

<body oncopy="return false" oncut="return false" onpaste="return false">
  <!-- Body -->
  <div class="box box-widget widget-user">
    <div class="widget-user-header bg-green-old">
      <span class="title-text">Scan NOMO Bulk Sisa<br></span>
    </div>
    <div class="box-footer" style="margin-top: 0px; padding-top:0px;">
      <div class="btn-back" id="btn-back">
        <span class="text-back"><i class="fa fa-long-arrow-left" style="color:#4CAF50">&ensp;</i>Back</span>
      </div>
      <div class="scan-layout">
        <!-- Syntax Scanner -->
        <div class="sub-title-qr">
          <br>
          <span>Letakkan QR Code di dalam area</span>
          <br>
          <span class="sub-desc-qr">Scanning akan dimulai secara otomatis</span>
        </div>
        <div id="sourceSelectPanel" style="display:none">
          <label for="sourceSelect" style="display:none">Switch Camera</label>
          <select id="sourceSelect" style="max-width:400px; display:none;">x
          </select>
        </div>
        <div class="video-layout">
          <video id="video" width="100" height="100"></video>
        </div>
        <!-- Hingga line di Atas -->
        <div class="manual-layout">
          <center><h2><span>Jika kamera tidak bisa, input secara manual</span></h2></center>
          <button class="accordion bg-blue-gradient" style="padding:0px;">Input Manual (Klik Jika kamera tidak bisa)</button>
        </div>
        <div class="panel">
          <div class="col-md-12 column" style="margin-left:0px; margin-top:0px;width:100%">
            <label for="kode_manual">NOMO Bulk Sisa</label>
            <input type="text" name="nomo" id="nomo" value="">
            <button style="width:40%" onclick="check(document.getElementById('nomo').value)" >Check</button>
          </div>
        </div>
      </div>
      <div class="" style="margin-top:7rem;">

        <!-- Syntax Field Hasil Scan -->
        <div class="field-result">
          <div class="col-md-6">
            <div class="">
              <label for="nama_bulk">Nama FG</label>
              <input type="text" autocomplete="off" name="nama_fg" id="nama_fg" disabled>
            </div>
            <div class="">
              <label for="nama_bulk">Nama Bulk</label>
              <input type="text" autocomplete="off" name="nama_bulk" id="nama_bulk" disabled>
            </div>
            <div class="">
              <label for="no_batch">NOMO</label>
              <input type="text" autocomplete="off" name="nomo_bulk_sisa" id="nomo_bulk_sisa" disabled>
            </div>
            <div class="">
              <label for="kode_bulk">SNFG</label>
              <input type="text" autocomplete="off" name="kode_bulk" id="snfg" disabled>
            </div>
            <div class="">
              <label for="kode_bulk">Kode Bulk</label>
              <input type="text" autocomplete="off" name="kode_bulk" id="kode_bulk" disabled>
            </div>
          </div>
          <div class="col-md-6">
            <div class="">
              <label for="no_batch">No. Batch</label>
              <input type="text" autocomplete="off" name="no_batch" id="no_batch" disabled>
            </div>
            <div class="">
              <label for="zona_kemas">Zona Kemas</label>
              <input type="text" autocomplete="off" name="zona_kemas" id="zona_kemas" disabled>
            </div>
            <div class="">
              <label for="line_kemas">Line Kemas</label>
              <input type="text" autocomplete="off" name="line_kemas" id="line_kemas" disabled>
            </div>
            <div class="">
              <label for="nama_operator">Nama Operator</label>
              <input type="text" name="nama_operator" id="nama_operator" data-role="tagsinput"></input>
            </div>
          </div>
          <div class="col-md-12" style="margin-top:2rem">
            <button style="width:40%;" onclick="addBulk()" class="bg-green-old" id="btnStart">Start</button>
          </div>
        </div>
        <!-- Hingga Line di Atas -->
      </div>
    </div>
  </div>
  </body>

  <!-- Javascript -->
  <!-- Calling ZXing API CDN -->

<?php
$script = <<< JS
  $(document).ready(function() {
    var OS = platform.os.family;
    if (OS == "Android") {
      document.getElementById("video").style.width = '60%'
    }else{
      document.getElementById("video").style.width = '30%'
    }

    document.getElementById('btn-back').onclick = function(){
      window.location = "index.php?r=bulk-sisa/"
    }

    $('input[name="nama_operator"]').keydown(function(){
      var input = document.getElementsByName("nama_operator")[0];
      if (input.value.length > 0) {
        if (input.value.length % 4 == 0) {
          var e = jQuery.Event("keypress");
          e.which = 13; //enter keycode
          e.keyCode = 13;
          $('input[name="nama_operator"]').trigger(e);
        }
      }
      // console.log(text);
    })

    // /Syntax untuk handle button input manual
    var acc = document.getElementsByClassName("accordion");
    var i;
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = document.getElementsByClassName('panel')[0];
        if (panel.style.maxHeight) {
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        }
      });
    }
    // Hingga Line di Atas
  })
JS;
$this->registerJs($script);
?>
