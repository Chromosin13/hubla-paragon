<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use yii\helpers\Url;
use wbraganca\tagsinput\TagsinputWidget;

?>

<script type="text/javascript">
  var pic = '<?= Yii::$app->user->identity->username ?>';
  var valueSediaan = '';
  function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
      }
      return true;
  }

  function badgeContent(id,content){
    if (id.includes('status')) {
      if (content == "Siap Dikemas") {
        document.getElementById(id).style.backgroundColor = '#C1F3F3'
        document.getElementById(id).style.color = '#30D7D7'
        document.getElementById(id).innerHTML = '<b>Need Approval</b>'
      }else if (content == "Rejected") {
          document.getElementById(id).style.backgroundColor = '#F7D8D6'
          document.getElementById(id).style.color = '#D73A30'
          document.getElementById(id).innerHTML = '<b>Rejected</b>'
      }else if (content == "Waiting List") {
          document.getElementById(id).style.backgroundColor = '#FFE1B3'
          document.getElementById(id).style.color = '#FF9900'
          document.getElementById(id).innerHTML = '<b>'+content+'</b>'
      }else if (content == "Waiting Check") {
          document.getElementById(id).style.backgroundColor = '#FFE1B3'
          document.getElementById(id).style.color = '#FF9900'
          document.getElementById(id).innerHTML = '<b>'+content+'</b>'
      }else if (content == "On Progress") {
          document.getElementById(id).style.backgroundColor = '#dff0d8'
          document.getElementById(id).style.color = '#4CAF50'
          document.getElementById(id).innerHTML = '<b>'+content+'</b>'
      }else if (content == "Rework") {
          document.getElementById(id).style.backgroundColor = '#CCF9EF'
          document.getElementById(id).style.color = '#00E3AD'
          document.getElementById(id).innerHTML = '<b>'+content+'</b>'
      }else if (content == "Pending") {
          document.getElementById(id).style.backgroundColor = '#FDFF99'
          document.getElementById(id).style.color = '#969900'
          document.getElementById(id).innerHTML = '<b>'+content+'</b>'
      }else if (content == "Destroyed") {
          document.getElementById(id).style.backgroundColor = '#dff0d8'
          document.getElementById(id).style.color = '#4CAF50'
          document.getElementById(id).innerHTML = '<b>'+content+'</b>'
      }else{
          document.getElementById(id).style.backgroundColor = '#DDDDDD'
          document.getElementById(id).style.color = '#888888'
          document.getElementById(id).innerHTML = '<b>'+content+'</b>'
      }
    }

    if (id.includes('age')) {
      if (content.includes('Bulan')) {
        if (parseInt(content.split(' ')[0]) <= 5){
              document.getElementById(id).style.backgroundColor = '#FDFF99'
              document.getElementById(id).style.color = '#969900'
        }else if (parseInt(content.split(' ')[0]) > 5){
              document.getElementById(id).style.backgroundColor = '#F7D8D6'
              document.getElementById(id).style.color = '#D73A30'
        }else{
                document.getElementById(id).style.backgroundColor = '#DDDDDD'
                document.getElementById(id).style.color = '#888888'
        }
        document.getElementById(id).innerHTML = '<b>'+content+'</b>';
      }else if (content.includes('Jam')) {
        if (parseInt(content.split(' ')[0]) == 0){
                  document.getElementById(id).style.backgroundColor = '#DDDDDD'
                  document.getElementById(id).style.color = '#888888'
            document.getElementById(id).innerHTML = '<b>Baru Saja</b>'
        }else{
                document.getElementById(id).style.backgroundColor = '#DDDDDD'
                document.getElementById(id).style.color = '#888888'
          document.getElementById(id).innerHTML = '<b>'+content+'</b>'
        }
      }else{
                  document.getElementById(id).style.backgroundColor = '#DDDDDD'
                  document.getElementById(id).style.color = '#888888'
            document.getElementById(id).innerHTML = '<b>'+content+'</b>'
      }
    }

    if (id.includes('pic')) {
      if(content == 'in SPR'){
          document.getElementById(id).style.backgroundColor = '#E7B3FF'
          document.getElementById(id).style.color = '#AD00FF'
      }else if(content == 'in PPC'){
          document.getElementById(id).style.backgroundColor = '#ECEAD3'
          document.getElementById(id).style.color = '#BDB76B'
      }else if(content == 'in QC Bulk'){
          document.getElementById(id).style.backgroundColor = '#D8E8F2'
          document.getElementById(id).style.color = '#3C8DBC'
      }else if(content == 'in PRO'){
          document.getElementById(id).style.backgroundColor = '#DEF1EC'
          document.getElementById(id).style.color = '#6FB09A'
      }else if(content == 'in WPM'){
          document.getElementById(id).style.backgroundColor = '#F7D8D6'
          document.getElementById(id).style.color = '#D73A30'
      }else if (content == "-") {
          document.getElementById(id).style.backgroundColor = '#dff0d8'
          document.getElementById(id).style.color = '#4CAF50'
          document.getElementById(id).innerHTML = '<b>'+content+'</b>'
      }else{
          document.getElementById(id).style.backgroundColor = '#DDDDDD'
          document.getElementById(id).style.color = '#888888'
      }
      document.getElementById(id).innerHTML = '<b>'+content+'</b>'
    }
  }

  function toDetailInfo(id,nomo,snfg){
    var object = {
      'nomo':nomo,
      'snfg':snfg,
    }
    document.getElementById(id).style.backgroundColor = "#D8E8F2"
    document.getElementById(id).style.color = "#3C8DBC"
    document.getElementById(id).style.cursor = "pointer"
    document.getElementById(id).onclick = function(){
      window.location ='index.php?r=bulk-sisa/detail-bulk-info&obj='+btoa(JSON.stringify(object))
    }
  }

  function changeSediaan(){
    var bsTable = $('#tableBulkSisa').DataTable()
    var sediaan = document.getElementById('sediaan').value;
    if (sediaan == 'all') {
      bsTable.columns(5).search('').draw();
      valueSediaan = '';
    }else if (sediaan == 'liquid') {
      bsTable.columns(5).search('LIQUID').draw();
      valueSediaan = 'LIQUID';
    }else if (sediaan == 'powder') {
      bsTable.columns(5).search('POWDER').draw();
      valueSediaan = 'POWDER';
    }else if (sediaan == 'semsol') {
      bsTable.columns(5).search('SEMISOLID').draw();
      valueSediaan = 'SEMISOLID';
    }else{
      bsTable.columns(5).search('VARCOS').draw();
      valueSediaan = 'VARCOS';
    }
  }

  function btnDownloadPPC(){
    var bsTable = $('#tableBulkSisa').DataTable()
    console.log(bsTable.page.info().recordsDisplay);
    if (bsTable.page.info().recordsDisplay > 0) {
      window.open('index.php?r=bulk-sisa/download-ppc&sediaan='+document.getElementById('sediaan').value)
    }else{
      swal({
        title: "Peringatan!",
        text: "Tidak ada bulk sisa yang sedang menunggu penjadwalan.",
        icon: "warning",
        allowOutsideClick:false,
        closeOnClickOutside:false
      })
    }
  }

  function btnDownloadSPR(){
    var bsTable = $('#tableBulkSisa').DataTable()
    console.log(bsTable.page.info().recordsDisplay);
    if (bsTable.page.info().recordsDisplay > 0) {
      window.open('index.php?r=bulk-sisa/download-spr&sediaan='+document.getElementById('sediaan').value)
    }else{
      swal({
        title: "Peringatan!",
        text: "Tidak ada bulk sisa yang sedang menunggu demands.",
        icon: "warning",
        allowOutsideClick:false,
        closeOnClickOutside:false
      })
    }
  }
</script>

<style media="screen">

  .swal-button{
    width: 50%
  }

  .swal-button-container{
    width: 100%
  }

  code{
    padding: 0.4em;
  }

  blockquote span{
    font-size: 1em
  }

  #bulk-reject-info:hover,#bulk-rework-info:hover,#bulk-aging-info:hover{
    cursor: pointer;
  }

  .swal-button--confirm{
    background-color: #4CAF50!important;
  }

  .swal-footer,.swal-text{
    text-align: center!important;
  }

  ::-webkit-input-placeholder {
    font-style: italic;
  }
  :-moz-placeholder {
    font-style: italic;
  }
  ::-moz-placeholder {
    font-style: italic;
  }
  :-ms-input-placeholder {
    font-style: italic;
  }
  .bg-green-old{
    background-color: #4CAF50;
    color: white
  }

  .img-circle .project-logo{
    background-color: #B1CCA3;
  }

  .box-content{
    background-color: white;
    border-radius: 24px;
    padding: 24px;
    margin-top: 64px!important;
  }

  input#bulksisa-search{
    width: 20%;
    border: solid 2px #DDDFE2;
    border-radius: 10px;
    background: url('../web/images/loupe.svg') no-repeat scroll left 1rem center!important;
    background-size: auto 1.8rem!important;
    padding-left: 3.6rem!important;
    padding-top: 2rem!important;
    padding-bottom: 2rem!important
  }

  hr.rounded{
  border-top: 2px solid #DDDFE2;
  border-radius: 5px;
  margin-bottom: 2rem;
  }

  .filter-text{
    vertical-align: middle;
    color: #DDDFE2;
    font-weight: 600
  }

  .filter:hover .filter-text{
    color: #4CAF50;
  }

  .filter:hover{
    background-color: white;
    border: 2px solid #4CAF50;
    color: #4CAF50;
  }

  .filter{
    text-align: center;
    border: 2px solid #DDDFE2;
    border-radius: 10px;
    margin-right: 0.6rem;
    padding:1rem 1rem;
    margin-bottom: 2rem;
    width: 10rem
  }

  .filter-selected{
    background: #4CAF50;
    border: 2px solid #4CAF50;
  }

  .filter-selected .filter-text{
    color: white;
  }

  .dataTables_wrapper .dataTables_length {
    float: right;
    margin-bottom: 1rem;
    color:#AAA
  }

  .dataTables_wrapper .dataTables_length select {
    padding: 0.6rem;
    text-align: center!important;
    color: #4CAF50
  }

  .table-responsive{
    scrollbar-width:none;
  }

  table tbody{
    color: #555555!important;
  }

  table{
    border-bottom: 1px solid #EFEFEF!important;
  }

  table.table > thead > tr > th{
    height: 36px!important;
    vertical-align: middle!important;
    color:#4CAF50!important;
    padding-left: 24px!important
  }

  table.table > tbody > tr > td{
    border-bottom: 1px solid #EFEFEF!important;
    height: 36px!important;
    vertical-align: middle!important;
    padding-left: 24px!important
  }

  .dataTables_info{
    color:#A1A1A1!important;
  }

  .pagination{
    float: right;
    text-align: right;
  }

  .page-control:hover{
    cursor: pointer;
  }

  .vl {
    border-left: 2px solid #DDD;
    height: 16px;
  }

  .field-page{
    width: 6%!important;
    border: 1px solid #AAA!important;
    border-radius: 6px!important;
    text-align: center!important;
  }

  .dataTables_paginate,.dataTables_filter{
    display: none;
  }

  .btn-primary-add:hover{
    background-color: #3282AA;
    color:white;
  }

  .btn-primary-add{
    border-radius: 10px;
    background-color: #3C8DBC;
    color: white;
    float: right;
    width: 20rem;
    padding-top: 1.1rem!important;
    padding-bottom: 1.1rem!important
  }

  .badge-content{
    border-radius:1rem;
    display: inline-block;
    text-align: center;
    width: 16rem;
  }

  .badge-content:hover{
    cursor: text
  }

  #sediaan{
    border: none;
    color: #4CAF50;
    padding: 1rem;
    border-radius: 10px;
    font-weight: bold;
    background-color: transparent
  }

  #sediaan:hover{
    cursor: pointer;
  }

  #sediaan[0]{
    color: #AAA
  }

  .content-head-desc{
    font-size: 1.6rem;
    margin: 1rem 0rem;
    border: 2px solid #DDD;
    width: 15%;
    padding-left: 0.8rem;
    border-radius: 10px
  }
</style>

<div class="box box-widget widget-user">
  <!-- Add the bg color to the header using any of the bg-* classes -->
  <div class="widget-user-header bg-green-old">
    <h3 class="widget-user-username"><b>Bulk Sisa</b></h3>
    <h5 class="widget-user-desc">List of Bulk Sisa</h5>
  </div>
  <div class="widget-user-image">
    <img class="img-circle project-logo" src="../web/images/proses_icons/recycle_bs.svg" alt="BulkSisaIcon">
  </div>
</div>
<div class="box-content">
    <?php if (in_array(Yii::$app->user->identity->username,['kemas'])) { ?>
      <button id="btnAddJadwal" type="button" name="button" class="btn btn-primary-add"><b><i class="fa fa-search">&ensp;</i>Scan NOMO</b></button>
    <?php } ?>
    <div class="flow-input-mo-form">
        <div class="form-group field-search">
          <input type="text" id="bulksisa-search" class="form-control" name="bulksisa-search" aria-invalid="false" placeholder="Search by any parameter">
        </div>
    </div>
    <hr class="rounded">
    <div class="">
      <div class="content-head-desc">
        <span>Sediaan:</span>
        <select class="" name="sediaan" id="sediaan" onchange="changeSediaan()">
          <option value="all" selected>Semua</option>
          <option value="liquid">Liquid</option>
          <option value="powder">Powder</option>
          <option value="semsol">Semi Solid</option>
          <option value="varcos">Varcos</option>
        </select>
      </div>
    </div>
    <div class="filters">
      <div class="filter filter-selected btn" id="btnAll">
        <span class="filter-text">ALL</span>
      </div>

      <div class="filter btn" id="btnSPR">
        <span class="filter-text">SPR</span>
      </div>

      <div class="filter btn" id="btnPPC">
        <span class="filter-text">PPC</span>
      </div>

      <div class="filter btn" id="btnQC">
        <span class="filter-text">QC BULK</span>
      </div>

      <div class="filter btn" id="btnWPM">
        <span class="filter-text">WPM</span>
      </div>
    </div>
    <?php if (in_array(Yii::$app->user->identity->username,['kemas'])) { ?>
      <div class="">
        <?php if ($countAgesBulk > 0) { ?>
          <blockquote class="bg-warning" id="bulk-aging-info" style="border-left: 5px solid #8a6d3b">
            <span class="text-warning"><i class="fa fa-circle-o"></i>&ensp;Bulk sisa yang <b>EXPIRED</b> sebanyak <code style="color:#8a6d3b"><b><?= $countAgesBulk; ?> bulk</b></code>.</span>
          </blockquote>
        <?php }else{ ?>
          <blockquote class="bg-success" style="border-left: 5px solid #4CAF50">
              <span class="text-success"><i class="fa fa-circle-o"></i>&ensp;Tidak ada bulk sisa yang <b>EXPIRED</b>.</span>
          </blockquote>
        <?php } ?>
      </div>
    <?php } ?>
    <?php if (in_array(Yii::$app->user->identity->username,['spr','planner'])) { ?>
      <div class="">
        <?php if ($countRejected > 0) { ?>
          <blockquote class="bg-danger" id="bulk-reject-info" style="border-left: 5px solid #c7254e">
            <span class="text-danger"><i class="fa fa-circle-o"></i>&ensp;Bulk sisa yang <b>REJECT</b> hari ini sebanyak <code><b><?= $countRejected; ?> bulk</b></code>.</span>
          </blockquote>
        <?php }else{ ?>
          <blockquote class="bg-success" style="border-left: 5px solid #4CAF50">
              <span class="text-success"><i class="fa fa-circle-o"></i>&ensp;Tidak ada bulk sisa yang <b>REJECT</b> hari ini.</span>
          </blockquote>
        <?php } ?>
      </div>
      <?php } ?>
        <?php if (in_array(Yii::$app->user->identity->username,['planner'])) { ?>
          <div class="">
            <?php if ($countRework[0]['count'] > 0) { ?>
              <blockquote class="bg-warning" id="bulk-rework-info" style="border-left: 5px solid #8a6d3b">
                <span class="text-warning"><i class="fa fa-circle-o"></i>&ensp;Bulk sisa yang <b>REWORK</b> hari ini sebanyak <code style="color:#8a6d3b"><b><?= $countRework[0]['count']; ?> bulk</b></code>.</span>
              </blockquote>
            <?php }else{ ?>
              <blockquote class="bg-success" style="border-left: 5px solid #4CAF50">
                  <span class="text-success"><i class="fa fa-circle-o"></i>&ensp;Tidak ada bulk sisa yang <b>REWORK</b> hari ini.</span>
              </blockquote>
            <?php } ?>
          </div>
          <div class="">
            <?php if ($countAgesBulk > 0) { ?>
              <blockquote class="bg-warning" id="bulk-aging-info" style="border-left: 5px solid #8a6d3b">
                <span class="text-warning"><i class="fa fa-circle-o"></i>&ensp;Bulk sisa yang <b>EXPIRED</b> sebanyak <code style="color:#8a6d3b"><b><?= $countAgesBulk; ?> bulk</b></code>.</span>
              </blockquote>
            <?php }else{ ?>
              <blockquote class="bg-success" style="border-left: 5px solid #4CAF50">
                  <span class="text-success"><i class="fa fa-circle-o"></i>&ensp;Tidak ada bulk sisa yang <b>EXPIRED</b>.</span>
              </blockquote>
            <?php } ?>
          </div>
          <?PHP }?>

          <?php if (in_array(Yii::$app->user->identity->username,['planner'])) { ?>
            <button class="btn btn-primary" style="border-radius:10px; font-size:1.6rem" type="button" name="button" id="btnDownloadPPC" onclick="btnDownloadPPC()">Download List Bulk Sisa</button>
          <?php } ?>
          <?php if (in_array(Yii::$app->user->identity->username,['spr'])) { ?>
            <button class="btn btn-primary" style="border-radius:10px; font-size:1.6rem" type="button" name="button" id="btnDownloadSPR" onclick="btnDownloadSPR()">Download List Bulk Sisa</button>
          <?php } ?>
    <div class="table-responsive">
      <table class="table table-hover datatable" id="tableBulkSisa">
        <thead>
          <tr class="active">
            <th scope="col">NO.</th>
            <th scope="col">NOMO</th>
            <!-- <th scope="col">KODE ITEM</th> -->
            <th scope="col">SNFG</th>
            <th scope="col">SNFG BARU</th>
            <th scope="col">NAMA BULK</th>
            <th scope="col">SEDIAAN & ZONA</th>
            <th scope="col">AGES</th>
            <th scope="col">PIC</th>
            <th scope="col">STATUS</th>
            <th scope="col">IS EXPIRED</th>
            <th scope="col">ACTION</th>
          </tr>
        </thead>
        <tbody>
          <?php
            for ($i=0; $i < count($bulksisa); $i++) {
              date_default_timezone_set('Asia/Jakarta');
              // $tgl_timbang = strtotime($bulksisa[$i]['tgl_timbang_bulk_sisa']);
              $tgl_olah = strtotime($flowMO['datetime_start']);
              $now = time();
              $diff = abs($now - $tgl_olah);
              $years = floor($diff / (365*60*60*24));
              $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
              $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
              $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
              $ages = '-';
              if ($months < 1) {
                $weeks = floor($days/7);
                if ($weeks < 1) {
                  if ($days < 1) {
                    $ages = $hours." Jam";
                  }else{
                    $ages = $days." Hari";
                  }
                }else{
                  $ages = $weeks." Minggu";
                }
              }else{
                $ages = $months." Bulan";
              }
          ?>
              <tr id="row_<?= $i; ?>">
                <td scope="row"></td>
                <td><?= $bulksisa[$i]['nomo'] ?></td>
                <!-- <td><?php //$bulksisa[$i]['koitem_fg'] ?></td> -->
                <td><?= $bulksisa[$i]['snfg_awal'] ?></td>
                <td><?= $bulksisa[$i]['snfg_baru'] ?? '<i style="color:'.'#AAA'.'">(not set)</i>'; ?></td>
                <td><?= $bulksisa[$i]['nama_bulk'] ?></td>
                <?php
                  /*if (strpos($bulksisa[$i]['sediaan'],'S') !== false) {
                    $sediaan = 'SEMISOLID & '.$bulksisa[$i]['zona'];
                  }else if (strpos($bulksisa[$i]['sediaan'],'L') !== false) {
                    $sediaan = 'LIQUID & '.$bulksisa[$i]['zona'];
                  }else if (strpos($bulksisa[$i]['sediaan'],'P') !== false) {
                    $sediaan = 'POWDER & '.$bulksisa[$i]['zona'];
                  }else if (strpos($bulksisa[$i]['sediaan'],'V') !== false) {
                    $sediaan = 'VARCOS & '.$bulksisa[$i]['zona'];
                  }else{
                    $sediaan = $bulksisa[$i]['sediaan']." & ".$bulksisa[$i]['zona'];
                  }*/
                  $sediaan = $bulksisa[$i]['zona'];
                ?>
                <td><?= $sediaan ?></td>
                <td><span class="badge-content btn" id="age_<?= $i; ?>"></span></td>
                <td><span class="badge-content btn" id="pic_<?= $i; ?>"></span></td>
                <td><span class="badge-content btn" id="status_<?= $i; ?>"></span></td>
                <td><span class="badge-content btn"><?= $bulksisa[$i]['is_expired'] ?? 0 ?></span></td>
                <td><span class="badge-content btn" id="action_<?= $i; ?>"><i class="fa fa-search"></i><b>&ensp;See Details</b></span></td>
              </tr>
              <script type="text/javascript">
              var months = <?= $months; ?>;
              var isExpired = <?= $bulksisa[$i]['is_expired'] ?? 0 ?>;
              if (months > 3 && months < 6) {
                document.getElementById('row_<?= $i; ?>').style.backgroundColor = '#FEFFCC';
              }else if (months > 5 && isExpired == 0) {
                document.getElementById('row_<?= $i; ?>').style.backgroundColor = '#FFCFCC';
              }
              badgeContent('age_<?= $i; ?>','<?= $ages?>');
              badgeContent('status_<?= $i; ?>','<?= $bulksisa[$i]['status_bulksisa'] ?? 'On Weighing'?>');
              badgeContent('pic_<?= $i; ?>','<?= $bulksisa[$i]['pic'] ?? 'in Operator'?>');
              toDetailInfo('action_<?= $i; ?>','<?= $bulksisa[$i]['nomo']?>','<?= $bulksisa[$i]['snfg_awal'] ?>')
              </script>
            <?php }
          ?>
        </tbody>
      </table>
      <h1 id="pageInfo"></h1>
      <div class="pagination">
        <button type="button" name="button" class="page-control previous-btn" style="font-size:14px;color:#555;border:none;background:none">
          <i class="glyphicon glyphicon-menu-left" style="color:#4CAF50"></i>&ensp;PREVIOUS&ensp;
        </button>
        <span class="vl">&ensp;</span>
        <input class="field-page" id="field-page" type="text" name="" value="" onkeypress="return isNumber(event)">
        <span>&ensp;out of&ensp;</span>
        <span class="max-pages"></span>
        <span class="vl">&ensp;</span>
        <button type="button" name="button" class="page-control next-btn" style="font-size:14px;color:#555;border:none;background:none">
          &ensp;NEXT&ensp;<i class="glyphicon glyphicon-menu-right" style="color:#4CAF50"></i>
        </button>
      </div>
    </div>
</div>
<?php
$script = <<< JS
  $(document).ready(function() {
    // var dns = 'https://factory.pti-cosmetics.com/flowreport/web/';
    // var dns = 'https://factory.pti-cosmetics.com/flowreport_dev/web/';
    var dns ='';

    /*INI JQUERY UNTUK CUSTOMIZE TABLE*/
    var bsTable = $('#tableBulkSisa').DataTable({
      "language": {
        "info": "Showing <b>_START_</b> - <b>_END_</b> of <b>_TOTAL_</b> results.",
        "paginate": {
                        "next": '<span>NEXT<i class="glyphicon glyphicon-menu-right"></i></span>',
                        "previous": '<span style="font-size:14px;color:#555"><i class="glyphicon glyphicon-menu-left" style="color:#4CAF50"></i>PREVIOUS</span>'
                    }
      },
      "paging":true,
      "pagingType":"simple_numbers",
      "searching":true,
      "order":[],
      "lengthChange":true,
      "dom": '<"top"il>rt<"bottom"fp><"clear">',
      "columnDefs": [
        {
              "searchable": true,
              "orderable": false,
              // "targets": [1,2,3,4,5,6,7,8]
              "targets": [0,10]
          },
          {
                "searchable": true,
                "orderable": true,
                // "targets": [1,2,3,4,5,6,7,8]
                "targets": [1,2,3,4,5,6,7,8]
          },
          {
              "targets": [ 9 ],
              "visible": false
          } ],
        "order": [[ 1, 'asc' ]]
    })
    bsTable.on( 'order.dt search.dt', function () {
        bsTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $('#bulksisa-search').keyup(function(){
        bsTable.search($(this).val()).draw(true);
        $('.max-pages').text(bsTable.page.info().pages || 1);
    });
    $('.previous-btn').click(function(){
      bsTable.page("previous").draw(false);
      $('#field-page').val((bsTable.page.info().page+1))
    });
    $('.next-btn').click(function(){
      bsTable.page("next").draw(false);
      $('#field-page').val((bsTable.page.info().page+1))
    });
    $('.max-pages').text(bsTable.page.info().pages || 1);
    $('#field-page').val(bsTable.page.info().page+1)
    $('#field-page').keypress((e) => {
        // Enter key corresponds to number 13
        if (e.which === 13) {
            $('#field-page').submit();
            var currPage = $('#field-page').val()
            if (typeof(parseInt(currPage)) === 'number') {
              if (bsTable.page.info().pages > 1) {
                if (currPage >= bsTable.page.info().pages) {
                  pageToGoTo = bsTable.page.info().pages-1;
                  displayPage = bsTable.page.info().pages;
                }else if (currPage <= 0) {
                  pageToGoTo = 0;
                  displayPage = (pageToGoTo+1);
                }else{
                  pageToGoTo =(currPage-1);
                  displayPage = currPage;
                }
              }else{
                pageToGoTo = 0;
                displayPage = (pageToGoTo+1);
              }
            }else{
              pageToGoTo = bsTable.page.info().page;
              displayPage = (bsTable.page.info().page+1);
            }
            $('#field-page').val(displayPage)
            bsTable.page(parseInt(pageToGoTo)).draw(false);
        }
    })
    $('#tableBulkSisa').on( 'length.dt', function () {
      $('.max-pages').text(bsTable.page.info().pages || 1);
    });
    /*HINGGA LINE DI ATAS*/

    /*INI QUERY UNTUK HANDLE BUTTON-BUTTON*/

    $('#bulksisa-search').on('keyup', function() {
      console.log(key);
      var key = $(this).val();
      if (key.length > 0){
        if (document.getElementById('btnDownloadPPC')) {
          document.getElementById('btnDownloadPPC').style.display = 'none'
        }
        if (document.getElementById('btnDownloadSPR')) {
          document.getElementById('btnDownloadSPR').style.display = 'none'
        }
        $("#btnAll").attr('class', 'filter btn');
        $("#btnSPR").attr('class', 'filter btn');
        $("#btnPPC").attr('class', 'filter btn');
        $("#btnWPM").attr('class', 'filter btn');
        $("#btnQC").attr('class', 'filter btn');
      }
    });

    if (document.getElementById('bulk-reject-info')) {
      document.getElementById('bulk-reject-info').onclick = function(){
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var exc = '(not set)'

                //process only if input value
                if (exc.length > 0) {
                  var vfw = data[2]; // use data for the vfw column
                  var ref = new RegExp(exc, 'g');

                  // VFW
                  if (vfw.match(ref))
                  {
                      return false;
                  }
                }

                return true;
            }
        );
        bsTable.search('').draw(true) ;
        bsTable.columns().search('').draw();
        $("#btnAll").attr('class', 'filter btn');
        $("#btnSPR").attr('class', 'filter btn');
        $("#btnPPC").attr('class', 'filter btn');
        $("#btnWPM").attr('class', 'filter btn');
        $("#btnQC").attr('class', 'filter btn');
        bsTable.columns(8).search('Rejected').draw();
        bsTable.columns(3).search('-').draw();
        $('.max-pages').text(bsTable.page.info().pages || 1);
        $('#field-page').val(1)
        if (document.getElementById('btnDownloadPPC')) {
          document.getElementById('btnDownloadPPC').style.display = 'none'
        }
        if (document.getElementById('btnDownloadSPR')) {
          document.getElementById('btnDownloadSPR').style.display = 'none'
        }
      }
    }

    if (document.getElementById('bulk-aging-info')) {
      document.getElementById('bulk-aging-info').onclick = function(){
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var exc = data[6]
                if (exc.includes(' Bulan')) {
                  var numsStr = exc.replace(/[^0-9]/g,'');

                  //process only if input value
                  if (parseInt(numsStr) < 6) {
                    return false;
                  }
                  if (data[9] == 1) {
                    return false;
                  }
                  return  true
                }else{
                  return false;
                }
            }
        );
        bsTable.search('').draw(true) ;
        bsTable.columns().search('').draw();
        $("#btnAll").attr('class', 'filter btn');
        $("#btnSPR").attr('class', 'filter btn');
        $("#btnPPC").attr('class', 'filter btn');
        $("#btnWPM").attr('class', 'filter btn');
        $("#btnQC").attr('class', 'filter btn');
        $('.max-pages').text(bsTable.page.info().pages || 1);
        $('#field-page').val(1)
        if (document.getElementById('btnDownloadPPC')) {
          document.getElementById('btnDownloadPPC').style.display = 'none'
        }
        if (document.getElementById('btnDownloadSPR')) {
          document.getElementById('btnDownloadSPR').style.display = 'none'
        }
      }
    }

    if (document.getElementById('bulk-rework-info')) {
      document.getElementById('bulk-rework-info').onclick = function(){
        bsTable.search('').draw(true) ;
        bsTable.columns().search('').draw();
        $("#btnAll").attr('class', 'filter btn');
        $("#btnSPR").attr('class', 'filter btn');
        $("#btnPPC").attr('class', 'filter btn');
        $("#btnWPM").attr('class', 'filter btn');
        $("#btnQC").attr('class', 'filter btn');
        bsTable.columns(8).search('Rework').draw();
        $('.max-pages').text(bsTable.page.info().pages || 1);
        $('#field-page').val(1)
        if (document.getElementById('btnDownloadPPC')) {
          document.getElementById('btnDownloadPPC').style.display = 'none'
        }
        if (document.getElementById('btnDownloadSPR')) {
          document.getElementById('btnDownloadSPR').style.display = 'none'
        }
      }
    }

    document.getElementById('btnAll').onclick = function(){
        bsTable.search('').draw(true) ;
        $.fn.dataTable.ext.search.pop();
        $("#btnAll").attr('class', 'filter filter-selected btn');
        $("#btnSPR").attr('class', 'filter btn');
        $("#btnPPC").attr('class', 'filter btn');
        $("#btnWPM").attr('class', 'filter btn');
        $("#btnQC").attr('class', 'filter btn');
        $('.max-pages').text(bsTable.page.info().pages || 1);
        $('#field-page').val(1)
        bsTable.columns(6).search('').draw(true);
        bsTable.columns(8).search('').draw(true);
        bsTable.columns(3).search('').draw(true);
        if (document.getElementById('btnDownloadPPC')) {
          document.getElementById('btnDownloadPPC').style.display = 'none'
        }
        if (document.getElementById('btnDownloadSPR')) {
          document.getElementById('btnDownloadSPR').style.display = 'none'
        }
    }

    document.getElementById('btnSPR').onclick = function(){
        bsTable.search('in SPR').draw(true) ;
        $.fn.dataTable.ext.search.pop();
        $("#btnAll").attr('class', 'filter btn');
        $("#btnSPR").attr('class', 'filter filter-selected btn');
        $("#btnPPC").attr('class', 'filter btn');
        $("#btnWPM").attr('class', 'filter btn');
        $("#btnQC").attr('class', 'filter btn');
        $('.max-pages').text(bsTable.page.info().pages || 1);
        $('#field-page').val(1)
        bsTable.columns().search('').draw();if (document.getElementById('btnDownloadPPC')) {
          document.getElementById('btnDownloadPPC').style.display = 'none'
        }
        if (document.getElementById('btnDownloadSPR')) {
          document.getElementById('btnDownloadSPR').style.display = 'block'
        }
    }

    document.getElementById('btnPPC').onclick = function(){
        bsTable.search('in PPC').draw(true) ;
        $.fn.dataTable.ext.search.pop();
        $("#btnAll").attr('class', 'filter btn');
        $("#btnSPR").attr('class', 'filter btn');
        $("#btnPPC").attr('class', 'filter filter-selected btn');
        $("#btnWPM").attr('class', 'filter btn');
        $("#btnQC").attr('class', 'filter btn');
        $('.max-pages').text(bsTable.page.info().pages || 1);
        $('#field-page').val(1)
        bsTable.columns().search('').draw();
        if (document.getElementById('btnDownloadPPC')) {
          document.getElementById('btnDownloadPPC').style.display = 'block'
        }
        if (document.getElementById('btnDownloadSPR')) {
          document.getElementById('btnDownloadSPR').style.display = 'none'
        }
    }

    document.getElementById('btnQC').onclick = function(){
        bsTable.search('in QC').draw(true) ;
        $.fn.dataTable.ext.search.pop();
        $("#btnAll").attr('class', 'filter btn');
        $("#btnSPR").attr('class', 'filter btn');
        $("#btnPPC").attr('class', 'filter btn');
        $("#btnWPM").attr('class', 'filter btn');
        $("#btnQC").attr('class', 'filter filter-selected btn');
        $('.max-pages').text(bsTable.page.info().pages || 1);
        $('#field-page').val(1)
        bsTable.columns().search('').draw();
        if (document.getElementById('btnDownloadPPC')) {
          document.getElementById('btnDownloadPPC').style.display = 'none'
        }
        if (document.getElementById('btnDownloadSPR')) {
          document.getElementById('btnDownloadSPR').style.display = 'none'
        }
    }

    document.getElementById('btnWPM').onclick = function(){
        bsTable.search('in WPM').draw(true) ;
        $.fn.dataTable.ext.search.pop();
        $("#btnAll").attr('class', 'filter btn');
        $("#btnSPR").attr('class', 'filter btn');
        $("#btnPPC").attr('class', 'filter btn');
        $("#btnWPM").attr('class', 'filter filter-selected btn');
        $("#btnQC").attr('class', 'filter btn');
        $('.max-pages').text(bsTable.page.info().pages || 1);
        $('#field-page').val(1)
        bsTable.columns().search('').draw();
        if (document.getElementById('btnDownloadPPC')) {
          document.getElementById('btnDownloadPPC').style.display = 'none'
        }
        if (document.getElementById('btnDownloadSPR')) {
          document.getElementById('btnDownloadSPR').style.display = 'none'
        }
    }

    if (pic == 'qcbulk') {
      document.getElementById('btnQC').click();
    }else if (pic == 'spr') {
      document.getElementById('btnSPR').click();
    }else if (pic == 'planner') {
      document.getElementById('btnPPC').click();
    }else if (pic == 'wpm') {
      document.getElementById('btnWPM').click();
    } else{
      document.getElementById('btnAll').click();
    }

    $('#btnAddJadwal').click(function(){
        window.location = dns+"index.php?r=bulk-sisa/scan-nomo"
    })
    /*HINGGA LINE DI ATAS*/
  });
JS;
$this->registerJs($script);
?>
