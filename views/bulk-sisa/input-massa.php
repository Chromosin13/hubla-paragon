<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use app\models\LogPenimbanganRm;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm  */
?>

<link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">
<style>
  .column {
    float: left;
    width: 50%;
  }

  .accordion {

    color: white;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
  }

  .active, .accordion:hover {
    background-color: #ccc;
  }

  .accordion:after {
    content: '\002B';
    color: white;
    font-weight: bold;
    float: right;
    margin-left: 5px;
  }

  .active:after {
    content: "\2212";
    color: white;
  }

  .panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
  }

  /* Clear floats after the columns */
  .row:after {
    content: "";
    display: table;
    clear: both;
  }

  .bg-green-old{
    background-color: #4CAF50;
    color: white;
    border: none;
  }

  .btn-back{
    float: left;
    padding: 1em;
    margin-bottom: 2em!important;
  }

  .btn-back:hover{
    cursor: pointer;
  }

  .text-back{
    color: #888;
    font-size: 2rem;
    font-weight: 600;
  }

  .swal-button-container{
    text-align: center!important;
    display: inline-block;
  }

  .swal-footer{
    text-align: center;
  }

  .swal-text{
    text-align: center;
  }

  .swal-button--confirm{
    background-color: #4CAF50
  }

  .swal-button:hover{
    background-color: #aaa!important
  }


  .swal-button{
    vertical-align: middle!important;
    padding: 0em 1em!important;
    text-transform: capitalize;
    width: 10em;
  }

</style>

<script type="text/javascript">

  var threshold = 0;
  var netto_refil = <?= $netto_refil['child_1_qty'] ?? 0;?>;
  var het = <?= $het['fixed_price'] ?>; //Set default Rp75.000
  var hargaBulk = het * netto_refil;
  var maxValue = 0;
  var sumQty = <?= $sumQty['sum'] ?? 0; ?>;
  var syaratZona =
  [
    ['LIQUID A','TUP01'],['LIQUID A','TUP02'],['LIQUID A','TUP03'],['LIQUID A','TUP04'],
    ['LIQUID A','TUP05'],['LIQUID A','TUP06'],['LIQUID A','TUP07'],['LIQUID A','TUP08'],
    ['LIQUID A','TUP09'],['LIQUID B','JAP01'],['LIQUID B','BLP01'],['LIQUID B','BLP02'],
    ['LIQUID B','BOP01'],['LIQUID B','BOP02'],['LIQUID B','BOP03'],['LIQUID B','BOP04'],
    ['LIQUID B','BOP05'],['LIQUID B','BOP06'],['LIQUID B','EMP02'],['LIQUID B','EMP05'],
    ['LIQUID B','LQP04'],['LIQUID C','JAP03'],['LIQUID C','BOP08'],['LIQUID C','BOP09'],
    ['LIQUID C','BOP10'],['LIQUID C','CUP01'],['LIQUID C','EMP01'],['LIQUID C','EMP03'],
    ['LIQUID C','EMP07'],['LIQUID C','EMP15'],['LIQUID C','EMP16'],['LIQUID C','EMP17'],
    ['LIQUID C','OSP01'],

    ['POWDER A','LPP01'],['POWDER A','LPP02'],['POWDER A','LPP03'],['POWDER A','PNP01'],
    ['POWDER A','PNP03'],['POWDER A','PNP04'],['POWDER A','PNP01MANUAL'],['POWDER A','PNP04MANUAL'],
    ['POWDER B','PCP01'],['POWDER B','PCP02'],['POWDER B','PCP03'],['POWDER B','PCP04'],
    ['POWDER B','PCP05'],['POWDER B','PEP01'],['POWDER B','PEP02'],['POWDER B','PEP04'],
    ['POWDER B','PEP05'],['POWDER B','PEP06'],

    ['BOTTLE LINE - VARCOS','VSN01'],['BOTTLE LINE - VARCOS','VSN02'],['BOTTLE LINE - VARCOS','VSN03'],['BOTTLE LINE - VARCOS','VMN01'],['BOTTLE LINE - VARCOS','VMN02'],

    ['BOTTLE LINE - SEMISOLID','BTP02'],['BOTTLE LINE - SEMISOLID','BTP04'],['BOTTLE LINE - SEMISOLID','LIP11'],
    ['BOTTLE LINE - SEMISOLID','LIP18'],['BOTTLE LINE - SEMISOLID','LRP01'],

    ['JAR & FOUNDATION - SEMISOLID','FOP03'],['JAR & FOUNDATION - SEMISOLID','JRP02'],['JAR & FOUNDATION - SEMISOLID','SBP01'],
    ['LIPSTICK LINE - SEMISOLID','SAP01'],['LIPSTICK LINE - SEMISOLID','ALP03'],['LIPSTICK LINE - SEMISOLID','LIP06'],
    ['LIPSTICK LINE - SEMISOLID','LIP08'],['LIPSTICK LINE - SEMISOLID','LIP10'],['LIPSTICK LINE - SEMISOLID','LIP12'],
    ['LIPSTICK LINE - SEMISOLID','LIP14'],['LIPSTICK LINE - SEMISOLID','LIP16'], ['LIPSTICK LINE - SEMISOLID','SMALLBATCH']
  ]
  var syaratList =
  [
    ['LIQUID A',30.0,100],['LIQUID B',30.0,100],['LIQUID C',10.0,100],
    ['POWDER A',4.0,50],['POWDER B',2.0,50],
    ['BOTTLE LINE - VARCOS',4.0,50],['LIPSTICK LINE - SEMISOLID',2.0,50],['BOTTLE LINE - SEMISOLID',3.0,50],
    ['JAR & FOUNDATION - SEMISOLID',4.0,50]
  ]
  var dns_ip = 'http://10.3.5.102/flowreport/web/'
  // var dns_ip = 'http://10.3.5.102/flowreport_dev/web/'
  // var dns_ip = ''
  var line_kemas = '<?= $jsonObj->line_kemas ?>';
  var syarat_kemas = '-'
  for (var i = 0; i < syaratZona.length; i++) {
    if (syaratZona[i][1] == line_kemas) {
      syarat_kemas = syaratZona[i][0]
      break;
    }
  }
  for (var i = 0; i < syaratList.length; i++) {
    if (syaratList[i][0] == syarat_kemas) {
      /*if (syaratList[i][0] == 'LIQUID C') {
        threshold = minBerat;
        maxValue = 50;
      }else{
        threshold = syaratList[i][1];
        maxValue = syaratList[i][2];
      }*/
      threshold = syaratList[i][1];
      maxValue = syaratList[i][2];
      break;
    }
  }

    function massa_change(){
      // this.value = this.value.replace(/,/g, '.')
      var timbangan = $('input#timbangan').val();
      var decimal_digit = <?= $data_timbangan['angka_belakang_koma'] ?>;

      $.post("index.php?r=log-formula-breakdown/get-decimal-digit&timbangan="+timbangan, function (data){
        // var decimal_digit = parseInt(data);
        var nilai = document.getElementById("massa").value.replace(/,/g, '.');
        var nilai = parseFloat(Number(nilai).toFixed(decimal_digit));
        document.getElementById("massa").value = nilai;
      });
    }

    function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if(charCode == 46){
        return true;
      }else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }

    function customAlert(type,title,message){
      swal({
        closeOnClickOutside: false,
        title: title,
        text: message,
        icon: type,
        backdrop:true,
        button: "Mengerti!",
      });
    }

    function submit(){
      var weight_bulk = document.getElementById("massa").value;
      console.log(parseFloat(weight_bulk)+sumQty);
      if (parseFloat(weight_bulk) > 0) {
        swal({
          html:true,
          title: "Konfirmasi Kembali",
          text: "Apakah anda yakin berat bulk sudah sesuai ["+weight_bulk+" Kg]?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((res) => {
          if (res) {
            var weight_teoritis = <?= $scm_planner['besar_batch'] ?>;
            if (weight_bulk <= weight_teoritis) {
              if ("<?= $scm_planner['snfg'] ?>".includes('/BS')) {
                status = 'Rejected';
                keterangan = 'Leftovers';
              }else{
                if (syaratList[i][0] == 'LIQUID C') {
                  var theValue = (((parseFloat(weight_bulk)+sumQty)*1000)/netto_refil)*(het*0.3*0.3);
                  if (theValue >= 5000000) {
                    status = 'Siap Dikemas';
                    keterangan = null;
                  }else{
                    status = 'Rejected';
                    keterangan = 'Threshold';
                  }
                }else{
                  var theValue = (parseFloat(weight_bulk)+sumQty);
                  if (theValue >= threshold) {
                    status = 'Siap Dikemas';
                    keterangan = null;
                  }else{
                    status = 'Rejected';
                    keterangan = 'Threshold';
                  }
                }
              }
              var object = {
                "nomo" :"<?= $bulk_sisa['nomo'] ?>",
                "snfg" :"<?= $scm_planner['snfg'] ?>",
                "nama_operator" :" <?= $bulk_sisa['nama_op'] ?>",
                "kode_bulk":"<?= $bulk_sisa['kode_bulk'] ?>",
                "line_kemas":"<?= $jsonObj->line_kemas; ?>",
                "no_batch":"<?= $bulk_sisa['no_batch'] ?>",
                "qty":parseFloat(weight_bulk),
                "zona":"<?= $jsonObj->zona ?>",
                "syaratZona":syarat_kemas,
                "status":status,
                "threshold":threshold,
                "keterangan":keterangan,
              }
              // var stringAlert = "Qty. BS: "+(parseFloat(weight_bulk)+sumQty)+"\nNetto: "+netto_refil+"\nHET: "+het+"\nHarga Bulk: "+(het*0.3*0.4)+"\nTotal: "+theValue
              // alert(stringAlert)
              // window.location = "index.php?r=bulk-sisa/update-weight&obj="+btoa(JSON.stringify(object));
              $.get('index.php?r=bulk-sisa/update-weight',{ obj:btoa(JSON.stringify(object))},function(data){
                var bd = $('<div class=".swal-overlay"></div>');
                bd.appendTo(document.body);
                if (data == 1) {
                  bd.remove();
                  swal({
                    title: "Berhasil!",
                    text: "Data berhasil disimpan!",
                    icon: "success",
                  })
                  .then((res) => {
                    swal({
                      html:true,
                      title: "Konfirmasi",
                      text: "Apakah anda ingin menambah storage?",
                      icon: "info",
                      allowOutsideClick: false,
                      closeOnClickOutside: false,
                      dangerMode: true,
                      buttons: {
                        cancel: {
                          text: "Tidak",
                          value: false,
                          visible: true,
                          closeModal: true
                        },
                        confirm: {
                          text: "Tambah Storage",
                          value: true,
                          visible: true,
                          className: "text-full-width",
                          closeModal: true
                        }
                      },
                    })
                    .then((res) => {
                      if (res) {
                        var currUrl = window.location.href;
                        var url = new URL(currUrl);
                        var obj = url.searchParams.get("obj");
                        window.location = dns_ip+"index.php?r=bulk-sisa/measure-massa&obj="+obj
                      }else{
                        window.location = "index.php?r=bulk-sisa/detail-bulk-info&obj="+btoa(JSON.stringify(object));
                      }
                    })
                  })
                }else{
                  bd.remove();
                  console.log(data);
                }
              })
            }else{
              customAlert('warning','Berat Bulk Tidak Tepat','Pastikan berat bulk sisa tidak melebihi dari berat teoritis!')
            }
          } else {
            swal("Proses dibatalkan!", {
              icon: "error",
            });
          }
        });
      }else{
        customAlert("warning","Berat Bulk Tidak Tepat","Silahkan input berat bulk sisa terlebih dahulu!")
      }
    }

</script>

<div class="box box-widget widget-user" id="main-box" >
  <!-- Add the bg color to the header using any of the bg-* classes -->
  <div class="widget-user-header bg-green-old">
    <h3 class="widget-user-username"><b>Bulk Sisa</b></h3>
    <h5 class="widget-user-desc">Input Manual Berat Bulk Sisa</h5>
  </div>
  <div class="widget-user-image">
    <img class="img-circle project-logo" src="../web/images/proses_icons/recycle_bs.svg" alt="BulkSisaIcon">
  </div><div class="btn-back" id="btn-back">
    <span class="text-back"><i class="fa fa-long-arrow-left" style="color:#4CAF50">&ensp;</i>Back</span>
  </div>
  <div class="box-footer">

    <div class="widget-user-header " style="width:90%; text-align: center; display: block; margin-left: auto; margin-right: auto; margin-top: 2em;background-color : #E6E6E6; height:100%;">
      <h4 class="widget-user-username" style="font-size:1em;"><b><?= $scm_planner['koitem_bulk'];?></b></h4>
      <h3 class="widget-user-username" style="font-size:1em;"><b><?= $bulk_sisa['no_batch'];?></b></h3>
      <h2 class="widget-user-desc" style="margin-bottom: 3px;"><b><?= $scm_planner['nama_bulk'];?></b></h2>
      <h4 class="widget-user-desc" style="color:black; margin-bottom: 0px; margin-top:0px;">Timbangan : <b><?= $map_device->timbangan;?></b></h4>
      <?php if ($sumQty['sum'] > 0) {
        $storage = $jsonObj->countStorage+1;
      }else{
        $storage = $jsonObj->countStorage;
      } ?>
      <h4 class="widget-user-desc" style="color:black; margin-bottom: 0px; margin-top:0px;">Storage ke - <b><?= $storage;?></b></h4>
    </div>
    <br>
    <div class="col-md-6" style="text-align: center;">
      <label for="massa">Jumlah Realisasi</label>
      <input type="text" name="massa" id="massa" onchange="massa_change()" onkeypress="return isNumber(event)">
      <!-- /input-group -->
    </div>
    <div class="col-md-6" style="text-align: center;">
       <label for="sat">UoM</label>
      <?php echo "<input disabled type='text' name='sat' id='sat' value='kg'>";?>
      <!-- /input-group -->
    </div>
    <br>
    <div class="col-md-12">
      <button onclick="submit()" class="bg-green-old" style="display: block; margin-left: auto; margin-right: auto; border-radius: 10px; width:100%; font-size:1.4em;">Submit</button>
    </div>
    <!-- /.row -->
  </div>
</div>

<?php
$script = <<< JS
  $(document).ready(function() {
    document.getElementById('btn-back').onclick = function(){
      var currUrl = window.location.href;
      var url = new URL(currUrl);
      var obj = url.searchParams.get("obj");
      window.location = dns_ip+"index.php?r=bulk-sisa/measure-massa&obj="+obj
    }
  })
JS;
$this->registerJs($script);
?>
