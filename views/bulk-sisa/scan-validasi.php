<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowAnalyst;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- CSS For Video Selector -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="ZXing for JS">

  <title>ZXing TypeScript | Decoding from camera stream</title>

  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
  href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
  href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
  href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">

<style>
  .column {
    float: left;
    width: 50%;
  }

  .accordion {

    color: white;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 80%;
    transition: 0.4s;
  }

  .active, .accordion:hover {
    background-color: #ccc;
  }

  .accordion:after {
    content: '\002B';
    color: white;
    font-weight: bold;
    float: right;
    margin-left: 5px;
  }

  .active:after {
    content: "\2212";
    color: white;
  }

  .panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
  }

  .box{
    text-align: center;
  }

  .header{
    padding :1px;
    text-align: center;
  }

  .title-text{
    font-size: 28px;
    vertical-align: middle;
    font-weight: 600
  }

  .widget-user-header{
    text-align: center;
    margin-left: auto;
    margin-right: auto;
    height: 30%!important
  }

  .bg-green-old{
    background-color: #4CAF50;
    color: white;
    border: none;
  }

  video{
    height: auto!important;
    border: 2px solid #555;
    display: block;
    margin: 0 auto;
    margin-top: 2%;
    margin-bottom: 2%;
    object-fit: fill;
  }

  .sub-title-qr{
    margin-top: 2%!important;
    font-size: 2.2rem;
    font-weight: 600;
    color: #555
  }

  .sub-desc-qr{
    font-size: 1.8rem;
    color: #555;
    font-weight: 500;
  }

  .form-rejecting,.form-sampling{
    display: none;
  }

  .accordion{
    text-align: center;
  }

  input:disabled{
    font-weight: 600;
    background-color: #EEE
  }

  .swal-button-container{
    text-align: center!important;
    display: inline-block;
  }

  .swal-button--confirm{
    background-color: #4CAF50
  }

  .swal-button:hover{
    background-color: #aaa!important
  }


  .swal-button{
    vertical-align: middle!important;
    padding: 0px!important;
    width: 10rem!important;
    text-transform: capitalize;
  }

  .swal-footer{
    text-align: center;
  }

  .bootstrap-tagsinput{
    min-height: 3.8rem;
    text-align: left;
  }

  .badge-info{
    margin-right: 1%!important;
    text-transform: uppercase!important;
    background-color: #4CAF50
  }

  .bootstrap-tagsinput input{
    position: absolute!important;
    padding: 0px!important;
    margin-top: -0.5%!important;
    margin-left: 1%!important;
    -moz-box-shadow: none!important;
    border: none!important;
    outline: none!important;
    -webkit-box-shadow: none!important;
  }

  .btn-back{
    float: left;
    margin: 1%;
  }

  .btn-back:hover{
    cursor: pointer;
  }

  .text-back{
    color: #888;
    font-size: 2rem;
    font-weight: 600;
  }

  .manual-layout span{
    font-size: 2rem
  }

  table tr td{
    border: none!important;
    text-align: center;
  }

  .content-title{
    font-weight: 600;
    font-size: 1em;
  }

  input{
    text-align: center;
  }

</style>

<script type="text/javascript" src="zxing.js"></script>
<!-- Code -->
<script type="text/javascript">

  var jsonObj = {};
  var snfg = '';
  // QR Code Scanner
  let selectedDeviceId;
  const codeReader = new ZXing.BrowserMultiFormatReader()
  console.log('ZXing code reader initialized')
  codeReader.getVideoInputDevices()
  .then((videoInputDevices) => {
    const sourceSelect = document.getElementById('sourceSelect')
    // selectedDeviceId = videoInputDevices[1].deviceId
    if (videoInputDevices.length >= 1) {
      videoInputDevices.forEach((element) => {
        const sourceOption = document.createElement('option')
        sourceOption.text = element.label
        sourceOption.value = element.deviceId
        sourceSelect.appendChild(sourceOption)
      })
      const sourceSelectPanel = document.getElementById('sourceSelectPanel')
      sourceSelectPanel.style.display = 'block'
    }

    // Initialize Execute Canvas when Page first loaded
    codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
      if (result) {
        var nomo = result.text
        check(nomo)
      }
    })
  })
  .catch((err) => {
    console.error(err)
  })

  // Function untuk cek nomo ke db
  function check(nomo){
    if ((nomo != "")){
      if (nomo[0] == 'M' && nomo[1] == 'O' && nomo[2] == '-') {
        var url = new URL(window.location.href);
        var encrypt = url.searchParams.get("obj");
        jsonObj = JSON.parse(atob(encrypt));
        if (nomo == jsonObj.nomo) {
          codeReader.reset();
          if (jsonObj.jenis == 'sampling') {
            document.getElementsByClassName('form-sampling')[0].style.display = 'block'
            document.getElementsByClassName('field-result')[0].style.display = 'block'
            document.getElementsByClassName('scan-layout')[0].style.display = 'none'
            document.getElementById('title').innerHTML = 'BULK SUDAH SESUAI';
            document.getElementById('nomo_bulk_sisa').innerHTML = jsonObj.nomo+'<br>'
            document.getElementById('nama_bulk').innerHTML = jsonObj.nama_bulk
            document.getElementById('nama_fg').value = '<?= $jadwal['nama_fg'] ?>';
            document.getElementById('snfg').value = '<?= $jadwal['snfg'] ?>';
            document.getElementById('kode_sl').value = '<?= $jadwal['streamline'] ? $jadwal['streamline']  : '-' ?>';
            document.getElementById('no_batch').value = '<?= $jadwal['nobatch'] ?>';
            document.getElementById('qty').value = <?= $jadwal['besar_batch']?>+" Kg"
          }else{
            snfg = jsonObj.snfg_awal;
            document.getElementsByClassName('form-rejecting')[0].style.display = 'block'
            document.getElementsByClassName('field-result')[0].style.display = 'block'
            document.getElementsByClassName('scan-layout')[0].style.display = 'none'
            document.getElementById('title').innerHTML = 'BULK SUDAH SESUAI<br>';
            document.getElementById('nomo_bulk_sisa').innerHTML = jsonObj.nomo+'<br>'
            document.getElementById('form-nama_bulk').value = jsonObj.nama_bulk
            document.getElementById('form-kode_bulk').value = jsonObj.koitem_bulk;
            document.getElementById('form-kode_sl').value = jsonObj.kode_sl;
            document.getElementById('form-no_batch').value = jsonObj.no_batch_awal+"X";
            document.getElementById('form-qty').value = jsonObj.berat+" Kg";
          }
        }else{
          customAlert('warning','SCAN GAGAL!','Pastikan anda scan NOMO Bulk yang sesuai')
        }
      }else{
        document.getElementsByClassName('field-result')[0].style.display = "none";
        customAlert('warning',"SCAN GAGAL!","Pastikan anda scan NOMO pada Wadah Bulk Sisa")
      }
    } else {
      customAlert("warning","Form Belum Lengkap","Silahkan lengkapi NOMO Bulk Sisa terlebih dahulu")
      document.getElementsByClassName('field-result')[0].style.display = "none"
    }
  }

  // Function custom alert
  function customAlert(type,title,message){
    swal({
      closeOnClickOutside: false,
      title: title,
      text: message,
      icon: type,
      backdrop:true,
      button: "Mengerti!",
    });
  }

  function next(){
    swal({
      title: "Peringatan",
      text: "Apakah anda ingin memberikan status langsung?",
      icon: "warning",
      allowOutsideClick: false,
      closeOnClickOutside: false,
      buttons: {
        cancel: {
          text: "Tidak",
          value: false,
          visible: true,
          closeModal: true
        },
        confirm: {
          text: "Ya",
          value: true,
          visible: true,
          className: "text-full-width",
          closeModal: true
        }
      },
    })
    .then((res) => {
      if (res) {
        var url = new URL(window.location.href);
        var obj = url.searchParams.get("obj");
        window.location = 'index.php?r=qc-bulk-entry-komponen/create-rincian&nomo='+jsonObj.nomo+"&obj="+obj+"&jenis=sisa"
      }else{
        window.location = 'index.php?r=bulk-sisa'
      }
    })
  }

  function nextDestroy(){
    swal({
      title: "Peringatan",
      text: "Apakah anda yakin ingin memusnahkan bulk ini?",
      icon: "warning",
      allowOutsideClick: false,
      closeOnClickOutside: false,
      buttons: {
        cancel: {
          text: "Tidak",
          value: false,
          visible: true,
          closeModal: true
        },
        confirm: {
          text: "Ya",
          value: true,
          visible: true,
          className: "text-full-width",
          closeModal: true
        }
      },
    })
    .then((res) => {
      if (res) {
        $.get('index.php?r=bulk-sisa/update-status-bulk&snfg='+snfg,function(data){
          if (data == 1) {
            swal({
              title: "Berhasil!",
              text: "Bulk sisa berhasil dimusnahkan",
              icon: "success",
              allowOutsideClick: false,
              closeOnClickOutside: false,
            }).then((res) => {
              window.close();
            })
          }else{
            customAlert('error','Gagal!','Terjadi kesalahan pada sistem!')
          }
        })
      }
    })
  }

  function print(){
    var currUrl = window.location.href;
    var url = new URL(currUrl);
    var obj = url.searchParams.get("obj");
    window.open("index.php?r=bulk-sisa/print-label-sample&obj="+obj)
  }

</script>

</head>

<body oncopy="return false" oncut="return false" onpaste="return false">
  <!-- Body -->
  <div class="box box-widget widget-user">
    <div class="widget-user-header bg-green-old">
      <span class="title-text" id="title">Scan Validasi Bulk Sisa<br></span>
      <span class="content-title" id="nomo_bulk_sisa"></span>
      <span class="content-title" id="nama_bulk"></span>
    </div>
    <div class="box-footer" style="margin-top: 0px; padding-top:0px;">
      <div class="scan-layout">
        <!-- Syntax Scanner -->
        <div class="sub-title-qr">
          <br>
          <span id="title-page">Arahkan kamera ke QR Code yang terletak pada Wadah Bulk Sisa</span>
          <br>
          <span class="sub-desc-qr">Scanning akan dimulai secara otomatis</span>
        </div>
        <div id="sourceSelectPanel" style="display:none">
          <label for="sourceSelect" style="display:none">Switch Camera</label>
          <select id="sourceSelect" style="max-width:400px; display:none;">x
          </select>
        </div>
        <div class="video-layout">
          <video id="video" width="100" height="100"></video>
        </div>
        <!-- Hingga line di Atas -->
        <div class="manual-layout">
          <center><h2><span>Jika kamera tidak bisa, input secara manual</span></h2></center>
          <button class="accordion bg-blue-gradient" style="padding:0px;">Input Manual (Klik Jika kamera tidak bisa)</button>
        </div>
        <div class="panel">
          <div class="col-md-12 column" style="margin-left:0px; margin-top:0px;width:100%">
            <label for="kode_manual">NOMO Bulk Sisa</label>
            <input type="text" name="nomo" id="nomo" value="">
            <button style="width:40%" onclick="check(document.getElementById('nomo').value)" >Next</button>
          </div>
        </div>
      </div>
      <div class="" style="margin-top:2rem;">

        <!-- Syntax Field Hasil Scan -->
        <div class="form-sampling">
          <div class="field-result">
            <div class="col-md-12">
              <div class="">
                <label for="nama_bulk">Nama FG</label>
                <input type="text" autocomplete="off" name="nama_bulk" id="nama_fg" disabled>
              </div>
              <div class="">
                <label for="nama_bulk">SNFG</label>
                <input type="text" autocomplete="off" name="nama_fg" id="snfg" disabled>
              </div>
              <div class="">
                <label for="no_batch">Kode Streamline</label>
                <input type="text" autocomplete="off" name="nomo_bulk_sisa" id="kode_sl" disabled>
              </div>
              <div class="">
                <label for="kode_bulk">No Batch</label>
                <input type="text" autocomplete="off" name="kode_bulk" id="no_batch" disabled>
              </div>
              <div class="">
                <label for="kode_bulk">Berat Bulk Sisa</label>
                <input type="text" autocomplete="off" name="kode_bulk" id="qty" disabled>
              </div>
            </div>
            <div class="col-md-12" style="display:flex;flex: auto;justify-content:center;width:100%">
              <button style="width:50%;margin-right:4px" onclick="print()" class="" id="btnStart">Print</button>
              <button style="width:50%;margin-left:4px" onclick="next()" class="bg-green-old" id="btnStart">Next</button>
            </div>
          </div>
        </div>
        <div class="form-rejecting" hidden>
          <div class="field-result">
            <div class="col-md-12">
              <div class="">
                <label for="nama_bulk">Nama Bulk</label>
                <input type="text" autocomplete="off" name="nama_bulk" id="form-nama_bulk" disabled>
              </div>
              <div class="">
                <label for="nama_bulk">Kode Bulk</label>
                <input type="text" autocomplete="off" name="nama_fg" id="form-kode_bulk" disabled>
              </div>
              <div class="">
                <label for="no_batch">Kode Streamline</label>
                <input type="text" autocomplete="off" name="nomo_bulk_sisa" id="form-kode_sl" disabled>
              </div>
              <div class="">
                <label for="kode_bulk">No Batch</label>
                <input type="text" autocomplete="off" name="kode_bulk" id="form-no_batch" disabled>
              </div>
              <div class="">
                <label for="kode_bulk">Berat Bulk Sisa</label>
                <input type="text" autocomplete="off" name="kode_bulk" id="form-qty" disabled>
              </div>
            </div>
            <div class="col-md-12" style="margin-top:2rem;text-align:center;font-size:1em">
              <button style="width:50%;" onclick="nextDestroy()" class="bg-green-old" id="form-btnStart">Next</button>
            </div>
          </div>
        </div>
        <!-- Hingga Line di Atas -->
      </div>
    </div>
  </div>
  </body>

  <!-- Javascript -->
  <!-- Calling ZXing API CDN -->

<?php
$script = <<< JS
  $(document).ready(function() {
    var OS = platform.os.family;
    if (OS == "Android") {
      document.getElementById("video").style.width = '60%'
    }else{
      document.getElementById("video").style.width = '30%'
    }

    // /Syntax untuk handle button input manual
    var acc = document.getElementsByClassName("accordion");
    var i;
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = document.getElementsByClassName('panel')[0];
        if (panel.style.maxHeight) {
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        }
      });
    }
    // Hingga Line di Atas
  })
JS;
$this->registerJs($script);
?>
