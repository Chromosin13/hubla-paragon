<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use yii\helpers\Url;
use wbraganca\tagsinput\TagsinputWidget;
use app\controllers\BulkSisaController;

?>

<style media="screen">
  .list-other-action > *:hover{
    cursor: pointer;
  }

  .modal-dialog-centered{
    top:30%!important;
  }

  .flex-item{
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .modal-dialog-centered{
    top:50%;
    margin: 0px auto;
  }

  .modal-content{
    border-radius: 10px
  }

  #exampleModalCenter .modal-body{
    width: auto;
    display: grid;
    grid-gap: 8px;
    grid-template-columns: repeat(4,minmax(0,1fr))!important
  }

  #exampleModal form{
    width: auto;
    display: grid;
    grid-gap: 8px;
    grid-template-columns: repeat(4,minmax(0,1fr))!important
  }

  .grid-item{
    display: inline-grid;
    color:white;
  }

  #addStorage:hover{
    cursor: pointer;
  }

  /* SPINNER LOADING */
  .sk-circle {
    margin: 100px auto;
    width: 100px;
    height: 100px;
    position: relative;
  }
  .sk-circle .sk-child {
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
  }
  .sk-circle .sk-child:before {
    content: '';
    display: block;
    margin: 0 auto;
    width: 15%;
    height: 15%;
    background-color: #333;
    border-radius: 100%;
    -webkit-animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
            animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
  }
  .sk-circle .sk-circle2 {
    -webkit-transform: rotate(30deg);
        -ms-transform: rotate(30deg);
            transform: rotate(30deg); }
  .sk-circle .sk-circle3 {
    -webkit-transform: rotate(60deg);
        -ms-transform: rotate(60deg);
            transform: rotate(60deg); }
  .sk-circle .sk-circle4 {
    -webkit-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
            transform: rotate(90deg); }
  .sk-circle .sk-circle5 {
    -webkit-transform: rotate(120deg);
        -ms-transform: rotate(120deg);
            transform: rotate(120deg); }
  .sk-circle .sk-circle6 {
    -webkit-transform: rotate(150deg);
        -ms-transform: rotate(150deg);
            transform: rotate(150deg); }
  .sk-circle .sk-circle7 {
    -webkit-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
            transform: rotate(180deg); }
  .sk-circle .sk-circle8 {
    -webkit-transform: rotate(210deg);
        -ms-transform: rotate(210deg);
            transform: rotate(210deg); }
  .sk-circle .sk-circle9 {
    -webkit-transform: rotate(240deg);
        -ms-transform: rotate(240deg);
            transform: rotate(240deg); }
  .sk-circle .sk-circle10 {
    -webkit-transform: rotate(270deg);
        -ms-transform: rotate(270deg);
            transform: rotate(270deg); }
  .sk-circle .sk-circle11 {
    -webkit-transform: rotate(300deg);
        -ms-transform: rotate(300deg);
            transform: rotate(300deg); }
  .sk-circle .sk-circle12 {
    -webkit-transform: rotate(330deg);
        -ms-transform: rotate(330deg);
            transform: rotate(330deg); }
  .sk-circle .sk-circle2:before {
    -webkit-animation-delay: -1.1s;
            animation-delay: -1.1s; }
  .sk-circle .sk-circle3:before {
    -webkit-animation-delay: -1s;
            animation-delay: -1s; }
  .sk-circle .sk-circle4:before {
    -webkit-animation-delay: -0.9s;
            animation-delay: -0.9s; }
  .sk-circle .sk-circle5:before {
    -webkit-animation-delay: -0.8s;
            animation-delay: -0.8s; }
  .sk-circle .sk-circle6:before {
    -webkit-animation-delay: -0.7s;
            animation-delay: -0.7s; }
  .sk-circle .sk-circle7:before {
    -webkit-animation-delay: -0.6s;
            animation-delay: -0.6s; }
  .sk-circle .sk-circle8:before {
    -webkit-animation-delay: -0.5s;
            animation-delay: -0.5s; }
  .sk-circle .sk-circle9:before {
    -webkit-animation-delay: -0.4s;
            animation-delay: -0.4s; }
  .sk-circle .sk-circle10:before {
    -webkit-animation-delay: -0.3s;
            animation-delay: -0.3s; }
  .sk-circle .sk-circle11:before {
    -webkit-animation-delay: -0.2s;
            animation-delay: -0.2s; }
  .sk-circle .sk-circle12:before {
    -webkit-animation-delay: -0.1s;
            animation-delay: -0.1s; }

  @-webkit-keyframes sk-circleBounceDelay {
    0%, 80%, 100% {
      -webkit-transform: scale(0);
              transform: scale(0);
    } 40% {
      -webkit-transform: scale(1);
              transform: scale(1);
    }
  }

  @keyframes sk-circleBounceDelay {
    0%, 80%, 100% {
      -webkit-transform: scale(0);
              transform: scale(0);
    } 40% {
      -webkit-transform: scale(1);
              transform: scale(1);
    }
  }

    ::-webkit-input-placeholder {
      font-style: italic;
    }
    :-moz-placeholder {
      font-style: italic;
    }
    ::-moz-placeholder {
      font-style: italic;
    }
    :-ms-input-placeholder {
      font-style: italic;
    }
    .bg-green-old{
      background-color: #4CAF50;
      color: white
    }

    .img-circle .project-logo{
      background-color: #B1CCA3;
      width: 5em!important;
      height: auto!important;
    }

    .box-content{
      background-color: white;
      border-radius: 24px;
      padding: 24px;
      margin-top: 64px!important;
      min-height: 100%;
    }

    hr.rounded{
    border-top: 2px solid #DDDFE2;
    border-radius: 5px;
    }

    .content-title{
      font-size: 1em;
    }

    .badge-content{
      padding:8% 4%!important;
      border-radius:12px;
      font-size:2em;
      display: inline-block;
      margin-top: 0.2em;
      min-width: 12em;
      text-align: center;
    }

    .content-layout{
      padding: 0%;
      min-height: 48vh!important;
      margin-top: 4%!important
    }

    .row-content{
      margin: 0%;
      margin-top: 8%;
    }

    .row-content span{
      display: inline-block;
      padding: 1%
    }

    .content-sub-title{
      font-size: 1em;
      color: #777;
      vertical-align: middle!important;
    }

    .content-sub-data{
      font-size: 1.4em;
      color: #4CAF50;
      vertical-align: middle!important;
    }

    .btn-back{
      float: left;
      margin: 0%;
    }

    .btn-back:hover{
      cursor: pointer;
    }

    .text-back{
      color: #888;
      font-size: 2rem;
      font-weight: 600;
    }

    #label:hover{
      cursor: pointer;
    }

    .widget-user-username{
      font-size: 1.4em!important;
      padding: 0%!important;
      display: block;
    }
    .widget-user-desc{
      font-size: 1em!important;
      padding: 0%!important;
    }

    .bootstrap-tagsinput{
      min-height: 3.8rem;
      text-align: left;
      background-color: transparent!important;
      border:none!important;
      -moz-box-shadow: none!important;
      outline: none!important;
      margin-left: 0em!important;
      padding:0em!important;
      -webkit-box-shadow: none!important;
    }

    .badge-info span{
      display: none!important
    }

    .badge-info{
      margin-right: 1%!important;
      text-transform: uppercase!important;
      background-color: #4CAF50;
      padding: 1em!important;
      font-size: 1.6em!important;
      margin-left: 0em!important;
    }

    .bootstrap-tagsinput input{
      position: absolute!important;
      padding: 0px!important;
      margin-top: -0.5%!important;
      margin-left: 1%!important;
      -moz-box-shadow: none!important;
      outline: none!important;
      -webkit-box-shadow: none!important;
      background-color: transparent!important;
      border:none!important;
      display: none!important
    }

    table tr td{
      border:0em!important;
    }

    .badge-column{
      display: inline-block;
      padding: 0%!important;
      margin-right: 2em!important
    }

    .badge-column .btn{
      padding: 0%
    }

    .table-responsive{
      margin-top: 1em;
    }

    .filter-text{
      vertical-align: middle;
      color: #DDDFE2;
      font-weight: 600
    }

    .filter:hover .filter-text{
      color: #4CAF50;
    }

    .filter:hover{
      background-color: white;
      border: 2px solid #4CAF50;
      color: #4CAF50;
    }

    .filter{
      text-align: center;
      border: 2px solid #DDDFE2;
      border-radius: 10px;
      margin-right: 0.6rem;
      padding:1em 1em;
      width: 14em
    }

    .filter-selected{
      background: #4CAF50;
      border: 2px solid #4CAF50;
    }

    .filter-selected .filter-text{
      color: white;
    }

    .swal-button-container{
      text-align: center!important;
      display: inline-block;
    }

    .swal-footer{
      text-align: center;
    }

    .swal-text{
      text-align: center;
    }

    .swal-button--confirm{
      background-color: #4CAF50
    }

    .swal-button:hover{
      background-color: #aaa!important
    }


    .swal-button{
      vertical-align: middle!important;
      padding: 1em 1em!important;
      text-transform: capitalize;
      width: 10em;
    }

    #btnSave:hover{
      cursor: pointer;
    }

    .js-example-basic-single{
      width: 50%;
    }

    .select2-container--default .select2-selection--single, .select2-selection .select2-selection--single {
      border-radius: 8px!important;
      border:2px solid #4CAF50;
    }

    .select2-selection__rendered{
      color: #4CAF50!important;
      font-weight: 600;
    }

    .select2 .select2-container .select2-container--default{
      padding: 12em
    }

    .select2-selection__arrow{
      color: #4CAF50!important;
      display: inline-block;
    }

    .defaultValue{
      color: #777;
      font-weight: 500;
    }

    .btn-print-sweetalert{
      text-transform: uppercase;
      width: 100%
    }

    .jadwal:hover{
      color:white!important;
      background-color: #4CAF50!important;
      border: 1px solid #4CAF50;
    }

    .jadwal{
      color:#4CAF50!important;
      background-color: transparent!important;
      border: 1px solid #4CAF50;
      text-transform: uppercase;
      font-weight: 600;
      padding:1em 1em;
      border-radius: 10px;
    }

    .text-full-width{
      width: 100%
    }

    #other:hover{
      cursor: pointer;
    }
</style>

<script type="text/javascript">
  var dns = "http://10.3.5.102/flowreport/web/";
  var pic = "<?= $bulk_sisa['pic']?>";
  var ages = "<?= $bulk_sisa['ages'];?>";
  var numAge = parseInt(ages.replace(/[^0-9]/g,''));
  var flagAging =  <?= $bulk_sisa['flagAging']; ?>;
  var keterangan = "<?= $bulk_sisa['keterangan'] ?>";
  var threshold = 0;
  var countQCStatus = <?php
    if (!empty($qcStatus)) {
      echo count($qcStatus);
    }else{
      echo 0;
    }
  ?>;
  var weight_bulk = <?= $bulk_sisa['qty']; ?>;
  var line_kemas = "<?= $flow_input_snfgkomponen['nama_line'] ?? $flow_input_snfg['nama_line'] ?>";
  var syaratZona =
  [
    ['LIQUID A','TUP01'],['LIQUID A','TUP02'],['LIQUID A','TUP03'],['LIQUID A','TUP04'],
    ['LIQUID A','TUP05'],['LIQUID A','TUP06'],['LIQUID A','TUP07'],['LIQUID A','TUP08'],
    ['LIQUID A','TUP09'],['LIQUID B','JAP01'],['LIQUID B','BLP01'],['LIQUID B','BLP02'],
    ['LIQUID B','BOP01'],['LIQUID B','BOP02'],['LIQUID B','BOP03'],['LIQUID B','BOP04'],
    ['LIQUID B','BOP05'],['LIQUID B','BOP06'],['LIQUID B','EMP02'],['LIQUID B','EMP05'],
    ['LIQUID B','LQP04'],['LIQUID C','JAP03'],['LIQUID C','BOP08'],['LIQUID C','BOP09'],
    ['LIQUID C','BOP10'],['LIQUID C','CUP01'],['LIQUID C','EMP01'],['LIQUID C','EMP03'],
    ['LIQUID C','EMP07'],['LIQUID C','EMP15'],['LIQUID C','EMP16'],['LIQUID C','EMP17'],
    ['LIQUID C','OSP01'],

    ['POWDER A','LPP01'],['POWDER A','LPP02'],['POWDER A','LPP03'],['POWDER A','PNP01'],
    ['POWDER A','PNP03'],['POWDER A','PNP04'],['POWDER A','PNP01MANUAL'],['POWDER A','PNP04MANUAL'],
    ['POWDER B','PCP01'],['POWDER B','PCP02'],['POWDER B','PCP03'],['POWDER B','PCP04'],
    ['POWDER B','PCP05'],
    ['POWDER B','PEP01'],['POWDER B','PEP02'],['POWDER B','PEP03'],['POWDER B','PEP04'],
    ['POWDER B','PEP05'],['POWDER B','PEP06'],

    ['BOTTLE LINE - VARCOS','VSN01'],['BOTTLE LINE - VARCOS','VSN02'],['BOTTLE LINE - VARCOS','VSN03'],['BOTTLE LINE - VARCOS','VMN01'],['BOTTLE LINE - VARCOS','VMN02'],

    ['BOTTLE LINE - SEMISOLID','BTP02'],['BOTTLE LINE - SEMISOLID','BTP04'],['BOTTLE LINE - SEMISOLID','LIP11'],
    ['BOTTLE LINE - SEMISOLID','LIP18'],['BOTTLE LINE - SEMISOLID','LRP01'],

    ['JAR & FOUNDATION - SEMISOLID','FOP03'],['JAR & FOUNDATION - SEMISOLID','JRP02'],['JAR & FOUNDATION - SEMISOLID','SBP01'],
    ['LIPSTICK LINE - SEMISOLID','SAP01'],['LIPSTICK LINE - SEMISOLID','ALP03'],['LIPSTICK LINE - SEMISOLID','LIP06'],
    ['LIPSTICK LINE - SEMISOLID','LIP08'],['LIPSTICK LINE - SEMISOLID','LIP10'],['LIPSTICK LINE - SEMISOLID','LIP12'],
    ['LIPSTICK LINE - SEMISOLID','LIP14'],['LIPSTICK LINE - SEMISOLID','LIP16']
  ]
  var syaratList =
  [
    ['LIQUID A',30.0,100],['LIQUID B',30.0,100],['LIQUID C',10.0,100],
    ['POWDER A',4.0,50],['POWDER B',2.0,50],
    ['BOTTLE LINE - VARCOS',4.0,50],['LIPSTICK LINE - SEMISOLID',2.0,50],['BOTTLE LINE - SEMISOLID',3.0,50],
    ['JAR & FOUNDATION - SEMISOLID',4.0,50]
  ]
  var operator = '<?= $bulk_sisa['nama_op']; ?>';
  var konfMPS = <?php if ($bulk_sisa['konf_mps'] == null ){echo 0;}else{echo $bulk_sisa['konf_mps'];} ?>;
  var konfPacks = <?php if ($bulk_sisa['konf_packaging'] == null ){echo 0;}else{echo $bulk_sisa['konf_packaging'];} ?>;
  var currKonfMPS = -1;
  var currKonfPack = -1;
  var currDemands = '-';
  var currNameDemands = '-';
  var demands = '<?php if ($bulk_sisa['snfg_baru'] == null ){echo "-";}else{echo explode("/BS",$bulk_sisa['snfg_baru'])[0];} ?>';
  var username = '<?=Yii::$app->user->identity->username; ?>';
  var netto_claim = <?= $master_data_product['netto_klaim'] ?? 0 ?>;
  var qty_karbox = <?= $master_data_koli['qty'] ?? 0 ?>;
  var totalQty = <?= $sumQty['sum'] ?>;
  var status_bulk = '<?= $list_bulk_sisa[count($list_bulk_sisa)-1]['status']; ?>';
  var netto_refil = <?= $netto_refil['child_1_qty']; ?>;
  var fg_pcs = Math.floor((totalQty-0.01*totalQty)*1000/(netto_refil*qty_karbox))*qty_karbox;
  var fg_pack = Math.ceil(1.05*fg_pcs);
  var het = <?= round($het['fixed_price']) ?>; //Set default Rp75.000
  var hargaBulk = (het*0.3*0.3) * netto_refil;
  var theValue = (weight_bulk*1000)/hargaBulk;
  var maxValue = 0;
  var no_batch = '<?= $bulk_sisa['no_batch'] ?>';
  var is_printed = <?= $bulk_sisa['is_printed'] ?? 0; ?>;
  var nomo = '<?= $bulk_sisa['nomo'] ?>';
  var snfg = '<?= $bulk_sisa['snfg'] ?>';
  var snfg_new = '<?= $bulk_sisa['snfg_baru'] ?>';
  var isPrint = '<?= $isPrint ?>';
  var totalStorage = '<?= count($list_bulk_sisa) ?>';

  function customAlert(type,title,message){
    swal({
      closeOnClickOutside: false,
      title: title,
      text: message,
      icon: type,
      backdrop:true,
      button: "Mengerti!",
    });
  }

  function updateJadwal(){
    swal({
      title: "Konfirmasi Kembali",
      text: "Apakah anda yakin ingin mengubah data pada bulk ini?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
      closeOnClickOutside:false,
      allowOutsideClick:false
    })
    .then((res) => {
      if (res) {
        var body = document.getElementsByTagName("BODY")[0];
        body.style.opacity = "0.5";
        document.getElementById("spinner").style.visibility = "visible";
        body.disabled = true;
        var object = {
          'snfg':snfg_new,
          'startDate':$('#start-date').val(),
          'dueDate':$('#due-date').val(),
          'week':$('#week').val(),
          'olahPremix':$('#olah-premix').val(),
          'olah1' : $('#olah-1').val(),
          'olah2':$('#olah-2').val(),
          'adjustOlah1' : $('#adjust-olah-1').val(),
          'adjustOlah2' : $('#adjust-olah-2').val(),
          'kemas1' : $('#kemas-1').val(),
          'kemas2':$('#kemas-2').val()
        }
        $.get('index.php?r=bulk-sisa/update-jadwal',{ obj:btoa(JSON.stringify(object))},function(data){
          if (data == 1) {
            body.style.opacity = "1";
            document.getElementById("spinner").style.visibility = "hidden";
            body.disabled = false;
            swal({
              title: "Berhasil!",
              text: "Update data bulk berhasil disimpan!",
              icon: "success",
            })
            .then((res) => {
              location.reload();
            })
          }else{
            window.alert("Update gagal!")
          }
        })
      }
    });
  }

</script>

<div id="spinner" style="visibility: hidden;position: fixed;height: 10em;width: 10em;left: 50%;z-index: 1000;top: 50%;margin-top: -5em;margin-left: -5em;">
  <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
  </div>
</div>
<div class="box box-widget widget-user">
  <!-- Add the bg color to the header using any of the bg-* classes -->
  <div class="widget-user-header bg-green-old">
    <table style="width:100%">
      <tr>
        <td>
          <span class="widget-user-username"><b>Bulk Sisa</b></span>
          <span class="widget-user-desc">Detail information of Bulk Sisa</span>
        </td>
        <td style="text-align:right">
          <span class="widget-user-username"><b><?= $bulk_sisa['nomo']; ?></b></span>
          <span class="widget-user-username"><b><?= $planner['koitem_fg']; ?></b></span>
        </td>
      </tr>
    </table>
  </div>
  <div class="widget-user-image">
    <img class="img-circle project-logo" src="../web/images/proses_icons/recycle_bs.svg" alt="BulkSisaIcon">
  </div>
</div>
<div class="box-content">
  <table style="width:100%;border:none!important">
    <tbody style="">
      <tr style="">
        <td style="">
          <div class="btn-back" id="btn-back">
            <span class="text-back"><i class="fa fa-long-arrow-left" style="color:#4CAF50">&ensp;</i>Back</span>
          </div>
        </td>
        <td style="text-align:right;font-size:2rem;color:#4CAF50;display:none" id="btnSave"><b>Save</b></td>
      </tr>
    </tbody>
  </table>
  <hr class="rounded">
  <?php if (Yii::$app->user->identity->username == 'kemas') { ?>
    <div class="table-responsive">
      <table class="table">
        <tr>
          <td class="content-sub-title">Nama Item</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data"><?= $planner['nama_fg'] ?></td>
          <td rowspan="10">
            <div class="" style="margin-top:1em">
              <span>Status</span>
            </div>
            <div class="">
              <span class="badge-content" id="status"></span>
            </div>
            <div class="" style="margin-top:1em">
              <span>Usia Bulk</span>
            </div>
            <div class="">
              <span class="badge-content" id="age"></span>
            </div>
            <div class="" style="margin-top:1em">
              <span>PIC</span>
            </div>
            <div class="">
              <span class="badge-content" id="pic"></span>
            </div>
            <div class="" style="margin-top:1em">
              <span>Other Action</span>
            </div>
            <div class="">
              <span class="badge-content" style="background-color:#F0DCBE;color:#D2B493" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#exampleModalCenter" id="label"><b>Print Label</b></span>
            </div>
            <?php if (in_array($bulk_sisa['pic'],['in SPR','in WPM']) && $bulk_sisa['snfg_baru'] == null) { ?>
              <div class="" style="margin-top:0.4em">
                <span class="badge-content" id="addStorage"></span>
              </div>
              <div class="" style="margin-top:0.4em">
                <span class="badge-content" id="other"></span>
              </div>
          <?php } ?>
          </td>
        </tr>
        <tr>
            <td class="content-sub-title">Nama Bulk</td>
            <td class="content-sub-title" style="width:1em">:</td>
            <td class="content-sub-data"><?= $planner['nama_bulk'] ?></td>
        </tr>
        <tr>
          <td class="content-sub-title">NOMO</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data"><?= $bulk_sisa['nomo'] ?></td>
        </tr>
        <tr>
            <td class="content-sub-title">SNFG</td>
            <td class="content-sub-title" style="width:1em">:</td>
            <td class="content-sub-data"><?= $bulk_sisa['snfg'] ?></td>
        </tr>
        <tr>
          <td class="content-sub-title">No Batch Awal</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data"><?= $bulk_sisa['no_batch'] ?></td>
        </tr>
        <tr>
          <td class="content-sub-title">No Batch Bulk Sisa</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data">
            <?php
            if ($bulk_sisa['no_batch_baru'] == '') {
              echo '<span style="color:#aaa"><i>Belum Ada</i></span>';
            }else{
              echo $bulk_sisa['no_batch_baru'];
            }
            ?>
          </td>
        </tr>
        <tr>
            <td class="content-sub-title">Zona Kemas</td>
            <td class="content-sub-title" style="width:1em">:</td>
            <td class="content-sub-data"><?= $bulk_sisa['zona'] ?></td>
        </tr>
        <tr>
          <td class="content-sub-title">Line Kemas</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data"><?= $flow_input_snfgkomponen['nama_line'] ?? $flow_input_snfg['nama_line'] ?></td>
        </tr>
        <tr>
          <td class="content-sub-title">Tanggal Timbang</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data">
            <?php
            setlocale(LC_TIME,'id_ID.UTF8');
            echo strftime("%d %B %Y",strtotime($bulk_sisa['tgl_timbang']));
            ?>
          </td>
        </tr>
        <tr>
          <td class="content-sub-title">Total Storage</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data"><?= count($list_bulk_sisa) ?> Storage</td>
        </tr>
        <tr>
          <td class="content-sub-title">Berat Bulk Sisa</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data"><?= $sumQty['sum'] ?> Kg</td>
        </tr>
        <tr>
            <td class="content-sub-title">Nama Operator</td>
            <td class="content-sub-title" style="width:1em;vertical-align:middle;">:</td>
            <td><input type="text" name="nama_operator" id="nama_operator" data-role="tagsinput" disabled></input></td>
        </tr>
      </table>
    </div>
  <!-- <?php //} else if(Yii::$app->user->identity->username == 'spr') {?> -->
  <?php } else {?>
    <div class="table-responsive">
      <table class="table">
        <tr>
          <td class="content-sub-title">Nama Item</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data"><?= $planner['nama_fg'] ?></td>
          <td rowspan="9">
            <div class="" style="margin-top:1em">
              <span>Status</span>
            </div>
            <div class="">
              <span class="badge-content" id="status"></span>
            </div>
            <div class="" style="margin-top:1em">
              <span>Usia Bulk</span>
            </div>
            <div class="">
              <span class="badge-content" id="age"></span>
            </div>
            <div class="" style="margin-top:1em">
              <span>PIC</span>
            </div>
            <div class="">
              <span class="badge-content" id="pic"></span>
            </div>
            <?php if (in_array($bulk_sisa['pic'],['in PPC']) && in_array(Yii::$app->user->identity->username,['spr'])) { ?>
            <div id="title-other-action" style="margin-top:1em;display:none">
              <span>Other Action</span>
            </div>
            <div class="">
              <span class="badge-content" id="other"></span>
            </div>
          <?php } ?>
          <?php if (in_array($bulk_sisa['keterangan'],['Due Date']) && in_array(Yii::$app->user->identity->username,['spr'])) { ?>
            <div class="" style="margin-top:1em">
              <span>Other Action</span>
            </div>
            <div class="">
              <span class="badge-content" style="background-color:#F0DCBE;color:#D2B493" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#exampleModalCenter" id="label"><b>Print Label</b></span>
            </div>
        <?php } ?>
        <?php if (in_array($bulk_sisa['keterangan'],['Canceled']) && in_array(Yii::$app->user->identity->username,['planner'])) { ?>
          <div class="" style="margin-top:1em">
            <span>Other Action</span>
          </div>
          <div class="">
            <span class="badge-content" style="background-color:#F0DCBE;color:#D2B493" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#exampleModalCenter" id="label"><b>Print Label</b></span>
          </div>
      <?php } ?>
          <?php if ($bulk_sisa['flagAging'] == 'true' && in_array(Yii::$app->user->identity->username,['spr']) && $bulk_sisa['status'] != 'Rejected') { ?>
          <div id="title-other-action" style="margin-top:1em;">
            <span>Other Action</span>
          </div>
          <div class="">
            <span class="badge-content" id="other"></span>
          </div>
        <?php } ?>
        <!--<?php //if (in_array($bulk_sisa['pic'],['in WPM']) && in_array($bulk_sisa['status'],['Rejected']) && in_array(Yii::$app->user->identity->username,['planner'])) { ?>
          <div id="title-other-action" style="margin-top:1em;display:none">
            <span>Other Action</span>
          </div>
          <div class="">
            <span class="badge-content" id="other"></span>
          </div>
        <?php //} ?>-->
        <?php if (in_array($bulk_sisa['pic'],['in WPM']) && in_array($bulk_sisa['status'],['Rejected']) && in_array(Yii::$app->user->identity->username,['wpm'])) { ?>
        <div id="title-other-action" style="margin-top:1em;display:none">
          <span>Other Action</span>
        </div>
        <div class="">
          <span class="badge-content" id="other"></span>
        </div>
      <?php } ?>
        <?php
        if (in_array(Yii::$app->user->identity->username,['planner']) && !in_array($bulk_sisa['status'],['Rejected'])) {
        // }
        // if(strpos($bulk_sisa['ages'], 'Bulan') !== false){
        //   $months= (int) filter_var($bulk_sisa['ages'], FILTER_SANITIZE_NUMBER_INT);
        //   if ($months > 5){ ?>
          <div id="title-other-action" style="margin-top:1em;display:none">
            <span>Other Action</span>
          </div>
          <div class="list-other-action">
            <span class="badge-content" id="other"></span>
          </div>
      <?php } //}?>
        <?php if (in_array($bulk_sisa['pic'],['in QC Bulk']) && in_array($bulk_sisa['status'],['Pending','Rework']) && in_array(Yii::$app->user->identity->username,['qcbulk'])) { ?>
        <div id="title-other-action" style="margin-top:1em;display:none">
          <span>Other Action</span>
        </div>
        <div class="list-other-action">
          <span class="badge-content" id="other"></span>
        </div>
      <?php } ?>
          </td>
        </tr>
        <?php // NOTE: NAMA BULK ?>
        <tr>
            <td class="content-sub-title">Nama Bulk</td>
            <td class="content-sub-title" style="width:1em">:</td>
            <td class="content-sub-data"><?= $planner['nama_bulk'] ?></td>
        </tr>
        <?php // NOTE: NOMO ?>
        <tr>
          <td class="content-sub-title">NOMO</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data" id="nomo"><?= $bulk_sisa['nomo'] ?></td>
        </tr>
        <?php // NOTE: SNFG AWAL ?>
        <tr>
            <td class="content-sub-title">SNFG Awal</td>
            <td class="content-sub-title" style="width:1em">:</td>
            <td class="content-sub-data"><?= $bulk_sisa['snfg'] ?></td>
        </tr>
        <?php // NOTE: SNFG BARU?>
        <tr>
          <td class="content-sub-title">SNFG Bulk Sisa</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data">
            <?php
            if ($bulk_sisa['snfg_baru'] == '') {
              echo '<span style="color:#aaa"><i>Belum Ada</i></span>';
            }else{
              echo $bulk_sisa['snfg_baru'];
            }
            ?>
          </td>
        </tr>
        <?php // NOTE: NO BATCH AWAL ?>
        <tr>
          <td class="content-sub-title">No Batch Awal</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data"><?= $bulk_sisa['no_batch'] ?></td>
        </tr>
        <?php // NOTE: NO BATCH BULK SISA ?>
        <tr>
          <td class="content-sub-title">No Batch Bulk Sisa</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data">
            <?php
            if ($bulk_sisa['no_batch_baru'] == '') {
              echo '<span style="color:#aaa"><i>Belum Ada</i></span>';
            }else{
              echo $bulk_sisa['no_batch_baru'];
            }
            ?>
          </td>
        </tr>
        <?php // NOTE: ZONA KEMAS ?>
        <tr>
            <td class="content-sub-title">Zona Kemas</td>
            <td class="content-sub-title" style="width:1em">:</td>
            <td class="content-sub-data"><?= $bulk_sisa['zona'] ?></td>
        </tr>
        <?php // NOTE: KODE STREAMLINE ?>
        <tr>
            <td class="content-sub-title">Kode Streamline</td>
            <td class="content-sub-title" style="width:1em">:</td>
            <td class="content-sub-data"><?= $planner['kode_jadwal']?></td>
        </tr>
        <?php // NOTE: LINE KEMAS ?>
        <tr>
          <td class="content-sub-title">Line Kemas</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data"><?= $flow_input_snfgkomponen['nama_line'] ?? $flow_input_snfg['nama_line']?></td>
        </tr>
        <?php // NOTE: TANGGAL TIMBANG ?>
        <tr>
          <td class="content-sub-title">Tanggal Timbang</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data">
            <?php
            setlocale(LC_TIME,'id_ID.UTF8');
            echo strftime("%d %B %Y",strtotime($bulk_sisa['tgl_timbang']));
            ?>
          </td>
        </tr>
        <?php // NOTE: WEEK DIHASILKAN ?>
        <tr>
          <td class="content-sub-title">Week Dihasilkan</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data">
            In Week <?= date("W", strtotime($bulk_sisa['tgl_timbang'])) ?>
          </td>
        </tr>
        <?php // NOTE: TOTAL STORAGE ?>
        <tr>
          <td class="content-sub-title">Total Storage</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data"><?= count($list_bulk_sisa) ?> Storage</td>
        </tr>
        <?php // NOTE: BERAT BULK SISA ?>
        <tr>
          <td class="content-sub-title">Berat Bulk Sisa</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data"><?= $sumQty['sum'] ?> Kg</td>
        </tr>
        <?php // NOTE: QTY. FINISH GOODS ?>
        <tr>
          <td class="content-sub-title">Qty. Finish Goods</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data" id="fg_pcs"></td>
        </tr>
        <?php // NOTE: QTY. SUPPLY PACKAGING ?>
        <tr>
          <td class="content-sub-title">Qty. Supply Packaging</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data" id="fg_pack"></td>
        </tr>
        <?php // NOTE: NAMA OPERATOR ?>
        <tr>
            <td class="content-sub-title">Nama Operator</td>
            <td class="content-sub-title" style="width:1em;vertical-align:middle;">:</td>
            <td><input type="text" name="nama_operator" id="nama_operator" data-role="tagsinput" disabled></input></td>
        </tr>
        <?php // NOTE: KONFIRMASI MPS ?>
        <tr id="mps_action">
          <td class="content-sub-title">Konfirmasi MPS</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data">
            <div class="" id="editorMps">
              <div class="filter btn" id="konfMPS">
                <span class="filter-text">Konfirmasi</span>
              </div>
              <div class="filter btn" id="notKonfMPS">
                <span class="filter-text">Belum Konfirmasi</span>
              </div>
            </div>
            <?php if ($bulk_sisa['konf_mps'] == 0 || $bulk_sisa['konf_mps'] == null) {
              echo "<span style='color:#aaa;display:none' id='spanMps'>Belum Dikonfirmasi</span>";
            }else{
              echo "<span style='display:none;' id='spanMps'><b>Sudah Dikonfirmasi</b></span>";
            } ?>
          </td>
        </tr>
        <?php // NOTE: KONFIRMASI PACKAGING ?>
        <tr id="packaging_action">
          <td class="content-sub-title">Konfirmasi Packaging</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data">
            <div class="" id="editorPack">
              <div class="filter btn" id="konfPack">
                <span class="filter-text">Konfirmasi</span>
              </div>
              <div class="filter btn" id="notKonfPack">
                <span class="filter-text">Belum Konfirmasi</span>
              </div>
            </div>
            <?php if ($bulk_sisa['konf_packaging'] == 0 || $bulk_sisa['konf_packaging'] == null) {
              echo "<span style='color:#aaa;display:none' id='spanPack'>Belum Dikonfirmasi</span>";
            }else{
              echo "<span style='display:none;' id='spanPack'><b>Sudah Dikonfirmasi</b></span>";
            } ?>
          </td>
        </tr>
        <?php // NOTE: DEMANDS ?>
        <tr id="demand_action">
          <td class="content-sub-title">Demands</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data">
            <div class="" id="editorDemand">
              <select class="js-example-basic-single" name="state" id="selectFg">
                <option value="-" disabled>Pilih Demands</option>
                <?php for ($i=0; $i < count($listFg); $i++) {
                  echo '<option value="'.$listFg[$i]['koitem_fg'].'">'.$listFg[$i]['koitem_fg'].' - '.$listFg[$i]['nama_fg'].'</option>';
                } ?>
              </select>
            </div>
            <?php if ($bulk_sisa['snfg_baru'] == null) {
              echo "<span style='color:#aaa;display:none' id='spanDemand'>Belum Ditentukan</span>";
            }else{
              echo "<span style='display:none;' id='spanDemand'><b>".explode('/BS',$bulk_sisa['snfg_baru'])[0]."</b></span>";
            } ?>
          </td>
        </tr>
        <?php // NOTE: JADWAL PLANNER ?>
        <tr id="line_action">
          <td class="content-sub-title">Penjadwalan Ulang</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data">
            <div class="" id="editorJadwal">
              <button type="button" name="button" class="btn jadwal" id="btnJadwal">Jadwalkan Sekarang</button>
            </div>
            <?php if (empty($jadwal)) {
              echo "<span style='color:#aaa;display:none' id='spanJadwal'><i>Belum Dijadwalkan</i></span>";
            }else{
              echo "<span style='display:none;' id='spanJadwal'><b>Dijadwalkan pada week ".$jadwal['week']."</b></span>";
            } ?>
          </td>
        </tr>
        <?php // NOTE: QC CHECK ?>
        <tr id="qc_check_action">
          <td class="content-sub-title">QC Check</td>
          <td class="content-sub-title" style="width:1em">:</td>
          <td class="content-sub-data">
            <div class="" id="editorQC">
              <button type="button" name="button" class="btn jadwal" id="btnQC">Periksa Sekarang</button>
            </div>
            <div style="display:flex;flex-wrap:wrap;gap:0.5em;">
            <?php
            if (empty($jadwal)) {
              echo "<span style='color:#aaa;display:none' id='spanQCBulk'><i>Belum Dijadwalkan</i></span>";
            }else{
              if(empty($qcStatus)){
                echo "<span style='color:#aaa;display:none' id='spanQCBulk'><i>Belum Diperiksa</i></span>";
              }else{
                $counter = 0;
                for ($i=0; $i < count($qcStatus); $i++) { ?>
                  <div style='display:none;' id='qcStatus-<?= ($i+1) ?>'>
                  <span style='text-align:center;font-size:0.7em'><b>Storage <?= ($i+1) ?></b></span>
                  <span style='text-align:center'><b><?= $qcStatus[$i]['status'] ?? 'NOT CHECKED YET' ?></b></span>
                  </div>
              <?php  $counter++; }
              }
            }
            ?>
          </div>
          </td>
        </tr>
        </tr>
      </table>
    </div>
  <?php }?>
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <span>Pilih label storage</span>
          <button id="btnClose" type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:right">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php $countPrint = 0; for ($i=0; $i < count($list_bulk_sisa); $i++) { ?>
            <button class="flex-item btn btn-success" type="button" name="button" id="print-<?= $i; ?>">
              Storage ke - <?= $i+1; ?>&ensp;
              <?php if ($list_bulk_sisa[$i]['is_printed'] == 0 || $list_bulk_sisa[$i]['is_printed'] == null) { $countPrint++;?>
                <i class="glyphicon glyphicon-exclamation-sign"></i>
              <?php } ?>
            </button>
            <script type="text/javascript">
            if (document.getElementById('print-<?= $i ?>')) {
                document.getElementById('print-<?= $i; ?>').onclick = function(){
                  var no_storage = <?= $i+1 ?>;
                  var isPrinted = <?= $list_bulk_sisa[$i]['is_printed'] ?? 0 ?>;
                  if (isPrinted == 1 && status_bulk == 'Rejected') {
                    $.get('index.php?r=bulk-sisa/update-print-label&nomo='+nomo+'&snfg='+snfg+'&no_storage='+no_storage+'&ket=isExpired',function(data){
                      if (data == 1) {
                        var syarat_kemas = '-'
                        for (var i = 0; i < syaratZona.length; i++) {
                          if (syaratZona[i][1] == line_kemas) {
                            syarat_kemas = syaratZona[i][0]
                            break;
                          }
                        }
                        for (var i = 0; i < syaratList.length; i++) {
                          if (syaratList[i][0] == syarat_kemas) {
                            // if (syaratList[i][0] == 'LIQUID C') {
                            //   threshold = minBerat;
                            //   maxValue = 50;
                            // }else{
                            //   threshold = syaratList[i][1];
                            //   maxValue = syaratList[i][2];
                            // }
                            threshold = syaratList[i][1];
                            maxValue = syaratList[i][2];
                            if (parseFloat(weight_bulk) >= threshold) {
                              status_bulk = 'Siap Dikemas';
                              break;
                            }else{
                              status_bulk = 'Rejected';
                              break;
                            }
                          }
                        }
                        var object = {
                          'nomo' : '<?= $jsonObj->nomo ?>',
                          'snfg' : '<?= $bulk_sisa['snfg'] ?>',
                          'syarat_kemas' : 'syarat_kemas',
                          'threshold' : threshold,
                          'totalStorage' : <?= count($list_bulk_sisa) ?>,
                          'no_storage' : <?= $i+1 ?>,
                          'totalQty' : <?= $sumQty['sum'] ?>
                        }
                        window.open("index.php?r=bulk-sisa/print-pdf&obj="+btoa(JSON.stringify(object)))
                        location.reload();
                      }else{
                        window.alert("Update gagal!")
                        location.reload();
                      }
                    })
                  }else{
                    $.get('index.php?r=bulk-sisa/update-print-label&nomo='+nomo+'&snfg='+snfg+'&no_storage='+no_storage,function(data){
                      if (data == 1) {
                        var syarat_kemas = '-'
                        for (var i = 0; i < syaratZona.length; i++) {
                          if (syaratZona[i][1] == line_kemas) {
                            syarat_kemas = syaratZona[i][0]
                            break;
                          }
                        }
                        for (var i = 0; i < syaratList.length; i++) {
                          if (syaratList[i][0] == syarat_kemas) {
                            // if (syaratList[i][0] == 'LIQUID C') {
                            //   threshold = minBerat;
                            //   maxValue = 50;
                            // }else{
                            //   threshold = syaratList[i][1];
                            //   maxValue = syaratList[i][2];
                            // }
                            threshold = syaratList[i][1];
                            maxValue = syaratList[i][2];
                            if (parseFloat(weight_bulk) >= threshold) {
                              status_bulk = 'Siap Dikemas';
                              break;
                            }else{
                              status_bulk = 'Rejected';
                              break;
                            }
                          }
                        }
                        var object = {
                          'nomo' : '<?= $jsonObj->nomo ?>',
                          'snfg' : '<?= $bulk_sisa['snfg'] ?>',
                          'syarat_kemas' : syarat_kemas,
                          'threshold' : threshold,
                          'totalStorage' : <?= count($list_bulk_sisa) ?>,
                          'no_storage' : <?= $i+1 ?>,
                          'totalQty' : <?= $sumQty['sum'] ?>
                        }
                        window.open("index.php?r=bulk-sisa/print-pdf&obj="+btoa(JSON.stringify(object)))
                        location.reload();
                      }else{
                        window.alert("Update gagal!")
                        location.reload();
                      }
                    })
                  }
                }
            }
            </script>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <?php if (!empty($jadwal)) { ?>
  <div class="modal fade bs-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Update Jadwal</h4>
        </div>
        <div class="modal-body">
          <form class="" id="form_update_jadwal" onsubmit="updateJadwal(); return false">
            <div class="form-group">
              <label for="start-date" class="control-label">Start Date:</label>
              <input type="date" class="form-control" id="start-date" value="<?= $jadwal['start'] ?>" required>
            </div>
            <div class="form-group">
              <label for="due-date" class="control-label">Due Date:</label>
              <input type="date" class="form-control" id="due-date" value="<?= $jadwal['due'] ?>" required>
            </div>
            <div class="form-group">
              <label for="week" class="control-label">Week:</label>
              <input type="text" class="form-control" id="week" value="<?= $jadwal['week'] ?>" required>
            </div>
            <div class="form-group">
              <label for="olah-premix" class="control-label">Line Olah Premix:</label>
              <input type="text" class="form-control" id="olah-premix" value="<?= $jadwal['line_olah_premix'] ?>">
            </div>
            <div class="form-group">
              <label for="olah-1" class="control-label">Line Olah 1:</label>
              <input type="text" class="form-control" id="olah-1" value="<?= $jadwal['line_olah'] ?>">
            </div>
            <div class="form-group">
              <label for="olah-2" class="control-label">Line Olah 2:</label>
              <input type="text" class="form-control" id="olah-2" value="<?= $jadwal['line_olah_2'] ?>">
            </div>
            <div class="form-group">
              <label for="adjust-olah-1" class="control-label">Adjust Olah 1:</label>
              <input type="text" class="form-control" id="adjust-olah-1" value="<?= $jadwal['line_adjust_olah_1'] ?>">
            </div>
            <div class="form-group">
              <label for="adjust-olah-2" class="control-label">Adjust Olah 2:</label>
              <input type="text" class="form-control" id="adjust-olah-2" value="<?= $jadwal['line_adjust_olah_2'] ?>">
            </div>
            <div class="form-group">
              <label for="kemas-1" class="control-label">Line Kemas 1:</label>
              <input type="text" class="form-control" id="kemas-1" value="<?= $jadwal['line_kemas_1'] ?>">
            </div>
            <div class="form-group">
              <label for="kemas-2" class="control-label">Line Kemas 2:</label>
              <input type="text" class="form-control" id="kemas-2" value="<?= $jadwal['line_kemas_2'] ?>">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary text-full-width" form="form_update_jadwal" id="submitJadwal">Update</button>
        </div>
      </div>
    </div>
  </div>
  <?php }?>
</div>
<?php
$script = <<< JS
$(document).ready(function() {
  badgeContent('status',status_bulk);
  badgeContent('pic',pic);
  badgeContent('age',ages);

  if ($countPrint > 0) {
    document.getElementById('btnClose').style.display = 'none'
  }
  if (!isPrint) {
    if (username == 'kemas') {
      swal({
        title: "Peringatan",
        text: "Silahkan print label terlebih dahulu!",
        icon: "warning",
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
          cancel: false,
          confirm: {
            text: "Print Sekarang",
            value: true,
            visible: true,
            className: "btn-print-sweetalert",
            closeModal: true
          }
        },
      })
      .then((res) => {
        document.getElementById('label').click();
      })
    }else{
      swal({
        title: "Peringatan",
        text: "Operator belum print label, silahkan print label terlebih dahulu!",
        icon: "warning",
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: false
      })
    }
  }
  if (username != 'kemas') {
    if (status_bulk == 'Rejected') {
      if (countQCStatus == 0) {
          document.getElementById('mps_action').style.display = "none"
          document.getElementById('packaging_action').style.display = "none"
          document.getElementById('demand_action').style.display = "none"
          document.getElementById('line_action').style.display = "none"
          document.getElementById('qc_check_action').style.display = "none"
      }else{
        if (username == 'planner') {
            document.getElementById('title-other-action').style.display = 'block'
            badgeContent('other','Cancel Jadwal');
            document.getElementById('other').onclick = function(){
              swal({
                title: "Peringatan",
                text: "Apakah anda yakin ingin mengcancel jadwal ini? Bulk sisa ini akan terhapus pada dashboard, begitu pula juga dengan jadwalnya.",
                icon: "warning",
                allowOutsideClick: false,
                closeOnClickOutside: false,
                buttons: {
                  cancel: {
                    text: "Batal",
                    value: false,
                    visible: true,
                    closeModal: true
                  },
                  confirm: {
                    text: "Cancel Sekarang",
                    value: true,
                    visible: true,
                    className: "text-full-width",
                    closeModal: true
                  }
                },
              })
              .then((res) => {
                if (res) {
                  $.get('index.php?r=bulk-sisa/delete-bulk-sisa-jadwal&snfg='+snfg_new,function(data){
                    if (data == 1) {
                      swal({
                        title: "Berhasil!",
                        text: "Jadwal bulk sisa berhasil dicancel!",
                        icon: "success",
                      })
                      .then((res) => {
                        // window.location = 'index.php?r=bulk-sisa'
                      })
                    }else{
                      window.alert("Cancel gagal!")
                      location.reload();
                    }
                  })
                }
              })
            }
        }
      }
    }else{
      if (flagAging) {
        if (username == 'spr' && pic == 'in SPR') {
          if (document.getElementById('title-other-action') && document.getElementById('other')) {
            document.getElementById('title-other-action').style.display = 'block'
            badgeContent('other','Reject Bulk');
            document.getElementById('other').onclick = function(){
              swal({
                title: "Peringatan",
                text: "Apakah anda yakin ingin mengereject bulk ini? Bulk sisa ini akan terhapus pada dashboard, begitu pula juga dengan jadwalnya.",
                icon: "warning",
                allowOutsideClick: false,
                closeOnClickOutside: false,
                buttons: {
                  cancel: {
                    text: "Batal",
                    value: false,
                    visible: true,
                    closeModal: true
                  },
                  confirm: {
                    text: "Reject Sekarang",
                    value: true,
                    visible: true,
                    className: "text-full-width",
                    closeModal: true
                  }
                },
              })
              .then((res) => {
                if (res) {
                  $.get('index.php?r=bulk-sisa/reject-bulk-sisa&snfg='+snfg,function(data){
                    if (data == 1) {
                      swal({
                        title: "Berhasil!",
                        text: "Bulk sisa berhasil direject!",
                        icon: "success",
                      })
                      .then((res) => {
                        location.reload();
                        // window.location = 'index.php?r=bulk-sisa'
                      })
                    }else{
                      window.alert("Cancel gagal!")
                      location.reload();
                    }
                  })
                }
              })
            }
          }
        }
        if (username == 'planner' && pic == 'in PPC' && status_bulk == 'Waiting List') {
          if (document.getElementById('title-other-action') && document.getElementById('other')) {
            document.getElementById('title-other-action').style.display = 'block'
            badgeContent('other','Reject Bulk');
            document.getElementById('other').onclick = function(){
              swal({
                title: "Peringatan",
                text: "Apakah anda yakin ingin mengereject bulk ini? Bulk sisa ini akan terhapus pada dashboard, begitu pula juga dengan jadwalnya.",
                icon: "warning",
                allowOutsideClick: false,
                closeOnClickOutside: false,
                buttons: {
                  cancel: {
                    text: "Batal",
                    value: false,
                    visible: true,
                    closeModal: true
                  },
                  confirm: {
                    text: "Reject Sekarang",
                    value: true,
                    visible: true,
                    className: "text-full-width",
                    closeModal: true
                  }
                },
              })
              .then((res) => {
                if (res) {
                  $.get('index.php?r=bulk-sisa/reject-bulk-sisa&snfg='+snfg+'&ket=Canceled',function(data){
                    if (data == 1) {
                      swal({
                        title: "Berhasil!",
                        text: "Bulk sisa berhasil direject!",
                        icon: "success",
                      })
                      .then((res) => {
                        location.reload();
                        // window.location = 'index.php?r=bulk-sisa'
                      })
                    }else{
                      window.alert("Cancel gagal!")
                      location.reload();
                    }
                  })
                }
              })
            }
          }
        }
      }
    }
    document.getElementById('fg_pcs').innerHTML = fg_pcs+" Pcs"
    document.getElementById('fg_pack').innerHTML = fg_pack+" Pcs"
    document.getElementById('konfMPS').onclick = function(){
        $("#konfMPS").attr('class', 'filter filter-selected btn');
        $("#notKonfMPS").attr('class', 'filter btn');
        currKonfMPS = 1;
        if (currKonfMPS >= 0 || currKonfPack >= 0) {
          if (currKonfMPS != konfMPS || currKonfPack != konfPacks) {
            $('#btnSave').css('display','block')
          }else{
            $('#btnSave').css('display','none')
          }
        }
    }
    document.getElementById('notKonfMPS').onclick = function(){
        $("#konfMPS").attr('class', 'filter btn');
        $("#notKonfMPS").attr('class', 'filter btn filter-selected');
        currKonfMPS = 0;
        if (currKonfMPS >= 0 || currKonfPack >= 0) {
          if (currKonfMPS != konfMPS || currKonfPack != konfPacks) {
            $('#btnSave').css('display','block')
          }else{
            $('#btnSave').css('display','none')
          }
        }
    }
    document.getElementById('konfPack').onclick = function(){
        $("#konfPack").attr('class', 'filter filter-selected btn');
        $("#notKonfPack").attr('class', 'filter btn');
        currKonfPack = 1;
        if (currKonfMPS >= 0 || currKonfPack >= 0) {
          if (currKonfMPS != konfMPS || currKonfPack != konfPacks) {
            $('#btnSave').css('display','block')
          }else{
            $('#btnSave').css('display','none')
          }
        }
    }
    document.getElementById('notKonfPack').onclick = function(){
        $("#konfPack").attr('class', 'filter btn');
        $("#notKonfPack").attr('class', 'filter btn filter-selected');
        currKonfPack = 0;
        if (currKonfMPS >= 0 || currKonfPack >= 0) {
          if (currKonfMPS != konfMPS || currKonfPack != konfPacks) {
            $('#btnSave').css('display','block')
          }else{
            $('#btnSave').css('display','none')
          }
        }
    }
    if (pic == 'in SPR' && status_bulk == 'Siap Dikemas' && username == 'spr') {
      if (flagAging) {
          document.getElementById('editorMps').style.display = 'none';
          document.getElementById('editorPack').style.display = 'none';
          document.getElementById('editorDemand').style.display = 'none';
          document.getElementById('spanMps').style.display = 'block';
          document.getElementById('spanPack').style.display = 'block';
          document.getElementById('spanDemand').style.display = 'block';
      }else{
        var select = document.getElementById('selectFg');
        select.value = demands;
        currDemands = demands;
        select.onchange = function(){
          currDemands = select.value;
          currNameDemands = select.options[select.selectedIndex].text;
          if (currDemands != demands) {
            $('#btnSave').css('display','block')
          }else{
            $('#btnSave').css('display','none')
          }
        }
        document.getElementById('editorMps').style.display = 'block';
        document.getElementById('editorPack').style.display = 'block';
        document.getElementById('editorDemand').style.display = 'block';
        document.getElementById('spanMps').style.display = 'none';
        document.getElementById('spanPack').style.display = 'none';
        document.getElementById('spanDemand').style.display = 'none';
      }
      /*}else if(!flagAging){
      document.getElementById('editorMps').style.display = 'none';
      document.getElementById('editorPack').style.display = 'none';
      document.getElementById('editorDemand').style.display = 'none';
      document.getElementById('spanMps').style.display = 'block';
      document.getElementById('spanPack').style.display = 'block';
      document.getElementById('spanDemand').style.display = 'block';*/
    // }else{
      // document.getElementById('mps_action').style.display = "none"
      // document.getElementById('packaging_action').style.display = "none"
      // document.getElementById('demand_action').style.display = "none"
      // document.getElementById('line_action').style.display = "none"
      // document.getElementById('qc_check_action').style.display = "none"
    }else{
        document.getElementById('editorMps').style.display = 'none';
        document.getElementById('editorPack').style.display = 'none';
        document.getElementById('editorDemand').style.display = 'none';
        document.getElementById('spanMps').style.display = 'block';
        document.getElementById('spanPack').style.display = 'block';
        document.getElementById('spanDemand').style.display = 'block';
    }
    if (pic == 'in PPC' && status_bulk == 'Waiting List' && username == 'planner') {
      document.getElementById('editorJadwal').style.display = 'block';
      document.getElementById('spanJadwal').style.display = 'none';
      document.getElementById('btnJadwal').onclick = function(){
        var object = {
          "nomo" : '$bulk_sisa[nomo]',
          "nama_bulk" : "$planner[nama_bulk]",
          "koitem_bulk" : '$planner[koitem_bulk]',
          "nama_fg" : "$planner[nama_fg]",
          "snfg_awal" : '$planner[snfg]',
          "snfg_baru" : '$bulk_sisa[snfg_baru]',
          "no_batch_awal" : '$bulk_sisa[no_batch]',
          "no_batch_baru" : '$bulk_sisa[no_batch_baru]',
          "berat" : '$bulk_sisa[qty]',
          "jumlah_pcs" : fg_pcs,
          "sediaan" : '$planner[sediaan]',
          "streamline" : '$planner[streamline]',
          "kode_jadwal" : '$planner[kode_jadwal]',
          "line_kemas_1" : '$planner[line_kemas_1]',
          "line_kemas_2" : '$planner[line_kemas_2]',
        }
        window.location = "index.php?r=scm-planner%2Fcreate&obj="+btoa(JSON.stringify(object));
      }
    }else{
      document.getElementById('editorJadwal').style.display = 'none';
      document.getElementById('spanJadwal').style.display = 'block';
    }
    if (pic == 'in QC Bulk' && status_bulk == 'Waiting Check' && username == 'qcbulk') {
      document.getElementById('editorQC').style.display = 'block';
      document.getElementById('btnQC').onclick = function(){
        var object = {
          "nomo" : '$bulk_sisa[nomo]',
          "nama_bulk" : "$planner[nama_bulk]",
          "koitem_bulk" : '$planner[koitem_bulk]',
          "nama_fg" : "$planner[nama_fg]",
          "snfg_awal" : '$planner[snfg]',
          "snfg_baru" : '$bulk_sisa[snfg_baru]',
          "no_batch_awal" : '$bulk_sisa[no_batch]',
          "no_batch_baru" : '$bulk_sisa[no_batch_baru]',
          "berat" : '$bulk_sisa[qty]',
          "kode_sl" : '$planner[streamline]',
          "totalStorage" : totalStorage,
          "jenis" : 'sampling'
        }
        // window.open("index.php?r=bulk-sisa/scan-sampling&nomo="+btoa(nomo));
        window.open("index.php?r=bulk-sisa/scan-validasi&obj="+btoa(JSON.stringify(object)));
      }
    }else{
      document.getElementById('editorQC').style.display = 'none';
      if (countQCStatus > 0) {
        for (var i = 0; i < countQCStatus; i++) {
          if (document.getElementById('qcStatus-'+(i+1))) {
              document.getElementById('qcStatus-'+(i+1)).style.display = 'inline-flex';
              document.getElementById('qcStatus-'+(i+1)).style.flexDirection = 'column';
              document.getElementById('qcStatus-'+(i+1)).style.backgroundColor = '#4CAF50';
              document.getElementById('qcStatus-'+(i+1)).style.color = 'white';
              document.getElementById('qcStatus-'+(i+1)).style.padding = '0.5em';
              document.getElementById('qcStatus-'+(i+1)).style.borderRadius = '0.5em';
              document.getElementById('qcStatus-'+(i+1)).style.flex = '1 1 0';
          }
        }
      }else{
        if (document.getElementById('spanQCBulk')){
            document.getElementById('spanQCBulk').style.display = 'block';
        }
      }
    }
    if (konfMPS == 0){
      $('#konfMPS').attr('class', 'filter btn');
      $('#notKonfMPS').attr('class', 'filter btn filter-selected');
      currKonfMPS = konfMPS
    } else {
      $('#konfMPS').attr('class', 'filter btn filter-selected');
      $('#notKonfMPS').attr('class', 'filter btn');
      currKonfMPS = konfMPS
    }
    if (konfPacks == 0){
      $('#konfPack').attr('class', 'filter btn');
      $('#notKonfPack').attr('class', 'filter btn filter-selected');
      currKonfPack = konfPacks
    } else {
      $('#konfPack').attr('class', 'filter btn filter-selected');
      $('#notKonfPack').attr('class', 'filter btn');
      currKonfPack = konfPacks
    }
    if (document.getElementById('other')) {
      if (username == 'spr') {
        if (flagAging) {
          document.getElementById('title-other-action').style.display = 'block'
          badgeContent('other','Reject Bulk');
          document.getElementById('other').onclick = function(){
            swal({
              title: "Peringatan",
              text: "Apakah anda yakin ingin mengereject bulk ini? Bulk sisa ini akan terhapus pada dashboard, begitu pula juga dengan jadwalnya.",
              icon: "warning",
              allowOutsideClick: false,
              closeOnClickOutside: false,
              buttons: {
                cancel: {
                  text: "Batal",
                  value: false,
                  visible: true,
                  closeModal: true
                },
                confirm: {
                  text: "Reject Sekarang",
                  value: true,
                  visible: true,
                  className: "text-full-width",
                  closeModal: true
                }
              },
            })
            .then((res) => {
              if (res) {
                $.get('index.php?r=bulk-sisa/reject-bulk-sisa&snfg='+snfg,function(data){
                  if (data == 1) {
                    swal({
                      title: "Berhasil!",
                      text: "Bulk sisa berhasil direject!",
                      icon: "success",
                    })
                    .then((res) => {
                      location.reload();
                      // window.location = 'index.php?r=bulk-sisa'
                    })
                  }else{
                    window.alert("Cancel gagal!")
                    location.reload();
                  }
                })
              }
            })
          }
        }else{
          if (['Waiting List'].includes(status_bulk)) {
            document.getElementById('title-other-action').style.display = 'block'
            badgeContent('other','Ubah Demands');
            document.getElementById('other').onclick = function(){
              swal({
                title: "Peringatan",
                text: "Apakah anda yakin ingin mengubah demand ini?",
                icon: "warning",
                allowOutsideClick: false,
                closeOnClickOutside: false,
                buttons: {
                  cancel: {
                    text: "Batal",
                    value: false,
                    visible: true,
                    closeModal: true
                  },
                  confirm: {
                    text: "Ubah Sekarang",
                    value: true,
                    visible: true,
                    className: "text-full-width",
                    closeModal: true
                  }
                },
              })
              .then((res) => {
                if (res) {
                  $.get('index.php?r=bulk-sisa/reset-bulk-sisa&nomo='+nomo+'&snfg='+snfg,function(data){
                    if (data == 1) {
                      swal({
                        title: "Berhasil!",
                        text: "Update data bulk berhasil disimpan!",
                        icon: "success",
                      })
                      .then((res) => {
                        location.reload();
                      })
                    }else{
                      window.alert("Delete gagal!")
                      location.reload();
                    }
                  })
                }
              })
            }
          }
        }
      }
      if (username == 'planner') {
        if (['Rework'].includes(status_bulk) && ['in PPC'].includes(pic)) {
          document.getElementById('title-other-action').style.display = 'block'
          badgeContent('other','Update Jadwal');
          document.getElementById('other').setAttribute("data-backdrop","static");
          document.getElementById('other').setAttribute("data-keyboard","false");
          document.getElementById('other').setAttribute("data-toggle","modal");
          document.getElementById('other').setAttribute("data-target","#exampleModal");
          if (ages.includes('Bulan')) {
            if (parseInt(ages.split(' ')[0]) > 5){
              var span = document.createElement("span");
              span.id = 'actionCancelJadwal';
              span.className = 'badge-content';
              document.getElementsByClassName('list-other-action')[0].appendChild(document.createElement("br"));
              document.getElementsByClassName('list-other-action')[0].appendChild(document.createElement("br"));
              document.getElementsByClassName('list-other-action')[0].appendChild(span);
              badgeContent('actionCancelJadwal','Cancel Jadwal');
              document.getElementById('actionCancelJadwal').onclick = function(){
                swal({
                  title: "Peringatan",
                  text: "Apakah anda yakin ingin mengcancel jadwal ini? Bulk sisa ini akan terhapus pada dashboard, begitu pula juga dengan jadwalnya.",
                  icon: "warning",
                  allowOutsideClick: false,
                  closeOnClickOutside: false,
                  buttons: {
                    cancel: {
                      text: "Batal",
                      value: false,
                      visible: true,
                      closeModal: true
                    },
                    confirm: {
                      text: "Cancel Sekarang",
                      value: true,
                      visible: true,
                      className: "text-full-width",
                      closeModal: true
                    }
                  },
                })
                .then((res) => {
                  if (res) {
                    $.get('index.php?r=bulk-sisa/delete-bulk-sisa-jadwal&snfg='+snfg_new,function(data){
                      if (data == 1) {
                        swal({
                          title: "Berhasil!",
                          text: "Jadwal bulk sisa berhasil dicancel!",
                          icon: "success",
                        })
                        .then((res) => {
                          // window.location = 'index.php?r=bulk-sisa'
                        })
                      }else{
                        window.alert("Cancel gagal!")
                        location.reload();
                      }
                    })
                  }
                })
              }
            }
          }
        } else if (['On Progress','Rework','Waiting Check','Pending'].includes(status_bulk) && flagAging) {
          document.getElementById('title-other-action').style.display = 'block'
          badgeContent('other','Cancel Jadwal');
          document.getElementById('other').onclick = function(){
            swal({
              title: "Peringatan",
              text: "Apakah anda yakin ingin mengcancel jadwal ini? Bulk sisa ini akan terhapus pada dashboard, begitu pula juga dengan jadwalnya.",
              icon: "warning",
              allowOutsideClick: false,
              closeOnClickOutside: false,
              buttons: {
                cancel: {
                  text: "Batal",
                  value: false,
                  visible: true,
                  closeModal: true
                },
                confirm: {
                  text: "Cancel Sekarang",
                  value: true,
                  visible: true,
                  className: "text-full-width",
                  closeModal: true
                }
              },
            })
            .then((res) => {
              if (res) {
                $.get('index.php?r=bulk-sisa/delete-bulk-sisa-jadwal&snfg='+snfg_new,function(data){
                  if (data == 1) {
                    swal({
                      title: "Berhasil!",
                      text: "Jadwal bulk sisa berhasil dicancel!",
                      icon: "success",
                    })
                    .then((res) => {
                      // window.location = 'index.php?r=bulk-sisa'
                    })
                  }else{
                    window.alert("Cancel gagal!")
                    location.reload();
                  }
                })
              }
            })
          }
        }
      }
      if (username == 'qcbulk') {
        if (['Pending','Rework'].includes(status_bulk)) {
          document.getElementById('title-other-action').style.display = 'block'
          badgeContent('other','Cek Kualitas');
          document.getElementById('other').onclick = function(){
            swal({
              title: "Peringatan",
              text: "Apakah anda yakin ingin mengubah kualitas bulk ini?",
              icon: "warning",
              allowOutsideClick: false,
              closeOnClickOutside: false,
              buttons: {
                cancel: {
                  text: "Batal",
                  value: false,
                  visible: true,
                  closeModal: true
                },
                confirm: {
                  text: "Ubah Sekarang",
                  value: true,
                  visible: true,
                  className: "text-full-width",
                  closeModal: true
                }
              },
            })
            .then((res) => {
              if (res) {
                var object = {
                  "nomo" : '$bulk_sisa[nomo]',
                  "nama_bulk" : "$planner[nama_bulk]",
                  "koitem_bulk" : '$planner[koitem_bulk]',
                  "nama_fg" : "$planner[nama_fg]",
                  "snfg_awal" : '$planner[snfg]',
                  "jenis" : 'sampling',
                  "snfg_baru" : '$bulk_sisa[snfg_baru]',
                  "no_batch_awal" : '$bulk_sisa[no_batch]',
                  "no_batch_baru" : '$bulk_sisa[no_batch_baru]',
                  "berat" : '$bulk_sisa[qty]',
                  "kode_sl" : '$planner[kode_jadwal]'.toUpperCase(),
                  "totalStorage" : totalStorage,
                }
                // window.open("index.php?r=bulk-sisa/scan-sampling&nomo="+btoa(nomo));
                window.open("index.php?r=bulk-sisa/scan-validasi&obj="+btoa(JSON.stringify(object)));
              }
            })
          }
        }
      }
      if (username == 'wpm') {
        if (['in WPM'].includes(pic)) {
          document.getElementById('title-other-action').style.display = 'block'
          badgeContent('other','Musnahkan Bulk');
          document.getElementById('other').onclick = function(){
            if (keterangan != null || keterangan != '') {
              var object = {
                "nomo" : '$bulk_sisa[nomo]',
                "nama_bulk" : "$planner[nama_bulk]",
                "koitem_bulk" : '$planner[koitem_bulk]',
                "nama_fg" : "$planner[nama_fg]",
                "snfg_awal" : '$planner[snfg]',
                "snfg_baru" : '$bulk_sisa[snfg_baru]',
                "no_batch_awal" : '$bulk_sisa[no_batch]',
                "no_batch_baru" : '$bulk_sisa[no_batch_baru]',
                "berat" : '$bulk_sisa[qty]',
                "kode_sl" : '$planner[streamline]',
                "totalStorage" : totalStorage,
                "jenis" : 'rejecting'
              }
              // window.open("index.php?r=bulk-sisa/scan-sampling&nomo="+btoa(nomo));
              window.open("index.php?r=bulk-sisa/scan-validasi&obj="+btoa(JSON.stringify(object)));
            }else{
              customAlert('info','Informasi','Print label reject belum di print. Silahkan print label reject baru terlebih dahulu')
            }
          }
        }
      }
    }
  }else{
    if (document.getElementById('other')) {
      badgeContent('other','Hapus Jadwal');
      document.getElementById('other').onclick = function(){
        swal({
          title: "Peringatan",
          text: "Apakah anda yakin ingin menghapus jadwal ini?",
          icon: "warning",
          allowOutsideClick: false,
          closeOnClickOutside: false,
          buttons: {
            cancel: {
              text: "Batal",
              value: false,
              visible: true,
              closeModal: true
            },
            confirm: {
              text: "Hapus Sekarang",
              value: true,
              visible: true,
              className: "text-full-width",
              closeModal: true
            }
          },
        })
        .then((res) => {
          if (res) {
            $.get('index.php?r=bulk-sisa/delete-bulk-sisa&nomo='+nomo+'&snfg='+snfg,function(data){
              if (data == 1) {
                swal({
                  title: "Berhasil!",
                  text: "Hapus jadwal bulk sisa berhasil dilakukan!",
                  icon: "success",
                })
                .then((res) => {
                  window.location = 'index.php?r=bulk-sisa'
                })
              }else{
                window.alert("Delete gagal!")
                location.reload();
              }
            })
          }
        })
      }
    }
    if (document.getElementById('addStorage')) {
      badgeContent('addStorage','Tambah Storage');
      document.getElementById('addStorage').onclick = function(){
        swal({
          html:true,
          title: "Konfirmasi Kembali",
          text: "Apakah anda yakin ingin menambah storage pada bulk sisa ini?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
          allowOutsideClick: false,
          closeOnClickOutside: false,
        })
        .then((res) => {
          if (res) {
            for (var i = 0; i < syaratZona.length; i++) {
              var zona_kemas = '-';
              if (syaratZona[i][1] == line_kemas) {
                zona_kemas = syaratZona[i][0]
                break;
              }
            }
            var currUrl = window.location.href;
            var url = new URL(currUrl);
            var obj = url.searchParams.get("obj");
            var jsonObj = JSON.parse(atob(obj))
            jsonObj.line_kemas = line_kemas;
            jsonObj.no_batch = no_batch;
            jsonObj.zona = '$bulk_sisa[zona]';
            jsonObj.nama_operator = operator;
            window.location = dns+"index.php?r=bulk-sisa/measure-massa&obj="+btoa(JSON.stringify(jsonObj))
          }
        });
      }
    }
  }

  function badgeContent(id,content){
    var defaults = {
      'background-color':'#DDDDDD',
      'color':'#888888',
    }
    var red = {
      'background-color':'#F7D8D6',
      'color':'#D73A30',
    };
    var gold = {
      'background-color':'#ECEAD3',
      'color':'#BDB76B',
    }
    var yellow = {
      'background-color':'#FDFF99',
      'color':'#969900',
    };
    var lightBlue = {
      'background-color':'#C1F3F3',
      'color':'#30D7D7',
    }
    var softBlue = {
      'background-color':'#D8E8F2',
      'color':'#3C8DBC',
    };
    var lime = {
      'background-color':'#CCF9EF',
      'color':'#00E3AD',
    }
    var purple = {
      'background-color':'#E7B3FF',
      'color':'#AD00FF',
    }
    var orange_pastel = {
      'background-color':'#FECAB2',
      'color':'#E87C72',
    }
    var orange = {
      'background-color':'#FFE1B3',
      'color':'#FF9900',
    }
    var green = {
      'background-color':'#dff0d8',
      'color':'#4CAF50',
    }
    var soft_green = {
      'background-color':'#DEF1EC',
      'color':'#6FB09A',
    }

    if (content == "Ubah Demands") {
      $('#'+id).css(softBlue)
      $('#'+id).html('<b>'+content+'</b>')
    }else if (content == "Tambah Storage") {
      $('#'+id).css(orange_pastel)
      $('#'+id).html('<b>'+content+'</b>')
    }else if(content == 'Hapus Jadwal'){
      $('#'+id).css(red)
      $('#'+id).html('<b>'+content+'</b>')
    }else if(content == 'Cancel Jadwal'){
      $('#'+id).css(orange_pastel)
      $('#'+id).html('<b>'+content+'</b>')
    }else if(content == 'Update Jadwal'){
      $('#'+id).css(softBlue)
      $('#'+id).html('<b>'+content+'</b>')
    }else if(content == 'Cek Kualitas'){
      $('#'+id).css(softBlue)
      $('#'+id).html('<b>'+content+'</b>')
    }else if(content == 'Reject Bulk'){
      $('#'+id).css(orange_pastel)
      $('#'+id).html('<b>'+content+'</b>')
    }else if (content == "Musnahkan Bulk") {
      $('#'+id).css(orange_pastel)
      $('#'+id).html('<b>'+content+'</b>')
    }else{
      $('#'+id).css(defaults)
      $('#'+id).html('<b>'+content+'</b>')
    }

    if (id == 'status') {
      if (content == "Siap Dikemas") {
        $('#'+id).css(lightBlue)
        $('#'+id).html('<b>Need Approval</b>')
      }else if (content == "Rejected") {
        $('#'+id).css(red)
        $('#'+id).html('<b>Rejected</b>')
      }else if (content == "Waiting List") {
        $('#'+id).css(orange)
        $('#'+id).html('<b>'+content+'</b>')
      }else if (content == "Waiting Check") {
        $('#'+id).css(orange)
        $('#'+id).html('<b>'+content+'</b>')
      }else if (content == "On Progress") {
        $('#'+id).css(green)
        $('#'+id).html('<b>'+content+'</b>')
      }else if (content == "Rework") {
        $('#'+id).css(lime)
        $('#'+id).html('<b>'+content+'</b>')
      }else if (content == "Pending") {
        $('#'+id).css(yellow)
        $('#'+id).html('<b>'+content+'</b>')
      }else{
        $('#'+id).css(defaults)
        $('#'+id).html('<b>'+content+'</b>')
      }
    }

    if (id == 'age') {
      if (content.includes('Bulan')) {
        if (parseInt(content.split(' ')[0]) < 6){
          $('#'+id).css(yellow)
        }else if (parseInt(content.split(' ')[0]) >= 6){
          $('#'+id).css(red)
        }else{
          $('#'+id).css(defaults)
        }
        $('#'+id).html('<b>'+content+'</b>')
      }else if (content.includes('Jam')) {
        if (parseInt(content.split(' ')[0]) == 0){
            $('#'+id).css(defaults)
            $('#'+id).html('<b>Baru Saja</b>')
        }else{
          $('#'+id).css(defaults)
          $('#'+id).html('<b>'+content+'</b>')
        }
      }else{
        $('#'+id).css(defaults)
        $('#'+id).html('<b>'+content+'</b>')
      }
    }

    if (id == 'pic') {
      if(content == 'in SPR'){
        $('#'+id).css(purple)
        $('#'+id).html('<b>'+content+'</b>')
      }else if(content == 'in PPC'){
        $('#'+id).css(gold)
        $('#'+id).html('<b>'+content+'</b>')
      }else if(content == 'in QC Bulk'){
        $('#'+id).css(softBlue)
        $('#'+id).html('<b>'+content+'</b>')
      }else if(content == 'in PRO'){
        $('#'+id).css(soft_green)
        $('#'+id).html('<b>'+content+'</b>')
      }else if(content == 'in WPM'){
        $('#'+id).css(red)
        $('#'+id).html('<b>'+content+'</b>')
      }else{
        $('#'+id).css(defaults)
        $('#'+id).html('<b>'+content+'</b>')
      }
    }

  }

  document.getElementById('btn-back').onclick = function(){
    window.location = "index.php?r=bulk-sisa/"
  }

  $('.js-example-basic-single').select2();

  document.getElementById('btnSave').onclick = function(){
    swal({
      title: "Konfirmasi Kembali",
      text: "Apakah anda yakin ingin mengubah data pada bulk ini?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((res) => {
      if (res) {
        var body = document.getElementsByTagName("BODY")[0];
        body.style.opacity = "0.5";
        document.getElementById("spinner").style.visibility = "visible";
        body.disabled = true;
        var object = {
          'nomo':document.getElementById('nomo').innerHTML,
          'konfMPS':currKonfMPS,
          'konfPack':currKonfPack,
          'demands':currDemands,
          'nama_demands':currNameDemands,
          'no_batch_baru' : no_batch+'X'
        }
        $.get('index.php?r=bulk-sisa/update-konf-spr',{ obj:btoa(JSON.stringify(object))},function(data){
          if (data == 1) {
            body.style.opacity = "1";
            document.getElementById("spinner").style.visibility = "hidden";
            body.disabled = false;
            swal({
              title: "Berhasil!",
              text: "Update data bulk berhasil disimpan!",
              icon: "success",
            })
            .then((res) => {
              location.reload();
            })
          }else{
            window.alert("Update gagal!")
          }
        })
      } else {
        swal("Proses dibatalkan!", {
          icon: "error",
        });
      }
    });
  }

  if (operator != "") {
    var operators = operator.split(",")
    for (var i = 0; i < operators.length; i++) {
      $('#nama_operator').tagsinput('add', operators[i]);
    }
  }

});
JS;
$this->registerJs($script);
?>
