<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;
use Da\QrCode\QrCode;

/* @var $this yii\web\View */
/* @var $model app\models\LogPenimbanganRm */
if ($planner['sediaan']=='L'){
  $sediaan = 'Liquid';
}else if ($planner['sediaan']=='P'){
  $sediaan = 'Powder';
}else if ($planner['sediaan']=='S'){
  $sediaan = 'Semisolid';
}else if ($planner['sediaan']=='V'){
  $sediaan = 'Varcos';
}else {
  $sediaan = '-';
}
?>
<div class="table-responsive">
  <table class="table" style="margin:0px; border-collapse: collapse;width:100%;">
    <tbody>
      <tr>
        <td style="border:1px solid black;text-align:center;">
          <img src="../web/images/logo-pti.png" style="width:240px; text-align:center; margin-left:17px;padding:8px 0px"></td>
        </td>
        <td style="border:1px solid black;text-align:center;padding:8px 0px;">
          <p style="margin-top:0px; margin-bottom:3px; font-size: 1.8em;"><b>PT PARAGON TECHNOLOGY AND INNOVATION</b></p>
        </td>
      </tr>
    </tbody>
  </table>

  <table class="table" style="margin:0px; border-collapse: collapse;width:100%">
    <tbody>
      <tr>
        <td style="border:1px solid black;text-align:center;padding:1% 0%;width:100%;">
          <h3 style="margin:0px"><b>STATUS PRODUK RUAHAN SISA KEMAS</b></h3>
        </td>
      </tr>
    </tbody>
  </table>

  <table class="table" style="margin:0px; border-collapse: collapse;width:100%;">
    <tbody>
      <!-- NAMA BULK -->
      <tr style="padding:2% 2%">
        <td style="border:1px solid black;border-right:none;width:20%;vertical-align:middle">
          <span class="data-text">Nama Bulk</span>
        </td>
        <td style="border:1px solid black;border-left:none;border-right:none;width:1%;vertical-align:middle">
          <span class="data-text">:</span>
        </td>
        <td style="border:1px solid black;border-left:none;width:40%">
          <span class="data-text"><?= $planner['nama_bulk'] ?></span>
        </td>
        <td style="border:1px solid black;text-align:center;vertical-align:middle" rowspan="8">
          <?php
            if ($bulk_sisa['status'] != 'Rejected') {
                echo "<h1><b>KARANTINA</b></h1>";
            }else{
                echo "<img src='".Url::base(true)."/images/rejected-stamp.png' style='width:30%'><br>";
                echo '<span class="data-text">'.$planner["nama_bulk"].'</span><br>';
                echo '<span class="data-text">No Batch: '.$bulk_sisa["no_batch"].'</span><br>';
                echo '<span class="data-text">Berat: '.$bulk_sisa["qty"].' Kg</span><br>';
            }
          ?>
        </td>
      </tr>
      <!-- NO BATCH -->
      <tr style="padding:2% 2%">
        <td style="border:1px solid black;border-right:none;width:20%;vertical-align:middle">
          <span class="data-text">No Batch</span>
        </td>
        <td style="border:1px solid black;border-left:none;border-right:none;width:1%;vertical-align:middle">
          <span class="data-text">:</span>
        </td>
        <td style="border:1px solid black;border-left:none;width:40%">
          <?php
            if ($bulk_sisa['status'] != 'Rejected') {
              echo '<span class="data-text">'.$bulk_sisa["no_batch"].'X</span>';
            }else{
              echo '<span class="data-text">'.$bulk_sisa["no_batch"].'</span>';
            }
          ?>
        </td>
      </tr>
      <!-- SNFG -->
      <tr style="padding:2% 2%">
        <td style="border:1px solid black;border-right:none;width:20%;vertical-align:middle">
          <span class="data-text">SNFG</span>
        </td>
        <td style="border:1px solid black;border-left:none;border-right:none;width:1%;vertical-align:middle">
          <span class="data-text">:</span>
        </td>
        <td style="border:1px solid black;border-left:none;width:40%">
          <span class="data-text"><?= $planner['snfg'] ?></span>
        </td>
      </tr>
      <!-- STREAMLINE & KODE SL -->
      <tr style="padding:2% 2%">
        <td style="border:1px solid black;border-right:none;width:20%;vertical-align:middle">
          <span class="data-text">Streamline & Kode SL</span>
        </td>
        <td style="border:1px solid black;border-left:none;border-right:none;width:1%;vertical-align:middle">
          <span class="data-text">:</span>
        </td>
        <td style="border:1px solid black;border-left:none;width:40%">
          <span class="data-text"><?= $planner['streamline']; ?> & <?= $planner['kode_jadwal'] ?></span>
        </td>
      </tr>
      <!-- LINE KEMAS -->
      <tr style="padding:2% 2%">
        <td style="border:1px solid black;border-right:none;width:20%;vertical-align:middle">
          <span class="data-text">Line Kemas</span>
        </td>
        <td style="border:1px solid black;border-left:none;border-right:none;width:1%;vertical-align:middle">
          <span class="data-text">:</span>
        </td>
        <td style="border:1px solid black;border-left:none;width:40%">
          <span class="data-text"><?= $flow_input_snfgkomponen['nama_line'] ?? $flow_input_snfg['nama_line'] ?></span>
        </td>
      </tr>
      <!-- TANGGAL TIMBANG BULK SISA -->
      <tr style="padding:2% 2%">
        <td style="border:1px solid black;border-right:none;width:20%;vertical-align:middle">
          <span class="data-text">Tanggal Timbang</span>
        </td>
        <td style="border:1px solid black;border-left:none;border-right:none;width:1%;vertical-align:middle">
          <span class="data-text">:</span>
        </td>
        <td style="border:1px solid black;border-left:none;width:40%">
          <span class="data-text"><?= date('d-m-Y',strtotime($bulk_sisa['tgl_timbang'])); ?></span>
        </td>
      </tr>
      <!-- NAMA OPERATOR -->
      <tr style="padding:2% 2%">
        <td style="border:1px solid black;border-right:none;width:20%;vertical-align:middle">
          <span class="data-text">Nama Operator</span>
        </td>
        <td style="border:1px solid black;border-left:none;border-right:none;width:1%;vertical-align:middle">
          <span class="data-text">:</span>
        </td>
        <td style="border:1px solid black;border-left:none;width:40%">
          <span class="data-text"><?= $bulk_sisa['nama_op'] ?></span>
        </td>
      </tr>
      <!-- NO STORAGE -->
      <tr style="padding:2% 2%">
        <td style="border:1px solid black;border-right:none;width:20%;vertical-align:middle">
          <span class="data-text">No Storage</span>
        </td>
        <td style="border:1px solid black;border-left:none;border-right:none;width:1%;vertical-align:middle">
          <span class="data-text">:</span>
        </td>
        <td style="border:1px solid black;border-left:none;width:40%">
          <span class="data-text"><?= $jsonObj->no_storage; ?> / <?= $jsonObj->totalStorage ?></span>
        </td>
      </tr>
      <!-- Berat Bulk Sisa -->
      <tr style="padding:2% 2%">
        <td style="border:1px solid black;border-right:none;width:20%;vertical-align:middle">
          <span class="data-text">Berat Bulk Sisa</span>
        </td>
        <td style="border:1px solid black;border-left:none;border-right:none;width:1%;vertical-align:middle">
          <span class="data-text">:</span>
        </td>
        <td style="border:1px solid black;border-left:none;width:40%">
          <span class="data-text"><?= $bulk_sisa['qty'] ?> Kg</span>
        </td>
        <td style="border:1px solid black;border-top: none;text-align:center;vertical-align:middle" rowspan="5">
          <?php
          $path = Url::base(true)."/qr_suratjalan/".preg_replace('/[^A-Za-z0-9\-]/', '', $jsonObj->nomo).".png";
          // Use get_headers() function
          $headers = @get_headers($path);

          // Use condition to check the existence of URL
          if(!($headers && strpos( $headers[0], '200'))) {
            $qrCode = (new QrCode($jsonObj->nomo))
                ->setSize(200)
                ->setMargin(3)
                ->useForegroundColor(0, 0, 0);
            $qrCode->writeFile('/var/www/html/flowreport/web/qr_suratjalan/'. preg_replace('/[^A-Za-z0-9\-]/', '', $jsonObj->nomo).'.png');
          }
          echo "
            <img src='".$path."' style='margin-bottom:1%;width:10%'><br>
          ";
          $status = $jsonObj->nomo;

          // Display result
          echo '<span class="data-text">'.$status.'</span>';
          ?>
        </td>
      </tr>
      <!-- Berat Total Bulk Sisa -->
      <tr style="padding:2% 2%">
        <td style="border:1px solid black;border-right:none;width:20%;vertical-align:middle">
          <span class="data-text">Berat Total Bulk Sisa</span>
        </td>
        <td style="border:1px solid black;border-left:none;border-right:none;width:1%;vertical-align:middle">
          <span class="data-text">:</span>
        </td>
        <td style="border:1px solid black;border-left:none;width:40%">
          <span class="data-text"><?= $jsonObj->totalQty ?> Kg</span>
        </td>
      </tr>
      <!-- SL STORAGE -->
      <tr style="padding:2% 2%">
        <td style="border:1px solid black;border-right:none;width:20%;vertical-align:middle">
          <span class="data-text">Shelf Life Storage</span>
        </td>
        <td style="border:1px solid black;border-left:none;border-right:none;width:1%;vertical-align:middle">
          <span class="data-text">:</span>
        </td>
        <td style="border:1px solid black;border-left:none;width:40%;vertical-align:middle">
            <span class="data-text"><?= $qc_bulk_entry_komponen['savelife_storage'] ? date('d-m-Y',strtotime($qc_bulk_entry_komponen['savelife_storage'])) : '-' ?></span>
        </td>
      </tr>
      <!-- KETERANGAN -->
      <tr style="padding:2% 2%">
        <td style="border:1px solid black;border-right:none;border-bottom: none;width:20%;height:1vh!important">
          <span class="data-text">Keterangan</span>
        </td>
        <td style="border:1px solid black;border-left:none;border-bottom: : none;border-right:none;width:1%;">
          <span class="data-text">:</span>
        </td>
        <td style="border:1px solid black;border-left:none;border-bottom: none;width:40%">
        </td>

      </tr>
      <tr style="padding:2% 2%">
        <td style="border:1px solid black;border-top: none;text-align:center" colspan="3">
          <?php if ($bulk_sisa['zona'] == 'LIQUID C') { ?>
            <?php if ($bulk_sisa['keterangan'] == 'Threshold') { ?>
              <b><h5>VALUE BULK < Rp5.000.000 SEHINGGA TIDAK MEMENUHI SYARAT PENJADWALAN KEMBALI</h5></b>
            <?php } else { ?>
              <b><h5>VALUE BULK ≥ Rp5.000.000 SEHINGGA MEMENUHI SYARAT PENJADWALAN KEMBALI</h5></b>
            <?php } ?>
          <?php } else { ?>
            <?php if ($bulk_sisa['status'] == 'Rejected') { ?>
              <?php if ($bulk_sisa['keterangan'] == 'Threshold') { ?>
                <b><h5>BERAT BULK < <?= $jsonObj->threshold; ?> KG SEHINGGA TIDAK MEMENUHI SYARAT PENJADWALAN KEMBALI</h5></b>
              <?php } else if (in_array($bulk_sisa['keterangan'],['Due Date','Canceled'])) { ?>
                  <b><h5>BULK SISA SUDAH MELEBIHI EXPIRED DATE SEHINGGA TIDAK MEMENUHI SYARAT PENJADWALAN KEMBALI</h5></b>
              <?php } else {?>
                  <b><h5>BULK SISA SUDAH PERNAH SISA SEHINGGA TIDAK MEMENUHI SYARAT PENJADWALAN KEMBALI</h5></b>
              <?php } ?>
            <?php } else {?>
              <b><h5>BERAT BULK ≥ <?= $jsonObj->threshold; ?> KG SEHINGGA MEMENUHI SYARAT PENJADWALAN KEMBALI</h5></b>
            <?php } ?>
          <?php } ?>
        </td>
      </tr>
    </tbody>
  </table>

</div>
