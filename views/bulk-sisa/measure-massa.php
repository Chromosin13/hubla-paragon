<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use wbraganca\tagsinput\TagsinputWidget;
use app\models\FlowAnalyst;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>
<link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">
<?php
$script = <<< JS



JS;
$this->registerJs($script);
?>

<style>
  .swal-button-container{
    text-align: center!important;
    display: inline-block;
  }

  .swal-footer{
    text-align: center;
  }

  .swal-text{
    text-align: center;
  }

  .swal-button--confirm{
    background-color: #4CAF50
  }

  .swal-button:hover{
    background-color: #aaa!important
  }


  .swal-button{
    vertical-align: middle!important;
    padding: 0em 1em!important;
    text-transform: capitalize;
    width: 10em;
  }

  .wegihing-layout {
    position:relative;
    margin:auto;
  }

  .weighing-layout .wl-header{
    text-align:center;
    font-size: 3.8vh;
    font-family: Exo;
  }

  .weighing-layout .wl-container{
    background : #4CAF50;
    width: 14em;
    height:20em;
    position: relative;
    border-style: solid;
    border-left:1em solid black;
    border-bottom:1em solid black;
    border-right:1em solid black;
    border-top:1em solid white;
    border-radius: 0 0 0.6em 0.6em;
    display: absolute;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
  }
  .weighing-layout .wl-container .wl-fill {
    margin-bottom: 1px;
    width: 100%;
    height: 100%;
    background: #e0e0e0;
    -webkit-transition: height 1s;
    transition: height 1s;
    position: absolute;;
  }

  .weighing-layout  .wl-container .hl {
    position:absolute;
    border-top: 3px solid red;
    width: 130%;
  }

  .wl-textbox{
    border: 4px solid #4CAF50!important;
    border-radius: 1em!important;
    text-align: center!important;
    width: 10em;
    padding: 1em 0em!important;
    margin-top: 1em;
    margin-left: auto;;
    margin-right: auto;
    margin-bottom: 2em;
  }

  .wl-textbox .pl{
    text-align:center;
    font-size: 1.4em;
    color: #4CAF50
  }

  .widget-user-header{
    text-align: center;
    margin-left: auto;
    margin-right: auto;
    height: 30%!important
  }

  .bg-green-old{
    background-color: #4CAF50;
    color: white;
    border: none;
  }

  .title-text{
    font-size: 28px;
    vertical-align: middle;
    font-weight: 600
  }

  .sub-title{
    text-align: left;
    margin-left: 1vw!important;
    font-size: 0.8em;
    color: #4CAF50;
  }

  .form-control{
    margin-bottom: 4px
  }

  .accordion {
    color: white;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: center;
    outline: none;
    font-size: 1em  ;
    transition: 0.4s;
    margin-top: 1em;
    margin-bottom: 0em;
  }

  .active, .accordion:hover {
    background-color: #ccc;
  }

  .accordion:after {
    content: '\002B';
    color: white;
    font-weight: bold;
    float: right;
    margin-left: 5px;
  }

  .active:after {
    content: "\2212";
    color: white;
  }

  .bootstrap-tagsinput{
    min-height: 3.8rem;
    text-align: left;
    background-color: transparent!important;
    border:none!important;
    -moz-box-shadow: none!important;
    outline: none!important;
    -webkit-box-shadow: none!important;
  }

  .badge-info span{
    display: none!important
  }

  .badge-info{
    margin-right: 1%!important;
    text-transform: uppercase!important;
    background-color: #4CAF50;
    padding: 1em!important;
    font-size: 1em!important;
  }

  .bootstrap-tagsinput input{
    position: absolute!important;
    padding: 0px!important;
    margin-top: -0.5%!important;
    margin-left: 1%!important;
    -moz-box-shadow: none!important;
    outline: none!important;
    -webkit-box-shadow: none!important;
    background-color: transparent!important;
    border:none!important;
    display: none!important
  }

  .content-layout{
    background-color:#4CAF50;
    padding: 1vw;
    border-radius: 1vw;
  }

  .title{
    font-size: 1vw;
    color: white;
  }

  .panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
  }

  .data-contains{
    color: #4CAF50;
    font-size: 1.4em;
    font-weight: 600;
    margin-top: 0em
  }

  label{
    color: #aaa;
    font-size: 1em;
    margin-top: 1em;
    margin-bottom: 0em;
  }

  .btns{
    font-size: 1.2em!important;
    border:none!important;
  }

  .btns:hover{
    background-color: #777!important;
  }

</style>

<script type="text/javascript">
  var dns = 'https://factory.pti-cosmetics.com/flowreport/web/'
  // var dns = 'http://factory.pti-cosmetics.com/flowreport_dev/web/'
  // var dns = ''
  var timbangan = '<?= $map_device['timbangan']; ?>';
  var massa = 0;
  var nomo = '<?= $jsonObj->nomo ?>';
  var uom = '<?= $data_timbangan['uom'] ?>';
  var decimal_digit = <?= $data_timbangan['angka_belakang_koma'] ?>;
  // Untuk galat sementara 0
  var galat = <?= $data_timbangan['galat'] ?>;
  var raspi_ip = '<?= $map_device['raspi_ip']; ?>';
  var weight_batch =<?= $scm_planner['besar_batch'] ?>;
  var threshold = 0;
  var netto_refil = <?= $netto_refil['child_1_qty'] ?? 0;?>;
  var het = <?= round($het['fixed_price']) ?>; //Set default Rp75.000
  var hargaBulk = het * netto_refil;
  var maxValue = 0;
  var keterangan = null;
  var sumQty = <?= $sumQty['sum'] ?? 0; ?>;
  var syaratZona =
  [
    ['LIQUID A','TUP01'],['LIQUID A','TUP02'],['LIQUID A','TUP03'],['LIQUID A','TUP04'],
    ['LIQUID A','TUP05'],['LIQUID A','TUP06'],['LIQUID A','TUP07'],['LIQUID A','TUP08'],
    ['LIQUID A','TUP09'],['LIQUID B','JAP01'],['LIQUID B','BLP01'],['LIQUID B','BLP02'],
    ['LIQUID B','BOP01'],['LIQUID B','BOP02'],['LIQUID B','BOP03'],['LIQUID B','BOP04'],
    ['LIQUID B','BOP05'],['LIQUID B','BOP06'],['LIQUID B','EMP02'],['LIQUID B','EMP05'],
    ['LIQUID B','LQP04'],['LIQUID C','JAP03'],['LIQUID C','BOP08'],['LIQUID C','BOP09'],
    ['LIQUID C','BOP10'],['LIQUID C','CUP01'],['LIQUID C','EMP01'],['LIQUID C','EMP03'],
    ['LIQUID C','EMP07'],['LIQUID C','EMP15'],['LIQUID C','EMP16'],['LIQUID C','EMP17'],
    ['LIQUID C','OSP01'],

    ['POWDER A','LPP01'],['POWDER A','LPP02'],['POWDER A','LPP03'],['POWDER A','PNP01'],
    ['POWDER A','PNP03'],['POWDER A','PNP04'],['POWDER A','PNP01MANUAL'],['POWDER A','PNP04MANUAL'],
    ['POWDER B','PCP01'],['POWDER B','PCP02'],['POWDER B','PCP03'],['POWDER B','PCP04'],
    ['POWDER B','PCP05'],['POWDER B','PEP01'],['POWDER B','PEP02'],['POWDER B','PEP04'],
    ['POWDER B','PEP05'],['POWDER B','PEP06'],

    ['BOTTLE LINE - VARCOS','VSN01'],['BOTTLE LINE - VARCOS','VSN02'],['BOTTLE LINE - VARCOS','VSN03'],['BOTTLE LINE - VARCOS','VMN01'],['BOTTLE LINE - VARCOS','VMN02'],

    ['BOTTLE LINE - SEMISOLID','BTP02'],['BOTTLE LINE - SEMISOLID','BTP04'],['BOTTLE LINE - SEMISOLID','LIP11'],
    ['BOTTLE LINE - SEMISOLID','LIP18'],['BOTTLE LINE - SEMISOLID','LRP01'],

    ['JAR & FOUNDATION - SEMISOLID','FOP03'],['JAR & FOUNDATION - SEMISOLID','JRP02'],['JAR & FOUNDATION - SEMISOLID','SBP01'],
    ['LIPSTICK LINE - SEMISOLID','SAP01'],['LIPSTICK LINE - SEMISOLID','ALP03'],['LIPSTICK LINE - SEMISOLID','LIP06'],
    ['LIPSTICK LINE - SEMISOLID','LIP08'],['LIPSTICK LINE - SEMISOLID','LIP10'],['LIPSTICK LINE - SEMISOLID','LIP12'],
    ['LIPSTICK LINE - SEMISOLID','LIP14'],['LIPSTICK LINE - SEMISOLID','LIP16'], ['LIPSTICK LINE - SEMISOLID','SMALLBATCH']
  ]
  var syaratList =
  [
    ['LIQUID A',30.0,100],['LIQUID B',30.0,100],['LIQUID C',10.0,100],
    ['POWDER A',4.0,50],['POWDER B',2.0,50],
    ['BOTTLE LINE - VARCOS',4.0,50],['LIPSTICK LINE - SEMISOLID',2.0,50],['BOTTLE LINE - SEMISOLID',3.0,50],
    ['JAR & FOUNDATION - SEMISOLID',4.0,50]
  ]

  var line_kemas = '<?= $jsonObj->line_kemas ?>';
  var syarat_kemas = '-'
  for (var i = 0; i < syaratZona.length; i++) {
    if (syaratZona[i][1] == line_kemas) {
      syarat_kemas = syaratZona[i][0]
      break;
    }
  }
  for (var i = 0; i < syaratList.length; i++) {
    if (syaratList[i][0] == syarat_kemas) {
      /*if (syaratList[i][0] == 'LIQUID C') {
        threshold = minBerat;
        maxValue = 50;
      }else{
        threshold = syaratList[i][1];
        maxValue = syaratList[i][2];
      }*/
      threshold = syaratList[i][1];
      maxValue = syaratList[i][2];
      break;
    }
  }
  url = "http://"+raspi_ip+":6900";
  console.log(url);
  // console.log('Threshold: '+threshold+' >< Max: '+maxValue);

  var j = setInterval(function() {
    $.getJSON(url,function(result){
      j = result[0];
      massa = j.weight;
      animation = massa/maxValue*100;
      massaFix = parseFloat(massa).toFixed(decimal_digit);

      if (animation<=100){
        document.getElementById("fill").style.height = (100-animation)+"%";
      } else {
        document.getElementById("fill").style.height = "0%";
      }
      $("#massa").text(massaFix);
    });
  }, 1000);

  function submit(){
    var weight_bulk = document.getElementById("massa").innerHTML;
    console.log("Berat: "+parseFloat(weight_bulk)+"; "+sumQty)
    if (parseFloat(weight_bulk) > 0) {
      swal({
        html:true,
        title: "Konfirmasi Kembali",
        text: "Apakah anda yakin berat bulk sudah sesuai ["+weight_bulk+" Kg]?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((res) => {
        if (res) {
          var weight_teoritis = <?= $scm_planner['besar_batch'] ?>;
          if (weight_bulk <= weight_teoritis) {
            if ("<?= $scm_planner['snfg'] ?>".includes('/BS')) {
              status = 'Rejected';
              keterangan = 'Leftovers';
            }else{
              if (syaratList[i][0] == 'LIQUID C') {
                var theValue = (((parseFloat(weight_bulk)+sumQty)*1000)/netto_refil)*(het*0.3*0.3);
                if (theValue >= 5000000) {
                  status = 'Siap Dikemas';
                  keterangan = null;
                }else{
                  status = 'Rejected';
                  keterangan = 'Threshold';
                }
              }else{
                var theValue = (parseFloat(weight_bulk)+sumQty);
                if (theValue >= threshold) {
                  status = 'Siap Dikemas';
                  keterangan = null;
                }else{
                  status = 'Rejected';
                  keterangan = 'Threshold';
                }
              }
            }
            var object = {
              "nomo" :"<?= $bulk_sisa['nomo'] ?>",
              "snfg" :"<?= $scm_planner['snfg'] ?>",
              "nama_operator" :" <?= $bulk_sisa['nama_op'] ?>",
              "kode_bulk":"<?= $bulk_sisa['kode_bulk'] ?>",
              "line_kemas":"<?= $jsonObj->line_kemas; ?>",
              "no_batch":"<?= $bulk_sisa['no_batch'] ?>",
              "qty":parseFloat(weight_bulk),
              "zona":"<?= $jsonObj->zona ?>",
              "syaratZona":syarat_kemas,
              "status":status,
              "threshold":threshold,
              "keterangan":keterangan,
            }
            // window.location = "index.php?r=bulk-sisa/update-weight&obj="+btoa(JSON.stringify(object));
            $.get('index.php?r=bulk-sisa/update-weight',{ obj:btoa(JSON.stringify(object))},function(data){
              if (data == 1) {
                swal({
                  title: "Berhasil!",
                  text: "Data berhasil disimpan!",
                  icon: "success",
                })
                .then((res) => {
                  swal({
                    html:true,
                    title: "Konfirmasi",
                    text: "Apakah anda ingin menambah storage?",
                    icon: "info",
                    allowOutsideClick: false,
                    closeOnClickOutside: false,
                    dangerMode: true,
                    buttons: {
                      cancel: {
                        text: "Tidak",
                        value: false,
                        visible: true,
                        closeModal: true
                      },
                      confirm: {
                        text: "Tambah Storage",
                        value: true,
                        visible: true,
                        className: "text-full-width",
                        closeModal: true
                      }
                    },
                  })
                  .then((res) => {
                    if (res) {
                      var currUrl = window.location.href;
                      var url = new URL(currUrl);
                      var obj = url.searchParams.get("obj");
                      window.location = dns+"index.php?r=bulk-sisa/measure-massa&obj="+obj
                    }else{
                      window.location = dns+"index.php?r=bulk-sisa/detail-bulk-info&obj="+btoa(JSON.stringify(object));
                    }
                  })
                })
              }else{
                console.log(data);
              }
            })
          }else{
            customAlert('warning','Berat Bulk Tidak Tepat','Pastikan berat bulk sisa tidak melebihi dari berat teoritis!')
          }
        } else {
          swal("Proses dibatalkan!", {
            icon: "error",
          });
        }
      });
    }else{
      customAlert("warning","Berat Bulk Tidak Tepat","Silahkan input berat bulk sisa secara manual jika animasi tidak jalan!")
    }
  }

  function customAlert(type,title,message){
    swal({
      closeOnClickOutside: false,
      title: title,
      text: message,
      icon: type,
      backdrop:true,
      button: "Mengerti!",
    });
  }

  function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if(charCode == 46){
        return true;
      }else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
      }
      return true;
  }

</script>

<div class="box box-widget widget-user">
  <div class="widget-user-header bg-green-old">
    <span class="title-text">Penimbangan Bulk Sisa</span>
  </div>
  <div class="box-footer">
    <div class="col-md-12 column" style="margin-top:4vh">
      <div class="weighing-layout" style="text-align:center">
        <div class="wl-container">
          <div class="wl-fill" id="fill"></div>
            <div class="hl" name="btsBawah" id="btsBawah"></div>
        </div>
      </div>
      <div class="wl-textbox">
        <div class="sub-title">
          <span>Weight :</span>
        </div>
        <div class="pl"><b id="massa" name="massa">0</b> Kg</div>
      </div>
    </div>
    <center><span style="font-size:1em">Jika animasi tidak jalan, input secara manual</span></center>
    <div class="col-md-12" style="margin-top:1em">
      <button type="button" name="button" class="btns" style="width:100%;font-size:2vh" id="btnInputManual">Input Manual</button>
    </div>
    <div class="data-content">
      <div class="col-md-6">
        <div class="">
          <label for="nama_fg">Nama FG</label>
          <span class="data-contains" id="nama_fg"><?= $scm_planner['nama_fg']; ?></span>
        </div>
        <div class="">
          <label for="nama_bulk">Nama Bulk</label>
          <span class="data-contains" id="nama_bulk"><?= $scm_planner['nama_bulk']; ?></span>
        </div>
        <div class="">
          <label for="no_batch">NOMO</label>
          <span class="data-contains" id="nomo_bulk_sisa"><?= $scm_planner['nomo']; ?></span>
        </div>
        <div class="">
          <label for="kode_bulk">SNFG</label>
          <span class="data-contains"><?= $scm_planner['snfg']; ?></span>
        </div>
        <div class="">
          <label for="kode_bulk">No. Storage</label>
          <?php if ($sumQty['sum'] > 0) {
            $storage = $jsonObj->countStorage+1;
          }else{
            $storage = $jsonObj->countStorage;
          } ?>
          <span class="data-contains"><?= 'Storage ke '.$storage;?></span>
        </div>
      </div>
      <div class="col-md-6">
        <div class="">
          <label for="no_batch">No. Batch</label>
          <span class="data-contains" id="no_batch"><?= $jsonObj->no_batch; ?></span>
        </div>
        <div class="">
          <label for="zona_kemas">Zona Kemas</label>
          <span class="data-contains" id="zona_kemas"><?= $jsonObj->zona; ?></span>
        </div>
        <div class="">
          <label for="line_kemas">Line Kemas</label>
          <span class="data-contains" id="line_kemas"><?= $jsonObj->line_kemas; ?></span>
        </div>
        <div class="">
          <label for="nama_operator">Nama Operator</label>
          <input type="text" name="nama_operator" id="nama_operator" data-role="tagsinput" disabled></input>
        </div>
      </div>
    </div>
    <div class="col-md-12" style="margin-top:1em">
      <!-- <button type="button" name="button" class="btns" style="width:100%;font-size:2vh" onclick="submit()">SUBMIT</button> -->
      <button onclick="submit()" class="bg-green-old btns" style="display: block; margin-left: auto; margin-right: auto; border-radius: 4px; width:100%; font-size:1.4em;">Submit</button>
    </div>
  </div>
</div>

<?php
$script = <<<JS

$.getJSON("http://"+raspi_ip+":6900/?reset_massa",function(result){

  if (result.status == 1){
    alert("Berhasil me-Reset massa!");
  } else {
    alert("Gagal me-Reset massa!");
  }
});
var operator = "$jsonObj->nama_operator"
if (operator != "") {
  var operator = operator.split(",")
  for (var i = 0; i < operator.length; i++) {
    $('#nama_operator').tagsinput('add', operator[i]);
  }
}


if (syarat_kemas == 'LIQUID C') {
  document.getElementById('btsBawah').style.display = 'none'
}

document.getElementsByClassName('hl')[0].style.bottom = (threshold/maxValue*100)+"%";
// /Syntax untuk handle button input manual
document.getElementById('btnInputManual').onclick = function(){
  var currUrl = window.location.href;
  var url = new URL(currUrl);
  var obj = url.searchParams.get("obj");
  window.location = dns+"index.php?r=bulk-sisa/input-massa&obj="+obj
}
// Hingga Line di Atas

JS;
$this->registerJs($script);
?>
