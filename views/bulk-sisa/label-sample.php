<?php
use yii\helpers\Url;
use yii\helpers\Html;
use Da\QrCode\QrCode;

?>

<?php for ($i=0; $i < $jsonObj->totalStorage; $i++) { ?>
  <div class="table-responsive" style="width:100%!important;padding:0px">
    <table class="table" style="border:2px solid black;margin:0px;width:100%!important;padding:0px">
      <tbody>
        <tr>
          <td style="border:none;border-bottom:2px solid black;text-align:center;padding:0" colspan="2">
            <span class="title-text"><b>LABEL SAMPLING</b></span>
          </td>
        </tr>
        <tr>
          <td style="border:2px solid black;text-align:center;vertical-align:middle;padding:0px;width:50%" rowspan="3">
            <?php
            $path = Url::base(true)."/qr_suratjalan/".preg_replace('/[^A-Za-z0-9\-]/', '', $jsonObj->nomo).".png";
            // Use get_headers() function
            $headers = @get_headers($path);

            // Use condition to check the existence of URL
            if(!($headers && strpos( $headers[0], '200'))) {
              $qrCode = (new QrCode($jsonObj->nomo))
                  ->setSize(300)
                  ->setMargin(0)
                  ->useForegroundColor(0, 0, 0);
              $qrCode->writeFile('/var/www/html/flowreport/web/qr_sample_bulk/'. preg_replace('/[^A-Za-z0-9\-]/', '', $jsonObj->nomo).'.png');
            }
            echo "
              <img src='".$path."' style='width:100%;'><br>
            ";
            ?>
          </td>
        </tr>
        <tr>
          <td style="width:100%">
                <span class="data-text"><b>No. MO</b></span><br>
                <span class="content-text"><b><?= $jsonObj->nomo; ?></b></span><br>
                <span class="data-text"><b>Nama Bulk</b></span><br>
                <span class="content-text"><b><?= $jsonObj->nama_bulk; ?></b></span><br>
                <span class="data-text"><b>No. Batch</b></span><br>
                <span class="content-text"><b><?= $jsonObj->no_batch_awal; ?>X </b></span><br>
                <span class="data-text"><b>No. Storage</b></span><br>
                <span class="content-text"><b><?= ($i+1); ?> / <?= $jsonObj->totalStorage; ?> </b></span>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
<?php } ?>
