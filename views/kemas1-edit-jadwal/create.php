<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Kemas1EditJadwal */

$this->title = 'Create Kemas1 Edit Jadwal';
$this->params['breadcrumbs'][] = ['label' => 'Kemas1 Edit Jadwals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kemas1-edit-jadwal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
