<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kemas1EditJadwal */

$this->title = 'Update Kemas1 Edit Jadwal: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kemas1 Edit Jadwals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kemas1-edit-jadwal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
