<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kemas1EditJadwalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kemas1-edit-jadwal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'snfg_komponen') ?>

    <?= $form->field($model, 'lanjutan') ?>

    <?= $form->field($model, 'start') ?>

    <?= $form->field($model, 'stop') ?>

    <?php // echo $form->field($model, 'jenis_kemas') ?>

    <?php // echo $form->field($model, 'nama_line') ?>

    <?php // echo $form->field($model, 'nama_operator') ?>

    <?php // echo $form->field($model, 'jumlah_realisasi') ?>

    <?php // echo $form->field($model, 'is_done') ?>

    <?php // echo $form->field($model, 'start_id') ?>

    <?php // echo $form->field($model, 'stop_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
