<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Scandit Web SDK</title>

    <!-- Add the library, as explained on http://docs.scandit.com/stable/web/index.html -->
    <script src="https://cdn.jsdelivr.net/npm/scandit-sdk@4.x"></script>

    <style>
        body {
            margin: 0;
            padding: 0;
            background-color: black;
            color: white;
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
            align-items: center;
            font-size: 3vh;
            font-family: 'Open Sans', sans-serif;
            width: 100vw;
            height: 100vh;
        }

        #scandit-barcode-picker {
            width: 100%;
            height: 100%;
            max-height: 70vh;
        }

        #scandit-barcode-result {
            display: flex;
            justify-content: center;
            align-items: center;
            flex: 1;
            width: 100%;
        }

        button,
        button:active,
        button[disabled]:hover {
            --webkit-appearance: none;
            border: none;
            border-radius: none;
            outline: none;
            font-size: 0.7em;
            padding: 0.4em 0.6em;
            margin: 20px;
            text-transform: uppercase;
            font-family: 'Open Sans', sans-serif;
            background: #2ec0cc;
            color: white;
        }

        button:hover {
            background: #30d0d8;
        }

        button[disabled] {
            opacity: 0.4;
        }

        #continue-scanning-button {
            margin-bottom: 10vh;
        }
    </style>
</head>

<body>
    <!-- Containers for the picker and the results -->
    <div id="scandit-barcode-picker"></div>
    <div id="scandit-barcode-result">No codes scanned yet</div>
    <!-- Button to continue scanning after a barcode was scanned -->
    <button id="continue-scanning-button" onclick="continueScanning()">Continue Scanning</button>

    <script>
        // Helper function called when the "Continue Scanning" button is clicked
        function continueScanning() {
            if (picker) {
                continueButton.disabled = true;

                // Resume scanning
                picker.resumeScanning();
            }
        }

        // Configure the library and activate it with a license key
        const licenseKey = "AYSt3jkWJxdqE5r5JQ5oZI5EHqjiC0LJfGz1UgocVKYcOJz8nkFr5XxPLc7OXRAesDTx9pQocX8idFDl6iI6C+5XXKU4R6H6KkIswGYE6r+AH2qAmFreFM4iZMK/ea6mQiu7nfY3ZqRSKzJuJ6P5A6rbimbjI2Q9AMNK71Y4Sh7daILOx3y0N3Xak+34lAn10z319ljq2HVu0nm40QlxYDCSvSjZaYE/zu8AKZZktoeZq4/xkRxnrWGzsosXSSmbTJfOQ0Uoogp4P4tIEssLJ7uo/KBaF75+Q91AivadqaiJDK7C+vxcmSKlV/9XWqSrcOvVt1IRohuV03KlELIlOJ0Mn4GsHrGXEb1X11RVT1vVvsiIFX/cmGLX/WMUm5i3C0HBEuxFRDYXTrx63RADOQ6wCQCzPi7lMb6PKPVmAdCOYQ4OfMnkuinq1Yvz9BurezOwt9sOy73jOc8Eo1zyxiyC0FxwbKiQJO49ZInRAfknUS8TF8pf5H2ObJt1aPq9poSRqkomTAH+e3sZELh99AgZtsiXHRI8aVm+o2ArLJDGsVO29DBGncMToyW/rq1Jtd4dgSQkg5uDLhPTI1sve2U96RbcTkY2mQ8b/S30RCFOL/y72jA6AKDzgCsFiYpFbCy1Mnf0T46fUVwkrOUj/MPN29N/neaQ16pvdWxTkWSiAmZJuAc5AUAh9Sd48qH+Dja/YHl8ylxixTyckQcjMdA9tSxT0aQ+V6kQlvxjoFGCuu0BxguAlQr3FZHe8uMphksIlNbdhFoktktrt173PyWZtuunBo0wX8kgC3Oi+XOdosCg2pmHFhWKll5w";

        // Configure the engine location, as explained on http://docs.scandit.com/stable/web/index.html
        // const engineLocation = "build"; // the folder containing the engine
        // or, if using a CDN:
        const engineLocation = "https://cdn.jsdelivr.net/npm/scandit-sdk@4.x/build"

        ScanditSDK.configure(licenseKey, { engineLocation: engineLocation });

        const scannerContainer = document.getElementById("scandit-barcode-picker");
        const resultContainer = document.getElementById("scandit-barcode-result");
        const continueButton = document.getElementById("continue-scanning-button");
        continueButton.disabled = true;
        continueButton.hidden = true;
        let picker;

        // Create & start the picker
        ScanditSDK.BarcodePicker.create(scannerContainer, {
                playSoundOnScan: true,
                vibrateOnScan: true
            })
            .then(barcodePicker => {
                picker = barcodePicker;
                // Create the settings object to be applied to the scanner
                const scanSettings = new ScanditSDK.ScanSettings({
                    enabledSymbologies: ["ean8", "ean13", "upca", "upce", "code128", "code39", "code93",
                        "itf","qr"
                    ],
                    codeDuplicateFilter: 1000
                });
                picker.applyScanSettings(scanSettings);

                // If a barcode is scanned, show it to the user and pause scanning
                // (scanning is resumed when the user clicks "Continue Scanning")
                picker.on("scan", scanResult => {
                    // continueButton.hidden = false;
                    // continueButton.disabled = false;
                    // // picker.pauseScanning();
                    // resultContainer.innerHTML = scanResult.barcodes.reduce((string, barcode) =>
                    //     string +
                    //     `${ScanditSDK.Barcode.Symbology.toHumanizedName(barcode.symbology)}: ${barcode.data}<br>`,
                    //     '');
                    // alert(barcode.data);
                    // alert(`${ScanditSDK.Barcode.Symbology.toHumanizedName(barcode.symbology)}: ${barcode.data}`);
                    alert(scanResult.barcodes.reduce(function(string, barcode) {
                        return string + ScanditSDK.Barcode.Symbology.toHumanizedName(barcode.symbology) + ": " + barcode.data + "\n";
                      }, ""));
                });
                picker.on("scanError", error => {
                    console.error(error.message);
                });
                picker.resumeScanning();
            })
            .catch(error => {
                alert(error);
            });
    </script>
</body>

</html>
