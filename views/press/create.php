<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Press */
?>
<div class="press-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
