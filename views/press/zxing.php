<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Press */
?>



<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="ZXing for JS">

  <title>ZXing TypeScript | Decoding from camera stream</title>

  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">
</head>

<body>

      <div>
<!--         <button id="startButton">Switch</button>
        <button id="resetButton">Reset</button> -->
      </div>

      <div id="sourceSelectPanel" style="display:none">
        <label for="sourceSelect">Change video source:</label>
        <select id="sourceSelect" style="max-width:400px">x
        </select>
      </div>

      <div>
        <video id="video" width="330" height="300" style="border: 0.5px solid gray"></video>
      </div>

      

      <label>Resultador:</label>
      <pre><code id="result"></code></pre>

      <p>See the <a href="https://github.com/zxing-js/library/tree/master/docs/examples/multi-camera/">source code</a>
        for this example.</p>



  <script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script>
  <script type="text/javascript">
    window.addEventListener('load', function () {
      let selectedDeviceId;
      const codeReader = new ZXing.BrowserMultiFormatReader()
      console.log('ZXing code reader initialized')
      codeReader.getVideoInputDevices()
        .then((videoInputDevices) => {
          const sourceSelect = document.getElementById('sourceSelect')
          selectedDeviceId = videoInputDevices[1].deviceId
          if (videoInputDevices.length >= 1) {
            videoInputDevices.forEach((element) => {
              const sourceOption = document.createElement('option')
              sourceOption.text = element.label
              sourceOption.value = element.deviceId
              sourceSelect.appendChild(sourceOption)
            })

            sourceSelect.onchange = () => {
              selectedDeviceId = sourceSelect.value;

              codeReader.reset()
              codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
              if (result) {
               //  console.log(result)
               // document.getElementById('result').textContent = result.text
               alert(result);
              }
              // if (err && !(err instanceof ZXing.NotFoundException)) {
              //   console.error(err)
              //   document.getElementById('result').textContent = err
              // }
              })

            };

            const sourceSelectPanel = document.getElementById('sourceSelectPanel')
            sourceSelectPanel.style.display = 'block'
          }

          
          codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
              if (result) {
                // console.log(result)
                alert(result);
                // document.getElementById('result').textContent = result.text
              	
              }
              // if (err && !(err instanceof ZXing.NotFoundException)) {
              //   console.error(err)
              //   document.getElementById('result').textContent = err
              // }
           })

          // document.getElementById('startButton').addEventListener('click', () => {
          //   codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
          //     if (result) {
          //       console.log(result)
          //       document.getElementById('result').textContent = result.text
          //     	alert(result);
          //     }
          //     if (err && !(err instanceof ZXing.NotFoundException)) {
          //       console.error(err)
          //       document.getElementById('result').textContent = err
          //     }
          //   })
          //   console.log(`Started continous decode from camera with id ${selectedDeviceId}`)
          // })

          // document.getElementById('resetButton').addEventListener('click', () => {
          //   codeReader.reset()
          //   document.getElementById('result').textContent = '';
          //   console.log('Reset.')
          // })

        })
        .catch((err) => {
          console.error(err)
        })
    })
  </script>

</body>

</html>
