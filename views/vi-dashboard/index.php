<?php
use yii\helpers\Url;

?>
<div class="dashboard-index">
    <h1>HUBLAAA</h1>
    <input type="text" id="input_json" value="start">
    <input type="text" id="jalur_kemas_json" value="TUP06">

    <form action="<?= Url::base() ?>/dashboard/inspect/start" method="POST">
        <input type="text" id="id" name="id">
        <input type="text" id="kode_snfg" name="kode_snfg">
        <input type="text" id="kode_sku" name="kode_sku">
        <input type="text" id="jalur_kemas" name="jalur_kemas">
    </form>
    
    <?php
    $this->registerJsFile("@web/js/checkInput.js",[
        'depends' => [
            \yii\web\JqueryAsset::class,
        ]
    ]);
    ?>
</div>