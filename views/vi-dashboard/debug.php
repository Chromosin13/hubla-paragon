<div class="dashboard-debug">
    <h3>This is Bar Chart</h3>
    <a class="btn btn-secondary" id="a1" download="file.csv" onclick="getJsonInspectionResult()">Download CSV</a>
    <a class="btn btn-secondary" id="a2" download="file.csv" onclick="getDebug()">Download CSV</a>
</div>

<?php
$this->registerJsFile("@web/js/debug.js",[
    'depends' => [
        \yii\web\JqueryAsset::class,
    ]
]);
?>