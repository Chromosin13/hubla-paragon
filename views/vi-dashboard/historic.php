<?php
use yii\helpers\Url;

$value = 10;

?>
<div class="dashboard-history">
    <input type="text" id="id" value="<?= $product['schedule']['id']; ?>">
    <?= $productCard; ?>
    <?= $setPeriodChart; ?>

    <canvas id="current-chart"></canvas>
    <!-- <div class="row mb-1">
        <div class="col card m-1" style="border: none; text-align:center; width: 17rem;">
            <div class="card-body">
                <h5 class="card-title">
                    <a class="card-block stretched-link text-decoration-none" href="<?= Url::base(); ?>/navigation">
                        <svg xmlns="http://www.w3.org/2000/svg" width="90" height="90" fill="currentColor" class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                        <path d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"/>
                        </svg>
                    </a>
                </h5>
            </div>
        </div>
        <div class="col card m-1" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">SNFG</h5>
                <h6 class="card-subtitle mb-2 text-muted"><?= $product['info']['kode_sku']; ?></h6>
            </div>
        </div>
        <div class="col card m-1" style="width: 18wem;">
            <div class="card-body">
                <h5 class="card-title">Merek</h5>
                <h6 style="text-align: justify;" class="card-subtitle mb-2 text-muted"><?= $product['info']['merek']; ?></h6>
            </div>
        </div>
        <div class="col card m-1" style="width: 18wem;">
            <div class="card-body">
                <h5 class="card-title">Tipe</h5>
                <h6 style="text-align: justify;" class="card-subtitle mb-2 text-muted"><?= $product['info']['tipe']; ?></h6>
            </div>    
        </div>
        <div class="col card m-1" style="width: 18wem;">
            <div class="card-body">
                <h5 class="card-title">Varian</h5>
                <h6 style="text-align: justify;" class="card-subtitle mb-2 text-muted">
                    <?php 
                    if (!empty($product['info']['varian'])) {
                        echo $product['info']['varian']; 
                    } else {
                        echo "Tidak Memiliki Informasi varian";
                    }
                    ?>    
                </h6>
            </div>
        </div>
        <div class="col card m-1" style="width: 18wem;">
            <div class="card-body">
                <h5 class="card-title">Netto</h5>
                <h6 style="text-align: justify;" class="card-subtitle mb-2 text-muted">
                    <?php 
                    if (!empty($product['info']['netto'])) {
                        echo $product['info']['netto']; 
                    } else {
                        echo "Tidak Memiliki Informasi Netto";
                    }
                    ?>
                </h6>
            </div>
        </div>
    </div>
    
    <div class="row mb-1">
        <div class="col card m-1">
            <div class="card-body">
                <h5 class="card-title">Waktu Mulai Inspeksi</h5>
                <h6 style="text-align: justify;" class="card-subtitle mb-2 text-muted">
                    <?= $product['schedule']['start_time']; ?>
                </h6>                
            </div>
        </div>

        <div class="col card m-1">
            <div class="card-body">
                <h5 class="card-title">Waktu Selesai Inspeksi</h5>
                <h6 class="card-subtitle mb-2 text-muted">
                    <?= $product['schedule']['end_time']; ?>
                </h6>                
            </div>
        </div>

        <div class="col card m-1">
            <div class="card-body">
                <h5 class="card-title">Jumlah Defect Kemasan Sekunder</h5>
                <h6 class="card-subtitle mb-2 text-muted">
                    <?= $product['schedule']['jumlah_defect_sekunder']; ?>
                </h6>                
            </div>
        </div>
        
        <div class="col card m-1">
            <div class="card-body">
                <h5 class="card-title">Jumlah Defect Kemasan Primer</h5>
                <h6 class="card-subtitle mb-2 text-muted">
                    0
                </h6>                
            </div>
        </div>
    </div>

    <input type="hidden" id="id" value="<?= $product['schedule']['id']; ?>">
    
    <div class="row mb-5">
        <div class="col card">
            <div class="card-body">
                <h5 class="card-title text-center">Hasil Inspeksi Visual</h5>
                <div class="m-3" id="chart_div"></div>
            </div>
        </div>
    </div> -->
</div>
<?php
$this->registerJsFile("@web/js/historicLineChart.js",[
    'depends' => [
        \yii\web\JqueryAsset::class,
    ]
]);
?>