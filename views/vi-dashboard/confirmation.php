<?php
use yii\helpers\Url;
?>
<div class="dashboard-confirmation">
    <h1>The Inspection Finish!</h1>
    <a class="btn btn-primary btn-lg btn-block" type="button" href="<?= Url::base(); ?>/input">OK</a>
</div>