<?php
use yii\helpers\Url;
?>
<div class="dashboard-inspect">
    <input type="hidden" id="id-inspection" value="<?= $product['schedule']['id']; ?>">
    <input type="hidden" id="snfg-inspection" value="<?= $product['schedule']['snfg']; ?>">
    <?= $productCard; ?>
    
    <?= $chart; ?>
    <?= $tracibilityTable; ?>
</div>

<?php
$this->registerJsFile($js,[
    'depends' => [
        \yii\web\JqueryAsset::class,
    ],
    'type' => 'module'
]);
?>