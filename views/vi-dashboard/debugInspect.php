<?php
use yii\helpers\Url;

$this->title = $product['schedule']['snfg'];
$this->params['breadcrumbs'][] = ['label' => 'Navigation', 'url' => ['/navigation']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="dashboard-debugInspect">
    <input type="hidden" id="id-inspection" value="<?= $product['schedule']['id']; ?>">
    <?= $productCard; ?>
    
    <?= $chart; ?>
    <?= $tracibilityTable; ?>

    <?= $stopInspection; ?>
</div>

<?php
$this->registerJsFile($js,[
    'depends' => [
        \yii\web\JqueryAsset::class,
    ]
]);
?>