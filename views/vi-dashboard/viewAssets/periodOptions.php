<div class="viewAssets-periodOptions">
    <div class="row">
        <div class="col-lg-6 col-xs-12" align="left">
            <div class="btn-group">
                <button type="button" class="btn btn-default" id="dropdown-menu">10 Minutes</button>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <!-- <span class="caret"></span> -->
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#" class="set-period" name="2 second">ALL DATA</a></li>
                    <li><a href="#" class="set-period" name="5 minutes">5 Minutes</a></li>
                    <li><a href="#" class="set-period" name="10 minutes">10 Minutes</a></li>
                    <li><a href="#" class="set-period" name="15 minutes">15 Minutes</a></li>
                </ul>
            </div>
        </div>
        
        <div class="col-lg-6 col-xs-12" align="right">
            <div class="btn-group">
                <button type="button" name="bar" class="chart-type btn btn-success">Bar Chart</button>
                <button type="button" name="line" class="chart-type btn btn-success">Line Chart</button>
          </div>
        </div>
    </div>
</div>