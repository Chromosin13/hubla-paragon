<?php
use yii\helpers\Url;
?>

<div class="viewAssets-stopInspection m-1">
    <h5><strong>
        [TOMBOL INI TIDAK AKAN ADA PADA FINAL PRODUK, HANYA UNTUK MENSIMULASIKAN MENGHENTIKAN PROSES INSPEKSI]
    </strong></h5>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-danger btn-lg btn-block" data-toggle="modal" data-target="#exampleModal">
        STOP INSPEKSI
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Apakah kamu yakin ingin menghentikan proses inspeksi?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer"> -->
                <form action="<?= Url::base(); ?>/dashboard/inspect/end" method="POST">
                    <input type="text" name="id" id="id_json" value="<?= $product['schedule']['id']; ?>">
                    <input type="text" name="jumlah_defect_sekunder" id="jumlah_defect_sekunder_buffer">
                    <button class="btn btn-danger" data-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-success">Ya, saya yakin!</button>
                </form>

            </div>
            </div>
        </div>
    </div>
</div>