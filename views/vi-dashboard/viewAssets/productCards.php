<div class="viewAssets-productCards m-1">
    <input type="hidden" id="id" value="<?= $product['schedule']['id']; ?>">
    <div class="row mb-1">
        <div class="col-lg-3 col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3><strong>
                        <?= $product['schedule']['snfg']; ?>
                    </strong></h3>
                </div>
                <div class="box-body">
                    <h4 class="card-subtitle mb-2 text-muted">Kode SNFG</h4>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-12">
            <div class="col box box-primary">
                <div class="box-header">
                    <h3><strong>
                        <?= $product['info']['nama_fg']; ?>
                    </strong></h3>
                </div>
                <div class="box-body">
                    <h4 class="card-subtitle mb-2 text-muted">Deskripsi Produk</h4>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-12">
            <div class="col box box-primary">
                <div class="box-header">
                    <h3><strong>
                        <?= $product['schedule']['nama_line']; ?>
                    </strong></h3>
                </div>
                <div class="box-body">
                    <h4 class="card-subtitle mb-2 text-muted">Jalur Kemas</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-1">
        <div class="col-lg-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3><strong>
                        <?php
                            $datetime = strtotime($product['schedule']['datetime_start']);
                            echo date('l, d F Y H:i:s', $datetime);
                        ?>
                    </strong></h3>
                </div>
                <div class="box-body">
                    <h4 class="card-subtitle mb-2 text-muted">Waktu Mulai Inspeksi</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-1">
        <div class="col-lg-4 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua">
                    <i class="fa fa-search"></i>
                </span>

                <div class="info-box-content">
                    <span class="info-box-text">
                        <h5 class="info-box-number">
                            <strong>
                                <span class="info-box-number" id="jumlah-inspeksi-disp">0</span>
                            </strong>
                        </h5>
                    </span>
                    <h4>Jumlah Inspeksi</h4>    
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua">
                    <i class="fa fa-cube"></i>
                </span>

                <div class="info-box-content">
                    <span class="info-box-text">
                        <div>
                            <h5>
                                <strong>
                                    <span class="info-box-number" id="jumlah-defect-sekunder-disp">0</span>
                                </strong>
                            </h5>
                        </div>

                    </span>
                    <h4>Defect Kemasan Sekunder</h4>    
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua">
                    <i class="fa fa-cube"></i>
                </span>

                <div class="info-box-content">
                    <span class="info-box-text">
                        <h5><strong>
                            <span class="info-box-number" id="jumlah_defect_primer_disp">Coming Soon</span>
                        </h5></strong>
                    </span>
                    <h4>Defect Kemasan Primer</h4>    
                </div>
            </div>
        </div>
    </div>
</div>