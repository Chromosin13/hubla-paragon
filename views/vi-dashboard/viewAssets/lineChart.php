<div class="viewAssets-lineChart">
    <div class="box box-default collapsed-box">
        <div class="box-header with-border" align="left">
            <h3 class="col box-title">Line Chart</h3>
    
            <div class="col box-tools pull-right">
                <button type="button" class="btn btn-box-tool btn-block" data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
    
        <div class="box-body">
            <div>
                <?= $setPeriodChart; ?>
            </div>
            <input type="hidden" id="chart" value="line">
            <div class="chart">
                <canvas id="current-chart"></canvas>
            </div>
        </div>
    </div>
</div>
