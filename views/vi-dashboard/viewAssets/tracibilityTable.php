<div class="viewAssets-tracibilityTable">
    <div class="box box-default collapsed-box">
        <div class="box-header with-border" align="left">
            <h3 class="col box-title">Tabel Ketelusuran</h3>
    
            <div class="col box-tools pull-right">
                <button type="button" class="btn btn-box-tool btn-block" data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
    
        <div class="box-body">
            <table width="100%" id="tracibility-table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th scope="col">Periode Inspeksi</th>
                        <th scope="col">Jumlah Defect</th>
                        <th scope="col">Inspeksi Ke</th>
                        <th scope="col">Jenis Defect</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>