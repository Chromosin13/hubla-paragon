<div class="viewAssets-barChart">
    <div class="box box-default collapsed-box">
        <div class="box-header with-border" align="left">
            <h3 class="col box-title">Bar Chart</h3>
    
            <div class="col box-tools pull-right">
                <button type="button" class="btn btn-box-tool btn-block" data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
    
        <div class="box-body">
            <div>
                <?= $setPeriodChart; ?>
            </div>
            <input type="hidden" id="chart" value="bar">
            <div class="chart">
                <canvas id="current-bar-chart"></canvas>
            </div>
        </div>
    </div>
</div>
