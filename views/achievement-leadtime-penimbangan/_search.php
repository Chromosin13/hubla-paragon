<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLeadtimePenimbanganSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="achievement-leadtime-penimbangan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nama_line') ?>

    <?= $form->field($model, 'nomo') ?>

    <?= $form->field($model, 'koitem_bulk') ?>

    <?= $form->field($model, 'nama_bulk') ?>

    <?php // echo $form->field($model, 'nama_fg') ?>

    <?php // echo $form->field($model, 'jenis_penimbangan') ?>

    <?php // echo $form->field($model, 'real_leadtime') ?>

    <?php // echo $form->field($model, 'standard_leadtime') ?>

    <?php // echo $form->field($model, 'nama_operator') ?>

    <?php // echo $form->field($model, 'achievement') ?>

    <?php // echo $form->field($model, 'tanggal_stop') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
