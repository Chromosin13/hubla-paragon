<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLeadtimePenimbangan */

$this->title = 'Update Achievement Leadtime Penimbangan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Achievement Leadtime Penimbangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="achievement-leadtime-penimbangan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
