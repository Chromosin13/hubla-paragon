<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AchievementLeadtimePenimbangan */

$this->title = 'Create Achievement Leadtime Penimbangan';
$this->params['breadcrumbs'][] = ['label' => 'Achievement Leadtime Penimbangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-leadtime-penimbangan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
