<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kemas1 */

$this->title = 'Update Kemas1: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kemas1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kemas1-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_update', [
        'model' => $model,
        'modelsKendala' => $modelsKendala,
    ]) ?>

</div>
