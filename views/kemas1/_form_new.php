<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
use faryshta\widgets\JqueryTagsInput;
use kartik\checkbox\CheckboxX;
use app\models\ScmPlanner;
use yii\helpers\ArrayHelper;
use app\models\Kemas1;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\web\JsExpression;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\Kemas1 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kemas1-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <!-- Left Side -->
    <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-green">
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username"><b>KEMAS 1</b></h2>
              <h5 class="widget-user-desc">INPUT</h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                
                <!-- Content Begins Here  -->

                    <!-- Isi SNFG -->
                    <div id="form_snfg_komponen" class="box-header with-border">
                            <?= $form->field($model, 'snfg_komponen',  ['hintType' => ActiveField::HINT_SPECIAL, 
                                                                    'addon' =>  [
                                                                                'prepend' => [
                                                                                    'content'=>'<i class="fa fa-barcode"></i>'
                                                                                ],
                                                                                'append' => [
                                                                                    'content' => Html::button('Tekan Setelah Scan',['class'=>'btn btn-primary','id'=>'afterScnBtnKomponen']),
                                                                                    'asButton' => true
                                                                                ]
                                                                    ]
                        ])->textInput(['placeholder'=>'Klik Disini, Scan pada Barcode SNFG Komponen yang tertera di SPK'])->hint('PASTIKAN CAPS LOCK NON AKTIF');
                        ?>
                    </div>

                    <!-- Planner Info -->
                    <div id="planner-information">
                        <div class="box-header with-border">
                                
                                <h2 class="box-title"> <b>Planner Information</b></h2>
                                <br \>
                        </div>


                        <div class="box-header with-border">
                            <table id="table-snfg" class="table table-bordered">
                                <tbody><tr>
                                  <th>NOMOR MO</th>
                                  <th>STREAMLINE</th>
                                  <th>START</th>
                                  <th>DUE</th>
                                  <th>BESAR BATCH</th>
                                  <th>BESAR LOT</th>
                                  <th>LOT KE</th>
                                  <th>KODE JADWAL</th>
                                  <th>STATUS</th>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                    <!-- Last Position -->
                    <div id="last-position">

                            <div class="box-header with-border">
                                    
                                    <h2 class="box-title"> <b>Last Position</b></h2>
                                    <br \>
                            </div>


                            <div class="box-header with-border">


                                <table id="table-last" class="table table-bordered">
                                    <tbody><tr>
                                      <th style="width: 120px">SNFG Komponen</th>
                                      <th style="width: 50px">POSISI</th>
                                      <th>STATE</th>
                                      <th>JENIS PROSES</th>
                                      <th>LANJUTAN</th>
                                      <th style="width: 40px">IS DONE</th>
                                    </tr>
                                  </tbody>
                              </table>

                            </div>

                    </div>

                    <!-- Form Start Stop -->

                    <div class="box-header with-border" id="form_start_stop">
                            <a class="btn btn-success" id="start-button">
                                        <i class="fa fa-play"></i> Start
                            </a>

                            <a class="btn btn-danger" id="stop-button">
                                        <i class="fa fa-pause"></i> Stop
                            </a>
                            <p \>
                            <p \>
                            
                            <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>
                            
                            
                    </div>

                    <div class="box-header with-border" id="form_jenis">

                        <div id="lsb_validator">

                             <div id="kemas1-jenis_kemas_00">
                            <?= $form->field($model, 'jenis_kemas')->dropDownList(['PRESS' => 'PRESS','FILLING' => 'FILLING'],['prompt'=>'Select Option']); ?>
                            </div>

                            <div id="kemas1-jenis_kemas_0">
                            <?= $form->field($model, 'jenis_kemas')->textInput(['id'=>'kemas1-jenis_kemas_1', 'disabled'=>'true' , 'readonly' =>'true']); ?>
                            </div>


                        </div>

                    </div>


                <!-- Content Ends Here -->

              </ul>
            </div>


    </div>

    <div id="form_entry_start_stop">

            <div class="box" id="kemas1-start">
                    <div class="box-header with-border">
                      <h2 class="box-title">Start Entry</h2>


                        <!-- Nama Line DropDown -->
                        <?php

                        // echo $form->field($model, 'nama_line')->dropDownList(
                        //     ArrayHelper::map(Kemas1::find()->all()
                        //     ,'nama_line','nama_line')
                        //     ,['prompt'=>'Select Line']

                        // );

                        echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Kemas1::find()->all()
                            ,'nama_line','nama_line'),
                            'options' => ['placeholder' => 'Select Line'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);

                        ?>

                        <!-- Lanjutan Field -->
                        <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true'])?>

                        <!-- Nama Operator -->
                        <?php 

                            echo $form->field($model, 'nama_operator', ['hintType' => ActiveField::HINT_SPECIAL,'addon' => ['prepend' => ['content'=>'<i class="fa fa-barcode"></i>']]
                                ])->widget(TagsinputWidget::classname(), [
                                'clientOptions' => [
                                    'trimValue' => true,
                                    'allowDuplicates' => false,
                                    'maxChars'=> 4,
                                ]
                            ])->hint(' <b>Scan</b> : Klik pada isian dibawah kemudian Scan Barcode pada Nametag<p \> <b>Manual</b> : isi dengan Format 4 Huruf Kapital, Contoh : "ABCD" lalu diikuti dengan menekan Enter ');

                        ?>

                    </div>
            </div>

            <div class="box" id="kemas1-stop">
                    <div class="box-header with-border">
                      <h2 class="box-title">Stop Entry</h2>

                        <!-- Lanjutan -->
                        <?= $form->field($model, 'lanjutan')->textInput(['id'=>'kemas1-lanjutan_stop','readonly' => 'true']) ?>

                        <!-- Jumlah Realisasi -->
                        <?= $form->field($model, 'jumlah_realisasi')->textInput() ?>

                        <!-- Counter from Counter System -->
                        <?= $form->field($model, 'counter')->textInput(['readonly'=>true]) ?>

                        <!-- Kendala Checkbox -->
                        <label>
                          <input type="checkbox" id="is_kendala">
                          Terdapat Kendala ?
                        </label>

                        <!-- Kendala Form -->
                        <div id="kendala-form">
                            <?php DynamicFormWidget::begin([
                                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                'widgetBody' => '.container-items', // required: css class selector
                                'widgetItem' => '.item', // required: css class
                                'limit' => 3, // the maximum times, an element can be cloned (default 999)
                                'min' => 1, // 0 or 1 (default 1)
                                'insertButton' => '.add-item', // css class
                                'deleteButton' => '.remove-item', // css class
                                'model' => $modelsKendala[0],
                                'formId' => 'dynamic-form',
                                'formFields' => [
                                    'keterangan',
                                    'start',
                                    'stop',
                                ],
                            ]); ?>

                            <div class="container-items"><!-- widgetContainer -->
                                <?php foreach ($modelsKendala as $i => $modelKendala): ?>
                                <div class="item panel panel-default"><!-- widgetBody -->
                                    <div class="panel-heading">
                                        <h3 class="panel-title pull-left">Kendala</h3>
                                        <div class="pull-right">
                                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body">
                                        <?php
                                            // necessary for update action.
                                            if (! $modelKendala->isNewRecord) {
                                                echo Html::activeHiddenInput($modelKendala, "[{$i}]id");
                                            }
                                        ?>

                                        <div class="row">
                                            <div class="col-sm-3">

                                                <?= $form->field($modelKendala, "[{$i}]keterangan")->widget(Select2::classname(), [
                                                                    'data' => ArrayHelper::map(Kendala::find()->all()
                                                            ,'keterangan','keterangan'),
                                                                    'options' => ['placeholder' => 'Select Kendala'],
                                                                    'pluginOptions' => [
                                                                        // 'tags' => true,
                                                                        'allowClear' => true
                                                                    ],
                                                                    ]);
                                                ?>

                                            </div>
                                            <div class="col-sm-3">
                                                <?= $form->field($modelKendala, "[{$i}]start")->widget(TimePicker::classname(), [
                                                                'options' => ['placeholder' => 'Start'],
                                                                'pluginOptions' => [
                                                                        'minuteStep' => 5,
                                                                        'autoclose'=>true,
                                                                        'showMeridian' => false,
                                                                        'defaultTime' => '00:00'
                                                                ]
                                                ]); ?>
                                            </div>
                                        </div><!-- .row -->
                                    </div>
                                </div>
                                <?php endforeach; ?>
                                </div>
                                <?php DynamicFormWidget::end(); ?>
                        </div>

                        <p \>

                        <!-- Is Done Checkbox -->
                        <div id="is_done-form">
                        <?php
                            echo '<label class="cbx-label" for="is_done">Is Done?</label>';
                            echo CheckboxX::widget([
                                'model' => $model,
                                'attribute' => 'is_done',
                                'pluginOptions' => [
                                    'threeState' => false,
                                    'size'=>'sm'
                                ]
                            ]);
                        ?>
                        </div>
                    </div>
            </div>

            <!-- Validate Button -->
            <div class="form-group" id="create-button">
                <?php echo Dialog::widget([
                        'libName' => 'krajeeDialogCust', // a custom lib name
                        'options' => [  // customized BootstrapDialog options
                            'size' => Dialog::SIZE_LARGE, // large dialog text
                            'type' => Dialog::TYPE_WARNING, // bootstrap contextual color
                            'title' => 'Konfirmasi Input',
                            'buttons' => [
                                [
                                    'id' => 'btn-ok',
                                    'icon' => 'glyphicon glyphicon-check',
                                    'label' => 'Sudah Benar, Submit!',
                                    'cssClass' => 'btn-primary',
                                    'action' => new JsExpression("function() {
                                        $('#submitButton').submit();            
                                        }")
                                    // Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
                                ],
                            ]
                        ]
                    ]);
                ?>
                <button type="button" id="btn-custom" class="btn btn-info">Validate</button>
                <button id="submitButton" type="submit" class="btn btn-success">Creates</button>
               <!--  <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?> -->
            </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

// Hide Submit Button untuk Konfirmasi Input
$('#submitButton').hide();

$('#kemas1-snfg_komponen').keypress(function(e) { 
    var s = String.fromCharCode( e.which );
    if ( s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey ) {
        alert('caps is on');
    }
});


// Inisialisasi Form Hide
    $('#start-button').hide();
    $('#stop-button').hide(); 
    $('#form_start_stop').hide(); 
    $('#form_jenis').hide(); 
    $('#form_entry_start_stop').hide();

// After Klik Tombol Tekan Scan
    // Klik Scan Komponen
    // $('#afterScnBtnKomponen').click(function(){
    //     $('#form_start_stop').show();  
    // });


    $('#afterScnBtnKomponen').click(function(){

        var snfg_komponen = $('#kemas1-snfg_komponen').val();

        $.get('http://10.5.4.203/ptidms/php_action/test_get_redha.php',{ snfg : snfg_komponen },function(data){
                var data = $.parseJSON(data);
                var qtyaktual = data.qtyaktual;
                alert(qtyaktual);
                
                $('#kemas1-counter').attr('value',data.qtyaktual);

        });


        if(snfg_komponen==""){
            $('#form_start_stop').hide();    
        }
        else {
            $('#form_start_stop').show(); 
        }
    });

// After Klik Start Stop Button

    $('#start-button').click(function(){
        $('#form_jenis').show();
    });

    $('#stop-button').click(function(){
        $('#form_jenis').show();
        $('#form_entry_start_stop').show();
    });

// After Select Jenis Kemas

    $('#kemas1-jenis_kemas').change(function(){
        var jenis_kemas = $('#kemas1-jenis_kemas').val();
        if(jenis_kemas==""){
            $('#form_entry_start_stop').hide();
        } else {
            $('#form_entry_start_stop').show();
            var state = $('#kemas1-state').val();
            if(state=="START"){
                $('#create-button').hide();
                $('#kemas1-nama_line').change(function(){
                    var nama_line = $('#kemas1-nama_line').val();

                    $.get('index.php?r=kemas1/check-line',{ nama_line : nama_line },function(data){
                    var data = $.parseJSON(data);
                        
                        if(nama_line=="Select Line"){
                            $('#create-button').hide();
                        }else if(data.state=="START"){
                            alert('Mesin ini belum di STOP oleh nomor jadwal '+data.snfg_komponen);
                            $('#create-button').hide();
                        }else{
                            $('#create-button').fadeIn("slow");
                        }

                    });
                });
            }
        }

    });

// Validate Button

$("#btn-custom").on("click", function() {
    var snfg_komponen = $('#kemas1-snfg_komponen').val();
    var state = $('#kemas1-state').val();
    var jenis_kemas = $('#kemas1-jenis_kemas').val();
    var jenis_kemas_1 = $('#kemas1-jenis_kemas_1').val();
    var lanjutan = $('#kemas1-lanjutan').val();
    var lanjutan_stop = $('#kemas1-lanjutan_stop').val();
    var nama_line = $('#kemas1-nama_line').val();
    var nama_operator =  $('#kemas1-nama_operator').val();
    var jumlah_realisasi = $('#kemas1-jumlah_realisasi').val();   
    var is_done = $('#kemas1-is_done').val();

    if(lanjutan==null||lanjutan_stop==null){
            alert('Penarikan data Lanjutan Gagal, Mohon refresh halaman dan scan ulang jadwal');
    }else if(state=="START"){
        krajeeDialogCust.dialog('<b>SNFG Komponen</b><p>' + '<font color="blue"><i>'+(snfg_komponen)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Kemas</b><p>' + '<font color="blue"><i>'+(jenis_kemas)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>' +
                                '<b>Nama Line</b><p>' + '<font color="blue"><i>'+(nama_line)+'</i></font><p>' +  
                                '<b>Nama Operator</b><p>' + '<font color="blue"><i>'+(nama_operator)+'</i></font><p>',function(result) {}
        );
    }else if(state=="STOP"&&is_done==1){
    
        krajeeDialogCust.dialog('<b>SNFG Komponen</b><p>' + '<font color="blue"><i>'+(snfg_komponen)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Kemas</b><p>' + '<font color="blue"><i>'+(jenis_kemas_1)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>'+
                                '<b>Jumlah Realisasi</b><p>' + '<font color="blue"><i>'+(jumlah_realisasi)+'</i></font><p>' + 
                                '<b><font color="blue"><i>Jadwal Sudah Selesai (Tidak Lanjut Shift)</i></font></b><p>',function(result) {}
        );

    }else if(state=="STOP"&&is_done==0){

        krajeeDialogCust.dialog('<b>SNFG Komponen</b><p>' + '<font color="blue"><i>'+(snfg_komponen)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Kemas</b><p>' + '<font color="blue"><i>'+(jenis_kemas_1)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>'+
                                '<b>Jumlah Realisasi</b><p>' + '<font color="blue"><i>'+(jumlah_realisasi)+'</i></font><p>' +
                                '<b><font color="blue"><i>Jadwal Akan Dilanjutkan ke Shift Berikutnya (Belum Done)</i></font></b><p>' ,function(result) {}
        );

    }

});


$('#is_kendala').change(function(){

    if(document.getElementById('is_kendala').checked){
            $('#kendala-form').show();

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);

                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = false;
                    document.getElementById("kendala-"+(i)+"-start").disabled = false;
        
                    i++;
                }

    }else{
            $('#kendala-form').hide();

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);
                    
                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = true;
                    document.getElementById("kendala-"+(i)+"-start").disabled = true;  

                    i++;
                }

    } 
});


$('#kemas1-is_done').change(function(){
    var data = $(this).val();;
    if(data==1){
        $('#batch_split-form').hide();
    }
    else if(data==0){
        $('#batch_split-form').show();
    }
});

$('#kemas1-batch_split').change(function(){
    var data = $(this).val();;
    if(data==1){
        $('#is_done-form').hide();
    }
    else if(data==0){
        $('#is_done-form').show();
    }
});

$('#kemas1-snfg_komponen').change(function(){
    var snfg_komponen = $(this).val();

    // Assign Get Kendala Kemas
    $.post("index.php?r=scm-planner/get-kendala-kemas-komponen&snfg_komponen="+$('#kemas1-snfg_komponen').val(), function (data){
            $("select#kendala-0-keterangan").html(data);
    });

    $.post("index.php?r=scm-planner/get-line-kemas-komponen&snfg_komponen="+$('#kemas1-snfg_komponen').val(), function (data){
        $("select#kemas1-nama_line").html(data);
    });

    // $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
    //     var data = $.parseJSON(data);
    //     //alert(data.sediaan);
    //     $('#kemas1-snfg').attr('value',data.snfg);

    //     $('#streamline_results').html('STREAMLINE : '+data.streamline);
    //     $('#start_results').html('START : '+ data.start);
    //     $('#due_results').html('DUE : '+ data.due);
    //     $('#nama_bulk_results').html(data.nama_bulk);
    //     $('#nama_fg_results').html(data.nama_fg);
    //     $('#besar_batch_results').html('BESAR BATCH : '+ data.besar_batch);
    //     $('#besar_lot_results').html('BESAR LOT : '+ data.besar_lot);
    //     $('#lot_ke_results').html('LOT KE : '+ data.lot); ;
    //     $('#kode_jadwal_results').html('KODE JADWAL : '+ data.kode_jadwal);
    //     $('#jumlah_results').html('JUMLAH : '+ data.jumlah);


    // });

    $.get('index.php?r=scm-planner/get-planner-raw',{ snfg_komponen : snfg_komponen },function(data){
            var json = $.parseJSON(data);
            var tr;
            for (var i = 0; i < json.length; i++) {
                tr = $('<tr/>');
                tr.append("<td><span class='badge bg-grey'>" + json[i].nomo + "</span></td>");
                tr.append("<td><span class='badge bg-grey'>" + json[i].streamline + "</span></td>");
                tr.append("<td><span class='badge bg-grey'>" + json[i].start + "</span></td>");
                tr.append("<td><span class='badge bg-grey'>" + json[i].due + "</span></td>");
                tr.append("<td><span class='badge bg-grey'>" + json[i].besar_batch + "</span></td>");
                tr.append("<td><span class='badge bg-grey'>" + json[i].besar_lot + "</span></td>");
                tr.append("<td><span class='badge bg-grey'>" + json[i].lot_ke + "</span></td>");
                tr.append("<td><span class='badge bg-grey'>" + json[i].kode_jadwal + "</span></td>");
                tr.append("<td><span class='badge bg-green'>" + json[i].status + "</span></td>");
                $('#table-snfg').append(tr);
            }
            $('#planner-information').fadeIn("slow");
        });

    $.get('index.php?r=kemas1/get-kemas1-komponen',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#no-batch').attr('value',data.nobatch);
        $('#besar-batch-real').attr('value',data.besar_batch_real);
    });
    // $.get('index.php?r=scm-planner/get-ppr',{ snfg_komponen : snfg_komponen },function(data){
    //     var data = $.parseJSON(data);
    //     //alert(data.sediaan);
    //     $('#ppr-posisi').attr('value',data.posisi);
    //     $('#ppr-state').attr('value',data.state);
    //     $('#ppr-jenis-proses').attr('value',data.jenis_proses);
    //     $('#ppr-lanjutan').attr('value',data.lanjutan);
    //     $('#ppr-isdone').attr('value',data.is_done);
    // });

    $.get('index.php?r=scm-planner/get-ppr-raw',{ snfg_komponen : snfg_komponen },function(data){
        var json = $.parseJSON(data);
        var tr;
        for (var i = 0; i < json.length; i++) {
            tr = $('<tr/>');
            tr.append("<td><span class='badge bg-grey'>" + json[i].snfg_komponen + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].posisi + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].state + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].jenis_proses + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].lanjutan + "</span></td>");
            if(json[i].is_done=="Done"){
                tr.append("<td><span class='badge bg-green'>" + json[i].is_done + "</span></td>");
            }else if(json[i].is_done=="Not Done"){
                tr.append("<td><span class='badge bg-red'>" + json[i].is_done + "</span></td>");
            }
            $('#table-last').append(tr);
        }
        $('#last-position').fadeIn("slow");

    });

    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);

        if(data==null){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Not Found');
        }

        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#start-button').hide();
            $('#stop-button').hide();
            $('#istirahat-start-button').hide();
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Status ditetapkan sebagai HOLD/PAUSE dari Planner')
        }
        else{
                $.get('index.php?r=scm-planner/get-last',{ snfg_komponen : snfg_komponen },function(data){
                    var data = $.parseJSON(data);
                    if(data.posisi=="KEMAS 1" && data.state=="START"){
                        $('#start-button').hide();
                        $('#stop-button').show();

                        $('#kemas1-jenis_kemas_00').hide();
                        document.getElementById("kemas1-jenis_kemas").disabled = true;
                        $('#kemas1-jenis_kemas_0').show();
                        document.getElementById("kemas1-jenis_kemas_1").disabled = false;
                        $('#kemas1-jenis_kemas_1').attr('value',data.jenis_proses);

                        // Assign Lanjutan


                        $('#stop-button').click(function(){

                            var jenis_kemas = $('#kemas1-jenis_kemas_1').val();
                            var snfg_komponen = $('#kemas1-snfg_komponen').val();

                                $.get('index.php?r=kemas1/lanjutan-kemas1',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#kemas1-lanjutan').attr('value',data.lanjutan);
                                    $('#kemas1-lanjutan_stop').attr('value',data.lanjutan);


                                });

                        });


                        // EO Lanjutan


                    }else if(data.posisi=="KEMAS 1" && data.state=="STOP"){
                        $('#start-button').show();
                        $('#stop-button').hide();

                        $('#start-button').click(function(){

                            $('#kemas1-jenis_kemas').change(function(){

                                var jenis_kemas = $('#kemas1-jenis_kemas').val();
                                var snfg_komponen = $('#kemas1-snfg_komponen').val();

                                    $.get('index.php?r=kemas1/lanjutan-kemas1',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.sediaan);

                                        $('#kemas1-lanjutan').attr('value',data.lanjutan);
                                        $('#kemas1-lanjutan_stop').attr('value',data.lanjutan);



                                        });
                                });
                            });

                    }else if(data.posisi!="KEMAS 1" && data.state=="START"){
                        $('#start-button').hide();
                        $('#stop-button').hide();
                        alert('Jadwal sebelumnya belum di STOP');
                    }else if(data.posisi!="KEMAS 1" && data.state=="STOP" && data.is_done=="Not Done"){
                        $('#start-button').hide();
                        $('#stop-button').hide();
                        alert('Jadwal sebelumnya belum Is Done');
                    }else if(data.posisi!="KEMAS 1" && data!=null){
                        $('#start-button').fadeIn("slow");
                        $('#stop-button').hide();                
                    }
                });
            }
        });
});

$(function() {

    // DynamicForm override JS , Script Fix for non-working kartik date/timepicker afterInsert 
    // DynamicForm override JS , Fix for select2 kendala list


// DynamicForm override JS , Script Fix for non-working kartik date/timepicker afterInsert 
    // DynamicForm override JS , Fix for select2 kendala list

        // ID untuk penomoran nested dynamic form
        var id = 0;


        // Setelah Insert Button

        $(".dynamicform_wrapper").on('afterInsert', function(e, item) {
            var dateTimePickers = $(this).find('[data-krajee-kvtimepicker]');
            dateTimePickers.each(function(index, el) {
                $(this).parent().removeData().kvTimepicker('remove');
                $(this).parent().kvTimepicker(eval($(this).attr('data-krajee-kvtimepicker')));
            });
            

            // Override Fix for select2 kendala list 

            id += 1;
            console.log(id);


            $.post("index.php?r=scm-planner/get-kendala-kemas-komponen&snfg_komponen="+$('#kemas1-snfg_komponen').val(), function (data){

                    $("select#kendala-"+(id)+"-keterangan").html(data);

            });

        });

        // Setelah Delete Button

        $(".dynamicform_wrapper").on('afterDelete', function(e, item) {
            
            // Override Fix for select2 kendala list 

            id -= 1;
            console.log(id);

        });


        // Jika Mencapai Batas Maximum
        $(".dynamicform_wrapper").on('limitReached', function(e, item) {
            alert('Maximum 3');
        });


    // Hide and Disable DynamicFormWidget for Kendala Model //

        $('#kendala-form').hide();
            document.getElementById("kendala-0-keterangan").disabled = true;
            document.getElementById("kendala-0-start").disabled = true;
            


    $('#kemas1-jenis_kemas_0').hide(); //  Hide Jenis Proses Dropdown
    document.getElementById("kemas1-jenis_kemas_1").disabled = true; // Disable Jenis Proses auto-assign

    $('#kemas1-start').hide();
    $('#kemas1-stop').hide();
    $('#kemas1-istirahat-start').hide();
    $('#kemas1-istirahat-stop').hide();
    $('#start-button').click(function(){
            var start = "START";
            $('#kemas1-state').attr('value',start);
                    $('#kemas1-start').show();
                    $('#kemas1-stop').hide();

            $('#kemas1-jenis_kemas').change(function(){
                var jenis_kemas = $('#kemas1-jenis_kemas').val();
                var snfg_komponen = $('#kemas1-snfg_komponen').val();
                $.get('index.php?r=kemas1/lanjutan-kemas1',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);

                            $('#kemas1-lanjutan').attr('value',data.lanjutan);
                            $('#kemas1-lanjutan_stop').attr('value',data.lanjutan);

                        });
                $.get('index.php?r=kemas1/lanjutan-split-batch-kemas1',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas1-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas1-lanjutan_split_batch').attr('value',data.lanjutan_split_batch);
                            $('#kemas1-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                            $('#kemas1-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);
                        });
            });
        });

    $('#stop-button').click(function(){
             var stop = "STOP";
             $('#kemas1-state').attr('value',stop);
                    $('#kemas1-stop').show();
                    $('#kemas1-start').hide();

            $('#kemas1-jenis_kemas').change(function(){
                var jenis_kemas = $('#kemas1-jenis_kemas').val();
                var snfg_komponen = $('#kemas1-snfg_komponen').val();
                $.get('index.php?r=kemas1/lanjutan-kemas1',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);

                            $('#kemas1-lanjutan').attr('value',data.lanjutan);
                            $('#kemas1-lanjutan_stop').attr('value',data.lanjutan);
                        });
                $.get('index.php?r=kemas1/lanjutan-split-batch-kemas1',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                            var data = $.parseJSON(data);
                            $('#kemas1-lanjutan_split_batch_istirahat_start').attr('value',data.lanjutan_split_batch);
                            $('#kemas1-lanjutan_split_batch').attr('value',data.lanjutan_split_batch);
                            $('#kemas1-lanjutan_split_batch_stop').attr('value',data.lanjutan_split_batch);
                            $('#kemas1-lanjutan_split_batch_istirahat_stop').attr('value',data.lanjutan_split_batch);
                        });
            });
    });
});



JS;
$this->registerJs($script);
?>
