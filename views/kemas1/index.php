<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Kemas1Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kemas 1';
$this->params['breadcrumbs'][] = $this->title;
?>

    <p>
        <?= Html::a('Input Kemas 1', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


<div class="kemas1-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'snfg',
            'snfg_komponen',
            'jumlah_plan',
            'jumlah_realisasi',
            'nama_line',
            'nama_operator',
            'waktu',
            'state',
            'lanjutan',
            'jenis_kemas',
            'jumlah_operator',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
