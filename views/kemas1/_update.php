<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
use faryshta\widgets\JqueryTagsInput;
use kartik\checkbox\CheckboxX;
use app\models\ScmPlanner;
use yii\helpers\ArrayHelper;
use app\models\Kemas1;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\web\JsExpression;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\DateTimePicker;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Kemas1 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kemas1-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <!-- Left Side -->
    <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-green">
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username"><b>KEMAS 1</b></h2>
              <h5 class="widget-user-desc">EDIT JADWAL</h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                
                <!-- Content Begins Here  -->

                    <!-- Isi SNFG -->
                    <div id="form_snfg_komponen" class="box-header with-border">
                            <?= $form->field($model, 'snfg_komponen',  ['hintType' => ActiveField::HINT_SPECIAL, 
                                                                    'addon' =>  [
                                                                                'prepend' => [
                                                                                    'content'=>'<i class="fa fa-barcode"></i>'
                                                                                ]
                                                                    ]
                        ])->textInput(['readonly'=>true])->hint('PASTIKAN CAPS LOCK NON AKTIF');
                        ?>
                    </div>


                    <!-- Form Start Stop -->

                    <div class="box-header with-border" id="form_start_stop">
                            
                            <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>
                            
                            
                    </div>

                    <div class="box-header with-border" id="form_jenis">

                        <div id="lsb_validator">

                            <div id="kemas1-jenis_kemas_0">
                            <?= $form->field($model, 'jenis_kemas')->textInput(['id'=>'kemas1-jenis_kemas_1', 'readonly' =>'true']); ?>
                            </div>



                        </div>

                        <!-- Lanjutan -->
                        <?= $form->field($model, 'lanjutan')->textInput(['id'=>'kemas1-lanjutan_stop','readonly' => 'true']) ?>

                        <?= $form->field($model, 'edit_nama')->textInput(['readOnly'=>true,'value'=>Yii::$app->user->identity->username]) ?>

                        <?php
                            
                            date_default_timezone_set('Asia/Jakarta');

                            echo $form->field($model, 'edit_time')->textInput(['readonly' => 'true','value'=>date("Y-m-d H:i:s")])
                        ?>

                        <?php 
                            echo '<label class="control-label">Waktu</label>';
                            echo DateTimePicker::widget([
                                'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                                'model' => $model,
                                'attribute' => 'formatwaktu',
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'yyyy-mm-dd hh:ii:ss',
                                    'endDate' => $model->formatwaktu
                                ]
                            ]);
                        ?>

                    </div>

                    


                <!-- Content Ends Here -->

              </ul>
            </div>


    </div>

    <div id="form_entry_start_stop">

            <div class="box" id="kemas1-start">
                    <div class="box-header with-border">
                      <h2 class="box-title">Start Entry</h2>


                        <!-- Nama Line DropDown -->
                        <?php

                        echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Kemas1::find()->all()
                            ,'nama_line','nama_line'),
                            'options' => ['placeholder' => 'Select Line'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);

                        ?>


                        <!-- Nama Operator -->
                        <?php 

                            echo $form->field($model, 'nama_operator', ['hintType' => ActiveField::HINT_SPECIAL,'addon' => ['prepend' => ['content'=>'<i class="fa fa-barcode"></i>']]
                                ])->widget(TagsinputWidget::classname(), [
                                'clientOptions' => [
                                    'trimValue' => true,
                                    'allowDuplicates' => false,
                                    'maxChars'=> 4,
                                ]
                            ])->hint(' <b>Scan</b> : Klik pada isian dibawah kemudian Scan Barcode pada Nametag<p \> <b>Manual</b> : isi dengan Format 4 Huruf Kapital, Contoh : "ABCD" lalu diikuti dengan menekan Enter ');

                        ?>

                    </div>
            </div>

            <div class="box" id="kemas1-stop">
                    <div class="box-header with-border">
                      <h2 class="box-title">Stop Entry</h2>

                        

                        <!-- Jumlah Realisasi -->
                        <?= $form->field($model, 'jumlah_realisasi')->textInput() ?>

                        <!-- Counter from Counter System -->
                        <?= $form->field($model, 'counter')->textInput(['readonly'=>true]) ?>

                        <!-- Kendala Checkbox -->
                        <label>
                          <input type="checkbox" id="is_kendala">
                          Terdapat Kendala ?
                        </label>

                        <!-- Kendala Form -->
                        <div id="kendala-form">
                            <?php DynamicFormWidget::begin([
                                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                'widgetBody' => '.container-items', // required: css class selector
                                'widgetItem' => '.item', // required: css class
                                'limit' => 3, // the maximum times, an element can be cloned (default 999)
                                'min' => 1, // 0 or 1 (default 1)
                                'insertButton' => '.add-item', // css class
                                'deleteButton' => '.remove-item', // css class
                                'model' => $modelsKendala[0],
                                'formId' => 'dynamic-form',
                                'formFields' => [
                                    'keterangan',
                                    'start',
                                    'stop',
                                ],
                            ]); ?>

                            <div class="container-items"><!-- widgetContainer -->
                                <?php foreach ($modelsKendala as $i => $modelKendala): ?>
                                <div class="item panel panel-default"><!-- widgetBody -->
                                    <div class="panel-heading">
                                        <h3 class="panel-title pull-left">Kendala</h3>
                                        <div class="pull-right">
                                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body">
                                        <?php
                                            // necessary for update action.
                                            if (! $modelKendala->isNewRecord) {
                                                echo Html::activeHiddenInput($modelKendala, "[{$i}]id");
                                            }
                                        ?>

                                        <div class="row">
                                            <div class="col-sm-3">

                                                <?= $form->field($modelKendala, "[{$i}]keterangan")->widget(Select2::classname(), [
                                                                    'data' => ArrayHelper::map(Kendala::find()->all()
                                                            ,'keterangan','keterangan'),
                                                                    'options' => ['placeholder' => 'Select Kendala'],
                                                                    'pluginOptions' => [
                                                                        // 'tags' => true,
                                                                        'allowClear' => true
                                                                    ],
                                                                    ]);
                                                ?>

                                            </div>
                                            <div class="col-sm-3">
                                                <?= $form->field($modelKendala, "[{$i}]start")->widget(TimePicker::classname(), [
                                                                'options' => ['placeholder' => 'Start'],
                                                                'pluginOptions' => [
                                                                        'minuteStep' => 5,
                                                                        'autoclose'=>true,
                                                                        'showMeridian' => false,
                                                                        'defaultTime' => '00:00'
                                                                ]
                                                ]); ?>
                                            </div>
                                        </div><!-- .row -->
                                    </div>
                                </div>
                                <?php endforeach; ?>
                                </div>
                                <?php DynamicFormWidget::end(); ?>
                        </div>

                        <p \>

                        <!-- Is Done Checkbox -->
                        <div id="is_done-form">
                        <?php
                            echo '<label class="cbx-label" for="is_done">Is Done?</label>';
                            echo CheckboxX::widget([
                                'model' => $model,
                                'attribute' => 'is_done',
                                'pluginOptions' => [
                                    'threeState' => false,
                                    'size'=>'sm'
                                ]
                            ]);
                        ?>
                        </div>
                    </div>
            </div>

            <!-- Validate Button -->
            <div class="form-group" id="create-button">
                

               <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?> 
            </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

$(function() {
    var state = document.getElementById("kemas1-state").value;
     if(state=="START"){
         $('#kemas1-stop').hide();
         $('#kemas1-start').show();
     }else{
         $('#kemas1-start').hide();
         $('#kemas1-stop').show();
     }
});

// Hide Submit Button untuk Konfirmasi Input
$('#submitButton').hide();

$('#kemas1-snfg_komponen').keypress(function(e) { 
    var s = String.fromCharCode( e.which );
    if ( s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey ) {
        alert('caps is on');
    }
});


// Inisialisasi Form Hide
    // $('#start-button').hide();
    // $('#stop-button').hide(); 
    // $('#form_start_stop').hide(); 
    // $('#form_jenis').hide(); 
    // $('#form_entry_start_stop').hide();

// After Klik Tombol Tekan Scan
    // Klik Scan Komponen
    // $('#afterScnBtnKomponen').click(function(){
    //     $('#form_start_stop').show();  
    // });






$('#is_kendala').change(function(){

    if(document.getElementById('is_kendala').checked){
            $('#kendala-form').show();

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);

                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = false;
                    document.getElementById("kendala-"+(i)+"-start").disabled = false;
        
                    i++;
                }

    }else{
            $('#kendala-form').hide();

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);
                    
                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = true;
                    document.getElementById("kendala-"+(i)+"-start").disabled = true;  

                    i++;
                }

    } 
});





$('#kemas1-snfg_komponen').change(function(){
    var snfg_komponen = $(this).val();

    // Assign Get Kendala Kemas
    $.post("index.php?r=scm-planner/get-kendala-kemas-komponen&snfg_komponen="+$('#kemas1-snfg_komponen').val(), function (data){
            $("select#kendala-0-keterangan").html(data);
    });

    $.post("index.php?r=scm-planner/get-line-kemas-komponen&snfg_komponen="+$('#kemas1-snfg_komponen').val(), function (data){
        $("select#kemas1-nama_line").html(data);
    });

    // $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
    //     var data = $.parseJSON(data);
    //     //alert(data.sediaan);
    //     $('#kemas1-snfg').attr('value',data.snfg);

    //     $('#streamline_results').html('STREAMLINE : '+data.streamline);
    //     $('#start_results').html('START : '+ data.start);
    //     $('#due_results').html('DUE : '+ data.due);
    //     $('#nama_bulk_results').html(data.nama_bulk);
    //     $('#nama_fg_results').html(data.nama_fg);
    //     $('#besar_batch_results').html('BESAR BATCH : '+ data.besar_batch);
    //     $('#besar_lot_results').html('BESAR LOT : '+ data.besar_lot);
    //     $('#lot_ke_results').html('LOT KE : '+ data.lot); ;
    //     $('#kode_jadwal_results').html('KODE JADWAL : '+ data.kode_jadwal);
    //     $('#jumlah_results').html('JUMLAH : '+ data.jumlah);


    // });

    $.get('index.php?r=scm-planner/get-planner-raw',{ snfg_komponen : snfg_komponen },function(data){
            var json = $.parseJSON(data);
            var tr;
            for (var i = 0; i < json.length; i++) {
                tr = $('<tr/>');
                tr.append("<td><span class='badge bg-grey'>" + json[i].nomo + "</span></td>");
                tr.append("<td><span class='badge bg-grey'>" + json[i].streamline + "</span></td>");
                tr.append("<td><span class='badge bg-grey'>" + json[i].start + "</span></td>");
                tr.append("<td><span class='badge bg-grey'>" + json[i].due + "</span></td>");
                tr.append("<td><span class='badge bg-grey'>" + json[i].besar_batch + "</span></td>");
                tr.append("<td><span class='badge bg-grey'>" + json[i].besar_lot + "</span></td>");
                tr.append("<td><span class='badge bg-grey'>" + json[i].lot_ke + "</span></td>");
                tr.append("<td><span class='badge bg-grey'>" + json[i].kode_jadwal + "</span></td>");
                tr.append("<td><span class='badge bg-green'>" + json[i].status + "</span></td>");
                $('#table-snfg').append(tr);
            }
            $('#planner-information').fadeIn("slow");
        });

    $.get('index.php?r=kemas1/get-kemas1-komponen',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#no-batch').attr('value',data.nobatch);
        $('#besar-batch-real').attr('value',data.besar_batch_real);
    });
    // $.get('index.php?r=scm-planner/get-ppr',{ snfg_komponen : snfg_komponen },function(data){
    //     var data = $.parseJSON(data);
    //     //alert(data.sediaan);
    //     $('#ppr-posisi').attr('value',data.posisi);
    //     $('#ppr-state').attr('value',data.state);
    //     $('#ppr-jenis-proses').attr('value',data.jenis_proses);
    //     $('#ppr-lanjutan').attr('value',data.lanjutan);
    //     $('#ppr-isdone').attr('value',data.is_done);
    // });

    $.get('index.php?r=scm-planner/get-ppr-raw',{ snfg_komponen : snfg_komponen },function(data){
        var json = $.parseJSON(data);
        var tr;
        for (var i = 0; i < json.length; i++) {
            tr = $('<tr/>');
            tr.append("<td><span class='badge bg-grey'>" + json[i].snfg_komponen + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].posisi + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].state + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].jenis_proses + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].lanjutan + "</span></td>");
            if(json[i].is_done=="Done"){
                tr.append("<td><span class='badge bg-green'>" + json[i].is_done + "</span></td>");
            }else if(json[i].is_done=="Not Done"){
                tr.append("<td><span class='badge bg-red'>" + json[i].is_done + "</span></td>");
            }
            $('#table-last').append(tr);
        }
        $('#last-position').fadeIn("slow");

    });

    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);

        if(data==null){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Not Found');
        }

        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#start-button').hide();
            $('#stop-button').hide();
            $('#istirahat-start-button').hide();
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Status ditetapkan sebagai HOLD/PAUSE dari Planner')
        }
        else{
                $.get('index.php?r=scm-planner/get-last',{ snfg_komponen : snfg_komponen },function(data){
                    var data = $.parseJSON(data);
                    if(data.posisi=="KEMAS 1" && data.state=="START"){
                        $('#start-button').hide();
                        $('#stop-button').show();

                        $('#kemas1-jenis_kemas_00').hide();
                        document.getElementById("kemas1-jenis_kemas").disabled = true;
                        $('#kemas1-jenis_kemas_0').show();
                        document.getElementById("kemas1-jenis_kemas_1").disabled = false;
                        $('#kemas1-jenis_kemas_1').attr('value',data.jenis_proses);

                        // Assign Lanjutan


                        $('#stop-button').click(function(){

                            var jenis_kemas = $('#kemas1-jenis_kemas_1').val();
                            var snfg_komponen = $('#kemas1-snfg_komponen').val();

                                $.get('index.php?r=kemas1/lanjutan-kemas1',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#kemas1-lanjutan').attr('value',data.lanjutan);
                                    $('#kemas1-lanjutan_stop').attr('value',data.lanjutan);


                                });

                        });


                        // EO Lanjutan


                    }else if(data.posisi=="KEMAS 1" && data.state=="STOP"){
                        $('#start-button').show();
                        $('#stop-button').hide();

                        $('#start-button').click(function(){

                            $('#kemas1-jenis_kemas').change(function(){

                                var jenis_kemas = $('#kemas1-jenis_kemas').val();
                                var snfg_komponen = $('#kemas1-snfg_komponen').val();

                                    $.get('index.php?r=kemas1/lanjutan-kemas1',{ snfg_komponen : snfg_komponen, jenis_kemas : jenis_kemas },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.sediaan);

                                        $('#kemas1-lanjutan').attr('value',data.lanjutan);
                                        $('#kemas1-lanjutan_stop').attr('value',data.lanjutan);



                                        });
                                });
                            });

                    }else if(data.posisi!="KEMAS 1" && data.state=="START"){
                        $('#start-button').hide();
                        $('#stop-button').hide();
                        alert('Jadwal sebelumnya belum di STOP');
                    }else if(data.posisi!="KEMAS 1" && data.state=="STOP" && data.is_done=="Not Done"){
                        $('#start-button').hide();
                        $('#stop-button').hide();
                        alert('Jadwal sebelumnya belum Is Done');
                    }else if(data.posisi!="KEMAS 1" && data!=null){
                        $('#start-button').fadeIn("slow");
                        $('#stop-button').hide();                
                    }
                });
            }
        });
});

$(function() {

    // DynamicForm override JS , Script Fix for non-working kartik date/timepicker afterInsert 
    // DynamicForm override JS , Fix for select2 kendala list


// DynamicForm override JS , Script Fix for non-working kartik date/timepicker afterInsert 
    // DynamicForm override JS , Fix for select2 kendala list

        // ID untuk penomoran nested dynamic form
        var id = 0;


        // Setelah Insert Button

        $(".dynamicform_wrapper").on('afterInsert', function(e, item) {
            var dateTimePickers = $(this).find('[data-krajee-kvtimepicker]');
            dateTimePickers.each(function(index, el) {
                $(this).parent().removeData().kvTimepicker('remove');
                $(this).parent().kvTimepicker(eval($(this).attr('data-krajee-kvtimepicker')));
            });
            

            // Override Fix for select2 kendala list 

            id += 1;
            console.log(id);


            $.post("index.php?r=scm-planner/get-kendala-kemas-komponen&snfg_komponen="+$('#kemas1-snfg_komponen').val(), function (data){

                    $("select#kendala-"+(id)+"-keterangan").html(data);

            });

        });

        // Setelah Delete Button

        $(".dynamicform_wrapper").on('afterDelete', function(e, item) {
            
            // Override Fix for select2 kendala list 

            id -= 1;
            console.log(id);

        });


        // Jika Mencapai Batas Maximum
        $(".dynamicform_wrapper").on('limitReached', function(e, item) {
            alert('Maximum 3');
        });


    // Hide and Disable DynamicFormWidget for Kendala Model //

        $('#kendala-form').hide();
            document.getElementById("kendala-0-keterangan").disabled = true;
            document.getElementById("kendala-0-start").disabled = true;
            


});



JS;
$this->registerJs($script);
?>
