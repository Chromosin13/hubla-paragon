<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\CGridView;

/* @var $this yii\web\View */
/* @var $model app\models\PengecekanUmum */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pengecekan Umum', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengecekan-umum-view">

    <div class="box box-info box-info">
            <div class="box-header with-border">

              <p>
                <!-- <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?> -->
                <?= Html::a('Print Label Barcode', ['qc-fg-v2/create-rincian','snfg'=>$model_fg->snfg], ['class' => 'btn btn-info']) ?>
                <?= Html::a('Siklus Selanjutnya', ['pengecekan-umum/create','snfg'=>$model_fg->snfg], ['class' => 'btn btn-success']) ?>
                <?= Html::a('Ganti Produk/Item', ['pengecekan-umum/scan-snfg-inspeksi'], ['class' => 'btn btn-warning']) ?>
            </p>

            

<div class="box box-primary box-solid">
          <div class="box-header with-border">
            <h3 class="box-title" ><b>1. IDENTITAS FINISHED GOODS</b></h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
        <div class="box-body">          
          
            <?= DetailView::widget([
                        'model' => $model_fg,
                        'attributes' => [
                            'snfg',
                            'nama_fg',
                            'keterangan',
                            'batch_fg',
                            // 'serial_number',
                            'no_mo',
                            'exp_date',
                            'due',
                            'plant',
                            'line_kemas', 
                            // 'kode_sl',
                            'no_notifikasi',
                            'nama_produk_sebelumnya',
                            'batch_produk_sebelumnya',

                            
                        ],
                    ]) ?>
            <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [

                        'jam_mulai',
                        'jam_selesai',                          
                        
                        ],
                    ]) ?>
            <?= DetailView::widget([
                    'model' => $model_fg,
                    'attributes' => [
                          
                        'label_bersih_mesin',
                        'line_clearance', 
                        'jumlah_mpq',
                        ],
                    ]) ?>
            <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'sistem_cek',

                                               
                        ],
                    ]) ?>
            <?= DetailView::widget([
                        'model' => $model_fg,
                        'attributes' => [
                            'jumlah_palet',          
                            
                            'total_aql',
                            
                        ],
                    ]) ?>
            <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                                          
                        'aql_per_palet',
                        'aql_per_siklus',
                        'no_palet',
                        'siklus',
                        'koli_qa',                      
                        ],
                    ]) ?>
        </div>
    </div>

<div class="box box-primary box-solid">
    <div class="box-header with-border">
      <h3 class="box-title"><b>2. PARAMETER VALIDASI PROSES KERJA PRODUKSI (UMUM)</b></h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
      <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
  <div class="box-body">
      <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [                        
                        'proses_kerja',
                        'sistem_3s',
                        'proses_produksi',                                               
                    ],
                ]) ?>
    </div>
  <!-- /.box -->
</div>

<div class="box box-primary box-solid">
    <div class="box-header with-border">
      <h3 class="box-title"><b>3. PARAMETER CHECKLIST FUNGSI MESIN (UMUM)</b></h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
      <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
  <div class="box-body">
      <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        
                        
                        'checkweigher',
                        'alasan_checkweigher',
                        'tipe_checkweigher',
                        'indicator_lamp',
                        'tekanan_udara',
                        'rejection_timing',
                        'hasil_uji_bulk',
                        'hasil_uji_dusat',
                        'hasil_uji_komponen',
                        'inline_weigher',
                        'alasan_inline_weigher',
                        'tipe_inline_weigher',
                        'indicator_lamp_inline',
                        'metode_tare',
                        
                                                                       
                    ],
                ]) ?>
    </div>
  <!-- /.box -->
</div>

<div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>5. JUMLAH KETIDAKSESUAIAN TERHADAP PARAMETER UJI PRODUK (UMUM)</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'jumlah_komponen_bulk',
                        'visual_performance_bulk',
                        'visual_performance_bulk_2',
                        'visual_performance_bulk_3',
                        'visual_performance_bulk_4',
                        'visual_performance_bulk_5',
                        'visual_performance_bulk_6',
                        'visual_performance_bulk_7',
                        'visual_performance_bulk_8',            
                        'kesesuaian_identitas_packaging',
                        'fungsional_packaging',
                        'kesesuaian_warna_packaging',
                        'fisik_packaging',
                        'performance_segel',
                        'uji_kekencangan_cap',
                    ],
                ]) ?>
            </div>
            <!-- /.box-body -->
          </div>
<div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>6. JUMLAH KETIDAKSESUAIAN TERHADAP PARAMETER UJI PRODUK (SEMSOL)</b></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <?= DetailView::widget([
                'model' => $model_semsol,
                'attributes' => [
                    'uji_ayun',
                    'uji_kepatahan',
                    
                    'uji_ulir',
                    'uji_ketrok',
                    'uji_oles',                    
                    'pengecekan_warna',
                    'uji_torsi_cap',
                    
                ],
            ]) ?>
            <!-- /.box-body -->
            </div>
      </div>               


                      
 

<div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>7. JUMLAH KETIDAKSESUAIAN TERHADAP PARAMETER IDENTITAS PRODUK (UMUM)</b></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'kesesuaian_batch_primer',
                        'kesesuaian_exp_primer',
                        'kesesuaian_batch_sekunder',
                        'kesesuaian_exp_sekunder',
                        'batch_innerbox',
                        'exp_innerbox',
                        'kesesuaian_batch_karbox',
                        'kesesuaian_exp_karbox',
                        'kesesuaian_barcode_produk',
                        'kesesuaian_na_produk',
                        'kualitas_barcode_karbox',
                        
                    ],
                ]) ?>
            </div>
            <!-- /.box-body -->
          </div>

<div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>8. JUMLAH KETIDAKSESUAIAN TERHADAP KELENGKAPAN KOMPONEN PRODUK (UMUM)</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'jumlah_komponen_penyusun',
                        'komponen_penyusun_1',
                        'temuan_penyusun_1',
                        'komponen_penyusun_2',
                        'temuan_penyusun_2',
                        'komponen_penyusun_3',
                        'temuan_penyusun_3',
                        'komponen_penyusun_4',
                        'temuan_penyusun_4',
                        'komponen_penyusun_5',
                        'temuan_penyusun_5',
                        'komponen_penyusun_6',
                        'temuan_penyusun_6',
                        'komponen_penyusun_7',
                        'temuan_penyusun_7',
                        'komponen_penyusun_8',
                        'temuan_penyusun_8',
                    ],
                ]) ?>
            </div>
          </div>
<div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>9. PARAMETER SAMPLING AKHIR</b></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'netto',
                        'jumlah_per_karbox',
                        'jumlah_per_palet',                        
                        'jumlah_sampel_trial',
                        'kesesuaian_warna_lakban',
                        'sampel_retain',
                        'sampel_mikro',
                      
                    ],
                ]) ?>
            </div>
            <!-- /.box-body -->
          </div>
<div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>10. BUKTI PEMERIKSAAN FG & PENANGANAN KETIDAKSESUAIAN</b></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [


                        'inspector_fg',
                        'catatan_pengecekan',

                        'status_siklus',
                        'status_hasil_cek_qa',
                        'status_produk', 
                        'investigasi_ketidaksesuaian',
                        'penanganan_ketidaksesuaian',
                        'detail_penanganan',
                        'metode_rework',                     
                    ],
                ]) ?>
            </div>
            <!-- /.box-body -->
          </div>




                 




              
