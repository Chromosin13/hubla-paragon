<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PengecekanUmumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hasil Inspeksi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengecekan-umum-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        <?= Html::a('Create Pengecekan Umum', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nama_produk',
            'batch_produk',
            'kode_sl',
            'no_notifikasi',
            'barcode_produk',
            // 'exp_date',
            // 'no_mo',
            // 'line_kemas',
            // 'due_date',
            // 'jumlah_mpq',
            // 'total_aql',
            // 'jumlah_palet',
            // 'aql_per_palet',
            // 'aql_per_siklus',
            // 'jam_mulai',
            // 'jam_selesai',
            // 'visual_performance_bulk',
            // 'fisik_packaging',
            // 'fungsional_packaging',
            // 'kesesuaian_warna_packaging',
            // 'kesesuaian_identitas_packaging',
            // 'performance_segel',
            // 'kesesuaian_batch_primer',
            // 'kesesuaian_exp_primer',
            // 'kesesuaian_batch_sekunder',
            // 'kesesuaian_exp_sekunder',
            // 'kesesuaian_batch_karbox',
            // 'kesesuaian_exp_karbox',
            // 'kesesuaian_notifikasi_produk',
            // 'kesesuaian_barcode_produk',
            // 'kualitas_barcode_karbox',
            // 'kelengkapan_komponen',
            // 'kesesuaian_kelengkapan_komponen',
            // 'netto',
            // 'jumlah_per_karbox',
            // 'kesesuaian_warna_lakban',
            // 'jumlah_sampel_trial',
            // 'sampel_retain',
            // 'sampel_mikro',
            // 'inspector_fg',
            // 'investigasi_ketidaksesuaian',
            // 'penanganan_ketidaksesuaian',
            // 'approval_ketidaksesuaian',
            // 'status_produk',
            // 'label_bersih_bulk',
            // 'label_bersih_mesin',
            // 'plant',
            // 'keterangan',
            // 'nama_produk_sebelumnya',
            // 'batch_produk_sebelumnya',
            // 'air_bilasan_akhir',
            // 'verifikasi_trial_filling',
            // 'id',
            // 'serial_number',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
