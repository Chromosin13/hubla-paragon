<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowAnalyst */

?>
<div class="form-pengecekan-umum">

    <?= $this->render('_form-create-qr', [
        'model' => $model,
    ]) ?>

</div>
