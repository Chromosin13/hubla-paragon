<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\PengecekanUmum;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<style >

</style>


                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
                      <h3 class="widget-user-username"><b>Halaman Scan Barcode</b></h3>
                      <h5 class="widget-user-desc">Input SNFG</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/clipboard-icon.png" alt="User Avatar">
                    </div>
                  
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="zoom">
                              <?=Html::a('SCAN WITH QR', "https://factory.pti-cosmetics.com/flowreport/web/index.php?r=pengecekan-umum/create-qr", ['class' => 'btn btn-success btn-block']);?>
                            </div>
                            <p></p>
                              <div class="box-body">
                                  <div class="pengecekan-umum-form">

                                    <div class="form-group field-pengecekanumum-snfg has-success">
                                    <label class="control-label" for="pengecekanumum-snfg">Snfg</label>
                                    <input type="text" id="pengecekanumum-snfg" class="form-control" name="PengecekanUmum[snfg]" aria-invalid="false">

                                    </div>
                                  </div>
                              </div>
                        </div>
                      <!-- /.row -->
                      </div>
                    </div>


                  <!-- List Data -->

                   <div class="box box-widget widget-user">

                      <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                                    IN PROGRESS
                      </h4>

                      <div class="box-footer">

                        <div class="row">
                          <div class="box-body">
                               
                                  <div class="pengecekan-umum-form">



                                     <?= GridView::widget([
                                        
                                        'dataProvider' => $dataProvider,
                                        'filterModel' => $searchModel,
                                        'bordered' => true,
                                        'responsiveWrap' => false,
                                        'columns' => [

                                          ['class' => 'yii\grid\ActionColumn',
                                            'template' => '{view}'],
                                            ['class' => 'yii\grid\ActionColumn',
                                            'template' => '{update}'],

                                          // ['class' => 'yii\grid\SerialColumn'],
                                            
                                            'snfg',
                                            [
                                                'attribute' => 'no_palet',
                                                'label'=>'Palet/Trolly',
                                                'contentOptions' => function($model){
                                                    return ['style' => ' text-align: center;width: 150px;font-size: 14px'];
                                                },
                                            ],
                                            [
                                                'attribute' => 'siklus',
                                                'label'=>'Siklus',
                                                'contentOptions' => function($model){
                                                    return ['style' => ' text-align: center;width: 150px;font-size: 14px'];
                                                },
                                            ],
                                            [
                                                'attribute' => 'sistem_cek',
                                                'label'=>'Sistem Cek',
                                                'contentOptions' => function($model){
                                                    return ['style' => ' text-align: center;width: 150px;font-size: 14px'];
                                                },
                                            ],
                                            [
                                                'attribute' => 'koli_qa',
                                                'label'=>'Koli QA',
                                                'contentOptions' => function($model){
                                                    return ['style' => ' text-align: center;width: 150px;font-size: 14px'];
                                                },
                                            ],
                                            [
                                                'attribute' => 'catatan_pengecekan',
                                                'label'=>'Catatan Pengecekan',
                                                'contentOptions' => function($model){
                                                    return ['style' => ' text-align: left;width: 250px;font-size: 14px'];
                                                },
                                                'headerOptions' => ['style' => 'text-align: center !important;'],
                                            ],

                                            [
                                                'attribute' => 'extra_proses',
                                                'label'=>'Kebutuhan EP',
                                                'contentOptions' => function($model){
                                                    return ['style' => ' text-align: left;width: 250px;font-size: 14px'];
                                                },
                                                'headerOptions' => ['style' => 'text-align: center !important;'],
                                            ],
                                            [
                                                'attribute' => 'pengerjaan_extra_proses',
                                                'label'=>'Progress EP',
                                                'contentOptions' => function($model){
                                                    return ['style' => ' text-align: left;width: 250px;font-size: 14px'];
                                                },
                                                'headerOptions' => ['style' => 'text-align: center !important;'],
                                            ],
                                            [
                                                'attribute' => 'jam_mulai',
                                                'label'=>'Mulai',
                                                'contentOptions' => function($model){
                                                    return ['style' => ' text-align: left;width: 250px;font-size: 14px'];
                                                },
                                                'headerOptions' => ['style' => 'text-align: center !important;'],
                                            ],
                                            [
                                                'attribute' => 'jam_selesai',
                                                'label'=>'Selesai',
                                                'contentOptions' => function($model){
                                                    return ['style' => ' text-align: left;width: 250px;font-size: 14px'];
                                                },
                                                'headerOptions' => ['style' => 'text-align: center !important;'],
                                            ],
                                            [
                                                'attribute' => 'inspector_fg',
                                                'label'=>'Inspector QCFG',
                                                'contentOptions' => function($model){
                                                    return ['style' => ' text-align: center;width: 250px;font-size: 14px'];
                                                },
                                            ],
                                            [
                                                'attribute' => 'status_siklus',
                                                'label'=>'Status Siklus',
                                                'contentOptions' => function($model){
                                                    return ['style' => ' text-align: center;width: 250px;font-size: 14px'];
                                                },
                                            ],
                                            [
                                                'attribute' => 'status_hasil_cek_qa',
                                                'label'=>'Status QA',
                                                'contentOptions' => function($model){
                                                    return ['style' => ' text-align: center;width: 250px;font-size: 14px'];
                                                },
                                            ],
                                            [
                                                'attribute' => 'status_produk',
                                                'label'=>'Status Produk',
                                                'contentOptions' => function($model){
                                                    return ['style' => ' text-align: center;width: 250px;font-size: 14px'];
                                                },
                                            ],
                                            
                                        ],
                                    ]); ?>


                                  </div>
                          </div>
                        </div>
                        <!-- /.row -->
                      </div>
                  </div>


<?php
$script = <<< JS

  // AutoFocus SNFG Field

  document.getElementById("pengecekanumum-snfg").focus();


  // Check SNFG Validity

  $('#pengecekanumum-snfg').change(function(){  

      var snfg = $('#pengecekanumum-snfg').val().trim();
      
      if(snfg){
        window.location = "index.php?r=pengecekan-umum/create&snfg="+snfg;
      }else{
        alert('Nomor Jadwal kosong!');
      }
      
  });

  // $('#pengecekanumum-snfg').change(function(){  

  //     var snfg = $('#pengecekanumum-snfg').val();


  //     $.get('index.php?r=scm-planner/check-snfg',{ snfg : snfg },function(data){
  //         var data = $.parseJSON(data);
  //         if(data.id>=1){
            
  //           window.location = "index.php?r=flow-input-snfg/create-kemas2&snfg="+snfg;
             
  //         }

  //         else{

  //           alert('Nomor SNFG Tidak Ditemukan di Database; Silahkan mengontak Planner/Leader');
  //         }
      
  //     });
  // });



JS;
$this->registerJs($script);
?>