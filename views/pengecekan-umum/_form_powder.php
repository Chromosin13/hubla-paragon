<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\date\DatePicker;
use kartik\widgets\TimePicker;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\ListKomponenPenyusun;

/* @var $this yii\web\View */
/* @var $model app\models\PengecekanUmum */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo5.png') center center;">
                      <h3 class="widget-user-username"><b>Quality Finished Goods Powder</b></h3>
                      <h5 class="widget-user-desc">FORM IN-LINE INSPECTION PRODUCT</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/clipboard-icon.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">

                    
                            

<div class="pengecekan-umum-form">

    <?php $form = ActiveForm::begin(); ?>

      <div class="box box-primary box-solid">
          <div class="box-header with-border">
            <h3 class="box-title" ><b>1. IDENTITAS FINISHED GOODS</b></h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
        <div class="box-body">
      
          
          <div class="row"> 
            <div class="hide">
                <?= $form->field($model_fg, 'sediaan')->textInput(['readOnly'=>true,'value'=>$model_fg->isNewRecord? $scmplanner['sediaan']:$model_fg->sediaan]) ?>

            </div>    
            <div class="col-md-12">
                <?= $form->field($model_fg, 'snfg')->textInput(['readOnly'=>true,'value'=>$model_fg->isNewRecord? $scmplanner['snfg']:$model_fg->snfg]) ?>

            </div>

            <div class="col-md-12">
                <?= $form->field($model_fg, 'nama_fg')->textInput(['readOnly'=>true,'value'=>$model_fg->isNewRecord? $scmplanner['nama_fg']:$model_fg->nama_fg]) ?>
            </div>
 
            <div class="col-md-12">
              <!-- /input-group -->
              <?php
                  echo $form->field($model_fg, 'keterangan')->widget(Select2::classname(), [
                      'data' => ['NPD'=>'NPD','Reguler'=>'Reguler'],
                      'options' => ['placeholder' => 'e.g : NPD', 'style' => "font-style:italic"],
                      'pluginOptions' => ['allowClear' => true],
                  ]);
                  
              ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
              <?= $form->field($model_fg, 'batch_fg')->textInput(['value'=> $model_fg->isNewRecord? $flowinputsnfg['nobatch']:$model_fg->batch_fg,'placeholder' => 'e.g : AG30B', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
            
            <div class="col-md-12">
              <?= $form->field($model_fg, 'no_mo')->textInput(['placeholder' => 'e.g : 200027596', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-12">
            <?= $form->field($model_fg, 'exp_date')->widget(DatePicker::classname(), [
                  'options' => ['placeholder' => 'e.g: 2023/Jan/03', 'style' => "font-style:italic"],
                  'pluginOptions' => [
                      'autoclose'=>true,
                      'format' => 'yyyy/M/dd',
                  ]
              ]); 
            ?>
            </div>
              <!-- /input-group -->
            
             <div class="col-md-12">
            <?= $form->field($model_fg, 'due')->widget(DatePicker::classname(), [
                  'options' => ['placeholder' => 'e.g: 2023/Jan/03', 'style' => "font-style:italic"],
                  'pluginOptions' => [
                      'autoclose'=>true,
                      'format' => 'yyyy/M/dd',
                  ]
              ]); 
            ?>
            </div>
               
        </div> 


        <div class="row">
            <div class="col-md-12">
              <?= $form->field($model_fg, 'plant')->textInput(['placeholder' => 'e.g : J1', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
              <!-- /input-group -->
            <div class="col-md-12">
              <?= $form->field($model_fg, 'line_kemas')->textInput(['placeholder' => 'e.g : BOP 08', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
              
            
            <div class="col-md-12">
              <?= $form->field($model_fg, 'no_notifikasi')->textInput(['placeholder' => 'e.g : NA18200101729', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
        </div>        
        <div class="row" >
            <div class="col-md-12">
              <?= $form->field($model_fg, 'nama_produk_sebelumnya')->textInput(['placeholder' => 'Isi dengan nama lengkap produk', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-12">
              <?= $form->field($model_fg, 'batch_produk_sebelumnya')->textInput(['placeholder' => 'e.g : LF24A', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>       
            <div class="col-md-12">
              <?= $form->field($model, 'jam_mulai')->textInput(['readOnly'=>true,'value'=> $model->isNewRecord? $start_time:$model->jam_mulai]) ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-12">
                      <!-- /input-group -->
              <?php
                  echo $form->field($model_fg, 'label_bersih_mesin')->widget(Select2::classname(), [
                      'data' => ['Ada'=>'Ada',''=>'','Tidak Ada'=>'Tidak Ada'],
                      'options' => ['placeholder' => 'e.g : Ada', 'style' => "font-style:italic"],
                      'pluginOptions' => ['allowClear' => true],
                  ]);
                  
              ?>
            </div>
            <div class="col-md-12">
              <?php
                    echo $form->field($model_fg, 'line_clearance')->widget(Select2::classname(), [
                        'data' => ['OK'=>'OK','Not Ok'=>'Not Ok'],
                        'options' => ['placeholder' => 'e.g : Ok', 'style' => "font-style:italic"],
                        'pluginOptions' => ['allowClear' => true],
                    ]);
                ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-12">
              <?= $form->field($model_fg, 'jumlah_mpq')->textInput(['value'=>$model_fg->isNewRecord? $scmplanner['jumlah_pcs']:$model_fg->jumlah_mpq,'placeholder' => 'e.g : 4406', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-12">
              <?php
                    echo $form->field($model, 'sistem_cek')->widget(Select2::classname(), [
                        'data' => ['QA System'=>'QA System','AQL System'=>'AQL System'],
                        'options' => ['value'=> $model->isNewRecord? $pengecekanumum['sistem_cek']:$model->sistem_cek, 'placeholder' => 'e.g : QA System', 'style' => "font-style:italic"],
                        'pluginOptions' => ['allowClear' => true],

                    ]);
                ?>
              <!-- /input-group -->
            </div>
            <div id="jml_palet" class="col-md-12">
              <?= $form->field($model_fg, 'jumlah_palet')->textInput(['placeholder' => 'e.g : 5', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
            
            <div id="total_aql" class="col-md-12">
              <?= $form->field($model_fg, 'total_aql')->textInput(['placeholder' => 'e.g : 300', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
            <div id="sisa_aql" class="col-md-12" id="sisa_aql">
              <?= $form->field($model, 'sisa_aql_yang_harus_dicek')->textInput(['readOnly'=>true, 'value'=>$model->isNewRecord? $aql_sisa:$model->sisa_aql_yang_harus_dicek]) ?>
            </div>
            <div id="aql_palet" class="col-md-12">
              <?= $form->field($model, 'aql_per_palet')->textInput(['value'=>$model->isNewRecord? $pengecekanumum['aql_per_palet']:$model->aql_per_palet,'placeholder' => 'e.g : 100', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
            <div id="aql_siklus" class="col-md-12">
              <?= $form->field($model, 'aql_per_siklus')->textInput(['placeholder' => 'e.g : 30', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div> 
            <div class="col-md-12">
            <?= $form->field($model, 'no_palet')->textInput(['value'=>$model->isNewRecord? $palet:$model->no_palet, 'readOnly'=>true]) ?>
            <!-- /input-group -->
            </div>
            <div class="col-md-12">
              <?= $form->field($model, 'siklus')->textInput(['value'=>$model->isNewRecord? $siklus:$model->siklus,'readOnly'=>true]) ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-12" id="koli_qa">
              <?php
                    echo $form->field($model, 'koli_qa')->widget(Select2::classname(), [
                        'data' => ['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10'],
                        'options' => ['placeholder' => 'e.g : 2', 'style' => "font-style:italic"],
                        'pluginOptions' => ['allowClear' => true],
                    ]);
                ?>
              <!-- /input-group -->
            </div>
          </div>
        </div>
       
      </div>

<div class="box box-primary box-solid">
    <div class="box-header with-border">
      <h3 class="box-title"><b>2. PARAMETER VALIDASI PROSES KERJA PRODUKSI (UMUM)</b></h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
      <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
          
          <div class="col-md-12">
            <?php
                echo $form->field($model, 'proses_kerja',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                    'data' => ['Sesuai Standard'=>'Sesuai Standard','Sesuai Tapi Tidak Konsisten'=>'Sesuai Tapi Tidak Konsisten','Tidak Sesuai Standard'=>'Tidak Sesuai Standard'],
                    'options' => ['placeholder' => 'e.g : Sesuai Standard', 'style' => "font-style:italic"],
                    'pluginOptions' => ['allowClear' => true],
                      ])->hint('Checkpoint : Cek proses kerja operator produksi di berbagai pos <br>
                            Standard : Bekerja sesuai IK, BR Kemas, & Standard FG',['style'=>'font-size:12px; font-style:italic; color: #000000;']);

            ?>
            <!-- /input-group -->
          </div>
          <div class="col-md-12">
            <?php
                echo $form->field($model, 'sistem_3s',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                    'data' => ['Dilaksanakan di Seluruh Pos'=>'Dilaksanakan di Seluruh Pos','Dilaksanakan di Sebagian Pos'=>'Dilaksanakan di Sebagian Pos','Tidak Dilaksanakan'=>'Tidak Dilaksanakan'],
                    'pluginOptions' => ['allowClear' => true],
                    'options' => ['placeholder' => 'e.g : Dilaksanakan di sebagian pos', 'style' => "font-style:italic"],
                      ])->hint('Checkpoint : Cek proses kerja operator produksi di berbagai pos <br>
                            Standard : Opr memastikan kualitas FG sebelum dilanjutkan ke pos berikutnya)',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
            ?>
            <!-- /input-group -->
          </div>
         <div class="col-md-12">
            <?php
                echo $form->field($model, 'proses_produksi',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                    'data' => ['Konsisten one-piece flow'=>'Konsisten one-piece flow','Tidak mengalir & menggunung'=>'Tidak mengalir & menggunung'],
                    'options' => ['placeholder' => 'e.g : Konsisten one-piece flow', 'style' => "font-style:italic"],
                    'pluginOptions' => ['allowClear' => true],
                      ])->hint('Checkpoint : Cek proses kerja operator di berbagai proses <br>
                            Standard : FG tidak menggunung dan mengalir secara one-piece flow',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
            ?>
            <!-- /input-group -->
          </div>
         
      </div>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>



<div id='not-packaging'>
  <div class="box box-primary box-solid">
      <div class="box-header with-border">
        <h3 class="box-title"><b>3. PARAMETER CHECKLIST FUNGSI MESIN (UMUM)</b></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            
            

            <div class="col-md-12">
              <?php
                  echo $form->field($model, 'checkweigher',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                      'data' => ['Digunakan'=>'Digunakan','Tidak Digunakan'=>'Tidak Digunakan','Tidak Tersedia'=>'Tidak Tersedia'],
                      'options' => ['placeholder' => 'e.g : Digunakan', 'style' => "font-style:italic"],
                      'pluginOptions' => ['allowClear' => true],
                        ])->hint('Checkpoint : Cek HMI & Cek Fisik <br> Standard Penggunaan : Selalu digunakan',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
              ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-12" id="alasan_checkweigher">
                        <?= $form->field($model, 'alasan_checkweigher',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : Tidak mengerti setting mesin checkweigher - NAFA', 'style' => "font-style:italic"]);  ?>
                        <!-- /input-group -->
            </div>
            <div class="col-md-12">
              <!-- /input-group -->
              <?php
                  echo $form->field($model, 'tipe_checkweigher')->widget(Select2::classname(), [
                      'data' => ['Sartorius (acc 0.1 & max. 600 gr)'=>'Sartorius (acc 0.1 & max. 600 gr)','MK-Cell (acc 0.1 & max 150 gr)'=>'MK-Cell (acc 0.1 & max 150 gr)'],
                      'options' => ['placeholder' => 'e.g : Sartorius (acc 0.1 & max. 600 gr)', 'style' => "font-style:italic"],
                      'pluginOptions' => ['allowClear' => true],
                  ]);
              ?>
            </div>
            <div class="col-md-12">
              <?php
                  echo $form->field($model, 'indicator_lamp',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                      'data' => ['Lampu sesuai hasil timbang'=>'Lampu sesuai hasil timbang','Lampu tidak sesuai hasil timbang'=>'Lampu tidak sesuai hasil timbang'],
                      'options' => ['placeholder' => 'e.g : Berfungsi dan sesuai hasil timbang', 'style' => "font-style:italic"],
                      'pluginOptions' => ['allowClear' => true],
                        ])->hint('Checkpoint : Cek kesesuaian warna lampu terhadap hasil timbang
                            <br>Standard : hijau = Ok , kuning = kurang , merah = lebih',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
              ?>
              <!-- /input-group -->
            </div>

            <div class="col-md-12">
              <?php
                  echo $form->field($model, 'tekanan_udara',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                      'data' => ['< 0.35 MPa'=>'< 0.35 MPa','0.35 - 0.5 MPa'=>'0.35 - 0.5 MPa','> 0.5 MPa'],
                      'options' => ['placeholder' => 'e.g : 0.4', 'style' => "font-style:italic"],
                      'pluginOptions' => ['allowClear' => true],
                        ])->hint('Checkpoint : Cek angka yang ditunjukan jarum pada pressure gauge
                      <br> Standard : 0.35 - 0.5 MPa (3.5 bar - 5 bar)',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
              ?>
              <!-- /input-group -->
            </div>

            <div class="col-md-12">
              <?php
                  echo $form->field($model, 'rejection_timing',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                      'data' => ['Sesuai & FG te-reject sempurna,'=>'Sesuai & FG te-reject sempurna,','Terlalu cepat'=>'Terlalu cepat','Terlambat'=>'Terlambat'],
                      'options' => ['placeholder' => 'e.g : sesuai & FG tereject sempurna', 'style' => "font-style:italic"],
                      'pluginOptions' => ['allowClear' => true],
                        ])->hint('Checkpoint : Cek ketepatan waktu rejector menyemprot / mendorong FG ke box
                      <br> Standard : Rejector tidak kecepatan / terlambat saat mereject FG',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
              ?>
              <!-- /input-group -->
            </div>

            <div class="col-md-12">
              <?php
                  echo $form->field($model, 'hasil_uji_bulk',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                      'data' => ['Terdeteksi & te-reject sempurna'=>'Terdeteksi & te-reject sempurna','Tidak te-reject & kelolosan'=>'Tidak te-reject & kelolosan','N/A' => 'N/A'],
                      'options' => ['placeholder' => 'e.g : Terdeteksi & te-reject sempurna', 'style' => "font-style:italic"],
                      'pluginOptions' => ['allowClear' => true],
                        ])->hint('Checkpoint : Cek apakah FG te-reject atau tidak ke box saat melewati CW
                    <br> Standard : FG Bulk Kosong te-reject ke box kurang (low)',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
              ?>
              <!-- /input-group -->
            </div>

            <div class="col-md-12">
              <?php
                  echo $form->field($model, 'hasil_uji_dusat',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                      'data' => ['Terdeteksi & te-reject sempurna'=>'Terdeteksi & te-reject sempurna','Tidak te-reject & kelolosan'=>'Tidak te-reject & kelolosan','N/A' => 'N/A'],
                      'options' => ['placeholder' => 'e.g : Tidak te-reject', 'style' => "font-style:italic"],
                      'pluginOptions' => ['allowClear' => true],
                        ])->hint('Checkpoint : Cek apakah FG te-reject atau tidak ke box saat melewati CW
                    <br> Standard : FG Dusat Kosong te-reject ke box kurang (low)',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
              ?>
              <!-- /input-group -->
            </div>

            <div class="col-md-12">
              <?php
                  echo $form->field($model, 'hasil_uji_komponen',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                      'data' => ['Terdeteksi & te-reject sempurna'=>'Terdeteksi & te-reject sempurna','Tidak te-reject & kelolosan'=>'Tidak te-reject & kelolosan','N/A' => 'N/A'],
                      'options' => ['placeholder' => 'e.g : Terdeteksi & tereject sempurna', 'style' => "font-style:italic"],
                      'pluginOptions' => ['allowClear' => true],
                        ])->hint('Checkpoint : Cek apakah FG te-reject atau tidak ke box saat melewati CW
                      <br> Standard : FG Komponen Tidak Lengkap te-reject ke box kurang (low)',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
              ?>
              <!-- /input-group -->
            </div>

            <div class="col-md-12">
              <?php
                  echo $form->field($model, 'inline_weigher',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                      'data' => ['Digunakan'=>'Digunakan','Tidak Digunakan'=>'Tidak Digunakan','Tidak Tersedia'=>'Tidak Tersedia'],
                      'options' => ['placeholder' => 'e.g : Menyala normal', 'style' => "font-style:italic"],
                      'pluginOptions' => ['allowClear' => true],
                        ])->hint('Checkpoint : Cek HMI & Cek Fisik <br> Standard Penggunaan : Selalu digunakan',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
              ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-12" id="alasan_inline_weigher">
                        <?= $form->field($model, 'alasan_inline_weigher',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : Kapasitas inline weigher tidak cukup - NAFA', 'style' => "font-style:italic"]);  ?>
                        <!-- /input-group -->
            </div>
            <div class="col-md-12">
              <?php
                  echo $form->field($model, 'tipe_inline_weigher')->widget(Select2::classname(), [
                      'data' => ['Inline'=>'Inline','Non-inline/station'=>'Non-inline/station'],
                      'options' => ['placeholder' => 'e.g : Inline', 'style' => "font-style:italic"],
                      'pluginOptions' => ['allowClear' => true],
                  ]);
              ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-12">
              <?php
                  echo $form->field($model, 'indicator_lamp_inline',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                      'data' => ['Lampu sesuai hasil timbang'=>'Lampu sesuai hasil timbang','Lampu tidak sesuai hasil timbang'=>'Lampu tidak sesuai hasil timbang'],
                      'options' => ['placeholder' => 'e.g : Menyala normal', 'style' => "font-style:italic"],
                      'pluginOptions' => ['allowClear' => true],
                        ])->hint('Checkpoint : Cek kesesuaian warna lampu terhadap hasil timbang
                    <br> Standard : hijau = OK , kuning = kurang , merah = lebih',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
              ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-12">
              <?php
                  echo $form->field($model, 'metode_tare')->widget(Select2::classname(), [
                      'data' => ['Tare dengan karbox/innerbox saja'=>'Tare dengan karbox/innerbox saja','Tare dengan dummy packaging'=>'Tare dengan dummy packaging','N/A' => 'N/A'],
                      'options' => ['placeholder' => 'e.g : Tare dengan karbox/innerbox saja', 'style' => "font-style:italic"],
                      'pluginOptions' => ['allowClear' => true],
                  ]);
              ?>
              <!-- /input-group -->
            </div>
        </div>
    </div>
      <!-- /.box-body -->
  </div>
  <!-- /.box -->  
</div>


<div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>5. JUMLAH KETIDAKSESUAIAN TERHADAP PARAMETER UJI PRODUK (UMUM)</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                  <div id='not-packaging'>
                    
                    <div class="col-md-12">
                      <?php
                          echo $form->field($model, 'jumlah_komponen_bulk')->widget(Select2::classname(), [
                              'data' => ['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8'],
                              'options' => ['value' => $model->isNewRecord? $pengecekanumum['jumlah_komponen_bulk']:$model->jumlah_komponen_bulk, 'placeholder' => 'e.g : 1 bulk - single', 'style' => "font-style:italic"],
                              'pluginOptions' => ['allowClear' => true],
                          ]);
                      ?>
                    </div>
                    <div class="col-md-12" id="bulk1">
                      <?= $form->field($model, 'visual_performance_bulk')->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12" id="bulk2">
                      <?= $form->field($model, 'visual_performance_bulk_2')->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12" id="bulk3">
                      <?= $form->field($model, 'visual_performance_bulk_3')->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12" id="bulk4">
                      <?= $form->field($model, 'visual_performance_bulk_4')->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12" id="bulk5">
                      <?= $form->field($model, 'visual_performance_bulk_5')->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12" id="bulk6">
                      <?= $form->field($model, 'visual_performance_bulk_6')->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12" id="bulk7">
                      <?= $form->field($model, 'visual_performance_bulk_7')->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12" id="bulk8">
                      <?= $form->field($model, 'visual_performance_bulk_8')->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                  </div>  <!-- not-pacakging-2 -->

                    <div class="col-md-12">
                      <?= $form->field($model, 'kesesuaian_identitas_packaging',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"]) ->hint('Not Ok : Salah variant, beda dari standard terbaru',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'fungsional_packaging',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"]) ->hint('Not Ok : Flip top tidak mengunci, ulir rusak, dst',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'kesesuaian_warna_packaging',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"]) ->hint('Not Ok : Packaging memudar, beda dari standard terbaru',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'fisik_packaging',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Kotor, baret',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']); ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'performance_segel',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not ok : Longgar, tidak pas, mengkerut > standard',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'uji_kekencangan_cap',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Terlalu kencang, terlalu longgar',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>                    
                </div>
            </div>
            <!-- /.box-body -->
          </div>    
<div id = 'powder' class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>6. JUMLAH KETIDAKSESUAIAN TERHADAP PARAMETER UJI PRODUK (POWDER)</b></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">

                    <div class="col-md-12">
                      <?= $form->field($model_powder, 'uji_jatuh',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Bulk retak / grepes setelah di uji 2-3x dari jarak 30 cm',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model_powder, 'uji_oles',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Caking (bedak tidak ngangjat), berinjil, tidak homogen',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model_powder, 'pengecekan_warna',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Warna bulk diluar standard dan toleransi warna merah & kuning',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                </div>    

            </div>
            <!-- /.box-body -->
          </div>
 
<div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>7. JUMLAH KETIDAKSESUAIAN TERHADAP PARAMETER IDENTITAS PRODUK (UMUM)</b></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                    <div class="col-md-12">
                      <?= $form->field($model, 'kesesuaian_batch_primer',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : FG03A', 'style' => "font-style:italic"])->hint('Not Ok : No batch pada pack primer salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>

                    <div class="col-md-12">
                      <?= $form->field($model, 'kesesuaian_exp_primer',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput()->hint('Not Ok : Exp date pada pack primer salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;'])->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'e.g: 2023/Jan/03', 'style' => "font-style:italic"],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy/M/dd',
                            ]
                        ]); 
                      ?>
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'kesesuaian_batch_sekunder',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : FG03A', 'style' => "font-style:italic"])->hint('Not Ok : No batch pada pack sekunder salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                      <!-- /input-group -->
                    <div class="col-md-12">
                      <?= $form->field($model, 'kesesuaian_exp_sekunder',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput()->hint('Not Ok : Exp date pada pack sekunder salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;'])->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'e.g: 2023/Jan/03', 'style' => "font-style:italic"],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy/M/dd',
                            ]
                        ]); 
                      ?>
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'batch_innerbox',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : FG03A', 'style' => "font-style:italic"])->hint('Not Ok : No batch pada ibox salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'exp_innerbox',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput()->hint('Not Ok : Exp date pada innerbox salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;'])->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'e.g: 2023/Jan/03', 'style' => "font-style:italic"],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy/M/dd',
                            ]
                        ]); 
                      ?>
                    </div>
                    
                </div>    
                <div class="row">
                    <div class="col-md-12">
                      <?= $form->field($model, 'kesesuaian_batch_karbox',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : FG03A', 'style' => "font-style:italic"])->hint('Not Ok : No batch pada karbok salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    
                    <div class="col-md-12">
                      <?= $form->field($model, 'kesesuaian_exp_karbox',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput()->hint('Not Ok : Exp date pada karbok salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;'])->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'e.g: 2023/Jan/03', 'style' => "font-style:italic"],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy/M/dd',
                            ]
                        ]); 
                      ?>
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'kesesuaian_barcode_produk',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Barcode yang tertera pada produk salah ketik',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'kesesuaian_na_produk',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'ketik ulang NA e.g : NA181001017129', 'style' => "font-style:italic"])->hint('Not Ok : Nomor NA yang tertera di produk tidak sesuai.',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                      <?php
                          echo $form->field($model, 'kualitas_barcode_karbox')->widget(Select2::classname(), [
                              'data' => ['Ok'=>'Ok','Not Ok'=>'Not Ok'],
                              'options' => ['placeholder' => 'e.g : Ok', 'style' => "font-style:italic"],
                              'pluginOptions' => ['allowClear' => true],
                          ]);
                      ?>
                    </div>
                    <div class="col-md-12">
                    

      <!-- /input-group -->
                  </div>                     
                </div> 
               </div>
            <!-- /.box-body -->
          </div>  

   
<div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>8. JUMLAH KETIDAKSESUAIAN TERHADAP KELENGKAPAN KOMPONEN PRODUK (UMUM)
</b></h3>
              <h5 style="font-style:italic">Not Ok : komponen tidak ada atau rusak</h5>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                    <div class="col-md-12">
                      <?php
                          echo $form->field($model, 'jumlah_komponen_penyusun')->widget(Select2::classname(), [
                              'data' => ['2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8'],
                              'options' => ['value'=> $model->isNewRecord? $pengecekanumum['jumlah_komponen_penyusun']:$model->jumlah_komponen_penyusun, 'placeholder' => 'e.g : 3', 'style' => "font-style:italic"],
                              'pluginOptions' => ['allowClear' => true],
                          ]);
                      ?>
                    </div>
                    <div class="col-md-12" id="komponen1">
                      <!-- /input-group -->
                      <?php echo $form->field($model, 'komponen_penyusun_1')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(ListKomponenPenyusun::find()->all()
                                                        ,'komponen','komponen'),
                                                        'options' => ['placeholder' => 'Isi nama komponen, e.g : Botol'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); 
                      ?>
                    </div>
                    <div class="col-md-12" id="temuan1">
                      <?= $form->field($model, 'temuan_penyusun_1')->textInput(['placeholder' => 'Jumlah temuan Not OK. e.g : 0 ', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12" id="komponen2">
                      <!-- /input-group -->
                      <?php echo $form->field($model, 'komponen_penyusun_2')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(ListKomponenPenyusun::find()->all()
                                                        ,'komponen','komponen'),
                                                        'options' => ['placeholder' => 'Isi nama komponen, e.g : Botol'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); 
                      ?>
                    </div>
                    <div class="col-md-12" id="temuan2">
                      <?= $form->field($model, 'temuan_penyusun_2')->textInput(['placeholder' => 'Jumlah temuan Not OK. e.g : 0 ', 'style' => "font-style:italic"]) ?>
                    </div>
                    <div class="col-md-12" id="komponen3">
                      <?php echo $form->field($model, 'komponen_penyusun_3')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(ListKomponenPenyusun::find()->all()
                                                        ,'komponen','komponen'),
                                                        'options' => ['placeholder' => 'Isi nama komponen, e.g : Botol'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); 
                      ?>
                    </div>
                    <div class="col-md-12" id="temuan3">
                      <?= $form->field($model, 'temuan_penyusun_3')->textInput(['placeholder' => 'Jumlah temuan Not OK. e.g : 0 ', 'style' => "font-style:italic"]) ?>
                    </div>
                    <div class="col-md-12" id="komponen4">
                      <?php echo $form->field($model, 'komponen_penyusun_4')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(ListKomponenPenyusun::find()->all()
                                                        ,'komponen','komponen'),
                                                        'options' => ['placeholder' => 'Isi nama komponen, e.g : Botol'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); 
                      ?>
                    </div>
                    <div class="col-md-12" id="temuan4">
                      <?= $form->field($model, 'temuan_penyusun_4')->textInput(['placeholder' => 'Jumlah temuan Not OK. e.g : 0 ', 'style' => "font-style:italic"]) ?>
                    </div>
                    <div class="col-md-12" id="komponen5">
                      <?php echo $form->field($model, 'komponen_penyusun_5')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(ListKomponenPenyusun::find()->all()
                                                        ,'komponen','komponen'),
                                                        'options' => ['placeholder' => 'Isi nama komponen, e.g : Botol'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); 
                      ?>
                    </div>
                    <div class="col-md-12" id="temuan5">
                      <?= $form->field($model, 'temuan_penyusun_5')->textInput(['placeholder' => 'Jumlah temuan Not OK. e.g : 0 ', 'style' => "font-style:italic"]) ?>
                    </div>
                    <div class="col-md-12" id="komponen6">
                      <?php echo $form->field($model, 'komponen_penyusun_6')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(ListKomponenPenyusun::find()->all()
                                                        ,'komponen','komponen'),
                                                        'options' => ['placeholder' => 'Isi nama komponen, e.g : Botol'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); 
                      ?>
                    </div>
                    <div class="col-md-12" id="temuan6">
                      <?= $form->field($model, 'temuan_penyusun_6')->textInput(['placeholder' => 'Jumlah temuan Not OK. e.g : 0 ', 'style' => "font-style:italic"]) ?>
                    </div>
                    <div class="col-md-12" id="komponen7">
                      <?php echo $form->field($model, 'komponen_penyusun_7')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(ListKomponenPenyusun::find()->all()
                                                        ,'komponen','komponen'),
                                                        'options' => ['placeholder' => 'Isi nama komponen, e.g : Botol'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); 
                      ?>
                    </div>
                    <div class="col-md-12" id="temuan7">
                      <?= $form->field($model, 'temuan_penyusun_7')->textInput(['placeholder' => 'Jumlah temuan Not OK. e.g : 0 ', 'style' => "font-style:italic"]) ?>
                    </div>
                    <div class="col-md-12" id="komponen8">
                      <?php echo $form->field($model, 'komponen_penyusun_8')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(ListKomponenPenyusun::find()->all()
                                                        ,'komponen','komponen'),
                                                        'options' => ['placeholder' => 'Isi nama komponen, e.g : Botol'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); 
                      ?>
                    </div>
                    <div class="col-md-12" id="temuan8">
                      <?= $form->field($model, 'temuan_penyusun_8')->textInput(['placeholder' => 'Jumlah temuan Not OK. e.g : 0 ', 'style' => "font-style:italic"]) ?>
                    </div>
               </div>
            <!-- /.box-body -->
            </div>
          </div>
<div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>9. PARAMETER SAMPLING AKHIR</b></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                  <div id='not-packaging'>
                    <div class="col-md-12">
                      <?= $form->field($model, 'netto')->textInput(['placeholder' => 'e.g : 20.5 - 20.7 - 20.4', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                  </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'jumlah_per_karbox')->textInput(['placeholder' => 'e.g : 36', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'jumlah_per_palet')->textInput(['placeholder' => 'e.g : 3600', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'jumlah_sampel_trial')->textInput(['placeholder' => 'e.g : 0,1,2,dst', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>

                  <div id='not-packaging'>
                    <div class="col-md-12">
                      <?php
                          echo $form->field($model, 'kesesuaian_warna_lakban')->widget(Select2::classname(), [
                              'data' => ['Hijau'=>'Hijau','Oranye'=>'Oranye','Ungu'=>'Ungu','Putih'=>'Putih','Tosca'=>'Tosca','Merah Muda'=>'Merah Muda'],
                              'options' => ['placeholder' => 'e.g : Hijau', 'style' => "font-style:italic"],
                              'pluginOptions' => ['allowClear' => true],
                          ]);
                      ?>
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'sampel_retain_yang_sudah_diambil')->textInput(['value'=>$model->isNewRecord? $sampel_retain:$model->sampel_retain_yang_sudah_diambil,'readOnly'=>true]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'sampel_retain')->textInput(['placeholder' => 'e.g : 0,1,2,dst', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                   <div class="col-md-12">
                      <?= $form->field($model, 'sampel_mikro_yang_sudah_diambil')->textInput(['value'=>$model->isNewRecord? $sampel_mikro:$model->sampel_mikro_yang_sudah_diambil,'readOnly'=>true]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'sampel_mikro')->textInput(['placeholder' => 'e.g : 0,1,2,dst', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                  </div>
                    
                    
                </div>
            </div>

            <!-- /.box-body -->
          </div>
          <div class="box box-primary box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"><b>10. BUKTI PEMERIKSAAN FG & PENANGANAN KETIDAKSESUAIAN</b></h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    
                    <div class="col-md-12">
                      <?= $form->field($model, 'inspector_fg')->textInput(['placeholder' => 'e.g : YA', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12">
                      <?= $form->field($model, 'catatan_pengecekan')->textArea(['placeholder' => 'e.g : Pallete ini belum selesai cek AQL', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12" id="status_siklus">
                      <!-- /input-group -->
                      <?php
                          echo $form->field($model, 'status_siklus')->widget(Select2::classname(), [
                              'data' => ['Release'=>'Release','Rework'=>'Rework','Pending'=>'Pending','Pending Mikro'=>'Pending Mikro'],
                              'options' => ['placeholder' => 'e.g : Release', 'style' => "font-style:italic"],
                              'pluginOptions' => ['allowClear' => true],
                          ]);
                          
                      ?>
                    </div>
                    <div class="col-md-12" id="status_hasil_cek_qa">
                      <!-- /input-group -->
                      <?php
                          echo $form->field($model, 'status_hasil_cek_qa')->widget(Select2::classname(), [
                              'data' => ['Release Pallete'=>'Release Pallete','Rework Pallete'=>'Rework Pallete','Pending Pallete'=>'Pending Pallete'],
                              'options' => ['placeholder' => 'e.g : Release Pallete', 'style' => "font-style:italic"],
                              'pluginOptions' => ['allowClear' => true],
                          ]);
                          
                      ?>
                    </div>

                    <div class="col-md-12" id="status_akhir">
                      <?php
                          echo $form->field($model, 'status_produk',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                              'data' => ['Release'=>'Release','Reject'=>'Reject'],
                              'options' => ['placeholder' => 'e.g : Release', 'style' => "font-style:italic"],
                              'pluginOptions' => ['allowClear' => true],
                                ])->hint('Status akhir FG yang diisi ketika proses "Rework" sudah dilakukan',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
                      ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12" id="investigasi">
                      <?= $form->field($model, 'investigasi_ketidaksesuaian')->textArea(['placeholder' => 'e.g : Ditemukan 10 pcs tube dengan seal bocor dari 50 sample pengecekan, kebocoran terjadi pada bagian ujung seal FG', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                </div>   
                    
                <div class="row">
                    <div class="col-md-12" id="penanganan">
                      <?php
                          echo $form->field($model, 'penanganan_ketidaksesuaian')->widget(Select2::classname(), [
                              'data' => ['Sortir & Rework (100 %)'=>'Sortir & Rework (100 %)','Sortir & Rework (sampling)'=>'Sortir & Rework (sampling)','Tukar Batch Packaging'=>'Tukar Batch Packaging','Tukar Batch Bulk'=>'Tukar Batch Bulk','Batch Split'=>'Batch Split','Pindah Mesin'=>'Pindah Mesin'],
                              'options' => ['placeholder' => 'e.g : sortir & rework (100 %), tukar batch packaging', 'style' => "font-style:italic"],
                              'pluginOptions' => ['allowClear' => true],
                          ]);
                      ?>
                    </div>
                    <div class="col-md-12" id="detail_penanganan">
                      <?= $form->field($model, 'detail_penanganan')->textArea(['placeholder' => 'e.g : 100 / 15000, 10 / 100 , dari batch xxx ke batch xxx', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12" id="metode">
                      <?= $form->field($model, 'metode_rework')->textInput(['placeholder' => 'e.g : bulk pada seluruh FG dikeluarkan lalu disaring, FG yang terkontaminasi kemudian dihitung sebagai not ok', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    
                   
                </div>
              </div>
              <!-- /.box-body -->
            </div>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
            <!-- /.box -->
          </div> 
<div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

        </div> 
            <!-- /.box -->
          </div>

            <!-- /.box -->
          </div> 


        </div> 
            <!-- /.box -->
          </div>

            <!-- /.box -->
          </div> 


        </div> 
            <!-- /.box -->
          </div> 


        </div> 
              </div>
            </div>
        </div>
    </div>
</div>

<?php
$script = <<< JS

    $('#hide').hide();
    $('#hide1').hide();
    $('#1').hide();
    $('#2').hide();
    $('#3').hide();
    $('#4').hide();
    $('#5').hide();
    $('#6').hide();
    $('#7').hide();
    $('#8').hide();
    $('#alasan_checkweigher').hide();
    $('#alasan_inline_weigher').hide();
    $('#komponen1').hide();
    $('#komponen2').hide();
    $('#komponen3').hide();
    $('#komponen4').hide();
    $('#komponen5').hide();
    $('#komponen6').hide();
    $('#komponen7').hide();
    $('#komponen8').hide();
    $('#temuan1').hide();
    $('#temuan2').hide();
    $('#temuan3').hide();
    $('#temuan4').hide();
    $('#temuan5').hide();
    $('#temuan6').hide();
    $('#temuan7').hide();
    $('#temuan8').hide();
    $('#penanganan').hide();
    $('#detail_penanganan').hide();
    $('#investigasi').hide();
    $('#metode').hide();
    $('#status_akhir').hide();
    $('#progres').hide();
    $('#bulk2').hide();
    $('#bulk3').hide();
    $('#bulk4').hide();
    $('#bulk5').hide();
    $('#bulk6').hide();
    $('#bulk7').hide();
    $('#bulk8').hide();
    $('#blister').hide();
    $('#shrink').hide();
    $('#akhir_ep').hide();
    $('#penanganan_ep').hide();
    $('#investigasi_ep').hide();
    $('#metode_ep').hide();
    $('#detail_penanganan_ep').hide();
    $('#koli_qa').hide();

    var item_category = '$item_category';
    console.log(item_category);
    if (item_category == 'packaging'){
      $('div[id^="not-packaging"]').hide();
    }else{
      $('div[id^="not-packaging"]').show();
    }

    var jml_komponen_penyusun_ep = $('#pengecekanliquid-jml_komponen_penyusun_ep').val();

        if (jml_komponen_penyusun_ep == '1'){
          $('#penyusun_1_ep').fadeIn();
          $('#temuan_penyusun_1_ep').fadeIn();
          $('#penyusun_2_ep').fadeOut();
          $('#temuan_penyusun_2_ep').fadeOut();

        }else if (jml_komponen_penyusun_ep == '2'){
          $('#penyusun_1_ep').fadeIn();
          $('#temuan_penyusun_1_ep').fadeIn();
          $('#penyusun_2_ep').fadeIn();
          $('#temuan_penyusun_2_ep').fadeIn();

        }else {
          $('#penyusun_1_ep').fadeOut();
          $('#temuan_penyusun_1_ep').fadeOut();
          $('#penyusun_2_ep').fadeOut();
          $('#temuan_penyusun_2_ep').fadeOut();

        }

    var sistem_cek = $('#pengecekanumum-sistem_cek').val();

        if (sistem_cek == 'QA System'){
          $('#jml_palet').fadeOut();
          $('#total_aql').fadeOut();
          $('#sisa_aql').fadeOut();
          $('#aql_palet').fadeOut();
          $('#aql_siklus').fadeOut();
          $('#status_siklus').fadeOut();
          $('#status_hasil_cek_qa').fadeIn();
          $('#koli_qa').fadeIn();
        }else{
          $('#jml_palet').fadeIn();
          $('#total_aql').fadeIn();
          $('#sisa_aql').fadeIn();
          $('#aql_palet').fadeIn();
          $('#aql_siklus').fadeIn();
          $('#koli_qa').hide();
          $('#status_hasil_cek_qa').fadeOut();
          $('#status_siklus').fadeIn();
        }

    var tipe_extra_proses = $('#pengecekanumum-tipe_extra_proses').val();

      if (tipe_extra_proses == 'Proses Blister'){
        $('#blister').fadeIn();
        $('#shrink').fadeOut();
      }else if (tipe_extra_proses == 'Proses Shrink Wrap'){
        $('#blister').fadeOut();
        $('#shrink').fadeIn();
      }else{
        $('#blister').fadeOut();
        $('#shrink').fadeOut();
      }
    

    $('#pengecekanliquid-jml_komponen_penyusun_ep').change(function(){

        var jml_komponen_penyusun_ep = $('#pengecekanliquid-jml_komponen_penyusun_ep').val();

        if (jml_komponen_penyusun_ep == '1'){
          $('#penyusun_1_ep').fadeIn();
          $('#temuan_penyusun_1_ep').fadeIn();
          $('#penyusun_2_ep').fadeOut();
          $('#temuan_penyusun_2_ep').fadeOut();

        }else if (jml_komponen_penyusun_ep == '2'){
          $('#penyusun_1_ep').fadeIn();
          $('#temuan_penyusun_1_ep').fadeIn();
          $('#penyusun_2_ep').fadeIn();
          $('#temuan_penyusun_2_ep').fadeIn();

        }else {
          $('#penyusun_1_ep').fadeOut();
          $('#temuan_penyusun_1_ep').fadeOut();
          $('#penyusun_2_ep').fadeOut();
          $('#temuan_penyusun_2_ep').fadeOut();

        }
    });


    $('#pengecekanumum-status_siklus').change(function(){

        var status_siklus = $('#pengecekanumum-status_siklus').val();

        if (status_siklus == 'Release'){
          $('#status_akhir').fadeOut();
        }else{
          $('#status_akhir').fadeIn();
        }
    });

    $('#pengecekanumum-status_hasil_cek_qa').change(function(){

        var status_hasil_cek_qa = $('#pengecekanumum-status_hasil_cek_qa').val();

        if (status_hasil_cek_qa == 'Release Pallete'){
          $('#status_akhir').fadeOut();
        }else{
          $('#status_akhir').fadeIn();
        }
    });

    $('#pengecekanumum-sistem_cek').change(function(){

        var sistem_cek = $('#pengecekanumum-sistem_cek').val();

        if (sistem_cek == 'QA System'){
          $('#jml_palet').fadeOut();
          $('#total_aql').fadeOut();
          $('#sisa_aql').fadeOut();
          $('#aql_palet').fadeOut();
          $('#aql_siklus').fadeOut();
          $('#status_siklus').fadeOut();
          $('#status_hasil_cek_qa').fadeIn();
          $('#koli_qa').fadeIn();
        }else{
          $('#jml_palet').fadeIn();
          $('#total_aql').fadeIn();
          $('#sisa_aql').fadeIn();
          $('#aql_palet').fadeIn();
          $('#aql_siklus').fadeIn();
          $('#status_hasil_cek_qa').fadeOut();
          $('#status_siklus').fadeIn();
          $('#koli_qa').hide();
        }
    });

    
    $('#pengecekanumum-tipe_extra_proses').change(function(){

        var tipe_extra_proses = $('#pengecekanumum-tipe_extra_proses').val();

        if (tipe_extra_proses == 'Proses Blister'){
          $('#blister').fadeIn();
          $('#shrink').fadeOut();
        }else if (tipe_extra_proses == 'Proses Shrink Wrap'){
          $('#blister').fadeOut();
          $('#shrink').fadeIn();
        }else{
          $('#blister').fadeOut();
          $('#shrink').fadeOut();
        }
    });


    $('#pengecekanliquid-line_proses_blister').change(function(){

        var line_proses_blister = $('#pengecekanliquid-line_proses_blister').val();

        if (line_proses_blister == 'BLP 01'){
          $('#akhir_ep').fadeIn();
        }else if (line_proses_blister == 'BLP 02'){
          $('#akhir_ep').fadeIn();
        }else{
          $('#akhir_ep').fadeOut();
        }
    });

    var line_proses_blister = $('#pengecekanliquid-line_proses_blister').val();

        if (line_proses_blister == 'BLP 01'){
          $('#akhir_ep').fadeIn();
        }else if (line_proses_blister == 'BLP 02'){
          $('#akhir_ep').fadeIn();
        }else{
          $('#akhir_ep').fadeOut();
        }

    $('#pengecekanliquid-line_kemas_shrink').change(function(){

        var line_kemas_shrink = $('#pengecekanliquid-line_kemas_shrink').val();

        if (line_kemas_shrink == 'OWS'){
          $('#akhir_ep').fadeIn();
        }else if (line_kemas_shrink == 'Manual'){
          $('#akhir_ep').fadeIn();
        }else{
          $('#akhir_ep').fadeOut();
        }
    });

    var line_kemas_shrink = $('#pengecekanliquid-line_kemas_shrink').val();

        if (line_kemas_shrink == 'OWS'){
          $('#akhir_ep').fadeIn();
        }else if (line_kemas_shrink == 'Manual'){
          $('#akhir_ep').fadeIn();
        }else{
          $('#akhir_ep').fadeOut();
        }

    $('#pengecekanumum-extra_proses').change(function(){

        var extra_proses = $('#pengecekanumum-extra_proses').val();

        if (extra_proses == 'Butuh'){
          $('#progres').fadeIn();
          $('#tipe_extra_proses').fadeIn();
        }else{
          $('#pengecekanumum-tipe_extra_proses').val('').trigger('change');
          $('#pengecekanumum-pengerjaan_extra_proses').val('').trigger('change');
          $('#progres').fadeOut();
          $('#tipe_extra_proses').fadeOut();
        }
    });
        var extra_proses = $('#pengecekanumum-extra_proses').val();

        if (extra_proses == 'Butuh'){
          $('#progres').fadeIn();
          $('#tipe_extra_proses').fadeIn();
        }else{
          $('#pengecekanumum-tipe_extra_proses').val('').trigger('change');
          $('#pengecekanumum-pengerjaan_extra_proses').val('').trigger('change');
          $('#progres').fadeOut();
          $('#tipe_extra_proses').fadeOut();
        }

    $('#pengecekanumum-status_produk').change(function(){

        var status_produk = $('#pengecekanumum-status_produk').val();

        if (status_produk == 'Release','Reject'){
          $('#penanganan').fadeIn();
          $('#investigasi').fadeIn();
          $('#metode').fadeIn();
          $('#detail_penanganan').fadeIn();
        }else{
          $('#penanganan').fadeOut();
          $('#investigasi').fadeOut();
          $('#metode').fadeOut();
          $('#detail_penanganan').fadeOut();
        }
    });

    $('#pengecekanliquid-status_siklus_ep').change(function(){

        var status_siklus_ep = $('#pengecekanliquid-status_siklus_ep').val();

        if (status_siklus_ep == 'Release'){
          $('#status_akhir_ep').fadeOut();
        }else{
          $('#status_akhir_ep').fadeIn();
        }
    });

    $('#pengecekanliquid-status_produk_ep').change(function(){

        var status_produk_ep = $('#pengecekanliquid-status_produk_ep').val();

        if (status_produk_ep == 'Release'){
          $('#penanganan_ep').fadeIn();
          $('#investigasi_ep').fadeIn();
          $('#metode_ep').fadeIn();
          $('#detail_penanganan_ep').fadeIn();
        }else if (status_produk_ep == 'Reject'){
          $('#penanganan_ep').fadeIn();
          $('#investigasi_ep').fadeIn();
          $('#metode_ep').fadeIn();
          $('#detail_penanganan_ep').fadeIn();
        }else{
          $('#penanganan_ep').fadeOut();
          $('#investigasi_ep').fadeOut();
          $('#metode_ep').fadeOut();
          $('#detail_penanganan_ep').fadeOut();
        }
    });
    
    $('#pengecekanumum-checkweigher').change(function(){

        var checkweigher = $('#pengecekanumum-checkweigher').val();

        if (checkweigher == 'Tidak Digunakan'){
          $('#alasan_checkweigher').fadeIn();
        }else{
          $('#pengecekanumum-alasan_checkweigher').val('').trigger('change');
          $('#alasan_checkweigher').fadeOut();
        }
    });

    $('#pengecekanumum-inline_weigher').change(function(){

        var inline_weigher = $('#pengecekanumum-inline_weigher').val();

        if (inline_weigher == 'Tidak Digunakan'){
          $('#alasan_inline_weigher').fadeIn();
        }else{
          $('#pengecekanumum-alasan_inline_weigher').val('').trigger('change');
          $('#alasan_inline_weigher').fadeOut();
        }
    });

    $('#pengecekanliquid-tube_dedusting').change(function(){

        var tube_dedusting = $('#pengecekanliquid-tube_dedusting').val();

        if (tube_dedusting == 'Tidak Aktif'){
          $('#1').fadeIn();
        }else{
          $('#pengecekanliquid-alasan_tube_dedusting').val('').trigger('change');
          $('#1').fadeOut();
        }
    });

    $('#pengecekanliquid-strainer').change(function(){

        var strainer = $('#pengecekanliquid-strainer').val();

        if (strainer == 'Tidak Aktif'){
          $('#2').fadeIn();
        }else{
          $('#pengecekanliquid-alasan_strainer').val('').trigger('change');
          $('#2').fadeOut();
        }
    });

    $('#pengecekanliquid-bad_tube_ejector').change(function(){

        var bad_tube_ejector = $('#pengecekanliquid-bad_tube_ejector').val();

        if (bad_tube_ejector == 'Tidak Aktif'){
          $('#3').fadeIn();
        }else{
          $('#pengecekanliquid-alasan_bad_tube_ejector').val('').trigger('change');
          $('#3').fadeOut();
        }
    });

    $('#pengecekanliquid-product_sensing').change(function(){

        var product_sensing = $('#pengecekanliquid-product_sensing').val();

        if (product_sensing == 'Tidak Aktif'){
          $('#4').fadeIn();
        }else{
          $('#pengecekanliquid-alasan_product_sensing').val('').trigger('change');
          $('#4').fadeOut();
        }
    });

    $('#pengecekanliquid-bottle_dedusting').change(function(){

        var bottle_dedusting = $('#pengecekanliquid-bottle_dedusting').val();

        if (bottle_dedusting == 'Tidak Aktif'){
          $('#5').fadeIn();
        }else{
          $('#pengecekanliquid-alasan_bottle_dedusting').val('').trigger('change');
          $('#5').fadeOut();
        }
    });

    $('#pengecekanliquid-auto_capper').change(function(){

        var auto_capper = $('#pengecekanliquid-auto_capper').val();

        if (auto_capper== 'Tidak Aktif'){
          $('#6').fadeIn();
        }else{
          $('#pengecekanliquid-alasan_auto_capper').val('').trigger('change');
          $('#6').fadeOut();
        }
    });

    $('#pengecekanliquid-plug_checking').change(function(){

        var plug_checking = $('#pengecekanliquid-plug_checking').val();

        if (plug_checking == 'Tidak Aktif'){
          $('#7').fadeIn();
        }else{
          $('#pengecekanliquid-alasan_plug_checking').val('').trigger('change');
          $('#7').fadeOut();
        }
    });

    $('#pengecekanliquid-pot_dedusting').change(function(){

        var pot_dedusting = $('#pengecekanliquid-pot_dedusting').val();

        if (pot_dedusting == 'Tidak Aktif'){
          $('#8').fadeIn();
        }else{
          $('#pengecekanliquid-alasan_pot_dedusting').val('').trigger('change');
          $('#8').fadeOut();
        }
    });

    $('#pengecekanumum-jumlah_komponen_bulk').change(function(){

        var jumlah_komponen_bulk = $('#pengecekanumum-jumlah_komponen_bulk').val();

        if (jumlah_komponen_bulk == 1){
          $('#bulk1').fadeIn();
          $('#bulk2').fadeOut();
          $('#bulk3').fadeOut();
          $('#bulk4').fadeOut();
          $('#bulk5').fadeOut();
          $('#bulk6').fadeOut();
          $('#bulk7').fadeOut();
          $('#bulk8').fadeOut();

        }else if (jumlah_komponen_bulk == 2){
          $('#bulk1').fadeIn();
          $('#bulk2').fadeIn();
          $('#bulk3').fadeOut();
          $('#bulk4').fadeOut();
          $('#bulk5').fadeOut();
          $('#bulk6').fadeOut();
          $('#bulk7').fadeOut();
          $('#bulk8').fadeOut();

        }else if (jumlah_komponen_bulk == 3){
          $('#bulk1').fadeIn();
          $('#bulk2').fadeIn();
          $('#bulk3').fadeIn();
          $('#bulk4').fadeOut();
          $('#bulk5').fadeOut();
          $('#bulk6').fadeOut();
          $('#bulk7').fadeOut();
          $('#bulk8').fadeOut();

        }else if (jumlah_komponen_bulk == 4){
          $('#bulk1').fadeIn();
          $('#bulk2').fadeIn();
          $('#bulk3').fadeIn();
          $('#bulk4').fadeIn();
          $('#bulk5').fadeOut();
          $('#bulk6').fadeOut();
          $('#bulk7').fadeOut();
          $('#bulk8').fadeOut();

        }else if (jumlah_komponen_bulk == 5){
          $('#bulk1').fadeIn();
          $('#bulk2').fadeIn();
          $('#bulk3').fadeIn();
          $('#bulk4').fadeIn();
          $('#bulk5').fadeIn();
          $('#bulk6').fadeOut();
          $('#bulk7').fadeOut();
          $('#bulk8').fadeOut();

        }else if (jumlah_komponen_bulk == 6){
          $('#bulk1').fadeIn();
          $('#bulk2').fadeIn();
          $('#bulk3').fadeIn();
          $('#bulk4').fadeIn();
          $('#bulk5').fadeIn();
          $('#bulk6').fadeIn();
          $('#bulk7').fadeOut();
          $('#bulk8').fadeOut();

        }else if (jumlah_komponen_bulk == 7){
          $('#bulk1').fadeIn();
          $('#bulk2').fadeIn();
          $('#bulk3').fadeIn();
          $('#bulk4').fadeIn();
          $('#bulk5').fadeIn();
          $('#bulk6').fadeIn();
          $('#bulk7').fadeIn();
          $('#bulk8').fadeOut();

        }else{
          $('#bulk1').show();
          $('#bulk2').show();
          $('#bulk3').show();
          $('#bulk4').show();
          $('#bulk5').show();
          $('#bulk6').show();
          $('#bulk7').show();
          $('#bulk8').show();

        }
    });

    var jumlah_komponen_bulk = $('#pengecekanumum-jumlah_komponen_bulk').val();

        if (jumlah_komponen_bulk == 1){
          $('#bulk1').fadeIn();
          $('#bulk2').fadeOut();
          $('#bulk3').fadeOut();
          $('#bulk4').fadeOut();
          $('#bulk5').fadeOut();
          $('#bulk6').fadeOut();
          $('#bulk7').fadeOut();
          $('#bulk8').fadeOut();

        }else if (jumlah_komponen_bulk == 2){
          $('#bulk1').fadeIn();
          $('#bulk2').fadeIn();
          $('#bulk3').fadeOut();
          $('#bulk4').fadeOut();
          $('#bulk5').fadeOut();
          $('#bulk6').fadeOut();
          $('#bulk7').fadeOut();
          $('#bulk8').fadeOut();

        }else if (jumlah_komponen_bulk == 3){
          $('#bulk1').fadeIn();
          $('#bulk2').fadeIn();
          $('#bulk3').fadeIn();
          $('#bulk4').fadeOut();
          $('#bulk5').fadeOut();
          $('#bulk6').fadeOut();
          $('#bulk7').fadeOut();
          $('#bulk8').fadeOut();

        }else if (jumlah_komponen_bulk == 4){
          $('#bulk1').fadeIn();
          $('#bulk2').fadeIn();
          $('#bulk3').fadeIn();
          $('#bulk4').fadeIn();
          $('#bulk5').fadeOut();
          $('#bulk6').fadeOut();
          $('#bulk7').fadeOut();
          $('#bulk8').fadeOut();

        }else if (jumlah_komponen_bulk == 5){
          $('#bulk1').fadeIn();
          $('#bulk2').fadeIn();
          $('#bulk3').fadeIn();
          $('#bulk4').fadeIn();
          $('#bulk5').fadeIn();
          $('#bulk6').fadeOut();
          $('#bulk7').fadeOut();
          $('#bulk8').fadeOut();

        }else if (jumlah_komponen_bulk == 6){
          $('#bulk1').fadeIn();
          $('#bulk2').fadeIn();
          $('#bulk3').fadeIn();
          $('#bulk4').fadeIn();
          $('#bulk5').fadeIn();
          $('#bulk6').fadeIn();
          $('#bulk7').fadeOut();
          $('#bulk8').fadeOut();

        }else if (jumlah_komponen_bulk == 7){
          $('#bulk1').fadeIn();
          $('#bulk2').fadeIn();
          $('#bulk3').fadeIn();
          $('#bulk4').fadeIn();
          $('#bulk5').fadeIn();
          $('#bulk6').fadeIn();
          $('#bulk7').fadeIn();
          $('#bulk8').fadeOut();

        }else{
          $('#bulk1').show();
          $('#bulk2').show();
          $('#bulk3').show();
          $('#bulk4').show();
          $('#bulk5').show();
          $('#bulk6').show();
          $('#bulk7').show();
          $('#bulk8').show();

        }

    $('#pengecekanumum-jumlah_komponen_penyusun').change(function(){

        var jumlah_komponen_penyusun = $('#pengecekanumum-jumlah_komponen_penyusun').val();

        if (jumlah_komponen_penyusun == 2){
          $('#komponen1').fadeIn();
          $('#komponen2').fadeIn();
          $('#temuan2').fadeIn();
          $('#komponen3').fadeOut();
          $('#komponen4').fadeOut();
          $('#komponen5').fadeOut();
          $('#komponen6').fadeOut();
          $('#komponen7').fadeOut();
          $('#komponen8').fadeOut();
          $('#temuan1').fadeIn();
          $('#temuan3').fadeOut();
          $('#temuan4').fadeOut();
          $('#temuan5').fadeOut();
          $('#temuan6').fadeOut();
          $('#temuan7').fadeOut();
          $('#temuan8').fadeOut();
        }else if (jumlah_komponen_penyusun == 3){
          $('#komponen1').fadeIn();
          $('#komponen2').fadeIn();
          $('#komponen3').fadeIn();
          $('#komponen4').fadeOut();
          $('#komponen5').fadeOut();
          $('#komponen6').fadeOut();
          $('#komponen7').fadeOut();
          $('#komponen8').fadeOut();
          $('#temuan1').fadeIn();
          $('#temuan2').fadeIn();
          $('#temuan3').fadeIn();
          $('#temuan4').fadeOut();
          $('#temuan5').fadeOut();
          $('#temuan6').fadeOut();
          $('#temuan7').fadeOut();
          $('#temuan8').fadeOut();
        }else if (jumlah_komponen_penyusun == 4){
          $('#komponen1').fadeIn();
          $('#komponen2').fadeIn();
          $('#komponen3').fadeIn();
          $('#komponen4').fadeIn();
          $('#komponen5').fadeOut();
          $('#komponen6').fadeOut();
          $('#komponen7').fadeOut();
          $('#komponen8').fadeOut();
          $('#temuan1').fadeIn();
          $('#temuan2').fadeIn();
          $('#temuan3').fadeIn();
          $('#temuan4').fadeIn();
          $('#temuan5').fadeOut();
          $('#temuan6').fadeOut();
          $('#temuan7').fadeOut();
          $('#temuan8').fadeOut();
        }else if (jumlah_komponen_penyusun == 5){
          $('#komponen1').fadeIn();
          $('#komponen2').fadeIn();
          $('#komponen3').fadeIn();
          $('#komponen4').fadeIn();
          $('#komponen5').fadeIn();
          $('#komponen6').fadeOut();
          $('#komponen7').fadeOut();
          $('#komponen8').fadeOut();
          $('#temuan1').fadeIn();
          $('#temuan2').fadeIn();
          $('#temuan3').fadeIn();
          $('#temuan4').fadeIn();
          $('#temuan5').fadeIn();
          $('#temuan6').fadeOut();
          $('#temuan7').fadeOut();
          $('#temuan8').fadeOut();
        }else if (jumlah_komponen_penyusun == 6){
          $('#komponen1').fadeIn();
          $('#komponen2').fadeIn();
          $('#komponen3').fadeIn();
          $('#komponen4').fadeIn();
          $('#komponen5').fadeIn();
          $('#komponen6').fadeIn();
          $('#komponen7').fadeOut();
          $('#komponen8').fadeOut();
          $('#temuan1').fadeIn();
          $('#temuan2').fadeIn();
          $('#temuan3').fadeIn();
          $('#temuan4').fadeIn();
          $('#temuan5').fadeIn();
          $('#temuan6').fadeIn();
          $('#temuan7').fadeOut();
          $('#temuan8').fadeOut();
        }else if (jumlah_komponen_penyusun == 7){
          $('#komponen1').fadeIn();
          $('#komponen2').fadeIn();
          $('#komponen3').fadeIn();
          $('#komponen4').fadeIn();
          $('#komponen5').fadeIn();
          $('#komponen6').fadeIn();
          $('#komponen7').fadeIn();
          $('#komponen8').fadeOut();
          $('#temuan1').fadeIn();
          $('#temuan2').fadeIn();
          $('#temuan3').fadeIn();
          $('#temuan4').fadeIn();
          $('#temuan5').fadeIn();
          $('#temuan6').fadeIn();
          $('#temuan7').fadeIn();
          $('#temuan8').fadeOut();
        }else{
          $('#komponen1').show();
          $('#komponen2').show();
          $('#komponen3').show();
          $('#komponen4').show();
          $('#komponen5').show();
          $('#komponen6').show();
          $('#komponen7').show();
          $('#komponen8').show();
          $('#temuan1').show();
          $('#temuan2').show();
          $('#temuan3').show();
          $('#temuan4').show();
          $('#temuan5').show();
          $('#temuan6').show();
          $('#temuan7').show();
          $('#temuan8').show();
        }
    });

    var jumlah_komponen_penyusun = $('#pengecekanumum-jumlah_komponen_penyusun').val();

        if (jumlah_komponen_penyusun == 2){
          $('#komponen1').fadeIn();
          $('#komponen2').fadeIn();
          $('#temuan2').fadeIn();
          $('#komponen3').fadeOut();
          $('#komponen4').fadeOut();
          $('#komponen5').fadeOut();
          $('#komponen6').fadeOut();
          $('#komponen7').fadeOut();
          $('#komponen8').fadeOut();
          $('#temuan1').show();
          $('#temuan3').fadeOut();
          $('#temuan4').fadeOut();
          $('#temuan5').fadeOut();
          $('#temuan6').fadeOut();
          $('#temuan7').fadeOut();
          $('#temuan8').fadeOut();
        }else if (jumlah_komponen_penyusun == 3){
          $('#komponen1').fadeIn();
          $('#komponen2').fadeIn();
          $('#komponen3').fadeIn();
          $('#komponen4').fadeOut();
          $('#komponen5').fadeOut();
          $('#komponen6').fadeOut();
          $('#komponen7').fadeOut();
          $('#komponen8').fadeOut();
          $('#temuan1').show();
          $('#temuan2').fadeIn();
          $('#temuan3').fadeIn();
          $('#temuan4').fadeOut();
          $('#temuan5').fadeOut();
          $('#temuan6').fadeOut();
          $('#temuan7').fadeOut();
          $('#temuan8').fadeOut();
        }else if (jumlah_komponen_penyusun == 4){
          $('#komponen1').fadeIn();
          $('#komponen2').fadeIn();
          $('#komponen3').fadeIn();
          $('#komponen4').fadeIn();
          $('#komponen5').fadeOut();
          $('#komponen6').fadeOut();
          $('#komponen7').fadeOut();
          $('#komponen8').fadeOut();
          $('#temuan1').show();
          $('#temuan2').fadeIn();
          $('#temuan3').fadeIn();
          $('#temuan4').fadeIn();
          $('#temuan5').fadeOut();
          $('#temuan6').fadeOut();
          $('#temuan7').fadeOut();
          $('#temuan8').fadeOut();
        }else if (jumlah_komponen_penyusun == 5){
          $('#komponen1').fadeIn();
          $('#komponen2').fadeIn();
          $('#komponen3').fadeIn();
          $('#komponen4').fadeIn();
          $('#komponen5').fadeIn();
          $('#komponen6').fadeOut();
          $('#komponen7').fadeOut();
          $('#komponen8').fadeOut();
          $('#temuan1').show();
          $('#temuan2').fadeIn();
          $('#temuan3').fadeIn();
          $('#temuan4').fadeIn();
          $('#temuan5').fadeIn();
          $('#temuan6').fadeOut();
          $('#temuan7').fadeOut();
          $('#temuan8').fadeOut();
        }else if (jumlah_komponen_penyusun == 6){
          $('#komponen1').fadeIn();
          $('#komponen2').fadeIn();
          $('#komponen3').fadeIn();
          $('#komponen4').fadeIn();
          $('#komponen5').fadeIn();
          $('#komponen6').fadeIn();
          $('#komponen7').fadeOut();
          $('#komponen8').fadeOut();
          $('#temuan1').show();
          $('#temuan2').fadeIn();
          $('#temuan3').fadeIn();
          $('#temuan4').fadeIn();
          $('#temuan5').fadeIn();
          $('#temuan6').fadeIn();
          $('#temuan7').fadeOut();
          $('#temuan8').fadeOut();
        }else if (jumlah_komponen_penyusun == 7){
          $('#komponen1').fadeIn();
          $('#komponen2').fadeIn();
          $('#komponen3').fadeIn();
          $('#komponen4').fadeIn();
          $('#komponen5').fadeIn();
          $('#komponen6').fadeIn();
          $('#komponen7').fadeIn();
          $('#komponen8').fadeOut();
          $('#temuan1').show();
          $('#temuan2').fadeIn();
          $('#temuan3').fadeIn();
          $('#temuan4').fadeIn();
          $('#temuan5').fadeIn();
          $('#temuan6').fadeIn();
          $('#temuan7').fadeIn();
          $('#temuan8').fadeOut();
        }else{
          $('#komponen1').show();
          $('#komponen2').show();
          $('#komponen3').show();
          $('#komponen4').show();
          $('#komponen5').show();
          $('#komponen6').show();
          $('#komponen7').show();
          $('#komponen8').show();
          $('#temuan1').show();
          $('#temuan2').show();
          $('#temuan3').show();
          $('#temuan4').show();
          $('#temuan5').show();
          $('#temuan6').show();
          $('#temuan7').show();
          $('#temuan8').show();
        }  

JS;
$this->registerJs($script);
?>

