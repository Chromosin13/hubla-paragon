<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PengecekanUmum */

// $this->title = 'Create Pengecekan Umum';
// $this->params['breadcrumbs'][] = ['label' => 'Pengecekan Umums', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengecekan-umum-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_semsol', [
        'model' => $model,
        'model_fg' => $model_fg,
        'model_semsol' => $model_semsol,
        'scmplanner' => $scmplanner,
        'flowinputsnfg' => $flowinputsnfg,
        'pengecekanumum' => $pengecekanumum,
        'palet'=>$palet,
        'siklus'=>$siklus,
        'start_time'=>$start_time,
        'aql_sisa' =>$aql_sisa,
        'sampel_mikro' => $sampel_mikro,
        'sampel_retain' => $sampel_retain,
        'item_category' => $item_category,
    ]) ?>

</div>
