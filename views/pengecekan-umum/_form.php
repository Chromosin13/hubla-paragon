<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\date\DatePicker;
use kartik\widgets\TimePicker;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\ListKomponenPenyusun;

/* @var $this yii\web\View */
/* @var $model app\models\PengecekanUmum */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo5.png') center center;">
                      <h3 class="widget-user-username"><b>Quality Finished Goods</b></h3>
                      <h5 class="widget-user-desc">FORM IN-LINE INSPECTION PRODUCT</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/clipboard-icon.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">

                    
                            

<div class="pengecekan-umum-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="hide">
        <?= $form->field($model, 'sediaan')->textInput(['value'=>$sediaan]) ?>
    </div>

      <div class="box box-primary box-solid">
          <div class="box-header with-border">
            <h3 class="box-title" ><b>IDENTITAS FINISHED GOODS</b></h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
        <div class="box-body">
      
          
          <div class="row"> 
            <div class="hide">
                <?= $form->field($model_fg, 'sediaan')->textInput(['readOnly'=>true,'value'=>$sediaan]) ?>

            </div>    
            <div class="col-md-5">
                <?= $form->field($model_fg, 'snfg')->textInput(['readOnly'=>true,'value'=>$snfg]) ?>

            </div>

            <div class="col-md-5">
                <?= $form->field($model_fg, 'nama_fg')->textInput(['readOnly'=>true,'value'=>$nama]) ?>
            </div>
 
            <div class="col-md-2">
              <!-- /input-group -->
              <?php
                  echo $form->field($model_fg, 'keterangan')->widget(Select2::classname(), [
                      'data' => ['NPD'=>'NPD','Reguler'=>'Reguler'],
                      'options' => ['placeholder' => 'e.g : NPD', 'style' => "font-style:italic"],
                  ]);
                  
              ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
              <?= $form->field($model_fg, 'batch_fg')->textInput(['value'=> $nobatch]) ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-2">
              <?= $form->field($model_fg, 'serial_number')->textInput(['placeholder' => 'e.g : F0015', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-2">
              <?= $form->field($model_fg, 'no_mo')->textInput(['placeholder' => 'e.g : 200027596', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-2">
            <?= $form->field($model_fg, 'exp_date')->widget(DatePicker::classname(), [
                  'options' => ['placeholder' => 'e.g: 2023/Jan/03', 'style' => "font-style:italic"],
                  'pluginOptions' => [
                      'autoclose'=>true,
                      'format' => 'yyyy/M/dd',
                  ]
              ]); 
            ?>
            </div>
              <!-- /input-group -->
            
             <div class="col-md-2">
            <?= $form->field($model_fg, 'due')->widget(DatePicker::classname(), [
                  'options' => ['placeholder' => 'e.g: 2023/Jan/03', 'style' => "font-style:italic"],
                  'pluginOptions' => [
                      'autoclose'=>true,
                      'format' => 'yyyy/M/dd',
                  ]
              ]); 
            ?>
            </div>
            <div class="col-md-2">
              <?= $form->field($model_fg, 'jumlah_palet')->textInput(['placeholder' => 'e.g : 5', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>    
        </div> 


        <div class="row">
            <div class="col-md-2">
              <?= $form->field($model_fg, 'plant')->textInput(['placeholder' => 'e.g : J1', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
              <!-- /input-group -->
            <div class="col-md-2">
              <?= $form->field($model_fg, 'line_kemas')->textInput(['placeholder' => 'e.g : BOP 08', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-2">
              <?= $form->field($model_fg, 'jumlah_mpq')->textInput(['value'=>$mpq,'placeholder' => 'e.g : 4406', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-2">
              <?= $form->field($model_fg, 'total_aql')->textInput(['placeholder' => 'e.g : 300', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-2">
              <?= $form->field($model, 'aql_per_palet')->textInput(['value'=>$aql_per_palet,'placeholder' => 'e.g : 100', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-2" id="sisa_aql">
              <?= $form->field($model, 'sisa_aql_yang_harus_dicek')->textInput(['readOnly'=>true, 'value'=>$aql_sisa]) ?>
            </div>
            <div class="col-md-2">
              <?= $form->field($model, 'aql_per_siklus')->textInput(['placeholder' => 'e.g : 30', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>  
            <div class="col-md-2">
              <?= $form->field($model_fg, 'kode_sl')->textInput(['value'=>$streamline, ['placeholder' => 'e.g: AA atau aa', 'style' => "font-style:italic"]]) ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-2">
              <?= $form->field($model_fg, 'no_notifikasi')->textInput(['placeholder' => 'e.g : NA18200101729', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
        </div>        
        <div class="row" >
            <div class="col-md-4">
              <?= $form->field($model_fg, 'nama_produk_sebelumnya')->textInput(['placeholder' => 'Isi dengan nama lengkap produk', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-2">
              <?= $form->field($model_fg, 'batch_produk_sebelumnya')->textInput(['placeholder' => 'e.g : LF24A', 'style' => "font-style:italic"]) ?>
              <!-- /input-group -->
            </div>       
            <div class="col-md-2">
              <?= $form->field($model, 'jam_mulai')->textInput(['value'=> $start_time]) ?>
              <!-- /input-group -->
            </div>
            <div class="col-md-2">
                      <!-- /input-group -->
              <?php
                  echo $form->field($model_fg, 'label_bersih_mesin')->widget(Select2::classname(), [
                      'data' => ['Ada'=>'Ada',''=>'','Tidak Ada'=>'Tidak Ada'],
                      'options' => ['placeholder' => 'e.g : Ada', 'style' => "font-style:italic"],
                  ]);
                  
              ?>
            </div>
            <div class="col-md-2">
              <?php
                    echo $form->field($model_fg, 'line_clearance')->widget(Select2::classname(), [
                        'data' => ['OK'=>'OK','Not Ok'=>'Not Ok'],
                        'options' => ['placeholder' => 'e.g : Ok', 'style' => "font-style:italic"],
                    ]);
                ?>
              <!-- /input-group -->
            </div>
          </div>
        </div>
       
      </div>






<div class="box box-primary box-solid">
    <div class="box-header with-border">
      <h3 class="box-title"><b>PARAMETER SAMPLING AWAL</b></h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
      <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
          
          <div class="col-md-2">
            <?= $form->field($model, 'no_palet')->textInput(['value'=>$palet, 'readOnly'=>true]) ?>
            <!-- /input-group -->
          </div>
          <div class="col-md-2">
            <?= $form->field($model, 'siklus')->textInput(['value'=>$siklus,'readOnly'=>true]) ?>
            <!-- /input-group -->
          </div>

          <div class="col-md-2">
            <?php
                echo $form->field($model, 'checkweigher',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                    'data' => ['Digunakan'=>'Digunakan','Tidak Digunakan'=>'Tidak Digunakan','Tidak Tersedia'=>'Tidak Tersedia'],
                    'options' => ['placeholder' => 'e.g : Digunakan', 'style' => "font-style:italic"],
                      ])->hint('Checkpoint : Cek HMI & Cek Fisik <br> Standard Penggunaan : Selalu digunakan',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
            ?>
            <!-- /input-group -->
          </div>
          <div class="col-md-2">
            <!-- /input-group -->
            <?php
                echo $form->field($model, 'tipe_checkweigher')->widget(Select2::classname(), [
                    'data' => ['Sartorius (acc 0.1 & max. 600 gr)'=>'Sartorius (acc 0.1 & max. 600 gr)','MK-Cell (acc 0.1 & max 150 gr)'=>'MK-Cell (acc 0.1 & max 150 gr)'],
                    'options' => ['placeholder' => 'e.g : Sartorius (acc 0.1 & max. 600 gr)', 'style' => "font-style:italic"],
                ]);
            ?>
          </div>
          <div class="col-md-2">
            <?php
                echo $form->field($model, 'inline_weigher',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                    'data' => ['Digunakan'=>'Digunakan','Tidak Digunakan'=>'Tidak Digunakan','Tidak Tersedia'=>'Tidak Tersedia'],
                    'options' => ['placeholder' => 'e.g : Digunakan', 'style' => "font-style:italic"],
                      ])->hint('Checkpoint : Cek HMI & Cek Fisik <br> Standard Penggunaan : Selalu digunakan',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
            ?>
            <!-- /input-group -->
          </div>
          <div class="col-md-2">
            <?php
                echo $form->field($model, 'tipe_inline_weigher')->widget(Select2::classname(), [
                    'data' => ['Inline'=>'Inline','Non-inline/station'=>'Non-inline/station'],
                    'options' => ['placeholder' => 'e.g : Inline', 'style' => "font-style:italic"],
                ]);
            ?>
            <!-- /input-group -->
          </div>
          <div class="col-md-2">
            <?php
                echo $form->field($model, 'indicator_lamp')->widget(Select2::classname(), [
                    'data' => ['Menyala Normal'=>'Menyala Normal',''=>'','Tidak Menyala/Ada Yang Putus'=>'Tidak Menyala/Ada Yang Putus'],
                    'options' => ['placeholder' => 'e.g : Menyala Normal', 'style' => "font-style:italic"],

                ]);
            ?>
            <!-- /input-group -->
          </div>

      </div>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>

<div id = 'semsol' class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>JUMLAH KETIDAKSESUAIAN TERHADAP PARAMETER UJI PRODUK (SEMISOLID)</b></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                    
                    <div class="col-md-2">
                      <?= $form->field($model_semsol, 'uji_ayun',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Bulk lepas dari packaging saat di uji ayun',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model_semsol, 'uji_kepatahan',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Patah saat diuji',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model_semsol, 'uji_ulir',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Bulk patah saat diketrok, saat pack diputar bulk tidak bisa naik-turun',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div> 
                    <div class="col-md-2">
                      <?= $form->field($model_semsol, 'uji_ketrok',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Bulk patah saat diketrok, saat pack diputar bulk tidak bisa naik-turun',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model_semsol, 'uji_oles',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Tidak homogen & brinjil (lipcream & foundation), patah setelah uji oles 50x (lipstik)',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model_semsol, 'pengecekan_warna',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Warna bulk berbeda dengan warna dari bulk standard FG',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model_semsol, 'uji_torsi_cap',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi 3x cek nilai pengukuran : 4.5 , 4.7 , 5.1', 'style' => "font-style:italic"])->hint('Not Ok : Dalam 3x pengukuran, nilai torsi < standard torsi cap',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                </div>    

            </div>
            <!-- /.box-body -->
          </div>
<div id = 'liquid' class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>JUMLAH KETIDAKSESUAIAN TERHADAP PARAMETER UJI PRODUK (LIQUID)</b></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                    <div class="col-md-2">
                      <?= $form->field($model_liquid, 'uji_ketahanan_seal',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Seal bocor, terbuka, sehingga bulk keluar',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model_liquid, 'hasil_seal',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Seal miring, menguku, menguping',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model_liquid, 'uji_torsi_cap',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi 3x cek nilai pengukuran : 4.5 , 4.7 , 5.1', 'style' => "font-style:italic"])->hint('Not Ok : Dalam 3x pengukuran, nilai torsi < standard torsi cap',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>    

            </div>
            <!-- /.box-body -->
          </div>
      </div>

<div id = 'liquid1' class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>PARAMETER CHECKLIST FUNGSI MESIN (LIQUID)</b></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                    
                    <div class="col-md-2">
                      <?php
                          echo $form->field($model_liquid, 'tube_dedusting',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                              'data' => ['Aktif'=>'Aktif','Tidak Aktif'=>'Tidak Aktif','NA'=>'N/A'],
                              'options' => ['placeholder' => 'e.g : aktif', 'style' => "font-style:italic"],
                                ])->hint('Zone A : TUP 01-09 <br> Checkpoint : Cek HMI <br> Standard Penggunaan : Selalu aktif',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
                      ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2" id="1">
                      <?= $form->field($model_liquid, 'alasan1',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : Tube dedusting sedang rusak / NAFA', 'style' => "font-style:italic"]);  ?>
                      <!-- /input-group -->
                    </div>
                    
                    <div class="col-md-2">
                      <?php
                          echo $form->field($model_liquid, 'strainer',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                              'data' => ['Aktif'=>'Aktif','Tidak Aktif'=>'Tidak Aktif','NA'=>'N/A'],
                              'options' => ['placeholder' => 'e.g : aktif', 'style' => "font-style:italic"],
                                ])->hint('Zone A : TUP 01-09 | Zone B : BOP 01,03,04 & JAP 01 | Zone C : BOP 09-10, JAP 03, EMP 01,03,07 <br> Checkpoint : Cek HMI, Cek Fisik <br> Standard Penggunaan : Sesuai BR , Selalu Aktif',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
                      ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2" id="2">
                      <?= $form->field($model_liquid, 'alasan2',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : Beads tidak dapat melewati strainer / NAFA', 'style' => "font-style:italic"]);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?php
                          echo $form->field($model_liquid, 'bad_tube_ejector',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                              'data' => ['Aktif'=>'Aktif','Tidak Aktif'=>'Tidak Aktif','NA'=>'N/A'],
                              'options' => ['placeholder' => 'e.g : aktif', 'style' => "font-style:italic"],
                                ])->hint('Zone A : TUP 01,02,06,07,08,09 <br> Checkpoint : Cek HMI, Cek Fisik <br> Standard Penggunaan : Selalu Aktif , Sesuai BR',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
                      ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2" id="3">
                      <?= $form->field($model_liquid, 'alasan3',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : Bad tube ejector sedang maintenance / NAFA', 'style' => "font-style:italic"]);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?php
                          echo $form->field($model_liquid, 'product_sensing',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                              'data' => ['Aktif'=>'Aktif','Tidak Aktif'=>'Tidak Aktif','NA'=>'N/A'],
                              'options' => ['placeholder' => 'e.g : aktif', 'style' => "font-style:italic"],
                                ])->hint('Zone A : TUP 06-09 | Zone C : BOP 07-08 <br> Checkpoint : Cek HMI Standard Penggunaan : Selalu aktif',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
                      ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2" id="4">
                      <?= $form->field($model_liquid, 'alasan4',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : Product sensing sedang perbaikan / AMUN', 'style' => "font-style:italic"]);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?php
                          echo $form->field($model_liquid, 'bottle_dedusting',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                              'data' => ['Aktif'=>'Aktif','Tidak Aktif'=>'Tidak Aktif','NA'=>'N/A'],
                              'options' => ['placeholder' => 'e.g : aktif', 'style' => "font-style:italic"],
                                ])->hint('Zone B : BOP 03-05 | Zone C : BOP 09 , EMP 01,03,07 <br> Checkpoint : Cek HMI atau Cek Fungsi <br> Standard Penggunaan : Selalu aktif',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
                      ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2" id="5">
                      <?= $form->field($model_liquid, 'alasan5',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : suplai udara mati / AMUN', 'style' => "font-style:italic"]);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?php
                          echo $form->field($model_liquid, 'auto_capper',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                              'data' => ['Aktif'=>'Aktif','Tidak Aktif'=>'Tidak Aktif','NA'=>'N/A'],
                              'options' => ['placeholder' => 'e.g : aktif', 'style' => "font-style:italic"],
                                ])->hint('Zone B : BOP 03-06 | Zone C : BOP 10 (capping mobile) <br> Checkpoint : Cek HMI <br> Standard Penggunaan : Sesuai BR',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
                      ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2" id="6">
                      <?= $form->field($model_liquid, 'alasan6',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : capper sedang trouble / NAFA', 'style' => "font-style:italic"]);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?php
                          echo $form->field($model_liquid, 'plug_checking',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                              'data' => ['Aktif'=>'Aktif','Tidak Aktif'=>'Tidak Aktif','NA'=>'N/A'],
                              'options' => ['placeholder' => 'e.g : aktif', 'style' => "font-style:italic"],
                                ])->hint('Zone B : BOP 04-05 <br> Checkpoint : Cek HMI <br> Standard Penggunaan : Sesuai BR',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
                      ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2" id="7">
                      <?= $form->field($model_liquid, 'alasan7',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : sensor plug checking sedang rusak / AMUN', 'style' => "font-style:italic"]);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?php
                          echo $form->field($model_liquid, 'pot_dedusting',['template'=>"{label}\n{hint}\n{input}\n{error}"])->widget(Select2::classname(), [
                              'data' => ['Aktif'=>'Aktif','Tidak Aktif'=>'Tidak Aktif','NA'=>'N/A'],
                              'options' => ['placeholder' => 'e.g : aktif', 'style' => "font-style:italic"],
                                ])->hint('Zone B : JAP 01 | Zone C : JAP 03 <br> Checkpoint : Cek HMI atau Cek Fungsi <br> Standard Penggunaan : Selalu aktif',['style'=>'font-size:12px; font-style:italic; color: #000000;']);
                      ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2" id="8">
                      <?= $form->field($model_liquid, 'alasan8',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : nozzle dari dedusting rusak / NAFA', 'style' => "font-style:italic"]);  ?>
                      <!-- /input-group -->
                    </div>
                </div>    

            </div>
            <!-- /.box-body -->
          </div>

<div id = 'powder' class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>JUMLAH KETIDAKSESUAIAN TERHADAP PARAMETER UJI PRODUK (POWDER)</b></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">

                    <div class="col-md-2">
                      <?= $form->field($model_powder, 'uji_jatuh',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Bulk retak / grepes setelah di uji 2-3x dari jarak 30 cm',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model_powder, 'uji_oles',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Caking (bedak tidak ngangjat), berinjil, tidak homogen',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model_powder, 'pengecekan_warna',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Warna bulk diluar standard dan toleransi warna merah & kuning',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                </div>    

            </div>
            <!-- /.box-body -->
          </div>

<div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>JUMLAH KETIDAKSESUAIAN TERHADAP PARAMETER UJI PRODUK (UMUM)</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                    <div class="col-md-2">
                      <?= $form->field($model, 'visual_performance_bulk')->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model, 'kesesuaian_identitas_packaging',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"]) ->hint('Not Ok : Salah variant, beda dari standard terbaru',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model, 'fungsional_packaging',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"]) ->hint('Not Ok : Flip top tidak mengunci, ulir rusak, dst',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model, 'kesesuaian_warna_packaging',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"]) ->hint('Not Ok : Packaging memudar, beda dari standard terbaru',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model, 'fisik_packaging',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Kotor, baret',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']); ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model, 'performance_segel',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not ok : Longgar, tidak pas, mengkerut > standard',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model, 'uji_kekencangan_cap',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'Isi jumlah temuan Not Ok ( 0,1,2,dst )', 'style' => "font-style:italic"])->hint('Not Ok : Terlalu kencang, terlalu longgar',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>                    
                </div>
            </div>
            <!-- /.box-body -->
          </div>    

 
<div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>JUMLAH KETIDAKSESUAIAN TERHADAP PARAMETER IDENTITAS PRODUK (UMUM)</b></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                    <div class="col-md-2">
                      <?= $form->field($model, 'kesesuaian_batch_primer',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : FG03A', 'style' => "font-style:italic"])->hint('Not Ok : No batch pada pack primer salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>

                    <div class="col-md-2">
                      <?= $form->field($model, 'kesesuaian_exp_primer',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput()->hint('Not Ok : Exp date pada karbok salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;'])->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'e.g: 2023/Jan/03', 'style' => "font-style:italic"],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy/M/dd',
                            ]
                        ]); 
                      ?>
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model, 'kesesuaian_batch_sekunder',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : FG03A', 'style' => "font-style:italic"])->hint('Not Ok : No batch pada pack sekunder salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                      <!-- /input-group -->
                    <div class="col-md-2">
                      <?= $form->field($model, 'kesesuaian_exp_sekunder',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput()->hint('Not Ok : Exp date pada karbok salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;'])->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'e.g: 2023/Jan/03', 'style' => "font-style:italic"],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy/M/dd',
                            ]
                        ]); 
                      ?>
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model, 'batch_innerbox',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : FG03A', 'style' => "font-style:italic"])->hint('Not Ok : No batch pada ibox salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model, 'exp_innerbox',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput()->hint('Not Ok : Exp date pada karbok salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;'])->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'e.g: 2023/Jan/03', 'style' => "font-style:italic"],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy/M/dd',
                            ]
                        ]); 
                      ?>
                    </div>
                    
                </div>    
                <div class="row">
                    <div class="col-md-2">
                      <?= $form->field($model, 'kesesuaian_batch_karbox',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : FG03A', 'style' => "font-style:italic"])->hint('Not Ok : No batch pada karbok salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    
                    <div class="col-md-2">
                      <?= $form->field($model, 'kesesuaian_exp_karbox',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput()->hint('Not Ok : Exp date pada karbok salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;'])->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'e.g: 2023/Jan/03', 'style' => "font-style:italic"],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy/M/dd',
                            ]
                        ]); 
                      ?>
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model, 'kesesuaian_barcode_produk',['template'=>"{label}\n{hint}\n{input}\n{error}"])->textInput(['placeholder' => 'e.g : 8993137693769', 'style' => "font-style:italic"])->hint('Not Ok : No batch pada pack primer salah',
                          ['style'=>'font-size:12px; font-style:italic; color: #000000;']);  ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?php
                          echo $form->field($model, 'kualitas_barcode_karbox')->widget(Select2::classname(), [
                              'data' => ['Ok'=>'Ok','Not Ok'=>'Not Ok'],
                              'options' => ['placeholder' => 'e.g : Ok', 'style' => "font-style:italic"],
                          ]);
                      ?>
                    </div>
                    <div class="col-md-2">
                    

      <!-- /input-group -->
                  </div>                     
                </div> 
               </div>
            <!-- /.box-body -->
          </div>  

   
<div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>JUMLAH KETIDAKSESUAIAN TERHADAP KELENGKAPAN KOMPONEN PRODUK (UMUM)</b></h3>
              <h5 style="font-style:italic">Not Ok : komponen tidak ada atau rusak</h5>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                    <div class="col-md-2">
                      <?php
                          echo $form->field($model, 'jumlah_komponen_penyusun')->widget(Select2::classname(), [
                              'data' => ['2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8'],
                              'options' => ['value'=> $jumlah_komponen_penyusun, 'placeholder' => 'e.g : 3', 'style' => "font-style:italic"],
                          ]);
                      ?>
                    </div>
                    <div class="col-md-12" id="komponen1">
                      <!-- /input-group -->
                      <?php echo $form->field($model, 'komponen_penyusun_1')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(ListKomponenPenyusun::find()->all()
                                                        ,'komponen','komponen'),
                                                        'options' => ['placeholder' => 'Isi nama komponen, e.g : Botol'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); 
                      ?>
                    </div>
                    <div class="col-md-12" id="temuan1">
                      <?= $form->field($model, 'temuan_penyusun_1')->textInput(['placeholder' => 'Jumlah temuan Not OK. e.g : 0 ', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-12" id="komponen2">
                      <!-- /input-group -->
                      <?php echo $form->field($model, 'komponen_penyusun_2')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(ListKomponenPenyusun::find()->all()
                                                        ,'komponen','komponen'),
                                                        'options' => ['placeholder' => 'Isi nama komponen, e.g : Botol'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); 
                      ?>
                    </div>
                    <div class="col-md-12" id="temuan2">
                      <?= $form->field($model, 'temuan_penyusun_2')->textInput(['placeholder' => 'Jumlah temuan Not OK. e.g : 0 ', 'style' => "font-style:italic"]) ?>
                    </div>
                    <div class="col-md-12" id="komponen3">
                      <?php echo $form->field($model, 'komponen_penyusun_3')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(ListKomponenPenyusun::find()->all()
                                                        ,'komponen','komponen'),
                                                        'options' => ['placeholder' => 'Isi nama komponen, e.g : Botol'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); 
                      ?>
                    </div>
                    <div class="col-md-12" id="temuan3">
                      <?= $form->field($model, 'temuan_penyusun_3')->textInput(['placeholder' => 'Jumlah temuan Not OK. e.g : 0 ', 'style' => "font-style:italic"]) ?>
                    </div>
                    <div class="col-md-12" id="komponen4">
                      <?php echo $form->field($model, 'komponen_penyusun_4')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(ListKomponenPenyusun::find()->all()
                                                        ,'komponen','komponen'),
                                                        'options' => ['placeholder' => 'Isi nama komponen, e.g : Botol'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); 
                      ?>
                    </div>
                    <div class="col-md-12" id="temuan4">
                      <?= $form->field($model, 'temuan_penyusun_4')->textInput(['placeholder' => 'Jumlah temuan Not OK. e.g : 0 ', 'style' => "font-style:italic"]) ?>
                    </div>
                    <div class="col-md-12" id="komponen5">
                      <?php echo $form->field($model, 'komponen_penyusun_5')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(ListKomponenPenyusun::find()->all()
                                                        ,'komponen','komponen'),
                                                        'options' => ['placeholder' => 'Isi nama komponen, e.g : Botol'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); 
                      ?>
                    </div>
                    <div class="col-md-12" id="temuan5">
                      <?= $form->field($model, 'temuan_penyusun_5')->textInput(['placeholder' => 'Jumlah temuan Not OK. e.g : 0 ', 'style' => "font-style:italic"]) ?>
                    </div>
                    <div class="col-md-12" id="komponen6">
                      <?php echo $form->field($model, 'komponen_penyusun_6')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(ListKomponenPenyusun::find()->all()
                                                        ,'komponen','komponen'),
                                                        'options' => ['placeholder' => 'Isi nama komponen, e.g : Botol'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); 
                      ?>
                    </div>
                    <div class="col-md-12" id="temuan6">
                      <?= $form->field($model, 'temuan_penyusun_6')->textInput(['placeholder' => 'Jumlah temuan Not OK. e.g : 0 ', 'style' => "font-style:italic"]) ?>
                    </div>
                    <div class="col-md-12" id="komponen7">
                      <?php echo $form->field($model, 'komponen_penyusun_7')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(ListKomponenPenyusun::find()->all()
                                                        ,'komponen','komponen'),
                                                        'options' => ['placeholder' => 'Isi nama komponen, e.g : Botol'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); 
                      ?>
                    </div>
                    <div class="col-md-12" id="temuan7">
                      <?= $form->field($model, 'temuan_penyusun_7')->textInput(['placeholder' => 'Jumlah temuan Not OK. e.g : 0 ', 'style' => "font-style:italic"]) ?>
                    </div>
                    <div class="col-md-12" id="komponen8">
                      <?php echo $form->field($model, 'komponen_penyusun_8')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(ListKomponenPenyusun::find()->all()
                                                        ,'komponen','komponen'),
                                                        'options' => ['placeholder' => 'Isi nama komponen, e.g : Botol'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); 
                      ?>
                    </div>
                    <div class="col-md-12" id="temuan8">
                      <?= $form->field($model, 'temuan_penyusun_8')->textInput(['placeholder' => 'Jumlah temuan Not OK. e.g : 0 ', 'style' => "font-style:italic"]) ?>
                    </div>
               </div>
            <!-- /.box-body -->
            </div>
          </div>
<div class="box box-primary box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>PARAMETER SAMPLING AKHIR</b></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                      <?= $form->field($model, 'netto')->textInput(['placeholder' => 'e.g : 20.5 - 20.7 - 20.4', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model, 'jumlah_per_karbox')->textInput(['placeholder' => 'e.g : 36', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model, 'jumlah_per_palet')->textInput(['placeholder' => 'e.g : 3600', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model, 'jumlah_sampel_trial')->textInput(['placeholder' => 'e.g : 0,1,2,dst', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?php
                          echo $form->field($model, 'kesesuaian_warna_lakban')->widget(Select2::classname(), [
                              'data' => ['Hijau'=>'Hijau','Oranye'=>'Oranye','Ungu'=>'Ungu','Putih'=>'Putih','Tosca'=>'Tosca','Merah Muda'=>'Merah Muda'],
                              'options' => ['placeholder' => 'e.g : Hijau', 'style' => "font-style:italic"],
                          ]);
                      ?>
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model, 'sampel_retain_yang_sudah_diambil')->textInput(['value'=>$sampel_retain,'readOnly'=>true]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model, 'sampel_retain')->textInput(['placeholder' => 'e.g : 0,1,2,dst', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                   <div class="col-md-2">
                      <?= $form->field($model, 'sampel_mikro_yang_sudah_diambil')->textInput(['value'=>$sampel_retain,'readOnly'=>true]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-2">
                      <?= $form->field($model, 'sampel_mikro')->textInput(['placeholder' => 'e.g : 0,1,2,dst', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-3">
                      <?= $form->field($model, 'inspector_fg')->textInput(['placeholder' => 'e.g : YA', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    <div class="col-md-3">
                      <!-- /input-group -->
                      <?php
                          echo $form->field($model, 'status_siklus')->widget(Select2::classname(), [
                              'data' => ['Release'=>'Release','Rework'=>'Rework','Reject'=>'Reject','Pending'=>'Pending','Pending Mikro'=>'Pending Mikro'],
                              'options' => ['placeholder' => 'e.g : Release', 'style' => "font-style:italic"],
                          ]);
                          
                      ?>
                    </div>
                    <div class="col-md-3">
                      <!-- /input-group -->
                      <?php
                          echo $form->field($model, 'status_produk')->widget(Select2::classname(), [
                              'data' => ['Release'=>'Release','Reject'=>'Reject'],
                              'options' => ['placeholder' => 'e.g : Release', 'style' => "font-style:italic"],
                          ]);
                          
                      ?>
                    </div>
                    <div class="col-md-3" id="investigasi">
                      <?= $form->field($model, 'investigasi_ketidaksesuaian')->textArea(['placeholder' => 'e.g : Ditemukan 10 pcs tube dengan seal bocor dari 50 sample pengecekan, kebocoran terjadi pada bagian ujung seal FG', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                </div>   
                    
                <div class="row">
                    <div class="col-md-6" id="penanganan">
                      <?php
                          echo $form->field($model, 'penanganan_ketidaksesuaian')->widget(Select2::classname(), [
                              'data' => ['Sortir & Rework (100 %)'=>'Sortir & Rework (100 %)','Sortir & Rework (sampling)'=>'Sortir & Rework (sampling)','Tukar Batch Packaging'=>'Tukar Batch Packaging','Tukar Batch Bulk'=>'Tukar Batch Bulk','Batch Split'=>'Batch Split','Pindah Mesin'=>'Pindah Mesin'],
                              'options' => ['placeholder' => 'e.g : sortir & rework (100 %), tukar batch packaging', 'style' => "font-style:italic"],
                          ]);
                      ?>
                    </div>
                    <div class="col-md-3" id="metode">
                      <?= $form->field($model, 'metode_rework')->textInput(['placeholder' => 'e.g : bulk pada seluruh FG dikeluarkan lalu disaring, FG yang terkontaminasi kemudian dihitung sebagai not ok', 'style' => "font-style:italic"]) ?>
                      <!-- /input-group -->
                    </div>
                    

                </div>
            </div>
            <!-- /.box-body -->
          </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

              </div>
            </div>
        </div>
    </div>
</div>

<?php
$script = <<< JS

    $('#hide').hide();
    $('#hide1').hide();
    $('#1').hide();
    $('#2').hide();
    $('#3').hide();
    $('#4').hide();
    $('#5').hide();
    $('#6').hide();
    $('#7').hide();
    $('#8').hide();
    $('#komponen1').show();
    $('#komponen2').show();
    $('#komponen3').show();
    $('#komponen4').show();
    $('#komponen5').show();
    $('#komponen6').show();
    $('#komponen7').show();
    $('#komponen8').show();
    $('#temuan1').show();
    $('#temuan2').show();
    $('#temuan3').show();
    $('#temuan4').show();
    $('#temuan5').show();
    $('#temuan6').show();
    $('#temuan7').show();
    $('#temuan8').show();
    $('#penanganan').hide();
    $('#investigasi').hide();
    $('#metode').hide();
    
    var sediaan = $('#identitaspengecekanfg-sediaan').val();
    if (sediaan == 'S')
    {
        $('#semsol').show();
        $('#powder').hide();
        $('#liquid').hide();
        $('#liquid1').hide();
    }else if(sediaan == 'L' )
    {
        $('#semsol').hide();
        $('#powder').hide();
        $('#liquid').show();
        $('#liquid1').show();
    }else{
        $('#semsol').hide();
        $('#liquid').hide();
        $('#powder').show();
        $('#liquid1').hide();
    }

    var sisa = $('#pengecekanumum-sisa_aql_yang_harus_dicek').val();
    if (sisa_aql == '0')
    {
        $('#sisa').hide();
    }else{
        $('#sisa').show();
    }

    $('#pengecekanumum-status_produk').change(function(){

        var status_produk = $('#pengecekanumum-status_produk').val();

        if (status_produk == 'Release','Reject'){
          $('#penanganan').fadeIn();
          $('#investigasi').fadeIn();
          $('#metode').fadeIn();
        }else{
          $('#penanganan').fadeOut();
          $('#investigasi').fadeOut();
          $('#metode').fadeOut();
        }
    });

    $('#pengecekanliquid-tube_dedusting').change(function(){

        var tube_dedusting = $('#pengecekanliquid-tube_dedusting').val();

        if (tube_dedusting == 'Tidak Aktif'){
          $('#1').fadeIn();
        }else{
          $('#1').hide();
        }
    });

    $('#pengecekanliquid-strainer').change(function(){

        var strainer = $('#pengecekanliquid-strainer').val();

        if (strainer == 'Tidak Aktif'){
          $('#2').fadeIn();
        }else{
          $('#2').hide();
        }
    });

    $('#pengecekanliquid-bad_tube_ejector').change(function(){

        var bad_tube_ejector = $('#pengecekanliquid-bad_tube_ejector').val();

        if (bad_tube_ejector == 'Tidak Aktif'){
          $('#3').fadeIn();
        }else{
          $('#3').hide();
        }
    });

    $('#pengecekanliquid-product_sensing').change(function(){

        var product_sensing = $('#pengecekanliquid-product_sensing').val();

        if (product_sensing == 'Tidak Aktif'){
          $('#4').fadeIn();
        }else{
          $('#4').hide();
        }
    });

    $('#pengecekanliquid-bottle_dedusting').change(function(){

        var bottle_dedusting = $('#pengecekanliquid-bottle_dedusting').val();

        if (bottle_dedusting == 'Tidak Aktif'){
          $('#5').fadeIn();
        }else{
          $('#5').hide();
        }
    });

    $('#pengecekanliquid-auto_capper').change(function(){

        var auto_capper = $('#pengecekanliquid-auto_capper').val();

        if (auto_capper == 'Tidak Aktif'){
          $('#6').fadeIn();
        }else{
          $('#6').hide();
        }
    });

    $('#pengecekanliquid-plug_checking').change(function(){

        var plug_checking = $('#pengecekanliquid-plug_checking').val();

        if (plug_checking == 'Tidak Aktif'){
          $('#7').fadeIn();
        }else{
          $('#7').hide();
        }
    });

    $('#pengecekanliquid-pot_dedusting').change(function(){

        var pot_dedusting = $('#pengecekanliquid-pot_dedusting').val();

        if (pot_dedusting == 'Tidak Aktif'){
          $('#8').fadeIn();
        }else{
          $('#8').hide();
        }
    });

    $('#pengecekanumum-jumlah_komponen_penyusun').change(function(){

        var jumlah_komponen_penyusun = $('#pengecekanumum-jumlah_komponen_penyusun').val();

        if (jumlah_komponen_penyusun == '2'){
          $('#komponen2').fadeIn();
          $('#temuan2').fadeIn();
          $('#komponen3').fadeOut();
          $('#komponen4').fadeOut();
          $('#komponen5').fadeOut();
          $('#komponen6').fadeOut();
          $('#komponen7').fadeOut();
          $('#komponen8').fadeOut();
          $('#temuan3').fadeOut();
          $('#temuan4').fadeOut();
          $('#temuan5').fadeOut();
          $('#temuan6').fadeOut();
          $('#temuan7').fadeOut();
          $('#temuan8').fadeOut();
        }else if (jumlah_komponen_penyusun == '3'){
          $('#komponen2').fadeIn();
          $('#komponen3').fadeIn();
          $('#komponen4').fadeOut();
          $('#komponen5').fadeOut();
          $('#komponen6').fadeOut();
          $('#komponen7').fadeOut();
          $('#komponen8').fadeOut();
          $('#temuan2').fadeIn();
          $('#temuan3').fadeIn();
          $('#temuan4').fadeOut();
          $('#temuan5').fadeOut();
          $('#temuan6').fadeOut();
          $('#temuan7').fadeOut();
          $('#temuan8').fadeOut();
        }else if (jumlah_komponen_penyusun == '4'){
          $('#komponen2').fadeIn();
          $('#komponen3').fadeIn();
          $('#komponen4').fadeIn();
          $('#komponen5').fadeOut();
          $('#komponen6').fadeOut();
          $('#komponen7').fadeOut();
          $('#komponen8').fadeOut();
          $('#temuan2').fadeIn();
          $('#temuan3').fadeIn();
          $('#temuan4').fadeIn();
          $('#temuan5').fadeOut();
          $('#temuan6').fadeOut();
          $('#temuan7').fadeOut();
          $('#temuan8').fadeOut();
        }else if (jumlah_komponen_penyusun == '5'){
          $('#komponen2').fadeIn();
          $('#komponen3').fadeIn();
          $('#komponen4').fadeIn();
          $('#komponen5').fadeIn();
          $('#komponen6').fadeOut();
          $('#komponen7').fadeOut();
          $('#komponen8').fadeOut();
          $('#temuan2').fadeIn();
          $('#temuan3').fadeIn();
          $('#temuan4').fadeIn();
          $('#temuan5').fadeIn();
          $('#temuan6').fadeOut();
          $('#temuan7').fadeOut();
          $('#temuan8').fadeOut();
        }else if (jumlah_komponen_penyusun == '6'){
          $('#komponen2').fadeIn();
          $('#komponen3').fadeIn();
          $('#komponen4').fadeIn();
          $('#komponen5').fadeIn();
          $('#komponen6').fadeIn();
          $('#komponen7').fadeOut();
          $('#komponen8').fadeOut();
          $('#temuan2').fadeIn();
          $('#temuan3').fadeIn();
          $('#temuan4').fadeIn();
          $('#temuan5').fadeIn();
          $('#temuan6').fadeIn();
          $('#temuan7').fadeOut();
          $('#temuan8').fadeOut();
        }else if (jumlah_komponen_penyusun == '7'){
          $('#komponen2').fadeIn();
          $('#komponen3').fadeIn();
          $('#komponen4').fadeIn();
          $('#komponen5').fadeIn();
          $('#komponen6').fadeIn();
          $('#komponen7').fadeIn();
          $('#komponen8').fadeOut();
          $('#temuan2').fadeIn();
          $('#temuan3').fadeIn();
          $('#temuan4').fadeIn();
          $('#temuan5').fadeIn();
          $('#temuan6').fadeIn();
          $('#temuan7').fadeIn();
          $('#temuan8').fadeOut();
        }else{
          $('#komponen1').show();
          $('#komponen2').show();
          $('#komponen3').show();
          $('#komponen4').show();
          $('#komponen5').show();
          $('#komponen6').show();
          $('#komponen7').show();
          $('#komponen8').show();
          $('#temuan1').show();
          $('#temuan2').show();
          $('#temuan3').show();
          $('#temuan4').show();
          $('#temuan5').show();
          $('#temuan6').show();
          $('#temuan7').show();
          $('#temuan8').show();
        }
    });


    // $('#flowinputsnfg-jenis_kemas').change(function(){

    //     var jenis_kemas = $('#flowinputsnfg-jenis_kemas').val();

    //     $.get('index.php?r=flow-input-snfg/get-lanjutan-kemas2',{ snfg : snfg , jenis_kemas : jenis_kemas , posisi : posisi  }, function(data){
    //         var data = $.parseJSON(data);
    //         $('#flowinputsnfg-lanjutan').attr('value',data.lanjutan);

    //     });

    //     $.post("index.php?r=master-data-line/get-line-kemas-snfg&snfg="+$('#flowinputsnfg-snfg').val()+"&jenis_kemas="+$('#flowinputsnfg-jenis_kemas').val(), function (data){
    //         $("select#flowinputsnfg-nama_line").html(data);
    //     });

    //     $('#lanjutan').fadeIn();

    // });  



JS;
$this->registerJs($script);
?>