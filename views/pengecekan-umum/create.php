<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PengecekanUmum */

// $this->title = 'Create Pengecekan Umum';
// $this->params['breadcrumbs'][] = ['label' => 'Pengecekan Umums', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengecekan-umum-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_fg' => $model_fg,
        'model_semsol' => $model_semsol,
        'model_powder' => $model_powder,
        'model_liquid' => $model_liquid,
        'nama' => $nama,
        'streamline' => $streamline,
        'mpq' => $mpq,
        'sediaan' => $sediaan,
        'nama_line' => $nama_line,
        'nobatch' => $nobatch,
        'snfg' => $snfg,
        'palet'=>$palet,
        'siklus'=>$siklus,
        'start_time'=>$start_time,
        'due'=>$due,
        'aql_sisa' =>$aql_sisa,
        'sn' => $sn,
        'sn1' => $sn1,
        'aql_per_palet' => $aql_per_palet,
        'jumlah_komponen_penyusun' => $jumlah_komponen_penyusun,
        'sampel_mikro' => $sampel_mikro,
        'sampel_retain' => $sampel_retain,
        'komponen_penyusun_1' => $komponen_penyusun_1,
        'komponen_penyusun_2' => $komponen_penyusun_2,
        'komponen_penyusun_3' => $komponen_penyusun_3,
        'komponen_penyusun_4' => $komponen_penyusun_4,
        'komponen_penyusun_5' => $komponen_penyusun_5,
        'komponen_penyusun_6' => $komponen_penyusun_6,
        'komponen_penyusun_7' => $komponen_penyusun_7,
        'komponen_penyusun_8' => $komponen_penyusun_8,
    ]) ?>

</div>
