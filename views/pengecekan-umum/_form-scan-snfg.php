<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<div>
    <div class="box box-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
        <h3 class="widget-user-username"><b>Quality Finished Goods</b></h3>
        <h5 class="widget-user-desc">FORM IN-LINE INSPECTION PRODUCT</h5>
      </div>
      <div class="widget-user-image">
        <img class="img-circle" src="../web/images/clipboard-icon.png" alt="User Avatar">
      </div>

      <div class="box-footer">
        <div class="row">
          <div class="box-body">
          <div class="box-body">
            <div class="zoom">
                <?=Html::a('SCAN WITH QR', ['qc-bulk-entry/create-tab'], ['class' => 'btn btn-success btn-block']);?>
            </div>
            <p></p>
              <div class="box-body">
                 
                      

                  

              </div><!-- /.box-body -->
            </div><!-- /.box -->
            </div><!-- /.box -->
          </div>
      </div>
    </div>
</div>