<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PengecekanUmum */

// $this->title = 'Update Pengecekan Umum: ' . $model->id;
// $this->params['breadcrumbs'][] = ['label' => 'Pengecekan Umum', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pengecekan-umum-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_semsol', [
        'model' => $model,
        'model_fg' => $model_fg,
        'model_semsol' => $model_semsol,
        'item_category' => $item_category,
        // 'extra_proses' => $extra_proses,
        // 'pengerjaan_extra_proses' => $pengerjaan_extra_proses,
        // // 'model_powder' => $model_powder,
        // // 'model_liquid' => $model_liquid,
        // 'nama' => $nama,
        // 'jumlah_komponen_bulk' => $jumlah_komponen_bulk,
        // // 'streamline' => $streamline,
        // 'mpq' => $mpq,
        // 'sediaan' => $sediaan,
        // 'sistem_cek' => $sistem_cek,
        // 'nama_line' => $nama_line,
        // 'nobatch' => $nobatch,
        // 'snfg' => $snfg,
        // 'palet'=>$palet,
        // 'siklus'=>$siklus,
        // 'start_time'=>$start_time,
        // 'aql_sisa' =>$aql_sisa,
        // // 'sn' => $sn,
        // // 'sn1' => $sn1,
        // 'aql_per_palet' => $aql_per_palet,
        // 'jumlah_komponen_penyusun' => $jumlah_komponen_penyusun,
        // 'sampel_mikro' => $sampel_mikro,
        // 'sampel_retain' => $sampel_retain,
        // 'komponen_penyusun_1' => $komponen_penyusun_1,
        // 'komponen_penyusun_2' => $komponen_penyusun_2,
        // 'komponen_penyusun_3' => $komponen_penyusun_3,
        // 'komponen_penyusun_4' => $komponen_penyusun_4,
        // 'komponen_penyusun_5' => $komponen_penyusun_5,
        // 'komponen_penyusun_6' => $komponen_penyusun_6,
        // 'komponen_penyusun_7' => $komponen_penyusun_7,
        // 'komponen_penyusun_8' => $komponen_penyusun_8,
    ]) ?>

</div>
