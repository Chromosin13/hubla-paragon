<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PengecekanUmum */

// $this->title = 'Update Pengecekan Umum: ' . $model->id;
// $this->params['breadcrumbs'][] = ['label' => 'Pengecekan Umum', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pengecekan-umum-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_fg' => $model_fg,
        // 'model_semsol' => $model_semsol,
        // 'model_powder' => $model_powder,
        'model_liquid' => $model_liquid,
        'nama' => $nama,
        'streamline' => $streamline,
        'mpq' => $mpq,
        'sediaan' => $sediaan,
        'nama_line' => $nama_line,
        'nobatch' => $nobatch,
        'snfg' => $snfg,
        'palet'=>$palet,
        'siklus'=>$siklus,
        'start_time'=>$start_time,
        'aql_sisa' =>$aql_sisa,
        'sn' => $sn,
        // 'sn1' => $sn1,
        'aql_per_palet' => $aql_per_palet,
        'jumlah_komponen_penyusun' => $jumlah_komponen_penyusun,
        'sampel_mikro' => $sampel_mikro,
        'sampel_retain' => $sampel_retain,
        'komponen_penyusun_1' => $komponen_penyusun_1,
        'komponen_penyusun_2' => $komponen_penyusun_2,
        'komponen_penyusun_3' => $komponen_penyusun_3,
        'komponen_penyusun_4' => $komponen_penyusun_4,
        'komponen_penyusun_5' => $komponen_penyusun_5,
        'komponen_penyusun_6' => $komponen_penyusun_6,
        'komponen_penyusun_7' => $komponen_penyusun_7,
        'komponen_penyusun_8' => $komponen_penyusun_8,
    ]) ?>

</div>

<?php
$script = <<< JS

    var sediaan = $('#identitaspengecekanfg-sediaan').val();
    if (sediaan == 'S')
    {
        $('#semsol').show();
        $('#powder').hide();
        $('#liquid').hide();
        $('#liquid1').hide();
    }else if(sediaan == 'L' )
    {
        $('#semsol').hide();
        $('#powder').hide();
        $('#liquid').show();
        $('#liquid1').show();
    }else{
        $('#semsol').hide();
        $('#liquid').hide();
        $('#powder').show();
        $('#liquid1').hide();
    }

    JS;
$this->registerJs($script);
?>
