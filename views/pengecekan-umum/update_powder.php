<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PengecekanUmum */

// $this->title = 'Update Pengecekan Umum: ' . $model->id;
// $this->params['breadcrumbs'][] = ['label' => 'Pengecekan Umum', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pengecekan-umum-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_powder', [
        'model' => $model,
        'model_fg' => $model_fg,
        'model_powder' => $model_powder,
        'item_category' => $item_category,
    ]) ?>

</div>
