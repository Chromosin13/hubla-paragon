<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PengecekanUmumSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengecekan-umum-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'nama_produk') ?>

    <?= $form->field($model, 'batch_produk') ?>

    <?= $form->field($model, 'kode_sl') ?>

    <?= $form->field($model, 'no_notifikasi') ?>

    <?= $form->field($model, 'barcode_produk') ?>

    <?php // echo $form->field($model, 'exp_date') ?>

    <?php // echo $form->field($model, 'no_mo') ?>

    <?php // echo $form->field($model, 'line_kemas') ?>

    <?php // echo $form->field($model, 'due_date') ?>

    <?php // echo $form->field($model, 'jumlah_mpq') ?>

    <?php // echo $form->field($model, 'total_aql') ?>

    <?php // echo $form->field($model, 'jumlah_palet') ?>

    <?php // echo $form->field($model, 'aql_per_palet') ?>

    <?php // echo $form->field($model, 'aql_per_siklus') ?>

    <?php // echo $form->field($model, 'jam_mulai') ?>

    <?php // echo $form->field($model, 'jam_selesai') ?>

    <?php // echo $form->field($model, 'visual_performance_bulk') ?>

    <?php // echo $form->field($model, 'fisik_packaging') ?>

    <?php // echo $form->field($model, 'fungsional_packaging') ?>

    <?php // echo $form->field($model, 'kesesuaian_warna_packaging') ?>

    <?php // echo $form->field($model, 'kesesuaian_identitas_packaging') ?>

    <?php // echo $form->field($model, 'performance_segel') ?>

    <?php // echo $form->field($model, 'kesesuaian_batch_primer') ?>

    <?php // echo $form->field($model, 'kesesuaian_exp_primer') ?>

    <?php // echo $form->field($model, 'kesesuaian_batch_sekunder') ?>

    <?php // echo $form->field($model, 'kesesuaian_exp_sekunder') ?>

    <?php // echo $form->field($model, 'kesesuaian_batch_karbox') ?>

    <?php // echo $form->field($model, 'kesesuaian_exp_karbox') ?>

    <?php // echo $form->field($model, 'kesesuaian_notifikasi_produk') ?>

    <?php // echo $form->field($model, 'kesesuaian_barcode_produk') ?>

    <?php // echo $form->field($model, 'kualitas_barcode_karbox') ?>

    <?php // echo $form->field($model, 'kelengkapan_komponen') ?>

    <?php // echo $form->field($model, 'kesesuaian_kelengkapan_komponen') ?>

    <?php // echo $form->field($model, 'netto') ?>

    <?php // echo $form->field($model, 'jumlah_per_karbox') ?>

    <?php // echo $form->field($model, 'kesesuaian_warna_lakban') ?>

    <?php // echo $form->field($model, 'jumlah_sampel_trial') ?>

    <?php // echo $form->field($model, 'sampel_retain') ?>

    <?php // echo $form->field($model, 'sampel_mikro') ?>

    <?php // echo $form->field($model, 'inspector_fg') ?>

    <?php // echo $form->field($model, 'investigasi_ketidaksesuaian') ?>

    <?php // echo $form->field($model, 'penanganan_ketidaksesuaian') ?>

    <?php // echo $form->field($model, 'approval_ketidaksesuaian') ?>

    <?php // echo $form->field($model, 'status_produk') ?>

    <?php // echo $form->field($model, 'label_bersih_bulk') ?>

    <?php // echo $form->field($model, 'label_bersih_mesin') ?>

    <?php // echo $form->field($model, 'plant') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'nama_produk_sebelumnya') ?>

    <?php // echo $form->field($model, 'batch_produk_sebelumnya') ?>

    <?php // echo $form->field($model, 'air_bilasan_akhir') ?>

    <?php // echo $form->field($model, 'verifikasi_trial_filling') ?>

    <?php // echo $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'serial_number') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
