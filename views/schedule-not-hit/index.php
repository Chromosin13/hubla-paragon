<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use app\models\LogRework;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogReworkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<style >
#sediaan {
    background: #DDD;
    border: 3px solid white;
    color: #3C8DBC;
    padding: 1vh;
    border-radius: 8px;
    font-weight: bold;
}
</style>


<div class="callout callout-success">
  <h3>Loss Tree</h3>

  <!-- <p> Data Realtime </p> -->
  <h4> Cut off : <?php echo $yesterday; ?> 07:00:00 s/d <?php echo $current_date; ?> 07:00:00</h4>

</div>

<p></p>
<div class= "row">
    <div class="col-md-1">
        <span>
          <select class="" name="sediaan" id="sediaan" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
            <?php if ($sediaan == '') : ?>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index" style="" selected="selected">All Sediaan</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=L">Liquid</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=P">Powder</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=S">Semisolid</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=V">Varcos</option>
            <?php elseif ($sediaan == 'L'): ?>
             <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index" style="" >All Sediaan</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=L" selected="selected">Liquid</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=P">Powder</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=S">Semisolid</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=V">Varcos</option>

            <?php elseif ($sediaan == 'P'): ?>

             <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index" style="" >All Sediaan</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=L" >Liquid</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=P" selected="selected">Powder</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=S">Semisolid</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=V">Varcos</option>
            <?php elseif ($sediaan == 'S'): ?>
             <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index" style="" >All Sediaan</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=L" >Liquid</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=P">Powder</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=S" selected="selected">Semisolid</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=V">Varcos</option>
            <?php else: ?>
             <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index" style="" >All Sediaan</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=L" >Liquid</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=P">Powder</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=S">Semisolid</option>
            <option value="http://10.3.5.102/flowreport_development/web/index.php?r=schedule-not-hit/index&sediaan=V" selected="selected">Varcos</option>
            <?php endif; ?>
          </select>
        </span>        
    </div>

    <?php if (Yii::$app->user->identity->username == 'produksi') : ?>
    <div class="col-md-1">
        <a href="http://10.3.5.102/flowreport_development/web/index.php?r=loss-tree/validate">
            <button type="button" class="btn btn-block btn-success btn-flat">Validate</button>
        </a>
    </div>

<?php else : ?>
<?php endif; ?>

    <div class="pull-right">
        Download All Data Here
        <?php 

            $gridColumns = [
            ['class' => 'kartik\grid\SerialColumn'],
                'id',
                'snfg',
                'start',
                'schedule_due',
                'koitem_fg',
                'nama_fg',
                'jumlah_pcs',
                'qty_release',
                'qty_nh',
                'is_done',
                'timestamp_check',
                'remark',
                'level_1',
                'level_2',
                'level_3',
                'corrective_action',
                'preventive_action',
                'pic',
                'due',
            ['class' => 'kartik\grid\ActionColumn'],
            ];

            echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
      
        ]);?>

    </div>
</div>


<p></p>  

<div class="log-rework-index">
        
    <?php echo
     GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            // 'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'danger', 'heading'=>'Schedule Not Hit'],
            'formatter' => [
                'class' => 'yii\i18n\Formatter',
                'timeZone' => 'Asia/Jakarta'
            ],
            'toolbar' => [
            ],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'schedule_due',
            'snfg',
            'koitem_fg',
            'nama_fg',
            'jumlah_pcs',
            'qty_release',
            'qty_nh',
            'remark',
            'level_1',
            'level_2',
            'level_3',
            [
                'headerOptions'=>['style'=>'min-width:150px'],
                'attribute'=>'corrective_action',
                'format'=>'ntext',
            ],
            [
                'headerOptions'=>['style'=>'min-width:150px'],
                'attribute'=>'preventive_action',
                'format'=>'ntext',
            ],
            // 'corrective_action',
            // 'preventive_action',
            'pic',
            'due',
            [
              'class' => 'kartik\grid\ActionColumn',
              'template' => '{update-losstree}',
              'headerOptions' => ['style' => 'min-width:100px;'],
              // 'pageSummary' => true,
              // 'contentOptions'=>['style'=>"width:250px;"],
              'buttons' => [
                  // 'view' => function ($url, $model) {
                  //     return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                  //                 'title' => Yii::t('app', 'lead-view'),
                  //     ]);
                  // },

              'update-losstree' => function ($url, $dataProvider) {
                  return Html::a('<span class="btn btn-sm btn-default fa fa-pencil"></span>', $url, [
                              'title' => Yii::t('app', 'update'),
                              // 'class' => 'modalUpdateBahan grid-action',
                              // 'data-toogle'=>'modalbahanbaku',
                              // 'data-target'=>'#activity-modal',
                              // 'value'=>Url::to('index.php?r=jurnal-bahan-baku/update-bahan&id='.$key)
                              'onclick'=>"update_losstree('".$dataProvider->snfg."')",
                  ]);
              }

              ],
              'urlCreator' => function ($action, $model, $key, $index) {
                // if ($action === 'view') {
                //     $url ='index.php?r=client-login/lead-view&id='.$model->id;
                //     return $url;
                // }

                // if ($action === 'update') {
                //     $url ='index.php?r=client-login/lead-update&id='.$model->id;
                //     return $url;
                // }
                // if ($action === 'delete') {
                //     $url ='index.php?r=jurnal-bahan-baku/delete-bahan&id='.$model->id;
                //     return $url;
                // }

              }
          ],//action column

        ],
    ]); ?>
</div>

<script>
function update_losstree(snfg){
    // var value = 'index.php?r=jurnal-bahan-baku/update-bahan&id='+id+'&kode_unik_formula='+kuf+'&pos=Formulator';
    // console.log(value);
    // $('#modalbahanbaku').modal('show')
      // find id
      // .find('#modalContentUpdateBahan')
      // load id
      // .load(value);
    // console.log(id);
    window.location = "index.php?r=loss-tree/create&snfg="+snfg;
}

</script>
