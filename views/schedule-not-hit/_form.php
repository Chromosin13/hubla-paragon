<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScheduleNotHit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="schedule-not-hit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'start')->textInput() ?>

    <?= $form->field($model, 'schedule_due')->textInput() ?>

    <?= $form->field($model, 'koitem_fg')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'jumlah_pcs')->textInput() ?>

    <?= $form->field($model, 'qty_release')->textInput() ?>

    <?= $form->field($model, 'qty_nh')->textInput() ?>

    <?= $form->field($model, 'is_done')->textInput() ?>

    <?= $form->field($model, 'timestamp_check')->textInput() ?>

    <?= $form->field($model, 'remark')->textInput() ?>

    <?= $form->field($model, 'level_1')->textInput() ?>

    <?= $form->field($model, 'level_2')->textInput() ?>

    <?= $form->field($model, 'level_3')->textInput() ?>

    <?= $form->field($model, 'corrective_action')->textInput() ?>

    <?= $form->field($model, 'preventive_action')->textInput() ?>

    <?= $form->field($model, 'pic')->textInput() ?>

    <?= $form->field($model, 'due')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
