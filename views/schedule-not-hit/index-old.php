<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ScheduleNotHitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Schedule Not Hits';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schedule-not-hit-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Schedule Not Hit', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'snfg',
            'start',
            'due',
            'koitem_fg',
            // 'nama_fg',
            // 'jumlah_pcs',
            // 'qty',
            // 'is_done',
            // 'timestamp_check',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
