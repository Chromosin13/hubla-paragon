<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ScheduleNotHit */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Schedule Not Hits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schedule-not-hit-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'snfg',
            'start',
            'schedule_due',
            'koitem_fg',
            'nama_fg',
            'jumlah_pcs',
            'qty_release',
            'qty_nh',
            'is_done',
            'timestamp_check',
            'remark',
            'level_1',
            'level_2',
            'level_3',
            'corrective_action',
            'preventive_action',
            'pic',
            'due',
        ],
    ]) ?>

</div>
