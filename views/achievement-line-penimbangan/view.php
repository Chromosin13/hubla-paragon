<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLinePenimbangan */

$this->title = $model->row_number;
$this->params['breadcrumbs'][] = ['label' => 'Achievement Line Penimbangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-line-penimbangan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->row_number], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->row_number], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'row_number',
            'tanggal',
            'nama_line',
            'snfg',
            'snfg_komponen',
            'nama_item',
            'leadtime',
            'leadtime_std',
            'status_leadtime:ntext',
            'plan_start_timbang',
            'status_plan:ntext',
            'posisi:ntext',
        ],
    ]) ?>

</div>
