<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLinePenimbanganSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="achievement-line-penimbangan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'row_number') ?>

    <?= $form->field($model, 'tanggal') ?>

    <?= $form->field($model, 'nama_line') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'snfg_komponen') ?>

    <?php // echo $form->field($model, 'nama_item') ?>

    <?php // echo $form->field($model, 'leadtime') ?>

    <?php // echo $form->field($model, 'leadtime_std') ?>

    <?php // echo $form->field($model, 'status_leadtime') ?>

    <?php // echo $form->field($model, 'plan_start_timbang') ?>

    <?php // echo $form->field($model, 'status_plan') ?>

    <?php // echo $form->field($model, 'posisi') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
