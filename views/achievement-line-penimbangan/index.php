<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AchievementLinePenimbanganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Achievement Line Penimbangans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-line-penimbangan-index">

    <!-- <h1><?= Html::encode($this->title= 'Achievement Line Penimbangan') ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <div class="box-body">
                <div class="row">
                <div class="col-md-12">

                <div class="col-md-6">
                    


                </div>

                <?php


                                 $count = Yii::$app->db->createCommand("
                                                                  SELECT count(case when status_leadtime='ACHIEVED' then 1 end) as achieved,
                                                                       count(case when status_leadtime='NOT ACHIEVED' then 1 end) as not_achieved
                                                                    FROM 
                                                                        (SELECT a.*,
                                                                            slp.leadtime as std_leadtime,
                                                                            case when a.leadtime<=slp.leadtime then 'ACHIEVED'
                                                                            else 'NOT ACHIEVED' end as status_leadtime
                                                                        FROM
                                                                            (SELECT 
                                                                                    snfg_komponen
                                                                                    ,snfg
                                                                                    ,jenis_penimbangan
                                                                                    ,max(tanggal_stop) tanggal_stop
                                                                                    ,sum(is_done) is_done
                                                                                    ,sum(leadtime) leadtime
                                                                                FROM penimbangan_leadtime_1
                                                                                GROUP BY snfg_komponen
                                                                                    ,snfg
                                                                                    ,jenis_penimbangan) a
                                                                            LEFT JOIN std_leadtime_penimbangan slp on slp.koitem_bulk=split_part(a.snfg_komponen,'/',1)
                                                                            WHERE a.is_done>0
                                                                    ) b")->queryAll();

                                 $implode_achieved = intval(implode(',',array_column($count,'achieved')));     
                                 $implode_nachieved = intval(implode(',',array_column($count,'not_achieved')));      

                                  $tanggalList = Yii::$app->db->createCommand("
                                                                              SELECT tanggal_stop,
                                                                               count(case when status_leadtime='ACHIEVED' then 1 end) as achieved,
                                                                               count(case when status_leadtime='NOT ACHIEVED' then 1 end) as not_achieved,
                                                                               count(status_leadtime) as all
                                                                        FROM 
                                                                            (SELECT a.*,
                                                                                slp.leadtime as std_leadtime,
                                                                                case when a.leadtime<=slp.leadtime then 'ACHIEVED'
                                                                                else 'NOT ACHIEVED' end as status_leadtime
                                                                            FROM
                                                                                (SELECT 
                                                                                        snfg_komponen
                                                                                        ,snfg
                                                                                        ,jenis_penimbangan
                                                                                        ,max(tanggal_stop) tanggal_stop
                                                                                        ,sum(is_done) is_done
                                                                                        ,sum(leadtime) leadtime
                                                                                    FROM penimbangan_leadtime_1
                                                                                    GROUP BY snfg_komponen
                                                                                        ,snfg
                                                                                        ,jenis_penimbangan) a
                                                                                LEFT JOIN std_leadtime_penimbangan slp on slp.koitem_bulk=split_part(a.snfg_komponen,'/',1)
                                                                                WHERE a.is_done>0
                                                                            ) b
                                                                        GROUP BY tanggal_stop
                                                                        ORDER BY tanggal_stop asc")->queryAll();

                    // End of Combination Backend Code

                                                      echo Highcharts::widget([
                                                      'scripts' => [
                                                          'modules/exporting',
                                                          'themes/grid-light',
                                                      ],
                                                      'options' => [
                                                          'rangeSelector' => [
                                                                //'inputEnabled' => new JsExpression('$("#container").width() > 480'),
                                                                'selected' => 1
                                                            ],
                                                          'title' => [
                                                              'text' => 'Achievement Line Tanggal',
                                                          ],
                                                          'xAxis' => [
                                                              //'type' => new JsExpression('datetime'),
                                                              'categories' => new SeriesDataHelper($tanggalList,['tanggal_stop']),
                                                              //['PLANNER', 'PENIMBANGAN', 'PENGOLAHAN','QCBULK', 'INKJET', 'KEMAS 1','KEMAS 2', 'QCFG', 'QC TIMBANG FG'],
                                                          ],
                                                          'labels' => [
                                                              'items' => [
                                                                  [
                                                                      'html' => 'Flowboard',
                                                                      'style' => [
                                                                          'left' => '50px',
                                                                          'top' => '18px',
                                                                          'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                                                                      ],
                                                                  ],
                                                              ],
                                                          ],
                                                          'series' => [
                                                              [
                                                                  'type' => 'column',
                                                                  'name' => 'Achieved',
                                                                  'data' => new SeriesDataHelper($tanggalList,['achieved']),
                                                              ],
                                                              [
                                                                  'type' => 'column',
                                                                  'name' => 'Not Achieved',
                                                                  'data' => new SeriesDataHelper($tanggalList,['not_achieved']),
                                                              ],
                                                              [
                                                                  'type' => 'spline',
                                                                  'name' => 'Total',
                                                                  'data' => new SeriesDataHelper($tanggalList,['all']),
                                                                  'marker' => [
                                                                      'lineWidth' => 2,
                                                                      'lineColor' => new JsExpression('Highcharts.getOptions().colors[3]'),
                                                                      'fillColor' => 'white',
                                                                  ],
                                                              ],
                                                              [
                                                                  'type' => 'pie',
                                                                  'name' => 'Rekap',
                                                                  'data' => [
                                                                      [
                                                                          'name' => 'Achieved',
                                                                          'y' => $implode_achieved,
                                                                          'color' => new JsExpression('Highcharts.getOptions().colors[0]'), // Achieved's color
                                                                      ],
                                                                      [
                                                                          'name' => 'Not Achieved',
                                                                          'y' => $implode_nachieved,//new SeriesDataHelper($countWno,['count']),
                                                                          'color' => new JsExpression('Highcharts.getOptions().colors[1]'), // Not Achieved's color
                                                                      ],
                                                                  ],
                                                                  'center' => [100, 50],
                                                                  'size' => 60,
                                                                  'showInLegend' => true,
                                                                  'dataLabels' => [
                                                                      'enabled' => true,
                                                                  ],
                                                              ],
                                                          ],
                                                      ]
                                                  ]);

                
                                ?>
                </div>
                </div>
    </div>

    <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'primary', 'heading'=>'Achievement Line Penimbangan'],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                [
                    'attribute'=>'tanggal', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                ],
                [

                    'attribute'=>'nama_line', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>1,
                ],
                [
                    'attribute'=>'snfg', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>2,
                ],
                [
                    'attribute'=>'snfg_komponen', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>3,
                ],
                'nama_item',
                'leadtime',
                'leadtime_std',
                'status_leadtime',
                'plan_start_timbang',
                'status_plan',
                'posisi',
             ],
        ]);
    
    ?>



</div>
