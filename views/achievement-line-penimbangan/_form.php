<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLinePenimbangan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="achievement-line-penimbangan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'row_number')->textInput() ?>

    <?= $form->field($model, 'tanggal')->textInput() ?>

    <?= $form->field($model, 'nama_line')->textInput() ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'snfg_komponen')->textInput() ?>

    <?= $form->field($model, 'nama_item')->textInput() ?>

    <?= $form->field($model, 'leadtime')->textInput() ?>

    <?= $form->field($model, 'leadtime_std')->textInput() ?>

    <?= $form->field($model, 'status_leadtime')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'plan_start_timbang')->textInput() ?>

    <?= $form->field($model, 'status_plan')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'posisi')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
