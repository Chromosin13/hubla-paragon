<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AchievementLinePenimbangan */

$this->title = 'Create Achievement Line Penimbangan';
$this->params['breadcrumbs'][] = ['label' => 'Achievement Line Penimbangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-line-penimbangan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
