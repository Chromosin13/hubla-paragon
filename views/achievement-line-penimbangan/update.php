<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLinePenimbangan */

$this->title = 'Update Achievement Line Penimbangan: ' . $model->row_number;
$this->params['breadcrumbs'][] = ['label' => 'Achievement Line Penimbangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->row_number, 'url' => ['view', 'id' => $model->row_number]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="achievement-line-penimbangan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
