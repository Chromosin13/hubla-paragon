<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DashboardIdletimePengolahan */

$this->title = 'Create Dashboard Idletime Pengolahan';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Idletime Pengolahans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dashboard-idletime-pengolahan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
