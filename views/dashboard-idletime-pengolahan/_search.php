<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DashboardIdletimePengolahanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dashboard-idletime-pengolahan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

      <div class="box-header with-border">
        <h4 class="box-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false">
            Filter Field
          </a>
        </h4>
      </div>
    <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
        <div class="box-body">
            <p></p>
                <?= $form->field($model, 'nomo') ?>

                <?= $form->field($model, 'snfg') ?>

                <?= $form->field($model, 'snfg_komponen') ?>

                <?= $form->field($model, 'nama_fg') ?>

                <?= $form->field($model, 'sediaan') ?>

                <?php  echo $form->field($model, 'upload_date') ?>

                <?php  echo $form->field($model, 'week') ?>

                <?php  echo $form->field($model, 'idle_timbang_olah') ?>

                <?php  echo $form->field($model, 'idle_olahpremix_olah1') ?>

                <?php  echo $form->field($model, 'idle_olah1_olah2') ?>

                <?php  echo $form->field($model, 'idle_olah2_qcbulk') ?>

                <?php  echo $form->field($model, 'idle_timbang_olah_jam') ?>

                <?php  echo $form->field($model, 'idle_olahpremix_olah1_jam') ?>

                <?php  echo $form->field($model, 'idle_olah1_olah2_jam') ?>

                <?php  echo $form->field($model, 'idle_olah2_qcbulk_jam') ?>

                <?php  echo $form->field($model, 'timbang_max') ?>

                <?php  echo $form->field($model, 'olah_pertama') ?>

                <?php  echo $form->field($model, 'olah_premix_min') ?>

                <?php  echo $form->field($model, 'olah_premix_max') ?>

                <?php  echo $form->field($model, 'olah_1_min') ?>

                <?php  echo $form->field($model, 'olah_1_max') ?>

                <?php  echo $form->field($model, 'olah_2_min') ?>

                <?php  echo $form->field($model, 'olah_2_max') ?>

                <?php  echo $form->field($model, 'qcbulk_min') ?>

                <?php  echo $form->field($model, 'id') ?>

                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Reset', ['index'], ['class' => 'btn btn-default']) ?>
                </div>

                <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
