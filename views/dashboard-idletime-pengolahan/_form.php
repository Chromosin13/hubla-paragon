<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DashboardIdletimePengolahan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dashboard-idletime-pengolahan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomo')->textInput() ?>

    <?= $form->field($model, 'snfg')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'snfg_komponen')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'sediaan')->textInput() ?>

    <?= $form->field($model, 'upload_date')->textInput() ?>

    <?= $form->field($model, 'week')->textInput() ?>

    <?= $form->field($model, 'idle_timbang_olah')->textInput() ?>

    <?= $form->field($model, 'idle_olahpremix_olah1')->textInput() ?>

    <?= $form->field($model, 'idle_olah1_olah2')->textInput() ?>

    <?= $form->field($model, 'idle_olah2_qcbulk')->textInput() ?>

    <?= $form->field($model, 'idle_timbang_olah_jam')->textInput() ?>

    <?= $form->field($model, 'idle_olahpremix_olah1_jam')->textInput() ?>

    <?= $form->field($model, 'idle_olah1_olah2_jam')->textInput() ?>

    <?= $form->field($model, 'idle_olah2_qcbulk_jam')->textInput() ?>

    <?= $form->field($model, 'timbang_max')->textInput() ?>

    <?= $form->field($model, 'olah_pertama')->textInput() ?>

    <?= $form->field($model, 'olah_premix_min')->textInput() ?>

    <?= $form->field($model, 'olah_premix_max')->textInput() ?>

    <?= $form->field($model, 'olah_1_min')->textInput() ?>

    <?= $form->field($model, 'olah_1_max')->textInput() ?>

    <?= $form->field($model, 'olah_2_min')->textInput() ?>

    <?= $form->field($model, 'olah_2_max')->textInput() ?>

    <?= $form->field($model, 'qcbulk_min')->textInput() ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
