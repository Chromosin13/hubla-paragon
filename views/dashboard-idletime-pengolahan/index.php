<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DashboardIdletimePengolahanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dashboard Idletime Pengolahan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dashboard-idletime-pengolahan-index">

    <?php

        $gridColumns = [
        'nomo',
            'snfg:ntext',
            'snfg_komponen:ntext',
            'nama_fg',
            'sediaan',
            'upload_date',
            'week',
            'idle_timbang_olah',
            'idle_olahpremix_olah1',
            'idle_olah1_olah2',
            'idle_olah2_qcbulk',
            'idle_timbang_olah_jam',
            'idle_olahpremix_olah1_jam',
            'idle_olah1_olah2_jam',
            'idle_olah2_qcbulk_jam',
            'timbang_max',
            'olah_pertama',
            'olah_premix_min',
            'olah_premix_max',
            'olah_1_min',
            'olah_1_max',
            'olah_2_min',
            'olah_2_max',
            'qcbulk_min',
            'id',
        ];

        // Renders a export dropdown menu
        echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns
        ]);
        
    ?>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'nomo',
            'snfg:ntext',
            'snfg_komponen:ntext',
            'nama_fg',
            'sediaan',
            'upload_date',
            'week',
            'idle_timbang_olah',
            'idle_olahpremix_olah1',
            'idle_olah1_olah2',
            'idle_olah2_qcbulk',
            'idle_timbang_olah_jam',
            'idle_olahpremix_olah1_jam',
            'idle_olah1_olah2_jam',
            'idle_olah2_qcbulk_jam',
            'timbang_max',
            'olah_pertama',
            'olah_premix_min',
            'olah_premix_max',
            'olah_1_min',
            'olah_1_max',
            'olah_2_min',
            'olah_2_max',
            'qcbulk_min',
            'id',

        ],
    ]); ?>
</div>
