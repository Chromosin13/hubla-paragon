<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DashboardIdletimePengolahan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Idletime Pengolahans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dashboard-idletime-pengolahan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nomo',
            'snfg:ntext',
            'snfg_komponen:ntext',
            'nama_fg',
            'sediaan',
            'upload_date',
            'week',
            'idle_timbang_olah',
            'idle_olahpremix_olah1',
            'idle_olah1_olah2',
            'idle_olah2_qcbulk',
            'idle_timbang_olah_jam',
            'idle_olahpremix_olah1_jam',
            'idle_olah1_olah2_jam',
            'idle_olah2_qcbulk_jam',
            'timbang_max',
            'olah_pertama',
            'olah_premix_min',
            'olah_premix_max',
            'olah_1_min',
            'olah_1_max',
            'olah_2_min',
            'olah_2_max',
            'qcbulk_min',
            'id',
        ],
    ]) ?>

</div>
