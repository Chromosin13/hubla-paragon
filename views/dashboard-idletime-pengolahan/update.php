<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DashboardIdletimePengolahan */

$this->title = 'Update Dashboard Idletime Pengolahan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Idletime Pengolahans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dashboard-idletime-pengolahan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
