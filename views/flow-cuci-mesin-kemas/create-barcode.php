<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>


                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <!-- <div class="widget-user-header bg-black" style="background: url('../web/gif/machine.gif') center center;"> -->
                    <div class="widget-user-header bg-navy" style="background: url('../web/gif/tourtel.gif') center center;">
                      <h3 class="widget-user-username"><b>Cuci Mesin Kemas</b></h3>
                      <h5 class="widget-user-desc">Input Data</h5>
                    </div>
                    <div class="widget-user-image">
                      <!-- <img class="img-circle" src="../web/images/business-partnership.png" alt="User Avatar"> -->
                      <!-- <img class="img-circle" src="../web/gif/tourtel.gif" alt="User Avatar"> -->
                    </div>

                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                              <div class="box-body">
                                  <div class="flow-cuci-mesin-form">

                                    <div class="form-group field-flowcucimesinkemas-nama_line has-success">
                                    <label class="control-label" for="flowcucimesinkemas-nama_line">Nama Line / Kode Mesin</label>
                                    <input type="text" id="flowcucimesinkemas-nama_line" class="form-control" name="FlowCuciMesin[nama_line]" aria-invalid="false">

                                    </div>
                                  </div>
                              </div>
                        </div>
                      <!-- /.row -->
                      </div>
                    </div>

<?php
$script = <<< JS

  // AutoFocus nama_line Field

  document.getElementById("flowcucimesinkemas-nama_line").focus();


  // Check nama_line Validity

  $('#flowcucimesinkemas-nama_line').change(function(){

      var nama_line = $('#flowcucimesinkemas-nama_line').val().trim();

      $.get('index.php?r=flow-cuci-mesin-kemas/check-line',{ nama_line : nama_line },function(data){
          var data = $.parseJSON(data);
          // alert(data);
          // if(data==="null"){
          //   $('#create-form').hide();
          //     alert("TIDAK ADA JADWAL DENGAN nama_lineR MO "+data.nama_line+" PADA DATABASE PLANNER");
          // }
          if(data.running==1){

            alert('Mesin ini masih dalam proses pengolahan. Mohon untuk di stop terlebih dahulu proses olahnya.');
            // $('#create-form').hide();

          }else {

            // alert('Data Valid, Proses QC sudah berjalan, halaman akan diteruskan');
            // $('#create-form').fadeIn();
            // window.location = baseurl+"?r=flow-cuci-mesin/create-rincian&nama_line="+nama_line;
            window.location = "index.php?r=flow-cuci-mesin-kemas/check-status-cuci&nama_line="+nama_line;
            // window.location = "index.php?r=flow-cuci-mesin/create-rincian&nama_line="+nama_line;

          }

      });
  });


  // $('#flowcucimesinkemas-nama_line').change(function(){
  //
  //     var nama_line = $('#flowcucimesinkemas-nama_line').val();
  //
  //
  //
  //
  // });


  // $('#flowcucimesinkemas-nama_line').change(function(){

  //     var nama_line = $('#flowcucimesinkemas-nama_line').val();


  //     $.get('index.php?r=scm-planner/check-nama_line',{ nama_line : nama_line },function(data){
  //         var data = $.parseJSON(data);
  //         if(data.id>=1){

  //           window.location = "index.php?r=flow-input-nama_line/create-kemas2&nama_line="+nama_line;

  //         }

  //         else{

  //           alert('Nomor nama_line Tidak Ditemukan di Database; Silahkan mengontak Planner/Leader');
  //         }

  //     });
  // });



JS;
$this->registerJs($script);
?>
