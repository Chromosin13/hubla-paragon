<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\ActiveField;
use kartik\dialog\Dialog;
use kartik\checkbox\CheckboxX;
use app\models\FlowInputMo;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\flowcucimesin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="flow-cuci-mesin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        echo $form->field($model, 'nama_line')->label("Nama Line / Kode Mesin")->widget(Select2::classname(), [
            'data' => ArrayHelper::map(FlowInputMo::find()->all()
            ,'nama_line','nama_line'),
            'options' => ['placeholder' => 'Select Line'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
      ?>

    <div id="create-form" class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Input Proses Cuci Mesin' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <div id="baseurl" value="<?php echo Yii::$app->homeUrl; ?>">

</div>

<?php
$script = <<< JS

// Hide Create new Field

$('#create-form').hide();
// // Get Base Url
// var baseurl = $('#baseurl').val();
//
//
// $('#flowcucimesin-nama_line').change(function(){
//
//     var nama_line = $('#flowcucimesin-nama_line').val().trim();
//     if(nama_line){
//
//       $('#create-form').fadeIn();
//       // window.location = "index.php?r=flow-cuci-mesin/create-rincian&nama_line="+nama_line;
//     }else{
//       alert('Nama Line kosong!');
//     }
//
// });


$('#flowcucimesin-nama_line').change(function(){

    var nama_line = $('#flowcucimesin-nama_line').val();


    $.get('index.php?r=flow-cuci-mesin/check-line',{ nama_line : nama_line },function(data){
        var data = $.parseJSON(data);
        // alert(data);
        // if(data==="null"){
        //   $('#create-form').hide();
        //     alert("TIDAK ADA JADWAL DENGAN nama_lineR MO "+data.nama_line+" PADA DATABASE PLANNER");
        // }
        if(data.running==1){

          alert('Mesin ini masih dalam proses pengolahan. Mohon untuk di stop terlebih dahulu proses olahnya.');
          $('#create-form').hide();

        }else {

          // alert('Data Valid, Proses QC sudah berjalan, halaman akan diteruskan');
          $('#create-form').fadeIn();
          // var nama_line = $('#flowcucimesin-nama_line').val();
          // window.location = baseurl+"?r=flow-cuci-mesin/create-rincian&nama_line="+nama_line;
          // window.location = "index.php?r=flow-cuci-mesin/create-rincian&nama_line="+nama_line;

        }

    });

});


JS;
$this->registerJs($script);
?>
