<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FlowCuciMesinKemas */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Flow Cuci Mesin Kemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flow-cuci-mesin-kemas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nama_line',
            'nama_bulk',
            'nobatch',
            'nomo',
            'operator',
            'jenis_cuci',
            'verifikator',
            'gombalan',
            'sarung_tangan',
            'detergen',
            'parafin',
            'datetime_start',
            'datetime_stop',
            'id',
        ],
    ]) ?>

</div>
