<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputSnfg;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\Downtime;
use app\models\DowntimeSearch;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
    $row = Yii::$app->db->createCommand("SELECT * FROM history_mesin_kemas WHERE nama_line = '".$nama_line."' ORDER BY datetime_start DESC")->queryOne();
    $snfg = $row['snfg'];
    $nama_fg = $row['nama_fg'];
    $nobatch = $row['nobatch'];
?>

                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
                      <h3 class="widget-user-username"><b>Start Cuci Mesin KEMAS</b></h3>
                      <h5 class="widget-user-desc"><?php echo $nama_line; ?></h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">




                                <div class="flow-cuci-mesin-form">

                                    <?php $form = ActiveForm::begin(); ?>


                                    <div class="row">
                                      <div class="col-md-12">
                                        <?= $form->field($model, 'nama_fg')->textInput(['readOnly'=>true,'value'=>$nama_fg]) ?>
                                        <!-- /input-group -->
                                      </div>
                                      <div class="col-md-3">
                                          <?= $form->field($model, 'nama_line')->textInput(['readOnly'=>true,'value'=>$nama_line]) ?>
                                          <!-- /input-group -->
                                      </div>
                                      <div class="col-md-3">
                                        <?= $form->field($model, 'snfg')->textInput(['readOnly'=>true,'value'=>$snfg]) ?>
                                        <!-- /input-group -->
                                      </div>
                                      <div class="col-md-3">
                                          <?= $form->field($model, 'nobatch')->textInput(['value'=>$nobatch]) ?>
                                          <!-- /input-group -->
                                      </div>
                                    </div>

                                    <div class="row">
                                        <!-- /.col-lg-6 -->
                                        <div class="col-md-6">
                                          <?php
                                              echo $form->field($model, 'jenis_cuci')->widget(Select2::classname(), [
                                                  'data' => ['Cuci Bersih'=>'Cuci Bersih', 'Cuci Kasar'=>'Cuci Kasar'],
                                                  'options' => ['placeholder' => 'Select Jenis Cuci'],
                                                  'pluginOptions' => [
                                                      'allowClear' => true,
                                                      // 'required'=>true
                                                  ],
                                              ]);
                                            ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>
                                    <label class="control-label">Operator</label>
                                    <?= $form->field($model, 'operator')->widget(TagsinputWidget::classname(), [
                                                        'clientOptions' => [
                                                            'trimValue' => true,
                                                            'allowDuplicates' => false,
                                                            'maxChars'=> 4,
                                                            'minChars'=> 4,
                                                        ]
                                                    ])->label(false)?>

                                    <br></br>

                                    <div class="row">

                                        <div class="col-md-2 col-md-offset-5">

                                            <?php
                                                echo Html::submitButton($model->isNewRecord ? 'Start' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
                                            ?>

                                        </div>
                                    </div>

                                    <?php ActiveForm::end(); ?>

                                </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>


<div class="box box-widget widget-user">
    <div class="widget-user-header bg-blue">
                      <h3 class="widget-user-username"><b>Rincian</b></h3>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            // [
            //     'class' => 'kartik\grid\ExpandRowColumn',
            //     'value' => function($model,$key,$index,$column){
            //         return GridView::ROW_COLLAPSED;
            //     },
            //     'detail' => function($model,$key,$index,$column){
            //
            //         $searchModel = new DowntimeSearch();
            //
            //         $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            //         $dataProvider->query->where("flow_input_snfg_id=".$model->id);
            //
            //         return Yii::$app->controller->renderPartial('_expand-downtime-kemas2', [
            //             'searchModel' => $searchModel,
            //             'dataProvider' => $dataProvider,
            //             'flow_input_snfg_id' => $model->id,
            //         ]);
            //     },
            // ],
            'id',
            'nama_line',
            'datetime_start',
            'datetime_stop',
            'jenis_cuci',
            'operator',
        ],
    ]); ?>

</div>


<?php
$script = <<< JS

    // $('#lanjutan').hide();
    //
    // var snfg = $('#flowinputsnfg-snfg').val();
    //
    // var posisi = $('#flowinputsnfg-posisi').val();




    // $('#flowinputsnfg-jenis_kemas').change(function(){
    //
    //     var jenis_kemas = $('#flowinputsnfg-jenis_kemas').val();
    //
    //     $.get('index.php?r=flow-cuci-mesin/get-lanjutan-kemas2',{ snfg : snfg , jenis_kemas : jenis_kemas , posisi : posisi  }, function(data){
    //         var data = $.parseJSON(data);
    //         $('#flowinputsnfg-lanjutan').attr('value',data.lanjutan);
    //
    //     });
    //
    //     $.post("index.php?r=master-data-line/get-line-kemas-snfg&snfg="+$('#flowinputsnfg-snfg').val()+"&jenis_kemas="+$('#flowinputsnfg-jenis_kemas').val(), function (data){
    //         $("select#flowinputsnfg-nama_line").html(data);
    //     });
    //
    //     $('#lanjutan').fadeIn();
    //
    // });



JS;
$this->registerJs($script);
?>
