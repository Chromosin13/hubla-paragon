<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FlowCuciMesinKemasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Flow Cuci Mesin Kemas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flow-cuci-mesin-kemas-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Flow Cuci Mesin Kemas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nama_line',
            'nama_bulk',
            'nobatch',
            'nomo',
            'operator',
            // 'jenis_cuci',
            // 'verifikator',
            // 'gombalan',
            // 'sarung_tangan',
            // 'detergen',
            // 'parafin',
            // 'datetime_start',
            // 'datetime_stop',
            // 'id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
