<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterDataSediaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Data Sediaans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-data-sediaan-index">
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Master Data Sediaan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'sediaan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
