<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PenimbanganEditJadwal */

$this->title = 'Create Penimbangan Edit Jadwal';
$this->params['breadcrumbs'][] = ['label' => 'Penimbangan Edit Jadwals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penimbangan-edit-jadwal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
