<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PenimbanganEditJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penimbangan Edit Jadwals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penimbangan-edit-jadwal-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


<!--     <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nomo',
            'snfg_komponen',
            'lanjutan',
            'start',
            'stop',
            'jenis_penimbangan',
            'nama_line',
            'nama_operator',
            'start_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
 -->
        <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'danger', 'heading'=>'Edit Jadwal Penimbangan'],
            'columns'=>[
                ['class' => 'kartik\grid\ActionColumn','template' => '{update}',],
                //['class'=>'kartik\grid\SerialColumn'],
                            'id',
                'nomo',
                'snfg_komponen',
                'lanjutan',
                'start',
                'stop',
                'jenis_penimbangan',
                'nama_line',
                'nama_operator',
                // 'start_id',
             ],
            'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        ]);
    
        ?>
</div>
