<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
use yii\helpers\ArrayHelper;
use faryshta\widgets\JqueryTagsInput;
use app\models\PenimbanganEditJadwal;
use yii\widgets\Pjax; 
/* @var $this yii\web\View */
/* @var $model app\models\PenimbanganEditJadwal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="penimbangan-edit-jadwal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomo')->textInput(['disabled'=>'true']) ?>

    <?= $form->field($model, 'snfg_komponen')->textInput(['disabled'=>'true']) ?>

    <?= $form->field($model, 'lanjutan')->textInput(['disabled'=>'true']) ?>

    <?= $form->field($model, 'start')->textInput(['disabled'=>'true']) ?>

    <?= $form->field($model, 'stop')->textInput(['disabled'=>'true']) ?>

    <?= $form->field($model, 'jenis_penimbangan')->textInput(['disabled'=>'true']) ?>

    <!-- <?= $form->field($model, 'nama_line')->textInput() ?> -->

    <?= $form->field($model, 'nama_line')->dropDownList(
                            ArrayHelper::map(PenimbanganEditJadwal::find()->all()
                            ,'nama_line','nama_line')
                            ,['prompt'=>'Select Line']

    );?>

    <?php echo $form->field($model, 'nama_operator', ['hintType' => ActiveField::HINT_SPECIAL, 'addon' => ['prepend' => ['content'=>'<i class="fa fa-barcode"></i>']]
                            ])->widget(JqueryTagsInput::className([]))->hint(' <b>Scan</b> : Klik pada isian dibawah kemudian Scan Barcode pada Nametag<p \> <b>Manual</b> : isi dengan Format 4 Huruf Kapital, Contoh : "ABCD" lalu diikuti dengan menekan Enter ');
    ?>

    <?= $form->field($model, 'start_id')->textInput(['disabled'=>'true']) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$script = <<< JS

// Assign Nama Line
var nomo = $('#penimbanganeditjadwal-nomo').val();
var snfg_komponen = $('#penimbanganeditjadwal-snfg_komponen').val();
if(nomo!="" && snfg_komponen==""){
    $.post("index.php?r=scm-planner/get-line-timbang-nomo-edit&nomo="+$('#penimbanganeditjadwal-nomo').val()+"&nama_line="+$('#penimbanganeditjadwal-nama_line').val(), function (data){
        $("select#penimbanganeditjadwal-nama_line").html(data);
    });
} else if(nomo=="" && snfg_komponen!=""){
    $.post("index.php?r=scm-planner/get-line-timbang-komponen-edit&nomo="+$('#penimbanganeditjadwal-snfg_komponen').val()+"&nama_line="+$('#penimbanganeditjadwal-nama_line').val(), function (data){
        $("select#penimbanganeditjadwal-nama_line").html(data);
    });
}

    


JS;
$this->registerJs($script);
?>