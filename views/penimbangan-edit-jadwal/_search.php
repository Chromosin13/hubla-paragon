<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PenimbanganEditJadwalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="penimbangan-edit-jadwal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nomo') ?>

    <?= $form->field($model, 'snfg_komponen') ?>

    <?= $form->field($model, 'lanjutan') ?>

    <?= $form->field($model, 'start') ?>

    <?php // echo $form->field($model, 'stop') ?>

    <?php // echo $form->field($model, 'jenis_penimbangan') ?>

    <?php // echo $form->field($model, 'nama_line') ?>

    <?php // echo $form->field($model, 'nama_operator') ?>

    <?php // echo $form->field($model, 'start_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
