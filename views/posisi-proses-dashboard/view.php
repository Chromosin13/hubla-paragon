<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PosisiProsesDashboard */

$this->title = $model->snfg_komponen;
$this->params['breadcrumbs'][] = ['label' => 'Posisi Proses Dashboards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posisi-proses-dashboard-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->snfg_komponen], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->snfg_komponen], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nomo',
            'snfg',
            'snfg_komponen',
            'posisi',
            'jenis_proses:ntext',
            'lanjutan',
            'datetime_write',
            'start',
            'due',
            'nama_fg',
            'process_status:ntext',
            'is_done:ntext',
        ],
    ]) ?>

</div>
