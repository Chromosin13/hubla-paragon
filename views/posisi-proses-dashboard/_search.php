<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PosisiProsesDashboardSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="posisi-proses-dashboard-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'nomo') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'snfg_komponen') ?>

    <?= $form->field($model, 'posisi') ?>

    <?= $form->field($model, 'jenis_proses') ?>

    <?php // echo $form->field($model, 'lanjutan') ?>

    <?php // echo $form->field($model, 'datetime_write') ?>

    <?php // echo $form->field($model, 'start') ?>

    <?php // echo $form->field($model, 'due') ?>

    <?php // echo $form->field($model, 'nama_fg') ?>

    <?php // echo $form->field($model, 'process_status') ?>

    <?php // echo $form->field($model, 'is_done') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
