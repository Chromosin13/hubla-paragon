<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PosisiProsesDashboard */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="posisi-proses-dashboard-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomo')->textInput() ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'snfg_komponen')->textInput() ?>

    <?= $form->field($model, 'posisi')->textInput() ?>

    <?= $form->field($model, 'jenis_proses')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'lanjutan')->textInput() ?>

    <?= $form->field($model, 'datetime_write')->textInput() ?>

    <?= $form->field($model, 'start')->textInput() ?>

    <?= $form->field($model, 'due')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'process_status')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_done')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
