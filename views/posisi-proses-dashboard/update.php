<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PosisiProsesDashboard */

$this->title = 'Update Posisi Proses Dashboard: ' . $model->snfg_komponen;
$this->params['breadcrumbs'][] = ['label' => 'Posisi Proses Dashboards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->snfg_komponen, 'url' => ['view', 'id' => $model->snfg_komponen]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="posisi-proses-dashboard-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
