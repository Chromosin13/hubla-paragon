<style>
    .zoom {
        padding: 50px;
        transition: transform .2s; /* Animation */
        width: 200px;
        height: 200px;
        margin: 0 auto;
    }

    .zoom:hover {
        transform: scale(1.2); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    }

    @import url(https://fonts.googleapis.com/css?family=Lato:900);
    /*@import url('https://fonts.googleapis.com/css?family=Roboto+Condensed');*/
    *, *:before, *:after{
      box-sizing:border-box;
    }
    body{
      font-family: 'Lato', sans-serif;
        ;
    }
    div.foo{
      width: 90%;
      margin: 0 auto;
      text-align: center;
    }
    .letter{
      display: inline-block;
      font-weight: 650;
      font-size: 3em;
      margin: 0.2em;
      position: relative;
      color: #00B4F1;
      transform-style: preserve-3d;
      perspective: 400;
      z-index: 1;
    }
    .letter:before, .letter:after{
      position:absolute;
      content: attr(data-letter);
      transform-origin: top left;
      top:0;
      left:0;
    }
    .letter, .letter:before, .letter:after{
      transition: all 0.3s ease-in-out;
    }
    .letter:before{
      color: #fff;
      text-shadow: 
        -1px 0px 1px rgba(255,255,255,.8),
        1px 0px 1px rgba(0,0,0,.8);
      z-index: 3;
      transform:
        rotateX(0deg)
        rotateY(-15deg)
        rotateZ(0deg);
    }
    .letter:after{
      color: rgba(0,0,0,.11);
      z-index:2;
      transform:
        scale(1.08,1)
        rotateX(0deg)
        rotateY(0deg)
        rotateZ(0deg)
        skew(0deg,1deg);
    }
    .letter:hover:before{
      color: #fafafa;
      transform:
        rotateX(0deg)
        rotateY(-40deg)
        rotateZ(0deg);
    }
    .letter:hover:after{
      transform:
        scale(1.08,1)
        rotateX(0deg)
        rotateY(40deg)
        rotateZ(0deg)
        skew(0deg,22deg);
</style>

<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use app\models\PosisiProsesDashboard;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PosisiProsesDashboardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// $this->title = 'Track Process Position';
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="foo">
  <span class="letter" data-letter="T">T</span>
  <span class="letter" data-letter="R">R</span>
  <span class="letter" data-letter="A">A</span>
  <span class="letter" data-letter="C">C</span>
  <span class="letter" data-letter="K">K</span>
  <span class="letter" data-letter=".">.</span>
</div>

<div class="callout callout-success">
  <h4>Scope Data</h4>

  <p> Refresh Setiap 2 Menit </p>
  <p> 60 Hari (2 Bulan) Terakhir dari Tanggal Start Penimbangan Planner</p>

</div>


<div class="posisi-proses-dashboard-index">

    Download All Data Here
    <?php 

        $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],
                'nomo',
                'snfg',
                'snfg_komponen',
                'posisi',
                'jenis_proses',
                'lanjutan',
                'datetime_write',
                'start',
                'due',
                'nama_fg',
                'process_status',
                'is_done',
                'koitem_fg',
                'nobatch',
        ['class' => 'kartik\grid\ActionColumn'],
        ];

        echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
  
    ]);?>
<p></p>
    <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'responsiveWrap' => false,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'success', 'heading'=>'Tracking Process Position'],
            'formatter' => [
                'class' => 'yii\i18n\Formatter',
                'timeZone' => 'Asia/Jakarta'
            ],
            'toolbar' => [
            ],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                [
                    'attribute' => 'koitem_fg',
                    'label'=>'Image',
                    'format' => 'html',    
                    'value' => function ($data) {
                        return Html::img(Yii::getAlias('@web').'/images/products/'. $data['koitem_fg'].'.jpg',
                            ['width' => '50px']);
                    },
                ],
                [
                    'attribute'=>'nama_fg',
                    'label' => 'Finished Good', 
                    'width'=>'210px',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(PosisiProsesDashboard::find()->orderBy('nama_fg')->asArray()->all(), 'nama_fg', 'nama_fg'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'SKU'],
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 400px;font-size: 12px','class'=>'info'],
                    'group'=>true,  // enable grouping
                    //'subGroupOf'=>1,
                    'groupHeader'=>function ($model, $key, $index, $widget) { // Closure method
                        return [
                            'mergeColumns'=>[[1,4]], // columns to merge in summary
                            'content'=>[             // content to show in each summary cell
                                1=>'Summary (' . $model->nama_fg . ')',
                                //7=>GridView::F_SUM,
                                // 8=>GridView::F_AVG,
                                // 9=>' hrs',
                                //9=>GridView::F_SUM,
                            ],
                            'contentFormats'=>[      // content reformatting for each summary cell
                                //7=>['format'=>'number', 'decimals'=>2],
                                8=>['format'=>'number', 'decimals'=>1],
                                //8=>['append'=>' hrs'],
                                //9=>['format'=>'number', 'decimals'=>1],
                            ],
                            'contentOptions'=>[      // content html attributes for each summary cell
                                1=>['style'=>'font-variant:medium-caps;font-size: 12px'],
                                // 7=>['style'=>'text-align:right'],
                                8=>['style'=>'text-align:right'],
                                // 9=>['style'=>'text-align:right'],
                            ],
                            // html attributes for group summary row
                            'options'=>['class'=>'success','style'=>'font-weight:bold;']
                        ];
                    }
                ],
                
                  [
                    'attribute'=>'nomo',
                    'label' => 'Nomor MO',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>1,
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 250px;font-size: 12px','class'=>'info'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                ],
                [
                    'attribute' => 'snfg',
                    'label' => 'SNFG',
                    'format' => 'raw',
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 250px;font-size: 12px','class'=>'info'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>2,       
                ],
                [
                    'attribute' => 'snfg_komponen',
                    'label' => 'Komponen',
                    'format' => 'raw',
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 250px;font-size: 12px','class'=>'info'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>3,      
                ],
                [
                    'attribute' => 'posisi',
                    'label' => 'Posisi',
                    'format' => 'raw',
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 250px;font-size: 12px','class'=>'success'],
                    'headerOptions' => ['style' => 'text-align: center;'],     
                ],
                [
                    'attribute' => 'process_status',
                    'label' => 'Process Status',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'jenis_proses',
                    'label' => 'Process Type',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'is_done',
                    'label' => 'Done?',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'lanjutan',
                    'label' => 'Shift',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'start',
                    'label' => 'Start',
                    'format' => ['date', 'php:Y-m-d'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'contentOptions' => ['style' => ' text-align: center;width: 150px;font-size: 18px'
                    ],
                ],
                [
                    'attribute' => 'due',
                    'label' => 'Due',
                    'format' => ['date', 'php:Y-m-d'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'contentOptions' => ['style' => ' text-align: center;width: 150px;font-size: 18px'
                    ],
                ],
                [
                    'attribute' => 'datetime_write',
                    'label' => 'Last Update',
                    'format' => ['date', 'php:Y-m-d h:i'],
                    //'format' => 'raw',
                    'contentOptions' => ['style' => ' text-align: center;width: 200px'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                ],
                [
                    'attribute' => 'nobatch',
                    'label' => 'No Batch',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],


             ],
            
        ]);
    
    ?>
</div>
