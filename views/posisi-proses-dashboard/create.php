<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PosisiProsesDashboard */

$this->title = 'Create Posisi Proses Dashboard';
$this->params['breadcrumbs'][] = ['label' => 'Posisi Proses Dashboards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posisi-proses-dashboard-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
