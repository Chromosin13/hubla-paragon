<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterDataBarcodeMalaysiaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Data Barcode Malaysias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-data-barcode-malaysia-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Master Data Barcode Malaysia', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'koitem_lama',
            'koitem_baru',
            'nama_fg',
            'barcode_lama',
            'barcode_harmonize',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
