<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterDataBarcodeMalaysia */

$this->title = 'Create Master Data Barcode Malaysia';
$this->params['breadcrumbs'][] = ['label' => 'Master Data Barcode Malaysias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-data-barcode-malaysia-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
