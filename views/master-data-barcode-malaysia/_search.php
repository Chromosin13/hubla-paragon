<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MasterDataBarcodeMalaysiaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-data-barcode-malaysia-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'koitem_lama') ?>

    <?= $form->field($model, 'koitem_baru') ?>

    <?= $form->field($model, 'nama_fg') ?>

    <?= $form->field($model, 'barcode_lama') ?>

    <?php // echo $form->field($model, 'barcode_harmonize') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
