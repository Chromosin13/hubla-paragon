<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MasterDataBarcodeMalaysia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-data-barcode-malaysia-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'koitem_lama')->textInput() ?>

    <?= $form->field($model, 'koitem_baru')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'barcode_lama')->textInput() ?>

    <?= $form->field($model, 'barcode_harmonize')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
