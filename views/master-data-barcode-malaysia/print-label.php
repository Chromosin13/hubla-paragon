<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\Downtime;
use app\models\DowntimeSearch;
use kartik\date\DatePicker;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<script>
   
</script>

 <div class="box box-widget widget-user">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-black" style="background: url('../web/images/back-toska.jpg') center center; background-size:     cover; background-repeat:   no-repeat; border: 2px solid #DFDFDF;">
    <h3 class="widget-user-username" style="color:white; font-size:2.5em;"><b>PRINTER STATION - KARBOX LABEL</b></h3>
    <h5 class="widget-user-desc" style="color:white;">Input Data</h5>
  </div>
  <div class="widget-user-image">
    <img class="img-circle" src="../web/images/printer.png" alt="User Avatar">
  </div>

    <div class="box-footer">
        <div class="row">
            <div class="box-body">
                <div class="box-body">
                    <div class="karrbox-label-print-form">

                        <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($model, 'snfg')->textInput(['value'=>$snfg]) ?>

                        <!-- FG Name from Odoo Warehouse Database --->
                        <?php

                            if($nama_fg==""){
                                echo $form->field($model, 'nama_fg')->textInput(['placeholder'=>'Data was not found in Database, Please fill manually']);
                            } else {
                                echo $form->field($model, 'nama_fg')->textInput(['value'=>$nama_fg]);
                            }
                        ?>

                        <!-- Barcode --->
                        <?php

                            if($barcode==""){
                                echo $form->field($model, 'barcode')->textInput(['placeholder'=>'Data was not found in Database, Please fill manually']);
                            } else {
                                echo $form->field($model, 'barcode')->textInput(['value'=>$barcode]);
                            }
                        ?>

                        <?= $form->field($model, 'qty_request')->textInput(['type' => 'number', 'maxlength' => true]) ?>

                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?> 

                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
</div>
                  


<?php
$script = <<< JS

JS;
$this->registerJs($script);
?>