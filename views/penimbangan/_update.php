<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
use yii\helpers\ArrayHelper;
use app\models\ScmPlanner;
use yii\widgets\Pjax; 
// Menggunakan Pjax untuk Autorefresh
use yii\helpers\Url;
use faryshta\widgets\JqueryTagsInput;
use kartik\checkbox\CheckboxX;
/* @var $this yii\web\View */
/* @var $model app\models\Penimbangan */
/* @var $form yii\widgets\ActiveForm */
use app\models\Penimbangan;
use kartik\widgets\TimePicker;
use kartik\datetime\DateTimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\web\JsExpression;
use wbraganca\tagsinput\TagsinputWidget;

?>

        
<div class="penimbangan-form">

   <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomo')->textInput(['readOnly'=>true]) ?>

    <?php 

            echo $form->field($model, 'nama_operator', ['hintType' => ActiveField::HINT_SPECIAL,'addon' => ['prepend' => ['content'=>'<i class="fa fa-barcode"></i>']]
                ])->widget(TagsinputWidget::classname(), [
                'clientOptions' => [
                    'trimValue' => true,
                    'allowDuplicates' => false,
                    'maxChars'=> 4,
                ]
            ])->hint(' <b>Scan</b> : Klik pada isian dibawah kemudian Scan Barcode pada Nametag<p \> <b>Manual</b> : isi dengan Format 4 Huruf Kapital, Contoh : "ABCD" lalu diikuti dengan menekan Enter ');
    ?>

    <?= $form->field($model, 'edit_nama')->textInput(['readOnly'=>true,'value'=>Yii::$app->user->identity->username]) ?>

    <?php
        
        date_default_timezone_set('Asia/Jakarta');

        echo $form->field($model, 'edit_time')->textInput(['readonly' => 'true','value'=>date("Y-m-d H:i:s")])
    ?>

    <?php 
        echo '<label class="control-label">Waktu</label>';
        echo DateTimePicker::widget([
            'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
            'model' => $model,
            'attribute' => 'formatwaktu',
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd hh:ii:ss',
                'endDate' => $model->formatwaktu
            ]
        ]);
    ?>

    <?= $form->field($model, 'state')->textInput(['readOnly'=>true]) ?>

    <?= $form->field($model, 'lanjutan')->textInput(['readOnly'=>true]) ?>

    <?= $form->field($model, 'posisi')->textInput(['readOnly'=>true]) ?>

    <?= $form->field($model, 'jenis_penimbangan')->textInput(['readOnly'=>true]) ?>

    <?= $form->field($model, 'is_done')->textInput(['readOnly'=>true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

