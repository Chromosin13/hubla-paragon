<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
use yii\helpers\ArrayHelper;
use app\models\ScmPlanner;
use yii\widgets\Pjax; 
// Menggunakan Pjax untuk Autorefresh
use yii\helpers\Url;
use faryshta\widgets\JqueryTagsInput;
use kartik\checkbox\CheckboxX;
/* @var $this yii\web\View */
/* @var $model app\models\Penimbangan */
/* @var $form yii\widgets\ActiveForm */
use app\models\Penimbangan;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\web\JsExpression;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\datetime\DateTimePicker;

?>

        
<div class="penimbangan-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-blue">
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/speedometer-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username"><b>PENIMBANGAN</b></h2>
              <h5 class="widget-user-desc">INPUT</h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                
                <!-- Content Begins Here  -->

                    <!-- Isi SNFG -->
                    <div id="form_nomo" class="box-header with-border">
                        <?= $form->field($model, 'nomo', ['hintType' => ActiveField::HINT_SPECIAL, 
                                                          'addon' => [
                                                                        'prepend' => [
                                                                            'content'=>'<i class="fa fa-barcode"></i>'
                                                                        ],
                                                                        'append' => [
                                                                            'content' => Html::button('Tekan Setelah Scan',['class'=>'btn btn-primary','id'=>'afterScnBtnNomo']),
                                                                            'asButton' => true
                                                                        ]
                                                         ]
                        ])->textInput(['placeholder'=>'Klik Disini, Scan pada Barcode Nomor MO yang tertera di SPK'])->hint('PASTIKAN CAPS LOCK TIDAK AKTIF!');
                        ?>
                    </div>

                    <!-- Planner Info -->
                    <div id="planner-information">
                        <div class="box-header with-border">
                                
                                <h2 class="box-title"> <b>Planner Information</b></h2>
                                <br \>
                        </div>


                        <div class="box-header with-border">
                            <table id="table-snfg" class="table table-bordered">
                                <tbody><tr>
                                  <th>SNFG</th>
                                  <th>STREAMLINE</th>
                                  <th>START</th>
                                  <th>DUE</th>
                                  <th>BESAR BATCH</th>
                                  <th>BESAR LOT</th>
                                  <th>LOT KE</th>
                                  <th>KODE JADWAL</th>
                                  <th>STATUS</th>
                                  <th>NAMA BULK</th>
                                  <th>NAMA FG</th>
                                  <th>JUMLAH</th>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                    <!-- Last Position -->
                    <div id="last-position">

                            <div class="box-header with-border">
                                    
                                    <h2 class="box-title"> <b>Last Position</b></h2>
                                    <br \>
                            </div>


                            <div class="box-header with-border">

                                <table id="table-last" class="table table-bordered">
                                    <tbody><tr>
                                      <th style="width: 120px">SNFG Komponen</th>
                                      <th style="width: 50px">POSISI</th>
                                      <th>STATE</th>
                                      <th>JENIS PROSES</th>
                                      <th>LANJUTAN</th>
                                      <th style="width: 40px">IS DONE</th>
                                    </tr>
                                  </tbody>
                                </table>

                            </div>

                    </div>

                    <!-- Form Start Stop -->

                    <div class="box-header with-border" id="form_start_stop">
                        <a class="btn btn-success" id="start-button">
                                    <i class="fa fa-play"></i> Start
                        </a>

                        <a class="btn btn-danger" id="stop-button">
                                    <i class="fa fa-pause"></i> Stop
                        </a>
                        <p \>
                        <p \>
                        
                        <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>
                        
                        
                    </div>

                    <!-- Form Input Jadwal -->


                    <div class="box-header with-border">
                        <?= $form->field($model, 'edit_nama')->textInput(['readOnly'=>true,'value'=>Yii::$app->user->identity->username]) ?>

                        <?php
                            
                            date_default_timezone_set('Asia/Jakarta');

                            echo $form->field($model, 'edit_time')->textInput(['readonly' => 'true','value'=>date("Y-m-d H:i:s")])
                        ?>
    
                        <?php 
                            date_default_timezone_set('Asia/Jakarta');

                            echo '<label class="control-label">Waktu</label>';
                            echo DateTimePicker::widget([
                                'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                                'model' => $model,
                                'attribute' => 'waktu',
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'yyyy-mm-dd hh:ii:ss',
                                    'endDate' => date("Y-m-d H:i:s")
                                ]
                            ]);
                        ?>
                    </div>

                    <!-- Form Jenis Penimbangan -->

                    <div class="box-header with-border" id="form_jenis">
                        <!-- Jenis Penimbangan Dropdown -->
                        <div id="penimbangan-jenis_penimbangan_00">
                        <?= $form->field($model, 'jenis_penimbangan')->dropDownList(['JADWAL BARU' => 'JADWAL BARU','REWORK' => 'REWORK'],['prompt'=>'Select Option']); ?>
                        </div>

                        <!-- Jenis Penimbangan untuk State Stop (Autopopulate from Start) -->
                        <div id="penimbangan-jenis_penimbangan_0">
                        <?= $form->field($model, 'jenis_penimbangan')->textInput(['id'=>'penimbangan-jenis_penimbangan_1', 'disabled'=>'true' , 'readonly' =>'true']); ?>
                        </div>
                    </div>


                <!-- Content Ends Here -->

              </ul>
            </div>


    </div>


    <div id="form_entry_start_stop">

            <div class="box box-widget widget-user-2" id="penimbangan-start">

                    <div class="widget-user-header bg-green">
                      <div class="widget-user-image">
                        <img class="img-circle" src="../web/images/arrow-up-icon.png" alt="User Avatar">
                      </div>
                      <!-- /.widget-user-image -->
                      <h2 class="widget-user-username">Start</h2>
                      <h5 class="widget-user-desc">Entry</h5>
                    </div>

                    <div class="box-footer no-padding">
                      <ul class="nav nav-stacked">
                            
                            <!-- Content Begins Here -->
                            <div class="box-header with-border">
                                <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true','placeholder'=>'Ketika selesai memilih Jenis Penimbangan, Lanjutan secara Otomatis terisi, jika tidak, MAKA REFRESH ULANG HALAMAN (Ctrl+R) , DAN LAKUKAN SCAN ULANG DARI AWAL']) ?>

                                <?php

                                    echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                                        'data' => ArrayHelper::map(Penimbangan::find()->all()
                                        ,'nama_line','nama_line'),
                                        'options' => ['placeholder' => 'Select Line'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);


                                    // echo $form->field($model, 'nama_line')->dropDownList(
                                    //     ArrayHelper::map(Penimbangan::find()->all()
                                    //     ,'nama_line','nama_line')
                                    //     ,['prompt'=>'Select Line']

                                    // );
                                ?>

                                <!-- Nama Operator -->
                                <?php 

                                    echo $form->field($model, 'nama_operator', ['hintType' => ActiveField::HINT_SPECIAL,'addon' => ['prepend' => ['content'=>'<i class="fa fa-barcode"></i>']]
                                        ])->widget(TagsinputWidget::classname(), [
                                        'clientOptions' => [
                                            'trimValue' => true,
                                            'allowDuplicates' => false,
                                            'maxChars'=> 4,
                                        ]
                                    ])->hint(' <b>Scan</b> : Klik pada isian dibawah kemudian Scan Barcode pada Nametag<p \> <b>Manual</b> : isi dengan Format 4 Huruf Kapital, Contoh : "ABCD" lalu diikuti dengan menekan Enter ');

                                ?>
                                
                            </div>

                      </ul>
                    </div>

            </div>


            <div class="box box-widget widget-user-2" id="penimbangan-stop">

                    <div class="widget-user-header bg-red">
                      <div class="widget-user-image">
                        <img class="img-circle" src="../web/images/arrow-down-icon.png" alt="User Avatar">
                      </div>
                      <!-- /.widget-user-image -->
                      <h2 class="widget-user-username">Stop</h2>
                      <h5 class="widget-user-desc">Entry</h5>
                    </div>

                    <div class="box-footer no-padding">
                      <ul class="nav nav-stacked">
                            
                            <!-- Content Begins Here -->
                            <div class="box-header with-border">
                                
                                <?= $form->field($model, 'lanjutan')->textInput(['id'=>'penimbangan-lanjutan_stop','readonly' => 'true']) 
                                ?> 

                                <label>
                                  <input type="checkbox" id="is_kendala">
                                  Terdapat Kendala ?
                                </label>

                                <div id="kendala-form">
                                    
                                <?php DynamicFormWidget::begin([
                                        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                        'widgetBody' => '.container-items', // required: css class selector
                                        'widgetItem' => '.item', // required: css class
                                        'limit' => 3, // the maximum times, an element can be cloned (default 999)
                                        'min' => 1, // 0 or 1 (default 1)
                                        'insertButton' => '.add-item', // css class
                                        'deleteButton' => '.remove-item', // css class
                                        'model' => $modelsKendala[0],
                                        'formId' => 'dynamic-form',
                                        'formFields' => [
                                            'keterangan',
                                            'start',
                                            'stop',
                                        ],
                                        ]); 
                                    ?>

                                    <div class="container-items"><!-- widgetContainer -->
                                        <?php foreach ($modelsKendala as $i => $modelKendala): ?>
                                        <div class="item panel panel-default"><!-- widgetBody -->
                                            <div class="panel-heading">
                                                <h3 class="panel-title pull-left">Kendala</h3>
                                                <div class="pull-right">
                                                    <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="panel-body">
                                                <?php
                                                    // necessary for update action.
                                                    if (! $modelKendala->isNewRecord) {
                                                        echo Html::activeHiddenInput($modelKendala, "[{$i}]id");
                                                    }
                                                ?>

                                                <div class="row">
                                                    <div class="col-sm-3">

                                                     <?= $form->field($modelKendala, "[{$i}]keterangan")->widget(Select2::classname(), [
                                                                            'data' => ArrayHelper::map(Kendala::find()->all()
                                                                    ,'keterangan','keterangan'),
                                                                            'options' => ['placeholder' => 'Select Kendala'],
                                                                            'pluginOptions' => [
                                                                                // 'tags' => true,
                                                                                'allowClear' => true
                                                                            ],
                                                                            ]);
                                                    ?> 

                                                    </div>
                                                    <div class="col-sm-3">
                                                        <?= $form->field($modelKendala, "[{$i}]start")->widget(TimePicker::classname(), [
                                                                        'options' => ['placeholder' => 'Start'],
                                                                        'pluginOptions' => [
                                                                                'minuteStep' => 5,
                                                                                'autoclose'=>true,
                                                                                'showMeridian' => false,
                                                                                'defaultTime' => '00:00'
                                                                        ]
                                                        ]); ?>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <?= $form->field($modelKendala,"[{$i}]exception")->textInput(); ?>
                                                    </div>
                                                </div><!-- .row -->
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                        </div>
                                        <?php DynamicFormWidget::end(); ?>
                                </div>

                                <p \>

                                 <?php
                                    echo '<label class="cbx-label" for="is_done">Is Done?</label>';
                                    echo CheckboxX::widget([
                                        'model' => $model,
                                        'attribute' => 'is_done',
                                        'pluginOptions' => [
                                            'threeState' => false,
                                            'size' => 'lg'
                                        ]
                                    ]); 
                                ?>
                            </div>
                      </ul>
                    </div>

            </div>

            <div class="form-group" id="create-button">
                
                <?php echo Dialog::widget([
                        'libName' => 'krajeeDialogCust', // a custom lib name
                        'options' => [  // customized BootstrapDialog options
                            'size' => Dialog::SIZE_LARGE, // large dialog text
                            'type' => Dialog::TYPE_WARNING, // bootstrap contextual color
                            'title' => 'Konfirmasi Input',
                            'buttons' => [
                                [
                                    
                                    'id' => 'btn-ok',
                                    'icon' => 'glyphicon glyphicon-check',
                                    'label' => 'Sudah Benar, Submit!',
                                    'cssClass' => 'btn-primary',
                                    'action' => new JsExpression("function() {
                                        $('#submitButton').submit();            
                                        }")
                                    // Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
                                ],
                            ]
                        ]
                    ]);
                ?>

                <button type="button" id="btn-custom" class="btn btn-info">Validate</button>
                <button id="submitButton" type="submit" class="btn btn-success">Creates</button>
                <!-- <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?> -->
            </div>
    </div>
  

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

// Hide Submit Button untuk Konfirmasi Input
$('#submitButton').hide();


// Inisialisasi dengan Hide Form
    $('#start-button').hide();
    $('#stop-button').hide(); 
    $('#form_start_stop').hide(); 
    $('#form_jenis').hide(); 
    $('#form_entry_start_stop').hide();
    $('#last-position').hide();
    $('#planner-information').hide();



//  Tekan Setelah Scan after click event
$('#afterScnBtnNomo').click(function(){
    var nomo = $('#penimbangan-nomo').val();
    if(nomo==""){
        $('#form_start_stop').hide();    
    }
    else {
        $('#form_start_stop').fadeIn("slow"); 
    }
});


$('#afterScnBtnKomponen').click(function(){
    $('#form_start_stop').fadeIn("slow");  
});


$('#start-button').click(function(){
    $('#form_jenis').fadeIn("slow");
});

$('#stop-button').click(function(){
    $('#form_jenis').fadeIn("slow");
    $('#form_entry_start_stop').fadeIn("slow");
});


// Under Form Jenis
$('#penimbangan-jenis_penimbangan').change(function(){
    var jenis_penimbangan = $('#penimbangan-jenis_penimbangan').val();
    if(jenis_penimbangan==""){
        $('#form_entry_start_stop').hide(); 

    } else {
        $('#form_entry_start_stop').fadeIn("slow");
        var state = $('#penimbangan-state').val();
        if(state=="START"){
            $('#create-button').hide();
            
            $('#penimbangan-nama_line').change(function(){
            var nama_line = $('#penimbangan-nama_line').val();

                $.get('index.php?r=penimbangan/check-line',{ nama_line : nama_line },function(data){
                var data = $.parseJSON(data);
                    
                    if(nama_line=="Select Line"){
                        $('#create-button').hide();
                    }else if(data.state=="START"){
                        alert('Mesin ini belum di STOP oleh nomor jadwal '+data.nomo);
                        
                        //$('#create-button').hide();
                        $('#create-button').fadeIn("slow");
                    }else{
                        $('#create-button').fadeIn("slow");
                    }

                });
            });
        }
  
    }
    
});

// Validate Button

$("#btn-custom").on("click", function() {
    var nomo = $('#penimbangan-nomo').val();
    var state = $('#penimbangan-state').val();
    var jenis_penimbangan = $('#penimbangan-jenis_penimbangan').val();
    var jenis_penimbangan_1 = $('#penimbangan-jenis_penimbangan_1').val();
    var lanjutan = $('#penimbangan-lanjutan').val();
    var lanjutan_stop = $('#penimbangan-lanjutan_stop').val();
    var nama_line = $('#penimbangan-nama_line').val();
    var nama_operator =  $('#penimbangan-nama_operator').val();
    var is_done = $('#penimbangan-is_done').val();


    if(lanjutan==null||lanjutan_stop==null){
        alert('Penarikan data Lanjutan Gagal, Mohon refresh halaman dan scan ulang jadwal');
    }else if(state=="START"){
        krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(nomo)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Penimbangan</b><p>' + '<font color="blue"><i>'+(jenis_penimbangan)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>' +
                                '<b>Nama Line</b><p>' + '<font color="blue"><i>'+(nama_line)+'</i></font><p>' +  
                                '<b>Nama Operator</b><p>' + '<font color="blue"><i>'+(nama_operator)+'</i></font><p>',function(result) {}
        );
    }else if(state=="STOP"&&is_done==1){
    
        krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(nomo)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Penimbangan</b><p>' + '<font color="blue"><i>'+(jenis_penimbangan_1)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>'+
                                '<b><font color="blue"><i>Jadwal Sudah Selesai (Tidak Lanjut Shift)</i></font></b><p>',function(result) {}
        );

    }else if(state=="STOP"&&is_done==0){

        krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(nomo)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Penimbangan</b><p>' + '<font color="blue"><i>'+(jenis_penimbangan_1)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>'+
                                '<b><font color="blue"><i>Jadwal Akan Dilanjutkan ke Shift Berikutnya (Belum Done)</i></font></b><p>',function(result) {}
        );

    }

});

// 1 // SNFG Komponen / NOMO Checkbox Function


// $('#form_nomo').hide(); 

// $('#btn_nomo').click(function(){
//     document.getElementById("penimbangan-snfg_komponen").disabled = true
//     document.getElementById("penimbangan-nomo").disabled = false;
//     $('#form_snfg_komponen').hide(); 
//     $('#form_nomo').fadeIn("slow"); 
// });

$('#btn_snfg_komponen').click(function(){
    document.getElementById("penimbangan-snfg_komponen").disabled = false
    document.getElementById("penimbangan-nomo").disabled = true;
    $('#form_snfg_komponen').fadeIn("slow");
    $('#form_nomo').hide();
});

// $('#per_snfg_komponen').change(function(){
//     var per_snfg_komponen = document.getElementById("penimbangan-snfg_komponen").disabled;
//     if(per_snfg_komponen){
//         document.getElementById("penimbangan-snfg_komponen").disabled = false
//         document.getElementById("penimbangan-nomo").disabled = true;
//     }else{
//         document.getElementById("penimbangan-snfg_komponen").disabled = true
//         document.getElementById("penimbangan-nomo").disabled = false;
//     } 
// });

// 2 // Terdapat Kendala Checkbox Function

$('#is_kendala').change(function(){

    if(document.getElementById('is_kendala').checked){
            $('#kendala-form').fadeIn("slow");

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);

                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = false;
                    document.getElementById("kendala-"+(i)+"-start").disabled = false;
        
                    i++;
                }

    }else{
            $('#kendala-form').hide();

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);
                    
                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = true;
                    document.getElementById("kendala-"+(i)+"-start").disabled = true;  

                    i++;
                }

    } 
 });


// 3 // SNFG Komponen Onchange Function 

$('#penimbangan-nomo').change(function(){
    var nomo = $(this).val();

    $.post("index.php?r=scm-planner/get-kendala-timbang-nomo&nomo="+$('#penimbangan-nomo').val(), function (data){
            $("select#kendala-0-keterangan").html(data);
            
    });

    $.post("index.php?r=scm-planner/get-line-timbang-nomo&nomo="+$('#penimbangan-nomo').val(), function (data){
        $("select#penimbangan-nama_line").html(data);
    });


    $.get('index.php?r=scm-planner/get-planner-mo-raw',{ nomo : nomo },function(data){
        var json = $.parseJSON(data);
        var tr;
        for (var i = 0; i < json.length; i++) {
            tr = $('<tr/>');
            tr.append("<td><span class='badge bg-grey'>" + json[i].snfg + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].streamline + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].start + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].due + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].besar_batch + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].besar_lot + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].lot_ke + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].kode_jadwal + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].nama_bulk + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].nama_fg + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].jumlah + "</span></td>");
            tr.append("<td><span class='badge bg-green'>" + json[i].status + "</span></td>");
            $('#table-snfg').append(tr);
        }
        $('#planner-information').fadeIn("slow");
        
    });

    $.get('index.php?r=scm-planner/get-lastproses-mo',{ nomo : nomo },function(data){
        var json = $.parseJSON(data);
        var tr;
        for (var i = 0; i < json.length; i++) {
            tr = $('<tr/>');
            tr.append("<td><span class='badge bg-grey'>" + json[i].snfg_komponen + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].posisi + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].state + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].jenis_proses + "</span></td>");
            tr.append("<td><span class='badge bg-grey'>" + json[i].lanjutan + "</span></td>");
            if(json[i].is_done=="Done"){
                tr.append("<td><span class='badge bg-green'>" + json[i].is_done + "</span></td>");
            }else if(json[i].is_done=="Not Done"){
                tr.append("<td><span class='badge bg-red'>" + json[i].is_done + "</span></td>");
            }
            $('#table-last').append(tr);
        }
        $('#last-position').fadeIn("slow");
    });
    $.get('index.php?r=scm-planner/get-planner-mo',{ nomo : nomo },function(data){
        var data = $.parseJSON(data);

        if(data.status==null){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Not Found');
        }

        else if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Masih Terdapat Setidaknya 1 Komponen dari MO yang HOLD/PAUSE, Lanjutkan Per SNFG Komponen') 
        }

        else{
            $.get('index.php?r=penimbangan/get-last-resolusi',{ nojadwal : nomo },function(data){
                    var data = $.parseJSON(data);
                    console.log(data);
                    if(data===null){
                        $('#start-button').fadeIn("slow");
                        $('#stop-button').hide(); 
                        $('#start-button').click(function(){ 

                            $('#penimbangan-jenis_penimbangan').change(function(){

                                var jenis_penimbangan = $('#penimbangan-jenis_penimbangan').val();
                                var nomo = $('#penimbangan-nomo').val();                                
                                
                                    $.get('index.php?r=penimbangan/get-penimbanganmo',{ nomo : nomo, jenis_penimbangan : jenis_penimbangan },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.sediaan);
                                        $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                                        $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);

                                        document.getElementById("penimbangan-lanjutan-ist_istirahat_start").disabled = true;
                                        document.getElementById("penimbangan-lanjutan-ist_istirahat_stop").disabled = true;                                                                       
                                    
                                    });
                            });
                        });
                    }else if(data.state=="START"){
                        $('#start-button').hide();
                        $('#stop-button').fadeIn("slow"); 
                        

                        // Jenis Proses di Assign

                        $('#penimbangan-jenis_penimbangan_00').hide();
                        document.getElementById("penimbangan-jenis_penimbangan").disabled = true;
                        $('#penimbangan-jenis_penimbangan_0').fadeIn("slow");
                        document.getElementById("penimbangan-jenis_penimbangan_1").disabled = false;
                        $('#penimbangan-jenis_penimbangan_1').attr('value',data.jenis_proses);



                        $('#stop-button').click(function(){ 
                            
                            $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                            $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);
                            
                            $('#penimbangan-jenis_penimbangan').change(function(){
                                var jenis_penimbangan = $('#penimbangan-jenis_penimbangan_1').val();
                                var nomo = $('#penimbangan-nomo').val();                                
                                
                                    $.get('index.php?r=penimbangan/get-penimbanganmo',{ nomo : nomo, jenis_penimbangan : jenis_penimbangan },function(data){
                                        var data = $.parseJSON(data);

                                        $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                                        $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);                                                                  
                                    
                                    });
                            });

                        });


                        // EO Lanjutan dan Lanjutan Istirahat 

                    }else if(data.state=="STOP"){
                        $('#start-button').fadeIn("slow");
                        $('#stop-button').hide(); 
                        $('#start-button').click(function(){ 

                            $('#penimbangan-jenis_penimbangan').change(function(){

                                var jenis_penimbangan = $('#penimbangan-jenis_penimbangan').val();
                                var nomo = $('#penimbangan-nomo').val();                                
                                
                                    $.get('index.php?r=penimbangan/get-penimbanganmo',{ nomo : nomo, jenis_penimbangan : jenis_penimbangan },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.sediaan);
                                        $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                                        $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);

                                        document.getElementById("penimbangan-lanjutan-ist_istirahat_start").disabled = true;
                                        document.getElementById("penimbangan-lanjutan-ist_istirahat_stop").disabled = true;                                                                       
                                    
                                    });
                            });
                        });
                    }
                });
        }
    });

});

$(function() {

    // Default Hide and Disable DynamicFormWidget for Kendala Model //

        $('#kendala-form').hide();
            document.getElementById("kendala-0-keterangan").disabled = true;
            document.getElementById("kendala-0-start").disabled = true;

    // DynamicForm override JS , Script Fix for non-working kartik date/timepicker afterInsert 
    // DynamicForm override JS , Fix for select2 kendala list

        // ID untuk penomoran nested dynamic form
        var id = 0;

        // Setelah Insert Button
        $(".dynamicform_wrapper").on('afterInsert', function(e, item) {
            
            var dateTimePickers = $(this).find('[data-krajee-kvtimepicker]');
            dateTimePickers.each(function(index, el) {
                $(this).parent().removeData().kvTimepicker('remove');
                $(this).parent().kvTimepicker(eval($(this).attr('data-krajee-kvtimepicker')));
            });
            

            // Override Fix for select2 kendala list 

            id += 1;
            console.log(id);


            $.post("index.php?r=scm-planner/get-kendala-timbang-komponen&snfg_komponen="+$('#penimbangan-snfg_komponen').val(), function (data){

                    $("select#kendala-"+(id)+"-keterangan").html(data);

            });

            $.post("index.php?r=scm-planner/get-kendala-timbang-nomo&nomo="+$('#penimbangan-nomo').val(), function (data){

                    $("select#kendala-"+(id)+"-keterangan").html(data);

            });


        });

        // Setelah Delete Button

        $(".dynamicform_wrapper").on('afterDelete', function(e, item) {
            
            // Override Fix for select2 kendala list 

            id -= 1;
            console.log(id);

        });


        // Jika Mencapai Batas Maximum
        $(".dynamicform_wrapper").on('limitReached', function(e, item) {
            alert('Maximum 3');
        });


    $('#penimbangan-jenis_penimbangan_0').hide();
    document.getElementById("penimbangan-jenis_penimbangan_1").disabled = true;
    $('#penimbangan-start').hide();
    $('#penimbangan-stop').hide(); 

    $('#start-button').click(function(){
            var start = "START";
            $('#penimbangan-state').attr('value',start);
            $('#penimbangan-start').fadeIn("slow");
            $('#penimbangan-stop').hide(); 
    

            $('#penimbangan-jenis_penimbangan').change(function(){

                var jenis_penimbangan = $('#penimbangan-jenis_penimbangan').val();
                var snfg_komponen = $('#penimbangan-snfg_komponen').val(); 
                var nomo = $('#penimbangan-nomo').val(); 
                $.get('index.php?r=penimbangan/get-penimbanganmo',{ nomo : nomo, jenis_penimbangan : jenis_penimbangan },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    
                                    $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                                    $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);
                                    
                }); 
            });

            //                               
        });


    $('#stop-button').click(function(){
            var stop = "STOP";
            $('#penimbangan-state').attr('value',stop);
                    $('#penimbangan-stop').fadeIn("slow"); 
                    $('#penimbangan-start').hide(); 
                    $('#penimbangan-istirahat-start').hide(); 
                    $('#penimbangan-istirahat-stop').hide();
             $('#penimbangan-jenis_penimbangan').change(function(){
                var jenis_penimbangan = $('#penimbangan-jenis_penimbangan').val();
                var snfg_komponen = $('#penimbangan-snfg_komponen').val(); 
                var nomo = $('#penimbangan-nomo').val(); 
                var per_snfg_komponen = document.getElementById("penimbangan-snfg_komponen").disabled;
                            if(per_snfg_komponen){
                                $.get('index.php?r=penimbangan/get-penimbanganmo',{ nomo : nomo, jenis_penimbangan : jenis_penimbangan },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                                    $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);

                                }); 
                            }else{
                                $.get('index.php?r=penimbangan/get-penimbangan',{ snfg_komponen : snfg_komponen, jenis_penimbangan : jenis_penimbangan },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                                    $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);
                                }); 
                            } 
            });              
        });

    });

JS;
$this->registerJs($script);
?>