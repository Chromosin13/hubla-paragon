<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
use yii\helpers\ArrayHelper;
use app\models\ScmPlanner;
use yii\widgets\Pjax; 
// Menggunakan Pjax untuk Autorefresh
use yii\helpers\Url;
use faryshta\widgets\JqueryTagsInput;
use kartik\checkbox\CheckboxX;
/* @var $this yii\web\View */
/* @var $model app\models\Penimbangan */
/* @var $form yii\widgets\ActiveForm */
use app\models\Penimbangan;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\web\JsExpression;


?>

        
<div class="penimbangan-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <div class="info-box bg-aqua">
        <span class="info-box-icon"><i class="fa fa-balance-scale"></i></span>

        <div class="info-box-content">
          <b><div class="col-md-3 col-sm-6 col-xs-12" id="nama_bulk_results"></div></b>
          <!-- <div id="nama_fg_results"></div> -->
          <span class="info-box-number"><div id="nomo_results"></div><div id="snfg_komponen_results"></div></span>

          <div class="progress">
            <div class="progress-bar" style="width: 100%"></div>
          </div>
              <span class="progress-description">
              <div class="col-md-3 col-sm-6 col-xs-12" id="streamline_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="start_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="due_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="besar_batch_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="kode_jadwal_results"></div>
              </span>
        </div>
        <!-- /.info-box-content -->
    </div>

    <div class="box">
    <!-- <div class="col-md-4"> -->
                <div class="box-header with-border">
                    <h4><b>Pilih Jenis Scan</b></h4>            
                    <?=
                            Html::button('<b>Per-MO</b>',['class'=>'btn btn-success', 'id' => 'btn_nomo','style' => 'width: 120px; border-radius: 5px;']);
                    ?>
                    <!-- Change Per 8 Maret, Hilangkan Per-Komponen -->
                    <!-- 
                        <?=
                            Html::button('<b>Per-Komponen</b>',['class'=>'btn btn-primary', 'id' => 'btn_snfg_komponen', 'style' => 'width: 120px; border-radius: 5px;']);
                        ?> 
                    -->

                    <!--                     
                    <label>
                      <input type="checkbox" id="per_snfg_komponen">
                      Insert Per SNFG Komponen (Rework) ?
                    </label> 
                    -->
                </div>

                <div style="width:50%; float: left; display: inline-block;" class="box-header with-border">

                    <h2 class="box-title"> <b>Input dan Informasi SNFG dari Planner</b></h2>
                    <br \>
                    <br \>
                    <div id="form_nomo">
                        <?= $form->field($model, 'nomo', ['hintType' => ActiveField::HINT_SPECIAL, 
                                                          'addon' => [
                                                                        'prepend' => [
                                                                            'content'=>'<i class="fa fa-barcode"></i>'
                                                                        ],
                                                                        'append' => [
                                                                            'content' => Html::button('Tekan Setelah Scan',['class'=>'btn btn-primary','id'=>'afterScnBtnNomo']),
                                                                            'asButton' => true
                                                                        ]
                                                         ]
                        ])->textInput(['placeholder'=>'Klik Disini, Scan pada Barcode Nomor MO yang tertera di SPK'])->hint('PASTIKAN CAPS LOCK TIDAK AKTIF!');
                        ?>
                    </div>

                    <div id="form_snfg_komponen">
                        <?= $form->field($model, 'snfg_komponen',  ['hintType' => ActiveField::HINT_SPECIAL, 
                                                                    'addon' => [
                                                                                'append' => [
                                                                                    'content'=>'<i class="fa fa-barcode"></i>'
                                                                                ],
                                                                                'prepend' => [
                                                                                    'content' => Html::button('Tekan Setelah Scan',['class'=>'btn btn-primary','id'=>'afterScnBtnKomponen']),
                                                                                    'asButton' => true
                                                                                ]
                                                            ]
                        ])->textInput(['disabled'=>'true','placeholder'=>'Klik Disini, Scan pada Barcode SNFG Komponen yang tertera di SPK'])->hint('Klik Pada Isian dibawah ini, kemudian arahkan scan pada barcode Komponen yang tertera di SPK');
                        ?>
                    </div>

                    <p \>
                    <b>SNFG</b>
                         <input type="text" class="form-control" id="snfg" disabled>

                    <br \>
                    <!-- Button untuk change event START, STOP -->
                    <div id="form_start_stop">
                        <a class="btn btn-success" id="start-button">
                                    <i class="fa fa-play"></i> Start
                        </a>

                        <a class="btn btn-danger" id="stop-button">
                                    <i class="fa fa-pause"></i> Stop
                        </a>
                        <p \>
                        <p \>
                        
                        <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>
                        
                        
                    </div>
                    
                </div>


                <div style="width:50%; display: inline-block;" class="box-header with-border">
                    <h2 class="box-title"><b>Posisi Terakhir</b></h2>
                    <br \>
                    <br \>
                    <b>Posisi</b>
                    <input type="text" class="form-control" id="ppr-posisi" placeholder="" disabled>
                    <p \>
                    <b>State</b>
                    <input type="text" class="form-control" id="ppr-state" placeholder="" disabled>
                    <p \>
                    <b>Jenis Proses</b>
                    <input type="text" class="form-control" id="ppr-jenis-proses" placeholder="" disabled>
                    <p \>
                    <b>Lanjutan</b>
                    <input type="text" class="form-control" id="ppr-lanjutan" placeholder="" disabled>
                    <br \>
                    <br \>
                </div>
    </div>

    <div id="form_jenis">
        <!-- Jenis Penimbangan Dropdown -->
        <div id="penimbangan-jenis_penimbangan_00">
        <?= $form->field($model, 'jenis_penimbangan')->dropDownList(['JADWAL BARU' => 'JADWAL BARU','REWORK' => 'REWORK'],['prompt'=>'Select Option']); ?>
        </div>

        <!-- Jenis Penimbangan untuk State Stop (Autopopulate from Start) -->
        <div id="penimbangan-jenis_penimbangan_0">
        <?= $form->field($model, 'jenis_penimbangan')->textInput(['id'=>'penimbangan-jenis_penimbangan_1', 'disabled'=>'true' , 'readonly' =>'true']); ?>
        </div>
    </div>


    <div id="form_entry_start_stop">
        <div class="box" id="penimbangan-start">
                <div class="box-header with-border">
                  <h2 class="box-title">Start Entry</h2>

                    <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true','placeholder'=>'Ketika selesai memilih Jenis Penimbangan, Lanjutan secara Otomatis terisi, jika tidak, MAKA REFRESH ULANG HALAMAN (Ctrl+R) , DAN LAKUKAN SCAN ULANG DARI AWAL']) ?>

                    <?= $form->field($model, 'nama_line')->dropDownList(
                        ArrayHelper::map(Penimbangan::find()->all()
                        ,'nama_line','nama_line')
                        ,['prompt'=>'Select Line']

                    );?>

                    <?php echo $form->field($model, 'nama_operator', ['hintType' => ActiveField::HINT_SPECIAL, 'addon' => ['prepend' => ['content'=>'<i class="fa fa-barcode"></i>']]
                            ])->widget(JqueryTagsInput::className([]))->hint(' <b>Scan</b> : Klik pada isian dibawah kemudian Scan Barcode pada Nametag<p \> <b>Manual</b> : isi dengan Format 4 Huruf Kapital, Contoh : "ABCD" lalu diikuti dengan menekan Enter ');
                    ?>
                </div>
        </div>

        <div class="box" id="penimbangan-stop">
                <div class="box-header with-border">
                  <h2 class="box-title">Stop Entry</h2>
                    
                    <?= $form->field($model, 'lanjutan')->textInput(['id'=>'penimbangan-lanjutan_stop','readonly' => 'true']) 
                    ?> 

                    <label>
                      <input type="checkbox" id="is_kendala">
                      Terdapat Kendala ?
                    </label>

                    <div id="kendala-form">
                        
                    <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-items', // required: css class selector
                            'widgetItem' => '.item', // required: css class
                            'limit' => 3, // the maximum times, an element can be cloned (default 999)
                            'min' => 1, // 0 or 1 (default 1)
                            'insertButton' => '.add-item', // css class
                            'deleteButton' => '.remove-item', // css class
                            'model' => $modelsKendala[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'keterangan',
                                'start',
                                'stop',
                            ],
                            ]); 
                        ?>

                        <div class="container-items"><!-- widgetContainer -->
                            <?php foreach ($modelsKendala as $i => $modelKendala): ?>
                            <div class="item panel panel-default"><!-- widgetBody -->
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left">Kendala</h3>
                                    <div class="pull-right">
                                        <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                        <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                        // necessary for update action.
                                        if (! $modelKendala->isNewRecord) {
                                            echo Html::activeHiddenInput($modelKendala, "[{$i}]id");
                                        }
                                    ?>

                                    <div class="row">
                                        <div class="col-sm-3">

                                         <?= $form->field($modelKendala, "[{$i}]keterangan")->widget(Select2::classname(), [
                                                                'data' => ArrayHelper::map(Kendala::find()->all()
                                                        ,'keterangan','keterangan'),
                                                                'options' => ['placeholder' => 'Select Kendala'],
                                                                'pluginOptions' => [
                                                                    // 'tags' => true,
                                                                    'allowClear' => true
                                                                ],
                                                                ]);
                                        ?> 

                                        </div>
                                        <div class="col-sm-3">
                                            <?= $form->field($modelKendala, "[{$i}]start")->widget(TimePicker::classname(), [
                                                            'options' => ['placeholder' => 'Start'],
                                                            'pluginOptions' => [
                                                                    'minuteStep' => 5,
                                                                    'autoclose'=>true,
                                                                    'showMeridian' => false,
                                                                    'defaultTime' => '00:00'
                                                            ]
                                            ]); ?>
                                        </div>
                                    </div><!-- .row -->
                                </div>
                            </div>
                            <?php endforeach; ?>
                            </div>
                            <?php DynamicFormWidget::end(); ?>
                    </div>

                    <p \>

                     <?php
                        echo '<label class="cbx-label" for="is_done">Is Done?</label>';
                        echo CheckboxX::widget([
                            'model' => $model,
                            'attribute' => 'is_done',
                            'pluginOptions' => [
                                'threeState' => false,
                                'size' => 'lg'
                            ]
                        ]); 
                    ?>
                </div>
        </div>

        <div class="form-group" id="create-button">
            <?php echo Dialog::widget([
                        'libName' => 'krajeeDialogCust', // a custom lib name
                        'options' => [  // customized BootstrapDialog options
                            'size' => Dialog::SIZE_LARGE, // large dialog text
                            'type' => Dialog::TYPE_WARNING, // bootstrap contextual color
                            'title' => 'Konfirmasi Input',
                            'buttons' => [
                                [
                                    
                                    'id' => 'btn-ok',
                                    'icon' => 'glyphicon glyphicon-check',
                                    'label' => 'Sudah Benar, Submit!',
                                    'cssClass' => 'btn-primary',
                                    'action' => new JsExpression("function() {
                                        $('#submitButton').submit();            
                                        }")
                                    // Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
                                ],
                            ]
                        ]
                    ]);
            ?>

            <button type="button" id="btn-custom" class="btn btn-info">Validate</button>
            <button id="submitButton" type="submit" class="btn btn-success">Creates</button>
            <!-- <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?> -->

        </div>
    </div>

    

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

// Hide Submit Button untuk Konfirmasi Input
$('#submitButton').hide();


// Inisialisasi dengan Hide Form
    $('#start-button').hide();
    $('#stop-button').hide(); 
    $('#form_start_stop').hide(); 
    $('#form_jenis').hide(); 
    $('#form_entry_start_stop').hide();

//  Tekan Setelah Scan after click event
$('#afterScnBtnNomo').click(function(){
    var nomo = $('#penimbangan-nomo').val();
    if(nomo==""){
        $('#form_start_stop').hide();    
    }
    else {
        $('#form_start_stop').show(); 
    }
});


$('#afterScnBtnKomponen').click(function(){
    $('#form_start_stop').show();  
});


$('#start-button').click(function(){
    $('#form_jenis').show();
});

$('#stop-button').click(function(){
    $('#form_jenis').show();
    $('#form_entry_start_stop').show();
});


// Under Form Jenis
$('#penimbangan-jenis_penimbangan').change(function(){
    var jenis_penimbangan = $('#penimbangan-jenis_penimbangan').val();
    if(jenis_penimbangan==""){
        $('#form_entry_start_stop').hide(); 

    } else {
        $('#form_entry_start_stop').show();
        var state = $('#penimbangan-state').val();
        if(state=="START"){
            $('#create-button').hide();
            $('#penimbangan-nama_line').change(function(){
                var nama_line = $('#penimbangan-nama_line').val();
                if(nama_line=="Select Line"){
                    $('#create-button').hide();
                }else{
                    $('#create-button').show();
                }
            });
        }
  
    }
    
});

// Validate Button

$("#btn-custom").on("click", function() {
    var nomo = $('#penimbangan-nomo').val();
    var state = $('#penimbangan-state').val();
    var jenis_penimbangan = $('#penimbangan-jenis_penimbangan').val();
    var jenis_penimbangan_1 = $('#penimbangan-jenis_penimbangan_1').val();
    var lanjutan = $('#penimbangan-lanjutan').val();
    var lanjutan_stop = $('#penimbangan-lanjutan_stop').val();
    var nama_line = $('#penimbangan-nama_line').val();
    var nama_operator =  $('#penimbangan-nama_operator').val();
    var is_done = $('#penimbangan-is_done').val();


    if(lanjutan==null||lanjutan_stop==null){
        alert('Penarikan data Lanjutan Gagal, Mohon refresh halaman dan scan ulang jadwal');
    }else if(state=="START"){
        krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(nomo)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Penimbangan</b><p>' + '<font color="blue"><i>'+(jenis_penimbangan)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>' +
                                '<b>Nama Line</b><p>' + '<font color="blue"><i>'+(nama_line)+'</i></font><p>' +  
                                '<b>Nama Operator</b><p>' + '<font color="blue"><i>'+(nama_operator)+'</i></font><p>',function(result) {}
        );
    }else if(state=="STOP"&&is_done==1){
    
        krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(nomo)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Penimbangan</b><p>' + '<font color="blue"><i>'+(jenis_penimbangan_1)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>'+
                                '<b><font color="blue"><i>Jadwal Sudah Selesai (Tidak Lanjut Shift)</i></font></b><p>',function(result) {}
        );

    }else if(state=="STOP"&&is_done==0){

        krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(nomo)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Penimbangan</b><p>' + '<font color="blue"><i>'+(jenis_penimbangan_1)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>'+
                                '<b><font color="blue"><i>Jadwal Akan Dilanjutkan ke Shift Berikutnya (Belum Done)</i></font></b><p>',function(result) {}
        );

    }

});

// 1 // SNFG Komponen / NOMO Checkbox Function

$('#form_snfg_komponen').hide(); 
$('#form_nomo').hide(); 

$('#btn_nomo').click(function(){
    document.getElementById("penimbangan-snfg_komponen").disabled = true
    document.getElementById("penimbangan-nomo").disabled = false;
    $('#form_snfg_komponen').hide(); 
    $('#form_nomo').show(); 
});

$('#btn_snfg_komponen').click(function(){
    document.getElementById("penimbangan-snfg_komponen").disabled = false
    document.getElementById("penimbangan-nomo").disabled = true;
    $('#form_snfg_komponen').show();
    $('#form_nomo').hide();
});

// $('#per_snfg_komponen').change(function(){
//     var per_snfg_komponen = document.getElementById("penimbangan-snfg_komponen").disabled;
//     if(per_snfg_komponen){
//         document.getElementById("penimbangan-snfg_komponen").disabled = false
//         document.getElementById("penimbangan-nomo").disabled = true;
//     }else{
//         document.getElementById("penimbangan-snfg_komponen").disabled = true
//         document.getElementById("penimbangan-nomo").disabled = false;
//     } 
// });

// 2 // Terdapat Kendala Checkbox Function

$('#is_kendala').change(function(){

    if(document.getElementById('is_kendala').checked){
            $('#kendala-form').show();

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);

                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = false;
                    document.getElementById("kendala-"+(i)+"-start").disabled = false;
        
                    i++;
                }

    }else{
            $('#kendala-form').hide();

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);
                    
                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = true;
                    document.getElementById("kendala-"+(i)+"-start").disabled = true;  

                    i++;
                }

    } 
 });


// 3 // SNFG Komponen Onchange Function 

$('#penimbangan-snfg_komponen').change(function(){

    var snfg_komponen = $(this).val();

    // Populate the First Nested Form of Keterangan Kendala 
    // Jump to afterInsert and afterDelete

    $.post("index.php?r=scm-planner/get-kendala-timbang-komponen&snfg_komponen="+$('#penimbangan-snfg_komponen').val(), function (data){
            $("select#kendala-0-keterangan").html(data);
    });

    // Populate Nama Line Dropdown

    $.post("index.php?r=scm-planner/get-line-timbang-komponen&snfg_komponen="+$('#penimbangan-snfg_komponen').val(), function (data){
        $("select#penimbangan-nama_line").html(data);
    });

    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#penimbangan-nomo').attr('value',data.nomo);
        $('#snfg').attr('value',data.snfg);
        
        $('#streamline_results').html('STREAMLINE : '+data.streamline);
        $('#start_results').html('START : '+ data.start);
        $('#due_results').html('DUE : '+ data.due);
        $('#nama_bulk_results').html(data.nama_bulk);
        $('#nama_fg_results').html(data.nama_fg);
        $('#besar_batch_results').html('BESAR BATCH : '+ data.besar_batch);
        $('#besar_lot_results').html('BESAR LOT : '+ data.besar_lot);
        $('#lot_ke_results').html('LOT KE : '+ data.lot); ;
        $('#kode_jadwal_results').html('KODE JADWAL : '+ data.kode_jadwal); 
        $('#jumlah_results').html('JUMLAH : '+ data.jumlah);
        
    });
    $.get('index.php?r=scm-planner/get-ppr',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);

        if(data.status==null){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#create-button').hide();
            alert('Not Found');
        }

        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#create-button').hide();
            alert('Status ditetapkan sebagai HOLD/PAUSE dari Planner') 
        }else{
                $.get('index.php?r=scm-planner/get-last',{ snfg_komponen : snfg_komponen },function(data){
                    var data = $.parseJSON(data);
                    if(data.posisi=="PENIMBANGAN" && data.state=="START"){
                        $('#start-button').hide();
                        $('#stop-button').show(); 

                        $('#penimbangan-jenis_penimbangan_00').hide();
                        document.getElementById("penimbangan-jenis_penimbangan").disabled = true;
                        $('#penimbangan-jenis_penimbangan_0').show();
                        document.getElementById("penimbangan-jenis_penimbangan_1").disabled = false;
                        $('#penimbangan-jenis_penimbangan_1').attr('value',data.jenis_proses);

                        alert('Stop');
                        // Assign Lanjutan 

                        $('#stop-button').click(function(){ 

                            var jenis_penimbangan = $('#penimbangan-jenis_penimbangan_1').val();
                            var snfg_komponen = $('#penimbangan-snfg_komponen').val();                                
                            
                                $.get('index.php?r=penimbangan/get-penimbangan',{ snfg_komponen : snfg_komponen, jenis_penimbangan : jenis_penimbangan },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);

                                    $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                                    $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);

                                    alert(data.lanjutan);
                                    // Disable Post Value Lanjutan Istirahat

                                                                      
                                
                                });

                        });


                        // EO Lanjutan 
                                                   
                    
                    }else if(data.posisi=="PENIMBANGAN" && data.state=="STOP"){
                        $('#start-button').show();
                        $('#stop-button').hide(); 
                        $('#istirahat-start-button').hide(); 
                        $('#istirahat-stop-button').hide(); 

                        $('#start-button').click(function(){ 

                            $('#penimbangan-jenis_penimbangan').change(function(){

                                var jenis_penimbangan = $('#penimbangan-jenis_penimbangan').val();
                                var snfg_komponen = $('#penimbangan-snfg_komponen').val();                                
                                
                                    $.get('index.php?r=penimbangan/get-penimbangan',{ snfg_komponen : snfg_komponen, jenis_penimbangan : jenis_penimbangan },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.sediaan);
 
                                        $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                                        $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);

                                    
                                    });
                            });
                        });

                    }else if (data==null) {
                        $('#start-button').hide();
                        $('#stop-button').hide(); 
                        alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                    }
                });
        }
    });
});

$('#penimbangan-nomo').change(function(){
    var nomo = $(this).val();

    $.post("index.php?r=scm-planner/get-kendala-timbang-nomo&nomo="+$('#penimbangan-nomo').val(), function (data){
            $("select#kendala-0-keterangan").html(data);
            
    });

    $.post("index.php?r=scm-planner/get-line-timbang-nomo&nomo="+$('#penimbangan-nomo').val(), function (data){
        $("select#penimbangan-nama_line").html(data);
    });


    $.get('index.php?r=scm-planner/get-planner-mo',{ nomo : nomo },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#penimbangan-snfg_komponen').attr('value',data.snfg_komponen);
        $('#snfg').attr('value',data.snfg);

        
        $('#streamline_results').html('STREAMLINE : '+data.streamline);
        $('#start_results').html('START : '+ data.start);
        $('#due_results').html('DUE : '+ data.due);
        $('#nama_bulk_results').html(data.nama_bulk);
        $('#nama_fg_results').html(data.nama_fg);
        $('#besar_batch_results').html('BESAR BATCH : '+ data.besar_batch);
        $('#besar_lot_results').html('BESAR LOT : '+ data.besar_lot);
        $('#lot_ke_results').html('LOT KE : '+ data.lot); ;
        $('#kode_jadwal_results').html('KODE JADWAL : '+ data.kode_jadwal); 
        $('#jumlah_results').html('JUMLAH : '+ data.jumlah);


    });
    $.get('index.php?r=scm-planner/get-pp',{ nomo : nomo },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
    });
    $.get('index.php?r=scm-planner/get-planner-mo',{ nomo : nomo },function(data){
        var data = $.parseJSON(data);

        if(data.status==null){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Not Found');
        }

        else if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Masih Terdapat Setidaknya 1 Komponen dari MO yang HOLD/PAUSE, Lanjutkan Per SNFG Komponen') 
        }

        else{
            $.get('index.php?r=scm-planner/get-last-mo',{ nomo : nomo },function(data){
                    var data = $.parseJSON(data);
                    if(data.posisi=="PENIMBANGAN" && data.state=="START"){
                        $('#start-button').hide();
                        $('#stop-button').show(); 
                        

                        // Jenis Proses di Assign

                        $('#penimbangan-jenis_penimbangan_00').hide();
                        document.getElementById("penimbangan-jenis_penimbangan").disabled = true;
                        $('#penimbangan-jenis_penimbangan_0').show();
                        document.getElementById("penimbangan-jenis_penimbangan_1").disabled = false;
                        $('#penimbangan-jenis_penimbangan_1').attr('value',data.jenis_proses);



                        $('#stop-button').click(function(){ 
                            
                            $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                            $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);
                            
                            $('#penimbangan-jenis_penimbangan').change(function(){
                                var jenis_penimbangan = $('#penimbangan-jenis_penimbangan_1').val();
                                var nomo = $('#penimbangan-nomo').val();                                
                                
                                    $.get('index.php?r=penimbangan/get-penimbanganmo',{ nomo : nomo, jenis_penimbangan : jenis_penimbangan },function(data){
                                        var data = $.parseJSON(data);

                                        $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                                        $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);                                                                  
                                    
                                    });
                            });

                        });


                        // EO Lanjutan dan Lanjutan Istirahat 

                    }else if(data.posisi=="PENIMBANGAN" && data.state=="STOP"){
                        $('#start-button').show();
                        $('#stop-button').hide(); 
                        $('#istirahat-start-button').hide(); 
                        $('#istirahat-stop-button').hide(); 

                        $('#start-button').click(function(){ 

                            $('#penimbangan-jenis_penimbangan').change(function(){

                                var jenis_penimbangan = $('#penimbangan-jenis_penimbangan').val();
                                var nomo = $('#penimbangan-nomo').val();                                
                                
                                    $.get('index.php?r=penimbangan/get-penimbanganmo',{ nomo : nomo, jenis_penimbangan : jenis_penimbangan },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.sediaan);
                                        $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                                        $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);

                                        document.getElementById("penimbangan-lanjutan-ist_istirahat_start").disabled = true;
                                        document.getElementById("penimbangan-lanjutan-ist_istirahat_stop").disabled = true;                                                                       
                                    
                                    });
                            });
                        });

                    }else if(data.posisi!="PENIMBANGAN" && data!=null){
                        $('#start-button').show();
                        $('#stop-button').hide();
                        alert('Jadwal Baru');
                    }else if(data==null){
                        $('#start-button').hide();
                        $('#stop-button').hide(); 
                        $('#istirahat-start-button').hide(); 
                        $('#istirahat-stop-button').hide();
                        alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                    }
                });
        }
    });

});

$(function() {

    // Default Hide and Disable DynamicFormWidget for Kendala Model //

        $('#kendala-form').hide();
            document.getElementById("kendala-0-keterangan").disabled = true;
            document.getElementById("kendala-0-start").disabled = true;

    // DynamicForm override JS , Script Fix for non-working kartik date/timepicker afterInsert 
    // DynamicForm override JS , Fix for select2 kendala list

        // ID untuk penomoran nested dynamic form
        var id = 0;

        // Setelah Insert Button
        $(".dynamicform_wrapper").on('afterInsert', function(e, item) {
            
            var dateTimePickers = $(this).find('[data-krajee-kvtimepicker]');
            dateTimePickers.each(function(index, el) {
                $(this).parent().removeData().kvTimepicker('remove');
                $(this).parent().kvTimepicker(eval($(this).attr('data-krajee-kvtimepicker')));
            });
            

            // Override Fix for select2 kendala list 

            id += 1;
            console.log(id);


            $.post("index.php?r=scm-planner/get-kendala-timbang-komponen&snfg_komponen="+$('#penimbangan-snfg_komponen').val(), function (data){

                    $("select#kendala-"+(id)+"-keterangan").html(data);

            });

            $.post("index.php?r=scm-planner/get-kendala-timbang-nomo&nomo="+$('#penimbangan-nomo').val(), function (data){

                    $("select#kendala-"+(id)+"-keterangan").html(data);

            });


        });

        // Setelah Delete Button

        $(".dynamicform_wrapper").on('afterDelete', function(e, item) {
            
            // Override Fix for select2 kendala list 

            id -= 1;
            console.log(id);

        });


        // Jika Mencapai Batas Maximum
        $(".dynamicform_wrapper").on('limitReached', function(e, item) {
            alert('Maximum 3');
        });


    $('#penimbangan-jenis_penimbangan_0').hide();
    document.getElementById("penimbangan-jenis_penimbangan_1").disabled = true;
    $('#penimbangan-start').hide();
    $('#penimbangan-stop').hide(); 

    $('#start-button').click(function(){
            var start = "START";
            $('#penimbangan-state').attr('value',start);
                    $('#penimbangan-start').show();
                    $('#penimbangan-stop').hide(); 
    

            $('#penimbangan-jenis_penimbangan').change(function(){

                var jenis_penimbangan = $('#penimbangan-jenis_penimbangan').val();
                var snfg_komponen = $('#penimbangan-snfg_komponen').val(); 
                var nomo = $('#penimbangan-nomo').val(); 
                var per_snfg_komponen = document.getElementById("penimbangan-snfg_komponen").disabled;
                            if(per_snfg_komponen){
                                $.get('index.php?r=penimbangan/get-penimbanganmo',{ nomo : nomo, jenis_penimbangan : jenis_penimbangan },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    
                                    $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                                    $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);
                                    
                                }); 
                            }else{
                                $.get('index.php?r=penimbangan/get-penimbangan',{ snfg_komponen : snfg_komponen, jenis_penimbangan : jenis_penimbangan },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);

                                    $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                                    $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);

                                }); 
                            } 
            });

            //                               
        });


    $('#stop-button').click(function(){
            var stop = "STOP";
            $('#penimbangan-state').attr('value',stop);
                    $('#penimbangan-stop').show(); 
                    $('#penimbangan-start').hide(); 
                    $('#penimbangan-istirahat-start').hide(); 
                    $('#penimbangan-istirahat-stop').hide();
             $('#penimbangan-jenis_penimbangan').change(function(){
                var jenis_penimbangan = $('#penimbangan-jenis_penimbangan').val();
                var snfg_komponen = $('#penimbangan-snfg_komponen').val(); 
                var nomo = $('#penimbangan-nomo').val(); 
                var per_snfg_komponen = document.getElementById("penimbangan-snfg_komponen").disabled;
                            if(per_snfg_komponen){
                                $.get('index.php?r=penimbangan/get-penimbanganmo',{ nomo : nomo, jenis_penimbangan : jenis_penimbangan },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                                    $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);

                                }); 
                            }else{
                                $.get('index.php?r=penimbangan/get-penimbangan',{ snfg_komponen : snfg_komponen, jenis_penimbangan : jenis_penimbangan },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#penimbangan-lanjutan').attr('value',data.lanjutan);
                                    $('#penimbangan-lanjutan_stop').attr('value',data.lanjutan);
                                }); 
                            } 
            });              
        });

    });

JS;
$this->registerJs($script);
?>