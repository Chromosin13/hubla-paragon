<?php

$script = <<< JS
$('input').keydown( function (event) { //event==Keyevent
    if(event.which == 13) {
        var inputs = $(this).closest('form').find(':input:visible');
        inputs.eq( inputs.index(this)+ 1 ).focus();
        event.preventDefault(); //Disable standard Enterkey action
    }
    // event.preventDefault(); <- Disable all keys  action
});
JS;
$this->registerJs($script);
?>


<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\models\Standard;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Penimbangan */

// $this->title = 'Penimbangan';
// $this->params['breadcrumbs'][] = ['label' => 'Penimbangans', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="penimbangan-create">

    <?= $this->render('_form_resolusi', [
        'model' => $model,
        'modelsKendala' => $modelsKendala,
    ]) ?>

</div>
