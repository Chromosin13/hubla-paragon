<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ListProses */

$this->title = 'Create List Proses';
$this->params['breadcrumbs'][] = ['label' => 'List Proses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="list-proses-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
