<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Kendala */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kendalas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kendala-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'keterangan',
            'start',
            'stop',
            'penimbangan_id',
            'pengolahan_id',
            'qc_bulk_id',
            'kemas_1_id',
            'kemas_2_id',
            'qc_fg_id',
        ],
    ]) ?>

</div>
