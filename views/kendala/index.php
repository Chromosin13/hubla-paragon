<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KendalaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kendalas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kendala-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Kendala', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'keterangan',
            'start',
            'stop',
            'penimbangan_id',
            // 'pengolahan_id',
            // 'qc_bulk_id',
            // 'kemas_1_id',
            // 'kemas_2_id',
            // 'qc_fg_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
