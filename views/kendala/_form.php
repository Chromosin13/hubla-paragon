<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kendala */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kendala-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'keterangan')->textInput() ?>

    <?= $form->field($model, 'start')->textInput() ?>

    <?= $form->field($model, 'stop')->textInput() ?>

    <?= $form->field($model, 'penimbangan_id')->textInput() ?>

    <?= $form->field($model, 'pengolahan_id')->textInput() ?>

    <?= $form->field($model, 'qc_bulk_id')->textInput() ?>

    <?= $form->field($model, 'kemas_1_id')->textInput() ?>

    <?= $form->field($model, 'kemas_2_id')->textInput() ?>

    <?= $form->field($model, 'qc_fg_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
