<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KendalaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kendala-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'keterangan') ?>

    <?= $form->field($model, 'start') ?>

    <?= $form->field($model, 'stop') ?>

    <?= $form->field($model, 'penimbangan_id') ?>

    <?php // echo $form->field($model, 'pengolahan_id') ?>

    <?php // echo $form->field($model, 'qc_bulk_id') ?>

    <?php // echo $form->field($model, 'kemas_1_id') ?>

    <?php // echo $form->field($model, 'kemas_2_id') ?>

    <?php // echo $form->field($model, 'qc_fg_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
