<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MasterDataKoli */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-widget widget-user">
<!-- Add the bg color to the header using any of the bg-* classes -->
<div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
  <h3 class="widget-user-username"><b>PERUBAHAN WARNA LAKBAN</b></h3>
  <h5 class="widget-user-desc">Input Data</h5>
</div>
<div class="widget-user-image">
  <img class="img-circle" src="../web/images/business-partnership.png" alt="User Avatar">
</div>

<div class="box-footer">
  <div class="row">
    <div class="box-body">

        <!-- <div class="zoom">
          <?=Html::a('SCAN WITH QR', ['flow-input-snfg/check-kemas2-tab'], ['class' => 'btn btn-success btn-block']);?>
        </div> -->
        <p></p>

          <div class="box-body">
              <div class="flow-input-snfg-form">

                <div class="form-group field-flowinputsnfg-snfg has-success">
                <label class="control-label" for="flowinputsnfg-snfg">Snfg</label>
                <input type="text" id="flowinputsnfg-snfg" class="form-control" name="FlowInputSnfg[snfg]" aria-invalid="false">
                <br>
                <button id="checkbtn" class="btn btn-info btn-block" style="width:50%; margin:0 auto;">CHECK</button>

                </div>
              </div>
          </div>
    </div>
  <!-- /.row -->
  </div>
</div>


<?php
$script = <<< JS
$('#checkbtn').click(function(){  

      var snfg = $('#flowinputsnfg-snfg').val().trim();
      
      if(snfg){
        $.get('index.php?r=flow-input-snfg/is-fis-exist',{ snfg : snfg },function(data){ 
            a = JSON.parse(data);
            //console.log(a[0]['route']);
            if (a.id > 0) {
              window.location = "index.php?r=flow-input-snfg/update-lakban&id="+a.id;
            } else {
              alert("SNFG ini belum scan start");
            }
          });
      } else {
        alert("Isi field SNFG terlebih dahulu!.");
      }
      
  });

  $("#flowinputsnfg-snfg").on('keyup', function (e) {
    if (e.key === 'Enter' || e.keyCode === 13) {
      var snfg = $('#flowinputsnfg-snfg').val().trim();
      
      if(snfg){
        $.get('index.php?r=flow-input-snfg/is-fis-exist',{ snfg : snfg },function(data){ 
            a = JSON.parse(data);
            //console.log(a[0]['route']);
            if (a.id > 0) {
              window.location = "index.php?r=flow-input-snfg/update-lakban&id="+a.id;
            } else {
              alert("SNFG ini belum scan start");
            }
          });
      } else {
        alert("Isi field SNFG terlebih dahulu!.");
      }
    }
});
JS;
$this->registerJs($script);
?>