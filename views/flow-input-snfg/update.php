<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */

// $this->title = 'Update Flow Input Snfg: ' . $model->id;
// $this->params['breadcrumbs'][] = ['label' => 'Flow Input Snfgs', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = 'Update';
?>
<div class="flow-input-snfg-update">

<div style="padding: 20px 30px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;">
	<a href=# style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">Gunakan Sebijaknya; Pengubahan Data Akan Direkam dan menjadi bahan Evaluasi untuk Supervisor, Leader, dan Operator</a>
</div>


    <?= $this->render('_form-edit-kemas2', [
        'model' => $model,
    ]) ?>

</div>
