<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */


?>
<div class="flow-input-snfg-create">

    <?= $this->render('_form-check-kemas2', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
