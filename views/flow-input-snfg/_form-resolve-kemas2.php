<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputSnfg;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\Downtime;
use app\models\DowntimeSearch;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */
/* @var $form yii\widgets\ActiveForm */
?>


                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
                      <h3 class="widget-user-username"><b>Kemas 2 / Packing</b></h3>
                      <h5 class="widget-user-desc">Work Order</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">

                                


                                <div class="flow-input-snfg-form">

                                    <?php $form = ActiveForm::begin(); ?>

                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'snfg')->textInput(['readOnly'=>true,'value'=>$snfg]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'posisi')->textInput(['readOnly'=>true,'value'=>'KEMAS_2']) ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'jenis_kemas')->dropDownList(['PACKING' => 'PACKING' ,'PRESS+FILLING+PACKING' => 'PRESS+FILLING+PACKING', 'FILLING-PACKING_INLINE' => 'FILLING-PACKING_INLINE','FLAME-PACKING_INLINE' => 'FLAME-PACKING_INLINE', 'REWORK' => 'REWORK'],['prompt'=>'Select Option']); ?>
                                          <!-- /input-group -->
                                        </div>
                                        <!-- /.col-lg-6 -->
                                        <div class="col-md-3">
                                          <?php

                                                if(empty($nama_line)){
                                                    echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(FlowInputSnfg::find()->all()
                                                        ,'nama_line','nama_line'),
                                                        'options' => ['placeholder' => 'Select Line'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]);
                                                }else{
                                                    echo $form->field($model, 'nama_line')->textInput(['readOnly'=>true,'value'=>$nama_line]);
                                                }
                                            ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div id="lanjutan">
                                        <div class="col-md-3">
                                          <?= $form->field($model, 'lanjutan')->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div id="waktu_start_form" class="col-md-6">
                                        <?=
                                            $form->field($model, 'datetime_start')->widget(DateTimePicker::classname(), [
                                                'options' => ['placeholder' => 'Enter event time ...',
                                                              'readOnly' => true
                                                             ],
                                                    'pluginOptions' => [
                                                    'autoclose' => true,
                                                    'showMeridian' => false,
                                                    'minuteStep' => 5,
                                                    
                                                ]
                                            ]);

                                        ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div id="waktu_stop_form" class="col-md-6">
                                        <?=
                                            $form->field($model, 'datetime_stop')->widget(DateTimePicker::classname(), [
                                                'options' => ['placeholder' => 'Enter event time ...',
                                                              'readOnly' => true
                                                             ],
                                                    'pluginOptions' => [
                                                    'autoclose' => true,
                                                    'showMeridian' => false,
                                                    'minuteStep' => 5,
                                                    
                                                ]
                                            ]);

                                        ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>

                                    <span class="badge bg-blue" id="durasi-downtime"></span>

                                    <br></br>
 

                                    <label class="control-label">Nama Operator</label>
                                    <?= $form->field($model, 'nama_operator')->widget(TagsinputWidget::classname(), [
                                                        'clientOptions' => [
                                                            'trimValue' => true,
                                                            'allowDuplicates' => false,
                                                            'maxChars'=> 4,
                                                            'minChars'=> 4,
                                                        ]
                                                    ])->label(false)?>

                                    <br></br>

                                    <div id="create-button" class="row">

                                        <div class="col-md-2 col-md-offset-5">

                                            <?= Html::submitButton($model->isNewRecord ? 'Submit & Isi Downtime' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                            
                                        </div>
                                    </div>

                                    <?php ActiveForm::end(); ?>

                                </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>


<div class="box box-widget widget-user">
    <div class="widget-user-header bg-blue">
                      <h3 class="widget-user-username"><b>Rincian</b></h3>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'value' => function($model,$key,$index,$column){
                    return GridView::ROW_COLLAPSED;
                },
                'detail' => function($model,$key,$index,$column){
                    
                    $searchModel = new DowntimeSearch();
                    
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                    $dataProvider->query->where("flow_input_snfg_id=".$model->id);

                    return Yii::$app->controller->renderPartial('_expand-downtime-kemas2', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'flow_input_snfg_id' => $model->id,
                    ]);
                },
            ],
            'lanjutan',
            'snfg',
            'posisi',
            'datetime_start',
            'datetime_stop',
            'nama_line',
            'jenis_kemas',
            'nama_operator',
        ],
    ]); ?>

</div>


<?php
$script = <<< JS

    // Hide Lanjutan Jadwal , Tombol Submit, dan Date Time Stop
    $('#lanjutan').hide();
    $('#create-button').hide();
    $('#waktu_stop_form').hide();

    var snfg = $('#flowinputsnfg-snfg').val();
    
    var posisi = $('#flowinputsnfg-posisi').val();

    $.post("index.php?r=scm-planner/get-line-kemas-snfg&snfg="+$('#flowinputsnfg-snfg').val(), function (data){
        $("select#flowinputsnfg-nama_line").html(data);
    });


    $('#flowinputsnfg-jenis_kemas').change(function(){

        var jenis_kemas = $('#flowinputsnfg-jenis_kemas').val();

        $.get('index.php?r=flow-input-snfg/get-lanjutan-kemas2',{ snfg : snfg , jenis_kemas : jenis_kemas , posisi : posisi  }, function(data){
            var data = $.parseJSON(data);
            $('#flowinputsnfg-lanjutan').attr('value',data.lanjutan);

        });

        $('#lanjutan').fadeIn();

    });  

    // Time Validator

    $('#flowinputsnfg-datetime_start').change(function(){
        
        // Get Current Timestamp
        var now = new Date();

        // Current Date 
        var start_js = new Date($('#flowinputsnfg-datetime_start').val());
        if(start_js<now){
            // Show Stop Form
            $('#waktu_stop_form').fadeIn("slow");
        }else{
            // Hide Stop Form
            $('#waktu_stop_form').hide();
            alert('Pemilihan waktu harus sebelum waktu saat ini!');
        }

    });

    $('#flowinputsnfg-datetime_stop').change(function(){
        var start_js = new Date($('#flowinputsnfg-datetime_start').val());
        var stop_js = new Date($('#flowinputsnfg-datetime_stop').val());
        var keterangan = $('#downtime-keterangan').val();
        if(stop_js<=start_js){
            alert('Periksa kembali waktu, Waktu Stop Bulk tidak boleh sebelum Waktu Start Bulk!');
            $('#create-button').hide();
        }else{
            $('#durasi-downtime').html('Durasi : '+(parseFloat(stop_js-start_js)/parseFloat(60000))+' Menit atau '+(parseFloat(stop_js-start_js)/parseFloat(3600000)).toFixed(2)+' Jam' );
            $('#create-button').fadeIn("slow");
        };

    });

JS;
$this->registerJs($script);
?>