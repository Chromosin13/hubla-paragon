<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowAnalyst;
use wbraganca\tagsinput\TagsinputWidget;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
#modaltitle{
  display:inline-block;
}
.modal-body{
  font-weight:500;
  font-size:18px;
}
.modal-content{
  vertical-align:middle;
  margin-top: 25%;
  background-color : #f66257;
  /*text-align:center;*/
}

</style>

<!-- CSS For Video Selector -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="ZXing for JS">

  <title>ZXing TypeScript | Decoding from camera stream</title>

  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">
</head>



<!-- Body -->
<div class="box box-widget widget-user">
  <!-- Add the bg color to the header using any of the bg-* classes -->
  <div class="widget-user-header bg-green-gradient">
    <h3 class="widget-user-username"><b>Scan Jadwal Kemas 2</b></h3>
    <h5 class="widget-user-desc">QR Code</h5>
  </div>
  <div class="widget-user-image">
    <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
  </div>
  <div class="box-footer">
    <div class="row">
      <div class="box-body">
          <div class="box-body">

            <div id="sourceSelectPanel" style="display:none">
              <label for="sourceSelect">Switch Camera</label>
              <select id="sourceSelect" style="max-width:400px">x
              </select>
            </div>

            <div>
              <video id="video" width="330" height="300" style="border: 0.5px solid gray"></video>
            </div>

          </div>
      </div>
    </div>
    <!-- /.row -->
  </div>
</div>

<?php

    Modal::begin([
            'header'=>'<h2 style="text-align:center; font-weight:bold;">Line Kemas Sedang Digunakan</h2>',
            'id' => 'modalWarning',
            'size' => 'modal-lg',
        ]);

    echo "<div id='modalContentWarning'></div>";

    Modal::end();

?>

<?php

    Modal::begin([
            'header'=>'<h2 style="text-align:center; font-weight:bold;">Split Line Tidak Aktif</h2>',
            'id' => 'modalSplit',
            'size' => 'modal-lg',
        ]);

    echo "<div id='modalContentSplit'></div>";

    Modal::end();

?>


<!-- Javascript -->
  <!-- Calling ZXing API CDN -->
  <script type="text/javascript" src="zxing.js"></script>
  <!-- Code -->
  <script type="text/javascript">
      ipUrl = '<?php echo Yii::getAlias("@ipUrl"); ?>';

      let selectedDeviceId;
      const codeReader = new ZXing.BrowserMultiFormatReader()
      console.log('ZXing code reader initialized')
      codeReader.getVideoInputDevices()
        .then((videoInputDevices) => {
          const sourceSelect = document.getElementById('sourceSelect')
          selectedDeviceId = videoInputDevices[1].deviceId
          if (videoInputDevices.length >= 1) {
            videoInputDevices.forEach((element) => {
              const sourceOption = document.createElement('option')
              sourceOption.text = element.label
              sourceOption.value = element.deviceId
              sourceSelect.appendChild(sourceOption)
            })

            // When Changing Camera, Reset Scanner and Execute Canvas
            sourceSelect.onchange = () => {
              selectedDeviceId = sourceSelect.value;
              codeReader.reset()
              codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {

                if (result) {
                  var nama_line = '<?php echo $line; ?>';

                  // console.log(result.text);
                  $.get('index.php?r=scm-planner/check-snfg',{ snfg : result.text },function(data){
                        var data = $.parseJSON(data);

                        if(data.id>=1){
                          $.get('index.php?r=flow-input-snfg/check-active-snfg',{ nama_line : nama_line, snfg : result.text },function(data){ 
                              var data = $.parseJSON(data);

                              if (data.route == "to-stop" || data.route == "to-create"){
                                location.href=ipUrl+'flow-input-snfg/create-kemas2-inline&snfg='+result.text;
                                // location.href = "index.php?r=flow-input-snfg/create-kemas2-inline&snfg="+snfg;
                              } else if (data.route == "split-not-active"){
                                var value = "index.php?r=flow-input-snfg/split-not-active&nama_line="+data.nama_line+"&snfg="+result.text;
                                
                                $('#modalSplit').modal('show')
                                  .find('#modalContentSplit')
                                  .load(value);

                              } else {
                                var value = 'index.php?r=flow-input-snfg/line-not-available&nama_line='+nama_line;

                                $('#modalWarning').modal('show')
                                  .find('#modalContentWarning')
                                  .load(value);

                              }
                          });

                           // location.href=ipUrl+'flow-input-snfg/create-kemas2-inline&snfg='+result.text;

                        }
                         else{

                           alert('Nomor SNFG Tidak Ditemukan di Database; Silahkan mengontak Planner/Leader');
                        }

                  });
                }
                if (err && !(err instanceof ZXing.NotFoundException)) {
                  console.error(err)
                  document.getElementById('result').textContent = err
                }

              }) // on scanned
            }; // onchange

            const sourceSelectPanel = document.getElementById('sourceSelectPanel')
            sourceSelectPanel.style.display = 'block'
          }

          // Initialize Execute Canvas when Page first loaded
          codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
             if (result) {
                  var nama_line = '<?php echo $line; ?>';
                  // console.log(nama_line);
                  // console.log(result.text);
                  $.get('index.php?r=scm-planner/check-snfg',{ snfg : result.text },function(data){
                        var data = $.parseJSON(data);

                        if(data.id>=1){
                          $.get('index.php?r=flow-input-snfg/check-active-snfg',{ nama_line : nama_line, snfg : result.text },function(data){ 
                              var data = $.parseJSON(data);

                              if (data.route == "to-stop" || data.route == "to-create"){
                                location.href=ipUrl+'flow-input-snfg/create-kemas2-inline&snfg='+result.text;
                                // location.href = "index.php?r=flow-input-snfg/create-kemas2-inline&snfg="+snfg;
                              } else if (data.route == "split-not-active"){
                                var value = "index.php?r=flow-input-snfg/split-not-active&nama_line="+data.nama_line+"&snfg="+result.text;
                                
                                $('#modalSplit').modal('show')
                                  .find('#modalContentSplit')
                                  .load(value);

                              } else {
                                var value = 'index.php?r=flow-input-snfg/line-not-available&nama_line='+nama_line;

                                $('#modalWarning').modal('show')
                                  .find('#modalContentWarning')
                                  .load(value);

                              }
                          });

                           // location.href=ipUrl+'flow-input-snfg/create-kemas2-inline&snfg='+result.text;

                        }
                         else{

                           alert('Nomor SNFG Tidak Ditemukan di Database; Silahkan mengontak Planner/Leader');
                        }

                  });
                }
            if (err && !(err instanceof ZXing.NotFoundException)) {
              console.error(err)
              document.getElementById('result').textContent = err
            }
          }) // on scanned


        })
        .catch((err) => {
          console.error(err)
        })
  </script>
