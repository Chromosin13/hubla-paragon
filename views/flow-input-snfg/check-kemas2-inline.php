<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */


?>
<div class="flow-input-snfg-create">

    <?= $this->render('_form-check-kemas2-inline', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'frontend_ip' => $frontend_ip,
        'sbc_ip' => $sbc_ip,
        'line' => $line,
        'keterangan' => $keterangan,
        'is_line_exist' => $is_line_exist,
    ]) ?>

</div>
