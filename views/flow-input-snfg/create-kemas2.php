<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */


?>
<div class="flow-input-snfg-create">




    <?php 

    	echo $this->render('_form-kemas2', [
		        'model' => $model,
		        'snfg' => $snfg,
			    'searchModel' => $searchModel,
			    'dataProvider' => $dataProvider,
			    // 'nama_line' => $nama_line,
			    'jenis_proses_list' => $jenis_proses_list,
		        // 'frontend_ip' => $frontend_ip,
		        // 'sbc_ip' => $sbc_ip,
		        // 'line' => $line,
                'odoo_code' => $odoo_code,
		        'nosmb' => $nosmb,
		        'nobatch' => $nobatch,
		        'exp_date' => $exp_date,
		        'barcode' => $barcode,
                'fg_name' => $fg_name,
                // 'keterangan' => $keterangan
		]);

     ?>

</div>
