<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax; 
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FlowInputSnfgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
// $this->registerJsFile('@web/js/jquery.min.js');
// $this->registerJsFile('@web/js/jquery.knob.js');
?>
<style>
.bawahan {
   position: fixed;
   border-top:2px solid #D3D3D3;
   left: 0;
   bottom: 0;
   width: 100%;
   padding:15px 0;
   background-color: white;
   color: white;
   text-align: center;
}
</style>
<?php
$script = <<< JS

    $(".dial").knob({
        'min':0,
        'max':60,
        // 'angleOffset':-60,
        // 'angleArc':120,
        'readOnly':true,
        'step':0.1,
        'width':200,
        'height':200,
        'thickness':0.3,
    });




    $('#print-form').hide();

    var y = setInterval(function() {

        $.getJSON("http://10.3.5.102:3000/flow_input_snfg_wo?flow_input_snfg_id=eq.$id&order=id.desc",function(result){
        
            y = result;

        });

       $("#list_table_json").empty();
           var header = '<tr><th style="width: 20px">No Urut</th><th>Timestamp</th><th>Pack Date</th><th>Exp Date</th><th>Operator</th><th style="width: 40px">Status</th></tr>';
            $("#list_table_json").append(header);
           var event_data = '';
          $.each(y, function(index, value){
               /*console.log(value);*/
               event_data += '<tr>';
               event_data += '<td>'+value.nourut+'</td>';
               event_data += '<td>'+value.write_time+'</td>';
               //event_data += '<td><span class="badge bg-green">'+value.weight+' kg</span></td>';
               event_data += '<td>'+value.pack_date+'</td>';
               event_data += '<td>'+value.exp_date+'</td>';
               event_data += '<td>'+value.nama_operator+'</td>';
               if(value.status=='pass'){
                 event_data += '<td><span class="label label-success">Pass</span></td>'; 
               }else if(value.status=='less'){
                 event_data += '<td><span class="label label-warning">Less</span></td>';
               }else if(value.status=='over'){
                 event_data += '<td><span class="label label-danger">Over</span></td>';
               }
               
              event_data += '<tr>';
           });
           $("#list_table_json").append(event_data);

    }, 1000);


    var j = setInterval(function() {

        $.getJSON("http://10.3.5.102:3000/weigher_stats?flow_input_snfg_id=eq.$id",function(result){
        
            j = result[0];

            // alert(j.jumlah_wo);

            $("#jumlah_ftr").text(j.rft);
            $("#pass").text(j.pass);
            $("#less").text(j.less);
            $("#over").text(j.over);
            $("#error_rate").text(j.error_rate+'%');
            $("#success_rate").text(j.success_rate+'%');

        });

       

    }, 1000);


    var printer_status = setInterval(function() {

        $.getJSON("http://$sbc_ip:6900/",function(result){
        
            printer_status = result;
            // console.log(printer_status.printer);
            $("#isi_printer_status").text(printer_status.printer);
            

        });

       

    }, 10000);


    document.getElementById("stopbutton").onclick = function() {

        var id  = '$id';
        var snfg  = '$snfg';

        var result = confirm("Anda Yakin Ingin Menyelesaikan Work Order untuk SNFG $snfg ?");
        if(result){
          window.location = "https://factory.pti-cosmetics.com/flowreport/web/index.php?r=flow-input-snfg/stop-work-order&snfg="+snfg+"&last_id="+id;          
        }

    };

    document.getElementById("forceprint").onclick = function() {

        // var id  = '$id';
        // var snfg  = '$snfg';

        var result = confirm("Anda Yakin Ingin Print Manual ?");
        if(result){
          $.getJSON("http://$sbc_ip:6900/?test",function(result){
              alert(result.test);              
          });          
        }

    };


JS;
    $this->registerJs($script);
?>


<div class="row">
        
        <!-- ./col -->

        <div class="col-lg-3 col-xs-3">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3 id="pass">0</h3>

              <p>Pass</p>
            </div>
            <div class="icon">
              <i class="large material-icons" style="font-size:100px;">local_parking</i>
            </div>
            <a href="#" class="small-box-footer"><p>Line Packing : <?php echo $line_pack ?></p></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-3">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 id="jumlah_ftr">0</h3>

              <p>First Time Right</p>
            </div>
            <div class="icon">
              <i class="large material-icons" style="font-size:100px;">looks_one</i>
            </div>
            <a href="#" class="small-box-footer"><p><?php echo $snfg ?></p></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-3">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3 id="less">0</h3>

              <p>Less</p>
            </div>
            <div class="icon">
              <i class="large material-icons" style="font-size:100px;">indeterminate_check_box</i>
            </div>
            <a href="#" class="small-box-footer"><p>FE IP : <?php echo $frontend_ip?></p></p></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-3">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3 id="over">0</h3>

              <p>Over</p>
            </div>
            <div class="icon">
              <i class="large material-icons" style="font-size:100px;">control_point_duplicate</i>
            </div>
            <a href="#" class="small-box-footer"><p>SBC IP : <?php echo $sbc_ip.' ('.$line.')'?></p></a>
          </div>
        </div>
        <!-- ./col -->
</div>

<div class="row">
      <div class="col-lg-3 col-xs-3">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><b>Printer</b></h3>
              <p>Status</p>
            </div>
            <div class="icon">
              <i class="large material-icons" style="font-size:100px;">local_printshop</i>
            </div>
            <a href="#" class="small-box-footer"><p id="isi_printer_status"></p></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- Operator -->
        <div class="col-lg-3 col-xs-3">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><?php echo $sum_operator;?></h3>
              <p>Operators</p>
            </div>
            <div class="icon">
              <i class="large material-icons" style="font-size:100px;">people</i>
            </div>
            <a href="#" class="small-box-footer"><p>Current/Last : <?php echo $operator_terpilih;?></p></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-3">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3 id="error_rate">0</h3>
              <p>Error Rate</p>
            </div>
            <div class="icon">
              <i class="large material-icons" style="font-size:100px;">error</i>
            </div>
            <a href="#" class="small-box-footer">Error Rate</a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-3">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3 id="success_rate">%</h3>
              <p>Success Rate</p>
            </div>
            <div class="icon">
              <i class="large material-icons" style="font-size:100px;">check_circle</i>
            </div>
            <a href="#" class="small-box-footer">Success Rate</p></a>
          </div>
        </div>
</div>

<div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">List Work Order</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">

                      <table class="table table-striped" id="list_table_json">
                        <tbody>
                        <!-- Print Table Here -->
                      </tbody></table>

                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>

</div>





<?php $this->registerJsFile('/flowreport/web/js/jquery.knob.js', ['depends' => [yii\web\JqueryAsset::className()]]); ?>

