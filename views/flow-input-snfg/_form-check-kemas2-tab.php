<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowAnalyst;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- CSS For Video Selector -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="ZXing for JS">

  <title>ZXing TypeScript | Decoding from camera stream</title>

  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">
</head>



<!-- Body -->
<div class="box box-widget widget-user">
  <!-- Add the bg color to the header using any of the bg-* classes -->
  <div class="widget-user-header bg-green-gradient">
    <h3 class="widget-user-username"><b>Scan Jadwal Kemas 2</b></h3>
    <h5 class="widget-user-desc">QR Code</h5>
  </div>
  <div class="widget-user-image">
    <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
  </div>
  <div class="box-footer">
    <div class="row">
      <div class="box-body">
          <div class="box-body">

            <div id="sourceSelectPanel" style="display:none">
              <label for="sourceSelect">Switch Camera</label>
              <select id="sourceSelect" style="max-width:400px">x
              </select>
            </div>

            <div>
              <video id="video" width="330" height="300" style="border: 0.5px solid gray"></video>
            </div>

          </div>
      </div>
    </div>
    <!-- /.row -->
  </div>
</div>

<div class="modal fade"  id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="modaltitle">Pilih Line yang mau di stop</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="isi_modal" class="modal-body">
          
          <b class="text-left" style="font-size:1em;">Nama line</b> :  <p id="isi_snfg"></p>
          <br>
          
          <!-- Jika belum selesai, dan dilanjut di shift berikutnya, klik Belum All Batch. -->
      </div>
      <div class="modal-footer">
        <button id="cancel" type="button" class="btn btn-warning pull-left" data-dismiss="modal">Close</button>
        <!-- <button id="notdone" type="button" class="btn btn-secondary"></button>
        <button id="done" type="button" class="btn btn-primary">Sudah All Batch</button> -->
      </div>
    </div>
  </div>
</div>




<!-- Javascript -->
  <!-- Calling ZXing API CDN -->
  <script type="text/javascript" src="zxing.js"></script>
  <!-- Code -->
  <script type="text/javascript">

      let selectedDeviceId;
      const codeReader = new ZXing.BrowserMultiFormatReader()
      console.log('ZXing code reader initialized')
      codeReader.getVideoInputDevices()
        .then((videoInputDevices) => {
          const sourceSelect = document.getElementById('sourceSelect')

          if (videoInputDevices.length >= 1) {
            videoInputDevices.forEach((element) => {
              const sourceOption = document.createElement('option')
              sourceOption.text = element.label
              sourceOption.value = element.deviceId
              sourceSelect.appendChild(sourceOption)
            })
            selectedDeviceId = videoInputDevices[videoInputDevices.length-1].deviceId

            // When Changing Camera, Reset Scanner and Execute Canvas
            sourceSelect.onchange = () => {
              selectedDeviceId = sourceSelect.value;

              codeReader.reset()
              codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {

                if (result) {
                  var snfg = result.text;
                  $.get('index.php?r=flow-input-snfg/check-if-split-line',{ snfg : snfg },function(data){ 
                    a = JSON.parse(data);
                    //console.log(a[0]['route']);
                    if (a[0]['route'] == 'to-create') {
                      window.location = "index.php?r=flow-input-snfg/create-kemas2&snfg="+snfg;
                    } else {
                      $.get('index.php?r=flow-input-snfg/check-is-split',{ snfg : snfg },function(data){
                        if (data == 1){
                          document.getElementById("isi_snfg").innerHTML = "";
                          a.forEach(myFunction);
                          $('#modalConfirm').appendTo('body').modal('show');
                        } else {
                          window.location = "index.php?r=flow-input-snfg/create-kemas2&snfg="+snfg;
                        }
                      });
                    }
                  });
                  // location.href='http://factory.pti-cosmetics.com/flowreport/web/index.php?r=flow-input-snfg/create-kemas2&snfg='+result.text;

                }
                if (err && !(err instanceof ZXing.NotFoundException)) {
                  console.error(err)
                  document.getElementById('result').textContent = err
                }

              }) // on scanned
            }; // onchange

            const sourceSelectPanel = document.getElementById('sourceSelectPanel')
            sourceSelectPanel.style.display = 'block'
          }

          // Initialize Execute Canvas when Page first loaded
          codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
             if (result) {
                var snfg = result.text;
                $.get('index.php?r=flow-input-snfg/check-if-split-line',{ snfg : snfg },function(data){ 
                  a = JSON.parse(data);
                  //console.log(a[0]['route']);
                  if (a[0]['route'] == 'to-create') {
                    window.location = "index.php?r=flow-input-snfg/create-kemas2&snfg="+snfg;
                  } else {
                    $.get('index.php?r=flow-input-snfg/check-is-split',{ snfg : snfg },function(data){
                      if (data == 1){
                        document.getElementById("isi_snfg").innerHTML = "";
                        a.forEach(myFunction);
                        $('#modalConfirm').appendTo('body').modal('show');
                      } else {
                        window.location = "index.php?r=flow-input-snfg/create-kemas2&snfg="+snfg;
                      }
                    });
                  }
                });
                // location.href='http://factory.pti-cosmetics.com/flowreport/web/index.php?r=flow-input-snfg/create-kemas2&snfg='+result.text;

              }
            if (err && !(err instanceof ZXing.NotFoundException)) {
              console.error(err)
              document.getElementById('result').textContent = err
            }
          }) // on scanned


        })
        .catch((err) => {
          console.error(err)
        })
  </script>
  <div id="back">
      <p>
          <!-- <?= Html::a('Back', ['site/index'], ['class' => 'btn btn-warning btn-block']) ?> -->
      </p>
  </div>


<script >
  function myFunction(item, index) {
    // var snfg = result.text;
    document.getElementById("isi_snfg").innerHTML += `<button class='btn btn-info btn-block' onclick='window.location="index.php?r=flow-input-snfg/stop-work-order&snfg=`+item['snfg']+`&last_id=`+item['id']+`&pos=inline"' style='width:50%; margin:0 auto;'>`+item['nama_line']+`</button><br>`;
  }
</script>