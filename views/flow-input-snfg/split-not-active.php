<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */

?>
<div class="flow-input-snfg-view">

    <p class="text-center" style="font-size:1.25em;">Anda sudah scan jadwal ini di line (<?php echo $nama_line;?>) hari ini.</p><br>
    <p class="text-center" style="font-size:1.25em;">Aktifkan split line terlebih dahulu di <?php echo $nama_line ?> untuk melanjutkan proses scan!</p>

  <div class="modal-footer">
    <!-- <button id="no" type="button" class="btn btn-warning pull-left" data-dismiss="modal">Tidak</button> -->
    <button id="yes" type="button" class="btn btn-success" data-dismiss="modal">Oke</button>
  </div>
</div>