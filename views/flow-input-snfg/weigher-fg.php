<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax; 
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FlowInputSnfgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
// $this->registerJsFile('@web/js/jquery.min.js');
// $this->registerJsFile('@web/js/jquery.knob.js');
?>
<style>
.bawahan {
   position: fixed;
   border-top:2px solid #D3D3D3;
   left: 0;
   bottom: 0;
   width: 100%;
   padding:15px 0;
   background-color: white;
   color: white;
   text-align: center;
}

.settings {
   position: fixed;
   bottom: 0;
   padding:15px 0;
   text-align: center;
   margin-bottom:55px;
   margin-left:85%;
}

.restartProgram {
   position: fixed;
   bottom: 0;
   padding:15px 0;
   text-align: center;
   margin-bottom:135px;
   margin-left:73%;
}

.clearAntrian {
   position: fixed;
   bottom: 0;
   padding:15px 0;
   text-align: center;
   margin-bottom:95px;
   margin-left:71%;
}

.restartRaspi {
   position: fixed;
   bottom: 0;
   padding:15px 0;
   text-align: center;
   margin-bottom:55px;
   margin-left:69%;
}

@media (max-width:800px)  {
 /* portrait tablets, portrait iPad, e-readers (Nook/Kindle), landscape 800x480 phones (Android) */
.btnSettings{
  width:50px;
}
.settings {
   position: fixed;
   bottom: 0;
   padding:15px 0;
   text-align: center;
   margin-bottom:53px;
   margin-left:86.5%;
}

.restartProgram {
   position: fixed;
   bottom: 0;
   padding:15px 0;
   text-align: center;
   margin-bottom:135px;
   margin-left:70%;
   transition: all 0.1s linear;
}

.clearAntrian {
   position: fixed;
   bottom: 0;
   padding:15px 0;
   text-align: center;
   margin-bottom:95px;
   margin-left:68%;
   transition: all 0.1s linear;
}

.restartRaspi {
   position: fixed;
   bottom: 0;
   padding:15px 0;
   text-align: center;
   margin-bottom:55px;
   margin-left:73%;
   transition: all 0.1s linear;
}
}

@media (min-width:801px ) and (max-width:999px)  {
 /* portrait tablets, portrait iPad, e-readers (Nook/Kindle), landscape 800x480 phones (Android) */
.btnSettings{
  width:50px;
}
.settings {
   position: fixed;
   bottom: 0;
   padding:15px 0;
   text-align: center;
   margin-bottom:53px;
   margin-left:86.3%;
}

.restartProgram {
   position: fixed;
   bottom: 0;
   padding:15px 0;
   text-align: center;
   margin-bottom:135px;
   margin-left:77%;
   transition: all 0.1s linear;
}

.clearAntrian {
   position: fixed;
   bottom: 0;
   padding:15px 0;
   text-align: center;
   margin-bottom:95px;
   margin-left:75%;
   transition: all 0.1s linear;
}

.restartRaspi {
   position: fixed;
   bottom: 0;
   padding:15px 0;
   text-align: center;
   margin-bottom:55px;
   margin-left:78%;
   transition: all 0.1s linear;
}
}

@media (min-width:1000px)  {
 /* portrait tablets, portrait iPad, e-readers (Nook/Kindle), landscape 800x480 phones (Android) */
.btnSettings{
  width:50px;
}
.settings {
   position: fixed;
   bottom: 0;
   padding:15px 0;
   text-align: center;
   margin-bottom:53px;
   margin-left:93.1%;
}

.restartProgram {
   position: fixed;
   bottom: 0;
   padding:15px 0;
   text-align: center;
   margin-bottom:135px;
   margin-left:88%;
   transition: all 0.1s linear;
}

.clearAntrian {
   position: fixed;
   bottom: 0;
   padding:15px 0;
   text-align: center;
   margin-bottom:95px;
   margin-left:87%;
   transition: all 0.1s linear;
}

.restartRaspi {
   position: fixed;
   bottom: 0;
   padding:15px 0;
   text-align: center;
   margin-bottom:55px;
   margin-left:88.9%;
   transition: all 0.1s linear;
}
}

.hidden {
  display: none;
}

.visuallyhidden {
  opacity: 0;
}


/*.myButton{
    background:url(../web/images/settings-logo.png) no-repeat;
    cursor:pointer;
    border:none;
    width:50px;
    height:50px;
    background-size: 100% 100%;
}

.myButton:active
{   
    box-shadow: 0 3px #666;
    transform: translateY(3px);
}*/

.click{

}
.click:active{
  box-shadow: 0 3px #666;
    transform: translateY(3px);
}

#snackbar {
  visibility: hidden;
  min-width: 250px;
  background-color: #333;
  color: #fff;
  text-align: center;
  border-radius: 2px;
  padding: 16px;
  position: fixed;
  z-index: 1;
  left: 50%;
  -ms-transform: translateX(-50%);
  transform: translateX(-50%);
  bottom: 30px;
  font-size: 17px;
}

#snackbar.show {
  visibility: visible;
  -webkit-animation: fadein 0.5s, fadeout 0.5s 4.5s;
  animation: fadein 0.5s, fadeout 0.5s 4.5s;
}

@-webkit-keyframes fadein {
  from {bottom: 0; opacity: 0;} 
  to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
  from {bottom: 0; opacity: 0;}
  to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
  from {bottom: 30px; opacity: 1;} 
  to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
  from {bottom: 30px; opacity: 1;}
  to {bottom: 0; opacity: 0;}
}


.modal-body{
  font-weight:500;
  font-size:18px;
}
.modal-content{
  vertical-align:middle;
  margin-top: 25%;
  background-color : #f66257;
  /*text-align:center;*/
}

.swal-overlay {
  background-color: #9f0e3199;
}
.swal-text{
  font-size: 20px;
  text-align: center;
}
</style>

<script type="text/javascript">
let btnRestartProgram = document.getElementById('restartProgram'),
    btnClearAntrian = document.getElementById('clearAntrian'),
    btnRestartRaspi = document.getElementById('restartRaspi'),
    btnSettings = document.getElementById('btnSettings');

btnSettings.addEventListener('click', function () {
  
  if (btnRestartProgram.classList.contains('hidden')) {
    btnRestartProgram.classList.remove('hidden');
    setTimeout(function () {
      btnRestartProgram.classList.remove('visuallyhidden');
    }, 20);
  } else {
    btnRestartProgram.classList.add('visuallyhidden');    
    btnRestartProgram.addEventListener('transitionend', function(e) {
      btnRestartProgram.classList.add('hidden');
    }, {
      capture: false,
      once: true,
      passive: false
    });
  }
  
}, false);

function restartProgram(){
  
}

function clearAntrian(){
  
}

function restartProgram(){
  
} 


</script>

<div class="row">

  <div class="col-lg-12 col-xs-12">
    <p><?php 

            if($status_tare=='tare_on'){
              // echo '<button type="button" class="btn btn-success btn-md active">'.$value.'</button> ';
              //echo Html::a('SELESAI TARE', ['insert-tare', 'id' => $id], ['id'=>'tareBtn','class'=>'btn btn-danger btn-block active']);
              echo '<a class="btn btn-danger btn-block active" value="off" id="tareBtn">SELESAI TARE</a>';
              echo '  ';
            }else{
              // echo '<button type="button" class="btn btn-warning btn-md">'.$value.'</button> ';
              //echo Html::a('MULAI TARE', ['insert-tare', 'id' => $id], ['id'=>'tareBtn','class'=>'btn btn-warning btn-block']);
              echo '<a class="btn btn-warning btn-block" value="on" id="tareBtn">MULAI TARE</a>';
              echo '  ';
            }

        ?>
    </p>
  </div>

  <?php if ($is_counter){ ?>
    <div class="col-lg-6 col-xs-6">
      <p>
        <a class="btn btn-block" style="background:#afafaf; color:white;" id="splitBtn">SPLIT LINE : 
          <?php if ($status_split == 'AKTIF')
              {
                echo '<b style="color:green;">'; echo $status_split; echo '</b>';
              } else {
                echo '<b style="color:red;">'; echo $status_split; echo '</b>';
              }
          ?>
        </a>
      </p>
    </div>
    <div class="col-lg-3 col-xs-3">
      <p><?php 

              echo '<a class="btn btn-block" style="background-opacity:0; color:black;  font-size:1.2em; font-weight:bolder;" id="counter_">INNERBOX : 0 / ';
              echo $innerQty; 
              echo'</a>';
              echo '  ';

        ?>
      </p>
    </div>
    <div class="col-lg-3 col-xs-3">
      <p><?php ?>

              <button class="btn btn-block btn-info" id="reset_counter">RESET COUNTER</button>

        <?php ?>
      </p> 
    </div>
  <?php } else { ?>
    <div class="col-lg-12 col-xs-12">
      <p>
        <a class="btn btn-block" style="background:#afafaf; color:white;" id="splitBtn">SPLIT LINE : 
          <?php if ($status_split == 'AKTIF')
              {
                echo '<b style="color:green;">'; echo $status_split; echo '</b>';
              } else {
                echo '<b style="color:red;">'; echo $status_split; echo '</b>';
              }
          ?>
        </a>
      </p>
    </div>
  <?php } ?>
    
  <?php 

      if($status_tare!='tare_on'){
        echo '
        <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><b>Printer</b></h3>
              <p>Status</p>
            </div>
            <div class="icon">
              <i class="large material-icons" style="font-size:100px;">local_printshop</i>
            </div>
            <a href="#" class="small-box-footer"><p id="isi_printer_status"></p></a>
          </div>
        </div>
        <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>';echo $operator_terpilih.','.$operator_terpilih_innerbox;echo '</h3>
              <p>Current Operator</p>
            </div>
            <div class="icon">
              <i class="large material-icons" style="font-size:100px;">people</i>
            </div>
            <a href="#" class="small-box-footer"><p>'; echo $sum_operator; echo '</p></a>
          </div>
        </div>';
    } else {
      echo'
      <div id="tareMode">
        <div class="col-lg-12 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 style="text-align:center;"><b>MODE TARE</b></h3>
              <p style="text-align:center;">Data selama penimbangan tidak akan masuk</p>
            </div>
          </div>
        </div>
      </div>';}?>
</div>
<div class="row">
        

        <div class="col-lg-3 col-xs-3">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3 id="pass">0</h3>

              <p>Pass</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer"><p>Nama Line : <?php echo $line ?></p></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-3">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 id="jumlah_ftr">0</h3>

              <p>First Time Right</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer"><p><?php echo $snfg ?></p></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-3 col-xs-3">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3 id="less">0</h3>

              <p>Less</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer"><p>FE IP : <?php echo $frontend_ip?></p></p></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-3">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3 id="over">0</h3>

              <p>Over</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer"><p>SBC IP : <?php echo $sbc_ip?></p></a>
          </div>
        </div>
        <!-- ./col -->
</div>


<div class="row">
  <!-- <div class="col-lg-6 col-xs-6"> -->
    <?php if ($is_counter){ ?>
      <div class="col-lg-3 col-xs-3">
        <label for="op_karbox" style="text-align:center; display:block; font-size:1.25em;">KARBOX</label>
        <select id="op_karbox" name="op_karbox" style="display:block; width: 100%; height:2.25em;">
        <?php 
        
        echo '<option value="" selected></option>';

        foreach($list_operator as $value){ 
            if($value==$operator_terpilih){
              // echo '<button type="button" class="btn btn-success btn-md active">'.$value.'</button> ';
              echo '<option value='.$value.' selected>'.$value.'</option>';
              echo '  ';
            }else{
              // echo '<button type="button" class="btn btn-warning btn-md">'.$value.'</button> ';
              echo '<option value='.$value.'>'.$value.'</option>';
              echo '  ';
            }
             
          } ?>
          </select>
      </div>
      <div class="col-lg-3 col-xs-3">
        <label for="op_innerbox" style="text-align:center; display:block; font-size:1.25em;">INNERBOX</label>
        <select id="op_innerbox" name="op_innerbox" style="display:block; width: 100%; height:2.25em;">
        <?php 
        
        echo '<option value="" selected></option>';

        foreach($list_operator_innerbox as $value){ 
            if($value==$operator_terpilih_innerbox){
              // echo '<button type="button" class="btn btn-success btn-md active">'.$value.'</button> ';
              echo '<option value='.$value.' selected>'.$value.'</option>';
              echo '  ';
            }else{
              // echo '<button type="button" class="btn btn-warning btn-md">'.$value.'</button> ';
              echo '<option value='.$value.'>'.$value.'</option>';
              echo '  ';
            }
             
          } ?>
          </select>
      </div>
          <?php
          } else { ?>
            <div class="col-lg-6 col-xs-6">
            <?php
            foreach($list_operator as $value){ 
              if($value==$operator_terpilih){
                // echo '<button type="button" class="btn btn-success btn-md active">'.$value.'</button> ';
                echo Html::a($value, ['insert-swo', 'id' => $id, 'nama_operator' => $value], ['class'=>'btn btn-primary btn-block active']);
                echo '  ';
              }else{
                // echo '<button type="button" class="btn btn-warning btn-md">'.$value.'</button> ';
                echo Html::a($value, ['insert-swo', 'id' => $id, 'nama_operator' => $value], ['class'=>'btn btn-default btn-block']);
                echo '  ';
              }
               
            }?>
          </div>
          <?php
          }

        ?>
  <!-- </div> -->
  <div class="col-lg-6 col-xs-6">
    <button type="button" id="print_awal" class="btn btn-block btn-info">Print Contoh Label</button>
    <button type="button" id="forceprint" class="btn btn-block btn-warning">Test Print</button>
    <button type="button" id="printlast" class="btn btn-block btn-danger">Label Rusak</button>
    <row>
      <div class="col-lg-6 col-xs-6">
        <p style="text-align:center; margin-top:10px; color:#AFAFAF;">Status Tare : <?php echo $status_tare; ?></p>
      </div>
      <div class="col-lg-6 col-xs-6">
        <p style="text-align:center; margin-top:10px; color:#AFAFAF;">Datetime Stop : <?php echo $datetime_stop; ?></p>
      </div>
    </row>
  </div>
</div>

 <br>   

<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Last 10 Koli</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">

            <table class="table table-striped" id="list_table_json">
              <tbody>
              <!-- Print Table Here -->
            </tbody></table>

      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.box-body -->

</div>

<p></p>


    <!-- Add the bg color to the header using any of the bg-* classes -->
<br></br>


<div class="bawahan">
    <div class="row">
      <div style="width:100%; padding:0 30px;">
        <button type="button" id="stopbutton" class="btn btn-block btn-danger">SELESAI WORK ORDER</button>
      </div>
    </div>
</div>



 

    <!-- <div class="settings">
      <button class="img-circle myButton" id="btnSettings"></button>
    </div> -->

    <div class="settings">
          <img class="img-circle btnSettings click" id="btnSettings" src="../web/images/settings-logo.png" alt="User Avatar" style="">
    </div>

    <div class="restartProgram visuallyhidden hidden" id="div1">
        <button type="button" id="restartProgram" class="btn btn-info" style="border: 1px solid #01A7FF; opacity:0.9; font-weight:bolder; background-color:#7AD7F0; color: black; font:0.7em; height:2.2em;">Restart Program</button>
    </div>

    <div class="clearAntrian visuallyhidden hidden" id="div2">
        <button type="button" id="clearAntrian" class="btn btn-info" style="border: 1px solid #01A7FF; opacity:0.9; font-weight:bolder; background-color:#7AD7F0; color: black; font:0.7em; height:2.2em;">Hapus Antrian</button>
    </div>

    <div class="restartRaspi visuallyhidden hidden" id="div3">
        <button type="button" id="restartRaspi" class="btn" style="border: 1px solid #01A7FF; opacity:0.9; font-weight:bolder; background-color:#7AD7F0; color: black; font:0.7em; height:2.2em;">Reboot</button>
    </div>

    <!-- <div class="clearAntrian">
        <div class="row">
          <div style="width:100%; padding:0 30px;">
            <button type="button" id="restartProgram">Restart Program</button>
          </div>
        </div>
    </div>

    <div class="resRaspi">
        <div class="row">
          <div style="width:100%; padding:0 30px;">
            <button type="button" id="restartProgram">Restart Program</button>
          </div>
        </div>
    </div> -->

</div>

<div id="snackbar">Some text some message..</div>


<?php
$script = <<< JS

setInterval(function(){ 
  var op = "$operator_terpilih";
  var op_innerbox = "$operator_terpilih_innerbox";
  if (op==''){
    swal({
      title: "Operator Kosong!",
      text: "Pilih nama operator terlebih dahulu sebelum mulai kemas!",
      icon: "error",
      button: "OK!",
    });    
  }
}, 8000);


  let btnRestartProgram = document.getElementById('div1'),
    btnClearAntrian = document.getElementById('div2'),
    btnRestartRaspi = document.getElementById('div3'),
    btnSettings = document.getElementById('btnSettings');

  btnSettings.addEventListener('click', function () {
    
    if (btnRestartProgram.classList.contains('hidden')) {
      btnRestartProgram.classList.remove('hidden');
      setTimeout(function () {
        btnRestartProgram.classList.remove('visuallyhidden');
      }, 20);
    } else {
      btnRestartProgram.classList.add('visuallyhidden');    
      btnRestartProgram.addEventListener('transitionend', function(e) {
        btnRestartProgram.classList.add('hidden');
      }, {
        capture: false,
        once: true,
        passive: false
      });
    }

    if (btnClearAntrian.classList.contains('hidden')) {
      btnClearAntrian.classList.remove('hidden');
      setTimeout(function () {
        btnClearAntrian.classList.remove('visuallyhidden');
      }, 20);
    } else {
      btnClearAntrian.classList.add('visuallyhidden');    
      btnClearAntrian.addEventListener('transitionend', function(e) {
        btnClearAntrian.classList.add('hidden');
      }, {
        capture: false,
        once: true,
        passive: false
      });
    }

    if (btnRestartRaspi.classList.contains('hidden')) {
      btnRestartRaspi.classList.remove('hidden');
      setTimeout(function () {
        btnRestartRaspi.classList.remove('visuallyhidden');
      }, 20);
    } else {
      btnRestartRaspi.classList.add('visuallyhidden');    
      btnRestartRaspi.addEventListener('transitionend', function(e) {
        btnRestartRaspi.classList.add('hidden');
      }, {
        capture: false,
        once: true,
        passive: false
      });
    }
    
  }, false);

  document.getElementById("restartProgram").onclick = function() {
      var id  = '$id';
      var ip  = '$sbc_ip';
      var x = document.getElementById("snackbar");

      var result = confirm("Anda Yakin Ingin Me-Restart Program ?");
      if(result){
        $.get('index.php?r=flow-input-snfg/restart-program',{ ip:ip},function(data2){

          var data2 = $.parseJSON(data2);
          console.log(data2.status);
          if(data2.status=="done"){
            x.className = "show";
            x.innerHTML = "Program Berhasil di Restart";
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);

            var event = "Restart Program";
            var status = "Success";
            $.getJSON("index.php?r=flow-input-snfg/insert-log-problem",{ id:id, event:event, status:status},function(result2){
            
                if (result2.status == "done"){
                  console.log("success");
                }
            });
          } else {
            x.className = "show";
            x.innerHTML = "Raspberry (Panel) Mati atau tidak terhubung internet. Mohon hubungi teknisi atau matikan panel dan nyalakan kembali.";
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);

            var event = "Restart Program";
            var status = "Failed";
            $.getJSON("index.php?r=flow-input-snfg/insert-log-problem",{ id:id, event:event, status:status},function(result2){
            
                if (result2.status == "done"){
                  console.log("success");
                }
            });
          }
          
        });
      }

  };

  document.getElementById("clearAntrian").onclick = function() {
      var id  = '$id';
      var ip  = '$sbc_ip';
      var x = document.getElementById("snackbar");

      var result = confirm("Anda Yakin Ingin Menghapus Antrian Print Label ?");
      if(result){
        $.get('index.php?r=flow-input-snfg/clear-queue',{ ip:ip},function(data2){

          var data2 = $.parseJSON(data2);
          console.log(data2.status);
          if(data2.status=="done"){
            x.className = "show";
            x.innerHTML = "Antrian Printer Berhasil Dihapus";
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);

            var event = "Hapus Antrian Printer";
            var status = "Success";
            $.getJSON("index.php?r=flow-input-snfg/insert-log-problem",{ id:id, event:event, status:status},function(result2){
            
                if (result2.status == "done"){
                  console.log("success");
                }
            });
          } else {
            x.className = "show";
            x.innerHTML = "Raspberry (Panel) Mati atau tidak terhubung internet. Mohon hubungi teknisi atau matikan panel dan nyalakan kembali.";
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);

            var event = "Hapus Antrian Printer";
            var status = "Failed";
            $.getJSON("index.php?r=flow-input-snfg/insert-log-problem",{ id:id, event:event, status:status},function(result2){
            
                if (result2.status == "done"){
                  console.log("success");
                }
            });
          }
          
        });
      }

  };

  document.getElementById("restartRaspi").onclick = function() {
      var id  = '$id';
      var ip  = '$sbc_ip';
      var x = document.getElementById("snackbar");

      var result = confirm("Anda Yakin Ingin Me-Restart Perangkat Panel Raspberry ?");
      if(result){
        $.get('index.php?r=flow-input-snfg/restart-raspi',{ ip:ip},function(data2){

          var data2 = $.parseJSON(data2);
          console.log(data2.status);
          if(data2.status=="done"){
            x.className = "show";
            x.innerHTML = "Panel (Raspberry) Berhasil di Reboot";
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);

            var event = "Reboot";
            var status = "Success";
            $.getJSON("index.php?r=flow-input-snfg/insert-log-problem",{ id:id, event:event, status:status},function(result2){
            
                if (result2.status == "done"){
                  console.log("success");
                }
            });
          } else {
            x.className = "show";
            x.innerHTML = "Raspberry (Panel) Mati atau tidak terhubung internet. Mohon hubungi teknisi atau matikan panel dan nyalakan kembali.";
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);

            var event = "Reboot";
            var status = "Failed";
            $.getJSON("index.php?r=flow-input-snfg/insert-log-problem",{ id:id, event:event, status:status},function(result2){
            
                if (result2.status == "done"){
                  console.log("success");
                }
            });
          }
          
        });
      }
  };


    $(".dial").knob({
        'min':0,
        'max':60,
        // 'angleOffset':-60,
        // 'angleArc':120,
        'readOnly':true,
        'step':0.1,
        'width':200,
        'height':200,
        'thickness':0.3,
    });




    $('#print-form').hide();

    var y = setInterval(function() {

        var isInner  = '$isInner';

        // $.getJSON("http://10.3.5.102:3000/flow_input_snfg_wo?flow_input_snfg_id=eq.$id&order=id.desc&limit=10",function(result){
        $.getJSON("https://mesapi.pti-cosmetics.com/flow_input_snfg_wo?flow_input_snfg_id=eq.$id&order=id.desc&limit=10",function(result){
        
            y = result;

        });

       $("#list_table_json").empty();
           var header = '<tr><th style="width: 20px">No Urut</th><th>Timestamp</th><th>Pack Date</th><th>Exp Date</th><th>Operator</th><th style="width: 40px">Status</th><th style="width: 90px">Action</th></tr>';
            $("#list_table_json").append(header);
           var event_data = '';
          $.each(y, function(index, value){
               /*console.log(value);*/
               event_data += '<tr>';
               event_data += '<td>'+value.nourut+'</td>';
               event_data += '<td>'+value.write_time+'</td>';
               //event_data += '<td><span class="badge bg-green">'+value.weight+' kg</span></td>';
               event_data += '<td>'+value.pack_date+'</td>';
               event_data += '<td>'+value.exp_date+'</td>';
               event_data += '<td>'+value.nama_operator+'</td>';
               if(value.status=='pass'){
                 event_data += '<td><span class="label label-success">Pass</span></td>'; 
               }else if(value.status=='less'){
                 event_data += '<td><span class="label label-warning">Less</span></td>';
               }else if(value.status=='over'){
                 event_data += '<td><span class="label label-danger">Over</span></td>';
               }
               if ((value.status == "pass") && (isInner == 1)) {
                event_data += '<td><button id="delete-'+value.id+'" class="btn btn-block btn-danger" style="height:20px; font-size:1em; padding:0 0 0 0;">Delete</button></td>';
               }
               
              event_data += '<tr>';
           });
           $("#list_table_json").append(event_data);

    }, 1000);

    var m = setInterval(function() {
      $('button[id^="delete-"]').on("click",function() {
        var id_button = $(this).attr('id');
        //var id = $(this).val();
        var id_fis = '$id';

        id_wo = id_button.substring(id_button.lastIndexOf("-") + 1);
        // console.log(id_fis);

        var result = confirm("Anda Yakin Ingin Menghapus Data Ini ?");
        if(result){
          $.getJSON("index.php?r=flow-input-snfg/delete-row&id_fis="+id_fis+"&id_wo="+id_wo, function (data){
            $.getJSON("http://$sbc_ip:6900/?delete",function(value){
                  
                if (value){
                  location.reload();
                } else {
                  alert("Gagal delete di Raspi");
                }
            });
          });  
        }          

      });
    }, 1000);

    $('button[id^="reset_counter"]').on("click",function() {
      var result = confirm("Anda Yakin Ingin Mengulang Counter dari 0 ?");
      if(result){
        // console.log("berhasil");
        $.getJSON("http://$sbc_ip:6900/?reset_counter",function(result){
      
          if (result.status == 1){
            location.reload();
          } else {
            alert("Gagal me-Reset counter!");
          }

        });           
      }  
    });


    var j = setInterval(function() {

        // $.getJSON("http://10.3.5.102:3000/weigher_stats?flow_input_snfg_id=eq.$id",function(result){
        $.getJSON("https://mesapi.pti-cosmetics.com/weigher_stats?flow_input_snfg_id=eq.$id",function(result){
        
            j = result[0];

            // alert(j.jumlah_wo);
            // console.log(j);

            $("#jumlah_ftr").text(j.rft);
            $("#pass").text(j.pass);
            $("#less").text(j.less);
            $("#over").text(j.over);

        });

       

    }, 1000);


    var printer_status = setInterval(function() {

        $.getJSON("http://$sbc_ip:6900/",function(result){
        
            printer_status = result;
            // console.log(printer_status.printer);
            $("#isi_printer_status").text(printer_status.printer);
            

        });

       

    }, 10000);

    var is_counter  = '$is_counter';
    var innerQty = '$innerQty';
    if (is_counter == 1){
      var counter_status = setInterval(function() {

          $.getJSON("http://$sbc_ip:6900/?counter",function(result){
          
              text = 'INNERBOX : '+result.counter.toString()+' / '+innerQty.toString();
              $("#counter_").text(text);
              

          });

      }, 5000);
    }


    document.getElementById("stopbutton").onclick = function() {

        var id  = '$id';
        var snfg  = '$snfg';

        var result = confirm("Anda Yakin Ingin Menyelesaikan Work Order untuk SNFG $snfg ?");
        if(result){
          // window.location = "https://factory.pti-cosmetics.com/flowreport/web/index.php?r=flow-input-snfg/stop-work-order&snfg="+snfg+"&last_id="+id+"&pos=inline";    
          window.location = "index.php?r=flow-input-snfg/stop-work-order&snfg="+snfg+"&last_id="+id+"&pos=inline";          
        }

    };

    document.getElementById("tareBtn").onclick = function() {

        var id  = '$id';
        var status_tare = this.getAttribute("value");
        console.log(this.getAttribute("value"));

        if (status_tare == 'on'){
          var result = confirm("Anda Yakin Ingin Memulai Proses Tare Timbangan ?");
          if(result){
            $.getJSON("http://$sbc_ip:6900/?tare_on",function(result){
                // alert(result.statusTare);              
            });
            window.location = "index.php?r=flow-input-snfg/insert-tare&id="+id;          
          }
        }else{
          var result = confirm("Anda Yakin Ingin Stop Proses Tare Timbangan ?");
          if(result){
            $.getJSON("http://$sbc_ip:6900/?tare_off",function(result){
                // alert(result.statusTare);              
            });
            window.location = "index.php?r=flow-input-snfg/insert-tare&id="+id;          
          }
        }

        // var id  = '$id';
        // var snfg  = '$snfg';

        // var result = confirm("Anda Yakin Ingin Print Contoh Label ?");
        // if(result){
        //   $.getJSON("http://$sbc_ip:6900/?awal",function(result){
        //       alert(result.test);              
        //   });          
        // }

    };

    document.getElementById("splitBtn").onclick = function() {

        var id  = '$id';
        var now  = '$status_split';
        var snfg  = '$snfg';
        // var countSplit  = '$countSplit';

        if (now == 'AKTIF'){
          var result = confirm("Anda Yakin Akan Mematikan Fitur SPLIT LINE ?");
          if(result){
            // window.location = "index.php?r=flow-input-snfg/deactivate-split-line&id="+id+"&now="+now+"&snfg="+snfg;          
            $.getJSON("index.php?r=flow-input-snfg/deactivate-split-line&id="+id+"&now="+now+"&snfg="+snfg, function(data){
              if(data.status == 'success'){
                window.location.reload();
              }else{
                window.alert("Fitur SPLIT LINE tidak bisa dimatikan karena sudah ada yang scan di line lain");
              }
            });
          }
        } else {
          var result = confirm("Anda Yakin Akan Mengaktifkan Fitur SPLIT LINE ?");
          if(result){
            window.location = "index.php?r=flow-input-snfg/activate-split-line&id="+id+"&now="+now;          
          }
        }
        

    };

    document.getElementById("forceprint").onclick = function() {

        // var id  = '$id';
        // var snfg  = '$snfg';

        var result = confirm("Anda Yakin Ingin Print Manual ?");
        if(result){
          $.getJSON("http://$sbc_ip:6900/?test",function(result){
              alert(result.test);              
          });          
        }

    };


    document.getElementById("print_awal").onclick = function() {

        // var id  = '$id';
        // var snfg  = '$snfg';

        var result = confirm("Anda Yakin Ingin Print Contoh Label ?");
        if(result){
          $.getJSON("http://$sbc_ip:6900/?awal",function(result){
              alert(result.test);              
          });          
        }

    };

    document.getElementById("printlast").onclick = function() {

        // var id  = '$id';
        // var snfg  = '$snfg';

        var result = confirm("Anda Yakin Ingin Print Label Terakhir ?");
        if(result){
          $.getJSON("http://$sbc_ip:6900/?last",function(result){
              if (result.test == 1){
                x.className = "show";
              x.innerHTML = "Cetak Ulang Label Berhasil";
              setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
              }
              //alert(result.test);              
          }).error(function() {
            x.className = "show";
            x.innerHTML = "Raspberry (Panel) Mati atau tidak terhubung internet. Mohon hubungi teknisi atau matikan panel dan nyalakan kembali.";
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000); 
          });          
        }

    };

    // if (is_counter == 1)
    // {
      var id  = '$id';
      $('select[id^="op_karbox"]').on("change",function() {
        var nilai = $(this).val();
        // console.log(nilai);

        $.post("index.php?r=flow-input-snfg/insert-swo&id="+id+"&nama_operator="+nilai, function (data){});
      });

      $('select[id^="op_innerbox"]').on("change",function() {
        var nilai = $(this).val();
        // console.log(nilai);
        $.post("index.php?r=flow-input-snfg/insert-operator-innerbox&id="+id+"&nama_operator="+nilai, function (data){});
      });
    // }



JS;
    $this->registerJs($script);
?>



<?php $this->registerJsFile('/flowreport/web/js/jquery.knob.js', ['depends' => [yii\web\JqueryAsset::className()]]); ?>

