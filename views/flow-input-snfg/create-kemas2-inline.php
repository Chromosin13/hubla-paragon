<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */


?>
<div class="flow-input-snfg-create">




    <?php 

    	echo $this->render('_form-kemas2-inline', [
		        'model' => $model,
		        'snfg' => $snfg,
			    'searchModel' => $searchModel,
			    'dataProvider' => $dataProvider,
			    'nama_line' => $nama_line,
			    'jenis_proses_list' => $jenis_proses_list,
		        'frontend_ip' => $frontend_ip,
		        'sbc_ip' => $sbc_ip,
		        'exp_date' => $exp_date,
		        'nosmb' => $nosmb,
		        'nobatch' => $nobatch,
		        'barcode' => $barcode,
                'fg_name' => $fg_name,
                'odoo_code' => $odoo_code,
                'is_split' => $is_split,
		]);

     ?>

</div>
