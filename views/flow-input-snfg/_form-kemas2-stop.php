<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputSnfg;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
#modaltitle{
  display:inline-block;
}
.modal-body{
  font-weight:500;
  font-size:24px;
}
.modal-content{
  vertical-align:middle;
  margin-top: 25%;
  background-color : #f66257;
  text-align:center;

}

</style>




                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
                      <h3 class="widget-user-username"><b>Kemas 2 / Packing</b></h3>
                      <h5 class="widget-user-desc">Work Order</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/scale.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">

                            <div class="box-body">

                                <div class="flow-input-mo-form">


                                    <?php $form = ActiveForm::begin(); ?>

                                    <div class="row">
                                        <div class="col-md-4">
                                          <?= $form->field($model, 'jumlah_realisasi')->textInput() ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-4">
                                          <?= $form->field($model, 'nobatch')->textInput(['value'=>$batch]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-4">
                                          <?= $form->field($model, 'counter')->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                          <?= $form->field($model, 'netto_min')->textInput(['value'=>$netto_min,'readOnly'=>true])->label('Netto Minimum (gr)',['class'=>'label-class']) ?>
                                        </div>
                                        <div class="col-md-2">
                                          <?= $form->field($model, 'netto_1')->textInput(['id'=>'netto-1','required'=>true])->label('Netto Sample 1 (gr)',['class'=>'label-class']) ?>
                                        </div>
                                        <div class="col-md-2">
                                          <?= $form->field($model, 'netto_2')->textInput(['id'=>'netto-2','required'=>true])->label('Netto Sample 2 (gr)',['class'=>'label-class']) ?>
                                        </div>

                                        <div class="col-md-2">
                                          <?= $form->field($model, 'netto_3')->textInput(['id'=>'netto-3','required'=>true])->label('Netto Sample 3 (gr)',['class'=>'label-class']) ?>
                                        </div>
                                        <div class="col-md-2">
                                          <?= $form->field($model, 'netto_4')->textInput(['id'=>'netto-4','required'=>true])->label('Netto Sample 4 (gr)',['class'=>'label-class']) ?>
                                        </div>

                                        <div class="col-md-2">
                                          <?= $form->field($model, 'netto_5')->textInput(['id'=>'netto-5','required'=>true])->label('Netto Sample 5 (gr)',['class'=>'label-class']) ?>
                                          <span id=netto-5-e></span>
                                           <!-- /input-group -->

                                        </div>
                                    </div>
                                    <div class="row" >
                                        <div class="col-md-12">
                                          <?= $form->field($model, 'staging')->checkbox(); ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-12" id="div-hide">
                                          <?= $form->field($model, 'is_done')->checkbox(['id'=>'check-alert']); ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>

                                <div class="flow-input-snfg-form", id = 'form-sisa-kemas'>

                                    <div class="row">
                                        <div class="col-md-4">
                                          <?= $form->field($model, 'sisa_bulk')->textInput(['id'=>'sb'])->label('Sisa Bulk (kg)',['class'=>'label-class']) ?>
                                        </div>
                                        <div class="col-md-4">
                                          <?= $form->field($model, 'fg_retur_layak_pakai')->textInput(['id'=>'fg-layak'])->label('FG Retur Layak Pakai (pcs)',['class'=>'label-class']) ?>
                                        </div>

                                        <div class="col-md-4">
                                          <?= $form->field($model, 'fg_retur_reject')->textInput(['id'=>'fg-reject'])->label('FG Retur Reject (pcs)',['class'=>'label-class']) ?>
                                        </div>
                                    </div>




                                </div>

                                    <div class="col-md-6 col-md-offset-5">

                                        <a id='buttonStop' class = 'btn btn-danger'>Stop Jadwal</a>
                                        <?php echo Html::submitButton('Stop Jadwal',['hidden'=>'hidden','id'=>'stop-jadwal','class'=>'btn btn-danger'], ['stop-kemas2', 'snfg'=>$snfg,'last_id'=>$last_id, 'pos'=>$pos]) ?>

                                    </div>

                                    <?php ActiveForm::end(); ?>



                                </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>


<div class="box box-widget widget-user">
    <div class="widget-user-header bg-blue">
                      <h3 class="widget-user-username"><b>Rincian</b></h3>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            // ['class' => 'kartik\grid\SerialColumn'],
            'lanjutan',
            'snfg',
            'posisi',
            'datetime_start',
            'datetime_stop',
            // 'datetime_write',
            // 'nama_operator',
            'nama_line',
            'jenis_kemas',
            'nama_operator',
            'jumlah_realisasi',
            // 'shift_plan_start_olah',
            // 'shift_plan_end_olah',
            // 'plan_start_olah',
            // 'plan_end_olah',
            // 'turun_bulk_start',
            // 'turun_bulk_stop',
            // 'adjust_start',
            // 'adjust_stop',
            // 'ember_start',
            // 'ember_stop',
            // 'is_done',
            // 'id',

            // ['class' => 'kartik\grid\ActionColumn'],
        ],
    ]); ?>

</div>

<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
  Launch demo modal
</button> -->

    <div class="modal fade"  id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h2 class="modal-title" id="modaltitle">Confirm</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              Apakah proses <?php echo $jenis_kemas;?> sudah selesai All Batch / Is Done?<br>
              <!-- Jika belum selesai, dan dilanjut di shift berikutnya, klik Belum All Batch. -->
          </div>
          <div class="modal-footer">
            <button id="cancel" type="button" class="btn btn-warning pull-left" data-dismiss="modal">Cancel</button>
            <button id="notdone" type="button" class="btn btn-secondary">Belum All Batch</button>
            <button id="done" type="button" class="btn btn-primary">Sudah All Batch</button>
          </div>
        </div>
      </div>
    </div>


<?php
$script = <<< JS
    $('#div-hide').hide();
    $('#stop-jadwal').hide();

    $('#form-sisa-kemas').hide();

    // $("#check-alert").on("click", function() {
    //     if ($('input[type="checkbox"]').is(':checked')) {
    //         krajeeDialog.alert("!!! PERHATIAN !!! <div> Anda mengaktifkan pilihan ISDONE. <div> Pastikan batch benar-benar sudah selesai, atau SCAN SELANJUTNYA TIDAK BISA DILAKUKAN");
    //
    //         $('#form-sisa-kemas').fadeIn("slow");
    //
    //         return true;
    //
    //     }else{
    //         $('#form-sisa-kemas').hide();
    //         return true;
    //
    //     }
    // });

    $("#notdone").on("click", function(){
        $('#check-alert').prop("checked", false);
        $('#stop-jadwal').trigger('click');
    });

    $("#done").on("click", function(){
        $('#check-alert').prop("checked", true);
        $('#stop-jadwal').trigger('click');
    });

    // $("#stop-jadwal").on("click", function validation(){
    //           var sb = document.getElementById('sb').value;
    //           var fg_layak = document.getElementById('fg-layak').value;
    //           var fg_reject = document.getElementById('fg-reject').value;
    //           var done = document.getElementById('check-alert').value;
    //           if ($('input[id="check-alert"]').is(':checked') && (sb==''||fg_layak==''||fg_reject=='')){
    //             alert('Data harus diisi semua');
    //             return false;
    //           }else{
    //             return true;
    //           }
    //         });
    
    $("#buttonStop").on("click", function validation(){
        // console.log("berhasil");
        // var sb = document.getElementById('sb').value;
        var netto_min = "$netto_min";
        var netto1 = document.getElementById('netto-1').value;
        var netto2 = document.getElementById('netto-2').value;
        var netto3 = document.getElementById('netto-3').value;
        var netto4 = document.getElementById('netto-4').value;
        var netto5 = document.getElementById('netto-5').value;

        if (netto1==''||netto2==''||netto3==''||netto4==''||netto5==''){
          alert('Data harus diisi semua');
          return false;
        }else{
          $('#modalConfirm').appendTo('body').modal('show');
          
          // // Jika field netto sudah terisi semua;
          // if (netto_min == null || netto_min == ''){
          //   //Jika netto_min bernilai null, validasi tidak dilakukan
          //   $('#modalConfirm').appendTo('body').modal('show');
          // }else{
          //   //Jika netto_min tidak null
          //   if (netto1 < netto_min + 40*netto_min/100 && netto1 > netto_min - 40*netto_min/100){
          //     if (netto2 < netto_min + 40*netto_min/100 && netto2 > netto_min - 40*netto_min/100){
          //       if (netto3 < netto_min + 40*netto_min/100 && netto3 > netto_min - 40*netto_min/100){
          //         if (netto4 < netto_min + 40*netto_min/100 && netto4 > netto_min - 40*netto_min/100){
          //           if (netto5 < netto_min + 40*netto_min/100 && netto5 > netto_min - 40*netto_min/100){
          //             $('#modalConfirm').appendTo('body').modal('show');
          //           }else{
          //             alert ('Netto 5 tidak sesuai standar netto');
          //             return false;
          //           }
          //         }else{
          //           alert ('Netto 4 tidak sesuai standar netto');
          //           return false;
          //         }
          //       }else{
          //         alert ('Netto 3 tidak sesuai standar netto');
          //         return false;
          //       }
          //     }else{
          //       alert ('Netto 2 tidak sesuai standar netto');
          //       return false;
          //     }
          //   }else{
          //     alert ('Netto 1 tidak sesuai standar netto');
          //     return false;
          //   }
          // }
        }
    });


    $('#lanjutan').hide();

    var snfg = $('#flowinputsnfg-snfg').val();

    var posisi = $('#flowinputsnfg-posisi').val();

    // $.post("index.php?r=scm-planner/get-line-kemas-snfg&snfg="+$('#flowinputsnfg-snfg').val(), function (data){
    //     $("select#flowinputsnfg-nama_line").html(data);
    // });

    $('#flowinputsnfg-jumlah_realisasi').change(function(){

        var current = $('#flowinputsnfg-jumlah_realisasi').val();
        var snfg = "$snfg";
        var jenis_kemas = "$jenis_kemas";

        $.get('index.php?r=flow-input-snfg/get-realisasi-before',{ snfg : snfg , jenis_kemas : jenis_kemas , posisi : posisi  }, function(data){
            var data = $.parseJSON(data);

        });

        $('#lanjutan').fadeIn();

    });


    $('#flowinputsnfg-jenis_kemas').change(function(){

        var jenis_kemas = $('#flowinputsnfg-jenis_kemas').val();

        $.get('index.php?r=flow-input-snfg/get-lanjutan-kemas2',{ snfg : snfg , jenis_kemas : jenis_kemas , posisi : posisi  }, function(data){
            var data = $.parseJSON(data);
            $('#flowinputsnfg-lanjutan').attr('value',data.lanjutan);

        });

        $('#lanjutan').fadeIn();

    });

JS;
$this->registerJs($script);
?>
