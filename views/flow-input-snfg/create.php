<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */

$this->title = 'Create Flow Input Snfg';
$this->params['breadcrumbs'][] = ['label' => 'Flow Input Snfgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flow-input-snfg-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
