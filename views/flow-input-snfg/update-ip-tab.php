<?php

use app\models\WeigherFgMapDevice;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="weigher-fg-map-device-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'frontend_ip')->textInput(['readOnly'=>true,'value'=>$frontend_ip]) ?>

    <!-- <?= $form->field($model, 'line')->textInput() ?> -->
    <?php
        echo $form->field($model, 'line')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(WeigherFgMapDevice::find()->all()
            ,'line','line'),
            'options' => ['placeholder' => 'Select Line'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('Nama line');
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Update' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
