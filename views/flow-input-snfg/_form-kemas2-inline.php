<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputSnfg;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\Downtime;
use app\models\DowntimeSearch;
use kartik\date\DatePicker;
use app\models\MasterDataLine;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */
/* @var $form yii\widgets\ActiveForm */
?>


                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
                      <h3 class="widget-user-username"><b>Kemas 2 / Packing</b></h3>
                      <h5 class="widget-user-desc">Work Order</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                      <?php if ($is_split == 1){ ?>
                                        <h2 style="text-align: center; background-color:yellow;">SAAT INI ANDA SEDANG MENGAKTIFKAN FITUR SPLIT LINE UNTUK SNFG INI</h2>
                                      <?php } ?>
                                    </div>
                                </div>
                                <br>
                                <div class="flow-input-snfg-form">

                                    <?php $form = ActiveForm::begin(); ?>

                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'snfg')->textInput(['readOnly'=>true,'value'=>$snfg]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'posisi')->textInput(['readOnly'=>true,'value'=>'KEMAS_2']) ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                          <?php 

                                                echo $form->field($model, 'jenis_kemas')
                                                ->dropDownList(
                                                    $jenis_proses_list,           // Flat array ('id'=>'label')
                                                    ['prompt'=>'Select Options']    // options
                                                )->label('Jenis Kemas');  

                                            ?>
                                          <!-- /input-group -->
                                        </div>
                                        <!-- /.col-lg-6 -->
                                        <div class="col-md-3">
                                          <?php 

                                                if(empty($nama_line)){
                                                    echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(MasterDataLine::find()->where("sediaan not like '%off%'")->all()
                                                        ,'nama_line','nama_line'),
                                                        'options' => ['placeholder' => 'Select Line'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]);
                                                }else{
                                                    // echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                                                    //     'data' => ArrayHelper::map(MasterDataLine::find()->where("sediaan not like '%off%'")->all()
                                                    //     ,'nama_line','nama_line'),
                                                    //     'options' => ['placeholder' => 'Select Line', 'value' => $nama_line],
                                                    //     'pluginOptions' => [
                                                    //         'allowClear' => true
                                                    //     ],
                                                    // ]);
                                                    echo $form->field($model, 'nama_line')->textInput(['value'=>$nama_line,'readOnly'=>true]);

                                                }
                                            ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div id="lanjutan">
                                        <div class="col-md-3">
                                          <?= $form->field($model, 'lanjutan')->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        </div>
                                    </div>
 

                                    <label class="control-label">Nama Operator</label>
                                    <?= $form->field($model, 'nama_operator')->widget(TagsinputWidget::classname(), [
                                                        'clientOptions' => [
                                                            'trimValue' => true,
                                                            'allowDuplicates' => false,
                                                            'maxChars'=> 4,
                                                            'minChars'=> 4,
                                                        ]
                                                    ])->label(false)?>

                                    <br></br>

                                    <?php 

                                        if($fg_name==""){
                                            echo $form->field($model, 'fg_name_odoo')->textInput(['placeholder'=>'Data tidak ditemukan, silahkan isi manual']);
                                        } else {
                                            echo $form->field($model, 'fg_name_odoo')->textInput(['value'=>$fg_name]); 
                                        }
                                    ?>

                                    <?php 

                                        if($barcode==""){
                                            echo $form->field($model, 'barcode')->textInput(['placeholder'=>'13 digit angka lihat di packaging, contoh : 8897652751724']);
                                        } else {
                                            echo $form->field($model, 'barcode')->textInput(['value'=>$barcode]); 
                                        }
                                    ?>

                                     <?php 

                                        if($nobatch=="-"){
                                            echo $form->field($model, 'nobatch')->textInput(['placeholder'=>'Data tidak ditemukan, silahkan isi manual']);
                                        } else {
                                            echo $form->field($model, 'nobatch')->textInput(['value'=>$nobatch]); 
                                        }
                                    ?>

                                    <?php 
                                        if (strpos($snfg,'MY-BRUSEBS-EM')!==false)
                                        {
                                            echo $form->field($model, 'exp_date')->textInput(); 
                                        }else{
                                            echo 
                                            DatePicker::widget([
                                            'model' => $model,
                                            'form' => $form,
                                            'name' => 'exp_date',
                                            'attribute' => 'exp_date', 
                                            //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                                            'options' => ['required'=>'required','value'=>$exp_date,'placeholder' => 'Pilih Exp Date'],
                                            'pluginOptions' => [
                                                'format' => 'yyyy-M-dd',
                                                'todayHighlight' => true
                                                ]
                                            ]);
                                        }
                                    ?>

                                    <p id='warning' style='color:red; visibility: hidden'>Expired date salah, terhitung hanya 1 tahun. Mohon koreksi.</p>

                                    <?php 

                                        if($nosmb=="-"){
                                            echo $form->field($model, 'nosmb')->textInput(['placeholder'=>'Data tidak ditemukan, silahkan isi manual']);
                                        } else {
                                            echo $form->field($model, 'nosmb')->textInput(['value'=>$nosmb]); 
                                        }
                                    ?>

                                    <div class="row">
                                        <div class="col-md-2 col-md-offset-5" id="hide-submit">
                                            <?php 
                                                echo Html::submitButton($model->isNewRecord ? 'Start' : 'Update', ['id'=>'btn-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
                                            ?>
                                        </div>
                                        <div class="col-md-2 col-md-offset-5">
                                            
                                            <?php 
                                                echo Html::Button('Start', ['id'=>'btn-check','class' => 'btn btn-success']);
                                            ?>
                                            
                                        </div>
                                    </div>

                                    <?php ActiveForm::end(); ?>

                                </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>


<div class="box box-widget widget-user">
    <div class="widget-user-header bg-blue">
                      <h3 class="widget-user-username"><b>Rincian</b></h3>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'value' => function($model,$key,$index,$column){
                    return GridView::ROW_COLLAPSED;
                },
                'detail' => function($model,$key,$index,$column){
                    
                    $searchModel = new DowntimeSearch();
                    
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                    $dataProvider->query->where("flow_input_snfg_id=".$model->id);

                    return Yii::$app->controller->renderPartial('_expand-downtime-kemas2', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'flow_input_snfg_id' => $model->id,
                    ]);
                },
            ],
            'lanjutan',
            'snfg',
            'posisi',
            'datetime_start',
            'datetime_stop',
            'nama_line',
            'jenis_kemas',
            'nama_operator',
        ],
    ]); ?>

</div>


<?php

    Modal::begin([
            'header'=>'<h4>Line Kemas Sedang Digunakan</h4>',
            'id' => 'modalWarning',
            'size' => 'modal-lg',
        ]);

    echo "<div id='modalContentWarning'></div>";

    Modal::end();

?>

<script type="text/javascript">
function check_exp(){
    var date_exp = $('#flowinputsnfg-exp_date').val();
    var exp_date = new Date(date_exp);

    var current_date = new Date();

    exp_year = exp_date.getFullYear();
    current_year = current_date.getFullYear();

    const diffTime = Math.abs(exp_date - current_date);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

    if ((diffDays)<=365){
        $( "#btn-check" ).prop( "disabled", true );
        $("#warning").css("visibility", "visible");
    } else {
        $( "#btn-check" ).prop( "disabled", false );
        $("#warning").css("visibility", "hidden");
    }
    // console.log(exp_date.getFullYear());
    // console.log(current_date.getFullYear());
      
}
</script>



<?php
$script = <<< JS

    $('#lanjutan').hide();
    $('#hide-submit').hide();
    $( "#dummy-submit" ).show();

    var snfg = $('#flowinputsnfg-snfg').val();
    
    var posisi = $('#flowinputsnfg-posisi').val();

    // $.post("index.php?r=scm-planner/get-line-kemas-snfg2&snfg="+$('#flowinputsnfg-snfg').val(), function (data){
    //     $("select#flowinputsnfg-nama_line").html(data);
    // });

    check_exp();

    $('#btn-check').on('click',function(){

        var snfg = $('#flowinputsnfg-snfg').val();
        var posisi = $('#flowinputsnfg-posisi').val();
        var jenis_kemas = $('#flowinputsnfg-jenis_kemas').val();
        var nama_line = $('#flowinputsnfg-nama_line').val();
        var lanjutan = $('#flowinputsnfg-lanjutan').val();
        var nama_operator = $('#flowinputsnfg-nama_operator').val();
        var fg_name_odoo = $('#flowinputsnfg-fg_name_odoo').val();
        var nobatch = $('#flowinputsnfg-nobatch').val();
        var exp_date = $('#flowinputsnfg-exp_date').val();
        var nosmb = $('#flowinputsnfg-nosmb').val();
        var test = null;

        if (snfg){
            if (posisi){
                if (jenis_kemas){
                    if (nama_line){
                        if (lanjutan){
                            if (nama_operator){
                                if (fg_name_odoo){
                                    if (nobatch){
                                        if (exp_date){
                                            if (nosmb){
                                                $( "#btn-check" ).prop( "disabled", true );
                                                $('#btn-submit').trigger('click');                                                        

                                            }else{ alert('No SMB Kosong'); }
                                        }else{ alert('Exp Date Kosong'); }
                                    }else{ alert('Nomor Batch Kosong'); }
                                }else{ alert('Nama Item Kosong'); }
                            }else{ alert('Nama Operator Kosong'); }
                        }else{ alert('Lanjutan Kosong'); }
                    }else{ alert('Nama Line Kosong'); }
                }else{ alert('Jenis Kemas Kosong'); }
            }else{ alert('Posisi Proses Kosong'); }
        }else{ alert('SNFG Kosong'); }

    });  

    var odoo_code = "$odoo_code";
    console.log("Odoo Code : "+odoo_code);
    if (odoo_code == ''){
        // window.alert("Odoo Code tidak ditemukan, proses scan kemas tidak bisa dilanjutkan, silahkan lapor ke tim IT.");
        swal("Oops!", "Odoo Code tidak ditemukan, proses scan kemas tidak bisa dilanjutkan, silahkan lapor ke tim IT.", "error");
        $('button#btn-check').hide();
    }


    $('#flowinputsnfg-jenis_kemas').change(function(){

        var jenis_kemas = $('#flowinputsnfg-jenis_kemas').val();

        $.get('index.php?r=flow-input-snfg/get-lanjutan-kemas2',{ snfg : snfg , jenis_kemas : jenis_kemas , posisi : posisi  }, function(data){
            var data = $.parseJSON(data);
            $('#flowinputsnfg-lanjutan').attr('value',data.lanjutan);

        });

        $('#lanjutan').fadeIn();

    });  

    document.getElementById('flowinputsnfg-nobatch').addEventListener('input', function (e) {
      //e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1,').trim();
        e.target.value = e.target.value.toUpperCase();
        // if (e.target.value.length % 5 == 0){
        //     //e.target.value = e.target.value.replace(/(.{4})/g, '$1').replace(/[ ,]+/g, ",");
        //     e.target.value = e.target.value.replace(/.$/,",");
        // }
        
    });

    $('input[id^="flowinputsnfg-exp_date"]').on("change",function() {
      check_exp();

    });



JS;
$this->registerJs($script);
?>