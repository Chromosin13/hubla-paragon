<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowAnalyst */

?>
<div class="flow-checkkemas2-qr">

    <?= $this->render('_form-check-kemas2-qr', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'frontend_ip' => $frontend_ip,
        'sbc_ip' => $sbc_ip,
        'line' => $line,
    ]) ?>

</div>
