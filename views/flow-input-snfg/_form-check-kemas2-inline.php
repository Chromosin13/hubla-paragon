<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>


<style>
#modaltitle{
  display:inline-block;
}
.modal-body{
  font-weight:500;
  font-size:18px;
}
.modal-content{
  vertical-align:middle;
  margin-top: 25%;
  background-color : #f66257;
  /*text-align:center;*/
}

</style>

<?php

Modal::begin([
        'header'=>'<h4>Update IP Tablet</h4>',
        'id' => 'modalupdateiptablet',
        'size' => 'modal-lg',
    ]);

echo "<div id='modalContentUpdateIpTablet'></div>";

Modal::end();

?>


<div class="row">
  <div class="col-md-4">
  
        <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Line</span>
              <span class="info-box-number"><?php echo $line?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
              <span class="progress-description">
                Jika Line Diatas tidak sesuai, maka hubungi Teknisi
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
    </div>
    <div class="col-md-4">

        <div class="info-box bg-red">
            <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">FRONTEND IP</span>
              <span class="info-box-number"><?php echo $frontend_ip?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
              <span class="progress-description">
                    -
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
    </div>
    <div class="col-md-4">

        <div class="info-box bg-blue">
            <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">SBC IP</span>
              <span class="info-box-number"><?php echo $sbc_ip?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
              <span class="progress-description">
                    <?php echo $keterangan?>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
  </div>
</div>


<div class="box box-widget widget-user">
  <!-- Add the bg color to the header using any of the bg-* classes -->
  <div class="widget-user-header bg-blue-gradient">
    <h3 class="widget-user-username"><b>Kemas 2</b></h3>
    <h5 class="widget-user-desc">Input Data</h5>
  </div>
  <div class="widget-user-image">
    <img class="img-circle" src="../web/images/business-partnership.png" alt="User Avatar">
  </div>


<!-- Form Scan QR / Input Manual -->
<div id="linkage-warning">
  <div class="callout callout-danger">
    <h4>Perhatian!</h4>

    <p>Perangkat Tablet Tidak Terdaftar ke Line / Mesin Printer manapun, Mohon menghubungi teknisi</p>
    <!-- <button type="button" class="btn btn-warning modalUpdateIpTablet" data-toggle="modal modalupdateiptablet" data-target="#activity-modal" value=<?php echo Url::to('index.php?r=flow-input-snfg/update-ip-tablet');?>>
    UPDATE IP TABLET
    </button> -->
    <!-- <?= Html::a('UPDATE IP TABLET', [''], 
                                    [
                                      'class' => 'btn btn-success btn-block modalUpdateIpTablet grid-action',
                                      'data-toogle'=>'modalupdateiptablet',
                                      'data-target'=>'#activity-modal',
                                      'value'=>Url::to('index.php?r=flow-input-snfg/update-ip-tablet')
                                    ]); ?> -->
  </div>
</div>
<div id="input-form">
    <div class="box-footer">
        <div class="row">
          <div class="box-body">
              <div class="box-body">
                  <div class="zoom">
                    <?=Html::a('SCAN WITH QR', Yii::getAlias("@dnsUrl").'flow-input-snfg/check-kemas2-qr', ['class' => 'btn btn-success btn-block']);?>
                    </div>
                  <p></p>
                  <div class="flow-input-snfg-form">

                    <div class="form-group field-flowinputsnfg-snfg has-success">
                    <label class="control-label" for="flowinputsnfg-snfg">Snfg</label>
                    <input type="text" id="flowinputsnfg-snfg" class="form-control" name="FlowInputSnfg[snfg]" aria-invalid="false">
                    <br>
                    <button id="checkbtn" class="btn btn-info btn-block" style="width:50%; margin:0 auto;">CHECK</button>

                  </div>
              </div>
          </div>
        </div>
        <!-- /.row -->
      </div>
    </div>
</div>


<!-- List Data -->

 <div class="box box-widget widget-user">

    <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                  IN PROGRESS
    </h4>

    <div class="box-footer">

      <div class="row">
        <div class="box-body">
             
                <div class="flow-input-snfg-form">



                   <?= GridView::widget([
                      'dataProvider' => $dataProvider,
                      'columns' => [
                          
                          'snfg',
                          'lanjutan',
                          'posisi',
                          'datetime_start',
                          'datetime_stop',
                          'nama_line',
                          'jenis_kemas',
                          'nama_operator',
                      ],
                  ]); ?>


                </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
</div>

<div class="modal fade"  id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="modaltitle">Split Line not active</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="isi_modal" class="modal-body">
          <p class="text-center" style="font-size:1.25em;">Mohon maaf, jadwal ini sedang berjalan di line (<?php echo $line;?>) di hari ini.</p><br>
          <b class="text-left" style="font-size:1em;">SNFG</b> :  <p id="isi_snfg"></p>
          <br>
          <p class="text-center" style="font-size:1.25em;">Silahkan scan stop dahulu!<br>
          <p><b>Notes</b> : Ada kemungkinan line lain sedang jalan menggunakan nama line ini (<?php echo $line;?>) karena salah pilih. Nama line di-lock untuk memastikan hanya akan ada 1 jadwal yang berjalan di suatu line.</p>
          <!-- Jika belum selesai, dan dilanjut di shift berikutnya, klik Belum All Batch. -->
      </div>
      <div class="modal-footer">
        <button id="cancel" type="button" class="btn btn-warning pull-left" data-dismiss="modal">Close</button>
        <!-- <button id="notdone" type="button" class="btn btn-secondary"></button>
        <button id="done" type="button" class="btn btn-primary">Sudah All Batch</button> -->
      </div>
    </div>
  </div>
</div>


<?php

    Modal::begin([
            'header'=>'<h2 style="text-align:center; font-weight:bold;">Line Kemas Sedang Digunakan</h2>',
            'id' => 'modalWarning',
            'size' => 'modal-lg',
        ]);

    echo "<div id='modalContentWarning'></div>";

    Modal::end();

?>

<?php

    Modal::begin([
            'header'=>'<h2 style="text-align:center; font-weight:bold;">Split Line Tidak Aktif</h2>',
            'id' => 'modalSplit',
            'size' => 'modal-lg',
        ]);

    echo "<div id='modalContentSplit'></div>";

    Modal::end();

?>


<?php
$script = <<< JS
  $('#linkage-warning').hide();
  var line = '$line';
  var is_line_exist = '$is_line_exist';
  if(line =='LINKAGE NOT FOUND!'){
    $('#input-form').hide();
    $('#linkage-warning').fadeIn();
  }
  // else{
    
  //   $('#input-form').hide();
  //   $('#linkage-warning').fadeIn();
  
  // }

  // AutoFocus SNFG Field

  document.getElementById("flowinputsnfg-snfg").focus();


  // Check SNFG Validity

    $('#checkbtn').click(function(){  

      var snfg = $('#flowinputsnfg-snfg').val().trim();
      var nama_line = '$line';
      
      if(snfg){
        $.get('index.php?r=flow-input-snfg/check-active-snfg',{ nama_line : nama_line, snfg : snfg },function(data){ 
            var data = $.parseJSON(data);

            if (data.route == "to-stop" || data.route == "to-create"){
              location.href = "index.php?r=flow-input-snfg/create-kemas2-inline&snfg="+snfg;
            } else if (data.route == "split-not-active"){
              var value = "index.php?r=flow-input-snfg/split-not-active&nama_line="+data.nama_line+"&snfg="+snfg;
              
              $('#modalSplit').modal('show')
                .find('#modalContentSplit')
                .load(value);

            } else {
              var value = 'index.php?r=flow-input-snfg/line-not-available&nama_line='+nama_line;

              $('#modalWarning').modal('show')
                .find('#modalContentWarning')
                .load(value);

              // document.getElementById("isi_snfg").innerHTML = "";
              // data.forEach(myFunction);
              // $('#modalConfirm').appendTo('body').modal('show');
              
              // $.get('index.php?r=scm-planner/periksa-status-snfg',{ snfg : snfg },function(data){ 
                //var data = $.parseJSON(data);
                // console.log(data);
                // if (data=="UNHOLD"){
                  //location.href = "index.php?r=flow-input-snfg/create-kemas2-inline&snfg="+snfg;
                // } else {

                //}
              //});
                //location.href = "index.php?r=flow-input-snfg/create-kemas2-inline&snfg="+snfg;
            }
        });
      } else {
        alert("Isi field SNFG terlebih dahulu!.");
      }
      
  });

  $(function(){
	  // get the click of the create button
	  $(".modalUpdateIpTablet").click(function() {
	      // alert();
        $('#modalupdateiptablet').modal('show')
	        // find id
	        .find('#modalContentUpdateIpTablet')
	        // load id
	        .load($(this).attr('value'));
        // return false;
	  });

	});

  $("#flowinputsnfg-snfg").on('keyup', function (e) {
    if (e.key === 'Enter' || e.keyCode === 13) {
      var snfg = $('#flowinputsnfg-snfg').val().trim();
      var nama_line = '$line';
      if(snfg){
        $.get('index.php?r=flow-input-snfg/check-active-snfg',{ nama_line : nama_line, snfg : snfg },function(data){ 
            var data = $.parseJSON(data);
            //console.log(data);
            if (data.route == "to-stop" || data.route == "to-create"){
              location.href = "index.php?r=flow-input-snfg/create-kemas2-inline&snfg="+snfg;
            } else {
              document.getElementById("isi_snfg").innerHTML = "";
              data.forEach(myFunction);
              $('#modalConfirm').appendTo('body').modal('show');
              
              // $.get('index.php?r=scm-planner/periksa-status-snfg',{ snfg : snfg },function(data){ 
                //var data = $.parseJSON(data);
                // console.log(data);
                // if (data=="UNHOLD"){
                  //location.href = "index.php?r=flow-input-snfg/create-kemas2-inline&snfg="+snfg;
                // } else {

                //}
              //});
                //location.href = "index.php?r=flow-input-snfg/create-kemas2-inline&snfg="+snfg;
            }
        });
      } else {
        alert("Isi field SNFG terlebih dahulu!.");
      }
    }
});

  function myFunction(item, index) {
    document.getElementById("isi_snfg").innerHTML += (index+1) + ". " +item + "<br>";
  }


JS;
$this->registerJs($script);
?>