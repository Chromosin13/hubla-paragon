<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputSnfg */

?>
<div class="flow-input-snfg-view">

  <p style="font-size: 20px; font-weight: bold;">Line Ini sedang mengerjakan Jadwal berikut :</p><br>

    <?= DetailView::widget([
      'model' => $model,
      // 'striped' => false,
      'options' =>['class' => 'table table-bordered'],
      'attributes' => [
          'snfg',
          'fg_name_odoo',
          'nobatch',
          'datetime_start',
          'nama_operator',
          'nama_line',
      ],
  ]) ?>
  <br>
  <p style="font-size: 20px; text-align: center; font-weight: bold;">Mohon di stop terlebih dahulu jadwal tersebut atau hubungi Leader</p>
<p style="font-size: 20px;">Notes : Ada kemungkinan line lain sedang jalan menggunakan nama line ini <?php echo $model->nama_line; ?> karena salah pilih. Nama line di-lock untuk memastikan hanya akan ada 1 jadwal yang berjalan di suatu line.</p>

  <div class="modal-footer">
    <!-- <button id="no" type="button" class="btn btn-warning pull-left" data-dismiss="modal">Tidak</button> -->
    <button id="yes" type="button" class="btn btn-success" data-dismiss="modal">Oke</button>
  </div>
</div>