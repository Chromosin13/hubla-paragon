<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowAnalyst;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<?php

$this->registerJsFile('js/instascan.min.js', ['position' => \yii\web\View::POS_HEAD, 'type' => 'text/javascript']);

?>


                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-blue-gradient">
                      <h3 class="widget-user-username"><b>QC Bulk Entry</b></h3>
                      <h5 class="widget-user-desc">QR Code</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">
                                <?php
                                      echo '<div class="col-sm-12">';
                                      echo  '<video id="preview"></video>';
                                      echo '<script type="text/javascript">';
                                      echo "   let scanner = new Instascan.Scanner({ video: document.getElementById('preview'),
                                          mirror: false // prevents the video to be mirrored
                                          });
                                          scanner.addListener('scan', function (content) {

                                            // alert(content);
                                            $.get('index.php?r=scm-planner/check-nomo',{ nomo : content },function(data){
                                                var data = $.parseJSON(data);
                                                if(data.id>=1){

                                                  location.href='http://factory.pti-cosmetics.com/flowreport/web/index.php?r=qc-bulk-entry/create-rincian&nomo='+content;

                                                }

                                                else{

                                                  alert('Nomor SNFGKomponen Tidak Ditemukan di Database; Silahkan mengontak Planner/Leader');
                                                }

                                            });


                                             // window.location = 'index.php?r=flow-analyst/create-flow-analyst&kode_unik_formula='+content;
                                          });
                                          Instascan.Camera.getCameras().then(function (cameras) {
                                            if (cameras.length > 0) {
                                              // scanner.start(cameras[0]);
                                              if(cameras[1]){ scanner.start(cameras[1]); } else { scanner.start(cameras[0]); }
                                            } else {
                                              console.error('No cameras found.');
                                            }
                                          }).catch(function (e) {
                                            console.error(e);
                                          });
                                        </script>
                                      </div>";
                                ?>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>


                  <!-- List Data -->




<?php
$script = <<< JS


JS;
$this->registerJs($script);
?>
