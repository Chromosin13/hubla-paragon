<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\QcBulkEntry */

// $this->title = 'Create Qc Bulk Entry';
// $this->params['breadcrumbs'][] = ['label' => 'Qc Bulk Entries', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="qc-bulk-entry-create">

  
    <?= $this->render('_form-rincian', [
        'model' => $model,
        'nomo' => $nomo,
        'nama_fg' => $nama_fg,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'nourut' => $nourut,
    ]) ?>

</div>
