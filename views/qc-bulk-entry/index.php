<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QcBulkEntrySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Qc Bulk Entries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qc-bulk-entry-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Qc Bulk Entry', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nourut',
            'nomo',
            'status',
            'timestamp',
            // 'nama_inspektor',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
