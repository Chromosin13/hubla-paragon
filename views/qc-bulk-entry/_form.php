<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\ActiveField;
use kartik\dialog\Dialog;
use kartik\checkbox\CheckboxX;
/* @var $this yii\web\View */
/* @var $model app\models\QcBulkEntry */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qc-bulk-entry-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomo')->textInput() ?>

    <div id="create-form" class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <div id="baseurl" value="<?php echo Yii::$app->homeUrl; ?>">

</div>

<?php
$script = <<< JS

// Hide Create new Field

    $('#create-form').hide();
// Get Base Url
var baseurl = $('#baseurl').val();


$('#qcbulkentry-nomo').change(function(){  

    var nomo = $('#qcbulkentry-nomo').val().trim();
    
    if(nomo){
      window.location = "index.php?r=qc-bulk-entry/create-rincian&nomo="+nomo;
    }else{
      alert('Nomor Jadwal kosong!');
    }
    
});


// $('#qcbulkentry-nomo').change(function(){  

//     var nomo = $('#qcbulkentry-nomo').val();


//     $.get('index.php?r=qc-bulk-entry/check-nomo',{ nomo : nomo },function(data){
//         var data = $.parseJSON(data);
//         // alert(data);
//         // if(data==="null"){
//         //   $('#create-form').hide();
//         //     alert("TIDAK ADA JADWAL DENGAN NOMOR MO "+data.nomo+" PADA DATABASE PLANNER");
//         // }
//         if(data.id==1){

//           alert('Data Valid, belum dilakukan QC, silahkan tekan tombol CREATE untuk inisiasi QC');
          
//           $('#create-form').show();
           
//         }

//         else if(data.id==2){

//           alert('Data Valid, Proses QC sudah berjalan, halaman akan diteruskan');
//           var nomo = $('#qcbulkentry-nomo').val();
//           // window.location = baseurl+"?r=qc-bulk-entry/create-rincian&nomo="+nomo;
//           window.location = "index.php?r=qc-bulk-entry/create-rincian&nomo="+nomo;
           
//         }
    
//     });
        
// });


JS;
$this->registerJs($script);
?>
