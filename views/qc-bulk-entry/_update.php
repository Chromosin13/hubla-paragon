<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\ActiveField;
use kartik\dialog\Dialog;
use kartik\checkbox\CheckboxX;
/* @var $this yii\web\View */
/* @var $model app\models\QcBulkEntry */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qc-bulk-entry-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomo')->textInput(['readOnly'=>true]) ?>

    <?= $form->field($model, 'nourut')->textInput(['readOnly'=>true]) ?>

    <?= $form->field($model, 'jenis_adjust')->dropDownList(['ADJUST WARNA' => 'ADJUST WARNA',
                                                            'ADJUST MIXING' => 'ADJUST MIXING',
                                                            'ADJUST GILING' => 'ADJUST GILING',
                                                            'ADJUST SARING' => 'ADJUST SARING',
                                                            'ADJUST AYAK' => 'ADJUST AYAK'],['prompt'=>'Select Option']); ?>

    <?= $form->field($model, 'keterangan_adjust')->textInput() ?>

    <div id="create-form" class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <div id="baseurl" value="<?php echo Yii::$app->homeUrl; ?>">

</div>

<?php
$script = <<< JS


JS;
$this->registerJs($script);
?>
