<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\QcBulkEntry */

$this->title = 'Update Qc Bulk Entry: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Qc Bulk Entries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="qc-bulk-entry-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_update', [
        'model' => $model,
    ]) ?>

</div>
