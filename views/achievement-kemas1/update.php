<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementKemas1 */

$this->title = 'Update Achievement Kemas1: ' . $model->row_number;
$this->params['breadcrumbs'][] = ['label' => 'Achievement Kemas1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->row_number, 'url' => ['view', 'id' => $model->row_number]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="achievement-kemas1-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
