<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AchievementKemas1 */

$this->title = 'Create Achievement Kemas1';
$this->params['breadcrumbs'][] = ['label' => 'Achievement Kemas1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-kemas1-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
