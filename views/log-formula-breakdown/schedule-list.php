<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use app\models\LogFormulaBreakdown;
use app\models\LogApprovalBr;

/* @var $this yii\web\View */
/* @var $model app\models\LogFormulaBreakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
.tableFixHead          { overflow-y: auto; height: 81vh; z-index:5;}
.tableFixHead thead th { position: sticky; top: -1px; z-index:5;}

.table-bordered{
  border-left:1.5px solid rgba(135,206,250,.25);
  border-bottom:1.5px solid rgba(135,206,250,.25);
  border-top:1px solid rgba(135,206,250,.25);
}
.table-bordered td,.table-bordered th{
  border:1px solid rgba(135,206,250,.25)
}
.table-bordered thead td,.table-bordered thead th{
  border-bottom-width:2px
}

.table-striped tbody tr:nth-of-type(odd){
  background-color:rgba(135,206,250,.25)
}

.btn-info{color:#fff;background-color:#5DADE2;border-color:#5DADE2}.btn-info:hover{color:#fff;background-color:#138496;border-color:#117a8b}.btn-info.focus,.btn-info:focus{color:#fff;background-color:#138496;border-color:#117a8b;box-shadow:0 0 0 .2rem rgba(58,176,195,.5)}.btn-info.disabled,.btn-info:disabled{color:#fff;background-color:#17a2b8;border-color:#17a2b8}.btn-info:not(:disabled):not(.disabled).active,.btn-info:not(:disabled):not(.disabled):active,.show>.btn-info.dropdown-toggle{color:#fff;background-color:#117a8b;border-color:#10707f}.btn-info:not(:disabled):not(.disabled).active:focus,.btn-info:not(:disabled):not(.disabled):active:focus,.show>.btn-info.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(58,176,195,.5)}

.btn-add:hover {
  box-shadow: 0 5px 9px 0 rgba(0,0,0,0.24), 0 5px 9px 0 rgba(0,0,0,0.19);
}

.btn-minus:hover {
  box-shadow: 0 5px 9px 0 rgba(0,0,0,0.24), 0 5px 9px 0 rgba(0,0,0,0.19);
}

table tbody tr{
    height: 1em;
}
.img-circle{
  border-radius:50%;
}

#sediaan {
    /*background: rgba(135, 206, 250, 0.25);*/
    background-color: #DDD;
    border: 3px solid white;
    /*color: #3C8DBC;*/
    padding: 1vh;
    border-radius: 8px;
    font-weight: bold;
    color: black;
}
</style>

<?php

  // Modal::begin([
  //         'header'=>'<h4>Review BR</h4>',
  //         'id' => 'modalverifieditems',
  //         'size' => 'modal-lg',
  //     ]);

  // echo "<div id='modalVerifiedItems'></div>";

  // Modal::end();

?>

<div class="box widget-user" style="display: inline-block;">
    <div>
        <h1 class="text-center" style="margin-top:0px; padding-top :0.8em; padding-bottom :0.3em;">JADWAL PRODUKSI WEEK <?php echo $week-3; ?>-<?php echo $week+1; ?></h1>
    </div>


    <div style="width:96%; margin: 0 auto;">
      <div class="row center" style="text-align:center;">
       
          <span>
            <select class="" name="sediaan" id="sediaan" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
              <?php if ($sediaan == '') : ?>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list" style="" selected="selected">All Sediaan</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=L">Liquid</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=P">Powder</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=S">Semisolid</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=V">Varcos</option>
              <?php elseif ($sediaan == 'L'): ?>
               <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list" style="" >All Sediaan</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=L" selected="selected">Liquid</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=P">Powder</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=S">Semisolid</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=V">Varcos</option>
              <?php elseif ($sediaan == 'P'): ?>
               <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list" style="" >All Sediaan</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=L" >Liquid</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=P" selected="selected">Powder</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=S">Semisolid</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=V">Varcos</option>
              <?php elseif ($sediaan == 'S'): ?>
               <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list" style="" >All Sediaan</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=L" >Liquid</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=P">Powder</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=S" selected="selected">Semisolid</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=V">Varcos</option>
              <?php else: ?>
               <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list" style="" >All Sediaan</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=L" >Liquid</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=P">Powder</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=S">Semisolid</option>
              <option value="<?php echo Yii::getAlias("@ipUrl"); ?>log-formula-breakdown/schedule-list&sediaan=V" selected="selected">Varcos</option>
              <?php endif; ?>
            </select>
          </span>                
      </div>


    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => [
          ['class' => 'yii\grid\SerialColumn'],

          'nomo',
          [
                'attribute' => 'nama_fg',
                'contentOptions' => function ($dataProvider) {
                    return ['style' => 'text-align:left;width:20%'];
                },
          ],
          [
                'attribute' => 'qty_batch',
                'contentOptions' => function ($dataProvider) {
                    return ['style' => 'text-align:center;width:5%'];
                },
          ],
          [
                'attribute' => 'scheduled_start',
                'contentOptions' => function ($dataProvider) {
                    return ['style' => 'text-align:center;width:5%'];
                },
          ],
          [
                'attribute' => 'formula_reference',
                'contentOptions' => function ($dataProvider) {
                    return ['style' => 'text-align:center;width:5%'];
                },
          ],
          [
                'attribute' => 'nama_line',
                'contentOptions' => function ($dataProvider) {
                    return ['style' => 'text-align:center;width:5%'];
                },
          ],
          [
                'attribute' => 'week',
                'contentOptions' => function ($dataProvider) {
                    return ['style' => 'text-align:center;width:5%'];
                },
          ],
          // [
          //       'attribute' => 'lokasi',
          //       'contentOptions' => function ($dataProvider) {
          //           return ['style' => 'text-align:center;width:8%'];
          //       },
          // ],
          [
                // 'class' => 'kartik\grid\EditableColumn',
                // 'attribute' => 'lokasi',
                // 'format'=> ['decimal'=>2],
                'value' => function($dataProvider, $key, $index) {
					$log_formula_breakdown = new LogFormulaBreakdown();
                    return $log_formula_breakdown->get_current_pic($dataProvider->nomo);
                },
                'headerOptions' => ['style' => 'text-align:center; width : 120px;'],
                'label' => 'Current PIC',
                // 'editableOptions' => [
                //   // 'asPopover' => false,
                //   'submitOnEnter' => true,
                // ],

          ],
          [
                // 'class' => 'kartik\grid\EditableColumn',
                // 'attribute' => 'lokasi',
                // 'format'=> ['decimal'=>2],
                'value' => function($dataProvider, $key, $index) {
					$log_formula_breakdown = new LogFormulaBreakdown();
                    return $log_formula_breakdown->get_current_status($dataProvider->nomo);
                },
                'headerOptions' => ['style' => 'text-align:center; width : 120px;'],
                'label' => 'Current Status',
                // 'editableOptions' => [
                //   // 'asPopover' => false,
                //   'submitOnEnter' => true,
                // ],

          ],
          [
                'attribute' => 'status',
                'value' => function ($dataProvider){
                    return $dataProvider->status == null ? 'unverified':$dataProvider->status;
                },
                'contentOptions' => function ($dataProvider) {
                    return ['style' => 'text-align:center;width:8%;color:'
                        . (in_array($dataProvider->status, ['unverified',null]) ? '#d54354' : 'green')];
                },
          ],
          // [
          //   'format' => 'raw',
          //   'header' => '<a style="color:#469ED8;">Input New / Edit</a>',
          //   // 'label' => [function(){return Html::a('','',['style'=>['color'=>'#5DADE2']]);}],
          //   'value' => function($dataProvider) {
          //               return Html::a(
          //                       '<i class="fa fa-pencil"></i>',
          //                       Url::to(['log-formula-breakdown/input-detail-br2', 'nomo' => $dataProvider->nomo,'reference' => $dataProvider->formula_reference]),
          //                       [
          //                         'id'=>'grid-custom-button',
          //                         'data-pjax'=>true,
          //                         // 'action'=>Url::to(['log-formula-breakdown/input-detail-br2', 'nomo' => $dataProvider->nomo,'reference' => $dataProvider->formula_reference]),
          //                         'class'=>'button btn btn-default',
          //                         'style' => ['background' => '#D3D3D3', 'opacity' => '0.75', 'border-radius' => '0.7em', 'vertical-align'=>'middle','display'=>'block','margin'=>'0 auto', 'height'=>'100%', 'width'=>'90%']
          //                       ]);
          //               }
          // ],
          [
            'format' => 'raw',
            'header' => '<a style="color:#469ED8;">Input New/ Edit</a>',
            // 'label' => [function(){return Html::a('','',['style'=>['color'=>'#5DADE2']]);}],
            'value' => function($dataProvider) {
                        return Html::button('',['class'=>'button btn btn-info fa fa-pencil',
                                  'style' => ['background' => '#D3D3D3', 'color' => 'black', 'opacity' => '0.75', 'border-radius' => '0.7em', 'vertical-align'=>'middle','display'=>'block','margin'=>'0 auto', 'height'=>'100%', 'width'=>'80%'],
                                  'id' => 'edit-'.$dataProvider->nomo.'-'.$dataProvider->formula_reference,
                                  'qty_batch'=>$dataProvider->qty_batch
                                ]);
                        },
          ],
          [
            'format' => 'raw',
            'header' => '<a style="color:#469ED8;">Edit V2</a>',
            // 'label' => [function(){return Html::a('','',['style'=>['color'=>'#5DADE2']]);}],
            'value' => function($dataProvider) {
                        return Html::button('',['class'=>'button btn btn-info fa fa-pencil',
                                  'style' => ['background' => '#D3D3D3', 'color' => 'black', 'opacity' => '0.75', 'border-radius' => '0.7em', 'vertical-align'=>'middle','display'=>'block','margin'=>'0 auto', 'height'=>'100%', 'width'=>'80%'],
                                  'id' => 'edit2-'.$dataProvider->nomo.'-'.$dataProvider->formula_reference,
                                  'qty_batch'=>$dataProvider->qty_batch
                                ]);
                        },
          ],
          [
            'format' => 'raw',
            'header' => '<a style="color:#469ED8;">Copy from</a>',
            'value' => function ($dataProvider) {
                      return Html::textInput('Reference', '', ['class' => 'form-control', 'id' => 'reference-'.$dataProvider->nomo]);
            },
            'contentOptions' => function ($dataProvider) {
                    return ['style' => 'text-align:center;width:5%'];
                },
          ],
          [
            'format' => 'raw',
            'header' => '<a style="color:#469ED8;"></a>',
            'value' => function($dataProvider) {
                        return Html::a('',['log-penimbangan-rm/list-verified-items','nomo'=>$dataProvider->nomo],[
                                  'class'=>'button btn btn-info fa fa-file-text',
                                  'style' => ['vertical-align'=>'middle'],
                                  'id' => 'view-items',
                                  // 'onclick'=>"modalVerifiedItems('".$dataProvider->nomo."')"
                                ]);
                        },
          ],

          // [
          //   'format' => 'raw',
          //   'header' => '<a style="color:#469ED8;">Sync</a>',
          //   // 'label' => [function(){return Html::a('','',['style'=>['color'=>'#5DADE2']]);}],
          //   'value' => function($dataProvider) {
          //               return Html::button('',['class'=>'button btn btn-info fa fa-refresh',
          //                         'style' => ['opacity' => '0.75', 'border-radius' => '0.7em', 'vertical-align'=>'middle','display'=>'block','margin'=>'0 auto', 'height'=>'100%', 'width'=>'90%'],
          //                         'id' => 'id-'.$dataProvider->nomo
          //                       ]);
          //               }
          // ],
          [
            'format' => 'raw',
            'header' => '<a style="color:#469ED8;">Delete</a>',
            // 'label' => [function(){return Html::a('','',['style'=>['color'=>'#5DADE2']]);}],
            'value' => function($dataProvider) {
                        return Html::a('<span class="button btn btn-danger fa fa-trash"></span>',
                                    ['delete-detail-br', 'nomo' => $dataProvider->nomo,'qty_batch'=>$dataProvider->qty_batch,'reference'=>$dataProvider->formula_reference],
                                    [
                                      'style' => ['opacity' => '0.75', 'border-radius' => '0.7em', 'vertical-align'=>'middle','display'=>'block','margin'=>'0 auto', 'height'=>'100%', 'width'=>'90%'],
                                      // 'id' => 'id-'.$dataProvider->nomo,
                                      'data-confirm' => Yii::t('app', 'Apakah Anda yakin akan mendelete data? Data akan diganti dengan record terbaru dari Odoo'),
                                      'method'=>'post',
                                ]);
                        }
          ],
      ],
    ]); ?>
  </div>
</div>

<script>
  function modalVerifiedItems(nomo){
    // console.log(nomo);
    url = '<?php echo Url::to("index.php?r=log-penimbangan-rm/list-verified-items&nomo=");?>'+nomo;

    // console.log (url);

    // $('#modalverifieditems').modal('show')
    //   // find id
    //   .find('#modalVerifiedItems')
    //   // load id
    //   .load(url);
  }
</script>

<?php
$script = <<< JS

    $('button[id^="id-"]').on("click",function() {
      var id = $(this).attr('id');
      //var nilai = $(this).val();

      nomo = id.substring(id.indexOf("-") + 1);
      //console.log(nomo);

      $.get("index.php?r=log-formula-breakdown/sync-data&nomo="+nomo, function (data){
        //console.log(data);
        if (data==0) {
          window.alert('Gagal melakukan sinkronisasi');
        } else if (data==1){
          window.alert('Sinkronisasi Berhasil');
          location.reload();
        } else {
          window.alert('Data Sudah sama dengan yang ada di Odoo.');
        }
      });
    });

    $('button[id^="edit-"]').on("click",function() {
      var id = $(this).attr('id');
      var qty_batch = $(this).attr('qty_batch');
      //var nilai = $(this).val();

      nomo = id.substring(id.indexOf("-") + 1,id.lastIndexOf("-"));
      kode_bulk = nomo.substring(nomo.indexOf("-") + 1,nomo.lastIndexOf("/"));
      reference = id.substring(id.lastIndexOf("-") + 1);
      // console.log(nomo);
      // console.log('current reference '+reference);
      // console.log(kode_bulk);
      id_ref = 'reference-'+nomo;

      reference_copy = document.getElementById(id_ref).value;
      // console.log('reference copy '+reference_copy);
      // console.log(qty_batch);

      if (reference_copy != '' && reference_copy != reference){
        $.get("index.php?r=log-formula-breakdown/copy-reference&kode_bulk="+kode_bulk+"&reference="+reference+"&reference_copy="+reference_copy+"&qty_batch="+qty_batch, function (data){
          // console.log(reference_copy);
          if (data==2){
            // window.alert('Proses Copy Gagal! Reference & Qty Batch ini sudah ada datanya di FRO, tidak dapat menimpa. Minta tolong admin IT untuk menghapus terlebih dahulu.');
            window.alert('Proses Copy Gagal! Reference dan Qty Batch Copy tidak ada di FRO, tidak dapat melakukan copy data!');
            // window.location = "index.php?r=log-formula-breakdown/input-detail-br2&nomo="+nomo+"&reference="+reference+"&reference_copy="+reference_copy+"&qty_batch="+qty_batch;
          } else if (data==1) {
            window.alert('Proses Copy Berhasil!');
            window.location = "index.php?r=log-formula-breakdown/input-detail-br2&nomo="+nomo+"&reference="+reference+"&reference_copy="+reference_copy+"&qty_batch="+qty_batch;
          } else {
            window.alert('Proses Copy Gagal!');
            // window.alert('Reference ini belum ada di FRO, tidak dapat melakukan copy data!');
          }
        });
      } else {
        $.get("index.php?r=log-formula-breakdown/check-data&nomo="+nomo, function (data){
          //console.log(data);
          if (data==0) {
            window.alert('Data belum sama dengan di odoo, silahkan tekan tombol sync!');
          } else if (data==1){
            var valueNull = '';
            window.location = "index.php?r=log-formula-breakdown/input-detail-br2&nomo="+nomo+"&reference="+reference+"&reference_copy="+valueNull+"&qty_batch="+qty_batch;
          }
        });
      }

      //window.alert(new_ref);
    });

    $('button[id^="edit2-"]').on("click",function() {
      var id = $(this).attr('id');
      var qty_batch = $(this).attr('qty_batch');
      //var nilai = $(this).val();

      nomo = id.substring(id.indexOf("-") + 1,id.lastIndexOf("-"));
      kode_bulk = nomo.substring(nomo.indexOf("-") + 1,nomo.lastIndexOf("/"));
      reference = id.substring(id.lastIndexOf("-") + 1);
      // console.log(nomo);
      // console.log('current reference '+reference);
      // console.log(kode_bulk);
      id_ref = 'reference-'+nomo;

      reference_copy = document.getElementById(id_ref).value;
      // console.log('reference copy '+reference_copy);
      // console.log(qty_batch);

      if (reference_copy != '' && reference_copy != reference){
        $.get("index.php?r=log-formula-breakdown/copy-reference&kode_bulk="+kode_bulk+"&reference="+reference+"&reference_copy="+reference_copy+"&qty_batch="+qty_batch, function (data){
          console.log(data);
          if (data==2){
            // window.alert('Proses Copy Gagal! Reference & Qty Batch ini sudah ada datanya di FRO, tidak dapat menimpa. Minta tolong admin IT untuk menghapus terlebih dahulu.');
            window.alert('Proses Copy Gagal! Reference dan Qty Batch Copy tidak ada di FRO, tidak dapat melakukan copy data!');
            // window.location = "index.php?r=log-formula-breakdown/form-batch-record&nomo="+nomo+"&reference="+reference+"&reference_copy="+reference_copy+"&qty_batch="+qty_batch+"&pos=";
            // window.location = "index.php?r=log-formula-breakdown/input-detail-br2&nomo="+nomo+"&reference="+reference+"&reference_copy="+reference_copy+"&qty_batch="+qty_batch;
          } else if (data==1) {
            window.alert('Proses Copy Berhasil!');
            window.location = "index.php?r=log-formula-breakdown/form-batch-record&nomo="+nomo+"&reference="+reference+"&reference_copy="+reference_copy+"&qty_batch="+qty_batch+"&pos=";
            // window.location = "index.php?r=log-formula-breakdown/input-detail-br2&nomo="+nomo+"&reference="+reference+"&reference_copy="+reference_copy+"&qty_batch="+qty_batch;
          } else {
            window.alert('Proses Copy Gagal!');
            // window.alert('Reference ini belum ada di FRO, tidak dapat melakukan copy data!');
          }
        });
      } else {
        $.get("index.php?r=log-formula-breakdown/check-data&nomo="+nomo, function (data){
          //console.log(data);
          if (data==0) {
            window.alert('Data belum sama dengan di odoo, silahkan tekan tombol sync!');
          } else if (data==1){
            var valueNull = '';
            window.location = "index.php?r=log-formula-breakdown/form-batch-record&nomo="+nomo+"&reference="+reference+"&reference_copy="+valueNull+"&qty_batch="+qty_batch+"&pos=";
            // window.location = "index.php?r=log-formula-breakdown/input-detail-br2&nomo="+nomo+"&reference="+reference+"&reference_copy="+valueNull+"&qty_batch="+qty_batch;
          }
        });
      }

      // $.get("index.php?r=log-formula-breakdown/check-version&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
      //   if (data == 0 || data == 1){
      //     window.location = "index.php?r=log-formula-breakdown/form-batch-record&nomo="+nomo+"&reference="+reference+"&qty_batch="+qty_batch;
      //     } else if (data == 2){
      //       alert("Edit formula ini menggunakan versi lama!");
      //     }
      // });
    });



JS;
$this->registerJs($script);
?>
