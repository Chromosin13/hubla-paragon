<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LogFormulaBreakdown */

$this->title = 'Update Log Formula Breakdown: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Log Formula Breakdowns', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="log-formula-breakdown-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
