<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\FlowInputMo;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\bootstrap\Modal;

?>

<style>
/*.tableFixHead          { overflow-y: auto; height: 80vh; z-index:5;}
.tableFixHead thead th { position: sticky; top: -1px; z-index:5;}*/

.table-bordered{
  border-left:1.5px solid rgba(135,206,250,.25);
  border-bottom:1.5px solid rgba(135,206,250,.25);
  border-top:1px solid rgba(135,206,250,.25);
}
.table-bordered td,.table-bordered th{
  border:1px solid rgba(135,206,250,.25)
}
.table-bordered thead td,.table-bordered thead th{
  border-bottom-width:2px
}
.table-striped tbody tr:nth-of-type(odd){
  background-color:rgba(135,206,250,.25)
}

.btn-info{
  color:#fff;
  background-color:#5DADE2;
  border-color:#5DADE2
}
.btn-info:hover{
  color:#fff;
  background-color:#138496;
  border-color:#117a8b
}
.btn-info.focus,.btn-info:focus{
  color:#fff;
  background-color:#138496;
  border-color:#117a8b;
  box-shadow:0 0 0 .2rem rgba(58,176,195,.5)
}
.btn-info.disabled,.btn-info:disabled{
  color:#fff;
  background-color:#17a2b8;
  border-color:#17a2b8
}
.btn-info:not(:disabled):not(.disabled).active,.btn-info:not(:disabled):not(.disabled):active,.show>.btn-info.dropdown-toggle{
  color:#fff;
  background-color:#117a8b;
  border-color:#10707f
}
.btn-info:not(:disabled):not(.disabled).active:focus,.btn-info:not(:disabled):not(.disabled):active:focus,.show>.btn-info.dropdown-toggle:focus{
  box-shadow:0 0 0 .2rem rgba(58,176,195,.5)
}

.btn-add:hover {
  box-shadow: 0 5px 9px 0 rgba(0,0,0,0.24), 0 5px 9px 0 rgba(0,0,0,0.19);
}

.btn-minus:hover {
  box-shadow: 0 5px 9px 0 rgba(0,0,0,0.24), 0 5px 9px 0 rgba(0,0,0,0.19);
}

/* PENAMBAHAN SUFFIX "kg" */
.div2 {
  display: inline-block;
  position: relative;
}
.div2::after {
  position: absolute;
  top: 2px;
  right: .5em;
  transition: all .05s ease-in-out;
}
.div2.kg::after {
  content: 'kg';
  margin-top:5px;
}
</style>

<?php

  Modal::begin([
          'header'=>'<h4>Preview</h4>',
          'id' => 'modalverifieditems',
          'size' => 'modal-lg',
      ]);

  echo "<div id='modalVerifiedItems'></div>";

  Modal::end();

?>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">
  var bom =  <?php echo json_encode($bom); ?>;
  var nomo =  <?php echo json_encode($nomo); ?>;
  var timbangans =  <?php echo json_encode($timbangans); ?>;
  var actions =  <?php echo json_encode($actions); ?>;
  var qty_batch =  <?php echo $qty_batch; ?>;
  var pic =  <?php echo json_encode($pic); ?>;
  var kode_bulk = bom[0]['kode_bulk'];
  var reference = bom[0]['formula_reference'];
  var sumFinal = 0.0;
  var idNomo = bom[0]['id'];
  var array = "";
  var array_kg = "";
  var array_g = "";
  var arrayAction = "";

  for (i=0;i<timbangans.length;i++){
    if (timbangans[i]['uom']=='kg'){
      array_kg += "<option value='"+timbangans[i]["kode_timbangan"]+"'>"+timbangans[i]["kode_timbangan"]+"</option>";
    }else if (timbangans[i]['uom']=='g'){
      array_g += "<option value='"+timbangans[i]["kode_timbangan"]+"'>"+timbangans[i]["kode_timbangan"]+"</option>";
    }else{
      array += "<option value='"+timbangans[i]["kode_timbangan"]+"'>"+timbangans[i]["kode_timbangan"]+"</option>";
    }
  }

  for (i=0;i<actions.length;i++){
    arrayAction += "<option value='"+actions[i]["kode"]+"'>"+actions[i]["kode"]+"</option>";
  }

  function deleteRow(x,y)
  {
    var row = document.getElementById(x);
    row.parentNode.removeChild(row);

    id = x.substring(x.lastIndexOf("-") + 1);
    idSiklus = x.substring(x.indexOf("-") + 1, x.lastIndexOf("-"));

    $.getJSON("index.php?r=log-formula-breakdown/minus-split-weighing",{id:y},function(result){
      //console.log("a");
    });

    $.post("index.php?r=log-formula-breakdown/delete-split-qty&id_split="+id, function (data){
      $.post("index.php?r=log-formula-breakdown/get-standard-qty&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data2)
      {
        standard = Number(data2).toFixed(4);
        $.post("index.php?r=log-formula-breakdown/get-sum&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data4){

          sum_ = Number(data4).toFixed(4);
          if (parseFloat(sum_)>(parseFloat(standard)+0.005))
          {
            $('input[id="sum"]').val(sum_);
            $('input[id="sum"]').css('box-shadow', '0px 0px 2px 2px #ff6666');
          } else if ((parseFloat(standard)-0.005).toFixed(4)<=parseFloat(sum_) && parseFloat(sum_)<=(parseFloat(standard)+0.005)){
            $('input[id="sum"]').val(sum_);
            $('input[id="sum"]').css('box-shadow', '0px 0px 2px 2px #3CB371');
          } else {
            $('input[id="sum"]').val(sum_);
            $('input[id="sum"]').css('box-shadow', '0px 0px 2px 2px #ffff00');
          }
        });
      });

      $.getJSON("index.php?r=log-formula-breakdown/get-split",{id:idSiklus},function(result){
        if (result==0){
          document.getElementById("siklus-"+idSiklus+"-0").disabled = false;
          document.getElementById("kode_olah-"+idSiklus+"-0").disabled = false;
          document.getElementById("operator-"+idSiklus+"-0").disabled = false;
          document.getElementById("timbangan-"+idSiklus+"-0").disabled = false;
          //document.getElementById("keterangan-"+idSiklus+"-0").disabled = false;
        }
      });
    });
  }

  function clickDispatcherTable(x,y) {

    $.getJSON("index.php?r=log-formula-breakdown/add-split-weighing",{id:y},function(result){

      if (result){
        var bom =  <?php echo json_encode($bom); ?>;

        $.post("index.php?r=log-formula-breakdown/insert-split-qty&id="+bom[x-1]['id']+"&qty=0.00000", function (data){
          data = JSON.parse(data);
          id = data.status;
          var kode_bb = bom[x-1]['kode_bb'];
          var nama_bb = bom[x-1]['nama_bb'];
          var uom = bom[x-1]['uom'];
          var reference = bom[x-1]['formula_reference'];
          var location = bom[x-1]['lokasi'];
          var siklus = bom[x-1]['siklus'];
          let plusButtonParentId = $('#add-'+bom[x-1]['id']).parent().parent().attr('id');
          var plusButtonParent = document.getElementById(plusButtonParentId);

          $.post("index.php?r=log-formula-breakdown/get-standard-qty&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data2)
          {
            standard = Number(data2).toFixed(4);
            $.post("index.php?r=log-formula-breakdown/get-sum&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data4){

              sum_ = Number(data4).toFixed(4);
              if (parseFloat(sum_)>(parseFloat(standard)+0.005))
              {
                $('input[id="sum"]').val(sum_);
                $('input[id="sum"]').css('box-shadow', '0px 0px 2px 2px #ff6666');
              } else if ((parseFloat(standard)-0.005).toFixed(4)<=parseFloat(sum_) && parseFloat(sum_)<=(parseFloat(standard)+0.005)){
                //document.getElementById("sum").style.boxShadow = "";
                $('input[id="sum"]').val(sum_);
                $('input[id="sum"]').css('box-shadow', '0px 0px 2px 2px #3CB371');
              } else {
                $('input[id="sum"]').val(sum_);
                $('input[id="sum"]').css('box-shadow', '0px 0px 2px 2px #ffff00');
              }
            });
          });

          document.getElementById("siklus-"+bom[x-1]['id']+"-0").disabled = true;
          document.getElementById("operator-"+bom[x-1]['id']+"-0").disabled = true;
          document.getElementById("timbangan-"+bom[x-1]['id']+"-0").disabled = true;
          document.getElementById("kode_olah-"+bom[x-1]['id']+"-0").disabled = true;
          //document.getElementById("keterangan-"+bom[x-1]['id']+"-0").disabled = true;
          // $.getJSON("index.php?r=log-formula-breakdown/get-split",{id:y},function(data){

          plusButtonParent.insertAdjacentHTML('afterend', `
          <tr id="row-`+bom[x-1]['id']+`-`+id+`";>
            <td class="text-left" style="font-size:1em;"> </td>
            <td class="text-left" style="font-size:1em;"></td>
            <td class="text-left" style="font-size:1em;"></td>
            <td class="text-left" style="font-size:1em;vertical-align: middle;">`+nama_bb+`</td>
            <td style="vertical-align: middle;"><input type="numeric" style="font-size:1em; display:block; margin:0 auto; width:100%; vertical-align: middle; padding-left:5px;" value="0.00000" id="qty-`+bom[x-1]['id']+`-`+id+`">
                 </input>
            </td>
            <td class="text-center" style="font-size:1em; vertical-align: middle;">
              <select id="uom-`+bom[x-1]['id']+`-`+id+`" style="height:2em; padding-top:3px; text-align:center; width: 90%; display:block; margin:0 auto;" class="form-control form-control-sm text-center">
                  <option value="kg">kg</option>
                  <option value="g">g</option>
              </select>
            </td>

            <td class="text-center" style="font-size:1em; vertical-align: middle;">
              <select id="siklus-`+bom[x-1]['id']+`-`+id+`" style="height:2em; text-align:center; width: 90%; display:block; margin:0 auto;" class="form-control form-control-sm text-center">
                  <option value="0">Pilih Siklus</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
              </select>
            </td>

            <td class="text-center" style="font-size:1em;vertical-align: middle;">
              <input type="text" style="font-size:1em; text-align: center; display:block; margin:0 auto; width:100%; padding-left:5px;" id="kode_olah-`+bom[x-1]['id']+`-`+id+`">
              </input>
            </td>

            <td class="text-left" style="font-size:1em;vertical-align: middle; margin:0 auto;">
            <select id='action-`+bom[x-1]['id']+`-`+id+`' style='height:2em; text-align:center; width: 90%; display:block; margin:0 auto;' class='form-control form-control-sm text-center' disabled>
              <option value='Action' disabled>Action</option>
            `+arrayAction+`
            </select></td>

            <td class="text-center" style="font-size:1em; vertical-align: middle;">
              <textarea type="text" placeholder="jika repack tulis REPACK" style="font-size:1em; display:block; margin:0 auto; width:100%; padding-left:5px;" id="keterangan-`+bom[x-1]['id']+`-`+id+`" >
              </textarea>
            </td>

            <td class="text-center" style="font-size:1em; vertical-align: middle;">
              <select style='height:2em; text-align:left; width: 90%; display:block; margin:0 auto;' class='form-control form-control-sm text-center' id="operator-`+bom[x-1]['id']+`-`+id+`">
                <option disabled selected value='0'>Pilih Operator</option>
                <option value='1'>1</option>
                <option value='2'>2</option>
                <option value='3'>3</option>
                <option value='4'>4</option>
              </select>
            </td>
            <td class="text-left" style="font-size:1em;vertical-align: middle; margin:0 auto;">
            <select id='timbangan-`+bom[x-1]['id']+`-`+id+`' style='height:2em; text-align:center; width: 90%; display:block; margin:0 auto;' class='form-control form-control-sm text-center'>
              <option value='Timbangan' disabled selected>Timbangan</option>`+(bom[x-1]['uom']='kg'?array_kg:array_g)+`</select></td>
            <td class="text-left" style="font-size:1em; vertical-align: middle;">
              <button type="button" onclick="deleteRow('row-`+bom[x-1]['id']+`-`+id+`',`+ bom[x-1]['id']+`)" class="btn btn-dark btn-sm product2 btn-minus" style="display:block; margin: 0 auto; background:#FF4C4C; width:75%; opacity:0.85; border-radius:0.7em;" id="minus-`+bom[x-1]['id']+`"><i class="fa fa-minus"></i></button>
            </td>
          </tr>`);
          // });
        });

        idKodeOlah = "kode_olah-"+bom[x-1]['id']+"-0";

        //console.log(value_action);
        // $.post("index.php?r=log-formula-breakdown/update-kode-olah&id="+idItem+"&value="+nilai +"&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
        $.post("index.php?r=log-formula-breakdown/update-kode-olah&id="+bom[x-1]['id']+"&value=" +"&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
          $('input[id^='+idKodeOlah+']').val("");
        });

        $.getJSON("index.php?r=log-formula-breakdown/get-detail&id="+bom[x-1]['id'], function (data){
          var idSiklus = "siklus-"+bom[x-1]['id']+"-"+id;
          var idAction = "action-"+bom[x-1]['id']+"-"+id;
          var idKeterangan = "keterangan-"+bom[x-1]['id']+"-"+id;
          var idUom = "uom-"+bom[x-1]['id']+"-"+id;
          // var idOperator = "operator-"+bom[x-1]['id']+"-"+id;
          // console.log(idAction);
          //console.log(data['action']);
          var siklus = $('select[id="'+idSiklus+'"]').val(data['siklus']);
          var action = $('select[id="'+idAction+'"]').val(data['action']);
          var keterangan = $('textarea[id="'+idKeterangan+'"]').val(data['keterangan']);
          var uom = $('select[id="'+idUom+'"]').val(data['uom']);
          // var operator = $('select[id="'+idOperator+'"]').val(data['operator']);
        });

        //$('select[id^="'+idActionAll+'"]').prop("selectedIndex",value_action);


        // $('textarea[id^="keterangan-"]').on("change",function() {
        //   var id = $(this).attr('id');
        //   var nilai = $(this).val();

        //   //console.log(nilai);

        //   idSplit = id.substring(id.lastIndexOf("-") + 1);
        //   idItem = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

        //   allId = "keterangan-"+idItem+"-";

        //   $('textarea[id^='+allId+']').val(nilai);

        //   $.post("index.php?r=log-formula-breakdown/update-keterangan&id="+idItem+"&value="+nilai, function (data){});
        // });

      }
    });
    $('#reload-script').trigger("click");
  }

  function back(){
    //window.location = "index.php?r=log-formula-breakdown/schedule-list";
    window.history.back();
  }

  function modalConfirm(){
        // console.log(nomo);
        url = '<?php echo Url::to(['log-formula-breakdown/preview-verified-items', 'kode_bulk' => $kode_bulk,'reference'=>$reference,'qty_batch'=>$qty_batch]); ?>';

        console.log (url);

        $('#modalverifieditems').modal('show')
        //   // find id
          .find('#modalVerifiedItems')
        //   // load id
          .load(url);
  }


  function verify(){
    $('#modalverifieditems').modal('hide')
    var dokumen = document.getElementById("dokumen").value;
    var revisi = document.getElementById("revisi").value;
    var nama_line = document.getElementById("line").value;

    if (dokumen == "" || revisi == "" || nama_line == 'Pilih Line'){
      window.alert("No. Dokumen atau No. Revisi atau Nama Line masih kosong");
    } else {
      $.post("index.php?r=log-formula-breakdown/update-nomor&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch+"&dokumen="+dokumen+"&revisi="+revisi, function (data){

        if (data == 1){
          $.post("index.php?r=log-formula-breakdown/get-standard-qty&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data2)
          {
            standard = Number(data2).toFixed(4);

            $.post("index.php?r=log-formula-breakdown/get-sum&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
              sumFix = Number(data).toFixed(4);

              if (parseFloat(sumFix)>(parseFloat(standard)+0.005)){
                window.alert("Total quantity lebih dari standard (OVER).");
              } else if ((parseFloat(standard)-0.005).toFixed(4)<=parseFloat(sumFix) && parseFloat(sumFix)<=(parseFloat(standard)+0.005)){
                $.post("index.php?r=log-formula-breakdown/check-operator-complete&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                  if (data==1)
                  {
                    $.post("index.php?r=log-formula-breakdown/check-timbangan-complete&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                      if (data==1)
                      {
                        $.post("index.php?r=log-formula-breakdown/check-kode-olah-complete&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                          if (data==1)
                          {
                            $.post("index.php?r=log-formula-breakdown/check-kode-internal-complete&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                              if (data==1)
                              {
                                $.post("index.php?r=log-formula-breakdown/send-data-to-weighing&nomo="+nomo+"&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                                  if (data==1)
                                  {
                                    $.post("index.php?r=log-formula-breakdown/update-status&nomo="+nomo+"&pic="+pic, function (data){
                                      // console.log(data);
                                      // console.log(nomo);
                                      if (data == 1){
                                        window.alert("Data Berhasil Diverifikasi");
                                        window.location = "index.php?r=log-formula-breakdown/schedule-list";
                                      } else {
                                        window.alert("Gagal Memverifikasi1");
                                      }
                                    });
                                  } else if (data==2){
                                    // $.post("index.php?r=log-formula-breakdown/update-status&nomo="+nomo+"&pic="+pic, function (data){
                                    //   // console.log(data);
                                    //   // console.log(nomo);
                                    //   if (data == 1){
                                        window.alert("Proses penimbangan sudah dilakukan sehingga tidak dapat verifikasi ulang");
                                        //window.location = "index.php?r=log-formula-breakdown/schedule-list";
                                      // } else {
                                      //   window.alert("Gagal Memverifikasi2");
                                      // }
                                    // });
                                  } else {
                                    window.alert("Gagal Memverifikasi3");
                                  }
                                });

                              }else{
                                window.alert("Ada kolom kode internal yang belum terisi");
                              }
                            });
                          } else{
                            window.alert("Ada kolom kode olah yang belum terisi");
                          }
                        });
                      } else {
                        window.alert("Ada kolom timbangan yang belum terisi");
                      }
                    });
                  } else {
                    window.alert("Ada kolom operator yang belum terisi");
                  }
                });
              } else {
                window.alert("Total quantity kurang dari standard (LESS).");
              }
            });
          });
        } else {
          window.alert("Gagal Memverifikasi, Silahkan Lapor");
        }
      });
    }
  }

  // $.post("index.php?r=master-data-timbangan-rm/get-timbang-nomo&nomo="+nomo, function (data){
  //         $('div[id^="timbangan-"]').html(data);

  //         var id = "siklus-"+bom[x-1]['id']+"-"+bom[x-1]['is_split'];
  //         var a = 0;
  //         if(bom[x-1]['siklus']==1){
  //           a = 0;
  //         } else if (bom[x-1]['siklus']==2){
  //           a = 1;
  //         } else if (bom[x-1]['siklus']==3){
  //           a = 2;
  //         } else{
  //           a = 3;
  //         }

  //         var tag = $('select[id="'+id+'"]').prop("selectedIndex",a);
  //       });

  //$('select[id="line"]').val("LWE 03");

  // .html("<div style='background-color: white; height:20px; margin-left:0px;'><p>HALO</p></div>");
  // var j = $('section[class="content-header"]').prop("tagName");
  // console.log(j);

</script>

<div class="box" style="display: inline-block;">
  <div style="width:96%; margin: 0 auto;" >

    <!-- BUTTON BACK AND VERIFY -->
    <br>

    <div class="row" style="width:100%;">
      <button class="btn btn-info" onclick="back()" style="margin-bottom:1.2em; margin-left:15px; font-size:1em; float: left;"><i class="fa fa-arrow-left"></i></button>
      <button id="calculate" class="btn btn-danger" style="margin-bottom:1.2em; margin-left:15px; font-size:1em; float: left;"><i class="fa fa-calculator"></i></button>
      <div class="div2 kg">

      <input  type="text" id="sum" style="font-weight:bolder; vertical-align:middle; margin-top:4px; margin-left:10px; width:125px; padding-left:5px;" value="0.00000" readonly>
      </div>
      <button type="submit" onclick="modalConfirm()" class="btn btn-info" style="margin-bottom:1em; font-size:1.2em; float: right;">Verify</button>
      <button type="button" id="reload-script" class="btn btn-info" style="margin-bottom:1em; font-size:1.2em; float: right; display: none;" >Reload Script</button>
      <div id='nama_line' style="width:200px;margin-top:4px; margin-right:80px; margin-bottom:1em; font-size:1.2em; float: right;"></div>
      <h4 id='label_nama_line' style="margin-top:6px; margin-right:5px; margin-bottom:1em; float: right;">Line Timbang :</h4>
      <?php echo '<input id="revisi" type="number" value="'; echo $bom[0]['no_revisi']; echo '" style="width:50px;margin-top:4px; margin-right:200px; margin-bottom:1em; font-size:1.2em; float: right; padding-left:8px;"></input>' ?>
      <h4 style="margin-top:6px; margin-right:15px; margin-bottom:1em; float: right;">No. Revisi :</h4>
      <?php echo '<input id="dokumen" value="'; echo $bom[0]['no_dokumen']; echo '" style="width:200px;margin-top:4px; margin-right:15px; margin-bottom:1em; font-size:1.2em; float: right; padding-left:8px;"></input>' ?>
      <h4 style="margin-top:6px; margin-right:15px; margin-bottom:1em; float: right;">No. Dokumen :</h4>
    </div>

    <!-- TABLE CARD -->
    <div>
      <p style="margin-bottom:0px; margin-top:0px;">Showing <b>1-<?php echo count($bom); ?></b> of <b><?php echo count($bom); ?></b> items.</p>
      <div id="table-breakdown">
        <table class="table table-bordered table-striped table-hover" >
          <thead>
            <tr style="padding-top:0;">
              <th class="text-center" style="width: 2%; vertical-align: middle; background:white;">
                <a style="color:black;">#</a>
              </th>
              <th class="text-center" style="width: 11%; vertical-align: middle; background:white;">
                <a style="color:#469ED8;">Kode Bahan Baku</a>
              </th>
              <th class="text-center" style="width: 13%; vertical-align: middle; background:white;">
                <a style="color:#469ED8;">Kode Internal</a>
              </th>
              <th class="text-center" style="width: 15%; vertical-align: middle; background:white;">
                <a style="color:#469ED8;">Nama Bahan Baku</a>
              </th>
              <th class="text-center" style="width: 8%; vertical-align: middle; background:white;">
                <a style="color:#469ED8;">Quantity</a>
              </th>
              <th class="text-center" style="width: 6%; vertical-align: middle; background:white;">
                <a style="color:#469ED8;">UoM</a>
              </th>
              <!-- <th class="text-center" style="width: 6%; vertical-align: middle; background:white;">
                <a style="color:#469ED8;">Reference</a>
              </th>
              <th class="text-center" style="width: 6%; vertical-align: middle; background:white;">
                <a style="color:#469ED8;">Lokasi</a>
              </th> -->
              <th class="text-center" style="width: 5%; vertical-align: middle; background:white;">
                <a style="color:#469ED8;">Siklus</a>
              </th>
              <th class="text-center" style="width: 5%; vertical-align: middle; background:white;">
                <a style="color:#469ED8;">Kode Olah</a>
              </th>
              <th class="text-center" style="width: 7%; vertical-align: middle; background:white;">
                <a style="color:#469ED8;">Action</a>
              </th>
              <th class="text-center" style="width: 9%; vertical-align: middle; background:white;">
                <a style="color:#469ED8;">Keterangan</a>
              </th>
              <th class="text-center" style="width: 6%; vertical-align: middle; background:white;">
                <a style="color:#469ED8;">Operator</a>
              </th>
              <th class="text-center" style="width: 8%; vertical-align: middle; background:white;">
                <a style="color:#469ED8;">Timbangan</a>
              </th>
              <th class="text-center" style="width: 8%; vertical-align: middle; background:white;">
                <a style="color:#469ED8;">Split Quantity</a>
              </th>
            </tr>
          </thead>
          <tbody>
            <?php
              for ($x = 0; $x < count($bom); $x++) {
                if ($bom[$x]['kode_bb'] == "AIR-RO--L"){
                  echo '<tr style="opacity:0.5" id="row-';echo $bom[$x]['id']; echo '" disabled>';
                    echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">'; echo $x+1; echo '</td>';
                    echo '<td class="text-left" style="font-size:1em;vertical-align: middle;"><input disabled value="'.$bom[$x]['kode_bb'].'"></input></td>';
                    echo '<td class="text-left" style="font-size:1em;vertical-align: middle;"><input disabled value="'.$bom[$x]['kode_internal'].'"></input></td>';
                    echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['nama_bb']; echo '</td>';
                    echo '<td class="text-left" style="font-size:1em;vertical-align: middle;" id="qty-'; echo $bom[$x]['id']; echo '-0">'; echo round($bom[$x]['qty'],6); echo '</td>';
                    echo '<td class="text-center" style="font-size:1em;vertical-align: middle;" id="uom-'; echo $bom[$x]['id']; echo '">'; echo $bom[$x]['uom']; echo '</td>';
                    // echo '<td class="text-center" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['formula_reference']; echo '</td>';
                    // echo '<td class="text-center" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['lokasi']; echo '</td>';
                    echo '<td class="text-center" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['siklus']; echo '</td>';
                    echo '<td class="text-center" style="font-size:1em;vertical-align: middle;"></td>';
                    echo '<td class="text-center" style="font-size:1em;vertical-align: middle;"></td>';
                    echo '<td class="text-center" style="font-size:1em;vertical-align: middle;"></td>';
                    echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">'; echo '</td>';
                    // echo '<td class="text-center" style="font-size:1.15em;">'; echo "<input></input>"; echo '</td>';
                    echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">'; echo '</td>';
                    echo '<td class="text-left product2" style="font-size:1em;vertical-align: middle;">'; echo '</td>';
                  echo '</tr>';
                } else {
                  echo '<tr id="row-';echo $bom[$x]['id']; echo '">';
                    echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">'; echo $x+1; echo '</td>';
                    echo '<td class="text-left" style="font-size:1em;vertical-align: middle;"><input id="kode_bb-'.$bom[$x]['id'].'-0" value="'.$bom[$x]['kode_bb'].'"></input></td>';
                    echo '<td class="text-left" style="font-size:1em;vertical-align: middle;"><input style="display:inline; width:75%" id="kode_internal-'.$bom[$x]['id'].'-0" value="'.$bom[$x]['kode_internal'].'"></input>
                      <button type="button" title="Sync Kode Internal ke FRO" class="btn btn-sm product2 btn-info" style="display:inline; margin: 0 auto; width:20%; opacity:0.75; border-radius:0.7em;" id="sync_internal-'; echo $bom[$x]['id']; echo '"><i class="fa fa-refresh"></i></button></td>';
                    // echo '<td class="text-left" style="font-size:1em;vertical-align: middle;" id="kode_internal-'.$bom[$x]['id'].'-0">'; echo $bom[$x]['kode_internal']; echo '</td>';
                    echo '<td class="text-left" id="nama_bb-'.$bom[$x]['id'].'-0" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['nama_bb']; echo '</td>';
                    // echo '<td class="text-center" style="font-size:1em;vertical-align: middle;">'; echo round($bom[$x]['qty'],4);
                    echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">';
                    echo "<input type='numeric' style='font-size:1em; display:block; margin:0 auto; width:100%; padding-left:5px;' value='"; echo number_format($bom[$x]['qty'],6,'.',''); echo "'' id='qty-"; echo $bom[$x]['id']; echo "-0' ></input>";
                    echo '</td>';

                    echo '<td class="text-center" style="font-size:1em;vertical-align: middle;">
                          <select id="uom-'; echo $bom[$x]['id']; echo'-0" style="height:2em; padding-top:3px; text-align:center; width: 90%; display:block; margin:0 auto;" class="form-control form-control-sm text-center">';
                          if ($bom[$x]['uom'] == 'kg' or $bom[$x]['uom'] == 'KG' or $bom[$x]['uom'] == 'Kg'){
                            echo '<option value="kg" selected>kg</option>
                                  <option value="g">g</option>';
                          } else if ($bom[$x]['uom'] == 'g' or $bom[$x]['uom'] == 'G') {
                            echo '<option value="kg">kg</option>
                                  <option value="g" selected>g</option>';
                          }
                    echo '</select></td>';
                    // echo '<td class="text-center" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['formula_reference']; echo '</td>';
                    // echo '<td class="text-center" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['lokasi']; echo '</td>';
                    // echo '<td class="text-center" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['siklus']; echo '</td>';
                    echo '<td lass="text-center" style="font-size:1em;vertical-align: middle;">';
                    echo '<select id="'; echo 'siklus-'; echo $bom[$x]['id']; echo'-0" style="height:2em; text-align:center; width: 90%; display:block; margin:0 auto;" class="form-control form-control-sm text-center"'; if($bom[$x]['is_split'] != 0){ echo ' disabled'; } echo '>';
                      if ($bom[$x]['siklus'] == 1){
                        echo '<option value="0">Pilih Siklus</option>
                        <option value="1" selected>1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>';
                      } else if ($bom[$x]['siklus'] == 2) {
                        echo '<option value="0">Pilih Siklus</option>
                        <option value="1">1</option>
                        <option value="2" selected>2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>';
                      } else if ($bom[$x]['siklus'] == 3) {
                        echo '<option value="0">Pilih Siklus</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3" selected>3</option>
                        <option value="4">4</option>';
                      } else if ($bom[$x]['siklus'] == 4) {
                        echo '<option value="0">Pilih Siklus</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4" selected>4</option>';
                      } else {
                        echo '<option value="0" selected>Pilih Siklus</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>';
                      }
                      '</select>' ; echo '</td>';

                    echo '<td class="text-center" style="font-size:1em;vertical-align: middle;"><input value="'; echo $bom[$x]['kode_olah']; echo '" type="text" style="font-size:1em; display:block; text-align:center; margin:0 auto; width:100%; padding-left:5px;" id="kode_olah-'; echo $bom[$x]['id']; echo '-0"'; if($bom[$x]['is_split'] != 0){ echo ' disabled'; } echo '>'; echo '</input></td>';

                    echo '<td class="text-left" style="font-size:1em;vertical-align: middle; margin:0 auto;">';
                    echo "<select id='action-"; echo $bom[$x]['id']; echo"-0' style='height:2em; text-align:center;vertical-align: middle; width: 90%; display:block; margin:0 auto;' class='form-control form-control-sm text-center'>";
                    if ($bom[$x]['action'] == "^"){
                        echo '<option value="" disabled>Action</option><option value=""></option>
                        <option value="^" selected>^</option>
                        <option value="^^">^^</option>
                        <option value="B U I">B U I</option>
                        <option value="***">***</option>
                        <option value="**">**</option>';
                      } else if ($bom[$x]['action'] == "^^") {
                        echo '<option value="" disabled>Action</option><option value=""></option>
                        <option value="^">^</option>
                        <option value="^^" selected>^^</option>
                        <option value="B U I">B U I</option>
                        <option value="***">***</option>
                        <option value="**">**</option>';
                      } else if ($bom[$x]['action'] == "B U I") {
                        echo '<option value="" disabled>Action</option><option value=""></option>
                        <option value="^">^</option>
                        <option value="^^">^^</option>
                        <option value="B U I" selected>B U I</option>
                        <option value="***">***</option>
                        <option value="**">**</option>';
                      } else if ($bom[$x]['action'] == "***") {
                        echo '<option value="" disabled>Action</option><option value=""></option>
                        <option value="^">^</option>
                        <option value="^^">^^</option>
                        <option value="B U I">B U I</option>
                        <option value="***" selected>***</option>
                        <option value="**">**</option>';
                      } else if ($bom[$x]['action'] == "**") {
                        echo '<option value="" disabled>Action</option><option value=""></option>
                        <option value="^">^</option>
                        <option value="^^">^^</option>
                        <option value="B U I">B U I</option>
                        <option value="***">***</option>
                        <option value="**" selected>**</option>';
                      } else {
                        echo '<option value="" disabled selected>Action</option><option value=""></option>
                        <option value="^">^</option>
                        <option value="^^">^^</option>
                        <option value="B U I">B U I</option>
                        <option value="***">***</option>
                        <option value="**">**</option>';
                      }
                    // foreach($actions as $action){
                    //     echo "<option value='".$action['kode']."'>".$action['kode']."</option>";
                    // }
                        echo '</select></td>';

                    echo '<td class="text-center" style="font-size:1em;vertical-align: middle;"><textarea type="text" placeholder="jika repack tulis REPACK" style="font-size:1em; display:block; margin:0 auto; width:100%; padding-left:5px;" id="keterangan-'; echo $bom[$x]['id']; echo '-0">'; echo $bom[$x]['keterangan']; echo '</textarea></td>';

                    // if (strpos(strtolower($bom[$x]['keterangan']),'repack') !== false and strpos($nama_line,'LW') !== false){
                    //   echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">'; echo "<select id='operator-"; echo $bom[$x]['id']; echo"-0' style='height:2em; text-align:center; width: 90%; display:block; margin:0 auto;' class='form-control form-control-sm text-center'"; if($bom[$x]['is_split'] != 0){ echo ' disabled'; } echo '>';
                    //     if ($bom[$x]['operator'] == 4){
                    //         echo '<option value="0">Pilih Operator</option>
                    //         <option value="4" selected>4</option>
                    //         <option value="5">5</option>
                    //         <option value="6">6</option>';
                    //       } else if ($bom[$x]['operator'] == 5) {
                    //         echo '<option value="0">Pilih Operator</option>
                    //         <option value="4">4</option>
                    //         <option value="5" selected>5</option>
                    //         <option value="6">6</option>';
                    //       } else if ($bom[$x]['operator'] == 6) {
                    //         echo '<option value="0">Pilih Operator</option>
                    //         <option value="4">4</option>
                    //         <option value="5">5</option>
                    //         <option value="6" selected>6</option>';
                    //       } else {
                    //         echo '<option value="0" selected>Pilih Operator</option>
                    //         <option value="4">4</option>
                    //         <option value="5">5</option>
                    //         <option value="6">6</option>';
                    //       }
                    //       echo "</select>"; echo '</td>';
                    // } else {
                      echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">'; echo "<select id='operator-"; echo $bom[$x]['id']; echo"-0' style='height:2em; text-align:center; width: 90%; display:block; margin:0 auto;' class='form-control form-control-sm text-center'"; if($bom[$x]['is_split'] != 0){ echo ' disabled'; } echo '>';
                        if ($bom[$x]['operator'] == 1){
                            echo '<option value="0">Pilih Operator</option>
                            <option value="1" selected>1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>';
                          } else if ($bom[$x]['operator'] == 2) {
                            echo '<option value="0">Pilih Operator</option>
                            <option value="1">1</option>
                            <option value="2" selected>2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>';
                          } else if ($bom[$x]['operator'] == 3) {
                            echo '<option value="0">Pilih Operator</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3" selected>3</option>
                            <option value="4">4</option>';
                          } else if ($bom[$x]['operator'] == 4) {
                            echo '<option value="0">Pilih Operator</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4" selected>4</option>';
                          } else {
                            echo '<option value="0" selected>Pilih Operator</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>';
                          }
                          echo "</select>"; echo '</td>';
                    // }

                    // echo '<td class="text-center" style="font-size:1.15em;">'; echo "<input></input>"; echo '</td>';
                    echo '<td class="text-left" style="font-size:1em;vertical-align: middle; margin:0 auto;">';
                    echo "<select id='timbangan-"; echo $bom[$x]['id']; echo"-0' style='height:2em; text-align:center;vertical-align: middle; width: 90%; display:block; margin:0 auto;' class='form-control form-control-sm text-center'"; if($bom[$x]['is_split'] != 0){ echo ' disabled'; } echo '>';
                    echo "<option value='Timbangan' disabled selected>Timbangan</option>";
                    // if (strpos(strtolower($bom[$x]['keterangan']),'repack') !== false){
                    //   $connection5 = Yii::$app->db;
                    //   $command5 = $connection5->createCommand("

                    //       SELECT kode_timbangan,uom
                    //       FROM master_data_timbangan_rm
                    //       WHERE line=:line_timbang
                    //       ORDER BY id ASC
                    //   ",
                    //   [':line_timbang'=> 'LWE03'
                    //   ]);

                    //   $timbangans_repack = $command5->queryAll();
                    //   $timbangans_option = $timbangans_repack;
                    // }else{
                    //   $timbangans_option = $timbangans;
                    // }
                    if (strpos($bom[$x]['keterangan'],'repack')!==false and strpos($nama_line,'LW') !== false){
                        if ($bom[$x]['uom']=='kg'){
                          // print_r();
                          $timbangans_uom = array_filter($timbangans_repack,function($idx){return $idx['uom']=='kg';});
                        }else if ($bom[$x]['uom']=='g'){
                          $timbangans_uom = array_filter($timbangans_repack,function($idx){return $idx['uom']=='g';});
                        }else{
                          $timbangans_uom = array_filter($timbangans_repack,function($idx){return $idx['uom']=='';});
                        }
                        foreach($timbangans_uom as $timbangan){
                            echo "<option value='".$timbangan['kode_timbangan']."' "; if($timbangan['kode_timbangan']==$bom[$x]['timbangan']){echo "selected";}; echo ">".$timbangan['kode_timbangan']."</option>";
                        }

                    }else{
                        if ($bom[$x]['uom']=='kg'){
                          // print_r();
                          $timbangans_uom = array_filter($timbangans,function($idx){return $idx['uom']=='kg';});
                        }else if ($bom[$x]['uom']=='g'){
                          $timbangans_uom = array_filter($timbangans,function($idx){return $idx['uom']=='g';});
                        }else{
                          $timbangans_uom = array_filter($timbangans,function($idx){return $idx['uom']=='';});
                        }
                        foreach($timbangans_uom as $timbangan){
                            echo "<option value='".$timbangan['kode_timbangan']."' "; if($timbangan['kode_timbangan']==$bom[$x]['timbangan']){echo "selected";}; echo ">".$timbangan['kode_timbangan']."</option>";
                        }

                    }

                        echo '</select></td>';
                    echo '<td class="text-left product2" style="font-size:1em;vertical-align: middle;">';

                        echo '<button type="button" onclick="clickDispatcherTable(';echo $x+1; echo ',';echo $bom[$x]['id']; echo ')" class="btn btn-sm product2 btn-add" style="display:inline; margin: 0 auto; background:#68e468; width:45%; opacity:0.75; border-radius:0.7em;" id="add-'; echo $bom[$x]['id']; echo '"><i class="fa fa-plus"></i></button>&nbsp';

                        echo '<button type="button" title="Sync Data ke Odoo" class="btn btn-sm product2 btn-info" style="display:inline; margin: 0 auto; width:45%; opacity:0.75; border-radius:0.7em;" id="sync-'; echo $bom[$x]['id']; echo '"><i class="fa fa-refresh"></i></button>';
                        /*if($bom[$x]['is_split'] > 0 && $bom[$x]['is_split'] < 99){
                            echo '<button type="button" title="Reset Split Item" class="btn btn-sm product2 btn-warning" style="display:inline; margin: 0 auto; width:45%; opacity:0.75; border-radius:0.7em;" id="reset-'; echo $bom[$x]['id']; echo '"><i class="fa fa-repeat text-black"></i></button>';
                        }*/

                    echo '</td>';
                  echo '</tr>';
                }

                if ($bom[$x]['is_split'] != 0){
                  $i = 0;
                  for ($j = 0; $j < count($data_split); $j++){
                    if ($bom[$x]['id'] == $data_split[$j]['log_formula_breakdown_id']){
                      echo "<tr id='row-"; echo $bom[$x]['id']; echo "-"; echo $data_split[$j]['id']; echo "'>";
                      echo "<td class='text-left' style='font-size:1em;vertical-align: middle;'></td>
                            <td class='text-left' style='font-size:1em;vertical-align: middle;'></td>
                            <td class='text-left' style='font-size:1em;vertical-align: middle;'></td>
                            <td class='text-left' style='font-size:1em;vertical-align: middle;'>"; echo $bom[$x]['nama_bb']; echo "</td>
                            <td style='vertical-align: middle;'><input type='numeric' style='font-size:1em; display:block; margin:0 auto; width:100%; padding-left:5px;' value='"; echo number_format($data_split[$j]['qty'],6,'.',''); echo "'' id='qty-"; echo $bom[$x]['id']; echo "-"; echo $data_split[$j]['id']; echo "'></input></td>";
                      echo '<td class="text-center" style="font-size:1em;vertical-align: middle;">
                            <select id="uom-'; echo $bom[$x]['id']; echo'-';  echo $data_split[$j]['id']; echo '" style="height:2em; padding-top:3px; text-align:center; width: 90%; display:block; margin:0 auto;" class="form-control form-control-sm text-center">';
                            if ($data_split[$j]['uom'] == 'kg' or $data_split[$j]['uom'] == 'KG' or $data_split[$j]['uom'] == 'Kg'){
                              echo '<option value="kg" selected>kg</option>
                                    <option value="g">g</option>';
                            } else if ($data_split[$j]['uom'] == 'g' or $data_split[$j]['uom'] == 'G') {
                              echo '<option value="kg">kg</option>
                                    <option value="g" selected>g</option>';
                            }
                      echo '</select></td>';

                              echo "<td class='text-center' style='font-size:1em;vertical-align: middle;'>
                                <select id='siklus-"; echo $bom[$x]['id']; echo "-"; echo $data_split[$j]['id']; echo "' style='height:2em; text-align:center; width: 90%; display:block; margin:0 auto;' class='form-control form-control-sm text-center'>";

                              if ($data_split[$j]['siklus'] == 1){
                                echo '<option value="0">Pilih Siklus</option>
                                <option value="1" selected>1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>';
                              } else if ($data_split[$j]['siklus'] == 2) {
                                echo '<option value="0">Pilih Siklus</option>
                                <option value="1">1</option>
                                <option value="2" selected>2</option>
                                <option value="3">3</option>
                                // <option value="4">4</option>';
                              } else if ($data_split[$j]['siklus'] == 3) {
                                echo '<option value="0">Pilih Siklus</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3" selected>3</option>
                                <option value="4">4</option>';
                              } else if ($data_split[$j]['siklus'] == 4) {
                                echo '<option value="0">Pilih Siklus</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4" selected>4</option>';
                              } else {
                                echo '<option value="0" selected>Pilih Siklus</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>';
                              }
                            echo "</select>
                            </td>";

                            echo '<td class="text-center" style="font-size:1em;vertical-align: middle;"><input value="'; echo $data_split[$j]['kode_olah']; echo '" type="text" style="font-size:1em; display:block; text-align:center; margin:0 auto; width:100%; padding-left:5px;" id="kode_olah-'; echo $bom[$x]['id']; echo '-'; echo $data_split[$j]['id']; echo '"></input></td>';

                            echo '<td class="text-left" style="font-size:1em;vertical-align: middle; margin:0 auto;">';
                            echo "<select id='action-"; echo $bom[$x]['id']; echo"-"; echo $data_split[$j]['id']; echo "' style='height:2em; text-align:center; width: 90%; display:block; margin:0 auto;' class='form-control form-control-sm text-center' disabled>";
                            if ($bom[$x]['action'] == "^"){
                              echo '<option value="" disabled>Action</option><option value=""></option>
                              <option value="^" selected>^</option>
                              <option value="^^">^^</option>
                              <option value="B U I">B U I</option>
                              <option value="***">***</option>
                              <option value="**">**</option>';
                            } else if ($bom[$x]['action'] == "^^") {
                              echo '<option value="" disabled>Action</option><option value=""></option>
                              <option value="^">^</option>
                              <option value="^^" selected>^^</option>
                              <option value="B U I">B U I</option>
                              <option value="***">***</option>
                              <option value="**">**</option>';
                            } else if ($bom[$x]['action'] == "B U I") {
                              echo '<option value="" disabled>Action</option><option value=""></option>
                              <option value="^">^</option>
                              <option value="^^">^^</option>
                              <option value="B U I" selected>B U I</option>
                              <option value="***">***</option>
                              <option value="**">**</option>';
                            } else if ($bom[$x]['action'] == "***") {
                              echo '<option value="" disabled>Action</option><option value=""></option>
                              <option value="^">^</option>
                              <option value="^^">^^</option>
                              <option value="B U I">B U I</option>
                              <option value="***" selected>***</option>
                              <option value="**">**</option>';
                            } else if ($bom[$x]['action'] == "**") {
                              echo '<option value="" disabled>Action</option><option value=""></option>
                              <option value="^">^</option>
                              <option value="^^">^^</option>
                              <option value="B U I">B U I</option>
                              <option value="***">***</option>
                              <option value="**" selected>**</option>';
                            } else {
                              echo '<option value="" disabled selected>Action</option><option value=""></option>
                              <option value="^">^</option>
                              <option value="^^">^^</option>
                              <option value="B U I">B U I</option>
                              <option value="***">***</option>
                              <option value="**">**</option>';
                            }
                            echo '</select></td>';

                            echo '<td class="text-center" style="font-size:1em;vertical-align: middle;"><textarea type="text" placeholder="jika repack tulis REPACK" style="font-size:1em; display:block; margin:0 auto; width:100%; padding-left:5px;" id="keterangan-'; echo $bom[$x]['id']; echo '-'; echo $data_split[$j]['id']; echo '">'; echo $data_split[$j]['keterangan']; echo '</textarea></td>';


                            // if (strpos(strtolower($data_split[$j]['keterangan']),'repack') !== false and strpos($nama_line,'LW') !== false){

                            //   echo "<td class='text-center' style='font-size:1em;vertical-align: middle;'>
                            //     <select id='operator-"; echo $bom[$x]['id']; echo"-"; echo $data_split[$j]['id']; echo "' style='height:2em; text-align:left; width: 90%; display:block; margin:0 auto;' class='form-control form-control-sm text-center'>";
                            //       if ($data_split[$j]['operator'] == 4){
                            //         echo '<option value="0">Pilih Operator</option>
                            //         <option value="4" selected>4</option>
                            //         <option value="5">5</option>
                            //         <option value="3">3</option>';
                            //       } else if ($data_split[$j]['operator'] == 5) {
                            //         echo '<option value="0">Pilih Operator</option>
                            //         <option value="4">4</option>
                            //         <option value="5" selected>5</option>
                            //         <option value="6">6</option>';
                            //       } else if ($data_split[$j]['operator'] == 6) {
                            //         echo '<option value="0">Pilih Operator</option>
                            //         <option value="4">4</option>
                            //         <option value="5">5</option>
                            //         <option value="6" selected>6</option>';
                            //       } else {
                            //         echo '<option value="0" selected>Pilih Operator</option>
                            //         <option value="4">4</option>
                            //         <option value="5">5</option>
                            //         <option value="6">6</option>';
                            //       }
                            //     echo "</select>
                            //   </td>";
                            // } else {
                              echo "<td class='text-center' style='font-size:1em;vertical-align: middle;'>
                                <select id='operator-"; echo $bom[$x]['id']; echo"-"; echo $data_split[$j]['id']; echo "' style='height:2em; text-align:left; width: 90%; display:block; margin:0 auto;' class='form-control form-control-sm text-center'>";
                                  if ($data_split[$j]['operator'] == 1){
                                    echo '<option value="0">Pilih Operator</option>
                                    <option value="1" selected>1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>';
                                  } else if ($data_split[$j]['operator'] == 2) {
                                    echo '<option value="0">Pilih Operator</option>
                                    <option value="1">1</option>
                                    <option value="2" selected>2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>';
                                  } else if ($data_split[$j]['operator'] == 3) {
                                    echo '<option value="0">Pilih Operator</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3" selected>3</option>
                                    <option value="4">4</option>';
                                  } else if ($data_split[$j]['operator'] == 4) {
                                    echo '<option value="0">Pilih Operator</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4" selected>4</option>';
                                  } else {
                                    echo '<option value="0" selected>Pilih Operator</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>';
                                  }
                                echo "</select>
                              </td>";
                            // }
                            echo '<td class="text-center" style="font-size:1em;vertical-align: middle; margin:0 auto;">';
                            echo "<select id='timbangan-"; echo $bom[$x]['id']; echo"-"; echo $data_split[$j]['id']; echo"' style='height:2em; text-align:center; width: 90%; display:block;vertical-align: middle; margin:0 auto;' class='form-control form-control-sm text-center'"; echo '>';
                            echo "<option value='Timbangan' disabled selected>Timbangan</option>";

                            // if (strpos(strtolower($data_split[$j]['keterangan']),'repack') !== false){
                            //   $connection5 = Yii::$app->db;
                            //   $command5 = $connection5->createCommand("

                            //       SELECT kode_timbangan,uom
                            //       FROM master_data_timbangan_rm
                            //       WHERE line=:line_timbang
                            //       ORDER BY id ASC
                            //   ",
                            //   [':line_timbang'=> 'LWE03'
                            //   ]);

                            //   $timbangans_repack = $command5->queryAll();
                            //   $timbangans_option = $timbangans_repack;
                            // }else{
                            //   $timbangans_option = $timbangans;
                            // }

                            if (strpos($data_split[$j]['keterangan'],'repack')!==false and strpos($nama_line,'LW') !== false){
                                if ($data_split[$j]['uom']=='kg'){
                                  // print_r();
                                  $timbangans_uom = array_filter($timbangans_repack,function($idx){return $idx['uom']=='kg';});
                                }else if ($data_split[$j]['uom']=='g'){
                                  $timbangans_uom = array_filter($timbangans_repack,function($idx){return $idx['uom']=='g';});
                                }else{
                                  $timbangans_uom = array_filter($timbangans_repack,function($idx){return $idx['uom']=='';});
                                }
                                foreach($timbangans_uom as $timbangan){
                                    echo "<option value='".$timbangan['kode_timbangan']."' "; if($timbangan['kode_timbangan']==$data_split[$j]['timbangan']){echo "selected";}; echo ">".$timbangan['kode_timbangan']."</option>";
                                }
                            }else{
                                if ($data_split[$j]['uom']=='kg'){
                                  // print_r();
                                  $timbangans_uom = array_filter($timbangans,function($idx){return $idx['uom']=='kg';});
                                }else if ($data_split[$j]['uom']=='g'){
                                  $timbangans_uom = array_filter($timbangans,function($idx){return $idx['uom']=='g';});
                                }else{
                                  $timbangans_uom = array_filter($timbangans,function($idx){return $idx['uom']=='';});
                                }
                                foreach($timbangans_uom as $timbangan){
                                    echo "<option value='".$timbangan['kode_timbangan']."' "; if($timbangan['kode_timbangan']==$data_split[$j]['timbangan']){echo "selected";}; echo ">".$timbangan['kode_timbangan']."</option>";
                                }
                            }


                                echo '</select></td>';
                            echo "<td class='text-left' style='font-size:1em;vertical-align: middle;'>
                              <button type='button' onclick=deleteRow('"; echo "row-"; echo $bom[$x]['id']; echo "-"; echo $data_split[$j]['id']; echo "',"; echo $bom[$x]['id']; echo ") class='btn btn-dark btn-sm product2 btn-minus' style='display:block; margin: 0 auto; background:#FF4C4C; width:50%; opacity:0.85; border-radius:0.7em;' id='minus-"; echo $bom[$x]['id']; echo "'><i class='fa fa-minus'></i></button>
                            </td>
                          </tr>";
                    }
                  }
                }
              }?>
          </tbody>
        </table>
      </div>

    </div>

  </div>
</div>


<!-- <div class="box box-widget widget-user" style="width:40%;display: block; margin-left: auto; margin-right: auto;">
    <div class="widget-user-header bg-blue" style="height:50%;">
                      <h3 class="widget-user-username" style="text-align:center"><b>VERIFICATION HISTORY</b></h3>
    </div>
    <?= GridView::widget([
        'dataProvider' => $verificationHistory,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nomo',
            'pic',
            'timestamp',
        ],
    ]); ?>

</div> -->

<?php
$script = <<< JS

  window.alert('Pastikan UoM sudah sesuai dengan Timbangan yang dipilih!');
  $('#reload-sript').hide();
  var nomo = '$nomo';
  var reference = '$reference';
  var lokasi = '$lokasi';
  var nama_line = '$nama_line';
  var kode_bulk = '$kode_bulk';
  var status_timbang = '$status_timbang';
  // var kode_bulk = nomo.substring(nomo.firstIndexOf("-")+1,nomo.lastIndexOf("/"));
  //console.log(kode_bulk);
  var qty_batch = '$qty_batch';
  var standard = 0.0;
  var sumFix = 0.0;
  var flag = 1;

  $.post("index.php?r=log-formula-breakdown/get-standard-qty&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data2)
  {
    standard = Number(data2).toFixed(4);

    $.post("index.php?r=log-formula-breakdown/get-sum&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
      sumFix = Number(data).toFixed(4);

      //console.log(sumFix);

      if (parseFloat(sumFix)>(parseFloat(standard)+0.005)){
        $('input[id="sum"]').val(sumFix);
        document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #ff6666";
      } else if ((parseFloat(standard)-0.005).toFixed(4)<=parseFloat(sumFix) && parseFloat(sumFix)<=(parseFloat(standard)+0.005)){
        $('input[id="sum"]').val(sumFix);
        document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #3CB371";
      } else {
        $('input[id="sum"]').val(sumFix);
        document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #ffff00";
      }
    });
  });

  $.post("index.php?r=master-data-timbangan-rm/get-line-timbang-rm-nomo&nomo="+nomo, function (data){
      $('div[id="nama_line"]').html(data);
      $('select[id="line"]').val(nama_line);
      console.log(nama_line);
  });

  // $.post("index.php?r=master-data-timbangan-rm/get-timbang-nomo&nomo="+nomo, function (data){
  //     $('div[id^="timbangan-"]').html(data);
  // });

  var j = $('section[class="content-header"]').html(`
    <div class="row" style='background-color: white; height:50px; margin-left:0px; margin-top:0px; padding-left:1%; padding-top:1px;'>
      <div class="col-6" style="float:left">
        <h4>
          <a style='color:#1fd164;'>Bills of Materials</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <a style='color:#6f6f6f;'>`+bom[0]['id']+`: [`+bom[0]['kode_bulk']+`] `+bom[0]['nama_fg']+`</a>
        </h4>
      </div>
      <div class="col-6" style="float:right; padding-right:5%;">

        <h4>
          <a style='color:#1fd164;'>Reference</a> : <a style='color:#6f6f6f; margin-right:35px;'>`+reference+`</a>
          <a style='color:#1fd164;'>Lokasi</a> : <a style='color:#6f6f6f; margin-right:35px;'>`+lokasi+`</a>
          <a style='color:#1fd164;'>Batch Qty</a> : <a style='color:#6f6f6f;'>`+qty_batch+` kg</a>
        </h4>

      </div>
    </div>
    <div class="row" style='background-color: white; height:50px; margin-left:0px; margin-top:0px; padding-left:1%; padding-top:1px;'>
      <div class="col-12" style="float:left">
        <h4>
          <a style='color:#1fd164;'>Status Penimbangan</a>&nbsp;: <a style='color:#6f6f6f;'>`+status_timbang+`</a>
        </h4>
      </div>
    </div>`);

    $('input[id^="kode_olah-"]').on("change",function() {
      var id = $(this).attr('id');
      var nilai = $(this).val();

      idSplit = id.substring(id.lastIndexOf("-") + 1);
      idItem = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      if (idSplit == 0){
        $.post("index.php?r=log-formula-breakdown/update-kode-olah&id="+idItem+"&value="+nilai +"&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
            if (data == 2){
              alert("KODE OLAH SUDAH DIGUNAKAN DI BAHAN BAKU LAIN");
              document.getElementById(id).value = "";
            } else if (data == 0){
              alert ("Gagal menyimpan kode olah, lapor IT");
            }
          });
      } else {
        $.post("index.php?r=log-formula-breakdown/update-kode-olah-split&id="+idItem+"&id_split="+idSplit+"&value="+nilai +"&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
            if (data == 2){
              alert("KODE OLAH SUDAH DIGUNAKAN DI BAHAN BAKU LAINa");
              document.getElementById(id).value = "";
            } else if (data == 0){
              alert ("Gagal menyimpan kode olah, lapor IT");
            }
          });
      }
    });

    $('input[id^="kode_internal-"]').on("change",function() {
      var id = $(this).attr('id');
      var nilai = $(this).val();

      //idSplit = id.substring(id.lastIndexOf("-") + 1);
      idItem = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      $.post("index.php?r=log-formula-breakdown/update-kode-internal&id="+idItem+"&value="+nilai, function (data){});
    });

    $('select[id^="action-"]').on("change",function() {
      var id = $(this).attr('id');
      var nilai = $(this).val();

      idSplit = id.substring(id.lastIndexOf("-") + 1);
      idItem = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      allId = "action-"+idItem+"-";

      $('select[id^='+allId+']').val(nilai);

      $.post("index.php?r=log-formula-breakdown/update-action&id="+idItem+"&value="+nilai, function (data){});
    });

    $('input[id^="kode_bb-"]').on("change",function() {
      var id = $(this).attr('id');
      var nilai = $(this).val();

      idSplit = id.substring(id.lastIndexOf("-") + 1);
      idItem = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      //allId = "action-"+idItem+"-";

      //$('select[id^='+allId+']').val(nilai);

      $.post("index.php?r=log-formula-breakdown/update-kodebb&id="+idItem+"&value="+nilai, function (data){});
    });

    $('button[id^="sync-"]').on("click",function() {
      var id = $(this).attr('id');
      idItem = id.substring(id.lastIndexOf("-") + 1);
      var nilai = $('#kode_bb-'+idItem+'-0').val();
      // console.log(nilai);

      // idSplit = id.substring(id.lastIndexOf("-") + 1);
      // idItem = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      //allId = "action-"+idItem+"-";

      //$('select[id^='+allId+']').val(nilai);

      $.getJSON("index.php?r=log-formula-breakdown/sync-kodebb&nomo="+nomo+"&id="+idItem+"&value="+nilai, function (data){

          if (data.status == "success"){
            // console.log(data.status);
            $('#nama_bb-'+idItem+'-0').html(data.nama_bb);
            $('#qty-'+idItem+'-0').val(data.qty);
            $('#uom-'+idItem+'-0').val(data.uom);
            $('#kode_internal-'+idItem+'-0').val(data.kode_internal);
          } else {
            alert("Gagal sync, data di warehouse belum update. Hubungi tim IT untuk memastikan.");
          }
        });
    });

    $('button[id^="reset-"]').on("click",function() {
      var id = $(this).attr('id');
      idItem = id.substring(id.lastIndexOf("-") + 1);
      var nilai = $('#kode_bb-'+idItem+'-0').val();
      // console.log(nilai);

      // idSplit = id.substring(id.lastIndexOf("-") + 1);
      // idItem = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      //allId = "action-"+idItem+"-";

      //$('select[id^='+allId+']').val(nilai);

      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })

      swalWithBootstrapButtons.fire({
        title: 'Apakah anda yakin ingin reset seluruh split item?',
        text: "Seluruh item split akan direset. Anda tidak dapat mengembalikan aksi ini.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Reset sekarang',
        cancelButtonText: 'Batal',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {

          $.post("index.php?r=log-formula-breakdown/reset-split&nomo="+nomo+"&reference="+reference+"&kode_bb="+nilai, function (data){
              if (data == 1) {
                  swalWithBootstrapButtons.fire(
                      'Reseted!',
                      'Split item berhasil disetel ulang.',
                      'success'
                  ).then((result) => {
                      window.location.reload(false);
                  })
              }
          });

        }
      })
    });

    $('button[id^="sync_internal-"]').on("click",function() {
      var id = $(this).attr('id');
      idItem = id.substring(id.lastIndexOf("-") + 1);
      var nilai = $('#kode_bb-'+idItem+'-0').val();
      // console.log(nilai);

      $.getJSON("index.php?r=log-formula-breakdown/sync-kodeinternal&kode_bb="+nilai+"&id="+idItem, function (data){

          if (data.status == "success"){
            // console.log(data.kode_internal);
            $('#kode_internal-'+idItem+'-0').val(data.kode_internal);
          } else {
            alert("Kode Internal belum pernah tersimpan di FRO");
          }
        });
    });

    $('textarea[id^="keterangan-"]').on("change",function() {
      var id = $(this).attr('id');
      var nilai = $(this).val();

      //console.log(nilai);

      idSplit = id.substring(id.lastIndexOf("-") + 1);
      idItem = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      allId = "keterangan-"+idItem+"-";

      var uom = $('#uom-'+idItem+'-'+idSplit).val();
      var timbangan = $('#timbangan-'+idItem+'-'+idSplit).val();

      if (idSplit == 0){
        $('textarea[id^='+allId+']').val(nilai);
      }

      if (idSplit!=0){
        $.post("index.php?r=log-formula-breakdown/update-keterangan-split&id_split="+idSplit+"&value="+nilai, function (data){});
      } else {
        $.post("index.php?r=log-formula-breakdown/update-keterangan&id="+idItem+"&value="+nilai, function (data){});
      }

      // console.log(nilai);
      // console.log(nilai.toLowerCase().indexOf('repack'));
      // if (nilai.toLowerCase().indexOf('repack') >= 0) {
      //   if (nama_line.indexOf('LW')>=0){
      //     console.log('berhasil');
      //     $.get("index.php?r=master-data-timbangan-rm/get-timbangan-repack&uom="+uom+"&kode_timbangan="+timbangan, function (data){

      //       if (data=="-") {
      //         flag = 0;
      //         window.alert('Daftar timbangan untuk line tersebut belum ada di database.');
      //       } else {
      //         $('select#timbangan-'+idItem+'-'+idSplit).html(data);
      //         $('input#qty-'+idItem+'-'+idSplit).trigger("change");
      //       }

      if (nama_line.indexOf('LW')>=0){
        if (nilai.toLowerCase().indexOf('repack') >= 0) {
          $.get("index.php?r=log-formula-breakdown/get-operator-repack&idItem="+idItem+"&idSplit="+idSplit+"&is_repack="+1, function (data){
            $('select#operator-'+idItem+'-'+idSplit).html(data);
            $('input#operator-'+idItem+'-'+idSplit).trigger("change");

          });
          window.alert('Pilih timbangan khusus Repack di LWE 03!');

          // $.get("index.php?r=master-data-timbangan-rm/get-timbangan-repack&uom="+uom+"&kode_timbangan="+timbangan+"&idItem="+idItem+"&idSplit="+idSplit, function (data){

          //   if (data=="-") {
          //     flag = 0;
          //     window.alert('Daftar timbangan untuk line tersebut belum ada di database.');
          //   } else {
          //     $('select#timbangan-'+idItem+'-'+idSplit).html(data);
          //     $('input#qty-'+idItem+'-'+idSplit).trigger("change");
          //   }
          // });
          // window.alert('Pilih timbangan khusus Repack di LWE 03!');
        } else {
          $.get("index.php?r=log-formula-breakdown/get-operator-repack&idItem="+idItem+"&idSplit="+idSplit+"&is_repack="+0, function (data){
            $('select#operator-'+idItem+'-'+idSplit).html(data);
            $('input#operator-'+idItem+'-'+idSplit).trigger("change");

          });
        }
      }

    });


    $("input[id^='qty-']").each(function () {
      var id = $(this).attr('id');
      var nilai = $(this).val();

      var id2 = id.substring(id.lastIndexOf("-") + 1);
      var id3 = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      var timbangan = $('#timbangan-'+id3+'-'+id2).val();
      $.post("index.php?r=log-formula-breakdown/get-decimal-digit&timbangan="+timbangan, function (data){
        var decimal_digit = parseInt(data);
        if (id2!=0){

          // INPUT DATA SPLIT DARI ITEM TERPILIH
          if ((nilai>0) && (!isNaN(nilai)) && (nilai!="")){
            nilai = Number(nilai).toFixed(decimal_digit);
            document.getElementById(id).value = nilai;
            $.post("index.php?r=log-formula-breakdown/update-split-qty&id_split="+id2+"&qty="+nilai, function (data){});
          } else {
            nilai = Number(0).toFixed(decimal_digit);
            document.getElementById(id).value = nilai;
            $.post("index.php?r=log-formula-breakdown/update-split-qty&id_split="+id2+"&qty="+nilai, function (data){});
          }
        } else {
          // INPUT DATA SPLIT DARI ITEM TERPILIH
          if ((nilai>0) && (!isNaN(nilai)) && (nilai!="")){
            nilai = Number(nilai).toFixed(decimal_digit);
            document.getElementById(id).value = nilai;
            $.post("index.php?r=log-formula-breakdown/update-qty&id="+id3+"&qty="+nilai, function (data){});
          } else {
            nilai = Number(0).toFixed(decimal_digit);
            document.getElementById(id).value = nilai;
            $.post("index.php?r=log-formula-breakdown/update-qty&id="+id3+"&qty="+nilai, function (data){});
          }
        }
      });
    });

  // setInterval(function(){

  $('#reload-script').on("click", function(){
    console.log('reload script');
    $('input[id^="qty-"]').on("click",function() {
      var nilai = $(this).val();
      if (nilai != 0){} else { $(this).val(""); }
    });

    $('input[id^="qty-"]').on("focusout blur change",function() {
      var id = $(this).attr('id');
      var nilai = $(this).val();

      var id2 = id.substring(id.lastIndexOf("-") + 1);
      var id3 = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      var timbangan = $('#timbangan-'+id3+'-'+id2).val();
      $.post("index.php?r=log-formula-breakdown/get-decimal-digit&timbangan="+timbangan, function (data){
        var decimal_digit = parseInt(data);
        if (id2!=0){

          // INPUT DATA SPLIT DARI ITEM TERPILIH
          if ((nilai>0) && (!isNaN(nilai)) && (nilai!="")){
            nilai = Number(nilai).toFixed(decimal_digit);
            document.getElementById(id).value = nilai;
            $.post("index.php?r=log-formula-breakdown/update-split-qty&id_split="+id2+"&qty="+nilai, function (data){});
          } else {
            nilai = Number(0).toFixed(decimal_digit);
            document.getElementById(id).value = nilai;
            $.post("index.php?r=log-formula-breakdown/update-split-qty&id_split="+id2+"&qty="+nilai, function (data){});
          }
        } else {
          // INPUT DATA SPLIT DARI ITEM TERPILIH
          if ((nilai>0) && (!isNaN(nilai)) && (nilai!="")){
            nilai = Number(nilai).toFixed(decimal_digit);
            document.getElementById(id).value = nilai;
            $.post("index.php?r=log-formula-breakdown/update-qty&id="+id3+"&qty="+nilai, function (data){});
          } else {
            nilai = Number(0).toFixed(decimal_digit);
            document.getElementById(id).value = nilai;
            $.post("index.php?r=log-formula-breakdown/update-qty&id="+id3+"&qty="+nilai, function (data){});
          }
        }
      });


    });

    $('input[id^="kode_olah-"]').on("change",function() {
      var id = $(this).attr('id');
      var nilai = $(this).val();

      idSplit = id.substring(id.lastIndexOf("-") + 1);
      idItem = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      if (idSplit == 0){
        $.post("index.php?r=log-formula-breakdown/update-kode-olah&id="+idItem+"&value="+nilai +"&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
            if (data == 2 && flag){
              flag = 0;
              console.log(flag);
              if (window.confirm('KODE OLAH SUDAH DIGUNAKAN DI BAHAN BAKU LAIN'))
              {
                flag = 1;
              }
              else
              {
                  flag = 1;
              }
              document.getElementById(id).value = "";
            } else if (data == 0){
              alert ("Gagal menyimpan kode olah, lapor IT");
            }
          });
      } else {
        $.post("index.php?r=log-formula-breakdown/update-kode-olah-split&id="+idItem+"&id_split="+idSplit+"&value="+nilai +"&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
            if (data == 2 && flag){
              flag = 0;
              if (window.confirm('KODE OLAH SUDAH DIGUNAKAN DI BAHAN BAKU LAIN'))
              {
                flag = 1;
              }
              else
              {
                  flag = 1;
              }
              document.getElementById(id).value = "";
            } else if (data == 0){
              alert ("Gagal menyimpan kode olah, lapor IT");
            }
          });
      }
    });

    $('select[id^="action-"]').on("change",function() {
      var id = $(this).attr('id');
      var nilai = $(this).val();

      idSplit = id.substring(id.lastIndexOf("-") + 1);
      idItem = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      allId = "action-"+idItem+"-";

      $('select[id^='+allId+']').val(nilai);

      $.post("index.php?r=log-formula-breakdown/update-action&id="+idItem+"&value="+nilai, function (data){});
    });

    $('textarea[id^="keterangan-"]').on("change",function() {
      var id = $(this).attr('id');
      var nilai = $(this).val();

      //console.log(nilai);

      idSplit = id.substring(id.lastIndexOf("-") + 1);
      idItem = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      allId = "keterangan-"+idItem+"-";

      if (idSplit == 0){
        $('textarea[id^='+allId+']').val(nilai);
      }

      if (idSplit!=0){
        $.post("index.php?r=log-formula-breakdown/update-keterangan-split&id_split="+idSplit+"&value="+nilai, function (data){});
      } else {
        $.post("index.php?r=log-formula-breakdown/update-keterangan&id="+idItem+"&value="+nilai, function (data){});
      }
    });

    $('select[id^="siklus-"]').on("change",function() {
      var id = $(this).attr('id');
      var nilai = $(this).val();

      id2 = id.substring(id.lastIndexOf("-") + 1);
      id3 = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      if (id2!=0){
        $.post("index.php?r=log-formula-breakdown/update-siklus-split&id_split="+id2+"&siklus="+nilai, function (data){});
      } else {
        $.post("index.php?r=log-formula-breakdown/update-siklus&id="+id3+"&siklus="+nilai, function (data){
        });
      }
    });

    $('select[id^="operator-"]').on("change",function() {
      var id = $(this).attr('id');
      var nilai = $(this).val();

      id2 = id.substring(id.lastIndexOf("-") + 1);
      id3 = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));
      line = $('#line').val();
      uom = $('#uom-'+id3+'-'+id2).val();

      if (id2!=0){
        $.post("index.php?r=log-formula-breakdown/update-operator-split&id_split="+id2+"&operator="+nilai+"&line="+line+"&uom="+uom, function (data){
            if (data=="-") {
              window.alert('Daftar timbangan untuk line tersebut belum ada di database.');
            } else {
              $('select#timbangan-'+id3+'-'+id2).html(data);
              $('input#qty-'+id3+'-'+id2).trigger("change");
            }
          });
      } else {
        $.post("index.php?r=log-formula-breakdown/update-operator&id="+id3+"&operator="+nilai+"&line="+line+"&uom="+uom, function (data){
            if (data=="-") {
              window.alert('Daftar timbangan untuk line tersebut belum ada di database.');
            } else {
              $('select#timbangan-'+id3+'-'+id2).html(data);
              $('input#qty-'+id3+'-'+id2).trigger("change");
            }
        });
      }
    });

    $('select[id^="timbangan-"]').on("change",function() {
      var id = $(this).attr('id');
      var nilai = $(this).val();

      id2 = id.substring(id.lastIndexOf("-") + 1);
      id3 = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      if (id2!=0){
        $.post("index.php?r=log-formula-breakdown/update-timbangan-split&id_split="+id2+"&timbangan="+nilai, function (data){});
      } else {
        $.post("index.php?r=log-formula-breakdown/update-timbangan&id="+id3+"&timbangan="+nilai, function (data){
        });
      }

      $('#qty-'+id3+'-'+id2).trigger("change");

    });

  });//reload-script

  $('#reload-script').trigger("click");

  // },10000);

  setInterval(function(){

    $('select[id="line"]').on("change",function() {
      console.log('berubah');
      var id = $(this).attr('id');
      var nama_line = $(this).val();
      //console.log(nama_line);
      flag = 1;
      idNotSplit = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      $.get("index.php?r=log-formula-breakdown/update-line&kode_bulk="+kode_bulk+"&reference="+reference+"&nama_line="+nama_line+"&nomo="+nomo, function (data){
        // console.log('loop get');
        if (data == 1){
          var x = 1;
          $("select[id^='timbangan-']").each(function () {
             //It'll be an array of elements
            var id_timbangan = $(this).attr('id');
            var idSplit = id_timbangan.substring(id_timbangan.lastIndexOf("-") + 1);
            var idItem = id_timbangan.substring(id_timbangan.indexOf("-") + 1, id_timbangan.lastIndexOf("-"));
            var operator = $('#operator-'+idItem+'-'+idSplit).val();


            console.log(id_timbangan);
            $(this).empty();
            $.get("index.php?r=master-data-timbangan-rm/get-timbangan-by-line&nama_line="+nama_line+"&idItem="+idItem+"&idSplit="+idSplit+"&operator="+operator, function (data){

              if (data=="-" && flag) {
                flag = 0;
                window.alert('Daftar timbangan untuk line tersebut belum ada di database.');
              } else {
                // $("select[id^='timbangan-']").each(function () {
                //   $(this).html(data);
                // });
                $('select#timbangan-'+idItem+'-'+idSplit).html(data);
              }
            });
          });
        } else {
          window.alert("Tidak berhasil update nama line, tolong kontak PIC sistem nya");
        }
      });
      $('#reload-script').trigger("click");

    });
  },2000);


  $('button[id="calculate"]').on("click",function()
  {
    //console.log("a");
    //var nilai = $(this).val();
    //var id = $(this).attr('id');
    var sumPerItem = 0.0;
    var qty_standard_per_item = 0.0;

    //id2 = id.substring(id.lastIndexOf("-") + 1);
    //id3 = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

    $.post("index.php?r=log-formula-breakdown/get-standard-qty&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data2){
      standard = Number(data2).toFixed(4);
      $.post("index.php?r=log-formula-breakdown/get-sum&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
        //sumFix = data;
        sumFix = Number(data).toFixed(4);
        //standard = data2;

        // console.log("sum:"+sumFix+"  standard:"+standard);

        if (parseFloat(sumFix)>(parseFloat(standard)+0.005)){
          $('input[id="sum"]').val(sumFix);
          document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #ff6666";
        } else if ((parseFloat(standard)-0.005).toFixed(4)<=parseFloat(sumFix) && parseFloat(sumFix)<=(parseFloat(standard)+0.005)){
          $('input[id="sum"]').val(sumFix);
          document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #3CB371";
        } else {
          $('input[id="sum"]').val(sumFix);
          document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #ffff00";
        }
      });
    });
  });

  $('select[id^="uom-"]').on("change",function() {
    var id = $(this).attr('id');
    var uom = $(this).val();

    idSplit = id.substring(id.lastIndexOf("-") + 1);
    idNotSplit = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));
    var operator = $('#operator-'+idNotSplit+'-'+idSplit).val();

    //id_ = 'uom-'+idNotSplit+'-';
    //idQty = 'qty-'+idNotSplit+'-0';

    if (idSplit == '0'){
      $.getJSON("index.php?r=log-formula-breakdown/update-uom&id="+idNotSplit+"&uom="+uom, function (data){
        if (data.status == 'not-success'){
          window.alert('Penggantian UoM gagal');
        }else{
          $.get("index.php?r=master-data-timbangan-rm/get-timbangan-by-line&nama_line="+nama_line+"&idItem="+idNotSplit+"&idSplit="+idSplit+"&operator="+operator, function (data){

            if (data=="-" && flag) {
              flag = 0;
              window.alert('Daftar timbangan untuk line tersebut belum ada di database.');
            } else {
              // $("select[id^='timbangan-']").each(function () {
              //   $(this).html(data);
              // });
              // $('#timbangan-'+idNotSplit+'-'+idSplit).attr('value','');
              // $('#timbangan-'+idNotSplit+'-'+idSplit).trigger("change");
              // console.log('#timbangan-'+idNotSplit+'-'+idSplit);
              $('select#timbangan-'+idNotSplit+'-'+idSplit).html(data);
              $('input#qty-'+idNotSplit+'-'+idSplit).trigger("change");
            }
          });
        }
      });
    } else {
      $.getJSON("index.php?r=log-formula-breakdown/update-uom-split&id_split="+idSplit+"&uom="+uom, function (data){
        if (data.status == 'not-success'){
          window.alert('Penggantian UoM gagal');
        }else{
          $.get("index.php?r=master-data-timbangan-rm/get-timbangan-by-line&nama_line="+nama_line+"&idItem="+idNotSplit+"&idSplit="+idSplit+"&operator="+operator, function (data){

            if (data=="-" && flag) {
              flag = 0;
              window.alert('Daftar timbangan untuk line tersebut belum ada di database.');
            } else {
              // $("select[id^='timbangan-']").each(function () {
              //   $(this).html(data);
              // });
              // $('#timbangan-'+idNotSplit+'-'+idSplit).attr('value','LWE01');
              // console.log('#timbangan-'+idNotSplit+'-'+idSplit);
              $('select#timbangan-'+idNotSplit+'-'+idSplit).html(data);
              $('input#qty-'+idNotSplit+'-'+idSplit).trigger('change');
            }
          });
        }
      });
    }

    // $.getJSON("index.php?r=log-formula-breakdown/update-uom&id="+idNotSplit+"&uom="+uom, function (data){
    //   //console.log(data.status);
    //   if (data.status=="cannot"){
    //     if(uom=='kg'){
    //       var uom_change = $('select[id="'+id+'"]').val('g');
    //     } else {
    //       var uom_change = $('select[id="'+id+'"]').val('kg');
    //     }
    //     window.alert('Mohon lakukan perubahan UoM sebelum Quantity di Split. (Hapus dulu split nya)');
    //   } else if (data.status=="not-success"){

    //   } else {
    //     //$('input[id^="'+idQty+'"]').val(Number(data.qty).toFixed(4));
    //     $('select[id^="'+id_+'"]').val(uom);
    //     window.alert("Jangan lupa tekan tombol calculate untuk melihat totalnya, setelah pergantian UoM.");
    //   }
    // });



    //console.log(uom);
    if (uom == 'kg')
    {

      // $("[input^='idQty']").each(function () {
      //   //$(this).val = $(this).val *1000;
      //     console.log($(this).val());
      // });
    }

    // if (uom=='kg'){
    //   $.post("index.php?r=log-formula-breakdown/update-timbangan-split&id_split="+id2+"&timbangan="+nilai, function (data){});
    // } else {
    //   $.post("index.php?r=log-formula-breakdown/update-timbangan&id="+id3+"&timbangan="+nilai, function (data){
    //   });
    // }
  });


JS;
$this->registerJs($script);
?>
