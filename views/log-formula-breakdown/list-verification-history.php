<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\LogFormulaBreakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<style>

table tbody tr{
    height: 1em;
}
</style>


        
<div class="box box-widget widget-user" style="width:40%;display: block; margin-left: auto; margin-right: auto;">
    <div class="widget-user-header bg-blue" style="height:50%;">
                      <h3 class="widget-user-username" style="text-align:center"><b>VERIFICATION HISTORY</b></h3>
    </div>
    <?= GridView::widget([
        'dataProvider' => $verificationHistory,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nomo',
            'pic',
            'timestamp',
        ],
    ]); ?>

</div>

<script>
  function back(){
    window.location = "index.php?r=log-formula-breakdown/verification-history";
    // window.history.back();
  }

</script>


<?php
$script = <<< JS


JS;
$this->registerJs($script);
?>