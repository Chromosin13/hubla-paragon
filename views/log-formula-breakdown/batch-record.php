<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LogPenimbanganRm */
?>

<style type="text/css">

</style>

<?php 
    $seq_item = 1;
    $seq_page = 1;
    $total_page = count($operator_komponen);

    if ($header_komponen[0]['sediaan']=='L'){
        $sediaan = 'Liquid';
    }else if ($header_komponen[0]['sediaan']=='P'){
        $sediaan = 'Powder';
    }else if ($header_komponen[0]['sediaan']=='S'){
        $sediaan = 'Semisolid';
    }else if ($header_komponen[0]['sediaan']=='V'){
        $sediaan = 'Varcos';
    }else {
        $sediaan = '-';
    }

    foreach($siklus_komponen as $siklus_){ ?>
    <?php foreach($operator_komponen as $op){ 

        //Populate list action per halaman untuk memberikan keterangan di footer
        $list_action = [];
        if ($op->siklus == $siklus_->siklus){ ?>

<body>
<div class="log-penimbangan-rm-view">
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="border: 1px solid black;width:320px;">
                    <img src="../web/images/logo-pti.png" style="width:280px; text-align:center; margin-left:17px;"></td>
                </td>
                <?php 
                    if (strpos($header_komponen[0]['no_dokumen'],'.F')!== false){
                        $formula = explode('.F', $header_komponen[0]['no_dokumen'])[1];
                    }else{
                        $formula = explode(',F', $header_komponen[0]['no_dokumen'])[1];
                    }
                    // $formula_code = substr($formula,1);
                    // $formula_code = preg_replace('/[^0-9]/','',$formula);
                    if (strpos($formula, '.')!==false){
                        $formula_code = explode('.',$formula)[0];
                    }else{
                        $formula_code = explode(',',$formula)[0];
                    }

                 ?>
                <td style="text-align: center;border: 1px solid black;width:480px;">
                    <p style="margin-top:0px; margin-bottom:3px; font-size: 1.8em;"><b>BATCH RECORD <?php echo $header_komponen[0]['status_formula']; ?></b></p>
                    <p style="margin-top:0px; margin-bottom:0px; font-size: 1.4em;"><b><?php echo $header_komponen[0]['nama_bulk']; ?></b></p>
                    <!-- <p style="margin-top:0px; margin-bottom:0px; font-size: 1.2em;"><b>Formula <?php echo substr($header_komponen[0]['no_dokumen'], strpos($header_komponen[0]['no_dokumen'],".F")+2, 2); ?></b></p> -->
                    <p style="margin-top:0px; margin-bottom:0px; font-size: 1.2em;"><b>Formula <?php echo $formula_code; ?></b></p>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="border: 1px solid black;width:130px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Nama Brand</p>
                </td>
                <td style="border: 1px solid black;width:190px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $header_komponen[0]['brand']; ?></p>
                </td>
                <td style="border: 1px solid black;width:150px; padding-left:7px;">
                    <p style="font-size: 1.1em;">No. Dokumen</p>
                </td>
                <td style="border: 1px solid black;width:330px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $header_komponen[0]['no_dokumen']; ?></p>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="border: 1px solid black;width:130px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Kode Bulk</p>
                </td>
                <td style="border: 1px solid black;width:190px; margin:0px; padding-left:7px;">
                    <?php if (strlen($header_komponen[0]['kode_bulk'])>17) : ?>
                        <p style="font-size: 0.9em;"><?php echo $header_komponen[0]['kode_bulk']; ?></p>
                    <?php else : ?>
                        <p style="font-size: 1.1em;"><?php echo $header_komponen[0]['kode_bulk']; ?></p>
                    <?php endif; ?>
                </td>
                <td style="border: 1px solid black;width:150px; padding-left:7px;">
                    <p style="font-size: 1.1em;">No. Revisi</p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $header_komponen[0]['no_revisi']; ?></p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Halaman</p>
                </td>
                <td style="border: 1px solid black;width:130px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $seq_page; ?> dari <?php echo $total_page; ?></p>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="border: 1px solid black;width:130px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Bentuk Sediaan</p>
                </td>
                <td style="border: 1px solid black;width:190px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $header_komponen[0]['bentuk_sediaan']; ?></p>
                </td>
                <td style="border: 1px solid black;width:150px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Tgl. Mulai Diolah</p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;"></p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;">No Batch</p>
                </td>
                <td style="border: 1px solid black;width:130px; padding-left:7px;">
                    <p style="font-size: 1.1em;"></p>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="border: 1px solid black;width:130px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Besar Batch</p>
                </td>
                <td style="border: 1px solid black;width:190px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo number_format($pic['qty_batch_real'],4); ?> kg</p>
                </td>
                <td style="border: 1px solid black;width:150px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Tgl. Selesai Diolah</p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;"></p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Tgl. Berlaku</p>
                </td>
                <td style="border: 1px solid black;width:130px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $date; ?></p>
                </td>
            </tr>
        </tbody>
    </table>

    <br>
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="text-align: center; border: 1px solid black;width:320px;">
                    <p style="margin-top:0px; margin-bottom:3px; font-size: 1.1em;"><b>Siklus</b></p>
                </td>
                <td style="text-align: center; border: 1px solid black;width:480px;">
                    <p style="margin-top:0px; margin-bottom:3px; font-size: 1.1em;"><b><?php echo $siklus_->siklus; ?></b></p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; border: 1px solid black;width:320px;">
                    <?php if($op->operator == 11) : ?>
                        <p style="margin-top:0px; margin-bottom:3px; font-size: 1.1em;"><b>Nama Operator Timbang <?php echo 'Air RO'; ?></b></p>
                    <?php else : ?>
                        <p style="margin-top:0px; margin-bottom:3px; font-size: 1.1em;"><b>Nama Operator Timbang <?php echo $op->operator; ?></b></p>
                    <?php endif ; ?>
                </td>
                <td style="text-align: center; border: 1px solid black;width:480px;">
                    <p style="margin-top:0px; margin-bottom:3px; font-size: 1.1em;"><b></b></p>
                </td>
            </tr>
        </tbody>
    </table>
    <br>
    <p style="margin-left:0px;">
        <b style="font-size:1em; color:black;">II. KOMPOSISI DAN PENIMBANGAN</b><br>
    </p>
    <p style="margin-left:0px;">
        <b style="font-size:1em;  color:black;"><i>Bahan baku sudah tersertifikasi halal. Pastikan bahan baku tidak terkontaminasi bahan yang tidak halal selama proses penimbangan<i></b><br>
    </p>

    <?php
        $seq_uom = 'A';

        foreach($weigherList as $weigher){

            if ($weigher->siklus == $siklus_->siklus and $weigher->operator == $op->operator) { ?>
                <p style="margin-left:0px;">
                    <b style="font-size:1em; color:black;"><u><?php echo $seq_uom; ?>. Timbangan Satuan <?php if ($weigher->uom == 'kg' ){ echo "Kg";} else { echo "Gram";}; if ($op->operator == 11){echo ' = Timbangan Air RO';}else{echo' = '.$weigher->timbangan;}?> </u></b><br>

<!--                     <?php if ($weigher->uom == 'kg' ){ echo "Kg";} else { echo "Gram";} ?></u></b><br>

                    <?php if ($weigher->uom == 'kg') { ?>
                            <b style="font-size:1em;  color:black;">Timbangan  
                            <?php 
                                $lastElementKg = end($timbangan_kg);
                                $lastElementGram = end($timbangan_gram);
                                foreach($timbangan_kg as $timb_kg) {
                                echo $timb_kg; ?>
                                01 / 02 / 03 / 04 / 05 <?php if ($timb_kg!=$lastElementKg){ echo "atau";} ?>  
                            <?php } ?>
                            (lingkari mesin yang digunakan)</b><br>
                    <?php } else { ?>
                            <b style="font-size:1em;  color:black;">Timbangan  
                            <?php 
                                $lastElementKg = end($timbangan_kg);
                                $lastElementGram = end($timbangan_gram);
                                foreach($timbangan_gram as $timb_gram) {
                                echo $timb_gram; ?>
                                01 / 02 / 03 / 04 / 05 <?php if ($timb_gram!=$lastElementGram){ echo "atau";} ?>  
                            <?php } ?>
                            (lingkari mesin yang digunakan)</b><br>
                    <?php } ?>
 -->
                    <table style="margin:0px; border-collapse: collapse;">
                        <thead>
                          <tr>
                            <th class="text-center" style="font-size: 1em; width: 3%; border: 1px solid black;">
                              <b>No</b>
                            </th>
                            <th class="text-center" style="font-size: 1em; width: 13%; border: 1px solid black;">
                              <b>Kode Internal</b>
                            </th>
                            <th class="text-center" style="font-size: 1em; width: 6%; border: 1px solid black;">
                              <b>Kode Olah</b>
                            </th>
                            <th class="text-center" style="font-size: 1em; width: 15%; border: 1px solid black;">
                              <b>Nama Bahan</b>
                            </th>
                            <th class="text-center" style="font-size: 1em; width: 9%; border: 1px solid black;">
                              <b>Keterangan</b>
                            </th>
                            <th class="text-center" style="font-size: 1em; width: 14%; border: 1px solid black;">
                              <b>No.Batch</b>
                            </th>
                            <th class="text-center" style="font-size: 1em; width: 8%; border: 1px solid black;">
                              <b>Jumlah Dibutuhkan (<?php echo $weigher->uom ?>)</b>
                            </th>
                            <th class="text-center" style="font-size: 1em; width: 8%; border: 1px solid black;">
                              <b>Jumlah Ditimbang (<?php echo $weigher->uom ?>)</b>
                            </th>
                            <th class="text-center" style="font-size: 1em; width: 8%; border: 1px solid black;">
                              <b>Std-Cycle Time (Menit)</b>
                            </th>
                            <th class="text-center" style="font-size: 1em; width: 8%; border: 1px solid black;">
                              <b>Pelaksana</b>
                            </th>
                            <th class="text-center" style="font-size: 1em; width: 8%; border: 1px solid black;">
                              <b>Pemeriksa</b>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php 
                            for ($i = 0; $i < count($bom_komponen); $i++){
                                if ($bom_komponen[$i]['siklus'] == $siklus_->siklus and $bom_komponen[$i]['operator'] == $op->operator and $bom_komponen[$i]['timbangan'] == $weigher->timbangan) {
                                    if (strlen($bom_komponen[$i]['kode_internal'])>14) {
                                        $str_arr = explode('-',$bom_komponen[$i]['kode_internal']);
                                        $kode_bb_1 ='';
                                        foreach ($str_arr as $str) {
                                            $kode_bb_1 .= $str.'-';
                                            if (strlen($kode_bb_1)>10) {
                                                break;
                                            }
                                        }
                                        $jml_sisa_karakter = strlen($kode_bb_1)-strlen($bom_komponen[$i]['kode_internal']);
                                        $kode_bb_2 = substr($bom_komponen[$i]['kode_internal'],$jml_sisa_karakter);
                                    }else{
                                        $kode_bb_1 = $bom_komponen[$i]['kode_internal'];
                                        $kode_bb_2 = '';
                                    } ?>

                                    <tr>
                                        <td class="text-center" style="font-size:1.3em; border: 1px solid black;"><p><?php echo $seq_item; ?></p></td>
                                        <td class="text-left" style="font-size:1.3em; padding-left:3px; border: 1px solid black;"><p><?php echo $kode_bb_1; ?></p><p><?php echo $kode_bb_2; ?></p></td>
                                        
                                        <td class="text-center" style="font-size:1.3em; border: 1px solid black;"><p><?php echo $bom_komponen[$i]['kode_olah'] ?></p></td>
                                        
                                        <?php 
                                            $str = '<td class="text-left ';

                                            if (strpos($bom_komponen[$i]['action'],'B')!==false){
                                                $str .= 'bold ';
                                                $list_action[] = $bom_komponen[$i]['action'] == 'B'? 'B' : 'B+U+I' ;
                                            } 
                                            if (strpos($bom_komponen[$i]['action'],'I')!==false){
                                                $str .= 'italic ';
                                                $list_action[] = 'B+U+I';
                                            }
                                            if (strpos($bom_komponen[$i]['action'],'U')!==false){
                                                $str .= 'underline ';
                                                $list_action[] = 'B+U+I';
                                            }
                                            

                                            $str .= '" style="font-size:1.3em; padding-left:3px; border: 1px solid black;"><p>'.$bom_komponen[$i]['nama_bb']; 

                                            if ($bom_komponen[$i]['action']=='^'){
                                                $str .= ' (^)';
                                                $list_action[] = '^';
                                            }
                                            if ($bom_komponen[$i]['action']=='^^'){
                                                $str .= ' (^^)';
                                                $list_action[] = '^^';
                                            }
                                            if ($bom_komponen[$i]['action']=='*'){
                                                $str .= ' (*)';
                                                $list_action[] = '*';
                                            }
                                            if ($bom_komponen[$i]['action']=='**'){
                                                $str .= ' (**)';
                                                $list_action[] = '**';
                                            }
                                            if ($bom_komponen[$i]['action']=='***'){
                                                $str .= ' (***)';
                                                $list_action[] = '***';
                                            }
                                            if ($bom_komponen[$i]['action']=='~'){
                                                $str .= ' (~)';
                                                $list_action[] = '~';
                                            }

                                            $str .= '</p></td>';

                                            echo $str;
                                        ?>

                                        <!-- <td class="text-left" style="font-size:1.3em;font-weight: bold ;padding-left:3px; border: 1px solid black;"><p><?php echo $bom_komponen[$i]['nama_bb'] ?></p></td> -->

                                        <?php if (strlen($bom_komponen[$i]['keterangan']) > 7) : ?>
                                            <td class="text-left" style="font-size:1.0em; padding-left:3px; border: 1px solid black;"><p><?php echo $bom_komponen[$i]['keterangan'] ?></p></td>
                                        <?php else : ?>
                                            <td class="text-left" style="font-size:1.3em; padding-left:3px; border: 1px solid black;"><p><?php echo $bom_komponen[$i]['keterangan'] ?></p></td>
                                        <?php endif; ?>
                                        <td class="text-center" style="font-size:1.3em; border: 1px solid black;"><p></p></td>
                                        <td class="text-center" style="font-size:1.3em; border: 1px solid black;"><p><?php echo $bom_komponen[$i]['qty'] ?></p></td>
                                        <td class="text-center" style="font-size:1.3em; border: 1px solid black;"><p></p></td>
                                        <td class="text-center" style="font-size:1.3em; border: 1px solid black;"><p><?php echo $bom_komponen[$i]['cycle_time_std'] ?></p></td>
                                        <td class="text-center" style="font-size:1.3em; border: 1px solid black;"><p></p></td>
                                        <td class="text-center" style="font-size:1.3em; border: 1px solid black;"><p></p></td>
                                    </tr>

                                <?php 
                                    // $serial_arr = explode('.',strval($bom_komponen[$i]['serial_manual']) );
                                    // if(count($serial_arr)>1){
                                    //     if ($serial_arr[1]=='1'){
                                    //         $seq_item++;
                                    //     }
                                    // }else{
                                    //     $seq_item++;
                                    // }
                                
                                    $seq_item++;
                                    
                                }
                            } ?>
                        </tbody>
                    </table>
                    <br>
                </p>

            <!-- Tutup kurung untuk if siklus= siklus dan operator = operator -->
            <?php $seq_uom++; } ?>
        <?php 
            $connection2 = Yii::$app->db;
            $command2 = $connection2->createCommand("

            SELECT sum(cycle_time_std)
            FROM log_formula_breakdown
            WHERE kode_bulk = '".$bom_komponen[0]['kode_bulk']."' and formula_reference = '".$bom_komponen[0]['formula_reference']."' and qty_batch = ".$bom_komponen[0]['qty_batch']." AND (is_split = 0 or is_split = 99) AND siklus = '".$siklus_->siklus."' and operator = ".$op->operator."
            GROUP BY kode_bulk,formula_reference,qty_batch,siklus,operator
            ");

            $total_cycle_time = $command2->queryScalar(); 
            // print_r($total_cycle_time);
        ?>

    <!-- Tutup kurung untuk weigherList -->
    <?php } ?>

    <p id="keterangan" style="margin-left:0px;">
        <b style="font-size:1em;  color:black;">Keterangan  :</b>
        <?php 
            $str_keterangan = '';
            if (in_array('B', $list_action)){
                $str_keterangan .= '<p id="keterangan" style="font-size:1em;  color:black;">(Bold)      : Bahan yang mengalami perubahan konsentrasi atau subtitusi.</p>';
            }
            if (in_array('B+U+I', $list_action)){
                $str_keterangan .= '<p id="keterangan" style="font-size:1em;  color:black;">(Bold, Underline, Italic) : Bahan baku yang berpotensi menyebabkan iritasi.</p>';
            }
            if (in_array('^', $list_action)){
                $str_keterangan .= '<p id="keterangan" style="font-size:1em;  color:black;">(^) : Bahan baku yang disterilisasi.</p>';
            }
            if (in_array('^^', $list_action)){
                $str_keterangan .= '<p id="keterangan" style="font-size:1em;  color:black;">(^^) : Bahan baku yang tidak perlu disterilisasi.</p>';
            }
            if (in_array('*', $list_action)){
                $str_keterangan .= '<p id="keterangan" style="font-size:1em;  color:black;">(*) : Bahan baku menggunakan drum.</p>';
            }
            if (in_array('**', $list_action)){
                $str_keterangan .= '<p id="keterangan" style="font-size:1em;  color:black;">(**) : Bahan baku yang tidak boleh terpapar udara.</p>';
            }
            if (in_array('***', $list_action)){
                $str_keterangan .= '<p id="keterangan" style="font-size:1em;  color:black;">(***) : Saat melakukan proses penimbangan, pastikan menggunakan masker 3M khusus fume / uap, kacamata goggle, dan sarung tangan panjang.</p>';
            }
            if (in_array('~', $list_action)){
                $str_keterangan .= '<p style="font-size:1em;  color:black;">(~) : Bahan baku diaduk terlebih dahulu sebelum ditimbang dan digunakan.</p>';
            }

            echo $str_keterangan;
         ?>
        
    </p>
    <table style="margin:5px; width:100%; border-collapse: collapse;">
        <thead>
            <tr>
                <th rowspan="2" class="text-center" style="width: 25%; font-size:1em;border: 1px solid black;">
                    <b>Total</b>
                </th>
                <th class="text-center" style="width: 25%; font-size:1em;border: 1px solid black;">
                    <b>Std Cycle Time</b>
                </th>
                <th class="text-center" style="width: 25%; font-size:1em;border: 1px solid black;">
                    <b>Real Cycle Time</b>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center" style="border: 1px solid black;"><p><?php echo $total_cycle_time ?></p></td>
                <td class="text-center" style="border: 1px solid black;"><p style="visibility:hidden;">a</p></td>
            </tr>
        </tbody>
    </table>
    <br>
    <div class="container">
        <table style="table-layout:fixed; width:100%; margin:5px; width:100%; border-collapse: collapse; vertical-align: bottom;">
            <tbody>
                <tr>
                    <td class="text-center" style="table-layout:fixed; width : 50%; border: 0px solid black;">
                        <p class="text-center" style="font-size:0.9em;margin-bottom:0px; margin-top:17px; padding-bottom:0px;">   </p>
                        <?php if ($op->operator == 11) : ?>
                            <p class="text-center" style="font-size:0.9em;">Operator Timbang <?php echo 'Air RO'; ?></p>
                        <?php else : ?>
                            <p class="text-center" style="font-size:0.9em;">Operator Timbang <?php echo $op->operator; ?></p>
                        <?php endif ; ?>
                        <br>
                        <br>
                        <br>
                        <br>
                        <p class="text-center" style="border-top: 2px solid black;font-size:0.9em; margin-top:0px; padding-top:0px;"><b><span style="line-height:2;">Tanggal:.........</span></b></p>

                    </td>
                    <td class="text-center" style="table-layout:fixed; width : 50%; border: 0px solid black;">
                        <p class="text-center" style="font-size:0.9em;margin-bottom:0px; padding-bottom:0px;">Pemeriksa</p>
                        <?php if ($op->operator == 11) : ?>
                            <p class="text-center" style="font-size:0.9em;margin-top:0px; padding-top:0px;">Kapten/Leader Olah</p>
                        <?php else : ?>
                            <p class="text-center" style="font-size:0.9em;margin-top:0px; padding-top:0px;">Kapten/Leader Timbang</p>
                        <?php endif ; ?>
                        <br>
                        <br>
                        <br>
                        <br>
                        <p class="text-center" style="border-top: 2px solid black;font-size:0.9em; margin-top:0px; padding-top:0px;"><b><span style="line-height:2;">Tanggal:.........</span></b></p>                        
                    </td>

                </tr>
            </tbody>
        </table>

        <!-- <div class="column" style="float:left;width:50%;">
            <p class="text-center" style="font-size:0.9em;margin-bottom:0px; margin-top:17px; padding-bottom:0px;">   </p>
            <?php if ($op->operator == 11) : ?>
                <p class="text-center" style="font-size:0.9em;">Operator Timbang <?php echo 'Air RO'; ?></p>
            <?php else : ?>
                <p class="text-center" style="font-size:0.9em;">Operator Timbang <?php echo $op->operator; ?></p>
            <?php endif ; ?>
            <br>
            <hr style="width:45%; color:black; height: 2px;  margin-bottom:0px; padding-bottom:0px;">
            <p class="text-center" style="font-size:0.9em; margin-top:0px; padding-top:0px;"><b><i>Tanggal:.........</i></b></p>
        </div>
        <div class="column" style="float:left;width:50%;">
            <p class="text-center" style="font-size:0.9em;margin-bottom:0px; padding-bottom:0px;">Pemeriksa</p>
            <?php if ($op->operator == 11) : ?>
                <p class="text-center" style="font-size:0.9em;margin-top:0px; padding-top:0px;">Kapten/Leader Olah</p>
            <?php else : ?>
                <p class="text-center" style="font-size:0.9em;margin-top:0px; padding-top:0px;">Kapten/Leader Timbang</p>
            <?php endif ; ?>
            <br>
            <hr style="width:45%; color:black; height: 2px; margin-bottom:0px; padding-bottom:0px;">
            <p class="text-center" style="font-size:0.9em; margin-top:0px; padding-top:0px;"><b><i>Tanggal:.........</i></b></p>
        </div> -->
    </div>
    
</div>
</body>
<footer style="position: fixed; display: absolute; margin-bottom: -55px!important;
                bottom: 0;">
    <div>
        <table style="table-layout:fixed; width:100%; margin:5px; width:100%; border-collapse: collapse;">
            <tbody>
                <tr>
                    <td class="text-center" style="table-layout:fixed; width : 35%; border: 1px solid black;">
                        <p>Disusun oleh :</p>
                        <p><?php echo $pic->creator; ?></p>
                    </td>
                    <td class="text-center" style="table-layout:fixed; width : 35%; border: 1px solid black;">
                        <p>Diperiksa oleh :</p>
                        <p><?php echo $reviewer; ?></p>
                    </td>
                    <td class="text-center" style="table-layout:fixed; width : 35%; border: 1px solid black;">
                        <p>Disetujui oleh :</p>
                        <?php if (!empty($pic->approver)){ ?>
                            <p>
                            <?php echo $approver;} else { ?>
                            <p style="visibility: hidden;">a
                            
                        <?php } ?></p>
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black;">
                        <p class="text-center" style="font-size:0.9em;margin-bottom:0px; padding-bottom:0px;">Tanggal</p>
                        <p class="text-center" style="font-size:0.9em;margin-top:0px; padding-top:0px;"><?php echo date('d/m/Y'); ?></p>
                    </td>
                    <td class="text-center" style="border: 1px solid black;">
                        <p class="text-center" style="font-size:0.9em;margin-bottom:0px; padding-bottom:0px;">Tanggal</p>
                        <p class="text-center" style="font-size:0.9em;margin-top:0px; padding-top:0px;"><?php echo date('d/m/Y'); ?></p>
                    </td>
                    <td class="text-center" style="border: 1px solid black;">
                        <p class="text-center" style="font-size:0.9em;margin-bottom:0px; padding-bottom:0px;">Tanggal</p>
                        <p class="text-center" style="font-size:0.9em;margin-top:0px; padding-top:0px;"><?php echo  date('d/m/Y'); ?></p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</footer>

    
    <?php $seq_page++; ?>
    <pagebreak>
    <?php } ?>
    <?php } ?>
    <!-- <pagebreak> -->



<?php } ?>

<?php $jml_data = count($bom_komponen); ?>

<?php for ($y=0;$y<ceil($jml_data/8);$y++) : ?>


<body>
<div class="log-penimbangan-rm-view">
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="border: 1px solid black;width:320px;">
                    <img src="../web/images/logo-pti.png" style="width:280px; text-align:center; margin-left:17px;"></td>
                </td>
                <?php 
                    $formula = explode('.', $header_komponen[0]['no_dokumen'])[3];
                    // $formula_code = substr($formula,1);
                    $formula_code = preg_replace('/[^0-9]/','',$formula);
                    
                 ?>
                <td style="text-align: center;border: 1px solid black;width:480px;">
                    <p style="margin-top:0px; margin-bottom:3px; font-size: 1.8em;"><b>BATCH RECORD <?php echo $header_komponen[0]['status_formula']; ?></b></p>
                    <p style="margin-top:0px; margin-bottom:0px; font-size: 1.4em;"><b><?php echo $header_komponen[0]['nama_bulk']; ?></b></p>
                    <p style="margin-top:0px; margin-bottom:0px; font-size: 1.2em;"><b>Formula <?php echo $formula_code; ?></b></p>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="border: 1px solid black;width:130px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Nama Brand</p>
                </td>
                <td style="border: 1px solid black;width:190px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $header_komponen[0]['brand']; ?></p>
                </td>
                <td style="border: 1px solid black;width:150px; padding-left:7px;">
                    <p style="font-size: 1.1em;">No. Dokumen</p>
                </td>
                <td style="border: 1px solid black;width:330px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $header_komponen[0]['no_dokumen']; ?></p>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="border: 1px solid black;width:130px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Kode Bulk</p>
                </td>
                <td style="border: 1px solid black;width:190px; margin:0px; padding-left:7px;">
                    <?php if (strlen($header_komponen[0]['kode_bulk'])>17) : ?>
                        <p style="font-size: 0.9em;"><?php echo $header_komponen[0]['kode_bulk']; ?></p>
                    <?php else : ?>
                        <p style="font-size: 1.1em;"><?php echo $header_komponen[0]['kode_bulk']; ?></p>
                    <?php endif; ?>
                </td>
                <td style="border: 1px solid black;width:150px; padding-left:7px;">
                    <p style="font-size: 1.1em;">No. Revisi</p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $header_komponen[0]['no_revisi']; ?></p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Halaman</p>
                </td>
                <td style="border: 1px solid black;width:130px; padding-left:7px;">
                    <!-- <p style="font-size: 1.1em;"><?php echo $seq_page; ?> dari <?php echo $total_page; ?></p> -->
                    <p style="font-size: 1.1em;">-</p>
                
                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="border: 1px solid black;width:130px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Bentuk Sediaan</p>
                </td>
                <td style="border: 1px solid black;width:190px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $header_komponen[0]['bentuk_sediaan']; ?></p>
                </td>
                <td style="border: 1px solid black;width:150px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Tgl. Mulai Diolah</p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;"></p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;">No. Batch</p>
                </td>
                <td style="border: 1px solid black;width:130px; padding-left:7px;">
                    <p style="font-size: 1.1em;"></p>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="border: 1px solid black;width:130px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Besar Batch</p>
                </td>
                <td style="border: 1px solid black;width:190px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo number_format($pic['qty_batch_real'],4); ?> kg</p>
                </td>
                <td style="border: 1px solid black;width:150px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Tgl. Selesai Diolah</p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;"></p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Tgl. Berlaku</p>
                </td>
                <td style="border: 1px solid black;width:130px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $date; ?></p>
                </td>
            </tr>
        </tbody>
    </table>

    <br>
    <p style="margin-left:0px;">
        <b style="font-size:1.2em; color:black;">TABEL STIKER BAHAN BAKU</b>
    </p>
    <p style="margin-left:0px; font-size:1.2em;  color:black;">
        Tempelkan stiker bahan baku pada tabel ini sesuai dengan kode olah<br>
    </p>
    <br>


    <table style="margin:0px; border-collapse: collapse;">
        <thead>
          <tr>
            <th class="text-center" style="font-size: 1.3em; width: 40px; border: 1px solid black;">
              <b>No</b>
            </th>
            <th class="text-center" style="font-size: 1.3em; width: 400px; border: 1px solid black;">
              <b>Stiker</b>
            </th>
            <th class="text-center" style="font-size: 1.3em; width: 40px; border: 1px solid black;">
              <b>No</b>
            </th>
            <th class="text-center" style="font-size: 1.3em; width: 400px; border: 1px solid black;">
              <b>Stiker</b>
            </th>
          </tr>
        </thead>
        <tbody>
            <?php for ($x=0;$x<4;$x++) : ?>
            
            <tr>
                <td class="text-center" style="font-size:1.3em; border: 1px solid black; height:230px;"><p><?php if (!empty($bom_komponen[$x+$y*8]['kode_olah'])) {echo $x+1+$y*8;}else{echo '';} ?></p></td>
                <td class="text-center" style="font-size:2em; padding-left:3px; border: 1px solid black; height:230px;"><p><?php if (!empty($bom_komponen[$x+$y*8]['kode_olah'])) {echo $bom_komponen[$x+$y*8]['kode_olah'];} else { echo '';} ?></p></td>
                <td class="text-center" style="font-size:1.3em; border: 1px solid black; height:230px;"><p><?php if (!empty($bom_komponen[4+$x+$y*8])){echo 4+$x+1+$y*8;}else{echo '';} ?></p></td>
                <td class="text-center" style="font-size:2em; padding-left:3px; border: 1px solid black; height:230px;"><p><?php if (!empty($bom_komponen[4+$x+$y*8])) {echo $bom_komponen[4+$x+$y*8]['kode_olah'];} else {echo '';} ?></p></td>
            </tr>
            <?php endfor ;//end for loop for one page ?>
        </tbody>
    </table>

</div>
</body>

<!-- <?php if($y < ceil($jml_data/8)) : ?>
    <pagebreak>
<?php else : ?>
<?php endif ; ?> -->


<footer style="position: fixed; display: absolute; margin-bottom: -55px!important;
                bottom: 0;">
    <div>
        <table style="margin:5px; width:100%; border-collapse: collapse;">
            <tbody>
                <tr>
                    <td class="text-center" style="border: 1px solid black;">
                        <p>Disusun oleh :</p>
                        <p><?php echo $pic->creator; ?></p>
                    </td>
                    <td class="text-center" style="border: 1px solid black;">
                        <p>Diperiksa oleh :</p>
                        <p><?php echo $reviewer; ?></p>
                    </td>
                    <td class="text-center" style="border: 1px solid black;">
                        <p>Disetujui oleh :</p>
                        <?php if (!empty($pic->approver)){ ?>
                            <p>
                            <?php echo $approver;} else { ?>
                            <p style="visibility: hidden;">a
                            
                        <?php } ?></p>
                    </td>
                </tr>
                <tr>
                    <td class="text-center" style="border: 1px solid black;">
                        <p class="text-center" style="font-size:0.9em;margin-bottom:0px; padding-bottom:0px;">Tanggal</p>
                        <p class="text-center" style="font-size:0.9em;margin-top:0px; padding-top:0px;"><?php echo date('d/m/Y'); ?></p>
                    </td>
                    <td class="text-center" style="border: 1px solid black;">
                        <p class="text-center" style="font-size:0.9em;margin-bottom:0px; padding-bottom:0px;">Tanggal</p>
                        <p class="text-center" style="font-size:0.9em;margin-top:0px; padding-top:0px;"><?php echo date('d/m/Y'); ?></p>
                    </td>
                    <td class="text-center" style="border: 1px solid black;">
                        <p class="text-center" style="font-size:0.9em;margin-bottom:0px; padding-bottom:0px;">Tanggal</p>
                        <p class="text-center" style="font-size:0.9em;margin-top:0px; padding-top:0px;"><?php echo  date('d/m/Y'); ?></p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</footer>
<pagebreak>
<?php endfor ;//end for loop for all bom_komponen ?>

<script>
// $("td[id^='nama_bb-']").each(function(){
//     var id = $(this).attr(id);
//     console.log(id);

// });
    

</script>


<?php 
$script = <<< JS
$("td[id^='nama_bb-']").each(function(){
    var id_nama_bb = $(this).attr('id');

    var id = id_nama_bb.substring(id_nama_bb.lastIndexOf("-") + 1);

    // var action = document.getElementById('action-'+id).value;
    // var nama_bb = $('#nama_bb-'+id).val();

    // console.log('nama_bb-'+id);

    $('#nama_bb-'+id).attr('berhasil');



});
 
JS;
$this->registerJs($script);

 ?>
   

