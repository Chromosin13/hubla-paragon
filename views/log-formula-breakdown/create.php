<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LogFormulaBreakdown */

$this->title = 'Create Log Formula Breakdown';
$this->params['breadcrumbs'][] = ['label' => 'Log Formula Breakdowns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-formula-breakdown-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
