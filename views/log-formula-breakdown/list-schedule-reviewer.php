<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use kartik\editable\Editable;

/* @var $this yii\web\View */
/* @var $model app\models\LogFormulaBreakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
.tableFixHead          { overflow-y: auto; height: 81vh; z-index:5;}
.tableFixHead thead th { position: sticky; top: -1px; z-index:5;}

.table-bordered{
  border-left:1.5px solid rgba(135,206,250,.25);
  border-bottom:1.5px solid rgba(135,206,250,.25);
  border-top:1px solid rgba(135,206,250,.25);
}
.table-bordered td,.table-bordered th{
  border:1px solid rgba(135,206,250,.25)
}
.table-bordered thead td,.table-bordered thead th{
  border-bottom-width:2px
}

.table-striped tbody tr:nth-of-type(odd){
  background-color:rgba(135,206,250,.25)
}

.btn-info{color:#fff;background-color:#5DADE2;border-color:#5DADE2}.btn-info:hover{color:#fff;background-color:#138496;border-color:#117a8b}.btn-info.focus,.btn-info:focus{color:#fff;background-color:#138496;border-color:#117a8b;box-shadow:0 0 0 .2rem rgba(58,176,195,.5)}.btn-info.disabled,.btn-info:disabled{color:#fff;background-color:#17a2b8;border-color:#17a2b8}.btn-info:not(:disabled):not(.disabled).active,.btn-info:not(:disabled):not(.disabled):active,.show>.btn-info.dropdown-toggle{color:#fff;background-color:#117a8b;border-color:#10707f}.btn-info:not(:disabled):not(.disabled).active:focus,.btn-info:not(:disabled):not(.disabled):active:focus,.show>.btn-info.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(58,176,195,.5)}

.btn-add:hover {
  box-shadow: 0 5px 9px 0 rgba(0,0,0,0.24), 0 5px 9px 0 rgba(0,0,0,0.19);
}

.btn-minus:hover {
  box-shadow: 0 5px 9px 0 rgba(0,0,0,0.24), 0 5px 9px 0 rgba(0,0,0,0.19);
}

table tbody tr{
    height: 1em;
}
.img-circle{
  border-radius:50%;
}
</style>

<?php

  // Modal::begin([
  //         'header'=>'<h4>Review BR</h4>',
  //         'id' => 'modalverifieditems',
  //         'size' => 'modal-lg',
  //     ]);

  // echo "<div id='modalVerifiedItems'></div>";

  // Modal::end();

?>
        
<div class="box widget-user" style="display: inline-block;"> 
  <h2 class="text-center">
    IN PROGRESS / REJECT
  </h2>


    <div style="width:96%; margin: 0 auto;"> 

    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => [
          ['class' => 'yii\grid\SerialColumn'],

          'nomo',
          // [
          //       // 'class' => 'kartik\grid\EditableColumn',
          //       'attribute' => 'nama_fg',
          //       // 'headerOptions' => ['style' => 'text-align:center; width : 220px;'],
          //       'label' => 'Nama Finish Good',
          //       // 'editableOptions' => [
          //       //   // 'asPopover' => false,
          //       // ],
          // ],
          'nama_bulk',
          'formula_reference',
          'qty_batch',
          [
                // 'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'status_reviewer',
                // 'headerOptions' => ['style' => 'text-align:center; width : 220px;'],
                'label' => 'Status',
                // 'editableOptions' => [
                //   // 'asPopover' => false,
                // ],
                // 'contentOptions' => ['style' => 'color:#77DD77; font-weight:bold;'],
                'contentOptions' => function($dataProvider) {
                        if ($dataProvider->status_reviewer == 'REJECT'){
                          return ['style' => 'color:red; font-weight:bold;'];
                        } else {
                          return ['style' => 'color:#77DD77; font-weight:bold;'];
                        }
                        
                }
          ],
          // [
          //       // 'class' => 'kartik\grid\EditableColumn',
          //       'attribute' => 'reject_note_reviewer',
          //       // 'headerOptions' => ['style' => 'text-align:center; width : 220px;'],
          //       'label' => 'Note',
          //       // 'editableOptions' => [
          //       //   // 'asPopover' => false,
          //       // ],

          // ],
          [
            'format' => 'raw',
            'header' => '<a style="color:#469ED8;">View BR</a>',
            'label' => [function(){return Html::a('Batch Record','',['style'=>['color'=>'#5DADE2']]);}],
            'value' => function($dataProvider) {
                        return Html::button('',['class'=>'button btn btn-info fa fa-file-text',
                                  'style' => ['opacity' => '0.75', 'border-radius' => '0.7em', 'vertical-align'=>'middle','display'=>'block','margin'=>'0 auto', 'height'=>'100%', 'width'=>'90%'],
                                  'id' => 'view'
                                ]);
                        }
          ],
          [
            'format' => 'raw',
            'header' => '<a class="text-center" style="color:#469ED8;">Action</a>',
            'label' => [function(){return Html::a('Action','',['style'=>['color'=>'#77DD77']]);}],
            'value' => function($dataProvider) {
                        return Html::button('',['class'=>'button btn btn-info fa fa-arrow-right',
                                  'style' => ['opacity' => '0.75', 'border-radius' => '0.7em', 'vertical-align'=>'middle','display'=>'block','margin'=>'0 auto', 'height'=>'100%', 'width'=>'90%','background-color' =>'#77DD77'],
                                  'id' => 'redirect'
                                ]);
                        }
          ],
          // [
          //   'format' => 'raw',
          //   'header' => '<a class="text-center" style="color:#469ED8;">Approve</a>',
          //   'label' => [function(){return Html::a('Batch Record','',['style'=>['color'=>'#77DD77']]);}],
          //   'value' => function($dataProvider) {
          //               return Html::button('',['class'=>'button btn btn-info fa fa-check',
          //                         'style' => ['opacity' => '0.75', 'border-radius' => '0.7em', 'vertical-align'=>'middle','display'=>'block','margin'=>'0 auto', 'height'=>'100%', 'width'=>'90%','background-color' =>'#77DD77'],
          //                         'id' => 'approve'
          //                       ]);
          //               }
          // ],
          // [
          //   'format' => 'raw',
          //   'header' => '<a class="text-center" style="color:#469ED8;">Reject</a>',
          //   'label' => [function(){return Html::a('Batch Record','',['style'=>['color'=>'#77DD77']]);}],
          //   'value' => function($dataProvider) {
          //               return Html::button('',['class'=>'button btn btn-info fa fa-close',
          //                         'style' => ['opacity' => '0.75', 'border-radius' => '0.7em', 'vertical-align'=>'middle','display'=>'block','margin'=>'0 auto', 'height'=>'100%', 'width'=>'90%','background-color' =>'red'],
          //                         'id' => 'reject'
          //                       ]);
          //               }
          // ],
      ],
    ]); ?>
  </div>
</div>
  <br>
  <br>
<div class="box widget-user" style="display: inline-block;"> 
  <h2 class="text-center">
    HISTORY (APPROVED)
  </h2>

  <div style="width:96%; margin: 0 auto;"> 

    <?= GridView::widget([
      'dataProvider' => $dataProvider2,
      'filterModel' => $searchModel2,
      'columns' => [
          ['class' => 'yii\grid\SerialColumn'],

          'nomo',
          // [
          //       // 'class' => 'kartik\grid\EditableColumn',
          //       'attribute' => 'nama_fg',
          //       // 'headerOptions' => ['style' => 'text-align:center; width : 220px;'],
          //       'label' => 'Nama Finish Good',
          //       // 'editableOptions' => [
          //       //   // 'asPopover' => false,
          //       // ],
          // ],
          'nama_bulk',
          'formula_reference',
          'qty_batch',
          [
                // 'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'status_reviewer',
                // 'headerOptions' => ['style' => 'text-align:center; width : 220px;'],
                'label' => 'Status',
                // 'editableOptions' => [
                //   // 'asPopover' => false,
                // ],
                'contentOptions' => ['style' => 'color:#77DD77; font-weight:bold;'],
          ],
          [
                // 'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'reject_note_reviewer',
                // 'headerOptions' => ['style' => 'text-align:center; width : 220px;'],
                'label' => 'Note',
                // 'editableOptions' => [
                //   // 'asPopover' => false,
                // ],
                // 'contentOptions' => ['style' => 'color:#77DD77; font-weight:bold;'],
          ],
          [
            'format' => 'raw',
            'header' => '<a style="color:#469ED8;">Download BR</a>',
            'label' => [function(){return Html::a('Batch Record','',['style'=>['color'=>'#5DADE2']]);}],
            'value' => function($dataProvider) {
                        return Html::button('',['class'=>'button btn btn-success fa fa-download',
                                  'style' => ['opacity' => '0.75', 'border-radius' => '0.7em', 'vertical-align'=>'middle','display'=>'block','margin'=>'0 auto', 'height'=>'100%', 'width'=>'90%'],
                                  'id' => 'download-br'
                                ]);
                        }
          ],
          // [
          //   'format' => 'raw',
          //   'header' => '<a style="color:#469ED8;">Sync</a>',
          //   'label' => [function(){return Html::a('Batch Record','',['style'=>['color'=>'#5DADE2']]);}],
          //   'value' => function($dataProvider) {
          //               return Html::button('',['class'=>'button btn btn-info fa fa-file-text',
          //                         'style' => ['opacity' => '0.75', 'border-radius' => '0.7em', 'vertical-align'=>'middle','display'=>'block','margin'=>'0 auto', 'height'=>'100%', 'width'=>'90%'],
          //                         'id' => 'view'
          //                       ]);
          //               }
          // ],
          // [
          //   'format' => 'raw',
          //   'header' => '<a class="text-center" style="color:#469ED8;">Approve</a>',
          //   'label' => [function(){return Html::a('Batch Record','',['style'=>['color'=>'#77DD77']]);}],
          //   'value' => function($dataProvider) {
          //               return Html::button('',['class'=>'button btn btn-info fa fa-check',
          //                         'style' => ['opacity' => '0.75', 'border-radius' => '0.7em', 'vertical-align'=>'middle','display'=>'block','margin'=>'0 auto', 'height'=>'100%', 'width'=>'90%','background-color' =>'#77DD77'],
          //                         'id' => 'approve'
          //                       ]);
          //               }
          // ],
          // [
          //   'format' => 'raw',
          //   'header' => '<a class="text-center" style="color:#469ED8;">Reject</a>',
          //   'label' => [function(){return Html::a('Batch Record','',['style'=>['color'=>'#77DD77']]);}],
          //   'value' => function($dataProvider) {
          //               return Html::button('',['class'=>'button btn btn-info fa fa-close',
          //                         'style' => ['opacity' => '0.75', 'border-radius' => '0.7em', 'vertical-align'=>'middle','display'=>'block','margin'=>'0 auto', 'height'=>'100%', 'width'=>'90%','background-color' =>'red'],
          //                         'id' => 'reject'
          //                       ]);
          //               }
          // ],
      ],
    ]); ?>
  </div>
</div>

<script>
  
</script>

<?php
$script = <<< JS

$('button[id^="download-br"]').on("click",function() {
    nomo = $(this).parent().siblings("td:eq('1')").html();
    formula_reference = $(this).parent().siblings("td:eq('3')").html();
    qty_batch = $(this).parent().siblings("td:eq('4')").html();
    // console.log(nomo);
    // console.log(formula_reference);
    // console.log(qty_batch);

    // $.post("index.php?r=log-formula-breakdown/update-status-approver&nomo="+nomo, function (data){
    //   if (data == 1) {
    window.open('index.php?r=log-formula-breakdown/pdf&nomo='+nomo+'&reference='+formula_reference+'&qty_batch='+qty_batch, 'name');
    //     location.reload();
    //   } else {
    //     alert("Gagal update status approver!");
    //   }
    // });

});
    
$('button[id^="view"]').on("click",function() {
    nomo = $(this).parent().siblings("td:eq('1')").html();
    formula_reference = $(this).parent().siblings("td:eq('3')").html();
    qty_batch = $(this).parent().siblings("td:eq('4')").html();
    // console.log(nomo);
    // console.log(formula_reference);
    // console.log(qty_batch);

    $.post("index.php?r=log-formula-breakdown/update-status-reviewer&nomo="+nomo, function (data){
      if (data == 1) {
        window.open('index.php?r=log-formula-breakdown/pdf&nomo='+nomo+'&reference='+formula_reference+'&qty_batch='+qty_batch, 'name');
        location.reload();
      } else {
        alert("Gagal update status reviewer!");
      }
    });

});

$('button[id^="redirect"]').on("click",function() {
    nomo = $(this).parent().siblings("td:eq('1')").html();
    reference = $(this).parent().siblings("td:eq('3')").html();
    qty_batch = $(this).parent().siblings("td:eq('4')").html();
    // console.log(nomo);

    window.location = "index.php?r=log-formula-breakdown/form-batch-record&nomo="+nomo+"&reference="+reference+"&qty_batch="+qty_batch+"&pos=reviewer";

    // if (confirm('Are you sure to approve this nomo?')) {
    //   // Save it!
    //   $.post("index.php?r=log-formula-breakdown/approve-reviewer&nomo="+nomo, function (data){
    //     if (data == 1) {
    //       location.reload();
    //     } else if (data ==2) {
    //       alert("Note wajib diisi!");
    //     } else  {
    //       alert("Gagal update database");
    //     }
    //   });
    // } else {
    //   // Do nothing!
    //   // console.log('Thing was not saved to the database.');
    // }
});

// $('button[id^="approve"]').on("click",function() {
//     nomo = $(this).parent().siblings("td:eq('1')").html();
//     formula_reference = $(this).parent().siblings("td:eq('2')").html();
//     qty_batch = $(this).parent().siblings("td:eq('3')").html();
//     // console.log(nomo);

//     if (confirm('Are you sure to approve this nomo?')) {
//       // Save it!
//       $.post("index.php?r=log-formula-breakdown/approve-reviewer&nomo="+nomo, function (data){
//         if (data == 1) {
//           location.reload();
//         } else if (data ==2) {
//           alert("Note wajib diisi!");
//         } else  {
//           alert("Gagal update database");
//         }
//       });
//     } else {
//       // Do nothing!
//       // console.log('Thing was not saved to the database.');
//     }
// });

// $('button[id^="reject"]').on("click",function() {
//     nomo = $(this).parent().siblings("td:eq('1')").html();
//     formula_reference = $(this).parent().siblings("td:eq('2')").html();
//     qty_batch = $(this).parent().siblings("td:eq('3')").html();
//     // console.log(nomo);

//     if (confirm('Are you sure to reject this nomo?')) {
//       // Save it!
//       $.post("index.php?r=log-formula-breakdown/reject-reviewer&nomo="+nomo, function (data){
//         if (data == 1) {
//           location.reload();
//         } else if (data ==2) {
//           alert("Note wajib diisi!");
//         } else  {
//           alert("Gagal update database");
//         }
//       });
//     } else {
//       // Do nothing!
//       // console.log('Thing was not saved to the database.');
//     }
// });


JS;
$this->registerJs($script);
?>