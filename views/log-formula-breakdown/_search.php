<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogFormulaBreakdownSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-formula-breakdown-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nomo') ?>

    <?= $form->field($model, 'fg_name') ?>

    <?= $form->field($model, 'reference') ?>

    <?= $form->field($model, 'location') ?>

    <?php // echo $form->field($model, 'scheduled_start') ?>

    <?php // echo $form->field($model, 'week') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
