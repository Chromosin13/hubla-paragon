<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\grid\GridView;
use app\models\LogMasterBr;
use app\models\LogMasterBrSearch;

/* @var $this yii\web\View */
/* @var $model app\models\LogFormulaBreakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
.tableFixHead          { overflow-y: auto; height: 81vh; z-index:5;}
.tableFixHead thead th { position: sticky; top: -1px; z-index:5;}

.table-bordered{border-left:1.5px solid rgba(135,206,250,.25); border-bottom:1.5px solid rgba(135,206,250,.25); border-top:1px solid rgba(135,206,250,.25);}
.table-bordered td,.table-bordered th{border:1px solid rgba(135,206,250,.25)}
.table-bordered thead td,.table-bordered thead th{border-bottom-width:2px}

.table-striped tbody tr:nth-of-type(odd){background-color:rgba(135,206,250,.25)}

.btn-info{color:#fff;background-color:#5DADE2;border-color:#5DADE2}.btn-info:hover{color:#fff;background-color:#138496;border-color:#117a8b}.btn-info.focus,.btn-info:focus{color:#fff;background-color:#138496;border-color:#117a8b;box-shadow:0 0 0 .2rem rgba(58,176,195,.5)}.btn-info.disabled,.btn-info:disabled{color:#fff;background-color:#17a2b8;border-color:#17a2b8}.btn-info:not(:disabled):not(.disabled).active,.btn-info:not(:disabled):not(.disabled):active,.show>.btn-info.dropdown-toggle{color:#fff;background-color:#117a8b;border-color:#10707f}.btn-info:not(:disabled):not(.disabled).active:focus,.btn-info:not(:disabled):not(.disabled):active:focus,.show>.btn-info.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(58,176,195,.5)}

.btn-add:hover {
  box-shadow: 0 5px 9px 0 rgba(0,0,0,0.24), 0 5px 9px 0 rgba(0,0,0,0.19);
}

.btn-minus:hover {
  box-shadow: 0 5px 9px 0 rgba(0,0,0,0.24), 0 5px 9px 0 rgba(0,0,0,0.19);
}

table tbody tr{
    height: 1em;
}
</style>
        
<div class="box widget-user" style="display: inline-block;"> 
    <div>
        <h1 class="text-center" style="margin-top:0px; padding-top :0.8em; padding-bottom :0.3em;">MASTER DATA BATCH RECORD</h1>
    </div>

    <div style="width:96%; margin: 0 auto;">

    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => [
          ['class' => 'yii\grid\SerialColumn'],

          'kode_bulk',
          [
                'attribute' => 'formula_reference',
                'label' => 'Reference',
                'contentOptions' => function ($dataProvider) {
                    return ['style' => 'text-align:center;width:5%;'];
                },
          ],
          [
                'attribute' => 'nama_bulk',
                'label' => 'Nama Bulk',
                'contentOptions' => function ($dataProvider) {
                    return ['style' => 'text-align:left;width:25%'];
                },
          ],
          [
                'attribute' => 'timestamp',
                'label' => 'Timestamp',
                'contentOptions' => function ($dataProvider) {
                    return ['style' => 'text-align:center;width:10%'];
                },
          ],
          [
                'attribute' => 'last_user',
                'label' => 'Edited by',
                'contentOptions' => function ($dataProvider) {
                    return ['style' => 'text-align:center;width:15%'];
                },
          ],
          [
                'attribute' => 'sediaan',
                'label' => 'Sediaan',
                'contentOptions' => function ($dataProvider) {
                    return ['style' => 'text-align:center;width:5%'];
                },
          ],
          [
            'format' => 'raw',
            'header' => '<a style="color:#469ED8;">Action</a>',
            // 'label' => [function(){return Html::a('','',['style'=>['color'=>'#5DADE2']]);}],
            'value' => function($dataProvider) {
                        return Html::a(
                                '<i class="fa fa-pencil"></i>',
                                Url::to(['log-formula-breakdown/edit-master-br', 'kode_bulk' => $dataProvider->kode_bulk,'reference' => $dataProvider->formula_reference]),
                                [
                                  'id'=>'grid-custom-button',
                                  'data-pjax'=>true,
                                  'action'=>Url::to(['log-formula-breakdown/edit-master-br', 'kode_bulk' => $dataProvider->kode_bulk,'reference' => $dataProvider->formula_reference]),
                                  'class'=>'button btn btn-default',
                                  'style' => ['background' => '#D3D3D3', 'opacity' => '0.75', 'border-radius' => '0.7em', 'vertical-align'=>'middle','display'=>'block','margin'=>'0 auto', 'height'=>'100%', 'width'=>'90%']
                                ]);
                        }
          ],
          // [
          //       'attribute' => 'qty_batch',
          //       'contentOptions' => function ($dataProvider) {
          //           return ['style' => 'text-align:center;width:12%'];
          //       },
          // ],
          // [
          //       'attribute' => 'scheduled_start',
          //       'contentOptions' => function ($dataProvider) {
          //           return ['style' => 'text-align:center;width:10%'];
          //       },
          // ],
          // [
          //       'attribute' => 'formula_reference',
          //       'contentOptions' => function ($dataProvider) {
          //           return ['style' => 'text-align:center;width:5%'];
          //       },
          // ],
          // [
          //       'attribute' => 'nama_line',
          //       'contentOptions' => function ($dataProvider) {
          //           return ['style' => 'text-align:center;width:5%'];
          //       },
          // ],
          // [
          //       'attribute' => 'week',
          //       'contentOptions' => function ($dataProvider) {
          //           return ['style' => 'text-align:center;width:5%'];
          //       },
          // ],
          // [
          //       'attribute' => 'lokasi',
          //       'contentOptions' => function ($dataProvider) {
          //           return ['style' => 'text-align:center;width:5%'];
          //       },
          // ],
          // [
          //       'attribute' => 'status',
          //       'contentOptions' => function ($dataProvider) {
          //           return ['style' => 'text-align:center;width:8%;color:' 
          //               . ($dataProvider->status == 'unverified' ? '#d54354' : 'green')];
          //       },
          // ],
          // [
          //   'format' => 'raw',
          //   'header' => '<a style="color:#469ED8;">Input New / Edit</a>',
          //   // 'label' => [function(){return Html::a('','',['style'=>['color'=>'#5DADE2']]);}],
          //   'value' => function($dataProvider) {
          //               return Html::a(
          //                       '<i class="fa fa-pencil"></i>',
          //                       Url::to(['log-formula-breakdown/input-detail-br2', 'nomo' => $dataProvider->nomo,'reference' => $dataProvider->formula_reference]),
          //                       [
          //                         'id'=>'grid-custom-button',
          //                         'data-pjax'=>true,
          //                         'action'=>Url::to(['log-formula-breakdown/input-detail-br2', 'nomo' => $dataProvider->nomo,'reference' => $dataProvider->formula_reference]),
          //                         'class'=>'button btn btn-default',
          //                         'style' => ['background' => '#D3D3D3', 'opacity' => '0.75', 'border-radius' => '0.7em', 'vertical-align'=>'middle','display'=>'block','margin'=>'0 auto', 'height'=>'100%', 'width'=>'90%']
          //                       ]);
          //               }
          // ],
      ],
    ]); ?>
</div>
</div>

<?php
$script = <<< JS

// $.getJSON("index.php?r=log-formula-breakdown/get-detail-log&kode_bulk=FW-LWF-BULK&reference=3",function(data){
//     console.log(data.timestamp);
//   });

JS;
$this->registerJs($script);
?>