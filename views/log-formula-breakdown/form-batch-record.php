<?php

use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use app\models\ListBentukSediaan;
use app\models\FlowInputMo;
use app\models\ListOperatorBr;
use app\models\ListSiklusBr;
use app\models\ListUomBr;
use app\models\ListReviewer;
use app\models\ListApprover;
use app\models\MasterDataTimbanganRm;
use app\models\KodeActionPenimbanganRm;
use app\models\ListStatusFormulaBr;
use app\models\LogFormulaBreakdown;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
// use kartik\grid\GridView;
use yii\bootstrap\Modal;
use kartik\editable\Editable;
// use yii\helpers\Json;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\widgets\Pjax;

?>

<style>
/*.tableFixHead          { overflow-y: auto; height: 80vh; z-index:5;}
.tableFixHead thead th { position: sticky; top: -1px; z-index:5;}*/

.table-bordered{
  border-left:1.5px solid rgba(135,206,250,.25);
  border-bottom:1.5px solid rgba(135,206,250,.25);
  border-top:1px solid rgba(135,206,250,.25);
}
.table-bordered td,.table-bordered th{
  border:1px solid rgba(135,206,250,.25)
}
.table-bordered thead td,.table-bordered thead th{
  border-bottom-width:2px
}
.table-striped tbody tr:nth-of-type(odd){
  background-color:rgba(135,206,250,.25)
}

.btn-info{
  color:#fff;
  background-color:#5DADE2;
  border-color:#5DADE2
}
.btn-info:hover{
  color:#fff;
  background-color:#138496;
  border-color:#117a8b
}
.btn-info.focus,.btn-info:focus{
  color:#fff;
  background-color:#138496;
  border-color:#117a8b;
  box-shadow:0 0 0 .2rem rgba(58,176,195,.5)
}
.btn-info.disabled,.btn-info:disabled{
  color:#fff;
  background-color:#17a2b8;
  border-color:#17a2b8
}
.btn-info:not(:disabled):not(.disabled).active,.btn-info:not(:disabled):not(.disabled):active,.show>.btn-info.dropdown-toggle{
  color:#fff;
  background-color:#117a8b;
  border-color:#10707f
}
.btn-info:not(:disabled):not(.disabled).active:focus,.btn-info:not(:disabled):not(.disabled):active:focus,.show>.btn-info.dropdown-toggle:focus{
  box-shadow:0 0 0 .2rem rgba(58,176,195,.5)
}

.btn-add:hover {
  box-shadow: 0 5px 9px 0 rgba(0,0,0,0.24), 0 5px 9px 0 rgba(0,0,0,0.19);
}

.btn-minus:hover {
  box-shadow: 0 5px 9px 0 rgba(0,0,0,0.24), 0 5px 9px 0 rgba(0,0,0,0.19);
}

/* PENAMBAHAN SUFFIX "kg" */
.div2 {
  display: inline-block;
  position: relative;
}
.div2::after {
  position: absolute;
  top: 2px;
  right: .5em;
  transition: all .05s ease-in-out;
}
.div2.kg::after {
  content: 'kg';
  margin-top:5px;
}

/* SPINNER LOADING */
.sk-circle {
  margin: 100px auto;
  width: 100px;
  height: 100px;
  position: relative;
}
.sk-circle .sk-child {
  width: 100%;
  height: 100%;
  position: absolute;
  left: 0;
  top: 0;
}
.sk-circle .sk-child:before {
  content: '';
  display: block;
  margin: 0 auto;
  width: 15%;
  height: 15%;
  background-color: #333;
  border-radius: 100%;
  -webkit-animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
          animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
}
.sk-circle .sk-circle2 {
  -webkit-transform: rotate(30deg);
      -ms-transform: rotate(30deg);
          transform: rotate(30deg); }
.sk-circle .sk-circle3 {
  -webkit-transform: rotate(60deg);
      -ms-transform: rotate(60deg);
          transform: rotate(60deg); }
.sk-circle .sk-circle4 {
  -webkit-transform: rotate(90deg);
      -ms-transform: rotate(90deg);
          transform: rotate(90deg); }
.sk-circle .sk-circle5 {
  -webkit-transform: rotate(120deg);
      -ms-transform: rotate(120deg);
          transform: rotate(120deg); }
.sk-circle .sk-circle6 {
  -webkit-transform: rotate(150deg);
      -ms-transform: rotate(150deg);
          transform: rotate(150deg); }
.sk-circle .sk-circle7 {
  -webkit-transform: rotate(180deg);
      -ms-transform: rotate(180deg);
          transform: rotate(180deg); }
.sk-circle .sk-circle8 {
  -webkit-transform: rotate(210deg);
      -ms-transform: rotate(210deg);
          transform: rotate(210deg); }
.sk-circle .sk-circle9 {
  -webkit-transform: rotate(240deg);
      -ms-transform: rotate(240deg);
          transform: rotate(240deg); }
.sk-circle .sk-circle10 {
  -webkit-transform: rotate(270deg);
      -ms-transform: rotate(270deg);
          transform: rotate(270deg); }
.sk-circle .sk-circle11 {
  -webkit-transform: rotate(300deg);
      -ms-transform: rotate(300deg);
          transform: rotate(300deg); }
.sk-circle .sk-circle12 {
  -webkit-transform: rotate(330deg);
      -ms-transform: rotate(330deg);
          transform: rotate(330deg); }
.sk-circle .sk-circle2:before {
  -webkit-animation-delay: -1.1s;
          animation-delay: -1.1s; }
.sk-circle .sk-circle3:before {
  -webkit-animation-delay: -1s;
          animation-delay: -1s; }
.sk-circle .sk-circle4:before {
  -webkit-animation-delay: -0.9s;
          animation-delay: -0.9s; }
.sk-circle .sk-circle5:before {
  -webkit-animation-delay: -0.8s;
          animation-delay: -0.8s; }
.sk-circle .sk-circle6:before {
  -webkit-animation-delay: -0.7s;
          animation-delay: -0.7s; }
.sk-circle .sk-circle7:before {
  -webkit-animation-delay: -0.6s;
          animation-delay: -0.6s; }
.sk-circle .sk-circle8:before {
  -webkit-animation-delay: -0.5s;
          animation-delay: -0.5s; }
.sk-circle .sk-circle9:before {
  -webkit-animation-delay: -0.4s;
          animation-delay: -0.4s; }
.sk-circle .sk-circle10:before {
  -webkit-animation-delay: -0.3s;
          animation-delay: -0.3s; }
.sk-circle .sk-circle11:before {
  -webkit-animation-delay: -0.2s;
          animation-delay: -0.2s; }
.sk-circle .sk-circle12:before {
  -webkit-animation-delay: -0.1s;
          animation-delay: -0.1s; }

@-webkit-keyframes sk-circleBounceDelay {
  0%, 80%, 100% {
    -webkit-transform: scale(0);
            transform: scale(0);
  } 40% {
    -webkit-transform: scale(1);
            transform: scale(1);
  }
}

@keyframes sk-circleBounceDelay {
  0%, 80%, 100% {
    -webkit-transform: scale(0);
            transform: scale(0);
  } 40% {
    -webkit-transform: scale(1);
            transform: scale(1);
  }
}
</style>

<?php

  Modal::begin([
          'header'=>'<h4>Preview</h4>',
          'id' => 'modalverifieditems',
          'size' => 'modal-lg',
      ]);

  echo "<div id='modalVerifiedItems'></div>";

  Modal::end();

?>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script type="text/javascript">

  var reference = '<?php echo $reference; ?>';
  var kode_bulk = '<?php echo $kode_bulk; ?>';
  var qty_batch = '<?php echo $qty_batch; ?>';
  var nomo = '<?php echo $nomo; ?>';

function calculate(){
    standard = Number(qty_batch).toFixed(4);
    $.get("index.php?r=log-formula-breakdown/get-sum-new&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
      sumFix = Number(data).toFixed(4);
      console.log(sumFix);

      if (parseFloat(sumFix)>(parseFloat(standard)+0.004)){
        $('input[id="sum"]').val(sumFix);
        document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #ff6666";
      } else if ((parseFloat(standard)-0.004)<=parseFloat(sumFix) && parseFloat(sumFix)<=(parseFloat(standard)+0.004)){
        $('input[id="sum"]').val(sumFix);
        document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #3CB371";
      } else {
        $('input[id="sum"]').val(sumFix);
        document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #ffff00";
      }
    });
}

 function back(){
    //window.location = "index.php?r=log-formula-breakdown/schedule-list";
    window.history.back();
  }

function verify(){
  // console.log(qty_batch);
    // $('#modalverifieditems').modal('hide')
    // var dokumen = document.getElementById("dokumen").value;
    // var revisi = document.getElementById("revisi").value;
    standard = Number(qty_batch).toFixed(4);
    // var reviewer = document.getElementById("reviewer").value;
    // var approver = document.getElementById("approver").value;
    // var creator = '<?php echo $pic ?>';
    // var kode_bulk = '<?php echo $kode_bulk ?>';
    // console.log(reviewer);

    $.get("index.php?r=log-formula-breakdown/check-angka-belakang-koma&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
      $.get("index.php?r=log-formula-breakdown/check-approval&nomo="+nomo, function (data){
        if (data==1){
          $.get("index.php?r=log-formula-breakdown/check-detail-br&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
            if (data==1){
                  $.get("index.php?r=log-formula-breakdown/get-sum-new&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                    sumFix = Number(data).toFixed(4);
                    // console.log(data);
                    // console.log(standard);

                    if (parseFloat(sumFix)>(parseFloat(standard)+0.004)){
                      alert("Total quantity melebihi batas toleransi");
                    } else if ((parseFloat(standard)-0.004)<=parseFloat(sumFix) && parseFloat(sumFix)<=(parseFloat(standard)+0.004)){
                      $.get("index.php?r=log-formula-breakdown/check-operator&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                          if (data==1){
                              $.post("index.php?r=log-formula-breakdown/check-timbangan&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                                if (data==1){
                                  $.post("index.php?r=log-formula-breakdown/check-kode-olah&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                                    if (data==1){
                                      $.post("index.php?r=log-formula-breakdown/check-kode-internal&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                                        if (data==1){
                                          $.post("index.php?r=log-formula-breakdown/update-approval&nomo="+nomo+"&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                                            if (data==1){
                                              alert("Data has been sent to reviewer and approver");
                                               window.location = "index.php?r=log-formula-breakdown/schedule-list";
                                             } else {
                                              alert("Data sedang dalam proses review, tidak dapat overwrite. Minta reviewer / approver untuk me reject terlebih dahulu");
                                             }
                                            // } else if (data ==2){
                                            //   alert("Anda sudah pernah mengirim nomo ini ke reviewer.");
                                            // }
                                          });
                                        } else {
                                          alert("Terdapat kode internal yang masih kosong!");
                                        }
                                      });
                                    } else {
                                      alert("Terdapat kode olah yang masih kosong!");
                                    }
                                  });
                                } else {
                                  alert("Terdapat kolom timbangan yang masih kosong!");
                                }
                              });
                          } else {
                            alert("Terdapat kolom operator yang masih kosong!");
                          }
                        });
                    } else {
                      alert("Total quantity kurang dari batas toleransi");
                    }

                  });

                    // if (parseFloat(sumFix)>(parseFloat(standard)+0.004)){
                    //   window.alert("Total quantity lebih dari standard (OVER).");
                    // } else if ((parseFloat(standard)-0.004)<=parseFloat(sumFix) && parseFloat(sumFix)<=(parseFloat(standard)+0.004)){
                    //   $.post("index.php?r=log-formula-breakdown/check-operator-complete&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                    //     if (data==1)
                    //     {
            } else {
              window.alert("Informasi header belum lengkap!");
            }
          });
        } else {
          window.alert("Reviewer atau Approver belum dipilih");
        }
      });
    });

}

function view_br(){
  var nomo = '<?php echo $nomo ?>';
  var formula_reference = '<?php echo $reference ?>';
  var qty_batch = '<?php echo $qty_batch ?>';
  $.get("index.php?r=log-formula-breakdown/check-header",{nomo:nomo,formula_reference:formula_reference,qty_batch:qty_batch},function(data){
    if (data == 'ok'){
      // window.location('index.php?r=log-formula-breakdown/pdf&nomo='+nomo+'&reference='+formula_reference+'&qty_batch='+qty_batch, 'name');
      window.open('index.php?r=log-formula-breakdown/pdf&nomo='+nomo+'&reference='+reference+'&qty_batch='+qty_batch, 'name');
      // location.reload();      
    }else if (data == 'dokumen-fail'){
      alert("Nomor Dokumen tidak sesuai format, perbaiki nomor dokumen terlebih dahulu!");
    }else if (data == 'header-null'){
      alert("Terdapat bagian header yang masih kosong, isi data header terlebih dahulu sebelum lihat BR!");
    }else{
      alert("Terdapat bagian approval yang masih kosong, isi data approval terlebih dahulu sebelum lihat BR!");
    }
  });

}

function synchronizeV1(){
  var nomo = '<?php echo $nomo;  ?>';
  var formula_reference = '<?php echo $reference ?>';
  var qty_batch = '<?php echo $qty_batch ?>';
  if (confirm('Apakah anda yakin sinkronisasi dengan V1? Seluruh formula pada V2 akan tertimpa dan tidak dapat dikembalikan.')) {

    document.getElementById("bodyy").style.opacity = "0.5";
    document.getElementById("spinner").style.visibility = "visible";
    document.getElementById("bodyy").disabled = true;

    $.get("index.php?r=log-formula-breakdown/synchronize&nomo="+nomo+"&reference="+formula_reference+"&qty_batch="+qty_batch, function (data){
        alert("Sinkronisasi berhasil dilakukan.");
        location.reload();
    })
  } else {
      alert('Sinkronisasi dibatalkan.');
  }
}

function deleteLogSync(){
  var nomo = '<?php echo $nomo;  ?>';
  var formula_reference = '<?php echo $reference ?>';
  var qty_batch = '<?php echo $qty_batch ?>';
  document.getElementById("bodyy").style.opacity = "0.5";
  document.getElementById("spinner").style.visibility = "visible";
  document.getElementById("bodyy").disabled = true;
  $.get("index.php?r=log-formula-breakdown/delete-log-sync&nomo="+nomo+"&reference="+formula_reference+"&qty_batch="+qty_batch, function (data){
      if (data == 1) {
        alert("Sinkronisasi berhasil dilakukan.");
        location.reload();
      }else{
        alert('Sinkronisasi gagal dilakukan.');
      }
  })
}

</script>

<body id="bodyy">

<!-- <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
  <div class="toast-header">
    <img src="..." class="rounded mr-2" alt="...">
    <strong class="mr-auto">Bootstrap</strong>
    <small class="text-muted">11 mins ago</small>
    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="toast-body">
    Hello, world! This is a toast message.
  </div>
</div> -->

<div id="spinner" style="visibility: hidden; position:fixed; margin-left: auto;
  margin-right: auto; left:50%; z-index:1000;">
  <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
  </div>
</div>

<div class="box" style="display: inline-block; content: "";
  display: table;
  clear: both;">
  <div class="" style="width:96%; margin: 0 auto; float: left;
  width: 50%;
  padding: 10px;" >
    <?= GridView::widget([
      'dataProvider' => $detailBr,
      // 'filterModel' => $searchModel,
      'columns' => [
          // ['class' => 'yii\grid\SerialColumn'],
          // 'kode_bb',
          [
              'class' => 'kartik\grid\EditableColumn',
              'attribute' => 'status_formula',
              'headerOptions' => ['style' => 'width:35%; text-align:center;'],
              'label' => 'Status Formula',
              'contentOptions' => ['style' => 'text-align:center;'],
              'editableOptions' => [
                'asPopover' => false,
                'inputType' => Editable::INPUT_DROPDOWN_LIST,
                'data' => ArrayHelper::map(ListStatusFormulaBr::find()->orderBy('status')->asArray()->all(),'status','status'),
              ],
          ],
          [
              'class' => 'kartik\grid\EditableColumn',
              'attribute' => 'bentuk_sediaan',
              'headerOptions' => ['style' => 'width:35%; text-align:center;'],
              'label' => 'Bentuk Sediaan',
              'contentOptions' => ['style' => 'text-align:center;'],
              'editableOptions' => [
                'asPopover' => false,
                'inputType' => Editable::INPUT_DROPDOWN_LIST,
                'data' => ArrayHelper::map(ListBentukSediaan::find()->orderBy('id')->asArray()->all(),'nama','nama'),
              ],
          ],
          [
              'class' => 'kartik\grid\EditableColumn',
              'attribute' => 'brand',
              'headerOptions' => ['style' => 'width:35%; text-align:center;'],
              'label' => 'Brand',
              'contentOptions' => ['style' => 'text-align:center;'],
              'editableOptions' => [
                'asPopover' => false,
                'inputType' => Editable::INPUT_DROPDOWN_LIST,
                'data' => ['Wardah'=>'Wardah','Make Over'=>'Make Over','Emina'=>'Emina','Kahf'=>'Kahf','OMG'=>'OMG','Putri'=>'Putri','Biodef'=>'Biodef','Labore'=>'Labore'],
              ],
          ],
          // [
          //     // 'class' => 'kartik\grid\EditableColumn',
          //     'attribute' => 'brand',
          //     'headerOptions' => ['style' => 'width:30%; text-align:center;'],
          //     'label' => 'Brand',
          //     'contentOptions' => ['style' => 'text-align:center;'],
          //     // 'editableOptions' => [
          //     //   // 'asPopover' => false,
          //     // ],
          // ],

      ],
    ]); ?>
  </div>
  <div class="" style="width:96%; margin: 0 auto; float: left;
  width: 50%;
  padding: 10px;" >
    <?= GridView::widget([
      'dataProvider' => $detailBr,
      // 'filterModel' => $searchModel,
      'columns' => [
          // ['class' => 'yii\grid\SerialColumn'],
          // 'kode_bb',
          [
              'class' => 'kartik\grid\EditableColumn',
              'attribute' => 'status_jurnal',
              'headerOptions' => ['style' => 'width:30%; text-align:center;'],
              'label' => 'Status Jurnal',
              'contentOptions' => ['style' => 'text-align:center;'],
              'editableOptions' => [
                'asPopover' => false,
                'inputType' => Editable::INPUT_DROPDOWN_LIST,
                'data' => ['LAMA' => 'LAMA', 'BARU' => 'BARU'],
              ],
          ],
          [
              'class' => 'kartik\grid\EditableColumn',
              'attribute' => 'no_dokumen',
              'headerOptions' => ['style' => 'width:35%; text-align:center;'],
              'label' => 'No. Dokumen',
              'contentOptions' => ['style' => 'text-align:center;'],
              'editableOptions' => [
                'asPopover' => false,
              ],
          ],
          [
              'class' => 'kartik\grid\EditableColumn',
              'attribute' => 'no_revisi',
              'headerOptions' => ['style' => 'width:35%; text-align:center;'],
              'label' => 'No. Revisi',
              'contentOptions' => ['style' => 'text-align:center;'],
              'editableOptions' => [
                'asPopover' => false,
              ],
          ],
      ],
    ]); ?>
  </div>
</div>

<div class="box" style="display: inline-block; content: "";
  display: table;
  clear: both;">
  <div class="" style="width:96%; margin: 0 auto; float: left;
  padding: 10px;" >
    <?php if ($pos == 'reviewer'){
      echo GridView::widget([
        'dataProvider' => $approvalBr,
        // 'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            // 'kode_bb',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'reviewer',
                'headerOptions' => ['style' => 'width:25%; text-align:center;'],
                'label' => 'Reviewer',
                'contentOptions' => ['style' => 'text-align:center;'],
                'editableOptions' => [
                  'asPopover' => false,
                  'inputType' => Editable::INPUT_DROPDOWN_LIST,
                  'data' => ArrayHelper::map(ListReviewer::find()->where("sediaan='".$sediaan."'")->orderBy('reviewer')->asArray()->all(),'username','reviewer'),
                ],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'approver',
                'headerOptions' => ['style' => 'width:25%; text-align:center;'],
                'label' => 'Approver',
                'contentOptions' => ['style' => 'text-align:center;'],
                'editableOptions' => [
                  'asPopover' => false,
                  'inputType' => Editable::INPUT_DROPDOWN_LIST,
                  'data' => ArrayHelper::map(ListApprover::find()->orderBy('approver')->asArray()->all(),'username','approver'),
                ],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'reject_note_reviewer',
                'headerOptions' => ['style' => 'width:25%; text-align:center;'],
                'label' => 'Note from Reviewer',
                'contentOptions' => ['style' => 'text-align:center; color:red;'],
                // 'editableOptions' => [
                  // 'asPopover' => false,
                  // 'inputType' => Editable::INPUT_DROPDOWN_LIST,
                  // 'data' => ArrayHelper::map(ListApprover::find()->orderBy('approver')->asArray()->all(),'username','approver'),
                // ],
            ],
            [
                // 'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'reject_note_approver',
                'headerOptions' => ['style' => 'width:25%; text-align:center;'],
                'label' => 'Note from Approver',
                'contentOptions' => ['style' => 'text-align:center; color:red;'],
                // 'editableOptions' => [
                  // 'asPopover' => false,
                  // 'inputType' => Editable::INPUT_DROPDOWN_LIST,
                  // 'data' => ArrayHelper::map(ListApprover::find()->orderBy('approver')->asArray()->all(),'username','approver'),
                // ],
            ],
        ],
      ]);
    } else if ($pos == 'approver') {
      echo GridView::widget([
        'dataProvider' => $approvalBr,
        // 'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            // 'kode_bb',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'reviewer',
                'headerOptions' => ['style' => 'width:25%; text-align:center;'],
                'label' => 'Reviewer',
                'contentOptions' => ['style' => 'text-align:center;'],
                'editableOptions' => [
                  'asPopover' => false,
                  'inputType' => Editable::INPUT_DROPDOWN_LIST,
                  'data' => ArrayHelper::map(ListReviewer::find()->where("sediaan='".$sediaan."'")->orderBy('reviewer')->asArray()->all(),'username','reviewer'),
                ],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'approver',
                'headerOptions' => ['style' => 'width:25%; text-align:center;'],
                'label' => 'Approver',
                'contentOptions' => ['style' => 'text-align:center;'],
                'editableOptions' => [
                  'asPopover' => false,
                  'inputType' => Editable::INPUT_DROPDOWN_LIST,
                  'data' => ArrayHelper::map(ListApprover::find()->orderBy('approver')->asArray()->all(),'username','approver'),
                ],
            ],
            [
                // 'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'reject_note_reviewer',
                'headerOptions' => ['style' => 'width:25%; text-align:center;'],
                'label' => 'Note from Reviewer',
                'contentOptions' => ['style' => 'text-align:center; color:red;'],
                // 'editableOptions' => [
                  // 'asPopover' => false,
                  // 'inputType' => Editable::INPUT_DROPDOWN_LIST,
                  // 'data' => ArrayHelper::map(ListApprover::find()->orderBy('approver')->asArray()->all(),'username','approver'),
                // ],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'reject_note_approver',
                'headerOptions' => ['style' => 'width:25%; text-align:center;'],
                'label' => 'Note from Approver',
                'contentOptions' => ['style' => 'text-align:center; color:red;'],
                // 'editableOptions' => [
                  // 'asPopover' => false,
                  // 'inputType' => Editable::INPUT_DROPDOWN_LIST,
                  // 'data' => ArrayHelper::map(ListApprover::find()->orderBy('approver')->asArray()->all(),'username','approver'),
                // ],
            ],
        ],
      ]);
    } else {
        // print_r('expressionexpressionexpressionexpressionexpression');

      echo GridView::widget([
        'dataProvider' => $approvalBr,
        // 'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            // 'kode_bb',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'reviewer',
                'headerOptions' => ['style' => 'width:25%; text-align:center;'],
                'label' => 'Reviewer',
                'contentOptions' => ['style' => 'text-align:center;'],
                'editableOptions' => [
                  'asPopover' => false,
                  'inputType' => Editable::INPUT_DROPDOWN_LIST,
                  'data' => ArrayHelper::map(ListReviewer::find()->where("sediaan='".$sediaan."'")->orderBy('reviewer')->asArray()->all(),'username','reviewer'),
                ],
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'approver',
                'headerOptions' => ['style' => 'width:25%; text-align:center;'],
                'label' => 'Approver',
                'contentOptions' => ['style' => 'text-align:center;'],
                'editableOptions' => [
                  'asPopover' => false,
                  'inputType' => Editable::INPUT_DROPDOWN_LIST,
                  'data' => ArrayHelper::map(ListApprover::find()->orderBy('approver')->asArray()->all(),'username','approver'),
                ],
            ],
            [
                // 'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'reject_note_reviewer',
                'headerOptions' => ['style' => 'width:25%; text-align:center;'],
                'label' => 'Note from Reviewer',
                'contentOptions' => ['style' => 'text-align:center; color:red;'],
                // 'editableOptions' => [
                  // 'asPopover' => false,
                  // 'inputType' => Editable::INPUT_DROPDOWN_LIST,
                  // 'data' => ArrayHelper::map(ListApprover::find()->orderBy('approver')->asArray()->all(),'username','approver'),
                // ],
            ],
            [
                // 'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'reject_note_approver',
                'headerOptions' => ['style' => 'width:25%; text-align:center;'],
                'label' => 'Note from Approver',
                'contentOptions' => ['style' => 'text-align:center; color:red;'],
                // 'editableOptions' => [
                  // 'asPopover' => false,
                  // 'inputType' => Editable::INPUT_DROPDOWN_LIST,
                  // 'data' => ArrayHelper::map(ListApprover::find()->orderBy('approver')->asArray()->all(),'username','approver'),
                // ],
            ],
        ],
      ]);
      } ?>
  </div>
</div>

<div class="box" <?php if ($pos=="reviewer" or $pos=="approver"){echo "style='display: inline-block;'";} else {echo "style='display: inline-block; display:none;'";} ?>>
  <div style="width:100%; margin: 0 auto; padding:10px;" >
    <div class="row" style="width:100%;">
      <div style="float: left; text-align:center; width: 33%;">
        <h3><a>VIEW BR</a></h3>
        <button id="view" class="button btn btn-info fa fa-file-text" style="margin-bottom:1em; width:80%; font-size:1.2em; text-align:center;"></button>
      </div>
      <div style="float: left; text-align:center; width: 33%;">
        <h3><a>APPROVE</a></h3>
        <button type="approve" id="approve" class="button btn btn-success fa fa-check" style="margin-bottom:1em; width:80%; font-size:1.2em; text-align:center;"></button>
      </div>
      <div style="float: left; text-align:center; width: 33%;">
        <h3><a>REJECT</a></h3>
        <button type="reject" id="reject" class="button btn btn-danger fa fa-close" style="margin-bottom:1em; width:80%; font-size:1.2em; text-align:center;"></button>
      </div>
    </div>
  </div>
</div>

<div class="box" <?php if ($pos=="reviewer" or $pos=="approver"){echo "style='display: inline-block; display:none;'";} else {echo "style='display: inline-block;'";} ?>>
  <div style="width:100%; margin: 0 auto; padding:10px;" >

    <!-- BUTTON BACK AND VERIFY -->
    <br>

    <div class="row" style="width:100%;">
      <button class="btn btn-info" onclick="back()" style="margin-bottom:1.2em; margin-left:15px; font-size:1em; float: left;"><i class="fa fa-arrow-left"></i></button>
      <button class="btn btn-success" onclick="location.reload()" style="margin-bottom:1.2em; margin-left:15px; font-size:1em; float: left;"><i class="fa fa-refresh"></i></button>
      <button id="calculate" onclick="calculate()" class="btn btn-danger" style="margin-bottom:1.2em; margin-left:15px; font-size:1em; float: left;"><i class="fa fa-calculator"></i></button>
      <button onclick="view_br()" class="btn btn-info" style="margin-bottom:1.2em; margin-left:15px; font-size:1em; float: left;"><i class="fa  fa-file-text"></i></button>
      <?php if (strcasecmp($row_approval['current_status'],"approved") != 0) { ?>
      <button title="Migrasi data dari V1" id="sync-br" onclick="synchronizeV1()" class="btn btn-warning" style="margin-bottom:1.2em; margin-left:15px; font-size:1em; float: left;"><i class="fa fa-exchange"></i></button>
      <?php } ?>
      <button title="Synchronize with Odoo" id="sync-odoo" onclick="deleteLogSync()" class="btn btn-primary" style="margin-bottom:1.2em; margin-left:15px; font-size:1em; float: left;"><i class="fa fa-sitemap"></i></button>
      <div class="div2 kg">
      <input  type="text" id="sum" style="font-weight:bolder; vertical-align:middle; margin-top:4px; margin-left:10px; width:125px; padding-left:5px;" value="0.0000" readonly>
      </div>
      <?php if ($is_lock == 1) {?>
        <?php if ($is_can_send == 1) {?>
          <button type="submit" id="send_timbang" class="btn btn-success" style="margin-bottom:1em; font-size:1.2em; float: right;">Send to Timbang</button>
        <?php } ?>
        <button type="submit" id="open_edit" class="btn btn-info" style="margin-right:25px;margin-bottom:1em; font-size:1.2em; float: right;">EDIT</button>
      <?php } else { ?>
        <button type="submit" onclick="verify()" class="btn btn-info" style="margin-bottom:1em; font-size:1.2em; float: right;">Send to reviewer</button>
      <?php } ?>

      <div id='nama_line' style="width:200px;margin-top:4px; margin-right:80px; margin-bottom:1em; font-size:1.2em; float: right;">
          <select id='line' class='form-control form-control-sm text-center' style='height:2em; text-align:center; float:left;' <?php if ($is_lock == 1) { echo "disabled"; } ?>>
            <option value='Pilih Line'>Pilih Line</options>
          <?php foreach($lines as $line){
              echo "<option value='".$line->line."'"; if ($line->line == $nama_line){ echo " selected";}; echo ">".$line->line."</options>";
          } ?>
          </select>
      </div>
      <h4 id='label_nama_line' style="margin-top:6px; margin-right:5px; margin-bottom:1em; float: right;">Line Timbang :</h4>
    </div>

    <!-- TABLE CARD -->
    <div>

      <?php

      $kg = ArrayHelper::map(MasterDataTimbanganRm::find()->select('line,kode_timbangan')->where("line = '".$nama_line."' and uom = 'kg'")->orderBy('kode_timbangan')->asArray()->all(), 'kode_timbangan', 'kode_timbangan');

      $gram = ArrayHelper::map(MasterDataTimbanganRm::find()->select('line,kode_timbangan')->where("line = '".$nama_line."' and uom = 'g'")->orderBy('kode_timbangan')->asArray()->all(), 'kode_timbangan', 'kode_timbangan');

      ?>

    <?php Pjax::begin(['id' => 'list-br']); ?>
      <!-- TABEL -->
      <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            // 'kode_bb',
            [
                'attribute' => 'serial_manual',
                'headerOptions' => ['style' => 'text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;'],
                'value' => function($dataProvider, $key, $index) {
                    if ($dataProvider->is_split == 99){
                      return "";
                    } else {
                      return $dataProvider->serial_manual;
                    }
                },
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'kode_bb',
                'headerOptions' => ['style' => 'text-align:center; width : 190px;'],
                'label' => 'Kode Item',
                'editableOptions' => [
                  // 'asPopover' => false,
                  'submitOnEnter' => true,
                ],
                'readonly' => function($dataProvider, $key, $index, $grid) use ($is_lock) {
                  if ($is_lock == 1 || $dataProvider->kode_bb=='AIR-RO--L'){
                    return true;
                  } else {
                    if ($dataProvider->is_split == 99){
                      return true;
                    }
                  }
                },
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'kode_internal',
                'headerOptions' => ['style' => 'text-align:center; width : 150px;'],
                'width' => '180px',
                'label' => 'Kode Internal',
                'editableOptions' => [
                  // 'asPopover' => false,
                  'submitOnEnter' => true,
                ],
                'readonly' => function($dataProvider, $key, $index, $grid) use ($is_lock) {
                  if ($is_lock == 1){
                    return true;
                  } else {
                    if ($dataProvider->is_split == 99){
                      return true;
                    }
                  }
                },
            ],
            [
                // 'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'nama_bb',
                'headerOptions' => ['style' => 'text-align:center; width : 220px;'],
                'label' => 'Nama Bahan Baku',
                // 'editableOptions' => [
                //   // 'asPopover' => false,
                // ],
            ],
            // 'nama_bb',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'qty',
                // 'format'=> ['decimal'=>2],
                // 'value' => function($dataProvider, $key, $index) {
                //     return number_format($dataProvider->qty, LogFormulaBreakdown::get_decimal($dataProvider->timbangan));
                // },
                'headerOptions' => ['style' => 'text-align:center; width : 120px;'],
                'label' => 'Quantity',
                'editableOptions' => [
                  // 'asPopover' => false,
                  'submitOnEnter' => true,
                ],
                'readonly' => function($dataProvider, $key, $index, $grid) use ($is_lock) {
                  if ($is_lock == 1){
                    return true;
                  } else {
                    if ($dataProvider->is_split > 0 and $dataProvider->is_split < 99){
                      return true;
                    }
                  }
                },
            ],
            // 'kode_timbangan',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'uom',
                'headerOptions' => ['style' => 'text-align:center;'],
                'vAlign' => 'middle',
                'width' => '80px',
                'label' => 'UoM',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(ListUomBr::find()->orderBy('uom')->asArray()->all(), 'uom', 'uom'),
                'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                'filterInputOptions' => ['placeholder' => 'Pilih', 'multiple' => false], // allows multiple authors to be chosen
                'format' => 'raw',
                'contentOptions' => ['style' => 'text-align:center;'],
                'editableOptions' => [
                  // 'asPopover' => false,
                  'inputType' => Editable::INPUT_DROPDOWN_LIST,
                  'data' => ArrayHelper::map(ListUomBr::find()->orderBy('uom')->asArray()->all(),'uom','uom'),
                ],
                'readonly' => function($dataProvider, $key, $index, $grid) use ($is_lock) {
                  if ($is_lock == 1 ){
                    return true;
                  } else {
                    if ($dataProvider->is_split > 0 and $dataProvider->is_split < 99){
                      return true;
                    }
                  }
                },
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'siklus',
                'headerOptions' => ['style' => 'text-align:center;'],
                'vAlign' => 'middle',
                'width' => '80px',
                'label' => 'Siklus',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(ListSiklusBr::find()->orderBy('siklus')->asArray()->all(), 'siklus', 'siklus'),
                'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                'filterInputOptions' => ['placeholder' => 'Pilih', 'multiple' => false], // allows multiple authors to be chosen
                'format' => 'raw',
                'contentOptions' => ['style' => 'text-align:center;'],
                'editableOptions' => [
                  // 'asPopover' => false,
                  'inputType' => Editable::INPUT_DROPDOWN_LIST,
                  'data' => ArrayHelper::map(ListSiklusBr::find()->orderBy('siklus')->asArray()->all(),'siklus','siklus'),
                ],
                'readonly' => function($dataProvider, $key, $index, $grid) use ($is_lock) {
                  if ($is_lock == 1){
                    return true;
                  } else {
                    if ($dataProvider->is_split > 0 and $dataProvider->is_split < 99){
                      return true;
                    }
                  }
                },
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'kode_olah',
                'headerOptions' => ['style' => 'text-align:center;'],
                'vAlign' => 'middle',
                'contentOptions' => ['style' => 'text-align:center;'],
                'width' => '100px',
                'label' => 'Kode Olah',
                'editableOptions' => [
                  // 'asPopover' => false,
                  'submitOnEnter' => true,
                ],
                'readonly' => function($dataProvider, $key, $index, $grid) use ($is_lock) {
                  if ($is_lock == 1 || $dataProvider->kode_bb=='AIR-RO--L'){
                    return true;
                  } else {
                    if ($dataProvider->is_split > 0 and $dataProvider->is_split < 99){
                      return true;
                    }
                  }
                },
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'action',
                'headerOptions' => ['style' => 'text-align:center;'],
                'vAlign' => 'middle',
                'width' => '80px',
                'label' => 'Action',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(KodeActionPenimbanganRm::find()->orderBy(['kode'=>SORT_ASC])->all(), 'kode', 'kode'),
                'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                'filterInputOptions' => ['placeholder' => 'Pilih', 'multiple' => false], // allows multiple authors to be chosen
                'format' => 'raw',
                'contentOptions' => function($dataProvider, $key, $index, $grid) {
                    if ($dataProvider->is_split == 99 || $dataProvider->kode_bb=='AIR-RO--L'){
                      return ['style' => 'text-align:center;', 'id'=>'action-split-'.$dataProvider->kode_bb];
                    } else {
                      return ['style' => 'text-align:center;', 'id'=>'action-main-'.$dataProvider->kode_bb];
                    }

                },
                'editableOptions' => [
                  // 'asPopover' => false,
                  // 'placeholder' => 'Pilih',
                  'options' => ['placeholder'=>'Pilih..'],
                  // 'options' => [
                  //     'pluginOptions' => [

                  //     ]
                  // ]
                  'inputType' => Editable::INPUT_DROPDOWN_LIST,
                  'data' => ArrayHelper::map(KodeActionPenimbanganRm::find()->orderBy(['kode'=>SORT_ASC])->all(), 'kode', 'kode'),
                ],
                'readonly' => function($dataProvider, $key, $index, $grid) use ($is_lock) {
                  if ($is_lock == 1 || $dataProvider->kode_bb=='AIR-RO--L'){
                    return true;
                  } else {
                    if ($dataProvider->is_split == 99){
                      return true;
                    }
                  }
                },
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'keterangan',
                'headerOptions' => ['style' => 'text-align:center;'],
                'label' => 'Keterangan',
                'contentOptions' => ['style' => 'text-align:center;'],
                'editableOptions' => [
                  // 'asPopover' => false,
                  'inputType' => Editable::INPUT_TEXTAREA,
                  'submitOnEnter' => false,
                ],
                'readonly' => function($dataProvider, $key, $index, $grid) use ($is_lock) {
                  if ($is_lock == 1 || $dataProvider->kode_bb=='AIR-RO--L'){
                    return true;
                  }
                },
            ],
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'pageSummary' => '<small>(amounts in $)</small>',
                'pageSummaryOptions' => ['colspan' => 3, 'data-colspan-dir' => 'rtl'],
                'contentOptions'=> function($dataProvider, $key, $index, $grid) use ($is_lock) {
                    if ($is_lock == 1 || $dataProvider->kode_bb=='AIR-RO--L'){
                      return ['style' => 'visibility:hidden;'];
                    } else {
                      return ['id' => 'repack-'.$dataProvider->id];
                    }
                },
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'operator',
                'headerOptions' => ['style' => 'text-align:center;'],
                'vAlign' => 'middle',
                'width' => '80px',
                'label' => 'Operator',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(ListOperatorBr::find()->where("sediaan='".$sediaan."'")->orderBy('operator')->asArray()->all(), 'operator', 'operator'),
                'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                'filterInputOptions' => ['placeholder' => 'Pilih', 'multiple' => false], // allows multiple authors to be chosen
                'format' => 'raw',
                'contentOptions' => ['style' => 'text-align:center;'],
                'editableOptions' => function ($dataProvider, $key, $index) use($sediaan) {
                    // if ($dataProvider->is_repack == 1 && strpos($dataProvider->nama_line, 'LW')!== false){
                    //   return [
                    //     // 'asPopover' => false,
                    //     'inputType' => Editable::INPUT_DROPDOWN_LIST,
                    //     'data' => ArrayHelper::map(ListOperatorBr::find()->where("sediaan ilike '%repack%' ")->orderBy('operator')->asArray()->all(),'operator','operator'),
                    //   ];
                    // }else{
                      return [
                        // 'asPopover' => false,
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => ArrayHelper::map(ListOperatorBr::find()->where("sediaan='".$sediaan."'")->orderBy('operator')->asArray()->all(),'operator','operator'),

                      ];
                    // }

                },
                'readonly' => function($dataProvider, $key, $index, $grid) use ($is_lock) {
                  if ($is_lock == 1 || $dataProvider->kode_bb=='AIR-RO--L'){
                    return true;
                  }
                },
            ],
            // [
            //     'class' => 'kartik\grid\EditableColumn',
            //     'attribute' => 'is_repack',
            //     'headerOptions' => ['style' => 'text-align:center;'],
            //     'label' => 'Is Repack',
            //     'contentOptions' => ['style' => 'text-align:center;'],
            //     'editableOptions' => [
            //       // 'asPopover' => true,
            //       'inputType' => Editable::INPUT_CHECKBOX_X,
            //       // 'data' => ArrayHelper::map(ListOperatorBr::find()->orderBy('operator')->asArray()->all(),'operator','operator'),
            //     ],
            // ],

            // [
            //     'attribute' => 'is_repack',
            //     'headerOptions' => ['style' => 'text-align:center;'],
            //     'contentOptions' => ['style' => 'text-align:center; display:none;'],
            //     'visible' => true,
            // ],
            // [
            //     'class' => 'kartik\grid\EditableColumn',
            //     'attribute' => 'is_repack',
            //     'label' => 'Apakah Repack?',
            //     'editableOptions' => [
            //       'asPopover' => false,
            //       'inputType' => Editable::INPUT_RADIO,
            //       'data' => [0 => false, 1 => true],
            //     ],
            // ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'timbangan',
                'headerOptions' => ['style' => 'text-align:center;'],
                'vAlign' => 'middle',
                'width' => '150px',
                'label' => 'Timbangan',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(MasterDataTimbanganRm::find()->select('line,kode_timbangan')->where("line = '".$nama_line."'")->orderBy('kode_timbangan')->asArray()->all(), 'kode_timbangan', 'kode_timbangan'),
                'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                'filterInputOptions' => ['placeholder' => 'Pilih', 'multiple' => false], // allows multiple authors to be chosen
                'format' => 'raw',
                'contentOptions' => ['style' => 'text-align:center;'],
                'editableOptions' => function ($dataProvider, $key, $index) {
                    if (empty($dataProvider->operator) ){
                      if (strpos($dataProvider->nama_line, 'LW')!== false && $dataProvider->is_repack==1){
                        return [
                          'inputType' => Editable::INPUT_DROPDOWN_LIST,
                          'data' => ArrayHelper::map(MasterDataTimbanganRm::find()->select('line,kode_timbangan')->where("line = 'LWE03' and uom = '".$dataProvider->uom."' ")->orderBy('kode_timbangan')->asArray()->all(), 'kode_timbangan', 'kode_timbangan'),
                          ];
                      }else{
                        return [
                          'inputType' => Editable::INPUT_DROPDOWN_LIST,
                          'data' => ArrayHelper::map(MasterDataTimbanganRm::find()->select('line,kode_timbangan')->where("line = '".$dataProvider->nama_line."' and uom = '".$dataProvider->uom."'")->orderBy('kode_timbangan')->asArray()->all(), 'kode_timbangan', 'kode_timbangan'),
                          ];
                      }

                    } else if ($dataProvider->is_repack==1 && strpos($dataProvider->nama_line, 'LW')!== false){
                      return [
                          'inputType' => Editable::INPUT_DROPDOWN_LIST,
                          'data' => ArrayHelper::map(MasterDataTimbanganRm::find()->select('line,kode_timbangan')->where("line = 'LWE03' and uom = '".$dataProvider->uom."' and (operator = ".$dataProvider->operator." or operator = 0) ")->orderBy('kode_timbangan')->asArray()->all(), 'kode_timbangan', 'kode_timbangan'),
                          ];

                    } else {
                      return [
                          'inputType' => Editable::INPUT_DROPDOWN_LIST,
                          'data' => ArrayHelper::map(MasterDataTimbanganRm::find()->select('line,kode_timbangan')->where("line = '".$dataProvider->nama_line."' and uom = '".$dataProvider->uom."' and (operator = ".$dataProvider->operator." or operator = 0)")->orderBy('kode_timbangan')->asArray()->all(), 'kode_timbangan', 'kode_timbangan'),
                          ];
                    }

                },
                'readonly' => function($dataProvider, $key, $index, $grid) use ($is_lock) {
                  if ($is_lock == 1 || $dataProvider->kode_bb=='AIR-RO--L'){
                    return true;
                  } else {
                    if ($dataProvider->is_split > 0 and $dataProvider->is_split < 99){
                      return true;
                    }
                  }
                },
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'cycle_time_std',
                'headerOptions' => ['style' => 'text-align:center;'],
                'vAlign' => 'middle',
                'contentOptions' => ['style' => 'text-align:center;'],
                'width' => '100px',
                'label' => 'Cycle Time',
                // 'editableOptions' => [
                //   'asPopover' => false,
                // ],
                'readonly' => function($dataProvider, $key, $index, $grid) use ($is_lock) {
                  if ($is_lock == 1 || $dataProvider->kode_bb=='AIR-RO--L'){
                    return true;
                  }
                },
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{download} {delete} {view}',
                'buttons' => [
                    'download' => function ($url,$dataProvider) {
                      if ($dataProvider->is_split<99) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-plus" style="color:green;"></span>',
                            // ['log-formula-breakdown/schedule-list', 'id' => $model->id],
                            'javascript:void(0);',
                            [
                                'title' => 'Add',
                                'data-pjax' => '0',
                                'id' => 'add-'.$dataProvider->id,
                            ]
                        );
                      }
                    },
                    'view' => function ($url,$dataProvider) {
                      if ($dataProvider->is_split<99) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-refresh"></span>',
                            'javascript:void(0);',
                            [
                                'title' => 'Sync',
                                'data-pjax' => '0',
                                'id' => 'sync-'.$dataProvider->is_split."-".$dataProvider->id,
                            ]
                        );
                      }
                    },
                    'delete' => function ($url,$dataProvider) {
                        if ($dataProvider->is_split==99) {
                          return
                          Html::a(
                            '<span class="glyphicon glyphicon-minus" style="color:red;"></span>',
                            'javascript:void(0);',
                            [
                                'title' => 'Delete',
                                'data-pjax' => '0',
                                'id' => 'minus-'.$dataProvider->id,
                            ]
                          );
                        };
                    },
                ],
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                // 'contentOptions' => ['style' => 'text-align:center'],
                'contentOptions'=> function($dataProvider, $key, $index, $grid) {
                    if ($dataProvider->is_lock == 1 || $dataProvider->kode_bb=='AIR-RO--L'){
                      return ['style' => 'visibility:hidden;'];
                    } else {
                      return ['style' => 'text-align:center'];
                    }
                },
            ],
        ],
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' =>  [
            [
                // 'content' =>
                //     Html::button('<i class="fas fa-plus"></i>', [
                //         'class' => 'btn btn-success',
                //         'title' => 'Add Line',
                //         'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");'
                //     ]) . ' '.
                //     Html::a('<i class="fas fa-redo"></i>', ['grid-demo'], [
                //         'class' => 'btn btn-outline-secondary',
                //         'title'=> 'Reset Grid',
                //         'data-pjax' => 0,
                //     ]),
                'options' => ['class' => 'btn-group mr-2']
            ],
            '{export}',
            '{toggleData}',
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        // 'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<i class="fas fa-book"></i>  Batch Record',
        ],
    ]); ?>
    <?php Pjax::end(); ?>


  </div>
</div>
</div>

</body>

<?php
$script = <<< JS

  // window.alert('Pastikan UoM sudah sesuai dengan Timbangan yang dipilih!');

  var nomo = '$nomo';
  var pic = '$pic';
  var idBom = '$idBom';
  var nama_bulk = '$nama_bulk';
  var reference = '$reference';
  var lokasi = '$lokasi';
  var nama_line = '$nama_line';
  var pos = '$pos';
  // console.log(nama_line)
  var kode_bulk = '$kode_bulk';
  var status_timbang = '$status_timbang';
  var qty_batch = '$qty_batch';
  var standard = 0.0;
  var sumFix = 0.0;
  var flag = 1;

  // $.post("index.php?r=master-data-timbangan-rm/get-line-timbang-rm-nomo&nomo="+nomo, function (data){
  //     $('div[id="nama_line"]').html(data);
  //     $('select[id="line"]').val(nama_line);
  //     // console.log(nama_line);
  // });

  $('th[class="kartik-sheet-style kv-all-select kv-align-center kv-align-middle skip-export kv-merged-header"]').empty();
  console.log();
  $('th[class="kartik-sheet-style kv-all-select kv-align-center kv-align-middle skip-export kv-merged-header"]').append( "<a href='javascript:void(0);'>Is Repack?</a>" );

  standard = Number(qty_batch).toFixed(4);
  $.get("index.php?r=log-formula-breakdown/get-sum-new&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
    sumFix = Number(data).toFixed(4);
    // console.log(sumFix);

    if (parseFloat(sumFix)>(parseFloat(standard)+0.004)){
      $('input[id="sum"]').val(sumFix);
      document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #ff6666";
    } else if ((parseFloat(standard)-0.004)<=parseFloat(sumFix) && parseFloat(sumFix)<=(parseFloat(standard)+0.004)){
      $('input[id="sum"]').val(sumFix);
      document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #3CB371";
    } else {
      $('input[id="sum"]').val(sumFix);
      document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #ffff00";
    }
  });

  var j = $('section[class="content-header"]').html(`
    <div class="row" style='background-color: white; height:50px; margin-left:0px; margin-top:0px; padding-left:1%; padding-top:1px;'>
      <div class="col-6" style="float:left">
        <h4>
          <a style='color:#1fd164;'>Bills of Materials</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <a style='color:#6f6f6f;'>`+idBom+`: [`+kode_bulk+`] `+nama_bulk+`</a>
        </h4>
      </div>
      <div class="col-6" style="float:right; padding-right:5%;">

        <h4>
          <a style='color:#1fd164;'>Reference</a> : <a style='color:#6f6f6f; margin-right:35px;'>`+reference+`</a>
          <a style='color:#1fd164;'>Lokasi</a> : <a style='color:#6f6f6f; margin-right:35px;'>`+lokasi+`</a>
          <a style='color:#1fd164;'>Batch Qty</a> : <a style='color:#6f6f6f;'>`+qty_batch+` kg</a>
        </h4>

      </div>
    </div>
    <div class="row" style='background-color: white; height:50px; margin-left:0px; margin-top:0px; padding-left:1%; padding-top:1px;'>
      <div class="col-12" style="float:left">
        <h4>
          <a style='color:#1fd164;'>Status Penimbangan</a>&nbsp;: <a style='color:#6f6f6f;'>`+status_timbang+`</a>
        </h4>
      </div>
    </div>`
  );

  $('input[class="kv-row-checkbox"]').each(function () {
     //It'll be an array of elements
     var id = $(this).val();

    // console.log(id);
    // $(this).empty();
    $.get("index.php?r=log-formula-breakdown/get-repack-status&id="+id, function (data){
      if (data==1) {
        // $(this).prop('checked', true);
        // var var_class = $(id_repack).attr('class');

        var id_repack = '#repack-'+id;

        $(id_repack).children().prop('checked', true);
        $(id_repack).parent().addClass( "danger" );

        // console.log(id);
        // console.log(id+" : "+data);
      }
      // $.pjax.reload({container: '#list-br'});

    });
  });

  // $('input[name*="[qty]"]').on("focusout",function() {
  //   name = $(this).attr('name');
  //   value = $(this).val();
  //   row = name.substring(name.indexOf("[") + 1, name.indexOf("]"));
  //   col = name.substring(name.lastIndexOf("[") + 1, name.lastIndexOf("]"));
  //   id_ori = 'logformulabreakdown-'+row+'-'+col+'-targ';

  //   id = 'logformulabreakdown-'+row+'-timbangan-targ';
  //   timbangan = document.getElementById(id).innerHTML;
  //   $.post("index.php?r=log-formula-breakdown/get-decimal&timbangan="+timbangan, function (data){
  //     console.log(data);
  //     document.getElementById(id_ori).value = Number(value).toFixed(data);
  //     console.log(document.getElementById(id_ori).value);
  //     // $(this).val(Number(value).toFixed(data));
  //   });


    // document.getElementById(id).innerHTML = Number(value).toFixed(6);
    // logformulabreakdown-1-qty-targ
    // console.log("yyyy");
  //});

  // $('select[name*="[action]"]').on("change",function() {
  //   name = $(this).attr('name');
  //   value = $(this).val();
  //   id_db = $(this).parent().prev().prev().val();

  //   $.get("index.php?r=log-formula-breakdown/get-kode-bb&id="+id_db, function (data){

  //     id_destination = 'action-split-'+data;

  //     $('td[id="'+id_destination+'"]').each(function () {
  //       console.log("data");
  //       $(this).children().html(value);
  //     });
  //   });
  // });

  $('a[id^="add-"]').on("click",function() {
    var id_attr = $(this).attr('id');
    // var nilai = $(this).val();

    //idSplit = id.substring(id.lastIndexOf("-") + 1);
    idItem = id_attr.substring(id_attr.lastIndexOf("-") + 1);
    // console.log(idItem);

    document.getElementById("bodyy").style.opacity = "0.5";
    document.getElementById("spinner").style.visibility = "visible";
    document.getElementById("bodyy").disabled = true;

    $.post("index.php?r=log-formula-breakdown/add-split&id="+idItem, function (data){
        if (data == 1) {
          // console.log("yes");
            // $.pjax.reload({container: '#list-br'});
          window.location.reload(false);
        }
      });
  });

  $('a[id^="minus-"]').on("click",function() {
    var id_attr = $(this).attr('id');
    // var nilai = $(this).val();

    //idSplit = id.substring(id.lastIndexOf("-") + 1);
    idItem = id_attr.substring(id_attr.lastIndexOf("-") + 1);
    // console.log(idItem);

    document.getElementById("bodyy").style.opacity = "0.5";
    document.getElementById("spinner").style.visibility = "visible";
    document.getElementById("bodyy").disabled = true;

    $.post("index.php?r=log-formula-breakdown/minus-split&id="+idItem, function (data){
        if (data == 1) {

          // console.log("minus");
            // $.pjax.reload({container: '#list-br'});
          window.location.reload(false);
        } else if (data == 2){
          alert("Harus menghapus dari yang paling akhir.");
          document.getElementById("bodyy").style.opacity = "1";
          document.getElementById("spinner").style.visibility = "hidden";
          document.getElementById("bodyy").disabled = false;
        }
      });
  });

  $('a[id^="sync-"]').on("click",function() {
    var id = $(this).attr('id');
    idItem = id.substring(id.lastIndexOf("-") + 1);
    count_split = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

    // if (count_split > 0){
    //   alert ("Hapus breakdown formula sebelum melakukan sync!");
    // } else {

      document.getElementById("bodyy").style.opacity = "0.5";
      document.getElementById("spinner").style.visibility = "visible";
      document.getElementById("bodyy").disabled = true;

      kode_bb = $(this).parent().siblings("td:eq('1')").children().children().html();

      $.getJSON("index.php?r=log-formula-breakdown/sync-kodebb&nomo="+nomo+"&id="+idItem+"&value="+kode_bb, function (data){

          if (data.status == "success"){
            console.log(data.status);
            window.location.reload(false);
          } else {
            alert("Gagal sync, data di warehouse belum update. Tunggu beberapa saat atau hubungi tim IT jika masih belum berhasil.");
            document.getElementById("bodyy").style.opacity = "1";
            document.getElementById("spinner").style.visibility = "hidden";
            document.getElementById("bodyy").disabled = false;
          }
        });
    // }
  });

  /*$('a[id^="reset-"]').on("click",function() {
    var id_attr = $(this).attr('id');
    // var nilai = $(this).val();

    //idSplit = id.substring(id.lastIndexOf("-") + 1);
    idItem = id_attr.substring(id_attr.lastIndexOf("-") + 1);
    // console.log(idItem);

    kode_bb = $(this).parent().siblings("td:eq('1')").children().children().html();

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Apakah anda yakin ingin reset seluruh split item?',
      text: "Seluruh item split akan direset. Anda tidak dapat mengembalikan aksi ini.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Reset sekarang',
      cancelButtonText: 'Batal',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        document.getElementById("bodyy").style.opacity = "0.5";
        document.getElementById("spinner").style.visibility = "visible";
        document.getElementById("bodyy").disabled = true;

        $.post("index.php?r=log-formula-breakdown/reset-split&nomo="+nomo+"&reference="+reference+"&kode_bb="+kode_bb, function (data){
            if (data == 1) {
                document.getElementById("bodyy").style.opacity = "1";
                document.getElementById("spinner").style.visibility = "hidden";
                document.getElementById("bodyy").disabled = false;
                swalWithBootstrapButtons.fire(
                    'Reseted!',
                    'Split item berhasil disetel ulang.',
                    'success'
                ).then((result) => {
                    document.getElementById("bodyy").style.opacity = "0.5";
                    document.getElementById("spinner").style.visibility = "visible";
                    document.getElementById("bodyy").disabled = true;
                    window.location.reload(false);
                })
            }
        });

      }
    })
  });*/

  $('input[class="kv-row-checkbox"]').on("click",function() {
    // var id_attr = $(this).attr('id');
    var id = $(this).val();
    var value = $(this).prop("checked");
    // var id_uom = $(this).parents('tr').data('key');
    var id_uom = $('button[id="logformulabreakdown-"]');
    var uom = $(id_uom).val();

    console.log(id_uom);
    console.log(id);


    if (value == true){
      repack = 1;
    } else {
      repack = 0;
    }

    // document.getElementById("bodyy").style.opacity = "0.5";
    // document.getElementById("spinner").style.visibility = "visible";
    // document.getElementById("bodyy").disabled = true;

    // $.post("index.php?r=log-formula-breakdown/update-repack&id="+id+"&value="+repack, function (data){
    $.getJSON("index.php?r=log-formula-breakdown/update-repack&id="+id+"&value="+repack, function (data){
        if (data.status == 1) {
          // console.log("yes");
          // $.pjax.reload({container: '#list-br'});
          // window.location.reload(false);

          if (data.value==1){
            if (nama_line.indexOf('LW') >= 0){
              window.alert('Anda harus refresh halaman untuk memilih timbangan Repack, dan pilih timbangan di LWE 03!');
            }
            // $.get("index.php?r=master-data-timbangan-rm/get-timbangan-repack&uom="+uom+"&kode_timbangan="+timbangan, function (data){
            //   if (data=="-") {
            //     flag = 0;
            //     window.alert('Daftar timbangan untuk line tersebut belum ada di database.');
            //   } else {
            //     $('select#timbangan-'+idItem+'-'+idSplit).html(data);
            //     $('input#qty-'+idItem+'-'+idSplit).trigger("change");
            //   }
            // });
          }else{

          }

        }
      });
  });

  // setInterval(function(){
  $('select[id="line"]').on("change",function() {
      // console.log('hai');
      var id = $(this).attr('id');
      var nama_line = $(this).val();
      // console.log(nama_line);
      // console.log(kode_bulk);
      // console.log(reference);
      // console.log(qty_batch);
      // flag = 1;
      // idNotSplit = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      document.getElementById("bodyy").style.opacity = "0.5";
      document.getElementById("spinner").style.visibility = "visible";
      document.getElementById("bodyy").disabled = true;

      $.get("index.php?r=log-formula-breakdown/update-line2&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch+"&nama_line="+nama_line, function (data){
        if (data == 1) {
          window.location.reload(false);
        }
      });


    });
    // },2000);

    $('button[id^="view"]').on("click",function() {
      console.log("halo");

      window.open('index.php?r=log-formula-breakdown/pdf&nomo='+nomo+'&reference='+reference+'&qty_batch='+qty_batch, 'name');

      // $.post("index.php?r=log-formula-breakdown/update-status-reviewer&nomo="+nomo, function (data){
      //   if (data == 1) {
      //     window.open('index.php?r=log-formula-breakdown/pdf&nomo='+nomo+'&reference='+reference+'&qty_batch='+qty_batch, 'name');
      //     // location.reload();
      //   } else {
      //     alert("Gagal update status reviewer!");
      //   }
      // });

    });

    $('button[id^="approve"]').on("click",function() {
        // nomo = $(this).parent().siblings("td:eq('1')").html();
        // formula_reference = $(this).parent().siblings("td:eq('2')").html();
        // qty_batch = $(this).parent().siblings("td:eq('3')").html();
        // console.log(nomo);

        if (confirm('Are you sure to approve this nomo?')) {
          // Save it!
          if (pos == 'reviewer'){
            $.post("index.php?r=log-formula-breakdown/approve-reviewer&nomo="+nomo, function (data){
              if (data == 1) {
                window.location = "index.php";
              } else if (data ==2) {
                alert("Note reviewer wajib diisi!");
              } else  {
                alert("Gagal update database");
              }
            });
          } else if (pos == 'approver'){
            $.post("index.php?r=log-formula-breakdown/approve-approver&nomo="+nomo+"&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
              if (data == 1) {

                $.post("index.php?r=log-formula-breakdown/send-data-to-weighing2&nomo="+nomo+"&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch+"&param=0", function (data){
                    if (data == 1) {
                      $.post("index.php?r=log-formula-breakdown/update-status&nomo="+nomo+"&pic="+pic, function (data){
                        // console.log(data);
                        // console.log(nomo);
                        if (data == 1){
                          document.getElementById("bodyy").style.opacity = "0.5";
                          document.getElementById("spinner").style.visibility = "visible";
                          document.getElementById("bodyy").disabled = true;
                          alert("Data has been sent to Timbang");
                          window.location = "index.php?r=log-formula-breakdown/list-schedule-approver";
                        } else {
                          window.alert("Gagal Memverifikasi1");
                        }
                      });

                      // document.getElementById("bodyy").style.opacity = "0.5";
                      // document.getElementById("spinner").style.visibility = "visible";
                      // document.getElementById("bodyy").disabled = true;
                      // alert("Data has been sent to Timbang");
                      // window.location = "index.php";
                    } else if (data==2){
                       window.alert("Proses penimbangan sudah dilakukan sehingga tidak dapat mengirim ulang");
                    } else {
                      window.alert("Error kirim ke timbang, lapor IT");
                    }
                  });
              } else if (data ==2) {
                alert("Note approver wajib diisi!");
              } else  {
                alert("Gagal update database");
              }
            });
          }
        } else {
          // Do nothing!
          // console.log('Thing was not saved to the database.');
        }
    });

    $('button[id^="reject"]').on("click",function() {
        // nomo = $(this).parent().siblings("td:eq('1')").html();
        // formula_reference = $(this).parent().siblings("td:eq('2')").html();
        // qty_batch = $(this).parent().siblings("td:eq('3')").html();
        // console.log(nomo);

        if (confirm('Are you sure to reject this nomo?')) {
          // Save it!
          if (pos == 'reviewer'){
            $.post("index.php?r=log-formula-breakdown/reject-reviewer&nomo="+nomo+"&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
              if (data == 1) {
                window.location = "index.php";
              } else if (data ==2) {
                alert("Note reviewer wajib diisi!");
              } else  {
                alert("Gagal update database");
              }
            });
          } else if (pos == 'approver'){
            $.post("index.php?r=log-formula-breakdown/reject-approver&nomo="+nomo+"&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
              if (data == 1) {
                window.location = "index.php";
              } else if (data ==2) {
                alert("Note approver wajib diisi!");
              } else  {
                alert("Gagal update database");
              }
            });
          }

        } else {
          // Do nothing!
          // console.log('Thing was not saved to the database.');
        }
    });

    $('button[id^="open_edit"]').on("click",function() {

        if (confirm('Are you sure to edit this formula?')) {
          // Save it!
          $.post("index.php?r=log-formula-breakdown/open-edit&nomo="+nomo+"&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
            if (data == 1) {
              document.getElementById("bodyy").style.opacity = "0.5";
              document.getElementById("spinner").style.visibility = "visible";
              document.getElementById("bodyy").disabled = true;
              window.location.reload(false);
            } else if (data==2) {
              alert("Tidak bisa edit karena sedang di review, minta reviewer atau approver untuk me-reject terlebih dahulu!");
            } else {
              alert("Error edit, lapor tim IT");
            }
          });
        } else {
          // Do nothing!
          // console.log('Thing was not saved to the database.');
        }
    });

    $('button[id^="send_timbang"]').on("click",function() {

        if (confirm('Are you sure to send this formula to Timbang?')) {
          // Save it!
          $.get("index.php?r=log-formula-breakdown/check-angka-belakang-koma&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
            $.get("index.php?r=log-formula-breakdown/check-approval&nomo="+nomo, function (data){
              if (data==1){
                $.get("index.php?r=log-formula-breakdown/check-detail-br&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                  if (data==1){
                        $.get("index.php?r=log-formula-breakdown/get-sum-new&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                          sumFix = Number(data).toFixed(4);
                          // console.log(data);
                          // console.log(standard);

                          if (parseFloat(sumFix)>(parseFloat(standard)+0.004)){
                            alert("Total quantity melebihi batas toleransi");
                          } else if ((parseFloat(standard)-0.004)<=parseFloat(sumFix) && parseFloat(sumFix)<=(parseFloat(standard)+0.004)){
                            $.get("index.php?r=log-formula-breakdown/check-operator&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                                if (data==1){
                                    $.post("index.php?r=log-formula-breakdown/check-timbangan&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                                      if (data==1){
                                        $.post("index.php?r=log-formula-breakdown/check-kode-olah&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                                          if (data==1){
                                            $.post("index.php?r=log-formula-breakdown/check-kode-internal&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                                              if (data==1){
                                                $.post("index.php?r=log-formula-breakdown/send-data-to-weighing2&nomo="+nomo+"&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch+"&param=1", function (data){
                                                  if (data == 1) {
                                                    $.post("index.php?r=log-formula-breakdown/update-status&nomo="+nomo+"&pic="+pic, function (data){
                                                      // console.log(data);
                                                      // console.log(nomo);
                                                      if (data == 1){
                                                        document.getElementById("bodyy").style.opacity = "0.5";
                                                        document.getElementById("spinner").style.visibility = "visible";
                                                        document.getElementById("bodyy").disabled = true;
                                                        alert("Data has been sent to Timbang");
                                                        window.location.reload(false);
                                                        // window.location = "index.php?r=log-formula-breakdown/schedule-list";
                                                      } else {
                                                        window.alert("Gagal Memverifikasi1");
                                                      }
                                                    });

                                                  } else if (data==2){
                                                     window.alert("Proses penimbangan sudah dilakukan sehingga tidak dapat mengirim ulang");
                                                  } else {
                                                    window.alert("Error kirim ke timbang, lapor IT");
                                                  }
                                                });
                                              } else {
                                                alert("Terdapat kode internal yang masih kosong!");
                                              }
                                            });
                                          } else {
                                            alert("Terdapat kode olah yang masih kosong!");
                                          }
                                        });
                                      } else {
                                        alert("Terdapat kolom timbangan yang masih kosong!");
                                      }
                                    });
                                } else {
                                  alert("Terdapat kolom operator yang masih kosong!");
                                }
                              });
                          } else {
                            alert("Total quantity kurang dari batas toleransi");
                          }

                        });

                          // if (parseFloat(sumFix)>(parseFloat(standard)+0.004)){
                          //   window.alert("Total quantity lebih dari standard (OVER).");
                          // } else if ((parseFloat(standard)-0.004)<=parseFloat(sumFix) && parseFloat(sumFix)<=(parseFloat(standard)+0.004)){
                          //   $.post("index.php?r=log-formula-breakdown/check-operator-complete&kode_bulk="+kode_bulk+"&reference="+reference+"&qty_batch="+qty_batch, function (data){
                          //     if (data==1)
                          //     {
                  } else {
                    window.alert("Informasi header belum lengkap!");
                  }
                });
              } else {
                window.alert("Reviewer atau Approver belum dipilih");
              }
            });
          });

        } else {
          // Do nothing!
          // console.log('Thing was not saved to the database.');
        }
    });


JS;
$this->registerJs($script);
?>
