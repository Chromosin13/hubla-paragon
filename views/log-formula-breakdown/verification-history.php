<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\LogFormulaBreakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
.tableFixHead          { overflow-y: auto; height: 81vh; z-index:5;}
.tableFixHead thead th { position: sticky; top: -1px; z-index:5;}

.table-bordered{
  border-left:1.5px solid rgba(135,206,250,.25);
  border-bottom:1.5px solid rgba(135,206,250,.25);
  border-top:1px solid rgba(135,206,250,.25);
}
.table-bordered td,.table-bordered th{
  border:1px solid rgba(135,206,250,.25)
}
.table-bordered thead td,.table-bordered thead th{
  border-bottom-width:2px
}

.table-striped tbody tr:nth-of-type(odd){
  background-color:rgba(135,206,250,.25)
}

.btn-info{color:#fff;background-color:#5DADE2;border-color:#5DADE2}.btn-info:hover{color:#fff;background-color:#138496;border-color:#117a8b}.btn-info.focus,.btn-info:focus{color:#fff;background-color:#138496;border-color:#117a8b;box-shadow:0 0 0 .2rem rgba(58,176,195,.5)}.btn-info.disabled,.btn-info:disabled{color:#fff;background-color:#17a2b8;border-color:#17a2b8}.btn-info:not(:disabled):not(.disabled).active,.btn-info:not(:disabled):not(.disabled):active,.show>.btn-info.dropdown-toggle{color:#fff;background-color:#117a8b;border-color:#10707f}.btn-info:not(:disabled):not(.disabled).active:focus,.btn-info:not(:disabled):not(.disabled):active:focus,.show>.btn-info.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(58,176,195,.5)}

.btn-add:hover {
  box-shadow: 0 5px 9px 0 rgba(0,0,0,0.24), 0 5px 9px 0 rgba(0,0,0,0.19);
}

.btn-minus:hover {
  box-shadow: 0 5px 9px 0 rgba(0,0,0,0.24), 0 5px 9px 0 rgba(0,0,0,0.19);
}

table tbody tr{
    height: 1em;
}
.img-circle{
  border-radius:50%;
}
</style>

<?php

  // Modal::begin([
  //         'header'=>'<h4>Review BR</h4>',
  //         'id' => 'modalverifieditems',
  //         'size' => 'modal-lg',
  //     ]);

  // echo "<div id='modalVerifiedItems'></div>";

  // Modal::end();

?>
        
<div class="box widget-user" style="display: inline-block;"> 
    <div>
        <h1 class="text-center" style="margin-top:0px; padding-top :0.8em; padding-bottom :0.3em;">VERIFICATION HISTORY</h1>
    </div>

    <div style="width:96%; margin: 0 auto;"> 

    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => [
          ['class' => 'yii\grid\SerialColumn',
           'contentOptions' => function ($dataProvider) {
                    return ['style' => 'text-align:center;width:5%'];
                },
          ],
          // [
          //       'attribute' => 'nomo',
          //       'contentOptions' => function ($dataProvider) {
          //           return ['style' => 'text-align:left;width:20%'];
          //       },
          // ],
          'nomo',
          [
            'format' => 'raw',
            'header' => '<a style="color:#469ED8; text-align:center;">Action</a>',
            'contentOptions' => function ($dataProvider) {
                    return ['style' => 'text-align:center;width:8%'];
                },
            'value' => function($dataProvider) {
                        return Html::a('',['log-formula-breakdown/list-verification-history','nomo'=>$dataProvider->nomo],[
                                  'class'=>'button btn btn-info fa fa-file-text',
                                  'style' => ['vertical-align'=>'middle'],
                                  'id' => 'view-items',
                                  // 'onclick'=>"modalVerifiedItems('".$dataProvider->nomo."')"
                                ]);
                        },
          ],
      ],
    ]); ?>
  </div>
</div>

<script>
  function modalVerifiedItems(nomo){
    // console.log(nomo);
    url = '<?php echo Url::to("index.php?r=log-penimbangan-rm/list-verified-items&nomo=");?>'+nomo;

    // console.log (url);

    // $('#modalverifieditems').modal('show')
    //   // find id
    //   .find('#modalVerifiedItems')
    //   // load id
    //   .load(url);
  }
</script>

<?php
$script = <<< JS



JS;
$this->registerJs($script);
?>