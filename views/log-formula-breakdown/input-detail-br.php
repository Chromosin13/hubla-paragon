<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\FlowInputMo;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\LogFormulaBreakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
.tableFixHead          { overflow-y: auto; height: 80vh; z-index:5;}
.tableFixHead thead th { position: sticky; top: -1px; z-index:5;}

.table-bordered{border-left:1.5px solid rgba(135,206,250,.25); border-bottom:1.5px solid rgba(135,206,250,.25); border-top:1px solid rgba(135,206,250,.25);}
.table-bordered td,.table-bordered th{border:1px solid rgba(135,206,250,.25)}
.table-bordered thead td,.table-bordered thead th{border-bottom-width:2px}

.table-striped tbody tr:nth-of-type(odd){background-color:rgba(135,206,250,.25)}

.btn-info{color:#fff;background-color:#5DADE2;border-color:#5DADE2}.btn-info:hover{color:#fff;background-color:#138496;border-color:#117a8b}.btn-info.focus,.btn-info:focus{color:#fff;background-color:#138496;border-color:#117a8b;box-shadow:0 0 0 .2rem rgba(58,176,195,.5)}.btn-info.disabled,.btn-info:disabled{color:#fff;background-color:#17a2b8;border-color:#17a2b8}.btn-info:not(:disabled):not(.disabled).active,.btn-info:not(:disabled):not(.disabled):active,.show>.btn-info.dropdown-toggle{color:#fff;background-color:#117a8b;border-color:#10707f}.btn-info:not(:disabled):not(.disabled).active:focus,.btn-info:not(:disabled):not(.disabled):active:focus,.show>.btn-info.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(58,176,195,.5)}

.btn-add:hover {
  box-shadow: 0 5px 9px 0 rgba(0,0,0,0.24), 0 5px 9px 0 rgba(0,0,0,0.19);
}

.btn-minus:hover {
  box-shadow: 0 5px 9px 0 rgba(0,0,0,0.24), 0 5px 9px 0 rgba(0,0,0,0.19);
}

/* prepare wrapper element */
.div2 {
  display: inline-block;
  position: relative;
}

/* position the unit to the right of the wrapper */
.div2::after {
  position: absolute;
  top: 2px;
  right: .5em;
  transition: all .05s ease-in-out;
}

/* move unit more to the left on hover or focus within
   for arrow buttons will appear to the right of number inputs */
.div2.kg::after {
  content: 'kg';
  margin-top:5px;
}

</style>

<script type="text/javascript">

  //$(".btn .btn-dark .btn-sm").on("click", this.clickDispatcherTable.bind(this));

  var bom =  <?php echo json_encode($bom); ?>;
  var nomo =  <?php echo json_encode($nomo); ?>;
  var kode_bulk = bom[0]['kode_bulk'];
  var reference = bom[0]['formula_reference'];
  var sumFinal = 0.0;
  //var qty_batch = <?php echo json_encode($qty_batch); ?>;

  //console.log(bom[0]);

  function deleteRow(x,y){
    var row = document.getElementById(x);
    row.parentNode.removeChild(row);

    //var id = $(this).attr('id');
    // id = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));
    id = x.substring(x.lastIndexOf("-") + 1);
    idSiklus = x.substring(x.indexOf("-") + 1, x.lastIndexOf("-"));

    $.getJSON("index.php?r=log-formula-breakdown/minus-split-weighing",{id:y},function(result){});

    $.post("index.php?r=log-formula-breakdown/delete-split-qty&id_split="+id, function (data){
      $.post("index.php?r=log-formula-breakdown/get-split&id="+idSiklus, function (split){
      //     if (split==0){
      $.post("index.php?r=log-formula-breakdown/get-standard-qty-per-item&id="+idSiklus, function (data2)
      {
        qty_standard_per_item = Number(data2).toFixed(4);
        $.post("index.php?r=log-formula-breakdown/get-sum-per-item&id="+idSiklus, function (data3)
        {
          var idQty = "qty-"+idSiklus;
          sumPerItem = Number(data3).toFixed(4);

          // console.log("sum:"+sumPerItem+"  standard:"+qty_standard_per_item);
          // console.log(sumPerItem-qty_standard_per_item);
          $.post("index.php?r=log-formula-breakdown/get-sum&kode_bulk="+kode_bulk+"&reference="+reference, function (data4){
            //sumFix = data;
            sumFinal = Number(data4).toFixed(4);;
            if (split==0){
              if ((sumPerItem-qty_standard_per_item)>0)
              {
                $('input[id="sum"]').val("over");
                $('input[id="sum"]').css('box-shadow', '0px 0px 2px 2px #ff6666');
                console.log("3");
              } else if ((sumPerItem-qty_standard_per_item)<0){
                //document.getElementById("sum").style.boxShadow = "";
                $('input[id="sum"]').val(sumFinal);
                $('input[id="sum"]').css('box-shadow', '0px 0px 2px 2px #ffff00');
                console.log("4");
              } else {
                $('input[id="sum"]').val(sumFinal);
                $('input[id="sum"]').css('box-shadow', '0px 0px 2px 2px #3CB371');
                console.log("5");
              }
            } else {
              if ((sumPerItem-qty_standard_per_item)>0)
              {
                $('input[id="sum"]').val("over");
                $('input[id^="qty-105-"]').css('box-shadow', '0px 0px 2px 2px #ff6666');
                $('input[id="sum"]').css('box-shadow', '0px 0px 2px 2px #ff6666');
                console.log("3");
              } else if ((sumPerItem-qty_standard_per_item)<0){
                //document.getElementById("sum").style.boxShadow = "";
                $('input[id="sum"]').val(sumFinal);
                $('input[id^="qty-105-"]').css('box-shadow', '0px 0px 2px 2px #ffff00');
                $('input[id="sum"]').css('box-shadow', '0px 0px 2px 2px #ffff00');
                console.log("4");
              } else {
                $('input[id="sum"]').val(sumFinal);
                $('input[id^="qty-105-"]').css('box-shadow', '0px 0px 2px 2px #3CB371');
                $('input[id="sum"]').css('box-shadow', '0px 0px 2px 2px #3CB371');
                console.log("5");
              }
            }
          });
        });
      });
      //     }
      });
    });

    // AMBIL DATA STANDARD QTY PER BAHAN BAKU
    // $.post("index.php?r=log-formula-breakdown/get-standard-qty-per-item&id="+id3, function (data)
    // {
    //   qty_standard_per_item = Number(data).toFixed(4);

    //   // INPUT DATA SPLIT DARI ITEM TERPILIH
    //   if ((nilai>0) && (!isNaN(nilai)) && (nilai!="")){
    //     $.post("index.php?r=log-formula-breakdown/update-split-qty&id_split="+id2+"&qty="+nilai, function (data){});
    //     console.log("1");
    //   } else {
    //     //$(this).val(0);
    //     document.getElementById(id).value = 0;
    //     $.post("index.php?r=log-formula-breakdown/update-split-qty&id_split="+id2+"&qty=0", function (data){});
    //     console.log("2");
    //   }

    //   // AMBIL DATA TOTAL QTY YANG SUDAH DI INPUT (SPLIT) UNTUK BAHAN BAKU TERPILIH
    //   $.post("index.php?r=log-formula-breakdown/get-sum-per-item&id="+id3, function (data)
    //   {
    //     var idQty = "qty-"+id3;
    //     sumPerItem = Number(data).toFixed(4);

    //     console.log("sum:"+sumPerItem+"  standard:"+qty_standard_per_item);
    //     console.log(sumPerItem-qty_standard_per_item);
        
    //     if ((sumPerItem-qty_standard_per_item)>0)
    //     {
    //       $('input[id^="qty-105-"]').css('box-shadow', '0px 0px 2px 2px #ff6666');
    //       console.log("3");
    //     } else if ((sumPerItem-qty_standard_per_item)<0){
    //       //document.getElementById("sum").style.boxShadow = "";
    //       $('input[id^="qty-105-"]').css('box-shadow', '0px 0px 2px 2px #ffff00');
    //       console.log("4");
    //     } else {
    //       $('input[id^="qty-105-"]').css('box-shadow', '0px 0px 2px 2px #3CB371');
    //       console.log("5");
    //     }
    //   });

    //   $.post("index.php?r=log-formula-breakdown/get-standard-qty&kode_bulk="+kode_bulk+"&reference="+reference, function (data2){
    //     standard = Number(data2).toFixed(4);
    //     $.post("index.php?r=log-formula-breakdown/get-sum&kode_bulk="+kode_bulk+"&reference="+reference, function (data){
    //       //sumFix = data;
    //       sumFix = Number(data).toFixed(4);
    //       //standard = data2;
          
    //       // console.log("sum:"+sumFix+"  standard:"+standard);
    //       // console.log(sumFix-standard);
          
    //       if ((sumFix-standard)>0){
    //         $('input[id="sum"]').val("over");
    //         document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #ff6666";
    //         console.log("6");
    //       } else if ((sumFix-standard)<0){
    //         $('input[id="sum"]').val(sumFix);
    //         document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #ffff00";
    //         console.log("7");
    //       } else {
    //         $('input[id="sum"]').val(sumFix);
    //         document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #3CB371";
    //         console.log("8");
    //       }
    //     });
    //   });
    // });

    // $.post("index.php?r=log-formula-breakdown/get-sum&kode_bulk="+kode_bulk+"&reference="+reference, function (data){
    //   sumFix = data;
    //   sumFix = Number(sumFix).toFixed(4);
    //   //console.log("boing");
    //   //$('input[id="sum"]').val("over");
    //   $.post("index.php?r=log-formula-breakdown/get-standard-qty&kode_bulk="+kode_bulk+"&reference="+reference, function (data2){
    //     //console.log(data2+"loh");
    //     buff = data2;
    //     buff = Number(buff).toFixed(4);
    //     if (sumFix>buff){
    //       $('input[id="sum"]').val("over");
    //       document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #ff6666";
    //       //console.log("itu");
    //     } else {
    //       $('input[id="sum"]').val(sumFix);
    //       //console.log(sumFix+"ini");
    //       document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #3CB371";
    //     }
    //   });
    // });

    // $.getJSON("index.php?r=log-formula-breakdown/get-split",{id:idSiklus},function(data){
    //   if (data==0){
    //     document.getElementById("siklus-"+idSiklus+"-0").disabled = false;
    //     $.post("index.php?r=log-formula-breakdown/get-standard-qty-per-item&id="+idSiklus, function (data)
    //     {
    //       var qtyPerItem = Number(data).toFixed(4);
    //       //console.log(qtyPerItem);
    //     });
    //   } else {
    //     $.post("index.php?r=log-formula-breakdown/get-sum-per-item&id="+idSiklus, function (data)
    //     {
    //       var qtyPerItem = Number(data).toFixed(4);
    //       //console.log(qtyPerItem);
    //     });
    //   }
    // });
  }

  function clickDispatcherTable(x,y) {

    $.getJSON("index.php?r=log-formula-breakdown/add-split-weighing",{id:y},function(result){
    
      if (result){
        var bom =  <?php echo json_encode($bom); ?>;

        $.post("index.php?r=log-formula-breakdown/insert-split-qty&id="+bom[x-1]['id']+"&qty=0", function (data){
          data = JSON.parse(data);
          id = data.status;
          var kode_bb = bom[x-1]['kode_bb'];
          var nama_bb = bom[x-1]['nama_bb'];
          var uom = bom[x-1]['uom'];
          var reference = bom[x-1]['formula_reference'];
          var location = bom[x-1]['lokasi'];
          var siklus = bom[x-1]['siklus'];
          let plusButtonParentId = $('#add-'+bom[x-1]['id']).parent().parent().attr('id');
          var plusButtonParent = document.getElementById(plusButtonParentId);

          $.post("index.php?r=log-formula-breakdown/get-sum&kode_bulk="+kode_bulk+"&reference="+reference, function (data){
            sumFix = data;
            sumFix = Number(sumFix).toFixed(4);
            $('input[id="sum"]').val(sumFix);
          });

          document.getElementById("siklus-"+bom[x-1]['id']+"-0").disabled = true;
          // $.getJSON("index.php?r=log-formula-breakdown/get-split",{id:y},function(data){
         
          plusButtonParent.insertAdjacentHTML('afterend', `
          <tr id="row-`+bom[x-1]['id']+`-`+id+`";>
            <td class="text-left" style="font-size:1em;"> </td>
            <td class="text-left" style="font-size:1em;"></td>
            <td class="text-left" style="font-size:1em;">`+nama_bb+`</td>
            <td ><input type="numeric" style="font-size:1em; display:block; margin:0 auto; width:100%; padding-left:5px;" value="0" id="qty-`+bom[x-1]['id']+`-`+id+`"></input></td>
            <td class="text-center" style="font-size:1em;" id="uom-`+bom[x-1]['id']+`">`+uom+`</td>
            <td class="text-center" style="font-size:1em;"></td>
            <td class="text-center" style="font-size:1em;"></td>
            <td class="text-center" style="font-size:1em;">
              <select id="siklus-`+bom[x-1]['id']+`-`+id+`" style="height:2em; text-align:center; width: 70%; display:block; margin:0 auto;" class="form-control form-control-sm text-center">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
              </select>
            </td>
            <td class="text-center" style="font-size:1em;">
              <select style='height:2em; text-align:left; width: 70%; display:block; margin:0 auto;' class='form-control form-control-sm text-center'>
                <option disabled selected value>Choose...</option>
                <option value='1'>1</option>
                <option value='2'>2</option>
                <option value='3'>3</option>
                <option value='4'>4</option>
              </select>
            </td>
            <td class="text-left" style="font-size:1em;"><div id='timbangan-`+x+`'></div></td>
            <td class="text-left" style="font-size:1em;">
              <button type="button" onclick="deleteRow('row-`+bom[x-1]['id']+`-`+id+`',`+ bom[x-1]['id']+`)" class="btn btn-dark btn-sm product2 btn-minus" style="display:block; margin: 0 auto; background:#FF4C4C; width:75%; opacity:0.85; border-radius:0.7em;" id="minus-`+bom[x-1]['id']+`"><i class="fa fa-minus"></i></button>
            </td>
          </tr>`);
          // });
        });

        $.post("index.php?r=master-data-timbangan-rm/get-timbang-nomo&nomo="+nomo, function (data){
          $('div[id^="timbangan-"]').html(data);
          
          var id = "siklus-"+bom[x-1]['id']+"-"+bom[x-1]['is_split'];
          var a = 0;
          if(bom[x-1]['siklus']==1){
            a = 0;
          } else if (bom[x-1]['siklus']==2){
            a = 1;
          } else if (bom[x-1]['siklus']==3){
            a = 2;
          } else{
            a = 3;
          }

          var tag = $('select[id="'+id+'"]').prop("selectedIndex",a);
        });
      }
    });
  }

  function back(){
    window.location = "index.php?r=log-formula-breakdown/schedule-list";
  }

  // .html("<div style='background-color: white; height:20px; margin-left:0px;'><p>HALO</p></div>");
  // var j = $('section[class="content-header"]').prop("tagName");
  // console.log(j);

</script>

<div class="box" style="display: inline-block;">
  <div style="width:96%; margin: 0 auto;" > 
      
    <!-- BUTTON BACK AND VERIFY -->
    <br>
    
    <div class="row" style="width:100%;">
      <button class="btn btn-info" onclick="back()" style="margin-bottom:1.2em; margin-left:15px; font-size:1em; float: left;"><i class="fa fa-arrow-left"></i></button>
      <button id="calculate" class="btn btn-danger" style="margin-bottom:1.2em; margin-left:15px; font-size:1em; float: left;"><i class="fa fa-calculator"></i></button>
      <div class="div2 kg">
        
      <input  type="text" id="sum" style="font-weight:bolder; vertical-align:middle; margin-top:4px; margin-left:10px; width:100px; padding-left:5px;" readonly>
      </div>
      <button type="submit" class="btn btn-info" style="margin-bottom:1em; font-size:1.2em; float: right;">Verify</button>
      <div id='nama_line' style="width:200px;margin-top:4px; margin-right:80px; margin-bottom:1em; font-size:1.2em; float: right;"></div>
      <h4 id='nama_line' style="margin-top:6px; margin-right:5px; margin-bottom:1em; float: right;">Line Timbang :</h4>
      <!-- <input  type="number" id="tes" value="0" style="font-weight:bolder; vertical-align:middle; margin-top:4px; margin-left:10px; width:100px; padding-left:5px;"> -->
    </div>
      
    <!-- TABLE CARD -->
    <div>
      <p style="margin-bottom:0px; margin-top:0px;">Showing <b>1-<?php echo count($bom); ?></b> of <b><?php echo count($bom); ?></b> items.</p>
      <table class="table table-bordered table-striped table-hover">
        <thead>
          <tr style="padding-top:0;">
            <th class="text-center" style="width: 2%; vertical-align: middle; background:white;">
              <a style="color:black;">#</a>
            </th>
            <th class="text-center" style="width: 15%; vertical-align: middle; background:white;">
              <a style="color:#469ED8;">Kode Bahan Baku</a>
            </th>
            <th class="text-center" style="width: 15%; vertical-align: middle; background:white;">
              <a style="color:#469ED8;">Nama Bahan Baku</a>
            </th>
            <th class="text-center" style="width: 17%; vertical-align: middle; background:white;">
              <a style="color:#469ED8;">Quantity</a>
            </th>
            <th class="text-center" style="width: 6%; vertical-align: middle; background:white;">
              <a style="color:#469ED8;">UoM</a>
            </th>
            <th class="text-center" style="width: 6%; vertical-align: middle; background:white;">
              <a style="color:#469ED8;">Reference</a>
            </th>
            <th class="text-center" style="width: 6%; vertical-align: middle; background:white;">
              <a style="color:#469ED8;">Lokasi</a>
            </th>
            <th class="text-center" style="width: 6%; vertical-align: middle; background:white;">
              <a style="color:#469ED8;">Siklus</a>
            </th>
            <th class="text-center" style="width: 10%; vertical-align: middle; background:white;">
              <a style="color:#469ED8;">Operator</a>
            </th>
            <th class="text-center" style="width: 10%; vertical-align: middle; background:white;">
              <a style="color:#469ED8;">No. Timbangan</a>
            </th>
            <th class="text-center" style="width: 7%; vertical-align: middle; background:white;">
              <a style="color:#469ED8;">Split Quantity</a>
            </th>
          </tr>
        </thead>
        <tbody>
        <?php
            for ($x = 0; $x < count($bom); $x++) {
              if ($bom[$x]['kode_bb'] == "AIR-RO--L"){
                echo '<tr style="opacity:0.5" id="row-';echo $bom[$x]['id']; echo '" disabled>';
                  echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">'; echo $x+1; echo '</td>';
                  echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['kode_bb']; echo '</td>';
                  echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['nama_bb']; echo '</td>';
                  echo '<td class="text-left" style="font-size:1em;vertical-align: middle;" id="qty-'; echo $bom[$x]['id']; echo '-0">'; echo round($bom[$x]['qty'],4); echo '</td>';
                  echo '<td class="text-center" style="font-size:1em;vertical-align: middle;" id="uom-'; echo $bom[$x]['id']; echo '">'; echo $bom[$x]['uom']; echo '</td>';
                  echo '<td class="text-center" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['formula_reference']; echo '</td>';
                  echo '<td class="text-center" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['lokasi']; echo '</td>';
                  echo '<td class="text-center" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['siklus']; echo '</td>';
                  echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">'; echo '</td>';
                  // echo '<td class="text-center" style="font-size:1.15em;">'; echo "<input></input>"; echo '</td>';
                  echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">'; echo '</td>';
                  echo '<td class="text-left product2" style="font-size:1em;vertical-align: middle;">'; echo '</td>';
                echo '</tr>';
              } else {
                echo '<tr id="row-';echo $bom[$x]['id']; echo '">';
                  echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">'; echo $x+1; echo '</td>';
                  echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['kode_bb']; echo '</td>';
                  echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['nama_bb']; echo '</td>';
                  // echo '<td class="text-center" style="font-size:1em;vertical-align: middle;">'; echo round($bom[$x]['qty'],4);
                  echo '<td class="text-left" style="font-size:1em;vertical-align: middle;" id="qty-'; echo $bom[$x]['id']; echo '-0">'; echo number_format($bom[$x]['qty'],4,'.',''); echo '</td>';
                  echo '<td class="text-center" style="font-size:1em;vertical-align: middle;" id="uom-'; echo $bom[$x]['id']; echo '">'; echo $bom[$x]['uom']; echo '</td>';
                  echo '<td class="text-center" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['formula_reference']; echo '</td>';
                  echo '<td class="text-center" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['lokasi']; echo '</td>';
                  // echo '<td class="text-center" style="font-size:1em;vertical-align: middle;">'; echo $bom[$x]['siklus']; echo '</td>';
                  echo '<td lass="text-center" style="font-size:1em;vertical-align: middle;">';
                  echo '<select id="'; echo 'siklus-'; echo $bom[$x]['id']; echo'-0" style="height:2em; text-align:center; width: 70%; display:block; margin:0 auto;" class="form-control form-control-sm text-center"'; if($bom[$x]['is_split'] != 0){ echo ' disabled'; } echo '>';
                    if ($bom[$x]['siklus'] == 1){
                      echo '<option value="1" selected>1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>';
                    } else if ($bom[$x]['siklus'] == 2) {
                      echo '<option value="1">1</option>
                      <option value="2" selected>2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>';
                    } else if ($bom[$x]['siklus'] == 3) {
                      echo '<option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3" selected>3</option>
                      <option value="4">4</option>';
                    } else if ($bom[$x]['siklus'] == 4) {
                      echo '<option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4" selected>4</option>';
                    }
                    '</select>' ; echo '</td>';
                  echo '<td class="text-left" style="font-size:1em;vertical-align: middle;">'; echo "<select style='height:2em; text-align:center; width: 70%; display:block; margin:0 auto;' class='form-control form-control-sm text-center'><option disabled selected value>Choose...</option>
                      <option value='1'>1</option>
                      <option value='2'>2</option>
                      <option value='3'>3</option>
                      <option value='4'>4</option></select>"; echo '</td>';
                  // echo '<td class="text-center" style="font-size:1.15em;">'; echo "<input></input>"; echo '</td>';
                  echo '<td class="text-left" style="font-size:1em;vertical-align: middle; display:block; margin:0 auto;">'; echo "<div id='timbangan-";echo $x; echo"'></div>"; echo '</td>';
                  echo '<td class="text-left product2" style="font-size:1em;vertical-align: middle;">'; echo '<button type="button" onclick="clickDispatcherTable(';echo $x+1; echo ',';echo $bom[$x]['id']; echo ')" class="btn btn-sm product2 btn-add" style="display:block; margin: 0 auto; background:#68e468; width:75%; opacity:0.75; border-radius:0.7em;" id="add-'; echo $bom[$x]['id']; echo '"><i class="fa fa-plus"></i></button>'; echo '</td>';
                echo '</tr>';
              }

              if ($bom[$x]['is_split'] != 0){
                $i = 0;
                for ($j = 0; $j < count($data_split); $j++){
                  if ($bom[$x]['id'] == $data_split[$j]['log_formula_breakdown_id']){
                    echo "<tr id='row-"; echo $bom[$x]['id']; echo "-"; echo $data_split[$j]['id']; echo "'>";
                    echo "<td class='text-left' style='font-size:1em;'></td>
                          <td class='text-left' style='font-size:1em;'></td>
                          <td class='text-left' style='font-size:1em;'>"; echo $bom[$x]['nama_bb']; echo "</td>
                          <td ><input type='numeric' style='font-size:1em; display:block; margin:0 auto; width:100%; padding-left:5px;' value='"; echo $data_split[$j]['qty']; echo "'' id='qty-"; echo $bom[$x]['id']; echo "-"; echo $data_split[$j]['id']; echo "'></input></td>
                          <td class='text-center' style='font-size:1em;' id='uom-"; echo $bom[$x]['id']; echo "'>"; echo $data_split[$j]['uom']; echo "</td>
                          <td class='text-center' style='font-size:1em;'></td>
                          <td class='text-center' style='font-size:1em;'></td>
                          <td class='text-center' style='font-size:1em;'>
                            <select id='siklus-"; echo $bom[$x]['id']; echo "-"; echo $data_split[$j]['id']; echo "' style='height:2em; text-align:center; width: 70%; display:block; margin:0 auto;' class='form-control form-control-sm text-center'>";
                              if ($data_split[$j]['siklus'] == 1){
                                echo '<option value="1" selected>1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>';
                              } else if ($data_split[$j]['siklus'] == 2) {
                                echo '<option value="1">1</option>
                                <option value="2" selected>2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>';
                              } else if ($data_split[$j]['siklus'] == 3) {
                                echo '<option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3" selected>3</option>
                                <option value="4">4</option>';
                              } else if ($data_split[$j]['siklus'] == 4) {
                                echo '<option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4" selected>4</option>';
                              }
                            echo "</select>
                          </td>
                          <td class='text-center' style='font-size:1em;'>
                            <select style='height:2em; text-align:left; width: 70%; display:block; margin:0 auto;' class='form-control form-control-sm text-center'>
                              <option disabled selected value>Choose...</option>
                              <option value='1'>1</option>
                              <option value='2'>2</option>
                              <option value='3'>3</option>
                              <option value='4'>4</option>
                            </select>
                          </td>
                          <td class='text-left' style='font-size:1em;'><div id='timbangan-"; echo $x+1; echo "'></div></td>
                          <td class='text-left' style='font-size:1em;'>
                            <button type='button' onclick=deleteRow('"; echo "row-"; echo $bom[$x]['id']; echo "-"; echo $data_split[$j]['id']; echo "',"; echo $bom[$x]['id']; echo ") class='btn btn-dark btn-sm product2 btn-minus' style='display:block; margin: 0 auto; background:#FF4C4C; width:75%; opacity:0.85; border-radius:0.7em;' id='minus-"; echo $bom[$x]['id']; echo "'><i class='fa fa-minus'></i></button>
                          </td>
                        </tr>";
                  }
                }
              }
            }?> 
            </tbody>
      </table>  
    </div>
    
  </div>
</div>

<?php
$script = <<< JS

  var nomo = '$nomo';
  var reference = '$reference';
  var kode_bulk = nomo.substring(0,nomo.lastIndexOf("/"));
  //console.log(kode_bulk);
  var qty_batch = '$qty_batch';
  var standard = 0.0;

  $.post("index.php?r=log-formula-breakdown/get-standard-qty&kode_bulk="+kode_bulk+"&reference="+reference, function (data2){
    standard = data2;
  });

  $.post("index.php?r=master-data-timbangan-rm/get-line-timbang-rm-nomo&nomo="+nomo, function (data){
      $('div[id^="nama_line"]').html(data);
  });

  $.post("index.php?r=master-data-timbangan-rm/get-timbang-nomo&nomo="+nomo, function (data){
      $('div[id^="timbangan-"]').html(data);
  });

  var j = $('section[class="content-header"]').html(`
    <div class="row" style='background-color: white; height:50px; margin-left:0px; margin-top:0px; padding-left:1%; padding-top:1px;'>
      <div class="col-6" style="float:left">
        <h4>
          <a style='color:#1fd164;'>Bills of Materials</a> / <a style='color:#6f6f6f;'>`+bom[0]['id']+`: [`+bom[0]['kode_bulk']+`] `+bom[0]['nama_fg']+`</a>
        </h4>
      </div>
      <div class="col-6" style="float:right; padding-right:5%;">
        <h4>
          <a style='color:#1fd164;'>Quantity</a> : <a style='color:#6f6f6f;'>`+qty_batch+` kg</a>
        </h4>
      </div>
    </div>`);

  var sumFix = 0.0;
  $.post("index.php?r=log-formula-breakdown/get-sum&kode_bulk="+kode_bulk+"&reference="+reference, function (data){
    sumFix = data;
    
    sumFix = Number(sumFix).toFixed(4);
    $('input[id="sum"]').val(sumFix);
  });

  setInterval(function(){ 
    $('input[id^="qty-"]').on("click",function() {
      var nilai = $(this).val();
      if (nilai != 0){} else { $(this).val(""); }
    });

    $('input[id^="qty-"]').on("focusout blur",function() {
      var nilai = $(this).val();
      var id = $(this).attr('id');
      var sumPerItem = 0.0;
      var qty_standard_per_item = 0.0;

      id2 = id.substring(id.lastIndexOf("-") + 1);
      id3 = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      // AMBIL DATA STANDARD QTY PER BAHAN BAKU
      $.post("index.php?r=log-formula-breakdown/get-standard-qty-per-item&id="+id3, function (data)
      {
        qty_standard_per_item = Number(data).toFixed(4);

        // INPUT DATA SPLIT DARI ITEM TERPILIH
        if ((nilai>0) && (!isNaN(nilai)) && (nilai!="")){
          $.post("index.php?r=log-formula-breakdown/update-split-qty&id_split="+id2+"&qty="+nilai, function (data){});
          console.log("1");
        } else {
          //$(this).val(0);
          document.getElementById(id).value = 0;
          $.post("index.php?r=log-formula-breakdown/update-split-qty&id_split="+id2+"&qty=0", function (data){});
          console.log("2");
        }

        // AMBIL DATA TOTAL QTY YANG SUDAH DI INPUT (SPLIT) UNTUK BAHAN BAKU TERPILIH
        $.post("index.php?r=log-formula-breakdown/get-sum-per-item&id="+id3, function (data)
        {
          var idQty = "qty-"+id3;
          sumPerItem = Number(data).toFixed(4);

          console.log("sum:"+sumPerItem+"  standard:"+qty_standard_per_item);
          console.log(sumPerItem-qty_standard_per_item);
          
          if ((sumPerItem-qty_standard_per_item)>0)
          {
            $('input[id^="qty-105-"]').css('box-shadow', '0px 0px 2px 2px #ff6666');
            console.log("3");
          } else if ((sumPerItem-qty_standard_per_item)<0){
            //document.getElementById("sum").style.boxShadow = "";
            $('input[id^="qty-105-"]').css('box-shadow', '0px 0px 2px 2px #ffff00');
            console.log("4");
          } else {
            $('input[id^="qty-105-"]').css('box-shadow', '0px 0px 2px 2px #3CB371');
            console.log("5");
          }
        });

        $.post("index.php?r=log-formula-breakdown/get-standard-qty&kode_bulk="+kode_bulk+"&reference="+reference, function (data2){
          standard = Number(data2).toFixed(4);
          $.post("index.php?r=log-formula-breakdown/get-sum&kode_bulk="+kode_bulk+"&reference="+reference, function (data){
            //sumFix = data;
            sumFix = Number(data).toFixed(4);
            //standard = data2;
            
            // console.log("sum:"+sumFix+"  standard:"+standard);
            // console.log(sumFix-standard);
            
            if ((sumFix-standard)>0){
              $('input[id="sum"]').val("over");
              document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #ff6666";
              console.log("6");
            } else if ((sumFix-standard)<0){
              $('input[id="sum"]').val(sumFix);
              document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #ffff00";
              console.log("7");
            } else {
              $('input[id="sum"]').val(sumFix);
              document.getElementById("sum").style.boxShadow = "0px 0px 2px 2px #3CB371";
              console.log("8");
            }
          });
        });
      });
    });

    $('select[id^="siklus-"]').on("change",function() {
      var id = $(this).attr('id');
      var nilai = $(this).val();

      id2 = id.substring(id.lastIndexOf("-") + 1);
      id3 = id.substring(id.indexOf("-") + 1, id.lastIndexOf("-"));

      if (id2!=0){
        $.post("index.php?r=log-formula-breakdown/update-siklus-split&id_split="+id2+"&siklus="+nilai, function (data){});
      } else {
        $.post("index.php?r=log-formula-breakdown/update-siklus&id="+id3+"&siklus="+nilai, function (data){
        });
      }
    });

  }, 2000);


JS;
$this->registerJs($script);
?>