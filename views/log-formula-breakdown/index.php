<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogFormulaBreakdownSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Formula Breakdowns';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-formula-breakdown-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Log Formula Breakdown', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nomo',
            'fg_name',
            'reference',
            'location',
            // 'scheduled_start',
            // 'week',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
