<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FlowReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="flow-report-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nosmb') ?>

    <?= $form->field($model, 'nobatch') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'nadagang') ?>

    <?php // echo $form->field($model, 'process_position') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
