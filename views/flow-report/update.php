<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FlowReport */

$this->title = 'Update Flow Report: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Flow Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="flow-report-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
