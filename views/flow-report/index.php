<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FlowReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Flow Reports';
/*
$this->params['breadcrumbs'][] = $this->title;
*/
?>
<div class="flow-report-index">
<?php
$countSql = Yii::$app->db->createCommand('
    SELECT COUNT(*) FROM flow_report')->queryScalar();
?>

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        <?= Html::a('Create Flow Report', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
     -->


<!--  -->
 <?php 
 //session_destroy();
 if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
 if(empty($_SESSION['count'])) $_SESSION['count'] = 0;
 $order =  $_SESSION['count']+1;
 $_SESSION['count'] =  $order; 
 //$order = 0;
 if($order>$countSql/25) session_destroy();
 //$order = 0;
 //if ($order == 10) $order = 0; 
 $uri = "index.php?r=flow-report/index&page=$order";
 ?>

<!-- This Is The Code To Refresh The Page , Click Refresh Button for Every 5 Seconds  -->
<?php
$script = <<< JS
$(document).ready(function() {
    setInterval(function(){ $("#siteIndex").click(); }, 5000);

});
JS;
$this->registerJs($script);
?>

<!-- End Of Auto Refresh Script -->
    

      


    <?php Pjax::begin(); ?>
    <!-- Define Refresh Button -->
    <?=
    Html::a("", ['flow-report/index'], [/*'class' => 'btn btn-lg btn-primary',*/ 'id' => 'refreshButton']) 
    ?> 
    <!-- Define Next Page Button-->
    <!-- Not yet Implemented    -->
    <?=
    Html::a("", $uri, [/*'class' => 'btn btn-lg btn-primary',*/ 'id' => 'siteIndex']);
    ?>
    <!-- Define Grid View -->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        
        //'filterModel' => $searchModel,
        'rowOptions' => function($model){
                if($model->process_position == 'QC Bulk')
                {
                    return ['class'=>'danger'];
                }else if($model->process_position == 'Pengolahan')
                {
                    return ['style'=>'background-color:#ccd9ff', 'font-size' => 16];
                }
                else if($model->process_position == 'Press')
                {
                    return ['style'=>'background-color:#b3ffb3', 'font-size' => 16];
                }
                else if($model->process_position == 'Kemas 1')
                {
                    return ['style'=>'background-color:#ffdab3', 'font-size' => 16];
                }
                else if($model->process_position == 'Kemas 2')
                {
                    return ['style'=>'background-color: #9999ff', 'font-size' => 16];
                }
                else if($model->process_position == 'Penimbangan')
                {
                    return ['style'=>'background-color:#ffe699', 'font-size' => 16];
                }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],



            //'id',
            //[
            //'value' => 'nosmb',
            //'contentOptions'=>['style'=>'background-color:#ffe699'] // <-- right here
            // ],
            'nosmb',
            'nobatch',
            'snfg',
            'nadagang',
            'process_position',

            //['option' => ['style' => 'line-height:90px;']]

            /*[
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            ],*/

        ],
    ]); ?>

    <?php Pjax::end(); ?>

    



</div>
