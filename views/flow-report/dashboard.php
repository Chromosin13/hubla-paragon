<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FlowReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dashboard Panel';
/*
$this->params['breadcrumbs'][] = $this->title;
*/
?>
<div class="flow-report-index">
<?php
$countSql = Yii::$app->db->createCommand('
    SELECT COUNT(*) FROM posisi_proses')->queryScalar();
?>

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        <?= Html::a('Create Flow Report', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
     -->


<!--  -->

<!-- This Is The Code To Refresh The Page , Click Refresh Button for Every 5 Seconds  -->
<?php
$script = <<< JS
$(document).ready(function() {
    setInterval(function(){ $("#refreshButton").click(); }, 600000);

});
JS;
$this->registerJs($script);
?>

<!-- End Of Auto Refresh Script -->


    <?php Pjax::begin(); ?>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
              <!-- Apply any bg-* class to to the icon to color it -->
              <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Number Of Entry SMB</span>
                <span class="info-box-number">
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT COUNT(*) FROM flow_report')->queryScalar();
                    ?>          
                </span>
              </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <!-- Apply any bg-* class to to the icon to color it -->
          <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Last No SMB</span>
            <span class="info-box-number"> 
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT nosmb FROM flow_report where id=(select max(id) from flow_report)')->queryScalar();
                    ?>    
            </span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->  
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-files-o"></i></span>
        <div class="info-box-content">
            <span class="info-box-text">Last SNFG</span>
            <span class="info-box-number">
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT snfg FROM flow_report where id=(select max(id) from flow_report)')->queryScalar();
                    ?>   
            </span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-flag-o"></i></span>
        <div class="info-box-content">
            <span class="info-box-text">Current Most Process
            </span>
            <span class="info-box-number">
                <?php
                       
     $processList = Yii::$app->db->createCommand('
                                                SELECT a.process_position from
                        (SELECT process_position,count(process_position) process_count
                                                    FROM flow_report
                                                    GROUP BY process_position) a
                        where a.process_count=(SELECT max(b.process_count) from
                        (SELECT process_position,count(process_position) process_count
                                                    FROM flow_report
                                                    GROUP BY process_position) b
                        )'
                        )->queryAll();
    echo implode(', ',array_column($processList,'process_position'));

                    ?>   


            </span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div>

    <!-- Define Refresh Button -->
    <?=
    Html::a("", ['flow-report/dashboard'], [/*'class' => 'btn btn-lg btn-primary',*/ 'id' => 'refreshButton']) 
    ?> 
    <!-- Define Next Page Button-->
    <!-- Not yet Implemented    -->


    <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Dashboard Flowboard</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <!-- <p class="text-center">
                    <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
                  </p>
 -->
                  <div class="chart">
                   <?php
                    $countPengolahan = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position='Pengolahan'")->queryScalar();
                    $countPress = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position='Press'")->queryScalar();
                    $countQCBulk = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position='QC Bulk'")->queryScalar();
                    $countQCFG = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position='QC FG'")->queryScalar();
                    $countKTimbang = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position='Karantina Timbang'")->queryScalar();
                    $countQCKemas = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position ilike 'Kemas%'")->queryScalar();
                    echo Highcharts::widget([
                                                'scripts' => [
                                                    'modules/exporting',
                                                    'themes/grid-light',
                                                ],
                                               'options' => [
                                                  'title' => ['text' => 'Status'],
                                                  'xAxis' => [
                                                     'categories' => ['Pengolahan', 'Press', 'QC Bulk','QC FG', 'Karantina Timbang', 'Kemas']
                                                  ],
                                                  'yAxis' => [
                                                     'title' => ['text' => 'Jumlah Barang']
                                                  ],
                                                   'plotOptions' => [
                                                                    'line' => [
                                                                        'dataLabels' => [
                                                                                            'enabled' => true
                                                                                        ],
                                                                        'enableMouseTracking' => true
                                                                        ]
                                                                    ],
                                                  'series' => [
                                                     ['name' => 'Proses', 'data' => [$countPengolahan,  $countPress, $countQCBulk, $countQCFG, $countKTimbang, $countQCKemas]]
                                                  ]
                                               ]
                                            ]);
                    ?>

                    <!-- Sales Chart Canvas -->
                    <!-- <canvas id="salesChart" style="height: 189px; width: 499px;" width="474" height="241">
                    
                    </canvas> -->
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <p class="text-center">
                    <strong>Production Stats</strong>
                  </p>

                  <div class="progress-group">
                    <span class="progress-text">Pengolahan</span>
                    <span class="progress-number"><b>
                    <?=
                                                        $countSql = Yii::$app->db->createCommand("
                                                        SELECT COUNT(process_position) FROM flow_report where process_position='Pengolahan'")->queryScalar();
                    ?>
                    </b>
                    /
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT COUNT(*) FROM flow_report')->queryScalar();
                    ?>

                    </span>
        
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="width: 
                                       <?php
                                                $countPengolahan = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position='Pengolahan'")->queryScalar();
                                                $countAll = Yii::$app->db->createCommand('
                                                                             SELECT COUNT(*) FROM flow_report')->queryScalar();
                                                                     
                                      if ($countAll==0) {
                                          echo 0;
                                      } else {
                                          echo ceil(($countPengolahan/$countAll)*100);
                                      }
                                      ?>%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Press</span>
                    <span class="progress-number"><b>
                    <?=
                                                        $countSql = Yii::$app->db->createCommand("
                                                        SELECT COUNT(process_position) FROM flow_report where process_position='Press'")->queryScalar();
                    ?>
                    </b>
                    /
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT COUNT(*) FROM flow_report')->queryScalar();
                    ?></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: <?php
                                                $countPress = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position='Press'")->queryScalar();
                                                $countAll = Yii::$app->db->createCommand('
                                                                             SELECT COUNT(*) FROM flow_report')->queryScalar();
                                                                     
                                      if ($countAll==0) {
                                          echo 0;
                                      } else {
                                          echo ceil(($countPress/$countAll)*100);
                                      }
                                      ?>%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">QC Bulk</span>
                    <span class="progress-number"><b>
                    <?=
                                                        $countSql = Yii::$app->db->createCommand("
                                                        SELECT COUNT(process_position) FROM flow_report where process_position='QC Bulk'")->queryScalar();
                    ?>
                    </b>
                    /
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT COUNT(*) FROM flow_report')->queryScalar();
                    ?></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: <?php
                                                $countQCBulk = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position='QC Bulk'")->queryScalar();
                                                $countAll = Yii::$app->db->createCommand('
                                                                             SELECT COUNT(*) FROM flow_report')->queryScalar();
                                                                     
                                      if ($countAll==0) {
                                          echo 0;
                                      } else {
                                          echo ceil(($countQCBulk/$countAll)*100);
                                      }
                                      ?>%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">QC FG</span>
                    <span class="progress-number"><b>
                    <?=
                                                        $countSql = Yii::$app->db->createCommand("
                                                        SELECT COUNT(process_position) FROM flow_report where process_position='QC FG'")->queryScalar();
                    ?>
                    </b>
                    /
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT COUNT(*) FROM flow_report')->queryScalar();
                    ?></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: <?php
                                                $countQCFG = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position='QC FG'")->queryScalar();
                                                $countAll = Yii::$app->db->createCommand('
                                                                             SELECT COUNT(*) FROM flow_report')->queryScalar();
                                                                     
                                      if ($countAll==0) {
                                          echo 0;
                                      } else {
                                          echo ceil(($countQCFG/$countAll)*100);
                                      }
                                      ?>%"></div>
                    </div>
                  </div>

                  <div class="progress-group">
                    <span class="progress-text">Karantina Timbang</span>
                    <span class="progress-number"><b>
                    <?=
                                                        $countSql = Yii::$app->db->createCommand("
                                                        SELECT COUNT(process_position) FROM flow_report where process_position='Karantina Timbang'")->queryScalar();
                    ?>
                    </b>
                    /
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT COUNT(*) FROM flow_report')->queryScalar();
                    ?></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-black" style="width: 
                              <?php
                                                $countKTimbang = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position='Karantina Timbang'")->queryScalar();
                                                $countAll = Yii::$app->db->createCommand('
                                                                             SELECT COUNT(*) FROM flow_report')->queryScalar();
                                                                     
                                      if ($countAll==0) {
                                          echo 0;
                                      } else {
                                          echo ceil(($countKTimbang/$countAll)*100);
                                      }?>%">
                      </div>
                    </div>
                  </div>

                  <div class="progress-group">
                    <span class="progress-text">Kemas</span>
                    <span class="progress-number"><b>
                    <?=
                                                        $countSql = Yii::$app->db->createCommand("
                                                        SELECT COUNT(process_position) FROM flow_report where process_position ilike 'Kemas%'")->queryScalar();
                    ?>
                    </b>
                    /
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT COUNT(*) FROM flow_report')->queryScalar();
                    ?></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-light-blue" style="width: <?php
                                                $countKemas = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position ilike 'Kemas%'")->queryScalar();
                                                $countAll = Yii::$app->db->createCommand('
                                                                             SELECT COUNT(*) FROM flow_report')->queryScalar();
                                                                     
                                      if ($countAll==0) {
                                          echo 0;
                                      } else {
                                          echo ceil(($countKemas/$countAll)*100);
                                      }?>%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                </div>
                <div class="col-md-8">
                    <?php
                    $this->registerJsFile('http://code.highcharts.com/mapdata/countries/id/id-all.js', 
                    [
                        'depends' => 'miloschuman\highcharts\HighchartsAsset'
                    ]);

                    echo Highmaps::widget([

                        'options' => [
                            'title' => [
                                'text' => 'Peta Indonesia',
                            ],
                            'mapNavigation' => [
                                'enabled' => true,
                                'buttonOptions' => [
                                    'verticalAlign' => 'bottom',
                                ]
                            ],
                            'colorAxis' => [
                                'min' => 0,
                            ],
                            'series' => [
                                [
                                    'data' => [
                                        ['hc-key' => 'id-ac', 'value' => 100],
                                    ],
                                    'mapData' => new JsExpression('Highcharts.maps["countries/id/id-all"]'),
                                    'joinBy' => 'hc-key',
                                    'name' => 'Random data',
                                    'states' => [
                                        'hover' => [
                                            'color' => '#BADA55',
                                        ]
                                    ],
                                    'dataLabels' => [
                                        'enabled' => true,
                                        'format' => '{point.name}',
                                    ]
                                ]
                            ]
                        ]
                    ]);

                    ?>
                </div>
                <div class="col-md-4">

                <?php

                $countPengolahan = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position='Pengolahan'")->queryScalar();
                    $countPress = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position='Press'")->queryScalar();
                    $countQCBulk = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position='QC Bulk'")->queryScalar();
                    $countQCFG = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position='QC FG'")->queryScalar();
                    $countKTimbang = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position='Karantina Timbang'")->queryScalar();
                    $countQCKemas = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(process_position) FROM flow_report where process_position ilike 'Kemas%'")->queryScalar();

                echo Highcharts::widget([
                    'options' => [
                        'title' => ['text' => 'Pie Chart - Progress'],
                        'plotOptions' => [
                            'pie' => [
                                'cursor' => 'pointer',
                            ],
                        ],
                        'series' => [
                            [ // new opening bracket
                                'type' => 'pie',
                                'name' => 'Elements',
                                'data' => [
                                    ['Pengolahan', $countPengolahan],
                                    ['Press', $countPress],
                                    ['QC Bulk', $countQCBulk],
                                    ['QC FG', $countQCFG],
                                    ['Karantina Timbang', $countKTimbang],
                                    ['Kemas',$countQCKemas]
                                ],
                            ] // new closing bracket
                        ],
                    ],
                ]);            
                ?>

                </div>
                
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>
                    <h5 class="description-header">$35,210.43</h5>
                    <span class="description-text">TOTAL REVENUE</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
                    <h5 class="description-header">$10,390.90</h5>
                    <span class="description-text">TOTAL COST</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>
                    <h5 class="description-header">$24,813.53</h5>
                    <span class="description-text">TOTAL PROFIT</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block">
                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
                    <h5 class="description-header">1200</h5>
                    <span class="description-text">GOAL COMPLETIONS</span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>


    <?php Pjax::end(); ?>

    <?php 
        echo \miloschuman\highcharts\Highcharts::widget([
        'scripts' => [
            'highcharts-3d',
            'modules/drilldown',
            'modules/exporting',
            'themes/sand-signika',
        ],
        'options' => [
            "chart" => [
                "type" => "pie",
                "options3d" => [
                    "enabled" => true,
                    "alpha" => 45
                ]
            ],
            "title" => [
                "text" => "Browser market shares. January, 2015 to May, 2015"
            ],
            "subtitle" => [
                "text" => "Click the slices to view versions. Source: netmarketshare.com."
            ],
            "plotOptions" => [
                "pie" => [
                    "innerSize" => 100,
                    "depth" => 45
                ],
                "series" => [
                    "dataLabels" => [
                        "enabled" => true,
                        "format" => "{point.name}: {point.y:.1f}%"
                    ]
                ]
            ],
            "series" => [
                [
                    "name" => "Brands",
                    "colorByPoint" => true,
                    "data" => [
                        [
                            "name" => "Microsoft Internet Explorer",
                            "y" => 56.33,
                            "drilldown" => "Microsoft Internet Explorer"
                        ],
                        [
                            "name" => "Chrome",
                            "y" => 24.03,
                            "drilldown" => "Chrome"
                        ],
                        [
                            "name" => "Firefox",
                            "y" => 10.38,
                            "drilldown" => "Firefox"
                        ],
                        [
                            "name" => "Safari",
                            "y" => 4.77,
                            "drilldown" => "Safari"
                        ],
                        [
                            "name" => "Opera",
                            "y" => 0.91,
                            "drilldown" => "Opera"
                        ]
                    ]
                ]
            ],
            "drilldown" => [
                "series" => [
                    [
                        "name" => "Microsoft Internet Explorer",
                        "id" => "Microsoft Internet Explorer",
                        "data" => [
                            ["v11.0", 24.13],
                            ["v8.0", 17.2],
                            ["v9.0", 8.11],
                            ["v10.0", 5.33],
                            ["v6.0", 1.06],
                            ["v7.0", 0.5]
                        ]
                    ],
                    [
                        "name" => "Chrome",
                        "id" => "Chrome",
                        "data" => [
                            ["v40.0", 5],
                            ["v41.0", 4.32],
                            ["v42.0", 3.68],
                            ["v39.0", 2.96],
                            ["v36.0", 2.53],
                            ["v43.0", 1.45],
                            ["v31.0", 1.24],
                            ["v35.0", 0.85],
                            ["v38.0", 0.6],
                            ["v32.0", 0.55],
                            ["v37.0", 0.38],
                            ["v33.0", 0.19],
                            ["v34.0", 0.14],
                            ["v30.0", 0.14]
                        ]
                    ],
                    [
                        "name" => "Firefox",
                        "id" => "Firefox",
                        "data" => [
                            ["v35", 2.76],
                            ["v36", 2.32],
                            ["v37", 2.31],
                            ["v34", 1.27],
                            ["v38", 1.02],
                            ["v31", 0.33],
                            ["v33", 0.22],
                            ["v32", 0.15]
                        ]
                    ],
                    [
                        "name" => "Safari",
                        "id" => "Safari",
                        "data" => [
                            ["v8.0", 2.56],
                            ["v7.1", 0.77],
                            ["v5.1", 0.42],
                            ["v5.0", 0.3],
                            ["v6.1", 0.29],
                            ["v7.0", 0.26],
                            ["v6.2", 0.17]
                        ]
                    ],
                    [
                        "name" => "Opera",
                        "id" => "Opera",
                        "data" => [
                            ["v12.x", 0.34],
                            ["v28", 0.24],
                            ["v27", 0.17],
                            ["v29", 0.16]
                        ]
                    ]
                ]
            ]
    ]]);

    ?>


</div>

