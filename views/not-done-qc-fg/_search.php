<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NotDoneQcFgSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="not-done-qc-fg-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'status_palet') ?>

    <?= $form->field($model, 'jumlah_palet_diterima_ndc') ?>

    <?= $form->field($model, 'jumlah_palet') ?>

    <?= $form->field($model, 'last_update') ?>

    <?= $form->field($model, 'due') ?>

    <?php echo $form->field($model, 'hari_ini') ?>

    <?php echo $form->field($model, 'snfg') ?>

    <?php echo $form->field($model, 'nama_fg') ?>

    <?php echo $form->field($model, 'nama_bulk') ?>

    <?php echo $form->field($model, 'margin') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Reset', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
