<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NotDoneQcFg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="not-done-qc-fg-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status_palet')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'jumlah_palet_diterima_ndc')->textInput() ?>

    <?= $form->field($model, 'jumlah_palet')->textInput() ?>

    <?= $form->field($model, 'last_update')->textInput() ?>

    <?= $form->field($model, 'due')->textInput() ?>

    <?= $form->field($model, 'hari_ini')->textInput() ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'nama_bulk')->textInput() ?>

    <?= $form->field($model, 'margin')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
