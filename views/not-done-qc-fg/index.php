<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NotDoneQcFgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<div class="not-done-qc-fg-index">


    <div class="panel box box-primary">
      <div class="box-header with-border">
        <h4 class="box-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="collapsed" aria-expanded="false">
            Filter Search
          </a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
        <div class="box-body">
             <?php 
                echo $this->render('_search', ['model' => $searchModel]); 
            ?>
        </div>
      </div>
    </div>


    <?php
        Modal::begin([
            'header'=>'<h4>INPUT ALASAN IS DONE MANUAL</h4>',
            'id'=>'update-modal',
            'size'=>'modal-success modal-lg',
            
        ]);

        echo "<div id='updateModalContent'></div>";

        Modal::end();
    ?>
   

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'floatHeader'=>'true',
        'floatHeaderOptions' => ['position' => 'absolute',],
        'showPageSummary'=>true,
        'pjax'=>true,
        'striped'=>true,
        'hover'=>true,
        'condensed'=>true,
        'panel'=>['type'=>'danger', 'heading'=>'Dashboard BELUM DONE QC FG'],
        'toolbar' => [
        ],
        'rowOptions'   => function ($model, $key, $index, $grid) {
            if($model['status_palet']=='Complete NDC'){

                return [
                        'value'=>Url::to('index.php?r=exception-is-done-qc-fg/update-data&snfg='.$model['snfg']),
                        'class' => 'danger',
                        'data-toggle' => 'tooltip',
                        'data-id' => $model['snfg'],
                        // 'id'=>'popupModal'
                        
                ];
            }else if($model['status_palet']=='Partial NDC'){
                return [
                        'value'=>Url::to('index.php?r=exception-is-done-qc-fg/update-data&snfg='.$model['snfg']),
                        'class' => 'warning',
                        'data-toggle' => 'tooltip',
                        'data-id' => $model['snfg'],
                        // 'id'=>'popupModal'
                        
                ];
            }else{
                return [
                        'value'=>Url::to('index.php?r=exception-is-done-qc-fg/update-data&snfg='.$model['snfg']),
                        'class' => 'default',
                        'data-toggle' => 'tooltip',
                        'data-id' => $model['snfg'],
                        // 'id'=>'popupModal'
                        
                ];
            }

        },
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'snfg',
            'status_palet:ntext',
            'jumlah_palet_diterima_ndc',
            'jumlah_palet',
            'last_update',
            'due',
            'hari_ini',
            'nama_fg',
            'nama_bulk',
            'margin',
        ],
    ]); ?>
</div>


<?php
$this->registerJs("


    $('td').click(function (e) {

        e.preventDefault();

         $('#update-modal').modal('show')
         .find('#updateModalContent')
         .load($(this).closest('tr').attr('value'))
        
        $('#update-modal').on('shown.bs.modal', function () {
          $('#qty_done').focus()
        })
    
    });


");

?>
