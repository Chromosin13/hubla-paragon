<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NotDoneQcFg */

$this->title = 'Update Not Done Qc Fg: ' . $model->snfg;
$this->params['breadcrumbs'][] = ['label' => 'Not Done Qc Fgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->snfg, 'url' => ['view', 'id' => $model->snfg]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="not-done-qc-fg-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
