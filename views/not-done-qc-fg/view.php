<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\NotDoneQcFg */

$this->title = $model->snfg;
$this->params['breadcrumbs'][] = ['label' => 'Not Done Qc Fgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="not-done-qc-fg-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->snfg], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->snfg], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'status_palet:ntext',
            'jumlah_palet_diterima_ndc',
            'jumlah_palet',
            'last_update',
            'due',
            'hari_ini',
            'snfg',
            'nama_fg',
            'nama_bulk',
            'margin',
        ],
    ]) ?>

</div>
