<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\NotDoneQcFg */

$this->title = 'Create Not Done Qc Fg';
$this->params['breadcrumbs'][] = ['label' => 'Not Done Qc Fgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="not-done-qc-fg-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
