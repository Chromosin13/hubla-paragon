<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IdentitasProduk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="identitas-produk-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'id')->textInput() ?> -->

    <?= $form->field($model, 'nama_produk')->textInput() ?>

    <?= $form->field($model, 'batch_produk')->textInput() ?>

    <?= $form->field($model, 'kode_sl')->textInput() ?>

    <?= $form->field($model, 'no_notifikasi')->textInput() ?>

    <?= $form->field($model, 'barcode_produk')->textInput() ?>

    <?= $form->field($model, 'exp_date')->textInput() ?>

    <?= $form->field($model, 'no_mo')->textInput() ?>

    <?= $form->field($model, 'line_kemas')->textInput() ?>

    <?= $form->field($model, 'due_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
