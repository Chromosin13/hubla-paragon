<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IdentitasProdukSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Identitas Produks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="identitas-produk-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Identitas Produk', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nama_produk',
            'batch_produk',
            'kode_sl',
            'no_notifikasi',
            'barcode_produk',
            'exp_date',
            'no_mo',
            'line_kemas',
            'due_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
