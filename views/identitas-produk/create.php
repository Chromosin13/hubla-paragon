<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\IdentitasProduk */

$this->title = 'Create Identitas Produk';
$this->params['breadcrumbs'][] = ['label' => 'Identitas Produks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="identitas-produk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
