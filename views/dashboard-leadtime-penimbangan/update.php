<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DashboardLeadtimePenimbangan */

$this->title = 'Update Dashboard Leadtime Penimbangan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Leadtime Penimbangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dashboard-leadtime-penimbangan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
