<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DashboardLeadtimePenimbangan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dashboard-leadtime-penimbangan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'nomo')->textInput() ?>

    <?= $form->field($model, 'posisi')->textInput() ?>

    <?= $form->field($model, 'start')->textInput() ?>

    <?= $form->field($model, 'stop')->textInput() ?>

    <?= $form->field($model, 'sum_leadtime_raw')->textInput() ?>

    <?= $form->field($model, 'sum_leadtime_bruto')->textInput() ?>

    <?= $form->field($model, 'jenis_proses')->textInput() ?>

    <?= $form->field($model, 'sediaan')->textInput() ?>

    <?= $form->field($model, 'sum_downtime')->textInput() ?>

    <?= $form->field($model, 'sum_leadtime_net')->textInput() ?>

    <?= $form->field($model, 'keterangan_downtime')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'snfg')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'snfg_komponen')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'upload_date')->textInput() ?>

    <?= $form->field($model, 'week')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
