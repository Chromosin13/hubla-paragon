<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DashboardLeadtimePenimbangan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Leadtime Penimbangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dashboard-leadtime-penimbangan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nama_fg',
            'nomo',
            'posisi',
            'start',
            'stop',
            'sum_leadtime_raw',
            'sum_leadtime_bruto',
            'jenis_proses',
            'sediaan',
            'sum_downtime',
            'sum_leadtime_net',
            'keterangan_downtime:ntext',
            'snfg:ntext',
            'snfg_komponen:ntext',
            'id',
            'upload_date',
            'week',
        ],
    ]) ?>

</div>
