<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DashboardLeadtimePengolahanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="box box-widget widget-user">
    <div class="widget-user-header bg-blue-gradient">
      <h3 class="widget-user-username"><b>Leadtime Penimbangan</b></h3>
      <h4 class="widget-user-desc">Dashboard</h4>
    </div>
    <div class="widget-user-image">
      <img class="img-circle" src="../web/images/proses_icons/weighing.png" alt="User Avatar" style="width:20%">
    </div>

    <div class="box-footer">
      <div class="row">
        <div class="box-body">

            <div class="box-body">
                <?php

                    $gridColumns = [
                    'nama_fg',
                    'nomo',
                    'posisi',
                    'nama_line',
                    'start',
                    'stop',
                    'sum_leadtime_raw',
                    'sum_leadtime_bruto',
                    'jenis_proses',
                    'sediaan',
                    'sum_downtime',
                    'sum_leadtime_net',
                    'keterangan_downtime:ntext',
                    'snfg:ntext',
                    'snfg_komponen:ntext',
                    'id',
                    'week',
                    'upload_date'
                    ];

                    // Renders a export dropdown menu
                    echo ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $gridColumns
                    ]);
                ?>

                <?php echo $this->render('_search', ['model' => $searchModel,'sediaan_list'=>$sediaan_list,'nama_fg_list'=>$nama_fg_list]); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'floatHeader'=>'true',
                    'floatHeaderOptions' => ['position' => 'absolute',],
                    'showPageSummary'=>true,
                    'pjax'=>true,
                    'striped'=>true,
                    'hover'=>true,
                    'panel'=>['type'=>'primary', 'heading'=>'Grid'],
                    'toolbar' => [
                    ],
                    'columns' => [
                        ['class' => 'kartik\grid\SerialColumn'],
                        'week',
                        'upload_date',
                        'nama_fg',
                        'nomo',
                        'posisi',
                        'nama_line',
                        'start',
                        'stop',
                        'sum_leadtime_raw',
                        'sum_leadtime_bruto',
                        'jenis_proses',
                        'sediaan',
                        'sum_downtime',
                        'sum_leadtime_net',
                        'keterangan_downtime:ntext',
                        'snfg:ntext',
                        'snfg_komponen:ntext',
                        'id',
                    ],
                ]); ?>
            </div>

        </div>
      </div>
      <!-- /.row -->
    </div>
</div>
