<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\widgets\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\DashboardLeadtimePengolahanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dashboard-leadtime-pengolahan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <p></p>

    <!-- Accordion -->
    <div class="panel box box-primary">
      <div class="box-header with-border">
        <h4 class="box-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false">
            Filter Field
          </a>
        </h4>
      </div>
      <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
        <div class="box-body">
            <p></p>

            <div class="row">
                <div class="col-md-6">
                  <?php 
                        echo '<label class="control-label">Upload Date Range</label>';
                        echo 

                             DateRangePicker::widget([
                                'model'=>$model,
                                'attribute'=>'upload_date',
                                'convertFormat'=>true,
                                'pluginOptions'=>[
                                    'timePicker'=>true,
                                    'timePickerIncrement'=>30,
                                    // 'startDate'=>'2018-01-01',
                                    // 'endDate'=>'2018-12-31',
                                    'ranges' => [
                                        '1 Year' => ["moment().startOf('day').subtract(1, 'year')", "moment()"],
                                        '1 Month' => ["moment().startOf('day').subtract(1, 'month')", "moment()"],
                                    ],
                                    'locale'=>[
                                        'format'=>'Y-m-d'
                                    ],
                                    'opens'=>'left',
                                ]
                            ]);
                    ?>
                    <p></p>
                  <!-- /input-group -->
                </div>
                <div class="col-md-3">
                    <?php 

                        echo $form->field($model, 'nama_fg')->widget(Select2::classname(), [
                            'data' => $nama_fg_list,
                            'options' => ['placeholder' => 'By Nama FG'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);

                    ?>
                  <!-- /input-group -->
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'nomo') ?>
                  <!-- /input-group -->
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <?php echo $form->field($model, 'jenis_proses') ?>
                  <!-- /input-group -->
                </div>
                <div class="col-md-3">
                    <?php 

                        echo $form->field($model, 'sediaan')->widget(Select2::classname(), [
                            'data' => $sediaan_list,
                            'options' => ['placeholder' => 'Select Sediaan'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);

                    ?>
                  <!-- /input-group -->
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'nomo') ?>
                  <!-- /input-group -->
                </div>
                <div class="col-md-3">
                    <?php echo $form->field($model, 'snfg') ?>
                  <!-- /input-group -->
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <?php echo $form->field($model, 'snfg_komponen') ?>
                  <!-- /input-group -->
                </div>
                <div class="col-md-4">
                    <?php echo $form->field($model, 'keterangan_downtime') ?>
                  <!-- /input-group -->
                </div>
            </div>


            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'start') ?>
                  <!-- /input-group -->
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'stop') ?>
                  <!-- /input-group -->
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Reset', ['index'], ['class' => 'btn btn-default']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
      </div>
    </div>




</div>


