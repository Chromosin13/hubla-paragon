<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DashboardLeadtimePenimbangan */

$this->title = 'Create Dashboard Leadtime Penimbangan';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Leadtime Penimbangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dashboard-leadtime-penimbangan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
