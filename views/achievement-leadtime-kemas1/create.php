<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AchievementLeadtimeKemas1 */

$this->title = 'Create Achievement Leadtime Kemas1';
$this->params['breadcrumbs'][] = ['label' => 'Achievement Leadtime Kemas1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-leadtime-kemas1-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
