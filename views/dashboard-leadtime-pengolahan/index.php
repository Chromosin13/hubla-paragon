<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DashboardLeadtimePengolahanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>


<div class="box box-widget widget-user">
    <div class="widget-user-header bg-blue-gradient">
      <h3 class="widget-user-username"><b>Leadtime Pengolahan</b></h3>
      <h4 class="widget-user-desc">Dashboard</h4>
    </div>
    <div class="widget-user-image">
      <img class="img-circle" src="../web/images/proses_icons/mixer.png" alt="User Avatar" style="width:40%">
    </div>

    <div class="box-footer">
      <div class="row">
        <div class="box-body">

            <div class="box-body">
                <?php

                    $gridColumns = [
                    'nama_fg',
                    'nomo',
                    'posisi',
                    'start',
                    'stop',
                    'sum_leadtime_raw',
                    'sum_leadtime_bruto',
                    'jenis_proses',
                    'sediaan',
                    'sum_downtime',
                    'sum_adjust',
                    'sum_leadtime_net',
                    'keterangan_downtime:ntext',
                    'keterangan_adjust:ntext',
                    'snfg:ntext',
                    'snfg_komponen:ntext',
                    'id',
                    'week',
                    'upload_date',
                    'nama_line',
                    'besar_batch_real',
                    'status',
                    'time_release',
                    ];

                    // Renders a export dropdown menu
                    echo ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $gridColumns
                    ]);
                ?>

                <?php echo $this->render('_search', ['model' => $searchModel,'sediaan_list'=>$sediaan_list,'nama_fg_list'=>$nama_fg_list]); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'responsiveWrap' => false,
                    'floatHeader'=>'true',
                    'floatHeaderOptions' => ['position' => 'absolute',],
                    'showPageSummary'=>true,
                    'pjax'=>true,
                    'striped'=>true,
                    'hover'=>true,
                    'panel'=>['type'=>'primary', 'heading'=>'Grid'],
                    'toolbar' => [
                    ],
                    'columns' => [
                        ['class' => 'kartik\grid\SerialColumn'],
                        'week',
                        'upload_date',
                        'nama_fg',
                        'nomo',
                        'posisi',
                        'start',
                        'stop',
                        'sum_leadtime_raw',
                        'sum_leadtime_bruto',
                        'jenis_proses',
                        'sediaan',
                        'sum_downtime',
                        'sum_adjust',
                        'sum_leadtime_net',
                        'keterangan_downtime:ntext',
                        'keterangan_adjust:ntext',
                        'snfg:ntext',
                        'snfg_komponen:ntext',
                        'nama_line',
                        'besar_batch_real',
                        'status',
                        'time_release',
                        'id',
                    ],
                ]); ?>
            </div>

        </div>
      </div>
      <!-- /.row -->
    </div>
</div>

<!-- Accordion -->
    <div class="panel box box-primary">
      <div class="box-header with-border">
        <h4 class="box-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="collapsed" aria-expanded="false">
            Test Grafik
          </a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
        <div class="box-body">
            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>
      </div>
    </div>

<script>

$.getJSON(
    'https://factory.pti-cosmetics.com/flowreport/web/index.php?r=dashboard-leadtime-pengolahan/json-leadtime-pengolahan',
    function (data) {

        Highcharts.chart('container', {
            chart: {
                zoomType: 'x'
            },
            title: {
                text: 'Average Time in Minutes Leadtime Nett rate over time'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                        'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: 'Leadtime Nett (In Minutes)'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [{
                type: 'area',
                name: 'Minutes',
                data: data
            }]
        });
    }
);  
</script>