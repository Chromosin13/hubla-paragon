<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DashboardLeadtimePengolahan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Leadtime Pengolahans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dashboard-leadtime-pengolahan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nama_fg',
            'nomo',
            'posisi',
            'start',
            'stop',
            'sum_leadtime_raw',
            'sum_leadtime_bruto',
            'jenis_proses',
            'sediaan',
            'sum_downtime',
            'sum_adjust',
            'sum_leadtime_net',
            'keterangan_downtime:ntext',
            'keterangan_adjust:ntext',
            'snfg:ntext',
            'snfg_komponen:ntext',
            'id',
        ],
    ]) ?>

</div>
