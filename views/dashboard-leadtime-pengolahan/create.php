<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DashboardLeadtimePengolahan */

$this->title = 'Create Dashboard Leadtime Pengolahan';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Leadtime Pengolahans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dashboard-leadtime-pengolahan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
