// Fitur Turun Bulk / Pengemberan

$('#stop-turun-bulk-form').hide();  
$('#pengolahan-turun_bulk_start').change(function(){
        
    // Get Current Timestamp
    var now = new Date();

    // Current Date 
    var start_js = new Date($('#pengolahan-turun_bulk_start').val());
    if(start_js<now){
        // Show Stop Form
        $('#stop-turun-bulk-form').fadeIn("slow");
    }else{
        // Hide Stop Form
        $('#stop-turun-bulk-form').hide();
        alert('Pemilihan waktu harus sebelum waktu saat ini!');
    }

});

$('#pengolahan-turun_bulk_stop').change(function(){
    var start_js = new Date($('#pengolahan-turun_bulk_start').val());
    var stop_js = new Date($('#pengolahan-turun_bulk_stop').val());
    if(stop_js<=start_js){
        alert('Periksa kembali waktu, Waktu Stop Bulk tidak boleh sebelum Waktu Start Bulk!');
        $('#create-button').hide();
    }else{
        $('#durasi-ember').html('Durasi Waktu Pengemberan : '+(parseFloat(stop_js-start_js)/parseFloat(60000))+' Menit atau '+(parseFloat(stop_js-start_js)/parseFloat(3600000)).toFixed(2)+' Jam' );
        $('#create-button').fadeIn("slow");
    };

});

// Loading Screen before all of the initial form is hidden
$(window).load(function() {
    $('#submitButton').hide(); // Hide Submit Button untuk Konfirmasi Input
    $('#start-button').hide(); // Hide Start Button
    $('#stop-button').hide(); // Hide Stop Button
    $('#form_start_stop').hide(); // Hide The Start Stop Button
    $('#form_jenis').hide(); // Hide Jenis Form
    $('#form_entry_start_stop').hide(); // Hide Form Entry Start Stop
    $('#turun-bulk-form').hide(); // Hide Turun Bulk Form
    $('#proses-input').hide(); // Hide Jenis Form
    $('#loading').hide(); // Hide Jenis Form

    document.getElementById("pengolahan-turun_bulk_start").disabled = true; // Disable Pengolahan Turun Bulk Start 
});

// Get Variables Values

function nomo() {
    var nomo = $('#pengolahan-nomo').val();
    return nomo;
};

function jenis_olah() {
    var jenis_olah = $('#pengolahan-jenis_olah').val();
    return jenis_olah;
};

function jenis_olah_1() {
    var jenis_olah_1 = $('#pengolahan-jenis_olah_1').val();
    return jenis_olah_1;
};

function state() {
    var state = $('#pengolahan-state').val();
    return state;
};

function nama_line() {
    var nama_line = $('#pengolahan-nama_line').val();
    return nama_line;
};

function lanjutan() {
    var lanjutan = $('#pengolahan-lanjutan').val();
    return lanjutan;
};

function lanjutan_stop() {
    var lanjutan_stop = $('#pengolahan-lanjutan_stop').val();
    return lanjutan_stop;
};

function nama_operator() {
    var nama_operator = $('#pengolahan-nama_operator').val();
    return nama_operator;
};


function plan_start_olah() {
    var plan_start_olah = $('#pengolahan-plan_start_olah').val();
    return plan_start_olah;
};

function plan_end_olah() {
    var plan_end_olah = $('#pengolahan-plan_end_olah').val();
    return plan_end_olah;
};

function plan_start_shift() {
    var plan_start_shift = $('#pengolahan-plan_start_shift').val();
    return plan_start_shift;
};

function plan_end_shift() {
    var plan_end_shift = $('#pengolahan-plan_end_shift').val();
    return plan_end_shift;
};

function is_done() {
    var is_done = $('#pengolahan-is_done').val();
    return is_done;
};

// Nomor MO Validator
    // Klik Scan MO
    $('#afterScnBtnNomo').click(function(){
        if(nomo()==""){
            $('#form_start_stop').hide();    
        }
        else {
            $('#form_start_stop').fadeIn("slow");
            $('#proses-input').fadeIn("slow"); 
        }
    });

// Start Stop Button Clicked Validator
    $('#start-button').click(function(){
        $('#form_jenis').fadeIn("slow");
    });

    $('#stop-button').click(function(){
        $('#form_jenis').fadeIn("slow");
        $('#form_entry_start_stop').fadeIn("slow");
    });

// After Select Jenis Olah
    $('#pengolahan-jenis_olah').change(function(){
        if(jenis_olah()==""){
            $('#form_entry_start_stop').hide(); 

        } else {
            $('#form_entry_start_stop').show()
            if(state()=="START"){
                $('#create-button').hide();
                $('#pengolahan-nama_line').change(function(){
                    if(nama_line()=="Select Line"){
                        $('#create-button').hide();
                    }else{
                        $('#create-button').fadeIn("slow");
                    }
                });
            }
      
        }
        
    });

// After Click Terdapat Penurunan Bulk


$('#btn-turun-bulk').click(function(){
    
    if($('#turun-bulk-form').is(':hidden')){
        $('#create-button').hide();
        $('#turun-bulk-form').fadeIn("slow");
        document.getElementById("pengolahan-turun_bulk_start").disabled = false;
    } 
    else{
        $('#create-button').fadeIn("slow");
        $('#turun-bulk-form').hide();
    }
});

// Validate Button

$("#btn-custom").on("click", function() {

    if(lanjutan==null||lanjutan_stop==null){
        alert('Penarikan data Lanjutan Gagal, Mohon refresh halaman dan scan ulang jadwal');
    }else if(state()=="START"&&lanjutan()==1){
        krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(nomo())+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state())+'</i></font><p>' +
                                '<b>Jenis Olah</b><p>' + '<font color="blue"><i>'+(jenis_olah())+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan())+'</i></font><p>' +
                                '<b>Nama Line</b><p>' + '<font color="blue"><i>'+(nama_line())+'</i></font><p>' +  
                                '<b>Nama Operator</b><p>' + '<font color="blue"><i>'+(nama_operator())+'</i></font><p>' +
                                '<b>Plan Start Olah</b><p>' + '<font color="blue"><i>'+(plan_start_olah())+'</i></font><p>' +
                                '<b>Plan End Olah</b><p>' + '<font color="blue"><i>'+(plan_end_olah())+'</i></font><p>' +
                                '<b>Plan Start Shift</b><p>' + '<font color="blue"><i>'+(plan_start_shift())+'</i></font><p>' +
                                '<b>Plan End Shift</b><p>' + '<font color="blue"><i>'+(plan_end_shift())+'</i></font><p>',function(result) {}
        );
    }else if(state()=="START"&&lanjutan()!=1){
        krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(nomo())+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state())+'</i></font><p>' +
                                '<b>Jenis Olah</b><p>' + '<font color="blue"><i>'+(jenis_olah())+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan())+'</i></font><p>' +
                                '<b>Nama Line</b><p>' + '<font color="blue"><i>'+(nama_line())+'</i></font><p>' +  
                                '<b>Nama Operator</b><p>' + '<font color="blue"><i>'+(nama_operator())+'</i></font><p>',function(result) {}
        );
    }else if(state()=="STOP"&&is_done()==1){
    
        krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(nomo())+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state())+'</i></font><p>' +
                                '<b>Jenis Olah</b><p>' + '<font color="blue"><i>'+(jenis_olah_1())+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan())+'</i></font><p>'+
                                '<b><font color="blue"><i>Jadwal Sudah Selesai (Tidak Lanjut Shift)</i></font></b><p>',function(result) {}
        );

    }else if(state()=="STOP"&&is_done()==0){

        krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(nomo())+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state())+'</i></font><p>' +
                                '<b>Jenis Olah</b><p>' + '<font color="blue"><i>'+(jenis_olah_1())+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan())+'</i></font><p>'+
                                '<b><font color="blue"><i>Jadwal Akan Dilanjutkan ke Shift Berikutnya (Belum Done)</i></font></b><p>',function(result) {}
        );

    }

});

 
$('#is_kendala').click(function(){

    // if(document.getElementById('is_kendala').checked){
    if($('#kendala-form').is(':hidden')){
            $('#kendala-form').fadeIn("slow");
            var i = 0;
            var maxnested = 3;                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);

                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = false;
                    document.getElementById("kendala-"+(i)+"-start").disabled = false;
        
                    i++;
                }

    }else{
            $('#kendala-form').hide();

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);
                    
                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = true;
                    document.getElementById("kendala-"+(i)+"-start").disabled = true;  

                    i++;
                }

    } 
 });


$('#view-scm').click(function(){

    $.post("index.php?r=scm-planner/get-komponen-mo&nomo="+nomo(), function (data){
            $("textarea#snfg_komponen_rincian").html(data);
    });

});



    $('#pengolahan-nomo').change(function(){

        $('#pengolahan-stop').hide();
        $('#pengolahan-start').hide(); 

        $('#loading').show(); // Hide Jenis Form

        // Populate Nama Line berdasarkan Nomor MO dan Sediaan 

        $.post("index.php?r=scm-planner/get-line-olah-nomo&nomo="+nomo(), function (data){
            $("select#pengolahan-nama_line").html(data);
        });

        // Populate Keterangan berdasarkan Nomor MO dan Sediaan 

        $.post("index.php?r=scm-planner/get-kendala-olah-nomo&nomo="+nomo(), function (data){
            $("select#kendala-0-keterangan").html(data);
                // $("select#kendala-1-keterangan").html(data);
                // $("select#kendala-2-keterangan").html(data);
        });

        $.get('index.php?r=scm-planner/get-planner-mo',{ nomo : nomo() },function(data){
            var data = $.parseJSON(data);
            //alert(data.sediaan);
            // $('#pengolahan-snfg').attr('value',data.snfg);
            $('#pengolahan-snfg').html(data.snfg);


            $('#streamline_results').html('STREAMLINE : '+data.streamline);
            $('#start_results').html('START : '+ data.start);
            $('#due_results').html('DUE : '+ data.due);
            $('#nama_bulk_results').html(data.nama_bulk);
            $('#nama_fg_results').html(data.nama_fg);
            $('#besar_batch_results').html('BESAR BATCH : '+ data.besar_batch);
            $('#besar_lot_results').html('BESAR LOT : '+ data.besar_lot);
            $('#lot_ke_results').html('LOT KE : '+ data.lot); ;
            $('#kode_jadwal_results').html('KODE JADWAL : '+ data.kode_jadwal); 
            $('#jumlah_results').html('JUMLAH : '+ data.jumlah);

        });
        $.get('index.php?r=scm-planner/get-pp',{ nomo : nomo() },function(data){
            var data = $.parseJSON(data);

            $('#ppr-posisi').html('Posisi : '+data.posisi);
            $('#ppr-state').html('State : '+data.state);
            $('#ppr-jenis-proses').html('Jenis Proses : '+data.jenis_proses);
            $('#ppr-lanjutan').html('Lanjutan : '+data.lanjutan);
            $('#ppr-isdone').html('Is Done : '+data.is_done);

            
            
        });
        $.get('index.php?r=scm-planner/get-planner-mo',{ nomo : nomo() },function(data){
            var data = $.parseJSON(data);

            if(data.status==null){
                $('#start-button').hide();
                $('#stop-button').hide(); 
                $('#create-button').hide();
                alert('Not Found');
            }
            
            if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
                $('#start-button').hide();
                $('#stop-button').hide(); 
                $('#create-button').hide();
                alert('Status ditetapkan sebagai HOLD/PAUSE dari Planner')
            }
            else{
                $.get('index.php?r=scm-planner/get-last-mo',{ nomo : nomo() },function(data){
                    var data = $.parseJSON(data);
                    $('#loading').hide(); // Hide Jenis Form
                    if(data.posisi=="PENGOLAHAN" && data.state=="START"){

                        $('#start-button').hide();
                        $('#stop-button').fadeIn("slow"); 

                            // Jenis Proses di Assign

                            $('#pengolahan-jenis_olah_00').hide();
                            document.getElementById("pengolahan-jenis_olah").disabled = true;
                            $('#pengolahan-jenis_olah_0').fadeIn("slow");
                            document.getElementById("pengolahan-jenis_olah_1").disabled = false;
                            $('#pengolahan-jenis_olah_1').attr('value',data.jenis_proses);


                            // Assign Lanjutan dan Lanjutan Istirahat

                            $('#stop-button').click(function(){                                
                                
                                $.get('index.php?r=pengolahan/lanjutan-pengolahanmo',{ nomo : nomo(), jenis_olah : jenis_olah_1() },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                                    $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);                                                                       
                                
                                });

                            });


                            // EO Lanjutan dan Lanjutan Istirahat 

                    }else if(data.posisi=="PENGOLAHAN" && data.state=="STOP"){
                        
                        $('#start-button').fadeIn("slow");
                        $('#stop-button').hide(); 
                        $('#start-button').click(function(){ 

                            $('#pengolahan-jenis_olah').change(function(){                              
                                
                                $.get('index.php?r=pengolahan/lanjutan-pengolahanmo',{ nomo : nomo(), jenis_olah : jenis_olah() },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                                    $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                                                   
                                
                                });
                            });
                        });

                    }else if(data.posisi!="PENGOLAHAN" && data!=null){
                        $('#start-button').fadeIn("slow");
                        $('#stop-button').hide();
                        alert('Jadwal Baru');
                    
                    }else {
                        $('#start-button').fadeIn("slow");
                        $('#stop-button').fadeIn("slow"); 
                        alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                    }
                });
            }
        });
    });

    



$(function() {


    // Default Hide and Disable DynamicFormWidget for Kendala Model //

        $('#kendala-form').hide();
        document.getElementById("kendala-0-keterangan").disabled = true;
        document.getElementById("kendala-0-start").disabled = true;


    // DynamicForm override JS , Script Fix for non-working kartik date/timepicker afterInsert 
    // DynamicForm override JS , Fix for select2 kendala list

        // ID untuk penomoran nested dynamic form
        var id = 0;


        // Setelah Insert Button

        $(".dynamicform_wrapper").on('afterInsert', function(e, item) {
            var dateTimePickers = $(this).find('[data-krajee-kvtimepicker]');
            dateTimePickers.each(function(index, el) {
                $(this).parent().removeData().kvTimepicker('remove');
                $(this).parent().kvTimepicker(eval($(this).attr('data-krajee-kvtimepicker')));
            });
            

            // Override Fix for select2 kendala list 

            id += 1;
            console.log(id);

            $.post("index.php?r=scm-planner/get-kendala-olah-nomo&nomo="+nomo(), function (data){

                    $("select#kendala-"+(id)+"-keterangan").html(data);

            });


        });

        // Setelah Delete Button

        $(".dynamicform_wrapper").on('afterDelete', function(e, item) {
            
            // Override Fix for select2 kendala list 

            id -= 1;
            console.log(id);

        });


        // Jika Mencapai Batas Maximum
        $(".dynamicform_wrapper").on('limitReached', function(e, item) {
            alert('Maximum 3');
        });


    $('#pengolahan-jenis_olah_0').hide(); //  Hide Jenis Proses Dropdown
    document.getElementById("pengolahan-jenis_olah_1").disabled = true; // Disable Jenis Proses auto-assign

    $('#pengolahan-start').hide();
    $('#pengolahan-stop').hide(); 
    $('#start-button').click(function(){
            var start = "START";
            $('#pengolahan-state').attr('value',start);
                    $('#pengolahan-start').fadeIn("slow");
                    $('#pengolahan-stop').hide(); 
            $('#pengolahan-jenis_olah').change(function(){
               
                                    
                $.get('index.php?r=pengolahan/lanjutan-pengolahanmo',{ nomo : nomo(), jenis_olah : jenis_olah() },function(data){
                    var data = $.parseJSON(data);
                    //alert(data.sediaan);
                    $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                    $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                    var notFirst = $('#pengolahan-lanjutan').val();
                        if(notFirst>1){
                            $('#planstart-box').hide();
                        }else{
                            $('#planstart-box').fadeIn("slow");
                        }
                }); 
                                

            });                     
    });


    $('#stop-button').click(function(){
            var stop = "STOP";
            $('#pengolahan-state').attr('value',stop);
                    $('#pengolahan-stop').fadeIn("slow"); 
                    $('#pengolahan-start').hide();    
            $('#pengolahan-jenis_olah').change(function(){
                
                $.get('index.php?r=pengolahan/lanjutan-pengolahanmo',{ nomo : nomo(), jenis_olah : jenis_olah() },function(data){
                    var data = $.parseJSON(data);
                    //alert(data.sediaan);
                    $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                    $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                }); 
                            
            });
            $('#rincian-hasil-olah').hide();

            $('#pengolahan-is_done').change(function(){
                if(is_done()==1){
                    $('#rincian-hasil-olah').fadeIn("slow");
                }
                else{
                    $('#rincian-hasil-olah').hide();
                }
            });
        });             

});