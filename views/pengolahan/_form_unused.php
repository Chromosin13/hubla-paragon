<?php

use faryshta\widgets\JqueryTagsInput;
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ScmPlanner;
use yii\widgets\Pjax; 
// Menggunakan Pjax untuk Autorefresh
use yii\helpers\Url;
use unclead\widgets\MultipleInput;
use unclead\widgets\MultipleInputColumn;
//use unclead\widgets\TabularInput;
use yii\base\Model;
use mdm\widgets\TabularInput;
use mdm\widgets\GridInput;
use app\models\Pengolahan;
use app\models\PengolahanSearch;

/* @var $this yii\web\View */
/* @var $model app\models\Pengolahan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengolahan-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Input dan Informasi SNFG dari Planner</h2>
                <?= $form->field($model, 'snfg_komponen')->textInput() ?>

                <?= $form->field($model, 'snfg')->textInput(['readonly' => 'true']) ?>
                <b>Streamline</b>
                <input type="text" class="form-control" id="streamline" disabled>
                <b>Start</b>
                <input type="text" class="form-control" id="start" placeholder="" disabled>
                <b>Due</b>
                <input type="text" class="form-control" id="due" placeholder="" disabled>
                <b>Nama Bulk</b>
                <input type="text" class="form-control" id="nama-bulk" placeholder="" disabled>
                <b>Nama FG</b>
                <input type="text" class="form-control" id="nama-fg" placeholder="" disabled>
                <b>Besar Batch</b>
                <input type="text" class="form-control" id="besar-batch" placeholder="" disabled>
                <b>Besar per lot (Kg)</b>
                <input type="text" class="form-control" id="besar-lot" placeholder="" disabled>
                <b>Lot ke-</b>
                <input type="text" class="form-control" id="lot" placeholder="" disabled>
                <b>Kode Jadwal</b>
                <input type="text" class="form-control" id="kode-jadwal" placeholder="" disabled>
            </div>
    </div>

    

    <!-- Button untuk change event START, STOP, ISTIRAHAT START, ISTIRAHAT STOP -->

    <a class="btn btn-app" id="start-button">
                <i class="fa fa-play"></i> Start
    </a>

    <a class="btn btn-app" id="stop-button">
                <i class="fa fa-pause"></i> Stop
    </a>

    <a class="btn btn-app" id="istirahat-start-button">
                <i class="fa fa-play"></i> Istirahat Start
    </a>

    <a class="btn btn-app" id="istirahat-stop-button">
                <i class="fa fa-pause"></i> Istirahat Stop
    </a>

    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'jenis_olah')->dropDownList(['REWORK' => 'REWORK','OLAH PREMIX' => 'OLAH PREMIX','OLAH 1' => 'OLAH 1','OLAH 2' => 'OLAH 2','ADJUST OLAH 1' => 'ADJUST OLAH 1','ADJUST OLAH 2' => 'ADJUST OLAH 2'],['prompt'=>'Select Option']); ?>

    <div class="box" id="pengolahan-start">
            <div class="box-header with-border">
              <h2 class="box-title">Start Entry</h2>

                <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>

                <?= $form->field($model, 'nama_line')->textInput() ?>

                <?= $form->field($model, 'jumlah_operator')->textInput() ?>

                <?php echo $form->field($model, 'nama_operator')->widget(JqueryTagsInput::className([]));
                ?>
                


            </div>
    </div>

    <div class="box" id="pengolahan-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Stop Entry</h2>

                <?= $form->field($model, 'lanjutan')->textInput(['id'=>'pengolahan-lanjutan_stop','readonly' => 'true']) ?> 
               
            </div>
    </div>

    <div class="box" id="pengolahan-istirahat-start">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Start Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'pengolahan-lanjutan_istirahat_start','readonly' => 'true']) ?> 
            </div>
    </div>

    <div class="box" id="pengolahan-istirahat-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Stop Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'pengolahan-lanjutan_istirahat_stop','readonly' => 'true']) ?>
            </div>
    </div>    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>


<?php
$script = <<< JS



$('#pengolahan-snfg_komponen').change(function(){
    var snfg_komponen = $(this).val();
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#pengolahan-snfg').attr('value',data.snfg);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
        $('#besar-batch').attr('value',data.besar_batch);
        $('#besar-lot').attr('value',data.besar_lot);
        $('#lot').attr('value',data.lot);
        $('#kode-jadwal').attr('value',data.kode_jadwal);
    });
});


$(function() {            
    $('#pengolahan-start').hide();
    $('#pengolahan-stop').hide(); 
    $('#pengolahan-istirahat-start').hide(); 
    $('#pengolahan-istirahat-stop').hide(); 
    $('#start-button').click(function(){
            var start = "START";
            $('#pengolahan-state').attr('value',start);
                    $('#pengolahan-start').show();
                    $('#pengolahan-stop').hide(); 
                    $('#pengolahan-istirahat-start').hide(); 
                    $('#pengolahan-istirahat-stop').hide();
            $('#pengolahan-jenis_olah').change(function(){
                var jenis_olah = $('#pengolahan-jenis_olah').val();
                var snfg_komponen = $('#pengolahan-snfg_komponen').val();    
                $.get('index.php?r=pengolahan/lanjutan-pengolahan',{ snfg_komponen : snfg_komponen, jenis_olah : jenis_olah },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#pengolahan-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                            $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                            $('#pengolahan-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                     
        });

    $('#istirahat-start-button').click(function(){
            var ist_start = "ISTIRAHAT START";
            $('#pengolahan-state').attr('value',ist_start);
                    $('#pengolahan-istirahat-start').show(); 
                    $('#pengolahan-stop').hide(); 
                    $('#pengolahan-start').hide(); 
                    $('#pengolahan-istirahat-stop').hide(); 
            $('#pengolahan-jenis_olah').change(function(){
                var jenis_olah = $('#pengolahan-jenis_olah').val();
                var snfg_komponen = $('#pengolahan-snfg_komponen').val();    
                $.get('index.php?r=pengolahan/lanjutan-pengolahan',{ snfg_komponen : snfg_komponen, jenis_olah : jenis_olah },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#pengolahan-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                            $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                            $('#pengolahan-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                     
        });

    $('#istirahat-stop-button').click(function(){
            var ist_stop = "ISTIRAHAT STOP";
            $('#pengolahan-state').attr('value',ist_stop);
                    $('#pengolahan-istirahat-stop').show();
                    $('#pengolahan-stop').hide(); 
                    $('#pengolahan-istirahat-start').hide(); 
                    $('#pengolahan-start').hide(); 
            $('#pengolahan-jenis_olah').change(function(){
                var jenis_olah = $('#pengolahan-jenis_olah').val();
                var snfg_komponen = $('#pengolahan-snfg_komponen').val();    
                $.get('index.php?r=pengolahan/lanjutan-pengolahan',{ snfg_komponen : snfg_komponen, jenis_olah : jenis_olah },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#pengolahan-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                            $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                            $('#pengolahan-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });                         
        });

    $('#stop-button').click(function(){
             var stop = "STOP";
             $('#pengolahan-state').attr('value',stop);
                    $('#pengolahan-stop').show(); 
                    $('#pengolahan-start').hide(); 
                    $('#pengolahan-istirahat-start').hide(); 
                    $('#pengolahan-istirahat-stop').hide();    
            });
            $('#pengolahan-jenis_olah').change(function(){
                var jenis_olah = $('#pengolahan-jenis_olah').val();
                var snfg_komponen = $('#pengolahan-snfg_komponen').val();    
                $.get('index.php?r=pengolahan/lanjutan-pengolahan',{ snfg_komponen : snfg_komponen, jenis_olah : jenis_olah },function(data){
                            var data = $.parseJSON(data);
                            //alert(data.sediaan);
                            $('#pengolahan-lanjutan_istirahat_start').attr('value',data.lanjutan);
                            $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                            $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                            $('#pengolahan-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                        });
            });               
    });

JS;
$this->registerJs($script);
?>