<?php

use faryshta\widgets\JqueryTagsInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ScmPlanner;
use yii\widgets\Pjax; 
// Menggunakan Pjax untuk Autorefresh
use yii\helpers\Url;
//use unclead\widgets\MultipleInput;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Pengolahan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengolahan-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>


    <?= $form->field($model, 'state')->hiddenInput()->label(false);?>

    <?= $form->field($model, 'jenis_olah')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'lanjutan')->hiddenInput()->label(false); ?> 

     <div class="row">
            <div class="panel panel-default">
            <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Rincian Hasil Pengolahan</h4></div>
            <div class="panel-body">
                  

                 <?php DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-items', // required: css class selector
                    'widgetItem' => '.item', // required: css class
                    'limit' => 20, // the maximum times, an element can be cloned (default 999)
                    'min' => 1, // 0 or 1 (default 1)
                    'insertButton' => '.add-item', // css class
                    'deleteButton' => '.remove-item', // css class
                    'model' => $modelsPengolahanItem[0],
                    'formId' => 'dynamic-form',
                    'formFields' => [
                        'nobatch',
                        'besar_batch_real',
                    ],
                ]); ?>

                <div class="container-items"><!-- widgetContainer -->
                <?php foreach ($modelsPengolahanItem as $i => $modelPengolahanItem): ?>
                    <div class="item panel panel-default"><!-- widgetBody -->
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">Pengolahan Item</h3>
                            <div class="pull-right">
                                <!-- <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button> -->
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                                // necessary for update action.
                                if (! $modelPengolahanItem->isNewRecord) {
                                    echo Html::activeHiddenInput($modelPengolahanItem, "[{$i}]id");
                                }
                            ?>

                            <div class="row">
                                <div class="col-sm-3">
                                    <?= $form->field($modelPengolahanItem, "[{$i}]snfg_komponen")->textInput(['maxlength' => true, 'readonly' => 'true']) ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $form->field($modelPengolahanItem, "[{$i}]nobatch")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $form->field($modelPengolahanItem, "[{$i}]besar_batch_real")->textInput(['maxlength' => true]) ?>
                                </div>
                            </div><!-- .row -->
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
                <?php DynamicFormWidget::end(); ?>
            </div>
            </div>
        </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$script = <<< JS
$(function() {
    var state = document.getElementById("pengolahan-state").value;
     if(state=="START"){
         $('#pengolahan-stop').hide();
     }else{
         $('#pengolahan-start').hide();
     }
});


$('#view-scm').click(function(){
    var snfg_komponen = $('#pengolahan-snfg_komponen').val();
    var nomo = $('#pengolahan-nomo').val();
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#pengolahan-snfg').attr('value',data.snfg);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
        $('#besar-batch').attr('value',data.besar_batch);
        $('#besar-lot').attr('value',data.besar_lot);
        $('#lot').attr('value',data.lot);
        $('#kode-jadwal').attr('value',data.kode_jadwal);
    });
    $.get('index.php?r=scm-planner/get-planner-mo',{ nomo : nomo },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#pengolahan-snfg_komponen').attr('value',data.snfg_komponen);
        $('#pengolahan-snfg').attr('value',data.snfg);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
        $('#besar-batch').attr('value',data.besar_batch);
        $('#besar-lot').attr('value',data.besar_lot);
        $('#lot').attr('value',data.lot);
        $('#kode-jadwal').attr('value',data.kode_jadwal);
    });
    $.get('index.php?r=scm-planner/get-planner-mo-mo',{ nomo : nomo },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#snfg_komponen_rincian').attr('value',data.snfg_komponen);
    });
});

JS;
$this->registerJs($script);
?>