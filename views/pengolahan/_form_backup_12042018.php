<?php

use faryshta\widgets\JqueryTagsInput;
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
use yii\helpers\ArrayHelper;
use app\models\ScmPlanner;
use yii\widgets\Pjax; 
// Menggunakan Pjax untuk Autorefresh
use yii\helpers\Url;
//use unclead\widgets\MultipleInput;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\date\DatePicker;
use kartik\widgets\DateTimePicker;
use kartik\widgets\TimePicker;
use kartik\checkbox\CheckboxX;
use kartik\widgets\Select2;
use dosamigos\tinymce\TinyMce;
use app\models\Pengolahan;
use app\models\Kendala;
use kartik\dialog\Dialog;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Pengolahan */
/* @var $form yii\widgets\ActiveForm */
?>


<?php
$script = <<< JS

// Hide Submit Button untuk Konfirmasi Input
$('#submitButton').hide();

// Inisialisasi Form Hide
    $('#start-button').hide();
    $('#stop-button').hide(); 
    $('#form_start_stop').hide(); 
    $('#form_jenis').hide(); 
    $('#form_entry_start_stop').hide();
    $('#turun-bulk-form').hide();
    document.getElementById("pengolahan-turun_bulk_start").disabled = true;
    // document.getElementById("pengolahan-turun_bulk_stop").disabled = true;
    

// After Klik Tombol Tekan Scan Nomo
    // Klik Scan MO
    $('#afterScnBtnNomo').click(function(){
        var nomo = $('#pengolahan-nomo').val();
        if(nomo==""){
            $('#form_start_stop').hide();    
        }
        else {
            $('#form_start_stop').show(); 
        }
    });

    // Klik Scan Komponen
    $('#afterScnBtnKomponen').click(function(){
        $('#form_start_stop').show();  
    });

// After Klik Start Stop Button
    $('#start-button').click(function(){
        $('#form_jenis').show();
    });

    $('#stop-button').click(function(){
        $('#form_jenis').show();
        $('#form_entry_start_stop').show();
    });

// After Select Jenis Olah
    $('#pengolahan-jenis_olah').change(function(){
        var jenis_olah = $('#pengolahan-jenis_olah').val();
        if(jenis_olah==""){
            $('#form_entry_start_stop').hide(); 

        } else {
            $('#form_entry_start_stop').show();
            var state = $('#pengolahan-state').val();
            if(state=="START"){
                $('#create-button').hide();
                $('#pengolahan-nama_line').change(function(){
                    var nama_line = $('#pengolahan-nama_line').val();
                    if(nama_line=="Select Line"){
                        $('#create-button').hide();
                    }else{
                        $('#create-button').show();
                    }
                });
            }
      
        }
        
    });

// After Click Terdapat Penurunan Bulk


$('#btn-turun-bulk').click(function(){
    
    if($('#turun-bulk-form').is(':hidden')){
        alert('test');
        $('#turun-bulk-form').show();
        document.getElementById("pengolahan-turun_bulk_start").disabled = false;
        // document.getElementById("pengolahan-turun_bulk_stop").disabled = false;
    } 
    else{
        alert('test_else');
        $('#turun-bulk-form').hide();
        document.getElementById("pengolahan-turun_bulk_start").disabled=true;
        // document.getElementById("pengolahan-turun_bulk_stop").disabled=true;
    }
});

// Validate Button

$("#btn-custom").on("click", function() {
    var nomo = $('#pengolahan-nomo').val();
    var state = $('#pengolahan-state').val();
    var jenis_olah = $('#pengolahan-jenis_olah').val();
    var jenis_olah_1 = $('#pengolahan-jenis_olah_1').val();
    var lanjutan = $('#pengolahan-lanjutan').val();
    var lanjutan_stop = $('#pengolahan-lanjutan_stop').val();
    var nama_line = $('#pengolahan-nama_line').val();
    var nama_operator =  $('#pengolahan-nama_operator').val();
    var plan_start_olah =  $('#pengolahan-plan_start_olah').val();
    var plan_end_olah =  $('#pengolahan-plan_end_olah').val();
    var plan_start_shift =  $('#pengolahan-shift_plan_start_olah').val();
    var plan_end_shift =  $('#pengolahan-shift_plan_end_olah').val();
    var is_done = $('#pengolahan-is_done').val();

    if(lanjutan==null||lanjutan_stop==null){
        alert('Penarikan data Lanjutan Gagal, Mohon refresh halaman dan scan ulang jadwal');
    }else if(state=="START"&&lanjutan==1){
        krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(nomo)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Olah</b><p>' + '<font color="blue"><i>'+(jenis_olah)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>' +
                                '<b>Nama Line</b><p>' + '<font color="blue"><i>'+(nama_line)+'</i></font><p>' +  
                                '<b>Nama Operator</b><p>' + '<font color="blue"><i>'+(nama_operator)+'</i></font><p>' +
                                '<b>Plan Start Olah</b><p>' + '<font color="blue"><i>'+(plan_start_olah)+'</i></font><p>' +
                                '<b>Plan End Olah</b><p>' + '<font color="blue"><i>'+(plan_end_olah)+'</i></font><p>' +
                                '<b>Plan Start Shift</b><p>' + '<font color="blue"><i>'+(plan_start_shift)+'</i></font><p>' +
                                '<b>Plan End Shift</b><p>' + '<font color="blue"><i>'+(plan_end_shift)+'</i></font><p>',function(result) {}
        );
    }else if(state=="START"&&lanjutan!=1){
        krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(nomo)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Olah</b><p>' + '<font color="blue"><i>'+(jenis_olah)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>' +
                                '<b>Nama Line</b><p>' + '<font color="blue"><i>'+(nama_line)+'</i></font><p>' +  
                                '<b>Nama Operator</b><p>' + '<font color="blue"><i>'+(nama_operator)+'</i></font><p>',function(result) {}
        );
    }else if(state=="STOP"&&is_done==1){
    
        krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(nomo)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Olah</b><p>' + '<font color="blue"><i>'+(jenis_olah_1)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>'+
                                '<b><font color="blue"><i>Jadwal Sudah Selesai (Tidak Lanjut Shift)</i></font></b><p>',function(result) {}
        );

    }else if(state=="STOP"&&is_done==0){

        krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(nomo)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Olah</b><p>' + '<font color="blue"><i>'+(jenis_olah_1)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>'+
                                '<b><font color="blue"><i>Jadwal Akan Dilanjutkan ke Shift Berikutnya (Belum Done)</i></font></b><p>',function(result) {}
        );

    }

});


// 1 // SNFG Komponen / NOMO Checkbox Function

$('#form_snfg_komponen').hide(); 
$('#form_nomo').hide(); 

$('#btn_nomo').click(function(){
    document.getElementById("pengolahan-snfg_komponen").disabled = true
    document.getElementById("pengolahan-nomo").disabled = false;
    $('#form_snfg_komponen').hide(); 
    $('#form_nomo').show(); 
});

$('#btn_snfg_komponen').click(function(){
    document.getElementById("pengolahan-snfg_komponen").disabled = false
    document.getElementById("pengolahan-nomo").disabled = true;
    $('#form_snfg_komponen').show();
    $('#form_nomo').hide();
});

// $('#per_snfg_komponen').change(function(){
//     var per_snfg_komponen = document.getElementById("pengolahan-snfg_komponen").disabled;
//     if(per_snfg_komponen){
//         document.getElementById("pengolahan-snfg_komponen").disabled = false
//         document.getElementById("pengolahan-nomo").disabled = true;
//     }else{
//         document.getElementById("pengolahan-snfg_komponen").disabled = true
//         document.getElementById("pengolahan-nomo").disabled = false;
//     } 
// });


$('#is_kendala').change(function(){

    if(document.getElementById('is_kendala').checked){
            $('#kendala-form').show();

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);

                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = false;
                    document.getElementById("kendala-"+(i)+"-start").disabled = false;
        
                    i++;
                }

    }else{
            $('#kendala-form').hide();

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);
                    
                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = true;
                    document.getElementById("kendala-"+(i)+"-start").disabled = true;  

                    i++;
                }

    } 
 });


$('#view-scm').click(function(){
    var snfg_komponen = $('#pengolahan-snfg_komponen').val();
    var nomo = $('#pengolahan-nomo').val();
    
    $.post("index.php?r=scm-planner/get-komponen-mo&nomo="+$('#pengolahan-nomo').val(), function (data){
            $("textarea#snfg_komponen_rincian").html(data);
    });

});



$('#pengolahan-snfg_komponen').change(function(){
    var snfg_komponen = $(this).val();

    $.post("index.php?r=scm-planner/get-line-olah-komponen&snfg_komponen="+$('#pengolahan-snfg_komponen').val(), function (data){
        $("select#pengolahan-nama_line").html(data);
    });

    $.post("index.php?r=scm-planner/get-kendala-olah-komponen&snfg_komponen="+$('#pengolahan-snfg_komponen').val(), function (data){
        $("select#kendala-0-keterangan").html(data);
    });


    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#pengolahan-nomo').attr('value',data.nomo);
        $('#pengolahan-snfg').attr('value',data.snfg);


        $('#streamline_results').html('STREAMLINE : '+data.streamline);
        $('#start_results').html('START : '+ data.start);
        $('#due_results').html('DUE : '+ data.due);
        $('#nama_bulk_results').html(data.nama_bulk);
        $('#nama_fg_results').html(data.nama_fg);
        $('#besar_batch_results').html('BESAR BATCH : '+ data.besar_batch);
        $('#besar_lot_results').html('BESAR LOT : '+ data.besar_lot);
        $('#lot_ke_results').html('LOT KE : '+ data.lot); ;
        $('#kode_jadwal_results').html('KODE JADWAL : '+ data.kode_jadwal); 
        $('#jumlah_results').html('JUMLAH : '+ data.jumlah);

    });
    $.get('index.php?r=scm-planner/get-ppr',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Status ditetapkan sebagai HOLD/PAUSE dari Planner')
        }
        else{
            $.get('index.php?r=scm-planner/get-last',{ snfg_komponen : snfg_komponen },function(data){
                var data = $.parseJSON(data);
                if(data.posisi=="PENGOLAHAN" && data.state=="START"){
                    $('#start-button').hide();
                    $('#stop-button').show(); 
                    $('#istirahat-start-button').show(); 
                    $('#istirahat-stop-button').hide(); 

                    $('#pengolahan-jenis_olah_00').hide();
                    document.getElementById("pengolahan-jenis_olah").disabled = true;
                    $('#pengolahan-jenis_olah_0').show();
                    document.getElementById("pengolahan-jenis_olah_1").disabled = false;
                    $('#pengolahan-jenis_olah_1').attr('value',data.jenis_proses);

                    // Assign Lanjutan 

                    $('#stop-button').click(function(){ 

                        var jenis_olah = $('#pengolahan-jenis_olah_1').val();
                        var snfg_komponen = $('#pengolahan-snfg_komponen').val();                                
                        
                            $.get('index.php?r=pengolahan/lanjutan-pengolahan',{ snfg_komponen : snfg_komponen, jenis_olah : jenis_olah },function(data){
                                var data = $.parseJSON(data);
                                //alert(data.sediaan);
                                $('#pengolahan-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                                $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                                $('#pengolahan-lanjutan_istirahat_stop').attr('value',data.lanjutan);

                                // Disable Post Value Lanjutan Istirahat

                                document.getElementById("pengolahan-lanjutan-ist_istirahat_start").disabled = true;
                                document.getElementById("pengolahan-lanjutan-ist_istirahat_stop").disabled = true;                                                                      
                            
                            });

                    });


                    // EO Lanjutan                     

                }else if(data.posisi=="PENGOLAHAN" && data.state=="STOP"){
                    $('#start-button').show();
                    $('#stop-button').hide(); 
                    $('#istirahat-start-button').hide(); 
                    $('#istirahat-stop-button').hide(); 

                    $('#start-button').click(function(){ 

                            $('#pengolahan-jenis_olah').change(function(){

                                var jenis_olah = $('#pengolahan-jenis_olah').val();
                                var snfg_komponen = $('#pengolahan-snfg_komponen').val();                                
                                
                                    $.get('index.php?r=pengolahan/lanjutan-pengolahan',{ snfg_komponen : snfg_komponen, jenis_olah : jenis_olah },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.sediaan);
                                        $('#pengolahan-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                        $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                                        $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                                        $('#pengolahan-lanjutan_istirahat_stop').attr('value',data.lanjutan);

                                        document.getElementById("pengolahan-lanjutan-ist_istirahat_start").disabled = true;
                                        document.getElementById("pengolahan-lanjutan-ist_istirahat_stop").disabled = true;                                                                       
                                    
                                        });
                                });
                            });

                }else if(data.posisi!="PENGOLAHAN" && data!=null){
                    $('#start-button').show();
                    $('#stop-button').hide();
                    alert('Jadwal Baru');
                
                }else {
                    $('#start-button').show();
                    $('#stop-button').show(); 
                    alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                }
            });
        }
    });
});

$('#pengolahan-nomo').change(function(){
    var nomo = $(this).val();

    // Populate Nama Line berdasarkan Nomor MO dan Sediaan 

    $.post("index.php?r=scm-planner/get-line-olah-nomo&nomo="+$('#pengolahan-nomo').val(), function (data){
        $("select#pengolahan-nama_line").html(data);
    });

    // Populate Keterangan berdasarkan Nomor MO dan Sediaan 

    $.post("index.php?r=scm-planner/get-kendala-olah-nomo&nomo="+$('#pengolahan-nomo').val(), function (data){
            $("select#kendala-0-keterangan").html(data);
            // $("select#kendala-1-keterangan").html(data);
            // $("select#kendala-2-keterangan").html(data);
    });

    $.get('index.php?r=scm-planner/get-planner-mo',{ nomo : nomo },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#pengolahan-snfg_komponen').attr('value',data.snfg_komponen);
        $('#pengolahan-snfg').attr('value',data.snfg);


        $('#streamline_results').html('STREAMLINE : '+data.streamline);
        $('#start_results').html('START : '+ data.start);
        $('#due_results').html('DUE : '+ data.due);
        $('#nama_bulk_results').html(data.nama_bulk);
        $('#nama_fg_results').html(data.nama_fg);
        $('#besar_batch_results').html('BESAR BATCH : '+ data.besar_batch);
        $('#besar_lot_results').html('BESAR LOT : '+ data.besar_lot);
        $('#lot_ke_results').html('LOT KE : '+ data.lot); ;
        $('#kode_jadwal_results').html('KODE JADWAL : '+ data.kode_jadwal); 
        $('#jumlah_results').html('JUMLAH : '+ data.jumlah);

    });
    $.get('index.php?r=scm-planner/get-pp',{ nomo : nomo },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });
    $.get('index.php?r=scm-planner/get-planner-mo',{ nomo : nomo },function(data){
        var data = $.parseJSON(data);

        if(data.status==null){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Not Found');
        }
        
        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Status ditetapkan sebagai HOLD/PAUSE dari Planner')
        }
        else{
            $.get('index.php?r=scm-planner/get-last-mo',{ nomo : nomo },function(data){
                var data = $.parseJSON(data);
                if(data.posisi=="PENGOLAHAN" && data.state=="START"){
                    $('#start-button').hide();
                    $('#stop-button').show(); 

                        // Jenis Proses di Assign

                        $('#pengolahan-jenis_olah_00').hide();
                        document.getElementById("pengolahan-jenis_olah").disabled = true;
                        $('#pengolahan-jenis_olah_0').show();
                        document.getElementById("pengolahan-jenis_olah_1").disabled = false;
                        $('#pengolahan-jenis_olah_1').attr('value',data.jenis_proses);


                        // Assign Lanjutan dan Lanjutan Istirahat

                        $('#stop-button').click(function(){ 

                            var jenis_olah = $('#pengolahan-jenis_olah_1').val();
                            var nomo = $('#pengolahan-nomo').val();                                
                            
                                $.get('index.php?r=pengolahan/lanjutan-pengolahanmo',{ nomo : nomo, jenis_olah : jenis_olah },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#pengolahan-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                                    $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#pengolahan-lanjutan_istirahat_stop').attr('value',data.lanjutan);

                                    document.getElementById("pengolahan-lanjutan-ist_istirahat_start").disabled = true;
                                    document.getElementById("pengolahan-lanjutan-ist_istirahat_stop").disabled = true;                                                                       
                                
                                });

                        });


                        // EO Lanjutan dan Lanjutan Istirahat 

                }else if(data.posisi=="PENGOLAHAN" && data.state=="STOP"){
                    $('#start-button').show();
                    $('#stop-button').hide(); 
                    $('#start-button').click(function(){ 

                        $('#pengolahan-jenis_olah').change(function(){

                            var jenis_olah = $('#pengolahan-jenis_olah').val();
                            var nomo = $('#pengolahan-nomo').val();                                
                            
                                $.get('index.php?r=pengolahan/lanjutan-pengolahanmo',{ nomo : nomo, jenis_olah : jenis_olah },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                                    $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                                                   
                                
                                });
                        });
                    });

                }else if(data.posisi!="PENGOLAHAN" && data!=null){
                    $('#start-button').show();
                    $('#stop-button').hide();
                    alert('Jadwal Baru');
                
                }else {
                    $('#start-button').show();
                    $('#stop-button').show(); 
                    alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                }
            });
        }
    });
});



$(function() {


    // Default Hide and Disable DynamicFormWidget for Kendala Model //

        $('#kendala-form').hide();
        document.getElementById("kendala-0-keterangan").disabled = true;
        document.getElementById("kendala-0-start").disabled = true;


    // DynamicForm override JS , Script Fix for non-working kartik date/timepicker afterInsert 
    // DynamicForm override JS , Fix for select2 kendala list

        // ID untuk penomoran nested dynamic form
        var id = 0;


        // Setelah Insert Button

        $(".dynamicform_wrapper").on('afterInsert', function(e, item) {
            var dateTimePickers = $(this).find('[data-krajee-kvtimepicker]');
            dateTimePickers.each(function(index, el) {
                $(this).parent().removeData().kvTimepicker('remove');
                $(this).parent().kvTimepicker(eval($(this).attr('data-krajee-kvtimepicker')));
            });
            

            // Override Fix for select2 kendala list 

            id += 1;
            console.log(id);


            $.post("index.php?r=scm-planner/get-kendala-olah-komponen&snfg_komponen="+$('#pengolahan-snfg_komponen').val(), function (data){

                    $("select#kendala-"+(id)+"-keterangan").html(data);

            });

            $.post("index.php?r=scm-planner/get-kendala-olah-nomo&nomo="+$('#pengolahan-nomo').val(), function (data){

                    $("select#kendala-"+(id)+"-keterangan").html(data);

            });


        });

        // Setelah Delete Button

        $(".dynamicform_wrapper").on('afterDelete', function(e, item) {
            
            // Override Fix for select2 kendala list 

            id -= 1;
            console.log(id);

        });


        // Jika Mencapai Batas Maximum
        $(".dynamicform_wrapper").on('limitReached', function(e, item) {
            alert('Maximum 3');
        });


    $('#pengolahan-jenis_olah_0').hide(); //  Hide Jenis Proses Dropdown
    document.getElementById("pengolahan-jenis_olah_1").disabled = true; // Disable Jenis Proses auto-assign

    $('#pengolahan-start').hide();
    $('#pengolahan-stop').hide(); 
    $('#pengolahan-istirahat-start').hide(); 
    $('#pengolahan-istirahat-stop').hide(); 
    $('#start-button').click(function(){
            var start = "START";
            $('#pengolahan-state').attr('value',start);
                    $('#pengolahan-start').show();
                    $('#pengolahan-stop').hide(); 
                    $('#pengolahan-istirahat-start').hide(); 
                    $('#pengolahan-istirahat-stop').hide();
            $('#pengolahan-jenis_olah').change(function(){
                var jenis_olah = $('#pengolahan-jenis_olah').val();
                var snfg_komponen = $('#pengolahan-snfg_komponen').val();
                var nomo = $('#pengolahan-nomo').val();
                var per_snfg_komponen = document.getElementById("pengolahan-snfg_komponen").disabled;
                                    if(per_snfg_komponen){
                                        $.get('index.php?r=pengolahan/lanjutan-pengolahanmo',{ nomo : nomo, jenis_olah : jenis_olah },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#pengolahan-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                                            $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#pengolahan-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                            var notFirst = $('#pengolahan-lanjutan').val();
                                                if(notFirst>1){
                                                    $('#planstart-box').hide();
                                                }else{
                                                    $('#planstart-box').show();
                                                }
                                        }); 
                                    }else{
                                        $.get('index.php?r=pengolahan/lanjutan-pengolahan',{ snfg_komponen : snfg_komponen, jenis_olah : jenis_olah },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#pengolahan-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                                            $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#pengolahan-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                        }); 
                                    } 

            });                     
        });


    $('#stop-button').click(function(){
            var stop = "STOP";
            $('#pengolahan-state').attr('value',stop);
                    $('#pengolahan-stop').show(); 
                    $('#pengolahan-start').hide(); 
                    $('#pengolahan-istirahat-start').hide(); 
                    $('#pengolahan-istirahat-stop').hide();    
            $('#pengolahan-jenis_olah').change(function(){
                var jenis_olah = $('#pengolahan-jenis_olah').val();
                var snfg_komponen = $('#pengolahan-snfg_komponen').val();    
                var nomo = $('#pengolahan-nomo').val(); 
                var per_snfg_komponen = document.getElementById("pengolahan-snfg_komponen").disabled;
                            if(per_snfg_komponen){
                                $.get('index.php?r=pengolahan/lanjutan-pengolahanmo',{ nomo : nomo, jenis_olah : jenis_olah },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#pengolahan-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                                    $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#pengolahan-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                }); 
                            }else{
                                $.get('index.php?r=pengolahan/lanjutan-pengolahan',{ snfg_komponen : snfg_komponen, jenis_olah : jenis_olah },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#pengolahan-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                                    $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#pengolahan-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                }); 
                            }
            });
            $('#rincian-hasil-olah').hide();

            $('#pengolahan-is_done').change(function(){
                var isDone = $('#pengolahan-is_done').val();
                if(isDone==1){
                    $('#rincian-hasil-olah').show();
                }
                else{
                    $('#rincian-hasil-olah').hide();
                }
            });
        });             

});

JS;
$this->registerJs($script);
?>

<div class="pengolahan-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <div class="info-box bg-aqua">
        <span class="info-box-icon"><i class="fa fa-hourglass-half"></i></span>

        <div class="info-box-content">
          <b><div class="col-md-3 col-sm-6 col-xs-12" id="nama_bulk_results"></div></b>
          <span class="info-box-number"><div id="nomo_results"></div><div id="snfg_komponen_results"></div></span>

          <div class="progress">
            <div class="progress-bar" style="width: 100%"></div>
          </div>
              <span class="progress-description">
              <div class="col-md-3 col-sm-6 col-xs-12" id="streamline_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="start_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="due_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="besar_batch_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="besar_lot_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="lot_ke_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="kode_jadwal_results"></div>
              </span>
        </div>
        <!-- /.info-box-content -->
    </div>

    
    <div class="box">
            
            <div class="box-header with-border">        
                    <h4><b>Pilih Jenis Scan</b></h4>            
                    <?=
                            Html::button('<b>Per-MO</b>',['class'=>'btn btn-success', 'id' => 'btn_nomo','style' => 'width: 120px; border-radius: 5px;']);
                    ?>
                    <!--<?=
                            Html::button('<b>Per-Komponen</b>',['class'=>'btn btn-primary', 'id' => 'btn_snfg_komponen', 'style' => 'width: 120px; border-radius: 5px;']);
                    ?> -->
            </div>

            <div id="infoform" style="width:50%; float: left; display: inline-block;" class="box-header with-border">

                    <h2 class="box-title"> <b>Input dan Informasi SNFG dari Planner</b></h2>
                    <br \>
                    <br \>

                    <div id="form_nomo">
                        <?= $form->field($model, 'nomo', ['hintType' => ActiveField::HINT_SPECIAL, 
                                                          'addon' => [
                                                                        'prepend' => [
                                                                            'content'=>'<i class="fa fa-barcode"></i>'
                                                                        ],
                                                                        'append' => [
                                                                            'content' => Html::button('Tekan Setelah Scan',['class'=>'btn btn-primary','id'=>'afterScnBtnNomo']),
                                                                            'asButton' => true
                                                                        ]
                                                         ]
                        ])->textInput(['placeholder'=>'Klik Disini, Scan pada Barcode Nomor MO yang tertera di SPK'])->hint('PASTIKAN CAPS LOCK TIDAK AKTIF!');
                        ?>
                    </div>

                    <div id="form_snfg_komponen">
                        <?= $form->field($model, 'snfg_komponen',  ['hintType' => ActiveField::HINT_SPECIAL, 
                                                                    'addon' => [
                                                                                'append' => [
                                                                                    'content'=>'<i class="fa fa-barcode"></i>'
                                                                                ],
                                                                                'prepend' => [
                                                                                    'content' => Html::button('Tekan Setelah Scan',['class'=>'btn btn-primary','id'=>'afterScnBtnKomponen']),
                                                                                    'asButton' => true
                                                                                ]
                                                            ]
                        ])->textInput(['disabled'=>'true','placeholder'=>'Klik Disini, Scan pada Barcode SNFG Komponen yang tertera di SPK'])->hint('Klik Pada Isian dibawah ini, kemudian arahkan scan pada barcode Komponen yang tertera di SPK');
                        ?>
                    </div>

                    <p \>
                    <b>SNFG</b>
                         <input type="text" class="form-control" id="pengolahan-snfg" disabled>
                    
                    <br \>
                    
                    <!-- Button untuk change event START, STOP -->
                    <div id="form_start_stop">
                        <a class="btn btn-success" id="start-button">
                                    <i class="fa fa-play"></i> Start
                        </a>

                        <a class="btn btn-danger" id="stop-button">
                                    <i class="fa fa-pause"></i> Stop
                        </a>
                        <p \>
                        <p \>
                        
                        <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>
                        
                        
                    </div>

            </div>
            <div style="width:50%; display: inline-block;" class="box-header with-border">
                <h2 class="box-title"><b>Posisi Terakhir</b></h2>
                    <br \>
                    <br \>
                    <b>Posisi</b>
                    <input type="text" class="form-control" id="ppr-posisi" placeholder="" disabled>
                    <p \>
                    <b>State</b>
                    <input type="text" class="form-control" id="ppr-state" placeholder="" disabled>
                    <p \>
                    <b>Jenis Proses</b>
                    <input type="text" class="form-control" id="ppr-jenis-proses" placeholder="" disabled>
                    <p \>
                    <b>Lanjutan</b>
                    <input type="text" class="form-control" id="ppr-lanjutan" placeholder="" disabled>
                    <p \>
                    <b>Is Done</b>
                    <input type="text" class="form-control" id="ppr-isdone" placeholder="" disabled>
                    <br \>
                    <br \>
                </div>
    </div>

    <div id="form_jenis">
        <!-- Jenis Olah Dropdown 00 -->
        <div id="pengolahan-jenis_olah_00"> 
        <?= $form->field($model, 'jenis_olah')->dropDownList(['REWORK' => 'REWORK','OLAH PREMIX' => 'OLAH PREMIX','OLAH 1' => 'OLAH 1','OLAH 2' => 'OLAH 2','ADJUST' => 'ADJUST'],['prompt'=>'Select Option']); ?>
        </div>

        <!-- Jenis Olah  Auto Assign -->

        <div id="pengolahan-jenis_olah_0">
        <?= $form->field($model, 'jenis_olah')->textInput(['id'=>'pengolahan-jenis_olah_1', 'disabled'=>'true' , 'readonly' =>'true']); ?>   
        </div>
    </div> 


    <div id="form_entry_start_stop">
        <div class="box" id="pengolahan-start">
                <div class="box-header with-border">
                  <h2 class="box-title">Start Entry</h2>

                    <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>

                    <?= $form->field($model, 'nama_line')->dropDownList(
                        ArrayHelper::map(Pengolahan::find()->all()
                        ,'nama_line','nama_line')
                        ,['prompt'=>'Select Line']

                    );?>


                    <?php echo $form->field($model, 'nama_operator', ['hintType' => ActiveField::HINT_SPECIAL, 'addon' => ['prepend' => ['content'=>'<i class="fa fa-barcode"></i>']]
                            ])->widget(JqueryTagsInput::className(),[])->hint(' <b>Scan</b> : Klik pada isian dibawah kemudian Scan Barcode pada Nametag<p \> <b>Manual</b> : isi dengan Format 4 Huruf Kapital, Contoh : "ABCD" lalu diikuti dengan menekan Enter ');
                    ?>


                    <div class="box" id="planstart-box">
                            <?= 
                                DatePicker::widget([
                                'model' => $model,
                                'form' => $form,
                                'name' => 'plan_start_olah',
                                'attribute' => 'plan_start_olah', 
                                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                                'options' => ['placeholder' => 'Select issue date ...'],
                                'pluginOptions' => [
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true
                                    ]
                                ]);
                            ?>

                            <?= $form->field($model, 'shift_plan_start_olah')->dropDownList([1 => 'Shift 1',2 => 'Shift 2'],['prompt'=>'Select Shift']); 
                            ?>

                            <?= 
                                DatePicker::widget([
                                'model' => $model,
                                'form' => $form,
                                'name' => 'plan_end_olah',
                                'attribute' => 'plan_end_olah', 
                                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                                'options' => ['placeholder' => 'Select issue date ...'],
                                'pluginOptions' => [
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true
                                    ]
                                ]);
                            ?>

                            <?= $form->field($model, 'shift_plan_end_olah')->dropDownList([1 => 'Shift 1',2 => 'Shift 2'],['prompt'=>'Select Shift']); 
                            ?>
                    </div>

                </div>
        </div>

        <div class="box" id="pengolahan-stop">
                <div class="box-header with-border">
                    <h2 class="box-title">Stop Entry</h2>

                     <?= $form->field($model, 'lanjutan')->textInput(['id'=>'pengolahan-lanjutan_stop','readonly' => 'true']) ?> 
                    
                    <label>
                      <input type="checkbox" id="is_kendala">
                      Terdapat Kendala ?
                    </label>

                    <div id="kendala-form">
                        <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-items', // required: css class selector
                            'widgetItem' => '.item', // required: css class
                            'limit' => 3, // the maximum times, an element can be cloned (default 999)
                            'min' => 1, // 0 or 1 (default 1)
                            'insertButton' => '.add-item', // css class
                            'deleteButton' => '.remove-item', // css class
                            'model' => $modelsKendala[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'keterangan',
                                'start',
                                // 'stop',
                            ],
                        ]); ?>

                        <div class="container-items"><!-- widgetContainer -->
                            <?php foreach ($modelsKendala as $i => $modelKendala): ?>
                            <div class="item panel panel-default"><!-- widgetBody -->
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left">Kendala</h3>
                                    <div class="pull-right">
                                        <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                        <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                        // necessary for update action.
                                        if (! $modelKendala->isNewRecord) {
                                            echo Html::activeHiddenInput($modelKendala, "[{$i}]id");
                                        }
                                    ?>

                                    <div class="row">
                                        <div class="col-sm-3">

                                                    <?= $form->field($modelKendala, "[{$i}]keterangan")->widget(Select2::classname(), [
                                                                'data' => ArrayHelper::map(Kendala::find()->all()
                                                        ,'keterangan','keterangan'),
                                                                'options' => ['placeholder' => 'Select Kendala'],
                                                                'pluginOptions' => [
                                                                    // 'tags' => true,
                                                                    'allowClear' => true
                                                                ],
                                                                ]);
                                                            ?>


                                        <!--  old  -->
                                       <!-- <?= $form->field($modelKendala, "[{$i}]keterangan")->dropDownList(
                                                        ArrayHelper::map(Kendala::find()->all()
                                                        ,'keterangan','keterangan')
                                                        ,['prompt'=>'Select Kendala']

                                                    );
                                            ?>  -->
                                        
                                        </div>
                                        <div class="col-sm-3">
                                            <?= $form->field($modelKendala, "[{$i}]start")->widget(TimePicker::classname(), [
                                                            'options' => ['placeholder' => 'Start'],
                                                            'pluginOptions' => [
                                                                    'minuteStep' => 5,
                                                                    'autoclose'=>true,
                                                                    'showMeridian' => false,
                                                                    'defaultTime' => '00:00'
                                                            ]
                                            ]); ?>
                                        </div>

                                        </div>
                                    </div><!-- .row -->
                                </div>
                            </div>
                            <?php endforeach; ?>
                    </div>

                    <?php DynamicFormWidget::end(); ?>
                </div>

                <div class="box-header with-border">
                    
                    <?=
                            Html::button('<b>Turun Bulk?</b>',['class'=>'btn btn-danger', 'id' => 'btn-turun-bulk','style' => 'width: 120px; border-radius: 5px;']);
                    ?>

                    <div id="turun-bulk-form">
                        <div class="container-items">
                             <div class="row">
                                <div class="col-sm-4">
                                <?= $form->field($model, 'turun_bulk_start')->widget(TimePicker::classname(), [
                                                                    'options' => ['placeholder' => 'Start'],
                                                                    'pluginOptions' => [
                                                                            'minuteStep' => 5,
                                                                            'autoclose'=>true,
                                                                            'showMeridian' => false,
                                                                            'defaultTime' => '00:00'
                                                                    ]
                                                    ]); 
                                ?>
                                </div>
                             </div>
                        </div>
                    </div>

                    <p \>

                    <?php
                            echo '<label class="cbx-label" for="is_done">Is Done?</label>';
                            echo CheckboxX::widget([
                                'model' => $model,
                                'attribute' => 'is_done',
                                'pluginOptions' => [
                                    'threeState' => false,
                                    'size' => 'lg'
                                ]
                            ]); 
                    ?>
                </div>
        </div>         

        <div class="form-group" id="create-button">
            <?php echo Dialog::widget([
                        'libName' => 'krajeeDialogCust', // a custom lib name
                        'options' => [  // customized BootstrapDialog options
                            'size' => Dialog::SIZE_LARGE, // large dialog text
                            'type' => Dialog::TYPE_WARNING, // bootstrap contextual color
                            'title' => 'Konfirmasi Input',
                            'buttons' => [
                                [
                                    
                                    'id' => 'btn-ok',
                                    'icon' => 'glyphicon glyphicon-check',
                                    'label' => 'Sudah Benar, Submit!',
                                    'cssClass' => 'btn-primary',
                                    'action' => new JsExpression("function() {
                                        $('#submitButton').submit();            
                                        }")
                                    // Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
                                ],
                            ]
                        ]
                    ]);
            ?>
            <button type="button" id="btn-custom" class="btn btn-info">Validate</button>
            <button id="submitButton" type="submit" class="btn btn-success">Creates</button>
            <!-- <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?> -->
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>


