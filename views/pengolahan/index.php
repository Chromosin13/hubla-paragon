

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PengolahanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengolahans';
$this->params['breadcrumbs'][] = $this->title;
?>
    <p>
        <?= Html::a('Create Pengolahan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<div class="pengolahan-index" style="overflow-x:auto;height:1000px;">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'nomo',
            'snfg',
            'snfg_komponen',
            // 'nobatch',
            'nama_line',
            'jumlah_operator',
            'nama_operator',
            'operator_2',
            'waktu',
            'state',
            'jenis_olah',
            'lanjutan',
            'besar_batch_real',
            'plan_start_olah',
            'plan_end_olah',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

<!-- <div class="pengolahan-index" style="overflow-x:auto;height:1000px;">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'snfg',
            'snfg_komponen',
            // 'nobatch',
            'nama_line',
            'jumlah_operator',
            'nama_operator',
            'operator_2',
            'waktu',
            'state',
            'jenis_olah',
            'lanjutan',
            'besar_batch_real',
            'plan_start_olah',
            'plan_end_olah',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?> -->

</div>