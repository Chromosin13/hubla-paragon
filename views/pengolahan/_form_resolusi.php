<?php

use faryshta\widgets\JqueryTagsInput;
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
use yii\helpers\ArrayHelper;
use app\models\ScmPlanner;
use yii\widgets\Pjax; 
// Menggunakan Pjax untuk Autorefresh
use yii\helpers\Url;
//use unclead\widgets\MultipleInput;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\date\DatePicker;
use kartik\widgets\DateTimePicker;
use kartik\widgets\TimePicker;
use kartik\checkbox\CheckboxX;
use kartik\widgets\Select2;
use dosamigos\tinymce\TinyMce;
use app\models\Pengolahan;
use app\models\Kendala;
use kartik\dialog\Dialog;
use yii\web\JsExpression;
use kartik\spinner\Spinner;
use wbraganca\tagsinput\TagsinputWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Pengolahan */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="pengolahan-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
    <!-- Left Side -->
    <div style="width:50%; float: left; display: inline-block;" class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-primary">
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/cmyk-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username"><b>PENGOLAHAN</b></h2>
              <h5 class="widget-user-desc">INPUT</h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <div id="form_nomo" class="box-header with-border">
                        <?= $form->field($model, 'nomo', ['hintType' => ActiveField::HINT_SPECIAL, 
                                                          'addon' => [
                                                                        'prepend' => [
                                                                            'content'=>'<i class="fa fa-barcode"></i>'
                                                                        ],
                                                                        'append' => [
                                                                            'content' => Html::button('Tekan Setelah Scan',['class'=>'btn btn-primary','id'=>'afterScnBtnNomo']),
                                                                            'asButton' => true
                                                                        ]
                                                         ]
                        ])->textInput(['placeholder'=>'Klik Disini, Scan pada Barcode Nomor MO yang tertera di SPK'])->hint('PASTIKAN CAPS LOCK TIDAK AKTIF!');
                        ?>
                    </div>
                <li><a>STREAMLINE <span class="pull-right badge bg-grey"><div id="streamline_results"></div></span></a>
                </li>
                <li><a>START <span class="pull-right badge bg-grey"><div id="start_results"></div></span></a>
                </li>
                <li><a>DUE <span class="pull-right badge bg-grey"><div id="due_results"></div></span></a>
                </li>
                <li><a>BESAR BATCH <span class="pull-right badge bg-grey"><div id="besar_batch_results"></div></span></a>
                </li>
                <li><a>BESAR LOT <span class="pull-right badge bg-grey"><div id="besar_lot_results"></div></span></a>
                </li>
                <li><a>LOT KE <span class="pull-right badge bg-grey"><div id="lot_ke_results"></div></span></a>
                </li>
                <li><a>KODE JADWAL <span class="pull-right badge bg-grey"><div id="kode_jadwal_results"></div></span></a>
                </li>
               <!--  <li><a href="#">Completed Projects <span class="pull-right badge bg-green">12</span></a></li>
                <li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li> -->
              </ul>
            </div>

    </div>



    <!-- Right Side -->
    <div id="right-side" style="width:50%; display: inline-block;" class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-primary">
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/clipboard-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username"><b>LAST POSITION</b></h2>
              <h5 class="widget-user-desc">SCHEDULE</h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                    <div  class="box-header with-border">
                        <div style="line-height:76.5px;">Posisi Terakhir</div>
                    </div>
                <li><a>POSISI <span class="pull-right badge bg-grey"><div id="ppr-posisi"></div></span></a>
                </li>
                <li><a>STATE <span class="pull-right badge bg-grey"><div id="ppr-state"></div></span></a>
                </li>
                <li><a>JENIS PROSES <span class="pull-right badge bg-grey"><div id="ppr-jenis-proses"></div></span></a>
                </li>
                <li><a>LANJUTAN <span class="pull-right badge bg-grey"><div id="ppr-lanjutan"></div></span></a>
                </li>
                <li><a>IS DONE <span class="pull-right badge bg-grey"><div id="ppr-isdone"></div></span></a>
                </li>
                <li><a>SNFG<span class="pull-right badge bg-grey"><div id="pengolahan-snfg"></div></span></a>
                </li>
                <li><a>NAMA FG<span class="pull-right badge bg-grey"><div id="nama_fg_results"></div></span></a>
                </li>
               <!--  <li><a href="#">Completed Projects <span class="pull-right badge bg-green">12</span></a></li>
                <li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li> -->
              </ul>
            </div>
    </div>

    <div class="well" id="loading">
    <?= Spinner::widget([
        'preset' => Spinner::LARGE,
    ])?>
    </div>

    <!-- Form Input Jadwal -->


    <div class="box-header with-border">
    <?= $form->field($model, 'edit_nama')->textInput(['readOnly'=>true,'value'=>Yii::$app->user->identity->username]) ?>

    <?php
        
        date_default_timezone_set('Asia/Jakarta');

        echo $form->field($model, 'edit_time')->textInput(['readonly' => 'true','value'=>date("Y-m-d H:i:s")])
    ?>

        <?php 
            date_default_timezone_set('Asia/Jakarta');

            echo '<label class="control-label">Waktu</label>';
            echo DateTimePicker::widget([
                'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                'model' => $model,
                'attribute' => 'waktu',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd hh:ii:ss',
                    'endDate' => date("Y-m-d H:i:s")
                ]
            ]);
        ?>
    </div>

    <div id="proses-input" class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-green">
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/check-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username">Proses</h2>
              <h5 class="widget-user-desc">Input</h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                    <div  class="box-header with-border">
                        <div id="form_start_stop">
                            <a style="width: 150px; border-radius: 5px;" class="btn btn-success" id="start-button">
                                        <i class="fa fa-play"></i>  START
                            </a>

                            <a style="width: 150px; border-radius: 5px;" class="btn btn-danger" id="stop-button">
                                        <i class="fa fa-pause"></i>  STOP
                            </a>
                            <p \>
                            
                            <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>
                            
                            
                        </div>

                        

                        <div id="form_jenis">
                            <!-- Jenis Olah Dropdown 00 -->
                            <div id="pengolahan-jenis_olah_00"> 
                            <?= $form->field($model, 'jenis_olah')->dropDownList(['REWORK' => 'REWORK','OLAH PREMIX' => 'OLAH PREMIX','OLAH 1' => 'OLAH 1','OLAH 2' => 'OLAH 2','ADJUST' => 'ADJUST'],['prompt'=>'Select Option']); ?>
                            </div>

                            <!-- Jenis Olah  Auto Assign -->

                            <div id="pengolahan-jenis_olah_0">
                            <?= $form->field($model, 'jenis_olah')->textInput(['id'=>'pengolahan-jenis_olah_1', 'disabled'=>'true' , 'readonly' =>'true']); ?>   
                            </div>
                        </div>

                    </div>
               <!--  <li><a href="#">Completed Projects <span class="pull-right badge bg-green">12</span></a></li>
                <li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li> -->
              </ul>
            </div>
    </div>


    <div id="form_entry_start_stop">
        <div class="box" id="pengolahan-start">
                <div class="box-header with-border">
                  <h2 class="box-title">Start Entry</h2>

                    <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>

                    <?php

                        echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Pengolahan::find()->all()
                            ,'nama_line','nama_line'),
                            'options' => ['placeholder' => 'Select Line'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);

                        // echo $form->field($model, 'nama_line')->dropDownList(
                        //     ArrayHelper::map(Pengolahan::find()->all()
                        //     ,'nama_line','nama_line')
                        //     ,['prompt'=>'Select Line']
                        // );
                    ?>

                    <!-- Nama Operator -->
                    <?php 

                        echo $form->field($model, 'nama_operator', ['hintType' => ActiveField::HINT_SPECIAL,'addon' => ['prepend' => ['content'=>'<i class="fa fa-barcode"></i>']]
                            ])->widget(TagsinputWidget::classname(), [
                            'clientOptions' => [
                                'trimValue' => true,
                                'allowDuplicates' => false,
                                'maxChars'=> 4,
                            ]
                        ])->hint(' <b>Scan</b> : Klik pada isian dibawah kemudian Scan Barcode pada Nametag<p \> <b>Manual</b> : isi dengan Format 4 Huruf Kapital, Contoh : "ABCD" lalu diikuti dengan menekan Enter ');

                    ?>


                    <div class="box" id="planstart-box">
                            <?= 
                                DatePicker::widget([
                                'model' => $model,
                                'form' => $form,
                                'name' => 'plan_start_olah',
                                'attribute' => 'plan_start_olah', 
                                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                                'options' => ['placeholder' => 'Select issue date ...'],
                                'pluginOptions' => [
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true
                                    ]
                                ]);
                            ?>

                            <?= $form->field($model, 'shift_plan_start_olah')->dropDownList([1 => 'Shift 1',2 => 'Shift 2'],['prompt'=>'Select Shift']); 
                            ?>

                            <?= 
                                DatePicker::widget([
                                'model' => $model,
                                'form' => $form,
                                'name' => 'plan_end_olah',
                                'attribute' => 'plan_end_olah', 
                                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                                'options' => ['placeholder' => 'Select issue date ...'],
                                'pluginOptions' => [
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true
                                    ]
                                ]);
                            ?>

                            <?= $form->field($model, 'shift_plan_end_olah')->dropDownList([1 => 'Shift 1',2 => 'Shift 2'],['prompt'=>'Select Shift']); 
                            ?>
                    </div>

                </div>
        </div>

        <div class="box" id="pengolahan-stop">
                <div class="box-header with-border">
                    <h2 class="box-title">Stop Entry</h2>

                     <?= $form->field($model, 'lanjutan')->textInput(['id'=>'pengolahan-lanjutan_stop','readonly' => 'true']) ?> 
                    
                    <?=
                            Html::button('<b>(+) Kendala</b>',['class'=>'btn btn-warning', 'id' => 'is_kendala','style' => 'width: 150px; border-radius: 5px;']);
                    ?>

                    

                    <div id="kendala-form">
                        <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-items', // required: css class selector
                            'widgetItem' => '.item', // required: css class
                            'limit' => 3, // the maximum times, an element can be cloned (default 999)
                            'min' => 1, // 0 or 1 (default 1)
                            'insertButton' => '.add-item', // css class
                            'deleteButton' => '.remove-item', // css class
                            'model' => $modelsKendala[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'keterangan',
                                'start',
                                // 'stop',
                            ],
                        ]); ?>

                        <div class="container-items"><!-- widgetContainer -->
                            <?php foreach ($modelsKendala as $i => $modelKendala): ?>
                            <div class="item panel panel-default"><!-- widgetBody -->
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left">Kendala</h3>
                                    <div class="pull-right">
                                        <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                        <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                        // necessary for update action.
                                        if (! $modelKendala->isNewRecord) {
                                            echo Html::activeHiddenInput($modelKendala, "[{$i}]id");
                                        }
                                    ?>

                                    <div class="row">
                                        <div class="col-sm-3">

                                                    <?= $form->field($modelKendala, "[{$i}]keterangan")->widget(Select2::classname(), [
                                                                'data' => ArrayHelper::map(Kendala::find()->all()
                                                        ,'keterangan','keterangan'),
                                                                'options' => ['placeholder' => 'Select Kendala'],
                                                                'pluginOptions' => [
                                                                    // 'tags' => true,
                                                                    'allowClear' => true
                                                                ],
                                                                ]);
                                                            ?>
                                        
                                        </div>
                                        <div class="col-sm-3">
                                            <?= $form->field($modelKendala, "[{$i}]start")->widget(TimePicker::classname(), [
                                                            'options' => ['placeholder' => 'Start'],
                                                            'pluginOptions' => [
                                                                    'minuteStep' => 5,
                                                                    'autoclose'=>true,
                                                                    'showMeridian' => false,
                                                                    'defaultTime' => '00:00'
                                                            ]
                                            ]); ?>
                                        </div>

                                        </div>
                                    </div><!-- .row -->
                                </div>
                            </div>
                            <?php endforeach; ?>
                    </div>

                    <?php DynamicFormWidget::end(); ?>
                </div>

                <div class="box-header with-border">
                    
                    <?=
                            Html::button('<b>(+) Pengemberan</b>',['class'=>'btn btn-danger', 'id' => 'btn-turun-bulk','style' => 'width: 150px; border-radius: 5px;']);
                    ?>

                    <br></br>

                    <div id="turun-bulk-form">
                        <div class="container-items">
                             <div class="row">
                                <div class="col-sm-4">

                                <?=
                                    $form->field($model, 'ember_start')->widget(DateTimePicker::classname(), [
                                        'options' => ['placeholder' => 'Enter event time ...'],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'showMeridian' => false,
                                            'minuteStep' => 5,
                                        ]
                                    ]);

                                ?>
                                </div>
                                <div id="stop-turun-bulk-form" class="col-sm-4">
                                <?=
                                    $form->field($model, 'ember_stop')->widget(DateTimePicker::classname(), [
                                        'options' => ['placeholder' => 'Enter event time ...'],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'showMeridian' => false,
                                            'minuteStep' => 5,
                                        ]
                                    ]);

                                ?>
                                </div>
                             </div>
                             <span class="badge bg-grey" id="durasi-ember"></span>
                             <br></br>
                        </div>
                    </div>

                    <?=
                            Html::button('<b>(+) Adjust</b>',['class'=>'btn btn-success', 'id' => 'btn-adjust','style' => 'width: 150px; border-radius: 5px;']);
                    ?>

                    <br></br>

                    <div id="adjust-form">
                        <div class="container-items">
                             <div class="row">
                                <div class="col-sm-4">

                                <?=
                                    $form->field($model, 'adjust_start')->widget(DateTimePicker::classname(), [
                                        'options' => ['placeholder' => 'Start Adjust ...'],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'showMeridian' => false,
                                            'minuteStep' => 5,
                                        ]
                                    ]);

                                ?>
                                </div>
                                <div id="stop-adjust-form" class="col-sm-4">
                                <?=
                                    $form->field($model, 'adjust_stop')->widget(DateTimePicker::classname(), [
                                        'options' => ['placeholder' => 'Stop Adjust ...'],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'showMeridian' => false,
                                            'minuteStep' => 5,
                                        ]
                                    ]);

                                ?>
                                </div>
                             </div>
                             <span class="badge bg-grey" id="durasi-adjust"></span>
                        </div>
                    </div>

                    <br></br>

                    <?php
                            echo '<label class="cbx-label" for="is_done"><b>Is Done?</b></label>';
                            echo CheckboxX::widget([
                                'model' => $model,
                                'attribute' => 'is_done',
                                'pluginOptions' => [
                                    'threeState' => false,
                                    'size' => 'lg'
                                ]
                            ]); 
                    ?>
                </div>
        </div>         

        <div class="form-group" id="create-button">
            <?php echo Dialog::widget([
                        'libName' => 'krajeeDialogCust', // a custom lib name
                        'options' => [  // customized BootstrapDialog options
                            'size' => Dialog::SIZE_LARGE, // large dialog text
                            'type' => Dialog::TYPE_SUCCESS, // bootstrap contextual color
                            'title' => 'Konfirmasi Input',
                            'buttons' => [
                                [
                                    
                                    'id' => 'btn-ok',
                                    'icon' => 'glyphicon glyphicon-check',
                                    'label' => 'Sudah Benar, Submit!',
                                    'cssClass' => 'btn-primary',
                                    'action' => new JsExpression("function() {
                                        $('#submitButton').submit();            
                                        }")
                                    // Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
                                ],
                            ]
                        ]
                    ]);
            ?>
            <button type="button" id="btn-custom" class="btn btn-info">Validate</button>
            <button id="submitButton" type="submit" class="btn btn-success">Creates</button>
            <!-- <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?> -->
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$script = <<< JS

// Adjust
$('#stop-adjust-form').hide();
$('#pengolahan-adjust_start').change(function(){
    
    // Get Current Timestamp
    var now = new Date();

    // Current Date 
    var start_js = new Date($('#pengolahan-adjust_start').val());
    if(start_js<now){
        // Show Stop Form
        $('#stop-adjust-form').fadeIn("slow");
        
    }else{
        // Hide Stop Form
        $('#stop-adjust-form').hide();
        alert('Pemilihan waktu harus sebelum waktu saat ini!');
    }

});

$('#pengolahan-adjust_stop').change(function(){
    var start_js = new Date($('#pengolahan-adjust_start').val());
    var stop_js = new Date($('#pengolahan-adjust_stop').val());
    if(stop_js<=start_js){
        alert('Periksa kembali waktu, Waktu Stop Adjust tidak boleh < Waktu Start Adjust!');
        $('#create-button').hide();
    }else{
        $('#durasi-adjust').html('Durasi Waktu Adjust : '+(parseFloat(stop_js-start_js)/parseFloat(60000))+' Menit atau '+(parseFloat(stop_js-start_js)/parseFloat(3600000)).toFixed(2)+' Jam' );
        $('#create-button').fadeIn("slow");
    };

});


//  Turun Bulk atau Pengemberan

$('#stop-turun-bulk-form').hide();
$('#pengolahan-ember_start').change(function(){
    

    
    // Get Current Timestamp
    var now = new Date();

    // Current Date 
    var start_js = new Date($('#pengolahan-ember_start').val());
    if(start_js<now){
        // Show Stop Form
        $('#stop-turun-bulk-form').fadeIn("slow");
    }else{
        // Hide Stop Form
        $('#stop-turun-bulk-form').hide();
        alert('Pemilihan waktu harus sebelum waktu saat ini!');
    }

});

$('#pengolahan-ember_stop').change(function(){
    var start_js = new Date($('#pengolahan-ember_start').val());
    var stop_js = new Date($('#pengolahan-ember_stop').val());
    if(stop_js<=start_js){
        alert('Periksa kembali waktu, Waktu Stop Bulk tidak boleh sebelum Waktu Start Bulk!');
        $('#create-button').hide();
    }else{
        $('#durasi-ember').html('Durasi Waktu Pengemberan : '+(parseFloat(stop_js-start_js)/parseFloat(60000))+' Menit atau '+(parseFloat(stop_js-start_js)/parseFloat(3600000)).toFixed(2)+' Jam' );
        $('#create-button').fadeIn("slow");
    };

});

// Loading Screen before all of the initial form is hidden
$(window).load(function() {
    $('#submitButton').hide(); // Hide Submit Button untuk Konfirmasi Input
    $('#start-button').hide(); // Hide Start Button
    $('#stop-button').hide(); // Hide Stop Button
    $('#form_start_stop').hide(); // Hide The Start Stop Button
    $('#form_jenis').hide(); // Hide Jenis Form
    $('#form_entry_start_stop').hide(); // Hide Form Entry Start Stop
    $('#turun-bulk-form').hide(); // Hide Turun Bulk Form
    $('#adjust-form').hide(); // Hide Adjust Form
    $('#proses-input').hide(); // Hide Jenis Form
    $('#loading').hide(); // Hide Jenis Form

    document.getElementById("pengolahan-ember_start").disabled = true; // Disable Pengolahan Turun Bulk Start 
});

// Get Variables Values

function nomo() {
    var nomo = $('#pengolahan-nomo').val();
    return nomo;
};

function jenis_olah() {
    var jenis_olah = $('#pengolahan-jenis_olah').val();
    return jenis_olah;
};

function jenis_olah_1() {
    var jenis_olah_1 = $('#pengolahan-jenis_olah_1').val();
    return jenis_olah_1;
};

function state() {
    var state = $('#pengolahan-state').val();
    return state;
};

function nama_line() {
    var nama_line = $('#pengolahan-nama_line').val();
    return nama_line;
};

function lanjutan() {
    var lanjutan = $('#pengolahan-lanjutan').val();
    return lanjutan;
};

function lanjutan_stop() {
    var lanjutan_stop = $('#pengolahan-lanjutan_stop').val();
    return lanjutan_stop;
};

function nama_operator() {
    var nama_operator = $('#pengolahan-nama_operator').val();
    return nama_operator;
};


function plan_start_olah() {
    var plan_start_olah = $('#pengolahan-plan_start_olah').val();
    return plan_start_olah;
};

function plan_end_olah() {
    var plan_end_olah = $('#pengolahan-plan_end_olah').val();
    return plan_end_olah;
};

function shift_plan_start_olah() {
    var shift_plan_start_olah = $('#pengolahan-shift_plan_start_olah').val();
    return shift_plan_start_olah;
};

function shift_plan_end_olah() {
    var shift_plan_end_olah = $('#pengolahan-shift_plan_end_olah').val();
    return shift_plan_end_olah;
};

function is_done() {
    var is_done = $('#pengolahan-is_done').val();
    return is_done;
};

function ppr_posisi() {
    var ppr_posisi = $('#ppr-posisi').val();
    return ppr_posisi;
};

// Nomor MO Validator
    // Klik Scan MO
    $('#afterScnBtnNomo').click(function(){
        if(nomo()==""){
            $('#form_start_stop').hide();    
        }
        else {
            $('#form_start_stop').fadeIn("slow");
            $('#proses-input').fadeIn("slow"); 
        }
    });

// Start Stop Button Clicked Validator
    $('#start-button').click(function(){
        $('#form_jenis').fadeIn("slow");
    });

    $('#stop-button').click(function(){
        $('#form_jenis').fadeIn("slow");
        $('#form_entry_start_stop').fadeIn("slow");
    });

// After Select Jenis Olah
    $('#pengolahan-jenis_olah').change(function(){
        if(jenis_olah()==""){
            $('#form_entry_start_stop').hide(); 

        } else {
            $('#form_entry_start_stop').show()
            if(state()=="START"){
                $('#create-button').hide();
                $('#pengolahan-nama_line').change(function(){
                  var nama_line = $('#pengolahan-nama_line').val();

                    $.get('index.php?r=pengolahan/check-line',{ nama_line : nama_line },function(data){
                    var data = $.parseJSON(data);
                        
                        if(nama_line=="Select Line"){
                            $('#create-button').hide();
                        }else if(data.state=="START"){
                            alert('Mesin ini belum di STOP oleh nomor jadwal '+data.nomo);
                            // $('#create-button').hide();
                            $('#create-button').fadeIn("slow");
                        }else{
                            $('#create-button').fadeIn("slow");
                        }

                    });
                });
            }
      
        }
        
    });

// After Click Pengemberan


$('#btn-turun-bulk').click(function(){
    
    if($('#turun-bulk-form').is(':hidden')){
        $('#create-button').hide();
        $('#turun-bulk-form').fadeIn("slow");
        document.getElementById("pengolahan-ember_start").disabled = false;
    } 
    else{
        $('#create-button').fadeIn("slow");
        $('#turun-bulk-form').hide();
    }
});

// After Click 

$('#btn-adjust').click(function(){
    
    if($('#adjust-form').is(':hidden')){
        $('#adjust-form').fadeIn("slow");
        document.getElementById("pengolahan-adjust_start").disabled = false;
        document.getElementById("pengolahan-adjust_stop").disabled = false;
    } 
    else{
        $('#adjust-form').hide();
        document.getElementById("pengolahan-adjust_start").disabled = true;
        document.getElementById("pengolahan-adjust_stop").disabled = true;
    }
});

// Validate Button

$("#btn-custom").on("click", function() {

    if(lanjutan==null||lanjutan_stop==null){
        alert('Penarikan data Lanjutan Gagal, Mohon refresh halaman dan scan ulang jadwal');
    }else if(state()=="START"&&lanjutan()==1){
        krajeeDialogCust.dialog('Nomor MO<span class="pull-right badge bg-red">'+(nomo())+'</span><br>'+
                                'State<span class="pull-right badge bg-red">'+(state())+'</span><br>'+
                                'Jenis Olah<span class="pull-right badge bg-red">'+(jenis_olah())+'</span><br>'+
                                'Lanjutan<span class="pull-right badge bg-red">'+(lanjutan())+'</span><br>'+
                                'Nama Line<span class="pull-right badge bg-red">'+(nama_line())+'</span><br>'+
                                'Nama Operator<span class="pull-right badge bg-red">'+(nama_operator())+'</span><br>'+
                                'Plan Start Olah<span class="pull-right badge bg-red">'+(plan_start_olah())+'</span><br>'+
                                'Plan End Olah<span class="pull-right badge bg-red">'+(plan_end_olah())+'</span><br>'+
                                'Plan Start Shift<span class="pull-right badge bg-red">'+(shift_plan_start_olah())+'</span><br>'+
                                'Plan End Shift<span class="pull-right badge bg-red">'+(shift_plan_end_olah())+'</span><br>'
                                ,function(result) {}
        );
    }else if(state()=="START"&&lanjutan()!=1){
        krajeeDialogCust.dialog(
                                'Nomor MO<span class="pull-right badge bg-red">'+(nomo())+'</span><br>'+
                                'State<span class="pull-right badge bg-red">'+(state())+'</span><br>'+
                                'Jenis Olah<span class="pull-right badge bg-red">'+(jenis_olah())+'</span><br>'+
                                'Lanjutan<span class="pull-right badge bg-red">'+(lanjutan())+'</span><br>'+
                                'Nama Line<span class="pull-right badge bg-red">'+(nama_line())+'</span><br>'+
                                'Nama Operator<span class="pull-right badge bg-red">'+(nama_operator())+'</span><br>'
                                ,function(result) {}
        );
    }else if(state()=="STOP"&&is_done()==1){
    
        krajeeDialogCust.dialog('Nomor MO<span class="pull-right badge bg-red">'+(nomo())+'</span><br>'+
                                'State<span class="pull-right badge bg-red">'+(state())+'</span><br>'+
                                'Jenis Olah<span class="pull-right badge bg-red">'+(jenis_olah_1())+'</span><br>'+
                                'Lanjutan<span class="pull-right badge bg-red">'+(lanjutan())+'</span><br>'+
                                '<b>Jadwal Sudah Selesai (Tidak Lanjut Shift)</b><p>',function(result) {}
        );

    }else if(state()=="STOP"&&is_done()==0){

        krajeeDialogCust.dialog('Nomor MO<span class="pull-right badge bg-red">'+(nomo())+'</span><br>'+
                                'State<span class="pull-right badge bg-red">'+(state())+'</span><br>'+
                                'Jenis Olah<span class="pull-right badge bg-red">'+(jenis_olah_1())+'</span><br>'+
                                'Lanjutan<span class="pull-right badge bg-red">'+(lanjutan())+'</span><br>'+
                                '<b>Jadwal Akan Dilanjutkan ke Shift Berikutnya (Belum Done)</b><p>',function(result) {}
        );

    }

});

 
$('#is_kendala').click(function(){

    // if(document.getElementById('is_kendala').checked){
    if($('#kendala-form').is(':hidden')){
            $('#kendala-form').fadeIn("slow");
            var i = 0;
            var maxnested = 3;                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);

                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = false;
                    document.getElementById("kendala-"+(i)+"-start").disabled = false;
        
                    i++;
                }

    }else{
            $('#kendala-form').hide();

            var i = 0;
            var maxnested = 3;
                
                for (; i < maxnested; ) { 
                    
                    // Debug count
                    //console.log(i);
                    
                    document.getElementById("kendala-"+(i)+"-keterangan").disabled = true;
                    document.getElementById("kendala-"+(i)+"-start").disabled = true;  

                    i++;
                }

    } 
 });


$('#view-scm').click(function(){

    $.post("index.php?r=scm-planner/get-komponen-mo&nomo="+nomo(), function (data){
            $("textarea#snfg_komponen_rincian").html(data);
    });

});



    $('#pengolahan-nomo').change(function(){

        $('#pengolahan-stop').hide();
        $('#pengolahan-start').hide(); 

        $('#loading').show(); // Hide Jenis Form

        // Populate Nama Line berdasarkan Nomor MO dan Sediaan 

        $.post("index.php?r=scm-planner/get-line-olah-nomo&nomo="+nomo(), function (data){
            $("select#pengolahan-nama_line").html(data);
        });

        // Populate Keterangan berdasarkan Nomor MO dan Sediaan 

        $.post("index.php?r=scm-planner/get-kendala-olah-nomo&nomo="+nomo(), function (data){
            $("select#kendala-0-keterangan").html(data);
                // $("select#kendala-1-keterangan").html(data);
                // $("select#kendala-2-keterangan").html(data);
        });

        $.get('index.php?r=scm-planner/get-planner-mo',{ nomo : nomo() },function(data){
            var data = $.parseJSON(data);
            //alert(data.sediaan);
            // $('#pengolahan-snfg').attr('value',data.snfg);
            $('#pengolahan-snfg').html(data.snfg);


            $('#streamline_results').html('STREAMLINE : '+data.streamline);
            $('#start_results').html('START : '+ data.start);
            $('#due_results').html('DUE : '+ data.due);
            $('#nama_bulk_results').html(data.nama_bulk);
            $('#nama_fg_results').html(data.nama_fg);
            $('#besar_batch_results').html('BESAR BATCH : '+ data.besar_batch);
            $('#besar_lot_results').html('BESAR LOT : '+ data.besar_lot);
            $('#lot_ke_results').html('LOT KE : '+ data.lot); ;
            $('#kode_jadwal_results').html('KODE JADWAL : '+ data.kode_jadwal); 
            $('#jumlah_results').html('JUMLAH : '+ data.jumlah);

        });
        $.get('index.php?r=scm-planner/get-pp',{ nomo : nomo() },function(data){
            var data = $.parseJSON(data);

            $('#ppr-posisi').html('Posisi : '+data.posisi);
            $('#ppr-state').html('State : '+data.state);
            $('#ppr-jenis-proses').html('Jenis Proses : '+data.jenis_proses);
            $('#ppr-lanjutan').html('Lanjutan : '+data.lanjutan);
            $('#ppr-isdone').html('Is Done : '+data.is_done);

            
            
        });
        $.get('index.php?r=scm-planner/get-planner-mo',{ nomo : nomo() },function(data){
            var data = $.parseJSON(data);

            if(data.status==null){
                $('#start-button').hide();
                $('#stop-button').hide(); 
                $('#create-button').hide();
                alert('Not Found');
            }
            
            if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
                $('#start-button').hide();
                $('#stop-button').hide(); 
                $('#create-button').hide();
                alert('Status ditetapkan sebagai HOLD/PAUSE dari Planner')
            }
            else{
                    $.get('index.php?r=pengolahan/get-last-resolusi',{ nojadwal : nomo() },function(data){
                        var data = $.parseJSON(data);
                        console.log(data);
                        $('#loading').hide(); // Hide Jenis Form
                        if(data===null){
                            $('#start-button').fadeIn("slow");
                            $('#stop-button').hide(); 
                            $('#start-button').click(function(){ 

                                $('#pengolahan-jenis_olah').change(function(){

                                    var jenis_olah = $('#pengolahan-jenis_olah').val();
                                    var nomo = $('#penimbangan-nomo').val();                                
                                    
                                        $.get('index.php?r=penimbangan/get-penimbanganmo',{ nomo : nomo, jenis_penimbangan : jenis_penimbangan },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                                            $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);                                                                    
                                        
                                        });
                                });
                            });
                        }else if(data.state=="START"){

                            $('#start-button').hide();
                            $('#stop-button').fadeIn("slow"); 

                                // Jenis Proses di Assign

                                $('#pengolahan-jenis_olah_00').hide();
                                document.getElementById("pengolahan-jenis_olah").disabled = true;
                                $('#pengolahan-jenis_olah_0').fadeIn("slow");
                                document.getElementById("pengolahan-jenis_olah_1").disabled = false;
                                $('#pengolahan-jenis_olah_1').attr('value',data.jenis_proses);


                                // Assign Lanjutan dan Lanjutan Istirahat

                                $('#stop-button').click(function(){                                
                                    
                                    $.get('index.php?r=pengolahan/lanjutan-pengolahanmo',{ nomo : nomo(), jenis_olah : jenis_olah_1() },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.sediaan);
                                        $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                                        $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);                                                                       
                                    
                                    });

                                });


                                // EO Lanjutan dan Lanjutan Istirahat 

                        }else if(data.state=="STOP"){
                            
                            $('#start-button').fadeIn("slow");
                            $('#stop-button').hide(); 
                            $('#start-button').click(function(){ 

                                $('#pengolahan-jenis_olah_1').change(function(){                              
                                    
                                    $.get('index.php?r=pengolahan/lanjutan-pengolahanmo',{ nomo : nomo(), jenis_olah : jenis_olah_1() },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.sediaan);
                                        $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                                        $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                                                       
                                    
                                    });
                                });
                            });

                        }
                    });
            }
        });
    });

    



$(function() {


    // Default Hide and Disable DynamicFormWidget for Kendala Model //

        $('#kendala-form').hide();
        document.getElementById("kendala-0-keterangan").disabled = true;
        document.getElementById("kendala-0-start").disabled = true;


    // DynamicForm override JS , Script Fix for non-working kartik date/timepicker afterInsert 
    // DynamicForm override JS , Fix for select2 kendala list

        // ID untuk penomoran nested dynamic form
        var id = 0;


        // Setelah Insert Button

        $(".dynamicform_wrapper").on('afterInsert', function(e, item) {
            var dateTimePickers = $(this).find('[data-krajee-kvtimepicker]');
            dateTimePickers.each(function(index, el) {
                $(this).parent().removeData().kvTimepicker('remove');
                $(this).parent().kvTimepicker(eval($(this).attr('data-krajee-kvtimepicker')));
            });
            

            // Override Fix for select2 kendala list 

            id += 1;
            console.log(id);

            $.post("index.php?r=scm-planner/get-kendala-olah-nomo&nomo="+nomo(), function (data){

                    $("select#kendala-"+(id)+"-keterangan").html(data);

            });


        });

        // Setelah Delete Button

        $(".dynamicform_wrapper").on('afterDelete', function(e, item) {
            
            // Override Fix for select2 kendala list 

            id -= 1;
            console.log(id);

        });


        // Jika Mencapai Batas Maximum
        $(".dynamicform_wrapper").on('limitReached', function(e, item) {
            alert('Maximum 3');
        });


    $('#pengolahan-jenis_olah_0').hide(); //  Hide Jenis Proses Dropdown
    document.getElementById("pengolahan-jenis_olah_1").disabled = true; // Disable Jenis Proses auto-assign

    $('#pengolahan-start').hide();
    $('#pengolahan-stop').hide(); 
    $('#start-button').click(function(){
            var start = "START";
            $('#pengolahan-state').attr('value',start);
                    $('#pengolahan-start').fadeIn("slow");
                    $('#pengolahan-stop').hide(); 
            $('#pengolahan-jenis_olah').change(function(){
               
                                    
                $.get('index.php?r=pengolahan/lanjutan-pengolahanmo',{ nomo : nomo(), jenis_olah : jenis_olah() },function(data){
                    var data = $.parseJSON(data);
                    //alert(data.sediaan);
                    $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                    $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                    var notFirst = $('#pengolahan-lanjutan').val();
                        if(notFirst>1){
                            $('#planstart-box').hide();
                        }else{
                            $('#planstart-box').fadeIn("slow");
                        }
                }); 
                                

            });                     
    });


    $('#stop-button').click(function(){
            var stop = "STOP";
            $('#pengolahan-state').attr('value',stop);
                    $('#pengolahan-stop').fadeIn("slow"); 
                    $('#pengolahan-start').hide();    
            $('#pengolahan-jenis_olah').change(function(){
                
                $.get('index.php?r=pengolahan/lanjutan-pengolahanmo',{ nomo : nomo(), jenis_olah : jenis_olah() },function(data){
                    var data = $.parseJSON(data);
                    //alert(data.sediaan);
                    $('#pengolahan-lanjutan').attr('value',data.lanjutan);
                    $('#pengolahan-lanjutan_stop').attr('value',data.lanjutan);
                }); 
                            
            });
            $('#rincian-hasil-olah').hide();

            $('#pengolahan-is_done').change(function(){
                if(is_done()==1){
                    $('#rincian-hasil-olah').fadeIn("slow");
                }
                else{
                    $('#rincian-hasil-olah').hide();
                }
            });
        });             

});

JS;
$this->registerJs($script);
?>