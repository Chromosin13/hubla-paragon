<?php

use faryshta\widgets\JqueryTagsInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ScmPlanner;
use yii\widgets\Pjax; 
// Menggunakan Pjax untuk Autorefresh
use yii\helpers\Url;
//use unclead\widgets\MultipleInput;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\widgets\TimePicker;
use app\models\Pengolahan;
/* @var $this yii\web\View */
/* @var $model app\models\Pengolahan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengolahan-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>


    <div class="box">
            <div class="box-header with-border">
                  <a class="btn btn-app" id="view-scm">
                <i class="fa fa-play"></i> View SCM Data
                </a>
                 <?= $form->field($model, 'nomo')->textInput(['disabled' => 'true']) ?>

                 <?= $form->field($model, 'snfg_komponen')->textInput(['disabled' => 'true']) ?> 

                <!-- <?= $form->field($model, 'snfg_komponen')->textInput(['disabled' => 'true']) ?> -->

                <?= $form->field($model, 'snfg')->textInput(['readonly' => 'true']) ?>
                <b>Streamline</b>
                <input type="text" class="form-control" id="streamline" disabled>
                <b>Start</b>
                <input type="text" class="form-control" id="start" placeholder="" disabled>
                <b>Due</b>
                <input type="text" class="form-control" id="due" placeholder="" disabled>
                <b>Nama Bulk</b>
                <input type="text" class="form-control" id="nama-bulk" placeholder="" disabled>
                <b>Nama FG</b>
                <input type="text" class="form-control" id="nama-fg" placeholder="" disabled>
                <b>Besar Batch</b>
                <input type="text" class="form-control" id="besar-batch" placeholder="" disabled>
                <b>Besar per lot (Kg)</b>
                <input type="text" class="form-control" id="besar-lot" placeholder="" disabled>
                <b>Lot ke-</b>
                <input type="text" class="form-control" id="lot" placeholder="" disabled>
                <b>Kode Jadwal</b>
                <input type="text" class="form-control" id="kode-jadwal" placeholder="" disabled>
            </div>
    </div>

    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'jenis_olah')->textInput(['readonly' => 'true']); ?>

    <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>

    <div class="box" id="pengolahan-start">
            <div class="box-header with-border">
              <h2 class="box-title">Start Entry</h2>

                <?= $form->field($model, 'nama_line')->textInput() ?>

                <?= $form->field($model, 'jumlah_operator')->textInput() ?>

                <?php echo $form->field($model, 'nama_operator')->widget(JqueryTagsInput::className([]));
                ?>
                
            </div>
    </div>

    <div class="box" id="pengolahan-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Stop Entry</h2>

                

                <?= $form->field($model, 'lanjutan')->textInput(['id'=>'pengolahan-lanjutan_stop','readonly' => 'true']) ?>

                
                 <div class="row">
                        <div class="panel panel-default">
                        
                        <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Rincian Kendala </h4>

                                <div class="panel-body">

                                        <div id="kendala-form">
                                                <?php DynamicFormWidget::begin([
                                                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                                    'widgetBody' => '.container-items', // required: css class selector
                                                    'widgetItem' => '.item', // required: css class
                                                    'limit' => 3, // the maximum times, an element can be cloned (default 999)
                                                    'min' => 0, // 0 or 1 (default 1)
                                                    'insertButton' => '.add-item', // css class
                                                    'deleteButton' => '.remove-item', // css class
                                                    'model' => $modelsKendala[0],
                                                    'formId' => 'dynamic-form',
                                                    'formFields' => [
                                                        'keterangan',
                                                        'start',
                                                        'stop',
                                                    ],
                                                ]); ?>

                                                <div class="container-items"><!-- widgetContainer -->
                                                    <?php foreach ($modelsKendala as $i => $modelKendala): ?>
                                                    <div class="item panel panel-default"><!-- widgetBody -->
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title pull-left">Kendala</h3>
                                                            <div class="pull-right">
                                                                <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="panel-body">
                                                            <?php
                                                                // necessary for update action.
                                                                if (! $modelKendala->isNewRecord) {
                                                                    echo Html::activeHiddenInput($modelKendala, "[{$i}]id");
                                                                }
                                                            ?>

                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <?= $form->field($modelKendala, "[{$i}]keterangan")->dropDownList(['MESIN RUSAK' => 'MESIN RUSAK','CUCI' => 'CUCI','SOSIALISASI/MEETING' => 'SOSIALISASI/MEETING','LAIN-LAIN' => 'LAIN-LAIN'],['prompt'=>'Select Option']); 
                                                                    ?>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <?= $form->field($modelKendala, "[{$i}]start")->widget(TimePicker::classname(), [
                                                                                    'options' => ['placeholder' => 'Start'],
                                                                                    'pluginOptions' => [
                                                                                            'autoclose'=>true,
                                                                                            'showMeridian' => false,
                                                                                    ]
                                                                    ]); ?>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <?= $form->field($modelKendala, "[{$i}]stop")->widget(TimePicker::classname(), [
                                                                                    'options' => ['placeholder' => 'Stop'],
                                                                                    'pluginOptions' => [
                                                                                        'autoclose'=>true,
                                                                                        'showMeridian' => false,
                                                                                    ]
                                                                ]); ?>
                                                                </div>
                                                            </div><!-- .row -->
                                                        </div>
                                                    </div>
                                                    <?php endforeach; ?>
                                                    </div>
                                                    <?php DynamicFormWidget::end(); ?>
                                        </div>

                                </div>

                        </div>
                                
                        <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Rincian Hasil Pengolahan</h4></div>
                        <div class="panel-body">
                                    
                                    <!-- <textarea rows="4" cols="50" class="form-control" id="snfg_komponen_rincian">
                                    </textarea> -->

                             <!-- <input type="text" class="form-control" id="snfg_komponen_rincian" readonly="true">
 -->
                             <?php DynamicFormWidget::begin([
                                'widgetContainer' => 'dynamicform_inner', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                'widgetBody' => '.container-items', // required: css class selector
                                'widgetItem' => '.item', // required: css class
                                'limit' => 20, // the maximum times, an element can be cloned (default 999)
                                'min' => 1, // 0 or 1 (default 1)
                                'insertButton' => '.add-item', // css class
                                'deleteButton' => '.remove-item', // css class
                                'model' => $modelsPengolahanItem[0],
                                'formId' => 'dynamic-form',
                                'formFields' => [
                                    'nobatch',
                                    'besar_batch_real',
                                ],
                            ]); ?>

                            <div class="container-items"><!-- widgetContainer -->
                            <?php foreach ($modelsPengolahanItem as $i => $modelPengolahanItem): ?>
                                <div class="item panel panel-default"><!-- widgetBody -->
                                    <div class="panel-heading">
                                        <h3 class="panel-title pull-left">Pengolahan Item</h3>
                                        <div class="pull-right">
                                            <!-- <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button> -->
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body">
                                        <?php
                                            // necessary for update action.
                                            if (! $modelPengolahanItem->isNewRecord) {
                                                echo Html::activeHiddenInput($modelPengolahanItem, "[{$i}]id");
                                            }
                                        ?>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <?= $form->field($modelPengolahanItem, "[{$i}]snfg_komponen")->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-3">
                                                <?= $form->field($modelPengolahanItem, "[{$i}]nobatch")->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-sm-3">
                                                <?= $form->field($modelPengolahanItem, "[{$i}]besar_batch_real")->textInput(['maxlength' => true]) ?>
                                            </div>
                                        </div><!-- .row -->
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            </div>
                            <?php DynamicFormWidget::end(); ?>







                        </div>
                        </div>
                    </div>

            </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>


                
    <?php ActiveForm::end(); ?>



</div>


<?php
$script = <<< JS
$(function() {
    var state = document.getElementById("pengolahan-state").value;
     if(state=="START"){
         $('#pengolahan-stop').hide();
     }else{
         $('#pengolahan-start').hide();
     }
});


$('#view-scm').click(function(){
    var snfg_komponen = $('#pengolahan-snfg_komponen').val();
    var nomo = $('#pengolahan-nomo').val();
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#pengolahan-snfg').attr('value',data.snfg);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
        $('#besar-batch').attr('value',data.besar_batch);
        $('#besar-lot').attr('value',data.besar_lot);
        $('#lot').attr('value',data.lot);
        $('#kode-jadwal').attr('value',data.kode_jadwal);
    });
    $.get('index.php?r=scm-planner/get-planner-mo',{ nomo : nomo },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#pengolahan-snfg_komponen').attr('value',data.snfg_komponen);
        $('#pengolahan-snfg').attr('value',data.snfg);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
        $('#besar-batch').attr('value',data.besar_batch);
        $('#besar-lot').attr('value',data.besar_lot);
        $('#lot').attr('value',data.lot);
        $('#kode-jadwal').attr('value',data.kode_jadwal);
    });
    $.get('index.php?r=scm-planner/get-planner-mo-mo',{ nomo : nomo },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#snfg_komponen_rincian').attr('value',data.snfg_komponen);
    });
});

JS;
$this->registerJs($script);
?>