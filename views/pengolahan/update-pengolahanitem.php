<?php
$script = <<< JS
$('input').keydown( function (event) { //event==Keyevent
    if(event.which == 13) {
        var inputs = $(this).closest('form').find(':input:visible');
        inputs.eq( inputs.index(this)+ 1 ).focus();
        event.preventDefault(); //Disable standard Enterkey action
    }
    // event.preventDefault(); <- Disable all keys  action
});

$(function() {


    //var pengolahan_id = $("#pengolahan-id").val();
    var pengolahan_ = $("#pengolahan-jenis_olah").val();
    var pengolahan_state = $("#pengolahan-state").val();
    var pengolahan_jenis_olah = $("#pengolahan-jenis_olah").val();
    var pengolahan_lanjutan = $("#pengolahan-lanjutan").val();

    //$('#id_results').html('ID Pengolahan : '+pengolahan_id);
    $('#state_results').html(' <b class="fa fa-plus"></b><font size="2"> STATE : '+pengolahan_state+'</font>');
    $('#jenis_olah_results').html(' <b class="fa fa-plus"></b><font size="2"> JENIS OLAH : '+ pengolahan_jenis_olah + '</font>');
    $('#lanjutan_results').html(' <b class="fa fa-plus"></b><font size="2"> LANJUTAN : '+ pengolahan_lanjutan + '</font>');



});
JS;
$this->registerJs($script);
?>

<div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-th-list"></i></span>
            <div class="info-box-content">
              <b>
              <div id="insert_batch_data_results">
                  <?php
                  if (is_null($model->snfg_komponen)){
                        echo $this->title = ' Nomor MO : ' . $model->nomo ;
                    } else {

                        echo $this->title = ' SNFG Komponen : ' . $model->snfg_komponen ;
                    }
                  ?>
              </div>

<!--               <div id="id_results"></div> -->
        <!--   <span class="info-box-number">
              <div id="jenis_olah_results"></div><div id="snfg_komponen_results"></div></span> -->
<!--                   <div class="col-md-3 col-sm-6 col-xs-12" id="state_results"></div>
                  <div class="col-md-3 col-sm-6 col-xs-12" id="jenis_olah_results"></div>
                  <div class="col-md-3 col-sm-6 col-xs-12" id="lanjutan_results"></div>
 -->
                   <div  id="state_results"></div>
                  <div  id="jenis_olah_results"></div>
                  <div id="lanjutan_results"></div>

            </div>
            <!-- /.info-box-content -->
        </div>


<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pengolahan */

// if (is_null($model->snfg_komponen)){
//                     echo $this->title = 'Insert Batch Data : ' . ' ' . $model->id . ' Nomor MO' . $model->nomo ;
//                 } else {

//                     echo $this->title = 'Insert Batch Data : ' . ' ' . $model->id . ' SNFG Komponen : ' . $model->snfg_komponen ;
//                 }


$this->params['breadcrumbs'][] = ['label' => 'Pengolahans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pengolahan-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_update-pengolahanitem', [
        'model' => $model,
        'modelsPengolahanItem' => $modelsPengolahanItem,
    ]) ?>

</div>
