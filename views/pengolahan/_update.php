<?php

use faryshta\widgets\JqueryTagsInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ScmPlanner;
use yii\widgets\Pjax; 
// Menggunakan Pjax untuk Autorefresh
use yii\helpers\Url;
//use unclead\widgets\MultipleInput;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\widgets\TimePicker;
use app\models\Pengolahan;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\DateTimePicker;
use kartik\checkbox\CheckboxX;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Pengolahan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengolahan-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <?= $form->field($model, 'nomo')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'jenis_olah')->textInput(['readonly' => 'true']); ?>

    <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'edit_nama')->textInput(['readOnly'=>true,'value'=>Yii::$app->user->identity->username]) ?>

    <?php
        
        date_default_timezone_set('Asia/Jakarta');

        echo $form->field($model, 'edit_time')->textInput(['readonly' => 'true','value'=>date("Y-m-d H:i:s")])
    ?>

    <?php 
        echo '<label class="control-label">Waktu</label>';
        echo DateTimePicker::widget([
            'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
            'model' => $model,
            'attribute' => 'formatwaktu',
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd hh:ii:ss',
                'endDate' => $model->formatwaktu
            ]
        ]);
    ?>

    <br></br>

    <div id="pengolahan-start">
    <div class="box-header with-border">
        <h2 class="box-title">Start Entry</h2>

        <?= $form->field($model, 'nama_line')->textInput() ?>

        <?= $form->field($model, 'nama_operator')->widget(TagsinputWidget::classname(), [
                                    'clientOptions' => [
                                        'trimValue' => true,
                                        'allowDuplicates' => false,
                                        'maxChars'=> 4,
                                        'minChars'=> 4,
                                    ]
        ]) ?>

        <div id="planstart-box">
                            <?= 
                                DatePicker::widget([
                                'model' => $model,
                                'form' => $form,
                                'name' => 'plan_start_olah',
                                'attribute' => 'plan_start_olah', 
                                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                                'options' => ['placeholder' => 'Select issue date ...'],
                                'pluginOptions' => [
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true
                                    ]
                                ]);
                            ?>

                            <?= $form->field($model, 'shift_plan_start_olah')->dropDownList([1 => 'Shift 1',2 => 'Shift 2'],['prompt'=>'Select Shift']); 
                            ?>

                            <?= 
                                DatePicker::widget([
                                'model' => $model,
                                'form' => $form,
                                'name' => 'plan_end_olah',
                                'attribute' => 'plan_end_olah', 
                                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                                'options' => ['placeholder' => 'Select issue date ...'],
                                'pluginOptions' => [
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true
                                    ]
                                ]);
                            ?>

                            <?= $form->field($model, 'shift_plan_end_olah')->dropDownList([1 => 'Shift 1',2 => 'Shift 2'],['prompt'=>'Select Shift']); 
                            ?>
        </div>
                            
    </div>
    </div>



   
    <div id="pengolahan-stop">
    <div class="box-header with-border">
        <h2 class="box-title">Stop Entry</h2>

        <div id="turun-bulk-form">
                        <div class="container-items">
                             <div class="row">
                                <div class="col-sm-4">

                                <?=
                                    $form->field($model, 'ember_start')->widget(DateTimePicker::classname(), [
                                        'options' => ['placeholder' => 'Enter event time ...'],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'showMeridian' => false,
                                            'minuteStep' => 5,
                                        ]
                                    ]);

                                ?>
                                </div>
                                <div id="stop-turun-bulk-form" class="col-sm-4">
                                <?=
                                    $form->field($model, 'ember_stop')->widget(DateTimePicker::classname(), [
                                        'options' => ['placeholder' => 'Enter event time ...'],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'showMeridian' => false,
                                            'minuteStep' => 5,
                                        ]
                                    ]);

                                ?>
                                </div>
                             </div>
                             <span class="badge bg-grey" id="durasi-ember"></span>
                             <br></br>
                        </div>
        </div>

        <div id="adjust-form">
                        <div class="container-items">
                             <div class="row">
                                <div class="col-sm-4">

                                <?=
                                    $form->field($model, 'adjust_start')->widget(DateTimePicker::classname(), [
                                        'options' => ['placeholder' => 'Start Adjust ...'],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'showMeridian' => false,
                                            'minuteStep' => 5,
                                        ]
                                    ]);

                                ?>
                                </div>
                                <div id="stop-adjust-form" class="col-sm-4">
                                <?=
                                    $form->field($model, 'adjust_stop')->widget(DateTimePicker::classname(), [
                                        'options' => ['placeholder' => 'Stop Adjust ...'],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'showMeridian' => false,
                                            'minuteStep' => 5,
                                        ]
                                    ]);

                                ?>
                                </div>
                             </div>
                             <span class="badge bg-grey" id="durasi-adjust"></span>
                        </div>
        </div>

        <?php
                            echo '<label class="cbx-label" for="is_done"><b>Is Done?</b></label>';
                            echo CheckboxX::widget([
                                'model' => $model,
                                'attribute' => 'is_done',
                                'pluginOptions' => [
                                    'threeState' => false,
                                    'size' => 'lg'
                                ]
                            ]); 
        ?>

                
        <div class="row">
        <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Rincian Kendala </h4>
        <div class="panel-body">
        <div id="kendala-form">
            <?php DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-items', // required: css class selector
                    'widgetItem' => '.item', // required: css class
                    'limit' => 3, // the maximum times, an element can be cloned (default 999)
                    'min' => 0, // 0 or 1 (default 1)
                    'insertButton' => '.add-item', // css class
                    'deleteButton' => '.remove-item', // css class
                    'model' => $modelsKendala[0],
                    'formId' => 'dynamic-form',
                    'formFields' => [
                        'keterangan',
                        'start',
                        'stop',
                    ],
            ]); ?>
            <div class="container-items"><!-- widgetContainer -->
                    <?php foreach ($modelsKendala as $i => $modelKendala): ?>
                    <div class="item panel panel-default"><!-- widgetBody -->
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">Kendala</h3>
                            <div class="pull-right">
                                <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                                // necessary for update action.
                                if (! $modelKendala->isNewRecord) {
                                    echo Html::activeHiddenInput($modelKendala, "[{$i}]id");
                                }
                            ?>

                            <div class="row">
                                <div class="col-sm-3">
                                    <?= $form->field($modelKendala, "[{$i}]keterangan")->dropDownList(['MESIN RUSAK' => 'MESIN RUSAK','CUCI' => 'CUCI','SOSIALISASI/MEETING' => 'SOSIALISASI/MEETING','LAIN-LAIN' => 'LAIN-LAIN'],['prompt'=>'Select Option']); 
                                    ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $form->field($modelKendala, "[{$i}]start")->widget(TimePicker::classname(), [
                                                    'options' => ['placeholder' => 'Start'],
                                                    'pluginOptions' => [
                                                            'autoclose'=>true,
                                                            'showMeridian' => false,
                                                    ]
                                    ]); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $form->field($modelKendala, "[{$i}]stop")->widget(TimePicker::classname(), [
                                                    'options' => ['placeholder' => 'Stop'],
                                                    'pluginOptions' => [
                                                        'autoclose'=>true,
                                                        'showMeridian' => false,
                                                    ]
                                ]); ?>
                                </div>
                            </div><!-- .row -->
                        </div>
                    </div>
                    <?php endforeach; ?>
                    </div>
            <?php DynamicFormWidget::end(); ?>
            </div>

        </div>
        </div>
                                
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Rincian Hasil Pengolahan</h4></div>
        <div class="panel-body">
                            
             <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_inner', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 20, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelsPengolahanItem[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'nobatch',
                    'besar_batch_real',
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($modelsPengolahanItem as $i => $modelPengolahanItem): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Pengolahan Item</h3>
                        <div class="pull-right">
                            <!-- <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button> -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                            // necessary for update action.
                            if (! $modelPengolahanItem->isNewRecord) {
                                echo Html::activeHiddenInput($modelPengolahanItem, "[{$i}]id");
                            }
                        ?>

                        <div class="row">
                            <div class="col-sm-3">
                                <?= $form->field($modelPengolahanItem, "[{$i}]snfg_komponen")->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-3">
                                <?= $form->field($modelPengolahanItem, "[{$i}]nobatch")->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-3">
                                <?= $form->field($modelPengolahanItem, "[{$i}]besar_batch_real")->textInput(['maxlength' => true]) ?>
                            </div>
                        </div><!-- .row -->
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
        </div>
        </div>
        </div>
        </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>


                
    <?php ActiveForm::end(); ?>



</div>


<?php
$script = <<< JS
$(function() {
    var state = document.getElementById("pengolahan-state").value;
     if(state=="START"){
         $('#pengolahan-stop').hide();
     }else{
         $('#pengolahan-start').hide();
     }
});


// $('#view-scm').click(function(){
//     var snfg_komponen = $('#pengolahan-snfg_komponen').val();
//     var nomo = $('#pengolahan-nomo').val();
//     $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
//         var data = $.parseJSON(data);
//         //alert(data.sediaan);
//         $('#pengolahan-snfg').attr('value',data.snfg);
//         $('#streamline').attr('value',data.streamline);
//         $('#start').attr('value',data.start);
//         $('#due').attr('value',data.due);
//         $('#nama-bulk').attr('value',data.nama_bulk);
//         $('#nama-fg').attr('value',data.nama_fg);
//         $('#besar-batch').attr('value',data.besar_batch);
//         $('#besar-lot').attr('value',data.besar_lot);
//         $('#lot').attr('value',data.lot);
//         $('#kode-jadwal').attr('value',data.kode_jadwal);
//     });
//     $.get('index.php?r=scm-planner/get-planner-mo',{ nomo : nomo },function(data){
//         var data = $.parseJSON(data);
//         //alert(data.sediaan);
//         $('#pengolahan-snfg_komponen').attr('value',data.snfg_komponen);
//         $('#pengolahan-snfg').attr('value',data.snfg);
//         $('#streamline').attr('value',data.streamline);
//         $('#start').attr('value',data.start);
//         $('#due').attr('value',data.due);
//         $('#nama-bulk').attr('value',data.nama_bulk);
//         $('#nama-fg').attr('value',data.nama_fg);
//         $('#besar-batch').attr('value',data.besar_batch);
//         $('#besar-lot').attr('value',data.besar_lot);
//         $('#lot').attr('value',data.lot);
//         $('#kode-jadwal').attr('value',data.kode_jadwal);
//     });
//     $.get('index.php?r=scm-planner/get-planner-mo-mo',{ nomo : nomo },function(data){
//         var data = $.parseJSON(data);
//         //alert(data.sediaan);
//         $('#snfg_komponen_rincian').attr('value',data.snfg_komponen);
//     });
// });

JS;
$this->registerJs($script);
?>