<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterDataLine */

$this->title = 'Create Master Data Line';
$this->params['breadcrumbs'][] = ['label' => 'Master Data Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-data-line-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'list_sediaan' => $list_sediaan,
    ]) ?>

</div>
