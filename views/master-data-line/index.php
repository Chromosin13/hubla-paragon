<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterDataLineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Data Lines';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-data-line-index">

    <p>
        <?= Html::a('Create Master Data Line', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'nama_line',
            'sediaan',
            'posisi',

            ['class' => 'yii\grid\ActionColumn',
             'template' => '{view} {update}'],
        ],
    ]); ?>
</div>
