<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\MasterDataSediaan;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MasterDataLine */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-data-line-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_line')->textInput() ?>

    <?php 
                        echo $form->field($model, 'sediaan')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(MasterDataSediaan::find()->all(),'sediaan','sediaan'),
                            'options' => ['placeholder' => 'Select Sediaan'],
                            // 'pluginOptions' => [
                            //     'templateResult' => new JsExpression('format'),
                            //     'escapeMarkup' => $escape,
                            //     'allowClear' => true,
                            // ],
                        ])->label('Sediaan');
                        

                    ?>

    <?= $form->field($model, 'posisi')->dropDownList(['PENIMBANGAN' => 'PENIMBANGAN','PENGOLAHAN' => 'PENGOLAHAN','KEMAS' => 'KEMAS'],['prompt'=>'Select Posisi']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
