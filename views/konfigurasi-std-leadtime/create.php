<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KonfigurasiStdLeadtime */

$this->title = 'Create Konfigurasi Std Leadtime';
$this->params['breadcrumbs'][] = ['label' => 'Konfigurasi Std Leadtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="konfigurasi-std-leadtime-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
