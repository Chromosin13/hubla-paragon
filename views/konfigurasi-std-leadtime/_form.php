<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KonfigurasiStdLeadtime */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="konfigurasi-std-leadtime-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'koitem')->textInput() ?>

    <?= $form->field($model, 'koitem_bulk')->textInput() ?>

    <?= $form->field($model, 'naitem')->textInput() ?>

    <?= $form->field($model, 'std_type')->textInput() ?>

    <?= $form->field($model, 'last_update')->textInput() ?>

    <?= $form->field($model, 'jenis')->textInput() ?>

    <?= $form->field($model, 'value')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
