<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KonfigurasiStdLeadtimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


if($type=='P'){
    $this->title = 'Update Standard Planner';

}else{
    $this->title = 'Update Standard Bonus';
}




$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="panel box box-primary">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed">
                        Quick Tutorial
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="box-body">
                        Click on the highlighted value then set the new value, each changes will be recorded in the database. To see the historical changes of the value, you can click on the eye icon located at the right.
                    </div>
                  </div>
    </div>

<div class="konfigurasi-std-leadtime-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'koitem',
                'filter' => false,
                
            ],
            [
                'attribute' => 'std_type',
                'filter' => false,
                
            ],
            'koitem_bulk',
            'naitem',
            'last_update',
            'jenis',
            [
                    'class'=>'kartik\grid\EditableColumn',
                    'attribute'=>'value',
                    'editableOptions' => [
                        'asPopover' => false,
                    ],
            ],
            ['class' => 'yii\grid\ActionColumn',
             'template' => '{index}',
                        'contentOptions' => ['style' => 'max-width:40px;'],
                        'buttons' => [
                            'index' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', 'index.php?r=konfigurasi-std-leadtime-history/index&id='.$model->id, [
                                            'title' => Yii::t('app', 'History'),
                                ]);
                            }
                        ],
            ],
        ],
    ]); ?>

    <p>
        <?php 

            if($type=='P'){
                echo Html::a('Next', ['konfigurasi-std-leadtime/fill-b', 'koitem' => $koitem], ['class' => 'btn btn-success']); 

            }else{
                echo Html::a('Check Page', ['konfigurasi-std-leadtime/check'], ['class' => 'btn btn-info']);
            }
        ?>
    </p>
</div>
