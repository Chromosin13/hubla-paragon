<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\KonfigurasiStdLeadtime */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="konfigurasi-std-leadtime-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'koitem')->textInput() ?>

    <div id="form-create">
        <?= 

            // Html::submitButton($model->isNewRecord ? 'Buat Work Order' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])

            Html::a('Buat Konfigurasi Baru', ['konfigurasi-std-leadtime/create-trigger'], ['class' => 'btn btn-success']);  


        ?>
    </div>

    <div id="form-update">
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$script = <<< JS

$('#konfigurasistdleadtime-koitem').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});


$('#form-create').hide();
$('#form-update').hide();

$('#konfigurasistdleadtime-koitem').change(function(){  

    var koitem = $('#konfigurasistdleadtime-koitem').val();


    $.get('index.php?r=konfigurasi-std-leadtime/check-koitem',{ koitem : koitem },function(data){
        var data = $.parseJSON(data);
        if(data==null){
            $('#form-create').show();
            $('#form-update').hide();
            alert("Koitem doesn't exist, you may create a new entry. Otherwise refesh the page and be sure to insert Koitem without whitespaces");
        }

        else if(data.id>0){

          alert('Koitem found, will redirect to the list page');
          
          $('#form-create').hide();
          $('#form-update').show();
          window.location = "http://10.3.5.102/flowreport/web/index.php?r=konfigurasi-std-leadtime/fill-p&koitem="+data.koitem;

          var koitem = $('#konfigurasistdleadtime-koitem').val();

          
        }
    
    });
});


JS;
$this->registerJs($script);
?>

