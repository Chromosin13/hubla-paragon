<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KonfigurasiStdLeadtime */

$this->title = 'Update Konfigurasi Std Leadtime: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Konfigurasi Std Leadtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="konfigurasi-std-leadtime-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
