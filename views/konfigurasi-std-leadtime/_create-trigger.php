<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KonfigurasiStdLeadtime */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="konfigurasi-std-leadtime-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'koitem')->textInput() ?>

    <?= $form->field($model, 'koitem_bulk')->textInput() ?>

    <?= $form->field($model, 'naitem')->textInput() ?>

    <div class="form-group">
        <!-- <?= Html::submitButton('Create', ['konfigurasi-std-leadtime/create-trigger'], ['class' => 'btn btn-success']);  ?> -->

        <?= Html::submitButton('Submit', ['class' => 'submit']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
