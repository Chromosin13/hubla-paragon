<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KonfigurasiStdLeadtimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Konfigurasi Std Leadtimes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="konfigurasi-std-leadtime-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Konfigurasi Std Leadtime', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'koitem',
            'koitem_bulk',
            'naitem',
            'std_type',
            'last_update',
            'jenis',
            'value',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
