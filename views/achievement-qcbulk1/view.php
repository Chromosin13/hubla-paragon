<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementQcbulk1 */

$this->title = $model->row_number;
$this->params['breadcrumbs'][] = ['label' => 'Achievement Qcbulk1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-qcbulk1-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->row_number], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->row_number], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'row_number',
            'tanggal_stop',
            'tanggal_start',
            'nama_line',
            'sediaan',
            'nomo',
            'snfg',
            'snfg_komponen',
            'nama_fg',
            'jenis_periksa',
            'leadtime',
            'std_leadtime',
            'status_leadtime:ntext',
            'achievement_rate',
            'rft',
            'koitem_bulk',
            'koitem_fg',
            'nama_bulk',
            'is_done',
            'status',
        ],
    ]) ?>

</div>
