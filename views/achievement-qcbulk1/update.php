<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementQcbulk1 */

$this->title = 'Update Achievement Qcbulk1: ' . $model->row_number;
$this->params['breadcrumbs'][] = ['label' => 'Achievement Qcbulk1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->row_number, 'url' => ['view', 'id' => $model->row_number]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="achievement-qcbulk1-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
