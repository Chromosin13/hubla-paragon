<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AchievementQcbulk1 */

$this->title = 'Create Achievement Qcbulk1';
$this->params['breadcrumbs'][] = ['label' => 'Achievement Qcbulk1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-qcbulk1-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
