<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;


/* @var $this yii\web\View */
/* @var $searchModel app\models\AchievementQcbulk1Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Achievement Qcbulk1s';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-qcbulk1-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box-body">
                <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                    
                    <?php

                      $dataASTD= Yii::$app->db->createCommand(
                                      "select
                                          round((count(case when status_leadtime='ACHIEVED' then 1 end)::numeric/count(status_leadtime)::numeric)*100,1)::numeric as astd,
                                          tanggal_start
                                      from achievement_qcbulk_1
                                      where tanggal_start between current_date - interval '7 day' and current_date
                                      group by tanggal_start
                                      order by tanggal_start asc
                      ")->queryAll();


                      echo Highcharts::widget([
                                                'scripts' => [
                                                    'modules/exporting',
                                                    'themes/grid-light',
                                                ],
                                               'options' => [
                                                  'title' => ['text' => 'ASTD'],
                                                  'xAxis' => [
                                                     'categories' => new SeriesDataHelper($dataASTD,['tanggal_start']),
                                                  ],
                                                  'yAxis' => [
                                                     'title' => ['text' => '(%) Achievement Standar Pengolahan ASTD']
                                                  ],
                                                   'plotOptions' => [
                                                                    'line' => [
                                                                        'dataLabels' => [
                                                                                            'enabled' => true
                                                                                        ],
                                                                        'enableMouseTracking' => true
                                                                        ]
                                                                    ],
                                                  'series' => [
                                                     ['name' => 'Proses', 'data' => 
                                                     new SeriesDataHelper($dataASTD,['astd:float']),
                                                     ]
                                                  ]
                                               ]
                                            ]);

                      ?>
                      </div>
                      <div class="col-md-6">
                      <?php

                      $datarft= Yii::$app->db->createCommand(
                                      "select
                                          round(
                                                (
                                                    sum(rft)::numeric
                                                    /count(rft)::numeric
                                                )*100
                                                ,1)::numeric as rft,
                                          tanggal_start
                                      from achievement_qcbulk_1
                                      where tanggal_start between current_date - interval '7 day' and current_date
                                      group by tanggal_start
                                      order by tanggal_start asc
                      ")->queryAll(); 

                      echo Highcharts::widget([
                                                'scripts' => [
                                                    'modules/exporting',
                                                    'themes/grid-light',
                                                ],
                                               'options' => [
                                                  'title' => ['text' => 'RFT'],
                                                  'xAxis' => [
                                                     'categories' => new SeriesDataHelper($datarft,['tanggal_start']),
                                                  ],
                                                  'yAxis' => [
                                                     'title' => ['text' => '(%) RFT']
                                                  ],
                                                   'plotOptions' => [
                                                                    'line' => [
                                                                        'dataLabels' => [
                                                                                            'enabled' => true
                                                                                        ],
                                                                        'enableMouseTracking' => true
                                                                        ]
                                                                    ],
                                                  'series' => [
                                                     ['name' => 'Proses', 'data' => 
                                                     new SeriesDataHelper($datarft,['rft:float']),
                                                     ]
                                                  ]
                                               ]
                                            ]);

                    ?>
                    </div>
                </div>
            </div>
        </div>


     <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'primary', 'heading'=>'Achievement Line Penimbangan'],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                'row_number',
                'tanggal_stop',
                'tanggal_start',
                'nama_line',
                'sediaan',
                'nomo',
                'snfg',
                'snfg_komponen',
                'nama_fg',
                'jenis_periksa',
                'leadtime',
                'std_leadtime',
                'status_leadtime:ntext',
                'achievement_rate',
                'rft',
                'koitem_bulk',
                'koitem_fg',
                'nama_bulk',
                'is_done',
                'status',
             ],
        ]);
    
    ?>

    <!-- <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'row_number',
            'tanggal_stop',
            'tanggal_start',
            'nama_line',
            'sediaan',
            'nomo',
            'snfg',
            'snfg_komponen',
            'nama_fg',
            'jenis_periksa',
            'leadtime',
            'std_leadtime',
            'status_leadtime:ntext',
            'achievement_rate',
            'rft',
            'koitem_bulk',
            'koitem_fg',
            'nama_bulk',
            'is_done',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?> -->
</div>
