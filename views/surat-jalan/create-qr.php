<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SuratJalan */


?>
<div class="surat-jalan-create">
	
    <?= $this->render('_form-qr', [
        'model' => $model,
    ]) ?>

</div>
