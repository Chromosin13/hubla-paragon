<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SuratJalan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="surat-jalan-form">

    <?php $form = ActiveForm::begin(); ?>

    <div id="hide">
    	<?= $form->field($model, 'nosj')->textInput(['readOnly'=>true,'value'=>'DRAFT [ID]']) ?>
	</div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>



<?php
$script = <<< JS

// Hide Create new Field

// $('#hide').hide();


JS;
$this->registerJs($script);
?>
