<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SuratJalanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="surat-jalan-index">

   <!--  <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Surat Jalan', ['create'], ['class' => 'btn btn-success btn-block']) ?>
    </p>
    <p>
        <?= Html::a('Create Surat Jalan with QR Code', ['create-qr'], ['class' => 'btn btn-warning btn-block']) ?>
    </p>

    <?php

        if(Yii::$app->user->identity->role==111){
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsiveWrap' => false,
                'columns' => [
                    [
                     'class' => 'kartik\grid\ActionColumn', 
                     'template' => '{create-rincian}{create-rincian-qr}',
                     'buttons' => [
                            'create-rincian' => function ($url,$model) {
                                $url = "index.php?r=surat-jalan-rincian/create-rincian&id=".$model->id;
                                return Html::a(
                                    '<span class="btn btn-sm btn-success fa fa-cart-plus";"></span>',
                                    $url, 
                                    [
                                        'title' => 'Rincian',
                                        'style' => 'margin:5px;'
                                    ]
                                );
                            },
                            'create-rincian-qr' => function ($url,$model) {
                                $url = "factory.pti-cosmetics.com/flowreport/web/index.php?r=surat-jalan-rincian/create-rincian-qr&id=".$model->id;
                                return Html::a(
                                    '<span class="btn btn-sm btn-warning fa fa-plus";"></span>',
                                    $url, 
                                    [
                                        'title' => 'QR surat',
                                        'style' => 'margin:5px;'
                                    ]
                                );
                            },
                      ],
                    ],
                    // 'id',
                    'nosj',
                    'timestamp',
                    [
                        'class' => 'kartik\grid\ActionColumn', 
                        'header' => 'Delete',
                        'template' => '{delete}'
                    ],

                ],
            ]); 
        }else{
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsiveWrap' =>false,
                'columns' => [
                    ['class' => 'kartik\grid\ActionColumn', 
                    'template' => '{create-rincian}{create-rincian-qr}',
                     'buttons' => [
                        'create-rincian' => function ($url,$model) {
                            $url = "index.php?r=surat-jalan-rincian/create-rincian&id=".$model->id;
                            return Html::a(
                                '<span class="btn btn-sm btn-success fa fa-cart-plus";"></span>',
                                $url, 
                                [
                                    'title' => 'Rincian',
                                    'style' => 'margin:5px;'
                                ]
                            );
                        },
                        'create-rincian-qr' => function ($url,$model) {
                            $url = "http://factory.pti-cosmetics.com/flowreport/web/index.php?r=surat-jalan-rincian/create-rincian-qr&id=".$model->id;
                            // $url = "index.php?r=surat-jalan-rincian/create-rincian-qr&id=".$model->id;
                            return Html::a(
                                '<span class="btn btn-sm btn-warning fa fa-plus";"></span>',
                                $url, 
                                [
                                    'title' => 'QR Rincian',
                                    'style' => 'margin:5px;'
                                ]
                            );
                        },
                      ],
                    ],
                    // ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'nosj',
                    'timestamp',

                    [
                        'class' => 'kartik\grid\ActionColumn', 
                        'header' => 'Delete',
                        'template' => '{delete}'
                    ],
                ],
            ]); 
        }
    ?>
</div>
