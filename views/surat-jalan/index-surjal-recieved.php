<?php

use app\models\SuratJalan;
use app\models\SuratJalanRincian;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\models\QcFgV2;

/* @var $this yii\web\View */
/* @var $model app\models\SuratJalanRincian */
/* @var $form yii\widgets\ActiveForm */
?>

<?php

$this->registerJsFile('js/instascan.min.js', ['position' => \yii\web\View::POS_HEAD, 'type' => 'text/javascript']);

?>


        <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <?php 

                if($nosj=="DRAFT"){
                    echo '<div class="widget-user-header bg-yellow">';
                }else{
                    echo '<div class="widget-user-header bg-green">';
                }
            ?>
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/clipboard-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username"><?=$nosj?></h2>
              <h5 class="widget-user-desc">Surat Jalan</h5>
            </div>
<p></p>





</div><br \>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'responsiveWrap' => false,
        'floatHeader'=>'true',
        'floatHeaderOptions' => ['position' => 'absolute',],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nosj',
            'timestamp',
            // [
            //       'attribute' => 'nourut',
            //       'label'=>'PALET',
            // ],
            // [
            //       'attribute' => 'qty',
            //       'label'=>'QTY',
            // ],
        ],
    ]); 
    ?>


    <div id="form-confirm">


    </div>

    <p></p>


<<!-- ?php 

  if(empty($is_received)){
      echo Html::a('KONFIRMASI TERIMA NDC', ['qc-fg-v2/terima-surat-jalan-ndc', 'surat_jalan' => $nosj], ['class' => 'btn btn-danger btn-block']);
  }else{
      echo 'Sudah Dilakukan Penerimaan';
  }

?>
 -->


<?php
$script = <<< JS

JS;
$this->registerJs($script);
?>