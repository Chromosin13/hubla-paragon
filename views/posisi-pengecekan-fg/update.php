<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PosisiPengecekanFg */

$this->title = 'Update Posisi Pengecekan Fg: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Posisi Pengecekan Fgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="posisi-pengecekan-fg-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
