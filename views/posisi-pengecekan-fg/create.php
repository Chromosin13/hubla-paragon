<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PosisiPengecekanFg */

$this->title = 'Create Posisi Pengecekan Fg';
$this->params['breadcrumbs'][] = ['label' => 'Posisi Pengecekan Fgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posisi-pengecekan-fg-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
