<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PosisiPengecekanFgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posisi Pengecekan Fgs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posisi-pengecekan-fg-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Posisi Pengecekan Fg', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'snfg',
            'palet',
            'start',
            'stop',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
