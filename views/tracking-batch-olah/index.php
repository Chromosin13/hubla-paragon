<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use app\models\TrackingBatchOlah;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrackingSoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tracking Batch Olah';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
.table-bordered > thead > tr > th,
.table-bordered > tbody > tr > th,
.table-bordered > tfoot > tr > th,
.table-bordered > thead > tr > td,
.table-bordered > tbody > tr > td,
.table-bordered > tfoot > tr > td {
    /* border: 1px solid #00a65a; */
    /* border: 1px solid #960018; */
}

</style>

<div class="callout callout-success">
  <h4>Scope Data</h4>
  <p> 60 Hari (2 Bulan) Terakhir dari Tanggal Start Pengolahan</p>
</div>


<div class="tracking-batch-olah-index">
    <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'responsiveWrap' => false,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'danger', 'heading'=>'Tracking Batch Olah'],
            'formatter' => [
                'class' => 'yii\i18n\Formatter',
                'timeZone' => 'Asia/Jakarta'
            ],
            'toolbar' => [
            ],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                // [
                //     'attribute' => 'id_pengolahan',
                //     'contentOptions' => ['style' => ' text-align: center;width: 10px;'],
                //     'headerOptions' => ['style' => 'text-align: center;width: 10px'],
                // ],
                [
                    'attribute'=>'nama_fg',
                    'label' => 'Finished Good', 
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(TrackingBatchOlah::find()->orderBy('nama_fg')->asArray()->all(), 'nama_fg', 'nama_fg'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'SKU'],
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 250px;font-size: 14px'],
                    'headerOptions' => ['style' => 'text-align: center;width: 100px;'],
                    // 'group'=>true,  // enable grouping
                    //'subGroupOf'=>1,
                ],
                [
                    'attribute'=>'nama_bulk',
                    'label' => 'Nama Bulk', 
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(TrackingBatchOlah::find()->orderBy('nama_bulk')->asArray()->all(), 'nama_bulk', 'nama_bulk'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Bulk'],
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 250px;font-size: 14px'],
                    'headerOptions' => ['style' => 'text-align: center;width: 100px;'],
                    // 'group'=>true,  // enable grouping
                    //'subGroupOf'=>1,
                ],
                [
                    'attribute' => 'sediaan',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(TrackingBatchOlah::find()->orderBy('sediaan')->asArray()->all(), 'sediaan', 'sediaan'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Sediaan'],
                    'contentOptions' => ['style' => ' text-align: center;width: 50px;'],
                    'headerOptions' => ['style' => 'text-align: center;width: 50px;'],
                ],
                [
                    'attribute' => 'snfg',
                    'label' => 'SNFG',
                    // 'format' => 'raw',
                    'contentOptions' => ['style' => ' text-align: center;width: 100px;'],
                    'headerOptions' => ['style' => 'text-align: center;width: 100px;'],
                ],
                [
                    'attribute' => 'nomo',
                    'label' => 'NOMO',
                    'contentOptions' => ['style' => ' text-align: center;width: 100px;'],
                    'headerOptions' => ['style' => 'text-align: center;width: 100px'],
                ],
                [
                    'attribute' => 'scan_start',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'nobatch',
                    'contentOptions' => ['style' => ' text-align: center;width: 50px;'],
                    'headerOptions' => ['style' => 'text-align: center;width: 50px'],
                ],
                [
                    'attribute' => 'nama_operator',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],


             ],
            
        ]);
    
    ?>
</div>
