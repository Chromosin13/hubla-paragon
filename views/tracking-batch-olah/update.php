<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TrackingBatchOlah */

$this->title = 'Update Tracking Batch Olah: ' . $model->id_pengolahan;
$this->params['breadcrumbs'][] = ['label' => 'Tracking Batch Olahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_pengolahan, 'url' => ['view', 'id' => $model->id_pengolahan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tracking-batch-olah-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
