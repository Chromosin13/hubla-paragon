<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TrackingBatchOlahSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tracking Batch Olahs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tracking-batch-olah-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tracking Batch Olah', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_pengolahan',
            'nama_fg',
            'snfg',
            'nomo',
            'scan_start',
            // 'nobatch',
            // 'nama_operator',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
