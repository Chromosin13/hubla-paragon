<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TrackingBatchOlah */

$this->title = 'Create Tracking Batch Olah';
$this->params['breadcrumbs'][] = ['label' => 'Tracking Batch Olahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tracking-batch-olah-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
