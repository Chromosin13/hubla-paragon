<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SisaKemas */

$this->title = 'Create Sisa Kemas';
$this->params['breadcrumbs'][] = ['label' => 'Sisa Kemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sisa-kemas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
