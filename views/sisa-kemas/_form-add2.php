<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SisaKemas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sisa-kemas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'snfg')->textInput(['value' => $snfg,'readOnly' => true]) ?>

    <?= $form->field($model, 'sisa_bulk')->textInput()->label('Sisa Bulk (kg)',['class'=>'label-class']) ?>

    <?= $form->field($model, 'sisa_packaging')->textInput()->label('Sisa Packaging (pcs)',['class'=>'label-class']) ?>

    <?= $form->field($model, 'packaging_reject')->textInput()->label('Packaging Reject (pcs)',['class'=>'label-class']) ?>

    <?= $form->field($model, 'finish_good_reject')->textInput()->label('Finish Good Reject (pcs)',['class'=>'label-class']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
