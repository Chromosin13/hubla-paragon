<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SisaKemasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sisa Kemas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sisa-kemas-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sisa Kemas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'snfg',
            'sisa_bulk',
            'sisa_packaging',
            'packaging_reject',
            'finish_good_reject',
            

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
