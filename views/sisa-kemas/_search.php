<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SisaKemasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sisa-kemas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'sisa_bulk') ?>

    <?= $form->field($model, 'sisa_packaging') ?>

    <?= $form->field($model, 'packaging_reject') ?>

    <?= $form->field($model, 'finish_good_reject') ?>

    <?php // echo $form->field($model, 'snfg') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
