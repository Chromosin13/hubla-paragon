<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SisaKemas */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="box box-widget widget-user-2">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-red">
      <div class="widget-user-image">
        <img class="img-circle" src="images/gears.png" alt="User Avatar">
      </div>
      <!-- /.widget-user-image -->
      <h2 class="widget-user-username">Sisa Kemas</h2>
      <h5 class="widget-user-desc">Scan SNFG</h5>
    </div>
    <div class="box-footer">

        <div class="sisa-kemas-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'snfg')->textInput(['value'=>$snfg,'readOnly'=>true]) ?>

            <?= $form->field($model, 'sisa_bulk')->textInput() ?>

            <?= $form->field($model, 'sisa_packaging')->textInput() ?>

            <?= $form->field($model, 'packaging_reject')->textInput() ?>

            <?= $form->field($model, 'finish_good_reject')->textInput() ?>

            

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    
    </div>
  </div>

  

