<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SisaKemas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sisa-kemas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sisa_bulk')->textInput() ?>

    <?= $form->field($model, 'sisa_packaging')->textInput() ?>

    <?= $form->field($model, 'packaging_reject')->textInput() ?>

    <?= $form->field($model, 'finish_good_reject')->textInput() ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
