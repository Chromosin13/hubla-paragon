<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SisaKemas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-widget widget-user-2">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-red">
      <div class="widget-user-image">
        <img class="img-circle" src="images/gears.png" alt="User Avatar">
      </div>
      <!-- /.widget-user-image -->
      <h2 class="widget-user-username">Sisa Kemas</h2>
      <h5 class="widget-user-desc">Scan SNFG</h5>
    </div>
    <div class="box-footer">


        <div class="form-group field-sisakemas-snfg has-success">
        <label class="control-label" for="sisakemas-snfg">Snfg</label>
        <input type="text" id="sisakemas-snfg" class="form-control" name="SisaKemas[snfg]" aria-invalid="false">

        <div class="help-block"></div>
        </div>

        <!-- <input type="button" id="submit" value="Add" class="btn btn-primary"> -->

    
    </div>
</div>



<?php
$script = <<< JS

$(document).ready(function(){

    // Disable Enter Key
    $(document).ready(function() {
    $(window).keydown(function(event){
        if(event.keyCode == 13) {

            var snfg = $('#sisakemas-snfg').val();

            // Returns successful data submission message when the entered information is stored in database.
            
            var dataString = 'snfg='+ snfg;
            
            if(snfg=='')
            {
                alert("Mohon isi SNFG");
            }
            else
            {
                
                // AJAX Code To Submit Form.
                
                $.ajax({
                  type: "POST",
                  url: "index.php?r=sisa-kemas/add-data",
                  cache: false,
                  data: dataString,
                  success: function(result){ alert(result) }
                });
                
            }   


          // event.preventDefault();
          // return false;
        }
      });
    });

    $("#submit").click(function(){
        
        

    });
});

JS;
$this->registerJs($script);
?>