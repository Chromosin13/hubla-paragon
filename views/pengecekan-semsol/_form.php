<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PengecekanSemsol */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengecekan-semsol-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'uji_ayun')->textInput() ?>

    <?= $form->field($model, 'uji_ulir')->textInput() ?>

    <?= $form->field($model, 'uji_ketrok')->textInput() ?>

    <?= $form->field($model, 'uji_oles')->textInput() ?>

    <?= $form->field($model, 'uji_sweating')->textInput() ?>

    <?= $form->field($model, 'pengecekan_warna')->textInput() ?>

    <?= $form->field($model, 'id_pengecekan_umum')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
