<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PengecekanSemsol */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pengecekan Semsols', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengecekan-semsol-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'uji_ayun',
            'uji_ulir',
            'uji_ketrok',
            'uji_oles',
            'uji_sweating',
            'pengecekan_warna',
            'id_pengecekan_umum',
        ],
    ]) ?>

</div>
