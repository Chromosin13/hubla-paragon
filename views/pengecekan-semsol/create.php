<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PengecekanSemsol */

$this->title = 'Create Pengecekan Semsol';
$this->params['breadcrumbs'][] = ['label' => 'Pengecekan Semsols', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengecekan-semsol-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
