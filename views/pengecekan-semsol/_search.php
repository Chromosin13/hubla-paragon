<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PengecekanSemsolSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengecekan-semsol-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'uji_ayun') ?>

    <?= $form->field($model, 'uji_ulir') ?>

    <?= $form->field($model, 'uji_ketrok') ?>

    <?= $form->field($model, 'uji_oles') ?>

    <?php // echo $form->field($model, 'uji_sweating') ?>

    <?php // echo $form->field($model, 'pengecekan_warna') ?>

    <?php // echo $form->field($model, 'id_pengecekan_umum') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
