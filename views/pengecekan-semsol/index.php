<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PengecekanSemsolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengecekan Semsols';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengecekan-semsol-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pengecekan Semsol', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'uji_ayun',
            'uji_ulir',
            'uji_ketrok',
            'uji_oles',
            // 'uji_sweating',
            // 'pengecekan_warna',
            // 'id_pengecekan_umum',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
