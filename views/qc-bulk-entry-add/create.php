<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\QcBulkEntryAdd */

$this->title = 'ISI PENAMBAHAN BAHAN BAKU ADJUST';
$this->params['breadcrumbs'][] = ['label' => 'Qc Bulk Entry Adds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qc-bulk-entry-add-create">


    <?= $this->render('_form', [
        'model' => $model,
        'nomo' => $nomo,
    ]) ?>

</div>
