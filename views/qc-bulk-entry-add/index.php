<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QcBulkEntryAddSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Qc Bulk Entry Adds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qc-bulk-entry-add-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Qc Bulk Entry Add', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'penambahan_bb_adjust',
            'nomo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
