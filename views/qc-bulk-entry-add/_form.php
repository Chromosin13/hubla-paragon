<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\QcBulkEntryAdd */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qc-bulk-entry-add-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomo')->textInput(['value'=>$nomo,'readOnly'=>true]) ?>

    <?= $form->field($model, 'penambahan_bb_adjust')->textInput()->label('Penambahan BB Adjust (Kg)') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
