<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Downtime */

$this->title = 'Create Downtime';
$this->params['breadcrumbs'][] = ['label' => 'Downtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="downtime-create">

    <?= $this->render('_form-penimbangan', [
        'model' => $model,
        'flow_input_mo_id' => $flow_input_mo_id,
    ]) ?>

</div>
