<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DowntimeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="downtime-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'flow_input_mo_id') ?>

    <?= $form->field($model, 'jenis') ?>

    <?= $form->field($model, 'posisi') ?>

    <?= $form->field($model, 'waktu_start') ?>

    <?= $form->field($model, 'waktu_stop') ?>

    <?php // echo $form->field($model, 'id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
