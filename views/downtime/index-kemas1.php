<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax; 
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DowntimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Downtimes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="downtime-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::button('Tambah Downtime', ['value'=>Url::to('index.php?r=downtime/create-kemas1&id='.$flow_input_snfgkomponen_id),'class' => 'btn btn-success','id'=>'modalButton','data-id'=>$flow_input_snfgkomponen_id]) ?>
    </p>

    <?php

        Modal::begin([
                'header'=>'<h4>Downtime</h4>',
                'id' => 'modal',
                'size' => 'modal-lg',
            ]);

        echo "<div id='modalContent'></div>";

        Modal::end();

    ?>


    <?php Pjax::begin(['id'=>'downtimeGrid']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'pjax' => true,
        'showPageSummary' => true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'flow_input_snfgkomponen_id',
            'jenis',
            'posisi',
            'waktu_start',
            'waktu_stop',
            [
              'attribute'=>'durasiMenit',
              'pageSummary'=>true
            ], 
            'keterangan',

            ['class' => 'kartik\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>


    <?= Html::a('Selesai', ['flow-input-snfgkomponen/check-kemas1'], ['class'=>'btn btn-danger']) ?>
</div>
