<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\web\JsExpression;
use wbraganca\tagsinput\TagsinputWidget;
use yii\helpers\Url;
use kartik\widgets\DateTimePicker;
use app\models\Downtime;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Downtime */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="downtime-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>


    <div class="row">
    
        <div class="col-md-6">

            <?= $form->field($model, 'flow_input_mo_id')->textInput(['readOnly'=>true,'value'=>$flow_input_mo_id]) ?>
        </div>

        <div class="col-md-6">
        
            <?= $form->field($model, 'posisi')->textInput(['readOnly'=>true,'value'=>'PENIMBANGAN']) ?>

        </div>
    </div>

    <div class="row">


        <div class="col-md-6">

            <?= $form->field($model, 'jenis')->dropDownList(['KENDALA' => 'KENDALA'],['prompt'=>'Select Option']); ?>
        </div>

        <div class="col-md-6">
        
            <?php
                echo $form->field($model, 'keterangan')->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(Downtime::find()->all()
                                    ,'keterangan','keterangan'),
                                    'options' => ['placeholder' => 'Select Keterangan'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
            ?>
        </div>
    </div>

    <div class="row">
        
        <div id="waktu_start_form" class="col-md-6">
            <?=
                $form->field($model, 'waktu_start')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Enter event time ...',
                                  'readOnly' => true
                                 ],
                        'pluginOptions' => [
                        'autoclose' => true,
                        'showMeridian' => false,
                        'minuteStep' => 5,
                        
                    ]
                ]);

            ?>
        </div>
        <div id="waktu_stop_form" class="col-md-6">
            <?=
                $form->field($model, 'waktu_stop')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Enter event time ...',
                                  'readOnly' => true
                                 ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'showMeridian' => false,
                        'minuteStep' => 5,
                        
                    ]
                ]);

            ?>
        </div>
    </div>


        <span class="badge bg-blue" id="durasi-downtime"></span>
    
    <br></br>

    <div id="create-button" class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$script = <<< JS

$('#downtime-jenis').change(function(){

    var jenis = $('#downtime-jenis').val();

    if(jenis=="KENDALA"){

        $.post("index.php?r=scm-planner/get-kendala-timbang-id&id="+$('#downtime-flow_input_mo_id').val(), function (data){

                    $("select#downtime-keterangan").html(data);

        });
    }

});

$('form#{$model->formName()}').on('beforeSubmit', function(e)
{
    var \$form = $(this);
     $.post(
        \$form.attr("action"), // serialize Yii2 form
        \$form.serialize()
     )

        .done(function(result){
         console.log(result);
         if(result == 1)
            {   
                $(document).find('#modal').modal('hide');
                // $(\$form).trigger("reset");

                $.pjax.reload({container:'#downtimeGrid'});
            }else{
                
                $("#message").html(result);
                alert('Kemas Id not found, Please refresh');
            } 
        })
        .fail(function()
        {
            console.log("server error");
        });
    return false;

});


$('#create-button').hide();


$('#downtime-waktu_start').change(function(){
    

    
    // Get Current Timestamp
    var now = new Date();

    // Current Date 
    var start_js = new Date($('#downtime-waktu_start').val());
    if(start_js<now){
        // Show Stop Form
        $('#waktu_stop_form').fadeIn("slow");
    }else{
        // Hide Stop Form
        $('#waktu_stop_form').hide();
        alert('Pemilihan waktu harus sebelum waktu saat ini!');
    }

});

$('#downtime-waktu_stop').change(function(){
    var start_js = new Date($('#downtime-waktu_start').val());
    var stop_js = new Date($('#downtime-waktu_stop').val());
    var keterangan = $('#downtime-keterangan').val();
    if(stop_js<=start_js){
        alert('Periksa kembali waktu, Waktu Stop Bulk tidak boleh sebelum Waktu Start Bulk!');
        $('#create-button').hide();
    }else{
        $('#durasi-downtime').html('Durasi '+keterangan+' : '+(parseFloat(stop_js-start_js)/parseFloat(60000))+' Menit atau '+(parseFloat(stop_js-start_js)/parseFloat(3600000)).toFixed(2)+' Jam' );
        $('#create-button').fadeIn("slow");
    };

});



JS;
$this->registerJs($script);
?>