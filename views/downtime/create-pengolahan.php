<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Downtime */

$this->title = 'Create Downtime';
$this->params['breadcrumbs'][] = ['label' => 'Downtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="downtime-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-pengolahan', [
        'model' => $model,
        'flow_input_mo_id' => $flow_input_mo_id,
    ]) ?>

</div>
