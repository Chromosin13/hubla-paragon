<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Downtime */

$this->title = 'Create Downtime';
$this->params['breadcrumbs'][] = ['label' => 'Downtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="downtime-create">


    <?= $this->render('_form-pra-kemas', [
        'model' => $model,
        'flow_pra_kemas_id' => $flow_pra_kemas_id,
    ]) ?>

</div>
