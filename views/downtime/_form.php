<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Downtime */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="downtime-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'flow_input_mo_id')->textInput() ?>

    <?= $form->field($model, 'jenis')->textInput() ?>

    <?= $form->field($model, 'posisi')->textInput() ?>

    <?= $form->field($model, 'waktu_start')->textInput() ?>

    <?= $form->field($model, 'waktu_stop')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
