<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax; 
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DowntimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Downtimes';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .zoom {
        padding: 50px;
        transition: transform .2s; /* Animation */
        width: 200px;
        height: 200px;
        margin: 0 auto;
    }

    .zoom:hover {
        transform: scale(1.2); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    }
</style>

<div class="downtime-index">
    
    <p>
        <?= Html::button('Tambah Downtime', ['value'=>Url::to('index.php?r=downtime/create-pengolahan&id='.$flow_input_mo_id),'class' => 'btn btn-success','id'=>'modalButton','data-id'=>$flow_input_mo_id]) ?>
    </p>

    <?php

        Modal::begin([
                'header'=>'<h4>Downtime</h4>',
                'id' => 'modal',
                'size' => 'modal-lg',
            ]);

        echo "<div id='modalContent'></div>";

        Modal::end();

    ?>


    <?php Pjax::begin(['id'=>'downtimeGrid']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'flow_input_mo_id',
            'jenis',
            'posisi',
            'waktu_start',
            'waktu_stop',
            'keterangan',
            'bahan_baku_adjust',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

    <?= Html::a('Selesai & Isi Informasi Batch', ['pengolahan-batch/index', 'id'=>$flow_input_mo_id], ['class'=>'btn btn-danger']) ?>
</div>
