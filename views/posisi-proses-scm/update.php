<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PosisiProsesScm */

$this->title = 'Update Posisi Proses Scm: ' . $model->snfg;
$this->params['breadcrumbs'][] = ['label' => 'Posisi Proses Scms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->snfg, 'url' => ['view', 'id' => $model->snfg]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="posisi-proses-scm-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
