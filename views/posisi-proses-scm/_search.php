<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PosisiProsesScmSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="posisi-proses-scm-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'koitem_fg') ?>

    <?= $form->field($model, 'nama_fg') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'start') ?>

    <?= $form->field($model, 'due') ?>

    <?php // echo $form->field($model, 'posisi') ?>

    <?php // echo $form->field($model, 'jumlah_pcs') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'lanjutan_split_batch') ?>

    <?php // echo $form->field($model, 'time') ?>

    <?php // echo $form->field($model, 'jumlah_aktual') ?>

    <?php // echo $form->field($model, 'hit_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
