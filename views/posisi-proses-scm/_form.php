<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PosisiProsesScm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="posisi-proses-scm-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'koitem_fg')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'start')->textInput() ?>

    <?= $form->field($model, 'due')->textInput() ?>

    <?= $form->field($model, 'posisi')->textInput() ?>

    <?= $form->field($model, 'jumlah_pcs')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'lanjutan_split_batch')->textInput() ?>

    <?= $form->field($model, 'time')->textInput() ?>

    <?= $form->field($model, 'jumlah_aktual')->textInput() ?>

    <?= $form->field($model, 'hit_status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
