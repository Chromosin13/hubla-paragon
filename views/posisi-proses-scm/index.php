<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PosisiProsesScmSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posisi Proses SCM';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posisi-proses-scm-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


        Download All Data Here
    <?php 

        $gridColumns = [
                'sediaan',
                'koitem_fg',
                'nama_fg',
                'snfg',
                'start',
                'due',
                'posisi',
                'jumlah_pcs',
                'status',
                'lanjutan_split_batch',
                'time',
                'jumlah_aktual',
                'hit_status',

        ];

        echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
  
    ]);?>

    <br />

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'floatHeader'=>'true',
        'floatHeaderOptions' => ['position' => 'absolute',],
        'showPageSummary'=>true,
        'pjax'=>true,
        'striped'=>true,
        'hover'=>true,
        'panel'=>['type'=>'primary', 'heading'=>'Dashboard SCM'],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'sediaan',
            'koitem_fg',
            'nama_fg',
            'snfg',
            'start',
            'due',
            'posisi',
            'jumlah_pcs',
            'status',
            'lanjutan_split_batch',
            'time',
            'jumlah_aktual',
            'hit_status',


            // ['class' => 'kartik\grid\ActionColumn', 'template' => '{}'],
        ],
    ]); ?>
</div>
