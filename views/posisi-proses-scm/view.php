<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PosisiProsesScm */

$this->title = $model->snfg;
$this->params['breadcrumbs'][] = ['label' => 'Posisi Proses Scms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posisi-proses-scm-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->snfg], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->snfg], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'koitem_fg',
            'nama_fg',
            'snfg',
            'start',
            'due',
            'posisi',
            'jumlah_pcs',
            'status',
            'lanjutan_split_batch',
            'time',
            'jumlah_aktual',
            'hit_status',
        ],
    ]) ?>

</div>
