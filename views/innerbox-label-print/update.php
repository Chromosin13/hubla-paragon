<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InnerboxLabelPrint */

$this->title = 'Update Innerbox Label Print: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Innerbox Label Prints', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="innerbox-label-print-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
