<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\InnerboxLabelPrint */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Innerbox Label Prints', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="innerbox-label-print-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'flow_input_snfg_id',
            'snfg',
            'timestamp:datetime',
            'no_batch',
            'exp_date',
            'nama_line',
            'qty_request',
            'qty_total',
            'nama_fg',
            'brand',
            'innerbox_qty',
        ],
    ]) ?>

</div>
