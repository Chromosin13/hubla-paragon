<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\Downtime;
use app\models\DowntimeSearch;
use kartik\date\DatePicker;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<script>
   
</script>

 <div class="box box-widget widget-user">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-blue-gradient">
        <h3 class="widget-user-username"><b>Penimbangan / Weighing</b></h3>
        <h5 class="widget-user-desc">Work Order</h5>
    </div>
    <div class="widget-user-image">
        <img class="img-circle" src="../web/images/scale.png" alt="User Avatar">
    </div>
    <div class="box-footer">
        <div class="row">
            <div class="box-body">
                <div class="box-body">
                    <div class="innerbox-label-print-form">

                        <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($model, 'snfg')->textInput(['value'=>$snfg]) ?>

                        <!-- FG Name from Odoo Warehouse Database --->
                        <?php

                            if($fg_name==""){
                                echo $form->field($model, 'nama_fg')->textInput(['placeholder'=>'Data was not found in Odoo Database, Please fill manually']);
                            } else {
                                echo $form->field($model, 'nama_fg')->textInput(['value'=>$fg_name]);
                            }
                        ?>

                        <!-- No Batch from FRO Database --->
                        <?php
                            if($nobatch=="-"){ 
                                echo $form->field($model, 'no_batch')->textInput(['placeholder'=>'Data tidak ditemukan, silahkan isi manual']);
                            } else {
                                echo $form->field($model, 'no_batch')->textInput(['value'=>$nobatch]);
                            }
                        ?>

                        <?php

                            if($exp_date=="" or $exp_date=="1999-09-09"){
                                echo DatePicker::widget([
                                        'model' => $model,
                                        'form' => $form,
                                        'name' => 'exp_date',
                                        'attribute' => 'exp_date', 
                                        //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                                        'pluginOptions' => [
                                            'format' => 'yyyy-mm-dd',
                                            'todayHighlight' => true
                                            ],
                                        ]);
                            } else {
                                if ($exp_date != ""){
                                    $model->exp_date = $exp_date;
                                }
                                
                                echo DatePicker::widget([
                                        'model' => $model,
                                        'form' => $form,
                                        'name' => 'exp_date',
                                        'attribute' => 'exp_date', 
                                        // 'disabled' => !empty($model->exp_date),
                                        'options' => ['readonly' => false],
                                        'pluginOptions' => [
                                            'format' => 'yyyy-mm-dd',
                                            'todayHighlight' => true
                                            ],
                                        ]);
                            }
                        ?>

                        <?php

                                    if(empty($nama_line)){
                                        echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(FlowInputMo::find()->all()
                                            ,'nama_line','nama_line'),
                                            'options' => ['placeholder' => 'Select Line'],
                                            'pluginOptions' => [
                                                'allowClear' => true 
                                            ],
                                        ]);
                                    }else{
                                        echo $form->field($model, 'nama_line')->textInput(['value'=>$nama_line]);
                                    }
                                ?>

                        <?= $form->field($model, 'operator')->widget(Select2::classname(), [
                                            'data' => $list_operator,
                                            'options' => ['placeholder' => 'Select Operator'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]) 
                        ?>

                        

                        <?= $form->field($model, 'barcode')->textInput(['value'=>$barcode]) ?>

                        <?php

                            if($na_number==""){
                                echo $form->field($model, 'na_number')->textInput(['placeholder'=>'Data tidak ditemukan, silahkan isi manual']);
                            } else {
                                echo $form->field($model, 'na_number')->textInput(['value'=>$na_number]); 
                            }
                        ?>

                        <?= $form->field($model, 'odoo_code')->textInput(['value'=>$kode_odoo]) ?>

                        <?php if ($qty!=0){ ?>
                            <?= $form->field($model, 'innerbox_qty')->textInput(['value'=>$qty]) ?>
                        <?php } else { ?>
                            <?= $form->field($model, 'innerbox_qty')->textInput(['placeholder' => 'Data tidak ditemukan, silahkan isi manual manually']) ?>
                        <?php }?>

                        <?= $form->field($model, 'qty_request')->textInput(['type' => 'number', 'maxlength' => true]) ?>

                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
</div>



                  


<?php
$script = <<< JS

    // document.getElementById('input_operator').addEventListener('input', function (e) {
    //   //e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1,').trim();
    //     e.target.value = e.target.value.toUpperCase();
    //     if (e.target.value.length % 5 == 0){
    //         //e.target.value = e.target.value.replace(/(.{4})/g, '$1').replace(/[ ,]+/g, ",");
    //         e.target.value = e.target.value.replace(/.$/,",");
    //     }
        
    // }); 

    // function forceInputUppercase(e)
    //   {
    //     var start = e.target.selectionStart;
    //     var end = e.target.selectionEnd;
    //     e.target.value = e.target.value.toUpperCase();
    //     e.target.setSelectionRange(start, end);
    //   }

    // document.getElementById("subcat-id").addEventListener("keyup", forceInputUppercase, false);

    $('#lanjutan').hide();

    var nomo = $('#flowinputmo-nomo').val();
    
    var posisi = $('#flowinputmo-posisi').val();

    // $.post("index.php?r=master-data-line/get-line-timbang-nomo&nomo="+$('#flowinputmo-nomo').val(), function (data){
    //     $("select#flowinputmo-nama_line").html(data);
    // });


    $('#flowinputmo-jenis_penimbangan').change(function(){

        var jenis_penimbangan = $('#flowinputmo-jenis_penimbangan').val();

        $.get('index.php?r=flow-input-mo/get-lanjutan-penimbangan',{ nomo : nomo , jenis_penimbangan : jenis_penimbangan , posisi : posisi  }, function(data){
            var data = $.parseJSON(data);
            $('#flowinputmo-lanjutan').attr('value',data.lanjutan);

        });

        $('#lanjutan').fadeIn();

    });  



JS;
$this->registerJs($script);
?>