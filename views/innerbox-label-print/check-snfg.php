<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-widget widget-user" style="border: 2px solid #EFEFEF; box-shadow: 2px 3px #9F9F9F;">
  <!-- Add the bg color to the header using any of the bg-* classes -->
  <div class="widget-user-header bg-black" style="background: url('../web/images/photo-white.jpg') center center; border: 2px solid #DFDFDF;">
    <h3 class="widget-user-username" style="color:#6F6F6F;"><b>Printer Station - Innerbox Label</b></h3>
    <h5 class="widget-user-desc" style="color:#6F6F6F;">Input Data</h5>
  </div>
  <div class="widget-user-image">
    <img class="img-circle" src="../web/images/printer.png" alt="User Avatar">
  </div>

  <div class="box-footer">
    <div class="row">
      <div class="box-body">
        <div class="box-body">
            <div class="innerbox-label-print-form">

              <div class="form-group field-flowinputsnfg-snfg has-success">
              <label class="control-label" for="flowinputsnfg-snfg">SNFG</label>
              <input type="text" id="flowinputsnfg-snfg" class="form-control" name="FlowInputSnfg[snfg]" aria-invalid="false">

              </div>
            </div>
        </div>
      </div>
    <!-- /.row -->
    </div>
  </div>

  <div class="row" style="margin-left:3px; margin-right:3px;">
    <div class="col-lg-3 col-xs-3">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3 id="isi_printer_status">-</h3>

          <p>Printer Status</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
        <a href="#" class="small-box-footer">
          <p> - </p>
        </a>
      </div>
    </div>
    <div class="col-lg-3 col-xs-3">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3 id="jumlah_today"><?php echo $qty_today ?></h3>

          <p>Jumlah label hari ini</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="#" class="small-box-footer">
          <p> - </p>
        </a>
      </div>
    </div>
    <!-- ./col -->
    <!-- ./col -->
    <div class="col-lg-3 col-xs-3">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3 id="jumlah_all"><?php echo $qty_total ?></h3>

          <p>Jumlah Label All Time</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="#" class="small-box-footer">
          <p>FE IP : <?php echo $frontend_ip?></p>
        </a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-3">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3 id="status_raspi">-</h3>

          <p id="deskripsi_raspi">Status Panel</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
        <a href="#" class="small-box-footer">
          <p>SBC IP : <?php echo $sbc_ip?></p>
        </a>
      </div>
    </div>
    <!-- ./col -->
  </div>

<!-- List Data -->

<div class="box box-widget widget-user">
  <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                PRINTING LIST
  </h4>

  <div class="box-footer">

    <div class="row">
      <div class="box-body">
           
              <div class="innerbox-label-print-form">



                 <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        
                        'snfg',
                        'no_batch',
                        'exp_date',
                        'nama_line',
                        'qty_request',
                        'qty_total',
                        'timestamp',
                        'status',
                    ],
                ]); ?>


              </div>
      </div>
    </div>
    <!-- /.row -->
  </div>
</div>


<?php
$script = <<< JS

  // AutoFocus SNFG Field

  document.getElementById("flowinputsnfg-snfg").focus();


  // Check SNFG Validity

  $('#flowinputsnfg-snfg').change(function(){  

      var snfg = $('#flowinputsnfg-snfg').val().trim();
      
      $.get('index.php?r=innerbox-label-print/check-snfg-exist',{ snfg : snfg },function(data){

        if(data==1){
          window.location = "index.php?r=innerbox-label-print/create-innerbox-label&snfg="+snfg;
          
        } else if (data==2){
          alert('Anda belum melakukan scan start jadwal!');
        } else {
          alert('SNFG Tidak Terdaftar');
        }
      });
       
  });

  var printer_status = setInterval(function() {

        $.getJSON("http://$sbc_ip:6900/",function(result){
        
            printer_status = result;
            // console.log(printer_status.printer);
            $("#status_raspi").text("Connected");
            $("#deskripsi_raspi").text("Status Panel");
            $("#isi_printer_status").text(printer_status.printer);
        }).error(function() {
            $("#deskripsi_raspi").text("Status Panel : Raspberry / Panel Mati, Hubungi Teknisi");
            $("#status_raspi").text("Disconnected");
            $("#isi_printer_status").text("Panel Unreachable");
          });

    }, 10000);



JS;
$this->registerJs($script);
?>