<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InnerboxLabelPrintSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="innerbox-label-print-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'flow_input_snfg_id') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'timestamp') ?>

    <?= $form->field($model, 'no_batch') ?>

    <?php // echo $form->field($model, 'exp_date') ?>

    <?php // echo $form->field($model, 'nama_line') ?>

    <?php // echo $form->field($model, 'qty_request') ?>

    <?php // echo $form->field($model, 'qty_total') ?>

    <?php // echo $form->field($model, 'nama_fg') ?>

    <?php // echo $form->field($model, 'brand') ?>

    <?php // echo $form->field($model, 'innerbox_qty') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
