<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\InnerboxLabelPrint */

$this->title = 'Create Innerbox Label Print';
$this->params['breadcrumbs'][] = ['label' => 'Innerbox Label Prints', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="innerbox-label-print-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
