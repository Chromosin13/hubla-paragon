<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InnerboxLabelPrintSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Innerbox Label Prints';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="innerbox-label-print-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Innerbox Label Print', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'flow_input_snfg_id',
            'snfg',
            'timestamp:datetime',
            'no_batch',
            // 'exp_date',
            // 'nama_line',
            // 'qty_request',
            // 'qty_total',
            // 'nama_fg',
            // 'brand',
            // 'innerbox_qty',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
