<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrackingSoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tracking-so-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'week') ?>

    <?= $form->field($model, 'sediaan') ?>

    <?= $form->field($model, 'streamline') ?>

    <?= $form->field($model, 'start') ?>

    <?php // echo $form->field($model, 'due') ?>

    <?php // echo $form->field($model, 'leadtime') ?>

    <?php // echo $form->field($model, 'nama_fg') ?>

    <?php // echo $form->field($model, 'tglpermintaan') ?>

    <?php // echo $form->field($model, 'mpq') ?>

    <?php // echo $form->field($model, 'qty_in_process') ?>

    <?php // echo $form->field($model, 'qty_karantina') ?>

    <?php // echo $form->field($model, 'qty_ndc') ?>

    <?php // echo $form->field($model, 'timestamp_complete') ?>

    <?php // echo $form->field($model, 'qty_total') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'timestamp') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
