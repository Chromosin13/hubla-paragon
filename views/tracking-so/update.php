<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TrackingSo */

$this->title = 'Update Tracking So: ' . $model->snfg;
$this->params['breadcrumbs'][] = ['label' => 'Tracking Sos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->snfg, 'url' => ['view', 'id' => $model->snfg]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tracking-so-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
