<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TrackingSo */

$this->title = 'Create Tracking So';
$this->params['breadcrumbs'][] = ['label' => 'Tracking Sos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tracking-so-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
