<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrackingSo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tracking-so-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'week')->textInput() ?>

    <?= $form->field($model, 'sediaan')->textInput() ?>

    <?= $form->field($model, 'streamline')->textInput() ?>

    <?= $form->field($model, 'start')->textInput() ?>

    <?= $form->field($model, 'due')->textInput() ?>

    <?= $form->field($model, 'leadtime')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'tglpermintaan')->textInput() ?>

    <?= $form->field($model, 'mpq')->textInput() ?>

    <?= $form->field($model, 'qty_in_process')->textInput() ?>

    <?= $form->field($model, 'qty_karantina')->textInput() ?>

    <?= $form->field($model, 'qty_ndc')->textInput() ?>

    <?= $form->field($model, 'timestamp_complete')->textInput() ?>

    <?= $form->field($model, 'qty_total')->textInput() ?>

    <?= $form->field($model, 'status')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'timestamp')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
