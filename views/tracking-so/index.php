<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use app\models\TrackingSo;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrackingSoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tracking SO';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="callout callout-success">
  <h4>Scope Data</h4>

  <p> Refresh Setiap 2 Menit </p>
  <p> 60 Hari (2 Bulan) Terakhir dari Tanggal Start Penimbangan Planner</p>
  <p> Jadwal yang berhenti di QC FG</p>
  <p> Kolom Status <b>Complete</b> berdasarkan IS DONE QC FG</p>
  <p> Kolom Status NDC <b>Complete</b> berdasarkan Palet Release yang sudah dikirim ke NDC</p>

</div>


<div class="tracking-so-index">

        Download All Data Here
    <?php 

        $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],
                'snfg',
                'nobatch',
                'week',
                'sediaan',
                'streamline',
                'start',
                'due',
                'leadtime',
                'nama_fg',
                'tglpermintaan',
                'mpq',
                'mpq_supply',
                'qty_in_process',
                'qty_karantina',
                'qty_ndc',
                'timestamp_complete',
                'qty_total',
                'status:ntext',
                'timestamp',
                'status_ndc:ntext',
                'qty_sample',
                'qty_reject',
                'qty_ndc_pending_mikro',
                'qty_ndc_release',
                'status_koli_karantina',
                'status_terima_karantina',
                'first_terima_karantina',
                'latest_terima_karantina',
        ['class' => 'kartik\grid\ActionColumn'],
        ];

        echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
  
    ]);?>
<p></p>
    <?php 

        // echo GridView::widget([
        //     'dataProvider' => $dataProvider,
        //     'filterModel' => $searchModel,
        //     'columns' => [
        //         ['class' => 'yii\grid\SerialColumn'],

        //         'snfg',
        //         'week',
        //         'sediaan',
        //         'streamline',
        //         'start',
        //         'due',
        //         'leadtime',
        //         'nama_fg',
        //         'tglpermintaan',
        //         'mpq',
        //         'qty_in_process',
        //         'qty_karantina',
        //         'qty_ndc',
        //         'timestamp_complete',
        //         'qty_total',
        //         'status:ntext',
        //         'timestamp',

        //         ['class' => 'yii\grid\ActionColumn'],
        //     ],
        // ]); 

    ?>

    <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'responsiveWrap' => false,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'danger', 'heading'=>'Tracking SO'],
            'formatter' => [
                'class' => 'yii\i18n\Formatter',
                'timeZone' => 'Asia/Jakarta'
            ],
            'toolbar' => [
            ],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                [
                    'attribute' => 'koitem_fg',
                    'label'=>'Image',
                    'format' => 'html',    
                    'value' => function ($data) {
                        return Html::img(Yii::getAlias('@web').'/images/products/'. $data['koitem_fg'].'.jpg',
                            ['width' => '50px']);
                    },
                ],
                [
                    'attribute'=>'nama_fg',
                    'label' => 'Finished Good', 
                    'width'=>'350px',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(TrackingSo::find()->orderBy('nama_fg')->asArray()->all(), 'nama_fg', 'nama_fg'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'SKU'],
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 400px;font-size: 12px'],
                    // 'group'=>true,  // enable grouping
                    //'subGroupOf'=>1,
                ],
                [
                    'attribute' => 'snfg',
                    'label' => 'SNFG',
                    'format' => 'raw',
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 250px;font-size: 12px','class'=>'info'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                ],
                [
                    'attribute' => 'nobatch',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'start',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'due',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'mpq',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'mpq_supply',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'qty_in_process',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'qty_karantina',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'qty_ndc',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'qty_ndc_pending_mikro',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'qty_ndc_release',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'timestamp_complete',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'qty_total',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'status',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'timestamp',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'status_ndc',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'qty_sample',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'qty_reject',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'first_terima_karantina',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'latest_terima_karantina',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'status_terima_karantina',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'status_koli_karantina',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],


             ],
            
        ]);
    
    ?>
</div>
