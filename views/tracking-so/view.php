<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TrackingSo */

$this->title = $model->snfg;
$this->params['breadcrumbs'][] = ['label' => 'Tracking Sos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tracking-so-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->snfg], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->snfg], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'snfg',
            'week',
            'sediaan',
            'streamline',
            'start',
            'due',
            'leadtime',
            'nama_fg',
            'tglpermintaan',
            'mpq',
            'qty_in_process',
            'qty_karantina',
            'qty_ndc',
            'timestamp_complete',
            'qty_total',
            'status:ntext',
            'timestamp',
        ],
    ]) ?>

</div>
