<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SamplerQcFg */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sampler Qc Fgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sampler-qc-fg-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'jumlah_kedatangan_batch',
            'aql_kedatangan',
            'pengambilan_sampel',
            'temuan_defect:ntext',
            'kategori_aql',
            'datetime_start',
            'datetime_stop',
            'datetime_write',
            'nopo',
            'no_surjal',
            'koitem_fg',
            'nama_fg',
            'supplier',
            'batch',
            'penempatan',
            'sampler_id',
            'koordinat_penempatan',
            'status_analis',
            'priority',
            'kategori',
            'date_penerimaanlog',
            'sop_npd',
            'jumlah_palet',
            'flag_analis',
        ],
    ]) ?>

</div>
