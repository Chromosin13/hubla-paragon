<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\SamplerQcFg;
use app\models\Packaging;
// use app\models\Kategori;
// use app\models\JumlahSamplingAql;
// use app\models\KategoriAql;
// use app\models\Priority;
use app\models\PenerimaanlogFg;
// use app\models\OperatorLogistik;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\dialog\Dialog;
use yii\web\JsExpression;
use kartik\date\DatePicker;
use kartik\widgets\DateTimePicker;
use kartik\file\FileInput;
use yii\web\UploadedFile;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $modelCustomer app\modules\yii2extensions\models\Customer */
/* @var $modelsSamplerQcFg app\modules\yii2extensions\models\Addres */

?>

<style>
    .bootstrap-tagsinput {
        /*padding: 50px;*/
        width: 1920px;
        height: auto;
    }

    .zoom {
        /*padding: 50px;*/
        transition: transform .3s; /* Animation */
        width: auto;
        height: auto;
        margin: 0 auto;
    }

    .zoom:hover {
        transform: scale(0.99); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    }

    @import url(https://fonts.googleapis.com/css?family=Lato:900);
    *, *:before, *:after{
      box-sizing:border-box;
    }
    body{
      font-family: 'Lato', sans-serif;
        ;
    }
    div.foo{
      width: 90%;
      margin: 0 auto;
      text-align: center;
    }
    .letter{
      display: inline-block;
      font-weight: 900;
      font-size: 4em;
      margin: 0.2em;
      position: relative;
      color: #00B4F1;
      transform-style: preserve-3d;
      perspective: 400;
      z-index: 1;
    }
    .letter:before, .letter:after{
      position:absolute;
      content: attr(data-letter);
      transform-origin: top left;
      top:0;
      left:0;
    }
    .letter, .letter:before, .letter:after{
      transition: all 0.3s ease-in-out;
    }
    .letter:before{
      color: #fff;
      text-shadow:
        -1px 0px 1px rgba(255,255,255,.8),
        1px 0px 1px rgba(0,0,0,.8);
      z-index: 3;
      transform:
        rotateX(0deg)
        rotateY(-15deg)
        rotateZ(0deg);
    }
    .letter:after{
      color: rgba(0,0,0,.11);
      z-index:2;
      transform:
        scale(1.08,1)
        rotateX(0deg)
        rotateY(0deg)
        rotateZ(0deg)
        skew(0deg,1deg);
    }
    .letter:hover:before{
      color: #fafafa;
      transform:
        rotateX(0deg)
        rotateY(-40deg)
        rotateZ(0deg);
    }
    .letter:hover:after{
      transform:
        scale(1.08,1)
        rotateX(0deg)
        rotateY(40deg)
        rotateZ(0deg)
        skew(0deg,22deg);
</style>


<div class="box box-widget widget-user">
    <div class="widget-user-header bg-yellow-gradient">
      <h3 class="widget-user-username"><b>Membuat Form</b></h3>
      <h5 class="widget-user-desc">Sampler QC</h5>
    </div>
    <div class="widget-user-image">
      <img class="img-circle" src="../web/images/gambarlog/question.png" alt="User Avatar">
    </div>
    <br></br>
  <div class="box-body">

    <div class="nama-samplerqc-form">

        <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

        	<?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>


        <?php DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 5, // the maximum times, an element can be added (default 999)
            'min' => 0, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $modelsSamplerQcFg[0],
            'formId' => 'dynamic-form',
            'formFields' => [
              'jumlah_kedatangan_per_po',
	            'jumlah_kedatangan_batch',
	            'aql_kedatangan',
	            'pengambilan_sampel',
	            'kategori_aql',
	            'temuan_defect',
	            'nopo',
	            'no_surjal',
	            'koitem_fg',
	            'nama_fg',
	            'supplier',
	            'batch',
	            'penempatan',
	            'koordinat_penempatan',
              'priority'

            ],
        ]); ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <i class="glyphicon glyphicon-envelope"></i> Sampler QC
                    <button type="button" class="add-item btn btn-success btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> ADD</button>
                </h4>
            </div>
            <div class="panel-body">
                <div class="container-items"><!-- widgetBody -->
                <?php foreach ($modelsSamplerQcFg as $i => $modelSamplerQcFg): ?>
                    <div class="item panel panel-default"><!-- widgetItem -->
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">Form Sampler</h3>
                            <div class="pull-right">
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                                // necessary for update action.
                                if (! $modelSamplerQcFg->isNewRecord) {
                                    echo Html::activeHiddenInput($modelSamplerQcFg, "[{$i}]id");
                                }
                            ?>

                            		<div class="row">

                                        <div class="col-md-4">
                                          <?php
                                              echo $form->field($modelSamplerQcFg, "[{$i}]nopo")->textInput(['readOnly'=>true]) ?>
                                        </div>
                                        <div class="col-md-4">
                                          <?php echo $form->field($modelSamplerQcFg, "[{$i}]koitem_fg")->textInput(['readOnly'=>true]) ?>
                                        </div>
                                        <div class="col-md-4">
                                          <?= $form->field($modelSamplerQcFg, "[{$i}]nama_fg")->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-4">
                                          <?= $form->field($modelSamplerQcFg, "[{$i}]no_surjal")->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>

                                        <div class="col-md-4">
                                           <?= $form->field($modelSamplerQcFg, "[{$i}]supplier")->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-4">
                                          <?= $form->field($modelSamplerQcFg, "[{$i}]batch")->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-4">
                                          <?= $form->field($modelSamplerQcFg, "[{$i}]koordinat_penempatan")->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-4">
                                          <?= $form->field($modelSamplerQcFg, "[{$i}]penempatan")->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-4">
                                            <?= $form->field($modelSamplerQcFg, "[{$i}]jumlah_kedatangan_batch")->textInput() ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>


                                    <div class="row">
                                      <div class="col-md-4">
                                          <?php

                                              $array = Yii::$app->db->createCommand("

                                              SELECT id,kategori_aql
                                              FROM kategori_aql
                                              ORDER BY id asc

                                              ")->queryAll();

                                              // $kode1=KategoriAql::findBySql($sql23)->all();

                                              //use yii\helpers\ArrayHelper;
                                              $listData1=ArrayHelper::map($array,'kategori_aql','kategori_aql');

                                              echo $form->field($modelSamplerQcFg, "[{$i}]kategori_aql")->dropDownList(
                                              $listData1,
                                              ['prompt'=>'Select...']
                                              );

                                          ?>

                                          <!-- /input-group -->
                                        </div>

                                        <div class="col-md-4">
                                          <?php
                                              $array2 =Yii::$app->db->createCommand("

                                              SELECT id,jumlah_sampling_aql
                                              FROM jumlah_sampling_aql
                                              ORDER BY id asc

                                              ")->queryAll();
                                              // $kode=JumlahSamplingAql::findBySql($sql22)->all();

                                              //use yii\helpers\ArrayHelper;
                                              $listData=ArrayHelper::map($array2,'jumlah_sampling_aql','jumlah_sampling_aql');

                                              echo $form->field($modelSamplerQcFg, "[{$i}]aql_kedatangan")->dropDownList(
                                              $listData,
                                              ['prompt'=>'Select...']
                                              );

                                          ?>

                                          <!-- /input-group -->
                                        </div>

                                        <div class="col-md-4">
                                            <?= $form->field($modelSamplerQcFg, "[{$i}]pengambilan_sampel")->textInput() ?>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <?php
                                                $array3 =Yii::$app->db->createCommand("

                                                SELECT id,kategori
                                                FROM kategori
                                                ORDER BY id asc

                                                ")->queryAll();
                                                // $kode7=Kategori::findBySql($sql28)->all();

                                                //use yii\helpers\ArrayHelper;
                                                $listData7=ArrayHelper::map($array3,'kategori','kategori');

                                                echo $form->field($modelSamplerQcFg, "[{$i}]kategori")->dropDownList(
                                                $listData7,
                                                ['prompt'=>'Select...']
                                                );

                                            ?>
                                        </div>
                                        <div class="col-md-4">
                                           <?php


                                              $array4 =Yii::$app->db->createCommand("

                                              SELECT id,priority
                                              FROM priority
                                              ORDER BY id asc

                                              ")->queryAll();

                                              // $kode2=Priority::findBySql($sql27)->all();

                                              //use yii\helpers\ArrayHelper;
                                              $listData2=ArrayHelper::map($array4,'priority','priority');

                                              echo $form->field($modelSamplerQcFg, "[{$i}]priority")->dropDownList(
                                              $listData2,
                                              ['prompt'=>'Select...']
                                              );

                                            ?>
                                        </div>
                                        <div class="col-md-4">
                                          <?= $form->field($modelSamplerQcFg, "[{$i}]temuan_defect")->textarea(['rows' => 6]) ?>
                                          <!-- /input-group -->
                                        </div>

                                    </div>





                          </div>
                      <?php endforeach; ?>
                      </div>
                  </div>
              </div><!-- .panel -->
              <?php DynamicFormWidget::end(); ?>

        <div class="form-group">
            <?= Html::submitButton($modelSamplerQcFg->isNewRecord ? 'CREATE' : 'Create', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
  </div>
</div>

<?php
$script = <<< JS

//$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
      	$('select[name^="SamplerQcFg["]').change(function() {

		        var prefix = $(this).attr('id').split("-");

		        prefix.splice(-1, 1);
		        var sup_id = '#' + prefix.join('-') + '-supplier';
		        var item_code_id = '#' + prefix.join('-') + '-koitem_fg';
		        var item_name_id = '#' + prefix.join('-') + '-nama_fg';
		        var surjal_id = '#' + prefix.join('-') + '-no_surjal';
		        var batch_id = '#' + prefix.join('-') + '-batch';
		        var penempatan_id = '#' + prefix.join('-') + '-penempatan';
		        var koordinat_id = '#' + prefix.join('-') + '-koordinat_penempatan';
		        var nopo = $(this).val();





		          $.get('index.php?r=sampler-qc-fg/get-po',{ nopo : nopo }, function(data){
		            var data = $.parseJSON(data);

		              // $(item_code_id).empty().append($('<option>', {
		              //     value:'',
		              //     text:'Select...'
		              // }));

		              $.each(data, function(idx, value) {
		                $(item_code_id).append($('<option>', {
		                  value: value['koitem_fg'],
		                  text: value['koitem_fg']
		                }));
		              });
		          });
	      	});

			    $('select[name^="SamplerQcFg["]').change(function() {
			      var prefix = $(this).attr('id').split("-");
			      prefix.splice(-1, 1);
			      var sup_id = '#' + prefix.join('-') + '-supplier';
			      var item_code_id = '#' + prefix.join('-') + '-koitem_fg';
			      var item_name_id = '#' + prefix.join('-') + '-nama_fg';
			      var surjal_id = '#' + prefix.join('-') + '-no_surjal';
			      var batch_id = '#' + prefix.join('-') + '-batch';
			      var penempatan_id = '#' + prefix.join('-') + '-penempatan';
		        var koordinat_id = '#' + prefix.join('-') + '-koordinat_penempatan';
            var jumlah_id = '#' + prefix.join('-') + '-jumlah_kedatangan_batch';
			      var nopo = $(this).val();

			      $(item_code_id).change(function(){

			        var koitem_fg = $(this).val();
			          $.get('index.php?r=sampler-qc-fg/get-po',{ nopo: nopo, koitem_fg : koitem_fg }, function(data){
			            var data = $.parseJSON(data);

			            $.each(data, function(idx, value) {
			              $(sup_id).val(value['supplier']);
			              $(item_name_id).val(value['nama_fg']);
			              $(surjal_id).val(value['no_surjal']);
			              $(batch_id).val(value['batch']);
			              $(penempatan_id).val(value['penempatan']);
			              $(koordinat_id).val(value['koordinat_penempatan']);
                    $(jumlah_id).val(value['jumlah']);
			            });
			          });
			      	});
				});
//});

$(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
    console.log("beforeInsert");
});

$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    console.log("afterInsert");
});

$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
    if (! confirm("Are you sure you want to delete this item?")) {
        return false;
    }
    return true;
});

$(".dynamicform_wrapper").on("afterDelete", function(e) {
    console.log("Deleted item!");
});

$(".dynamicform_wrapper").on("limitReached", function(e, item) {
    alert("Limit reached");
});


jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title-sampler-qc-fg").each(function(index) {
        jQuery(this).html("SamplerQcFg: " + (index + 1))
    });
});

jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title-sampler-qc-fg").each(function(index) {
        jQuery(this).html("SamplerQcFg: " + (index + 1))
    });
});


JS;
$this->registerJs($script);
?>
