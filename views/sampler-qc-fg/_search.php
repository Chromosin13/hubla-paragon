<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SamplerQcFgSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sampler-qc-fg-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'jumlah_kedatangan_batch') ?>

    <?= $form->field($model, 'aql_kedatangan') ?>

    <?= $form->field($model, 'pengambilan_sampel') ?>

    <?= $form->field($model, 'temuan_defect') ?>

    <?php // echo $form->field($model, 'kategori_aql') ?>

    <?php // echo $form->field($model, 'datetime_start') ?>

    <?php // echo $form->field($model, 'datetime_stop') ?>

    <?php // echo $form->field($model, 'datetime_write') ?>

    <?php // echo $form->field($model, 'nopo') ?>

    <?php // echo $form->field($model, 'no_surjal') ?>

    <?php // echo $form->field($model, 'koitem_fg') ?>

    <?php // echo $form->field($model, 'nama_fg') ?>

    <?php // echo $form->field($model, 'supplier') ?>

    <?php // echo $form->field($model, 'batch') ?>

    <?php // echo $form->field($model, 'penempatan') ?>

    <?php // echo $form->field($model, 'sampler_id') ?>

    <?php // echo $form->field($model, 'koordinat_penempatan') ?>

    <?php // echo $form->field($model, 'status_analis') ?>

    <?php // echo $form->field($model, 'priority') ?>

    <?php // echo $form->field($model, 'kategori') ?>

    <?php // echo $form->field($model, 'date_penerimaanlog') ?>

    <?php // echo $form->field($model, 'sop_npd') ?>

    <?php // echo $form->field($model, 'jumlah_palet') ?>

    <?php // echo $form->field($model, 'flag_analis') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
