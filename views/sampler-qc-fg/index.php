<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\SamplerQcFg;
use wbraganca\tagsinput\TagsinputWidget;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FormKendaliTugasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<style>
    .bootstrap-tagsinput {
        /*padding: 50px;*/
        width: 1920px;
        height: auto;
    }

    .zoom {
        /*padding: 50px;*/
        transition: transform .3s; /* Animation */
        width: auto;
        height: auto;
        margin: 0 auto;
    }

    .zoom:hover {
        transform: scale(0.99); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    }

    @import url(https://fonts.googleapis.com/css?family=Lato:900);
    *, *:before, *:after{
      box-sizing:border-box;
    }
    body{
      font-family: 'Lato', sans-serif;
        ;
    }
    div.foo{
      width: 90%;
      margin: 0 auto;
      text-align: center;
    }
    .letter{
      display: inline-block;
      font-weight: 900;
      font-size: 4em;
      margin: 0.2em;
      position: relative;
      color: #00B4F1;
      transform-style: preserve-3d;
      perspective: 400;
      z-index: 1;
    }
    .letter:before, .letter:after{
      position:absolute;
      content: attr(data-letter);
      transform-origin: top left;
      top:0;
      left:0;
    }
    .letter, .letter:before, .letter:after{
      transition: all 0.3s ease-in-out;
    }
    .letter:before{
      color: #fff;
      text-shadow:
        -1px 0px 1px rgba(255,255,255,.8),
        1px 0px 1px rgba(0,0,0,.8);
      z-index: 3;
      transform:
        rotateX(0deg)
        rotateY(-15deg)
        rotateZ(0deg);
    }
    .letter:after{
      color: rgba(0,0,0,.11);
      z-index:2;
      transform:
        scale(1.08,1)
        rotateX(0deg)
        rotateY(0deg)
        rotateZ(0deg)
        skew(0deg,1deg);
    }
    .letter:hover:before{
      color: #fafafa;
      transform:
        rotateX(0deg)
        rotateY(-40deg)
        rotateZ(0deg);
    }
    .letter:hover:after{
      transform:
        scale(1.08,1)
        rotateX(0deg)
        rotateY(40deg)
        rotateZ(0deg)
        skew(0deg,22deg);
</style>

<div class="box box-widget widget-user">
    <div class="widget-user-header bg-yellow-gradient">
      <h3 class="widget-user-username"><b>Form Kendali</b></h3>
      <h4 class="widget-user-desc">Sampler QC</h4>
    </div>
    <div class="widget-user-image">
      <a href="/packaging/web/index.php?r=analis-qc/index-print"><img class="img-circle" src="../web/images/gambarlog/leader_2.png" alt="User Avatar" style="width:65%"></a>
    </div>
    <br></br>


    <div class="box-body">
        <div class="zoom">
            <?=Html::a('SCAN WITH QR', ['sampler-qc-fg/check-sampler-qr'], ['class' => 'btn btn-danger btn-block']);?>
        </div>
        <p></p>
        <div class="sampler-qc-fg-form">

            <div class="form-group field-samplerqcfg-nopo has-success">
            <label class="control-label" for="samplerqcfg-nopo">No PO</label>
            <input type="text" id="samplerqcfg-nopo" class="form-control" name="samplerqcfg[nopo]" aria-invalid="false">

            <div class="help-block"></div>
            </div>

        </div>


        <?php
        yii\bootstrap\Modal::begin([

                'header' => 'Check Analis QC',

                'id'=>'editModalId',

                'class' =>'modal',

                'size' => 'modal-lg',

            ]);

            echo "<div class='modalContent'></div>";

            yii\bootstrap\Modal::end();


                $this->registerJs(

                "$(document).on('ready pjax:success', function() {

                        $('.modalButton').click(function(e){

                           e.preventDefault(); //for prevent default behavior of <a> tag.

                           var tagname = $(this)[0].tagName;

                           $('#editModalId').modal('show').find('.modalContent').load($(this).attr('href'));

                       });

                    });

                ");


                // JS: Update response handling

                $this->registerJs(

                'jQuery(document).ready(function($){

                    $(document).ready(function () {

                        $("body").on("beforeSubmit", "form#lesson-learned-form-id", function () {

                            var form = $(this);

                            // return false if form still have some validation errors

                            if (form.find(".has-error").length) {

                                return false;

                            }

                            var postData = new FormData($("#lesson-learned-form-id")[0]);

                            // submit form

                            $.ajax({

                                url    : form.attr("action"),

                                type   : "post",

                                // data   : form.serialize(),

                                processData: false,
                                contentType: false,
                                data : postData,

                                success: function (response) {

                                    $("#editModalId").modal("toggle");

                                    $.pjax.reload({container:"#lessons-grid-container-id"}); //for pjax update

                                },

                                error  : function () {

                                    console.log("internal server error");

                                }

                            });

                            return false;

                         });

                        });

                    });'

                );
    ?>


    <?php Pjax::begin(['id'=>'lessons-grid-container-id']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'floatHeader'=>'true',
        'floatHeaderOptions' => ['position' => 'absolute',],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            ['class' => 'kartik\grid\ActionColumn', 'template'=>'{custom_view}',

                'buttons' =>

                [   'custom_view' => function ($url, $model) {
                            return Html::a( '<i class="glyphicon glyphicon-tasks"></i>',

                                ['sampler-qc-fg/update-analis1', 'id'=>$model['id']],

                                ['class' => 'btn btn-warning modalButton', 'title'=>'view/edit', ]

                            );


                    },

                ]

            ],
            'kategori',
            'jumlah_kedatangan_batch',
            'kategori_aql',
            'aql_kedatangan',
            'pengambilan_sampel',
            'temuan_defect',
            'nopo' ,
            'no_surjal',
            'koitem_fg',
            'nama_fg',
            'supplier',
            'batch',
            'penempatan',
            'koordinat_penempatan',
            'datetime_start',
            'datetime_stop',
            'datetime_write',
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>
</div>

<?php
$script = <<< JS

$(function () {
    $('.update-modal-click').click(function () {
        $('#update-modal')
            .modal('show')
            .find('#updateModalContent')
            .load($(this).attr('value'));
    });
});

// AutoFocus Nomo Field

document.getElementById("samplerqcfg-nopo").focus();

// Check SNFG Komponen Validity

$('#samplerqcfg-nopo').change(function(){

    var nopo = $('#samplerqcfg-nopo').val();

    $.get('index.php?r=adminlog-fg/check-nopo',{ nopo : nopo },function(data){
        var data = $.parseJSON(data);
        if(data.id>=1){

          window.location = "index.php?r=sampler-qc-fg/update-item&nopo="+nopo;//"index.php?r=nama-samplerqcfg/create&nopo="+nopo;//sampler-qc-fg/create-sampler-qc&koitem_fg="+koitem_fg;

        }
        else{

          alert('Nomor PO Tidak Ditemukan');
        }
    });
});


JS;
$this->registerJs($script);
