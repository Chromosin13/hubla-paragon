<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SamplerQcFg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sampler-qc-fg-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jumlah_kedatangan_batch')->textInput() ?>

    <?= $form->field($model, 'aql_kedatangan')->textInput() ?>

    <?= $form->field($model, 'pengambilan_sampel')->textInput() ?>

    <?= $form->field($model, 'temuan_defect')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'kategori_aql')->textInput() ?>

    <?= $form->field($model, 'datetime_start')->textInput() ?>

    <?= $form->field($model, 'datetime_stop')->textInput() ?>

    <?= $form->field($model, 'datetime_write')->textInput() ?>

    <?= $form->field($model, 'nopo')->textInput() ?>

    <?= $form->field($model, 'no_surjal')->textInput() ?>

    <?= $form->field($model, 'koitem_fg')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'supplier')->textInput() ?>

    <?= $form->field($model, 'batch')->textInput() ?>

    <?= $form->field($model, 'penempatan')->textInput() ?>

    <?= $form->field($model, 'sampler_id')->textInput() ?>

    <?= $form->field($model, 'koordinat_penempatan')->textInput() ?>

    <?= $form->field($model, 'status_analis')->textInput() ?>

    <?= $form->field($model, 'priority')->textInput() ?>

    <?= $form->field($model, 'kategori')->textInput() ?>

    <?= $form->field($model, 'date_penerimaanlog')->textInput() ?>

    <?= $form->field($model, 'sop_npd')->textInput() ?>

    <?= $form->field($model, 'jumlah_palet')->textInput() ?>

    <?= $form->field($model, 'flag_analis')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
