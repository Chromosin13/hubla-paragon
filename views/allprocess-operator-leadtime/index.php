<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\export\ExportMenu;
use kartik\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\AllprocessOperatorLeadtimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Allprocess Operator Leadtimes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="allprocess-operator-leadtime-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'row_number',
            'posisi:ntext',
            'snfg',
            'snfg_komponen',
            'jenis_proses',
            'lanjutan',
            'nama:ntext',
            'tanggal',
            'leadtime',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?> -->
    <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'primary', 'heading'=>'All Process Operator Leadtime'],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                [
                    'attribute'=>'nama', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                ],
                [

                    'attribute'=>'posisi', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>1,
                ],
                [
                    'attribute'=>'snfg', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>2,
                ],
                [
                    'attribute'=>'snfg_komponen', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>3,
                ],
                'tanggal',
                //'snfg_komponen',
                'jenis_proses',
                'lanjutan',
                //'nama',
                [
                    'attribute'=>'leadtime', 
                    'width'=>'50px',
                    'pageSummary'=>true,
                ],
             ],
        ]);
    
    ?>

    <?php 
        $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
                'row_number',
                'posisi:ntext',
                'snfg',
                'snfg_komponen',
                'jenis_proses',
                'lanjutan',
                'nama:ntext',
                'tanggal',
                'leadtime',
        ['class' => 'yii\grid\ActionColumn'],
        ];

        echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_PDF => false,
                                ExportMenu::FORMAT_HTML => false
                          ]        
    ]);?>

</div>
