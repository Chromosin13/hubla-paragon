<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AllprocessOperatorLeadtime */

$this->title = 'Create Allprocess Operator Leadtime';
$this->params['breadcrumbs'][] = ['label' => 'Allprocess Operator Leadtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="allprocess-operator-leadtime-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
