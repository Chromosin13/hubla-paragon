<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AllprocessOperatorLeadtime */

$this->title = $model->row_number;
$this->params['breadcrumbs'][] = ['label' => 'Allprocess Operator Leadtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="allprocess-operator-leadtime-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->row_number], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->row_number], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'row_number',
            'posisi:ntext',
            'snfg',
            'snfg_komponen',
            'jenis_proses',
            'lanjutan',
            'nama:ntext',
            'tanggal',
            'leadtime',
        ],
    ]) ?>

</div>
