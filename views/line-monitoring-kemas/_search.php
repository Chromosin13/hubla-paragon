<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LineMonitoringKemasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="line-monitoring-kemas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'proses') ?>

    <?= $form->field($model, 'nama_line') ?>

    <?= $form->field($model, 'nojadwal') ?>

    <?= $form->field($model, 'waktu') ?>

    <?= $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'lanjutan') ?>

    <?php // echo $form->field($model, 'jenis_proses') ?>

    <?php // echo $form->field($model, 'tipe_jadwal') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
