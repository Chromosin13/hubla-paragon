<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\LineMonitoringKemas;
/* @var $this yii\web\View */
/* @var $searchModel app\models\LineMonitoringKemasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Line Monitoring Kemas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="line-monitoring-kemas-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'proses:ntext',
            [
                'attribute'=>'nama_line',
                'filter'=>ArrayHelper::map(LineMonitoringKemas::find()->asArray()->all(), 'nama_line', 'nama_line'),
            ],
            'nojadwal',
            'waktu',
            [
                    'attribute' => 'state',
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'contentOptions' => function($model){
                             if($model->state == 'STOP')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                                                }
                            else if($model->state == 'START') 
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'success'];
                                                }
                            else                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'default'];
                                                }
                    },
            ],
            'lanjutan',
            'jenis_proses',
            'tipe_jadwal:ntext',

        ],
    ]); ?>
</div>
