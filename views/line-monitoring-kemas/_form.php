<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LineMonitoringKemas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="line-monitoring-kemas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'proses')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'nama_line')->textInput() ?>

    <?= $form->field($model, 'nojadwal')->textInput() ?>

    <?= $form->field($model, 'waktu')->textInput() ?>

    <?= $form->field($model, 'state')->textInput() ?>

    <?= $form->field($model, 'lanjutan')->textInput() ?>

    <?= $form->field($model, 'jenis_proses')->textInput() ?>

    <?= $form->field($model, 'tipe_jadwal')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
