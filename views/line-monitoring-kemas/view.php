<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LineMonitoringKemas */

$this->title = $model->nama_line;
$this->params['breadcrumbs'][] = ['label' => 'Line Monitoring Kemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="line-monitoring-kemas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->nama_line], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->nama_line], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'proses:ntext',
            'nama_line',
            'nojadwal',
            'waktu',
            'state',
            'lanjutan',
            'jenis_proses',
            'tipe_jadwal:ntext',
        ],
    ]) ?>

</div>
