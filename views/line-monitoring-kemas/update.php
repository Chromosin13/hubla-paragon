<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LineMonitoringKemas */

$this->title = 'Update Line Monitoring Kemas: ' . $model->nama_line;
$this->params['breadcrumbs'][] = ['label' => 'Line Monitoring Kemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama_line, 'url' => ['view', 'id' => $model->nama_line]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="line-monitoring-kemas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
