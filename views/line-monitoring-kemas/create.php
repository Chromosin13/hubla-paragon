<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LineMonitoringKemas */

$this->title = 'Create Line Monitoring Kemas';
$this->params['breadcrumbs'][] = ['label' => 'Line Monitoring Kemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="line-monitoring-kemas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
