<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PengolahanItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengolahan Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengolahan-item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pengolahan Item', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nobatch',
            'snfg_komponen',
            'besar_batch_real',
            'pengolahan_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
