<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PengolahanItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengolahan-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nobatch')->textInput() ?>

    <?= $form->field($model, 'besar_batch_real')->textInput() ?>

    <?= $form->field($model, 'pengolahan_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
