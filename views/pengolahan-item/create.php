<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PengolahanItem */

$this->title = 'Create Pengolahan Item';
$this->params['breadcrumbs'][] = ['label' => 'Pengolahan Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengolahan-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
