<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ListKendalaScf */

$this->title = 'Create List Kendala Scf';
$this->params['breadcrumbs'][] = ['label' => 'List Kendala Scfs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="list-kendala-scf-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
