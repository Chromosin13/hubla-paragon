<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UniqueKoitem */

$this->title = 'Create Unique Koitem';
$this->params['breadcrumbs'][] = ['label' => 'Unique Koitems', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unique-koitem-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
