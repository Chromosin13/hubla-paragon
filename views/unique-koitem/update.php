<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UniqueKoitem */

$this->title = 'Update Unique Koitem: ' . $model->koitem;
$this->params['breadcrumbs'][] = ['label' => 'Unique Koitems', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->koitem, 'url' => ['view', 'id' => $model->koitem]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="unique-koitem-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
