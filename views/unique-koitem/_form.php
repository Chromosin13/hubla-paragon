<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UniqueKoitem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="unique-koitem-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'koitem')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
