<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\QcbulkRft */

$this->title = 'Update Qcbulk Rft: ' . $model->row_number;
$this->params['breadcrumbs'][] = ['label' => 'Qcbulk Rfts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->row_number, 'url' => ['view', 'id' => $model->row_number]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="qcbulk-rft-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
