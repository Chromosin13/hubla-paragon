<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\QcbulkRft */

$this->title = $model->row_number;
$this->params['breadcrumbs'][] = ['label' => 'Qcbulk Rfts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qcbulk-rft-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->row_number], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->row_number], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'row_number',
            'start_id',
            'sediaan',
            'snfg',
            'snfg_komponen',
            'nama_fg',
            'lanjutan',
            'jenis_periksa',
            'jumlah_operator',
            'nama_qc',
            'stop_id',
            'ph',
            'viskositas',
            'berat_jenis',
            'kadar',
            'warna',
            'bau',
            'performance',
            'bentuk',
            'mikro',
            'kejernihan',
            'status',
            'is_done',
            'status_rft:ntext',
        ],
    ]) ?>

</div>
