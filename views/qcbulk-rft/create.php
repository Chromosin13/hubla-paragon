<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\QcbulkRft */

$this->title = 'Create Qcbulk Rft';
$this->params['breadcrumbs'][] = ['label' => 'Qcbulk Rfts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qcbulk-rft-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
