<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\QcbulkRftSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qcbulk-rft-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'row_number') ?>

    <?= $form->field($model, 'start_id') ?>

    <?= $form->field($model, 'sediaan') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'snfg_komponen') ?>

    <?php // echo $form->field($model, 'nama_fg') ?>

    <?php // echo $form->field($model, 'lanjutan') ?>

    <?php // echo $form->field($model, 'jenis_periksa') ?>

    <?php // echo $form->field($model, 'jumlah_operator') ?>

    <?php // echo $form->field($model, 'nama_qc') ?>

    <?php // echo $form->field($model, 'stop_id') ?>

    <?php // echo $form->field($model, 'ph') ?>

    <?php // echo $form->field($model, 'viskositas') ?>

    <?php // echo $form->field($model, 'berat_jenis') ?>

    <?php // echo $form->field($model, 'kadar') ?>

    <?php // echo $form->field($model, 'warna') ?>

    <?php // echo $form->field($model, 'bau') ?>

    <?php // echo $form->field($model, 'performance') ?>

    <?php // echo $form->field($model, 'bentuk') ?>

    <?php // echo $form->field($model, 'mikro') ?>

    <?php // echo $form->field($model, 'kejernihan') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'is_done') ?>

    <?php // echo $form->field($model, 'status_rft') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
