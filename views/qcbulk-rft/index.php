<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\QcbulkRftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Qcbulk Rfts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qcbulk-rft-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box-body">
                <div class="row">
                <div class="col-md-12">

                <?php


                                 $count = Yii::$app->db->createCommand("
                                                                  SELECT count(case when status_rft='RFT' then 1 end) as rft,
                                                                       count(case when status_rft='NON RFT' then 1 end) as non_rft
                                                                    FROM qcbulk_rft")->queryAll();

                                 $implode_rft = intval(implode(',',array_column($count,'rft')));     
                                 $implode_nrft = intval(implode(',',array_column($count,'non_rft')));      

                                  $tanggalList = Yii::$app->db->createCommand("
                                                                              SELECT tanggal_stop,
                                                                               count(case when status_rft='RFT' then 1 end) as rft,
                                                                               count(case when status_rft='NON RFT' then 1 end) as non_rft,
                                                                               count(status_rft) as all
                                                                        FROM 
                                                                            (SELECT * from qcbulk_rft) b
                                                                        GROUP BY tanggal_stop
                                                                        ORDER BY tanggal_stop asc")->queryAll();

                    // End of Combination Backend Code

                                                      echo Highcharts::widget([
                                                      'scripts' => [
                                                          'modules/exporting',
                                                          'themes/grid-light',
                                                      ],
                                                      'options' => [
                                                          'rangeSelector' => [
                                                                //'inputEnabled' => new JsExpression('$("#container").width() > 480'),
                                                                'selected' => 1
                                                            ],
                                                          'title' => [
                                                              'text' => 'Achievement RFT',
                                                          ],
                                                          'xAxis' => [
                                                              //'type' => new JsExpression('datetime'),
                                                              'categories' => new SeriesDataHelper($tanggalList,['tanggal_stop']),
                                                              //['PLANNER', 'PENIMBANGAN', 'PENGOLAHAN','QCBULK', 'INKJET', 'KEMAS 1','KEMAS 2', 'QCFG', 'QC TIMBANG FG'],
                                                          ],
                                                          'labels' => [
                                                              'items' => [
                                                                  [
                                                                      'html' => 'Flowboard',
                                                                      'style' => [
                                                                          'left' => '50px',
                                                                          'top' => '18px',
                                                                          'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                                                                      ],
                                                                  ],
                                                              ],
                                                          ],
                                                          'series' => [
                                                              [
                                                                  'type' => 'column',
                                                                  'name' => 'RFT',
                                                                  'data' => new SeriesDataHelper($tanggalList,['rft']),
                                                              ],
                                                              [
                                                                  'type' => 'column',
                                                                  'name' => 'NON RFT',
                                                                  'data' => new SeriesDataHelper($tanggalList,['non_rft']),
                                                              ],
                                                              [
                                                                  'type' => 'spline',
                                                                  'name' => 'Total',
                                                                  'data' => new SeriesDataHelper($tanggalList,['all']),
                                                                  'marker' => [
                                                                      'lineWidth' => 2,
                                                                      'lineColor' => new JsExpression('Highcharts.getOptions().colors[3]'),
                                                                      'fillColor' => 'white',
                                                                  ],
                                                              ],
                                                              [
                                                                  'type' => 'pie',
                                                                  'name' => 'Rekap',
                                                                  'data' => [
                                                                      [
                                                                          'name' => 'RFT',
                                                                          'y' => $implode_rft,
                                                                          'color' => new JsExpression('Highcharts.getOptions().colors[0]'), // Achieved's color
                                                                      ],
                                                                      [
                                                                          'name' => 'NON RFT',
                                                                          'y' => $implode_nrft,//new SeriesDataHelper($countWno,['count']),
                                                                          'color' => new JsExpression('Highcharts.getOptions().colors[1]'), // Not Achieved's color
                                                                      ],
                                                                  ],
                                                                  'center' => [100, 50],
                                                                  'size' => 60,
                                                                  'showInLegend' => true,
                                                                  'dataLabels' => [
                                                                      'enabled' => true,
                                                                  ],
                                                              ],
                                                          ],
                                                      ]
                                                  ]);

                
                                ?>
                </div>
                </div>
    </div>

    <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'primary', 'heading'=>'Achievement Line Penimbangan'],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                'start_id',
                'tanggal_stop',
                'sediaan',
                'snfg',
                'snfg_komponen',
                'nama_fg',
                'lanjutan',
                'jenis_periksa',
                'jumlah_operator',
                'nama_qc',
                'stop_id',
                'ph',
                'viskositas',
                'berat_jenis',
                'kadar',
                'warna',
                'bau',
                'performance',
                'bentuk',
                'mikro',
                'kejernihan',
                'status',
                'is_done',
                'status_rft:ntext',
             ],
        ]);
    
    ?>

</div>
