<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\QcbulkRft */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qcbulk-rft-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'row_number')->textInput() ?>

    <?= $form->field($model, 'start_id')->textInput() ?>

    <?= $form->field($model, 'sediaan')->textInput() ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'snfg_komponen')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'lanjutan')->textInput() ?>

    <?= $form->field($model, 'jenis_periksa')->textInput() ?>

    <?= $form->field($model, 'jumlah_operator')->textInput() ?>

    <?= $form->field($model, 'nama_qc')->textInput() ?>

    <?= $form->field($model, 'stop_id')->textInput() ?>

    <?= $form->field($model, 'ph')->textInput() ?>

    <?= $form->field($model, 'viskositas')->textInput() ?>

    <?= $form->field($model, 'berat_jenis')->textInput() ?>

    <?= $form->field($model, 'kadar')->textInput() ?>

    <?= $form->field($model, 'warna')->textInput() ?>

    <?= $form->field($model, 'bau')->textInput() ?>

    <?= $form->field($model, 'performance')->textInput() ?>

    <?= $form->field($model, 'bentuk')->textInput() ?>

    <?= $form->field($model, 'mikro')->textInput() ?>

    <?= $form->field($model, 'kejernihan')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'is_done')->textInput() ?>

    <?= $form->field($model, 'status_rft')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
