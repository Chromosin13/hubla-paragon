<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax; 
// Menggunakan Pjax untuk Autorefresh
use yii\helpers\Url;
// Untuk url
use yii\data\SqlDataProvider;
use stmswitcher\flipclock\FlipClock;
// Untuk melakukan perintah SQL
 

/* @var $this yii\web\View */
/* @var $searchModel app\models\PosisiProsesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Process Position';

?>


<!--     <?php 
            echo FlipClock::widget([
            'options' => [
                'clockFace' => 'TwentyFourHourClock', // For 24-hour format
            ]
        ]);
    ?> -->

<!-- <h6> Untuk Proses sebelum Kemas 2 adalah <b>SNFG Komponen</b>, Proses selanjutnya adalah <b>SNFG</b> </h6> -->
 <?php
    $countSql = Yii::$app->db->createCommand('
        SELECT COUNT(*) FROM posisi_proses')->queryScalar();
 // Melakukan Counting unntuk mendapatkan seluruh record dari Posisi Proses, dengan Unique key SNFG
 ?>

 <?php 
 // Menggunakan Session sebagai reset counter ketika sudah mencapai halaman terakhir
    if(!isset($_SESSION)) 
        { 
            session_start(); 
        } 
    if(empty($_SESSION['count'])) 
            $_SESSION['count'] = 0;
            $order =  $_SESSION['count']+1;
            $_SESSION['count'] =  $order; 
    if($order>$countSql/13) session_destroy();
    $uri = "index.php?r=posisi-proses/index&page=$order";
 ?>

<!-- This Is The Code To Refresh The Page , Click Refresh Button for Every 5 Seconds  -->
<?php
$script = <<< JS
$(document).ready(function() {
    setInterval(function(){ $("#siteIndex").click(); }, 5000);

});

JS;
    $this->registerJs($script);
?>

<!-- End Of Auto Refresh Script -->    

<?php Pjax::begin(); ?>

<?=
    Html::a("", $uri, [/*'class' => 'btn btn-lg btn-primary',*/ 'id' => 'siteIndex']);
?>

<!-- Define Grid View -->


<div class="posisi-proses-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [


                        [
                            'attribute' => 'nomo',
                            'label' => 'No MO',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 220px','class'=>'info'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        [
                            'attribute' => 'snfg',
                            'label' => 'SNFG',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 220px','class'=>'info'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        [
                            'attribute' => 'snfg_komponen',
                            'label' => 'Komponen',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 220px','class'=>'info'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        [
                            'attribute' => 'nama_fg',
                            'label' => 'Nama FG',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 220px','class'=>'info'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        'start',
                        [
                            'attribute' => 'due',
                            'label' => 'Due',
                            //'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => function($model){
                                     if($model->ontime == 'OVERDUE')
                                                        {
                                                            return ['class'=>'danger'];
                                                        }else 
                                                        {
                                                            return ['class'=>'info'];
                                                        }
                            },
                        ],
                        [
                            'attribute' => 'timestamp',
                            'label' => 'Last Update',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;width: 150px'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        [
                            'attribute' => 'posisi',
                            'label' => 'Posisi',
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => function($model){
                            // Pewarnaan untuk Posisi
                                                    if($model->posisi == 'QC Bulk')
                                                    {
                                                        return ['class'=>'danger'];
                                                    }else if($model->posisi == 'PLANNER')
                                                    {
                                                        return ['style'=>'background-color:#ff6600;color: #ffffff;width: 120px;text-align: center;font-weight:bold'];
                                                    }
                                                    else if($model->posisi == 'PENIMBANGAN')
                                                    {
                                                        return ['style'=>'background-color:#ffcc00;color: #ffffff;width: 120px;text-align: center;font-weight:bold'];
                                                    }
                                                    else if($model->posisi == 'PENGOLAHAN')
                                                    {
                                                        return ['style'=>'background-color:#ffff00;color: #ffffff;width: 120px;text-align: center;font-weight:bold'];
                                                    }
                                                    else if($model->posisi == 'QC BULK')
                                                    {
                                                        return ['style'=>'background-color: #ccff33;color: #ffffff;width: 120px;text-align: center;font-weight:bold'];
                                                    }
                                                    else if($model->posisi == 'PRESS')
                                                    {
                                                        return ['style'=>'background-color:#99cc00;color: #ffffff;width: 120px;text-align: center;font-weight:bold'];
                                                    }
                                                    else if($model->posisi == 'INKJET')
                                                    {
                                                        return ['style'=>'background-color:#669900;color: #ffffff;width: 120px;text-align: center;font-weight:bold'];
                                                    }
                                                    else if($model->posisi == 'KEMAS 1')
                                                    {
                                                        return ['style'=>'background-color:#009900;color: #ffffff;width: 120px;text-align: center;font-weight:bold'];
                                                    }
                                                    else if($model->posisi == 'KEMAS 2')
                                                    {
                                                        return ['style'=>'background-color:#009933;color: #ffffff;width: 120px;text-align: center;font-weight:bold'];
                                                    }
                                                    else if($model->posisi == 'QC FG')
                                                    {
                                                        return ['style'=>'background-color:#00ccff;color: #ffffff;width: 120px;text-align: center;font-weight:bold'];
                                                    }
                                                    else if($model->posisi == 'QC TIMBANG FG')
                                                    {
                                                        return ['style'=>'background-color:#0099ff;color: #ffffff;width: 120px;text-align: center;font-weight:bold'];
                                                    }
                                                },
                        ],
                        [
                            'attribute' => 'status',
                            'label' => 'Status',
                            //'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => function($model){
                                     if($model->status == 'Waiting For Next Operation')
                                                        {
                                                            return ['style'=>'background-color:#F37735;color: #ffffff;width: 120px;text-align: center;font-weight:bold'];
                                                        }else if($model->status == 'Work in Progress')
                                                        {
                                                            return ['style'=>'background-color:#00B159;color: #ffffff;width: 120px;text-align: center;font-weight:bold'];
                                                        }
                                                        else if($model->status == 'HOLD')
                                                        {
                                                            return ['style'=>'background-color:#F0A150;color: #ffffff;width: 120px;text-align: center;font-weight:bold'];
                                                        }
                                                        else if($model->status == 'Waiting For Next Shift')
                                                        {
                                                            return ['style'=>'background-color:#862d2d;color: #ffffff;width: 120px;text-align: center;font-weight:bold'];
                                                        }
                                                        else    
                                                        {
                                                            return ['style'=>'background-color:#F37735;color: #ffffff;width: 120px;text-align: center;font-weight:bold'];
                                                        }
                            },
                        ],
                        'lanjutan_split_batch',
                        'palet_ke',
                    ],
    ]); ?>
</div>

 <?php Pjax::end(); ?>


