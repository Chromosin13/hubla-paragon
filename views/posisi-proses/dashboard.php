<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FlowReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dashboard Panel';
/*
$this->params['breadcrumbs'][] = $this->title;
*/
?>
<div class="flow-report-index">
<?php
$countSql = Yii::$app->db->createCommand('
    SELECT COUNT(*) FROM posisi_proses')->queryScalar();
?>

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        <?= Html::a('Create Flow Report', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
     -->


<!--  -->

<!-- This Is The Code To Refresh The Page , Click Refresh Button for Every 60 Seconds  -->
<?php
$script = <<< JS
$(document).ready(function() {
    setInterval(function(){ $("#refreshButton").click(); }, 60000);

});
JS;
$this->registerJs($script);
?>

<!-- End Of Auto Refresh Script -->


    <?php Pjax::begin(); ?>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
              <!-- Apply any bg-* class to to the icon to color it -->
              <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Number Of Entry SMB</span>
                <span class="info-box-number">
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT COUNT(*) FROM posisi_proses')->queryScalar();
                    ?>          
                </span>
              </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <!-- Apply any bg-* class to to the icon to color it -->
          <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Last No SNFG</span>
            <span class="info-box-number"> 
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT snfg FROM posisi_proses limit 1')->queryScalar();
                    ?>    
            </span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->  
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-files-o"></i></span>
        <div class="info-box-content">
            <span class="info-box-text">Last Entry Process</span>
            <span class="info-box-number">
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT posisi FROM posisi_proses limit 1')->queryScalar();
                    ?>   
            </span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-flag-o"></i></span>
        <div class="info-box-content">
            <span class="info-box-text">Current Most Process
            </span>
            <span class="info-box-number">
                <?php
                       
                       $processList = Yii::$app->db->createCommand('
                                                                  SELECT a.posisi from
                                                                        (SELECT posisi,count(posisi) process_count
                                                                            FROM posisi_proses
                                                                          GROUP BY posisi
                                                                        ) a
                                                                  where a.process_count=(SELECT max(b.process_count) from
                                                                  (SELECT posisi,count(posisi) process_count
                                                                          FROM posisi_proses
                                                                    GROUP BY posisi) b )'
                                          )->queryAll();
                      echo implode(', ',array_column($processList,'posisi'));

                ?>   


            </span>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div>

    <!-- Define Refresh Button -->
    <?=
    Html::a("", ['posisi-proses/dashboard'], [/*'class' => 'btn btn-lg btn-primary',*/ 'id' => 'refreshButton']) 
    ?> 
    <!-- Define Next Page Button-->
    <!-- Not yet Implemented    -->


    <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Dashboard Flowboard</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <!-- <p class="text-center">
                    <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
                  </p>
 -->
                  <div class="chart">
                   <?php
                    $countPlanner = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(*) FROM posisi_proses where posisi='PLANNER'")->queryScalar();
                    $countPenimbangan = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(*) FROM posisi_proses where posisi='PENIMBANGAN'")->queryScalar();
                    $countPengolahan = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(*) FROM posisi_proses where posisi='PENGOLAHAN'")->queryScalar();
                    $countQcbulk = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(*) FROM posisi_proses where posisi='QC BULK'")->queryScalar();
                    $countInkjet = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(*) FROM posisi_proses where posisi='INKJET'")->queryScalar();
                    $countKemas1 = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(*) FROM posisi_proses where posisi ilike 'KEMAS 1'")->queryScalar();

                    $countKemas2 = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(*) FROM posisi_proses where posisi ilike 'KEMAS 2'")->queryScalar();
                    $countQcfg = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(*) FROM posisi_proses where posisi='QC FG'")->queryScalar();
                    $countQctimbangfg = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(*) FROM posisi_proses where posisi='QC TIMBANG FG'")->queryScalar();

                    echo Highcharts::widget([
                                                'scripts' => [
                                                    'modules/exporting',
                                                    'themes/grid-light',
                                                ],
                                               'options' => [
                                                  'title' => ['text' => 'Status'],
                                                  'xAxis' => [
                                                     'categories' => ['PLANNER', 'PENIMBANGAN', 'PENGOLAHAN','QCBULK', 'INKJET', 'KEMAS 1','KEMAS 2', 'QCFG', 'QC TIMBANG FG']
                                                  ],
                                                  'yAxis' => [
                                                     'title' => ['text' => 'Jumlah Barang']
                                                  ],
                                                   'plotOptions' => [
                                                                    'line' => [
                                                                        'dataLabels' => [
                                                                                            'enabled' => true
                                                                                        ],
                                                                        'enableMouseTracking' => true
                                                                        ]
                                                                    ],
                                                  'series' => [
                                                     ['name' => 'Proses', 'data' => [$countPlanner,  $countPenimbangan, $countPengolahan, $countQcbulk, $countInkjet, $countKemas1, $countKemas2, $countQcfg, $countQctimbangfg]]
                                                  ]
                                               ]
                                            ]);

        // Backend Process SQL to Get Data For Combination Chart
        // This may not be the best practice to write a backend code, but at least it's working, just as Mas Jay told me about Gibbs Theorem

                      $countWip = Yii::$app->db->createCommand("
                                                                  select count(*) as count from posisi_proses where status='Work in Progress'")->queryAll();

                      $countHold = Yii::$app->db->createCommand("
                                                                  select count(*) as count from posisi_proses where status='HOLD'")->queryAll();

                      $countPause = Yii::$app->db->createCommand("
                                                                  select count(*) as count from posisi_proses where status='PAUSE'")->queryAll();

                      $countWno = Yii::$app->db->createCommand("
                                                                  select count(*) as count from posisi_proses where status='Waiting For Next Operation'")->queryAll();

                      $implode_cwip = intval(implode(',',array_column($countWip,'count')));
                      $implode_cpause = intval(implode(',',array_column($countPause,'count')));
                      $implode_chold = intval(implode(',',array_column($countHold,'count')));
                      $implode_cwno = intval(implode(',',array_column($countWno,'count')));

                      $allList = Yii::$app->db->createCommand("
                                                                  select coalesce(pp.process_count,0) process_count from 
                                                                        (select  * from urut_proses order by id asc) up
                                                                  left join (select count(posisi) as process_count,posisi from posisi_proses group by posisi
                                                                             ) pp on pp.posisi=up.nama_proses")->queryAll();

                      $wipList = Yii::$app->db->createCommand("
                                                                  select coalesce(pp.process_count,0) process_count from 
                                                                        (select  * from urut_proses order by id asc) up
                                                                  left join (select count(posisi) as process_count,posisi from posisi_proses where status='Work in Progress' group by posisi
                                                                             ) pp on pp.posisi=up.nama_proses")->queryAll();
                      $wipListimplode = implode(',',array_column($wipList,'process_count'));

                      $wnoList = Yii::$app->db->createCommand("
                                                                  select coalesce(pp.process_count,0) process_count from 
                                                                        (select  * from urut_proses order by id asc) up
                                                                  left join (select count(posisi) as process_count,posisi from posisi_proses where status='Waiting For Next Operation' group by posisi
                                                                             ) pp on pp.posisi=up.nama_proses")->queryAll();
                      $wnoListimplode = implode(', ',array_column($wnoList,'process_count'));

                      $holdList = Yii::$app->db->createCommand("
                                                                  select coalesce(pp.process_count,0) process_count from 
                                                                        (select  * from urut_proses order by id asc) up
                                                                  left join (select count(posisi) as process_count,posisi from posisi_proses where status='HOLD' group by posisi
                                                                             ) pp on pp.posisi=up.nama_proses")->queryAll();
                      $pauseList = Yii::$app->db->createCommand("
                                                                  select coalesce(pp.process_count,0) process_count from 
                                                                        (select  * from urut_proses order by id asc) up
                                                                  left join (select count(posisi) as process_count,posisi from posisi_proses where status='PAUSE' group by posisi
                                                                             ) pp on pp.posisi=up.nama_proses")->queryAll();

        // End of Combination Backend Code

                                          echo Highcharts::widget([
                                          'scripts' => [
                                              'modules/exporting',
                                              'themes/grid-light',
                                          ],
                                          'options' => [
                                              'title' => [
                                                  'text' => 'Combination chart',
                                              ],
                                              'xAxis' => [
                                                  'categories' => ['PLANNER', 'PENIMBANGAN', 'PENGOLAHAN','QCBULK', 'INKJET', 'KEMAS 1','KEMAS 2', 'QCFG', 'QC TIMBANG FG'],
                                              ],
                                              'labels' => [
                                                  'items' => [
                                                      [
                                                          'html' => 'Flowboard',
                                                          'style' => [
                                                              'left' => '50px',
                                                              'top' => '18px',
                                                              'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                                                          ],
                                                      ],
                                                  ],
                                              ],
                                              'series' => [
                                                  [
                                                      'type' => 'column',
                                                      'name' => 'Work In Progress',
                                                      'data' => new SeriesDataHelper($wipList,['process_count']),
                                                  ],
                                                  [
                                                      'type' => 'column',
                                                      'name' => 'Waiting For Next Process',
                                                      'data' => new SeriesDataHelper($wnoList,['process_count']),
                                                  ],
                                                  [
                                                      'type' => 'column',
                                                      'name' => 'Hold',
                                                      'data' => new SeriesDataHelper($holdList,['process_count']),
                                                  ],
                                                  [
                                                      'type' => 'column',
                                                      'name' => 'Pause',
                                                      'data' => new SeriesDataHelper($pauseList,['process_count']),
                                                  ],
                                                  // [
                                                  //     'type' => 'column',
                                                  //     'name' => 'Joe',
                                                  //     'data' => [4, 3, 3, 9, 0],
                                                  // ],
                                                  [
                                                      'type' => 'spline',
                                                      'name' => 'Total',
                                                      'data' => new SeriesDataHelper($allList,['process_count']),
                                                      'marker' => [
                                                          'lineWidth' => 2,
                                                          'lineColor' => new JsExpression('Highcharts.getOptions().colors[3]'),
                                                          'fillColor' => 'white',
                                                      ],
                                                  ],
                                                  [
                                                      'type' => 'pie',
                                                      'name' => 'Total consumption',
                                                      'data' => [
                                                          [
                                                              'name' => 'Work In Progress',
                                                              'y' => $implode_cwip,
                                                              'color' => new JsExpression('Highcharts.getOptions().colors[0]'), // Jane's color
                                                          ],
                                                          [
                                                              'name' => 'Waiting For Next Operation',
                                                              'y' => $implode_cwno,//new SeriesDataHelper($countWno,['count']),
                                                              'color' => new JsExpression('Highcharts.getOptions().colors[1]'), // John's color
                                                          ],
                                                          [
                                                              'name' => 'Hold',
                                                              'y' => $implode_chold,//new SeriesDataHelper($countWno,['count']),
                                                              'color' => new JsExpression('Highcharts.getOptions().colors[8]'), // John's color
                                                          ],
                                                          [
                                                              'name' => 'Pause',
                                                              'y' => $implode_cpause,//new SeriesDataHelper($countWno,['count']),
                                                              'color' => new JsExpression('Highcharts.getOptions().colors[11]'), // John's color
                                                          ],
                                                      ],
                                                      'center' => [200, 50],
                                                      'size' => 80,
                                                      'showInLegend' => false,
                                                      'dataLabels' => [
                                                          'enabled' => false,
                                                      ],
                                                  ],
                                              ],
                                          ]
                                      ]);
                    ?>

                    <!-- Sales Chart Canvas -->
                    <!-- <canvas id="salesChart" style="height: 189px; width: 499px;" width="474" height="241">
                    
                    </canvas> -->
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <p class="text-center">
                    <strong>Production Stats</strong>
                  </p>

                  <div class="progress-group">
                    <span class="progress-text">Pengolahan</span>
                    <span class="progress-number"><b>
                    <?=
                                                        $countSql = Yii::$app->db->createCommand("
                                                        SELECT COUNT(posisi) FROM posisi_proses where posisi='PENGOLAHAN'")->queryScalar();
                    ?>
                    </b>
                    /
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT COUNT(*) FROM posisi_proses')->queryScalar();
                    ?>

                    </span>
        
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="width: 
                                       <?php
                                                $countPengolahan = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(posisi) FROM posisi_proses where posisi='PENGOLAHAN'")->queryScalar();
                                                $countAll = Yii::$app->db->createCommand('
                                                                             SELECT COUNT(*) FROM posisi_proses')->queryScalar();
                                                                     
                                      if ($countAll==0) {
                                          echo 0;
                                      } else {
                                          echo ceil(($countPengolahan/$countAll)*100);
                                      }
                                      ?>%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">INKJET</span>
                    <span class="progress-number"><b>
                    <?=
                                                        $countSql = Yii::$app->db->createCommand("
                                                        SELECT COUNT(posisi) FROM posisi_proses where posisi='INKJET'")->queryScalar();
                    ?>
                    </b>
                    /
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT COUNT(*) FROM posisi_proses')->queryScalar();
                    ?></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: <?php
                                                $countInkjet = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(posisi) FROM posisi_proses where posisi='INKJET'")->queryScalar();
                                                $countAll = Yii::$app->db->createCommand('
                                                                             SELECT COUNT(*) FROM posisi_proses')->queryScalar();
                                                                     
                                      if ($countAll==0) {
                                          echo 0;
                                      } else {
                                          echo ceil(($countInkjet/$countAll)*100);
                                      }
                                      ?>%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">QC Bulk</span>
                    <span class="progress-number"><b>
                    <?=
                                                        $countSql = Yii::$app->db->createCommand("
                                                        SELECT COUNT(posisi) FROM posisi_proses where posisi='QC Bulk'")->queryScalar();
                    ?>
                    </b>
                    /
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT COUNT(*) FROM posisi_proses')->queryScalar();
                    ?></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: <?php
                                                $countQCBulk = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(posisi) FROM posisi_proses where posisi='QC Bulk'")->queryScalar();
                                                $countAll = Yii::$app->db->createCommand('
                                                                             SELECT COUNT(*) FROM posisi_proses')->queryScalar();
                                                                     
                                      if ($countAll==0) {
                                          echo 0;
                                      } else {
                                          echo ceil(($countQCBulk/$countAll)*100);
                                      }
                                      ?>%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">QC FG</span>
                    <span class="progress-number"><b>
                    <?=
                                                        $countSql = Yii::$app->db->createCommand("
                                                        SELECT COUNT(posisi) FROM posisi_proses where posisi='QC FG'")->queryScalar();
                    ?>
                    </b>
                    /
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT COUNT(*) FROM posisi_proses')->queryScalar();
                    ?></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: <?php
                                                $countQCFG = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(posisi) FROM posisi_proses where posisi='QC FG'")->queryScalar();
                                                $countAll = Yii::$app->db->createCommand('
                                                                             SELECT COUNT(*) FROM posisi_proses')->queryScalar();
                                                                     
                                      if ($countAll==0) {
                                          echo 0;
                                      } else {
                                          echo ceil(($countQCFG/$countAll)*100);
                                      }
                                      ?>%"></div>
                    </div>
                  </div>

                  <div class="progress-group">
                    <span class="progress-text">Karantina Timbang</span>
                    <span class="progress-number"><b>
                    <?=
                                                        $countSql = Yii::$app->db->createCommand("
                                                        SELECT COUNT(posisi) FROM posisi_proses where posisi='Karantina Timbang'")->queryScalar();
                    ?>
                    </b>
                    /
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT COUNT(*) FROM posisi_proses')->queryScalar();
                    ?></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-black" style="width: 
                              <?php
                                                $countKTimbang = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(posisi) FROM posisi_proses where posisi='Karantina Timbang'")->queryScalar();
                                                $countAll = Yii::$app->db->createCommand('
                                                                             SELECT COUNT(*) FROM posisi_proses')->queryScalar();
                                                                     
                                      if ($countAll==0) {
                                          echo 0;
                                      } else {
                                          echo ceil(($countKTimbang/$countAll)*100);
                                      }?>%">
                      </div>
                    </div>
                  </div>

                  <div class="progress-group">
                    <span class="progress-text">Kemas</span>
                    <span class="progress-number"><b>
                    <?=
                                                        $countSql = Yii::$app->db->createCommand("
                                                        SELECT COUNT(posisi) FROM posisi_proses where posisi ilike 'Kemas%'")->queryScalar();
                    ?>
                    </b>
                    /
                    <?=
                        $countSql = Yii::$app->db->createCommand('
                        SELECT COUNT(*) FROM posisi_proses')->queryScalar();
                    ?></span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-light-blue" style="width: <?php
                                                $countKemas = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(posisi) FROM posisi_proses where posisi ilike 'Kemas%'")->queryScalar();
                                                $countAll = Yii::$app->db->createCommand('
                                                                             SELECT COUNT(*) FROM posisi_proses')->queryScalar();
                                                                     
                                      if ($countAll==0) {
                                          echo 0;
                                      } else {
                                          echo ceil(($countKemas/$countAll)*100);
                                      }?>%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                </div>
                <div class="col-md-4">

                <?php

                $countPlanner = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(posisi) FROM posisi_proses where posisi='PLANNER'")->queryScalar();

                $countPenimbangan = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(posisi) FROM posisi_proses where posisi='PENIMBANGAN'")->queryScalar();

                $countPengolahan = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(posisi) FROM posisi_proses where posisi='PENGOLAHAN'")->queryScalar();
                    $countInkjet = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(posisi) FROM posisi_proses where posisi='INKJET'")->queryScalar();
                    $countQCBulk = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(posisi) FROM posisi_proses where posisi='QC BULK'")->queryScalar();
                    $countQCFG = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(posisi) FROM posisi_proses where posisi='QC FG'")->queryScalar();
                    $countQctimbangfg = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(posisi) FROM posisi_proses where posisi='QC TIMBANG FG'")->queryScalar();
                    $countKemas1 = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(posisi) FROM posisi_proses where posisi ilike 'KEMAS 1'")->queryScalar();
                    $countKemas2 = Yii::$app->db->createCommand("
                                                                                SELECT COUNT(posisi) FROM posisi_proses where posisi ilike 'KEMAS 2'")->queryScalar();

                echo Highcharts::widget([
                    'options' => [
                        'title' => ['text' => 'Pie Chart - Progress'],
                        'plotOptions' => [
                            'pie' => [
                                'cursor' => 'pointer',
                            ],
                        ],
                        'series' => [
                            [ // new opening bracket
                                'type' => 'pie',
                                'name' => 'Elements',
                                'data' => [
                                    ['Planner', $countPlanner],
                                    ['Penimbangan', $countPenimbangan],
                                    ['Pengolahan', $countPengolahan],
                                    ['QC Bulk', $countQCBulk],
                                    ['Inkjet', $countInkjet],
                                    ['Kemas 1',$countKemas1],
                                    ['Kemas 2',$countKemas2],
                                    ['QC FG', $countQCFG],
                                    ['QC TIMBANG FG', $countQctimbangfg],
                                ],
                            ] // new closing bracket
                        ],
                    ],
                ]);            
                ?>

                </div>
                
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>
                    <h5 class="description-header">$35,210.43</h5>
                    <span class="description-text">TOTAL REVENUE</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
                    <h5 class="description-header">$10,390.90</h5>
                    <span class="description-text">TOTAL COST</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>
                    <h5 class="description-header">$24,813.53</h5>
                    <span class="description-text">TOTAL PROFIT</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block">
                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
                    <h5 class="description-header">1200</h5>
                    <span class="description-text">GOAL COMPLETIONS</span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>


    <?php Pjax::end(); ?>


</div>

