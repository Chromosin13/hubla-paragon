<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PosisiProses */

$this->title = 'Create Posisi Proses';
$this->params['breadcrumbs'][] = ['label' => 'Posisi Proses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posisi-proses-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
