<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax; 
// Menggunakan Pjax untuk Autorefresh
use yii\helpers\Url;
// Untuk url
use yii\data\SqlDataProvider;
use stmswitcher\flipclock\FlipClock;
// Untuk melakukan perintah SQL
 

/* @var $this yii\web\View */
/* @var $searchModel app\models\PosisiProsesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// $this->title = 'Process Position';

?>


<!-- <h6> Untuk Proses sebelum Kemas 2 adalah <b>SNFG Komponen</b>, Proses selanjutnya adalah <b>SNFG</b> </h6> -->
 <?php
    $countSql = Yii::$app->db->createCommand('
        SELECT COUNT(*) FROM posisi_proses')->queryScalar();
 // Melakukan Counting unntuk mendapatkan seluruh record dari Posisi Proses, dengan Unique key SNFG
 ?>

 <?php 
 // Menggunakan Session sebagai reset counter ketika sudah mencapai halaman terakhir
    if(!isset($_SESSION)) 
        { 
            session_start(); 
        } 
    if(empty($_SESSION['count'])) 
            $_SESSION['count'] = 0;
            $order =  $_SESSION['count']+1;
            $_SESSION['count'] =  $order; 
    if($order>$countSql/7) session_destroy();
    $uri = "index.php?r=posisi-proses/running&page=$order";
 ?>

<!-- This Is The Code To Refresh The Page , Click Refresh Button for Every 5 Seconds  -->
<?php
$script = <<< JS
$(document).ready(function() {
    setInterval(function(){ $("#siteIndex").click(); }, 5000);

});

JS;
    $this->registerJs($script);
?>

<!-- End Of Auto Refresh Script -->    
<!-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" id="offcanvasTrigger"></a>
 -->

<?php Pjax::begin(); ?>

<?=
    Html::a("", $uri, [/*'class' => 'btn btn-lg btn-primary',*/ 'id' => 'siteIndex']);

?>

<!-- Define Grid View -->


<div class="posisi-proses-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [

                        [
                            'attribute' => 'nama_fg',
                            'label' => 'Nama FG',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 220px;font-size: 18px','class'=>'info'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        [
                            'attribute' => 'nomo',
                            'label' => 'No MO',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'info'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        [
                            'attribute' => 'snfg',
                            'label' => 'SNFG',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 150px','class'=>'info'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        [
                            'attribute' => 'snfg_komponen',
                            'label' => 'Komponen',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 150px','class'=>'info'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        [
                            'attribute' => 'lanjutan_split_batch',
                            'label' => 'Sb',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;width: 15px'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        [  
                            'attribute' => 'start',
                            'label'     => 'Start',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px'
                            ]
                        ],
                        [
                            'attribute' => 'due',
                            'label' => 'Due',
                            //'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => function($model){
                                     if($model->ontime == 'OVERDUE')
                                                        {
                                                            return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                                                        }else 
                                                        {
                                                            return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'info'];
                                                        }
                            },
                        ],
                        [
                            'attribute' => 'time',
                            'label' => 'Last Update',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;width: 150px'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        [
                            'attribute' => 'posisi',
                            'label' => 'Posisi',
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => function($model){
                            // Pewarnaan untuk Posisi
                                                    if($model->posisi == 'PLANNER')
                                                    {
                                                        return ['style'=>'background-color:#1565C0;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                                    }
                                                    else if($model->posisi == 'PENIMBANGAN')
                                                    {
                                                        return ['style'=>'background-color:#1976D2;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                                    }
                                                    else if($model->posisi == 'PENGOLAHAN')
                                                    {
                                                        return ['style'=>'background-color:#1E88E5;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                                    }
                                                    else if($model->posisi == 'QC BULK')
                                                    {
                                                        return ['style'=>'background-color: #2196F3;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                                    }
                                                    else if($model->posisi == 'INKJET')
                                                    {
                                                        return ['style'=>'background-color:#42A5F5;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                                    }
                                                    else if($model->posisi == 'KEMAS 1')
                                                    {
                                                        return ['style'=>'background-color:#64B5F6;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                                    }
                                                    else if($model->posisi == 'KEMAS 2')
                                                    {
                                                        return ['style'=>'background-color:#90CAF9;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                                    }
                                                    else if($model->posisi == 'QC FG')
                                                    {
                                                        return ['style'=>'background-color:#BBDEFB;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                                    }
                                                },
                        ],
                        [
                            'attribute' => 'status',
                            'label' => 'Status',
                            //'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => function($model){
                                     if(strpos($model->status, 'Waiting For Next Operation') !== false)
                                    {
                                        return ['style'=>'background-color:#ECF0F1;color: #000000;width: 50px;text-align: center;font-weight:bold'];
                                    }
                                    else if($model->status == 'HOLD')
                                    {
                                        return ['style'=>'background-color:#E74C3C;color: #ffffff;width: 50px;text-align: center;font-weight:bold'];
                                    }
                                    else if($model->status == 'PAUSE')
                                    {
                                        return ['style'=>'background-color:#E74C3C;color: #ffffff;width: 50px;text-align: center;font-weight:bold'];
                                    }
                                    else if($model->status == 'Waiting For Next Shift')
                                    {
                                        return ['style'=>'background-color:#7F8C8D;color: #ffffff;width: 50px;text-align: center;font-weight:bold'];
                                    }
                                    else    
                                    {
                                        return ['style'=>'background-color:#2ECC71;color: #ffffff;width: 50px;text-align: center;font-weight:bold'];
                                    }
                            },
                        ],

                    ],
    ]); ?>
</div>

 <?php Pjax::end(); ?>


<?php
$script = <<< JS
// $(function() {
//     $('#offcanvasTrigger').click();
// });
JS;
$this->registerJs($script);
?>