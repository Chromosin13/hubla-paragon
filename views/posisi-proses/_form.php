<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PosisiProses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="posisi-proses-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'snfg_komponen')->textInput() ?>

    <?= $form->field($model, 'posisi')->textInput() ?>

    <?= $form->field($model, 'start')->textInput() ?>

    <?= $form->field($model, 'due')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
