<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax; 
// Menggunakan Pjax untuk Autorefresh
use yii\helpers\Url;
// Untuk url
use yii\data\SqlDataProvider;
use stmswitcher\flipclock\FlipClock;
// Untuk melakukan perintah SQL
use app\models\PosisiProses;
use yii\helpers\ArrayHelper;
 

/* @var $this yii\web\View */
/* @var $searchModel app\models\PosisiProsesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// $this->title = 'Process Position';

?>


<!-- <h6> Untuk Proses sebelum Kemas 2 adalah <b>SNFG Komponen</b>, Proses selanjutnya adalah <b>SNFG</b> </h6> -->
 <?php
    $countSql = Yii::$app->db->createCommand('
        SELECT COUNT(*) FROM posisi_proses')->queryScalar();
 // Melakukan Counting unntuk mendapatkan seluruh record dari Posisi Proses, dengan Unique key SNFG
 ?>

 <?php 
 // Menggunakan Session sebagai reset counter ketika sudah mencapai halaman terakhir
    if(!isset($_SESSION)) 
        { 
            session_start(); 
        } 
    if(empty($_SESSION['count']))  { $_SESSION['count'] = 0; }
            
                           // print 'SESSION INIT : '.$_SESSION['count'];

            // $order =  $_SESSION['count']+1;
                            // print ' ORDER : '.$order;
            

            // $_SESSION['count'] =  $order;

             $_SESSION['count'] =  $_SESSION['count']+1;

             $order =  $_SESSION['count'];
                        // print ' SESSION FIN : '.$_SESSION['count'];

    if($order>$countSql/7) session_destroy();
    // $uri = "http://10.3.5.102/flowreport/web/index.php?r=posisi-proses/index&page=$order";
    // $uri = "index.php?r=posisi-proses/index&page=$order";
    $uri = "index.php?r=posisi-proses/index&page=$order";
 ?>

<!-- This Is The Code To Refresh The Page , Click Refresh Button for Every 5 Seconds  -->
<?php
$script = <<< JS
// $(document).ready(function() {
//     setInterval(function(){ $("#siteIndex").click(); }, 5000);

// });

// $(document).ready(function() {
//     setInterval(function(){ $("#testAja").click(); }, 5000);

// });


JS;
    $this->registerJs($script);
?>

<!-- End Of Auto Refresh Script -->    

<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" id="offcanvasTrigger"></a>
<!-- <a href="<?php echo $uri; ?>" class="sidebar-toggle" role="button" id="testAja"> <?$order;?> </a> -->


<a href="<?php echo $uri;?>" id="testAja"></a>

<?php
    // Html::a($order." ".$_SESSION['count'], $uri, ['class' => 'btn btn-lg btn-primary', 'id' => 'siteIndex']);

?>




<?php Pjax::begin(); ?>



<!-- Define Grid View -->


<div class="posisi-proses-index">
    <!-- <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [

                        [
                            'attribute' => 'nama_fg',
                            'label' => 'Nama FG',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 220px;font-size: 18px','class'=>'info'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        [
                            'attribute' => 'nomo',
                            'label' => 'No MO',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'info'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        [
                            'attribute' => 'snfg',
                            'label' => 'SNFG',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 150px','class'=>'info'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        [
                            'attribute' => 'snfg_komponen',
                            'label' => 'Komponen',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 150px','class'=>'info'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        [
                            'attribute' => 'lanjutan_split_batch',
                            'label' => 'Sb',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;width: 15px'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        [  
                            'attribute' => 'start',
                            'label'     => 'Start',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px'
                            ]
                        ],
                        [
                            'attribute' => 'due',
                            'label' => 'Due',
                            //'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => function($model){
                                     if($model->ontime == 'OVERDUE')
                                                        {
                                                            return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                                                        }else 
                                                        {
                                                            return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'info'];
                                                        }
                            },
                        ],
                        [
                            'attribute' => 'time',
                            'label' => 'Last Update',
                            'format' => 'raw',
                            'contentOptions' => ['style' => ' text-align: center;width: 150px'],
                            'headerOptions' => ['style' => 'text-align: center;'],
                        ],
                        [
                            'attribute' => 'posisi',
                            'label' => 'Posisi',
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => function($model){
                            // Pewarnaan untuk Posisi
                                                    if($model->posisi == 'PLANNER')
                                                    {
                                                        return ['style'=>'background-color:#1565C0;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                                    }
                                                    else if($model->posisi == 'PENIMBANGAN')
                                                    {
                                                        return ['style'=>'background-color:#1976D2;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                                    }
                                                    else if($model->posisi == 'PENGOLAHAN')
                                                    {
                                                        return ['style'=>'background-color:#1E88E5;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                                    }
                                                    else if($model->posisi == 'QC BULK')
                                                    {
                                                        return ['style'=>'background-color: #2196F3;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                                    }
                                                    else if($model->posisi == 'INKJET')
                                                    {
                                                        return ['style'=>'background-color:#42A5F5;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                                    }
                                                    else if($model->posisi == 'KEMAS 1')
                                                    {
                                                        return ['style'=>'background-color:#64B5F6;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                                    }
                                                    else if($model->posisi == 'KEMAS 2')
                                                    {
                                                        return ['style'=>'background-color:#90CAF9;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                                    }
                                                    else if($model->posisi == 'QC FG')
                                                    {
                                                        return ['style'=>'background-color:#BBDEFB;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                                    }
                                                },
                        ],
                        [
                            'attribute' => 'status',
                            'label' => 'Status',
                            //'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => function($model){
                                     if(strpos($model->status, 'Waiting For Next Operation') !== false)
                                    {
                                        return ['style'=>'background-color:#ECF0F1;color: #000000;width: 50px;text-align: center;font-weight:bold'];
                                    }
                                    else if($model->status == 'HOLD')
                                    {
                                        return ['style'=>'background-color:#E74C3C;color: #ffffff;width: 50px;text-align: center;font-weight:bold'];
                                    }
                                    else if($model->status == 'PAUSE')
                                    {
                                        return ['style'=>'background-color:#E74C3C;color: #ffffff;width: 50px;text-align: center;font-weight:bold'];
                                    }
                                    else if($model->status == 'Waiting For Next Shift')
                                    {
                                        return ['style'=>'background-color:#7F8C8D;color: #ffffff;width: 50px;text-align: center;font-weight:bold'];
                                    }
                                    else    
                                    {
                                        return ['style'=>'background-color:#2ECC71;color: #ffffff;width: 50px;text-align: center;font-weight:bold'];
                                    }
                            },
                        ],

                    ],
    ]); ?> -->

        <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            // 'filterModel'=>$searchModel,
            'responsiveWrap' => false,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'primary', 'heading'=>'Process Position Monitoring Board'],
            'toolbar'=>[
            ],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                [
                    'attribute'=>'nama_fg', 
                    'width'=>'210px',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(PosisiProses::find()->orderBy('nama_fg')->asArray()->all(), 'nama_fg', 'nama_fg'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'SKU'],
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 220px;font-size: 18px;font-family: Verdana','class'=>'info'],
                    'group'=>true,  // enable grouping
                    //'subGroupOf'=>1,
                    'groupHeader'=>function ($model, $key, $index, $widget) { // Closure method
                        return [
                            'mergeColumns'=>[[1,4]], // columns to merge in summary
                            'content'=>[             // content to show in each summary cell
                                1=>'Summary (' . $model->nama_fg . ')',
                                //7=>GridView::F_SUM,
                                // 8=>GridView::F_AVG,
                                // 9=>' hrs',
                                //9=>GridView::F_SUM,
                            ],
                            'contentFormats'=>[      // content reformatting for each summary cell
                                //7=>['format'=>'number', 'decimals'=>2],
                                8=>['format'=>'number', 'decimals'=>1],
                                //8=>['append'=>' hrs'],
                                //9=>['format'=>'number', 'decimals'=>1],
                            ],
                            'contentOptions'=>[      // content html attributes for each summary cell
                                1=>['style'=>'font-variant:medium-caps;font-size: 20px'],
                                // 7=>['style'=>'text-align:right'],
                                8=>['style'=>'text-align:right'],
                                // 9=>['style'=>'text-align:right'],
                            ],
                            // html attributes for group summary row
                            'options'=>['class'=>'success','style'=>'font-weight:bold;']
                        ];
                    }
                ],
                
                  [
                    'attribute'=>'nomo',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(PosisiProses::find()->orderBy('nomo')->asArray()->all(), 'nomo', 'nomo'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'No MO'],
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>2,
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 220px;font-size: 20px','class'=>'info'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                ],
                [
                    'attribute' => 'snfg',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(PosisiProses::find()->orderBy('snfg')->asArray()->all(), 'snfg', 'snfg'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'SNFG'],
                    'label' => 'SNFG',
                    'format' => 'raw',
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 200px;font-size: 20px','class'=>'info'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>3,       
                ],
                [
                    'attribute' => 'snfg_komponen',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(PosisiProses::find()->orderBy('snfg_komponen')->asArray()->all(), 'snfg_komponen', 'snfg_komponen'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Komponen'],
                    'label' => 'Komponen',
                    'format' => 'raw',
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 200px;font-size: 20px','class'=>'info'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>4,      
                ],
                [
                    'attribute' => 'start',
                    'label' => 'Start',
                    'format' => ['date', 'php:Y-m-d'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px'
                    ],
                ],
                [
                    'attribute' => 'due',
                    'label' => 'Due',
                    'format' => ['date', 'php:Y-m-d'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'contentOptions' => function($model){
                             if($model->ontime == 'OVERDUE')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                                                }else 
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'info'];
                                                }
                    },
                ],
                [
                    'attribute' => 'timestamp',
                    'label' => 'Last Update',
                    //'format' => 'raw',
                    'contentOptions' => ['style' => ' text-align: center;width: 200px'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                ],
                // 'delta',
                [
                    'attribute' => 'posisi',
                    'label' => 'Posisi',
                    'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'contentOptions' => function($model){
                    // Pewarnaan untuk Posisi
                                            if($model->posisi == 'PLANNER')
                                            {
                                                return ['style'=>'background-color:#1565C0;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                            }
                                            else if($model->posisi == 'PENIMBANGAN')
                                            {
                                                return ['style'=>'background-color:#1976D2;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                            }
                                            else if($model->posisi == 'PENGOLAHAN')
                                            {
                                                return ['style'=>'background-color:#1E88E5;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                            }
                                            else if($model->posisi == 'QC BULK')
                                            {
                                                return ['style'=>'background-color: #2196F3;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                            }
                                            else if($model->posisi == 'INKJET')
                                            {
                                                return ['style'=>'background-color:#42A5F5;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                            }
                                            else if($model->posisi == 'KEMAS 1')
                                            {
                                                return ['style'=>'background-color:#64B5F6;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                            }
                                            else if($model->posisi == 'KEMAS 2')
                                            {
                                                return ['style'=>'background-color:#90CAF9;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                            }
                                            else if($model->posisi == 'QC FG')
                                            {
                                                return ['style'=>'background-color:#BBDEFB;color: #ffffff;width: 120px;text-align: center;font-weight:bold;font-size: 18px'];
                                            }
                                        },
                ],
                        [
                            'attribute' => 'status',
                            'label' => 'Status',
                            //'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                            'contentOptions' => function($model){
                                     if(strpos($model->status, 'Waiting For Next Operation') !== false)
                                                        {
                                                            return ['style'=>'width: 80px;background-color:#ECF0F1;color: #000000;text-align: center;font-weight:bold'];
                                                        }
                                                        else if($model->status == 'HOLD')
                                                        {
                                                            return ['style'=>'width: 80px;background-color:#E74C3C;color: #ffffff;text-align: center;font-weight:bold'];
                                                        }
                                                        else if($model->status == 'PAUSE')
                                                        {
                                                            return ['style'=>'width: 80px;background-color:#E74C3C;color: #ffffff;text-align: center;font-weight:bold'];
                                                        }
                                                        else if($model->status == 'Waiting For Next Shift')
                                                        {
                                                            return ['style'=>'width: 80px;background-color:#7F8C8D;color: #ffffff;text-align: center;font-weight:bold'];
                                                        }
                                                        else    
                                                        {
                                                            return ['style'=>'width: 80px;background-color:#2ECC71;color: #ffffff;text-align: center;font-weight:bold'];
                                                        }
                            },
                        ],
                        [
                            'attribute' => 'lanjutan_split_batch',
                            'label' => 'SB',
                            //'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                                                'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 75px','class'=>'info'],
                        ],
                        // 'palet_ke',
             ],
            
        ]);
    
    ?>
</div>

 <?php Pjax::end(); ?>


<?php
$script = <<< JS
$(function() {
    // $('#offcanvasTrigger').click();
    setInterval(function () {document.getElementById("testAja").click();}, 10000);
    // setInterval(function(){ alert('5 s');}, 5000);
});


JS;
$this->registerJs($script);
?>