<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PosisiProses */

$this->title = 'Update Posisi Proses: ' . $model->snfg;
$this->params['breadcrumbs'][] = ['label' => 'Posisi Proses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->snfg, 'url' => ['view', 'id' => $model->snfg]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="posisi-proses-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
