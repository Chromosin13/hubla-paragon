<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\QcFgV2 */

?>
<div class="qc-fg-v2-update">

    <?= $this->render('_form-karantina', [
        'model' => $model,
    ]) ?>

</div>
