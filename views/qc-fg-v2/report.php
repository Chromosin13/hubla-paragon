<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use Da\QrCode\QrCode;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApTableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<?php
            $qrCode = (new QrCode($nosj))
                ->setSize(75)
                ->setMargin(5)
                ->useForegroundColor(0, 0, 0);
            $qrCode->writeFile('/var/www/html/flowreport/web/qr_suratjalan/code.png');
?>

<div class="ap-table-index">
<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
                        <img src="../web/qr_suratjalan/code.png" >
            <i class="fa fa-globe"></i>NOMOR SJ : <?php echo $nosj; ?>
             <?php
              $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
              echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($nosj, $generator::TYPE_CODE_128)) . '">';
              ?>
            <small class="pull-left"><?php echo Yii::$app->formatter->asDate($timestamp,'full'); ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Scan Barcode Untuk Menerima Surat Jalan
          </p>
      <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
      </div>
<br>
</section>
    <!-- <h1>Pembayaran</h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nama_fg',
            'snfg',
            'nourut',
            'alokasi',
            'qty',
            'jumlah_koli',
            'nobatch',
        ],
    ]);
    ?>

    <br>
    <!-- <h4>=================================================================</h4> -->
    <!-- <h4 'style'='font-color:red;'>Tambahan, palet tidak perlu dikirim ke NDC!</h4> -->

</div>
