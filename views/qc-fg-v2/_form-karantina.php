<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\ActiveField;
use kartik\dialog\Dialog;
use kartik\checkbox\CheckboxX;
use wbraganca\tagsinput\TagsinputWidget;
/* @var $this yii\web\View */
/* @var $model app\models\QcFgV2 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qc-fg-v2-form">


<div class="box box-widget widget-user-2">
    <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-green">
    
      <div class="widget-user-image">
        <img class="img-circle" src="../web/images/clipboard-icon.png" alt="User Avatar">
      </div>
      <!-- /.widget-user-image -->
      <h2 class="widget-user-username">Form Karantina QC FG</h2>
      <h5 class="widget-user-desc"></h5>
    </div>

    <div class="box-footer no-padding">
        <?php $form = ActiveForm::begin(['validateOnSubmit' => false]); ?>

        <?= $form->field($model, 'snfg')->textInput(['readOnly'=>true]) ?>

        <?= $form->field($model, 'qty')->textInput(['readOnly'=>true]) ?>

        <?= $form->field($model, 'qty_sample')->textInput(['readOnly'=>true]) ?>

        <?= $form->field($model, 'qty_reject')->textInput(['readOnly'=>true]) ?>

        <?= $form->field($model, 'status_check')->textInput(['readOnly'=>true]) ?>

        <?= $form->field($model, 'qty_karantina')->textInput(['type' => 'number']) ?>

        <label class="control-label">Inspektor Karantina</label>
        <?= $form->field($model, 'inspektor_karantina')->widget(TagsinputWidget::classname(), [
                            'clientOptions' => [
                                'trimValue' => true,
                                'allowDuplicates' => false,
                                'maxChars'=> 4,
                                'minChars'=> 4,
                            ],
                        ])->label(false)?>

	</div>




</div>

</div>

        <div id="form-create" class="form-group">

            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'SIAP KIRIM NDC', ['id'=>'submit','class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary btn-block']) ?>
            
        </div>

    <?php ActiveForm::end(); ?>

<?php
$script = <<< JS

// Hide Quantity Field

$('#submit').on('click', function(){
    var inspektor = $('#qcfgv2-inspektor_karantina').val();
    var qty = $('#qcfgv2-qty_karantina').val();
    if (inspektor == '' || qty == '')
    {
        window.alert('Lengkapi data terlebih dahulu !');
        return false;
    }else{
        return true;
    }
});

JS;
$this->registerJs($script);
?>