<?php

use app\models\SuratJalan;
use app\models\SuratJalanRincian;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\models\QcFgV2;

/* @var $this yii\web\View */
/* @var $model app\models\SuratJalanRincian */
/* @var $form yii\widgets\ActiveForm */
?>

<?php


?>

        <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <?php 
                    echo '<div class="widget-user-header bg-green">';
                
            ?>
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/clipboard-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username">Form Terima NDC</h2>
              <h5 class="widget-user-desc">Input Nama Operator</h5>
            </div>

<p></p>

<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title"> </h3>
  </div>
  <!-- /.box-header -->
  <!-- form start -->
  <form role="form">
    <div class="box-body">
      <div class="form-group">
        <label for="surjal">Nomor Surat Jalan</label>
        <input type="text" class="form-control" id="surjal" placeholder="Surat Jalan" value="<?php echo $surat_jalan; ?>" readOnly=true>
      </div>
      <div class="form-group">
        <label for="operator">Nama Operator</label>
        <input type="text" class="form-control" id="operator" placeholder="Nama Operator">
      </div>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
      <button type="button" id="submit" class="btn btn-primary">Next</button>
    </div>
  </form>
</div>
<?php
$script = <<< JS

document.getElementById("operator").focus();

$('#submit').on('click', function(){  

  var nosj = $('#surjal').val();
  var operator = $('#operator').val();

  //alert(barcode);
  window.location = 'index.php?r=log-receive-ndc/task&nosj='+nosj+'&operator='+operator;
    
});


JS;
$this->registerJs($script);
?>