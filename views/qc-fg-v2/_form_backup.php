<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\ActiveField;
use kartik\dialog\Dialog;
use kartik\checkbox\CheckboxX;
/* @var $this yii\web\View */
/* @var $model app\models\QcFgV2 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qc-fg-v2-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 


            echo $form->field($model, 'snfg', [
                            'addon' => [
                                'prepend' => [
                                    'content'=>'<i class="fa fa-barcode"></i>'
                                    
                                ],
                                'append' => [
                                    'content' => Html::button('Tekan Setelah Scan',['class'=>'btn btn-primary','id'=>'afterScnBtnSnfg']),
                                    'asButton' => true
                                ]            
                            ]
                        ]);


    
    ?>


    <div id ="quantity-field">

	    <?= $form->field($model, 'qty')->textInput() ?>

	    <?= $form->field($model, 'qty_sample')->textInput() ?>

	</div>


        <div class="form-group">

            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            
        </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$script = <<< JS

// Hide Quantity Field

	$('#quantity-field').hide(); 

// Hide Create new Field

    // $('#form-create').hide();


$('#qcfgv2-snfg').change(function(){  

    var snfg = $('#qcfgv2-snfg').val();


    $.get('index.php?r=qc-fg-v2/check-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        if(data.id==0){
            $('#form-create').show();
            alert("Jadwal Belum Ada, silahkan membuat Jadwal");
        }

        else if(data.id>0){

          alert('Jadwal Ditemukan, Halaman akan diteruskan');
          
          $('#form-create').hide();

          
          var snfg = $('#qcfgv2-snfg').val();

          window.location = "http://10.3.5.102/flowreport_training5/web/index.php?r=qc-fg-v2/create-rincian&snfg="+snfg;
          
          
        }
    
    });
});


JS;
$this->registerJs($script);
?>