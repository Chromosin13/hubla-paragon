<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SuratJalanRincian */

?>
<div class="surat-jalan-rincian-create">


    <?= $this->render('_form-surjal-rincian-qr', [
        'model' => $model,
        'id' => $id,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
