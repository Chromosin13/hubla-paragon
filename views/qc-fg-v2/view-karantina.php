<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\QcFgV2 */
?>
<div class="qc-fg-v2-view">

    <p>
        <?= Html::a('Scan QR Lagi (QC Timbang FG)', ['scan-karantina'], ['class' => 'btn btn-primary btn-block']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'snfg',
            'nourut',
            'qty_karantina',
            'qty',
            'qty_sample',
            'qty_reject',
            'timestamp_terima_karantina',
            'inspektor_karantina',
            'inspektor_qcfg',
        ],
    ]) ?>

</div>
