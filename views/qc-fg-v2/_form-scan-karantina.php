<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\ActiveField;
use kartik\dialog\Dialog;
use kartik\checkbox\CheckboxX;
use wbraganca\tagsinput\TagsinputWidget;
/* @var $this yii\web\View */
/* @var $model app\models\QcFgV2 */
/* @var $form yii\widgets\ActiveForm */
?>


<!-- CSS For Video Selector -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="ZXing for JS">

  <title>ZXing TypeScript | Decoding from camera stream</title>

  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">




</head>



<div class="qc-fg-v2-form">

        <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-green">
            
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/clipboard-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username">Scan Karantina QC FG</h2>
              <h5 class="widget-user-desc"></h5>
            </div>
        <p></p>

        <div class="col-sm-12">
          <div class="box-body">
              <div class="box-body">

                <div id="sourceSelectPanel" style="display:none">
                  <label for="sourceSelect">Switch Camera</label>
                  <select id="sourceSelect" style="max-width:400px">x
                  </select>
                </div>

                <div>
                  <video id="video" width="330" height="300" style="border: 0.5px solid gray"></video>
                </div>
                  
              </div>
          </div>
        </div>

        <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-body">
              <div class="box-group bg-blue-gradient" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                
                <div class="panel box box-primary">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false">
                        Input Manual ID PALET (Jika kamera tidak aktif)
                      </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false">
                    <div class="box-body">
                      <div class="col-md-6 column" style="margin-left:0px; color: black; margin-top:0px;">
                        <label for="id_manual">ID Palet</label>
                        <input type="text" autocomplete="off" name="id_manual" id="id_manual" style="color: black;" >
                      </div>
                      <button onclick="check()" style="width:50%; display: block; margin-left: auto; margin-right: auto;">Check</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

<!-- Javascript -->
  <!-- Calling ZXing API CDN -->
  <!-- <script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script> -->
  <script type="text/javascript" src="zxing.js"></script>
  <!-- Code -->
  <script type="text/javascript">

      let selectedDeviceId;
      const codeReader = new ZXing.BrowserMultiFormatReader()
      console.log('ZXing code reader initialized')
      codeReader.getVideoInputDevices()
        .then((videoInputDevices) => {
          const sourceSelect = document.getElementById('sourceSelect')
          // selectedDeviceId = videoInputDevices[1].deviceId
          if (videoInputDevices.length >= 1) {
            videoInputDevices.forEach((element) => {
              const sourceOption = document.createElement('option')
              sourceOption.text = element.label
              sourceOption.value = element.deviceId
              sourceSelect.appendChild(sourceOption)
            })

            // When Changing Camera, Reset Scanner and Execute Canvas
            sourceSelect.onchange = () => {
              selectedDeviceId = sourceSelect.value;

              codeReader.reset()
              codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
                
                if (result) {

                  var input = result.text;
                  var fields = input.split('@');

                  if (fields.length > 1){
                    var snfg= fields[0];
                    var nourut = fields[1];
                    var id_palet = null;

                    window.location = 'index.php?r=qc-fg-v2/update-karantina&snfg='+snfg+'&nourut='+nourut+'&id_palet='+id_palet;

                  //if qr content is new format
                  }else{
                    var id_palet ='';
                    var odoo_code ='';
                    var qty = 0;
                    var nourut = 0;
                    var nobatch ='';
                    var exp_date ='';
                    var fields_new = input.split('|');
                    // var id_palet = fields_new[I];
                    fields_new.forEach(function(content,index){
                      if (content.substring(0,2)=='I:'){
                        id_palet  = content.substring(2);
                      // } else if (content.substring(0,2)=='O:'){
                      //   odoo_code  = content.substring(2);
                      // } else if (content.substring(0,2)=='Q:'){
                      //   qty  = content.substring(2);
                      // } else if (content.substring(0,2)=='N:'){
                      //   nourut  = content.substring(2);
                      // } else if (content.substring(0,2)=='B:'){
                      //   nobatch  = content.substring(2);
                      // } else if (content.substring(0,2)=='E:'){
                      //   exp_date  = content.substring(2);
                      }else{

                      }
                    });

                    var snfg = null;

                    // console.log(id_palet);

                    window.location = 'index.php?r=qc-fg-v2/update-karantina&snfg='+snfg+'&nourut='+nourut;

                  }
                  // console.log(fields);
                }
                if (err && !(err instanceof ZXing.NotFoundException)) {
                  console.error(err)
                  document.getElementById('result').textContent = err
                }

              }) // on scanned 
            }; // onchange

            const sourceSelectPanel = document.getElementById('sourceSelectPanel')
            sourceSelectPanel.style.display = 'block'
          }

          // Initialize Execute Canvas when Page first loaded 
          codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
             if (result) {

                  var input = result.text;
                  var fields = input.split('@');

                  if (fields.length > 1){
                    var snfg= fields[0];
                    var nourut = fields[1];
                    var id_palet = null;

                    window.location = 'index.php?r=qc-fg-v2/update-karantina&snfg='+snfg+'&nourut='+nourut;

                  //if qr content is new format
                  }else{
                    var id_palet ='';
                    var odoo_code ='';
                    var qty = 0;
                    var nourut = 0;
                    var nobatch ='';
                    var exp_date ='';
                    var fields_new = input.split('|');
                    // var id_palet = fields_new[I];
                    fields_new.forEach(function(content,index){
                      if (content.substring(0,2)=='I:'){
                        id_palet  = content.substring(2);
                      // } else if (content.substring(0,2)=='O:'){
                      //   odoo_code  = content.substring(2);
                      // } else if (content.substring(0,2)=='Q:'){
                      //   qty  = content.substring(2);
                      // } else if (content.substring(0,2)=='N:'){
                      //   nourut  = content.substring(2);
                      // } else if (content.substring(0,2)=='B:'){
                      //   nobatch  = content.substring(2);
                      // } else if (content.substring(0,2)=='E:'){
                      //   exp_date  = content.substring(2);
                      }else{

                      }
                    });

                    // console.log(id_palet);
                    window.location = 'index.php?r=qc-fg-v2/update-karantina&id_palet='+id_palet;

                  }
                  // console.log(fields);

                }
            if (err && !(err instanceof ZXing.NotFoundException)) {
              console.error(err)
              document.getElementById('result').textContent = err
            }
          }) // on scanned


        })
        .catch((err) => {
          console.error(err)
        })
  </script> 

<script>


  function check(){
    var id_palet = $('#id_manual').val();
    if (id_palet != ''){
      $.getJSON("index.php?r=qc-fg-v2/check-id-palet",{id_palet : id_palet}, function(data){
        if (data.status=='exist'){
          window.location = 'index.php?r=qc-fg-v2/update-karantina&id_palet='+id_palet;
        }else{
          window.alert('ID Palet tidak ditemukan !');
        }

      });
    }else{
      window.alert('ID Palet kosong!');
    }
  }
</script>

      
<?php $form = ActiveForm::begin(); ?>


<?php ActiveForm::end(); ?>

</div>


<?php
$script = <<< JS

// Hide Quantity Field



JS;
$this->registerJs($script);
?>