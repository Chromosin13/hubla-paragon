<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\QcFgV2 */

// $this->title = '';
// $this->params['breadcrumbs'][] = ['label' => 'Qc Fg V2s', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="qc-fg-v2-create">



    <?= $this->render('_form-rincian', [
        'model' => $model,
        'snfg' => $snfg,
        'category' => $category,
        'nobatch' => $nobatch,
        'nama_fg' => $nama_fg,
	    'searchModel' => $searchModel,
	    'dataProvider' => $dataProvider,
    ]) ?>

</div>
