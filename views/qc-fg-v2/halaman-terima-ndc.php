<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QcFgV2Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="qc-fg-v2-index">

    <?= Html::a('Scan With QR', ['qc-fg-v2/scan-konfirmasi-terima-ndc-qr'], ['class' => 'btn btn-primary btn-block']) ?>

    <?= Html::a('Scan With Barcode', ['qc-fg-v2/scan-barcode-terima-ndc'], ['class' => 'btn btn-info btn-block']) ?>
<br><br>
    <div class="row">
        <div class="col-md-12">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title" style = "text-align: center;">List Surat Jalan</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">

				<div class="row">
			        <div class="col-md-12">
			          <!-- Custom Tabs -->
			          <div class="nav-tabs-custom">
			            <ul class="nav nav-tabs">
			              <li class="active"><a href="#tab_1" data-toggle="tab">Surjal Diterima</a></li>
			              <li><a href="#tab_2" data-toggle="tab">Surjal Belum Diterima</a></li>
			            </ul>
			            <div class="tab-content">
			              <div class="tab-pane active" id="tab_1">
			                <?= GridView::widget([
						        'dataProvider' => $dataRecieved,
						        'filterModel' => $searchModel,
						        'responsiveWrap' => false,
						        'floatHeader'=>'true',
						        'floatHeaderOptions' => ['position' => 'absolute',],
						        'columns' => [
						            ['class' => 'yii\grid\SerialColumn'],
						            'nosj',
						            'timestamp',
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{rincian-item}',
                'contentOptions' => ['style' => 'min-width:150px;'],
          //       'rowOptions'=>function($model){
		        //     if($model->is_downloaded == 1){
		        //         return ['class' => 'success'];
		        //     }
		        // },
                'buttons' => [
                    'rincian-item' => function ($url,$model) {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/clipboard-icon.png" alt="Terima" width="30" height="30">',
                            $url, 
                            [
                                'title' => 'LIHAT RINCIAN',
                                'data-pjax' => '0',
                                // 'data-confirm' => Yii::t('app', 'Anda Yakin Memilih CEK HOMOGENITAS?'),
                            ]
                        );
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'rincian-item'){
                        $url ='index.php?r=surat-jalan-rincian/rincian-item&id='.$model->id;
                        return $url;                           
                    }
                    
                }
            ],
						            
						        ],
						    ]); 
    						?>

			              </div>
			              <!-- /.tab-pane -->
			              <div class="tab-pane" id="tab_2">
			                <?= GridView::widget([
						        'dataProvider' => $dataNotrecieved,
						        'responsiveWrap' => false,
						        'floatHeader'=>'true',
						        'floatHeaderOptions' => ['position' => 'absolute',],
						        'columns' => [
						            ['class' => 'yii\grid\SerialColumn'],
						            'nosj',
						            'timestamp',
				
						            // [
						            //       'attribute' => 'nourut',
						            //       'label'=>'PALET',
						            // ],
						            // [
						            //       'attribute' => 'qty',
						            //       'label'=>'QTY',
						            // ],
						        ],
						    ]); 
    						?>
			              </div>
			            </div>
			            <!-- /.tab-content -->
			          </div>
			          <!-- nav-tabs-custom -->
			        </div>
			      </div>


              The body of the box
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
</div>
