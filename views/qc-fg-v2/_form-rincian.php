<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputSnfg;
use app\models\FlowPraKemas;
use app\models\QcFgV2;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\Select2;
use app\models\AlokasiKirim;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\QcFgV2 */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="qc-fg-v2-form">

<?php

    Modal::begin([
            'header'=>'<h4>Pilih Printer</h4>',
            'id' => 'pilihprinter',
            'size' => 'modal-sm',
        ]);

    echo "<div id='modalPilihPrinter'></div>";

    Modal::end();

?>

<?php $sql="
                select case when count(id)=0 then 0::numeric
                        else coalesce(sum(is_done),0)::numeric/count(id)::numeric
                        end as id,
                        case when coalesce(sum(is_done),0)::numeric=count(id) then 1::numeric
                        else 0 end as is_done
                from qc_fg_v2
                where snfg='".$snfg."'
             ";

        $ch = QcFgV2::findBySql($sql)->one();

?>

<!-- <div class="modal fade" id="pilihprinter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id='modalPilihPrinter'></div>
        
      </div>
    </div>
  </div>
</div> -->


<?php $form = ActiveForm::begin(['id'=>'form-qc']); ?>
    <div class="box box-widget widget-user-2">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-yellow">
          <div class="widget-user-image">
            <img class="img-circle" src="../web/images/clipboard-icon.png" alt="User Avatar">
          </div>
          <!-- /.widget-user-image -->
          <h2 class="widget-user-username"><?=$snfg?></h2>
          <h5 class="widget-user-desc"><?=$nama_fg?></h5>
        </div>


        <div class="box box-body">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'qty')->textInput()?>
                </div>
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'qty_sample')->textInput()?>
                </div>
            </div>
            <div class="row">                
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'qty_reject')->textInput()?>
                </div>
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'jumlah_koli')->textInput()?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'nobatch')->textInput(['value'=>$nobatch])?>
                </div>
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($model, 'exp_date')->textInput(['readOnly'=>true])?>
                </div>
            </div>
              <?php
                  echo $form->field($model, 'alokasi')->widget(Select2::classname(), [
                      'data' => ArrayHelper::map(AlokasiKirim::find()->orderBy(['tujuan'=>SORT_ASC])->all()
                      ,'tujuan','nama'),
                      'options' => ['placeholder' => 'Pilih Alokasi'],
                      'pluginOptions' => [
                          'allowClear' => true
                      ],
                  ]); ?>
              <?= $form->field($model, 'inspektor_qcfg')->widget(TagsinputWidget::classname(), [
                        'clientOptions' => [
                            'trimValue' => true,
                            'allowDuplicates' => false,
                            'maxChars'=> 4,
                            'minChars'=> 4,
                        ]
                    ])?>
        </div>
    </div>



    <?= $form->field($model, 'nourut')->textInput(['readOnly'=>true]) ?>

    <div id = "hidden-field">
        <?= $form->field($model, 'snfg')->textInput(['value' => $snfg,'readOnly'=>true]) ?>
        <?= $form->field($model, 'status')->textInput(['value' => 'PROSES KARANTINA','readOnly'=>true]) ?>
    </div>

    <div id ="quantity-field">





	</div>


    <div id="form-add" class="form-group">


        <span class="pull-right">

        <?php

        if(in_array(Yii::$app->user->identity->username, array("ndc","karantina"))) {

        }else{


            if(($ch->id)<1 && !in_array(Yii::$app->user->identity->username, array("ndc","karantina")) ){
                echo Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['onclick'=>"add()",'id'=>'btnAdd','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
            }else{

            }


        }


        ?></span>

        <?=Html::a('Home', ['qc-fg-v2/create'], ['class' => 'btn btn-primary']);?>
        <!-- <?= Html::a('Back to Halaman Pengecekan', ['pengecekan-umum/scan-snfg-inspeksi'], ['class' => 'btn btn-warning']) ?> -->

        <?= Html::a('Revisi Nama FG', ['scm-planner/get-nama-fg-odoo', 'snfg' => $snfg], ['class' => 'btn bg-purple']);?>
    </div>


    <?php ActiveForm::end(); ?>

</div><br>
<!-- Gridview -->
<div>
    <?php 
        $data_batch = ArrayHelper::map(FlowInputSnfg::find()->where(['snfg'=>$snfg])->orderBy(['id'=>SORT_ASC])->all(),'nobatch','nobatch'); 
        if (empty($data_batch)){
            $data_batch = ArrayHelper::map(FlowPraKemas::find()->where(['snfg'=>$snfg])->orderBy(['id'=>SORT_ASC])->all(),'nobatch','nobatch'); 
        }
    ?>
    <?php
        
        if(in_array(Yii::$app->user->identity->username, array("qcfg","admin"))){
            echo  GridView::widget([
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'rowOptions' => function ($model) {
                if ($model->status_check == 'REJECT')
                {
                    return ['style'=>'background-color: #F1948A'];
                }
            },
            'columns' => [
                'nourut',
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'template' => '{release} {pending} {rework} {cek-mikro} {reject}',
                    'contentOptions' => ['style' => 'min-width:170px;'],
                    'buttons' => [
                        'release' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/release.png" alt="Terima" width="42" height="42">',
                                $url,
                                [
                                    'title' => 'RELEASE',
                                    'data-pjax' => '0',
                                    'data-confirm' => Yii::t('app', 'RELEASE?'),
                                ]
                            );
                        },
                        'pending' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/pending-1.png" alt="Terima" width="42" height="42">',
                                $url,
                                [
                                    'title' => 'PENDING',
                                    'data-pjax' => '0',
                                    'data-confirm' => Yii::t('app', 'Pending?'),
                                ]
                            );
                        },
                        'rework' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/rework.png" alt="Terima" width="42" height="42">',
                                $url,
                                [
                                    'title' => 'REWORK',
                                ]
                            );
                        },
                        'cek-mikro' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/cek-mikro.png" alt="Terima" width="42" height="42">',
                                $url,
                                [
                                    'title' => 'PARALLEL MIKRO',
                                    'data-pjax' => '0',
                                    'data-confirm' => Yii::t('app', 'Parallel Mikro?'),
                                ]
                            );
                        },
                        'reject' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/reject.png" alt="Terima" width="42" height="42">',
                                $url,
                                [
                                    'title' => 'REJECT',
                                    'data-pjax' => '0',
                                    'data-confirm' => Yii::t('app', 'Reject?'),
                                ]
                            );
                        },
                    ],
                    'visibleButtons' => [
                        'release' => function($model,$key,$index){
                                    if($model->status_check=="RELEASE"){
                                        return false;
                                    } else {
                                        return true;
                                    }
                        },
                        'pending' => function($model,$key,$index){
                                    if($model->status_check=="PENDING"){
                                        return false;
                                    } else {
                                        return true;
                                    }
                        },
                        'rework' => function($model,$key,$index){
                                    if($model->status_check=="REWORK"){
                                        return false;
                                    } else {
                                        return true;
                                    }
                        },
                        'cek-mikro' => function($model,$key,$index){
                                    if($model->status_check=="CEK MIKRO"){
                                        return false;
                                    } else {
                                        return true;
                                    }
                        },
                        'reject' => function($model,$key,$index){
                                    if($model->status_check=="REJECT"){
                                        return false;
                                    } else {
                                        return true;
                                    }
                        },
                    ],
                ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'template' => '{print-zebra} {print-label} {terima-karantina} {terima-ndc} {done}',
                    'contentOptions' => ['style' => 'max-width:40px;'],
                    'buttons' => [
                        'terima-karantina' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/dolly-icon.png" alt="Terima" width="42" height="42">',
                                $url,
                                [
                                    'title' => 'SIAP KIRIM NDC',
                                    'data-pjax' => '0',
                                    'data-confirm' => Yii::t('app', 'Anda yakin menerima barang di Karantina?'),
                                ]
                            );
                        },
                        'terima-ndc' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/truck-icon.png" alt="Terima" width="42" height="42">',
                                $url,
                                [
                                    'title' => 'NDC TERIMA BARANG',
                                    'data-pjax' => '0',
                                    'data-confirm' => Yii::t('app', 'Anda yakin menerima barang di NDC?'),
                                ]
                            );
                        },
                        'print-label' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/printer-icon.png" alt="Terima" width="42" height="42">',
                                $url,
                                [
                                    'title' => 'Print Label?',
                                ]
                            );
                        },
                        'done' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/check-icon.png" alt="Terima" width="42" height="42">',
                                '#',
                                [
                                    'title' => 'DONE',
                                ]
                            );
                        },
                        'print-zebra' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/cmyk-icon.png" alt="Terima" width="42" height="42">',
                                $url,
                                [
                                    // 'title' => 'PRINT ZEBRA',
                                    'title' => Yii::t('app', 'PRINT ZEBRA'),
                                    'class' => 'modalPilihPrinter grid-action',
                                    'data-toogle'=>'pilihprinter',
                                    'data-target'=>'#activity-modal',
                                    'value'=>Url::to('index.php?r=qc-fg-v2/printer-mapping&id='.$model->id)
                                ]

                            );
                        }




                    ],
                    'visibleButtons' => [
                        'terima-karantina' => function($model,$key,$index){
                                    if($model->status=="PROSES KARANTINA" && in_array(Yii::$app->user->identity->username, ['admin','qcfg'])){
                                        return true;
                                    } else {
                                        return false;
                                    }
                        },
                        'terima-ndc' => function($model,$key,$index){
                                    if($model->status=="SIAP KIRIM KE NDC" && Yii::$app->user->identity->username == 'admin'){
                                        return true;
                                    } else {
                                        return false;
                                    }
                        },
                        'print-label' => function($model,$key,$index){
                                    if(in_array($model->status,array('PROSES KARANTINA','SUDAH DITERIMA NDC','SIAP KIRIM KE NDC','REJECT'))){
                                        return true;
                                    } else {
                                        return false;
                                    }
                        },
                        'print-zebra' => function($model,$key,$index){
                                    if(in_array($model->status,array('PROSES KARANTINA','SUDAH DITERIMA NDC','SIAP KIRIM KE NDC'))){
                                        return true;
                                    } else {
                                        return false;
                                    }
                        },
                        'done' => function($model,$key,$index){
                                    if($model->status=="SUDAH DITERIMA NDC" && Yii::$app->user->identity->username == 'admin'){
                                        return true;
                                    } else {
                                        return false;

                                    }
                        },
                    ],
                ],
                'snfg',
                [
                    'class'=>'kartik\grid\EditableColumn',
                    'attribute'=>'nobatch',
                    'label' => 'Nobatch',
                    'editableOptions' => [
                        'asPopover' => false,
                        'inputType' => 'dropDownList',
                        'data' => $data_batch,
                        // 'options' => ['class'=>'form-control', 'prompt'=>'Select status...'],
                    ],
                ],
                [
                    'class'=>'kartik\grid\EditableColumn',
                    'attribute'=>'alokasi',
                    'label' => 'Alokasi',
                    'editableOptions' => [
                        'asPopover' => false,
                        'inputType' => 'dropDownList',
                        'data' => ArrayHelper::map(AlokasiKirim::find()->orderBy(['tujuan'=>SORT_ASC])->all()
                                ,'tujuan','nama'),

                    ],
                ],
                [
                    'class'=>'kartik\grid\EditableColumn',
                    'attribute'=>'jumlah_koli',
                    'label' => 'Jumlah Koli',
                    'editableOptions' => [
                        'asPopover' => false,
                    ],
                ],
                [
                    'class'=>'kartik\grid\EditableColumn',
                    'attribute'=>'qty',
                    'label' => 'Qty QCFG',
                    'editableOptions' => [
                        'asPopover' => false,
                    ],
                ],
                // 'qty_karantina',
                [
                    'class'=>'kartik\grid\EditableColumn',
                    'attribute'=>'qty_karantina',
                    'label' => 'Qty KARANTINA',
                    'editableOptions' => [
                        'asPopover' => false,
                    ],
                ],
                [
                    'class'=>'kartik\grid\EditableColumn',
                    'attribute'=>'qty_sample',
                    'label' => 'Qty Sample',
                    'editableOptions' => [
                        'asPopover' => false,
                    ],
                ],
                [
                    'class'=>'kartik\grid\EditableColumn',
                    'attribute'=>'qty_reject',
                    'label' => 'Qty Reject',
                    'editableOptions' => [
                        'asPopover' => false,
                    ],
                ],
                // 'qty_sample',
                // 'qty_reject',
                'inspektor_qcfg',
                'inspektor_karantina',
                [
                        'attribute' => 'status',
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'contentOptions' => function($model){
                                 if($model->status == 'PROSES KARANTINA')
                                                    {
                                                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                                                    }
                                else if($model->status == 'SIAP KIRIM KE NDC')
                                                    {
                                                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'warning'];
                                                    }
                                else                {
                                                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'success'];
                                                    }
                        },
                ],
                'timestamp_kirim',
                'timestamp_terima_karantina',
                'timestamp_terima_ndc',
                'status_check',
                'timestamp_check',
                'is_done',
                ['class' => 'kartik\grid\ActionColumn',
                 'template' => '{delete}'],


            ],
            ]);


        } else if(Yii::$app->user->identity->username=="ndc") {
            echo  GridView::widget([
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'rowOptions' => function ($model) {
                if ($model->status_check == 'REJECT')
                {
                    return ['style'=>'background-color: #F1948A'];
                }
            },
            'columns' => [
                'nourut',
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'template' => '{print-label} {terima-karantina} {terima-ndc} {done}',
                    'contentOptions' => ['style' => 'max-width:40px;'],
                    'buttons' => [
                        'terima-karantina' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/dolly-icon.png" alt="Terima" width="42" height="42">',
                                $url,
                                [
                                    'title' => 'SIAP KIRIM NDC',
                                    'data-pjax' => '0',
                                    'data-confirm' => Yii::t('app', 'Anda yakin menerima barang di Karantina?'),
                                ]
                            );
                        },
                        'terima-ndc' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/truck-icon.png" alt="Terima" width="42" height="42">',
                                $url,
                                [
                                    'title' => 'NDC TERIMA BARANG',
                                    'data-pjax' => '0',
                                    'data-confirm' => Yii::t('app', 'Anda yakin menerima barang di NDC?'),
                                ]
                            );
                        },
                        'print-label' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/printer-icon.png" alt="Terima" width="42" height="42">',
                                $url,
                                [
                                    'title' => 'Print Label?',
                                ]
                            );
                        },
                        'done' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/check-icon.png" alt="Terima" width="42" height="42">',
                                '#',
                                [
                                    'title' => 'DONE',
                                ]
                            );
                        },


                    ],
                    'visibleButtons' => [
                        'terima-karantina' => function($model,$key,$index){
                                    if($model->status=="PROSES KARANTINA"){
                                        return true;
                                    } else {
                                        return false;
                                    }
                        },
                        'terima-ndc' => function($model,$key,$index){
                                    if($model->status=="SIAP KIRIM KE NDC"){
                                        return true;
                                    } else {
                                        return false;
                                    }
                        },
                        'print-label' => function($model,$key,$index){
                                    if($model->status=="PROSES KARANTINA"){
                                        return true;
                                    } else {
                                        return false;
                                    }
                        },
                        'done' => function($model,$key,$index){
                                    if($model->status=="SUDAH DITERIMA NDC"){
                                        return true;
                                    } else {
                                        return false;

                                    }
                        },
                    ],
                ],
                'snfg',
                'alokasi',
                'jumlah_koli',
                [
                    'attribute'=>'qty',
                    'label' => 'Qty QCFG',
                ],
                'qty_karantina',
                // 'qty_karantina',
                'qty_sample',
                'qty_reject',
                'inspektor_qcfg',
                'inspektor_karantina',
                [
                        'attribute' => 'status',
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'contentOptions' => function($model){
                                 if($model->status == 'PROSES KARANTINA')
                                                    {
                                                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                                                    }
                                else if($model->status == 'SIAP KIRIM KE NDC')
                                                    {
                                                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'warning'];
                                                    }
                                else                {
                                                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'success'];
                                                    }
                        },
                ],
                'timestamp_kirim',
                'timestamp_terima_karantina',
                'timestamp_terima_ndc',
                'status_check',
                'timestamp_check',
                'is_done',
                ['class' => 'kartik\grid\ActionColumn',
                 'template' => '{delete}'],


            ],
            ]);
        }
        else{

            echo  GridView::widget([
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'rowOptions' => function ($model) {
                if ($model->status_check == 'REJECT')
                {
                    return ['style'=>'background-color: #F1948A'];
                }
            },
            'columns' => [
                'nourut',
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'template' => '{terima-karantina} {terima-ndc} {done}',
                    'contentOptions' => ['style' => 'max-width:40px;'],
                    'buttons' => [
                        'terima-karantina' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/dolly-icon.png" alt="Terima" width="42" height="42">',
                                $url,
                                [
                                    'title' => 'SIAP KIRIM NDC',
                                    'data-pjax' => '0',
                                    'data-confirm' => Yii::t('app', 'Anda yakin menerima barang di Karantina?'),
                                ]
                            );
                        },
                        'terima-ndc' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/truck-icon.png" alt="Terima" width="42" height="42">',
                                $url,
                                [
                                    'title' => 'NDC TERIMA BARANG',
                                    'data-pjax' => '0',
                                    'data-confirm' => Yii::t('app', 'Anda yakin menerima barang di NDC?'),
                                ]
                            );
                        },
                        'done' => function ($url,$model) {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/check-icon.png" alt="Terima" width="42" height="42">',
                                '#',
                                [
                                    'title' => 'DONE',
                                ]
                            );
                        },
                    ],
                    'visibleButtons' => [
                        'terima-karantina' => function($model,$key,$index){
                                    if($model->status=="PROSES KARANTINA"){
                                        return true;
                                    } else {
                                        return false;
                                    }
                        },
                        'terima-ndc' => function($model,$key,$index){
                                    if($model->status=="SIAP KIRIM KE NDC"){
                                        return true;
                                    } else {
                                        return false;
                                    }
                        },
                        'done' => function($model,$key,$index){
                                    if($model->status=="SUDAH DITERIMA NDC"){
                                        return true;
                                    } else {
                                        return false;

                                    }
                        },
                    ],
                ],
                'snfg',
                [
                    'class'=>'kartik\grid\EditableColumn',
                    'attribute'=>'nobatch',
                    'label' => 'Nobatch',
                    'editableOptions' => [
                        'asPopover' => false,
                    ],
                ],
                [
                    'class'=>'kartik\grid\EditableColumn',
                    'attribute'=>'alokasi',
                    'label' => 'Alokasi',
                    'editableOptions' => [
                        'asPopover' => false,
                        'inputType' => 'dropDownList',
                        'data' => ArrayHelper::map(AlokasiKirim::find()->orderBy(['tujuan'=>SORT_ASC])->all()
                                ,'tujuan','nama'),

                    ],
                ],
                // 'alokasi',
                // 'jumlah_koli',
                [
                    'attribute'=>'qty',
                    'label' => 'Qty QCFG',
                ],
                [
                        'class'=>'kartik\grid\EditableColumn',
                        'attribute'=>'qty_karantina',
                        'label' => 'Qty KARANTINA',
                        'editableOptions' => [
                            'asPopover' => false,
                        ],
                ],
                // 'qty_karantina',
                'qty_sample',
                'qty_reject',
                'inspektor_qcfg',
                [
                        'class'=>'kartik\grid\EditableColumn',
                        'attribute'=>'inspektor_karantina',
                        'label' => 'Inspektor Karantina',
                        'editableOptions' => [
                            'asPopover' => false,
                        ],
                ],
                [
                        'attribute' => 'status',
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'contentOptions' => function($model){
                                 if($model->status == 'PROSES KARANTINA')
                                                    {
                                                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                                                    }
                                else if($model->status == 'SIAP KIRIM KE NDC')
                                                    {
                                                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'warning'];
                                                    }
                                else                {
                                                        return ['style' => ' text-align: center;font-weight:bold;width: 150px;font-size: 18px','class'=>'success'];
                                                    }
                        },
                ],
                'timestamp_kirim',
                'timestamp_terima_karantina',
                'timestamp_terima_ndc',
                'status_check',
                'timestamp_check',
                'is_done',
                ['class' => 'kartik\grid\ActionColumn',
                 'template' => '{delete}'],


            ],
            ]);
        }

    ?>

<br \>

<?php



        if(($ch->id)<1 && !in_array(Yii::$app->user->identity->username, array("ndc","karantina")) ){
            echo Html::a('Complete', ['qc-fg-v2/is-done', 'snfg' => $snfg], ['class' => 'btn btn-danger']);
        }else if(($ch->is_done==1) && !in_array(Yii::$app->user->identity->username, array("ndc","karantina")) ){
            echo Html::a('Undone', ['qc-fg-v2/undone', 'snfg' => $snfg], ['class' => 'btn btn-primary']);
        }

?>





</div>

<!-- Button trigger modal -->
<!-- <button id="alert-null" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Launch demo modal
</button> -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function add(){
    var qty = document.getElementById('qcfgv2-qty').value;
    var qty_sample = document.getElementById('qcfgv2-qty_sample').value;
    var qty_reject = document.getElementById('qcfgv2-qty_reject').value;
    var jumlah_koli = document.getElementById('qcfgv2-jumlah_koli').value;
    var nobatch = document.getElementById('qcfgv2-nobatch').value;
    var exp_date = document.getElementById('qcfgv2-exp_date').value;
    var alokasi = document.getElementById('qcfgv2-alokasi').value;
    var inspektor_qcfg = document.getElementById('qcfgv2-inspektor_qcfg').value;
    if (qty == '' || qty_reject == '' || qty_sample == '' || jumlah_koli == '' || nobatch == '' || exp_date == '' || alokasi == '' || inspektor_qcfg == '') {
        // console.log('null');
        document.getElementById('btnAdd').disabled = false;
    }else{
        document.getElementById('btnAdd').disabled = true;
        $('#form-qc').submit();
    }
}
</script>

<?php
$script = <<< JS
// $('#alert-null').hide();

//Disable enter to submit form
$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
});

    $(function(){
      // get the click of the create button
      $(".modalPilihPrinter").click(function() {
          $('#pilihprinter').modal('show')
            // find id
            .find('#modalPilihPrinter')
            // load id
            .load($(this).attr('value'));

          return false;
      });

    });


    $('#hidden-field').hide();

    var nobatch = $('#qcfgv2-nobatch').val();
    var snfg = $('#qcfgv2-snfg').val();

    $.get('index.php?r=qc-fg-v2/get-nourut',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);

        $('#qcfgv2-nourut').attr('value',data.nourut);

    });


    $.get('index.php?r=qc-fg-v2/get-exp-date',{snfg:snfg,nobatch:nobatch},function(data){
        var data = $.parseJSON(data);
        if (data.exp_date != ''){
            $('#qcfgv2-exp_date').attr('value',data.exp_date);
        }else{
            $('#qcfgv2-exp_date').attr('value','');
            alert('Expired Date tidak ditemukan untuk nomor batch tersebut');
        }
    });

    $('#qcfgv2-nobatch').on('change', function(){
        var nobatch = $('#qcfgv2-nobatch').val();
        $.get('index.php?r=qc-fg-v2/get-exp-date',{snfg:snfg,nobatch:nobatch},function(data){
            var data = $.parseJSON(data);
            if (data.exp_date != ''){
                $('#qcfgv2-exp_date').attr('value',data.exp_date);
            }else{
                alert('Expired Date tidak ditemukan untuk nomor batch tersebut');
                // $('#exampleModal').appendTo('body').modal('show');
                // $('#alert-null').trigger('click');
                $('#qcfgv2-exp_date').attr('value','');
            }
        });
    });




JS;
$this->registerJs($script);
?>
