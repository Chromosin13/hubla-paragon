<?php

use app\models\SuratJalan;
use app\models\SuratJalanRincian;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\models\QcFgV2;

/* @var $this yii\web\View */
/* @var $model app\models\SuratJalanRincian */
/* @var $form yii\widgets\ActiveForm */
?>

<?php


?>

        <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <?php 
                    echo '<div class="widget-user-header bg-green">';
                
            ?>
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/clipboard-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username">Scan Terima NDC Barcode</h2>
              <h5 class="widget-user-desc">Terima NDC</h5>
            </div>

<p></p>

<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Input Barcode (Scan Barcode Garis)</h3>
            </div>
            <div class="box-body">
              <div class="input-group">
                <span class="input-group-addon">Barcode</span>
                <input type="text" id="barcode" class="form-control" placeholder="Scan Barcode disini">
              </div>
              <br>

              
            <!-- /.box-body -->
            </div>
</div>
<?php
$script = <<< JS

document.getElementById("barcode").focus();

$('#barcode').change(function(){  

  var barcode = $('#barcode').val().trim();
  if (barcode != ''){
    $.get('index.php?r=surat-jalan/check-surat-jalan',{ surat_jalan : barcode }, function(data){
      console.log(data);
      if (data == 1){
        window.location = 'index.php?r=qc-fg-v2/form-terima-ndc&surat_jalan='+barcode;
      }else if (data==2){
        window.alert('Surat Jalan sudah diterima!');
      }else if (data==3){
        window.location = 'index.php?r=surat-jalan-rincian/konfirmasi-terima-ndc-qr&surat_jalan='+barcode;
      }else{
        window.alert('Nomor Surat Jalan tidak ditemukan!');
      }

    });
  }else{
    window.alert('Nomor Surat Jalan kosong!');
  }

  //alert(barcode);
    
});


JS;
$this->registerJs($script);
?>