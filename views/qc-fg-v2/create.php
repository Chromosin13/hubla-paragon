<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\QcFgV2 */

$this->title = 'QC FG';
$this->params['breadcrumbs'][] = ['label' => 'Qc Fg V2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qc-fg-v2-create">

<div>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Check Jadwal</h3>
                <span class="label label-primary pull-right"><i class="fa fa-html5"></i></span>
              </div><!-- /.box-header -->
              <div class="box-body">
                <div class="zoom">
                  <?=Html::a('SCAN WITH QR', ['qc-fg-v2/create-tab'], ['class' => 'btn btn-success btn-block']);?>
                </div>
                <br>
                <!-- <p>Silahkan ketik/scan Kode Item pada form dibawah ini lalu klik sembarang</p> -->
                 <?= $this->render('_form', [
                      'model' => $model,
                  ]) ?>

              </div><!-- /.box-body -->
            </div><!-- /.box -->
</div>

</div>
