<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\ActiveField;
use kartik\dialog\Dialog;
use kartik\checkbox\CheckboxX;
/* @var $this yii\web\View */
/* @var $model app\models\QcFgV2 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qc-fg-v2-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 


            echo $form->field($model, 'snfg', [
                            'addon' => [
                                'prepend' => [
                                    'content'=>'<i class="fa fa-barcode"></i>'
                                    
                                ],
                                'append' => [
                                    'content' => Html::button('Tekan Setelah Scan',['class'=>'btn btn-primary','id'=>'afterScnBtnSnfg']),
                                    'asButton' => true
                                ]            
                            ]
                        ]);


    
    ?>




    <div id ="quantity-field">


	</div>


        <div id="form-create" class="form-group">

            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            
        </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$script = <<< JS

// Hide Quantity Field

	$('#quantity-field').hide(); 

// Hide Create new Field

    $('#form-create').hide();

$('#qcfgv2-snfg').change(function(){  

    var snfg = $('#qcfgv2-snfg').val().trim();
    
    if(snfg){
      window.location = "index.php?r=qc-fg-v2/create-rincian&snfg="+snfg;
    }else{
      alert('Nomor Jadwal kosong!');
    }
    
});


// $('#qcfgv2-snfg').change(function(){  

//     var snfg = $('#qcfgv2-snfg').val();


//     $.get('index.php?r=qc-fg-v2/check-snfg',{ snfg : snfg },function(data){
//         var data = $.parseJSON(data);
//         if(data.id==1){

//           alert('Data Valid, belum dilakukan QC, silahkan tekan tombol CREATE untuk inisiasi QC');
          
//           $('#form-create').show();
           
//         }

//         else if(data.id==2){

//           alert('Data Valid, Proses QC sudah berjalan, halaman akan diteruskan');
          
//           var snfg = $('#qcfgv2-snfg').val();

//           alert(snfg);

//           $('#form-create').hide();

//           // window.location = baseurl+"?r=qc-fg-v2/create-rincian&snfg="+snfg;
            
//           window.location = "index.php?r=qc-fg-v2/create-rincian&snfg="+snfg;

//           // alert('test'); 
//         }
    
//     });
// });


JS;
$this->registerJs($script);
?>