<?php

use app\models\SuratJalan;
use app\models\SuratJalanRincian;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\models\QcFgV2;

/* @var $this yii\web\View */
/* @var $model app\models\SuratJalanRincian */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- CSS For Video Selector -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="ZXing for JS">

  <title>ZXing TypeScript | Decoding from camera stream</title>

  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">
</head>



        <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <?php 
                    echo '<div class="widget-user-header bg-green">';
                
            ?>
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/clipboard-icon.png" alt="User Avatar">
              </div>
          <!-- /.widget-user-image -->
          <h2 class="widget-user-username">Scan Terima NDC QR</h2>
          <h5 class="widget-user-desc">Terima NDC</h5>
        </div>
        <p></p>


    <div class="col-sm-12">
      <div class="box-body">
          <div class="box-body">

            <div id="sourceSelectPanel" style="display:none">
              <label for="sourceSelect">Switch Camera</label>
              <select id="sourceSelect" style="max-width:400px">x
              </select>
            </div>

            <div>
              <video id="video" width="330" height="300" style="border: 0.5px solid gray"></video>
            </div>
              
          </div>
      </div>
    </div>


<!-- Javascript -->
  <!-- Calling ZXing API CDN -->
  <!-- <script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script> -->
  <script type="text/javascript" src="zxing.js"></script>
  <!-- Code -->
  <script type="text/javascript">

      let selectedDeviceId;
      const codeReader = new ZXing.BrowserMultiFormatReader()
      console.log('ZXing code reader initialized')
      codeReader.getVideoInputDevices()
        .then((videoInputDevices) => {
          const sourceSelect = document.getElementById('sourceSelect')
          selectedDeviceId = videoInputDevices[1].deviceId
          if (videoInputDevices.length >= 1) {
            videoInputDevices.forEach((element) => {
              const sourceOption = document.createElement('option')
              sourceOption.text = element.label
              sourceOption.value = element.deviceId
              sourceSelect.appendChild(sourceOption)
            })

            // When Changing Camera, Reset Scanner and Execute Canvas
            sourceSelect.onchange = () => {
              selectedDeviceId = sourceSelect.value;

              codeReader.reset()
              codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
                
                if (result) {
                  nosj = result.text.trim();
                  $.get('index.php?r=surat-jalan/check-surat-jalan',{ surat_jalan : nosj }, function(data){
                    if (data == 1){
                      window.location = 'index.php?r=qc-fg-v2/form-terima-ndc&surat_jalan='+nosj;
                    }else if (data == 2){
                      window.alert('Surat Jalan sudah diterima!');
                    }else if (data == 3){
                      window.location = 'index.php?r=surat-jalan-rincian/konfirmasi-terima-ndc-qr&surat_jalan='+nosj;
                    }else{
                      window.alert('Nomor Surat Jalan tidak ditemukan!');

                    }

                  });

                  // window.location = 'index.php?r=qc-fg-v2/form-terima-ndc&surat_jalan='+result.text;
                }
                if (err && !(err instanceof ZXing.NotFoundException)) {
                  console.error(err)
                  document.getElementById('result').textContent = err
                }

              }) // on scanned 
            }; // onchange

            const sourceSelectPanel = document.getElementById('sourceSelectPanel')
            sourceSelectPanel.style.display = 'block'
          }

          // Initialize Execute Canvas when Page first loaded 
          codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
             if (result) {
              nosj = result.text.trim();
              
              $.get('index.php?r=surat-jalan/check-surat-jalan',{ surat_jalan : nosj }, function(data){
                if (data == 1){
                  window.location = 'index.php?r=qc-fg-v2/form-terima-ndc&surat_jalan='+nosj;
                }else if (data==2){
                  window.alert('Surat Jalan sudah diterima!');
                }else if (data == 3){
                  window.location = 'index.php?r=surat-jalan-rincian/konfirmasi-terima-ndc-qr&surat_jalan='+nosj;
                }else{
                  window.alert('Nomor Surat Jalan tidak ditemukan!');
                }

              });
                  // console.log(result.text);
                  // window.location = 'index.php?r=qc-fg-v2/form-terima-ndc&surat_jalan='+result.text;
                }
            if (err && !(err instanceof ZXing.NotFoundException)) {
              console.error(err)
              document.getElementById('result').textContent = err
            }
          }) // on scanned


        })
        .catch((err) => {
          console.error(err)
        })
  </script> 


<?php
$script = <<< JS



JS;
$this->registerJs($script);
?>