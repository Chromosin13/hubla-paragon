<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WeigherFgMapDevice */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Weigher Fg Map Devices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="weigher-fg-map-device-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'frontend_ip',
            'sbc_user',
            'sbc_ip',
            'line',
            'posisi',
            'keterangan',
        ],
    ]) ?>

</div>
