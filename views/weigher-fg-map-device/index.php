<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WeigherFgMapDeviceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Weigher Fg Map Devices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="weigher-fg-map-device-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Weigher Fg Map Device', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'frontend_ip',
            'sbc_user',
            'sbc_ip',
            'line',
            'posisi',
            'keterangan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
