<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\MasterDataLine;

/* @var $this yii\web\View */
/* @var $model app\models\WeigherFgMapDevice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="weigher-fg-map-device-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'frontend_ip')->textInput() ?>

    <?= $form->field($model, 'sbc_user')->textInput() ?>

    <?= $form->field($model, 'sbc_ip')->textInput() ?>

     <!-- <?= $form->field($model, 'line')->dropDownList(['prompt'=>'Select Option']); ?> -->
     <?php echo $form->field($model, 'line')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(MasterDataLine::find()->where("posisi = 'KEMAS' and sediaan not like '%off%'")->orderBy(['nama_line'=>SORT_ASC])->all()
                                                        ,'nama_line','nama_line'),
                                                        'options' => ['placeholder' => 'Select Line', 'value' => $model->isNewRecord ? false : $model->line],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]); ?>

    <?= $form->field($model, 'posisi')->textInput() ?>

    <?= $form->field($model, 'keterangan')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

    // $.post("index.php?r=master-data-line/get-line-kemas", function (data){
    //         $("select#weigherfgmapdevice-line").html(data);
    // });

JS;
$this->registerJs($script);
?>