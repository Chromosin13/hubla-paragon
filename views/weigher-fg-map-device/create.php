<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WeigherFgMapDevice */

$this->title = 'Create Weigher Fg Map Device';
$this->params['breadcrumbs'][] = ['label' => 'Weigher Fg Map Devices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="weigher-fg-map-device-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
