<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WeigherFgMapDevice */

$this->title = 'Update Weigher Fg Map Device: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Weigher Fg Map Devices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="weigher-fg-map-device-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
