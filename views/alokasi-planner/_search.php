<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AlokasiPlannerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alokasi-planner-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sediaan') ?>

    <?= $form->field($model, 'koitem_baru') ?>

    <?= $form->field($model, 'naitem') ?>

    <?= $form->field($model, 'brand') ?>

    <?= $form->field($model, 'week_plot') ?>

    <?php // echo $form->field($model, 'nors') ?>

    <?php // echo $form->field($model, 'week_rs') ?>

    <?php // echo $form->field($model, 'alokasi') ?>

    <?php // echo $form->field($model, 'plot') ?>

    <?php // echo $form->field($model, 'row_number') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
