<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AlokasiPlanner */

$this->title = 'Create Alokasi Planner';
$this->params['breadcrumbs'][] = ['label' => 'Alokasi Planners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alokasi-planner-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
