<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AlokasiPlanner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alokasi-planner-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sediaan')->textInput() ?>

    <?= $form->field($model, 'koitem_baru')->textInput() ?>

    <?= $form->field($model, 'naitem')->textInput() ?>

    <?= $form->field($model, 'brand')->textInput() ?>

    <?= $form->field($model, 'week_plot')->textInput() ?>

    <?= $form->field($model, 'nors')->textInput() ?>

    <?= $form->field($model, 'week_rs')->textInput() ?>

    <?= $form->field($model, 'alokasi')->textInput() ?>

    <?= $form->field($model, 'plot')->textInput() ?>

    <?= $form->field($model, 'row_number')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
