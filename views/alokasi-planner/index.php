<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\AlokasiPlanner;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AlokasiPlannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alokasi Planners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alokasi-planner-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'primary', 'heading'=>'Alokasi Planner'],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                [
                    'attribute'=>'brand',
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(AlokasiPlanner::find()->orderBy('brand')->asArray()->all(), 'brand', 'brand'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Brand'],
                ],
                [
                    'attribute'=>'sediaan',
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(AlokasiPlanner::find()->orderBy('sediaan')->asArray()->all(), 'sediaan', 'sediaan'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Sediaan'],
                    'subGroupOf'=>1,
                ],
                [

                    'attribute'=>'koitem_baru',
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>2,
                ],
                  [
                    'attribute'=>'naitem',
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>3,
                ],
                 [
                    'attribute'=>'week_plot',
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(AlokasiPlanner::find()->orderBy('week_plot')->asArray()->all(), 'week_plot', 'week_plot'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Week Plot'],
                    'subGroupOf'=>4,
                ],
                [
                    'attribute'=>'nors',
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>5,
                ],
                'week_rs',
                'alokasi',
                'plot',
             ],
        ]);

    ?>


    <!-- <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'sediaan',
            'koitem_baru',
            'naitem',
            'brand',
            'week_plot',
            'nors',
            'week_rs',
            'alokasi',
            'plot',
            'row_number',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?> -->
</div>
