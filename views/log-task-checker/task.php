<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;
use yii\helpers\Json;
use yii\widgets\Pjax;


?>
<style>
    .bawahan {
       position: fixed;
       border-top:2px solid #D3D3D3;
       left: 0;
       bottom: 0;
       width: 100%;
       padding:15px 0;
       background-color: white;
       color: white;
       text-align: center;
    }
    .widget-user-2 .widget-user-header {
        padding: 2px;
        padding-left: 10px;
        padding-right: 10px;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
    }
    .widget-user-2 .widget-user-desc, .widget-user-2 .widget-user-username {
        margin-left: 10px;
    }
</style>

<script>
    var nomo = '<?php echo $nomo; ?>';
    var fim_id = '$fim_id';
</script>
<div class="row">
    <div class="col-md-12">
      <!-- Widget: user widget style 1 -->
      <div class="box box-widget widget-user-2">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-blue">
          <!-- <div class="widget-user-image">
            <img class="img-circle" src="../dist/img/user7-128x128.jpg" alt="User Avatar">
          </div> -->
          <!-- /.widget-user-image -->
          <!-- <h3 class="widget-user-username">Nadia Carmichael</h3> -->
          <!-- <h5 class="widget-user-desc">Lead Developer</h5> -->
          <!-- <h3 class="widget-user-username">Nadia Carmichael</h3> -->
          <div class="row">
            <h3 class="text-center"><?php echo $sp->nama_bulk ?> </h3>
          </div>
            <div class="row">
                    <div class="col-xs-8" style="font-size: 14px;"><?php echo 'SNFG : '.$sp->snfg; ?></div>
                    <div class="col-xs-4" style="font-size: 14px;"><?php echo 'OPR : '.$operator?></div>
            </div>
            <div class="row">
                <div class="col-xs-8" style="font-size: 14px;"><?php echo 'NOMO : '.$nomo; ?></div>
                <div class="col-xs-4" style="font-size: 14px;"><?php echo 'SIKLUS : '.$siklus?></div>
            </div>    
        </div>
        <div class="box-footer no-padding">
          <!-- <ul class="nav nav-stacked">
            <li><a href="#">Projects <span class="pull-right badge bg-blue">31</span></a></li>
            <li><a href="#">Tasks <span class="pull-right badge bg-aqua">5</span></a></li>
            <li><a href="#">Completed Projects <span class="pull-right badge bg-green">12</span></a></li>
            <li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li>
          </ul> -->

          <!-- <div class="box-footer"> -->
              <!-- <div class="row"> -->
                <div class="box-body">
                    <div class="box-body">
                      <div class="zoom">
                        <?=Html::a('SCAN ID BAHAN BAKU', ['log-task-checker/scan-qr','id'=>$fim_id], ['class' => 'btn btn-success btn-block']);?>
                      </div>

                        <p></p>

                      <div class="zoom">
                        <?=Html::a('INPUT DATA MANUAL', ['log-task-checker/input-detail-bb','nomo'=>$nomo,'siklus'=>$siklus,'pic'=>$operator], ['class' => 'btn btn-warning btn-block']);?>
                      </div>


                        <!-- <div class="form-group field-flowinputmo-nomo has-success">
                        <label class="control-label" for="flowinputmo-nomo">ID Bahan Baku</label>
                        <input type="text" id="logtaskchecker-id_bb" class="form-control" name="LogTaskChecker[id_bb]" aria-invalid="false">

                        </div> -->

                    </div>

                </div>

              <!-- </div> -->
              <!-- /.row -->
          <!-- </div> -->
          
        </div>
      </div>
      <!-- /.widget-user -->
    </div>
</div>






<?php Pjax::begin(['id' => 'evaluasi']); ?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'export' => false,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['style' => 'text-align:center; max-width:70px;'],
                'contentOptions' => ['style' => 'text-align:center;']
            ],
            [
                // 'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'status',
                'headerOptions' => ['style' => 'text-align:center; max-width:100px;'],
                // 'label' => 'Nama Bahan Baku',
                // 'editableOptions' => [
                //   // 'asPopover' => false,
                // ],
                'contentOptions' => function ($model, $key, $index, $column) {
                        return ['style' => 'text-align:center; max-width:100px; background-color:' 
                            . ($model->status == 'OK'
                                ? '#5CDB95' : '')];
                    },
            ],
            // [
            //     // 'class' => 'kartik\grid\EditableColumn',
            //     'attribute' => 'id_bb',
            //     'headerOptions' => ['style' => 'text-align:center; max-width:50px;'],
            //     // 'label' => 'Nama Bahan Baku',
            //     // 'editableOptions' => [
            //     //   // 'asPopover' => false,
            //     // ],
            //     'contentOptions' => ['style' => 'text-align:center;']
            // ],
            [
                // 'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'kode_olah',
                'headerOptions' => ['style' => 'text-align:center; max-width:100px;'],
                // 'label' => 'Nama Bahan Baku',
                // 'editableOptions' => [
                //   // 'asPopover' => false,
                // ],
                'contentOptions' => ['style' => 'text-align:center; max-width:100px;']
            ],
            [
                'attribute'=>'kode_internal',
                'headerOptions' => ['style' => 'text-align:center; max-width:200px;'],
                'contentOptions' => ['style' => 'text-align:left; max-width:200px;']
            ],
            [
                'attribute'=>'nama_bb',
                'headerOptions' => ['style' => 'text-align:center; max-width:250px;'],
                'contentOptions' => ['style' => 'text-align:left; max-width:250px;']
            ],
            // 'qty',
            // 'uom',
            // 'kode_olah',
            [
                'attribute' => 'qty',
                'label' => 'Qty',
                'value' => function ($dataProvider){
                    return $dataProvider->qty.' '.$dataProvider->uom;
                },
                'headerOptions' => ['style' => 'text-align:center; max-width:150px;'],
                'contentOptions' => ['style' => 'text-align:right; max-width:150px;']
            ],
            
            // 'status',
            
            // [
            //     'class' => 'kartik\grid\EditableColumn',
            //     'attribute' => 'kode_bb_scan',
            //     'headerOptions' => ['style' => 'text-align:center; width: 350px;'],
            //     'contentOptions' => ['style' => 'text-align:center;'],
            //     // 'format' => Editable::FORMAT_BUTTON,
            //     // 'editableValueOptions'=>['class'=>'text-danger'],
            //     'editableOptions' => [
            //       'asPopover' => false,
            //     ],
            // ],
            
            // [
            //     'class' => 'kartik\grid\ActionColumn',
            //     'template' => '{download}',
            //     'buttons' => [
            //         'download' => function ($url,$dataProvider) {
            //             return Html::a(
            //                 '<span class="fa fa-qrcode" style="color:blue; font-size:2em;"></span>',
            //                 // ['log-formula-breakdown/schedule-list', 'id' => $model->id],
            //                 'javascript:void(0);', 
            //                 [
            //                     'title' => 'Add',
            //                     'data-pjax' => '0',
            //                     'id' => 'scan-'.$dataProvider->id,
            //                  ]
            //             );
            //         },
            //     ],
            //     'headerOptions' => ['class' => 'kartik-sheet-style'],
            //     'contentOptions' => ['style' => 'text-align:center;'],
            // ],
            
            // [
            //     'class' => 'kartik\grid\ActionColumn',
            //     'template' => '{download} {view}',
            //     'buttons' => [
            //         'download' => function ($url) {
            //             return Html::a(
            //                 '<span class="glyphicon glyphicon-plus"></span>',
            //                 // ['log-formula-breakdown/schedule-list', 'id' => $model->id],
            //                 ['log-formula-breakdown/schedule-list'], 
            //                 [
            //                     'title' => 'Split',
            //                     'data-pjax' => '0',
            //                 ]
            //             );
            //         },
            //         'view' => function ($url) {
            //             return Html::a(
            //                 '<span class="glyphicon glyphicon-refresh"></span>',
            //                 ['log-formula-breakdown/schedule-list'], 
            //                 [
            //                     'title' => 'Split',
            //                     'data-pjax' => '0',
            //                 ]
            //             );
            //         },
            //     ],
            //     'headerOptions' => ['class' => 'kartik-sheet-style'],
            // ],
        ],
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'pjax' => true, // pjax is set to always true for this demo
            // set your toolbar
            // 'toolbar' =>  [
            //     [
            //         'content' =>
            //             Html::button('<i class="fas fa-plus"></i>', [
            //                 'class' => 'btn btn-success',
            //                 'title' => 'Add Line',
            //                 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");'
            //             ]) . ' '.
            //             Html::a('<i class="fas fa-redo"></i>', ['grid-demo'], [
            //                 'class' => 'btn btn-outline-secondary',
            //                 'title'=> 'Reset Grid',
            //                 'data-pjax' => 0, 
            //             ]), 
            //         'options' => ['class' => 'btn-group mr-2']
            //     ],
            //     '{export}',
            //     '{toggleData}',
            // ],
            // 'toggleDataContainer' => ['class' => 'btn-group mr-2'],
            // // set export properties
            // 'export' => [
            //     'fontAwesome' => true
            // ],
            // parameters from the demo form
            'bordered' => true,
            'striped' => true,
            'condensed' => true,
            // 'responsive' => true,
            'hover' => true,
            // 'showPageSummary' => true,
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
                'heading' => '<i class="fas fa-book"></i>  Task Checker',
            ],
    ]); ?>

    <div class="bawahan">
        <div class="row">
            <div class="col-6" style="width:50%; padding:0 30px; float:left;">
                <button type="button" id="btnCancel" class="btn btn-block btn-danger" style="font-size:12px;">CANCEL WORK ORDER</button>
            </div>
            <div class="col-6" style="width:50%; padding:0 30px; float:left;">
                <button type="button" id="btnFinish" class="btn btn-block btn-info" style="font-size:12px;">SELESAI CHECKING</button>
            </div>
        </div>
    </div>

<div id="modal" style="text-align: center; position: absolute; z-index: 10; top: 20%; left: 50%; transform: translate(-50%, -50%); background-color: white; width:70%; height:22vh; border-radius: 5px; display: none;">
  <div id="modal-header" style="background-color: #5DADE2; margin-bottom: 0px; border-top-left-radius: 5px; border-top-right-radius: 5px;">
    <p style="text-align:left; padding-left : 20px; padding-top : 6px; font-size:1.5em;"><b>Confirmation</b></p>
  </div>
  <div id="modal-content" style="height:30%;">
    <p style="padding-top:20px; font-size:1.2em;">Apakah data yang anda masukkan sudah benar?</p>
  </div>
  <hr>
  <div id="modal-footer" style="margin-top:0px;">
    <button id="addbatch" onclick="no()" class="btn btn-danger" style="position: absolute; top: 83%; left: 35%; transform: translate(-50%, -50%); width:20%;">
      <b>TIDAK</b>
    </button>
    <button id="addbatch" onclick="yes()" class="btn btn-success" style="position: absolute; top: 83%; left: 65%; transform: translate(-50%, -50%); width:20%;">
      <b>YA</b>
    </button>

  </div>
</div>

<?php
$script = <<< JS

    var id_timbang = '$id_timbang';
    var siklus = '$siklus';
    var nomo = '$nomo';
    
    $('button[id^="btnPrint"]').on("click",function() {
        window.location = "index.php?r=log-penimbangan-rm/pdf&id="+id_timbang+"&siklus=siklus";
    });

    $('button[id^="btnCancel"]').on("click",function() {
        var r = confirm("Anda yakin akan cancel proses checker? History yang sudah di scan akan dihapus!");
        if (r == true) {
            $.post("index.php?r=log-task-checker/cancel-checker&nomo="+nomo+"&siklus="+siklus, function (data){
                if (data==1){
                    window.location = "index.php?r=log-task-checker/check-nomo-checker";
                } else {
                    alert("Ada task yang belum selesai!");
                }
            });
        } else {
          
        }
    });

    $('button[id^="btnFinish"]').on("click",function() {
        var r = confirm("Anda yakin akan menyelesaikan proses checker?");
        if (r == true) {
            $.post("index.php?r=log-task-checker/check-unfinish-task&nomo="+nomo+"&siklus="+siklus, function (data){
                if (data == 1){
                    $.post("index.php?r=log-task-checker/finish-checker&nomo="+nomo+"&siklus="+siklus, function (data){
                        if (data==1){
                            $.post("index.php?r=log-task-checker/send-data-to-pengolahan&nomo="+nomo+"&siklus="+siklus, function (data){
                                if (data==1){
                                    window.location = "index.php?r=log-task-checker/check-nomo-checker";
                                }else {
                                    alert("gagal");
                                }
                            });
                        } else {
                            alert('Gagal stop proses checker!');
                        }
                    });                    
                }else{
                    var r2 = confirm("Ada task yang BELUM SELESAI, apakah anda tetap ingin menyelesaikan proses?");
                    if(r2 == true){
                        $.post("index.php?r=log-task-checker/finish-checker&nomo="+nomo+"&siklus="+siklus, function (data){
                            if (data==1){
                                $.post("index.php?r=log-task-checker/send-data-to-pengolahan&nomo="+nomo+"&siklus="+siklus, function (data){
                                    if (data==1){
                                        window.location = "index.php?r=log-task-checker/check-nomo-checker";
                                    }else {
                                        alert("gagal");
                                    }
                                });
                            } else {
                                alert('Gagal stop proses checker!');
                            }
                        });      
                    }else{
                        
                    }
                }

            });
        



        } else {
          
        }
    });

    var fim_id = '$fim_id';

    $('a[id^="scan-"]').on("click",function() {
        var id = $(this).attr('id');
        var wo = id.substring(id.lastIndexOf("-") + 1);
        
        console.log(wo);
        // window.location = "http://10.3.5.102/flowreport/web/index.php?r=log-task-checker/scan-bb&id="+fim_id+"&wo="+wo;
        window.location = "index.php?r=log-task-checker/scan-bb&id="+fim_id+"&wo="+wo;

    });

    $('#logtaskchecker-id_bb').change(function(){  

    var id_bb = $('#logtaskchecker-id_bb').val().trim();
    var operator = "$operator";
    var siklus = "$siklus";
    // console.log(operator);
    
    if(id_bb){
        $.get('index.php?r=log-task-checker/check-bb',{ id_bb : id_bb },function(data)
        {
          if(data==1){                    
             $.get('index.php?r=log-task-checker/update-status',{ id_bb : id_bb, log : "manual", pic : operator },function(data){
                if(data==1){
                  window.location = "index.php?r=log-task-checker/task&nomo="+nomo+"&siklus="+siklus;
                } else {
                  alert('Tidak berhasil update log status');
                }
             });           
          } else if (data == 2){
            window.alert("Bahan Baku sudah pernah di scan sebelumnya.");
          } else{
            window.alert("Kode bahan baku tidak sesuai.");
          }
        });
    }else{
      alert('ID Bahan Baku kosong!');
    }


    
});

JS;
$this->registerJs($script);
?>