<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LogTaskChecker */

$this->title = 'Update Log Task Checker: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Log Task Checkers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="log-task-checker-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
