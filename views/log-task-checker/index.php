<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogTaskCheckerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Task Checkers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-task-checker-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Log Task Checker', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'kode_bb',
            'nama_bb',
            'qty',
            'uom',
            // 'kode_olah',
            // 'status',
            // 'pic',
            // 'timestamp',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
