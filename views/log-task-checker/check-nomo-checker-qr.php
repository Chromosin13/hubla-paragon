<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowAnalyst;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- CSS For Video Selector -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="ZXing for JS">

  <title>ZXing TypeScript | Decoding from camera stream</title>

  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">
</head>

 

<!-- Body -->
<div class="box box-widget widget-user">
  <!-- Add the bg color to the header using any of the bg-* classes -->
  <div class="widget-user-header bg-green-gradient">
    <h3 class="widget-user-username"><b>Scan Jadwal Penimbangan Raw Material</b></h3>
    <h5 class="widget-user-desc">QR Code</h5>
  </div>
  <div class="widget-user-image">
    <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
  </div>
  <div class="box-footer">
    <div class="row">
      <div class="box-body">
          <div class="box-body">

            <div id="sourceSelectPanel" style="">
              <label for="sourceSelect" style="">Switch Camera</label>
              <select id="sourceSelect" style="max-width:400px; ">
              </select>
            </div>

            <div>
              <label for="video" style="text-align: center;"><h3><b>Scan QR code dari No.MO</b></h3></label>
              <video id="video" width="330" height="300" style="border: 0.5px solid gray; display: block; margin-left: auto; margin-right: auto;"></video>
            </div>
              
          </div>
      </div>
    </div>
    <!-- /.row -->
  </div>
</div>



<!-- Javascript -->
  <!-- Calling ZXing API CDN -->
  <script type="text/javascript" src="zxing.js"></script>
  <!-- Code -->
  <script type="text/javascript">

      let selectedDeviceId;
      const codeReader = new ZXing.BrowserMultiFormatReader()
      console.log('ZXing code reader initialized')
      codeReader.getVideoInputDevices()
        .then((videoInputDevices) => {
          const sourceSelect = document.getElementById('sourceSelect')
          // selectedDeviceId = videoInputDevices[1].deviceId
          if (videoInputDevices.length >= 1) {
            videoInputDevices.forEach((element) => {
              const sourceOption = document.createElement('option')
              sourceOption.text = element.label
              sourceOption.value = element.deviceId
              sourceSelect.appendChild(sourceOption)
            })

            // When Changing Camera, Reset Scanner and Execute Canvas
            sourceSelect.onchange = () => {
              selectedDeviceId = sourceSelect.value;

              codeReader.reset()
              codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
                
                if (result) { 
                  //window.alert(result.text);
                    $.get("index.php?r=log-task-checker/check-status-nomo&nomo="+result, function (data){
                      var data = JSON.parse(data);
                      if (data.status == 'generate') {
                        $.get("index.php?r=log-task-checker/check-status-task&nomo="+result, function (data2){
                          if (data2 == 1) {
                            window.location = "index.php?r=log-task-checker/form-checker&nomo="+result+"&message=";
                          } else if (data2 == 2){
                            window.alert("Proses checker untuk NOMO ini sudah selesai.");
                          } else {
                            window.alert("Belum ada task untuk nomo ini.");
                          
                          }
                        });
                      } else {
                        window.location = "index.php?r=log-task-checker/task&nomo="+result+"&siklus="+data.siklus+"&message=";
                      }
                    });
                }
                if (err && !(err instanceof ZXing.NotFoundException)) {
                  console.error(err)
                  document.getElementById('result').textContent = err
                }

              }) // on scanned 
            }; // onchange

            const sourceSelectPanel = document.getElementById('sourceSelectPanel')
            sourceSelectPanel.style.display = 'block'
          }

          // Initialize Execute Canvas when Page first loaded 
          codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
            if (result) { 
                  //window.alert(result.text);
                // $.get("index.php?r=log-task-checker/check-status-nomo&nomo="+result, function (data){
                //   var data = JSON.parse(data);
                //   console.log (data.status);
                //   if (data.status == 'generate') {
                //     $.get("index.php?r=log-task-checker/check-status-task&nomo="+result, function (data2){
                //       if (data2 == 1) {
                //         window.location = "index.php?r=log-task-checker/form-checker&nomo="+result+"&message=";
                //       } else {
                //         alert("Belum ada task untuk nomo ini karena penimbangan belum scan stop.");
                //       }
                //     });
                //   } else if (data == 2 ){
                //     // alert("Proses checker untuk NOMO ini sudah selesai.");
                //     window.location = "index.php?r=log-task-checker/task&nomo="+nomo+"&siklus="+data.siklus+"&message=";
                //   }
                // });

                $.get("index.php?r=log-task-checker/check-status-nomo&nomo="+result, function (data){
                  var data = JSON.parse(data);
                  if (data.status == 'generate') {
                    $.get("index.php?r=log-task-checker/check-status-task&nomo="+result, function (data2){
                      if (data2 == 1) {
                        window.location = "index.php?r=log-task-checker/form-checker&nomo="+result+"&message=";
                      } else if (data2 == 2){
                        window.alert("Proses checker untuk NOMO ini sudah selesai.");
                      } else {
                        window.alert("Belum ada task untuk nomo ini.");
                      
                      }
                    });
                  } else {
                    window.location = "index.php?r=log-task-checker/task&nomo="+result+"&siklus="+data.siklus+"&message=";
                  }
                });
            }
            if (err && !(err instanceof ZXing.NotFoundException)) {
              console.error(err)
              document.getElementById('result').textContent = err
            }
          }) // on scanned


        })
        .catch((err) => {
          console.error(err)
        })
  </script>