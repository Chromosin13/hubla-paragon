<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogTaskChecker */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-task-checker-form">

    <?php $form = ActiveForm::begin(['id'=>'form-manual']); ?>
    
    <?= $form->field($model, 'nomo')->textInput(['value'=>$nomo , 'readOnly'=>true]) ?>
    <?= $form->field($model, 'pic')->textInput(['value'=>$pic , 'readOnly'=>true]) ?>
    <?= $form->field($model, 'siklus')->textInput(['value'=>$siklus , 'readOnly'=>true]) ?>
    <?= $form->field($model, 'kode_internal')->textInput() ?>
    <?= $form->field($model, 'kode_olah')->textInput() ?>
    <?= $form->field($model, 'qty')->textInput() ?>

    <div class="form-group">
        <?= Html::Button($model->isNewRecord ? 'Check' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id'=>'submitBtn']) ?>
        <?= Html::Button('Back' , ['class' =>  'btn btn-warning', 'onclick'=>"back()"]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
  function back(){
    //window.location = "index.php?r=log-formula-breakdown/schedule-list";
    window.history.back();
  }
</script>

<?php
$script = <<< JS

$('#submitBtn').on('click',function(){
    var kode_internal = $('#logtaskchecker-kode_internal').val();
    var kode_olah = $('#logtaskchecker-kode_olah').val();
    var qty = $('#logtaskchecker-qty').val();
    var nomo = $('#logtaskchecker-nomo').val();
    var pic = $('#logtaskchecker-pic').val();
    var siklus = $('#logtaskchecker-siklus').val();
    $.getJSON("index.php?r=log-task-checker/check-bb-manual&nomo="+nomo+"&siklus="+siklus+"&kode_internal="+kode_internal+"&kode_olah="+kode_olah+"&qty="+qty, function (data){
            if (data.status=='exist') {
               $.get("index.php?r=log-task-checker/update-status-bb-manual&id="+data.id_bb+"&log=manual-detail&pic="+pic, function (data){
                    if(data=1){
                        window.location = "index.php?r=log-task-checker/task&nomo="+nomo+"&siklus="+siklus;
                    }else{
                        window.alert('Gagal update status bahan baku');

                    }
                });
            } else {
                window.alert('Bahan Baku tidak ditemukan dalam List Task');
            }
          });
});

// $('#form-id').yiiActiveForm('submitForm');

JS;
$this->registerJs($script);
?>
