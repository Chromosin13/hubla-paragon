<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogTaskChecker */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-task-checker-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kode_bb')->textInput() ?>

    <?= $form->field($model, 'nama_bb')->textInput() ?>

    <?= $form->field($model, 'qty')->textInput() ?>

    <?= $form->field($model, 'uom')->textInput() ?>

    <?= $form->field($model, 'kode_olah')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'pic')->textInput() ?>

    <?= $form->field($model, 'timestamp')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
