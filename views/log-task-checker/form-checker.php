<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use app\models\LogPenimbanganRm;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\Downtime;
use app\models\DowntimeSearch;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

                  
                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-blue-gradient">
                      <h3 class="widget-user-username"><b>Form Checker</b></h3>
                      <h5 class="widget-user-desc">Work Order</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/scale.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">

                                


                                <div class="flow-input-mo-form">

                                    <?php $form = ActiveForm::begin(); ?>

                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'nomo')->textInput(['readOnly'=>true,'value'=>$nomo]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'posisi')->textInput(['readOnly'=>true,'value'=>'CHECKER']) ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                          <?php
                                          echo $form->field($model, 'jenis_penimbangan')->textInput(['readOnly'=>true,'value'=>'JADWAL BARU']); 

                                           ?>
                                          <!-- /input-group -->
                                        </div>
                                        <!-- /.col-lg-6 -->
                                        <div class="col-md-3">
                                          <?php
                                                
                                                    echo $form->field($model, 'nama_line')->textInput(['readOnly'=>true,'value'=>'CHECKER']);
                                            ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div id="lanjutan">
                                        <div class="col-md-3" style="visibility:hidden;">
                                          <?= $form->field($model, 'lanjutan')->textInput(['value'=>1,'readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <!-- <label class="control-label">Nama Operator</label> -->
                                        <?= $form->field($model, 'nama_operator')->textInput(['maxlength' => true, 'style' => 'text-transform:uppercase','id'=>'input_operator']) ?>
                                        <?php if (!empty($message)) {
                                            echo "<p style='color:red;'>"; echo $message; echo "</p>";
                                        } ?>

                                    </div>

                                    <div class="col-md-12">
                                        <?php 
                                            $data = Yii::$app->db->createCommand("SELECT siklus from log_task_checker WHERE nomo = '".$nomo."' ")->queryAll();
                                            $siklus = ArrayHelper::map($data,'siklus','siklus');
                                            // $data = ArrayHelper::map(LogPenimbanganRm::find()->where("nomo='".$nomo."' ")->all()
                                                                                                    // ,'siklus','siklus');
                                            $siklus[0]= 'ALL';

                                            //set default to 0
                                            $model->siklus = 0;
                                            echo $form->field($model, 'siklus')->widget(Select2::classname(), [
                                                    'data' => $siklus,
                                                    'options' => ['placeholder' => 'Pilih Siklus'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ]);
                                       ?>

                                    </div>

                                    

                                    <br></br>

                                            <?php 
                                                echo Html::submitButton($model->isNewRecord ? 'START' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary']);
                                            ?>
                                            


                                    <?php ActiveForm::end(); ?>

                                </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>


<?php
$script = <<< JS
    
    document.getElementById('input_operator').addEventListener('input', function (e) {
      //e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1,').trim();
        e.target.value = e.target.value.toUpperCase();
        if (e.target.value.length % 5 == 0){
            //e.target.value = e.target.value.replace(/(.{4})/g, '$1').replace(/[ ,]+/g, ",");
            e.target.value = e.target.value.replace(/.$/,",");
        }
        
    }); 

    // $('#lanjutan').hide();

    var nomo = $('#flowinputmo-nomo').val();
    
    var posisi = $('#flowinputmo-posisi').val();

    $.post("index.php?r=master-data-line/get-line-timbang-nomo&nomo="+$('#flowinputmo-nomo').val(), function (data){
        $("select#flowinputmo-nama_line").html(data);
    });


    // $('#flowinputmo-jenis_penimbangan').change(function(){

    //     var jenis_penimbangan = $('#flowinputmo-jenis_penimbangan').val();

    //     $.get('index.php?r=flow-input-mo/get-lanjutan-penimbangan',{ nomo : nomo , jenis_penimbangan : jenis_penimbangan , posisi : posisi  }, function(data){
    //         var data = $.parseJSON(data);
    //         $('#flowinputmo-lanjutan').attr('value',data.lanjutan);

    //     });

    //     $('#lanjutan').fadeIn();

    // });  



JS;
$this->registerJs($script);
?>