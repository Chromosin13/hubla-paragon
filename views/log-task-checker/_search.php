<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogTaskCheckerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-task-checker-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'kode_bb') ?>

    <?= $form->field($model, 'nama_bb') ?>

    <?= $form->field($model, 'qty') ?>

    <?= $form->field($model, 'uom') ?>

    <?php // echo $form->field($model, 'kode_olah') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'pic') ?>

    <?php // echo $form->field($model, 'timestamp') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
