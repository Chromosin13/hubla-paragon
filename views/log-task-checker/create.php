<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LogTaskChecker */

$this->title = 'Create Log Task Checker';
$this->params['breadcrumbs'][] = ['label' => 'Log Task Checkers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-task-checker-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
