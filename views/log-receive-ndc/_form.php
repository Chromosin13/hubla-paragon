<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogReceiveNdc */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-receive-ndc-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nosj')->textInput() ?>

    <?= $form->field($model, 'id_palet')->textInput() ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'nourut')->textInput() ?>

    <?= $form->field($model, 'nobatch')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
