<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LogReceiveNdc */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Log Receive Ndcs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-receive-ndc-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nosj',
            'id_palet',
            'snfg',
            'nourut',
            'nobatch',
            'nama_fg',
            'status',
        ],
    ]) ?>

</div>
