<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LogReceiveNdc */

$this->title = 'Create Log Receive Ndc';
$this->params['breadcrumbs'][] = ['label' => 'Log Receive Ndcs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-receive-ndc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
