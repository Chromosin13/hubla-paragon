<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogReceiveNdcSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-receive-ndc-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nosj') ?>

    <?= $form->field($model, 'id_palet') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'nourut') ?>

    <?php // echo $form->field($model, 'nobatch') ?>

    <?php // echo $form->field($model, 'nama_fg') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
