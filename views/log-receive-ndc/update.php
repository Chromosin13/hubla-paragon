<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LogReceiveNdc */

$this->title = 'Update Log Receive Ndc: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Log Receive Ndcs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="log-receive-ndc-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
