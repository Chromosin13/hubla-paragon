<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;
use yii\helpers\Json;
use yii\widgets\Pjax;


?>
<style>
    .bawahan {
       position: fixed;
       border-top:2px solid #D3D3D3;
       left: 0;
       bottom: 0;
       width: 100%;
       padding:15px 0;
       background-color: white;
       color: white;
       text-align: center;
    }

    #modaltitle{
      display:inline-block;
    }
    .modal-body{
      font-weight:500;
      font-size:18px;
    }
    .modal-content{
      vertical-align:middle;
      margin-top: 25%;
      background-color : #f66257;
      /*text-align:center;*/
    }
</style>

<script>
    var nosj = '<?php echo $nosj; ?>';
    var operator = '<?php echo $operator; ?>';
</script>

<?php

$this->registerJsFile('js/instascan.min.js', ['position' => \yii\web\View::POS_HEAD, 'type' => 'text/javascript']);

?>


        <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <?php 

                if($nosj=="DRAFT"){
                    echo '<div class="widget-user-header bg-yellow">';
                }else{
                    echo '<div class="widget-user-header bg-green">';
                }
            ?>
              <div class="widget-user-image">
                <img class="img-circle" src="../web/images/clipboard-icon.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h2 class="widget-user-username"><?=$nosj?></h2>
              <h5 class="widget-user-desc">Surat Jalan</h5>
            </div>
            <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">
                              <div class="zoom">
                                <?=Html::a('SCAN WITH QR', ['log-receive-ndc/scan-palet','nosj'=>$nosj,'operator'=>$operator], ['class' => 'btn btn-success btn-block']);?>
                              </div>
                            <p></p>
<!--                                 <div class="input-group input-group-sm">
                                    <label class="control-label" for="logreceivendc-id_palet">ID Palet</label>
                                    <input type="text" id="id-palet" class="form-control">
                                        <span class="input-group-btn" style="padding-top:25px;">
                                          <button type="button" onclick = "check()" class="btn btn-info btn-flat">Terima</button>
                                        </span>
                                  </div> -->

                                <div class="form-group">
                                  <label>ID Palet</label>
                                  <input id = "id-palet" type="text" class="form-control" placeholder="Ketik/Scan Id Palet">
                                </div>

                                    <!-- <div class="form-group field-logreceivendc-id_palet has-success">
                                    <label class="control-label" for="logreceivendc-id_palet">ID Palet</label>
                                    <input type="text" id="id_palet_2" class="form-control" aria-invalid="false">

                                    <div class="help-block"></div>
                                    </div> -->

                            </div>

                        </div>

                      </div>
                      <!-- /.row -->
                    </div>

<?php Pjax::begin(['id' => 'evaluasi']); ?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'toolbar' => false,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            // 'status',
            // 'nama_fg',
            // 'nourut',
            // 'qty',

            // [
            //     'attribute' => 'status',
            //     'headerOptions' => ['style' => 'text-align:center; width:20%;'],
            //     'contentOptions' => function ($model, $key, $index, $column) {
            //             return ['style' => 'text-align:center; background-color:' 
            //                 . ($model->status == 'RECEIVED'
            //                     ? '#5CDB95' : '')];
            //         },
            // ],
            // [
            //     'attribute' => 'id_palet',
            //     'headerOptions' => ['style' => 'text-align:center; width:10%;'],
            //     'contentOptions' => ['style' => 'text-align:center;']
            // ],
            [
                'attribute' => 'nama_fg',
                'headerOptions' => ['style' => 'text-align:center; width:50%;'],
                'contentOptions' => ['style' => 'text-align:center;']
            ],
            [
                'attribute' => 'odoo_code',
                'headerOptions' => ['style' => 'text-align:center; width:10%;'],
                'contentOptions' => ['style' => 'text-align:center;']
            ],
            
            [
                'attribute' => 'nourut',
                'headerOptions' => ['style' => 'text-align:center; width:5%;'],
                'contentOptions' => ['style' => 'text-align:center;']
            ],
            [
                'attribute' => 'nobatch',
                'headerOptions' => ['style' => 'text-align:center; width:25%;'],
                'contentOptions' => ['style' => 'text-align:center;']
            ],
            // [
            //     'header' => 'Jumlah Koli',
            //     'value' => LogReceiveNdc::getJumlahKoli($dataProvider->id_palet),
            //     'headerOptions' => ['style' => 'text-align:center; width:20px;'],
            //     'contentOptions' => ['style' => 'text-align:center;']
            // ],
            [
                'attribute' => 'qty',
                'headerOptions' => ['style' => 'text-align:center; width:20px;'],
                'contentOptions' => ['style' => 'text-align:center;']
            ],
            // [
            //     'class' => 'kartik\grid\EditableColumn',
            //     'attribute' => 'kode_bb_scan',
            //     'headerOptions' => ['style' => 'text-align:center; width: 350px;'],
            //     'contentOptions' => ['style' => 'text-align:center;'],
            //     // 'format' => Editable::FORMAT_BUTTON,
            //     // 'editableValueOptions'=>['class'=>'text-danger'],
            //     'editableOptions' => [
            //       'asPopover' => false,
            //     ],
            // ],
            // [
            //     'class' => 'kartik\grid\ActionColumn',
            //     'template' => '{download}',
            //     'buttons' => [
            //         'download' => function ($url,$dataProvider) {
            //             return Html::a(
            //                 '<span class="fa fa-qrcode" style="color:blue; font-size:2em;"></span>',
            //                 'javascript:void(0);', 
            //                 [
            //                     'title' => 'Add',
            //                     'data-pjax' => '0',
            //                     'id' => 'scan-'.$dataProvider->id,
            //                  ]
            //             );
            //         },
            //     ],
            //     'headerOptions' => ['class' => 'kartik-sheet-style'],
            //     'contentOptions' => ['style' => 'text-align:center;'],
            // ],
            
            // [
            //     'class' => 'kartik\grid\ActionColumn',
            //     'template' => '{download} {view}',
            //     'buttons' => [
            //         'download' => function ($url) {
            //             return Html::a(
            //                 '<span class="glyphicon glyphicon-plus"></span>',
            //                 // ['log-formula-breakdown/schedule-list', 'id' => $model->id],
            //                 ['log-formula-breakdown/schedule-list'], 
            //                 [
            //                     'title' => 'Split',
            //                     'data-pjax' => '0',
            //                 ]
            //             );
            //         },
            //         'view' => function ($url) {
            //             return Html::a(
            //                 '<span class="glyphicon glyphicon-refresh"></span>',
            //                 ['log-formula-breakdown/schedule-list'], 
            //                 [
            //                     'title' => 'Split',
            //                     'data-pjax' => '0',
            //                 ]
            //             );
            //         },
            //     ],
            //     'headerOptions' => ['class' => 'kartik-sheet-style'],
            // ],
        ],
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'pjax' => true, // pjax is set to always true for this demo
            // set your toolbar
            // 'toolbar' =>  [
            //     [
            //         'content' =>
            //             Html::button('<i class="fas fa-plus"></i>', [
            //                 'class' => 'btn btn-success',
            //                 'title' => 'Add Line',
            //                 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");'
            //             ]) . ' '.
            //             Html::a('<i class="fas fa-redo"></i>', ['grid-demo'], [
            //                 'class' => 'btn btn-outline-secondary',
            //                 'title'=> 'Reset Grid',
            //                 'data-pjax' => 0, 
            //             ]), 
            //         'options' => ['class' => 'btn-group mr-2']
            //     ],
            //     '{export}',
            //     '{toggleData}',
            // ],
            // 'toggleDataContainer' => ['class' => 'btn-group mr-2'],
            // // set export properties
            // 'export' => [
            //     'fontAwesome' => true
            // ],
            // parameters from the demo form
            'bordered' => true,
            'striped' => true,
            'condensed' => true,
            'responsiveWrap' => false,
            'hover' => true,
            'export' => false,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            'rowOptions'=>function($model,$dataProvider){
                                    if($model->status == 'RECEIVED'){
                                        return ['class' => 'success'];
                                    }
                            },
            // 'showPageSummary' => true,
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
                'heading' => '<i class="fas fa-book"></i>  Task Receiving',
            ],
    ]); ?>

    <div class="bawahan">
        <div class="row">
            <!-- <div class="col-6" style="width:50%; padding:0 30px; float:left;">
                <button type="button" id="btnPrint" class="btn btn-block btn-info">PRINT RESUME PENIMBANGAN</button>
            </div> -->
            <div class="col-12" style="width:100%; padding:0 30px; float:left;">
                <button type="button" id="btnFinish" class="btn btn-block btn-danger">SELESAI PROSES RECEIVING</button>
            </div>
        </div>
    </div>

<div class="modal fade"  id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="modaltitle">Ada palet yang belum di scan!!</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="isi_modal" class="modal-body">
          <p class="text-center" style="font-size:1.25em;">Anda harus scan semua palet dalam surat jalan sebelum menyelesaikan task!<br>
          <!-- <p><b>Notes</b> : Jika ada kendala dilapangan yang menyebabkan .</p> -->
          <!-- Jika belum selesai, dan dilanjut di shift berikutnya, klik Belum All Batch. -->
      </div>
      <div class="modal-footer">
        <button id="cancel" type="button" class="btn btn-warning pull-left" data-dismiss="modal">Close</button>
        <!-- <button id="notdone" type="button" class="btn btn-secondary"></button> -->
        <button id="force-complete" type="button" class="btn btn-primary">Force Complete</button>
      </div>
    </div>
  </div>
</div>

<script>
function check(){
  var id_palet = document.getElementById("id-palet").value;
  var nosj = '<?php echo $nosj; ?>';
  var operator = '<?php echo $operator; ?>';
  // console.log(nomo);

  if ((id_palet != "")){
    $.get('index.php?r=log-receive-ndc/check-palet-by-id',{ id_palet : id_palet, nosj : nosj },function(data)
    {
        if(data==1){                    
           $.get('index.php?r=log-receive-ndc/update-status-by-id',{ id_palet : id_palet, log : "manual", pic : operator },function(data){
              if(data==1){
                window.location = "index.php?r=log-receive-ndc/task&nosj="+nosj+"&operator="+operator+"&operator="+operator;
              } else {
                alert('Tidak berhasil update log status');
              }
           });           
        } else if (data == 2){
          window.alert("Palet sudah pernah di scan sebelumnya.");
          // window.location = "index.php?r=log-task-checker/task&nomo="+nomo+"&siklus="+siklus;
        } else{
          window.alert("Palet tidak terdaftar di surat jalan.");
        }
    });
  } else {
    window.alert("ID Palet belum diisi!");
  }
} 
</script>
<?php
$script = <<< JS

    var nosj = '$nosj';
    var operator = '$operator';
    
    // $('button[id^="btnPrint"]').on("click",function() {
    //     window.location = "index.php?r=log-penimbangan-rm/pdf&id="+id_timbang+"&siklus=1";
    // });

    //Autofocus id palet manual
    document.getElementById("id-palet").focus();


    $('button[id^="btnFinish"]').on("click",function() {
        var r = confirm("Anda yakin akan menyelesaikan proses receiving?");
        if (r == true) {
            $.post("index.php?r=log-receive-ndc/finish-receiving&nosj="+nosj+"&operator="+operator, function (data){
                if (data==1){
                    window.location = "index.php?r=qc-fg-v2/terima-surat-jalan-ndc&surat_jalan="+nosj;
                } else {
                    // alert("Ada palet yang belum di scan!");
                    $('#modalConfirm').appendTo('body').modal('show');
                    return false;

                }
            });
        } else {
          
        }
    });

    $('button[id^="force-complete"]').on("click",function() {
        $.post("index.php?r=log-receive-ndc/force-complete&surat_jalan="+nosj, function(data){
            if (data == 1){
                window.location = "index.php?r=qc-fg-v2/terima-surat-jalan-ndc&surat_jalan="+nosj;
            }else{
                alert("Gagal force complete!");

            }
        });
    });

    $('#id-palet').on('change',function(){
        var nourut = 0;
        var id_palet = '';

        var result = $('#id-palet').val();
        if (result.trim() != '') {
            var input = result;
            var fields = input.split('@');
            
            if (fields.length > 1){
                var snfg= fields[0];
                var nourut = fields[1];
                var jumlah_koli = fields[2];

                $.get("index.php?r=log-receive-ndc/get-id-palet-by-old-label",{snfg : snfg , nourut : nourut},function(data){
                    var data = $.parseJSON(data);
                    var id_palet = '';
                    if (data.status == 'success'){
                        id_palet = data.value;
                        $.get('index.php?r=log-receive-ndc/check-palet-by-id',{ id_palet : id_palet, nosj : nosj },function(data)
                        {
                            if(data==1){                    
                               $.get('index.php?r=log-receive-ndc/update-status-by-id',{ id_palet : id_palet, log : "manual", pic : operator },function(data){
                                  if(data==1){
                                    window.location = "index.php?r=log-receive-ndc/task&nosj="+nosj+"&operator="+operator+"&operator="+operator;
                                  } else {
                                    alert('Tidak berhasil update log status');
                                  }
                               });           
                            } else if (data == 2){
                              window.alert("Palet sudah pernah di scan sebelumnya.");
                              // window.location = "index.php?r=log-task-checker/task&nomo="+nomo+"&siklus="+siklus;
                            } else{
                              window.alert("Palet tidak terdaftar di surat jalan.");
                            }
                        });
                    }else{
                        alert('ID Palet tidak ditemukan')
                    }
                });

            //if qr content is new format
            }else{
                var fields_new = input.split('|');

                if (fields_new.length>1){
                    var fields_new = input.split('|');
                    fields_new.forEach(function(content,index){
                        if (content.substring(0,2)=='I:'){
                          id_palet  = content.substring(2);
                        } else if (content.substring(0,2)=='N:'){
                          nourut  = content.substring(2);
                        }else{

                        }
                    });
                //If operator input manual ID Palet
                }else{
                    id_palet = result;
                }
                
                $.get('index.php?r=log-receive-ndc/check-palet-by-id',{ id_palet : id_palet, nosj : nosj },function(data)
                {
                    if(data==1){                    
                       $.get('index.php?r=log-receive-ndc/update-status-by-id',{ id_palet : id_palet, log : "manual", pic : operator },function(data){
                          if(data==1){
                            window.location = "index.php?r=log-receive-ndc/task&nosj="+nosj+"&operator="+operator+"&operator="+operator;
                          } else {
                            alert('Tidak berhasil update log status');
                          }
                       });           
                    } else if (data == 2){
                      window.alert("Palet sudah pernah di scan sebelumnya.");
                      // window.location = "index.php?r=log-task-checker/task&nomo="+nomo+"&siklus="+siklus;
                    } else{
                      window.alert("Palet tidak terdaftar di surat jalan.");
                    }
                });
            }
    
            
        }else{
            alert('Field ID Palet harus diisi');
        }
    });


    // var nosj = '$nosj';

    $('a[id^="scan-"]').on("click",function() {
        var id = $(this).attr('id');
        var wo = id.substring(id.lastIndexOf("-") + 1);
        
        console.log(wo);
        // window.location = "http://10.3.5.102/flowreport/web/index.php?r=log-task-checker/scan-bb&id="+nosj+"&wo="+wo;
        window.location = "index.php?r=log-task-checker/scan-bb&id="+nosj+"&wo="+wo;

    });

JS;
$this->registerJs($script);
?>