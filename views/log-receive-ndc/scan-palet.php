<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowAnalyst;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- CSS For Video Selector -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="ZXing for JS">

  <title>ZXing TypeScript | Decoding from camera stream</title>

  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">

<style>
.column {
  float: left;
  width: 100%;
}

.accordion {
  
  color: white;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  transition: 0.4s;
}

.active, .accordion:hover {
  background-color: #ccc;
}

.accordion:after {
  content: '\002B';
  color: white;
  font-weight: bold;
  float: right;
  margin-left: 5px;
}

.active:after {
  content: "\2212";
  color: white;
}

.panel {
  padding: 0 18px;
  background-color: white;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
}

.header{
  padding :1px;
  text-align: center;
}

</style>
</head>

<body oncopy="return false" oncut="return false" onpaste="return false">
<!-- Body -->
<div class="box box-widget widget-user">
  <!-- Add the bg color to the header using any of the bg-* classes -->
  <div class="widget-user-header bg-red-gradient" style="text-align: center; display: block; margin-left: auto; margin-right: auto;">
    <!-- <h3 class="widget-user-username"><b>Scan Kode Bahan Baku</b></h3>
    <h5 class="widget-user-desc">QR Code</h5> -->
    <div class="row">
    <div class="column" style="float:left;width:100%">
      <h4 class="widget-user-username" style="margin-bottom:0;font-weight:bolder;font-size:1.75em;">Scan Palet</h4>
      <h2 class="widget-user-desc" style="margin-bottom:0;"><b><?php echo $nosj;?></b></h2>
    </div>

  </div>
</div>
  <!-- <div class="widget-user-image">
    <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
  </div> -->
  <div class="box-footer" id="scanner" style="margin-top: 0px; padding-top:0px;">
    <div class="row" style="margin-bottom :0px; margin-top :0px; padding-top:0px;">
      <div class="box-body">
          <div class="box-body">

            <div id="sourceSelectPanel" style="display:none">
              <label for="sourceSelect" style="display:none">Switch Camera</label>
              <select id="sourceSelect" style="max-width:400px; display:none;">x
              </select>
            </div>

            <div >
              <!-- <label for="video" style="text-align: center;"><h3><b>Scan QR code dari No.MO</b></h3></label> -->
              <video id="video" width="330" height="300" style="border: 0.5px solid gray; display: block; margin-left: auto; margin-right: auto;"></video>
            </div>
              
          </div>
      </div>
    </div>
    <!-- <div style="margin-top:   0px;">
      <div class="col-md-12 column">
        <label for="kode">ID Bahan Baku</label>
        <input type="text" name="kode" id="kode" disabled>
      </div>
    </div> -->
  </div>
  
</div>
<br>

<!-- <button class="accordion bg-blue-gradient" style="padding-top:0px;">Input Manual (Klik Jika kamera tidak bisa)</button>
<div class="panel">
  <div class="col-md-12 column" style="margin-left:0px; margin-top:0px;">
    <label for="kode_manual">ID Bahan Baku</label>
    <input type="text" name="kode_manual" id="kode_manual" >
  </div>
  <button onclick="check()" style="width:50%; display: block; margin-left: auto; margin-right: auto;">Check</button>
</div> -->
</body>


<!-- Javascript -->
<!-- Calling ZXing API CDN -->
<script type="text/javascript" src="zxing.js"></script>
<!-- Code -->
<script type="text/javascript">

// setTimeout(function(){location.reload();}, 5000);

var operator = '<?php echo $operator; ?>';
var nosj = '<?php echo $nosj; ?>';
let selectedDeviceId;
const codeReader = new ZXing.BrowserMultiFormatReader()
console.log('ZXing code reader initialized')
codeReader.getVideoInputDevices()
  .then((videoInputDevices) => {
    const sourceSelect = document.getElementById('sourceSelect')
    // selectedDeviceId = videoInputDevices[1].deviceId
    if (videoInputDevices.length >= 1) {
      videoInputDevices.forEach((element) => {
        const sourceOption = document.createElement('option')
        sourceOption.text = element.label
        sourceOption.value = element.deviceId
        sourceSelect.appendChild(sourceOption)
      })

      // When Changing Camera, Reset Scanner and Execute Canvas
      sourceSelect.onchange = () => {
        selectedDeviceId = sourceSelect.value;

        codeReader.reset()
        codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
          
          if (result) {
            var operator = '<?php echo $operator; ?>';

            // console.log(result.text);
            var cam_obj = document.getElementById("scanner");
            cam_obj.remove();
            
            var input = result.text;
            var fields = input.split('@');

            if (fields.length > 1){
              var snfg= fields[0];
              var nourut = fields[1];
              var jumlah_koli = fields[2];

              // $('#qcfgv2-snfg').attr('value',snfg);
              // $('#qcfgv2-nourut').attr('value',nourut);
              // $('#qcfgv2-jumlah_koli').attr('value',jumlah_koli);

              $.get('index.php?r=log-receive-ndc/check-palet-by-snfg',{ snfg : snfg, nourut : nourut , nosj : nosj },function(data)
              {
                if(data==1){                    
                   $.get('index.php?r=log-receive-ndc/update-status-by-snfg',{ snfg : snfg, nourut : nourut , log : "scan", pic : operator },function(data){
                      if(data==1){
                        window.location = "index.php?r=log-receive-ndc/task&nosj="+nosj+"&operator="+operator;
                      } else {
                        if(window.alert('Tidak berhasil update log status')){}
                          else window.location.reload();
                      }
                   });           
                } else if (data == 2){
                  if(window.alert("Bahan Baku sudah pernah di scan sebelumnya.")){}
                  else window.location.reload();

                  // window.location = "index.php?r=log-task-checker/task&nomo="+nomo+"&siklus="+siklus;
                } else{
                  if(window.alert("Kode bahan baku tidak terdaftar di surat jalan.")){}
                  else window.location.reload();
                }
              });

            //if qr content is new format
            }else{
              var id_palet = '';
              var fields_new = input.split('|');
              // var id_palet = fields_new[I];
              fields_new.forEach(function(content,index){
                if (content.substring(0,2)=='I:'){
                  id_palet  = content.substring(2);
                }
              });
              // $('#logreceivendc-id_palet').attr('value',id_palet);
              // $('#qcfgv2-nourut').attr('value',nourut);
              $.get('index.php?r=log-receive-ndc/check-palet-by-id',{ id_palet : id_palet, nosj : nosj },function(data)
              {
                if(data==1){                    
                   $.get('index.php?r=log-receive-ndc/update-status-by-id',{ id_palet : id_palet, log : "scan", pic : operator },function(data){
                      if(data==1){
                        window.location = "index.php?r=log-receive-ndc/task&nosj="+nosj+"&operator="+operator;
                      } else {
                        if(window.alert('Tidak berhasil update log status')){}
                        else window.location.reload();
                      }
                   });           
                } else if (data == 2){
                  if(window.alert("Bahan Baku sudah pernah di scan sebelumnya.")){}
                  else window.location.reload();
                  // window.location = "index.php?r=log-task-checker/task&nomo="+nomo+"&siklus="+siklus;
                } else{
                  if(window.alert("Kode bahan baku tidak terdaftar di surat jalan.")){}
                  else window.location.reload();
                }
              });
            }

            // document.getElementById('formSubmit').click();
          // $('#scanner').show();
          }
          if (err && !(err instanceof ZXing.NotFoundException)) {
            console.error(err)
            document.getElementById('result').textContent = err
          }

        }) // on scanned 
      }; // onchange

      const sourceSelectPanel = document.getElementById('sourceSelectPanel')
      sourceSelectPanel.style.display = 'block'
    }

    // Initialize Execute Canvas when Page first loaded 
    codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {

      if (result) {
        var operator = '<?php echo $operator; ?>';
        var cam_obj = document.getElementById("scanner");
        cam_obj.remove();

        console.log(result.text);
        var input = result.text;
        var fields = input.split('@');

        if (fields.length > 1){
          // result.text = null;
          var snfg= fields[0];
          var nourut = fields[1];
          var jumlah_koli = fields[2];

          // $('#qcfgv2-snfg').attr('value',snfg);
          // $('#qcfgv2-nourut').attr('value',nourut);
          // $('#qcfgv2-jumlah_koli').attr('value',jumlah_koli);

          $.get('index.php?r=log-receive-ndc/check-palet-by-snfg',{ snfg : snfg, nourut : nourut , nosj : nosj },function(data)
          {
            if(data==1){                    
               $.get('index.php?r=log-receive-ndc/update-status-by-snfg',{ snfg : snfg, nourut : nourut , log : "scan", pic : operator },function(data){
                  if(data==1){
                    window.location = "index.php?r=log-receive-ndc/task&nosj="+nosj+"&operator="+operator;
                  } else {
                    if(window.alert('Tidak berhasil update log status')){}
                      else window.location.reload();
                  }
               });           
            } else if (data == 2){
              if(window.alert("Bahan Baku sudah pernah di scan sebelumnya.")){}
              else window.location.reload();

              // window.location = "index.php?r=log-task-checker/task&nomo="+nomo+"&siklus="+siklus;
            } else{
              if(window.alert("Kode bahan baku tidak terdaftar di surat jalan.")){}
              else window.location.reload();
            }
          });

        //if qr content is new format
        }else{
          // result.text = null;
          var id_palet ='';
          var fields_new = input.split('|');
          // var id_palet = fields_new[I];
          fields_new.forEach(function(content,index){
            if (content.substring(0,2)=='I:'){
              id_palet  = content.substring(2);
            }
          });
          // $('#logreceivendc-id_palet').attr('value',id_palet);
          // $('#qcfgv2-nourut').attr('value',nourut);
          $.get('index.php?r=log-receive-ndc/check-palet-by-id',{ id_palet : id_palet, nosj : nosj },function(data)
          {
            if(data==1){                    
               $.get('index.php?r=log-receive-ndc/update-status-by-id',{ id_palet : id_palet, log : "scan", pic : operator },function(data){
                  if(data==1){
                    window.location = "index.php?r=log-receive-ndc/task&nosj="+nosj+"&operator="+operator;
                  } else {
                    if(window.alert('Tidak berhasil update log status')){}
                    else window.location.reload();
                  }
               });           
            } else if (data == 2){
              if(window.alert("Bahan Baku sudah pernah di scan sebelumnya.")){}
              else window.location.reload();
              // window.location = "index.php?r=log-task-checker/task&nomo="+nomo+"&siklus="+siklus;
            } else{
              if(window.alert("Kode bahan baku tidak terdaftar di surat jalan.")){}
              else window.location.reload();
            }
          });
        }
        // $('#scanner').show();
      }
      if (err && !(err instanceof ZXing.NotFoundException)) {
        console.error(err)
        document.getElementById('result').textContent = err
      }
    }) // on scanned


  })
  .catch((err) => {
    console.error(err)
  })


/************************************************
 * Accordion
 *
************************************************/ 
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}


// function check(){
//   var kode = document.getElementById("kode_manual").value;
//   var operator = '<?php echo $operator; ?>';
//   // console.log(nomo);

//   if ((kode != "")){
//     $.get('index.php?r=log-task-checker/check-bb',{ wo : wo, kode_bb : kode },function(data)
//     {
//       if(data==1){                    
//          $.get('index.php?r=log-task-checker/update-status',{ wo : wo, log : "manual", pic : operator },function(data){
//             if(data==1){
//               window.location = "index.php?r=log-task-checker/task&nomo="+nomo;
//             } else {
//               alert('Tidak berhasil update log status');
//             }
//          });           
//       } else {
//         window.alert("Kode bahan baku tidak sesuai.");
//       }
//     });
//   } else {
//     window.alert("Kode bahan belum diisi!");
//   }
// } 
  </script>