<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLinePengolahan1 */

$this->title = $model->row_number;
$this->params['breadcrumbs'][] = ['label' => 'Achievement Line Pengolahan1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-line-pengolahan1-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->row_number], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->row_number], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'row_number',
            'tanggal_stop',
            'tanggal_start',
            'nama_line',
            'sediaan',
            'snfg',
            'snfg_komponen',
            'nama_fg',
            'jenis_olah',
            'leadtime',
            'std_leadtime',
            'status_leadtime:ntext',
            'plan_start_olah',
            'plan_start_kemas',
            'status_ontime_olah:ntext',
            'status_ontime_kemas:ntext',
            'is_done',
        ],
    ]) ?>

</div>
