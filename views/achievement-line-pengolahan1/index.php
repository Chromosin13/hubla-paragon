<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AchievementLinePengolahan1Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Achievement Line Pengolahan1s';
$this->params['breadcrumbs'][] = $this->title;
?>

<a href="http://10.3.81.32/untappd/pengolahan_line.html" class="btn btn-primary"><i class="fa fa-dashboard"></i> Visualisasi</a>
<br></br>

<div class="box-body">
                <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                    
                    <?php

                      $dataASTD= Yii::$app->db->createCommand(
                                      "select
                                          round((count(case when status_leadtime='ACHIEVED' then 1 end)::numeric/count(status_leadtime)::numeric)*100,1)::numeric as astd,
                                          tanggal_start
                                      from achievement_line_pengolahan_1
                                      where tanggal_start between current_date - interval '7 day' and current_date
                                      group by tanggal_start
                                      order by tanggal_start asc
                      ")->queryAll();


                      echo Highcharts::widget([
                                                'scripts' => [
                                                    'modules/exporting',
                                                    'themes/grid-light',
                                                ],
                                               'options' => [
                                                  'title' => ['text' => 'ASTD ALL'],
                                                  'xAxis' => [
                                                     'categories' => new SeriesDataHelper($dataASTD,['tanggal_start']),
                                                  ],
                                                  'yAxis' => [
                                                     'title' => ['text' => '(%) Achievement Standar Pengolahan ASTD']
                                                  ],
                                                   'plotOptions' => [
                                                                    'line' => [
                                                                        'dataLabels' => [
                                                                                            'enabled' => true
                                                                                        ],
                                                                        'enableMouseTracking' => true
                                                                        ]
                                                                    ],
                                                  'series' => [
                                                     ['name' => 'Proses', 'data' => 
                                                     new SeriesDataHelper($dataASTD,['astd:float']),
                                                     ]
                                                  ]
                                               ]
                                            ]);

                      ?>
                      </div>
                      <div class="col-md-6">
                      <?php

                      $dataASP= Yii::$app->db->createCommand(
                                      "select
                                          round((count(case when status_ontime_olah='ACHIEVED' then 1 end)::numeric/count(status_ontime_olah)::numeric)*100,1)::numeric as asp,
                                          tanggal_start
                                      from achievement_line_pengolahan_1
                                      where tanggal_start between current_date - interval '7 day' and current_date
                                      group by tanggal_start
                                      order by tanggal_start asc
                      ")->queryAll(); 

                      echo Highcharts::widget([
                                                'scripts' => [
                                                    'modules/exporting',
                                                    'themes/grid-light',
                                                ],
                                               'options' => [
                                                  'title' => ['text' => 'ASP ALL'],
                                                  'xAxis' => [
                                                     'categories' => new SeriesDataHelper($dataASP,['tanggal_start']),
                                                  ],
                                                  'yAxis' => [
                                                     'title' => ['text' => '(%) Achievement Start Planner ASP']
                                                  ],
                                                   'plotOptions' => [
                                                                    'line' => [
                                                                        'dataLabels' => [
                                                                                            'enabled' => true
                                                                                        ],
                                                                        'enableMouseTracking' => true
                                                                        ]
                                                                    ],
                                                  'series' => [
                                                     ['name' => 'Proses', 'data' => 
                                                     new SeriesDataHelper($dataASP,['asp:float']),
                                                     ]
                                                  ]
                                               ]
                                            ]);

                    ?>
                    </div>
                                        <div class="col-md-6">
                    
                    <?php

                      $dataASTD= Yii::$app->db->createCommand(
                                      "select
                                          round((count(case when status_leadtime='ACHIEVED' then 1 end)::numeric/count(status_leadtime)::numeric)*100,1)::numeric as astd,
                                          tanggal_start
                                      from achievement_line_pengolahan_1
                                      where tanggal_start between current_date - interval '7 day' and current_date
                                      and jenis_olah='OLAH 1'
                                      group by tanggal_start
                                      order by tanggal_start asc
                      ")->queryAll();


                      echo Highcharts::widget([
                                                'scripts' => [
                                                    'modules/exporting',
                                                    'themes/grid-light',
                                                ],
                                               'options' => [
                                                  'title' => ['text' => 'ASTD OLAH 1'],
                                                  'xAxis' => [
                                                     'categories' => new SeriesDataHelper($dataASTD,['tanggal_start']),
                                                  ],
                                                  'yAxis' => [
                                                     'title' => ['text' => '(%) Achievement Standar Pengolahan ASTD']
                                                  ],
                                                   'plotOptions' => [
                                                                    'line' => [
                                                                        'dataLabels' => [
                                                                                            'enabled' => true
                                                                                        ],
                                                                        'enableMouseTracking' => true
                                                                        ]
                                                                    ],
                                                  'series' => [
                                                     ['name' => 'Proses', 'data' => 
                                                     new SeriesDataHelper($dataASTD,['astd:float']),
                                                     ]
                                                  ]
                                               ]
                                            ]);

                      ?>
                      </div>
                      <div class="col-md-6">
                      <?php

                      $dataASP= Yii::$app->db->createCommand(
                                      "select
                                          round((count(case when status_ontime_olah='ACHIEVED' then 1 end)::numeric/count(status_ontime_olah)::numeric)*100,1)::numeric as asp,
                                          tanggal_start
                                      from achievement_line_pengolahan_1
                                      where tanggal_start between current_date - interval '7 day' and current_date
                                      and jenis_olah='OLAH 1'
                                      group by tanggal_start
                                      order by tanggal_start asc
                      ")->queryAll(); 

                      echo Highcharts::widget([
                                                'scripts' => [
                                                    'modules/exporting',
                                                    'themes/grid-light',
                                                ],
                                               'options' => [
                                                  'title' => ['text' => 'ASP OLAH 1'],
                                                  'xAxis' => [
                                                     'categories' => new SeriesDataHelper($dataASP,['tanggal_start']),
                                                  ],
                                                  'yAxis' => [
                                                     'title' => ['text' => '(%) Achievement Start Planner ASP']
                                                  ],
                                                   'plotOptions' => [
                                                                    'line' => [
                                                                        'dataLabels' => [
                                                                                            'enabled' => true
                                                                                        ],
                                                                        'enableMouseTracking' => true
                                                                        ]
                                                                    ],
                                                  'series' => [
                                                     ['name' => 'Proses', 'data' => 
                                                     new SeriesDataHelper($dataASP,['asp:float']),
                                                     ]
                                                  ]
                                               ]
                                            ]);

                    ?>
                    </div>
                                        <div class="col-md-6">
                    
                    <?php

                      $dataASTD= Yii::$app->db->createCommand(
                                      "select
                                          round((count(case when status_leadtime='ACHIEVED' then 1 end)::numeric/count(status_leadtime)::numeric)*100,1)::numeric as astd,
                                          tanggal_start
                                      from achievement_line_pengolahan_1
                                      where tanggal_start between current_date - interval '7 day' and current_date
                                      and jenis_olah='OLAH 2'
                                      group by tanggal_start
                                      order by tanggal_start asc
                      ")->queryAll();


                      echo Highcharts::widget([
                                                'scripts' => [
                                                    'modules/exporting',
                                                    'themes/grid-light',
                                                ],
                                               'options' => [
                                                  'title' => ['text' => 'ASTD OLAH 2'],
                                                  'xAxis' => [
                                                     'categories' => new SeriesDataHelper($dataASTD,['tanggal_start']),
                                                  ],
                                                  'yAxis' => [
                                                     'title' => ['text' => '(%) Achievement Standar Pengolahan ASTD']
                                                  ],
                                                   'plotOptions' => [
                                                                    'line' => [
                                                                        'dataLabels' => [
                                                                                            'enabled' => true
                                                                                        ],
                                                                        'enableMouseTracking' => true
                                                                        ]
                                                                    ],
                                                  'series' => [
                                                     ['name' => 'Proses', 'data' => 
                                                     new SeriesDataHelper($dataASTD,['astd:float']),
                                                     ]
                                                  ]
                                               ]
                                            ]);

                      ?>
                      </div>
                      <div class="col-md-6">
                      <?php

                      $dataASP= Yii::$app->db->createCommand(
                                      "select
                                          round((count(case when status_ontime_olah='ACHIEVED' then 1 end)::numeric/count(status_ontime_olah)::numeric)*100,1)::numeric as asp,
                                          tanggal_start
                                      from achievement_line_pengolahan_1
                                      where tanggal_start between current_date - interval '7 day' and current_date
                                      and jenis_olah='OLAH 2'
                                      group by tanggal_start
                                      order by tanggal_start asc
                      ")->queryAll(); 

                      echo Highcharts::widget([
                                                'scripts' => [
                                                    'modules/exporting',
                                                    'themes/grid-light',
                                                ],
                                               'options' => [
                                                  'title' => ['text' => 'ASP OLAH 2'],
                                                  'xAxis' => [
                                                     'categories' => new SeriesDataHelper($dataASP,['tanggal_start']),
                                                  ],
                                                  'yAxis' => [
                                                     'title' => ['text' => '(%) Achievement Start Planner ASP']
                                                  ],
                                                   'plotOptions' => [
                                                                    'line' => [
                                                                        'dataLabels' => [
                                                                                            'enabled' => true
                                                                                        ],
                                                                        'enableMouseTracking' => true
                                                                        ]
                                                                    ],
                                                  'series' => [
                                                     ['name' => 'Proses', 'data' => 
                                                     new SeriesDataHelper($dataASP,['asp:float']),
                                                     ]
                                                  ]
                                               ]
                                            ]);

                    ?>
                    </div>
                                        <div class="col-md-6">
                    
                    <?php

                      $dataASTD= Yii::$app->db->createCommand(
                                      "select
                                          round((count(case when status_leadtime='ACHIEVED' then 1 end)::numeric/count(status_leadtime)::numeric)*100,1)::numeric as astd,
                                          tanggal_start
                                      from achievement_line_pengolahan_1
                                      where tanggal_start between current_date - interval '7 day' and current_date
                                      and jenis_olah='OLAH PREMIX'
                                      group by tanggal_start
                                      order by tanggal_start asc
                      ")->queryAll();


                      echo Highcharts::widget([
                                                'scripts' => [
                                                    'modules/exporting',
                                                    'themes/grid-light',
                                                ],
                                               'options' => [
                                                  'title' => ['text' => 'ASTD OLAH PREMIX'],
                                                  'xAxis' => [
                                                     'categories' => new SeriesDataHelper($dataASTD,['tanggal_start']),
                                                  ],
                                                  'yAxis' => [
                                                     'title' => ['text' => '(%) Achievement Standar Pengolahan ASTD']
                                                  ],
                                                   'plotOptions' => [
                                                                    'line' => [
                                                                        'dataLabels' => [
                                                                                            'enabled' => true
                                                                                        ],
                                                                        'enableMouseTracking' => true
                                                                        ]
                                                                    ],
                                                  'series' => [
                                                     ['name' => 'Proses', 'data' => 
                                                     new SeriesDataHelper($dataASTD,['astd:float']),
                                                     ]
                                                  ]
                                               ]
                                            ]);

                      ?>
                      </div>
                      <div class="col-md-6">
                      <?php

                      $dataASP= Yii::$app->db->createCommand(
                                      "select
                                          round((count(case when status_ontime_olah='ACHIEVED' then 1 end)::numeric/count(status_ontime_olah)::numeric)*100,1)::numeric as asp,
                                          tanggal_start
                                      from achievement_line_pengolahan_1
                                      where tanggal_start between current_date - interval '7 day' and current_date
                                      and jenis_olah='OLAH PREMIX'
                                      group by tanggal_start
                                      order by tanggal_start asc
                      ")->queryAll(); 

                      echo Highcharts::widget([
                                                'scripts' => [
                                                    'modules/exporting',
                                                    'themes/grid-light',
                                                ],
                                               'options' => [
                                                  'title' => ['text' => 'ASP OLAH PREMIX'],
                                                  'xAxis' => [
                                                     'categories' => new SeriesDataHelper($dataASP,['tanggal_start']),
                                                  ],
                                                  'yAxis' => [
                                                     'title' => ['text' => '(%) Achievement Start Planner ASP']
                                                  ],
                                                   'plotOptions' => [
                                                                    'line' => [
                                                                        'dataLabels' => [
                                                                                            'enabled' => true
                                                                                        ],
                                                                        'enableMouseTracking' => true
                                                                        ]
                                                                    ],
                                                  'series' => [
                                                     ['name' => 'Proses', 'data' => 
                                                     new SeriesDataHelper($dataASP,['asp:float']),
                                                     ]
                                                  ]
                                               ]
                                            ]);

                    ?>
                    </div>
                                        <div class="col-md-6">
                    
                    <?php

                      $dataASTD= Yii::$app->db->createCommand(
                                      "select
                                          round((count(case when status_leadtime='ACHIEVED' then 1 end)::numeric/count(status_leadtime)::numeric)*100,1)::numeric as astd,
                                          tanggal_start
                                      from achievement_line_pengolahan_1
                                      where tanggal_start between current_date - interval '7 day' and current_date
                                      and jenis_olah='ADJUST OLAH 1'
                                      group by tanggal_start
                                      order by tanggal_start asc
                      ")->queryAll();


                      echo Highcharts::widget([
                                                'scripts' => [
                                                    'modules/exporting',
                                                    'themes/grid-light',
                                                ],
                                               'options' => [
                                                  'title' => ['text' => 'ASTD ADJUST OLAH 1'],
                                                  'xAxis' => [
                                                     'categories' => new SeriesDataHelper($dataASTD,['tanggal_start']),
                                                  ],
                                                  'yAxis' => [
                                                     'title' => ['text' => '(%) Achievement Standar Pengolahan ASTD']
                                                  ],
                                                   'plotOptions' => [
                                                                    'line' => [
                                                                        'dataLabels' => [
                                                                                            'enabled' => true
                                                                                        ],
                                                                        'enableMouseTracking' => true
                                                                        ]
                                                                    ],
                                                  'series' => [
                                                     ['name' => 'Proses', 'data' => 
                                                     new SeriesDataHelper($dataASTD,['astd:float']),
                                                     ]
                                                  ]
                                               ]
                                            ]);

                      ?>
                      </div>
                      <div class="col-md-6">
                      <?php

                      $dataASP= Yii::$app->db->createCommand(
                                      "select
                                          round((count(case when status_ontime_olah='ACHIEVED' then 1 end)::numeric/count(status_ontime_olah)::numeric)*100,1)::numeric as asp,
                                          tanggal_start
                                      from achievement_line_pengolahan_1
                                      where tanggal_start between current_date - interval '7 day' and current_date
                                      and jenis_olah='ADJUST OLAH 1'
                                      group by tanggal_start
                                      order by tanggal_start asc
                      ")->queryAll(); 

                      echo Highcharts::widget([
                                                'scripts' => [
                                                    'modules/exporting',
                                                    'themes/grid-light',
                                                ],
                                               'options' => [
                                                  'title' => ['text' => 'ASP ADJUST OLAH 1'],
                                                  'xAxis' => [
                                                     'categories' => new SeriesDataHelper($dataASP,['tanggal_start']),
                                                  ],
                                                  'yAxis' => [
                                                     'title' => ['text' => '(%) Achievement Start Planner ASP']
                                                  ],
                                                   'plotOptions' => [
                                                                    'line' => [
                                                                        'dataLabels' => [
                                                                                            'enabled' => true
                                                                                        ],
                                                                        'enableMouseTracking' => true
                                                                        ]
                                                                    ],
                                                  'series' => [
                                                     ['name' => 'Proses', 'data' => 
                                                     new SeriesDataHelper($dataASP,['asp:float']),
                                                     ]
                                                  ]
                                               ]
                                            ]);

                    ?>
                    </div>
                                        <div class="col-md-6">
                    
                    <?php

                      $dataASTD= Yii::$app->db->createCommand(
                                      "select
                                          round((count(case when status_leadtime='ACHIEVED' then 1 end)::numeric/count(status_leadtime)::numeric)*100,1)::numeric as astd,
                                          tanggal_start
                                      from achievement_line_pengolahan_1
                                      where tanggal_start between current_date - interval '7 day' and current_date
                                      and jenis_olah='ADJUST OLAH 2'
                                      group by tanggal_start
                                      order by tanggal_start asc
                      ")->queryAll();


                      echo Highcharts::widget([
                                                'scripts' => [
                                                    'modules/exporting',
                                                    'themes/grid-light',
                                                ],
                                               'options' => [
                                                  'title' => ['text' => 'ASTD ADJUST OLAH 2'],
                                                  'xAxis' => [
                                                     'categories' => new SeriesDataHelper($dataASTD,['tanggal_start']),
                                                  ],
                                                  'yAxis' => [
                                                     'title' => ['text' => '(%) Achievement Standar Pengolahan ASTD']
                                                  ],
                                                   'plotOptions' => [
                                                                    'line' => [
                                                                        'dataLabels' => [
                                                                                            'enabled' => true
                                                                                        ],
                                                                        'enableMouseTracking' => true
                                                                        ]
                                                                    ],
                                                  'series' => [
                                                     ['name' => 'Proses', 'data' => 
                                                     new SeriesDataHelper($dataASTD,['astd:float']),
                                                     ]
                                                  ]
                                               ]
                                            ]);

                      ?>
                      </div>
                      <div class="col-md-6">
                      <?php

                      $dataASP= Yii::$app->db->createCommand(
                                      "select
                                          round((count(case when status_ontime_olah='ACHIEVED' then 1 end)::numeric/count(status_ontime_olah)::numeric)*100,1)::numeric as asp,
                                          tanggal_start
                                      from achievement_line_pengolahan_1
                                      where tanggal_start between current_date - interval '7 day' and current_date
                                      and jenis_olah='ADJUST OLAH 2'
                                      group by tanggal_start
                                      order by tanggal_start asc
                      ")->queryAll(); 

                      echo Highcharts::widget([
                                                'scripts' => [
                                                    'modules/exporting',
                                                    'themes/grid-light',
                                                ],
                                               'options' => [
                                                  'title' => ['text' => 'ASP ADJUST OLAH 2'],
                                                  'xAxis' => [
                                                     'categories' => new SeriesDataHelper($dataASP,['tanggal_start']),
                                                  ],
                                                  'yAxis' => [
                                                     'title' => ['text' => '(%) Achievement Start Planner ASP']
                                                  ],
                                                   'plotOptions' => [
                                                                    'line' => [
                                                                        'dataLabels' => [
                                                                                            'enabled' => true
                                                                                        ],
                                                                        'enableMouseTracking' => true
                                                                        ]
                                                                    ],
                                                  'series' => [
                                                     ['name' => 'Proses', 'data' => 
                                                     new SeriesDataHelper($dataASP,['asp:float']),
                                                     ]
                                                  ]
                                               ]
                                            ]);

                    ?>
                    </div>
                <!-- <?php


                                 $count = Yii::$app->db->createCommand("
                                                                  SELECT count(case when status_leadtime='ACHIEVED' then 1 end) as achieved,
                                                                       count(case when status_leadtime='NOT ACHIEVED' then 1 end) as not_achieved,
                                                                       count(case when status_leadtime='LEADTIME STANDARD MISSING' then 1 end) as missing
                                                                    FROM 
                                                                        (SELECT a.*,
                                                                            slp.leadtime as std_leadtime,
                                                                            case when a.leadtime<=slp.leadtime then 'ACHIEVED'
                                                                            when slp.leadtime is null then 'LEADTIME STANDARD MISSING'
                                                                            else 'NOT ACHIEVED' end as status_leadtime
                                                                        FROM
                                                                            (SELECT 
                                                                                    snfg_komponen
                                                                                    ,snfg
                                                                                    ,jenis_olah
                                                                                    ,max(tanggal_stop) tanggal_stop
                                                                                    ,sum(is_done) is_done
                                                                                    ,sum(delta_hour_net) leadtime
                                                                                FROM pengolahan_leadtime_2
                                                                                GROUP BY snfg_komponen
                                                                                    ,snfg
                                                                                    ,jenis_olah) a
                                                                            LEFT JOIN std_leadtime_pengolahan slp on slp.koitem_bulk=split_part(a.snfg_komponen,'/',1)
                                                                            WHERE a.is_done>0
                                                                    ) b")->queryAll();

                                 $implode_achieved = intval(implode(',',array_column($count,'achieved')));     
                                 $implode_nachieved = intval(implode(',',array_column($count,'not_achieved')));  
                                 $implode_missing = intval(implode(',',array_column($count,'missing')));    

                                  $tanggalList = Yii::$app->db->createCommand("
                                                                              SELECT tanggal_stop,
                                                                               count(case when status_leadtime='ACHIEVED' then 1 end) as achieved,
                                                                               count(case when status_leadtime='NOT ACHIEVED' then 1 end) as not_achieved,
                                                                               count(case when status_leadtime='LEADTIME STANDARD MISSING' then 1 end) as missing,
                                                                               count(status_leadtime) as all
                                                                        FROM 
                                                                            (SELECT a.*,
                                                                                slp.leadtime as std_leadtime,
                                                                                case when a.leadtime<=slp.leadtime then 'ACHIEVED'
                                                                                when slp.leadtime is null then 'LEADTIME STANDARD MISSING'
                                                                                else 'NOT ACHIEVED' end as status_leadtime
                                                                            FROM
                                                                                (SELECT 
                                                                                        snfg_komponen
                                                                                        ,snfg
                                                                                        ,jenis_olah
                                                                                        ,max(tanggal_stop) tanggal_stop
                                                                                        ,sum(is_done) is_done
                                                                                        ,sum(delta_hour_net) leadtime
                                                                                    FROM pengolahan_leadtime_2
                                                                                    GROUP BY snfg_komponen
                                                                                        ,snfg
                                                                                        ,jenis_olah) a
                                                                                LEFT JOIN std_leadtime_pengolahan slp on slp.koitem_bulk=split_part(a.snfg_komponen,'/',1)
                                                                                WHERE a.is_done>0
                                                                            ) b
                                                                        GROUP BY tanggal_stop
                                                                        ORDER BY tanggal_stop asc")->queryAll();

                    // End of Combination Backend Code

                                                      echo Highcharts::widget([
                                                      'scripts' => [
                                                          'modules/exporting',
                                                          'themes/grid-light',
                                                      ],
                                                      'options' => [
                                                          'rangeSelector' => [
                                                                //'inputEnabled' => new JsExpression('$("#container").width() > 480'),
                                                                'selected' => 1
                                                            ],
                                                          'title' => [
                                                              'text' => 'Achievement Line Tanggal',
                                                          ],
                                                          'xAxis' => [
                                                              //'type' => new JsExpression('datetime'),
                                                              'categories' => new SeriesDataHelper($tanggalList,['tanggal_stop']),
                                                              //['PLANNER', 'pengolahan', 'PENGOLAHAN','QCBULK', 'INKJET', 'KEMAS 1','KEMAS 2', 'QCFG', 'QC TIMBANG FG'],
                                                          ],
                                                          'labels' => [
                                                              'items' => [
                                                                  [
                                                                      'html' => 'Flowboard',
                                                                      'style' => [
                                                                          'left' => '50px',
                                                                          'top' => '18px',
                                                                          'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                                                                      ],
                                                                  ],
                                                              ],
                                                          ],
                                                          'series' => [
                                                              [
                                                                  'type' => 'column',
                                                                  'name' => 'Achieved',
                                                                  'data' => new SeriesDataHelper($tanggalList,['achieved']),
                                                              ],
                                                              [
                                                                  'type' => 'column',
                                                                  'name' => 'Not Achieved',
                                                                  'data' => new SeriesDataHelper($tanggalList,['not_achieved']),
                                                              ],
                                                              [
                                                                  'type' => 'column',
                                                                  'name' => 'Missing',
                                                                  'data' => new SeriesDataHelper($tanggalList,['missing']),
                                                              ],
                                                              [
                                                                  'type' => 'spline',
                                                                  'name' => 'Total',
                                                                  'data' => new SeriesDataHelper($tanggalList,['all']),
                                                                  'marker' => [
                                                                      'lineWidth' => 2,
                                                                      'lineColor' => new JsExpression('Highcharts.getOptions().colors[3]'),
                                                                      'fillColor' => 'white',
                                                                  ],
                                                              ],
                                                              [
                                                                  'type' => 'pie',
                                                                  'name' => 'Rekap',
                                                                  'data' => [
                                                                      [
                                                                          'name' => 'Achieved',
                                                                          'y' => $implode_achieved,
                                                                          'color' => new JsExpression('Highcharts.getOptions().colors[0]'), // Achieved's color
                                                                      ],
                                                                      [
                                                                          'name' => 'Not Achieved',
                                                                          'y' => $implode_nachieved,//new SeriesDataHelper($countWno,['count']),
                                                                          'color' => new JsExpression('Highcharts.getOptions().colors[1]'), // Not Achieved's color
                                                                      ],
                                                                      [
                                                                          'name' => 'Missing',
                                                                          'y' => $implode_missing,//new SeriesDataHelper($countWno,['count']),
                                                                          'color' => new JsExpression('Highcharts.getOptions().colors[2]'), // Not Achieved's color
                                                                      ],
                                                                  ],
                                                                  'center' => [100, 50],
                                                                  'size' => 60,
                                                                  'showInLegend' => true,
                                                                  'dataLabels' => [
                                                                      'enabled' => true,
                                                                  ],
                                                              ],
                                                          ],
                                                      ]
                                                  ]);

                
                                ?> -->
                </div>
                </div>
    </div>

<div class="achievement-line-pengolahan1-index">
    <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'primary', 'heading'=>'Achievement Line pengolahan'],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                [
                    'attribute'=>'tanggal_stop', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                ],
                [
                    'attribute'=>'tanggal_start', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>1,
                ],
                [

                    'attribute'=>'nama_line', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>2,
                ],
                  [
                    'attribute'=>'sediaan', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>3,
                ],
                 [
                    'attribute'=>'nama_fg', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>4,
                ],
                [
                    'attribute'=>'snfg', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>5,
                ],
                [
                    'attribute'=>'snfg_komponen', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>6,
                ],
                'jenis_olah',
                'delta_hour_net',
                'std_leadtime',
                'status_leadtime:ntext',
                'plan_start_olah',
                'plan_end_olah',
                'status_ontime_olah:ntext',
                'status_ontime_kemas:ntext',
                'is_done',
             ],
        ]);
    
    ?>

    
</div>
