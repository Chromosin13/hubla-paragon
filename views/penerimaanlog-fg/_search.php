<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PenerimaanlogFgSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="penerimaanlog-fg-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'jumlah_koli') ?>

    <?= $form->field($model, 'qty_per_koli') ?>

    <?= $form->field($model, 'qty_koli_receh') ?>

    <?= $form->field($model, 'jumlah') ?>

    <?php // echo $form->field($model, 'jumlah_koli_palet') ?>

    <?php // echo $form->field($model, 'jumlah_palet') ?>

    <?php // echo $form->field($model, 'datetime_start') ?>

    <?php // echo $form->field($model, 'datetime_stop') ?>

    <?php // echo $form->field($model, 'adminlog_fg_id') ?>

    <?php // echo $form->field($model, 'jumlah_pcs_palet') ?>

    <?php // echo $form->field($model, 'koordinat_penempatan') ?>

    <?php // echo $form->field($model, 'operator_id') ?>

    <?php // echo $form->field($model, 'penempatan') ?>

    <?php // echo $form->field($model, 'sisa_koli_palet') ?>

    <?php // echo $form->field($model, 'sisa_pcs_palet') ?>

    <?php // echo $form->field($model, 'status_print') ?>

    <?php // echo $form->field($model, 'flag_sampler') ?>

    <?php // echo $form->field($model, 'nopo') ?>

    <?php // echo $form->field($model, 'no_surjal') ?>

    <?php // echo $form->field($model, 'koitem_fg') ?>

    <?php // echo $form->field($model, 'nama_fg') ?>

    <?php // echo $form->field($model, 'supplier') ?>

    <?php // echo $form->field($model, 'batch') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
