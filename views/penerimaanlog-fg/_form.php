<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PenerimaanlogFg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="penerimaanlog-fg-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jumlah_koli')->textInput() ?>

    <?= $form->field($model, 'qty_per_koli')->textInput() ?>

    <?= $form->field($model, 'qty_koli_receh')->textInput() ?>

    <?= $form->field($model, 'jumlah')->textInput() ?>

    <?= $form->field($model, 'jumlah_koli_palet')->textInput() ?>

    <?= $form->field($model, 'jumlah_palet')->textInput() ?>

    <?= $form->field($model, 'datetime_start')->textInput() ?>

    <?= $form->field($model, 'datetime_stop')->textInput() ?>

    <?= $form->field($model, 'adminlog_fg_id')->textInput() ?>

    <?= $form->field($model, 'jumlah_pcs_palet')->textInput() ?>

    <?= $form->field($model, 'koordinat_penempatan')->textInput() ?>

    <?= $form->field($model, 'operator_id')->textInput() ?>

    <?= $form->field($model, 'penempatan')->textInput() ?>

    <?= $form->field($model, 'sisa_koli_palet')->textInput() ?>

    <?= $form->field($model, 'sisa_pcs_palet')->textInput() ?>

    <?= $form->field($model, 'status_print')->textInput() ?>

    <?= $form->field($model, 'flag_sampler')->textInput() ?>

    <?= $form->field($model, 'nopo')->textInput() ?>

    <?= $form->field($model, 'no_surjal')->textInput() ?>

    <?= $form->field($model, 'koitem_fg')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'supplier')->textInput() ?>

    <?= $form->field($model, 'batch')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
