<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\OperatorPenerimaanlog;
use app\models\Penempatan;
use wbraganca\tagsinput\TagsinputWidget;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FormKendaliTugasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<style>
    .bootstrap-tagsinput {
        /*padding: 50px;*/
        width: 1920px;
        height: auto;
    }

    .zoom {
        /*padding: 50px;*/
        transition: transform .3s; /* Animation */
        width: auto;
        height: auto;
        margin: 0 auto;
    }

    .zoom:hover {
        transform: scale(0.99); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    }

    @import url(https://fonts.googleapis.com/css?family=Lato:900);
    *, *:before, *:after{
      box-sizing:border-box;
    }
    body{
      font-family: 'Lato', sans-serif;
        ;
    }
    div.foo{
      width: 90%;
      margin: 0 auto;
      text-align: center;
    }
    .letter{
      display: inline-block;
      font-weight: 900;
      font-size: 4em;
      margin: 0.2em;
      position: relative;
      color: #00B4F1;
      transform-style: preserve-3d;
      perspective: 400;
      z-index: 1;
    }
    .letter:before, .letter:after{
      position:absolute;
      content: attr(data-letter);
      transform-origin: top left;
      top:0;
      left:0;
    }
    .letter, .letter:before, .letter:after{
      transition: all 0.3s ease-in-out;
    }
    .letter:before{
      color: #fff;
      text-shadow:
        -1px 0px 1px rgba(255,255,255,.8),
        1px 0px 1px rgba(0,0,0,.8);
      z-index: 3;
      transform:
        rotateX(0deg)
        rotateY(-15deg)
        rotateZ(0deg);
    }
    .letter:after{
      color: rgba(0,0,0,.11);
      z-index:2;
      transform:
        scale(1.08,1)
        rotateX(0deg)
        rotateY(0deg)
        rotateZ(0deg)
        skew(0deg,1deg);
    }
    .letter:hover:before{
      color: #fafafa;
      transform:
        rotateX(0deg)
        rotateY(-40deg)
        rotateZ(0deg);
    }
    .letter:hover:after{
      transform:
        scale(1.08,1)
        rotateX(0deg)
        rotateY(40deg)
        rotateZ(0deg)
        skew(0deg,22deg);
    }
    .table-responsive{
      overflow-y: hidden;
    }

    @-moz-document url-prefix() {
      fieldset { display: table-cell; }
    }

</style>

<div class="box box-widget widget-user">
    <div class="widget-user-header bg-red-gradient">
      <h3 class="widget-user-username"><b>Form Kendali</b></h3>
      <h4 class="widget-user-desc">Operator Penerimaan Logistik</h4>
    </div>
    <div class="widget-user-image">
      <img class="img-circle" src="../web/images/gambarlog/target.png" alt="User Avatar">
    </div>
    <br></br>


    <div class="box-body">
        <div class="zoom">
            <?=Html::a('SCAN WITH QR', ['penerimaanlog-fg/check-penerimaan-qr'], ['class' => 'btn btn-danger btn-block']);?>
        </div>
        <p></p>
        <div class="penerimaanlog-fg-form">

            <div class="form-group field-penerimaanlogfg-nopo has-success">
            <label class="control-label" for="penerimaanlogfg-nopo">No PO</label>
            <input type="text" id="penerimaanlogfg-nopo" class="form-control" name="penerimaanlogfg[nopo]" aria-invalid="false">

            <div class="help-block"></div>
            </div>

        </div>

        <!-- <div class="zoom">
            <p>
                <?= Html::a('Klik ini untuk Membuat Form', ['create'], ['class' => 'btn btn-success btn-block']) ?>
            </p>
        </div> -->
        <?php Pjax::begin(['id'=>'lessons-grid-container-id']); ?>
        <?= GridView::widget([

            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            'columns' => [
                ['class' => 'kartik\grid\SerialColumn'],

                [
                'header'=>'Print Label',
                'format'=>'raw',
                'value' => function($model)
                {
                      if ($model->status_print == 'OK')
                      {
                        return
                        Html::a('<i class="glyphicon glyphicon glyphicon-ok"></i>', ['penerimaanlog-fg/pdf', 'id' => $model->id],['class' => 'btn btn-success','data-pjax'=>0]);
                      }
                      else
                      {
                        return
                        Html::a('<img class="img-circle" src="../web/images/printer-icon.png" alt="Terima" width="42" height="42">', ['penerimaanlog-fg/pdf', 'id' => $model->id],['class' => 'btn btn-danger','data-pjax'=>0, 'title' => 'Print Label', 'data-confirm' => Yii::t('app', 'Print Label?')]);
                      }
                }
                ],
                // [
                //     'class' => 'kartik\grid\ActionColumn',
                //     'template' => '{print-label}',
                //     'contentOptions' => ['style' => 'max-width:50px;'],
                //     'buttons' => [
                //         'print-label' => function ($url,$model,$id) {
                //           if ($model->status_print == 'OK')
                //           {
                //             return Html::a(
                //                 '<i class="glyphicon glyphicon glyphicon-ok"></i>',
                //                 $url,
                //                 ['class' => 'btn btn-success modalButton', 'title'=>'view/edit', ],
                //                 [
                //                     'title' => 'Print Label?',
                //                 ]
                //             );
                //           }
                //           else
                //           {
                //             return Html::a(
                //                 '<img class="img-circle" src="../web/images/printer-icon.png" alt="Terima" width="42" height="42">',
                //                 $url,
                //                 // ['class' => 'btn btn-warning modalButton', 'title'=>'view/edit', ],
                //                 [
                //                     'title' => 'Print Label',
                //                     'data-pjax' => '0',
                //                     'data-confirm' => Yii::t('app', 'Print Label?'),
                //                 ]
                //             );
                //           }
                //         },
                //     ],
                // ],
                'batch',
                'nopo',
                'nama_fg',
                'jumlah_koli',
                'qty_per_koli',
                'qty_koli_receh',
                'jumlah',
                'jumlah_koli_palet',
                'jumlah_palet',
                'jumlah_pcs_palet',
                'koordinat_penempatan',
                // 'id',
                'penempatan',
                'datetime_start',
                // 'datetime_write',
                'datetime_stop',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>

<?php
$script = <<< JS



document.getElementById("penerimaanlogfg-nopo").focus();

// Check SNFG Komponen Validity

$('#penerimaanlogfg-nopo').change(function(){

    var nopo = $('#penerimaanlogfg-nopo').val();

    $.get('index.php?r=adminlog-fg/check-nopo',{ nopo : nopo },function(data){
        var data = $.parseJSON(data);
        //alert(JSON.stringify(data));
        if(data.id>=1){

          window.location = "index.php?r=penerimaanlog-fg/update-item&nopo="+nopo; //"index.php?r=operator-penerimaanlog/create-operator-penerimaanlog&no_po="+no_po; "index.php?r=nama-operator%2Fcreate";

        }
        else{

          alert('Nomor PO Tidak Ditemukan');
        }
    });
});

JS;
$this->registerJs($script);
