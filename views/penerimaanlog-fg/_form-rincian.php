<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\PenerimaanlogFg;
use app\models\Penempatan;
use app\models\AdminlogFg;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\dialog\Dialog;
use yii\web\JsExpression;
use kartik\date\DatePicker;
use kartik\widgets\DateTimePicker;
use kartik\file\FileInput;
use yii\web\UploadedFile;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model app\models\NamaOperator */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .bootstrap-tagsinput {
        /*padding: 50px;*/
        width: 1920px;
        height: auto;
    }

    .zoom {
        /*padding: 50px;*/
        transition: transform .3s; /* Animation */
        width: auto;
        height: auto;
        margin: 0 auto;
    }

    .zoom:hover {
        transform: scale(0.99); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    }

    @import url(https://fonts.googleapis.com/css?family=Lato:900);
    *, *:before, *:after{
      box-sizing:border-box;
    }
    body{
      font-family: 'Lato', sans-serif;
        ;
    }
    div.foo{
      width: 90%;
      margin: 0 auto;
      text-align: center;
    }
    .letter{
      display: inline-block;
      font-weight: 900;
      font-size: 4em;
      margin: 0.2em;
      position: relative;
      color: #00B4F1;
      transform-style: preserve-3d;
      perspective: 400;
      z-index: 1;
    }
    .letter:before, .letter:after{
      position:absolute;
      content: attr(data-letter);
      transform-origin: top left;
      top:0;
      left:0;
    }
    .letter, .letter:before, .letter:after{
      transition: all 0.3s ease-in-out;
    }
    .letter:before{
      color: #fff;
      text-shadow:
        -1px 0px 1px rgba(255,255,255,.8),
        1px 0px 1px rgba(0,0,0,.8);
      z-index: 3;
      transform:
        rotateX(0deg)
        rotateY(-15deg)
        rotateZ(0deg);
    }
    .letter:after{
      color: rgba(0,0,0,.11);
      z-index:2;
      transform:
        scale(1.08,1)
        rotateX(0deg)
        rotateY(0deg)
        rotateZ(0deg)
        skew(0deg,1deg);
    }
    .letter:hover:before{
      color: #fafafa;
      transform:
        rotateX(0deg)
        rotateY(-40deg)
        rotateZ(0deg);
    }
    .letter:hover:after{
      transform:
        scale(1.08,1)
        rotateX(0deg)
        rotateY(40deg)
        rotateZ(0deg)
        skew(0deg,22deg);
</style>


<div class="box box-widget widget-user">
    <div class="widget-user-header bg-yellow-gradient">
      <h3 class="widget-user-username"><b>Membuat Form</b></h3>
      <h5 class="widget-user-desc">Operator Penerimaan Logistik</h5>
    </div>
    <div class="widget-user-image">
      <img class="img-circle" src="../web/images/gambarlog/question.png" alt="User Avatar">
    </div>
    <br></br>
  <div class="box-body">

    <div class="nama-admin-form">

        <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

        <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>


        <?php DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 7, // the maximum times, an element can be added (default 999)
            'min' => 0, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $modelsPenerimaanlogFg[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'nopo',
                'no_surjal',
                'koitem_fg',
                'nama_fg',
                'supplier',
                'batch',
                'jumlah_koli',
                'qty_per_koli',
                'qty_koli_receh',
                'jumlah',
                'jumlah_koli_palet',
                'jumlah_palet',
                'jumlah_pcs_palet',
                'koordinat_penempatan',
                'penempatan',

            ],
        ]); ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <i class="glyphicon glyphicon-envelope"></i> <?php echo $nopo; ?>
                    <button type="button" class="add-item btn btn-success btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> ADD</button>
                </h4>
            </div>
            <div class="panel-body">
                <div class="container-items"><!-- widgetBody -->
                <?php foreach ($modelsPenerimaanlogFg as $i => $modelPenerimaanlogFg): ?>
                    <div class="item panel panel-default"><!-- widgetItem -->
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">Form Operator</h3>
                            <div class="pull-right">
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                                // necessary for update action.
                                if (! $modelPenerimaanlogFg->isNewRecord) {
                                    echo Html::activeHiddenInput($modelPenerimaanlogFg, "[{$i}]id");
                                }
                            ?>

                                		    <div class="row">

                                          <div id="dua">
                                            <div class="col-md-4">
                                              <?= $form->field($modelPenerimaanlogFg, "[{$i}]nopo")->textInput(['readOnly'=>true]) ?>
                                            </div>
                                          </div>

                                          <div id="dua">
                                            <div class="col-md-4">
                                              <?= $form->field($modelPenerimaanlogFg, "[{$i}]koitem_fg")->textInput(['readOnly'=>true]) ?>
                                            </div>
                                          </div>

                                          <div id="dua">
                                            <div class="col-md-4">
                                              <?= $form->field($modelPenerimaanlogFg, "[{$i}]nama_fg")->textInput(['readOnly'=>true]) ?>
                                              <!-- /input-group -->
                                            </div>
                                          </div>

                                        </div>

                                        <div class="row">


                                          <div id="dua">
                                            <div class="col-md-4">
                                              <?= $form->field($modelPenerimaanlogFg, "[{$i}]no_surjal")->textInput(['readOnly'=>true]) ?>
                                              <!-- /input-group -->
                                            </div>
                                          </div>



                                          <div id="dua">
                                            <div class="col-md-4">
                                               <?= $form->field($modelPenerimaanlogFg, "[{$i}]supplier")->textInput(['readOnly'=>true]) ?>
                                              <!-- /input-group -->
                                            </div>
                                          </div>



                                          <div id="dua">
                                            <div class="col-md-4">
                                              <?= $form->field($modelPenerimaanlogFg, "[{$i}]batch")->textInput(['readOnly'=>true]) ?>
                                              <!-- /input-group -->
                                            </div>
                                          </div>

                                        </div>


                                        <div class="row">

                                          <div class="col-md-4">
                                              <?php
                                                  echo $form->field($modelPenerimaanlogFg, "[{$i}]jumlah_koli")->textInput()
                                              ?>
                                          </div>
                                          <div class="col-md-4">
                                              <?= $form->field($modelPenerimaanlogFg, "[{$i}]qty_per_koli")->textInput() ?>
                                          </div>
                                          <div class="col-md-4">
                                              <?= $form->field($modelPenerimaanlogFg, "[{$i}]qty_koli_receh")->textInput() ?>
                                          </div>
                                      	</div>

                                     	<div class="row">

                                          <div class="col-md-4">
                                              <?=  $form->field($modelPenerimaanlogFg, "[{$i}]jumlah")->textInput() ?>
                                            <!-- /input-group -->
                                          </div>
                                          <div class="col-md-4">
                                              <?= $form->field($modelPenerimaanlogFg, "[{$i}]jumlah_koli_palet")->textInput() ?>
                                            <!-- /input-group -->
                                          </div>
                                          <div class="col-md-4">
                                              <?= $form->field($modelPenerimaanlogFg, "[{$i}]jumlah_palet")->textInput() ?>
                                            <!-- /input-group -->
                                          </div>
                                      </div>


                                      <div class="row">
                                          <div class="col-md-4">
                                              <?= $form->field($modelPenerimaanlogFg, "[{$i}]jumlah_pcs_palet")->textInput() ?>
                                            <!-- /input-group -->
                                          </div>
                                          <div id="sisa">
                                            <div class="col-md-4">
                                                <?= $form->field($modelPenerimaanlogFg, "[{$i}]sisa_pcs_palet")->textInput() ?>
                                              <!-- /input-group -->
                                            </div>
                                          </div>
                                          <div id="sisa">
                                            <div class="col-md-4">
                                                <?= $form->field($modelPenerimaanlogFg, "[{$i}]sisa_koli_palet")->textInput() ?>
                                              <!-- /input-group -->
                                            </div>
                                          </div>
                                      </div>

                                        <div class="row">
                                          <div class="col-md-6">
                                              <?= $form->field($modelPenerimaanlogFg, "[{$i}]koordinat_penempatan")->textInput() ?>
                                            <!-- /input-group -->
                                          </div>
                                          <div class="col-md-6">
                                            <?php


                                                  $sql23 ="

                                                  SELECT id,penempatan
                                                  FROM penempatan
                                                  ORDER BY id asc

                                                  ";

                                                  $kode1=Penempatan::findBySql($sql23)->all();

                                                  //use yii\helpers\ArrayHelper;
                                                  $listData1=ArrayHelper::map($kode1,'penempatan','penempatan');

                                                  echo $form->field($modelPenerimaanlogFg, "[{$i}]penempatan")->dropDownList(
                                                  $listData1,
                                                  ['prompt'=>'Select...']
                                                  );

                                              ?>
                                            </div>
                                          </div>

                              </div>
                          </div>
                      <?php endforeach; ?>
                      </div>
                  </div>
              </div><!-- .panel -->
              <?php DynamicFormWidget::end(); ?>

        <div class="form-group">
            <?= Html::submitButton($modelPenerimaanlogFg->isNewRecord ? 'CREATE' : 'Create', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
  </div>
</div>

<?php
$script = <<< JS

//$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
      	$('select[name^="PenerimaanlogFg["]').change(function() {

		        var prefix = $(this).attr('id').split("-");

		        prefix.splice(-1, 1);
		        var sup_id = '#' + prefix.join('-') + '-supplier';
		        var item_code_id = '#' + prefix.join('-') + '-koitem_fg';
		        var item_name_id = '#' + prefix.join('-') + '-nama_fg';
		        var surjal_id = '#' + prefix.join('-') + '-no_surjal';
		        var batch_id = '#' + prefix.join('-') + '-batch';
		        var nopo = $(this).val();

		          $.get('index.php?r=penerimaanlog-fg/get-po',{ nopo : nopo }, function(data){
		            var data = $.parseJSON(data);

		              // $(item_code_id).empty().append($('<option>', {
		              //     value:'',
		              //     text:'Select...'
		              // }));

		              $.each(data, function(idx, value) {
		                $(item_code_id).append($('<option>', {
		                  value: value['koitem_fg'],
		                  text: value['koitem_fg']
		                }));
		              });

		          });
	      	});

			    $('select[name^="PenerimaanlogFg["]').change(function() {
			      var prefix = $(this).attr('id').split("-");
			      prefix.splice(-1, 1);
			      var sup_id = '#' + prefix.join('-') + '-supplier';
			      var item_code_id = '#' + prefix.join('-') + '-koitem_fg';
			      var item_name_id = '#' + prefix.join('-') + '-nama_fg';
			      var surjal_id = '#' + prefix.join('-') + '-no_surjal';
			      var batch_id = '#' + prefix.join('-') + '-batch';
			      var nopo = $(this).val();
			      $(item_code_id).change(function(){

			        var koitem_fg = $(this).val();
			          $.get('index.php?r=penerimaanlog-fg/get-po',{ nopo: nopo, koitem_fg : koitem_fg }, function(data){
			            var data = $.parseJSON(data);

			            $.each(data, function(idx, value) {
			              $(sup_id).val(value['supplier']);
			              $(item_name_id).val(value['nama_fg']);
			              $(surjal_id).val(value['no_surjal']);
			              $(batch_id).val(value['batch']);
			            });
			          });
			      	});
		});


	$('input[name^="PenerimaanlogFg["]').keyup(function(){
		var prefix = $(this).attr('id').split("-");
		prefix.splice(-1, 1);
		var quantity_per_koli_id = '#' + prefix.join('-') + '-qty_per_koli';
		var quantity_koli_receh_id = '#' + prefix.join('-') + '-qty_koli_receh';
		var jumlah_koli_pallet_id = '#' + prefix.join('-') + '-jumlah_koli_palet';
		var jumlah_id = '#' + prefix.join('-') + '-jumlah';
		var jumlah_palet_id = '#' + prefix.join('-') + '-jumlah_palet';
		var jumlah_pcs_pallet_id = '#' + prefix.join('-') + '-jumlah_pcs_palet';
		var jumlah_koli_id = '#' + prefix.join('-') + '-jumlah_koli';
    var sisa_pcs_pallet_id = '#' + prefix.join('-') + '-sisa_pcs_palet';
    var sisa_koli_pallet_id = '#' + prefix.join('-') + '-sisa_koli_palet';

	    var a = parseFloat($(jumlah_koli_id).val())||0;
	    var b = parseFloat($(quantity_per_koli_id).val())||0;
	    var c = parseFloat($(quantity_koli_receh_id).val())||0;
	    var d = parseFloat($(jumlah_koli_pallet_id).val())||0;
      var e = parseFloat($(jumlah_id).val())||0;
      var f = parseFloat($(jumlah_palet_id).val())||0;
      var g = parseFloat($(jumlah_pcs_pallet_id).val())||0;
      // var x = parseFloat($(quantity_koli_receh_id).val())==0;
      // var y = parseFloat($(quantity_koli_receh_id).val())>0;

      if ($(quantity_koli_receh_id).val()== 0) {
        $(jumlah_id).val(a*b+c);
        $(jumlah_palet_id).val(Math.ceil(a/d));
        $(jumlah_pcs_pallet_id).val(b*d);

        var a = parseFloat($(jumlah_koli_id).val())||0;
        var e = parseFloat($(jumlah_id).val())||0;
        var f = parseFloat($(jumlah_palet_id).val())||0;
        var g = parseFloat($(jumlah_pcs_pallet_id).val())||0;

        $(sisa_pcs_pallet_id).val(e-((f-1)*g));
        $(sisa_koli_pallet_id).val((a-((f-1)*d)));
        $('[id^="sisa"]').show();
      }
      else if ($(quantity_koli_receh_id).val()!=0){
        $('[id^="sisa"]').show();

        $(jumlah_id).val((a-1)*b+c);
        $(jumlah_palet_id).val(Math.ceil(a/d));
        $(jumlah_pcs_pallet_id).val(b*d);

        var a = parseFloat($(jumlah_koli_id).val())||0;
        var e = parseFloat($(jumlah_id).val())||0;
        var f = parseFloat($(jumlah_palet_id).val())||0;
        var g = parseFloat($(jumlah_pcs_pallet_id).val())||0;

        $(sisa_pcs_pallet_id).val(e-((f-1)*g));
        $(sisa_koli_pallet_id).val((a-((f-1)*d))-1);

      }
      else {
        allert("Input Salah");
      }


	    //alert($(jumlah).val());
	    //alert(jumlah_koli);
	});


//});

// $('[id^="satu"]').hide();
//     $('[id^="dua"]').hide();



$(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
    console.log("beforeInsert");
});

$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    console.log("afterInsert");
});

$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
    if (! confirm("Are you sure you want to delete this item?")) {
        return false;
    }
    return true;
});

$(".dynamicform_wrapper").on("afterDelete", function(e) {
    console.log("Deleted item!");
});

$(".dynamicform_wrapper").on("limitReached", function(e, item) {
    alert("Limit reached");
});


jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title-adminlog").each(function(index) {
        jQuery(this).html("Adminlog: " + (index + 1))
    });
});

jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title-adminlog").each(function(index) {
        jQuery(this).html("Adminlog: " + (index + 1))
    });
});

JS;
$this->registerJs($script);
?>
