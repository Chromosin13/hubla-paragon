<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use Da\QrCode\QrCode;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApTableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>




  <div style="position:absolute;left:5px;top:0px;"> <img src="../web/images/paragon.jpeg" height="22%"> </div>
  <div style="position:absolute;left:60px;top:0px;font-size:23px"> <span> <b>PT PARAGON</b></span> </div>
  <div style="position:absolute;left:60px;top:23px;font-size:12px"> <span> <b>Technology & Innovation</b></span> </div>



          <div style="position:absolute;left:420px;top:5px;font-size:12px">
            <?php
              
                $qrCode = (new QrCode($model->no_po))
                    ->setSize(75)
                    ->setMargin(3)
                    ->useForegroundColor(0, 0, 0);
                $qrCode->writeFile('/var/www/html/packaging/web/qr/'. preg_replace('/[^A-Za-z0-9\-]/', '', $model->no_po).'.png');

            ?>

            <?php 

                echo '<img src="../web/qr/';
                echo preg_replace('/[^A-Za-z0-9\-]/', '', $model->no_po);
                echo '.png" >';              

            ?>
          </div>

          <div style="position:absolute;left:420px;top:80px;text-align: left;font-size: 13px;">
            <b><?php echo $model->no_po;?></b>
          </div>

          <div style="position:absolute;left:610px;top:0px;text-align: left;font-size: 20px;">
            <b>STATUS</b>
          </div>
          <div style="position:absolute;left:540px;top:20px;text-align: left;font-size: 10px;">
            <p>--------------------------------------------------------------------</p>
          </div>

          <div style="position:absolute;left:535px;top:25px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:535px;top:35px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:535px;top:45px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:535px;top:55px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:535px;top:65px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:535px;top:75px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:535px;top:85px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:535px;top:95px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:535px;top:105px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:535px;top:115px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:535px;top:125px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>

          <div style="position:absolute;left:757px;top:25px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:757px;top:35px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:757px;top:45px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:757px;top:55px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:757px;top:65px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:757px;top:75px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:757px;top:85px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:757px;top:95px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:757px;top:105px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:757px;top:115px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:757px;top:125px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>        
          
          <div style="position:absolute;left:540px;top:130px;text-align: left;font-size: 10px;">
            <p>--------------------------------------------------------------------</p>
          </div>
          <div style="position:absolute;left:560px;top:65px;text-align: left;font-size: 30px;">
            <b>KARANTINA</b>
          </div>
          <div style="position:absolute;left:10px;top:50px;text-align: left;font-size: 20px;">
            <b>JUMLAH KOLI</b>
          </div>
          <div style="position:absolute;left:10px;top:80px;text-align: left;font-size: 20px;">
            <b>KOLI/PALLET</b>
          </div>
          <div style="position:absolute;left:10px;top:110px;text-align: left;font-size: 20px;">
            <b>TEMPAT</b>
          </div>
          <div style="position:absolute;left:10px;top:140px;text-align: left;font-size: 20px;">
            <b>SUPPLIER</b>
          </div>
          <div style="position:absolute;left:10px;top:170px;text-align: left;font-size: 20px;">
            <b>KODE ITEM</b>
          </div>
          <div style="position:absolute;left:10px;top:270px;text-align: left;font-size: 40px;">
            <b>ITEM</b>
          </div>
          <div style="position:absolute;left:10px;top:425px;text-align: left;font-size: 30px;">
            <b>BATCH</b>
          </div>
          <div style="position:absolute;left:10px;top:485px;text-align: left;font-size: 30px;">
            <b>JUMLAH</b>
          </div>
          <!-- <div style="position:absolute;left:590px;top:470px;text-align: left;font-size: 30px;">
            <b>PALLET</b>
          </div> -->
          <div style="position:absolute;left:660px;top:480px;text-align: left;font-size: 60px;">
            <b><?php echo $i?>/<?php echo $model->jumlah_palet;?></b>
          </div>
          <div style="position:absolute;left:650px;top:470px;text-align: left;font-size: 10px;">
            <p>--------------------------------------------</p>
          </div>
          <div style="position:absolute;left:648px;top:475px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:648px;top:485px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:648px;top:495px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:648px;top:505px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:648px;top:515px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:648px;top:525px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:648px;top:535px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:648px;top:545px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
          <div style="position:absolute;left:648px;top:555px;text-align: left;font-size: 10px;">
            <p>|</p>
          </div>
                 


          <div style="position:absolute;left:150px;top:50px;text-align: left;font-size: 20px;">
            <b>:</b>
          </div>
          <div style="position:absolute;left:150px;top:80px;text-align: left;font-size: 20px;">
            <b>:</b>
          </div>
          <div style="position:absolute;left:150px;top:110px;text-align: left;font-size: 20px;">
            <b>:</b>
          </div>
          <div style="position:absolute;left:150px;top:140px;text-align: left;font-size: 20px;">
            <b>:</b>
          </div>
          <div style="position:absolute;left:150px;top:170px;text-align: left;font-size: 20px;">
            <b>:</b>
          </div>
          <div style="position:absolute;left:150px;top:270px;text-align: left;font-size: 30px;">
            <b>:</b>
          </div>
          <div style="position:absolute;left:150px;top:425px;text-align: left;font-size: 30px;">
            <b>:</b>
          </div>
          <div style="position:absolute;left:150px;top:485px;text-align: left;font-size: 30px;">
            <b>:</b>
          </div>

          <div style="position:absolute;left:280px;top:0px;text-align: left;font-size: 40px;">
            <b><?php echo $model->koordinat_penempatan;?></b>
          </div>
          <div style="position:absolute;left:170px;top:50px;text-align: left;font-size: 20px;">
            <b><?php echo $model->jumlah_koli;?> Q.</b>
          </div>
          <div style="position:absolute;left:170px;top:80px;text-align: left;font-size: 20px;">
            <b><?php echo $model->jumlah_koli_pallet;?> Q @ <?php echo $model->quantity_per_koli;?> pcs</b>
          </div>
          <div style="position:absolute;left:170px;top:110px;text-align: left;font-size: 20px;">
            <b><?php echo $model->penempatan;?></b>
          </div>
          <div style="position:absolute;left:170px;top:140px;text-align: left;font-size: 20px;">
            <b><?php echo $model->supplier;?></b>
          </div>
          <div style="position:absolute;left:170px;top:170px;text-align: left;font-size: 20px;">
            <b><?php echo $model->kode_item;?></b>
          </div>
          <div style="position:absolute;left:170px;top:210px;text-align: left;font-size: 65px; line-height: 60px">
            <b><?php echo $model->nama_item;?></b>  
          </div>
          <div style="position:absolute;left:170px;top:400px;text-align: left;font-size: 65px;">
            <b><?php echo $model->no_batch;?></b>
          </div>
          <div style="position:absolute;left:170px;top:460px;text-align: left;font-size: 63px;">
            <b><?php echo $model->jumlah_pcs_pallet;?>/<?php echo $model->jumlah;?></b>
          </div>
          <div style="position:absolute;left:670px;top:440px;text-align: left;font-size: 30px;">
            <b><?php echo $array_namaoperator->namaoperator;?>.</b>
          </div>
          <!-- <div style="position:absolute;left:720px;top:470px;text-align: left;font-size: 30px;">
            <b><?php echo $i?>/</b>
          </div>
          <div style="position:absolute;left:720px;top:510px;text-align: left;font-size: 30px;">
            <b><?php echo $model->jumlah_palet;?></b>
          </div> -->
             


