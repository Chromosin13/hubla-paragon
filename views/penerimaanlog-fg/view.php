<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PenerimaanlogFg */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Penerimaanlog Fgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penerimaanlog-fg-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'jumlah_koli',
            'qty_per_koli',
            'qty_koli_receh',
            'jumlah',
            'jumlah_koli_palet',
            'jumlah_palet',
            'datetime_start',
            'datetime_stop',
            'adminlog_fg_id',
            'jumlah_pcs_palet',
            'koordinat_penempatan',
            'operator_id',
            'penempatan',
            'sisa_koli_palet',
            'sisa_pcs_palet',
            'status_print',
            'flag_sampler',
            'nopo',
            'no_surjal',
            'koitem_fg',
            'nama_fg',
            'supplier',
            'batch',
        ],
    ]) ?>

</div>
