<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\PenerimaanlogFg;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- CSS For Video Selector -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="ZXing for JS">

  <title>ZXing TypeScript | Decoding from camera stream</title>

  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">
</head>

<!-- Body -->
<div class="box box-widget widget-user">
  <!-- Add the bg color to the header using any of the bg-* classes -->
  <div class="widget-user-header bg-yellow-gradient">
    <h3 class="widget-user-username"><b>Operator Penerimaan Logistik</b></h3>
    <h5 class="widget-user-desc">Scan QR Code</h5>
  </div>
  <div class="widget-user-image">
    <img class="img-circle" src="../web/images/gambarlog/file.png" alt="User Avatar">
  </div>
  <div class="box-footer">
    <div class="row">
      <div class="box-body">
          <div class="box-body">

            <div id="sourceSelectPanel" style="display:none">
              <label for="sourceSelect">Switch Camera</label>
              <select id="sourceSelect" style="max-width:400px">x
              </select>
            </div>

            <div>
              <video id="video" width="330" height="300" style="border: 0.5px solid gray"></video>
            </div>

          </div>
      </div>
    </div>
    <!-- /.row -->
  </div>
</div>

<!-- Javascript -->
  <!-- Calling ZXing API CDN -->
  <script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script>
  <!-- Code -->
  <script type="text/javascript">

      let selectedDeviceId;
      const codeReader = new ZXing.BrowserMultiFormatReader()
      console.log('ZXing code reader initialized')
      codeReader.getVideoInputDevices()
        .then((videoInputDevices) => {
          const sourceSelect = document.getElementById('sourceSelect')
          selectedDeviceId = videoInputDevices[1].deviceId
          if (videoInputDevices.length >= 1) {
            videoInputDevices.forEach((element) => {
              const sourceOption = document.createElement('option')
              sourceOption.text = element.label
              sourceOption.value = element.deviceId
              sourceSelect.appendChild(sourceOption)
            })

            // When Changing Camera, Reset Scanner and Execute Canvas
            sourceSelect.onchange = () => {
              selectedDeviceId = sourceSelect.value;

              codeReader.reset()
              codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {

                if (result) {

                  // console.log(result.text);
                  $.get('index.php?r=adminlog-fg/check-nopo',{ nopo : result.text },function(data){
                        var data = $.parseJSON(data);

                        if(data.id>=1){

                           location.href='https://factory.pti-cosmetics.com/flowreport/web/index.php?r=penerimaanlog-fg/update-item&nopo='+result.text;

                        }
                         else{

                           alert('Nomor PO Tidak Ditemukan di Database');
                        }

                  });
                }
                if (err && !(err instanceof ZXing.NotFoundException)) {
                  console.error(err)
                  document.getElementById('result').textContent = err
                }

              }) // on scanned
            }; // onchange

            const sourceSelectPanel = document.getElementById('sourceSelectPanel')
            sourceSelectPanel.style.display = 'block'
          }

          // Initialize Execute Canvas when Page first loaded
          codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
             if (result) {

                  // console.log(result.text);
                  $.get('index.php?r=adminlog-fg/check-nopo',{ nopo : result.text },function(data){
                        var data = $.parseJSON(data);

                        if(data.id>=1){

                           location.href='https://factory.pti-cosmetics.com/flowreport/web/index.php?r=penerimaanlog-fg/update-item&nopo='+result;

                        }
                         else{

                           alert('Nomor PO Tidak Ditemukan di Database');
                        }

                  });
                }
            if (err && !(err instanceof ZXing.NotFoundException)) {
              console.error(err)
              document.getElementById('result').textContent = err
            }
          }) // on scanned


        })
        .catch((err) => {
          console.error(err)
        })
  </script>
