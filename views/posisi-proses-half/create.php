<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PosisiProsesHalf */

$this->title = 'Create Posisi Proses Half';
$this->params['breadcrumbs'][] = ['label' => 'Posisi Proses Halves', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posisi-proses-half-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
