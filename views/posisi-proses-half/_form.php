<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PosisiProsesHalf */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="posisi-proses-half-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomo')->textInput() ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'snfg_komponen')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'posisi')->textInput() ?>

    <?= $form->field($model, 'start')->textInput() ?>

    <?= $form->field($model, 'due')->textInput() ?>

    <?= $form->field($model, 'status')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'timestamp')->textInput() ?>

    <?= $form->field($model, 'ontime')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'lanjutan_split_batch')->textInput() ?>

    <?= $form->field($model, 'delta')->textInput() ?>

    <?= $form->field($model, 'time')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
