<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PosisiProsesHalfSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="posisi-proses-half-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'nomo') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'snfg_komponen') ?>

    <?= $form->field($model, 'nama_fg') ?>

    <?= $form->field($model, 'posisi') ?>

    <?php // echo $form->field($model, 'start') ?>

    <?php // echo $form->field($model, 'due') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'timestamp') ?>

    <?php // echo $form->field($model, 'ontime') ?>

    <?php // echo $form->field($model, 'lanjutan_split_batch') ?>

    <?php // echo $form->field($model, 'delta') ?>

    <?php // echo $form->field($model, 'time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
