<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PosisiProsesHalf */

$this->title = 'Update Posisi Proses Half: ' . $model->snfg_komponen;
$this->params['breadcrumbs'][] = ['label' => 'Posisi Proses Halves', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->snfg_komponen, 'url' => ['view', 'id' => $model->snfg_komponen]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="posisi-proses-half-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
