<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\LogPenimbanganRm;
use app\models\Downtime;
use app\models\DowntimeSearch;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<script>
   
</script>

 <div class="box box-widget widget-user">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-blue-gradient">
        <h3 class="widget-user-username"><b>Supply Raw Material - GBB</b></h3>
        <h5 class="widget-user-desc">Work Order</h5>
    </div>
    <div class="widget-user-image">
        <img class="img-circle" src="../web/images/trolley.png" alt="User Avatar">
    </div>
    <div class="box-footer">
        <div class="row">
            <div class="box-body">
                <div class="box-body">
                    <div class="flow-input-mo-form">
                        <?php $form = ActiveForm::begin(); ?>
                        <div class="row">
                            <div class="col-md-6">
                              <?= $form->field($model, 'nomo')->textInput(['readOnly'=>true,'value'=>$nomo]) ?>
                              <!-- /input-group -->
                            </div>
                            <div id="lanjutan">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'lanjutan')->textInput(['readOnly'=>true]) ?> 
                                    <!-- /input-group -->
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-md-12">
                                <label class="control-label">Nama Operator</label>
                                <?= $form->field($model, 'nama_operator')->widget(TagsinputWidget::classname(), [
                                                'options' => ['id'=>'subcat-id','onkeyup'=>'this.value = this.value.toUpperCase();'],
                                                'clientOptions' => [
                                                    'trimValue' => true,
                                                    'allowDuplicates' => false,
                                                    'maxChars'=> 4,
                                                    'minChars'=> 4,
                                                ]
                                            ])->label(false)?>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-md-6">
                              <?php 
                                    $data = ArrayHelper::map(LogPenimbanganRm::find()->where("nomo='".$nomo."' and status = 'Belum Ditimbang' ")->all()
                                                                                            ,'siklus','siklus');
                                    $data[0]= 'ALL';
                                    // $model->siklus = 3; 
                                    echo $form->field($model, 'siklus')->widget(Select2::classname(), [
                                            'data' => $data,
                                            'options' => ['placeholder' => 'Pilih Siklus'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);

                               ?>
                            </div>
                            <div class="col-md-6">
                                <!-- <label class="control-label">Nama Operator</label> -->
                                <?= $form->field($model, 'nama_operator')->textInput(['maxlength' => true, 'style' => 'text-transform:uppercase','id'=>'input_operator']) ?>

                            </div>
                        </div>

                        <br></br>
                        <?php 
                            echo Html::submitButton($model->isNewRecord ? 'START' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary']);
                        ?>
                                            


                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
</div>


<!-- <div class="box box-widget widget-user">
    <div class="widget-user-header bg-blue">
        <h3 class="widget-user-username"><b>Rincian</b></h3>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'lanjutan',
            'nomo',
            'datetime_start',
            'datetime_stop',
            'nama_operator',
            'id',
        ],
    ]); ?>

</div>  -->
                  


<?php
$script = <<< JS
    var nomo = "$nomo";
    document.getElementById('input_operator').addEventListener('input', function (e) {
      //e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1,').trim();
        e.target.value = e.target.value.toUpperCase();
        if (e.target.value.length % 5 == 0){
            //e.target.value = e.target.value.replace(/(.{4})/g, '$1').replace(/[ ,]+/g, ",");
            e.target.value = e.target.value.replace(/.$/,",");
        }
        
    }); 

    // function forceInputUppercase(e)
    //   {
    //     var start = e.target.selectionStart;
    //     var end = e.target.selectionEnd;
    //     e.target.value = e.target.value.toUpperCase();
    //     e.target.setSelectionRange(start, end);
    //   }

    // document.getElementById("subcat-id").addEventListener("keyup", forceInputUppercase, false);

    $.get("index.php?r=flow-input-gbb/get-lanjutan",{nomo:nomo},function(data){
        if (data.status == 'success'){
            $('#flowinputgbb-lanjutan').attr('value',data.value);
        }
    });



JS;
$this->registerJs($script);
?>