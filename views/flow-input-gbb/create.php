<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputGbb */

$this->title = 'Create Flow Input Gbb';
$this->params['breadcrumbs'][] = ['label' => 'Flow Input Gbbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flow-input-gbb-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
