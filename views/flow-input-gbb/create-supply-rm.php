<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */

?>
<div class="flow-input-gbb-create">



    <?php 	
		echo $this->render('_form-supply-rm', [
		        'model' => $model,
		        'nomo' => $nomo,
		        'last_datetime_start' => $last_datetime_start,
                'last_datetime_stop' => $last_datetime_stop,
                'last_id' => $last_id,
			    'searchModel' => $searchModel,
			    'dataProvider' => $dataProvider,
		    ]);

     ?>

</div>
