<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputGbb;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputGbb */
/* @var $form yii\widgets\ActiveForm */
?>


                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-blue-gradient">
                      <h3 class="widget-user-username"><b>GBB Transit</b></h3>
                      <h5 class="widget-user-desc">Input Data</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/trolley.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">
                              <div class="zoom">
                                <?=Html::a('SCAN WITH QR', Yii::getAlias('@dnsUrl').'flow-input-gbb/check-flow-gbb-qr', ['class' => 'btn btn-success btn-block']);?>
                              </div>
                              <p></p>
                              <div class="flow-input-gbb-form">
                                  <div class="form-group field-flowinputgbb-nomo has-success">
                                  <label class="control-label" for="flowinputgbb-nomo">Nomor MO</label>
                                  <input type="text" id="flowinputgbb-nomo" class="form-control" name="FlowInputGbb[nomo]" aria-invalid="false">

                              </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>

                  <!-- List Data -->
                  <h3 style="color:white;background-color:#76D7C4; font-size: 18px;text-align: center; padding: 7px 10px; margin-top: 0;" class="widget-user-username"><b>IN PROGRESS</b></h3>
                  <div class="box-body">
                               



                                     <?= GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'filterModel' => $searchModel,
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn'],
                                            'nomo',
                                            'lanjutan',
                                            'datetime_start',
                                            'datetime_stop',
                                            'nama_line',
                                            'nama_operator',
                                        ],
                                    ]); ?>

                          </div>

<?php
$script = <<< JS

// AutoFocus Nomo Field

document.getElementById("flowinputgbb-nomo").focus();


$('#flowinputgbb-nomo').change(function(){  

    var nomo = $('#flowinputgbb-nomo').val().trim();

    //console.log(nomo);
    
    if(nomo){

      $.getJSON('index.php?r=scm-planner/check-nomo',{ nomo : nomo },function(data){
        var data = data;
        //console.log(data.id);

        if(data.id>=1){
            //window.alert(result.text);           
              // window.location = "index.php?r=flow-input-gbb/create-penimbangan-rm&nomo="+nomo;

          $.getJSON('index.php?r=flow-input-gbb/check-page',{ nomo : nomo },function(tmp2){

            var data2 = tmp2;
            console.log(data2);
            
            if(data2.page=="no-task"){
              window.alert("Formula belum di verifikasi oleh tim Process Engineer (PE).");
            } else if(data2.page=="create"){
              console.log('create');
              window.location = "index.php?r=flow-input-gbb/create-supply-rm&nomo="+nomo;
            } else {
              window.location = "index.php?r=flow-input-gbb/task&nomo="+nomo;
            }
          });
        } else {
           alert('No. MO Tidak Ditemukan di Database; Silahkan mengontak Planner/Leader');
        }

      });
      //window.location = "index.php?r=flow-input-gbb/create-penimbangan-rm&nomo="+nomo;
      //alert('LOH!');
    }else{
      alert('Mohon isi No.MO terlebih dahulu!');
    }
    
});


JS;
$this->registerJs($script);
?>