<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LogPrintQueueChecker */

$this->title = 'Create Log Print Queue Checker';
$this->params['breadcrumbs'][] = ['label' => 'Log Print Queue Checkers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-print-queue-checker-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
