<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogPrintQueueChecker */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-print-queue-checker-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'timestamp')->textInput() ?>

    <?= $form->field($model, 'nomo')->textInput() ?>

    <?= $form->field($model, 'operator')->textInput() ?>

    <?= $form->field($model, 'is_print')->textInput() ?>

    <?= $form->field($model, 'siklus')->textInput() ?>

    <?= $form->field($model, 'timestamp_checked')->textInput() ?>

    <?= $form->field($model, 'nama_bulk')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
