<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LogPrintQueueChecker */

$this->title = 'Update Log Print Queue Checker: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Log Print Queue Checkers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="log-print-queue-checker-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
