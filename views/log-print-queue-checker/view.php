<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LogPrintQueueChecker */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Log Print Queue Checkers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-print-queue-checker-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'timestamp',
            'nomo',
            'operator',
            'is_print',
            'siklus',
            'timestamp_checked',
            'nama_bulk',
        ],
    ]) ?>

</div>
