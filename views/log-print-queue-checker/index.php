<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\LogPrintQueueCheckerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Print Queue Checkers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-print-queue-checker-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Log Print Queue Checker', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'timestamp',
            'nomo',
            'operator',
            'is_print',
            // 'siklus',
            // 'timestamp_checked',
            // 'nama_bulk',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
