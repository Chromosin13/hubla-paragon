<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogPrintQueueCheckerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-print-queue-checker-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'timestamp') ?>

    <?= $form->field($model, 'nomo') ?>

    <?= $form->field($model, 'operator') ?>

    <?= $form->field($model, 'is_print') ?>

    <?php // echo $form->field($model, 'siklus') ?>

    <?php // echo $form->field($model, 'timestamp_checked') ?>

    <?php // echo $form->field($model, 'nama_bulk') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
