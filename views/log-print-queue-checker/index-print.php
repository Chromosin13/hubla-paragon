<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\AnalisQc;
use app\models\StatusAnalis;
use app\models\ApprovSuperior;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\export\ExportMenu;
use yii\widgets\Pjax; 
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FormKendaliTugasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<style>
    .bootstrap-tagsinput {
        /*padding: 50px;*/
        width: 1920px;
        height: auto;
    }

    .zoom {
        /*padding: 50px;*/
        transition: transform .3s; /* Animation */
        width: auto;
        height: auto;
        margin: 0 auto;
    }

    .zoom:hover {
        transform: scale(0.99); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    }

    @import url(https://fonts.googleapis.com/css?family=Lato:900);
    *, *:before, *:after{
      box-sizing:border-box;
    }
    body{
      font-family: 'Lato', sans-serif;
        ;
    }
    div.foo{
      width: 90%;
      margin: 0 auto;
      text-align: center;
    }
    .letter{
      display: inline-block;
      font-weight: 900;
      font-size: 4em;
      margin: 0.2em;
      position: relative;
      color: #00B4F1;
      transform-style: preserve-3d;
      perspective: 400;
      z-index: 1;
    }
    .letter:before, .letter:after{
      position:absolute;
      content: attr(data-letter);
      transform-origin: top left;
      top:0;
      left:0;
    }
    .letter, .letter:before, .letter:after{
      transition: all 0.3s ease-in-out;
    }
    .letter:before{
      color: #fff;
      text-shadow: 
        -1px 0px 1px rgba(255,255,255,.8),
        1px 0px 1px rgba(0,0,0,.8);
      z-index: 3;
      transform:
        rotateX(0deg)
        rotateY(-15deg)
        rotateZ(0deg);
    }
    .letter:after{
      color: rgba(0,0,0,.11);
      z-index:2;
      transform:
        scale(1.08,1)
        rotateX(0deg)
        rotateY(0deg)
        rotateZ(0deg)
        skew(0deg,1deg);
    }
    .letter:hover:before{
      color: #fafafa;
      transform:
        rotateX(0deg)
        rotateY(-40deg)
        rotateZ(0deg);
    }
    .letter:hover:after{
      transform:
        scale(1.08,1)
        rotateX(0deg)
        rotateY(40deg)
        rotateZ(0deg)
        skew(0deg,22deg);
</style>

<div class="box box-widget widget-user">
    <div class="widget-user-header bg-yellow-gradient">
      <h3 class="widget-user-username"><b>PRINT LABEL</b></h3>
      
    </div>
    <div class="widget-user-image">
      <img class="img-circle" src="../web/images/gambarlog/leader_2.png" alt="User Avatar">
    </div>
    <br></br>


    <div class="box-body">

      <p></p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            'columns' => [
                // ['class' => 'kartik\grid\SerialColumn'],
                // [
                //     'class' => 'kartik\grid\ActionColumn',
                //     'template' => '{print-label}',
                //     'contentOptions' => ['style' => 'max-width:40px;'],
                //     'buttons' => [
                //         'print-label' => function ($url,$model) {
                //             return Html::a(
                //                 '<img class="img-circle" src="../web/images/printer-icon.png" alt="Terima" width="42" height="42">',
                //                 $url, 
                //                 [
                //                     'title' => 'Print Label?',
                //                 ]
                //             );
                //         }
                //     ],
                // ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'template' => '{print-zebra}',
                    'contentOptions' => ['style' => 'max-width:50px;'],
                    'buttons' => [
                        'print-zebra' => function ($url,$model,$id) {
                          if ($model->is_print == 'done')
                          {
                            return Html::a(
                                '<i class="glyphicon glyphicon glyphicon-ok"></i>',
                                $url,
                                ['class' => 'btn btn-success modalButton', 'title'=>'view/edit', ], 
                                [
                                    'title' => 'Print Label?',
                                ]
                            );
                          }
                          else
                          {
                            return Html::a(
                                '<img class="img-circle" src="../web/images/printer-icon.png" alt="Terima" width="42" height="42">',
                                $url, 
                                // ['class' => 'btn btn-warning modalButton', 'title'=>'view/edit', ],
                                [
                                    'title' => 'Print Label',
                                    'data-pjax' => '0',
                                    'data-confirm' => Yii::t('app', 'Print Label?'),
                                ]
                            );
                          }
                        },
                    ],
                ],
            'id',
            'nomo',
            'operator',
            'is_print',
            'siklus',
            'timestamp_checked',
            'nama_bulk',
                
            ],
        ]); ?>
    </div>

</div>


