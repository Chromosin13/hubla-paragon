<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterDataNetto */

$this->title = 'Create Master Data Netto';
$this->params['breadcrumbs'][] = ['label' => 'Master Data Nettos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-data-netto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
