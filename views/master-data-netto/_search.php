<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MasterDataNettoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-data-netto-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'koitem') ?>

    <?= $form->field($model, 'nama_item') ?>

    <?= $form->field($model, 'isi_koli') ?>

    <?= $form->field($model, 'netto_min') ?>

    <?php // echo $form->field($model, 'netto_max') ?>

    <?php // echo $form->field($model, 'timestamp') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
