<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\models\MasterDataNetto;

/* @var $this yii\web\View */
/* @var $model app\models\MasterDataNetto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-data-netto-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'koitem')->textInput() ?> -->
    <?php echo $form->field($model, 'koitem')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(MasterDataNetto::find()->distinct('koitem')->all()
            ,'koitem','koitem'),
            'options' => ['placeholder' => 'Select Koitem'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>

    <?= $form->field($model, 'nama_item')->textInput() ?>

    <!-- <?= $form->field($model, 'isi_koli')->textInput() ?> -->

    <?= $form->field($model, 'netto_min')->textInput() ?>
    <?= $form->field($model, 'pengali_netto')->textInput() ?>

    <!-- <?= $form->field($model, 'netto_max')->textInput() ?> -->

    <!-- <?= $form->field($model, 'timestamp')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<<JS

$('#masterdatanetto-koitem').change(function(){
    var koitem = $('#masterdatanetto-koitem').val();
    $.get('index.php?r=master-data-netto/get-netto-before',{koitem:koitem},function(data){
        var data = $.parseJSON(data);
        $('#masterdatanetto-nama_item').attr('value', data.nama_item);
        $('#masterdatanetto-isi_koli').attr('value', data.isi_koli);
        $('#masterdatanetto-netto_min').attr('value', data.netto_min);
        $('#masterdatanetto-netto_max').attr('value', data.netto_max);
    });   
});
JS;
$this->registerJs($script)
?>