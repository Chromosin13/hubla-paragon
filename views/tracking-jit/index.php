<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use app\models\TrackingSo;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrackingSoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tracking Just In Time';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- <div class="callout callout-success">
  <h4>Scope Data</h4>

  <p> Refresh Setiap 3 Menit </p>
  <p> 7 Hari (1 Week) Terakhir dari Tanggal Start QCFG</p>

</div> -->


<div class="tracking-so-index">

        <!-- Download All Data Here -->
    <?php

    //     $gridColumns = [
    //     ['class' => 'kartik\grid\SerialColumn'],
    //           'id',
    //           'nama_fg',
    //           'snfg',
    //           'no_palet',
    //           'qty',
    //           'status',
    //           'last_updated',
    //           'timestamp_check',
    //           'timestamp_terima_karantina',
    //           'timestamp_siap_kirim',
    //     ['class' => 'kartik\grid\ActionColumn'],
    //     ];
    //
    //     echo ExportMenu::widget([
    //     'dataProvider' => $dataProvider,
    //     'columns' => $gridColumns,
    //
    // ]);?>
<p></p>

    <?php Pjax::begin(['id' => 'jit']); ?>
    <?php
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'responsiveWrap' => false,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'danger', 'heading'=>'Tracking Just In Time'],
            'formatter' => [
                'class' => 'yii\i18n\Formatter',
                'timeZone' => 'Asia/Jakarta'
            ],
            'toolbar' => [
            ],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                [
                    'attribute' => 'koitem_fg',
                    'width'=>'70px',
                    'label'=>'Image',
                    'format' => 'html',
                    'value' => function ($data) {
                        return Html::img(Yii::getAlias('@web').'/images/products/'. $data['koitem_fg'].'.jpg',
                            ['width' => '50px']);
                    },
                ],
                [
                    'attribute'=>'nama_fg',
                    'label' => 'Finished Good',
                    'width'=>'200px',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(TrackingSo::find()->orderBy('nama_fg')->asArray()->all(), 'nama_fg', 'nama_fg'),
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'SKU'],
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 400px;font-size: 14px'],
                    // 'group'=>true,  // enable grouping
                    //'subGroupOf'=>1,
                ],
                [
                    'attribute' => 'snfg',
                    'label' => 'SNFG',
                    'width'=>'150px',
                    'format' => 'raw',
                    'contentOptions' => ['style' => ' text-align: center;font-weight:bold;width: 250px;font-size: 14px','class'=>'info'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                ],
                [
                    'attribute' => 'no_palet',
                    'width'=>'50px',
                    //'format' => 'raw',
                    'contentOptions' => ['style' => ' text-align: center;font-size: 14px'],
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'qty',
                    //'format' => 'raw',
                    'contentOptions' => ['style' => ' text-align: center;font-size: 14px'],
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'alokasi',
                    //'format' => 'raw',
                    'contentOptions' => ['style' => ' text-align: center;font-size: 14px'],
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'last_updated',
                    //'format' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                ],
                [
                    'attribute' => 'status',
                    //'value' => 'raw',
                    'headerOptions' => ['style' => 'text-align: center;width: 80px'],
                    'contentOptions' => function($model){
                             if($model->status == 'DITERIMA KARANTINA')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: #c65353;font-size: 14px;','class'=>'danger'];
                                                }
                            else if($model->status == 'PROSES KARANTINA')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: #df8d07;font-size: 14px;','class'=>'warning'];
                                                }
                            else if($model->status == 'SIAP KIRIM RDC')
                                                {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: #315a39;font-size: 14px;','class'=>'success'];
                                                }
                            else {
                                                    return ['style' => ' text-align: center;font-weight:bold;width: 150px;color: white;font-size: 14px','class'=>'default'];
                            }
                    },
                ],
             ],
        ]);
    ?>
    <?php Pjax::end(); ?>

    <?php
        $this->registerJs('
            setInterval(function(){
                 $.pjax.reload({container:"#jit"});
            }, 60000);', \yii\web\VIEW::POS_HEAD);
    ?>
</div>
