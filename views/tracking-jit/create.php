<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TrackingJit */

$this->title = 'Create Tracking Jit';
$this->params['breadcrumbs'][] = ['label' => 'Tracking Jits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tracking-jit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
