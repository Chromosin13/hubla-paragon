<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrackingJitSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tracking-jit-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nama_fg') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'no_palet') ?>

    <?= $form->field($model, 'qty') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'last_updated') ?>

    <?php // echo $form->field($model, 'timestamp_check') ?>

    <?php // echo $form->field($model, 'timestamp_terima_karantina') ?>

    <?php // echo $form->field($model, 'timestamp_siap_kirim') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
