<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model app\models\Kehadiran */
/* @var $form yii\widgets\ActiveForm */
?>

<?php

$this->registerJsFile('js/instascan.min.js', ['position' => \yii\web\View::POS_HEAD, 'type' => 'text/javascript']);

?>

<?php

echo '<div class="col-sm-12">';
    echo  '<video id="preview"></video>';
    echo '<script type="text/javascript">';
    echo "   let scanner = new Instascan.Scanner({ video: document.getElementById('preview'),
        mirror: false // prevents the video to be mirrored
        });
        scanner.addListener('scan', function (content) {
          // alert(content);
          var input = content;

          $.get('index.php?r=kehadiran/get-nama',{ unik : input },function(data){

          	confirm('Confirm Terima '+data+' ?');

          	window.location = 'index.php?r=kehadiran/update-hadir&unik='+input;

          });

          
          
          
        });
        Instascan.Camera.getCameras().then(function (cameras) {
          if (cameras.length > 0) {
            // scanner.start(cameras[0]);
            if(cameras[1]){ scanner.start(cameras[1]); } else { scanner.start(cameras[0]); }
          } else {
            console.error('No cameras found.');
          }
        }).catch(function (e) {
          console.error(e);
        });
      </script>
    </div>";

?>
