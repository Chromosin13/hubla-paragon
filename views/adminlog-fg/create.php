<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Adminlog */

$this->title = 'Create Adminlog';
$this->params['breadcrumbs'][] = ['label' => 'Adminlogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adminlog-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
