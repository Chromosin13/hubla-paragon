<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Adminlog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adminlog-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nopo')->textInput() ?>

    <?= $form->field($model, 'no_surjal')->textInput() ?>

    <?= $form->field($model, 'koitem_pack')->textInput() ?>

    <?= $form->field($model, 'nama_pack')->textInput() ?>

    <?= $form->field($model, 'supplier')->textInput() ?>

    <?= $form->field($model, 'datetime_start')->textInput() ?>

    <?= $form->field($model, 'datetime_stop')->textInput() ?>

    <?= $form->field($model, 'datetime_write')->textInput() ?>

    <?= $form->field($model, 'batch')->textInput() ?>

    <?= $form->field($model, 'admin_id')->textInput() ?>

    <?= $form->field($model, 'status_print')->textInput() ?>

    <?= $form->field($model, 'tgl_plan_kirim')->textInput() ?>

    <?= $form->field($model, 'qty_demand')->textInput() ?>

    <?= $form->field($model, 'price_unit')->textInput() ?>

    <?= $form->field($model, 'qty_kirim')->textInput() ?>

    <?= $form->field($model, 'tgl_po')->textInput() ?>

    <?= $form->field($model, 'tgl_kirim')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
