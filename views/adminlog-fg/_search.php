<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdminlogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adminlog-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nopo') ?>

    <?= $form->field($model, 'no_surjal') ?>

    <?= $form->field($model, 'koitem_pack') ?>

    <?= $form->field($model, 'nama_pack') ?>

    <?php // echo $form->field($model, 'supplier') ?>

    <?php // echo $form->field($model, 'datetime_start') ?>

    <?php // echo $form->field($model, 'datetime_stop') ?>

    <?php // echo $form->field($model, 'datetime_write') ?>

    <?php // echo $form->field($model, 'batch') ?>

    <?php // echo $form->field($model, 'admin_id') ?>

    <?php // echo $form->field($model, 'status_print') ?>

    <?php // echo $form->field($model, 'tgl_plan_kirim') ?>

    <?php // echo $form->field($model, 'qty_demand') ?>

    <?php // echo $form->field($model, 'price_unit') ?>

    <?php // echo $form->field($model, 'qty_kirim') ?>

    <?php // echo $form->field($model, 'tgl_po') ?>

    <?php // echo $form->field($model, 'tgl_kirim') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
