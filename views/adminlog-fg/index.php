<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
use app\models\Packaging;
use app\models\DataGrn;
use app\models\Adminlog;
use app\models\UserAdmin;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AdminlogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Adminlogs';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .bootstrap-tagsinput {
        /*padding: 50px;*/
        width: 1920px;
        height: auto;
    }

    .zoom {
        /*padding: 50px;*/
        transition: transform .3s; /* Animation */
        width: auto;
        height: auto;
        margin: 0 auto;
    }

    .zoom:hover {
        transform: scale(0.99); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    }

    @import url(https://fonts.googleapis.com/css?family=Lato:900);
    *, *:before, *:after{
      box-sizing:border-box;
    }
    body{
      font-family: 'Lato', sans-serif;
        ;
    }
    div.foo{
      width: 90%;
      margin: 0 auto;
      text-align: center;
    }
    .letter{
      display: inline-block;
      font-weight: 900;
      font-size: 4em;
      margin: 0.2em;
      position: relative;
      color: #00B4F1;
      transform-style: preserve-3d;
      perspective: 400;
      z-index: 1;
    }
    .letter:before, .letter:after{
      position:absolute;
      content: attr(data-letter);
      transform-origin: top left;
      top:0;
      left:0;
    }
    .letter, .letter:before, .letter:after{
      transition: all 0.3s ease-in-out;
    }
    .letter:before{
      color: #fff;
      text-shadow:
        -1px 0px 1px rgba(255,255,255,.8),
        1px 0px 1px rgba(0,0,0,.8);
      z-index: 3;
      transform:
        rotateX(0deg)
        rotateY(-15deg)
        rotateZ(0deg);
    }
    .letter:after{
      color: rgba(0,0,0,.11);
      z-index:2;
      transform:
        scale(1.08,1)
        rotateX(0deg)
        rotateY(0deg)
        rotateZ(0deg)
        skew(0deg,1deg);
    }
    .letter:hover:before{
      color: #fafafa;
      transform:
        rotateX(0deg)
        rotateY(-40deg)
        rotateZ(0deg);
    }
    .letter:hover:after{
      transform:
        scale(1.08,1)
        rotateX(0deg)
        rotateY(40deg)
        rotateZ(0deg)
        skew(0deg,22deg);
</style>

<div class="box box-widget widget-user">
    <div class="widget-user-header bg-red-gradient">
      <h3 class="widget-user-username"><b>Form Kendali</b></h3>
      <h4 class="widget-user-desc">Admin Logistik</h4>
    </div>
    <div class="widget-user-image">
      <img class="img-circle" src="../web/images/gambarlog/question.png" alt="User Avatar">
    </div>
    <br></br>


    <div class="box-body">


        <?php $form = ActiveForm::begin(); ?>

          <?php
              $sql11 ="

              SELECT nomor_po
              FROM data_grn
              WHERE state = 'assigned'

              ";
              $kode2=DataGrn::findBySql($sql11)->all();

              //use yii\helpers\ArrayHelper;
              $listData2=ArrayHelper::map($kode2,'nomor_po','nomor_po');

              echo $form->field($model, 'nopo')->widget(Select2::classname(), [
                'data' => $listData2,
                'options' => ['placeholder' => 'Input Nomor PO',
                              // 'multiple' => true
                              ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
              ]);

          ?>
          <?php
              // echo $form->field($model, 'admin_id')->widget(Select2::classname(), [
              //   'data' => ArrayHelper::map(UserAdmin::find()->orderBy(['nama'=>SORT_ASC])->all(),'id','nama'),
              //   'options' => ['placeholder' => 'Masukkan Nama Anda',
              //                 // 'multiple' => true
              //                 ],
              //   'pluginOptions' => [
              //       'allowClear' => true
              //   ],
              //]);

          ?>

        <div class="zoom">
          <div class="form-group">
              <?= Html::submitButton($model->isNewRecord ? 'CREATE FORM' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary']) ?>
          </div>
        </div>

        <?php ActiveForm::end(); ?>



    <div class="adminlog-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'floatHeader'=>'true',
        'floatHeaderOptions' => ['position' => 'absolute',],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{print-label}',
                'contentOptions' => ['style' => 'max-width:50px;'],
                'buttons' => [
                    'print-label' => function ($url,$model,$id) {
                      if ($model->status_print == 'OK')
                      {
                        return Html::a(
                            '<i class="glyphicon glyphicon glyphicon-ok"></i>',
                            $url,
                            ['class' => 'btn btn-success modalButton', 'title'=>'view/edit', ],
                            [
                                'title' => 'Print Label?',
                            ]
                        );
                      }
                      else
                      {
                        return Html::a(
                            '<img class="img-circle" src="../web/images/printer-icon.png" alt="Terima" width="42" height="42">',
                            $url,
                            // ['class' => 'btn btn-warning modalButton', 'title'=>'view/edit', ],
                            [
                                'title' => 'Print Label',
                                'data-pjax' => '0',
                                'data-confirm' => Yii::t('app', 'Print Label?'),
                            ]
                        );
                      }
                    },
                ],
            ],
            // 'id',
            'batch',
            'nopo',
            'no_surjal',
            'koitem_fg',
            'nama_fg',
            'supplier',
            'datetime_start',
            'datetime_stop',
            // 'datetime_write',
            // 'admin_id',
            // 'status_print',
            // 'tgl_plan_kirim',
            // 'qty_demand',
            // 'price_unit',
            // 'qty_kirim',
            // 'tgl_po',
            // 'tgl_kirim',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
  </div>
</div>
