<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Adminlog */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Adminlogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adminlog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nopo',
            'no_surjal',
            'koitem_pack',
            'nama_pack',
            'supplier',
            'datetime_start',
            'datetime_stop',
            'datetime_write',
            'batch',
            'admin_id',
            'status_print',
            'tgl_plan_kirim',
            'qty_demand',
            'price_unit',
            'qty_kirim',
            'tgl_po',
            'tgl_kirim',
        ],
    ]) ?>

</div>
