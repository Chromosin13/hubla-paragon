<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\AdminlogFg;
use app\models\NamaAdmin;
use app\models\DataPurchaseOrder;
use app\models\Wadah;
use app\models\Packaging;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\dialog\Dialog;
use yii\web\JsExpression;
use kartik\date\DatePicker;
use kartik\widgets\DateTimePicker;
use kartik\file\FileInput;
use yii\web\UploadedFile;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $modelCustomer app\modules\yii2extensions\models\Customer */
/* @var $modelsAdminlog app\modules\yii2extensions\models\Addres */

?>

<style>
    .bootstrap-tagsinput {
        /*padding: 50px;*/
        width: 1920px;
        height: auto;
    }

    .zoom {
        /*padding: 50px;*/
        transition: transform .3s; /* Animation */
        width: auto;
        height: auto;
        margin: 0 auto;
    }

    .zoom:hover {
        transform: scale(0.99); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    }

    @import url(https://fonts.googleapis.com/css?family=Lato:900);
    *, *:before, *:after{
      box-sizing:border-box;
    }
    body{
      font-family: 'Lato', sans-serif;
        ;
    }
    div.foo{
      width: 90%;
      margin: 0 auto;
      text-align: center;
    }
    .letter{
      display: inline-block;
      font-weight: 900;
      font-size: 4em;
      margin: 0.2em;
      position: relative;
      color: #00B4F1;
      transform-style: preserve-3d;
      perspective: 400;
      z-index: 1;
    }
    .letter:before, .letter:after{
      position:absolute;
      content: attr(data-letter);
      transform-origin: top left;
      top:0;
      left:0;
    }
    .letter, .letter:before, .letter:after{
      transition: all 0.3s ease-in-out;
    }
    .letter:before{
      color: #fff;
      text-shadow:
        -1px 0px 1px rgba(255,255,255,.8),
        1px 0px 1px rgba(0,0,0,.8);
      z-index: 3;
      transform:
        rotateX(0deg)
        rotateY(-15deg)
        rotateZ(0deg);
    }
    .letter:after{
      color: rgba(0,0,0,.11);
      z-index:2;
      transform:
        scale(1.08,1)
        rotateX(0deg)
        rotateY(0deg)
        rotateZ(0deg)
        skew(0deg,1deg);
    }
    .letter:hover:before{
      color: #fafafa;
      transform:
        rotateX(0deg)
        rotateY(-40deg)
        rotateZ(0deg);
    }
    .letter:hover:after{
      transform:
        scale(1.08,1)
        rotateX(0deg)
        rotateY(40deg)
        rotateZ(0deg)
        skew(0deg,22deg);
</style>


<div class="box box-widget widget-user">
    <div class="widget-user-header bg-yellow-gradient">
      <h3 class="widget-user-username"><b>Membuat Form</b></h3>
      <h5 class="widget-user-desc">Admin Logistik</h5>
    </div>
    <div class="widget-user-image">
      <img class="img-circle" src="../web/images/gambarlog/question.png" alt="User Avatar">
    </div>
    <br></br>
  <div class="box-body">

    <div class="nama-admin-form">

        <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

        <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>


        <?php DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 5, // the maximum times, an element can be added (default 999)
            'min' => 0, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $modelsAdminlogFg[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'nopo',
                'no_surjal',
                'koitem_fg',
                'nama_fg',
                'supplier',
                'batch',

            ],
        ]); ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <i class="glyphicon glyphicon-envelope"></i> <?php echo $nopo; ?>
                    <button type="button" class="add-item btn btn-success btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> ADD</button>
                </h4>
            </div>
            <div class="panel-body">
                <div class="container-items"><!-- widgetBody -->
                <?php foreach ($modelsAdminlogFg as $i => $modelAdminlog): ?>
                    <div class="item panel panel-default"><!-- widgetItem -->
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">Form Admin</h3>
                            <div class="pull-right">
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i>REMOVE</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                                // necessary for update action.
                                if (! $modelAdminlog->isNewRecord) {
                                    echo Html::activeHiddenInput($modelAdminlog, "[{$i}]id");
                                }
                            ?>
                            <div class="row">
                                      <div class="col-md-6">

                                          <?= $form->field($modelAdminlog, "[{$i}]nopo")->textInput(['readOnly'=>true]) ?>

                                      </div>
                                      <div class="col-md-6">
                                          <?= $form->field($modelAdminlog, "[{$i}]koitem_fg")->textInput(['readOnly'=>true]) ?>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-6">

                                        <?= $form->field($modelAdminlog, "[{$i}]supplier")->textInput(['readOnly'=>true]) ?>

                                      </div>
                                      <div class="col-md-6">
                                          <?= $form->field($modelAdminlog, "[{$i}]nama_fg")->textInput(['readOnly'=>true]) ?>
                                        <!-- /input-group -->
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-6">
                                      <?php
                                          //$today = date("ymd");

                                          $today = date("ymd");

                                          echo $form->field($modelAdminlog, "[{$i}]batch")->textInput(['readOnly'=>true, 'value'=>$today])
                                      ?>
                                      </div>
                                      <div class="col-md-6">
                                         <?= $form->field($modelAdminlog, "[{$i}]no_surjal")->textInput() ?>
                                        <!-- /input-group -->
                                      </div>
                                  </div>
                              </div>
                          </div>
                      <?php endforeach; ?>
                      </div>
                  </div>
              </div><!-- .panel -->
              <?php DynamicFormWidget::end(); ?>

        <div class="form-group">
            <?= Html::submitButton($modelAdminlog->isNewRecord ? 'CREATE' : 'Create', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
  </div>
</div>

<?php
$script = <<< JS

//$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
      // $('input[id^="adminlog-"][id$="-batch"]').each(function() {
      //   var d = new Date();
      //   //var value = d.getFullYear().toString().slice(-2) + (d.getMonth() + 1).toString() + d.getDate().toString();
      //   var month = d.getMonth()+1;
      //   var day = d.getDate();
      //
      //   var output = d.getFullYear().toString().slice(-2) +
      //   (month<10 ? '0' : '') + month +
      //   (day<10 ? '0' : '') + day;
      //   $(this).val(output);
      // });


      $('input[name^="Adminlog["]').change(function() {

        var prefix = $(this).attr('id').split("-");
        prefix.splice(-1, 1);
        var sup_id = '#' + prefix.join('-') + '-supplier';
        var item_code_id = '#' + prefix.join('-') + '-koitem_fg';
        var item_name_id = '#' + prefix.join('-') + '-nama_fg';
        var nopo = $(this).val();

          $.get('index.php?r=adminlog-fg/get-po',{ nopo : nopo }, function(data){
            var data = $.parseJSON(data);


              // $(item_code_id).empty().append($('<option>', {
              //     value:'',
              //     text:'Select...'
              // }));

              $.each(data, function(idx, value) {
                $(item_code_id).append($('<option>', {
                  value: value['koitem_fg'],
                  text: value['koitem_fg']
                }));
              });

          });
      });

    $('input[name^="Adminlog["]').change(function() {
      var prefix = $(this).attr('id').split("-");
      prefix.splice(-1, 1);
      var sup_id = '#' + prefix.join('-') + '-supplier';
      var item_code_id = '#' + prefix.join('-') + '-koitem_fg';
      var item_name_id = '#' + prefix.join('-') + '-nama_fg';
      var nopo = $(this).val();
      $(item_code_id).change(function(){

        var koitem_fg = $(this).val();
          $.get('index.php?r=adminlog-fg/get-po',{ nopo: nopo, koitem_fg : koitem_fg }, function(data){
            var data = $.parseJSON(data);

            $.each(data, function(idx, value) {
              $(sup_id).val(value['supplier']);
              $(item_name_id).val(value['nama_pabrik']);
            });
          });
      });
    });

        // $('input[name^="Adminlog["]').change(function() {

        //   if ($(this).attr('name').endsWith('no_batch]')) {

        //     var prefix = $(this).attr('id').split("-");
        //     // prefix.splice(-1, 1);
        //     // var batch_id = '#' + prefix.join('-') + '-no_batch';
        //     // var no_batch = $(this).val();

        //     $(batch_id).val(value['no_batch']);
        //   }
        // });

//});



// $(".dynamicform_wrapper").on('afterInsert', function(e, item) {
//     var datePickers = $(this).find('[data-krajee-kvdatepicker]');
//     datePickers.each(function(index, el) {
//         $(this).parent().removeData().kvDatepicker('remove');
//         $(this).parent().kvDatepicker(eval($(this).attr('data-krajee-kvdatepicker')));
//     });
// });

    // $('select[name^="Adminlog["]').change(function() {
    //         var prefix = $(this).attr('id').split("-");
    //         prefix.splice(-1, 1);
    //         var sup_id = '#' + prefix.join('-') + '-supplier';
    //         var item_code_id = '#' + prefix.join('-') + '-kode_item';
    //         var item_name_id = '#' + prefix.join('-') + '-nama_item';
    //         var item_batch_id = '#' + prefix.join('-') + '-no_batch';
    //         var no_po = $(this).val();

    //         $(item_batch_id).datepicker({
    //                 yearRange: '-71:+0',
    //                 changeMonth: true,
    //                 changeYear: true,
    //                 showMonthAfterYear: true,
    //                 dateFormat: 'dd-mm-yy',
    //                 minDate:"-71Y",
    //                 maxDate:"+0Y"
    //             });
    //

    //         $(item_batch_id).keydown(function (event) {
    //             event.preventDefault();
    //         })
    // });

$(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
    console.log("beforeInsert");
});

$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    console.log("afterInsert");
});

$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
    if (! confirm("Are you sure you want to delete this item?")) {
        return false;
    }
    return true;
});

$(".dynamicform_wrapper").on("afterDelete", function(e) {
    console.log("Deleted item!");
});

$(".dynamicform_wrapper").on("limitReached", function(e, item) {
    alert("Limit reached");
});


jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title-adminlog").each(function(index) {
        jQuery(this).html("Adminlog: " + (index + 1))
    });
});

jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title-adminlog").each(function(index) {
        jQuery(this).html("Adminlog: " + (index + 1))
    });
});

JS;
$this->registerJs($script);
?>
