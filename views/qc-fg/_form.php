<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
use faryshta\widgets\JqueryTagsInput;
use kartik\checkbox\CheckboxX;
use app\models\Kemas2;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\QcFg */
/* @var $form yii\widgets\ActiveForm */
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\dialog\Dialog;
use yii\web\JsExpression;

?>

<div class="qc-fg-form">

    <?php $form = ActiveForm::begin(['id'=>'dynamic-form']); ?>

        <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-search"></i></span>

            <div class="info-box-content">
              <b><div class="col-md-3 col-sm-6 col-xs-12" id="nama_fg_results"></div></b>
              <span class="info-box-number"><div id="snfg_results"></div><div id="snfg_komponen_results"></div></span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                  <span class="progress-description">
                  <div class="col-md-3 col-sm-6 col-xs-12" id="streamline_results"></div>
                  <div class="col-md-3 col-sm-6 col-xs-12" id="start_results"></div>
                  <div class="col-md-3 col-sm-6 col-xs-12" id="due_results"></div>
                  </span>
            </div>
            <!-- /.info-box-content -->
        </div>


        <div class="callout callout-danger" id="jadwal_tidak_ada">
          <h4>Warning!</h4>

          <p>Tampaknya Jadwal Tersebut belum di scan di proses Kemas 2</p>
          <p>Jika anda scan dengan SNFG, coba scan dengan SNFG Komponen, begitupula sebaliknya</p>
          <p>Jika masih muncul peringatan ini berarti Proses Tersebut belum di proses di Kemas 2<p> 
        </div>

        <div class="callout callout-info" id="batchsplit">
          <h4>Terdapat Batch Split!</h4>

          <p>Silahkan isi Lanjutan Split Batch dan Palet Ke</p>
        </div>

        <div class="callout callout-info" id="batchsplit1">
          <h4><b>Tidak</b> Terdapat Batch Split</h4>

          <p>Silahkan isi Lanjutan Split Batch dan Palet Ke</p>
        </div>

     <div class="box">

            <div class="box-header with-border">
            
            <h4><b>Pilih Jenis Scan</b></h4>            
            <?=
                    Html::button('<b>Per-SNFG</b>',['class'=>'btn btn-success', 'id' => 'btn_snfg','style' => 'width: 120px; border-radius: 5px;']);
            ?>
<!--             <?=
                    Html::button('<b>Per-Komponen</b>',['class'=>'btn btn-primary', 'id' => 'btn_snfg_komponen', 'style' => 'width: 120px; border-radius: 5px;']);
            ?> -->

            <!--    <label>
                      <input type="checkbox" id="per_snfg_komponen">
                      Insert Per SNFG Komponen ?
                    </label>
            -->
            </div>


            <div style="width:50%; float: left; display: inline-block;" class="box-header with-border">
              <h2 class="box-title">Input dan Informasi SNFG dari Planner</h2>
                    
                    <div id="form_snfg">
                    <?= $form->field($model, 'snfg', [
                            'addon' => [
                                'prepend' => [
                                    'content'=>'<i class="fa fa-barcode"></i>'
                                    
                                ],
                                'append' => [
                                    'content' => Html::button('Tekan Setelah Scan',['class'=>'btn btn-primary','id'=>'afterScnBtnSnfg']),
                                    'asButton' => true
                                ]            
                            ]
                        ]);
                    ?>
                    </div>

                    <div id="form_snfg_komponen">
                    <?= $form->field($model, 'snfg_komponen', [
                            'addon' => [
                                'prepend' => [
                                    'content'=>'<i class="fa fa-barcode"></i>'
                                    
                                ],
                                'append' => [
                                    'content' => Html::button('Tekan Setelah Scan',['class'=>'btn btn-primary','id'=>'afterScnBtnKomponen']),
                                    'asButton' => true
                                ]            
                            ]
                        ])->textInput(['disabled'=>'true']);
                    ?>
                    </div>

                <?= $form->field($model, 'jenis_kemas')->textInput(['id'=>'qcfg-jenis_kemas','readonly' => 'true']) ?> 

                <div id="form_lsb_palet">

                    <div class="box box-default box-solid"  id="lsb_palet">
                        
                        <div class="box-header with-border">
                          <h3 class="box-title">Lanjutan Split Batch</h3>

                          <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                          </div>
                          <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" style="display: block;">

                            <?= $form->field($model, 'lanjutan_split_batch')->dropDownList(
                                    ArrayHelper::map(Kemas2::find()->all()
                                    ,'lanjutan_split_batch','lanjutan_split_batch')
                                    ,['prompt'=>'Select Batch Split']
                                );
                            ?>

                        </div>
                    </div>

                </div>

                <div id="form_start_stop">
                    <a class="btn btn-danger" id="stop-button">
                    <i class="fa fa-pause" style ="width: 60px; border-radius: 5px;" ></i><b>STOP</b>
                    </a>


                    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>
                </div>
                
            </div>
            <div style="width:50%; display: inline-block;" class="box-header with-border">
                               <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Posisi Terakhir</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="display: block;">
                    <h2 class="box-title">Posisi Terakhir</h2><br>
                    <b>SNFG Komponen</b>
                    <input type="text" class="form-control" id="ppr-snfg_komponen" placeholder="" disabled>
                    <b>Posisi</b>
                    <input type="text" class="form-control" id="ppr-posisi" placeholder="" disabled>
                    <b>State</b>
                    <input type="text" class="form-control" id="ppr-state" placeholder="" disabled>
                    <b>Jenis Proses</b>
                    <input type="text" class="form-control" id="ppr-jenis-proses" placeholder="" disabled>
                    <b>Lanjutan</b>
                    <input type="text" class="form-control" id="ppr-lanjutan" placeholder="" disabled>
                    <b>Is Done</b>
                    <input type="text" class="form-control" id="ppr-isdone" placeholder="" disabled>
                </div>
                </div>
                <!-- /.box-body -->
              </div>

    </div>



            <div id="form_jenis">

                <div id="qcfg-jenis_periksa_00">    
                <?= $form->field($model, 'jenis_periksa')->dropDownList(['JADWAL BARU' => 'JADWAL BARU','REWORK' => 'REWORK','ADJUST' => 'ADJUST','PENDING' => 'PENDING','LANJUTAN' => 'LANJUTAN'],['prompt'=>'Select Option']); ?>

                </div>

                <div id="qcfg-jenis_periksa_0"> 
                <?= $form->field($model, 'jenis_periksa')->textInput(['id'=>'qcfg-jenis_periksa_1', 'disabled'=>'true' , 'readonly' =>'true']); 
                ?>
                </div>    
            
            </div>


            <div id="form_entry_start_stop">

                    <div class="box" id="qcfg-start">
                            <div class="box-header with-border">
                              <h2 class="box-title">Start Entry</h2>

                                <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>
                                
                            </div>
                    </div>

                    <div class="box" id="qcfg-stop">
                            <div class="box-header with-border">
                              <h2 class="box-title">Stop Entry</h2>
                                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'qcfg-lanjutan_stop','readonly' => 'true']) ?>



                                <?php echo $form->field($model, 'nama_inspektor', ['hintType' => ActiveField::HINT_SPECIAL,'addon' => ['prepend' => ['content'=>'<i class="fa fa-barcode"></i>']]
                                ])->widget(JqueryTagsInput::className([]))->hint(' <b>Scan</b> : Klik pada isian dibawah kemudian Scan Barcode pada Nametag<p \> <b>Manual</b> : isi dengan Format 4 Huruf Kapital, Contoh : "ABCD" lalu diikuti dengan menekan Enter ');
                                ?>


                                <?= $form->field($model, 'status')->dropDownList(['RELEASE' => 'RELEASE','REWORK' => 'REWORK','CEK_MIKRO' => 'CEK_MIKRO','PENDING' => 'PENDING','REJECT' => 'REJECT'],['prompt'=>'Select Option']); ?>
                                
                                <div id="check_box_is_done">
                                
                                <?= $form->field($model, 'qty', [
                                    'addon' => [
                                        'append' => [
                                            'content' => '<b>Pcs</b>'
                                        ]            
                                    ]
                                ])->textInput() ?>

                                <?= $form->field($model, 'qty_sample', [
                                    'addon' => [
                                        'append' => [
                                            'content' => '<b>Pcs</b>'
                                        ]            
                                    ]
                                ])->textInput() ?>

                                <?= $form->field($model, 'qty_reject', [
                                    'addon' => [
                                        'append' => [
                                            'content' => '<b>Pcs</b>'
                                        ]            
                                    ]
                                ])->textInput() ?>

                                <?php
                                    echo '<label class="cbx-label" for="is_done">Is Done?</label>';
                                    echo CheckboxX::widget([
                                        'model' => $model,
                                        'attribute' => 'is_done',
                                        'pluginOptions' => [
                                            'threeState' => false,
                                            'size' => 'lg'
                                        ]
                                    ]); 
                                ?>

                                </div>
                            </div>
                    </div>

                    <div class="form-group" id="create-button">
                        <?php echo Dialog::widget([
                        'libName' => 'krajeeDialogCust', // a custom lib name
                        'options' => [  // customized BootstrapDialog options
                            'size' => Dialog::SIZE_LARGE, // large dialog text
                            'type' => Dialog::TYPE_WARNING, // bootstrap contextual color
                            'title' => 'Konfirmasi Input',
                            'buttons' => [
                                            [
                                                
                                                'id' => 'btn-ok',
                                                'icon' => 'glyphicon glyphicon-check',
                                                'label' => 'Sudah Benar, Submit!',
                                                'cssClass' => 'btn-primary',
                                                'action' => new JsExpression("function() {
                                                    $('#submitButton').submit();            
                                                    }")
                                                // Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
                                            ],
                                        ]
                                    ]
                                ]);
                        ?>
                        <button type="button" id="btn-custom" class="btn btn-info">Validate</button>
                        <button id="submitButton" type="submit" class="btn btn-success">Creates</button>
                        <!-- <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?> -->
                    </div>
            </div>


    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

// Hide Submit Button untuk Konfirmasi Input
$('#submitButton').hide();


//document.getElementById("qcfg-lanjutan_split_batch").disabled = true;

// Inisialisasi Form Hide
    $('#form_start_stop').hide(); 
    $('#form_jenis').hide(); 
    $('#form_entry_start_stop').hide();

// After Klik Tombol Tekan Scan
    // Klik Scan SNFG
    $('#afterScnBtnSnfg').click(function(){
        $('#form_lsb_palet').show(); 
    });

    // Klik Scan Komponen
    $('#afterScnBtnKomponen').click(function(){
        $('#form_lsb_palet').show();  
    });

// After Klik Start Stop Button

    $('#stop-button').click(function(){
        $('#form_jenis').show();
    });

// After Select Jenis Olah
    $('#qcfg-jenis_periksa').change(function(){
        var jenis_periksa = $('#qcfg-jenis_periksa').val();
        if(jenis_periksa==""){
            $('#form_entry_start_stop').hide(); 
        } else {
            $('#form_entry_start_stop').show();
            $('#create-button').hide();
            $('#qcfg-status').change(function(){
                    var status = $('#qcfg-status').val();
                    if(status==""){
                        $('#create-button').hide();
                    }else{
                        $('#create-button').show();
                    }
                });
        }
    });

// Validate Button

$("#btn-custom").on("click", function() {
    var snfg = $('#qcfg-snfg').val();
    var state = $('#qcfg-state').val();
    var jenis_periksa = $('#qcfg-jenis_periksa').val();
    var jenis_kemas = $('#qcfg-jenis_kemas').val();
    var lanjutan = $('#qcfg-lanjutan').val();
    var nama_inspektor =  $('#qcfg-nama_inspektor').val();
    var is_done = $('#qcfg-is_done').val();
    var status = $('#qcfg-status').val();
    var qty = $('#qcfg-qty').val();
    var lanjutan_split_batch = $('#qcfg-lanjutan_split_batch').val();

        if(lanjutan==null||lanjutan_split_batch==null){
            alert('Penarikan data Lanjutan Gagal, Mohon refresh halaman dan scan ulang jadwal');
        }else if(is_done==1){
            krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(snfg)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Olah</b><p>' + '<font color="blue"><i>'+(jenis_kemas)+'</i></font><p>' + 
                                '<b>Jenis Periksa</b><p>' + '<font color="blue"><i>'+(jenis_periksa)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>' +  
                                '<b>Nama Inspektor</b><p>' + '<font color="blue"><i>'+(nama_inspektor)+'</i></font><p>' +
                                '<b>Status</b><p>' + '<font color="blue"><i>'+(status)+'</i></font><p>' +
                                '<b>Quantity</b><p>' + '<font color="blue"><i>'+(qty)+'</i></font><p>' +
                                '<b>Lanj. Split Batch</b><p>' + '<font color="blue"><i>'+(lanjutan_split_batch)+'</i></font><p>' +
                                '<b><font color="blue"><i>Jadwal Sudah Selesai (Tidak Lanjut Shift)</i></font></b><p>',function(result) {}
            );
        }else if(is_done!=1){
            krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(snfg)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Olah</b><p>' + '<font color="blue"><i>'+(jenis_kemas)+'</i></font><p>' + 
                                '<b>Jenis Periksa</b><p>' + '<font color="blue"><i>'+(jenis_periksa)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>' +  
                                '<b>Nama Inspektor</b><p>' + '<font color="blue"><i>'+(nama_inspektor)+'</i></font><p>' +
                                '<b>Status</b><p>' + '<font color="blue"><i>'+(status)+'</i></font><p>' +
                                '<b>Quantity</b><p>' + '<font color="blue"><i>'+(qty)+'</i></font><p>' +
                                '<b>Lanj. Split Batch</b><p>' + '<font color="blue"><i>'+(lanjutan_split_batch)+'</i></font><p>' +
                                '<b><font color="blue"><i>Jadwal Akan Dilanjutkan ke Shift Berikutnya (Belum Done)</i></font></b><p>',function(result) {}
            );
        }

});


$('#form_snfg_komponen').hide(); 
$('#form_snfg').hide(); 

$('#btn_snfg').click(function(){
    document.getElementById("qcfg-snfg_komponen").disabled = true
    document.getElementById("qcfg-snfg").disabled = false;
    $('#form_snfg_komponen').hide(); 
    $('#form_snfg').show(); 
});

$('#btn_snfg_komponen').click(function(){
    document.getElementById("qcfg-snfg_komponen").disabled = false
    document.getElementById("qcfg-snfg").disabled = true;
    $('#form_snfg_komponen').show();
    $('#form_snfg').hide();
});

// Commented on Peb 17 10:04
// $('#per_snfg_komponen').change(function(){
//     var per_snfg_komponen = document.getElementById("qcfg-snfg_komponen").disabled;
//     if(per_snfg_komponen){
//         document.getElementById("qcfg-snfg_komponen").disabled = false
//         document.getElementById("qcfg-snfg").disabled = true;
//     }else{
//         document.getElementById("qcfg-snfg_komponen").disabled = true
//         document.getElementById("qcfg-snfg").disabled = false;
//     } 
// });


$('#qcfg-snfg').change(function(){
    var snfg = $(this).val();

    $('#snfg_results').html($(this).val());

    $.get('index.php?r=kemas2/get-jenis-kemas-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        $('#qcfg-jenis_kemas').attr('value',data.jenis_kemas);
    });

    
    // Batch Split Validator
    $.get('index.php?r=qc-fg/check-batch-split-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        alert(data.last_status);

        
        // If Jadwal in Kemas 2 Not Found Then The Data Is Null, Hence Alert Error and Restrict Input
        if (data == null){
            alert('null');
           $('#jadwal_tidak_ada').show();
           $('#batchsplit').hide();
            $('#batchsplit1').hide();
            document.getElementById("qcfg-lanjutan_split_batch").disabled = true;
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide(); 

        // If Jadwal in Kemas 2 Has The Highest Number of Batch Split value > 1 then It has Batch Split, 
        // Hence Show the Input For Lanjutan Split Batch 
        } else if(data.last_status==1){
            alert('1');
            $('#jadwal_tidak_ada').hide();
            $('#batchsplit').show();
            $('#batchsplit1').hide();

            $('#lsb_palet').show();

            document.getElementById("qcfg-lanjutan_split_batch").disabled = false;
            

        // If Jadwal in Kemas 2 Has Batch Split Value = 1 ( or data.last_status == -1) then
        // Show the Input For Lanjutan Split Batch
        } else {
            alert('else');

            $('#jadwal_tidak_ada').hide();
            $('#batchsplit').hide();
            
            // Menampilkan Bagan Tidak Terdapat Batch Split
            $('#batchsplit1').show();

            // Show Form Lanjutan Split Batch
            $('#lsb_palet').show();

           document.getElementById("qcfg-lanjutan_split_batch").disabled = false;           
            
        }
    });

    $.get('index.php?r=scm-planner/get-planner-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);    
        $('#qcfg-snfg_komponen').attr('value',data.snfg_komponen);
        
        $('#streamline_results').html('STREAMLINE : '+data.streamline);
        $('#start_results').html('START : '+ data.start);
        $('#due_results').html('DUE : '+ data.due);
        $('#nama_bulk_results').html(data.nama_bulk);
        $('#nama_fg_results').html(data.nama_fg);
        //$('#jumlah').attr('value',data.jumlah);
    });
    $.get('index.php?r=pengolahan/get-pengolahan',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#no-batch').attr('value',data.nobatch);
    });
    $.get('index.php?r=scm-planner/get-pprr',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data);
        //alert(data);
        $('#ppr-snfg_komponen').attr('value',data.snfg_komponen);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });

    // Check Hold and Pause dari Planner
    $.get('index.php?r=scm-planner/get-planner-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);

        if(data.status==null){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Not Found');
        }

        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#form_entry_start_stop').hide();
            alert('Terdapat setidaknya 1 Komponen yang ditetapkan sebagai HOLD/PAUSE')
        }
        else{
                // Assign Batch Split and Palet Assign
                $.post("index.php?r=qc-fg/batch-split-assign-snfg&snfg="+$('#qcfg-snfg').val(), function (data){
                        $("select#qcfg-lanjutan_split_batch").html(data);

                            // ONCHANGE LANJUTAN SPLIT BATCH

                            $('#qcfg-lanjutan_split_batch').change(function(){

                                // Enable Form Start Stop

                                $('#form_start_stop').show();

                                // ASSIGN VARIABLE SPLIT BATCH

                                var lanjutan_split_batch = $('#qcfg-lanjutan_split_batch').val();

                                //$('#form_jenis').show();

                                $.get('index.php?r=scm-planner/get-last-proses-snfg',{ snfg : snfg, lanjutan_split_batch : lanjutan_split_batch },function(data){

                                        var data = $.parseJSON(data);

                                        if(data.posisi=="QC FG" && data.state=="START"){
                                            $('#start-button').hide();
                                            $('#stop-button').show(); 

                                            $('#qcfg-jenis_periksa_00').hide();
                                            document.getElementById("qcfg-jenis_periksa").disabled = true;
                                            $('#qcfg-jenis_periksa_0').show();
                                            document.getElementById("qcfg-jenis_periksa_1").disabled = false;
                                            $('#qcfg-jenis_periksa_1').attr('value',data.jenis_proses);


                                            $('#stop-button').click(function(){ 

                                                $('#qcfg-jenis_periksa').change(function(){

                                                    var jenis_periksa = $('#qcfg-jenis_periksa').val();
                                                    var snfg = $('#qcfg-snfg').val();
                                                    var lanjutan_split_batch = $('#qcfg-lanjutan_split_batch').val();

                                                    $.get('index.php?r=qc-fg/lanjutan-qc-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                                                var data = $.parseJSON(data);
                                                                //alert(data.sediaan);
                                                                
                                                                $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                                                $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                                                
          
                                                    });

                                                });

                                            });

                                        }else if(data.posisi=="QC FG" && data.state=="STOP"){
                                            $('#start-button').hide();
                                            $('#stop-button').show(); 


                                            $('#qcfg-jenis_periksa_00').hide();
                                            document.getElementById("qcfg-jenis_periksa").disabled = true;
                                            $('#qcfg-jenis_periksa_0').show();
                                            document.getElementById("qcfg-jenis_periksa_1").disabled = false;
                                            $('#qcfg-jenis_periksa_1').attr('value',data.jenis_proses);


                                            $('#stop-button').click(function(){ 

                                                $('#form_entry_start_stop').show();
                                                var lanjutan_split_batch = $('#qcfg-lanjutan_split_batch').val();
                                                var jenis_periksa = $('#qcfg-jenis_periksa_1').val();
                                                var snfg_komponen = $('#qcfg-snfg_komponen').val();                                        


                                                $.get('index.php?r=qc-fg/lanjutan-qc-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                                                var data = $.parseJSON(data);
                                                                //alert(data.sediaan);                                     
                                                                $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                                                $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                                                
          
                                                }); 

                                                $('#qcfg-jenis_periksa').change(function(){

                                                    var jenis_periksa = $('#qcfg-jenis_periksa').val();
                                                    var snfg = $('#qcfg-snfg').val();
                                                    var lanjutan_split_batch = $('#qcfg-lanjutan_split_batch').val();

                                                    $.get('index.php?r=qc-fg/lanjutan-qc-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                                                var data = $.parseJSON(data);
                                                                //alert(data.sediaan);
                                                                
                                                                $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                                                $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                                                
          
                                                    });

                                                });


                                            });


                                        }else if(data.posisi!="QC FG" && data!=null){
                                            $('#stop-button').show();
                                            alert('Jadwal Baru');
                                            
                                            $('#stop-button').click(function(){ 

                                                $('#qcfg-jenis_periksa').change(function(){

                                                    var jenis_periksa = $('#qcfg-jenis_periksa').val();
                                                    var snfg = $('#qcfg-snfg').val();
                                                    var lanjutan_split_batch = $('#qcfg-lanjutan_split_batch').val();

                                                    $.get('index.php?r=qc-fg/lanjutan-qc-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                                                var data = $.parseJSON(data);
                                                                //alert(data.sediaan);
                                                                
                                                                $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                                                $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                                                
          
                                                    });

                                                });

                                            });

                                        }else {

                                            $('#start-button').hide();
                                            $('#stop-button').hide(); 

                                            // $('#qcfg-jenis_periksa_00').hide();
                                            // document.getElementById("qcfg-jenis_periksa").disabled = true;
                                            // $('#qcfg-jenis_periksa_0').show();
                                            // document.getElementById("qcfg-jenis_periksa_1").disabled = false;
                                            // $('#qcfg-jenis_periksa_1').attr('value',data.jenis_proses);

                                            // $('#stop-button').click(function(){ 

                                            //     var jenis_periksa = $('#qcfg-jenis_periksa_1').val();
                                            //     var snfg = $('#qcfg-snfg').val();
                                            //     var lanjutan_split_batch = $('#qcfg-lanjutan_split_batch').val();

                                            //     $.get('index.php?r=qc-fg/lanjutan-qc-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                            //                 var data = $.parseJSON(data);
                                            //                 //alert(data.sediaan);
                                                            
                                            //                 $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                            //                 $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                                            
      
                                            //     });

                                            // });

                                        }

                                });

                });


                });

        }
    });
});


$('#qcfg-snfg_komponen').change(function(){
    var snfg_komponen = $(this).val();

    $('#snfg_komponen_results').html($(this).val());

    $.get('index.php?r=kemas2/get-jenis-kemas-komponen',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        $('#qcfg-jenis_kemas').attr('value',data.jenis_kemas);
    });

    //check batch split
    $.get('index.php?r=qc-fg/check-batch-split-komponen',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        if (data == null){
            $('#jadwal_tidak_ada').show();
            $('#batchsplit').hide();
            $('#batchsplit1').hide();

            document.getElementById("qcfg-lanjutan_split_batch").disabled = true;
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide(); 
        } else if(data.last_status==1){
            $('#jadwal_tidak_ada').hide();
            $('#batchsplit').show();
            $('#batchsplit1').hide();
            $('#lsb_palet').show();

            document.getElementById("qcfg-lanjutan_split_batch").disabled = false;
        } else if(data.last_status==0){
            alert('Proses pada Kemas 2 belum selesai')
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
        } else {
            $('#jadwal_tidak_ada').hide();
            $('#batchsplit').hide();
            $('#batchsplit1').show();
            $('#lsb_palet').show();

            document.getElementById("qcfg-lanjutan_split_batch").disabled = false;
        }
    });
    

    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        $('#qcfg-snfg').attr('value',data.snfg);
        
        $('#streamline_results').html('STREAMLINE : '+data.streamline);
        $('#start_results').html('START : '+ data.start);
        $('#due_results').html('DUE : '+ data.due);
        $('#nama_bulk_results').html(data.nama_bulk);
        $('#nama_fg_results').html(data.nama_fg);
        //$('#jumlah').attr('value',data.jumlah);
    });
    $.get('index.php?r=scm-planner/get-ppr',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        $('#ppr-snfg_komponen').attr('value',data.snfg_komponen);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#form_entry_start_stop').hide();
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Terdapat SNFG Komponen yang masih HOLD/PAUSE dari Planner')
        }
        else{

                // Assign Per Batch Split

                $.post("index.php?r=qc-fg/batch-split-assign-komponen&snfg_komponen="+$('#qcfg-snfg_komponen').val(), function (data){
                    $("select#qcfg-lanjutan_split_batch").html(data);
                        $('#qcfg-lanjutan_split_batch').change(function(){

                            // Enable Form Start Stop

                            $('#form_start_stop').show();

                            

                            // Show Palet Ke Dropdown

                            // $('#qcfg-palet_ke').show();

                            // // Assign Palet Ke

                            // $.post("index.php?r=qc-fg/palet-assign-komponen&snfg_komponen="+$('#qcfg-snfg_komponen').val()+"&lanjutan_split_batch="+$('#qcfg-lanjutan_split_batch').val(), function (data){
                                
                            //     $("select#qcfg-palet_ke").html(data);


                            // });

                            var lanjutan_split_batch = $('#qcfg-lanjutan_split_batch').val();

                            //$('#form_jenis').show();

                            $.get('index.php?r=scm-planner/get-last-proses-komponen',{ snfg_komponen : snfg_komponen , lanjutan_split_batch : lanjutan_split_batch },function(data){
                                var data = $.parseJSON(data);
                                if(data.posisi=="QC FG" && data.state=="START"){
                                    $('#start-button').hide();
                                    $('#stop-button').show(); 

                                    $('#qcfg-jenis_periksa_00').hide();
                                    document.getElementById("qcfg-jenis_periksa").disabled = true;
                                    $('#qcfg-jenis_periksa_0').show();
                                    document.getElementById("qcfg-jenis_periksa_1").disabled = false;
                                    $('#qcfg-jenis_periksa_1').attr('value',data.jenis_proses);

                                    // Assign Lanjutan 

                                    $('#stop-button').click(function(){ 


                                        $('#qcfg-jenis_periksa').change(function(){

                                            var lanjutan_split_batch = $('#qcfg-lanjutan_split_batch').val();
                                            var jenis_periksa = $('#qcfg-jenis_periksa').val();
                                            var snfg_komponen = $('#qcfg-snfg_komponen').val(); 

                                            $.get('index.php?r=qc-fg/lanjutan-qc-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.sediaan);                                               
                                                    $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                                    $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                                                                                                         
                                            });
                                        });

                                    });

                                    // EO Lanjutan

                                }else if(data.posisi=="QC FG" && data.state=="STOP"){
                                    $('#start-button').hide();
                                    $('#stop-button').show(); 
                                    $('#istirahat-start-button').hide(); 
                                    $('#istirahat-stop-button').hide();

                                    // Jenis Periksa Not Auto Assign
                                    $('#qcfg-jenis_periksa_00').hide();
                                    document.getElementById("qcfg-jenis_periksa").disabled = true;
                                    
                                    // Jenis Periksa Auto Assign
                                    $('#qcfg-jenis_periksa_0').show();
                                    document.getElementById("qcfg-jenis_periksa_1").disabled = false;
                                    $('#qcfg-jenis_periksa_1').attr('value',data.jenis_proses);


                                    $('#stop-button').click(function(){ 

                                        // Auto Assign Jenis Periksa

                                        $('#form_entry_start_stop').show();
                                        var lanjutan_split_batch = $('#qcfg-lanjutan_split_batch').val();
                                        var jenis_periksa = $('#qcfg-jenis_periksa_1').val();
                                        var snfg_komponen = $('#qcfg-snfg_komponen').val();                                        


                                        $.get('index.php?r=qc-fg/lanjutan-qc-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.sediaan);                                               
                                                    $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                                    $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                                                                                                         
                                        });                                        


                                        // Manually Assign
                                        $('#qcfg-jenis_periksa').change(function(){

                                            var lanjutan_split_batch = $('#qcfg-lanjutan_split_batch').val();
                                            var jenis_periksa = $('#qcfg-jenis_periksa').val();
                                            var snfg_komponen = $('#qcfg-snfg_komponen').val(); 

                                            $.get('index.php?r=qc-fg/lanjutan-qc-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.sediaan);                                               
                                                    $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                                    $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                                                                                                         
                                            });
                                        });

                                        
                                    });


                                }else if(data.posisi!="QC FG" && data!=null){
                                    $('#stop-button').show();
                                    alert('Jadwal Baru');


                                    $('#stop-button').click(function(){ 


                                        $('#qcfg-jenis_periksa').change(function(){

                                            var lanjutan_split_batch = $('#qcfg-lanjutan_split_batch').val();
                                            var jenis_periksa = $('#qcfg-jenis_periksa').val();
                                            var snfg_komponen = $('#qcfg-snfg_komponen').val(); 

                                            $.get('index.php?r=qc-fg/lanjutan-qc-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.sediaan);                                               
                                                    $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                                    $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                                                                                                         
                                            });
                                        });

                                    });
                            
                                }else {

                                    $('#start-button').hide();
                                    $('#stop-button').hide(); 

                                    // $('#qcfg-jenis_periksa_00').hide();
                                    // document.getElementById("qcfg-jenis_periksa").disabled = true;
                                    // $('#qcfg-jenis_periksa_0').show();
                                    // document.getElementById("qcfg-jenis_periksa_1").disabled = false;
                                    // $('#qcfg-jenis_periksa_1').attr('value',data.jenis_proses);

                                    // // Assign Lanjutan 

                                    // $('#stop-button').click(function(){ 

                                    //     var lanjutan_split_batch = $('#qcfg-lanjutan_split_batch').val();
                                    //     var jenis_periksa = $('#qcfg-jenis_periksa_1').val();
                                    //     var snfg_komponen = $('#qcfg-snfg_komponen').val(); 

                                    //     $.get('index.php?r=qc-fg/lanjutan-qc-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa, lanjutan_split_batch : lanjutan_split_batch },function(data){
                                    //             var data = $.parseJSON(data);
                                    //             //alert(data.sediaan);                                               
                                    //             $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                    //             $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                                                                                                     
                                    //     });

                                    // });

                                    // EO Lanjutan
                                }
                            });
                        
                        });

                });

        }
    });
});


$(function() {

    // Hide and Disable DynamicFormWidget for Kendala Model //

        // $('#kendala-form').hide();
        //     document.getElementById("kendala-0-keterangan").disabled = true;
        //     document.getElementById("kendala-0-start").disabled = true;
        //     document.getElementById("kendala-0-stop").disabled = true;


    //$('#state-create').hide();                                          //  Hide State Create
    $('#qcfg-jenis_periksa_0').hide();                                  //  Hide Jenis Proses Dropdown
    document.getElementById("qcfg-jenis_periksa_1").disabled = true;    //  Disable Jenis Proses auto-assign

    $('#lsb_palet').hide();
    $('#jadwal_tidak_ada').hide();
    $('#batchsplit').hide();
    $('#batchsplit1').hide();
    $('#qcfg-start').hide();
    $('#qcfg-stop').hide(); 

    $('#stop-button').click(function(){
            var stop = "STOP";
            $('#qcfg-state').attr('value',stop);
            $('#qcfg-stop').show(); 
            //         $('#qcfg-start').hide();   
            
            // $('#qcfg-jenis_periksa').change(function(){
            //     var jenis_periksa = $('#qcfg-jenis_periksa').val();
            //     var snfg = $('#qcfg-snfg').val();
            //     var snfg_komponen = $('#qcfg-snfg_komponen').val();
            //     // var per_snfg_komponen = document.getElementById("qcfg-snfg_komponen").disabled;
            //     if(per_snfg_komponen){    
            //         $.get('index.php?r=qc-fg/lanjutan-qc-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
            //                                 var data = $.parseJSON(data);
            //                                 //alert(data.sediaan);
            //                                 $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
            //                                 $('#qcfg-lanjutan').attr('value',data.lanjutan);
            //                                 $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
            //                                 $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
            //                             });                                        
            //     }else{
            //         $.get('index.php?r=qc-fg/lanjutan-qc-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
            //                                 var data = $.parseJSON(data);
            //                                 //alert(data.sediaan);
            //                                 $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
            //                                 $('#qcfg-lanjutan').attr('value',data.lanjutan);
            //                                 $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
            //                                 $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
            //                             });
            //     } 
            // });

            $('#qcfg-qty').hide(); 
            $('#check_box_is_done').hide();
                $('#qcfg-status').change(function(){
                    var status = $('#qcfg-status').val();
                    if (status=='RELEASE'){
                        $('#check_box_is_done').show();
                        $('#qcfg-qty').show();     
                    }else{
                        $('#check_box_is_done').hide();
                        $('#qcfg-qty').hide();
                    }

                }); 
    });               
});

    
JS;
$this->registerJs($script);
?>