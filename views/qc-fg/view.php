<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\QcFg */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Qc Fgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qc-fg-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'snfg',
            'tanggal',
            'nama_inspektor',
            'state',
            'aql',
            'filling_kesesuaian_bulk',
            'filling_kesesuaian_packaging_primer',
            'filling_netto',
            'filling_seal',
            'filling_leakage',
            'filling_warna_olesan',
            'filling_warna_performance',
            'filling_uji_ayun',
            'filling_uji_oles',
            'filling_uji_ketrok',
            'filling_drop_test',
            'filling_rub_test',
            'filling_identitas_packaging_primer',
            'filling_identitas_stc_bottom',
            'packing_kesesuaian_packaging_sekunder',
            'packing_qty_inner_box',
            'packing_identitas_unit_box',
            'packing_identitas_inner_box',
            'packing_performance_segel',
            'packing_posisi_packing',
            'paletting_qty_karton_box',
            'paletting_identitas_karton_box',
            'status',
            'waktu',
            'jumlah_inspektor',
            'jenis_periksa',
            'lanjutan',
            'nama_line',
            'retain_sample',

        ],
    ]) ?>

</div>
