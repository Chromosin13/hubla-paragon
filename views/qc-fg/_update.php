<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use faryshta\widgets\JqueryTagsInput;

/* @var $this yii\web\View */
/* @var $model app\models\QcFg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qc-fg-form">

    <?php $form = ActiveForm::begin(); ?>

     <div class="box">
            <div class="box-header with-border">
              <a class="btn btn-app" id="view-scm">
                <i class="fa fa-play"></i> View SCM Data
              </a>
                <?= $form->field($model, 'snfg')->textInput(['readonly'=>'true']) ?>
                <b>Streamline</b>
                <input type="text" class="form-control" id="streamline" disabled>
                <b>Start</b>
                <input type="text" class="form-control" id="start" placeholder="" disabled>
                <b>Due</b>
                <input type="text" class="form-control" id="due" placeholder="" disabled>
                <b>Nama Bulk</b>
                <input type="text" class="form-control" id="nama-bulk" placeholder="" disabled>
                <b>Nama FG</b>
                <input type="text" class="form-control" id="nama-fg" placeholder="" disabled>
                <b>No Batch</b>
                <input type="text" class="form-control" id="no-batch" placeholder="" disabled>
                <b>Jumlah Pc(s)</b>
                <input type="text" class="form-control" id="jumlah" placeholder="" disabled>
            </div>
    </div>

    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'jenis_periksa')->textInput(['readonly' => 'true']); ?>
    
    <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>

    <div class="box" id="qcfg-start">
            <div class="box-header with-border">
              <h2 class="box-title">Start Entry</h2>

                

                <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>

                <!-- <?= $form->field($model, 'nama_line')->textInput() ?> -->

                <?= $form->field($model, 'jumlah_inspektor')->textInput() ?>

                <?php echo $form->field($model, 'nama_inspektor')->widget(JqueryTagsInput::className([]));
                ?>
                
            </div>
    </div>

    <div class="box" id="qcfg-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Stop Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'qcfg-lanjutan_stop','readonly' => 'true']) ?> 

                 <?= $form->field($model, 'aql')->textInput() ?>

                <?= $form->field($model, 'filling_kesesuaian_bulk')->textInput() ?>

                <?= $form->field($model, 'filling_kesesuaian_packaging_primer')->textInput() ?>

                <?= $form->field($model, 'filling_netto')->textInput() ?>

                <?= $form->field($model, 'filling_seal')->textInput() ?>

                <?= $form->field($model, 'filling_leakage')->textInput() ?>

                <?= $form->field($model, 'filling_warna_olesan')->textInput() ?>

                <?= $form->field($model, 'filling_warna_performance')->textInput() ?>

                <?= $form->field($model, 'filling_uji_ayun')->textInput() ?>

                <?= $form->field($model, 'filling_uji_oles')->textInput() ?>

                <?= $form->field($model, 'filling_uji_ketrok')->textInput() ?>

                <?= $form->field($model, 'filling_drop_test')->textInput() ?>

                <?= $form->field($model, 'filling_rub_test')->textInput() ?>

                <?= $form->field($model, 'filling_identitas_packaging_primer')->textInput() ?>

                <?= $form->field($model, 'filling_identitas_stc_bottom')->textInput() ?>

                <?= $form->field($model, 'packing_kesesuaian_packaging_sekunder')->textInput() ?>

                <?= $form->field($model, 'packing_qty_inner_box')->textInput() ?>

                <?= $form->field($model, 'packing_identitas_unit_box')->textInput() ?>

                <?= $form->field($model, 'packing_identitas_inner_box')->textInput() ?>

                <?= $form->field($model, 'packing_performance_segel')->textInput() ?>

                <?= $form->field($model, 'packing_posisi_packing')->textInput() ?>

                <?= $form->field($model, 'paletting_qty_karton_box')->textInput() ?>

                <?= $form->field($model, 'paletting_identitas_karton_box')->textInput() ?>

                <?= $form->field($model, 'retain_sample')->textInput() ?>

                <?= $form->field($model, 'status')->dropDownList(['RELEASE' => 'RELEASE','REWORK' => 'REWORK','CEK_MIKRO' => 'CEK_MIKRO','PENDING' => 'PENDING','REJECT' => 'REJECT'],['prompt'=>'Select Option']); ?>
            </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

$(function() {
    var state = document.getElementById("qcfg-state").value;
     if(state=="START"){
         $('#qcfg-stop').hide();
     }else{
         $('#qcfg-start').hide();
     }
});


$('#view-scm').click(function(){
    var snfg = $('#qcfg-snfg').val();
    $.get('index.php?r=scm-planner/get-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
        $('#jumlah').attr('value',data.jumlah);
    });
    $.get('index.php?r=pengolahan/get-pengolahan',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#no-batch').attr('value',data.nobatch);
    });
});
    
JS;
$this->registerJs($script);
?>