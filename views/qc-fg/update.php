<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\QcFg */

$this->title = 'Update Qc Fg: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Qc Fgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="qc-fg-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_update', [
        'model' => $model,
        'modelsKendala' => $modelsKendala,
    ]) ?>

</div>
