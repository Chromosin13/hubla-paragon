$('#per_snfg_komponen').change(function(){
    var per_snfg_komponen = document.getElementById("qcfg-snfg_komponen").disabled;
    if(per_snfg_komponen){
        document.getElementById("qcfg-snfg_komponen").disabled = false
        document.getElementById("qcfg-snfg").disabled = true;
    }else{
        document.getElementById("qcfg-snfg_komponen").disabled = true
        document.getElementById("qcfg-snfg").disabled = false;
    } 
});


$('#qcfg-snfg').change(function(){
    var snfg = $(this).val();

    $('#snfg_results').html($(this).val());

    $.get('index.php?r=kemas2/get-jenis-kemas-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        $('#qcfg-jenis_kemas').attr('value',data.jenis_kemas);
    });

 
    $.get('index.php?r=qc-fg/check-batch-split-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        if (data == null){
           $('#jadwal_tidak_ada').show();
           $('#batchsplit').hide();
            $('#batchsplit1').hide();
            document.getElementById("qcfg-lanjutan_split_batch").disabled = true;
            document.getElementById("qcfg-palet_ke").disabled = true;
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide(); 
        } else if(data.last_status==1){
            $('#jadwal_tidak_ada').hide();
            $('#batchsplit').show();
            $('#batchsplit1').hide();

            document.getElementById("qcfg-lanjutan_split_batch").disabled = false;
            document.getElementById("qcfg-palet_ke").disabled = false;

            $('#qcfg-lanjutan_split_batch').show();
            $('#lsb_palet').show();
        } else if(data.last_status==0){
            alert('Proses pada Kemas 2 belum selesai')
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
        } else {
            $('#jadwal_tidak_ada').hide();
            $('#batchsplit').hide();
            $('#batchsplit1').show();

           document.getElementById("qcfg-lanjutan_split_batch").disabled = false;
           document.getElementById("qcfg-palet_ke").disabled = false;

            $('#qcfg-lanjutan_split_batch').show();
            $('#lsb_palet').show();
        }
    });

    $.get('index.php?r=scm-planner/get-planner-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);    
        $('#qcfg-snfg_komponen').attr('value',data.snfg_komponen);
        
        $('#streamline_results').html('STREAMLINE : '+data.streamline);
        $('#start_results').html('START : '+ data.start);
        $('#due_results').html('DUE : '+ data.due);
        $('#nama_bulk_results').html(data.nama_bulk);
        $('#nama_fg_results').html(data.nama_fg);
        //$('#jumlah').attr('value',data.jumlah);
    });
    $.get('index.php?r=pengolahan/get-pengolahan',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#no-batch').attr('value',data.nobatch);
    });
    $.get('index.php?r=scm-planner/get-pprr',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data);
        //alert(data);
        $('#ppr-snfg_komponen').attr('value',data.snfg_komponen);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });

    // Check Hold and Pause dari Planner
    $.get('index.php?r=scm-planner/get-planner-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#state-create').hide();
            alert('Terdapat setidaknya 1 Komponen yang ditetapkan sebagai HOLD/PAUSE')
        }
        else{
                // Assign Batch Split and Palet Assign
                $.post("index.php?r=qc-fg/batch-split-assign-snfg&snfg="+$('#qcfg-snfg').val(), function (data){
                        $("select#qcfg-lanjutan_split_batch").html(data);

                            // ONCHANGE LANJUTAN SPLIT BATCH

                            $('#qcfg-lanjutan_split_batch').change(function(){

                                // ASSIGN VARIABLE SPLIT BATCH

                                var lanjutan_split_batch = $('#qcfg-lanjutan_split_batch').val();


                                // SHOW PALET DROPDOWN BOX

                                // $('#qcfg-palet_ke').show();

                                // $.post("index.php?r=qc-fg/palet-assign-snfg&snfg="+$('#qcfg-snfg').val()+"&lanjutan_split_batch="+$('#qcfg-lanjutan_split_batch').val(), function (data){
                                        
                                //     $("select#qcfg-palet_ke").html(data);

                                
                                // });

                                $.get('index.php?r=scm-planner/get-last-proses-snfg',{ snfg : snfg, lanjutan_split_batch : lanjutan_split_batch },function(data){

                                        var data = $.parseJSON(data);

                                        if(data.posisi=="QC FG" && data.state=="START"){
                                            $('#start-button').hide();
                                            $('#stop-button').show(); 
                                            $('#istirahat-start-button').show(); 
                                            $('#istirahat-stop-button').hide();

                                            $('#qcfg-jenis_periksa_00').hide();
                                            document.getElementById("qcfg-jenis_periksa").disabled = true;
                                            $('#qcfg-jenis_periksa_0').show();
                                            document.getElementById("qcfg-jenis_periksa_1").disabled = false;
                                            $('#qcfg-jenis_periksa_1').attr('value',data.jenis_proses);


                                            $('#stop-button').click(function(){ 

                                                var jenis_periksa = $('#qcfg-jenis_periksa_1').val();
                                                var snfg = $('#qcfg-snfg').val();

                                                $.get('index.php?r=qc-fg/lanjutan-qc-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                                                            var data = $.parseJSON(data);
                                                            //alert(data.sediaan);
                                                            $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                                            $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                                            $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                                            $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);

                                                            // Disable Post Value Lanjutan Istirahat

                                                            document.getElementById("qc-fg-lanjutan-ist_istirahat_start").disabled = true;
                                                            document.getElementById("qc-fg-lanjutan-ist_istirahat_stop").disabled = true;                                                                      
                                                        
                                                        });

                                            });

                                        }else if(data.posisi=="QC FG" && data.state=="STOP"){
                                            $('#start-button').hide();
                                            $('#stop-button').show(); 
                                            $('#istirahat-start-button').hide(); 
                                            $('#istirahat-stop-button').hide(); 

                                            $('#qcfg-jenis_periksa_00').show();
                                            document.getElementById("qcfg-jenis_periksa").disabled = false;
                                            $('#qcfg-jenis_periksa_0').hide();
                                            document.getElementById("qcfg-jenis_periksa_1").disabled = true;

                                            $('#start-button').click(function(){ 

                                                $('#qcfg-jenis_periksa').change(function(){

                                                    var jenis_periksa = $('#qcfg-jenis_periksa').val();
                                                    var snfg = $('#qcfg-snfg').val();

                                                    $.get('index.php?r=qc-fg/lanjutan-qc-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                                                            var data = $.parseJSON(data);
                                                            //alert(data.sediaan);
                                                            $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                                            $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                                            $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                                            $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);

                                                            document.getElementById("qc-fg-lanjutan-ist_istirahat_start").disabled = true;
                                                            document.getElementById("qc-fg-lanjutan-ist_istirahat_stop").disabled = true;                                                                       
                                                        
                                                            });
                                                    });
                                                });

                                        }else {

                                            $('#qcfg-jenis_periksa_00').show();
                                            document.getElementById("qcfg-jenis_periksa").disabled = false;
                                            $('#qcfg-jenis_periksa_0').hide();
                                            document.getElementById("qcfg-jenis_periksa_1").disabled = true;

                                            $('#state-create').show();
                                            $('#start-button').show();
                                            $('#stop-button').show(); 
                                            $('#istirahat-start-button').show(); 
                                            $('#istirahat-stop-button').show();
                                            alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                                        }

                                });

                });

                // ONCHANGE PALET 

                $('#qcfg-palet_ke').change(function(){

                    // Unhide State Create

                    
                    var lanjutan_split_batch = $('#qcfg-lanjutan_split_batch').val();

                    var palet_ke = $('#qcfg-palet_ke').val();


                    });

                });

        }
    });
});


$('#qcfg-snfg_komponen').change(function(){
    var snfg_komponen = $(this).val();

    $('#snfg_komponen_results').html($(this).val());

    $.get('index.php?r=kemas2/get-jenis-kemas-komponen',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        $('#qcfg-jenis_kemas').attr('value',data.jenis_kemas);
    });

    //check batch split
    $.get('index.php?r=qc-fg/check-batch-split-komponen',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        if (data == null){
            $('#jadwal_tidak_ada').show();
            $('#batchsplit').hide();
            $('#batchsplit1').hide();

           document.getElementById("qcfg-lanjutan_split_batch").disabled = true;
           document.getElementById("qcfg-palet_ke").disabled = true;
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide(); 
        } else if(data.last_status==1){
            $('#jadwal_tidak_ada').hide();
            $('#batchsplit').show();
            $('#batchsplit1').hide();

           document.getElementById("qcfg-lanjutan_split_batch").disabled = false;
           document.getElementById("qcfg-palet_ke").disabled = false;
           
            $('#qcfg-lanjutan_split_batch').show();
            $('#lsb_palet').show();
        } else if(data.last_status==0){
            alert('Proses pada Kemas 2 belum selesai')
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
        } else {
            $('#jadwal_tidak_ada').hide();
            $('#batchsplit').hide();
            $('#batchsplit1').show();

           document.getElementById("qcfg-lanjutan_split_batch").disabled = false;
           document.getElementById("qcfg-palet_ke").disabled = false;
           
            $('#qcfg-lanjutan_split_batch').show();
            $('#lsb_palet').show();
        }
    });
    

    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        $('#qcfg-snfg').attr('value',data.snfg);
        
        $('#streamline_results').html('STREAMLINE : '+data.streamline);
        $('#start_results').html('START : '+ data.start);
        $('#due_results').html('DUE : '+ data.due);
        $('#nama_bulk_results').html(data.nama_bulk);
        $('#nama_fg_results').html(data.nama_fg);
        //$('#jumlah').attr('value',data.jumlah);
    });
    $.get('index.php?r=scm-planner/get-ppr',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        $('#ppr-snfg_komponen').attr('value',data.snfg_komponen);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#state-create').hide();
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Terdapat SNFG Komponen yang masih HOLD/PAUSE dari Planner')
        }
        else{

                // Assign Per Batch Split

                $.post("index.php?r=qc-fg/batch-split-assign-komponen&snfg_komponen="+$('#qcfg-snfg_komponen').val(), function (data){
                    $("select#qcfg-lanjutan_split_batch").html(data);
                        $('#qcfg-lanjutan_split_batch').change(function(){

                            var lanjutan_split_batch = $('#qcfg-lanjutan_split_batch').val();

                            // Show Palet Ke Dropdown

                            // $('#qcfg-palet_ke').show();

                            // // Assign Palet Ke

                            // $.post("index.php?r=qc-fg/palet-assign-komponen&snfg_komponen="+$('#qcfg-snfg_komponen').val()+"&lanjutan_split_batch="+$('#qcfg-lanjutan_split_batch').val(), function (data){
                                
                            //     $("select#qcfg-palet_ke").html(data);


                            // });

                            $('#state-create').show();

                            $.get('index.php?r=scm-planner/get-last-proses-komponen',{ snfg_komponen : snfg_komponen , lanjutan_split_batch : lanjutan_split_batch , palet_ke : palet_ke  },function(data){
                                var data = $.parseJSON(data);
                                if(data.posisi=="QC FG" && data.state=="START"){
                                    $('#start-button').hide();
                                    $('#stop-button').show(); 

                                    $('#qcfg-jenis_periksa_00').hide();
                                    document.getElementById("qcfg-jenis_periksa").disabled = true;
                                    $('#qcfg-jenis_periksa_0').show();
                                    document.getElementById("qcfg-jenis_periksa_1").disabled = false;
                                    $('#qcfg-jenis_periksa_1').attr('value',data.jenis_proses);

                                    // Assign Lanjutan 


                                    $('#stop-button').click(function(){ 

                                        var jenis_periksa = $('#qcfg-jenis_periksa_1').val();
                                        var snfg_komponen = $('#qcfg-snfg_komponen').val(); 

                                        $.get('index.php?r=qc-fg/lanjutan-qc-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                                                var data = $.parseJSON(data);
                                                //alert(data.sediaan);
                                                $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                                $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                                $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                                $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);

                                                // Disable Post Value Lanjutan Istirahat

                                                document.getElementById("qc-fg-lanjutan-ist_istirahat_start").disabled = true;
                                                document.getElementById("qc-fg-lanjutan-ist_istirahat_stop").disabled = true;                                                                      
                                            
                                            });

                                    });


                                    // EO Lanjutan

                                }else if(data.posisi=="QC FG" && data.state=="STOP"){
                                    $('#start-button').hide();
                                    $('#stop-button').show(); 
                                    $('#istirahat-start-button').hide(); 
                                    $('#istirahat-stop-button').hide();

                                    $('#qcfg-jenis_periksa_00').show();
                                    document.getElementById("qcfg-jenis_periksa").disabled = false;
                                    $('#qcfg-jenis_periksa_0').hide();
                                    document.getElementById("qcfg-jenis_periksa_1").disabled = true;

                                    $('#start-button').click(function(){ 

                                        $('#qcfg-jenis_periksa').change(function(){

                                            var jenis_periksa = $('#qcfg-jenis_periksa').val();
                                            var snfg_komponen = $('#qcfg-snfg_komponen').val();                                
                                            
                                                $.get('index.php?r=qc-fg/lanjutan-qc-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.sediaan);
                                                    $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                                    $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                                    $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                                    $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);

                                                    document.getElementById("qc-fg-lanjutan-ist_istirahat_start").disabled = true;
                                                    document.getElementById("qc-fg-lanjutan-ist_istirahat_stop").disabled = true;                                                                       
                                                
                                                    });
                                            });
                                        });

                                }else {

                                    $('#qcfg-jenis_periksa_00').show();
                                    document.getElementById("qcfg-jenis_periksa").disabled = false;
                                    $('#qcfg-jenis_periksa_0').hide();
                                    document.getElementById("qcfg-jenis_periksa_1").disabled = true;

                                    $('#state-create').show();
                                    $('#start-button').show();
                                    $('#stop-button').show(); 
                                    $('#istirahat-start-button').show(); 
                                    $('#istirahat-stop-button').show();
                                    alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                                }
                            });
                        
                        });

                });

                // ONCHANGE PALET , PALET HANYA BISA CHANGE KETIKA LSB TELAH DI ASSIGN

                $('#qcfg-palet_ke').change(function(){

                        // Unhide State Create


                        // Assign Variabel Lanjutan Split Batch

                        // var lanjutan_split_batch = $("#qcfg-lanjutan_split_batch").val();

                        // // Assign Variable Palet Ke

                        // var palet_ke = $('#qcfg-palet_ke').val();

                });

        }
    });
});


$(function() {

    // Hide and Disable DynamicFormWidget for Kendala Model //

        // $('#kendala-form').hide();
        //     document.getElementById("kendala-0-keterangan").disabled = true;
        //     document.getElementById("kendala-0-start").disabled = true;
        //     document.getElementById("kendala-0-stop").disabled = true;


    $('#state-create').hide();                                          //  Hide State Create
    $('#qcfg-jenis_periksa_0').hide();                                  //  Hide Jenis Proses Dropdown
    document.getElementById("qcfg-jenis_periksa_1").disabled = true;    //  Disable Jenis Proses auto-assign

    // $('#qcfg-lanjutan_split_batch').hide();
    $('#qcfg-palet_ke').hide();
    $('#lsb_palet').hide();
    $('#jadwal_tidak_ada').hide();
    $('#batchsplit').hide();
    $('#batchsplit1').hide();
    $('#qcfg-start').hide();
    $('#qcfg-stop').hide(); 
    $('#qcfg-istirahat-start').hide(); 
    $('#qcfg-istirahat-stop').hide(); 
    $('#start-button').click(function(){
            var start = "START";
            $('#qcfg-state').attr('value',start);
                    $('#qcfg-start').show();
                    $('#qcfg-stop').hide(); 
                    $('#qcfg-istirahat-start').hide(); 
                    $('#qcfg-istirahat-stop').hide();
            $('#qcfg-jenis_periksa').change(function(){
                var jenis_periksa = $('#qcfg-jenis_periksa').val();
                var snfg = $('#qcfg-snfg').val();
                var snfg_komponen = $('#qcfg-snfg_komponen').val();
                var per_snfg_komponen = document.getElementById("qcfg-snfg_komponen").disabled;
                if(per_snfg_komponen){ 


                    $.get('index.php?r=qc-fg/lanjutan-qc-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                        });

                    var a= $('#qcfg-jenis_periksa').val();
                    var b= $('#qcfg-palet_ke').val();
                    var c= $('#qcfg-lanjutan_split_batch').val();
                    var d = $('#qcfg-snfg').val();

                    if (a==null || a=="" || b==null || b=="" || b=="Select Palet" || c==null || c=="" || d==null || d=="")
                      {
                      alert("Please Fill All Required Field");
                      $('#create-button').hide(); 
                      } 
                    else {
                      $('#create-button').show();
                    }



                }else{

                    $.get('index.php?r=qc-fg/lanjutan-qc-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                        });

                    var a= $('#qcfg-jenis_periksa').val();
                    var b= $('#qcfg-palet_ke').val();
                    var c= $('#qcfg-lanjutan_split_batch').val();
                    var d = $('#qcfg-snfg_komponen').val();

                    if (a==null || a=="" || b==null || b=="" || b=="Select Palet" || c==null || c=="" || d==null || d=="")
                      {
                      alert("Please Fill All Required Field");
                      $('#create-button').hide(); 
                      }
                    else {
                        $('#create-button').show(); 
                        //alert(b);
                    }
                } 
            });                 
        });

    $('#istirahat-start-button').click(function(){
            var ist_start = "ISTIRAHAT START";
            $('#qcfg-state').attr('value',ist_start);
                    $('#qcfg-istirahat-start').show(); 
                    $('#qcfg-stop').hide(); 
                    $('#qcfg-start').hide(); 
                    $('#qcfg-istirahat-stop').hide(); 
            $('#qcfg-jenis_periksa').change(function(){
                var jenis_periksa = $('#qcfg-jenis_periksa').val();
                var snfg = $('#qcfg-snfg').val();
                var snfg_komponen = $('#qcfg-snfg_komponen').val();
                var per_snfg_komponen = document.getElementById("qcfg-snfg_komponen").disabled;
                if(per_snfg_komponen){ 
                    alert('NAMBAH LANJUTAN PER SNFG')  

                    $.get('index.php?r=qc-fg/lanjutan-qc-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                            var lanjutan = $('#qcfg-lanjutan_istirahat_start').val();
                                            $.get('index.php?r=qc-fg/lanjutan-ist-qc-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa, lanjutan : lanjutan },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.lanjutan_ist);
                                                    $('#qc-fg-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                    $('#qc-fg-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                            });
                                        });                                        
                }else{
                    alert('NAMBAH LANJUTAN PER SNFG KOMPONEN')
 
                    $.get('index.php?r=qc-fg/lanjutan-qc-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                            var lanjutan = $('#qcfg-lanjutan_istirahat_start').val();
                                            $.get('index.php?r=qc-fg/lanjutan-ist-qc-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa, lanjutan : lanjutan },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.lanjutan_ist);
                                                    $('#qc-fg-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                    $('#qc-fg-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                            }); 
                                        });
                } 
            });                     
        });

    $('#istirahat-stop-button').click(function(){
            var ist_stop = "ISTIRAHAT STOP";
            $('#qcfg-state').attr('value',ist_stop);
                    $('#qcfg-istirahat-stop').show();
                    $('#qcfg-stop').hide(); 
                    $('#qcfg-istirahat-start').hide(); 
                    $('#qcfg-start').hide(); 
            $('#qcfg-jenis_periksa').change(function(){
                var jenis_periksa = $('#qcfg-jenis_periksa').val();
                var snfg = $('#qcfg-snfg').val();
                var snfg_komponen = $('#qcfg-snfg_komponen').val();
                var per_snfg_komponen = document.getElementById("qcfg-snfg_komponen").disabled;
                if(per_snfg_komponen){    
                    $.get('index.php?r=qc-fg/lanjutan-qc-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                            var lanjutan = $('#qcfg-lanjutan_istirahat_start').val();
                                            $.get('index.php?r=qc-fg/lanjutan-ist-qc-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa, lanjutan : lanjutan },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.lanjutan_ist);
                                                    $('#qc-fg-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                    $('#qc-fg-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                            });
                                        });                                        
                }else{
                    $.get('index.php?r=qc-fg/lanjutan-qc-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                            var lanjutan = $('#qcfg-lanjutan_istirahat_start').val();
                                            $.get('index.php?r=qc-fg/lanjutan-ist-qc-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa, lanjutan : lanjutan },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.lanjutan_ist);
                                                    $('#qc-fg-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                    $('#qc-fg-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                            }); 
                                        });
                } 
            });                                                  
        });

    $('#stop-button').click(function(){
             var stop = "STOP";
             $('#qcfg-state').attr('value',stop);
                    $('#qcfg-stop').show(); 
                    $('#qcfg-start').hide(); 
                    $('#qcfg-istirahat-start').hide(); 
                    $('#qcfg-istirahat-stop').hide();    
            
            $('#qcfg-jenis_periksa').change(function(){
                var jenis_periksa = $('#qcfg-jenis_periksa').val();
                var snfg = $('#qcfg-snfg').val();
                var snfg_komponen = $('#qcfg-snfg_komponen').val();
                var per_snfg_komponen = document.getElementById("qcfg-snfg_komponen").disabled;
                if(per_snfg_komponen){    
                    $.get('index.php?r=qc-fg/lanjutan-qc-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                        });                                        
                }else{
                    $.get('index.php?r=qc-fg/lanjutan-qc-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#qcfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#qcfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                        });
                } 
            });
            $('#qcfg-qty').hide(); 
            $('#check_box_is_done').hide();
                $('#qcfg-status').change(function(){
                    var status = $('#qcfg-status').val();
                    if (status=='RELEASE'){
                        $('#check_box_is_done').show();
                        $('#qcfg-qty').show();     
                    }else{
                        $('#check_box_is_done').hide();
                        $('#qcfg-qty').hide();
                    }

                }); 
    });               
});
