<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KarboxLabelPrint */

$this->title = 'Update Karbox Label Print: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Karbox Label Prints', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="karbox-label-print-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
