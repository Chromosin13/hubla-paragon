<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\KarboxLabelPrint */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Karbox Label Prints', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="karbox-label-print-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'snfg',
            'koitem_fg',
            'nama_fg',
            'nosmb',
            'client',
            'nobatch',
            'na_number',
            'operator',
            'exp_date',
            'qty',
            'status',
        ],
    ]) ?>

</div>
