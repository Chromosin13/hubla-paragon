<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\Downtime;
use app\models\DowntimeSearch;
use kartik\date\DatePicker;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<script>
   
</script>

 <div class="box box-widget widget-user">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-black" style="background: url('../web/images/back-toska.jpg') center center; background-size:     cover; background-repeat:   no-repeat; border: 2px solid #DFDFDF;">
    <h3 class="widget-user-username" style="color:white; font-size:2.5em;"><b>PRINTER STATION - KARBOX LABEL</b></h3>
    <h5 class="widget-user-desc" style="color:white;">Input Data</h5>
  </div>
  <div class="widget-user-image">
    <img class="img-circle" src="../web/images/printer.png" alt="User Avatar">
  </div>

    <div class="box-footer">
        <div class="row">
            <div class="box-body">
                <div class="box-body">
                    <div class="karrbox-label-print-form">

                        <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($model, 'snfg')->textInput(['value'=>$snfg]) ?>

                        <!-- FG Name from Odoo Warehouse Database --->
                        <?php

                            if($fg_name==""){
                                echo $form->field($model, 'nama_fg')->textInput(['placeholder'=>'Data was not found in Odoo Database, Please fill manually']);
                            } else {
                                echo $form->field($model, 'nama_fg')->textInput(['value'=>$fg_name]);
                            }
                        ?>

                        <!-- No Batch from FRO Database --->
                        <?php
                            if($nobatch=="-"){ 
                                echo $form->field($model, 'nobatch')->textInput(['placeholder'=>'Data tidak ditemukan, silahkan isi manual']);
                            } else {
                                echo $form->field($model, 'nobatch')->textInput(['value'=>$nobatch]);
                            }
                        ?>

                        <!-- No SMB from Odoo Warehouse Database --->
                        <?php 

                            if($nosmb=="-"){
                                echo $form->field($model, 'nosmb')->textInput(['placeholder'=>'Data tidak ditemukan, silahkan isi manual']);
                            } else {
                                echo $form->field($model, 'nosmb')->textInput(['value'=>$nosmb]); 
                            }
                        ?>

                        <?php

                            if($na_number==""){
                                echo $form->field($model, 'na_number')->textInput(['placeholder'=>'Data tidak ditemukan, silahkan isi manual']);
                            } else {
                                echo $form->field($model, 'na_number')->textInput(['value'=>$na_number]); 
                            }
                        ?>


                        <?php

                            if($exp_date=="" or $exp_date=="1999-09-09"){
                                echo DatePicker::widget([
                                        'model' => $model,
                                        'form' => $form,
                                        'name' => 'exp_date',
                                        'attribute' => 'exp_date', 
                                        //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                                        'pluginOptions' => [
                                            'format' => 'yyyy-mm-dd',
                                            'todayHighlight' => true
                                            ],
                                        ]);
                            } else {
                                if ($exp_date != ""){
                                    $model->exp_date = $exp_date;
                                }
                                
                                echo DatePicker::widget([
                                        'model' => $model,
                                        'form' => $form,
                                        'name' => 'exp_date',
                                        'attribute' => 'exp_date', 
                                        // 'disabled' => !empty($model->exp_date),
                                        'options' => ['readonly' => false],
                                        'pluginOptions' => [
                                            'format' => 'yyyy-mm-dd',
                                            'todayHighlight' => true
                                            ],
                                        ]);
                            }
                        ?>

                        <?php

                            if($nama_line==""){
                                echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(FlowInputMo::find()->all()
                                    ,'nama_line','nama_line'),
                                    'options' => ['placeholder' => 'Select Line'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                            }else{
                                echo $form->field($model, 'nama_line')->textInput(['value'=>$nama_line]);
                            }
                        ?>

                        <?= $form->field($model, 'operator')->widget(Select2::classname(), [
                                            'data' => $list_operator,
                                            'options' => ['placeholder' => 'Select Operator'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]) 
                        ?>


                        <?php if ($qty!=0){ ?>
                            <?= $form->field($model, 'qty')->textInput(['value'=>$qty]) ?>
                        <?php } else { ?>
                            <?= $form->field($model, 'qty')->textInput(['placeholder' => 'Data tidak ditemukan, silahkan isi manual']) ?>
                        <?php }?>

                        <?= $form->field($model, 'barcode')->textInput(['value'=>$barcode]) ?>

                        <?= $form->field($model, 'odoo_code')->textInput(['value'=>$kode_odoo]) ?>

                        <?= $form->field($model, 'qty_request')->textInput(['type' => 'number', 'maxlength' => true]) ?>

                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>

                        <?php ActiveForm::end(); ?> 

                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
</div>
                  


<?php
$script = <<< JS

    document.getElementById('input_operator').addEventListener('input', function (e) {
        e.target.value = e.target.value.toUpperCase();
    }); 



JS;
$this->registerJs($script);
?>