<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KarboxLabelPrintSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="karbox-label-print-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'koitem_fg') ?>

    <?= $form->field($model, 'nama_fg') ?>

    <?= $form->field($model, 'nosmb') ?>

    <?php // echo $form->field($model, 'client') ?>

    <?php // echo $form->field($model, 'nobatch') ?>

    <?php // echo $form->field($model, 'na_number') ?>

    <?php // echo $form->field($model, 'operator') ?>

    <?php // echo $form->field($model, 'exp_date') ?>

    <?php // echo $form->field($model, 'qty') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
