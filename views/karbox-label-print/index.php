<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KarboxLabelPrintSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Karbox Label Prints';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="karbox-label-print-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Karbox Label Print', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'snfg',
            'koitem_fg',
            'nama_fg',
            'nosmb',
            // 'client',
            // 'nobatch',
            // 'na_number',
            // 'operator',
            // 'exp_date',
            // 'qty',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
