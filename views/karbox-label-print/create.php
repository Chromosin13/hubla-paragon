<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KarboxLabelPrint */

$this->title = 'Create Karbox Label Print';
$this->params['breadcrumbs'][] = ['label' => 'Karbox Label Prints', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="karbox-label-print-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
