<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\QctimbangfgEditable */

$this->title = 'Create Qctimbangfg Editable';
$this->params['breadcrumbs'][] = ['label' => 'Qctimbangfg Editables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qctimbangfg-editable-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
