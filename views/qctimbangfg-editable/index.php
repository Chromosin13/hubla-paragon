<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QctimbangfgEditableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Qctimbangfg Editables';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qctimbangfg-editable-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php 

        $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
                    'id',
                    'snfg',
                    'snfg_komponen',
                    'lanjutan_split_batch',
                    'qty',
                    'waktu',
                    'timestamp',
                    'lanjutan',
                    'jenis_periksa',

        ['class' => 'yii\grid\ActionColumn'],
        ];

        echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_PDF => false,
                                ExportMenu::FORMAT_HTML => false
                          ]        
    ]);?>

    <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'primary', 'heading'=>'Check Data Planner'],
            'columns'=>[
                ['class' => 'kartik\grid\ActionColumn'],
                ['class'=>'kartik\grid\SerialColumn'],
                    'id',
                    'snfg',
                    'snfg_komponen',
                    'lanjutan_split_batch',
                [
                        'class'=>'kartik\grid\EditableColumn',
                        'attribute'=>'qty',
                ],
                    'waktu',
                    'timestamp',
                    'lanjutan',
                    'jenis_periksa',
             ],
        ]);
    
    ?>
    <!-- <p>
        <?= Html::a('Create Qctimbangfg Editable', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'snfg',
            'snfg_komponen',
            'lanjutan_split_batch',
            'qty',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?> -->
</div>
