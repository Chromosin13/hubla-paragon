<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\QctimbangfgEditable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qctimbangfg-editable-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'snfg')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'snfg_komponen')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'lanjutan_split_batch')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'qty')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
