<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LossTree */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Loss Trees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loss-tree-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'snfg',
            'jumlah',
            'qty_release',
            'qty_not_hit',
            'remark',
            'level_1',
            'level_2',
            'level_3',
            'corrective_action',
            'preventive_action',
            'pic',
            'due',
        ],
    ]) ?>

</div>
