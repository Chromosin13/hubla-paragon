<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LossTreeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loss-tree-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'jumlah') ?>

    <?= $form->field($model, 'qty_release') ?>

    <?= $form->field($model, 'qty_not_hit') ?>

    <?php // echo $form->field($model, 'remark') ?>

    <?php // echo $form->field($model, 'level_1') ?>

    <?php // echo $form->field($model, 'level_2') ?>

    <?php // echo $form->field($model, 'level_3') ?>

    <?php // echo $form->field($model, 'corrective_action') ?>

    <?php // echo $form->field($model, 'preventive_action') ?>

    <?php // echo $form->field($model, 'pic') ?>

    <?php // echo $form->field($model, 'due') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
