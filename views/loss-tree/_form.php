<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\date\DatePicker;
use app\models\ListKendalaScf;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\LossTree */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loss-tree-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row" id="hide">
        <?= $form->field($model, 'snfg')->textInput(['value'=>$schedule_nh->snfg]) ?>

        <?= $form->field($model, 'jumlah')->textInput(['value'=>$schedule_nh->jumlah_pcs]) ?>

        <?= $form->field($model, 'qty_release')->textInput(['value'=>$schedule_nh->qty_release]) ?>

        <?= $form->field($model, 'qty_not_hit')->textInput(['value'=>$schedule_nh->qty_nh]) ?>
        
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'remark')->textArea(['rows'=>3]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?php 
                echo $form->field($model, 'level_1')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(ListKendalaScf::find()->all(),'level_1','level_1'),
                    'options' => ['placeholder' => 'Select Level 1'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
            ?>
        </div>
        <div class="col-md-4">
            <?php
                echo $form->field($model, 'level_2')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(ListKendalaScf::find()->all(),'level_2','level_2'),
                    'options' => ['placeholder' => 'Select Level 2'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
            ?>
        </div>
        <div class="col-md-4">
            <?php
                echo $form->field($model, 'level_3')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(ListKendalaScf::find()->all(),'level_3','level_3'),
                    'options' => ['placeholder' => 'Select Level 3'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                // echo $form->field($model, 'level_2')->textInput(); 
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'corrective_action')->textArea(['rows'=>5]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'preventive_action')->textArea(['rows'=>5]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'pic')->textInput() ?>
        </div>
        <div class="col-md-6">

            <?php 
                echo 
                DatePicker::widget([
                'model' => $model,
                'form' => $form,
                'name' => 'due',
                'attribute' => 'due', 
                //'value' => date('dd-mm-yyyy', strtotime('+0 days')),
                'options' => ['required'=>'required','placeholder' => 'Pilih Due'],
                'pluginOptions' => [
                    'format' => 'yyyy-M-dd',
                    'todayHighlight' => true
                    ]
                            ]);
                // $form->field($model, 'due')->textInput() 
            ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$script = <<< JS
$('#hide').hide();

$('#losstree-level_1').on('change',function(){
    var level_1 = $('#losstree-level_1').val();
    $("select#losstree-level_2").attr('value','');

    $.post("index.php?r=list-kendala-scf/get-level2&level_1="+level_1, function(data){
        $("select#losstree-level_2").html(data);
    });
});

$('#losstree-level_2').on('change',function(){
    var level_2 = $('#losstree-level_2').val();
    $("select#losstree-level_3").attr('value','');

    $.post("index.php?r=list-kendala-scf/get-level3&level_2="+level_2, function(data){
        $("select#losstree-level_3").html(data);
    });
});

JS;
$this->registerJs($script);
?>

