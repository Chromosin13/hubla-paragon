<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LossTree */

$this->title = 'Input Loss Tree';
// $this->params['breadcrumbs'][] = ['label' => 'Loss Trees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loss-tree-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'schedule_nh' => $schedule_nh,
    ]) ?>

</div>
