<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\LossTreeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Loss Trees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loss-tree-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Loss Tree', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'snfg',
            'jumlah',
            'qty_release',
            'qty_not_hit',
            // 'remark',
            // 'level_1',
            // 'level_2',
            // 'level_3',
            // 'corrective_action',
            // 'preventive_action',
            // 'pic',
            // 'due',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
