<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLeadtimePengolahan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Achievement Leadtime Pengolahans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-leadtime-pengolahan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nama_line',
            'nomo',
            'koitem_bulk',
            'nama_bulk',
            'nama_fg',
            'jenis_olah',
            'real_leadtime',
            'standard_leadtime',
            'nama_operator:ntext',
            'achievement',
            'tanggal_stop',
            'plan_start_olah',
            'plan_end_olah',
        ],
    ]) ?>

</div>
