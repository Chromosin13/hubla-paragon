<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLeadtimePengolahan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="achievement-leadtime-pengolahan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'nama_line')->textInput() ?>

    <?= $form->field($model, 'nomo')->textInput() ?>

    <?= $form->field($model, 'koitem_bulk')->textInput() ?>

    <?= $form->field($model, 'nama_bulk')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'jenis_olah')->textInput() ?>

    <?= $form->field($model, 'real_leadtime')->textInput() ?>

    <?= $form->field($model, 'standard_leadtime')->textInput() ?>

    <?= $form->field($model, 'nama_operator')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'achievement')->textInput() ?>

    <?= $form->field($model, 'tanggal_stop')->textInput() ?>

    <?= $form->field($model, 'plan_start_olah')->textInput() ?>

    <?= $form->field($model, 'plan_end_olah')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
