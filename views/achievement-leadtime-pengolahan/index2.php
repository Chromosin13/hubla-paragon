<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;
use app\models\AchievementLeadtimePengolahan;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AchievementLeadtimePengolahanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Achievement Leadtime Pengolahans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-leadtime-pengolahan-index">

<!--     <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--     <p>
        <?= Html::a('Create Achievement Leadtime Pengolahan', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->
  <div id="line_chart" style="width: auto;">
  <?php 

          // Graph 14 Hari

          $dataASTD= Yii::$app->db->createCommand(
                                        "select
                                            round(avg(achievement),2)::numeric as astd,
                                            tanggal_stop::date as tanggal_stop,
                                            100 as baseline
                                        from achievement_leadtime_pengolahan
                                        where tanggal_stop between current_date - interval '14 day' and current_date and achievement is not null 
                                        group by tanggal_stop::date
                                        order by tanggal_stop asc"
          )->queryAll();


           echo Highcharts::widget([
                                              'scripts' => [
                                                  'modules/exporting',
                                                  'themes/grid-light',
                                              ],
                                             'options' => [
                                                'title' => ['text' => 'Status'],
                                                'xAxis' => [
                                                   'categories' => new SeriesDataHelper($dataASTD,['tanggal_stop']),
                                                ],
                                                'yAxis' => [
                                                   'title' => ['text' => '(%) Achievement Standar Pengolahan ASTD']
                                                ],
                                                 'plotOptions' => [
                                                                  'line' => [
                                                                      'dataLabels' => [
                                                                                          'enabled' => true
                                                                                      ],
                                                                      'enableMouseTracking' => true
                                                                      ]
                                                                  ],
                                                'series' => [
                                                   ['name' => 'Proses', 'data' => 
                                                   new SeriesDataHelper($dataASTD,['astd:float']),
                                                   ],
                                                   ['name' => 'Baseline', 'data' => 
                                                   new SeriesDataHelper($dataASTD,['baseline']),
                                                   ]
                                                ]
                                             ]
                                          ]);

  ?>
  </div>

  <!-- Pie Chart -->

  <div id="pie_chart" style="width: auto;">
  <?php 

          // Graph Pie Chart

          $pieStandard = 
          Yii::$app->db->createCommand("

                                      SELECT 
                                          standard,
                                          count(standard) as count 
                                      from
                                      (select 
                                          case when achievement>=100 then 'Above Standard'
                                          else 'Below Standard' end as standard
                                      from achievement_leadtime_pengolahan
                                      where achievement is not null) a
                                      group by a.standard 
                                      "
                                        )->queryAll();

          foreach ($pieStandard as $msm){
              $standard= $msm['standard'];
              $count = $msm['count'];
              $data_msm[] = [$standard,$count];
              
          }

          foreach ($data_msm as $k=>$v )
          {
            $data_msm[$k]['name'] = $data_msm[$k]['0'];
            unset($data_msm[$k]['0']);
            $data_msm[$k]['y'] = $data_msm[$k]['1'];
            unset($data_msm[$k]['1']);
          }


          $data_msa = json_encode($data_msm, JSON_NUMERIC_CHECK);

          $decode = json_decode($data_msa,TRUE);


          echo Highcharts::widget([
                                  'options' => [
                                      'title' => ['text' => 'Rekap Standard'],
                                      'plotOptions' => [
                                          'pie' => [
                                              //'cursor' => 'pointer',
                                              'dataLabels' => [
                                                      'enabled' => true,
                                                      'format' =>'<b>{point.name}</b>: {point.percentage:.1f} %',
                                              ],
                                              'showInLegend' => true,
                                          ],
                                      ],
                                      'series' => [
                                          [ // new opening bracket
                                              'type' => 'pie',
                                              'name' => 'Elements',
                                              'data' => $decode,
                                          ] // new closing bracket
                                      ],
                                  ],
                              ]);  

          ///////

  ?>
  </div>


  <!-- Spline Line -->
  <div id="spline_chart" style="width: auto;">
  <?php

          $achievementLine = Yii::$app->db->createCommand("
                                  SELECT  nama_line,
                                          round(avg(achievement),2) as achievement,
                                          100 as baseline
                                  FROM achievement_leadtime_pengolahan
                                  where achievement is not null
                                  GROUP BY  
                                      nama_line
                                  ORDER BY nama_line
              ")->queryAll();

          echo Highcharts::widget([

                                                        'scripts' => [
                                                            'modules/exporting',
                                                            'themes/grid-light',
                                                        ],
                                                        'options' => [
                                                            'rangeSelector' => [
                                                                  //'inputEnabled' => new JsExpression('$("#container").width() > 480'),
                                                                  'selected' => 1
                                                              ],
                                                            'title' => [
                                                                'text' => 'Achievement Line',
                                                            ],
                                                            'xAxis' => [
                                                                //'type' => new JsExpression('datetime'),
                                                                'categories' => new SeriesDataHelper($achievementLine,['nama_line']),
                                                                //['PLANNER', 'PENIMBANGAN', 'PENGOLAHAN','QCBULK', 'INKJET', 'KEMAS 1','KEMAS 2', 'QCFG', 'QC TIMBANG FG'],
                                                            ],
                                                            'labels' => [
                                                                'items' => [
                                                                    [
                                                                        'html' => 'Flowboard',
                                                                        'style' => [
                                                                            'left' => '50px',
                                                                            'top' => '18px',
                                                                            'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                                                                        ],
                                                                    ],
                                                                ],
                                                            ],
                                                            'series' => [
                                                                [
                                                                    'type' => 'column',
                                                                    'name' => 'Average Achievement (%)',
                                                                    'data' => new SeriesDataHelper($achievementLine,['achievement:float']),
                                                                ],
                                                                [
                                                                    'type' => 'spline',
                                                                    'name' => 'Baseline',
                                                                    'data' => new SeriesDataHelper($achievementLine,['baseline']),
                                                                    'marker' => [
                                                                        'lineWidth' => 2,
                                                                        'lineColor' => new JsExpression('Highcharts.getOptions().colors[3]'),
                                                                        'fillColor' => 'white',
                                                                    ],
                                                                ],
                                                            ],
                                                        ]
                                  ]);
   
  ?>
  </div>

<?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
             'responsiveWrap' => false,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'success', 'heading'=>'Achievement Line Pengolahan'],
            'columns'=>[
                ['class'=>'kartik\grid\SerialColumn'],
                [
                    'attribute'=>'nama_line', 
                    'width'=>'210px',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(AchievementLeadtimePengolahan::find()->orderBy('nama_line')->asArray()->all(), 'nama_line', 'nama_line'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Line'],
                    'group'=>true,  // enable grouping
                    'groupHeader'=>function ($model, $key, $index, $widget) {
                        return [
                            'mergeColumns'=>[[1,4]], // columns to merge in summary
                            'content'=>[             // content to show in each summary cell
                                1=>'Summary (' . $model->nama_fg . ')',
                                //7=>GridView::F_SUM,
                                // 8=>GridView::F_AVG,
                                // 9=>' hrs',
                                //9=>GridView::F_SUM,
                            ],
                            'contentFormats'=>[      // content reformatting for each summary cell
                                //7=>['format'=>'number', 'decimals'=>2],
                                8=>['format'=>'number', 'decimals'=>1],
                                //8=>['append'=>' hrs'],
                                //9=>['format'=>'number', 'decimals'=>1],
                            ],
                            'contentOptions'=>[      // content html attributes for each summary cell
                                1=>['style'=>'font-variant:medium-caps;font-size: 20px'],
                                // 7=>['style'=>'text-align:right'],
                                8=>['style'=>'text-align:right'],
                                // 9=>['style'=>'text-align:right'],
                            ],
                            // html attributes for group summary row
                            'options'=>['class'=>'success','style'=>'font-weight:bold;']
                        ];
                    }
                ],
                [
                    'attribute'=>'nama_fg', 
                    'width'=>'210px',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(AchievementLeadtimePengolahan::find()->orderBy('nama_fg')->asArray()->all(), 'nama_fg', 'nama_fg'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Nama Finished Good'],
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>1,
                ],
                [

                    'attribute'=>'nomo', 
                    'width'=>'210px',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(AchievementLeadtimePengolahan::find()->orderBy('nomo')->asArray()->all(), 'nomo', 'nomo'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Nomor MO'],
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>2,
                ],
                [

                    'attribute'=>'jenis_olah', 
                    'width'=>'210px',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filter'=>ArrayHelper::map(AchievementLeadtimePengolahan::find()->orderBy('jenis_olah')->asArray()->all(), 'jenis_olah', 'jenis_olah'), 
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'filterInputOptions'=>['placeholder'=>'Jenis Olah'],
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>3,
                ],
                [
                    'attribute'=>'achievement', 
                    'label' => 'Achievement',
                    'width'=>'210px',
                    // 'group'=>true,  // enable grouping
                    // 'subGroupOf'=>3,
                    'format' => 'raw',
                    // 'headerOptions' => ['style' => 'text-align: center;'],
                    'contentOptions' => function($model){
                    // Pewarnaan untuk Posisi
                                            if($model->achievement==null)
                                            {
                                                return ['style'=>'font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                                            }            
                                            else if($model->achievement < 70)
                                            {
                                                return ['style'=>'font-weight:bold;width: 150px;font-size: 18px','class'=>'danger'];
                                            }
                                            else if($model->achievement >= 70 && $model->achievement <90)
                                            {
                                                return ['style'=>'font-weight:bold;width: 150px;font-size: 18px','class'=>'warning'];
                                            }
                                            else if($model->achievement >=90)
                                            {
                                                return ['style'=>'font-weight:bold;width: 150px;font-size: 18px','class'=>'success'];
                                            }

                    },
                    // 'contentOptions' => function($model){
                    //     if($model->achievement<65)
                    //     {
                    //         return ['class' => 'danger'];
                    //     }
                    //     else if ($model->achievement>=65 && $model->achievement<90)
                    //     {
                    //         return ['class' => 'warning'];
                    //     } 
                    //     else if ($model->achievement>=90)
                    //     {
                    //         return ['class'=> 'success'];
                    //     }
                    // },
                ],
                [
                    'attribute'=>'tanggal_stop', 
                    'width'=>'210px',
                    'group'=>true,  // enable grouping
                    'subGroupOf'=>4,
                ],
                // [
                //     'attribute'=>'snfg_komponen', 
                //     'width'=>'210px',
                //     'group'=>true,  // enable grouping
                //     'subGroupOf'=>5,
                // ],
                // 'leadtime',
                // 'std_leadtime',
                // 'status_leadtime:ntext',
                // 'plan_start_timbang',
                // 'status_ontime:ntext',
                // 'is_done',
             ],
        ]);
    
    ?>

<!--     <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nama_line',
            'nomo',
            'koitem_bulk',
            'nama_bulk',
            'nama_fg',
            'jenis_olah',
            'real_leadtime',
            'standard_leadtime',
            'nama_operator:ntext',
            'achievement',
            'tanggal_stop',
            'plan_start_olah',
            'plan_end_olah',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?> -->
</div>
