<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AchievementLeadtimePengolahan */

$this->title = 'Create Achievement Leadtime Pengolahan';
$this->params['breadcrumbs'][] = ['label' => 'Achievement Leadtime Pengolahans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-leadtime-pengolahan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
