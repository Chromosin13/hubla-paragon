<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;
use app\models\AchievementLeadtimePengolahan;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AchievementLeadtimePengolahanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Achievement Leadtime Pengolahans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-leadtime-pengolahan-index">

<!--     <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Achievement Leadtime Pengolahan', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->

      <div id="line_chart" style="width: auto;">
  <?php 

          // Graph 14 Hari

          $dataASTD= Yii::$app->db->createCommand(
                                        "select
                                            round(avg(achievement),2)::numeric as astd,
                                            tanggal_stop::date as tanggal_stop,
                                            100 as baseline
                                        from achievement_leadtime_pengolahan
                                        where tanggal_stop between current_date - interval '14 day' and current_date and achievement is not null 
                                        group by tanggal_stop::date
                                        order by tanggal_stop asc"
          )->queryAll();


           echo Highcharts::widget([
                                              'scripts' => [
                                                  'modules/exporting',
                                                  'themes/grid-light',
                                              ],
                                             'options' => [
                                                'title' => ['text' => 'Status'],
                                                'xAxis' => [
                                                   'categories' => new SeriesDataHelper($dataASTD,['tanggal_stop']),
                                                ],
                                                'yAxis' => [
                                                   'title' => ['text' => '(%) Achievement Standar Pengolahan ASTD']
                                                ],
                                                 'plotOptions' => [
                                                                  'line' => [
                                                                      'dataLabels' => [
                                                                                          'enabled' => true
                                                                                      ],
                                                                      'enableMouseTracking' => true
                                                                      ]
                                                                  ],
                                                'series' => [
                                                   ['name' => 'Proses', 'data' => 
                                                   new SeriesDataHelper($dataASTD,['astd:float']),
                                                   ],
                                                   ['name' => 'Baseline', 'data' => 
                                                   new SeriesDataHelper($dataASTD,['baseline']),
                                                   ]
                                                ]
                                             ]
                                          ]);

  ?>
  </div>

  <!-- Pie Chart -->

  <div id="pie_chart" style="width: auto;">
  <?php 

          // Graph Pie Chart

          $pieStandard = 
          Yii::$app->db->createCommand("

                                      SELECT 
                                          standard,
                                          count(standard) as count 
                                      from
                                      (select 
                                          case when achievement>=100 then 'Above Standard'
                                          else 'Below Standard' end as standard
                                      from achievement_leadtime_pengolahan
                                      where achievement is not null) a
                                      group by a.standard 
                                      "
                                        )->queryAll();

          foreach ($pieStandard as $msm){
              $standard= $msm['standard'];
              $count = $msm['count'];
              $data_msm[] = [$standard,$count];
              
          }

          foreach ($data_msm as $k=>$v )
          {
            $data_msm[$k]['name'] = $data_msm[$k]['0'];
            unset($data_msm[$k]['0']);
            $data_msm[$k]['y'] = $data_msm[$k]['1'];
            unset($data_msm[$k]['1']);
          }


          $data_msa = json_encode($data_msm, JSON_NUMERIC_CHECK);

          $decode = json_decode($data_msa,TRUE);


          echo Highcharts::widget([
                                  'options' => [
                                      'title' => ['text' => 'Rekap Standard'],
                                      'plotOptions' => [
                                          'pie' => [
                                              //'cursor' => 'pointer',
                                              'dataLabels' => [
                                                      'enabled' => true,
                                                      'format' =>'<b>{point.name}</b>: {point.percentage:.1f} %',
                                              ],
                                              'showInLegend' => true,
                                          ],
                                      ],
                                      'series' => [
                                          [ // new opening bracket
                                              'type' => 'pie',
                                              'name' => 'Elements',
                                              'data' => $decode,
                                          ] // new closing bracket
                                      ],
                                  ],
                              ]);  

          ///////

  ?>
  </div>


  <!-- Spline Line -->
  <div id="spline_chart" style="width: auto;">
  <?php

          $achievementLine = Yii::$app->db->createCommand("
                                  SELECT  nama_line,
                                          round(avg(achievement),2) as achievement,
                                          100 as baseline
                                  FROM achievement_leadtime_pengolahan
                                  where achievement is not null
                                  GROUP BY  
                                      nama_line
                                  ORDER BY nama_line
              ")->queryAll();

          echo Highcharts::widget([

                                                        'scripts' => [
                                                            'modules/exporting',
                                                            'themes/grid-light',
                                                        ],
                                                        'options' => [
                                                            'rangeSelector' => [
                                                                  //'inputEnabled' => new JsExpression('$("#container").width() > 480'),
                                                                  'selected' => 1
                                                              ],
                                                            'title' => [
                                                                'text' => 'Achievement Line',
                                                            ],
                                                            'xAxis' => [
                                                                //'type' => new JsExpression('datetime'),
                                                                'categories' => new SeriesDataHelper($achievementLine,['nama_line']),
                                                                //['PLANNER', 'PENIMBANGAN', 'PENGOLAHAN','QCBULK', 'INKJET', 'KEMAS 1','KEMAS 2', 'QCFG', 'QC TIMBANG FG'],
                                                            ],
                                                            'labels' => [
                                                                'items' => [
                                                                    [
                                                                        'html' => 'Flowboard',
                                                                        'style' => [
                                                                            'left' => '50px',
                                                                            'top' => '18px',
                                                                            'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                                                                        ],
                                                                    ],
                                                                ],
                                                            ],
                                                            'series' => [
                                                                [
                                                                    'type' => 'column',
                                                                    'name' => 'Average Achievement (%)',
                                                                    'data' => new SeriesDataHelper($achievementLine,['achievement:float']),
                                                                ],
                                                                [
                                                                    'type' => 'spline',
                                                                    'name' => 'Baseline',
                                                                    'data' => new SeriesDataHelper($achievementLine,['baseline']),
                                                                    'marker' => [
                                                                        'lineWidth' => 2,
                                                                        'lineColor' => new JsExpression('Highcharts.getOptions().colors[3]'),
                                                                        'fillColor' => 'white',
                                                                    ],
                                                                ],
                                                            ],
                                                        ]
                                  ]);
   
  ?>
  </div>
</div>


<?php
$script = <<< JS


$(function () {

    // $('#offcanvasTrigger').click();

    var counter = 0,
        divs = $('#line_chart, #pie_chart, #spline_chart');

    function showDiv () {
        divs.hide() // hide all divs
            .filter(function (index) { return index == counter % 3; }) // figure out correct div to show
            .fadeIn('fast'); // and show it

        counter++;
    }; // function to loop through divs and show correct div

    showDiv(); // show first div    

    setInterval(function () {
        showDiv('slow'); // show next div
    }, 10 * 1000); // do this every 10 seconds    

});


JS;
$this->registerJs($script);
?>
