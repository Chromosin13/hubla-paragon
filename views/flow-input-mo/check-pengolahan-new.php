<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>


                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-blue-gradient">
                      <h3 class="widget-user-username"><b>Pengolahan Tablet</b></h3>
                      <h5 class="widget-user-desc">Input Data</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/proses_icons/weighing.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">
                              <!-- <div class="zoom">
                                <?=Html::a('SCAN WITH QR', ['flow-input-mo/check-penimbangan-tab'], ['class' => 'btn btn-success btn-block']);?>
                              </div> -->
                              <!-- <br> -->
                                <div class="flow-input-mo-form">


                                    <div class="form-group field-flowinputmo-nomo has-success">
                                    <label class="control-label" for="flowinputmo-nomo">Nomor MO</label>
                                    <input type="text" id="flowinputmo-nomo" class="form-control" name="FlowInputMo[nomo]" aria-invalid="false">
                                    <br>
                                    <!-- <button id="checkbtn" class="btn btn-info btn-block" style="width:50%; margin:0 auto;">CHECK</button> -->

                                </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>

                  <!-- List Data -->
                  <h3 style="color:white;background-color:#76D7C4; font-size: 18px;text-align: center; padding: 7px 10px; margin-top: 0;" class="widget-user-username"><b>IN PROGRESS</b></h3>
                  <div class="box-body">
                               



                                     <?= GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'columns' => [
                                            
                                            'nomo',
                                            'lanjutan',
                                            'datetime_start',
                                            'datetime_stop',
                                            'nama_line',
                                            'jenis_penimbangan',
                                            'nama_operator',
                                        ],
                                    ]); ?>

                          </div>

<?php
$script = <<< JS

// AutoFocus Nomo Field

document.getElementById("flowinputmo-nomo").focus();


$('#checkbtn').click(function(){  

    var nomo = $('#flowinputmo-nomo').val().trim();
    // console.log("hai");
    if(nomo){
      $.get("index.php?r=flow-input-mo/check-status-nomo&nomo="+nomo, function (data){
        var data = JSON.parse(data);
        if (data.status == 'generate') {
          $.get("index.php?r=flow-input-mo/check-status-task&nomo="+nomo, function (data2){
            if (data2 == 1) {
              window.location = "index.php?r=flow-input-mo/create-pengolahan-new&nomo="+nomo;
            } else if (data2 == 2){
              alert("Proses pengolahan untuk NOMO ini sudah selesai.");

            } else {
              alert("Belum ada task untuk nomo ini.");
            }
          });
        } else {
          window.location = "index.php?r=flow-input-mo/task&nomo="+nomo+"&siklus="+data.siklus;
        }
      });
    } else {
      alert("Isi field MO terlebih dahulu!.");
    }
});

// $("#flowinputmo-nomo").on('keyup', function (e) {
//   if (e.key === 'Enter' || e.keyCode === 13) {
//     var nomo = $('#flowinputmo-nomo').val().trim();
//     // console.log("hai");
//     if(nomo){
//       $.get("index.php?r=flow-input-mo/check-status-nomo&nomo="+nomo, function (data){
//         // alert(data);
//         if (data == 0 || data == 1) {
//           $.get("index.php?r=flow-input-mo/check-status-task&nomo="+nomo, function (data2){
//             if (data2 == 1) {
//               window.location = "index.php?r=flow-input-mo/create-pengolahan-new&nomo="+nomo;
//             } else {
//               alert("Belum ada task untuk nomo ini karena penimbangan belum scan stop.");
//             }
//           });
//         } else if (data == 2 ){
//           alert("Proses pengolahan untuk NOMO ini sudah selesai.");
//         }
//       });
//     } else {
//       alert("Isi field MO terlebih dahulu!.");
//     }
//   }
// });

$("#flowinputmo-nomo").on('change', function (e) {
  var nomo = $('#flowinputmo-nomo').val().trim();
  // console.log("hai");
  // console.log(nomo);
  if(nomo){
    $.get("index.php?r=flow-input-mo/check-status-nomo&nomo="+nomo, function (data){
      // alert(data);
      var data = JSON.parse(data);
      if (data.status == 'generate') {
        $.get("index.php?r=flow-input-mo/check-status-task&nomo="+nomo, function (data2){
          if (data2 == 1) {
            window.location = "index.php?r=flow-input-mo/create-pengolahan-new&nomo="+nomo;
          } else if (data2 == 2){
            alert("Proses pengolahan untuk NOMO ini sudah selesai.");

          } else {
            alert("Belum ada task untuk nomo ini.");
          }
        });
      } else {
        window.location = "index.php?r=flow-input-mo/task&nomo="+nomo+"&siklus="+data.siklus;
      }
    });
  } else {
    alert("Isi field MO terlebih dahulu!.");
  }
});



JS;
$this->registerJs($script);
?>