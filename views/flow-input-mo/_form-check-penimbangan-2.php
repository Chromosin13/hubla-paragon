<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>


                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-blue-gradient">
                      <h3 class="widget-user-username"><b>Penimbangan</b></h3>
                      <h5 class="widget-user-desc">Input Data</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/proses_icons/weighing.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">
                                <div class="flow-input-mo-form">


                                    <div class="form-group field-flowinputmo-nomo has-success">
                                    <label class="control-label" for="flowinputmo-nomo">Nomor MO</label>
                                    <input type="text" id="flowinputmo-nomo" class="form-control" name="FlowInputMo[nomo]" aria-invalid="false">

                                </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>

                  <!-- List Data -->
                  <h3 style="color:white;background-color:#76D7C4; font-size: 18px;text-align: center; padding: 7px 10px; margin-top: 0; class="widget-user-username"><b>IN PROGRESS</b></h3>
                  <div class="box-body">
                               



                                     <?= GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'columns' => [
                                            
                                            'nomo',
                                            'lanjutan',
                                            'datetime_start',
                                            'datetime_stop',
                                            'nama_line',
                                            'jenis_penimbangan',
                                            'nama_operator',
                                        ],
                                    ]); ?>

                          </div>

<?php
$script = <<< JS

// AutoFocus Nomo Field

document.getElementById("flowinputmo-nomo").focus();

// Check SNFG Komponen Validity

$('#flowinputmo-nomo').change(function(){  

    var nomo = $('#flowinputmo-nomo').val().trim();
    
    if(nomo){
      window.location = "index.php?r=flow-input-mo/create-penimbangan-2&nomo="+nomo;
    }else{
      alert('Nomor Jadwal kosong!');
    }
    
});



JS;
$this->registerJs($script);
?>