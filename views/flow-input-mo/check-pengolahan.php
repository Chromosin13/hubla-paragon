<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */

?>
<div class="flow-input-mo-create">

    <?= $this->render('_form-check-pengolahan', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'dataProvider2' => $dataProvider2,
    ]) ?>

</div>
