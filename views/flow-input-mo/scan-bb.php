<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowAnalyst;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- CSS For Video Selector -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="ZXing for JS">

  <title>ZXing TypeScript | Decoding from camera stream</title>

  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">

<style>
.column {
  float: left;
  width: 100%;
}

.accordion {
  
  color: white;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  transition: 0.4s;
}

.active, .accordion:hover {
  background-color: #ccc;
}

.accordion:after {
  content: '\002B';
  color: white;
  font-weight: bold;
  float: right;
  margin-left: 5px;
}

.active:after {
  content: "\2212";
  color: white;
}

.panel {
  padding: 0 18px;
  background-color: white;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
}

.header{
  padding :1px;
  text-align: center;
}

</style>
</head>

<body oncopy="return false" oncut="return false" onpaste="return false">
<!-- Body -->
<div class="box box-widget widget-user">
  <!-- Add the bg color to the header using any of the bg-* classes -->
  <div class="widget-user-header bg-red-gradient" style="text-align: center; display: block; margin-left: auto; margin-right: auto;">
    <!-- <h3 class="widget-user-username"><b>Scan Kode Bahan Baku</b></h3>
    <h5 class="widget-user-desc">QR Code</h5> -->
    <div class="row">
    <div class="column col-sm-6 center" >
      <h4 class="widget-user-username" style="margin-bottom:0;font-weight:bolder;font-size:1.9em;">Operator</h4>
      <h2 class="widget-user-desc" style="margin-bottom:0;font-size:1.8em"><b><?php echo $operator;?></b></h2>
    </div>
    <div class="column col-sm-6 center" >
      <h4 class="widget-user-username" style="margin-bottom:0;font-size:1.9em;">Nama Bahan Baku</h4>
      <h2 class="widget-user-desc" style="margin-bottom:0;font-size:1.8em"><b><?php echo $task->nama_bb;?></b></h2>
    </div>
  </div>
</div>
  <!-- <div class="widget-user-image">
    <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
  </div> -->
  <div class="box-footer" style="margin-top: 0px; padding-top:20px;">
    <div class="row" style="margin-bottom :0px; margin-top :0px; padding-top:0px; justify-content: center;">
      <!-- <div class="box-body">
          <div class="box-body">

            <div id="sourceSelectPanel" style="display:none">
              <label for="sourceSelect" style="display:none">Switch Camera</label>
              <select id="sourceSelect" style="max-width:400px; display:none;">x
              </select>
            </div>

            <div >
              <video id="video" width="330" height="300" style="border: 0.5px solid gray; display: block; margin-left: auto; margin-right: auto;"></video>
            </div>
              
          </div>
      </div>
    </div> -->
    <br>
    <br>
    <div style="margin-top:   0px;">
      <div class="col-md-12 column" style="text-align:center;">
        <label for="kode">Input ID Bahan Baku</label>
        <input type="text" name="kode" id="kode">
        <!-- <button onclick="check()" style="width:50%; display: block; margin-left: auto; margin-right: auto;">Check</button> -->
      </div>
    </div>
  </div>
  
</div>
<br>
<br>


<button class="accordion bg-blue-gradient" style="padding-top:0px;">Input Manual (Klik Jika tidak bisa scan ID BB)</button>
<div class="panel">
  <div class="col-md-12 column" style="margin-left:0px; margin-top:0px;">
    <label for="kode_internal">Kode Internal</label>
    <input type="text" name="kode_internal" id="kode_internal" >
    <label for="kode_olah">Kode Olah</label>
    <input type="text" name="kode_olah" id="kode_olah" >
    <label for="qty">Qty</label>
    <input style="width:350px;" type="text" name="qty" id="qty" >  <?php echo $task->uom ?>
  </div>
  <button onclick="check()" style="width:50%; display: block; margin-left: auto; margin-right: auto;">Check</button>
</div>
</body>

  <!-- <button onclick="inputmanual()" style="width:50%; display: block; margin-left: auto; margin-right: auto;" class="btn btn block">Input Manual BB</button> -->



<!-- Javascript -->
<!-- Calling ZXing API CDN -->
<script type="text/javascript" src="zxing.js"></script>
<!-- Code -->
<script type="text/javascript">



/************************************************
 * Accordion
 *
************************************************/ 
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}


// function check(){
//   var id_bb_manual = document.getElementById("kode").value;
//   var wo = '<?php echo $wo; ?>';
//   var id = '<?php echo $id; ?>';
//   var id_bb = '<?php echo $id_bb; ?>';
//   var siklus = '<?php echo $op->siklus; ?>';
//   var nomo = '<?php echo $nomo; ?>';
//   var operator = '<?php echo $operator; ?>';
//   // console.log(nomo);

//   if ((id_bb_manual != "")){
//     // $.get('index.php?r=flow-input-mo/check-bb',{ wo : wo, kode_bb : kode },function(data)
//     // {
//     //   if(data==1){}
//     // }  
//     if(id_bb_manual == id_bb)
//     {
//       $.get('index.php?r=flow-input-mo/update-status',{ wo : wo, log : "manual", pic : operator },function(data){
//         if(data==1){
//           window.location = "index.php?r=flow-input-mo/task&nomo="+nomo+"&siklus="+siklus;
//         } else {
//           alert('Tidak berhasil update log status');
//         }
//       });           
//     } else {
//       window.alert("Kode bahan baku tidak sesuai.");
//     }
//   } else {
//     window.alert("Kode bahan belum diisi!");
//   }
// } 

function check(){
    var kode_internal = document.getElementById("kode_internal").value;
    var kode_olah = document.getElementById("kode_olah").value;
    var qty = document.getElementById("qty").value;
    var nomo = '<?php echo $nomo; ?>';
    var siklus = '<?php echo $op->siklus; ?>';
    var pic = '<?php echo $operator; ?>';
    var siklus = '<?php echo $op->siklus; ?>';
    var wo = '<?php echo $wo; ?>';
    var id_bb = '<?php echo $task->id_bb; ?>';
    $.getJSON("index.php?r=flow-input-mo/check-bb-manual&nomo="+nomo+"&siklus="+siklus+"&kode_internal="+kode_internal+"&kode_olah="+kode_olah+"&qty="+qty, function (data){
            if (data.status=='exist') {
              if(data.id == wo){
                $.get("index.php?r=flow-input-mo/update-status-bb-manual&id="+data.id+"&log=manual-detail&pic="+pic, function (data){
                    if(data=1){
                        window.location = "index.php?r=flow-input-mo/task&nomo="+nomo+"&siklus="+siklus;
                    }else{
                        window.alert('Gagal update status bahan baku');

                    }
                }); 
              }else{
                window.alert("Kode bahan baku tidak sesuai.");

              }
               
            } else {
                window.alert('Bahan Baku tidak ditemukan dalam List Task');
            }
          });
}

  document.getElementById("kode").focus();

  </script>


<?php
$script = <<< JS

$('#kode').on("change",function() {
  var kode = document.getElementById("kode").value;
  var wo = '$wo';
  var id = '$id';
  var id_bb = '$id_bb';
  var nomo = '$nomo';
  var siklus = '$op->siklus';
  var operator = '$operator';
  console.log(id_bb);

  // if ((kode != "")){
  //   $.get('index.php?r=flow-input-mo/check-bb',{ wo : wo, kode_bb : kode },function(data)
  //   {
  //     if(data==1){                    
  //        $.get('index.php?r=flow-input-mo/update-status',{ wo : wo, log : "scan/manual", pic : operator },function(data){
  //           if(data==1){
  //             window.location = "index.php?r=flow-input-mo/task&nomo="+nomo;
  //           } else {
  //             alert('Tidak berhasil update log status');
  //           }
  //        });           
  //     } else {
  //       window.alert("Kode bahan baku tidak sesuai.");
  //     }
  //   });
  // } else {
  //   window.alert("Kode bahan belum diisi!");
  // }

  if ((kode != "")){
    // $.get('index.php?r=flow-input-mo/check-bb',{ wo : wo, kode_bb : kode },function(data)
    // {
    //   if(data==1){}
    // }  
    if(kode == id_bb)
    {
      $.get('index.php?r=flow-input-mo/update-status',{ wo : wo, log : "scan", pic : operator },function(data){
        if(data==1){
          window.location = "index.php?r=flow-input-mo/task&nomo="+nomo+"&siklus="+siklus;
        } else {
          alert('Tidak berhasil update log status');
        }
      });           
    } else {
      window.alert("Kode bahan baku tidak sesuai.");
    }
  } else {
    window.alert("Kode bahan belum diisi!");
  }
});

JS;
$this->registerJs($script);
?>