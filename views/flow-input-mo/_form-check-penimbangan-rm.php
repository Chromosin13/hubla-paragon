<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>


                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-blue-gradient">
                      <h3 class="widget-user-username"><b>Penimbangan Inline</b></h3>
                      <h5 class="widget-user-desc">Input Data</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/proses_icons/weighing.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">
                              <div class="zoom">
                                <?=Html::a('SCAN WITH QR', Yii::getAlias('@dnsUrl').'flow-input-mo/check-penimbangan-rm-qr', ['class' => 'btn btn-success btn-block']);?>
                              </div>
                              <p></p>
                              <div class="flow-input-mo-form">
                                  <div class="form-group field-flowinputmo-nomo has-success">
                                  <label class="control-label" for="flowinputmo-nomo">Nomor MO</label>
                                  <input type="text" id="flowinputmo-nomo" class="form-control" name="FlowInputMo[nomo]" aria-invalid="false">

                              </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>

                  <!-- List Data -->
                  <h3 style="color:white;background-color:#76D7C4; font-size: 18px;text-align: center; padding: 7px 10px; margin-top: 0;" class="widget-user-username"><b>IN PROGRESS</b></h3>
                  <div class="box-body">
                               



                                     <?= GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'filterModel' => $searchModel,
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn'],
                                            'nomo',
                                            'lanjutan',
                                            'datetime_start',
                                            'datetime_stop',
                                            'nama_line',
                                            'jenis_penimbangan',
                                            'nama_operator',
                                        ],
                                    ]); ?>

                          </div>

<?php
$script = <<< JS

// AutoFocus Nomo Field

document.getElementById("flowinputmo-nomo").focus();


$('#flowinputmo-nomo').change(function(){  

    var nomo = $('#flowinputmo-nomo').val().trim();

    //console.log(nomo);
    
    if(nomo){

      $.getJSON('index.php?r=scm-planner/check-nomo',{ nomo : nomo },function(data){
        var data = data;
        //console.log(data.id);

        if(data.id>=1){
            //window.alert(result.text);           
              // window.location = "index.php?r=flow-input-mo/create-penimbangan-rm&nomo="+nomo;

          $.getJSON('index.php?r=flow-input-mo/check-page',{ nomo : nomo },function(tmp2){

            var data2 = tmp2;
            console.log(data2);
            
            if(data2.page=="no-task"){
              window.alert("Formula belum di verifikasi oleh tim Process Engineer (PE).");
            } else if(data2.page=="create"){
              console.log('create');
              window.location = "index.php?r=flow-input-mo/create-penimbangan-rm&nomo="+nomo;
            } else if (data2.page == "bom-confirmation"){
              window.location = "index.php?r=flow-input-mo/check-bom-rm&id="+data2.id;
            } else if (data2.page == "task"){
              window.location = "index.php?r=log-penimbangan-rm/task&id="+data2.id;
            } else if (data2.page == "stay"){
              window.alert("Kapten belum melakukan scan, mohon tunggu.");
              window.location = "index.php?r=flow-input-mo/check-penimbangan-rm";
            } else if (data2.page == "split-not-active"){
              window.alert("Jadwal ini sedang berjalan di line "+data2.id+", dan split line belum aktif, silahkan aktifkan split line terlebih dahulu.");
              window.location = "index.php?r=flow-input-mo/check-penimbangan-rm";
            } else if (data2.page == "done"){
              window.alert("Jadwal sudah selesai");
            } else {
              window.alert("Kapten belum melakukan Scan");
            }
          });
        } else {
           alert('No. MO Tidak Ditemukan di Database; Silahkan mengontak Planner/Leader');
        }

      });
      //window.location = "index.php?r=flow-input-mo/create-penimbangan-rm&nomo="+nomo;
      //alert('LOH!');
    }else{
      alert('Mohon isi No.MO terlebih dahulu!');
    }
    
});


JS;
$this->registerJs($script);
?>