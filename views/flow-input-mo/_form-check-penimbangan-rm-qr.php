<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowAnalyst;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- CSS For Video Selector -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="ZXing for JS">

  <title>ZXing TypeScript | Decoding from camera stream</title>

  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">
</head>

 

<!-- Body -->
<div class="box box-widget widget-user">
  <!-- Add the bg color to the header using any of the bg-* classes -->
  <div class="widget-user-header bg-green-gradient">
    <h3 class="widget-user-username"><b>Scan Jadwal Penimbangan Raw Material</b></h3>
    <h5 class="widget-user-desc">QR Code</h5>
  </div>
  <div class="widget-user-image">
    <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
  </div>
  <div class="box-footer">
    <div class="row">
      <div class="box-body">
          <div class="box-body">

            <div id="sourceSelectPanel" style="display:none">
              <label for="sourceSelect" style="display:none">Switch Camera</label>
              <select id="sourceSelect" style="max-width:400px; display: none;">x
              </select>
            </div>

            <div>
              <label for="video" style="text-align: center;"><h3><b>Scan QR code dari No.MO</b></h3></label>
              <video id="video" width="330" height="300" style="border: 0.5px solid gray; display: block; margin-left: auto; margin-right: auto;"></video>
            </div>
              
          </div>
      </div>
    </div>
    <!-- /.row -->
  </div>
</div>



<!-- Javascript -->
  <!-- Calling ZXing API CDN -->
  <script type="text/javascript" src="zxing.js"></script>
  <!-- Code -->
  <script type="text/javascript">

      let selectedDeviceId;
      const codeReader = new ZXing.BrowserMultiFormatReader()
      console.log('ZXing code reader initialized')
      codeReader.getVideoInputDevices()
        .then((videoInputDevices) => {
          const sourceSelect = document.getElementById('sourceSelect')
          // selectedDeviceId = videoInputDevices[1].deviceId
          if (videoInputDevices.length >= 1) {
            videoInputDevices.forEach((element) => {
              const sourceOption = document.createElement('option')
              sourceOption.text = element.label
              sourceOption.value = element.deviceId
              sourceSelect.appendChild(sourceOption)
            })

            // When Changing Camera, Reset Scanner and Execute Canvas
            sourceSelect.onchange = () => {
              selectedDeviceId = sourceSelect.value;

              codeReader.reset()
              codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
                
                if (result) { 
                  //window.alert(result.text);
                  if (!result.text.includes("MO-")){
                    window.alert('Salah scan! Yang ter-scan bukan NOMO. Silahkan posisikan ulang scanner.');
                  } else {
                    $.getJSON('index.php?r=scm-planner/check-nomo',{ nomo : result.text },function(tmp){
                          var data = tmp;

                          if(data.id>=1){
                              //window.alert(result.text);           
                             $.getJSON('index.php?r=flow-input-mo/check-page',{ nomo : result.text },function(tmp2){

                                var data2 = tmp2;
                                //window.alert(data);
                                // if(data2.page=="create"){
                                //   window.location = "index.php?r=flow-input-mo/create-penimbangan-rm&nomo="+result.text;
                                // } else if (data2.page == "bom-confirmation"){
                                //   window.location = "index.php?r=flow-input-mo/check-bom-rm&id="+data2.id;
                                // } else if (data2.page == "task-kapten"){
                                //   window.location = "index.php?r=log-penimbangan-rm/task&id="+data2.id;
                                // } else if (data2.page == "task-operator"){
                                //   window.location = "index.php?r=log-penimbangan-rm/task&id="+data2.id;
                                // } else if (data2.page == "stay"){
                                //   window.location = "index.php?r=flow-input-mo/check-penimbangan-rm-qr";
                                // } else if (data2.page == "done"){
                                //   window.alert("Jadwal sudah selesai");
                                // } else {
                                //   window.alert("Kapten belum melakukan Scan");
                                // }

                                if(data2.page=="no-task"){
                                  window.alert("Formula belum di verifikasi oleh tim Process Engineer (PE).");
                                } else if(data2.page=="create"){
                                  window.location = "index.php?r=flow-input-mo/create-penimbangan-rm&nomo="+result.text;
                                } else if (data2.page == "bom-confirmation"){
                                  window.location = "index.php?r=flow-input-mo/check-bom-rm&id="+data2.id;
                                } else if (data2.page == "task"){
                                  window.location = "index.php?r=log-penimbangan-rm/task&id="+data2.id;
                                } else if (data2.page == "stay"){
                                  window.alert("Kapten belum melakukan scan, mohon tunggu.");
                                  window.location = "index.php?r=flow-input-mo/check-penimbangan-rm";
                                } else if (data2.page == "done"){
                                  window.alert("Jadwal sudah selesai");
                                } else {
                                  window.alert("Kapten belum melakukan Scan");
                                }
                             });
                            
                          }
                           else{

                             alert('No. MO Tidak Ditemukan di Database; Silahkan mengontak Planner/Leader');
                          }

                    });
                  }
                }
                if (err && !(err instanceof ZXing.NotFoundException)) {
                  console.error(err)
                  document.getElementById('result').textContent = err
                }

              }) // on scanned 
            }; // onchange

            const sourceSelectPanel = document.getElementById('sourceSelectPanel')
            sourceSelectPanel.style.display = 'block'
          }

          // Initialize Execute Canvas when Page first loaded 
          codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
            if (result) {
              //window.alert(result.text);
              if (!result.text.includes("MO-")){
                window.alert('Salah scan! Yang ter-scan bukan NOMO. Silahkan posisikan ulang scanner.');
              } else {
                $.getJSON('index.php?r=scm-planner/check-nomo',{ nomo : result.text },function(tmp){
                      var data = tmp;

                      if(data.id>=1){
                                              
                         // location.href='http://10.3.5.102/flowreport_gustav/web/index.php?r=flow-input-mo/create-penimbangan-rm&nomo='+result.text;
                         $.getJSON('index.php?r=flow-input-mo/check-page',{ nomo : result.text },function(tmp2){

                            var data2 = tmp2;
                            //window.alert(data2);
                            if(data2.page=="no-task"){
                              window.alert("Formula belum di verifikasi oleh tim Process Engineer (PE).");
                            } else if(data2.page=="create"){
                              window.location = "index.php?r=flow-input-mo/create-penimbangan-rm&nomo="+result.text;
                            } else if (data2.page == "bom-confirmation"){
                              window.location = "index.php?r=flow-input-mo/check-bom-rm&id="+data2.id;
                            } else if (data2.page == "task"){
                              window.location = "index.php?r=log-penimbangan-rm/task&id="+data2.id;
                            } else if (data2.page == "stay"){
                              window.alert("Kapten belum melakukan scan, mohon tunggu.");
                              window.location = "index.php?r=flow-input-mo/check-penimbangan-rm";
                            } else if (data2.page == "done"){
                              window.alert("Jadwal sudah selesai");
                            } else {
                              window.alert("Kapten belum melakukan Scan");
                            }
                         });
                        
                      }
                       else{

                         alert('No. MO Tidak Ditemukan di Database; Silahkan mengontak Planner/Leader');
                      }

                });
              }
            }
            if (err && !(err instanceof ZXing.NotFoundException)) {
              console.error(err)
              document.getElementById('result').textContent = err
            }
          }) // on scanned


        })
        .catch((err) => {
          console.error(err)
        })
  </script>