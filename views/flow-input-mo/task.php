<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;
use yii\helpers\Json;
use yii\widgets\Pjax;


?>
<style>
    .bawahan {
       position: fixed;
       border-top:2px solid #D3D3D3;
       left: 0;
       bottom: 0;
       width: 100%;
       padding:15px 0px;
       padding-left: 50px;
       background-color: white;
       color: white;
       text-align: center;
    }
    .inner p { 
        font-size: 18px;
        text-align: center;
    }
    p {
        margin-block-end : 0px;
    }
    .small-box-footer{
        font-size: 16px;
    }
</style>

<script>
    var nomo = '<?php echo $nomo; ?>';
    var fim_id = '$fim_id';
</script>

  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="small-box btn-primary">
        <div class="inner">
         <p style="font-size:22px;"><?php echo $sp->nama_bulk ?></p>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-6 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <!-- <h3>150</h3> -->

          <p><?php echo $nomo; ?></p>
        </div>
        <a href="#" class="small-box-footer">
          NOMO
        </a>
      </div>
    </div>
    <!-- ./col -->

    <div class="col-lg-6 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <!-- <h3>150</h3> -->

          <p><?php echo $sp->snfg; ?></p>
        </div>
        <a href="#" class="small-box-footer">
          SNFG
        </a>
      </div>
    </div>
    <!-- ./col -->
  </div>

  <div class="row">
    <div class="col-lg-3 col-xs-3">
      <!-- small box -->
      <div class="small-box bg-teal">
        <div class="inner">
          <!-- <h3>150</h3> -->

          <p><?php echo $operator; ?></p>
        </div>
        <a href="#" class="small-box-footer">
          OPERATOR
        </a>
      </div>
    </div>
    <!-- ./col -->

    <div class="col-lg-3 col-xs-3">
      <!-- small box -->
      <div class="small-box bg-teal">
        <div class="inner">
          <!-- <h3>150</h3> -->

          <p><?php if ($siklus == 0 ){echo 'ALL';}else{ echo $siklus;} ?></p>
        </div>
        <a href="#" class="small-box-footer">
          SIKLUS
        </a>
      </div>
    </div>
    <!-- ./col -->

    <div class="col-lg-3 col-xs-3">
      <!-- small box -->
      <div class="small-box bg-teal">
        <div class="inner">
          <!-- <h3>150</h3> -->

          <p><?php echo $nobatch; ?></p>
        </div>
        <a href="#" class="small-box-footer">
          BATCH
        </a>
      </div>
    </div>
    <!-- ./col -->

 
  </div>

<div class="row">

    <div class="col-lg-12 col-xs-12">
    <button id='btnManual' type="button" class="btn bg-maroon btn-block">Input Manual ID Bahan Baku</button>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-xs-12">
      <!-- small box -->
      <div class="small-box bq-white">
        <div class="inner">
            <div class="form-group">
              <label>Input ID Bahan Baku</label>
              <input id = "kode" type="text" class="form-control" placeholder="Ketik/Scan ID Bahan Baku">
            </div>

        </div>

      </div>
    </div>
</div>



<?php Pjax::begin(['id' => 'evaluasi']); ?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'toolbar' => false,
        'export' => false,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            // 'kode_bb',
            [
                // 'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'status',
                'headerOptions' => ['style' => 'text-align:center; width:80px;'],
                // 'label' => 'Nama Bahan Baku',
                // 'editableOptions' => [
                //   // 'asPopover' => false,
                // ],
                'contentOptions' => function ($model, $key, $index, $column) {
                        return ['style' => 'text-align:center; background-color:' 
                            . ($model->status == 'OK'
                                ? '#5CDB95' : '')];
                    },
            ],

            // [
            //     // 'class' => 'kartik\grid\EditableColumn',
            //     'label' => 'ID BB',
            //     'attribute' => 'id_bb',
            //     'headerOptions' => ['style' => 'text-align:center; width:50px;'],
            //     // 'label' => 'Nama Bahan Baku',
            //     // 'editableOptions' => [
            //     //   // 'asPopover' => false,
            //     // ],
            //     'contentOptions' => ['style' => 'text-align:center;']
            // ],
            [
                // 'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'kode_olah',
                'headerOptions' => ['style' => 'text-align:center; width:30px;'],
                // 'label' => 'Nama Bahan Baku',
                // 'editableOptions' => [
                //   // 'asPopover' => false,
                // ],
                'contentOptions' => ['style' => 'text-align:center;']
            ],
            [
                'attribute'=>'kode_internal',
                'headerOptions' => ['style' => 'text-align:center; width:200px;'],
                'contentOptions' => ['style' => 'text-align:left;']
            ],
            [
                'label'=>'Nama BB',
                'attribute'=>'nama_bb',
                'headerOptions' => ['style' => 'text-align:center; width:250px;'],
                'contentOptions' => ['style' => 'text-align:left;']
            ],
            // 'nama_bb',
            // 'qty',
            [
                'attribute' => 'qty',
                'label' => 'Qty',
                'value' => function ($dataProvider){
                    return $dataProvider->qty.' '.$dataProvider->uom;
                },
                'headerOptions' => ['style' => 'text-align:center; width:150px;'],
                'contentOptions' => ['style' => 'text-align:right;']
            ],
            // 'uom',
            // 'kode_olah',
            // 'status',
            
            // [
            //     'class' => 'kartik\grid\EditableColumn',
            //     'attribute' => 'kode_bb_scan',
            //     'headerOptions' => ['style' => 'text-align:center; width: 350px;'],
            //     'contentOptions' => ['style' => 'text-align:center;'],
            //     // 'format' => Editable::FORMAT_BUTTON,
            //     // 'editableValueOptions'=>['class'=>'text-danger'],
            //     'editableOptions' => [
            //       'asPopover' => false,
            //     ],
            // ],
            // [
            //     'class' => 'kartik\grid\ActionColumn',
            //     'header' => 'Scan BB',
            //     'template' => '{download}',
            //     'buttons' => [
            //         'download' => function ($url,$dataProvider) {
            //             return Html::a(
            //                 '<span class="fa fa-qrcode" style="color:blue; font-size:2em;"></span>',
            //                 // ['log-formula-breakdown/schedule-list', 'id' => $model->id],
            //                 'javascript:void(0);', 
            //                 [
            //                     'title' => 'Add',
            //                     'data-pjax' => '0',
            //                     'id' => 'scan-'.$dataProvider->id,
            //                  ]
            //             );
            //         },
            //     ],
            //     'headerOptions' => ['class' => 'kartik-sheet-style'],
            //     'contentOptions' => ['style' => 'text-align:center;'],
            // ],
            
            // [
            //     'class' => 'kartik\grid\ActionColumn',
            //     'template' => '{download} {view}',
            //     'buttons' => [
            //         'download' => function ($url) {
            //             return Html::a(
            //                 '<span class="glyphicon glyphicon-plus"></span>',
            //                 // ['log-formula-breakdown/schedule-list', 'id' => $model->id],
            //                 ['log-formula-breakdown/schedule-list'], 
            //                 [
            //                     'title' => 'Split',
            //                     'data-pjax' => '0',
            //                 ]
            //             );
            //         },
            //         'view' => function ($url) {
            //             return Html::a(
            //                 '<span class="glyphicon glyphicon-refresh"></span>',
            //                 ['log-formula-breakdown/schedule-list'], 
            //                 [
            //                     'title' => 'Split',
            //                     'data-pjax' => '0',
            //                 ]
            //             );
            //         },
            //     ],
            //     'headerOptions' => ['class' => 'kartik-sheet-style'],
            // ],
        ],
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'pjax' => true, // pjax is set to always true for this demo
            // set your toolbar
            // 'toolbar' =>  [
            //     [
            //         'content' =>
            //             Html::button('<i class="fas fa-plus"></i>', [
            //                 'class' => 'btn btn-success',
            //                 'title' => 'Add Line',
            //                 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");'
            //             ]) . ' '.
            //             Html::a('<i class="fas fa-redo"></i>', ['grid-demo'], [
            //                 'class' => 'btn btn-outline-secondary',
            //                 'title'=> 'Reset Grid',
            //                 'data-pjax' => 0, 
            //             ]), 
            //         'options' => ['class' => 'btn-group mr-2']
            //     ],
            //     '{export}',
            //     '{toggleData}',
            // ],
            // 'toggleDataContainer' => ['class' => 'btn-group mr-2'],
            // // set export properties
            // 'export' => [
            //     'fontAwesome' => true
            // ],
            // parameters from the demo form
            'bordered' => true,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'hover' => true,
            // 'showPageSummary' => true,
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
                'heading' => '<i class="fas fa-book"></i>  Task Pengolahan',
            ],
    ]); ?>

    <div class="bawahan">
        <div class="row">
            <div style="width:100%; padding:0 30px; float:left;">
                <button type="button" id="btnFinish" class="btn btn-block btn-danger">SELESAI PROSES PENGOLAHAN</button>
            </div>
        </div>
    </div>
<?php
$script = <<< JS

    var fim_id = '$fim_id';
    var nomo = '$nomo';
    var siklus = '$siklus';
    var operator = '$operator';

    document.getElementById("kode").focus();

    $('button[id^="btnManual"]').on('click',function(){
        window.location = "index.php?r=flow-input-mo/input-bb-manual&id="+fim_id;
    });

    $('button[id^="btnFinish"]').on("click",function() {
        var r = confirm("Anda yakin akan menyelesaikan proses pengolahan?");
        if (r == true) {
            $.post("index.php?r=flow-input-mo/check-task&nomo="+nomo+"&siklus="+siklus, function (data){
                if (data==1){
                    window.location = "index.php?r=flow-input-mo/stop-pengolahan&nomo="+nomo+"&last_id="+$fim_id;
                } else {
                    var r2 = confirm("Ada task yang BELUM SELESAI, apakah anda tetap ingin menyelesaikan proses?");
                    if(r2 == true){
                        window.location = "index.php?r=flow-input-mo/stop-pengolahan&nomo="+nomo+"&last_id="+$fim_id;

                        // $.post("index.php?r=log-task-checker/finish-checker&nomo="+nomo+"&siklus="+siklus, function (data){
                        //     if (data==1){
                        //         $.post("index.php?r=log-task-checker/send-data-to-pengolahan&nomo="+nomo+"&siklus="+siklus, function (data){
                        //             if (data==1){
                        //                 window.location = "index.php?r=log-task-checker/check-nomo-checker";
                        //             }else {
                        //                 alert("gagal");
                        //             }
                        //         });
                        //     } else {
                        //         alert('Gagal stop proses checker!');
                        //     }
                        // });      
                    }else{
                        
                    }
                    // alert("Task belum selesai!");
                }
            });
        } else {
          
        }
    });

    $('a[id^="scan-"]').on("click",function() {
        var id = $(this).attr('id');
        var wo = id.substring(id.lastIndexOf("-") + 1);
        
        $.post("index.php?r=flow-input-mo/check-queue-pengolahan&nomo="+nomo+"&siklus="+siklus+"&wo="+wo, function (data){
            if (data==1){
                // console.log(wo);
                // window.location = "http://10.3.5.102/flowreport/web/index.php?r=flow-input-mo/scan-bb&id="+fim_id+"&wo="+wo;
                window.location = "index.php?r=flow-input-mo/scan-bb&id="+fim_id+"&wo="+wo;
            } else {
                alert("Item BB sebelumnya belum di scan!");
            }
        });


    });

$('#kode').on("change",function() {
  var kode = document.getElementById("kode").value;

  if ((kode != "")){


    $.get('index.php?r=flow-input-mo/check-bb',{ fim_id : fim_id, id_bb : kode },function(data)
    {
      if(data==1){
        $.post("index.php?r=flow-input-mo/check-queue-pengolahan&nomo="+nomo+"&siklus="+siklus+"&id_bb="+kode, function (data){
            if (data==1){
                $.get('index.php?r=flow-input-mo/update-status',{ id_bb : kode, log : "scan", pic : operator },function(data){
                    if(data==1){
                      window.location = "index.php?r=flow-input-mo/task&nomo="+nomo+"&siklus="+siklus;
                    } else {
                      alert('Tidak berhasil update log status');
                    }
                  });       
            } else {
                alert("Item BB sebelumnya belum di scan!");
            }
        });
      }else if (data==2){
        window.alert("Bahan baku sudah pernah di scan sebelumnya");
      }else{
        window.alert('Bahan baku tidak ditemukan dalam List Task');
      }

    });  
    
    // if(kode == id_bb)
    // {
    //   $.get('index.php?r=flow-input-mo/update-status',{ wo : wo, log : "scan", pic : operator },function(data){
    //     if(data==1){
    //       window.location = "index.php?r=flow-input-mo/task&nomo="+nomo+"&siklus="+siklus;
    //     } else {
    //       alert('Tidak berhasil update log status');
    //     }
    //   });           
    // } else {
    //   window.alert("Kode bahan baku tidak sesuai.");
    // }
  } else {
    window.alert("Kode bahan belum diisi!");
  }
});

JS;
$this->registerJs($script);
?>