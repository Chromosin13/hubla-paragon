<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>


                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-blue-gradient">
                      <h3 class="widget-user-username"><b>Pengolahan / Weighing</b></h3>
                      <h5 class="widget-user-desc">Work Order</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/cmyk-icon.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">

                                


                                <div class="flow-input-mo-form">


                                   
                                    <?php $form = ActiveForm::begin(); ?>

                                    <div class="row">
                                        <div class="col-md-12">
                                          <?= $form->field($model, 'staging')->checkbox(); ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-12">
                                          <?= $form->field($model, 'is_done')->checkbox(); ?>
                                          <!-- /input-group -->
                                        </div>
                                        
                                    </div>


                                    <div class="zoom">

                                        <?= Html::submitButton('STOP JADWAL',['class'=>'btn btn-danger btn-block'], ['stop-pengolahan', 'nomo'=>$nomo,'last_id'=>$last_id]) ?>
                                        
                                        </div>
                                

                                    

                                    <?php ActiveForm::end(); ?>

 

                                </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>


<div class="box box-widget widget-user">
    <div class="widget-user-header bg-blue-gradient">
                      <h3 class="widget-user-username"><b>Rincian</b></h3>
    </div>

    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'columns' => [
                // ['class' => 'kartik\grid\SerialColumn'],
                'id',
                'lanjutan',
                'nomo',
                'posisi',
                'datetime_start',
                'datetime_stop',
                // 'datetime_write',
                // 'nama_operator',
                'nama_line',
                'jenis_penimbangan',
                'nama_operator',
                // 'shift_plan_start_olah',
                // 'shift_plan_end_olah',
                // 'plan_start_olah',
                // 'plan_end_olah',
                // 'turun_bulk_start',
                // 'turun_bulk_stop',
                // 'adjust_start',
                // 'adjust_stop',
                // 'ember_start',
                // 'ember_stop',
                // 'is_done',
                // 'id',

                // ['class' => 'kartik\grid\ActionColumn'],
            ],
        ]); ?>

    </div>
</div>


<?php
$script = <<< JS

    $('#lanjutan').hide();

    var nomo = $('#flowinputmo-nomo').val();
    
    var posisi = $('#flowinputmo-posisi').val();

    $.post("index.php?r=scm-planner/get-line-olah-nomo&nomo="+$('#flowinputmo-nomo').val(), function (data){
        $("select#flowinputmo-nama_line").html(data);
    });


    $('#flowinputmo-jenis_penimbangan').change(function(){

        var jenis_penimbangan = $('#flowinputmo-jenis_penimbangan').val();

        $.get('index.php?r=flow-input-mo/get-lanjutan-penimbangan',{ nomo : nomo , jenis_penimbangan : jenis_penimbangan , posisi : posisi  }, function(data){
            var data = $.parseJSON(data);
            $('#flowinputmo-lanjutan').attr('value',data.lanjutan);

        });

        $('#lanjutan').fadeIn();

    });  



JS;
$this->registerJs($script);
?>