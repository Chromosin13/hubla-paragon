<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterDataFormulaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'BoM Confirmation';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bom-confirmation">

    <div class="box box-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-blue-gradient">
        <h3 class="widget-user-username"><b><?= Html::encode($this->title) ?></b></h3>
        <h5 class="widget-user-desc">Bill of Materials</h5>
      </div>
      <div class="widget-user-image">
        <img class="img-circle" src="../web/images/formula.png" alt="User Avatar">
      </div>
    </div>
    <br>
    <br>
    <div class="box box-widget widget-user">
        
      <!-- <div class="row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
        </div>
      </div>
      <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
        </div>
      </div> -->
    </div>



</div>