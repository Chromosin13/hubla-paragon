<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax; 
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DowntimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="batch-index">


    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'pjax' => true,
        'showPageSummary' => true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            // 'flow_input_mo_id',
            'snfg_komponen',
            'besar_batch_real',
            'nobatch',
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
