<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */

?>
<div class="flow-input-mo-create">

    <?= $this->render('_form-check-bom-rm', [
        //'model' => $model,
        //'kode_bb' => $bom2['kode_bb'],
        'id' => $id,
        'nomo' => $nomo,
        'bom' => $bom,
        // 'searchModel' => $searchModel,
        // 'dataProvider' => $dataProvider,
        'bomRows' => $bomRows,
        'nama_line' => $nama_line,
        'device_ip' => $device_ip,
        'operator' => $operator,
        'nomo' => $nomo,
        'no_formula' => $no_formula,
        'no_revisi' => $no_revisi,
        'nama_operator' => $nama_operator,
        'qtySiklus' => $qtySiklus,
        'siklus' => $siklus
        // 'dataProvider2' => $dataProvider2,
    ]) ?>

</div>
