<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */

// $this->title = 'Create Flow Input Mo';
// $this->params['breadcrumbs'][] = ['label' => 'Flow Input Mos', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="flow-input-mo-create">

  
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
