<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>


                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-blue-gradient">
                      <h3 class="widget-user-username"><b>Pengolahan</b></h3>
                      <h5 class="widget-user-desc">Input Data</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/proses_icons/mixer.png" alt="User Avatar" style="width:40%">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">
                              <div class="zoom">
                                <?=Html::a('SCAN WITH QR', ['flow-input-mo/check-pengolahan-tab'], ['class' => 'btn btn-success btn-block']);?>
                              </div>
                              <br>
                            <p></p>


                                    <div class="form-group field-flowinputmo-nomo has-success">
                                    <label class="control-label" for="flowinputmo-nomo">Nomor MO</label>
                                    <input type="text" id="flowinputmo-nomo" class="form-control" name="FlowInputMo[nomo]" aria-invalid="false">

                                    <div class="help-block"></div>
                                    </div>

                            </div>

                        </div>

                      </div>
                      <!-- /.row -->
                    </div>

                    <h3 style="color:white;background-color:#76D7C4; font-size: 18px;text-align: center; padding: 7px 10px; margin-top: 0; class="widget-user-username"><b>IN PROGRESS</b></h3>


                          <div class="box-body">

                             <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'columns' => [     
                                    'nomo',
                                    'lanjutan',
                                    'datetime_start',
                                    'datetime_stop',
                                    'nama_line',
                                    'jenis_penimbangan',
                                    'nama_operator',
                                ],
                            ]); ?>
                          </div>

                    <h3 style="color:white;background-color:#F9E79F; font-size: 18px;text-align: center; padding: 7px 10px; margin-top: 0; class="widget-user-username"><b>INBOUND PENIMBANGAN</b></h3>

                          <div class="box-body">
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider2,
                                'columns' => [        
                                    'nomo',
                                    'lanjutan',
                                    'datetime_start',
                                    'datetime_stop',
                                    'nama_line',
                                    'jenis_penimbangan',
                                    'nama_operator',
                                ],
                            ]); ?>
                          </div>




                    


<?php
$script = <<< JS

// AutoFocus Nomo Field

document.getElementById("flowinputmo-nomo").focus();

// Check SNFG Komponen Validity


$('#flowinputmo-nomo').change(function(){  

    var nomo = $('#flowinputmo-nomo').val().trim();
    
    if(nomo){
      window.location = "index.php?r=flow-input-mo/create-pengolahan&nomo="+nomo;
    }else{
      alert('Nomor Jadwal kosong!');
    }
    
});


// $('#flowinputmo-nomo').change(function(){  

//     var nomo = $('#flowinputmo-nomo').val();

//     $.get('index.php?r=scm-planner/check-nomo',{ nomo : nomo },function(data){
//         var data = $.parseJSON(data);
//         if(data.id>=1){
          
//           window.location = "index.php?r=flow-input-mo/create-pengolahan&nomo="+nomo;
           
//         }

//         else{

//           alert('Nomor Nomo Tidak Ditemukan di Database; Silahkan mengontak Planner/Leader');
//         }
    
//     });
// });


JS;
$this->registerJs($script);
?>