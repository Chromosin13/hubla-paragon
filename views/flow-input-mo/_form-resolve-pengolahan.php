<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\Downtime;
use app\models\DowntimeSearch;
use app\models\PengolahanBatch;
use app\models\PengolahanBatchSearch;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>


                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
                      <h3 class="widget-user-username"><b>Pengolahan / Mixing</b></h3>
                      <h5 class="widget-user-desc">Work Order</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/cmyk-icon.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                            <div class="box-body">

                                


                                <div class="flow-input-mo-form">

                                    <?php $form = ActiveForm::begin(); ?>

                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'nomo')->textInput(['readOnly'=>true,'value'=>$nomo]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'posisi')->textInput(['readOnly'=>true,'value'=>'PENGOLAHAN']) ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                          <?= $form->field($model, 'jenis_penimbangan')->dropDownList(['REWORK' => 'REWORK','OLAH PREMIX' => 'OLAH PREMIX','OLAH 1' => 'OLAH 1','OLAH 2' => 'OLAH 2','ADJUST' => 'ADJUST'],['prompt'=>'Select Option'])->label('Jenis Pengolahan'); ?>
                                          <!-- /input-group -->
                                        </div>
                                        <!-- /.col-lg-6 -->
                                        <div class="col-md-3">
                                          <?php

                                                if(empty($nama_line)){
                                                    echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                                                        'data' => ArrayHelper::map(FlowInputMo::find()->all()
                                                        ,'nama_line','nama_line'),
                                                        'options' => ['placeholder' => 'Select Line'],
                                                        'pluginOptions' => [
                                                            'allowClear' => true
                                                        ],
                                                    ]);
                                                }else{
                                                    echo $form->field($model, 'nama_line')->textInput(['readOnly'=>true,'value'=>$nama_line]);
                                                }
                                            ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div id="lanjutan">
                                        <div class="col-md-3">
                                          <?= $form->field($model, 'lanjutan')->textInput(['readOnly'=>true]) ?>
                                          <!-- /input-group -->
                                        </div>
                                        </div>
                                    </div>
 

                                    <div class="row">
                                        <div id="waktu_start_form" class="col-md-6">
                                        <?=
                                            $form->field($model, 'datetime_start')->widget(DateTimePicker::classname(), [
                                                'options' => ['placeholder' => 'Enter event time ...',
                                                              'readOnly' => true
                                                             ],
                                                    'pluginOptions' => [
                                                    'autoclose' => true,
                                                    'showMeridian' => false,
                                                    'minuteStep' => 5,
                                                    
                                                ]
                                            ]);

                                        ?>
                                          <!-- /input-group -->
                                        </div>
                                        <div id="waktu_stop_form" class="col-md-6">
                                        <?=
                                            $form->field($model, 'datetime_stop')->widget(DateTimePicker::classname(), [
                                                'options' => ['placeholder' => 'Enter event time ...',
                                                              'readOnly' => true
                                                             ],
                                                    'pluginOptions' => [
                                                    'autoclose' => true,
                                                    'showMeridian' => false,
                                                    'minuteStep' => 5,
                                                    
                                                ]
                                            ]);

                                        ?>
                                          <!-- /input-group -->
                                        </div>
                                    </div>

                                    <span class="badge bg-blue" id="durasi-downtime"></span>

                                    <br></br>
                                    <label class="control-label">Nama Operator</label>
                                    <?= $form->field($model, 'nama_operator')->widget(TagsinputWidget::classname(), [
                                                        'clientOptions' => [
                                                            'trimValue' => true,
                                                            'allowDuplicates' => false,
                                                            'maxChars'=> 4,
                                                            'minChars'=> 4,
                                                        ]
                                                    ])->label(false)?>

                                    <br></br>

                                    <div id="create-button" class="row">

                                        <div class="col-md-2 col-md-offset-5">

                                            <?= Html::submitButton($model->isNewRecord ? 'Submit & Isi Downtime' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                            
                                        </div>
                                    </div>

                                    <?php ActiveForm::end(); ?>

                                </div>
                            </div>
                        </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>


<div class="box box-widget widget-user">
    <div class="widget-user-header bg-blue">
                      <h3 class="widget-user-username"><b>Rincian</b></h3>
    </div>
    <div class="box-footer">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'value' => function($model,$key,$index,$column){
                        return GridView::ROW_COLLAPSED;
                    },
                    'detail' => function($model,$key,$index,$column){
                        
                        $searchModel = new DowntimeSearch();
                        
                        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                        $dataProvider->query->where("flow_input_mo_id=".$model->id);

                        return Yii::$app->controller->renderPartial('_expand-downtime-pengolahan', [
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                            'flow_input_mo_id' => $model->id,
                        ]);
                    },
                ],
                'lanjutan',
                'nomo',
                'posisi',
                'datetime_start',
                'datetime_stop',
                // 'datetime_write',
                
                'nama_line',
                'jenis_penimbangan',
                'nama_operator',
                // 'shift_plan_start_olah',
                // 'shift_plan_end_olah',
                // 'plan_start_olah',
                // 'plan_end_olah',
                // 'turun_bulk_start',
                // 'turun_bulk_stop',
                // 'adjust_start',
                // 'adjust_stop',
                // 'ember_start',
                // 'ember_stop',
                // 'is_done',
                // 'id',
                'durasiMenit',
                'durasiMenitbersih',
                'downtimeFilled',
                [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'value' => function($model,$key,$index,$column){
                        return GridView::ROW_COLLAPSED;
                    },
                    'detail' => function($model,$key,$index,$column){
                        
                        $searchModel = new PengolahanBatchSearch();
                        
                        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                        $dataProvider->query->where("pengolahan_id=".$model->id);

                        return Yii::$app->controller->renderPartial('_expand-batch-pengolahan', [
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                            'pengolahan_id' => $model->id,
                        ]);
                    },
                ],
            ],
        ]); ?>
    </div>

</div>


<?php
$script = <<< JS

    // Hide Lanjutan Jadwal , Tombol Submit, dan Date Time Stop
    $('#lanjutan').hide();
    $('#create-button').hide();
    $('#waktu_stop_form').hide();

    var nomo = $('#flowinputmo-nomo').val();
    
    var posisi = $('#flowinputmo-posisi').val();

    $.post("index.php?r=scm-planner/get-line-olah-nomo&nomo="+$('#flowinputmo-nomo').val(), function (data){
        $("select#flowinputmo-nama_line").html(data);
    });


    $('#flowinputmo-jenis_penimbangan').change(function(){

        var jenis_pengolahan = $('#flowinputmo-jenis_penimbangan').val();

        $.get('index.php?r=flow-input-mo/get-lanjutan-pengolahan',{ nomo : nomo , jenis_pengolahan : jenis_pengolahan , posisi : posisi  }, function(data){
            var data = $.parseJSON(data);
            $('#flowinputmo-lanjutan').attr('value',data.lanjutan);

        });

        $('#lanjutan').fadeIn();

    });  


    // Time Validator

    $('#flowinputmo-datetime_start').change(function(){
        
        // Get Current Timestamp
        var now = new Date();

        // Current Date 
        var start_js = new Date($('#flowinputmo-datetime_start').val());
        if(start_js<now){
            // Show Stop Form
            $('#waktu_stop_form').fadeIn("slow");
        }else{
            // Hide Stop Form
            $('#waktu_stop_form').hide();
            alert('Pemilihan waktu harus sebelum waktu saat ini!');
        }

    });

    $('#flowinputmo-datetime_stop').change(function(){
        var start_js = new Date($('#flowinputmo-datetime_start').val());
        var stop_js = new Date($('#flowinputmo-datetime_stop').val());
        var keterangan = $('#downtime-keterangan').val();
        if(stop_js<=start_js){
            alert('Periksa kembali waktu, Waktu Stop Bulk tidak boleh sebelum Waktu Start Bulk!');
            $('#create-button').hide();
        }else{
            $('#durasi-downtime').html('Durasi : '+(parseFloat(stop_js-start_js)/parseFloat(60000))+' Menit atau '+(parseFloat(stop_js-start_js)/parseFloat(3600000)).toFixed(2)+' Jam' );
            $('#create-button').fadeIn("slow");
        };

    });



JS;
$this->registerJs($script);
?>