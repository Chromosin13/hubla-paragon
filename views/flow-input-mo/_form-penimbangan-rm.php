<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\LogPenimbanganRm;
use app\models\Downtime;
use app\models\DowntimeSearch;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<script>
   
</script>

 <div class="box box-widget widget-user">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-blue-gradient">
        <h3 class="widget-user-username"><b>Penimbangan / Weighing</b></h3>
        <h5 class="widget-user-desc">Work Order</h5>
    </div>
    <div class="widget-user-image">
        <img class="img-circle" src="../web/images/scale.png" alt="User Avatar">
    </div>
    <div class="box-footer">
        <div class="row">
            <div class="box-body">
                <div class="box-body">
                    <div class="flow-input-mo-form">
                        <?php $form = ActiveForm::begin(); ?>
                        <div class="row">
                            <div class="col-md-6">
                              <?= $form->field($model, 'nomo')->textInput(['readOnly'=>true,'value'=>$nomo]) ?>
                              <!-- /input-group -->
                            </div>
                            <div class="col-md-6">
                              <?= $form->field($model, 'posisi')->textInput(['readOnly'=>true,'value'=>'PENIMBANGAN']) ?>
                              <!-- /input-group -->
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-6">
                              <?php
                                    if ($operator != 11){
                                        echo $form->field($model, 'jenis_penimbangan')
                                        ->dropDownList(
                                            $jenis_proses_list,           // Flat array ('id'=>'label')
                                            ['prompt'=>'Select Options']    // options
                                        )->label('Jenis Penimbangan');                                         
                                    }else{
                                        echo $form->field($model, 'jenis_penimbangan')->textInput(['readOnly'=>true,'value'=>'JADWAL BARU']);
                                    }

                               ?>
                              <!-- /input-group -->
                            </div>
                            <!-- /.col-lg-6 -->
                            <div class="col-md-3">
                              <?php

                                    if(empty($nama_line)){
                                        echo $form->field($model, 'nama_line')->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(FlowInputMo::find()->all()
                                            ,'nama_line','nama_line'),
                                            'options' => ['placeholder' => 'Select Line'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                    }else{
                                        echo $form->field($model, 'nama_line')->textInput(['readOnly'=>true,'value'=>$nama_line]);
                                    }
                                ?>
                              <!-- /input-group -->
                            </div>
                            <div id="lanjutan">
                                <div class="col-md-3">
                                    <?= $form->field($model, 'lanjutan')->textInput(['readOnly'=>true]) ?> 
                                    <!-- /input-group -->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                              <?= $form->field($model, 'no_formula')->textInput(['value'=>$no_formula]) ?>
                              <!-- /input-group -->
                            </div>
                            <div class="col-md-6">
                              <?= $form->field($model, 'no_revisi')->textInput(['value'=>$no_revisi]) ?>
                              <!-- /input-group -->
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-md-12">
                                <label class="control-label">Nama Operator</label>
                                <?= $form->field($model, 'nama_operator')->widget(TagsinputWidget::classname(), [
                                                'options' => ['id'=>'subcat-id','onkeyup'=>'this.value = this.value.toUpperCase();'],
                                                'clientOptions' => [
                                                    'trimValue' => true,
                                                    'allowDuplicates' => false,
                                                    'maxChars'=> 4,
                                                    'minChars'=> 4,
                                                ]
                                            ])->label(false)?>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-md-6">
                              <?php 
                                    if ($operator!=11){
                                        $data = ArrayHelper::map(LogPenimbanganRm::find()->where("nomo='".$nomo."' and status = 'Belum Ditimbang' ")->all()
                                                                                                ,'siklus','siklus');
                                        $data[-1]= 'REPACK';
                                        $data[0]= 'ALL';
                                        // $model->siklus = 3; 
                                        echo $form->field($model, 'siklus')->widget(Select2::classname(), [
                                                'data' => $data,
                                                'options' => ['placeholder' => 'Pilih Siklus'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);

                                    }else{
                                        echo $form->field($model, 'siklus')->textInput(['readOnly'=>true,'value'=>0]);
                                    }
                                    // echo $form->field($model, 'siklus')
                                    //     ->dropDownList(
                                    //         ['3'=>'ALL'],           // Flat array ('id'=>'label')
                                    //         ['prompt'=>'Select Options', 'readOnly'=>true, 'disabled'=> true]    // options
                                    //     )->label('Siklus');
                               ?>
                            </div>
                            <div class="col-md-6">
                                <!-- <label class="control-label">Nama Operator</label> -->
                                <?= $form->field($model, 'nama_operator')->textInput(['value'=>$nama_operator, 'maxlength' => true, 'style' => 'text-transform:uppercase','id'=>'input_operator']) ?>

                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-md-12">
                                <label class="control-label">Nama Operator</label>
                                <input id="input_operator" type="text" onkeyup="this.value = this.value.toUpperCase();">
                            </div>
                        </div> -->
                        <br></br>
                        <?php 
                            echo Html::submitButton($model->isNewRecord ? 'START' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-block' : 'btn btn-primary']);
                        ?>
                                            


                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
</div>


<!-- <div class="box box-widget widget-user">
    <div class="widget-user-header bg-blue">
        <h3 class="widget-user-username"><b>Rincian</b></h3>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'value' => function($model,$key,$index,$column){
                    return GridView::ROW_COLLAPSED;
                },
                'detail' => function($model,$key,$index,$column){
                    
                    $searchModel = new DowntimeSearch();
                    
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                    $dataProvider->query->where("flow_input_mo_id=".$model->id);

                    return Yii::$app->controller->renderPartial('_expand-downtime-penimbangan', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'flow_input_mo_id' => $model->id,
                    ]);
                },
            ],
            'lanjutan',
            'nomo',
            'posisi',
            'datetime_start',
            'datetime_stop',
            // 'datetime_write',
            
            'nama_line',
            'jenis_penimbangan',
            'nama_operator',
            // 'shift_plan_start_olah',
            // 'shift_plan_end_olah',
            // 'plan_start_olah',
            // 'plan_end_olah',
            // 'turun_bulk_start',
            // 'turun_bulk_stop',
            // 'adjust_start',
            // 'adjust_stop',
            // 'ember_start',
            // 'ember_stop',
            // 'is_done',
            'id',
            //'durasiMenit',
            //'durasiMenitbersih',
            //'downtimeFilled',
        ],
    ]); ?>

</div>  -->
                  


<?php
$script = <<< JS

    document.getElementById('input_operator').addEventListener('input', function (e) {
      //e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1,').trim();
        e.target.value = e.target.value.toUpperCase();
        if (e.target.value.length % 5 == 0){
            //e.target.value = e.target.value.replace(/(.{4})/g, '$1').replace(/[ ,]+/g, ",");
            e.target.value = e.target.value.replace(/.$/,",");
        }
        
    }); 

    // function forceInputUppercase(e)
    //   {
    //     var start = e.target.selectionStart;
    //     var end = e.target.selectionEnd;
    //     e.target.value = e.target.value.toUpperCase();
    //     e.target.setSelectionRange(start, end);
    //   }

    // document.getElementById("subcat-id").addEventListener("keyup", forceInputUppercase, false);

    $('#lanjutan').hide();

    var nomo = $('#flowinputmo-nomo').val();
    
    var posisi = $('#flowinputmo-posisi').val();

    $.post("index.php?r=master-data-line/get-line-timbang-nomo&nomo="+$('#flowinputmo-nomo').val(), function (data){
        $("select#flowinputmo-nama_line").html(data);
    });


    $('#flowinputmo-jenis_penimbangan').change(function(){

        var jenis_penimbangan = $('#flowinputmo-jenis_penimbangan').val();

        $.get('index.php?r=flow-input-mo/get-lanjutan-penimbangan',{ nomo : nomo , jenis_penimbangan : jenis_penimbangan , posisi : posisi  }, function(data){
            var data = $.parseJSON(data);
            $('#flowinputmo-lanjutan').attr('value',data.lanjutan);

        });

        $('#lanjutan').fadeIn();

    });  

    var operator = "$operator";
    if (operator == 11){
        var jenis_penimbangan = $('#flowinputmo-jenis_penimbangan').val();

        $.get('index.php?r=flow-input-mo/get-lanjutan-penimbangan',{ nomo : nomo , jenis_penimbangan : jenis_penimbangan , posisi : posisi  }, function(data){
            var data = $.parseJSON(data);
            $('#flowinputmo-lanjutan').attr('value',data.lanjutan);

        });

        $('#lanjutan').fadeIn();
    }



JS;
$this->registerJs($script);
?>