<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="flow-input-mo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'nomo') ?>

    <?= $form->field($model, 'posisi') ?>

    <?= $form->field($model, 'lanjutan') ?>

    <?= $form->field($model, 'datetime_start') ?>

    <?= $form->field($model, 'datetime_stop') ?>

    <?php // echo $form->field($model, 'datetime_write') ?>

    <?php // echo $form->field($model, 'nama_operator') ?>

    <?php // echo $form->field($model, 'nama_line') ?>

    <?php // echo $form->field($model, 'jenis_penimbangan') ?>

    <?php // echo $form->field($model, 'shift_plan_start_olah') ?>

    <?php // echo $form->field($model, 'shift_plan_end_olah') ?>

    <?php // echo $form->field($model, 'plan_start_olah') ?>

    <?php // echo $form->field($model, 'plan_end_olah') ?>

    <?php // echo $form->field($model, 'turun_bulk_start') ?>

    <?php // echo $form->field($model, 'turun_bulk_stop') ?>

    <?php // echo $form->field($model, 'adjust_start') ?>

    <?php // echo $form->field($model, 'adjust_stop') ?>

    <?php // echo $form->field($model, 'ember_start') ?>

    <?php // echo $form->field($model, 'ember_stop') ?>

    <?php // echo $form->field($model, 'is_done') ?>

    <?php // echo $form->field($model, 'id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
