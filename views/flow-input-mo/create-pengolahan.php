<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */

?>
<div class="flow-input-mo-create">





    <?php

    	echo $this->render('_form-pengolahan', [
		        'model' => $model,
		        'nomo' => $nomo,
			    'searchModel' => $searchModel,
			    'dataProvider' => $dataProvider,
			    'nama_line' => $nama_line,
                'jenis_proses_list' => $jenis_proses_list,
				'nobatch' => $nobatch,
		]);


     ?>

</div>
