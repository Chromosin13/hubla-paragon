<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowAnalyst;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- CSS For Video Selector -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="ZXing for JS">

  <title>ZXing TypeScript | Decoding from camera stream</title>

  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">

<style>
.column {
  float: left;
  width: 100%;
}

.accordion {
  
  color: white;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  transition: 0.4s;
}

.active, .accordion:hover {
  background-color: #ccc;
}

.accordion:after {
  content: '\002B';
  color: white;
  font-weight: bold;
  float: right;
  margin-left: 5px;
}

.active:after {
  content: "\2212";
  color: white;
}

.panel {
  padding: 0 18px;
  background-color: white;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
}

.header{
  padding :1px;
  text-align: center;
}

</style>
</head>

<div class="row">
  <div class="col-md-12">
    <div class="box box-info ">
      <div class="box-header with-border">
        <h3 class="box-title">Input Manual ID Bahan Baku</h3>

        <div class="box-tools pull-right">
          <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> -->
          <!-- </button> -->
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body" style="">
        <div class="form-group">
          <div class="col-md-12 column" style="margin-left:0px; margin-top:0px;">
            <label for="kode_internal">Kode Internal</label>
            <input type="text" name="kode_internal" id="kode_internal" >
            <label for="kode_olah">Kode Olah</label>
            <input type="text" name="kode_olah" id="kode_olah" >
            <div class="row">
              <div class="col-lg-6 col-xs-6">
                <label for="qty">Qty</label>
                <input style="width:350px;" type="text" name="qty" id="qty" >    
              </div>
              <div class="col-lg-6 col-xs-6">
                <label for="uom">Satuan</label>
                <!-- <input style="width:350px;" type="text" name="uom" id="uom" >  -->
                <select id="uom" name="uom">
                  <option value="kg">kg</option>
                  <option value="g">g</option>
                </select>
              </div>
            
          </div>
          <button onclick="check()" style="width:50%; display: block; margin-left: auto; margin-right: auto;">Check</button>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>



  <!-- <button onclick="inputmanual()" style="width:50%; display: block; margin-left: auto; margin-right: auto;" class="btn btn block">Input Manual BB</button> -->



<!-- Javascript -->
<!-- Calling ZXing API CDN -->
<script type="text/javascript" src="zxing.js"></script>
<!-- Code -->
<script type="text/javascript">



/************************************************
 * Accordion
 *
************************************************/ 
// var acc = document.getElementsByClassName("accordion");
// var i;

// for (i = 0; i < acc.length; i++) {
//   acc[i].addEventListener("click", function() {
//     this.classList.toggle("active");
//     var panel = this.nextElementSibling;
//     if (panel.style.maxHeight) {
//       panel.style.maxHeight = null;
//     } else {
//       panel.style.maxHeight = panel.scrollHeight + "px";
//     } 
//   });
// }


function check(){
    var kode_internal = document.getElementById("kode_internal").value;
    var kode_olah = document.getElementById("kode_olah").value;
    var qty = document.getElementById("qty").value;
    var uom = document.getElementById("uom").value;
    var nomo = '<?php echo $nomo; ?>';
    var siklus = '<?php echo $siklus; ?>';
    var pic = '<?php echo $operator; ?>';

    // console.log(kode_internal);
    // console.log(kode_olah);
    // console.log(qty);
    // console.log(nomo);
    // console.log(siklus);
    // console.log(pic);
    // console.log(uom);

    if (kode_internal == '' || kode_olah == '' || qty == '' || nomo == '' || siklus == '' || pic == '' || uom == '' ){
      console.log('null');
    }else{
      $.getJSON("index.php?r=flow-input-mo/check-bb-manual&nomo="+nomo+"&siklus="+siklus+"&kode_internal="+kode_internal+"&kode_olah="+kode_olah+"&qty="+qty+"&uom="+uom, function (data){
          if (data.status=='exist') {
              var id_bb = data.id_bb;
              $.post("index.php?r=flow-input-mo/check-queue-pengolahan&nomo="+nomo+"&siklus="+siklus+"&id_bb="+data.id_bb, function (data){
                  if (data==1){
                      $.get('index.php?r=flow-input-mo/update-status',{ id_bb : id_bb, log : "manual", pic : pic },function(data){
                          if(data==1){
                            window.location = "index.php?r=flow-input-mo/task&nomo="+nomo+"&siklus="+siklus;
                          } else {
                            alert('Tidak berhasil update log status');
                          }
                        });       
                  } else {
                      alert("Item BB sebelumnya belum di scan!");
                  }
              });
            
          }else if (data.status == 'scanned'){
              window.alert("Bahan baku sudah pernah di scan sebelumnya.");
   
          } else {
              window.alert('Bahan baku tidak ditemukan dalam List Task');
          }
        });  
    }
    
}

  // document.getElementById("kode").focus();

  </script>


<?php
$script = <<< JS

// $('#kode').on("change",function() {
//   var kode = document.getElementById("kode").value;
//   var id = '$id';
//   var nomo = '$nomo';
//   var siklus = '$op->siklus';
//   var operator = '$operator';
//   console.log(id_bb);

//   // if ((kode != "")){
//   //   $.get('index.php?r=flow-input-mo/check-bb',{ wo : wo, kode_bb : kode },function(data)
//   //   {
//   //     if(data==1){                    
//   //        $.get('index.php?r=flow-input-mo/update-status',{ wo : wo, log : "scan/manual", pic : operator },function(data){
//   //           if(data==1){
//   //             window.location = "index.php?r=flow-input-mo/task&nomo="+nomo;
//   //           } else {
//   //             alert('Tidak berhasil update log status');
//   //           }
//   //        });           
//   //     } else {
//   //       window.alert("Kode bahan baku tidak sesuai.");
//   //     }
//   //   });
//   // } else {
//   //   window.alert("Kode bahan belum diisi!");
//   // }

//   if ((kode != "")){
//     // $.get('index.php?r=flow-input-mo/check-bb',{ wo : wo, kode_bb : kode },function(data)
//     // {
//     //   if(data==1){}
//     // }  
//     if(kode == id_bb)
//     {
//       $.get('index.php?r=flow-input-mo/update-status',{ wo : wo, log : "scan", pic : operator },function(data){
//         if(data==1){
//           window.location = "index.php?r=flow-input-mo/task&nomo="+nomo+"&siklus="+siklus;
//         } else {
//           alert('Tidak berhasil update log status');
//         }
//       });           
//     } else {
//       window.alert("Kode bahan baku tidak sesuai.");
//     }
//   } else {
//     window.alert("Kode bahan belum diisi!");
//   }
// });

JS;
$this->registerJs($script);
?>