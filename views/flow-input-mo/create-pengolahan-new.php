<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */

?>
<div class="flow-input-mo-create">





    <?php

    	echo $this->render('_form-pengolahan-new', [
		        'model' => $model,
                'nomo' => $nomo,
                'last_datetime_start' => $last_datetime_start,
                'last_datetime_stop' => $last_datetime_stop,
                'last_id' => $last_id,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'nama_line' => $nama_line,
                'jenis_proses_list' => $jenis_proses_list,
                'nobatch' => $nobatch,
		]);


     ?>

</div>
