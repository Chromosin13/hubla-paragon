<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="flow-input-mo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomo')->textInput() ?>

    <?= $form->field($model, 'posisi')->textInput() ?>

    <?= $form->field($model, 'lanjutan')->textInput() ?>

    <?= $form->field($model, 'datetime_start')->textInput() ?>

    <?= $form->field($model, 'datetime_stop')->textInput() ?>

    <?= $form->field($model, 'datetime_write')->textInput() ?>

    <?= $form->field($model, 'nama_operator')->textInput() ?>

    <?= $form->field($model, 'nama_line')->textInput() ?>

    <?= $form->field($model, 'jenis_penimbangan')->textInput() ?>

    <?= $form->field($model, 'shift_plan_start_olah')->textInput() ?>

    <?= $form->field($model, 'shift_plan_end_olah')->textInput() ?>

    <?= $form->field($model, 'plan_start_olah')->textInput() ?>

    <?= $form->field($model, 'plan_end_olah')->textInput() ?>

    <?= $form->field($model, 'turun_bulk_start')->textInput() ?>

    <?= $form->field($model, 'turun_bulk_stop')->textInput() ?>

    <?= $form->field($model, 'adjust_start')->textInput() ?>

    <?= $form->field($model, 'adjust_stop')->textInput() ?>

    <?= $form->field($model, 'ember_start')->textInput() ?>

    <?= $form->field($model, 'ember_stop')->textInput() ?>

    <?= $form->field($model, 'is_done')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
