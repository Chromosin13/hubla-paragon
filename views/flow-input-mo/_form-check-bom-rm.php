<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterDataFormulaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'BoM Confirmation';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.resp-container { 
    /*position: relative;*/
    /*overflow: hidden;*/
    /*padding-top: 56.25%;*/
}

.resp-iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    border: 0;
}
.bawahan {
   position: fixed;
   border-top:2px solid #D3D3D3;
   left: 0;
   bottom: 0;
   width: 100%;
   padding:15px 0;
   background-color: white;
   color: white;
   text-align: center;
}

#snackbar {
  visibility: hidden;
  min-width: 250px;
  background-color: #333;
  color: #fff;
  text-align: center;
  border-radius: 2px;
  padding: 16px;
  position: fixed;
  z-index: 1;
  left: 50%;
  -ms-transform: translateX(-50%);
  transform: translateX(-50%);
  bottom: 30px;
  font-size: 17px;
}

#snackbar.show {
  visibility: visible;
  -webkit-animation: fadein 0.1s, fadeout 0.1s 0.5s;
  animation: fadein 0.1s, fadeout 0.1s 0.5s;
}

@-webkit-keyframes fadein {
  from {bottom: 0; opacity: 0;} 
  to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
  from {bottom: 0; opacity: 0;}
  to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
  from {bottom: 30px; opacity: 1;} 
  to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
  from {bottom: 30px; opacity: 1;}
  to {bottom: 0; opacity: 0;}
}
</style>

<div class="bom-confirmation" id="main-box">
  <div class="box box-widget widget-user">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-blue-gradient">
      <div class="row">
        <div class="column">
          <h1 class="header" style="font-weight: bolder; position:absolute; left:5vmin;" class="widget-user-username"><b><?php echo 'OPR '; echo $operator; ?></b></h1>
        </div>
        <!-- <h5 class="widget-user-desc">Weighing Task</h5> -->
        <div class="column">
          <h1 class="header" style="font-weight: bolder; position:absolute; right:5vmin;"><?php echo $nama_line; ?></h1>
        </div>
      </div>
      <div class="widget-user-image">
        <img class="img-circle" src="../web/images/formula.png" alt="User Avatar">
      </div>
    </div>
    <br>
    <br>
    <div class="box box-widget widget-user">
      <table class="table">
        <tbody>
          <tr>
            <td class="text-center" style="width: 35%; font-size:1.6em; background: #ffb3ba;">
              <b>No MO</b>
            </td>
            <td class="text-center" style="font-size:1.4em;"><?php echo $nomo; ?></td>
          </tr>
          <tr>
            <td class="text-center" style="width: 35%; font-size:1.6em; background: #ffdfba;">
              <b>No Formula</b>
            </td>
            <td class="text-center" style="font-size:1.4em;"><?php echo $bom[0]['no_formula']; ?></td>
          </tr>
          <tr>
            <td class="text-center" style="width: 35%; font-size:1.6em; background: #ffffba;">
              <b>No Revisi</b>
            </td>
            <td class="text-center" style="font-size:1.4em;"><?php echo $bom[0]['no_revisi']; ?></td>
          </tr>
          <tr>
            <td class="text-center" style="width: 35%; font-size:1.6em; background: #baffc9;">
              <b>Nama Item</b>
            </td>
            <td class="text-center" style="font-size:1.4em;"><?php echo $bom[0]['naitem_fg']; ?></td>
          </tr>
          <tr>
            <td class="text-center" style="width: 35%; font-size:1.6em; background: #bae1ff;">
              <b>Operator</b>
            </td>
            <td class="text-center" style="font-size:1.4em;"><?php echo $nama_operator; ?></td>
          </tr>
        </tbody>
      </table>
    </div>
    <hr>
    <div class="box box-widget widget-user">
      <?php 

      if ($siklus==-1){
        $list_timbangan = Yii::$app->db->createCommand("SELECT distinct on (kode_timbangan) kode_timbangan,uom FROM master_data_timbangan_rm WHERE line = 'LWE03' ")->queryAll();
      }

      for ($i = 0; $i<(count($qtySiklus)); $i++){
        echo '<h2 style="text-align:center; font-weight:bolder;">SIKLUS-'; echo $qtySiklus[$i]['siklus']; echo '</h2>';
        echo '<table class="table">
        <thead>
          <tr>
            <th class="text-center" style="width: 8%; font-size:1.3em;">
              <b>Kode BB</b>
            </th>
            <th class="text-center" style="width: 13%; font-size:1.3em;">
              <b>Nama BB</b>
            </th>
            <th class="text-center" style="width: 8%; font-size:1.3em;">
              <b>Kode Internal</b>
            </th>
            <!-- <th class="text-center" style="width: 10%; font-size:1.5em;">
              <b>Kode Olah</b>
            </th> -->
            <th class="text-center" style="width: 3%; font-size:1.3em;">
              <b>Siklus</b>
            </th>
            <th class="text-center" style="width: 7%; font-size:1.3em;">
              <b>Weight</b>
            </th>
            <th class="text-center" style="width: 3%; font-size:1.3em;">
              <b>Satuan</b>
            </th>
            <th class="text-center" style="width: 4%; font-size:1.3em;">
              <b>Timbangan</b>
            </th>
            <th class="text-center" style="width: 8%; font-size:1.3em;">
              <b>Operator</b>
            </th>
            <th class="text-center" style="width: 6%; font-size:1.3em;">
              <b>Status</b>
            </th>
          </tr>
        </thead>
        <tbody>';
        for ($x = 0; $x < $bomRows; $x++) {
          if ($bom[$x]['siklus'] == $qtySiklus[$i]['siklus']){
            echo '<tr>';
              echo '<td class="text-center">'; echo $bom[$x]['kode_bb']; echo '</td>';
              echo '<td class="text-center">'; echo $bom[$x]['nama_bb']; echo '</td>';
              echo '<td class="text-center">'; echo $bom[$x]['kode_internal']; echo '</td>';
              // echo '<td class="text-center">'; echo $bom2[$x]['kode_olah']; echo '</td>';
              echo '<td class="text-center">'; echo $bom[$x]['siklus']; echo '</td>';
              echo '<td class="text-center">'; echo number_format($bom[$x]['weight'],4); echo '</td>';
              echo '<td class="text-center">'; echo $bom[$x]['satuan']; echo '</td>';
              if ($siklus == -1){
                echo '<td class="text-center">'; echo '<select id="'; echo 'timbangan-'; echo $bom[$x]['id']; echo'">';
                echo '<option value="'.$bom[$x]['no_timbangan'].'" selected>'.$bom[$x]['no_timbangan'].'</option>';

                foreach ($list_timbangan as $timbangan){
                  if ($timbangan['uom']==$bom[$x]['satuan']){
                    echo '<option value="'.$timbangan['kode_timbangan'].'">'.$timbangan['kode_timbangan'].'</option>';
                  }
                }
                '</select>' ; echo '</td>';
                // echo '<td class="text-center">'; echo $bom[$x]['status']; echo '</td>'; 
              }else{
                echo '<td class="text-center">'; echo $bom[$x]['no_timbangan']; echo '</td>';
              }

              // if ($bom[$x]['is_repack'] == 1 && $siklus != -1){
              //     echo '<td class="text-center">'; echo '<select id="'; echo 'select-'; echo $bom[$x]['id']; echo'">'; 
              //     if ($bom[$x]['operator'] == 4){
              //       echo '<option value="4" selected>4</option>
              //       <option value="5">5</option>
              //       <option value="6">6</option>';
              //     } else if ($bom[$x]['operator'] == 5) {
              //       echo '<option value="4">4</option>
              //       <option value="5" selected>5</option>
              //       <option value="6">6</option>';
              //     } else if ($bom[$x]['operator'] == 6) {
              //       echo '<option value="4">4</option>
              //       <option value="5">5</option>
              //       <option value="6" selected>6</option>';
              //     }
              //     echo "</select>"; echo '</td>';
              // } else {
                  echo '<td class="text-center">'; echo '<select id="'; echo 'select-'; echo $bom[$x]['id']; echo'">'; 
                  if ($bom[$x]['operator'] == 1){
                    echo '<option value="1" selected>1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>';
                  } else if ($bom[$x]['operator'] == 2) {
                    echo '<option value="1">1</option>
                    <option value="2" selected>2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>';
                  } else if ($bom[$x]['operator'] == 3) {
                    echo '<option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3" selected>3</option>
                    <option value="4">4</option>';
                  } else if ($bom[$x]['operator'] == 4) {
                    echo '<option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4" selected>4</option>';
                  } else if ($bom[$x]['operator'] == 5) {
                    echo '<option value="5" selected>-</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>';
                  } else if ($bom[$x]['operator'] == 11) {
                    echo '<option value="11" selected>11</option>';
                  }
                  '</select>' ; echo '</td>';
              // }
              echo '<td class="text-center">'; echo $bom[$x]['status']; echo '</td>'; 
            echo '</tr>';
          }
        }
        echo '</tbody></table><hr>';
      } ?>
      <div class="bawahan">
        <div class="row">
          <div style="width:100%; padding:0 30px;">
            <!-- <?php echo '<a href="?r=log-penimbangan-rm/start&id='.$id.'" id="nextbutton" class="btn btn-block btn-info"><b>NEXT</b></a>'; ?> -->
            <a onclick="confirm()" class="btn btn-block btn-info"><b>NEXT</b></a>
          </div>
        </div>
      </div> 
    </div>
  </div>
</div>

<div id="snackbar">Some text some message..</div>

<div id="modal" style="text-align: center; position: absolute; z-index: 10; top: 20%; left: 50%; transform: translate(-50%, -50%); background-color: white; width:70%; height:22vh; border-radius: 5px; display: none;">
  <div id="modal-header" style="background-color: #5DADE2; margin-bottom: 0px; border-top-left-radius: 5px; border-top-right-radius: 5px;">
    <p style="text-align:left; padding-left : 20px; padding-top : 6px; font-size:1.5em;"><b>Confirmation</b></p>
  </div>
  <div id="modal-content" style="height:30%;">
    <p style="padding-top:20px; font-size:1.2em;">Apakah data yang anda masukkan sudah benar?</p>
  </div>
  <hr>
  <div id="modal-footer" style="margin-top:0px;">
    <button id="addbatch" onclick="no()" class="btn btn-danger" style="position: absolute; top: 83%; left: 35%; transform: translate(-50%, -50%); width:20%;">
      <b>TIDAK</b>
    </button>
    <button id="addbatch" onclick="yes()" class="btn btn-success" style="position: absolute; top: 83%; left: 65%; transform: translate(-50%, -50%); width:20%;">
      <b>YA</b>
    </button>

  </div>
</div>

<script>
  function confirm(){
    var id = <?php echo $id; ?>;
    $.getJSON('index.php?r=flow-input-mo/check-operator1',{ id : id },function(tmp){
          var data = tmp;
          
          if(data.op1!=1){
            window.alert("Operator 1 harus di assign ke Task");
          } else {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
            document.getElementById("main-box").style.opacity = "0.1";
            document.getElementById("modal").style.display = "block";
          }
        });

}

function yes(){
  var id = <?php echo $id; ?>; 
  window.location = "?r=log-penimbangan-rm/start&id="+id;
}

function no(){
  document.getElementById("main-box").style.opacity = "1";
  document.getElementById("modal").style.display = "none";
}
</script>

<?php
$script = <<< JS

// AutoFocus Nomo Field

//document.getElementById("nextbutton").focus();

//console.log($('#select-1306').val());

// Trigger Query

$('#nextbutton').change(function(){
  // var files = $('#files')[0].files;
  // var error = '';
  // var form_data = new FormData();
    $.ajax({
        url:"log-penimbangan-rm/coba",
        method:"POST",
        data:{halo:"1"},
        contentType:false,
        cache:false,
        processData:false,
        beforeSend:function()
        {
            $('#uploaded_images').html("<label class='text-success'>Uploading...</label>");
        },
        success:function(data)
        {
            $('#uploaded_images').html(data);
            //$('#files').val('');
        }
    })
});

$('select[id^="select"]').change(function(){
  var idRaw = $(this).attr('id');
  var id = idRaw.replace('select-', '');
  var operator = this.value;
  //console.log(id);
  //console.log(this.value);
  var x = document.getElementById("snackbar");

  $.getJSON("index.php?r=flow-input-mo/update-operator",{id:id, operator:operator},function(result){
    //console.log(result.status);
      
      if (result.status == "success"){
        
        x.className = "show";
        x.innerHTML = "Success";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 500);
      }
      
  }).error(function() {
      x.className = "show";
      x.innerHTML = "Failed";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 500);
  });
});

$('select[id^="timbangan"]').change(function(){
  var idRaw = $(this).attr('id');
  var id = idRaw.replace('timbangan-', '');
  var timbangan = this.value;
  console.log(idRaw);
  console.log(this);
  var x = document.getElementById("snackbar");

  $.getJSON("index.php?r=flow-input-mo/update-timbangan",{id:id, timbangan:timbangan},function(result){
    //console.log(result.status);
      
      if (result.status == "success"){
        
        x.className = "show";
        x.innerHTML = "Success";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 500);
      }
      
  }).error(function() {
      x.className = "show";
      x.innerHTML = "Failed";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 500);
  });
});

JS;
$this->registerJs($script);
?>