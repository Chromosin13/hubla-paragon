<?php

// use app\assets\ViAsset;
// ViAsset::register($this);
?>

<div class="navigation-index">
    <div class="container">
        <?= $historicTable; ?>
        <?= $currentTable; ?>
    </div>
</div>

<?php
$this->registerJsFile("@web/js/visual-inspection/navigation.js",[
    'depends' => [
        \yii\web\JqueryAsset::class,
    ],
    'type' => 'module'
]);
?>