<?php
use yii\helpers\Url;
?>

<div class="viewAssets-historicTable">
    <div class="box box-solid box-primary">
        <div class="box-header">
            <h3 class="box-title">Histori Hasil Inspeksi</h3>
        </div>
        <!-- /.box-header -->
                
        <div class="box-body">
            <table id="historic-table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th scope="col">Kode SNFG</th>
                        <th scope="col">Jalur Kemas</th>
                        <th scope="col">Mulai Kemas</th>
                        <th scope="col">Selesai Kemas</th>
                        <th scope="col">Defect Dusat</th>
                        <th scope="col">Total Inspeksi</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($products['done'] as $product): ?>
                    <tr>
                        <td class="kode_snfg"><?= $product['snfg'] ?></td>
                        <td><?= $product['nama_line'] ?></td>
                        <td><?= $product['datetime_start'] ?></td>
                        <td><?= $product['datetime_stop'] ?></td>
                        <td><?= $product['jumlah_defect_sekunder'] ?></td>
                        <td><?= $product['inspeksi_sekunder_ke'] ?></td>
                        <td>
                            <a class="btn btn-primary" href="<?= Url::base() ?>/dashboard/check/<?= $product['id'] ?>/<?= $product['snfg']; ?>">View</a>
                        </td>
                        <td>
                            <a class="btn btn-primary" id="<?= $product['id']; ?>" onclick="getJsonInspectionResult(<?= $product['id']; ?>)" download="<?= $product['snfg']; ?>.csv">Download CSV</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>                    
                </tbody>
            </table>
        </div>
                <!-- /.box-body -->
    </div>
</div>