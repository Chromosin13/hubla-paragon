<?php
use yii\helpers\Url;
?>

<div class="viewAssets-historicTable">
    <div class="box box-solid box-primary">
        <div class="box-header">
            <h3 class="box-title">Histori Hasil Inspeksi</h3>
        </div>
        <!-- /.box-header -->
                
        <div class="box-body">
            <table id="historic-table" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th scope="col">Kode SNFG</th>
                        <th scope="col">Nama Finished Good</th>
                        <th scope="col">Jalur Kemas</th>
                        <th scope="col">Mulai Kemas</th>
                        <th scope="col">Selesai Kemas</th>
                        <th scope="col">Defect Dusat</th>
                        <th scope="col">Total Inspeksi</th>
                        <th scope="col">Defect Rate</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
                <!-- /.box-body -->
    </div>
</div>