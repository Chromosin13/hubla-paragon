<?php
use yii\helpers\Url;
?>

<div class="viewAssets-currentTable">
    <div class="box box-solid box-primary">
        <div class="box-header">
            <h3 class="box-title">Histori Hasil Inspeksi</h3>
        </div>
        <!-- /.box-header -->
                
        <div class="box-body">
            <table id="current-table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th scope="col">Kode SNFG</th>
                        <th scope="col">Nama Finished Good</th>
                        <th scope="col">Jalur Kemas</th>
                        <th scope="col">Waktu Mulai Inspeksi</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody id="current-inspection">
                </tbody>
            </table>
        </div>
    </div>
</div>