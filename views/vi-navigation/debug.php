<?php
?>

<div class="vi-navigation-debug">
   <div class="box box-solid box-primary">
      <div class="box-header">
         <h3 class="box-title">Histori Hasil Inspeksi</h3>
      </div>
      
      <div class="box-body">
         <table id="example" class="table table-dark" width="100%">
            <thead>
               <tr>
               <th>Name</th>
               <th>Position</th>
               <th>Office</th>
               <th>Age</th>
               <th>Start date</th>
               <th>Salary</th>
               </tr>
            </thead>

            <tbody>
               <tr>
               <td>Tiger Nixon</td>
               <td>System Architect</td>
               <td>Edinburgh</td>
               <td>61</td>
               <td>2011/04/25</td>
               <td>$3,120</td>
               </tr>
               <tr>
               <td>Garrett Winters</td>
               <td>Director</td>
               <td>Edinburgh</td>
               <td>63</td>
               <td>2011/07/25</td>
               <td>$5,300</td>
               </tr>
               <tr>
               <td>Ashton Cox</td>
               <td>Technical Author</td>
               <td>San Francisco</td>
               <td>66</td>
               <td>2009/01/12</td>
               <td>$4,800</td>
               </tr>
               <tr>
               <td>Cedric Kelly</td>
               <td>Javascript Developer</td>
               <td>Edinburgh</td>
               <td>22</td>
               <td>2012/03/29</td>
               <td>$3,600</td>
               </tr>
               <tr>
               <td>Jenna Elliott</td>
               <td>Financial Controller</td>
               <td>Edinburgh</td>
               <td>33</td>
               <td>2008/11/28</td>
               <td>$5,300</td>
               </tr>
               <tr>
               <td>Brielle Williamson</td>
               <td>Integration Specialist</td>
               <td>New York</td>
               <td>61</td>
               <td>2012/12/02</td>
               <td>$4,525</td>
               </tr>
               <tr>
               <td>Herrod Chandler</td>
               <td>Sales Assistant</td>
               <td>San Francisco</td>
               <td>59</td>
               <td>2012/08/06</td>
               <td>$4,080</td>
               </tr>
               <tr>
               <td>Rhona Davidson</td>
               <td>Integration Specialist</td>
               <td>Edinburgh</td>
               <td>55</td>
               <td>2010/10/14</td>
               <td>$6,730</td>
               </tr>
               <tr>
               <td>Colleen Hurst</td>
               <td>Javascript Developer</td>
               <td>San Francisco</td>
               <td>39</td>
               <td>2009/09/15</td>
               <td>$5,000</td>
               </tr>
               <tr>
               <td>Rhona Davidson</td>
               <td>Integration Specialist</td>
               <td>Edinburgh</td>
               <td>55</td>
               <td>2010/10/14</td>
               <td>$6,730</td>
               </tr>
               <tr>
               <td>Colleen Hurst</td>
               <td>Javascript Developer</td>
               <td>San Francisco</td>
               <td>39</td>
               <td>2009/09/15</td>
               <td>$5,000</td>
               </tr>
               <tr>
               <td>Rhona Davidson</td>
               <td>Integration Specialist</td>
               <td>Edinburgh</td>
               <td>55</td>
               <td>2010/10/14</td>
               <td>$6,730</td>
               </tr>
               <tr>
               <td>Colleen Hurst</td>
               <td>Javascript Developer</td>
               <td>San Francisco</td>
               <td>39</td>
               <td>2009/09/15</td>
               <td>$5,000</td>
               </tr>
               <tr>
               <td>Rhona Davidson</td>
               <td>Integration Specialist</td>
               <td>Edinburgh</td>
               <td>55</td>
               <td>2010/10/14</td>
               <td>$6,730</td>
               </tr>
               <tr>
               <td>Colleen Hurst</td>
               <td>Javascript Developer</td>
               <td>San Francisco</td>
               <td>39</td>
               <td>2009/09/15</td>
               <td>$5,000</td>
               </tr>
               <tr>
               <td>Rhona Davidson</td>
               <td>Integration Specialist</td>
               <td>Edinburgh</td>
               <td>55</td>
               <td>2010/10/14</td>
               <td>$6,730</td>
               </tr>
               <tr>
               <td>Colleen Hurst</td>
               <td>Javascript Developer</td>
               <td>San Francisco</td>
               <td>39</td>
               <td>2009/09/15</td>
               <td>$5,000</td>
               </tr>
            </tbody>
         </table>
      </div>
   </div>        
</div>

<?php
$this->registerJsFile("@web/js/visual-inspection/debugNavigation.js",[
    'depends' => [
        \yii\web\JqueryAsset::class,
    ]
]);
?>