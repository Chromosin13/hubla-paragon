<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KonfigurasiStandar */

$this->title = 'Create Konfigurasi Standar';
$this->params['breadcrumbs'][] = ['label' => 'Konfigurasi Standars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="konfigurasi-standar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
