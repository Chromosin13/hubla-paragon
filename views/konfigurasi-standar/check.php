<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KonfigurasiStandar */
/* @var $form yii\widgets\ActiveForm */
?>

<?= Yii::$app->session->getFlash('success'); ?>


<div class="box box-widget widget-user-2">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-blue">
      <div class="widget-user-image">
        <img class="img-circle" src="images/gears.png" alt="User Avatar">
      </div>
      <!-- /.widget-user-image -->
      <h2 class="widget-user-username">Konfigurasi Standar</h2>
      <h5 class="widget-user-desc">Input Finish Good Kode Item</h5>
    </div>
    <div class="box-body">
    	<div class="row">
	    	<!-- Total -->
			<div class="col-lg-3 col-xs-3">
	          <!-- small box -->
	          <div class="small-box bg-blue">
	            <div class="inner">
	              <h3 id="error_rate"><?=$arr_konfigurasi_standar_planner_stats->jumlah_koitem_fg?></h3>
	              <p>Jumlah Item FG yang sudah dijadwalkan</p>
	            </div>
	            <div class="icon">
	              <i class="large material-icons" style="font-size:100px;">error</i>
	            </div>
	            
	          </div>
	        </div>


			<!-- Unfilled -->
			<div class="col-lg-3 col-xs-3">
	          <!-- small box -->
	          <div class="small-box bg-red">
	            <div class="inner">
	              <h3 id="error_rate"><?=$arr_konfigurasi_standar_planner_stats->jumlah_unfilled_standard?></h3>
	              <p>Jumlah Standar Item FG yang belum dilengkapi dan sudah dijadwalkan</p>
	            </div>
	            <div class="icon">
	              <i class="large material-icons" style="font-size:100px;">error</i>
	            </div>
	            <a href="#" class="small-box-footer">Download Item FG Standar belum lengkap</a>
	          </div>
	        </div>

			<!--Filled  -->
			<div class="col-lg-3 col-xs-3">
	          <!-- small box -->
	          <div class="small-box bg-green">
	            <div class="inner">
	              <h3 id="success_rate"><?=$arr_konfigurasi_standar_planner_stats->jumlah_filled_standard?></h3>
	              <p>Jumlah Standar Item FG yang sudah dilengkapi dan sudah dijadwalkan</p>
	            </div>
	            <div class="icon">
	              <i class="large material-icons" style="font-size:100px;">check_circle</i>
	            </div>
	          </div>
	        </div>
	    </div>
    </div>
	<div class="box-footer">


		<?php $form = ActiveForm::begin(); ?>

	    <?= $form->field($model, 'koitem')->textInput() ?>

	    <?php ActiveForm::end(); ?>

	    <input type="submit" id="submit" value="Add / Check" class="btn btn-primary">

	    
	</div>

	
</div>







<?php
$script = <<< JS

$(document).ready(function(){

	// Disable Enter Key
	$(document).ready(function() {
	$(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
	});

	$("#submit").click(function(){
		
		var koitem = $('#konfigurasistandar-koitem').val();

		// Returns successful data submission message when the entered information is stored in database.
		
		var dataString = 'koitem='+ koitem;
		
		if(koitem=='')
		{
			alert("Mohon isi Koitem");
		}
		else
		{
			
			// AJAX Code To Submit Form.
			
			$.ajax({
			  type: "POST",
			  url: "index.php?r=konfigurasi-standar/add-koitem",
			  cache: false,
			  data: dataString,
			  success: function(result){ alert(result) }
			});
			
		}	

	});
});

JS;
$this->registerJs($script);
?>