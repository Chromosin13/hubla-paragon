<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KonfigurasiStandar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="konfigurasi-standar-form">

	
    
    <div class="box box-widget widget-user-2">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-red">
          <div class="widget-user-image">
            <img class="img-circle" src="images/gears.png" alt="User Avatar">
          </div>
          <!-- /.widget-user-image -->
          <h2 class="widget-user-username"><?=$koitem?></h2>
          <h5 class="widget-user-desc">Konfigurasi Standar</h5>
        </div>
        <div class="box-footer">
	    	<?php
	    		if(empty($arr_jumlah_konfigurasi->jumlah_konfigurasi)){
	    			echo "
	    				<div class=\"box box-primary\">
						  <div class=\"box-header with-border\">
						    <h3 class=\"box-title\">Belum ada data Konfigurasi Standar</h3>
						    <span class=\"label label-primary pull-right\"><i class=\"fa fa-html5\"></i></span>
						  </div><!-- /.box-header -->
						  <div class=\"box-body\">
						    <p>Tambahkan Konfigurasi Standar melalui opsi Generate Konfigurasi</p>".
						    Html::a('Tambah Konfigurasi +', ['add','koi'=>$koi], ['class'=>'btn btn-primary'])."
						  </div><!-- /.box-body -->
						</div>
					";

	    		}else{

	    			echo "
	    				<div class=\"box box-primary\">
						  <div class=\"box-header with-border\">
						    <h3 class=\"box-title\">Terdapat ".$arr_jumlah_konfigurasi->jumlah_konfigurasi." Standar</h3>
						    <span class=\"label label-success pull-right\"><i class=\"fa fa-html5\"></i></span>
						  </div><!-- /.box-header -->
						  <div class=\"box-body\">
						    <p>Pilih Tambah Konfigurasi untuk menambahkan standar atau pilih List Konfigurasi untuk melihat standar existing</p>
						    ".Html::a('Tambah Konfigurasi +', ['add','koi'=>$koi], ['class'=>'btn btn-primary'])."
						    ".Html::a('List Konfigurasi', ['konfigurasi-standar-item/list','ks_id'=>$koi], ['class'=>'btn btn-danger'])."
						  </div><!-- /.box-body -->
						</div>
					";
	    			echo '<button id="fg" class="btn btn-primary">FG <b>('.$arr_jumlah_konfigurasi->jumlah_fg.')</b></button>';
				    echo '<button id="bulk" class="btn btn-danger">Bulk <b>('.$arr_jumlah_konfigurasi->jumlah_bulk.')</b></button>';
	    		}

	    	?>


            

	    	
	    	
			<p></p>
		    
		    <?= Html::a('Check Template Konfigurasi', ['list-proses/index'], ['class'=>'btn btn-warning']) ?>
	    </div>
    </div>


</div>

<?php
$script = <<< JS

// $(document).ready(function(){

// 	// Disable Enter Key
// 	$(document).ready(function() {
// 	$(window).keydown(function(event){
// 	    if(event.keyCode == 13) {
// 	      event.preventDefault();
// 	      return false;
// 	    }
// 	  });
// 	});

// 	$("#submit").click(function(){
		
// 		var koitem = $('#konfigurasistandar-koitem').val();

// 		// Returns successful data submission message when the entered information is stored in database.
		
// 		var dataString = 'koitem='+ koitem;
		
// 		if(koitem=='')
// 		{
// 			alert("Mohon isi Koitem");
// 		}
// 		else
// 		{
			
// 			// AJAX Code To Submit Form.
			
// 			$.ajax({
// 			  type: "POST",
// 			  url: "index.php?r=konfigurasi-standar/validate-koitem",
// 			  cache: false,
// 			  data: dataString,
// 			  success: function(result){ alert(result) }
// 			});
			
// 		}	

// 	});
// });

JS;
$this->registerJs($script);
?>