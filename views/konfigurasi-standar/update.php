<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KonfigurasiStandar */

$this->title = 'Update Konfigurasi Standar: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Konfigurasi Standars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="konfigurasi-standar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
