<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\KonfigurasiStandar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="konfigurasi-standar-form">


	<div class="box box-widget widget-user-2">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-red">
          <div class="widget-user-image">
            <img class="img-circle" src="images/gears.png" alt="User Avatar">
          </div>
          <!-- /.widget-user-image -->
          <h2 class="widget-user-username">Form</h2>
          <h5 class="widget-user-desc">Konfigurasi Standar</h5>
        </div>
        <div class="box-footer">
	    		<div class="row">
					
					<div class="col-md-2">
				    	<label class="control-label">Jenis Item</label>
				    	<select id="jenis-item" class="form-control">
				    	  <option value="">Select Jenis</option>
						  <option value="BULK">BULK</option>
						  <option value="FG">FG</option>
						</select>
				    </div>


				    <div class="col-md-2">
				    	<label class="control-label">MPQ</label>
				    	<input type="number" id="mpq" class="form-control" name="mpq" aria-invalid="false">
				    </div>
				    <div class="hide">
				    	<label class="control-label">Line</label>
				    	<?php
							echo Select2::widget([
								'id'=>'linedummy',
							    'name' => 'linedummy',
							    'data' => $list_nama_line,
							    'options' => [
							        'placeholder' => 'Pilih Nama Line ...'
							    ],
							]);
					    ?>
				    </div>
				    

				</div>


	    
			<br></br>

	    	
	    	<table id="table-proses" class="table table-bordered">
		                <tbody><tr>
		                  <th style="width: 30px">Nourut</th>
		                  <th style="width: 30px">Check</th>
		                  <th style="width: 50px">Jenis Proses</th>
		                  <th style="width: 40px">Jumlah Operator</th>
		                  <th style="width: 150px">Line</th>
		                  <th style="width: 120px">Zona</th>
		                  <th style="width: 50px">Print Type</th>
		                  <th style="width: 50px">Output</th>
		                  <th style="width: 50px">Kode Streamline</th>
		                  <th style="width: 50px">Standar Leadtime</th>
		                  <th style="width: 50px">PDT</th>		                  
		                </tr>
		                
		              </tbody>
		    </table>

		    <!-- 
			<br></br>
	    		<button id="button_add" class="btn btn-primary btn-block">Add +</button>
	    	<br></br>
	    	<table id="table-data" class="table table-bordered">
		                <tbody><tr>
		                  <th>MPQ (Pcs)</th>
		                  <th>Jumlah Operator</th>
		                  <th>Line</th>
		                  <th>Zona</th>
		                  <th>Print Flag</th>
		                  <th>Output</th>
		                  <th>Kode Streamline</th>
		                  <th>Standar Leadtime</th>
		                  <th>PDT</th>
		                  <th style="width: 40px"></th>
		                </tr>
		                
		              </tbody>
		    </table>


		    <p></p>
		    <input type="submit" id="submit" class="btn btn-primary">

		    <p></p> -->
		    <button id="log-submit" class="btn btn-primary btn-block">Add +</button>

		    

	    </div>
    </div>
	

    




<!--     <button id="button_add_jumlah_operator" class="btn btn-primary">+</button>
    <p></p>

    <table id="table-jumlah-operator" class="table table-bordered">
                <tbody><tr>
                  <th>Jumlah Operator</th>
                  <th style="width: 40px"></th>
                </tr>
                
              </tbody></table>


    
    <input type="number" id="line" class="form-control" name="line" aria-invalid="false">
    
    

    <p></p>
    <button id="button_add_line" class="btn btn-primary">+</button>
    <p></p>

    <table id="table-line" class="table table-bordered">
                <tbody><tr>
                  <th>Nama Line</th>
                  <th style="width: 40px"></th>
                </tr>
                
              </tbody></table>

    <br></br> -->




    

</div>

<?php
$script = <<< JS

$("#jenis-item").change(function(){


	$('#table-proses tr:gt(0)').remove()

	var val = $('#jenis-item').val();

	$.ajax({
	  type: "POST",
	  url: "index.php?r=konfigurasi-standar/get-proses",
	  cache: false,
	  data: {data:val},
	  dataType: 'json',
	  success: function(result){  
	  	// console.log(result);

	  	$.each(result,function(i,listproses){
	    $("#table-proses tbody").append(
	        "<tr>"
	            +"<td id='nourut'></td>"
	            +"<td><input type='checkbox'></td>"
	            +"<td id='jenis-proses'>"+listproses.proses+"</td>"
	            +"<td id='fill-jumlah-operator'></td>"
	            +"<td id='fill-line'></td>"
	            +"<td id='fill-zona'></td>"
	            +"<td id='fill-print-type'></td>"
	            +"<td id='fill-output'></td>"
	            +"<td id='fill-kode-streamline'></td>"
	            +"<td id='fill-standar-leadtime'></td>"
	            +"<td id='fill-pdt'></td>"
	        +"</tr>" )
	    })

	  }
	});
});





// Increment variable for nourut
var i=0;

$('body').on('change','#table-proses input[type="checkbox"]',function(){
	
	//your stuff
		var clone = $(this).closest("tr");
	// get cell positions Input Field for the same rows
		var nourut = clone.find("#nourut");
		var fill_jumlah_operator = clone.find("#fill-jumlah-operator");
		var fill_line = clone.find("#fill-line");
		var fill_zona = clone.find("#fill-zona");
		var fill_print_type = clone.find("#fill-print-type");
		var fill_output = clone.find("#fill-output");
		var fill_kode_streamline = clone.find("#fill-kode-streamline");
		var fill_standar_leadtime = clone.find("#fill-standar-leadtime");
		var fill_pdt = clone.find("#fill-pdt");

	if( $(this).is(":checked")) {
		i=i+1;
		// populate with input field
	    	nourut.text(i);
	    	fill_jumlah_operator.html('<input type="number" id="jumlah_operator" class="form-control" name="jumlah_operator" aria-invalid="false">');
	    	

	    	fill_line.html('<div class="line-select" id="line" style="width: 100%"></div>');
	    	
	    	$(document).ready(function(){
			    $(".line-select").select2({ 

			    	ajax: {
				        url: "index.php?r=konfigurasi-standar/get-line",
				        dataType: "json",
				        type: "GET",
				        data: function (term) {
				            return {
				                term: term
				            };
				        },
				        results: function (data) {
				            return {
				                results: $.map(data, function (item) {
				                    return {
				                        text: item.nama_line,
				                        id: item.id
				                    }
				                })
				            };
				        },
				        cache: true
				    }

				});
			});

			fill_zona.html('<input type="text" id="zona" class="form-control" name="zona" aria-invalid="false">');
			fill_print_type.html('<select id="print_type" class="form-control"><option value="">Pilih Jenis Print</option><option value="PRINTING">PRINTING</option><option value="LABELING">LABELING</option><option value="PRINTING & LABELING">PRINTING & LABELING</option></select>');
			fill_output.html('<input type="number" id="output" class="form-control" name="output" aria-invalid="false">');
			fill_kode_streamline.html('<input type="text" id="kode_streamline" class="form-control" name="kode_streamline" aria-invalid="false">');
			fill_standar_leadtime.html('<input type="number" id="standar_leadtime" class="form-control" name="standar_leadtime" aria-invalid="false">');
			fill_pdt.html('<input type="number" id="pdt" class="form-control" name="ptdt" aria-invalid="false">');
	
	}else{
		i=i-1;
		// Empty cell field
			// var clone = $(this).closest("tr");
			// var nourut = clone.find("#nourut");
	    	nourut.empty();
	    	fill_jumlah_operator.empty();
	    	fill_line.empty();
	    	fill_zona.empty();
	    	fill_print_type.empty();
	    	fill_output.empty();
	    	fill_kode_streamline.empty();
	    	fill_standar_leadtime.empty();
	    	fill_pdt.empty();	
	}
});



// Add Button
	$('#button_add').click(function(){
	    var mpq = $('#mpq').val();
	    var jumlah_operator = $('#jumlah_operator').val();
	    var line = $('#line').val();
	    var zona = $('#zona').val();
	    var print_flag = $('#print_flag').val();
	    var output = $('#output').val();
	    var kode_streamline = $('#kode_streamline').val();
	    var standar_leadtime = $('#standar_leadtime').val();
	    var pdt = $('#pdt').val();
	    $('#table-data').append('<tr><td>'+mpq+'</td><td>'+jumlah_operator+'</td><td>'+line+'</td><td>'+zona+'</td><td>'+print_flag+'</td><td>'+output+'</td><td>'+kode_streamline+'</td><td>'+standar_leadtime+'</td><td>'+pdt+'</td><td><button class="btn btn-danger btn-block">-</button></td></tr>');
	});

	$('#button_add_jumlah_operator').click(function(){
	    var jumlah_operator = $('#jumlah_operator').val();
	    $('#table-jumlah-operator').append('<tr><td>'+jumlah_operator+'</td><td><button class="btn btn-danger btn-block">-</button></td></tr>');
	});

	$('#button_add_line').click(function(){
	    var line = $('#line').val();
	    $('#table-line').append('<tr><td>'+line+'</td><td><button class="btn btn-danger btn-block">-</button></td></tr>');
	});


// Remove Button
	$('#table-data').on('click', ':button', function(){
	    $(this).closest ('tr').remove ();
	});

	$('#table-jumlah-operator').on('click', ':button', function(){
	    $(this).closest ('tr').remove ();
	});

	$('#table-line').on('click', ':button', function(){
	    $(this).closest ('tr').remove ();
	});


$("#submit").click(function(){

	var koitem = '$koitem';

	var table = document.getElementById("table-data");
	var tableArr = [];
	for ( var i = 1; i < table.rows.length; i++ ) {
	    tableArr.push({
	        mpq: table.rows[i].cells[0].innerHTML,
	        jumlah_operator: table.rows[i].cells[1].innerHTML,
	        line: table.rows[i].cells[2].innerHTML,
	        zona: table.rows[i].cells[3].innerHTML,
	        print_flag: table.rows[i].cells[4].innerHTML,
	        output: table.rows[i].cells[5].innerHTML,
	        kode_streamline: table.rows[i].cells[6].innerHTML
	    });
	}

	var data_array = JSON.stringify(tableArr);

	// console.log(data_array);
	// $.ajax({
	//   type: "POST",
	//   url: "index.php?r=konfigurasi-standar/postdata",
	//   cache: false,
	//   data: {data:data_array,koitem:koitem},
	//   success: function(result){ window.location = "index.php?r=konfigurasi-standar-item/list&ks_id="+result; }
	// });
	
});

$("#log-submit").click(function(){

	var tableProses = [];
	var koitem = '$koitem';

	// Get Parent Values (jenis item and mpq)
	var jenis_item = $('#jenis-item').val();
	var mpq = $('#mpq').val();


	$('#table-proses > tbody  > tr:gt(0)').each(function() {
		var nourut = $(this).find("#nourut").html();
		var jenis_proses = $(this).find("#jenis-proses").html(); 
		var jo = $(this).find("input#jumlah_operator").val();
		var line = $(this).find("div#line").val();
		var zona = $(this).find("input#zona").val();
		var print_type = $(this).find("select#print_type").val();
		var output = $(this).find("input#output").val();
		var kode_streamline = $(this).find("input#kode_streamline").val();
		var standar_leadtime = $(this).find("input#standar_leadtime").val();
		var pdt = $(this).find("input#pdt").val();

		if(nourut){
			tableProses.push({
				jenis_item: jenis_item,
				mpq: mpq,
				nourut: nourut,
				jenis_proses: jenis_proses,
				jumlah_operator: jo,
				line: line,
				zona: zona,
				print_type: print_type,
				output: output,
				kode_streamline: kode_streamline,
				standar_leadtime: standar_leadtime,
				pdt: pdt
			});			
		}

		
	});

	var data_array = JSON.stringify(tableProses);

	console.log(data_array);

	if (confirm("Anda akan menambahkan "+ i +" standar konfigurasi, Lanjutkan?") == true) {
  		 $.ajax({
		  type: "POST",
		  url: "index.php?r=konfigurasi-standar/posttable",
		  cache: false,
		  data: {data:data_array,koitem:koitem},
		  success: function(result){ 
		  	//window.location = "index.php?r=konfigurasi-standar-item/list&ks_id="+result; 
		  	window.location = "index.php?r=konfigurasi-standar/home&koi="+result;
		  	// console.log(result);
		  }
		});
	} 


	



	
});
JS;
$this->registerJs($script);
?>