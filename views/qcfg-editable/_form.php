<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\QcfgEditable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qcfg-editable-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'snfg')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'snfg_komponen')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'lanjutan_split_batch')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'jenis_periksa')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'status')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'waktu')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'posisi')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'timestamp')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'aql')->textInput() ?>

    <?= $form->field($model, 'filling_kesesuaian_bulk')->textInput() ?>

    <?= $form->field($model, 'filling_kesesuaian_packaging_primer')->textInput() ?>

    <?= $form->field($model, 'filling_netto')->textInput() ?>

    <?= $form->field($model, 'filling_seal')->textInput() ?>

    <?= $form->field($model, 'filling_leakage')->textInput() ?>

    <?= $form->field($model, 'filling_warna_olesan')->textInput() ?>

    <?= $form->field($model, 'filling_warna_performance')->textInput() ?>

    <?= $form->field($model, 'filling_uji_ayun')->textInput() ?>

    <?= $form->field($model, 'filling_uji_oles')->textInput() ?>

    <?= $form->field($model, 'filling_uji_ketrok')->textInput() ?>

    <?= $form->field($model, 'filling_drop_test')->textInput() ?>

    <?= $form->field($model, 'filling_rub_test')->textInput() ?>

    <?= $form->field($model, 'filling_identitas_packaging_primer')->textInput() ?>

    <?= $form->field($model, 'filling_identitas_stc_bottom')->textInput() ?>

    <?= $form->field($model, 'packing_kesesuaian_packaging_sekunder')->textInput() ?>

    <?= $form->field($model, 'packing_qty_inner_box')->textInput() ?>

    <?= $form->field($model, 'packing_identitas_unit_box')->textInput() ?>

    <?= $form->field($model, 'packing_identitas_inner_box')->textInput() ?>

    <?= $form->field($model, 'packing_performance_segel')->textInput() ?>

    <?= $form->field($model, 'packing_posisi_packing')->textInput() ?>

    <?= $form->field($model, 'paletting_qty_karton_box')->textInput() ?>

    <?= $form->field($model, 'paletting_identitas_karton_box')->textInput() ?>

    <?= $form->field($model, 'retain_sample')->textInput() ?>

    <?= $form->field($model, 'qty')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
