<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use miloschuman\highcharts\Highmaps;
use miloschuman\highcharts\Highstock;
use miloschuman\highcharts\SeriesDataHelper;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QcfgEditableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Qcfg Editables';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qcfg-editable-index" >

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php 

        $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
                                'id',
                                'snfg',
                                'snfg_komponen',
                                'lanjutan_split_batch',
                                'jenis_periksa',
                                'status',
                                'state',
                                'waktu',
                                'posisi',
                                'timestamp',
                                'aql',
                                'filling_kesesuaian_bulk',
                                'filling_kesesuaian_packaging_primer',
                                'filling_netto',
                                'filling_seal',
                                'filling_leakage',
                                'filling_warna_olesan',
                                'filling_warna_performance',
                                'filling_uji_ayun',
                                'filling_uji_oles',
                                'filling_uji_ketrok',
                                'filling_drop_test',
                                'filling_rub_test',
                                'filling_identitas_packaging_primer',
                                'filling_identitas_stc_bottom',
                                'packing_kesesuaian_packaging_sekunder',
                                'packing_qty_inner_box',
                                'packing_identitas_unit_box',
                                'packing_identitas_inner_box',
                                'packing_performance_segel',
                                'packing_posisi_packing',
                                'paletting_qty_karton_box',
                                'paletting_identitas_karton_box',
                                'retain_sample',
                                'qty',

        ['class' => 'yii\grid\ActionColumn'],
        ];

        echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_PDF => false,
                                ExportMenu::FORMAT_HTML => false
                          ]        
    ]);?>
    <p>

    <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'primary', 'heading'=>'Form Editable QC FG'],
            'columns'=>[
                ['class' => 'kartik\grid\ActionColumn','template' => '{update}',],
                ['class'=>'kartik\grid\SerialColumn'],
                            'id',
                            'snfg',
                            'snfg_komponen',
                            'lanjutan_split_batch',
                            'jenis_periksa',
                            'status',
                            'state',
                            'waktu',
                            'posisi',
                            'timestamp',
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'aql',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'filling_kesesuaian_bulk',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'filling_kesesuaian_packaging_primer',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'filling_netto',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'filling_seal',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'filling_leakage',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'filling_warna_olesan',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'filling_warna_performance',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'filling_uji_ayun',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'filling_uji_oles',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'filling_uji_ketrok',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'filling_drop_test',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'filling_rub_test',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'filling_identitas_packaging_primer',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'filling_rub_test',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'filling_identitas_stc_bottom',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'packing_kesesuaian_packaging_sekunder',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'packing_qty_inner_box',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'packing_identitas_unit_box',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'packing_identitas_inner_box',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'packing_performance_segel',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'packing_posisi_packing',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'paletting_qty_karton_box',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'paletting_identitas_karton_box',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'retain_sample',
                            ],
                            [
                                    'class'=>'kartik\grid\EditableColumn',
                                    'attribute'=>'qty',
                            ],
             ],
        ]);
    
    ?>

</div>
