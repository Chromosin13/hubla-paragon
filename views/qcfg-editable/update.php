<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\QcfgEditable */

$this->title = 'Update Qcfg Editable: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Qcfg Editables', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="qcfg-editable-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
