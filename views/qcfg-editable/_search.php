<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\QcfgEditableSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qcfg-editable-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'snfg_komponen') ?>

    <?= $form->field($model, 'lanjutan_split_batch') ?>

    <?= $form->field($model, 'jenis_periksa') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'waktu') ?>

    <?php // echo $form->field($model, 'posisi') ?>

    <?php // echo $form->field($model, 'timestamp') ?>

    <?php // echo $form->field($model, 'aql') ?>

    <?php // echo $form->field($model, 'filling_kesesuaian_bulk') ?>

    <?php // echo $form->field($model, 'filling_kesesuaian_packaging_primer') ?>

    <?php // echo $form->field($model, 'filling_netto') ?>

    <?php // echo $form->field($model, 'filling_seal') ?>

    <?php // echo $form->field($model, 'filling_leakage') ?>

    <?php // echo $form->field($model, 'filling_warna_olesan') ?>

    <?php // echo $form->field($model, 'filling_warna_performance') ?>

    <?php // echo $form->field($model, 'filling_uji_ayun') ?>

    <?php // echo $form->field($model, 'filling_uji_oles') ?>

    <?php // echo $form->field($model, 'filling_uji_ketrok') ?>

    <?php // echo $form->field($model, 'filling_drop_test') ?>

    <?php // echo $form->field($model, 'filling_rub_test') ?>

    <?php // echo $form->field($model, 'filling_identitas_packaging_primer') ?>

    <?php // echo $form->field($model, 'filling_identitas_stc_bottom') ?>

    <?php // echo $form->field($model, 'packing_kesesuaian_packaging_sekunder') ?>

    <?php // echo $form->field($model, 'packing_qty_inner_box') ?>

    <?php // echo $form->field($model, 'packing_identitas_unit_box') ?>

    <?php // echo $form->field($model, 'packing_identitas_inner_box') ?>

    <?php // echo $form->field($model, 'packing_performance_segel') ?>

    <?php // echo $form->field($model, 'packing_posisi_packing') ?>

    <?php // echo $form->field($model, 'paletting_qty_karton_box') ?>

    <?php // echo $form->field($model, 'paletting_identitas_karton_box') ?>

    <?php // echo $form->field($model, 'retain_sample') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
