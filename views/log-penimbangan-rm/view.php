<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LogPenimbanganRm */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Log Penimbangan Rms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-penimbangan-rm-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nomo',
            'no_formula',
            'no_revisi',
            'naitem_fg',
            'datetime_start',
            'datetime_stop',
            'datetime_write',
            'kode_bb',
            'nama_bb',
            'kode_olah',
            'siklus',
            'weight',
            'satuan',
            'sediaan',
            'cycle_time:datetime',
            'timbangan',
            'operator',
            'no_timbangan',
            'realisasi',
            'satuan_realisasi',
            'status',
        ],
    ]) ?>

</div>
