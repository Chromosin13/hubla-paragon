<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LogPenimbanganRm */

$this->title = 'Update Log Penimbangan Rm: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Log Penimbangan Rms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="log-penimbangan-rm-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
