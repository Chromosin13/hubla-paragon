<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LogPenimbanganRm */

$this->title = 'Create Log Penimbangan Rm';
$this->params['breadcrumbs'][] = ['label' => 'Log Penimbangan Rms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-penimbangan-rm-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
