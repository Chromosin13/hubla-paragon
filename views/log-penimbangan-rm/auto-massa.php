<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use app\models\LogPenimbanganRm;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$script = <<< JS



JS;
$this->registerJs($script);
?>

<style>
.wegihing-layout {
    position:relative;
    margin:auto;
}

.weighing-layout .wl-header{
    position:absolute;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%);
    margin-top:5vh;
    text-align:center;
    font-size: 3.8vh;
    font-family: Exo; 
}

.weighing-layout .wl-container{
    background : #ffff7f;
    width: 27vh;
    height:36vh;
    position: absolute;
    top: 65%;
    left: 30%;
    margin-right: -70%;
    transform: translate(-30%, -70%);
    border-style: solid;
    border-left:6px solid black;
    border-bottom:6px solid black;
    border-right:6px solid black;
    border-top:6px solid white;
    border-radius: 0 0 10px 10px;    
    display: inline-block;
  }

.weighing-layout .wl-container .wl-fill {
    margin-bottom: 1px;
    width: 100%;
    height: 100%;
    background: #e0e0e0;
    -webkit-transition: height 1s; 
    transition: height 1s;
  }
  
.weighing-layout .wl-container .wl-fill:hover {
    height: 20%;
}
  
.weighing-layout .wl-container .hl {
    position:absolute;
    top:10%;
    border-top: 3px solid red;
    width: 130%;
  }
  
.weighing-layout  .wl-container .hl2 {
    position:absolute;
    top:13%;
    border-top: 3px solid green;
    width: 30%;
    margin-left:100%;
  }

  .weighing-layout  .wl-container .hl3 {
    position:absolute;
    top:16%;
    border-top: 3px solid yellow;
    width: 30%;
    margin-left:100%;
  }

  .weighing-layout  .wl-container .wl-textbox{
    position:absolute;
    border: 2px solid black;
    border-radius: 3px;
    text-align: center;
    margin-left:140%;
    width:70%;
    height:22%;
  }

  .weighing-layout  .wl-container .wl-textbox .pl{
    padding:18px 15px 15px 15px; 
    font-family: Exo;
    white-space: nowrap;
    text-align:center;
    font-size: 4vh;
    /*font-weight:bold;*/
  }

  .weighing-layout  .wl-container .wl-text{
    position:absolute;
    font-family: Exo;
    font-size: 2vh;
    margin-left:140%;
    white-space: nowrap;
  }

  .weighing-layout  .wl-container btn btn-hero-success {
    position:absolute;
    margin-left:140%;
    width:80%;
  }
  
  .box5{
      background : #FFFFFF;
      height: 38px;
      width:110px;
    position:absolute;
    border-style: solid;
    border:3px solid black;
    border-radius:2px;
    display: inline-block;
    margin-left:630px;
  }
  
  .box6{
      background : #42db7d;
      height: 38px;
      width:110px;
    position:absolute;
    border-radius:10px;
    display: inline-block;
    margin-left:630px;
    box-shadow: 2px 3px #888888;
  }
  
  .box3{
      background : #FFFFFF;
      height: 42px;
      width:110px;
    position:absolute;
    border-style: solid;
    border:3px solid black;
    border-radius:5px;
    display: inline-block;
    margin-left:120px;
    top:270px;
  }
  
  .box4 p{
    position:absolute;
    display: inline-block;
    left:550px;
    top:400px;
  }

.bawahan {
   position: fixed;
   border-top:2px solid #D3D3D3;
   left: 0;
   bottom: 0;
   width: 100%;
   padding:15px 0;
   background-color: white;
   color: white;
   text-align: center;
}
</style>

<script type="text/javascript">

var timbangan = '<?php echo $timbangan; ?>';
var massa = 0;
var weight = <?php echo $weight; ?>;
var id = <?php echo $id; ?>;
var wo = <?php echo $wo; ?>;
var decimal_digit = <?php echo $decimal_digit; ?>;
var galat = <?php echo $galat; ?>;
var log = '<?php echo $log ?>';
var ip = '<?php echo $raspi_ip; ?>';
var ipUrl = '<?php echo Yii::getAlias('@ipUrl'); ?>';
var dnsUrl = '<?php echo Yii::getAlias('@dnsUrl'); ?>';


  url = "http://"+ip+":6900";
  console.log(url);


var j = setInterval(function() {
    $.getJSON(url,function(result){
        j = result[0];      
        massa = j.weight;
        animation = massa/weight*87;
        massaFix = parseFloat(massa).toFixed(decimal_digit);
        
        // var over = weight+0.05;
        // var ok = weight;
        // var less = weight-0.05;

        // var btsTengah = 13;
        // var btsAtas = over/ok*btsTengah;
        // var btsBawah = less/ok*btsTengah;

        // document.getElementById("btsAtas").style.top = btsAtas+"%";
        // document.getElementById("btsTengah").style.top = btsTengah+"%";
        // document.getElementById("btsBawah").style.top = btsBawah+"%";
        if (massaFix>weight+galat){
          document.getElementById("massa").style.color = "red";
        } else if (massaFix<weight-galat) {
          document.getElementById("massa").style.color = "black";
        } else {
          document.getElementById("massa").style.color = "green";
        }

        if (animation<=100){
          document.getElementById("fill").style.height = (100-animation)+"%";
        } else {
          document.getElementById("fill").style.height = "0%";
        }
        $("#massa").text(massaFix);
    });
}, 1000);

function update(){
  document.getElementById("submitbutton").disabled = true;
  //var realisasi = 0;

  $.getJSON(url,function(result){
    
        j = result[0];

        realisasi = j.weight;

        $.get('index.php?r=log-penimbangan-rm/update-log',{ id:id, wo : wo, realisasi : realisasi, log : log, log_realisasi : 'animasi'},function(data2){ 

          var data2 = $.parseJSON(data2);
          //window.alert(data2);
          // console.log(data2);
          if(data2.status=="success"){
            window.location = dnsUrl+"log-penimbangan-rm/task&id="+id;
          } else if (data2.status=="not-success"){
            window.location = "index.php?r=log-penimbangan-rm/auto-massa&id="+id+"&wo="+wo+"&log="+log;
          } else if (data2.status=="massa-null"){
            if (window.alert('Selisih massa dengan yang sebelumnya 0, data tidak bisa ditambahkan')){}else{
              document.getElementById("submitbutton").disabled = false;
            }
          } else {
            if (window.alert("Massa tidak sesuai standar")){}else{
              document.getElementById("submitbutton").disabled = false;
            }
          }
        });
    });


  //window.alert(data2);
}

function confirm(){
  document.getElementById("main-box").style.opacity = "0.1";
  document.getElementById("modal").style.display = "block";
}

function yes(){
  document.getElementById("add-yes").disabled = true;
  //window.location = "http://factory.pti-cosmetics.com/flowreport_gustav/web/index.php?r=log-penimbangan-rm/scan-bb&id="+id+"&wo="+wo;
  $.getJSON(url,function(result){
    j = result[0];
    realisasi = j.weight;

    $.get('index.php?r=log-penimbangan-rm/add-batch',{ id : id, wo : wo, realisasi : realisasi, log : log, log_realisasi : 'animasi'},function(data2){
      var data2 = $.parseJSON(data2);
      //window.alert(data2);
      if(data2.status=="success"){
        window.location = dnsUrl+"log-penimbangan-rm/scan-bb&id="+id+"&wo="+wo;
      } else if (data2.status=='massa-null'){
        if (window.alert('DATA BELUM TERINPUT! \nSelisih massa dengan batch sebelumnya 0, Klik PRINT pada timbangan sebelum tambah batch')){}
          else{document.getElementById("add-yes").disabled = false;}
        // window.location = dnsUrl+"log-penimbangan-rm/scan-bb&id="+id+"&wo="+wo;
      }else{
        if (window.alert('Data gagal di update')){}
          else{document.getElementById("add-yes").disabled = false;}
        // window.location = "index.php?r=log-penimbangan-rm/auto-massa&id="+id+"&wo="+wo+"&log="+log;
      }
    });
  });
}

function no(){
  document.getElementById("main-box").style.opacity = "1";
  document.getElementById("modal").style.display = "none";
}

function timbangmanual(){

  window.location = ipUrl+"log-penimbangan-rm/input-massa&id="+id+"&wo="+wo+"&log="+log;
}

</script>

<div class="box box-widget widget-user" id="main-box" style="height:600px;">
  <div class="box-body" style="padding-top:0;">
    <div class="col-md-12" style="text-align: center;">
      <h3 style="color:#9F9F9F;">Tekan tombol <b>PRINT</b> pada timbangan untuk update nilai penimbangan</h3>
      <!-- /input-group -->
    </div>
    <hr style="height:1px;border:none;color:#333;background-color:#333; margin-bottom:0px;" />
    <div class="weighing-layout" style="height:90%; margin-left:20px; margin-top:0px;">
      <div class="wl-header"><b><?php echo $nama_bb;?></b><hr style="margin:0px;padding:0px;">
      <h4 class="widget-user-desc" style="color:black; margin-bottom: 0px; margin-top:0px;">Timbangan : <b><?php echo $timbangan;?></b></h4></div>
      <div class="wl-container" style="margin-left:-50px; margin-top:25px;">
        <div class="wl-fill" id="fill"></div>
        <div class="hl" name="btsAtas" id="btsAtas"></div>
        <div class="hl" style="top:13%; border-top: 3px solid green;" name="btsTengah" id="btsTengah"></div>
        <div class="hl" style="top:16%; border-top: 3px solid yellow;" name="btsBawah" id="btsBawah"></div>
        <div class="wl-textbox" style="background: #f0f0f0; top:2%;">
            <div style="position:absolute; top:0px; left:4px;">Standard :</div>
            <div class="pl" style="color:blue;"><b><?php echo $weight;?></b> <?php echo $satuan;?></div>
        </div>
        <!-- <div class="wl-textbox" style="background: #ffff7f; top:25%;"> -->
        <div class="wl-textbox" style="background: #ffff6f; top:25%;">
          <div style="position:absolute; top:0px; left:4px;">Realisasi :</div>
            <div class="pl"><b id="massa" name="massa">0</b> <?php echo $satuan;?></div>
        </div>
        <div class="wl-text" style="top:55%;">Tare weight : - Kg</div>
        <div class="wl-text" style="top:65%;">Time : </div>
        <div class="wl-text" style="margin-left:165%; top:65%; font-weight:bold;" id="time">00:00:00</div>
        <button id="addbatch" onclick="confirm()" class="wl-text btn btn-warning" style="top:87%; "><b>TAMBAH BATCH</b></button>
        <br>
        <button id="timbang-manual" class="wl-text btn bg-maroon" data-toggle="modal" data-target="#warningModal" style="top:110%; "><b>INPUT MANUAL</b></button>
        <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#warningModal"> -->
          <!-- Launch demo modal -->
        <!-- </button> -->
      </div>
    </div>
  </div>
</div>

<!-- <div class="box-footer" style="margin-top: 0px; padding-top:0px;">
  <div class="row" style="margin-bottom :0px; margin-top :0px; padding-top:0px;">
    <h4 style='text-align:center; margin-bottom:0px;'>History Split Batch</h4>
    <div style='border: 2px solid #DDD; padding: 8px; width:95%; border-radius:5px; display: block; margin-left: auto; margin-right: auto; margin-bottom:0px;'>
      <table>
        <thead>
          <tr>
            <th class='header'>No</th>
            <th class='header'>Kode BB</th>
            <th class='header'>Kode Internal</th>
            <th class='header'>No. Batch</th>
            <th class='header'>Qty</th>
            <th class='header'>UoM</th>
          </tr>
        </thead>
        <tbody>
    <?php if($is_split == 0) {
      echo "<tr>
              <td class='header'>-</td>
              <td class='header'>-</td>
              <td class='header'>No Record</td>
              <td class='header'>No Record</td>
              <td class='header'>-</td>
              <td class='header'>-</td>
            </tr>";
    } else {
      for ($x = 0; $x < $splitQty; $x++) {
        echo '<tr>';
          echo '<td class="text-center">'; echo $x+1; echo '</td>';
          echo '<td class="text-center">'; echo $split_data[$x]['kode_bb']; echo '</td>';
          echo '<td class="text-center">'; echo $split_data[$x]['kode_internal']; echo '</td>';
          echo '<td class="text-center">'; echo $split_data[$x]['no_batch']; echo '</td>';
          echo '<td class="text-center">'; echo $split_data[$x]['realisasi']; echo '</td>';
          echo '<td class="text-center">'; echo $split_data[$x]['satuan_realisasi']; echo '</td>';
        echo '</tr>';
      }
    } 
    ?>
        </tbody>
      </table>
    </div>
  </div>
</div> -->



<div class="alert" role="alert" id="result"></div>
<div class="bawahan">
    <div class="row">
      <div style="width:100%; padding:0 30px;">
        <button onclick="update()"id="submitbutton" class="btn btn-block btn-info"><b>SUBMIT</b></button>
      </div>
    </div>
</div>

<div id="modal" style="text-align: center; position: absolute; z-index: 10; top: 50%; left: 50%; transform: translate(-50%, -50%); background-color: white; width:70%; height:22vh; border-radius: 5px; display: none;">
  <div id="modal-header" style="background-color: #5DADE2; margin-bottom: 0px; border-top-left-radius: 5px; border-top-right-radius: 5px;">
    <p style="text-align:left; padding-left : 20px; padding-top : 6px; font-size:1.5em;"><b>Confirmation</b></p>
  </div>
  <div id="modal-content" style="height:30%;">
    <p style="padding-top:20px; font-size:1.2em;">Apakah anda yakin untuk menambah batch?</p>
  </div>
  <hr>
  <div id="modal-footer" style="margin-top:0px;">
    <button onclick="no()" class="btn btn-danger" data-dismiss="modal" style="position: absolute; top: 83%; left: 35%; transform: translate(-50%, -50%); width:20%;">
      <b>TIDAK</b>
    </button>
    <button id="add-yes" onclick="yes()" class="btn btn-success" data-dismiss="modal" style="position: absolute; top: 83%; left: 65%; transform: translate(-50%, -50%); width:20%;">
      <b>YA</b>
    </button>

  </div>
</div>

<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="warningLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="warningLabel">
          <p style="text-align: center;font-weight: bold;font-size: 24px;"><img src="images/forbidden.png" alt="Forbidden" style="width:30px;height:30px;"> STOP <img src="images/forbidden.png" alt="Forbidden" style="width:30px;height:30px;"></p>
        </h5>
      </div>
      <div class="modal-body">
         
        <p style="text-align: center;font-weight: bold;font-size: 20px;">HUBUNGI LEADER SEBELUM MELANJUTKAN</p>
        <p style="text-align: center;font-weight: bold;font-size: 20px;">INPUT MANUAL HASIL TIMBANG!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" onclick="timbangmanual()" >Oke</button>
      </div>
    </div>
  </div>
</div>


<?php
$script = <<<JS
  var raspi_ip = "$raspi_ip";

  $.getJSON("http://"+raspi_ip+":6900/?reset_massa",function(result){

      if (result.status == 1){
        alert("Berhasil me-Reset massa!");
      } else {
        alert("Gagal me-Reset massa!");
      }
      

  });
JS;

$this->registerJs($script);
?>