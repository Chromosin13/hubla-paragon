<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogPenimbanganRmSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Penimbangan Rms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-penimbangan-rm-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Log Penimbangan Rm', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nomo',
            'no_formula',
            'no_revisi',
            'naitem_fg',
            // 'datetime_start',
            // 'datetime_stop',
            // 'datetime_write',
            // 'kode_bb',
            // 'nama_bb',
            // 'kode_olah',
            // 'siklus',
            // 'weight',
            // 'satuan',
            // 'sediaan',
            // 'cycle_time:datetime',
            // 'timbangan',
            // 'operator',
            // 'no_timbangan',
            // 'realisasi',
            // 'satuan_realisasi',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
