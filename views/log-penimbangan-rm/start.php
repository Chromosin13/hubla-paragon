<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogPenimbanganRmSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Log Penimbangan Rms';
//$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.lds-circle {
  /*display: inline-block;*/
  transform: translateZ(1px);
}
.lds-circle > div {
  /*display: inline-block;*/
  position : absolute;
  left: 50%;
  margin-left: -50px;
  top: 50%;
  margin-top: -50px;
  width: 8vh;
  height: 8vh;
  border-radius: 20%;
  /*background: #fff;*/
  animation: lds-circle 3s cubic-bezier(0, 0.2, 0.8, 1) infinite;
}
@keyframes lds-circle {
  0%, 100% {
    animation-timing-function: cubic-bezier(0.5, 0, 1, 0.5);
  }
  0% {
    transform: rotateY(0deg);
  }
  50% {
    transform: rotateY(900deg);
    animation-timing-function: cubic-bezier(0, 0.5, 0.5, 1);
  }
  100% {
    transform: rotateY(1800deg);
  }
}
</style>

    <div style="height:70vh" class="lds-circle">
      <!-- <h1 style="text-align:center; top:50%;">Loading, please wait..</h1> -->
      <div>
        <img style="position: absolute; top: 70%; left: 50%; margin-right: -50%; transform: translate(-50%, -50%); height:15vh;" src="../web/images/logo-pti-segitiga.png" alt="Error">
      </div>
    </div>

<?php
$script = <<< JS
setTimeout(function () {
   window.location.href = "?r=log-penimbangan-rm/task&id=$id"; //will redirect to your blog page (an ex: blog.html)
}, 3000); //will call the function after 2 secs.

JS;
$this->registerJs($script);
?>