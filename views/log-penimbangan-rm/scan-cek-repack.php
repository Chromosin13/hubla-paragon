<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowAnalyst;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- CSS For Video Selector -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="ZXing for JS">

  <title>ZXing TypeScript | Decoding from camera stream</title>

  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">

<style>
.column {
  float: left;
  width: 50%;
}

.accordion {
  
  color: white;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  transition: 0.4s;
}

.active, .accordion:hover {
  background-color: #ccc;
}

.accordion:after {
  content: '\002B';
  color: white;
  font-weight: bold;
  float: right;
  margin-left: 5px;
}

.active:after {
  content: "\2212";
  color: white;
}

.panel {
  padding: 0 18px;
  background-color: white;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
}

.header{
  padding :1px;
  text-align: center;
}

</style>
</head>

<body oncopy="return false" oncut="return false" onpaste="return false">
<!-- Body -->
<div class="box box-widget widget-user">
  <!-- Add the bg color to the header using any of the bg-* classes -->
  <div class="widget-user-header bg-red-gradient" style="text-align: center; display: block; margin-left: auto; margin-right: auto;">
    <!-- <h3 class="widget-user-username"><b>Scan Kode Bahan Baku</b></h3>
    <h5 class="widget-user-desc">QR Code</h5> -->
    <div class="row">
    <div class="column" style="width:20%">
      <h4 class="widget-user-username" style="margin-bottom:0;font-weight:bolder;font-size:1.75em;">Operator</h4>
      <h2 class="widget-user-desc" style="margin-bottom:0;"><b><?php echo $operator;?></b></h2>
    </div>
    <div class="column" style="width:60%">
      <h4 class="widget-user-username" style="margin-bottom:0;"></h4>
      <h2 class="widget-user-desc" style="margin-bottom:0;"><b><?php echo $nama_bb;?></b></h2>
    </div>
    <!-- <h5 class="widget-user-desc">Weighing Task</h5> -->
    <div class="column" style="width:20%">
      <h4 class="widget-user-username" style="margin-bottom:0;font-weight:bolder;font-size:1.75em;">Timbangan</h4>
      <h2 class="widget-user-desc" style="margin-bottom:0;"><b><?php echo $timbangan;?></b></h2>
    </div>
  </div>
</div>
  <!-- <div class="widget-user-image">
    <img class="img-circle" src="../web/images/packing-icon.png" alt="User Avatar">
  </div> -->
  <div class="box-footer" style="margin-top: 0px; padding-top:0px;">
    <div class="row" style="margin-bottom :0px; margin-top :0px; padding-top:0px;">
      <div class="box-body">
          <div class="box-body">

            <div id="sourceSelectPanel" style="display:none">
              <label for="sourceSelect" style="display:none">Switch Camera</label>
              <select id="sourceSelect" style="max-width:400px; display:none;">x
              </select>
            </div>

            <div >
              <!-- <label for="video" style="text-align: center;"><h3><b>Scan QR code dari No.MO</b></h3></label> -->
              <video id="video" width="330" height="300" style="border: 0.5px solid gray; display: block; margin-left: auto; margin-right: auto;"></video>
            </div>
            <!-- <div>
              <h2 style="float:left"><?php echo $kode_bb;?>
              </h2>
            </div> -->
              
          </div>
      </div>
    </div>
    <div style="margin-top:   0px;">
      <div class="col-md-6 column">
        <label for="kode">Kode Bahan Baku</label>
        <input type="text" autocomplete="off" name="kode" id="kode" disabled>
      </div>
      <div class="col-md-6 column">
        <label for="batch">Nomor Batch</label>
        <input type="text" autocomplete="off" name="batch" id="batch" disabled>
      </div>
    </div>
    <!-- <h4 style='text-align:center; margin-bottom:0px;'>History Split Batch</h4>
    <div style='border: 2px solid #DDD; padding: 8px; width:95%; border-radius:5px; display: block; margin-left: auto; margin-right: auto; margin-bottom:0px;'>
      <table>
        <thead>
          <tr>
            <th class='header'>No</th>
            <th class='header'>Kode BB</th>
            <th class='header'>Kode Internal</th>
            <th class='header'>No. Batch</th>
            <th class='header'>Qty</th>
            <th class='header'>UoM</th>
          </tr>
        </thead>
        <tbody>
    <?php if($is_split == 0) {
      echo "<tr>
              <td class='header'>-</td>
              <td class='header'>-</td>
              <td class='header'>No Record</td>
              <td class='header'>No Record</td>
              <td class='header'>-</td>
              <td class='header'>-</td>
            </tr>";
    } else {
      for ($x = 0; $x < $splitQty; $x++) {
        echo '<tr>';
          echo '<td class="text-center">'; echo $x+1; echo '</td>';
          echo '<td class="text-center">'; echo $split_data[$x]['kode_bb']; echo '</td>';
          echo '<td class="text-center">'; echo $split_data[$x]['kode_internal']; echo '</td>';
          echo '<td class="text-center">'; echo $split_data[$x]['no_batch']; echo '</td>';
          echo '<td class="text-center">'; echo $split_data[$x]['realisasi']; echo '</td>';
          echo '<td class="text-center">'; echo $split_data[$x]['satuan_realisasi']; echo '</td>';
        echo '</tr>';
      }
    } 
    ?>
        </tbody>
      </table>
    <div>
  </div> -->
  
<!-- </div> -->
<br>

<button class="accordion bg-blue-gradient" style="padding-top:0px;">Input Manual (Klik Jika kamera tidak bisa)</button>
<div class="panel">
  <div class="col-md-6 column" style="margin-left:0px; margin-top:0px;">
    <label for="kode_manual">ID Bahan Baku</label>
    <input type="text" autocomplete="off" name="kode_manual" id="kode_manual" >
  </div>
<!--   <div class="col-md-6 column">
    <label for="batch_manual">Nomor Batch</label>
    <input type="text" autocomplete="off" name="batch_manual" id="batch_manual">
  </div> -->
  <button onclick="check()" style="width:50%; display: block; margin-left: auto; margin-right: auto;">Check</button>
</div>
</body>


<!-- Javascript -->
<!-- Calling ZXing API CDN -->
<script type="text/javascript" src="zxing.js"></script>
<!-- Code -->
<script type="text/javascript">

var wo = <?php echo $wo; ?>;
var id = <?php echo $id; ?>;
var id_bb = <?php echo $id_bb; ?>;
var batch = "";
var kode_bb = "";
var link = "";
let selectedDeviceId;
const codeReader = new ZXing.BrowserMultiFormatReader()
console.log('ZXing code reader initialized')
codeReader.getVideoInputDevices()
  .then((videoInputDevices) => {
    const sourceSelect = document.getElementById('sourceSelect')
    selectedDeviceId = videoInputDevices[1].deviceId
    if (videoInputDevices.length >= 1) {
      videoInputDevices.forEach((element) => {
        const sourceOption = document.createElement('option')
        sourceOption.text = element.label
        sourceOption.value = element.deviceId
        sourceSelect.appendChild(sourceOption)
      })

      // When Changing Camera, Reset Scanner and Execute Canvas
      sourceSelect.onchange = () => {
        selectedDeviceId = sourceSelect.value;

        codeReader.reset()
        codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
          
          if (result) {
            var id_bb_scan = result_text;
            if (id_bb_scan){
              if (id_bb_scan == id_bb){
                $.get('index.php?r=log-penimbangan-rm/update-log-repack',{ id_bb:id_bb, log : 'scan' },function(data2){ 

                  // var data2 = $.parseJSON(data2);
                  //window.alert(data2);
                  if(data2==1){
                    window.location = "http://factory.pti-cosmetics.com/flowreport/web/index.php?r=log-penimbangan-rm/task&id="+id;
                  } else {
                    window.location = "index.php?r=log-penimbangan-rm/scan-cek-repack&id="+id+"&wo="+wo;
                  }
                });
              }else{
                window.alert("ID Bahan Baku tidak sesuai");
              }
            }
            
          }
          if (err && !(err instanceof ZXing.NotFoundException)) {
            console.error(err)
            document.getElementById('result').textContent = err
          }

        }) // on scanned 
      }; // onchange

      const sourceSelectPanel = document.getElementById('sourceSelectPanel')
      sourceSelectPanel.style.display = 'block'
    }

    // Initialize Execute Canvas when Page first loaded 
    codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {

        if (result) {
          var id_bb_scan = result_text;
          if (id_bb_scan){
            if (id_bb_scan == id_bb){
              $.get('index.php?r=log-penimbangan-rm/update-log-repack',{ id_bb:id_bb, log : 'scan' },function(data2){ 

                var data2 = $.parseJSON(data2);
                //window.alert(data2);
                if(data2.status=="success"){
                  window.location = "http://factory.pti-cosmetics.com/flowreport/web/index.php?r=log-penimbangan-rm/task&id="+id;
                } else if (data2.status=="not-success"){
                  window.location = "index.php?r=log-penimbangan-rm/scan-cek-repack&id="+id+"&wo="+wo;
                } else {
                }
              });
            }else{
              window.alert("ID Bahan Baku tidak sesuai");
            }
          }
          
        }
      if (err && !(err instanceof ZXing.NotFoundException)) {
        console.error(err)
        document.getElementById('result').textContent = err
      }
    }) // on scanned


  })
  .catch((err) => {
    console.error(err)
  })


/************************************************
 * Accordion
 *
************************************************/ 
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}


function check(){
  var id_bb_manual = document.getElementById("kode_manual").value;
  var id_bb = <?php echo $id_bb; ?>;
  var wo = <?php echo $wo; ?>;
  var id = <?php echo $id; ?>;

  if (id_bb != ""){
    if (id_bb_manual == id_bb){
      $.get('index.php?r=log-penimbangan-rm/update-log-repack',{ id_bb:id_bb, log : 'scan' },function(data2){ 

        // var data2 = $.parseJSON(data2);
        //window.alert(data2);
        window.alert("ID Bahan Baku sesuai");
        if(data2==1){
          window.location = "http://factory.pti-cosmetics.com/flowreport/web/index.php?r=log-penimbangan-rm/task&id="+id;
        } else {
          window.location = "index.php?r=log-penimbangan-rm/scan-cek-repack&id="+id+"&wo="+wo;
        }
      });
    }else{
      window.alert("ID Bahan Baku tidak sesuai");
    }
    // $.get('index.php?r=log-penimbangan-rm/insert-batch',{ wo : wo, nobatch : nobatch },function(data5){
    //   if (data5==1){
    //     $.get('index.php?r=log-penimbangan-rm/check-bb',{ wo : wo, kode_bb : kode },function(data){
    //       //console.log(data);
    //       //var data = $.parseJSON(data);
    //       if(data==1){                    
    //          // location.href='http://10.3.5.102/flowreport_gustav/web/index.php?r=flow-input-mo/create-penimbangan-rm&nomo='+result.text;
    //          $.get('index.php?r=log-penimbangan-rm/update-log-input',{ wo : wo, log : "manual" },function(data){
    //             if(data==1){
    //               window.location = "http://10.3.5.102/flowreport/web/index.php?r=log-penimbangan-rm/auto-massa&id="+id+"&wo="+wo+"&log=manual";
    //             } else {
    //               alert('Tidak berhasil update log status');
    //             }
    //          });
    //          //window.location = "index.php?r=log-penimbangan-rm/input-massa&id="+id+"&wo="+wo;
    //          //window.location = "index.php?r=log-penimbangan-rm/index";
            
    //       }
    //       else if(data==2){                   
    //          // location.href='http://10.3.5.102/flowreport_gustav/web/index.php?r=flow-input-mo/create-penimbangan-rm&nomo='+result.text;
    //          $.get('index.php?r=log-penimbangan-rm/update-log-input',{ wo : wo, log : "manual" },function(data){
    //             if(data==1){
    //               window.location = "http://10.3.5.102/flowreport/web/index.php?r=log-penimbangan-rm/input-massa&id="+id+"&wo="+wo+"&log=manual";
    //             } else {
    //               alert('Tidak berhasil update log status');
    //             }
    //          });
    //          //window.location = "index.php?r=log-penimbangan-rm/index";
    //       }
    //        else{
    //         alert('Bahan Baku Tidak Sesuai. Hubungi Leader sebelum melanjutkan proses timbang');
    //       }

    //     });
    //   } else {
    //     window.alert("Gagal Menyimpan No.Batch ke Database!")
    //   }
    // });
  } else {
    window.alert("Lengkapi Data Terlebih Dahulu!");
  }
}
  </script>