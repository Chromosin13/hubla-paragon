<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use app\models\LogPenimbanganRm;
use wbraganca\tagsinput\TagsinputWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterDataFormulaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Weighing Task';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.resp-container {
    /*position: relative;*/
    /*overflow: hidden;*/
    /*padding-top: 56.25%;*/
} 

.resp-iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    border: 0;
}
.bawahan {
   position: fixed;
   border-top:2px solid #D3D3D3;
   left: 0;
   bottom: 0;
   width: 100%;
   padding:15px 0;
   background-color: white;
   color: white;
   text-align: center;
}

@media (max-width:800px)  {
 /* portrait tablets, portrait iPad, e-readers (Nook/Kindle), landscape 800x480 phones (Android) */
.header{
  font-size:8vmin;
  top: 50%;
  margin:0;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
.column{
  float: left;
  width: 50%;
}
.info_box{
  float:left;
  width:63%;
  
}
.op_box{
  float:left;
  margin-left: 2%;
  width:35%;
  
}

.title_info{
  font-size:1.5em;
}
.body_info{
  font-size:1.3em;
}


}

@media (min-width:1000px)  {
 /* portrait tablets, portrait iPad, e-readers (Nook/Kindle), landscape 800x480 phones (Android) */
.header{
  font-size:10vmin;
  top: 50%;
  margin:0;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);

}
.column{
  float: left;
  width: 50%;
}
.op_box{
  width:100%;
}
.info_box{
  width:100%;
}

}
</style>

<style>
#confirm {
  display: none;
  background-color: #F3F5F6;
  color: #000000;
  border: 1px solid #aaa;
  position: fixed;
  width: 50%;
  height: 50%;
  left: 50%;
  top: 50%;
  margin:0;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
  margin-left: -100px;
  padding: 10px 20px 10px;
  box-sizing: border-box;
  text-align: center;
  z-index:5;
}
#confirm button {
  background-color: #FFFFFF;
  display: inline-block;
  border-radius: 12px;
  border: 4px solid #aaa;
  padding: 5px;
  text-align: center;
  width: 60px;
  cursor: pointer;
}
#confirm .message {
  text-align: left;
}

#modaltitle{
  display:inline-block;
}
.modal-body{
  font-weight:500;
  font-size:24px;
}
.modal-content{
  vertical-align:middle;
  margin-top: 25%;
  /*background-color : red;*/
  text-align:center;

}

</style>

<script>
var id = <?php echo $id; ?>;
var operator = <?php echo $operator; ?>;
var nama_line = '<?php echo $nama_line; ?>';
var dnsUrl = '<?php echo Yii::getAlias('@dnsUrl'); ?>';
// var j = setInterval(function() {
//     $.getJSON("http://10.3.5.102:3000/flow_input_mo?id=eq."+id,function(result){
//         j = result[0];
//         //console.log(j.datetime_stop);
//         if (operator != 1){
//           if (!j.datetime_stop){
//             // isFinisih = true;
            
//           } else {
            // window.location = dnsUrl+"flow-input-mo/check-penimbangan-rm"; 
//           }
//         }
//         // } else {
//         //   isFinisih = false;
//         // }
        
//     });
// }, 5000);

function print(siklus){
  var id = <?php echo $id; ?>;

    $.get('index.php?r=log-penimbangan-rm/goto-br',{ id:id,siklus:siklus,nama_line:nama_line},function(data2){

      var data2 = $.parseJSON(data2);
      window.alert(data2.status);
      if(data2.status=="done"){
        //window.location = "index.php?r=flow-input-mo/check-penimbangan-rm";
        window.location = "index.php?r=log-penimbangan-rm/pdf&id="+id+"&siklus="+siklus;
      } else {
        window.alert("Ada Material yang Belum Selesai Ditimbang.");
      }
    });
}

function siklusparalel(){
  var id = <?php echo $id; ?>;

  $('#modalConfirm').appendTo('body').modal('show');

  // Modal Confirm All Siklus
  $("#yes").on("click", function(){
        $.get('index.php?r=flow-input-mo/change-siklus-paralel',{ id:id},function(data2){

        var data2 = $.parseJSON(data2);
        //window.alert(data2);
        if(data2.status=="success"){
          //window.location = "index.php?r=flow-input-mo/check-penimbangan-rm";
          window.location = "index.php?r=flow-input-mo/check-bom-rm&id="+id;
        } else {
          // window.location = "index.php?r=log-penimbangan-rm/task&id="+id;

          $('#Flash').html('Update siklus paralel gagal').fadeIn().animate({opacity:1.0},3000).fadeOut('slow');
        }
      });
  });

}

function finish(){
  
  document.getElementById("nextbutton").disabled = true;

  var id = <?php echo $id; ?>;
  var nomo = "<?php echo $nomo ?>";

  $.get('index.php?r=log-penimbangan-rm/finish-timbang',{ id:id,nama_line:nama_line},function(data2){

    var data2 = $.parseJSON(data2);
    //window.alert(data2);
    if(data2.status=="done"){
      //window.location = "index.php?r=flow-input-mo/check-penimbangan-rm";
      $.get('index.php?r=log-penimbangan-rm/stop-jadwal-penimbangan',{ id:id},function(data3){

        var data3 = $.parseJSON(data3);
        //window.alert(data2);
        if(data3.status=="success"){
          $.post("index.php?r=flow-input-mo/send-data-to-checker&id="+id, function (data){
            if (data==1){
              //window.location = "index.php?r=flow-input-mo/check-penimbangan-rm";
              window.location = "index.php?r=downtime/index-penimbangan&id="+id;
            }
          });
        } else {
          window.location = "index.php?r=log-penimbangan-rm/print-document&id="+id;
        }
      });
    } else {
      if (window.alert("Ada Material yang Belum Selesai Ditimbang.")){}else{
        document.getElementById("nextbutton").disabled = false;
      }

    }
  });
}

function finish_repack(){
  var id = <?php echo $id; ?>;
  var nomo = "<?php echo $nomo ?>";

  $.get('index.php?r=log-penimbangan-rm/finish-timbang-repack',{ id:id,nama_line:nama_line},function(data2){

    var data2 = $.parseJSON(data2);
    //window.alert(data2);
    if(data2.status=="done"){
      //window.location = "index.php?r=flow-input-mo/check-penimbangan-rm";
      $.get('index.php?r=log-penimbangan-rm/stop-jadwal-penimbangan',{ id:id},function(data3){

        var data3 = $.parseJSON(data3);
        //window.alert(data2);
        if(data3.status=="success"){
          window.location = "index.php?r=downtime/index-penimbangan&id="+id;

        //   $.post("index.php?r=flow-input-mo/send-data-to-checker&nomo="+nomo, function (data){
        //     if (data==1){
        //       //window.location = "index.php?r=flow-input-mo/check-penimbangan-rm";
        //       window.location = "index.php?r=downtime/index-penimbangan&id="+id;
        //     }
        //   });
        // } else {
        //   window.location = "index.php?r=log-penimbangan-rm/print-document&id="+id;
        }else{
          window.alert("Gagal stop proses timbang.");
        }
      });
    } else {
      window.alert("Ada Material yang Belum Selesai Ditimbang.");
    }
  });
}

function finish_ro(){
  var id = <?php echo $id; ?>;
  var nomo = "<?php echo $nomo ?>";

  $.get('index.php?r=log-penimbangan-rm/finish-timbang-ro',{ id:id},function(data2){

    var data2 = $.parseJSON(data2);
    //window.alert(data2);
    if(data2.status=="done"){
      //window.location = "index.php?r=flow-input-mo/check-penimbangan-rm";
      $.get('index.php?r=log-penimbangan-rm/stop-jadwal-penimbangan',{ id:id},function(data3){

        var data3 = $.parseJSON(data3);
        //window.alert(data2);
        if(data3.status=="success"){
          window.location = "index.php?r=downtime/index-penimbangan&id="+id;

        //   $.post("index.php?r=flow-input-mo/send-data-to-checker&nomo="+nomo, function (data){
        //     if (data==1){
        //       //window.location = "index.php?r=flow-input-mo/check-penimbangan-rm";
        //       window.location = "index.php?r=downtime/index-penimbangan&id="+id;
        //     }
        //   });
        // } else {
        //   window.location = "index.php?r=log-penimbangan-rm/print-document&id="+id;
        }else{
          window.alert("Gagal stop proses timbang.");
        }
      });
    } else {
      window.alert("Ada Material yang Belum Selesai Ditimbang.");
    }
  });
}

function functionAlert(msg, myYes) {
  var confirmBox = $("#confirm");
  confirmBox.find(".message").text(msg);
  confirmBox.find(".yes").unbind().click(function() {
     confirmBox.hide();
  });
  confirmBox.find(".yes").click(myYes);
  confirmBox.show();
}
</script>

<body oncopy="return false" oncut="return false" onpaste="return false">

<div class="bom-confirmation"> 

    <div class="box box-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-blue-gradient">
        <div class="row">
        <div class="column">
          <h1 class="header" style="font-weight: bolder; position:absolute; left:5vmin;" class="widget-user-username"><b><?php echo 'OPR '; echo $operator; ?></b></h1>
        </div>
        <!-- <h5 class="widget-user-desc">Weighing Task</h5> -->
        <div class="column">
          <h1 class="header" style="font-weight: bolder; position:absolute; right:5vmin;"><?php echo $nama_line; ?></h1>
        </div>
      </div>
      </div>
      <div class="widget-user-image">
        <img class="img-circle" src="../web/images/formula.png" alt="User Avatar">
      </div>
    </div>

    <br>
    <br>

    <div name="expandd" id="expandd">
    </div>
    
    <div name="expand" id="expand">
    </div>

    <?php if (strpos($nama_line,'LW')!==false) : ?>
      <!-- <div class="box"> -->
        <div class="box-body">
          <div class="col-lg-12 col-xs-12">
            <p>
              <a class="btn btn-block" style="background:#afafaf; color:white;" id="splitBtn">SPLIT LINE REPACK: 
                <?php if (!empty($is_split_line) and $is_split_line == 1)
                    {
                      echo '<b style="color:green;">AKTIF</b>';
                    } else {
                      echo '<b style="color:red;">TIDAK AKTIF</b>';
                    }
                ?>
              </a>
            </p>
          </div>
        </div>
      <!-- </div> -->
    <?php endif ; ?>

    <div class="op_box box box-widget widget-user">
      <div class="row">
          <div class="col-lg-12 col-xs-12" style=" text-align: center; padding-left:5vmin; padding-right:5vmin;">
            <h4 style="font-weight: bolder; padding-top:1vmin;">PILIH OPERATOR</h4>
              <p><?php 

                    //var_dump($operator_terpilih,$operator_terpilih2);
                    foreach($list_operator as $value){ 
                      if($value==$operator_terpilih || $value==$operator_terpilih2){
                        //echo '<button type="button" class="btn btn-success btn-md active">'.$value.'</button> ';
                        echo Html::a($value, ['insert-selected-operator', 'id' => $id, 'nama_line'=>$nama_line,'nama_operator' => $value], ['class'=>'btn btn-primary btn-block active']);
                        // echo "<a class='btn btn-primary btn-block active' value='"; echo $value; echo "' onclick='insert()'>";
                        // echo $value;
                        // echo "</a>";
                        echo '  ';
                      }else{
                        echo Html::a($value, ['insert-selected-operator', 'id' => $id, 'nama_line'=>$nama_line,'nama_operator' => $value], ['class'=>'btn btn-default btn-block']);
                        // echo "<a class='btn btn-primary btn-block' value='"; echo $value; echo "' onclick='insert()'>";
                        // echo $value;
                        // echo "</a>";
                        echo '  ';
                      }
                       
                    }

                  ?>
              </p>
              <br>
          </div>
      </div>
    </div>
    <div name="expanddd" id="expanddd">
    </div>



    <?php if (count($op_terpilih['nama_operator']) > 0){
      echo '<div class="box box-widget widget-user" style="display: inline-block;">';
    } else {
      echo '<div class="box box-widget widget-user" style="display: inline-block; pointer-events: none; opacity: 0.4;">';
      echo '<h3 style="text-align:center; color:red; opacity:1; font-weight:bolder;">Mohon Pilih Operator Terlebih Dahulu</h3>';
    } ?>
      <?php 
      for ($i = 0; $i<(count($qtySiklus)); $i++){
        echo '<h2 style="text-align:center; font-weight:bolder;">SIKLUS-'; echo $qtySiklus[$i]['siklus']; echo '</h2>';
        echo'<table class="table">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 5%; font-size:1.5em;">
                      <b>ID BB</b>
                    </th>
                    <th class="text-center" style="width: 10%; font-size:1.5em;">
                      <b>Kode BB</b>
                    </th>
                    <th class="text-center" style="width: 14%; font-size:1.5em;">
                      <b>Nama BB</b>
                    </th>
                    <th class="text-center" style="width: 10%; font-size:1.5em;">
                      <b>Kode Internal</b>
                    </th>
                    <th class="text-center" style="width: 5%; font-size:1.5em;">
                      <b>Is repack</b>
                    </th>
                    <th class="text-center" style="width: 4%; font-size:1.5em;">
                      <b>Standar</b>
                    </th>
                    <th class="text-center" style="width: 5%; font-size:1.5em;">
                      <b>Realisasi</b>
                    </th>
                    <th class="text-center" style="width: 4%; font-size:1.5em;">
                      <b>Satuan</b>
                    </th>
                    <th class="text-center" style="width: 8%; font-size:1.5em;">
                      <b>Timbangan</b>
                    </th>
                    <th class="text-center" style="width: 9%; font-size:1.5em;">
                      <b>Status</b>
                    </th>
                  </tr>
                </thead>
                <tbody>';
        for ($x = 0; $x < $taskQty; $x++) {
          if ($task[$x]['siklus'] == $qtySiklus[$i]['siklus']){
            echo '<tr>';
              echo '<td class="text-center">'; echo $task[$x]['id_bb']; echo '</td>';
              echo '<td class="text-center">'; echo $task[$x]['kode_bb']; echo '</td>';
              echo '<td class="text-center">'; echo $task[$x]['nama_bb']; echo '</td>';
              echo '<td class="text-center">'; echo $task[$x]['kode_internal']; echo '</td>';
              echo '<td class="text-center">'; if ($task[$x]['is_repack']==1){ echo "Ya"; } else { echo "Tidak";} echo '</td>';
              echo '<td class="text-center">'; echo number_format($task[$x]['weight'],4,'.',''); echo '</td>';
              echo '<td class="text-center">'; echo number_format($task[$x]['realisasi'],4); echo '</td>';
              echo '<td class="text-center">'; echo $task[$x]['satuan']; echo '</td>';
              echo '<td class="text-center">'; echo $task[$x]['timbangan']; echo '</td>';
              if ($task[$x]['status'] == "Belum Ditimbang"){
                echo '<td style="background:#de3a26; color:white; font-weight:bolder;" class="text-center"><a href="'.Yii::getAlias('@dnsUrl').'log-penimbangan-rm/scan-bb&id='.$id.'&wo='.$task[$x]['id'].'">'; echo $task[$x]['status']; echo '</a></td>';
              } else if ($task[$x]['status'] == "Cek Timbang"){
                echo '<td style="background:#efa536; color:white; font-weight:bolder;" class="text-center"><a href="'.Yii::getAlias('@dnsUrl').'log-penimbangan-rm/scan-cek-repack&id='.$id.'&wo='.$task[$x]['id'].'">'; echo $task[$x]['status']; echo '</a></td>';
              } else {
                echo '<td style="background:#00b76f; color:white; font-weight:bolder;" class="text-center">'; echo $task[$x]['status']; echo '</td>';
              }
            echo '</tr>';
          }
        }
        echo '</tbody></table>';
        if($operator==1) {
          
          echo '<div class="row" style="text-align:center;">
                <button class="btn-hero-success" style="border-radius:5px;">
                <a style="font-size:1.5em;" onclick="print(';
          echo $qtySiklus[$i]['siklus'];
          echo  ')">Cetak BR Siklus-'; echo $qtySiklus[$i]['siklus']; echo '</a></button>
              </div><br><hr>';            
          
          if (count($qtySiklus) == $qtySiklus[$i]['siklus']){
            echo '<div class="row" style="text-align:center;">
                  <button class="btn-hero-success" style="border-radius:5px;">
                  <a style="font-size:1.5em;" onclick="print(0)">Cetak BR Semua Siklus</a></button>
                </div><br><hr>';
          }        

        } 
      }?>

      <?php if($operator==1) {
          if ($siklus!=0){
            echo '<div>
                <div class="row" >
                  <div style="width:100%; padding:0 30px;">
                    <button onclick="siklusparalel()"id="paralel" class="btn  bg-maroon center" ><b>Paralel Siklus</b></button>
                  </div>
                </div>
            </div>';

          }else{
            echo '<div>
                <div class="row" >
                  <div style="width:100%; padding:0 30px;">
                    <button onclick="siklusparalel()"id="paralel" class="btn btn-default center disabled" ><b>Paralel Siklus</b></button>
                  </div>
                </div>
            </div>';

          }
        } ?> 
    </div>


    <?php if($operator==1) {
            if ($siklus == -1){
              echo '<div class="bawahan">
                  <div class="row">
                    <div style="width:100%; padding:0 30px;">
                      <button onclick="finish_repack()"id="nextbutton" class="btn btn-block btn-info"><b>FINISH TIMBANG REPACK</b></button>
                    </div>
                  </div>
              </div>';
            }else{

              echo '<div class="bawahan">
                  <div class="row">
                    <div style="width:100%; padding:0 30px;">
                      <button onclick="finish()"id="nextbutton" class="btn btn-block btn-info"><b>FINISH</b></button>
                    </div>
                  </div>
              </div>';
            }
        }else if ($operator==11){
          echo '<div class="bawahan">
                  <div class="row">
                    <div style="width:100%; padding:0 30px;">
                      <button onclick="finish_ro()"id="nextbutton" class="btn btn-block btn-info"><b>FINISH TIMBANG AIR RO</b></button>
                    </div>
                  </div>
              </div>';
        } ?>
</div>

    <div class="modal fade"  id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h2 class="modal-title" id="modaltitle">Confirm</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              Anda akan mengubah Timbang Siklus <?= $siklus; ?> menjadi Timbang All Siklus ?<br>
              <!-- Jika belum selesai, dan dilanjut di shift berikutnya, klik Belum All Batch. -->
          </div>
          <div class="modal-footer">
            <button id="no" type="button" class="btn btn-warning pull-left" data-dismiss="modal">Tidak</button>
            <button id="yes" type="button" class="btn btn-success">Ya</button>
          </div>
        </div>
      </div>
    </div>

</body>
<?php
$book = array(
    "title" => "JavaScript: The Definitive Guide",
    "author" => "David Flanagan",
    "edition" => 6
);

$jsonData = json_encode($task);
?>

<?php
$script = <<< JS

// AutoFocus Nomo Field

//document.getElementById("nextbutton").focus();

// Trigger Query


var siklus = "$siklus";
if (siklus != 0){
  document.getElementById("paralel").disabled = false;
}else{
  document.getElementById("paralel").disabled = true;
}


window.onresize = function () {
    detectSwap();
};

window.onload = function () {
    detectSwap();
};

function detectSwap() {
    var width = window.innerWidth;
    var nomo = "$nomo";
    var no_formula = "$no_formula";
    var no_revisi = "$no_revisi";
    var nama_fg = "$naitem_fg";
    //console.log(width);
    if (width <= 800) {
        //$('#expand').html("<label class='text-success'>Uploading...</label>");
        document.getElementById("expandd").innerHTML = "<div class='box-widget widget-user'><div class='row'>";
        document.getElementById("expand").innerHTML = "<div class='info_box box box-widget widget-user'><table class='table'><tbody><tr><td class='text-center title_info' style='background: #ffdfba;'><b>No MO</b></td></tr><tr><td class='text-center body_info'>"+nomo+"</td></tr><tr><td class='text-center title_info' style='background: #ffffba;'><b>No Formula</b></td></tr><tr><td class='text-center body_info'>"+no_formula+"</td></tr><tr><td class='text-center title_info' style='background: #baffc9;'><b>No Revisi</b></td></tr><tr><td class='text-center body_info'>"+no_revisi+"</td></tr><tr><td class='text-center title_info' style='background: #bae1ff;'><b>Nama Item</b></td></tr><tr><td class='text-center body_info'>"+nama_fg+"</td></tr></tbody></table></div>";
        document.getElementById("expanddd").innerHTML = "</div></div>.";
    } else if (width > 800) {
        document.getElementById("expandd").innerHTML = "";
        document.getElementById("expand").innerHTML = "<div class='box box-widget widget-user isShow'><table class='table'><thead><tr><th class='text-center' style='width: 30%; font-size:1.8em; background: #ffdfba;'><b>No MO</b></th><th class='text-center' style='width: 20%; font-size:1.8em; background: #ffffba;'><b>No Formula</b></th><th class='text-center' style='width: 10%; font-size:1.8em; background: #baffc9;'><b>No Revisi</b></th><th class='text-center' style='width: 40%; font-size:1.8em; background: #bae1ff;'><b>Nama Item</b></th></tr></thead><tbody><tr><td class='text-center' style='font-size:1.5em;'>"+nomo+"</td><td class='text-center' style='font-size:1.5em;'>"+no_formula+"</td><td class='text-center' style='font-size:1.5em;'>"+no_revisi+"</td><td class='text-center' style='font-size:1.5em;'>"+nama_fg+"</td></tr></tbody></table></div>";
        document.getElementById("expanddd").innerHTML = "";
    }
    //console.log(width);
}

document.getElementById("splitBtn").onclick = function() {

    var id  = '$id';
    var now  = '$is_split_line';
    var nomo  = '$nomo';

    if (now == 1){
      var result = confirm("Anda Yakin Akan Mematikan Fitur SPLIT LINE ?");
      if(result){
        $.getJSON("index.php?r=flow-input-mo/deactivate-split-line&id="+id+"&now="+now+"&nomo="+nomo, function(data){
          if(data.status == 'success'){
            window.location.reload();
          }else{
            window.alert("Fitur SPLIT LINE tidak bisa dimatikan karena sudah ada yang scan di line lain");
          }
        });
      }
    } else {
      var result = confirm("Anda Yakin Akan Mengaktifkan Fitur SPLIT LINE ?");
      if(result){
        window.location = "index.php?r=flow-input-mo/activate-split-line&id="+id+"&now="+now;          
      }
    }
    

};



JS;
$this->registerJs($script);
?>