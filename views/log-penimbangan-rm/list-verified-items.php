<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\LogFormulaBreakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
.tableFixHead          { overflow-y: auto; height: 81vh; z-index:5;}
.tableFixHead thead th { position: sticky; top: -1px; z-index:5;}

.table-bordered{
  border-left:1.5px solid rgba(135,206,250,.25);
  border-bottom:1.5px solid rgba(135,206,250,.25);
  border-top:1px solid rgba(135,206,250,.25);
}
.table-bordered td,.table-bordered th{
  border:1px solid rgba(135,206,250,.25)
}
.table-bordered thead td,.table-bordered thead th{
  border-bottom-width:2px
}

.table-striped tbody tr:nth-of-type(odd){
  background-color:rgba(135,206,250,.25)
}

.btn-info{color:#fff;background-color:#5DADE2;border-color:#5DADE2}.btn-info:hover{color:#fff;background-color:#138496;border-color:#117a8b}.btn-info.focus,.btn-info:focus{color:#fff;background-color:#138496;border-color:#117a8b;box-shadow:0 0 0 .2rem rgba(58,176,195,.5)}.btn-info.disabled,.btn-info:disabled{color:#fff;background-color:#17a2b8;border-color:#17a2b8}.btn-info:not(:disabled):not(.disabled).active,.btn-info:not(:disabled):not(.disabled):active,.show>.btn-info.dropdown-toggle{color:#fff;background-color:#117a8b;border-color:#10707f}.btn-info:not(:disabled):not(.disabled).active:focus,.btn-info:not(:disabled):not(.disabled):active:focus,.show>.btn-info.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(58,176,195,.5)}

.btn-add:hover {
  box-shadow: 0 5px 9px 0 rgba(0,0,0,0.24), 0 5px 9px 0 rgba(0,0,0,0.19);
}

.btn-minus:hover {
  box-shadow: 0 5px 9px 0 rgba(0,0,0,0.24), 0 5px 9px 0 rgba(0,0,0,0.19);
}

table tbody tr{
    height: 1em;
}
</style>


        
<div class="box widget-user" style="display: inline-block;"> 
    <div>
          <h1 class="text-center" style="margin-top:0px; padding-top :0.8em; padding-bottom :0.3em;">
            <button class="btn btn-info" onclick="back()" style="margin-bottom:1.2em; margin-left:15px; font-size: 0.4em; float: left;"><i class="fa fa-arrow-left"></i></button>
            Verified Items
          </h1>
    </div>

    <div style="width:96%; margin: 0 auto;"> 

    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'columns' => [
          ['class' => 'yii\grid\SerialColumn'],

            'kode_bb',
            'kode_internal',
            'nama_bb',
            'weight',
            'satuan',
            'siklus',
            'kode_olah',
            'operator',
            'timbangan'
          
          // [
          //       'attribute' => 'scheduled_start',
          //       'contentOptions' => function ($dataProvider) {
          //           return ['style' => 'text-align:center;width:10%'];
          //       },
          // ],

      ],
    ]); ?>
  </div>
</div>

<script>
  function back(){
    window.location = "index.php?r=log-formula-breakdown/schedule-list";
    // window.history.back();
  }

</script>


<?php
$script = <<< JS


JS;
$this->registerJs($script);
?>