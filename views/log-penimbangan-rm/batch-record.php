<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LogPenimbanganRm */
?>

<?php 
    if ($planner['sediaan']=='L'){
        $sediaan = 'Liquid';
    }else if ($planner['sediaan']=='P'){
        $sediaan = 'Powder';
    }else if ($planner['sediaan']=='S'){
        $sediaan = 'Semisolid';
    }else if ($planner['sediaan']=='V'){
        $sediaan = 'Varcos';
    }else {
        $sediaan = '-';
    }
 ?>
<div class="log-penimbangan-rm-view">
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="border: 1px solid black;width:320px;">
                    <img src="../web/images/logo-pti.png" style="width:280px; text-align:center; margin-left:17px;"></td>
                </td>
                <?php 
                    if (strpos($task[0]['no_formula'],'.F')!==false){
                        $formula = explode('.F', $task[0]['no_formula'])[1];
                        if (strpos($formula,'.')!==false){
                            $formula_code = explode('.', $formula)[0];
                        }else{
                            $formula_code = explode(',', $formula)[0];
                        }
                    }else if (strpos($task[0]['no_formula'],',F')!==false){
                        $formula = explode(',F', $task[0]['no_formula'])[1];
                        if (strpos($formula,'.')!==false){
                            $formula_code = explode('.', $formula)[0];
                        }else{
                            $formula_code = explode(',', $formula)[0];
                        }
                    }else{
                        $formula = explode('.', $task[0]['no_formula'])[3];
                        $formula_code = preg_replace('/[^0-9]/','',$formula);
                    }
                    // $formula_code = substr($formula,1);
                    // $formula_code = preg_replace('/[^0-9]/','',$formula);


                ?>
                <td style="text-align: center;border: 1px solid black;width:480px;">
                    <p style="margin-top:0px; margin-bottom:3px; font-size: 1.8em;"><b>BATCH RECORD PENGOLAHAN</b></p>
                    <p style="margin-top:0px; margin-bottom:0px; font-size: 1.4em;"><b><?php echo $planner['nama_bulk']; ?></b></p>
                    <!-- <p style="margin-top:0px; margin-bottom:0px; font-size: 1.2em;"><b>Formula <?php echo $task[0]['no_revisi']; ?></b></p> -->
                    <p style="margin-top:0px; margin-bottom:0px; font-size: 1.2em;"><b>Formula <?php echo $formula_code; ?></b></p>

                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="border: 1px solid black;width:130px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;">MO Odoo</p>
                </td>
                <td style="border: 1px solid black;width:190px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $task[0]['mo_odoo']; ?></p>
                </td>
                <td style="border: 1px solid black;width:150px; padding-left:7px;">
                    <p style="font-size: 1.1em;">NOMO</p>
                </td>
                <td style="border: 1px solid black;width:330px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $task[0]['nomo']; ?></p>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="border: 1px solid black;width:130px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Nama Brand</p>
                </td>
                <td style="border: 1px solid black;width:190px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $brand; ?></p>
                </td>
                <td style="border: 1px solid black;width:150px; padding-left:7px;">
                    <p style="font-size: 1.1em;">No. Dokumen</p>
                </td>
                <td style="border: 1px solid black;width:330px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $task[0]['no_formula']; ?></p>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="border: 1px solid black;width:130px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Kode Bulk</p>
                </td>
                <td style="border: 1px solid black;width:190px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $planner['koitem_bulk']; ?></p>
                </td>
                <td style="border: 1px solid black;width:150px; padding-left:7px;">
                    <p style="font-size: 1.1em;">No. Revisi</p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $task[0]['no_revisi']; ?></p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Nama Line</p>
                </td>
                <td style="border: 1px solid black;width:130px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $flow_input_mo['nama_line']; ?></p>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="border: 1px solid black;width:130px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Bentuk Sediaan</p>
                </td>
                <td style="border: 1px solid black;width:190px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $sediaan; ?></p>
                </td>
                <td style="border: 1px solid black;width:150px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Tgl. Mulai Diolah</p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;"></p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Kode Jadwal</p>
                </td>
                <td style="border: 1px solid black;width:130px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $planner['kode_jadwal'] ?></p>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="border: 1px solid black;width:130px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Besar Batch</p>
                </td>
                <td style="border: 1px solid black;width:190px; margin:0px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo number_format($planner['besar_lot'],4); ?> kg</p>
                </td>
                <td style="border: 1px solid black;width:150px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Tgl. Selesai Diolah</p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;"></p>
                </td>
                <td style="border: 1px solid black;width:100px; padding-left:7px;">
                    <p style="font-size: 1.1em;">Tgl. Berlaku</p>
                </td>
                <td style="border: 1px solid black;width:130px; padding-left:7px;">
                    <p style="font-size: 1.1em;"><?php echo $date; ?></p>
                </td>
            </tr>
        </tbody>
    </table>
    <h3 class="text-center" style="font-size:1.5em; font-weight:bolder; margin-bottom:17px;">DATA PENIMBANGAN SIKLUS <?php echo $siklus; ?></h3>
    <!-- <br> -->
    <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="text-align: center; border: 1px solid black;width:320px;">
                    <p style="margin-top:0px; margin-bottom:3px; font-size: 1.1em;"><b>Nama Operator Penimbangan</b></p>
                </td>
                <td style="text-align: center; border: 1px solid black;width:480px;">
                    <p style="margin-top:0px; margin-bottom:3px; font-size: 1.1em;"><b><?php echo $list_operator; ?></b></p>
                </td>
            </tr>
        </tbody>
    </table>
    <br>
    <!-- <br>

    <p style="margin-left:20px;">
        <b style="font-size:1.1em;">A. Timbangan Satuan Kg </b><br>
        <b style="font-size:1em;">Bahan baku sudah tersertifikasi halal. Pastikan bahan baku tidak terkontaminasi bahan yang tidak halal selama proses penimbangan.</b>
    </p>
    <hr style="margin-bottom:7px;"> -->
    <!-- <p style="margin-left:20px;">
        <b style="font-size:1.1em;">B. List Proses Penimbangan</b><br>
    </p> -->
    <?php
        $seq_timbangan = 0;
        $no = 0;


        foreach($weigherList as $weigher){
            $seq_timbangan++;
            echo '<p style="margin-left:0px;">
                    <b style="font-size:1.1em;">';echo $seq_timbangan; echo '. Timbangan ';echo $weigher->no_timbangan; echo '</b><br>
                  </p>';
            // echo '<table style="margin:0px; border-collapse: collapse; margin-bottom:10px;">
            //             <tbody>
            //                 <tr>
            //                     <td style="text-align: center; border: 1px solid black;width:120px;">
            //                         <p style="margin-top:0px; margin-bottom:3px; font-size: 0.9em;"><b>Operator</b></p>
            //                     </td>
            //                     <td style="text-align: center; border: 1px solid black;width:200px;">
            //                         <p style="margin-top:0px; margin-bottom:3px; font-size: 0.9em;"><b>';
            // foreach($task as $tasks){
            //     if ($tasks['no_timbangan'] == $weigher->no_timbangan){
            //         echo $tasks['nama_operator']; break;
            //     }
            // }
            // echo '</b></p>
            //                     </td>
            //                 </tr>
            //             </tbody>
            //         </table>';  

            echo '<table style="margin:0px; border-collapse: collapse;">
                    <thead>
                      <tr>
                        <th class="text-center" style="width: 30px; font-size:1.05em;border: 1px solid black;">
                          <b>No.</b>
                        </th>
                        <th class="text-center" style="width: 160px; font-size:1.05em;border: 1px solid black;">
                          <b>Kode Bahan</b>
                        </th>
                        <th class="text-center" style="width: 160px; font-size:1.05em;border: 1px solid black;">
                          <b>Nama Bahan</b>
                        </th>
                        <th class="text-center" style="width: 140px; font-size:1.05em;border: 1px solid black;">
                          <b>No.Batch</b>
                        </th>
                        <th class="text-center" style="width: 90px; font-size:1.05em;border: 1px solid black;">
                          <b>Kebutuhan</b>
                        </th>
                        <th class="text-center" style="width: 90px; font-size:1.05em;border: 1px solid black;">
                          <b>Realisasi</b>
                        </th>
                        <th class="text-center" style="width: 40px; font-size:1.05em;border: 1px solid black;">
                          <b>Kode Olah</b>
                        </th>
                        <th class="text-center" style="width: 90px; font-size:1.05em;border: 1px solid black;">
                          <b>Operator</b>
                        </th>
                      </tr>
                    </thead>
                    <tbody>';

            for ($x = 0; $x < $taskQty; $x++) {
                
                if ($task[$x]['no_timbangan'] == $weigher->no_timbangan){
                    $no = $no + 1;
                    echo '<tr>';
                      echo '<td style="text-align:center; border: 1px solid black;font-size: 0.9em; margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $no; echo '</p></td>';
                      echo '<td style="border: 1px solid black;font-size: 0.95em;padding-left:7px;margin: 7px 7px 7px 7px; "><p style="margin: 7px 7px 7px 7px;">'; echo $task[$x]['kode_internal']; echo '</p></td>';
                      echo '<td style="border: 1px solid black;font-size: 0.95em;padding-left:7px;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $task[$x]['nama_bb']; echo '</p></td>';
                      if ($task[$x]['is_split'] == 1){
                        echo '<td style="border: 1px solid black;font-size: 0.95em;padding-left:7px;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;"><b>SPLIT BATCH</b></p></td>';
                      } else {
                        echo '<td class="text-center" style="border: 1px solid black;font-size: 0.95em;padding-left:7px;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $task[$x]['nobatch']; echo '</p></td>';
                      }
                      echo '<td style="text-align:center; border: 1px solid black;font-size: 0.95em;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $task[$x]['weight']; echo '</p></td>';
                      // echo '<td style="text-align:center; border: 1px solid black;font-size: 1.1em;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $task[$x]['satuan']; echo '</p></td>';
                      echo '<td style="text-align:center; border: 1px solid black;font-size: 0.95em;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $task[$x]['realisasi'].' '.$task[$x]['satuan']; echo '</p></td>';
                      echo '<td style="text-align:center; border: 1px solid black;font-size: 0.95em;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $task[$x]['kode_olah']; echo '</p></td>';
                      echo '<td style="text-align:center; border: 1px solid black;font-size: 0.95em;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $task[$x]['nama_operator']; echo '</p></td>';
                    echo '</tr>';
                }
                if (($task[$x]['is_split'] == 1) and ($task[$x]['no_timbangan'] == $weigher->no_timbangan)){
                    for ($m = 0; $m < $splitTaskQty; $m++) {
                        // if ($splitTask[$m]['kode_bb'] == $task[$x]['kode_bb']){
                        if ($splitTask[$m]['log_penimbangan_rm_id'] == $task[$x]['id']){
                            echo '<tr>';
                              echo '<td style="text-align:center; border: 1px solid black;font-size: 0.95em; margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">';echo $no; echo'.';echo $splitTask[$m]['urutan']; echo '</p></td>';
                              echo '<td style="text-align:center; border: 1px solid black;font-size: 0.95em;padding-left:7px;margin: 7px 7px 7px 7px; "><p style="margin: 7px 7px 7px 7px; text-align:center;">-</p></td>';
                              echo '<td style="text-align:center; border: 1px solid black;font-size: 0.95em;padding-left:7px;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px; text-align:center;">-</p></td>';
                              echo '<td style="border: 1px solid black;font-size: 0.95em;padding-left:7px;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $splitTask[$m]['no_batch']; echo '</p></td>';
                              echo '<td style="text-align:center; border: 1px solid black;font-size: 0.95em;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px; text-align:center;">-</p></td>';
                              echo '<td style="text-align:center; border: 1px solid black;font-size: 0.95em;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $splitTask[$m]['realisasi'].' '.$task[$x]['satuan']; echo '</p></td>';
                              echo '<td style="text-align:center; border: 1px solid black;font-size: 0.95em;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $task[$x]['kode_olah']; echo '</p></td>';
                              echo '<td style="text-align:center; border: 1px solid black;font-size: 0.95em;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">-</p></td>';
                            echo '</tr>';
                        }
                    }
                }
            }
            echo '</tbody>
                </table><br>';
        }

    ?>
      <br>
      <table style="margin:0px; border-collapse: collapse;">
        <tbody>
            <tr>
                <td style="text-align:center; width:400px">
                    <p style="font-size: 1.1em;">Kapten Line Timbang</p>
                    <br>
                    <br>
                    <br>
                    <br>
                    <p style="font-size: 1.1em; text-decoration: overline;">Tanggal : <?php echo $date; ?></p>
                </td>
                <td style="text-align:center; width:400px">
                    <p style="font-size: 1.1em;">Pemeriksa</p>
                    <br>
                    <br>
                    <br>
                    <br>
                    <p style="font-size: 1.1em; text-decoration: overline;">Tanggal : <?php echo $date; ?></p>
                </td>

                <!-- <td style="text-align: center; border: 1px solid black;width:300px;height:200px">
                    <p style="margin-top:0px; margin-bottom:3px; font-size: 1.1em; text-align: center;">Label Checker</p>
                </td> -->
            </tr>
        </tbody>
    </table>

</div>



<!-- <table style="margin:0px; border-collapse: collapse;">
        <thead>
          <tr>
            <th class="text-center" style="width: 40px; font-size:1.3em;border: 1px solid black;">
              <b>No.</b>
            </th>
            <th class="text-center" style="width: 160px; font-size:1.3em;border: 1px solid black;">
              <b>Kode Bahan</b>
            </th>
            <th class="text-center" style="width: 160px; font-size:1.3em;border: 1px solid black;">
              <b>Nama Bahan</b>
            </th>
            <th class="text-center" style="width: 120px; font-size:1.3em;border: 1px solid black;">
              <b>No.Batch</b>
            </th>
            <th class="text-center" style="width: 120px; font-size:1.3em;border: 1px solid black;">
              <b>Jumlah Dibutuhkan</b>
            </th>
            <th class="text-center" style="width: 40px; font-size:1.3em;border: 1px solid black;">
              <b>UoM</b>
            </th>
            <th class="text-center" style="width: 120px; font-size:1.3em;border: 1px solid black;">
              <b>Jumlah Ditimbang</b>
            </th>
            <th class="text-center" style="width: 40px; font-size:1.3em;border: 1px solid black;">
              <b>UoM</b>
            </th>
          </tr>
        </thead>
        <tbody>
            <?php for ($x = 0; $x < $taskQty; $x++) {
            echo '<tr>';
              echo '<td style="text-align:center; border: 1px solid black;font-size: 1.1em; margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $x+1; echo '</p></td>';
              echo '<td style="border: 1px solid black;font-size: 1.1em;padding-left:7px;margin: 7px 7px 7px 7px; "><p style="margin: 7px 7px 7px 7px;">'; echo $task[$x]['kode_bb']; echo '</p></td>';
              echo '<td style="border: 1px solid black;font-size: 1.1em;padding-left:7px;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $task[$x]['nama_bb']; echo '</p></td>';
              echo '<td style="border: 1px solid black;font-size: 1.1em;padding-left:7px;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $task[$x]['nobatch']; echo '</p></td>';
              echo '<td style="text-align:center; border: 1px solid black;font-size: 1.1em;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $task[$x]['weight']; echo '</p></td>';
              echo '<td style="text-align:center; border: 1px solid black;font-size: 1.1em;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $task[$x]['satuan']; echo '</p></td>';
              echo '<td style="text-align:center; border: 1px solid black;font-size: 1.1em;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $task[$x]['realisasi']; echo '</p></td>';
              echo '<td style="text-align:center; border: 1px solid black;font-size: 1.1em;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $task[$x]['satuan']; echo '</p></td>';
            echo '</tr>';
          } ?>
        </tbody>
      </table> -->
      <!-- <br>
      <p style="margin-left:20px;">
        <b style="font-size:1.1em;">C. Histori Perbedaan Batch dalam Satu Bahan Baku</b><br>
    </p>

    <table style="margin:0px; border-collapse: collapse;">
        <thead>
          <tr>
            <th class="text-center" style="width: 50px; font-size:1.3em;border: 1px solid black;">
              <b>No. Urut</b>
            </th>
            <th class="text-center" style="width: 200px; font-size:1.3em;border: 1px solid black;">
              <b>Kode Bahan</b>
            </th>
            <th class="text-center" style="width: 200px; font-size:1.3em;border: 1px solid black;">
              <b>Nama Bahan</b>
            </th>
            <th class="text-center" style="width: 180px; font-size:1.3em;border: 1px solid black;">
              <b>No Batch</b>
            </th>
            <th class="text-center" style="width: 160px; font-size:1.3em;border: 1px solid black;">
              <b>Jumlah Ditimbang</b>
            </th>
            <th class="text-center" style="width: 50px; font-size:1.3em;border: 1px solid black;">
              <b>UoM</b>
            </th>
          </tr>
        </thead>
        <tbody>
            <?php for ($x = 0; $x < $splitTaskQty; $x++) {
            echo '<tr>';
              echo '<td style="text-align:center; border: 1px solid black;font-size: 1.1em; margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $splitTask[$x]['urutan']; echo '</p></td>';
              echo '<td style="border: 1px solid black;font-size: 1.1em;padding-left:7px;margin: 7px 7px 7px 7px; "><p style="margin: 7px 7px 7px 7px;">'; echo $splitTask[$x]['kode_bb']; echo '</p></td>';
              echo '<td style="border: 1px solid black;font-size: 1.1em;padding-left:7px;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $splitTask[$x]['nama_bb']; echo '</p></td>';
              echo '<td style="border: 1px solid black;font-size: 1.1em;padding-left:7px;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $splitTask[$x]['no_batch']; echo '</p></td>';
              echo '<td style="text-align:center; border: 1px solid black;font-size: 1.1em;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $splitTask[$x]['realisasi']; echo '</p></td>';
              echo '<td style="text-align:center; border: 1px solid black;font-size: 1.1em;margin: 7px 7px 7px 7px;"><p style="margin: 7px 7px 7px 7px;">'; echo $splitTask[$x]['satuan_realisasi']; echo '</p></td>';
            echo '</tr>';
          } ?>
        </tbody>
      </table> -->