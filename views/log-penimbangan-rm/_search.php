<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogPenimbanganRmSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-penimbangan-rm-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nomo') ?>

    <?= $form->field($model, 'no_formula') ?>

    <?= $form->field($model, 'no_revisi') ?>

    <?= $form->field($model, 'naitem_fg') ?>

    <?php // echo $form->field($model, 'datetime_start') ?>

    <?php // echo $form->field($model, 'datetime_stop') ?>

    <?php // echo $form->field($model, 'datetime_write') ?>

    <?php // echo $form->field($model, 'kode_bb') ?>

    <?php // echo $form->field($model, 'nama_bb') ?>

    <?php // echo $form->field($model, 'kode_olah') ?>

    <?php // echo $form->field($model, 'siklus') ?>

    <?php // echo $form->field($model, 'weight') ?>

    <?php // echo $form->field($model, 'satuan') ?>

    <?php // echo $form->field($model, 'sediaan') ?>

    <?php // echo $form->field($model, 'cycle_time') ?>

    <?php // echo $form->field($model, 'timbangan') ?>

    <?php // echo $form->field($model, 'operator') ?>

    <?php // echo $form->field($model, 'no_timbangan') ?>

    <?php // echo $form->field($model, 'realisasi') ?>

    <?php // echo $form->field($model, 'satuan_realisasi') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
