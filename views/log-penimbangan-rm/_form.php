<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogPenimbanganRm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-penimbangan-rm-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomo')->textInput() ?>

    <?= $form->field($model, 'no_formula')->textInput() ?>

    <?= $form->field($model, 'no_revisi')->textInput() ?>

    <?= $form->field($model, 'naitem_fg')->textInput() ?>

    <?= $form->field($model, 'datetime_start')->textInput() ?>

    <?= $form->field($model, 'datetime_stop')->textInput() ?>

    <?= $form->field($model, 'datetime_write')->textInput() ?>

    <?= $form->field($model, 'kode_bb')->textInput() ?>

    <?= $form->field($model, 'nama_bb')->textInput() ?>

    <?= $form->field($model, 'kode_olah')->textInput() ?>

    <?= $form->field($model, 'siklus')->textInput() ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <?= $form->field($model, 'satuan')->textInput() ?>

    <?= $form->field($model, 'sediaan')->textInput() ?>

    <?= $form->field($model, 'cycle_time')->textInput() ?>

    <?= $form->field($model, 'timbangan')->textInput() ?>

    <?= $form->field($model, 'operator')->textInput() ?>

    <?= $form->field($model, 'no_timbangan')->textInput() ?>

    <?= $form->field($model, 'realisasi')->textInput() ?>

    <?= $form->field($model, 'satuan_realisasi')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
