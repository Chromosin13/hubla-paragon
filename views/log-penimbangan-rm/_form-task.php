<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use app\models\LogPenimbanganRm;
use wbraganca\tagsinput\TagsinputWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterDataFormulaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Weighing Task';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.resp-container {
    /*position: relative;*/
    /*overflow: hidden;*/
    /*padding-top: 56.25%;*/
}

.resp-iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    border: 0;
}
.bawahan {
   position: fixed;
   border-top:2px solid #D3D3D3;
   left: 0;
   bottom: 0;
   width: 100%;
   padding:15px 0;
   background-color: white;
   color: white;
   text-align: center;
}
</style>

<div class="bom-confirmation">

    <div class="box box-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-blue-gradient">
        <h3 class="widget-user-username"><b><?php echo 'Operator '; echo $operator; ?></b></h3>
        <h5 class="widget-user-desc">Weighing Task</h5>
        <h1 style="font-size:5em; font-weight: bolder; position:absolute; right:50px; top:0px">$nama_line</h1>
      </div>
      <div class="widget-user-image">
        <img class="img-circle" src="../web/images/formula.png" alt="User Avatar">
      </div>
    </div>
    <br>
    <br>
    <div class="box box-widget widget-user">
      <table class="table">
        <thead>
          <tr>
            <th class="text-center" style="width: 30%; font-size:1.8em; background: #ffdfba;">
              <b>No MO</b>
            </th>
            <th class="text-center" style="width: 20%; font-size:1.8em; background: #ffffba;">
              <b>No Formula</b>
            </th>
            <th class="text-center" style="width: 10%; font-size:1.8em; background: #baffc9;">
              <b>No Revisi</b>
            </th>
            <th class="text-center" style="width: 40%; font-size:1.8em; background: #bae1ff;">
              <b>Nama Item</b>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-center" style="font-size:1.5em;"><?php echo $nomo; ?></td>
            <td class="text-center" style="font-size:1.5em;"><?php echo $bom2[0]['no_formula']; ?></td>
            <td class="text-center" style="font-size:1.5em;"><?php echo $bom2[0]['no_revisi']; ?></td>
            <td class="text-center" style="font-size:1.5em;"><?php echo $bom[0]['naitem_fg']; ?></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="row">
  <div class="col-lg-12 col-xs-12">
    <p><?php 

          foreach($list_operator as $value){ 
            if($value==$operator_terpilih){
              // echo '<button type="button" class="btn btn-success btn-md active">'.$value.'</button> ';
              echo Html::a($value, ['insert-swo', 'id' => $id, 'nama_operator' => $value], ['class'=>'btn btn-primary btn-block active']);
              echo '  ';
            }else{
              // echo '<button type="button" class="btn btn-warning btn-md">'.$value.'</button> ';
              echo Html::a($value, ['insert-swo', 'id' => $id, 'nama_operator' => $value], ['class'=>'btn btn-default btn-block']);
              echo '  ';
            }
             
          }

        ?>
    </p>
  </div>
</div>
    <br>
    <div class="box box-widget widget-user">
      <table class="table">
        <thead>
          <tr>
            <th class="text-center" style="width: 10%; font-size:1.5em;">
              <b>Kode BB</b>
            </th>
            <th class="text-center" style="width: 15%; font-size:1.5em;">
              <b>Nama BB</b>
            </th>
            <!-- <th class="text-center" style="width: 10%; font-size:1.5em;">
              <b>Kode Olah</b>
            </th> -->
            <th class="text-center" style="width: 5%; font-size:1.5em;">
              <b>Siklus</b>
            </th>
            <th class="text-center" style="width: 10%; font-size:1.5em;">
              <b>Weight</b>
            </th>
            <th class="text-center" style="width: 5%; font-size:1.5em;">
              <b>Satuan</b>
            </th>
            <th class="text-center" style="width: 10%; font-size:1.5em;">
              <b>Sediaan</b>
            </th>
            <!-- <th class="text-center" style="width: 10%; font-size:1.5em;">
              <b>Cycle Time</b>
            </th>
            <th class="text-center" style="width: 10%; font-size:1.5em;">
              <b>Timbangan</b>
            </th>
            <th class="text-center" style="width: 5%; font-size:1.5em;">
              <b>Operator</b>
            </th>
            <th class="text-center" style="width: 10%; font-size:1.5em;">
              <b>No Timbangan</b>
            </th> -->
          </tr>
        </thead>
        <tbody>
          <?php for ($x = 0; $x < $bomRows; $x++) {
            echo '<tr>';
              echo '<td class="text-center">'; echo $bom2[$x]['kode_bb']; echo '</td>';
              echo '<td class="text-center">'; echo $bom2[$x]['nama_bb']; echo '</td>';
              // echo '<td class="text-center">'; echo $bom2[$x]['kode_olah']; echo '</td>';
              echo '<td class="text-center">'; echo $bom2[$x]['siklus']; echo '</td>';
              echo '<td class="text-center">'; echo $bom2[$x]['weight']; echo '</td>';
              echo '<td class="text-center">'; echo $bom2[$x]['satuan']; echo '</td>';
              echo '<td class="text-center">'; echo $bom2[$x]['sediaan']; echo '</td>';
              // echo '<td class="text-center">'; echo $bom2[$x]['cycle_time']; echo '</td>';
              // echo '<td class="text-center">'; echo $bom2[$x]['timbangan']; echo '</td>';
              // echo '<td class="text-center">'; echo $bom2[$x]['operator']; echo '</td>';
              // echo '<td class="text-center">'; echo $bom2[$x]['no_timbangan']; echo '</td>';
            echo '</tr>';
          } ?>
        </tbody>
      </table>

      <div class="bawahan">
        <div class="row">
          <div style="width:100%; padding:0 30px;">
            <?php echo '<a href="?r=log-penimbangan-rm/start&nomo='.$nomo.'&no_formula='.$bom2[0]['no_formula'].'&no_revisi='.$bom2[0]['no_revisi'].'" id="nextbutton" class="btn btn-block btn-info"><b>NEXT</b></a>'; ?>
          </div>
        </div>
    </div>
      
        
    </div>
     
</div>

<?php
$script = <<< JS

// AutoFocus Nomo Field

document.getElementById("nextbutton").focus();

// Trigger Query

$('#nextbutton').change(function(){
  // var files = $('#files')[0].files;
  // var error = '';
  // var form_data = new FormData();
    $.ajax({
        url:"log-penimbangan-rm/coba",
        method:"POST",
        data:{halo:"1"},
        contentType:false,
        cache:false,
        processData:false,
        beforeSend:function()
        {
            $('#uploaded_images').html("<label class='text-success'>Uploading...</label>");
        },
        success:function(data)
        {
            $('#uploaded_images').html(data);
            //$('#files').val('');
        }
    })
});

// $('#flowinputmo-nomo').change(function(){  

//     var nomo = $('#flowinputmo-nomo').val().trim();
    
//     if(nomo){
//       window.location = "index.php?r=flow-input-mo/create-pengolahan&nomo="+nomo;
//     }else{
//       alert('Nomor Jadwal kosong!');
//     }
    
// });


JS;
$this->registerJs($script);
?>