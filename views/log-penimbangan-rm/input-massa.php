<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use app\models\LogPenimbanganRm;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm  */
?>

<link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/normalize.css@8.0.0/normalize.css">
  <link rel="preload" as="style" onload="this.rel='stylesheet';this.onload=null"
    href="https://unpkg.com/milligram@1.3.0/dist/milligram.min.css">
<style>
.column {
  float: left;
  width: 50%;
}

.accordion {
  
  color: white;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  transition: 0.4s;
}

.active, .accordion:hover {
  background-color: #ccc;
}

.accordion:after {
  content: '\002B';
  color: white;
  font-weight: bold;
  float: right;
  margin-left: 5px;
}

.active:after {
  content: "\2212";
  color: white;
}

.panel {
  padding: 0 18px;
  background-color: white;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}  
</style>

<div class="box box-widget widget-user" id="main-box" >
  <!-- Add the bg color to the header using any of the bg-* classes -->
  <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
    <h3 class="widget-user-username"><b>Realisasi Penimbangan</b></h3>
    <h5 class="widget-user-desc">Manual Input</h5>
  </div>
  <div class="widget-user-image">
    <img class="img-circle" src="../web/images/scale.png" alt="User Avatar">
  </div>
  <div class="box-footer">
    
    <div class="widget-user-header " style="width:90%; text-align: center; display: block; margin-left: auto; margin-right: auto; background-color : #E6E6E6; height:100%;">
      <h4 class="widget-user-username" style="font-size:1em;"><b><?php echo $kode_bb;?></b></h4>
      <h3 class="widget-user-username" style="font-size:1em;"><b><?php echo $kode_internal;?></b></h3>
      <h2 class="widget-user-desc" style="margin-bottom: 3px;"><b><?php echo $nama_bb;?></b></h2>
      <h4 class="widget-user-desc" style="color:blue; margin-bottom: 0px; margin-top:0px;">Jumlah dibutuhkan : <b><?php echo $weight;?></b> <?php echo $satuan;?></h4>
      <h4 class="widget-user-desc" style="color:black; margin-bottom: 0px; margin-top:0px;">Timbangan : <b><?php echo $timbangan;?></b></h4>
    </div>
    <br>    
    <div class="col-md-6 column" style="text-align: center;">
      <input type="text" id="timbangan" value="<?php echo $timbangan;?>" >
      <label for="massa">Jumlah Realisasi</label>
      <input type="text" name="massa" id="massa" onchange="massa_change()">
      <!-- /input-group -->
    </div>
    <div class="col-md-6 column" style="text-align: center;">
       <label for="sat">UoM</label>
      <?php echo "<input disabled type='text' name='sat' id='sat' value='"; echo $satuan; echo"'>";?>
      <!-- /input-group -->
    </div>
    <br>
    <button onclick="update()" class="bg-green-gradient" style="display: block; margin-left: auto; margin-right: auto; border-radius: 10px; width:95%; font-size:1.8em;">Submit</button>
    <!-- /.row -->
  </div>
  <br>
  <div style="text-align:center;">
    <button id="addbatch" onclick="confirm()" class="wl-text btn btn-warning" style="top:87%; "><b>TAMBAH BATCH</b></button>
  </div>
  <br>
</div>

<div id="modal" style="text-align: center; position: absolute; z-index: 10; top: 50%; left: 50%; transform: translate(-50%, -50%); background-color: white; width:70%; height:22vh; border-radius: 5px; display: none;">
  <div id="modal-header" style="background-color: #5DADE2; margin-bottom: 0px; border-top-left-radius: 5px; border-top-right-radius: 5px;">
    <p style="text-align:left; padding-left : 20px; padding-top : 6px; font-size:1.5em;"><b>Confirmation</b></p>
  </div>
  <div id="modal-content" style="height:30%;">
    <p style="padding-top:20px; font-size:1.2em;">Apakah anda yakin untuk menambah batch?</p>
  </div>
  <hr>
  <div id="modal-footer" style="margin-top:0px;">
    <button id="addbatch" onclick="no()" class="btn btn-danger" style="position: absolute; top: 83%; left: 35%; transform: translate(-50%, -50%); width:20%;">
      <b>TIDAK</b>
    </button>
    <button id="addbatch" onclick="yes()" class="btn btn-success" style="position: absolute; top: 83%; left: 65%; transform: translate(-50%, -50%); width:20%;">
      <b>YA</b>
    </button>

  </div>
</div>

<script type="text/javascript">
var acc = document.getElementsByClassName("accordion");
var i;
var log = '<?php echo $log ?>';
var dnsUrl = '<?php echo Yii::getAlias('@dnsUrl') ?>';
//console.log(log);

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
  
function update(){
  var id = <?php echo $id; ?>;
  var wo = <?php echo $wo; ?>;
  var realisasi = document.getElementById("massa").value; 


  //window.alert(data2);

  $.get('index.php?r=log-penimbangan-rm/update-log',{ id:id, wo : wo, realisasi : realisasi, log : log, log_realisasi : 'manual'},function(data2){

    var data2 = $.parseJSON(data2);
    //window.alert(data2);
    if(data2.status=="success"){
      window.location = "index.php?r=log-penimbangan-rm/task&id="+id;
    } else if (data2.status=="not-success"){
      window.location = "index.php?r=log-penimbangan-rm/input-massa&id="+id+"&wo="+wo+"&log="+log;
    } else {
      window.alert("Massa tidak sesuai standar");
    }
  });
}

function confirm(){
  document.getElementById("main-box").style.opacity = "0.1";
  document.getElementById("modal").style.display = "block";
}

function yes(){
  //window.location = "http://factory.pti-cosmetics.com/flowreport_gustav/web/index.php?r=log-penimbangan-rm/scan-bb&id="+id+"&wo="+wo;
  realisasi = document.getElementById("massa").value;
  var id = <?php echo $id; ?>;
  var wo = <?php echo $wo; ?>;

  if(realisasi != null){
    $.get('index.php?r=log-penimbangan-rm/add-batch',{ id : id, wo : wo, realisasi : realisasi, log : log, log_realisasi : 'manual'},function(data2){
      var data2 = $.parseJSON(data2);
      //window.alert(data2);
      if(data2.status=="success"){
        window.location = dnsUrl+"log-penimbangan-rm/scan-bb&id="+id+"&wo="+wo;
      } else if (data2.status=='massa-null'){
        window.alert('Selisih massa dengan batch sebelumnya 0, data tidak bisa ditambahkan');
        // window.location = dnsUrl+"log-penimbangan-rm/scan-bb&id="+id+"&wo="+wo;
      } else{
        window.location = "index.php?r=log-penimbangan-rm/input-massa&id="+id+"&wo="+wo+"&log="+log;
      }
    });
  } else {
    window.alert("Tolong Lengkapi Jumlah Realisasi Terlebih Dahulu");
  }
}

function no(){
  document.getElementById("main-box").style.opacity = "1";
  document.getElementById("modal").style.display = "none";
}

function massa_change(){
    // this.value = this.value.replace(/,/g, '.')
    var timbangan = $('input#timbangan').val();

    $.post("index.php?r=log-formula-breakdown/get-decimal-digit&timbangan="+timbangan, function (data){
      var decimal_digit = parseInt(data);
      var nilai = document.getElementById("massa").value.replace(/,/g, '.');
      var nilai = parseFloat(Number(nilai).toFixed(decimal_digit));
      document.getElementById("massa").value = nilai;
    });
}
</script>

<?php
$script = <<< JS
$('#timbangan').hide();
// $('#massa').on("focusout blur change",function() {
//     var nilai = $('#massa').val();
//     var timbangan = $('#timbangan').val();

//     $.post("index.php?r=log-formula-breakdown/get-decimal-digit&timbangan="+timbangan, function (data){
//       var decimal_digit = parseInt(data);
//       nilai = Number(nilai).toFixed(decimal_digit);
//       $('#massa').attr('value',nilai);
//     });

    
//   });

// function update(){
//   var realisasi = $('#realisasi').val();

document.getElementById("massa").focus();

// document.getElementById("massa").addEventListener('input', function (e) {
//         e.target.value = e.target.value.replace(/,/g, '.');
//     } 
// }); 


//   $.get('index.php?r=log-penimbangan-rm/update-log',{ wo : wo, realisasi : realisasi, satuan:satuan },function(data2){

//     var data2 = $.parseJSON(data2);
//     window.alert(data2);
//     // if(data2.page=="create"){
//     //   window.location = "index.php?r=flow-input-mo/create-penimbangan-rm&nomo="+result.text;
//     // } else if (data2.page == "bom-confirmation"){
//     //   window.location = "index.php?r=flow-input-mo/check-bom-rm&id="+data2.id;
//     // } else if (data2.page == "task-kapten"){
//     //   window.location = "index.php?r=log-penimbangan-rm/task&id="+data2.id;
//     // } else if (data2.page == "task-operator"){
//     //   window.location = "index.php?r=log-penimbangan-rm/task&id="+data2.id;
//     // } else if (data2.page == "stay"){
//     //   window.location = "index.php?r=flow-input-mo/check-penimbangan-rm-qr";
//     // } else {
//     //   window.alert("HALO");
//     // }
//   });
// }

JS;
$this->registerJs($script);
?>
