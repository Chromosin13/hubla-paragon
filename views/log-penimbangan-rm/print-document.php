<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\grid\GridView;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use app\models\FlowInputMo;
use wbraganca\tagsinput\TagsinputWidget;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Kendala;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;
use yii\helpers\ArrayHelper;
use app\models\Downtime;
use app\models\DowntimeSearch;
use yii\widgets\Pjax; 
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $model app\models\FlowInputMo */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
.bawahan {
   position: fixed;
   border-top:2px solid #D3D3D3;
   left: 0;
   bottom: 0;
   width: 100%;
   padding:15px 0;
   background-color: white;
   color: white;
   text-align: center;
}
</style>

<script>
function finish(){
  var id = <?php echo $id; ?>;

    $.get('index.php?r=log-penimbangan-rm/stop-jadwal-penimbangan',{ id:id},function(data2){

      var data2 = $.parseJSON(data2);
      //window.alert(data2);
      if(data2.status=="success"){
        //window.location = "index.php?r=flow-input-mo/check-penimbangan-rm";
        window.location = "index.php?r=downtime/index-penimbangan&id="+id;
      } else {
        window.location = "index.php?r=log-penimbangan-rm/print-document&id="+id;
      }
    });
  //}
}

</script>

 <div class="box box-widget widget-user">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-blue-gradient">
        <h3 class="widget-user-username"><b>Cetak Batch Record dan Label Palet</b></h3>
        <h5 class="widget-user-desc">Generating PDF</h5>
    </div>
    <div class="widget-user-image">
        <img class="img-circle" src="../web/images/formula.png" alt="User Avatar">
    </div>
    <br>
    <br>
    <div class="box-footer">
        <div style="text-align: center;">
          <button class="btn-hero-success" style="width:200px; border-radius:5px;">
            <?php echo "<a style='font-size:2em;' href='http://factory.pti-cosmetics.com/flowreport/web/index.php?r=log-penimbangan-rm/pdf&id=";
            echo $id;
            echo "' target='_blank'>Cetak BR</a>" ?>
          </button>
        </div>
        <!-- /.row -->
    </div>
    <br>
    <br>
</div>
<?php if($operator==1) {
  echo '<div class="bawahan">
      <div class="row">
        <div style="width:100%; padding:0 30px;">
          <button onclick="finish()"id="nextbutton" class="btn btn-block btn-info"><b>FINISH</b></button>
        </div>
      </div>
  </div>';
  } ?>


          


<?php
$script = <<< JS



JS;
$this->registerJs($script);
?>