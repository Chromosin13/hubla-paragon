<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MonitoringLinePengolahan */

$this->title = 'Create Monitoring Line Pengolahan';
$this->params['breadcrumbs'][] = ['label' => 'Monitoring Line Pengolahans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monitoring-line-pengolahan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
