<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MonitoringLinePengolahanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="monitoring-line-pengolahan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sediaan') ?>

    <?= $form->field($model, 'nama_line') ?>

    <?= $form->field($model, 'nama_fg') ?>

    <?= $form->field($model, 'nomo') ?>

    <?= $form->field($model, 'datetime_write') ?>

    <?php echo $form->field($model, 'jenis_proses') ?>

    <?php echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
