<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DeviceMapQcbulk */

$this->title = 'Create Device Map Qcbulk';
$this->params['breadcrumbs'][] = ['label' => 'Device Map Qcbulks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="device-map-qcbulk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
