<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DeviceMapQcbulk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="device-map-qcbulk-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sbc_ip')->textInput() ?>

    <?= $form->field($model, 'frontend_ip')->textInput() ?>

    <?= $form->field($model, 'keterangan')->textInput() ?>

    <?= $form->field($model, 'sbc_user')->textInput() ?>

    <?= $form->field($model, 'posisi')->textInput() ?>

    <?= $form->field($model, 'mac')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
