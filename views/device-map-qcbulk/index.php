<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DeviceMapQcbulkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Device Map Qcbulks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="device-map-qcbulk-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Device Map Qcbulk', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'sbc_ip',
            'frontend_ip',
            'keterangan',
            'sbc_user',
            // 'posisi',
            // 'mac',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
