<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DeviceMapQcbulk */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Device Map Qcbulks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="device-map-qcbulk-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'sbc_ip',
            'frontend_ip',
            'keterangan',
            'sbc_user',
            'posisi',
            'mac',
        ],
    ]) ?>

</div>
