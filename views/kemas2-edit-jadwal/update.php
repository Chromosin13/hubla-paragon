<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kemas2EditJadwal */

$this->title = 'Update Kemas2 Edit Jadwal: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kemas2 Edit Jadwals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kemas2-edit-jadwal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
