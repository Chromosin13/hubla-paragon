<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Kemas2EditJadwal */

$this->title = 'Create Kemas2 Edit Jadwal';
$this->params['breadcrumbs'][] = ['label' => 'Kemas2 Edit Jadwals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kemas2-edit-jadwal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
