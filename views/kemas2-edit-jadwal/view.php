<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Kemas2EditJadwal */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kemas2 Edit Jadwals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kemas2-edit-jadwal-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'snfg',
            'snfg_komponen',
            'lanjutan',
            'start',
            'stop',
            'jenis_kemas',
            'nama_line',
            'nama_operator',
            'lanjutan_split_batch',
            'jumlah_realisasi',
            'nobatch',
            'is_done',
            'start_id',
            'stop_id',
        ],
    ]) ?>

</div>
