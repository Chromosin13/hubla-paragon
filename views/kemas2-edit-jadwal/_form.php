<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kemas2EditJadwal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kemas2-edit-jadwal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'snfg_komponen')->textInput() ?>

    <?= $form->field($model, 'lanjutan')->textInput() ?>

    <?= $form->field($model, 'start')->textInput() ?>

    <?= $form->field($model, 'stop')->textInput() ?>

    <?= $form->field($model, 'jenis_kemas')->textInput() ?>

    <?= $form->field($model, 'nama_line')->textInput() ?>

    <?= $form->field($model, 'nama_operator')->textInput() ?>

    <?= $form->field($model, 'lanjutan_split_batch')->textInput() ?>

    <?= $form->field($model, 'jumlah_realisasi')->textInput() ?>

    <?= $form->field($model, 'nobatch')->textInput() ?>

    <?= $form->field($model, 'is_done')->textInput() ?>

    <?= $form->field($model, 'start_id')->textInput() ?>

    <?= $form->field($model, 'stop_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
