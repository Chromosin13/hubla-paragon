<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\LogRework */

$this->title = $model->uid;
$this->params['breadcrumbs'][] = ['label' => 'Log Reworks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-rework-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->uid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->uid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'snfg',
            'snfg_komponen',
            'nomo',
            'posisi',
            'datetime_start',
            'datetime_stop',
            'datetime_write',
            'nama_operator',
            'lanjutan',
            'jenis_proses',
            'nama_line',
            'id',
            'uid',
        ],
    ]) ?>

</div>
