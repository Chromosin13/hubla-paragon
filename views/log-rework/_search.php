<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogReworkSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-rework-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'snfg_komponen') ?>

    <?= $form->field($model, 'nomo') ?>

    <?= $form->field($model, 'posisi') ?>

    <?= $form->field($model, 'datetime_start') ?>

    <?php // echo $form->field($model, 'datetime_stop') ?>

    <?php // echo $form->field($model, 'datetime_write') ?>

    <?php // echo $form->field($model, 'nama_operator') ?>

    <?php // echo $form->field($model, 'lanjutan') ?>

    <?php // echo $form->field($model, 'jenis_proses') ?>

    <?php // echo $form->field($model, 'nama_line') ?>

    <?php // echo $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'uid') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
