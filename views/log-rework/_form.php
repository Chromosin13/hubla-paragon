<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogRework */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-rework-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'snfg_komponen')->textInput() ?>

    <?= $form->field($model, 'nomo')->textInput() ?>

    <?= $form->field($model, 'posisi')->textInput() ?>

    <?= $form->field($model, 'datetime_start')->textInput() ?>

    <?= $form->field($model, 'datetime_stop')->textInput() ?>

    <?= $form->field($model, 'datetime_write')->textInput() ?>

    <?= $form->field($model, 'nama_operator')->textInput() ?>

    <?= $form->field($model, 'lanjutan')->textInput() ?>

    <?= $form->field($model, 'jenis_proses')->textInput() ?>

    <?= $form->field($model, 'nama_line')->textInput() ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'uid')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
