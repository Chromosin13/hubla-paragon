<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LogRework */

$this->title = 'Update Log Rework: ' . $model->uid;
$this->params['breadcrumbs'][] = ['label' => 'Log Reworks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->uid, 'url' => ['view', 'id' => $model->uid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="log-rework-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
