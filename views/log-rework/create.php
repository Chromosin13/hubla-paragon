<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LogRework */

$this->title = 'Create Log Rework';
$this->params['breadcrumbs'][] = ['label' => 'Log Reworks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-rework-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
