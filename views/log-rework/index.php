<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use app\models\LogRework;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogReworkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>


<div class="callout callout-success">
  <h4>Log Rework</h4>

  <p> Data Realtime </p>
  <p> Gabungan Rework dari seluruh jadwal yang melalui seluruh proses</p>

</div>

<p></p>
Download All Data Here
    <?php 

        $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],
                'snfg',
            'snfg_komponen',
            'nomo',
            'posisi',
            'datetime_start',
            'datetime_stop',
            'datetime_write',
            'nama_operator',
            'lanjutan',
            'jenis_proses',
            'nama_line',
            'id',
            'uid',
        ['class' => 'kartik\grid\ActionColumn'],
        ];

        echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
  
    ]);?>
<p></p>  

<div class="log-rework-index">

    <?php echo
     GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
            'floatHeader'=>'true',
            'floatHeaderOptions' => ['position' => 'absolute',],
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'danger', 'heading'=>'Log Rework'],
            'formatter' => [
                'class' => 'yii\i18n\Formatter',
                'timeZone' => 'Asia/Jakarta'
            ],
            'toolbar' => [
            ],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'snfg',
            'snfg_komponen',
            'nomo',
            'posisi',
            'datetime_start',
            'datetime_stop',
            'datetime_write',
            'nama_operator',
            'lanjutan',
            'jenis_proses',
            'nama_line',
            'id',
            'uid',

        ],
    ]); ?>
</div>
