<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MasterDataKoliSerach */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-data-koli-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'koitem') ?>

    <?= $form->field($model, 'naitem') ?>

    <?= $form->field($model, 'qty') ?>

    <?= $form->field($model, 'inner_box') ?>

    <?php // echo $form->field($model, 'sediaan') ?>

    <?php // echo $form->field($model, 'qty_inside_innerbox') ?>

    <?php // echo $form->field($model, 'qty_innerbox_per_karbox') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
