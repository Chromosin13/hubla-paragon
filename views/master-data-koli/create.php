<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterDataKoli */

$this->title = 'Create Master Data Koli';
$this->params['breadcrumbs'][] = ['label' => 'Master Data Kolis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-data-koli-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
