<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MasterDataKoliSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Data Kolis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-data-koli-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Master Data Koli', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'koitem',
            'naitem',
            'qty',
            'inner_box',
            // 'sediaan',
            'qty_inside_innerbox',
            // 'qty_innerbox_per_karbox',
            'timestamp',
            'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
