<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MasterDataKoli */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-data-koli-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'koitem')->textInput() ?>

    <?= $form->field($model, 'qty')->textInput() ?>

    <?= $form->field($model, 'inner_box')
                ->dropDownList(
                    ['0' => 'Tidak', '1' => 'Ya'],           // Flat array ('id'=>'label')
                    ['prompt'=>'Pilih']    // options
                )->label('Apakah menggunakan Innerbox?'); ?>

    <?= $form->field($model, 'sediaan')->textInput()->label('Sediaan (L/P/S)'); ?>

    <?= $form->field($model, 'qty_inside_innerbox')->textInput(['placeholder'=>'Isi 0 jika tidak pakai innerbox']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
