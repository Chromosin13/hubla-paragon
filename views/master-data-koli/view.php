<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MasterDataKoli */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Master Data Kolis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-data-koli-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'koitem',
            'naitem',
            'qty',
            'inner_box',
            'sediaan',
            'qty_inside_innerbox',
            'qty_innerbox_per_karbox',
            'timestamp',
            'updated_by',
        ],
    ]) ?>

</div>
