<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MasterBahanBakuRepack */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-bahan-baku-repack-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kode_internal')->textInput() ?>

    <?= $form->field($model, 'nama_bb')->textInput() ?>

    <?= $form->field($model, 'netto_repack')->textInput() ?>

    <?= $form->field($model, 'galat')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
