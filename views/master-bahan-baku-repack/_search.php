<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MasterBahanBakuRepackSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-bahan-baku-repack-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'kode_internal') ?>

    <?= $form->field($model, 'nama_bb') ?>

    <?= $form->field($model, 'netto_repack') ?>

    <?= $form->field($model, 'galat') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
