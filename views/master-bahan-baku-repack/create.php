<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterBahanBakuRepack */

$this->title = 'Create Master Bahan Baku Repack';
$this->params['breadcrumbs'][] = ['label' => 'Master Bahan Baku Repacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-bahan-baku-repack-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
