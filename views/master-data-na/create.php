<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterDataNa */

$this->title = 'Create Master Data Na';
$this->params['breadcrumbs'][] = ['label' => 'Master Data Nas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-data-na-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
