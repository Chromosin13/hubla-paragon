<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MasterDataNa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-data-na-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'koitem')->textInput()->label('Kode Item'); ?>

    <?= $form->field($model, 'nama_item')->textInput()->label('Nama Item'); ?>

    <?= $form->field($model, 'na_number')->textInput(['placeholder'=>'contoh : NA12801301429'])->label('Nomor NA (Cantumkan huruf depan : NA , NC, dll)'); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
