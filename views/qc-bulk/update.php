<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\QcBulk */

$this->title = 'Update Qc Bulk: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Qc Bulks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="qc-bulk-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_update', [
        'model' => $model,
        'modelsKendala' => $modelsKendala,
    ]) ?>

</div>
