<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\qcbulk;
use faryshta\widgets\JqueryTagsInput;

/* @var $this yii\web\View */
/* @var $model app\models\QcBulk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qcbulk-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="box">
            <div class="box-header with-border">
              <a class="btn btn-app" id="view-scm">
                <i class="fa fa-play"></i> View SCM Data
              </a>

                <?= $form->field($model, 'snfg_komponen')->textInput() ?>

                <?= $form->field($model, 'snfg')->textInput(['readonly' => 'true']) ?>
                <b>Streamline</b>
                <b>Streamline</b>
                <input type="text" class="form-control" id="streamline" disabled>
                <b>Start</b>
                <input type="text" class="form-control" id="start" placeholder="" disabled>
                <b>Due</b>
                <input type="text" class="form-control" id="due" placeholder="" disabled>
                <b>Nama Bulk</b>
                <input type="text" class="form-control" id="nama-bulk" placeholder="" disabled>
                <b>Nama FG</b>
                <input type="text" class="form-control" id="nama-fg" placeholder="" disabled>
                <b>Besar Batch</b>
                <input type="text" class="form-control" id="besar-batch" placeholder="" disabled>
                <b>Besar per lot (Kg)</b>
                <input type="text" class="form-control" id="besar-lot" placeholder="" disabled>
                <b>Lot ke-</b>
                <input type="text" class="form-control" id="lot" placeholder="" disabled>
                <b>Kode Jadwal</b>
                <input type="text" class="form-control" id="kode-jadwal" placeholder="" disabled>
                <b>No Batch</b>
                <input type="text" class="form-control" id="no-batch" placeholder="" disabled>
                <b>Besar Batch Real</b>
                <input type="text" class="form-control" id="besar-batch-real" placeholder="" disabled>
            </div>
    </div>
    

    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'jenis_periksa')->textInput(['readonly' => 'true']); ?>

    <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>
                
    <div class="box" id="qcbulk-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Entry Data</h2>

                <?= $form->field($model, 'ph')->textInput() ?>

                <?= $form->field($model, 'viskositas')->textInput() ?>

                <?= $form->field($model, 'berat_jenis')->textInput() ?>

                <?= $form->field($model, 'kadar')->textInput() ?>

                <?= $form->field($model, 'warna')->textInput() ?>

                <?= $form->field($model, 'bau')->textInput() ?>

                <?= $form->field($model, 'performance')->textInput() ?>

                <?= $form->field($model, 'bentuk')->textInput() ?>

                <?= $form->field($model, 'mikro')->textInput() ?>

                <?= $form->field($model, 'kejernihan')->textInput() ?>

                <?= $form->field($model, 'status')->dropDownList(['RELEASE' => 'RELEASE','REWORK' => 'REWORK','RELEASE_UNCOMFORMITY' => 'RELEASE_UNCOMFORMITY','REJECT' => 'REJECT','PENDING_1' => 'PENDING_1','PENDING_2' => 'PENDING_2','ADJUST' => 'ADJUST','PENDING_MIKRO' => 'PENDING_MIKRO'],['prompt'=>'Select Option']); ?>
            </div>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
$('#view-scm').click(function(){
    var snfg_komponen = $('#qcbulk-snfg_komponen').val();
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#qcbulk-snfg').attr('value',data.snfg);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
        $('#besar-batch').attr('value',data.besar_batch);
        $('#besar-lot').attr('value',data.besar_lot);
        $('#lot').attr('value',data.lot);
        $('#kode-jadwal').attr('value',data.kode_jadwal);
    });
    $.get('index.php?r=pengolahan/get-pengolahan-komponen',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#no-batch').attr('value',data.nobatch);
        $('#besar-batch-real').attr('value',data.besar_batch_real);
    });
});


JS;
$this->registerJs($script);
?>