<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
//use app\models\qcbulk;
use faryshta\widgets\JqueryTagsInput;
use kartik\checkbox\CheckboxX;
/* @var $this yii\web\View */
/* @var $model app\models\QcBulk */
/* @var $form yii\widgets\ActiveForm */
use app\models\QcBulk;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\dialog\Dialog;
use yii\web\JsExpression;

?>

<div class="qcbulk-form">

    <?php $form = ActiveForm::begin(['id'=>'dynamic-form']); ?>

    <div class="info-box bg-aqua">
        <span class="info-box-icon"><i class="fa fa-flask"></i></span>

        <div class="info-box-content">
          <b><div class="col-md-3 col-sm-6 col-xs-12" id="nama_bulk_results"></div></b>
          <!-- <div id="nama_fg_results"></div> -->
          <span class="info-box-number"><div id="nomo_results"></div><div id="snfg_komponen_results"></div></span>

          <div class="progress">
            <div class="progress-bar" style="width: 100%"></div>
          </div>
              <span class="progress-description">
              <div class="col-md-3 col-sm-6 col-xs-12" id="streamline_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="start_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="due_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="besar_batch_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="besar_lot_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="lot_ke_results"></div>
              <div class="col-md-3 col-sm-6 col-xs-12" id="kode_jadwal_results"></div>
              </span>
        </div>
        <!-- /.info-box-content -->
    </div>


    <div class="box">

            <div class="box-header with-border">        
                    <h4><b>Pilih Jenis Scan</b></h4>            
                    <?=
                            Html::button('<b>Per-MO</b>',['class'=>'btn btn-success', 'id' => 'btn_nomo','style' => 'width: 120px; border-radius: 5px;']);
                    ?>
                    <!--
                    <?=
                            Html::button('<b>Per-Komponen</b>',['class'=>'btn btn-primary', 'id' => 'btn_snfg_komponen', 'style' => 'width: 120px; border-radius: 5px;']);
                    ?>
                    -->
            </div>



            <div style="width:50%; float: left; display: inline-block;" class="box-header with-border">
                    
                    <h2 class="box-title"> <b>Input dan Informasi SNFG dari Planner</b></h2>
                    <br \>
                    <br \>

                    <div id="form_nomo">
                        <?= $form->field($model, 'nomo', ['hintType' => ActiveField::HINT_SPECIAL, 
                                                          'addon' => [
                                                                        'prepend' => [
                                                                            'content'=>'<i class="fa fa-barcode"></i>'
                                                                        ],
                                                                        'append' => [
                                                                            'content' => Html::button('Tekan Setelah Scan',['class'=>'btn btn-primary','id'=>'afterScnBtnNomo']),
                                                                            'asButton' => true
                                                                        ]
                                                         ]
                        ])->textInput(['placeholder'=>'Klik Disini, Scan pada Barcode Nomor MO yang tertera di SPK'])->hint('PASTIKAN CAPS LOCK TIDAK AKTIF!');
                        ?>
                    </div>

                    <div id="form_snfg_komponen">
                        <?= $form->field($model, 'snfg_komponen',  ['hintType' => ActiveField::HINT_SPECIAL, 
                                                                    'addon' =>  [
                                                                                'append' => [
                                                                                    'content'=>'<i class="fa fa-barcode"></i>'
                                                                                ],
                                                                                'prepend' => [
                                                                                    'content' => Html::button('Tekan Setelah Scan',['class'=>'btn btn-primary','id'=>'afterScnBtnKomponen']),
                                                                                    'asButton' => true
                                                                                ]
                                                                    ]
                        ])->textInput(['disabled'=>'true','placeholder'=>'Klik Disini, Scan pada Barcode SNFG Komponen yang tertera di SPK'])->hint('Klik Pada Isian dibawah ini, kemudian arahkan scan pada barcode Komponen yang tertera di SPK');
                        ?>
                    </div>

                    <?= $form->field($model, 'snfg')->textInput(['disabled' => 'true']) ?>
                    <?= $form->field($model, 'jenis_olah')->textInput(['id'=>'qcbulk-jenis_olah','readonly' => 'true']) ?> 
                    <br \>

                    <div id="form_start_stop">

                        <a class="btn btn-danger" id="stop-button">
                                    <i class="fa fa-pause"></i> Stop
                        </a>
                        <p \>
                        <p \>
                        
                        <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>
                        
                        
                    </div>


            </div>
            <div style="width:50%; display: inline-block;" class="box-header with-border">
                <h2 class="box-title"><b>Posisi Terakhir</b></h2>
                    <br \>
                    <br \>
                    <b>Posisi</b>
                    <input type="text" class="form-control" id="ppr-posisi" placeholder="" disabled>
                    <p \>
                    <b>State</b>
                    <input type="text" class="form-control" id="ppr-state" placeholder="" disabled>
                    <p \>
                    <b>Jenis Proses</b>
                    <input type="text" class="form-control" id="ppr-jenis-proses" placeholder="" disabled>
                    <p \>
                    <b>Lanjutan</b>
                    <input type="text" class="form-control" id="ppr-lanjutan" placeholder="" disabled>
                    <p \>
                    <b>Is Done</b>
                    <input type="text" class="form-control" id="ppr-isdone" placeholder="" disabled>
                    <br \>
                    <br \>
            </div>
    </div>


    <div id="form_jenis">
        <!-- Jenis Periksa Dropdown 00 -->

        <div id="qcbulk-jenis_periksa_00">     
        <?= $form->field($model, 'jenis_periksa')->dropDownList(['JADWAL BARU' => 'JADWAL BARU','REWORK' => 'REWORK'],['prompt'=>'Select Option']); ?>
        </div>


        <!-- Jenis Periksa  Auto Assign -->

        <div id="qcbulk-jenis_periksa_0"> 
        <?= $form->field($model, 'jenis_periksa')->textInput(['id'=>'qcbulk-jenis_periksa_1', 'disabled'=>'true' , 'readonly' =>'true']); ?>
        </div> 

    </div>


    <div id="form_entry_start_stop">

        <div class="box" id="qcbulk-start">
                <div class="box-header with-border">
                  <h2 class="box-title">Start Entry</h2>

                    <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>
                    
                </div>
        </div>

        <div class="box" id="qcbulk-stop">
                <div class="box-header with-border">
                  <h2 class="box-title">Stop Entry</h2>
                     <?= $form->field($model, 'lanjutan')->textInput(['id'=>'qcbulk-lanjutan_stop','readonly' => 'true']) ?> 

                    <?php echo $form->field($model, 'nama_qc', ['hintType' => ActiveField::HINT_SPECIAL,'addon' => ['prepend' => ['content'=>'<i class="fa fa-barcode"></i>']]
                            ])->widget(JqueryTagsInput::className([]))->hint(' <b>Scan</b> : Klik pada isian dibawah kemudian Scan Barcode pada Nametag<p \> <b>Manual</b> : isi dengan Format 4 Huruf Kapital, Contoh : "ABCD" lalu diikuti dengan menekan Enter ');
                    ?>

                    <?= $form->field($model, 'status')->dropDownList(['RELEASE' => 'RELEASE','REWORK' => 'REWORK','RELEASE_UNCOMFORMITY' => 'RELEASE_UNCOMFORMITY','REJECT' => 'REJECT','PENDING_1' => 'PENDING_1','PENDING_2' => 'PENDING_2','ADJUST' => 'ADJUST','PENDING_MIKRO' => 'PENDING_MIKRO'],['prompt'=>'Select Option']); ?>
                    <div id="check_box_is_done">
                    <?php
                        echo '<label class="cbx-label" for="is_done">Is Done?</label>';
                        echo CheckboxX::widget([
                            'model' => $model,
                            'attribute' => 'is_done',
                            'pluginOptions' => [
                                'threeState' => false,
                                'size' => 'lg'
                            ]
                        ]); 
                    ?>
                    </div>
                </div>
        </div>
        
        <div class="form-group" id="create-button">

                <?php echo Dialog::widget([
                        'libName' => 'krajeeDialogCust', // a custom lib name
                        'options' => [  // customized BootstrapDialog options
                            'size' => Dialog::SIZE_LARGE, // large dialog text
                            'type' => Dialog::TYPE_WARNING, // bootstrap contextual color
                            'title' => 'Konfirmasi Input',
                            'buttons' => [
                                [
                                    
                                    'id' => 'btn-ok',
                                    'icon' => 'glyphicon glyphicon-check',
                                    'label' => 'Sudah Benar, Submit!',
                                    'cssClass' => 'btn-primary',
                                    'action' => new JsExpression("function() {
                                        $('#submitButton').submit();            
                                        }")
                                ],
                            ]
                        ]
                    ]);
                ?>
                <button type="button" id="btn-custom" class="btn btn-info">Validate</button>
                <button id="submitButton" type="submit" class="btn btn-success">Creates</button>
                <!--             <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?> 
                -->
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

// Hide Submit Button untuk Konfirmasi Input
$('#submitButton').hide();

// Inisialisasi Form Hide
    $('#start-button').hide();
    $('#stop-button').hide(); 
    $('#form_start_stop').hide(); 
    $('#form_jenis').hide(); 
    $('#form_entry_start_stop').hide();

// After Klik Tombol Tekan Scan Nomo
    // Klik Scan MO
    $('#afterScnBtnNomo').click(function(){
        var nomo = $('#qcbulk-nomo').val();
        if(nomo==""){
            $('#form_start_stop').hide();    
        }
        else {
            $('#form_start_stop').show(); 
        }
    });

    // Klik Scan Komponen
    $('#afterScnBtnKomponen').click(function(){
        $('#form_start_stop').show();  
    });

// After Klik Start Stop Button

    $('#stop-button').click(function(){
        $('#form_jenis').show();
    });

// After Select Jenis Periksa
    
    $('#qcbulk-jenis_periksa').change(function(){
        var jenis_periksa = $('#qcbulk-jenis_periksa').val();
        if(jenis_periksa==""){
            $('#form_entry_start_stop').hide(); 
        } else {
            $('#form_entry_start_stop').show();
            $('#create-button').hide();
            $('#qcbulk-status').change(function(){
                    var status = $('#qcbulk-status').val();
                    if(status==""){
                        $('#create-button').hide();
                    }else{
                        $('#create-button').show();
                    }
                });
        }
    });


    // $('#qcbulk-jenis_periksa').change(function(){
    //     $('#form_entry_start_stop').show();
    // });


// Validate Button

$("#btn-custom").on("click", function() {
    var nomo = $('#qcbulk-nomo').val();
    var state = $('#qcbulk-state').val();
    var jenis_periksa = $('#qcbulk-jenis_periksa').val();
    var jenis_olah = $('#qcbulk-jenis_olah').val();
    var lanjutan = $('#qcbulk-lanjutan').val();
    var lanjutan_stop = $('#qcbulk-lanjutan_stop').val();
    var nama_qc =  $('#qcbulk-nama_qc').val();
    var is_done = $('#qcbulk-is_done').val();

        
        if(lanjutan==null||lanjutan_stop==null){
            alert('Penarikan data Lanjutan Gagal, Mohon refresh halaman dan scan ulang jadwal');
        }else if(is_done==1){
            krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(nomo)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Olah</b><p>' + '<font color="blue"><i>'+(jenis_olah)+'</i></font><p>' + 
                                '<b>Jenis Periksa</b><p>' + '<font color="blue"><i>'+(jenis_periksa)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>' +  
                                '<b>Nama QC</b><p>' + '<font color="blue"><i>'+(nama_qc)+'</i></font><p>' +
                                '<b><font color="blue"><i>Jadwal Sudah Selesai (Tidak Lanjut Shift)</i></font></b><p>',function(result) {}
            );
        }else if(is_done!=1){
            krajeeDialogCust.dialog('<b>Nomor MO</b><p>' + '<font color="blue"><i>'+(nomo)+'</i></font><p>' +
                                '<b>State</b><p>' + '<font color="blue"><i>'+(state)+'</i></font><p>' +
                                '<b>Jenis Olah</b><p>' + '<font color="blue"><i>'+(jenis_olah)+'</i></font><p>' + 
                                '<b>Jenis Periksa</b><p>' + '<font color="blue"><i>'+(jenis_periksa)+'</i></font><p>' + 
                                '<b>Lanjutan</b><p>' + '<font color="blue"><i>'+(lanjutan)+'</i></font><p>' +  
                                '<b>Nama QC</b><p>' + '<font color="blue"><i>'+(nama_qc)+'</i></font><p>' +
                                '<b><font color="blue"><i>Jadwal Akan Dilanjutkan ke Shift Berikutnya (Belum Done)</i></font></b><p>',function(result) {}
            );
        }

});

// 1 / 

$('#form_snfg_komponen').hide(); 
$('#form_nomo').hide(); 

$('#btn_nomo').click(function(){
    document.getElementById("qcbulk-snfg_komponen").disabled = true
    document.getElementById("qcbulk-nomo").disabled = false;
    $('#form_snfg_komponen').hide(); 
    $('#form_nomo').show(); 
});

$('#btn_snfg_komponen').click(function(){
    document.getElementById("qcbulk-snfg_komponen").disabled = false
    document.getElementById("qcbulk-nomo").disabled = true;
    $('#form_snfg_komponen').show();
    $('#form_nomo').hide();
});

$('#qcbulk-snfg_komponen').change(function(){
    var snfg_komponen = $(this).val();
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#qcbulk-snfg').attr('value',data.snfg);
        $('#qcbulk-nomo').attr('value',data.nomo);
        
        $('#streamline_results').html('STREAMLINE : '+data.streamline);
        $('#start_results').html('START : '+ data.start);
        $('#due_results').html('DUE : '+ data.due);
        $('#nama_bulk_results').html(data.nama_bulk);
        $('#nama_fg_results').html(data.nama_fg);
        $('#besar_batch_results').html('BESAR BATCH : '+ data.besar_batch);
        $('#besar_lot_results').html('BESAR LOT : '+ data.besar_lot);
        $('#lot_ke_results').html('LOT KE : '+ data.lot); ;
        $('#kode_jadwal_results').html('KODE JADWAL : '+ data.kode_jadwal); 
        $('#jumlah_results').html('JUMLAH : '+ data.jumlah);

    });
    
    // Get Last Jenis Olah from Pengolahan

    $.get('index.php?r=pengolahan/get-jenis-olah-komponen',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        $('#qcbulk-jenis_olah').attr('value',data.jenis_olah);
    });

    $.get('index.php?r=scm-planner/get-ppr',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#create-button').hide();
            alert('Status ditetapkan sebagai HOLD/PAUSE dari Planner')
        }
        else{
                $.get('index.php?r=scm-planner/get-last',{ snfg_komponen : snfg_komponen },function(data){
                    var data = $.parseJSON(data);
                    if(data.posisi=="QC BULK" && data.state=="START"){
                        $('#start-button').hide();
                        $('#stop-button').show(); 

                        $('#qcbulk-jenis_periksa_00').hide();
                        document.getElementById("qcbulk-jenis_periksa").disabled = true;
                        $('#qcbulk-jenis_periksa_0').show();
                        document.getElementById("qcbulk-jenis_periksa_1").disabled = false;
                        $('#qcbulk-jenis_periksa_1').attr('value',data.jenis_proses);

                        // Assign Lanjutan 


                        $('#stop-button').click(function(){ 

                            var jenis_periksa = $('#qcbulk-jenis_periksa_1').val();
                            var snfg_komponen = $('#qcbulk-snfg_komponen').val();                                
                            
                                $.get('index.php?r=qc-bulk/lanjutan-qc-bulk',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#qcbulk-lanjutan').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);                                                                 
                                
                                });

                        });


                        // EO Lanjutan

                    }else if(data.posisi=="QC BULK" && data.state=="STOP"){
                        $('#start-button').hide();
                        $('#stop-button').show(); 

                        $('#start-button').click(function(){ 

                            $('#qcbulk-jenis_periksa').change(function(){

                                var jenis_periksa = $('#qcbulk-jenis_periksa').val();
                                var snfg_komponen = $('#qcbulk-snfg_komponen').val();                                
                                
                                    $.get('index.php?r=qc-bulk/lanjutan-qc-bulk',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                                        var data = $.parseJSON(data);
                                        //alert(data.sediaan);
                                        $('#qcbulk-lanjutan').attr('value',data.lanjutan);
                                        $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
                                                              
                                    
                                        });
                                });
                            });


                    }else if(data.posisi!="QC BULK" && data!=null){
                        $('#start-button').hide();
                        $('#stop-button').show();
                        alert('Jadwal Baru');
                    
                    }else {
                        $('#start-button').hide();
                        $('#stop-button').hide(); 
                        alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                    }
                });
        }
    });
});

$('#qcbulk-nomo').change(function(){
    var nomo = $(this).val();
    $.get('index.php?r=scm-planner/get-planner-mo',{ nomo : nomo },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#qcbulk-snfg_komponen').attr('value',data.snfg_komponen);
        $('#qcbulk-snfg').attr('value',data.snfg);
        

        $('#streamline_results').html('STREAMLINE : '+data.streamline);
        $('#start_results').html('START : '+ data.start);
        $('#due_results').html('DUE : '+ data.due);
        $('#nama_bulk_results').html(data.nama_bulk);
        $('#nama_fg_results').html(data.nama_fg);
        $('#besar_batch_results').html('BESAR BATCH : '+ data.besar_batch);
        $('#besar_lot_results').html('BESAR LOT : '+ data.besar_lot);
        $('#lot_ke_results').html('LOT KE : '+ data.lot); ;
        $('#kode_jadwal_results').html('KODE JADWAL : '+ data.kode_jadwal); 
        $('#jumlah_results').html('JUMLAH : '+ data.jumlah);
        
    });

    $.get('index.php?r=pengolahan/get-jenis-olah-mo',{ nomo : nomo },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#qcbulk-jenis_olah').attr('value',data.jenis_olah);
    });

    $.get('index.php?r=scm-planner/get-pp',{ nomo : nomo },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });
    $.get('index.php?r=scm-planner/get-planner-mo',{ nomo : nomo },function(data){
        var data = $.parseJSON(data);

        if(data.status==null){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Not Found');
        }
        
        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Masih Terdapat Setidaknya 1 Komponen dari MO yang HOLD/PAUSE, Lanjutkan Per SNFG Komponen')
        }
        else{
            $.get('index.php?r=scm-planner/get-last-mo',{ nomo : nomo },function(data){
                var data = $.parseJSON(data);
                if(data.posisi=="QC BULK" && data.state=="START"){
                    $('#start-button').hide();
                    $('#stop-button').show(); 

                       // Jenis Proses di Assign

                        $('#qcbulk-jenis_periksa_00').hide();
                        document.getElementById("qcbulk-jenis_periksa").disabled = true;
                        $('#qcbulk-jenis_periksa_0').show();
                        document.getElementById("qcbulk-jenis_periksa_1").disabled = false;
                        $('#qcbulk-jenis_periksa_1').attr('value',data.jenis_proses);


                        // Assign Lanjutan dan Lanjutan Istirahat


                        $('#stop-button').click(function(){ 

                            var jenis_periksa = $('#qcbulk-jenis_periksa_1').val();
                            var nomo = $('#qcbulk-nomo').val();                                
                            
                                $.get('index.php?r=qc-bulk/lanjutan-qc-bulkmo',{ nomo : nomo, jenis_periksa : jenis_periksa },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);

                                    document.getElementById("qcbulk-lanjutan-ist_istirahat_start").disabled = true;
                                    document.getElementById("qcbulk-lanjutan-ist_istirahat_stop").disabled = true;                                                                       
                                
                                });

                        });


                        // EO Lanjutan dan Lanjutan Istirahat 


                }else if(data.posisi=="QC BULK" && data.state=="STOP"){
                    $('#start-button').hide();
                    $('#stop-button').show(); 

                    $('#start-button').click(function(){ 

                        $('#qcbulk-jenis_periksa').change(function(){

                            var jenis_periksa = $('#qcbulk-jenis_periksa').val();
                            var nomo = $('#qcbulk-nomo').val();                                
                            
                                $.get('index.php?r=qc-bulk/lanjutan-qc-bulkmo',{ nomo : nomo, jenis_periksa : jenis_periksa },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#qcbulk-lanjutan').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);                                                                      
                                
                                });
                        });
                    });

                }else if(data.posisi!="QC BULK" && data!=null){
                    $('#start-button').hide();
                    $('#stop-button').show();
                    alert('Jadwal Baru');
                
                }else {
                    $('#start-button').hide();
                    $('#stop-button').hide(); 
                    alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                }
            });
        }
    });
});


$(function() {

    // Hide and Disable DynamicFormWidget for Kendala Model //

        // $('#kendala-form').hide();
        //     document.getElementById("kendala-0-keterangan").disabled = true;
        //     document.getElementById("kendala-0-start").disabled = true;
        //     document.getElementById("kendala-0-stop").disabled = true;    


    $('#qcbulk-jenis_periksa_0').hide(); //  Hide Jenis Proses Dropdown
    document.getElementById("qcbulk-jenis_periksa_1").disabled = true; // Disable Jenis Proses auto-assign
    $('#qcbulk-start').hide();
    $('#qcbulk-stop').hide(); 
    $('#qcbulk-istirahat-start').hide(); 
    $('#qcbulk-istirahat-stop').hide(); 
    $('#start-button').click(function(){
            var start = "START";
            $('#qcbulk-state').attr('value',start);
                    $('#qcbulk-start').show();
                    $('#qcbulk-stop').hide(); 
                    $('#qcbulk-istirahat-start').hide(); 
                    $('#qcbulk-istirahat-stop').hide();
            $('#qcbulk-jenis_periksa').change(function(){
                var jenis_periksa = $('#qcbulk-jenis_periksa').val();
                var snfg_komponen = $('#qcbulk-snfg_komponen').val();    
                var nomo = $('#qcbulk-nomo').val(); 
                var per_snfg_komponen = document.getElementById("qcbulk-snfg_komponen").disabled;
                            if(per_snfg_komponen){
                                $.get('index.php?r=qc-bulk/lanjutan-qc-bulkmo',{ nomo : nomo, jenis_periksa : jenis_periksa },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                }); 
                            }else{
                                $.get('index.php?r=qc-bulk/lanjutan-qc-bulk',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                }); 
                            } 
            });                     
        });

    $('#istirahat-start-button').click(function(){
            var ist_start = "ISTIRAHAT START";
            $('#qcbulk-state').attr('value',ist_start);
                    $('#qcbulk-istirahat-start').show(); 
                    $('#qcbulk-stop').hide(); 
                    $('#qcbulk-start').hide(); 
                    $('#qcbulk-istirahat-stop').hide(); 
            $('#qcbulk-jenis_periksa').change(function(){
                var jenis_periksa = $('#qcbulk-jenis_periksa').val();
                var snfg_komponen = $('#qcbulk-snfg_komponen').val();    
                var nomo = $('#qcbulk-nomo').val(); 
                var per_snfg_komponen = document.getElementById("qcbulk-snfg_komponen").disabled;
                            if(per_snfg_komponen){
                                $.get('index.php?r=qc-bulk/lanjutan-qc-bulkmo',{ nomo : nomo, jenis_periksa : jenis_periksa },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                    var lanjutan = $('#qcbulk-lanjutan_istirahat_start').val();
                                        $.get('index.php?r=qc-bulk/lanjutan-ist-qc-bulkmo',{ nomo : nomo, jenis_periksa : jenis_periksa, lanjutan : lanjutan },function(data){
                                                var data = $.parseJSON(data);
                                                //alert(data.lanjutan_ist);
                                                $('#qcbulk-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                $('#qcbulk-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                        }); 
                                }); 
                            }else{
                                $.get('index.php?r=qc-bulk/lanjutan-qc-bulk',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                    var lanjutan = $('#qcbulk-lanjutan_istirahat_start').val();
                                        $.get('index.php?r=qc-bulk/lanjutan-ist-qc-bulk',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa, lanjutan : lanjutan },function(data){
                                                var data = $.parseJSON(data);
                                                //alert(data.lanjutan_ist);
                                                $('#qcbulk-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                $('#qcbulk-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                        }); 
                                }); 
                            }
            });                     
        });

    $('#istirahat-stop-button').click(function(){
            var ist_stop = "ISTIRAHAT STOP";
            $('#qcbulk-state').attr('value',ist_stop);
                    $('#qcbulk-istirahat-stop').show();
                    $('#qcbulk-stop').hide(); 
                    $('#qcbulk-istirahat-start').hide(); 
                    $('#qcbulk-start').hide(); 
            $('#qcbulk-jenis_periksa').change(function(){
                var jenis_periksa = $('#qcbulk-jenis_periksa').val();
                var snfg_komponen = $('#qcbulk-snfg_komponen').val();    
                var nomo = $('#qcbulk-nomo').val(); 
                var per_snfg_komponen = document.getElementById("qcbulk-snfg_komponen").disabled;
                            if(per_snfg_komponen){
                                $.get('index.php?r=qc-bulk/lanjutan-qc-bulkmo',{ nomo : nomo, jenis_periksa : jenis_periksa },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                    var lanjutan = $('#qcbulk-lanjutan_istirahat_start').val();
                                        $.get('index.php?r=qc-bulk/lanjutan-ist-qc-bulkmo',{ nomo : nomo, jenis_periksa : jenis_periksa, lanjutan : lanjutan },function(data){
                                                var data = $.parseJSON(data);
                                                //alert(data.lanjutan_ist);
                                                $('#qcbulk-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                $('#qcbulk-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                        }); 
                                }); 
                            }else{
                                $.get('index.php?r=qc-bulk/lanjutan-qc-bulk',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                    var lanjutan = $('#qcbulk-lanjutan_istirahat_start').val();
                                        $.get('index.php?r=qc-bulk/lanjutan-ist-qc-bulk',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa, lanjutan : lanjutan },function(data){
                                                var data = $.parseJSON(data);
                                                //alert(data.lanjutan_ist);
                                                $('#qcbulk-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                $('#qcbulk-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                        }); 
                                }); 
                            }
            });                         
        });

    $('#stop-button').click(function()
    {
             var stop = "STOP";
             $('#qcbulk-state').attr('value',stop);
                    $('#qcbulk-stop').show(); 
                    $('#qcbulk-start').hide(); 
                    $('#qcbulk-istirahat-start').hide(); 
                    $('#qcbulk-istirahat-stop').hide();    
             $('#qcbulk-jenis_periksa').change(function(){
                var jenis_periksa = $('#qcbulk-jenis_periksa').val();
                var snfg_komponen = $('#qcbulk-snfg_komponen').val();    
                var nomo = $('#qcbulk-nomo').val(); 
                var per_snfg_komponen = document.getElementById("qcbulk-snfg_komponen").disabled;
                            if(per_snfg_komponen){
                                $.get('index.php?r=qc-bulk/lanjutan-qc-bulkmo',{ nomo : nomo, jenis_periksa : jenis_periksa },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                }); 
                            }else{
                                $.get('index.php?r=qc-bulk/lanjutan-qc-bulk',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                                    var data = $.parseJSON(data);
                                    //alert(data.sediaan);
                                    $('#qcbulk-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_stop').attr('value',data.lanjutan);
                                    $('#qcbulk-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                }); 
                            } 
                });
                $('#check_box_is_done').hide();
                $('#qcbulk-status').change(function(){
                    var status = $('#qcbulk-status').val();
                    if (status=='RELEASE' || status=='RELEASE_UNCOMFORMITY'){
                        $('#check_box_is_done').show();     
                    }else{
                        $('#check_box_is_done').hide();
                    }

                });
    });               

});

JS;
$this->registerJs($script);
?>