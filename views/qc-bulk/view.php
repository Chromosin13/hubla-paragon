<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\QcBulk */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Qc Bulks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qc-bulk-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'snfg',
            'ph',
            'viskositas',
            'berat_jenis',
            'kadar',
            'warna',
            'bau',
            'performance',
            'bentuk',
            'mikro',
            'kejernihan',
            'status',
            'jumlah_operator',
            'nama_qc',
            'waktu',
            'state',
        ],
    ]) ?>

</div>
