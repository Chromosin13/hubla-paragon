<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InkjetNonInlineSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inkjet-non-inline-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'inkjet_sticker_bottom_jumlah_plan') ?>

    <?= $form->field($model, 'inkjet_sticker_bottom_jumlah_realisasi') ?>

    <?= $form->field($model, 'inkjet_sticker_bottom_nama_line') ?>

    <?php  echo $form->field($model, 'inkjet_sticker_bottom_jumlah_operator') ?>

    <?php  echo $form->field($model, 'inkjet_sticker_bottom_operator_1') ?>

    <?php  echo $form->field($model, 'inkjet_sticker_bottom_operator_2') ?>

    <?php  echo $form->field($model, 'inkjet_sticker_bottom_operator_3') ?>

    <?php  echo $form->field($model, 'inkjet_sticker_bottom_waktu') ?>

    <?php  echo $form->field($model, 'inkjet_sticker_bottom_state') ?>

    <?php  echo $form->field($model, 'inkjet_dus_satuan_nama_line') ?>

    <?php  echo $form->field($model, 'inkjet_dus_satuan_jumlah_operator') ?>

    <?php  echo $form->field($model, 'inkjet_dus_satuan_operator_1') ?>

    <?php  echo $form->field($model, 'inkjet_dus_satuan_operator_2') ?>

    <?php  echo $form->field($model, 'inkjet_dus_satuan_operator_3') ?>

    <?php  echo $form->field($model, 'inkjet_dus_satuan_waktu') ?>

    <?php  echo $form->field($model, 'inkjet_dus_satuan_state') ?>

    <?php  echo $form->field($model, 'inkjet_dus_12_jumlah_plan') ?>

    <?php  echo $form->field($model, 'inkjet_dus_12_jumlah_realisasi') ?>

    <?php  echo $form->field($model, 'inkjet_dus_12_nama_line') ?>

    <?php  echo $form->field($model, 'inkjet_dus_12_jumlah_operator') ?>

    <?php  echo $form->field($model, 'inkjet_dus_12_operator_1') ?>

    <?php  echo $form->field($model, 'inkjet_dus_12_operator_2') ?>

    <?php  echo $form->field($model, 'inkjet_dus_12_operator_3') ?>

    <?php  echo $form->field($model, 'inkjet_dus_12_waktu') ?>

    <?php  echo $form->field($model, 'inkjet_dus_12_state') ?>

    <?php  echo $form->field($model, 'inkjet_packaging_primer_jumlah_plan') ?>

    <?php  echo $form->field($model, 'inkjet_packaging_primer_jumlah_realisasi') ?>

    <?php  echo $form->field($model, 'inkjet_packaging_primer_nama_line') ?>

    <?php  echo $form->field($model, 'inkjet_packaging_primer_jumlah_operator') ?>

    <?php  echo $form->field($model, 'inkjet_packaging_primer_operator_1') ?>

    <?php  echo $form->field($model, 'inkjet_packaging_primer_operator_2') ?>

    <?php  echo $form->field($model, 'inkjet_packaging_primer_operator_3') ?>

    <?php  echo $form->field($model, 'inkjet_packaging_primer_waktu') ?>

    <?php  echo $form->field($model, 'inkjet_packaging_primer_state') ?>

    <?php  echo $form->field($model, 'inkjet_dus_satuan_jumlah_plan') ?>

    <?php  echo $form->field($model, 'inkjet_dus_satuan_jumlah_realisasi') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
