<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InkjetNonInline */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inkjet-non-inline-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Input dan Informasi SNFG dari Planner</h2>
                <?= $form->field($model, 'snfg')->textInput() ?>
                <b>Streamline</b>
                <input type="text" class="form-control" id="streamline" disabled>
                <b>Start</b>
                <input type="text" class="form-control" id="start" placeholder="" disabled>
                <b>Due</b>
                <input type="text" class="form-control" id="due" placeholder="" disabled>
                <b>Nama Bulk</b>
                <input type="text" class="form-control" id="nama-bulk" placeholder="" disabled>
                <b>Nama FG</b>
                <input type="text" class="form-control" id="nama-fg" placeholder="" disabled>
                <b>Besar Batch</b>
                <input type="text" class="form-control" id="besar-batch" placeholder="" disabled>
                <b>Besar per lot (Kg)</b>
                <input type="text" class="form-control" id="besar-lot" placeholder="" disabled>
                <b>Lot ke-</b>
                <input type="text" class="form-control" id="lot" placeholder="" disabled>
                <b>Kode Jadwal</b>
                <input type="text" class="form-control" id="kode-jadwal" placeholder="" disabled>
            </div>
    </div>

    <?= $form->field($model, 'inkjet_sticker_bottom_jumlah_plan')->textInput() ?>

    <?= $form->field($model, 'inkjet_sticker_bottom_jumlah_realisasi')->textInput() ?>

    <?= $form->field($model, 'inkjet_sticker_bottom_nama_line')->textInput() ?>

    <?= $form->field($model, 'inkjet_sticker_bottom_jumlah_operator')->textInput() ?>

    <?= $form->field($model, 'inkjet_sticker_bottom_operator_1')->textInput() ?>

    <?= $form->field($model, 'inkjet_sticker_bottom_operator_2')->textInput() ?>

    <?= $form->field($model, 'inkjet_sticker_bottom_operator_3')->textInput() ?>

    <?= $form->field($model, 'inkjet_sticker_bottom_state')->textInput() ?>

    <?= $form->field($model, 'inkjet_dus_satuan_nama_line')->textInput() ?>

    <?= $form->field($model, 'inkjet_dus_satuan_jumlah_operator')->textInput() ?>

    <?= $form->field($model, 'inkjet_dus_satuan_operator_1')->textInput() ?>

    <?= $form->field($model, 'inkjet_dus_satuan_operator_2')->textInput() ?>

    <?= $form->field($model, 'inkjet_dus_satuan_operator_3')->textInput() ?>

    <?= $form->field($model, 'inkjet_dus_satuan_state')->textInput() ?>

    <?= $form->field($model, 'inkjet_dus_12_jumlah_plan')->textInput() ?>

    <?= $form->field($model, 'inkjet_dus_12_jumlah_realisasi')->textInput() ?>

    <?= $form->field($model, 'inkjet_dus_12_nama_line')->textInput() ?>

    <?= $form->field($model, 'inkjet_dus_12_jumlah_operator')->textInput() ?>

    <?= $form->field($model, 'inkjet_dus_12_operator_1')->textInput() ?>

    <?= $form->field($model, 'inkjet_dus_12_operator_2')->textInput() ?>

    <?= $form->field($model, 'inkjet_dus_12_operator_3')->textInput() ?>

    <?= $form->field($model, 'inkjet_dus_12_state')->textInput() ?>

    <?= $form->field($model, 'inkjet_packaging_primer_jumlah_plan')->textInput() ?>

    <?= $form->field($model, 'inkjet_packaging_primer_jumlah_realisasi')->textInput() ?>

    <?= $form->field($model, 'inkjet_packaging_primer_nama_line')->textInput() ?>

    <?= $form->field($model, 'inkjet_packaging_primer_jumlah_operator')->textInput() ?>

    <?= $form->field($model, 'inkjet_packaging_primer_operator_1')->textInput() ?>

    <?= $form->field($model, 'inkjet_packaging_primer_operator_2')->textInput() ?>

    <?= $form->field($model, 'inkjet_packaging_primer_operator_3')->textInput() ?>

    <?= $form->field($model, 'inkjet_packaging_primer_state')->textInput() ?>

    <?= $form->field($model, 'inkjet_dus_satuan_jumlah_plan')->textInput() ?>

    <?= $form->field($model, 'inkjet_dus_satuan_jumlah_realisasi')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', 
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$script = <<< JS
$('#inkjetnoninline-snfg').change(function(){
    var snfg = $(this).val();
    $.get('index.php?r=scm-planner/get-planner',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
        $('#besar-batch').attr('value',data.besar_batch);
        $('#besar-lot').attr('value',data.besar_lot);
        $('#lot').attr('value',data.lot);
        $('#kode-jadwal').attr('value',data.kode_jadwal);
    });
});

JS;
$this->registerJs($script);
?>