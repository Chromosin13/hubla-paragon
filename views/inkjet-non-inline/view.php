<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\InkjetNonInline */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inkjet Non Inlines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inkjet-non-inline-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'snfg',
            'inkjet_sticker_bottom_jumlah_plan',
            'inkjet_sticker_bottom_jumlah_realisasi',
            'inkjet_sticker_bottom_nama_line',
            'inkjet_sticker_bottom_jumlah_operator',
            'inkjet_sticker_bottom_operator_1',
            'inkjet_sticker_bottom_operator_2',
            'inkjet_sticker_bottom_operator_3',
            'inkjet_sticker_bottom_waktu',
            'inkjet_sticker_bottom_state',
            'inkjet_dus_satuan_nama_line',
            'inkjet_dus_satuan_jumlah_operator',
            'inkjet_dus_satuan_operator_1',
            'inkjet_dus_satuan_operator_2',
            'inkjet_dus_satuan_operator_3',
            'inkjet_dus_satuan_waktu',
            'inkjet_dus_satuan_state',
            'inkjet_dus_12_jumlah_plan',
            'inkjet_dus_12_jumlah_realisasi',
            'inkjet_dus_12_nama_line',
            'inkjet_dus_12_jumlah_operator',
            'inkjet_dus_12_operator_1',
            'inkjet_dus_12_operator_2',
            'inkjet_dus_12_operator_3',
            'inkjet_dus_12_waktu',
            'inkjet_dus_12_state',
            'inkjet_packaging_primer_jumlah_plan',
            'inkjet_packaging_primer_jumlah_realisasi',
            'inkjet_packaging_primer_nama_line',
            'inkjet_packaging_primer_jumlah_operator',
            'inkjet_packaging_primer_operator_1',
            'inkjet_packaging_primer_operator_2',
            'inkjet_packaging_primer_operator_3',
            'inkjet_packaging_primer_waktu',
            'inkjet_packaging_primer_state',
            'inkjet_dus_satuan_jumlah_plan',
            'inkjet_dus_satuan_jumlah_realisasi',
        ],
    ]) ?>

</div>
