<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLinePenimbangan1 */

$this->title = $model->row_number;
$this->params['breadcrumbs'][] = ['label' => 'Achievement Line Penimbangan1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-line-penimbangan1-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->row_number], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->row_number], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'row_number',
            'snfg_komponen',
            'snfg',
            'nama_fg',
            'jenis_penimbangan',
            'nama_line',
            'tanggal_start',
            'tanggal_stop',
            'is_done',
            'leadtime',
            'std_leadtime',
            'status_leadtime:ntext',
            'plan_start_timbang',
            'status_ontime:ntext',
        ],
    ]) ?>

</div>
