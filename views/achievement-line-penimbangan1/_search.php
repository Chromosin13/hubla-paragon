<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLinePenimbangan1Search */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="achievement-line-penimbangan1-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'row_number') ?>

    <?= $form->field($model, 'snfg_komponen') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'nama_fg') ?>

    <?= $form->field($model, 'jenis_penimbangan') ?>

    <?php // echo $form->field($model, 'nama_line') ?>

    <?php // echo $form->field($model, 'tanggal_start') ?>

    <?php // echo $form->field($model, 'tanggal_stop') ?>

    <?php // echo $form->field($model, 'is_done') ?>

    <?php // echo $form->field($model, 'leadtime') ?>

    <?php // echo $form->field($model, 'std_leadtime') ?>

    <?php // echo $form->field($model, 'status_leadtime') ?>

    <?php // echo $form->field($model, 'plan_start_timbang') ?>

    <?php // echo $form->field($model, 'status_ontime') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
