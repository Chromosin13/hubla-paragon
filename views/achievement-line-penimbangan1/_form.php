<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AchievementLinePenimbangan1 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="achievement-line-penimbangan1-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'row_number')->textInput() ?>

    <?= $form->field($model, 'snfg_komponen')->textInput() ?>

    <?= $form->field($model, 'snfg')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'jenis_penimbangan')->textInput() ?>

    <?= $form->field($model, 'nama_line')->textInput() ?>

    <?= $form->field($model, 'tanggal_start')->textInput() ?>

    <?= $form->field($model, 'tanggal_stop')->textInput() ?>

    <?= $form->field($model, 'is_done')->textInput() ?>

    <?= $form->field($model, 'leadtime')->textInput() ?>

    <?= $form->field($model, 'std_leadtime')->textInput() ?>

    <?= $form->field($model, 'status_leadtime')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'plan_start_timbang')->textInput() ?>

    <?= $form->field($model, 'status_ontime')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
