<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AchievementLinePenimbangan1 */

$this->title = 'Create Achievement Line Penimbangan1';
$this->params['breadcrumbs'][] = ['label' => 'Achievement Line Penimbangan1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-line-penimbangan1-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
