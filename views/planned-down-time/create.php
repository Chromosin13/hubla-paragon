<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PlannedDownTime */

$this->title = 'Create Planned Down Time';
$this->params['breadcrumbs'][] = ['label' => 'Planned Down Times', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="planned-down-time-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
