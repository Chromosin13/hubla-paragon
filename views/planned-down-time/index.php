<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax; 
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PlannedDownTimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Planned Down Time';
$this->params['breadcrumbs'][] = $this->title;
?>

<input type="hidden" class="form-control" id=kemas_id value="<?=$i?>">
<input type="hidden" class="form-control" id=type_id value="<?=$j?>">

    <p>
        <?= Html::button('Tambah Opsi', ['value'=>Url::to('index.php?r=planned-down-time/create'),'class' => 'btn btn-success','id'=>'modalButton','data-id'=>$i]) ?>
    </p>

    <?php

        Modal::begin([
                'header'=>'<h4>Planned Down Time</h4>',
                'id' => 'modal',
                'size' => 'modal-lg',
            ]);

        echo "<div id='modalContent'></div>";

        Modal::end();

    ?>


<div class="planned-down-time-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?php Pjax::begin(['id'=>'planneddowntimeGrid']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'keterangan',
            'durasi',
            // 'kemas_id',
            // 'jenis',

            ['class' => 'yii\grid\ActionColumn',
             'template' => '{delete}',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>


 <br \>
        <?php

            // if($j==1){ 

            //     echo Html::a('Next : Achievement K1', ['achievement-leadtime-kemas1/view', 'id' => $i], ['class' => 'btn btn-info']); 
            // }else{

            //     echo Html::a('Next : Achievement K2', ['achievement-leadtime-kemas2-direct/view', 'id' => $i], ['class' => 'btn btn-info']);
            // }

            if($j==1){ 

                echo Html::a('Next : Input Kemas 1', ['kemas1/create'], ['class' => 'btn btn-info']); 
            }else{

                echo Html::a('Next : Input Kemas 2', ['kemas2/create'], ['class' => 'btn btn-info']);
            }

        ?>

