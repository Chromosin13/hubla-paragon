<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\DimPdt;
use kartik\widgets\Select2;
use kartik\widgets\TimePicker;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\PlannedDownTime */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="planned-down-time-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>

    <?= $form->field($model, 'keterangan')->widget(Select2::classname(),[
    		 'data'=>ArrayHelper::map(DimPdt::find()->all(),'keterangan','keterangan'),
    		 'options'=>[
    		 	'placeholder'=>'Pilih Keterangan'
    		 	],
    		 'pluginOptions'=>[
    		 	'allowClear'=>true
    		 ],

    ]); ?>

    <?= $form->field($model, "durasi")->widget(TimePicker::classname(), [
            'options' => ['placeholder' => 'Durasi'],
            'pluginOptions' => [
                    'minuteStep' => 5,
                    'autoclose'=>true,
                    'showMeridian' => false,
                    'defaultTime' => '00:00'
            ]
    ]); ?>

	<div id=debug-field>
	    <?= $form->field($model, 'kemas_id')->hiddenInput(['readonly'=>true]) ?>

	    <?= $form->field($model, 'jenis')->hiddenInput(['readonly'=>true]) ?>

    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

// GET VALUE FROM HIDDEN FORM OF i, and j from Index Page 

var kemas_id = $('#kemas_id').val();
var type_id = $('#type_id').val();

// Kemas Id and Type Id

$('#planneddowntime-kemas_id').attr('value',kemas_id);
$('#planneddowntime-jenis').attr('value',type_id);

// Hide Kemas Id and Jenis Id

$('#debug-field').hide();


$('form#{$model->formName()}').on('beforeSubmit', function(e)
{
	var \$form = $(this);
	 $.post(
	 	\$form.attr("action"), // serialize Yii2 form
	 	\$form.serialize()
	 )

	 	.done(function(result){
	 	 // console.log(result);
	 	 if(result == 1)
		 	{	
		 		$(document).find('#modal').modal('hide');
		 		// $(\$form).trigger("reset");

		 		$.pjax.reload({container:'#planneddowntimeGrid'});
		 	}else{
		 		
		 		$("#message").html(result);
		 		alert('Kemas Id not found, Please refresh');
		 	}
		})
	 	.fail(function()
	 	{
	 		console.log("server error");
	 	});
	return false;

});




JS;
$this->registerJs($script);
?>