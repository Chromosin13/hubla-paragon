
<?php

// use yii\helpers\Html;
// use yii\widgets\ActiveForm;


use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
//use app\models\qcbulk;
use faryshta\widgets\JqueryTagsInput;
use kartik\checkbox\CheckboxX;
/* @var $this yii\web\View */
/* @var $model app\models\QcBulk */
/* @var $form yii\widgets\ActiveForm */
use app\models\QcBulk;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\dialog\Dialog;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use app\models\ScmPlanner;
use app\models\PosisiProsesRaw;
use yii\helpers\ArrayHelper;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\PusatResolusi */
/* @var $form yii\widgets\ActiveForm */
?>


<?php
$sc = <<< JS
function printVal(value) {
  $('#pusatresolusi-s_id').attr('value',value);
}

JS;
$this->registerJs(
    $sc,
    yii\web\View::POS_HEAD
);
?>

<div class="pusat-resolusi-form">

    <div class="box box-solid">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="box-group" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <!-- <div class="panel box box-primary">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed">
                        Lupa Menambahkan Operator
                        , Salah Isi Nama Line / Quantity / Status 
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="box-body">
                        <?= Html::a('<i class="fa fa-balance-scale"></i>Timbang', ['/penimbangan-edit-jadwal/index'], ['class'=>'btn btn-success', 'style' => 'width: 150px; border-radius: 5px;']) ?>

                        <?= Html::a('<i class="fa fa-hourglass-half"></i>Olah', ['/pengolahan-edit-jadwal/index'], ['class'=>'btn btn-success', 'style' => 'width: 150px; border-radius: 5px;']) ?>

                        <?= Html::a('<i class="fa fa-flask"></i>QC Bulk', ['/qcbulk-editable/index'], ['class'=>'btn btn-success', 'style' => 'width: 150px; border-radius: 5px;']) ?>

                        <?= Html::a('<i class="fa fa-chevron-circle-right"></i>Kemas 1', ['/kemas1-edit-jadwal/'], ['class'=>'btn btn-success', 'style' => 'width: 150px; border-radius: 5px;']) ?>

                        <?= Html::a('<i class="fa fa-dropbox"></i>Kemas 2', ['/kemas2-edit-jadwal/index'], ['class'=>'btn btn-success', 'style' => 'width: 150px; border-radius: 5px;']) ?>

                        <?= Html::a('<i class="fa fa-search"></i>QC FG', ['/qcfg-editable/index'], ['class'=>'btn btn-success', 'style' => 'width: 150px; border-radius: 5px;']) ?>
                    </div>
                  </div>
                </div>
                <div class="panel box box-danger">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false">
                        Lupa Input Jadwal (START/STOP)
                      </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false">
                    <div class="box-body">
                        <?= Html::a('<i class="fa fa-balance-scale"></i>Timbang', ['/penimbangan/create'], ['class'=>'btn btn-primary', 'style' => 'width: 150px; border-radius: 5px;']) ?>

                        <?= Html::a('<i class="fa fa-hourglass-half"></i>Olah', ['/pengolahan/create'], ['class'=>'btn btn-primary', 'style' => 'width: 150px; border-radius: 5px;']) ?>

                        <?= Html::a('<i class="fa fa-flask"></i>QC Bulk', ['/qc-bulk/create'], ['class'=>'btn btn-primary', 'style' => 'width: 150px; border-radius: 5px;']) ?>

                        <?= Html::a('<i class="fa fa-chevron-circle-right"></i>Kemas 1', ['/kemas1/create'], ['class'=>'btn btn-primary', 'style' => 'width: 150px; border-radius: 5px;']) ?>

                        <?= Html::a('<i class="fa fa-dropbox"></i>Kemas 2', ['/kemas2/create'], ['class'=>'btn btn-primary', 'style' => 'width: 150px; border-radius: 5px;']) ?>

                        <?= Html::a('<i class="fa fa-search"></i>QC FG', ['/qc-fg/create'], ['class'=>'btn btn-primary', 'style' => 'width: 150px; border-radius: 5px;']) ?>
                    </div>
                  </div>
                </div> -->

                <div class="box box-widget widget-user">
                  <!-- Add the bg color to the header using any of the bg-* classes -->

                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
                      <h3 class="widget-user-username"><b>Pusat Resolusi</b></h3>
                      <h5 class="widget-user-desc">Self Service</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/business-partnership.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                                <?php $form = ActiveForm::begin(); ?>


                                    <?= $form->field($model, 'nama_operator')->textInput() ?>

                                    <div id="leader">
                                      <?= $form->field($model, 'nama_leader')->textInput(['readOnly'=>true,'value'=>Yii::$app->user->identity->username]) ?>
                                    </div>

                                    <?php

                                      // echo $form->field($model, 'nama_operator')->widget(TagsinputWidget::classname(), [
                                      //   'clientOptions' => [
                                      //       'trimValue' => true,
                                      //       'allowDuplicates' => false,
                                      //       'maxChars'=> 4,
                                      //       'minChars'=> 4,
                                      //   ]
                                      // ]) 
                                    ?>

                                    <?= $form->field($model, 'proses')->dropDownList(['penimbangan' => 'PENIMBANGAN','pengolahan' => 'PENGOLAHAN','kemas1' => 'KEMAS 1','kemas2' => 'KEMAS 2'],['prompt'=>'Pilih Proses']); ?>

                                    <?= $form->field($model, 'kategori')->dropDownList(['SALAH_INPUT' => 'SALAH INPUT / EDIT JADWAL','LUPA_INPUT' => 'LUPA INPUT / BELUM INPUT','OTHERS' => 'OTHERS'],['prompt'=>'Pilih Kategori']); ?>

                                    <div id="nojadwal-form">
                                      <span class="badge bg-blue" id="nomo-badge"></span>
                                      <span class="badge bg-yellow" id="snfg-badge"></span>
                                      <span class="badge bg-green" id="snfgkomponen-badge"></span>
                                      <p></p>
                                      <?= $form->field($model, 'nojadwal')->textInput() ?>  
                                      <span class="badge bg-red" id="alert-badge"></span>
                                      <span class="badge bg-green" id="valid-badge"></span>
                                      <p></p>                           
                                        
                                        <div id="log-input">

                                            <div class="box-header with-border">
                                              <h2 class="box-title"> <b>Log Input</b></h2>
                                              <br \>
                                            </div>
                                            <div class="box-header with-border">
                                                <table id="table-loginput" class="table table-bordered">
                                                    <tbody><tr>
                                                
                                                      <th style="width: 100px">Waktu</th>
                                                      <th style="width: 120px">Nomor Jadwal</th>
                                                      <th style="width: 50px">State</th>
                                                      <th>Jenis Proses</th>
                                                      <th>Lanjutan</th>
                                                      <th>Is Done</th>
                                                      <th style="width: 50px">UID</th>
                                                      <th style="width: 50px">Check</th>

                                                    </tr>
                                                  </tbody>
                                                </table>
                                            </div>

                                        </div>

                                    </div>

                                    <?= $form->field($model, 's_id')->textInput(['readOnly'=>true]) ?>

                                    <?= $form->field($model, 'permasalahan')->textArea() ?>





                                    <div id="form-id" class="form-group">
                                        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                    </div>


                                <!--     <?= Html::a('<i class="fa fa-chevron-circle-right"></i>Cari', ['pusat-resolusi/direct'], ['class'=>'btn btn-primary', 'style' => 'width: 150px; border-radius: 5px;']) ?>
                                 -->
                                <!--     <button id="submitButton" type="submit">Creates</button> -->


                                    <?php ActiveForm::end(); ?>
                            </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>


                </div>
            </div>
            <!-- /.box-body -->
          </div>

    

</div>


<?php
$script = <<< JS


// Is In Array

function isInArray(value, array) {
  return array.indexOf(value) > -1;
}




// Hide Nomor Jadwal Form

$('#leader').hide();
$('#nojadwal-form').hide();
$('#form-id').hide();
$('#log-input').hide();

// Get Kategori Field Value

$('#pusatresolusi-kategori').change(function(){  

    var kat = $('#pusatresolusi-kategori').val();
      if(kat=='SALAH_INPUT'){
        $('#pusatresolusi-nojadwal').attr('value','');
        $('#nojadwal-form').fadeIn("slow");
        $('#form-id').hide();
      }else{
        $('#nojadwal-form').hide();
        $('#pusatresolusi-nojadwal').attr('value','-');
        $('#form-id').fadeIn("slow");
      }
});


$('#pusatresolusi-proses').change(function(){  
    var proses = $('#pusatresolusi-proses').val();
     if(isInArray(proses, ['penimbangan','pengolahan','qc-bulk-entry'])){
        $('#nomo-badge').fadeIn("slow");
        $('#nomo-badge').html('Input/Scan Nomor MO' );
        $('#snfg-badge').hide();
        $('#snfgkomponen-badge').hide();
        document.getElementById('pusatresolusi-nojadwal').value = '';
        $('#form-id').hide();
     }else if(isInArray(proses, ['kemas2','qc-fg-v2'])){
        $('#snfg-badge').fadeIn("slow");
        $('#snfg-badge').html('Input/Scan SNFG' );
        $('#nomo-badge').hide();
        $('#snfgkomponen-badge').hide();
        document.getElementById('pusatresolusi-nojadwal').value = '';
        $('#form-id').hide();
     }else{
        $('#snfgkomponen-badge').fadeIn("slow");
        $('#snfgkomponen-badge').html('Input/Scan Nomor SNFG Komponen' );
        $('#nomo-badge').hide();
        $('#snfg-badge').hide();
        document.getElementById('pusatresolusi-nojadwal').value = '';
        $('#form-id').hide();  
     }
});


$('#pusatresolusi-nojadwal').change(function(){  
    var nojadwal = $('#pusatresolusi-nojadwal').val();
    var proses = $('#pusatresolusi-proses').val();

    if(nojadwal=='-'){
      alert('flag1');
      // do nothing
    }else if(isInArray(proses, ['penimbangan','pengolahan','qc-bulk-entry'])&&(nojadwal!='-')){

      // Check Validity
      $.get('index.php?r=pusat-resolusi/check-nomo',{ nojadwal : nojadwal },function(data){
           var data = $.parseJSON(data);
           console.log(data.id);
           if(data.id==0){
              $('#alert-badge').html('Nomor MO Tidak Ditemukan, Pastikan Nomor MO benar' );
              $('#form-id').hide();
              $('#valid-badge').hide();
              $('#alert-badge').fadeIn("slow");
           }else if(data.id>=1){
              $('#valid-badge').html('Valid');
              $('#form-id').fadeIn("slow");
              $('#valid-badge').fadeIn("slow");
              $('#alert-badge').hide();

              // Show Input Log

              $.get('index.php?r='+proses+'/get-log-input',{ nojadwal : nojadwal },function(data){
                var json = $.parseJSON(data);
                console.log(data);
                var tr;
                for (var i = 0; i < json.length; i++) {
                    tr = $('<tr/>');
                    tr.append("<td><span class='badge bg-grey'>" + json[i].waktu + "</span></td>");
                    tr.append("<td><span class='badge bg-grey'>" + json[i].nojadwal + "</span></td>");
                    if(json[i].state=="START"){
                        tr.append("<td><span class='badge bg-green'>"+ json[i].state +" </span></td>");
                    }
                    else if((json[i].state=="STOP")&&(json[i].state=="STOP")){
                        tr.append("<td><span class='badge bg-red'>" + json[i].state + "</span></td>");
                    }
                    tr.append("<td><span class='badge bg-grey'>" + json[i].jenis_proses + "</span></td>");
                    tr.append("<td><span class='badge bg-grey'>" + json[i].lanjutan + "</span></td>");
                    if(json[i].state=="START"){
                        tr.append("<td><span class='badge bg-grey'>-</span></td>");
                    }
                    else if((json[i].is_done==1)&&(json[i].state=="STOP")){
                        tr.append("<td><span class='badge bg-green'>Done</span></td>");
                    }else {
                        tr.append("<td><span class='badge bg-red'>Not Done</span></td>");
                    }
                    tr.append("<td><span class='badge bg-grey'>" + json[i].id + "</span></td>");
                    tr.append('<td><label><input type="radio" name="radioLogInput" onclick="printVal('+json[i].id+');" value='+json[i].id+'></label></td>');
            
                    
                    $('#table-loginput').append(tr);
                }
                $('#log-input').fadeIn("slow");


              });

              


           }
      });

      

    }else if(isInArray(proses, ['kemas2','qc-fg-v2'])&&(nojadwal!='-')){
      
      $.get('index.php?r=pusat-resolusi/check-snfg',{ nojadwal : nojadwal },function(data){
           var data = $.parseJSON(data);
           console.log(data.id);
           if(data.id==0){
              $('#alert-badge').html('Nomor SNFG Tidak Ditemukan, Pastikan Nomor SNFG benar' );
              $('#form-id').hide();
              $('#valid-badge').hide();
              $('#alert-badge').fadeIn("slow");
           }else{

              $('#valid-badge').html('Valid');
              $('#form-id').fadeIn("slow");
              $('#valid-badge').fadeIn("slow");
              $('#alert-badge').hide();


              // Show Input Log

              $.get('index.php?r='+proses+'/get-log-input',{ nojadwal : nojadwal },function(data){
                var json = $.parseJSON(data);
                console.log(data);
                var tr;
                for (var i = 0; i < json.length; i++) {
                    tr = $('<tr/>');
                    tr.append("<td><span class='badge bg-grey'>" + json[i].waktu + "</span></td>");
                    tr.append("<td><span class='badge bg-grey'>" + json[i].nojadwal + "</span></td>");
                    if(json[i].state=="START"){
                        tr.append("<td><span class='badge bg-green'>"+ json[i].state +" </span></td>");
                    }
                    else if((json[i].state=="STOP")&&(json[i].state=="STOP")){
                        tr.append("<td><span class='badge bg-red'>" + json[i].state + "</span></td>");
                    }
                    tr.append("<td><span class='badge bg-grey'>" + json[i].jenis_proses + "</span></td>");
                    tr.append("<td><span class='badge bg-grey'>" + json[i].lanjutan + "</span></td>");
                    if(json[i].state=="START"){
                        tr.append("<td><span class='badge bg-grey'>-</span></td>");
                    }
                    else if((json[i].is_done==1)&&(json[i].state=="STOP")){
                        tr.append("<td><span class='badge bg-green'>Done</span></td>");
                    }else {
                        tr.append("<td><span class='badge bg-red'>Not Done</span></td>");
                    }
                    tr.append("<td><span class='badge bg-grey'>" + json[i].id + "</span></td>");
                    tr.append('<td><label><input type="radio" name="radioLogInput" onclick="printVal('+json[i].id+');" value='+json[i].id+'></label></td>');
            
                    
                    $('#table-loginput').append(tr);
                }
                $('#log-input').fadeIn("slow");


              });
           }
      });
    }else{
      
      $.get('index.php?r=pusat-resolusi/check-snfgkomponen',{ nojadwal : nojadwal },function(data){
           var data = $.parseJSON(data);
           console.log(data.id);
           if(data.id==0){
              $('#alert-badge').html('Nomor SNFG Komponen Tidak Ditemukan, Pastikan Nomor SNFG Komponen benar' );
              $('#form-id').hide();
              $('#valid-badge').hide();
              $('#alert-badge').fadeIn("slow");
           }else{
              $('#valid-badge').html('Valid');
              $('#form-id').fadeIn("slow");
              $('#valid-badge').fadeIn("slow");
              $('#alert-badge').hide();

              // Show Input Log

              $.get('index.php?r='+proses+'/get-log-input',{ nojadwal : nojadwal },function(data){
                var json = $.parseJSON(data);
                console.log(data);
                var tr;
                for (var i = 0; i < json.length; i++) {
                    tr = $('<tr/>');
                    tr.append("<td><span class='badge bg-grey'>" + json[i].waktu + "</span></td>");
                    tr.append("<td><span class='badge bg-grey'>" + json[i].nojadwal + "</span></td>");
                    if(json[i].state=="START"){
                        tr.append("<td><span class='badge bg-green'>"+ json[i].state +" </span></td>");
                    }
                    else if((json[i].state=="STOP")&&(json[i].state=="STOP")){
                        tr.append("<td><span class='badge bg-red'>" + json[i].state + "</span></td>");
                    }
                    tr.append("<td><span class='badge bg-grey'>" + json[i].jenis_proses + "</span></td>");
                    tr.append("<td><span class='badge bg-grey'>" + json[i].lanjutan + "</span></td>");
                    if(json[i].state=="START"){
                        tr.append("<td><span class='badge bg-grey'>-</span></td>");
                    }
                    else if((json[i].is_done==1)&&(json[i].state=="STOP")){
                        tr.append("<td><span class='badge bg-green'>Done</span></td>");
                    }else {
                        tr.append("<td><span class='badge bg-red'>Not Done</span></td>");
                    }
                    tr.append("<td><span class='badge bg-grey'>" + json[i].id + "</span></td>");
                    tr.append('<td><label><input type="radio" name="radioLogInput" onclick="printVal('+json[i].id+');" value='+json[i].id+'></label></td>');
            
                    
                    $('#table-loginput').append(tr);
                }
                $('#log-input').fadeIn("slow");


              });
           }
      });
    }
});


// $('#durasi-ember').html('Durasi Waktu Pengemberan : '+(parseFloat(stop_js-start_js)/parseFloat(60000))+' Menit atau '+(parseFloat(stop_js-start_js)/parseFloat(3600000)).toFixed(2)+' Jam' );


//     $('#create-form').hide();
// // Get Base Url
// var baseurl = $('#baseurl').val();

// $('#qcbulkentry-nomo').change(function(){  

//     var nomo = $('#qcbulkentry-nomo').val();


//     $.get('index.php?r=qc-bulk-entry/check-nomo',{ nomo : nomo },function(data){
//         var data = $.parseJSON(data);
//         // alert(data);
//         // if(data==="null"){
//         //   $('#create-form').hide();
//         //     alert("TIDAK ADA JADWAL DENGAN NOMOR MO "+data.nomo+" PADA DATABASE PLANNER");
//         // }
//         if(data.id==1){

//           alert('Data Valid, belum dilakukan QC, silahkan tekan tombol CREATE untuk inisiasi QC');
          
//           $('#create-form').show();
           
//         }

//         else if(data.id==2){

//           alert('Data Valid, Proses QC sudah berjalan, halaman akan diteruskan');
//           var nomo = $('#qcbulkentry-nomo').val();
//           // window.location = baseurl+"?r=qc-bulk-entry/create-rincian&nomo="+nomo;
//           window.location = "index.php?r=qc-bulk-entry/create-rincian&nomo="+nomo;
           
//         }
    
//     });
        
// });


JS;
$this->registerJs($script);
?>
