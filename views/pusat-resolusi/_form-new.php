
<?php

// use yii\helpers\Html;
// use yii\widgets\ActiveForm;


use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\widgets\ActiveField;
//use app\models\qcbulk;
use faryshta\widgets\JqueryTagsInput;
use kartik\checkbox\CheckboxX;
/* @var $this yii\web\View */
/* @var $model app\models\QcBulk */
/* @var $form yii\widgets\ActiveForm */
use app\models\QcBulk;
use kartik\widgets\TimePicker;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\dialog\Dialog;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use app\models\ScmPlanner;
use app\models\PosisiProsesRaw;
use yii\helpers\ArrayHelper;
use wbraganca\tagsinput\TagsinputWidget;


/* @var $this yii\web\View */
/* @var $model app\models\PusatResolusi */
/* @var $form yii\widgets\ActiveForm */
?>


<?php
$sc = <<< JS
function printVal(value) {
  $('#pusatresolusi-s_id').attr('value',value);
}

JS;
$this->registerJs(
    $sc,
    yii\web\View::POS_HEAD
);
?>

<div class="pusat-resolusi-form">

    <div class="box box-solid">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="box-group" id="accordion">
                <div class="box box-widget widget-user">
                  <!-- Add the bg color to the header using any of the bg-* classes -->

                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../web/images/photo1.png') center center;">
                      <h3 class="widget-user-username"><b>Pusat Resolusi</b></h3>
                      <h5 class="widget-user-desc">Self Service</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="../web/images/business-partnership.png" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <div class="box-body">
                                <?php $form = ActiveForm::begin(); ?>


                                    <?= $form->field($model, 'nama_operator')->textInput() ?>

                                    <div id="leader">
                                      <?= $form->field($model, 'nama_leader')->textInput(['readOnly'=>true,'value'=>Yii::$app->user->identity->username]) ?>
                                    </div>


                                    <?= $form->field($model, 'proses')->dropDownList(['penimbangan' => 'PENIMBANGAN','pengolahan' => 'PENGOLAHAN','kemas1' => 'KEMAS 1','kemas2' => 'KEMAS 2'],['prompt'=>'Pilih Proses']); ?>

                                    <?= $form->field($model, 'kategori')->dropDownList(['SALAH_INPUT' => 'SALAH INPUT / EDIT JADWAL','LUPA_INPUT' => 'LUPA INPUT / BELUM INPUT','OTHERS' => 'OTHERS'],['prompt'=>'Pilih Kategori']); ?>

                                    <div id="nojadwal-form">
                                      <span class="badge bg-blue" id="nomo-badge"></span>
                                      <span class="badge bg-yellow" id="snfg-badge"></span>
                                      <span class="badge bg-green" id="snfgkomponen-badge"></span>
                                      <p></p>
                                      <?= $form->field($model, 'nojadwal')->textInput() ?>  
                                      <span class="badge bg-red" id="alert-badge"></span>
                                      <span class="badge bg-green" id="valid-badge"></span>
                                      <p></p>                           
                                        
                                        <div id="log-input">

                                            <div class="box-header with-border">
                                              <h2 class="box-title"> <b>Log Input</b></h2>
                                              <br \>
                                            </div>
                                            <div class="box-header with-border">
                                                <table id="table-loginput" class="table table-bordered">
                                                    <tbody><tr>
                                                
                                                      <th style="width: 100px">Waktu</th>
                                                      <th style="width: 120px">Nomor Jadwal</th>
                                                      <th>Jenis Proses</th>
                                                      <th>Lanjutan</th>
                                                      <th style="width: 50px">Nama Operator</th>
                                                      <th style="width: 50px">Check</th>

                                                    </tr>
                                                  </tbody>
                                                </table>
                                            </div>

                                        </div>

                                    </div>

                                    <?= $form->field($model, 's_id')->textInput(['readOnly'=>true]) ?>

                                    <?= $form->field($model, 'permasalahan')->textArea() ?>





                                    <div id="form-id" class="form-group">
                                        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                    </div>


                                <!--     <?= Html::a('<i class="fa fa-chevron-circle-right"></i>Cari', ['pusat-resolusi/direct'], ['class'=>'btn btn-primary', 'style' => 'width: 150px; border-radius: 5px;']) ?>
                                 -->
                                <!--     <button id="submitButton" type="submit">Creates</button> -->


                                    <?php ActiveForm::end(); ?>
                            </div>
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>


                </div>
            </div>
            <!-- /.box-body -->
          </div>

    

</div>


<?php
$script = <<< JS


// Is In Array

function isInArray(value, array) {
  return array.indexOf(value) > -1;
}




// Hide Nomor Jadwal Form

$('#leader').hide();
$('#nojadwal-form').hide();
$('#form-id').hide();
$('#log-input').hide();

// Get Kategori Field Value

$('#pusatresolusi-kategori').change(function(){  

    $('#nojadwal-form').fadeIn("slow");

    // var kat = $('#pusatresolusi-kategori').val();
    //   if(kat=='SALAH_INPUT'){
    //     $('#pusatresolusi-nojadwal').attr('value','');
    //     $('#nojadwal-form').fadeIn("slow");
    //     $('#form-id').hide();
    //   }else{
    //     $('#nojadwal-form').hide();
    //     $('#pusatresolusi-nojadwal').attr('value','-');
    //     $('#form-id').fadeIn("slow");
    //   }
});


$('#pusatresolusi-proses').change(function(){  

    var proses = $('#pusatresolusi-proses').val();
     if(isInArray(proses, ['penimbangan','pengolahan','qc-bulk-entry'])){
        $('#nomo-badge').fadeIn("slow");
        $('#nomo-badge').html('Input/Scan Nomor MO' );
        $('#snfg-badge').hide();
        $('#snfgkomponen-badge').hide();
        document.getElementById('pusatresolusi-nojadwal').value = '';
        $('#form-id').hide();
     }else if(isInArray(proses, ['kemas2','qc-fg-v2'])){
        $('#snfg-badge').fadeIn("slow");
        $('#snfg-badge').html('Input/Scan SNFG' );
        $('#nomo-badge').hide();
        $('#snfgkomponen-badge').hide();
        document.getElementById('pusatresolusi-nojadwal').value = '';
        $('#form-id').hide();
     }else{
        $('#snfgkomponen-badge').fadeIn("slow");
        $('#snfgkomponen-badge').html('Input/Scan Nomor SNFG Komponen' );
        $('#nomo-badge').hide();
        $('#snfg-badge').hide();
        document.getElementById('pusatresolusi-nojadwal').value = '';
        $('#form-id').hide();  
     }
});


$('#pusatresolusi-nojadwal').change(function(){

    var kategori = $('#pusatresolusi-kategori').val();
    var nojadwal = $('#pusatresolusi-nojadwal').val();
    var proses = $('#pusatresolusi-proses').val();


    if(kategori=='SALAH_INPUT'){

        if(nojadwal=='-'){
          alert('flag1');
          // do nothing
        }else if(isInArray(proses, ['penimbangan','pengolahan','qc-bulk-entry'])&&(nojadwal!='-')){

            // Show Input Log


            if(proses=='penimbangan'){
                  $.get('index.php?r=flow-input-mo/get-log-input-penimbangan',{ nojadwal : nojadwal },function(data){
                    var json = $.parseJSON(data);
                    console.log(data);
                    var tr;
                    for (var i = 0; i < json.length; i++) {
                        tr = $('<tr/>');
                        tr.append("<td><span class='badge bg-grey'>" + json[i].datetime_write + "</span></td>");
                        tr.append("<td><span class='badge bg-grey'>" + json[i].nomo + "</span></td>");
                        tr.append("<td><span class='badge bg-grey'>" + json[i].jenis_penimbangan + "</span></td>");
                        tr.append("<td><span class='badge bg-grey'>" + json[i].lanjutan + "</span></td>");
                        tr.append("<td><span class='badge bg-grey'>" + json[i].nama_operator + "</span></td>");
                        tr.append("<td><span class='badge bg-grey'>" + json[i].id + "</span></td>");
                        tr.append('<td><label><input type="radio" name="radioLogInput" onclick="printVal('+json[i].id+');" value='+json[i].id+'></label></td>');
                
                        
                        $('#table-loginput').append(tr);
                    }
                    $('#log-input').fadeIn("slow");
                    $('#form-id').fadeIn("slow");
                  });
            }else if(proses=='pengolahan'){
                  $.get('index.php?r=flow-input-mo/get-log-input-pengolahan',{ nojadwal : nojadwal },function(data){
                    var json = $.parseJSON(data);
                    console.log(data);
                    var tr;
                    for (var i = 0; i < json.length; i++) {
                        tr = $('<tr/>');
                        tr.append("<td><span class='badge bg-grey'>" + json[i].datetime_write + "</span></td>");
                        tr.append("<td><span class='badge bg-grey'>" + json[i].nomo + "</span></td>");
                        tr.append("<td><span class='badge bg-grey'>" + json[i].jenis_penimbangan + "</span></td>");
                        tr.append("<td><span class='badge bg-grey'>" + json[i].lanjutan + "</span></td>");
                        tr.append("<td><span class='badge bg-grey'>" + json[i].nama_operator + "</span></td>");
                        tr.append("<td><span class='badge bg-grey'>" + json[i].id + "</span></td>");
                        tr.append('<td><label><input type="radio" name="radioLogInput" onclick="printVal('+json[i].id+');" value='+json[i].id+'></label></td>');
          
                        
                        $('#table-loginput').append(tr);
                    }
                    $('#log-input').fadeIn("slow");
                    $('#form-id').fadeIn("slow");
                  });
            }

        }else if(isInArray(proses, ['kemas2','qc-fg-v2'])&&(nojadwal!='-')){
              

                  $.get('index.php?r=flow-input-snfg/get-log-input-kemas2',{ nojadwal : nojadwal },function(data){
                    var json = $.parseJSON(data);
                    console.log(data);
                    var tr;
                    for (var i = 0; i < json.length; i++) {
                        tr = $('<tr/>');
                        tr.append("<td><span class='badge bg-grey'>" + json[i].datetime_write + "</span></td>");
                        tr.append("<td><span class='badge bg-grey'>" + json[i].snfg + "</span></td>");
                        tr.append("<td><span class='badge bg-grey'>" + json[i].jenis_kemas + "</span></td>");
                        tr.append("<td><span class='badge bg-grey'>" + json[i].lanjutan + "</span></td>");
                        tr.append("<td><span class='badge bg-grey'>" + json[i].nama_operator + "</span></td>");
                        tr.append("<td><span class='badge bg-grey'>" + json[i].id + "</span></td>");
                        tr.append('<td><label><input type="radio" name="radioLogInput" onclick="printVal('+json[i].id+');" value='+json[i].id+'></label></td>');
          
                        
                        $('#table-loginput').append(tr);
                    }
                    $('#log-input').fadeIn("slow");
                    $('#form-id').fadeIn("slow");


                  });

        }else{
              

                    $.get('index.php?r=flow-input-snfgkomponen/get-log-input-kemas1',{ nojadwal : nojadwal },function(data){
                      var json = $.parseJSON(data);
                      console.log(data);
                      var tr;
                      for (var i = 0; i < json.length; i++) {
                            tr = $('<tr/>');
                            tr.append("<td><span class='badge bg-grey'>" + json[i].datetime_write + "</span></td>");
                            tr.append("<td><span class='badge bg-grey'>" + json[i].snfg_komponen + "</span></td>");
                            tr.append("<td><span class='badge bg-grey'>" + json[i].jenis_kemas + "</span></td>");
                            tr.append("<td><span class='badge bg-grey'>" + json[i].lanjutan + "</span></td>");
                            tr.append("<td><span class='badge bg-grey'>" + json[i].nama_operator + "</span></td>");
                            tr.append("<td><span class='badge bg-grey'>" + json[i].id + "</span></td>");
                            tr.append('<td><label><input type="radio" name="radioLogInput" onclick="printVal('+json[i].id+');" value='+json[i].id+'></label></td>');
              
                            
                            $('#table-loginput').append(tr);
                      }
                      $('#log-input').fadeIn("slow");
                      $('#form-id').fadeIn("slow");


                    });
        }
    }else if(kategori=='LUPA_INPUT'){

        if(nojadwal=='-'){
          alert('flag1');
          // do nothing
        }else if(isInArray(proses, ['penimbangan','pengolahan','qc-bulk-entry'])&&(nojadwal!='-')){
          
            $.get('index.php?r=scm-planner/check-nomo',{ nomo : nojadwal },function(data){

                var json = $.parseJSON(data);
                if(json.id>0){
                  alert('Valid');
                  $('#form-id').fadeIn("slow");
                }else{
                  alert('Nomor Jadwal Invalid');
                  $('#form-id').hide();
                }

            });

        }else if(isInArray(proses, ['kemas2','qc-fg-v2'])&&(nojadwal!='-')){

            $.get('index.php?r=scm-planner/check-snfg',{ snfg : nojadwal },function(data){

                var json = $.parseJSON(data);
                if(json.id>0){
                  alert('Valid');
                  $('#form-id').fadeIn("slow");
                }else{
                  alert('Nomor Jadwal Invalid');
                  $('#form-id').hide();
                }

            });
              

        }else{


            $.get('index.php?r=scm-planner/check-snfg-komponen',{ snfg_komponen : nojadwal },function(data){

                var json = $.parseJSON(data);
                if(json.id>0){
                  alert('Valid');
                  $('#form-id').fadeIn("slow");
                }else{
                  alert('Nomor Jadwal Invalid');
                  $('#form-id').hide();
                }

            });
              

        }      

    }
});




JS;
$this->registerJs($script);
?>
