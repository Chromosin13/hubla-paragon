<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PusatResolusi */

$this->title = 'Update Pusat Resolusi: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pusat Resolusis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pusat-resolusi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
