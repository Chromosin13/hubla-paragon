<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PusatResolusiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pusat Resolusis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pusat-resolusi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pusat Resolusi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

        <?php 
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'showPageSummary'=>true,
            'pjax'=>true,
            'striped'=>true,
            'hover'=>true,
            'panel'=>['type'=>'primary', 'heading'=>'Achievement Line Penimbangan'],
            'columns'=>[

                            'id',
                            'timestamp',
                            'proses',
                            'kategori',
                            'nojadwal',
                            'nama_operator',
                            'permasalahan',
                            'status',
             ],
        ]);
    
    ?>

<!--     <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'proses',
            'nojadwal',
            'permasalahan',
            'nama_operator',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?> -->
</div>
