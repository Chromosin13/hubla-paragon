<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JenisProsesPraKemas */

$this->title = 'Update Jenis Proses Pra Kemas: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Jenis Proses Pra Kemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jenis-proses-pra-kemas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
