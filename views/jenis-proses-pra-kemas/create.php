<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\JenisProsesPraKemas */

$this->title = 'Create Jenis Proses Pra Kemas';
$this->params['breadcrumbs'][] = ['label' => 'Jenis Proses Pra Kemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-proses-pra-kemas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
