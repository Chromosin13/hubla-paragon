<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JenisProsesPraKemas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jenis-proses-pra-kemas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jenis_proses_pra_kemas')->textInput() ?>

    <?= $form->field($model, 'is_print')->textInput() ?>

    <?= $form->field($model, 'sediaan')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
