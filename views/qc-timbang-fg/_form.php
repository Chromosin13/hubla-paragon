<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use faryshta\widgets\JqueryTagsInput;
use kartik\checkbox\CheckboxX;
use app\models\QcFg;
use app\models\Kemas2;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\QcTimbangFg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qc-timbang-fg-form">

    <?php $form = ActiveForm::begin(); ?>



    <div class="box">

                <div class="box-header with-border">
            
                    <label>
                      <input type="checkbox" id="per_snfg_komponen">
                      Insert Per SNFG Komponen ?
                    </label>
                </div>


            <div class="box-header with-border">
              <h2 class="box-title">Input dan Informasi SNFG dari Planner</h2>
                <?= $form->field($model, 'snfg')->textInput() ?>

                <?= $form->field($model, 'snfg_komponen')->textInput(['disabled'=>true]) ?>

                <?= $form->field($model, 'lanjutan_split_batch')->dropDownList(
                        ArrayHelper::map(QcFg::find()->all()
                        ,'lanjutan_split_batch','lanjutan_split_batch')
                        ,['prompt'=>'Select Batch Split']

                );?>

                <?= $form->field($model, 'palet_ke')->dropDownList(
                        ArrayHelper::map(Kemas2::find()->all()
                        ,'palet_ke','palet_ke')
                        ,['prompt'=>'Select Palet']

                );?>

                <b>Streamline</b>
                <input type="text" class="form-control" id="streamline" disabled>
                <b>Start</b>
                <input type="text" class="form-control" id="start" placeholder="" disabled>
                <b>Due</b>
                <input type="text" class="form-control" id="due" placeholder="" disabled>
                <b>Nama Bulk</b>
                <input type="text" class="form-control" id="nama-bulk" placeholder="" disabled>
                <b>Nama FG</b>
                <input type="text" class="form-control" id="nama-fg" placeholder="" disabled>
                <b>No Batch</b>
                <input type="text" class="form-control" id="no-batch" placeholder="" disabled>
            </div>
            <div class="box-header with-border">
                <h2 class="box-title">Posisi Terakhir</h2><br>
                    <b>SNFG Komponen</b>
                    <input type="text" class="form-control" id="ppr-snfg_komponen" placeholder="" disabled>
                    <b>Posisi</b>
                    <input type="text" class="form-control" id="ppr-posisi" placeholder="" disabled>
                    <b>State</b>
                    <input type="text" class="form-control" id="ppr-state" placeholder="" disabled>
                    <b>Jenis Proses</b>
                    <input type="text" class="form-control" id="ppr-jenis-proses" placeholder="" disabled>
                    <b>Lanjutan</b>
                    <input type="text" class="form-control" id="ppr-lanjutan" placeholder="" disabled>
                    <b>Is Done</b>
                    <input type="text" class="form-control" id="ppr-isdone" placeholder="" disabled>
                </div>
    </div>


    <a class="btn btn-app" id="start-button">
                <i class="fa fa-play"></i> Start
    </a>

    <a class="btn btn-app" id="stop-button">
                <i class="fa fa-pause"></i> Stop
    </a>

    <a class="btn btn-app" id="istirahat-start-button">
                <i class="fa fa-play"></i> Istirahat Start
    </a>

    <a class="btn btn-app" id="istirahat-stop-button">
                <i class="fa fa-pause"></i> Istirahat Stop
    </a>

    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'jenis_periksa')->dropDownList(['JADWAL BARU' => 'JADWAL BARU','REWORK' => 'REWORK'],['prompt'=>'Select Option']); ?>


    <div class="box" id="qctimbangfg-start">
            <div class="box-header with-border">
              <h2 class="box-title">Start Entry</h2>

                
                <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>

                <!-- <?= $form->field($model, 'nama_line')->textInput() ?> -->

                <?= $form->field($model, 'jumlah_inspektor')->textInput() ?>

                <?php echo $form->field($model, 'nama_inspektor')->widget(JqueryTagsInput::className([]));
                ?>
                
            </div>
    </div>

    <div class="box" id="qctimbangfg-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Stop Entry</h2>
                <?= $form->field($model, 'lanjutan')->textInput(['id'=>'qctimbangfg-lanjutan_stop','readonly' => 'true']) ?> 

                <?= $form->field($model, 'qty')->textInput() ?>

                <?= $form->field($model, 'status')->dropDownList(['RELEASE' => 'RELEASE','REWORK' => 'REWORK','RECOUNT' => 'RECOUNT','CEK_MIKRO' => 'CEK_MIKRO','LANJUTAN'=>'LANJUTAN'],['prompt'=>'Select Option']); ?>

                <div id="check_box_is_done">
                <?php
                    echo '<label class="cbx-label" for="is_done">Is Done?</label>';
                    echo CheckboxX::widget([
                        'model' => $model,
                        'attribute' => 'is_done',
                        'pluginOptions' => [
                            'threeState' => false,
                            'size' => 'lg'
                        ]
                    ]); 
                ?>
                </div>
            </div>
    </div>

    <div class="box" id="qctimbangfg-istirahat-start">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Start Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'qctimbangfg-lanjutan_istirahat_start','readonly' => 'true']) ?> 
                 <?= $form->field($model, 'lanjutan_ist')->textInput(['id'=>'qc-timbang-fg-lanjutan-ist_istirahat_start','readonly' => 'true']) ?>
            </div>
    </div>

    <div class="box" id="qctimbangfg-istirahat-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Istirahat Stop Entry</h2>
                 <?= $form->field($model, 'lanjutan')->textInput(['id'=>'qctimbangfg-lanjutan_istirahat_stop','readonly' => 'true']) ?>
                 <?= $form->field($model, 'lanjutan_ist')->textInput(['id'=>'qc-timbang-fg-lanjutan-ist_istirahat_stop','readonly' => 'true']) ?>
            </div>
    </div>    


    <div class="form-group" id="create-button">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
$('#per_snfg_komponen').change(function(){
    var per_snfg_komponen = document.getElementById("qctimbangfg-snfg_komponen").disabled;
    if(per_snfg_komponen){
        document.getElementById("qctimbangfg-snfg_komponen").disabled = false
        document.getElementById("qctimbangfg-snfg").disabled = true;
    }else{
        document.getElementById("qctimbangfg-snfg_komponen").disabled = true
        document.getElementById("qctimbangfg-snfg").disabled = false;
    } 
});


$('#qctimbangfg-snfg').change(function(){
    var snfg = $(this).val();

    $.post("index.php?r=qc-timbang-fg/palet-assign-snfg&snfg="+$(this).val(), function (data){
            $("select#qctimbangfg-palet_ke").html(data);
            $('#qctimbangfg-palet_ke').change(function(){
                var palet_ke = $("#qctimbangfg-palet_ke").val();
                $.get('index.php?r=scm-planner/get-last-palet-snfg',{ snfg : snfg, palet_ke : palet_ke },function(data){
                            var data = $.parseJSON(data);
                            if(data.posisi=="QC TIMBANG FG" && data.state=="START"){
                                $('#start-button').hide();
                                $('#stop-button').show(); 
                                $('#istirahat-start-button').show(); 
                                $('#istirahat-stop-button').hide(); 
                            }else if(data.posisi=="QC TIMBANG FG" && data.state=="ISTIRAHAT START"){
                                $('#start-button').hide();
                                $('#stop-button').hide(); 
                                $('#istirahat-start-button').hide(); 
                                $('#istirahat-stop-button').show(); 
                            }else if(data.posisi=="QC TIMBANG FG" && data.state=="ISTIRAHAT STOP"){
                                $('#start-button').hide();
                                $('#stop-button').show(); 
                                $('#istirahat-start-button').show(); 
                                $('#istirahat-stop-button').hide(); 
                            }else if(data.posisi=="QC TIMBANG FG" && data.state=="STOP"){
                                $('#start-button').show();
                                $('#stop-button').hide(); 
                                $('#istirahat-start-button').hide(); 
                                $('#istirahat-stop-button').hide(); 
                            }else {
                                $('#start-button').show();
                                $('#stop-button').show(); 
                                $('#istirahat-start-button').show(); 
                                $('#istirahat-stop-button').show();
                                alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                            }
                });

            });
    // tambahkan fungsi biar restriksi ssis tergantung dari pilihan palet

    });

    $.post("index.php?r=qc-timbang-fg/batch-split-assign-snfg&snfg="+$(this).val(), function (data){
            $("select#qctimbangfg-lanjutan_split_batch").html(data);
            $('#qctimbangfg-lanjutan_split_batch').change(function(){
                var lanjutan_split_batch = $("#qctimbangfg-lanjutan_split_batch").val();
                    $.get('index.php?r=scm-planner/get-last-induk-snfg',{ snfg : snfg, lanjutan_split_batch : lanjutan_split_batch },function(data){
                            var data = $.parseJSON(data);
                            if(data.posisi=="QC TIMBANG FG" && data.state=="START"){
                                $('#start-button').hide();
                                $('#stop-button').show(); 
                                $('#istirahat-start-button').show(); 
                                $('#istirahat-stop-button').hide(); 
                            }else if(data.posisi=="QC TIMBANG FG" && data.state=="ISTIRAHAT START"){
                                $('#start-button').hide();
                                $('#stop-button').hide(); 
                                $('#istirahat-start-button').hide(); 
                                $('#istirahat-stop-button').show(); 
                            }else if(data.posisi=="QC TIMBANG FG" && data.state=="ISTIRAHAT STOP"){
                                $('#start-button').hide();
                                $('#stop-button').show(); 
                                $('#istirahat-start-button').show(); 
                                $('#istirahat-stop-button').hide(); 
                            }else if(data.posisi=="QC TIMBANG FG" && data.state=="STOP"){
                                $('#start-button').show();
                                $('#stop-button').hide(); 
                                $('#istirahat-start-button').hide(); 
                                $('#istirahat-stop-button').hide(); 
                            }else {
                                $('#start-button').show();
                                $('#stop-button').show(); 
                                $('#istirahat-start-button').show(); 
                                $('#istirahat-stop-button').show();
                                alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                            }
                    });
                });
    });

    $.get('index.php?r=qc-timbang-fg/check-batch-split-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        if (data == null){
           alert('Komponen Tidak terdapat di Proses QC FG');
           document.getElementById("qctimbangfg-lanjutan_split_batch").disabled = true; 
        } else if(data.last_status==1){
            alert('masuk 1')
            alert('Terjadi Batch Split pada QC FG, Mohon Pilih Nomor Lanjutan Batch Split')
            $('#qctimbangfg-lanjutan_split_batch').show();
        } else if(data.last_status==0){
            alert('Proses pada QC FG belum selesai')
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
        } else {
            alert('else');
            document.getElementById("qctimbangfg-lanjutan_split_batch").disabled = true;
            $('#qctimbangfg-lanjutan_split_batch').hide();
        }
    });

    $.get('index.php?r=scm-planner/get-planner-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);    
        $('#qctimbangfg-snfg_komponen').attr('value',data.snfg_komponen);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
    });
    $.get('index.php?r=pengolahan/get-pengolahan',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data.sediaan);
        $('#no-batch').attr('value',data.nobatch);
    });
    $.get('index.php?r=scm-planner/get-pprr',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        //alert(data);
        //alert(data);
        $('#ppr-snfg_komponen').attr('value',data.snfg_komponen);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });
    $.get('index.php?r=scm-planner/get-planner-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Terdapat setidaknya 1 Komponen yang ditetapkan sebagai HOLD/PAUSE')
        }
        else{
                $.get('index.php?r=scm-planner/get-last-induk',{ snfg : snfg },function(data){
                    var data = $.parseJSON(data);
                    if(data.posisi=="QC TIMBANG FG" && data.state=="START"){
                        $('#start-button').hide();
                        $('#stop-button').show(); 
                        $('#istirahat-start-button').show(); 
                        $('#istirahat-stop-button').hide(); 
                    }else if(data.posisi=="QC TIMBANG FG" && data.state=="ISTIRAHAT START"){
                        $('#start-button').hide();
                        $('#stop-button').hide(); 
                        $('#istirahat-start-button').hide(); 
                        $('#istirahat-stop-button').show(); 
                    }else if(data.posisi=="QC TIMBANG FG" && data.state=="ISTIRAHAT STOP"){
                        $('#start-button').hide();
                        $('#stop-button').show(); 
                        $('#istirahat-start-button').show(); 
                        $('#istirahat-stop-button').hide(); 
                    }else if(data.posisi=="QC TIMBANG FG" && data.state=="STOP"){
                        $('#start-button').show();
                        $('#stop-button').hide(); 
                        $('#istirahat-start-button').hide(); 
                        $('#istirahat-stop-button').hide(); 
                    }else {
                        $('#start-button').show();
                        $('#stop-button').show(); 
                        $('#istirahat-start-button').show(); 
                        $('#istirahat-stop-button').show();
                        alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                    }
                });
        }
    });
});

$('#qctimbangfg-snfg_komponen').change(function(){
    var snfg_komponen = $(this).val();

    $.post("index.php?r=qc-timbang-fg/palet-assign-komponen&snfg_komponen="+$(this).val(), function (data){
            $("select#qctimbangfg-palet_ke").html(data);
            $('#qctimbangfg-palet_ke').change(function(){
                var palet_ke = $("#qctimbangfg-palet_ke").val();
                $.get('index.php?r=scm-planner/get-last-palet-komponen',{ snfg_komponen : snfg_komponen, palet_ke : palet_ke },function(data){
                            var data = $.parseJSON(data);
                            if(data.posisi=="QC TIMBANG FG" && data.state=="START"){
                                $('#start-button').hide();
                                $('#stop-button').show(); 
                                $('#istirahat-start-button').show(); 
                                $('#istirahat-stop-button').hide(); 
                            }else if(data.posisi=="QC TIMBANG FG" && data.state=="ISTIRAHAT START"){
                                $('#start-button').hide();
                                $('#stop-button').hide(); 
                                $('#istirahat-start-button').hide(); 
                                $('#istirahat-stop-button').show(); 
                            }else if(data.posisi=="QC TIMBANG FG" && data.state=="ISTIRAHAT STOP"){
                                $('#start-button').hide();
                                $('#stop-button').show(); 
                                $('#istirahat-start-button').show(); 
                                $('#istirahat-stop-button').hide(); 
                            }else if(data.posisi=="QC TIMBANG FG" && data.state=="STOP"){
                                $('#start-button').show();
                                $('#stop-button').hide(); 
                                $('#istirahat-start-button').hide(); 
                                $('#istirahat-stop-button').hide(); 
                            }else {
                                $('#start-button').show();
                                $('#stop-button').show(); 
                                $('#istirahat-start-button').show(); 
                                $('#istirahat-stop-button').show();
                                alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                            }
                });

            });

    });

    $.post("index.php?r=qc-timbang-fg/batch-split-assign-komponen&snfg_komponen="+$(this).val(), function (data){
            $("select#qctimbangfg-lanjutan_split_batch").html(data);
            $('#qctimbangfg-lanjutan_split_batch').change(function(){
                var lanjutan_split_batch = $("#qctimbangfg-lanjutan_split_batch").val();
                    $.get('index.php?r=scm-planner/get-last-induk-komponen',{ snfg_komponen : snfg_komponen, lanjutan_split_batch : lanjutan_split_batch },function(data){
                            var data = $.parseJSON(data);
                            if(data.posisi=="QC TIMBANG FG" && data.state=="START"){
                                $('#start-button').hide();
                                $('#stop-button').show(); 
                                $('#istirahat-start-button').show(); 
                                $('#istirahat-stop-button').hide(); 
                            }else if(data.posisi=="QC TIMBANG FG" && data.state=="ISTIRAHAT START"){
                                $('#start-button').hide();
                                $('#stop-button').hide(); 
                                $('#istirahat-start-button').hide(); 
                                $('#istirahat-stop-button').show(); 
                            }else if(data.posisi=="QC TIMBANG FG" && data.state=="ISTIRAHAT STOP"){
                                $('#start-button').hide();
                                $('#stop-button').show(); 
                                $('#istirahat-start-button').show(); 
                                $('#istirahat-stop-button').hide(); 
                            }else if(data.posisi=="QC TIMBANG FG" && data.state=="STOP"){
                                $('#start-button').show();
                                $('#stop-button').hide(); 
                                $('#istirahat-start-button').hide(); 
                                $('#istirahat-stop-button').hide(); 
                            }else {
                                $('#start-button').show();
                                $('#stop-button').show(); 
                                $('#istirahat-start-button').show(); 
                                $('#istirahat-stop-button').show();
                                alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                            }
                    });
                });
    });
    $.get('index.php?r=qc-timbang-fg/check-batch-split-komponen',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        if (data == null){
           alert('Komponen Tidak terdapat di Proses QC FG');
           document.getElementById("qctimbangfg-lanjutan_split_batch").disabled = true; 
        } else if(data.last_status==1){
            alert('Terjadi Batch Split pada QC FG, Mohon Pilih Nomor Lanjutan Batch Split')
            $('#qctimbangfg-lanjutan_split_batch').show();
        } else if(data.last_status==0){
            alert('Proses pada QC FG belum selesai');
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
        }  else {
            document.getElementById("qctimbangfg-lanjutan_split_batch").disabled = true;
            $('#qctimbangfg-lanjutan_split_batch').hide();
        }
    });

    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        $('#qctimbangfg-snfg').attr('value',data.snfg);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
        $('#jumlah').attr('value',data.jumlah);
    });
    $.get('index.php?r=scm-planner/get-ppr',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        $('#ppr-snfg_komponen').attr('value',data.snfg_komponen);
        $('#ppr-posisi').attr('value',data.posisi);
        $('#ppr-state').attr('value',data.state);
        $('#ppr-jenis-proses').attr('value',data.jenis_proses);
        $('#ppr-lanjutan').attr('value',data.lanjutan);
        $('#ppr-isdone').attr('value',data.is_done);
    });
    $.get('index.php?r=scm-planner/get-planner',{ snfg_komponen : snfg_komponen },function(data){
        var data = $.parseJSON(data);
        if(data.status.match(/\bHOLD\b/i) || data.status.match(/\bPAUSE\b/i)){
            $('#start-button').hide();
            $('#stop-button').hide(); 
            $('#istirahat-start-button').hide(); 
            $('#istirahat-stop-button').hide();
            $('#create-button').hide();
            alert('Terdapat SNFG Komponen yang masih HOLD/PAUSE dari Planner')
        }
        else{
                $.get('index.php?r=scm-planner/get-last',{ snfg_komponen : snfg_komponen },function(data){
                    var data = $.parseJSON(data);
                    if(data.posisi=="QC TIMBANG FG" && data.state=="START"){
                        $('#start-button').hide();
                        $('#stop-button').show(); 
                        $('#istirahat-start-button').show(); 
                        $('#istirahat-stop-button').hide(); 
                    }else if(data.posisi=="QC TIMBANG FG" && data.state=="ISTIRAHAT START"){
                        $('#start-button').hide();
                        $('#stop-button').hide(); 
                        $('#istirahat-start-button').hide(); 
                        $('#istirahat-stop-button').show(); 
                    }else if(data.posisi=="QC TIMBANG FG" && data.state=="ISTIRAHAT STOP"){
                        $('#start-button').hide();
                        $('#stop-button').show(); 
                        $('#istirahat-start-button').show(); 
                        $('#istirahat-stop-button').hide(); 
                    }else if(data.posisi=="QC TIMBANG FG" && data.state=="STOP"){
                        $('#start-button').show();
                        $('#stop-button').hide(); 
                        $('#istirahat-start-button').hide(); 
                        $('#istirahat-stop-button').hide(); 
                    }else {
                        $('#start-button').show();
                        $('#stop-button').show(); 
                        $('#istirahat-start-button').show(); 
                        $('#istirahat-stop-button').show();
                        alert('Posisi Terakhir Komponen berada di Proses ' + data.posisi + ' dengan kondisi ' + data.state);
                    }
                });
        }
    });
});


$(function() {
    $('#qctimbangfg-lanjutan_split_batch').hide();
    $('#qctimbangfg-start').hide();
    $('#qctimbangfg-stop').hide(); 
    $('#qctimbangfg-istirahat-start').hide(); 
    $('#qctimbangfg-istirahat-stop').hide(); 
    $('#start-button').click(function(){
            var start = "START";
            $('#qctimbangfg-state').attr('value',start);
                    $('#qctimbangfg-start').show();
                    $('#qctimbangfg-stop').hide(); 
                    $('#qctimbangfg-istirahat-start').hide(); 
                    $('#qctimbangfg-istirahat-stop').hide();
            $('#qctimbangfg-jenis_periksa').change(function(){
                var jenis_periksa = $('#qctimbangfg-jenis_periksa').val();
                var snfg = $('#qctimbangfg-snfg').val();
                var snfg_komponen = $('#qctimbangfg-snfg_komponen').val();
                var per_snfg_komponen = document.getElementById("qctimbangfg-snfg_komponen").disabled;
                if(per_snfg_komponen){    
                    $.get('index.php?r=qc-timbang-fg/lanjutan-qc-timbang-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#qctimbangfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                        });                                        
                }else{
                    $.get('index.php?r=qc-timbang-fg/lanjutan-qc-timbang-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#qctimbangfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                        });
                } 
            });                               
        });

    $('#istirahat-start-button').click(function(){
            var ist_start = "ISTIRAHAT START";
            $('#qctimbangfg-state').attr('value',ist_start);
                    $('#qctimbangfg-istirahat-start').show(); 
                    $('#qctimbangfg-stop').hide(); 
                    $('#qctimbangfg-start').hide(); 
                    $('#qctimbangfg-istirahat-stop').hide(); 
            $('#qctimbangfg-jenis_periksa').change(function(){
                var jenis_periksa = $('#qctimbangfg-jenis_periksa').val();
                var snfg = $('#qctimbangfg-snfg').val();
                var snfg_komponen = $('#qctimbangfg-snfg_komponen').val();
                var per_snfg_komponen = document.getElementById("qctimbangfg-snfg_komponen").disabled;
                if(per_snfg_komponen){ 
                    alert('NAMBAH LANJUTAN PER SNFG')   
                    $.get('index.php?r=qc-timbang-fg/lanjutan-qc-timbang-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#qctimbangfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                            var lanjutan = $('#qctimbangfg-lanjutan_istirahat_start').val();
                                            $.get('index.php?r=qc-timbang-fg/lanjutan-ist-qc-timbang-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa, lanjutan : lanjutan },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.lanjutan_ist);
                                                    $('#qc-timbang-fg-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                    $('#qc-timbang-fg-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                            });
                                        });                                        
                }else{
                    alert('NAMBAH LANJUTAN PER SNFG KOMPONEN') 
                    $.get('index.php?r=qc-timbang-fg/lanjutan-qc-timbang-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#qctimbangfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                            var lanjutan = $('#qctimbangfg-lanjutan_istirahat_start').val();
                                            $.get('index.php?r=qc-timbang-fg/lanjutan-ist-qc-timbang-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa, lanjutan : lanjutan },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.lanjutan_ist);
                                                    $('#qc-timbang-fg-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                    $('#qc-timbang-fg-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                            }); 
                                        });
                } 
            });                                      
        });

    $('#istirahat-stop-button').click(function(){
            var ist_stop = "ISTIRAHAT STOP";
            $('#qctimbangfg-state').attr('value',ist_stop);
                    $('#qctimbangfg-istirahat-stop').show();
                    $('#qctimbangfg-stop').hide(); 
                    $('#qctimbangfg-istirahat-start').hide(); 
                    $('#qctimbangfg-start').hide(); 
            $('#qctimbangfg-jenis_periksa').change(function(){
                var jenis_periksa = $('#qctimbangfg-jenis_periksa').val();
                var snfg = $('#qctimbangfg-snfg').val();
                var snfg_komponen = $('#qctimbangfg-snfg_komponen').val();
                var per_snfg_komponen = document.getElementById("qctimbangfg-snfg_komponen").disabled;
                if(per_snfg_komponen){    
                    $.get('index.php?r=qc-timbang-fg/lanjutan-qc-timbang-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#qctimbangfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                            var lanjutan = $('#qctimbangfg-lanjutan_istirahat_start').val();
                                            $.get('index.php?r=qc-timbang-fg/lanjutan-ist-qc-timbang-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa, lanjutan : lanjutan },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.lanjutan_ist);
                                                    $('#qc-timbang-fg-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                    $('#qc-timbang-fg-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                            });
                                        });                                        
                }else{
                    $.get('index.php?r=qc-timbang-fg/lanjutan-qc-timbang-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#qctimbangfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                            var lanjutan = $('#qctimbangfg-lanjutan_istirahat_start').val();
                                            $.get('index.php?r=qc-timbang-fg/lanjutan-ist-qc-timbang-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa, lanjutan : lanjutan },function(data){
                                                    var data = $.parseJSON(data);
                                                    //alert(data.lanjutan_ist);
                                                    $('#qc-timbang-fg-lanjutan-ist_istirahat_start').attr('value',data.lanjutan_ist);
                                                    $('#qc-timbang-fg-lanjutan-ist_istirahat_stop').attr('value',data.lanjutan_ist);
                                            }); 
                                        });
                } 
            });                         
        });

    $('#stop-button').click(function(){
             var stop = "STOP";
             $('#qctimbangfg-state').attr('value',stop);
                    $('#qctimbangfg-stop').show(); 
                    $('#qctimbangfg-start').hide(); 
                    $('#qctimbangfg-istirahat-start').hide(); 
                    $('#qctimbangfg-istirahat-stop').hide();    
            
            $('#qctimbangfg-jenis_periksa').change(function(){
                var jenis_periksa = $('#qctimbangfg-jenis_periksa').val();
                var snfg = $('#qctimbangfg-snfg').val();
                var snfg_komponen = $('#qctimbangfg-snfg_komponen').val();
                var per_snfg_komponen = document.getElementById("qctimbangfg-snfg_komponen").disabled;
                if(per_snfg_komponen){    
                    $.get('index.php?r=qc-timbang-fg/lanjutan-qc-timbang-fgsnfg',{ snfg : snfg, jenis_periksa : jenis_periksa },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#qctimbangfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                        });                                        
                }else{
                    $.get('index.php?r=qc-timbang-fg/lanjutan-qc-timbang-fg',{ snfg_komponen : snfg_komponen, jenis_periksa : jenis_periksa },function(data){
                                            var data = $.parseJSON(data);
                                            //alert(data.sediaan);
                                            $('#qctimbangfg-lanjutan_istirahat_start').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan_stop').attr('value',data.lanjutan);
                                            $('#qctimbangfg-lanjutan_istirahat_stop').attr('value',data.lanjutan);
                                        });
                } 
            }); 
            $('#check_box_is_done').hide();
            $('#qctimbangfg-qty').hide();
                $('#qctimbangfg-status').change(function(){
                    var status = $('#qctimbangfg-status').val();
                    if (status=='RELEASE'){
                        $('#check_box_is_done').show();
                        $('#qctimbangfg-qty').show();     
                    }else{
                        $('#check_box_is_done').hide();
                        $('#qctimbangfg-qty').hide();
                    }

                }); 

    });
});

JS;
$this->registerJs($script);
?>