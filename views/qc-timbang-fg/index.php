<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\QcTimbangFgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'QC Timbang Finish Good';
$this->params['breadcrumbs'][] = $this->title;
?>

    <p>
        <?= Html::a('Input QC FG Data', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


<div class="qc-timbang-fg-index" style="overflow-x:auto;height:1000px;">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'snfg',
            'palet_ke',
            'tanggal',
            'nama_inspektor',
            'waktu',
            'state',
            'qty',
            'status',
            'jumlah_inspektor',
            'nama_line',
            'jenis_periksa',
            'lanjutan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
