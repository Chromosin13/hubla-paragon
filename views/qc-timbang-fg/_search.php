<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\QcTimbangFgSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qc-timbang-fg-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'snfg') ?>

    <?= $form->field($model, 'palet_ke') ?>

    <?= $form->field($model, 'tanggal') ?>

    <?= $form->field($model, 'nama_inspektor') ?>

    <?php echo $form->field($model, 'waktu') ?>

    <?php echo $form->field($model, 'state') ?>

    <?php echo $form->field($model, 'qty') ?>

    <?php echo $form->field($model, 'status') ?>

    <?php echo $form->field($model, 'periksa') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
