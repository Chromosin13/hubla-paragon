<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use faryshta\widgets\JqueryTagsInput;

/* @var $this yii\web\View */
/* @var $model app\models\QcTimbangFg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qc-timbang-fg-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box">
            <div class="box-header with-border">
                <a class="btn btn-app" id="view-scm">
                <i class="fa fa-play"></i> View SCM Data
                </a>
                <?= $form->field($model, 'snfg')->textInput() ?>
                <b>Streamline</b>
                <input type="text" class="form-control" id="streamline" disabled>
                <b>Start</b>
                <input type="text" class="form-control" id="start" placeholder="" disabled>
                <b>Due</b>
                <input type="text" class="form-control" id="due" placeholder="" disabled>
                <b>Nama Bulk</b>
                <input type="text" class="form-control" id="nama-bulk" placeholder="" disabled>
                <b>Nama FG</b>
                <input type="text" class="form-control" id="nama-fg" placeholder="" disabled>
                <b>No Batch</b>
                <input type="text" class="form-control" id="no-batch" placeholder="" disabled>
            </div>
    </div>

    <?= $form->field($model, 'state')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'jenis_periksa')->textInput(['readonly' => 'true']); ?>
                
    <?= $form->field($model, 'lanjutan')->textInput(['readonly' => 'true']) ?>


    <div class="box" id="qctimbangfg-start">
            <div class="box-header with-border">
              <h2 class="box-title">Start Entry</h2>


                <?= $form->field($model, 'palet_ke')->textInput() ?>

                <!-- <?= $form->field($model, 'nama_line')->textInput() ?> -->

                <?= $form->field($model, 'jumlah_inspektor')->textInput() ?>

                <?php echo $form->field($model, 'nama_inspektor')->widget(JqueryTagsInput::className([]));
                ?>
                
            </div>
    </div>
                
    <div class="box" id="qctimbangfg-stop">
            <div class="box-header with-border">
              <h2 class="box-title">Stop Entry</h2>

                 <?= $form->field($model, 'qty')->textInput() ?>

                <?= $form->field($model, 'status')->dropDownList(['RELEASE' => 'RELEASE','REWORK' => 'REWORK','RECOUNT' => 'RECOUNT','CEK_MIKRO' => 'CEK_MIKRO'],['prompt'=>'Select Option']); ?>
            </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

$(function() {
    var state = document.getElementById("qctimbangfg-state").value;
     if(state=="START"){
         $('#qctimbangfg-stop').hide();
     }else{
         $('#qctimbangfg-start').hide();
     }
});


$('#view-scm').click(function(){
    var snfg = $('#qctimbangfg-snfg').val();
    $.get('index.php?r=scm-planner/get-snfg',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        $('#streamline').attr('value',data.streamline);
        $('#start').attr('value',data.start);
        $('#due').attr('value',data.due);
        $('#nama-bulk').attr('value',data.nama_bulk);
        $('#nama-fg').attr('value',data.nama_fg);
    });
    $.get('index.php?r=pengolahan/get-pengolahan',{ snfg : snfg },function(data){
        var data = $.parseJSON(data);
        $('#no-batch').attr('value',data.nobatch);
    });
});

JS;
$this->registerJs($script);
?>