<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MonitoringLineKemas */

$this->title = 'Update Monitoring Line Kemas: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Monitoring Line Kemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="monitoring-line-kemas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
