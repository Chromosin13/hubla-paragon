<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MonitoringLineKemas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="monitoring-line-kemas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sediaan')->textInput() ?>

    <?= $form->field($model, 'nama_line')->textInput() ?>

    <?= $form->field($model, 'nama_fg')->textInput() ?>

    <?= $form->field($model, 'nojadwal')->textInput() ?>

    <?= $form->field($model, 'datetime_write')->textInput() ?>

    <?= $form->field($model, 'jenis_proses')->textInput() ?>

    <?= $form->field($model, 'status')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
