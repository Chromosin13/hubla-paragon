<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MonitoringLineKemas */

$this->title = 'Create Monitoring Line Kemas';
$this->params['breadcrumbs'][] = ['label' => 'Monitoring Line Kemas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monitoring-line-kemas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
