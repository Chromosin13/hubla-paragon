<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MonitoringLineKemasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="monitoring-line-kemas-index">

    <div class="callout callout-success">
      <h4>Monitoring Line Kemas</h4>

      <p> Refresh Setiap 3 Menit </p>
      <p> Jenis Proses PACKING diabaikan pada dashboard ini</p>

    </div>


    <?php

        $gridColumns = [
            'sediaan',
            'nama_line',
            'nama_fg',
            'nojadwal',
            'datetime_write',
            'jenis_proses',
            'status:ntext',
        ];

        // Renders a export dropdown menu
        echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns
        ]);
    ?>

    <p></p>

    <div class="panel box box-primary">
        <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false">
                Filter Field
              </a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="box-body">
               <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div> 
    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'floatHeader'=>'true',
        'floatHeaderOptions' => ['position' => 'absolute',],
        'showPageSummary'=>true,
        'pjax'=>true,
        'striped'=>true,
         'condensed'=>true, 
        'hover'=>true,
        'panel'=>['type'=>'default', 'heading'=>'Monitoring Line Kemas', 'beforeOptions'=>['class'=>'grid_panel_remove'],],
        'toolbar'=> false,
        'rowOptions'   => function ($model, $key, $index, $grid) {
            if($model->status=='In Process'){
                return [
                            'class' => 'success',                            
                    ];
            }else if($model->status=='OFF'){
                return [
                            'class' => 'warning',                            
                    ];
            }else{
                return [
                            'class' => 'default',                            
                    ];
            }
        },
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'sediaan',
            'nama_line',
            'nama_fg',
            'nojadwal',
            'datetime_write',
            'jenis_proses',
            'status:ntext',
            'current_jadwal_id',
            // 'id',

            // ['class' => 'yii\grid\ActionColumn'],
            [
              'label' =>'Realtime Track', 

              'value' => function ($model) 

              { 

              return Html::a('<span class="glyphicon glyphicon-zoom-in"></span>', ['flow-input-snfg/weigher-fg-preview', 'id' => $model->current_jadwal_id]);

              },

              'format'=>'raw', 

            ],
        ],
    ]); ?>
</div>
