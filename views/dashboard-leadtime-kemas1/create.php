<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DashboardLeadtimeKemas1 */

$this->title = 'Create Dashboard Leadtime Kemas1';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Leadtime Kemas1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dashboard-leadtime-kemas1-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
