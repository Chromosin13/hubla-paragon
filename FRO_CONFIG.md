### Flowreport / MES Documentation

Prerequisites

- [ ] Install PostgreSQL PHP Driver
- [ ] PHP version must be 7.0


#### Install PHP Version 7.0 (Or Downgrade to 7.0 from Higher Version)

```
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install php7.0-cli php7.0-common libapache2-mod-php7.0 php7.0 php7.0-mysql php7.0-fpm php7.0-curl php7.0-gd php7.0-bz2
```

And Execute this script (to switch from higher version to 7.0)

```
update-alternatives --set php /usr/bin/php7.0
```

#### Install PostgreSQL PHP Driver
 
 ```
 sudo apt-get install php7.0-pgsql
 ```

Don't forget to restart Apache
 
 ```
 sudo service apache2 restart
 ```
 
#### Gitlab Repo
 
 [Flowreport Repo](https://gitlab.com/redha.hari.w/flowreport)
 
 
#### How to Pull 

 ```
 git pull origin master
 ```
 
#### How to Push 
 
 
```
git remote add origin https://gitlab.com/redha.hari.w/flowreport.git

git add .

git commit -m "komentar bebas mengenai commit"

git push -u origin master

```
