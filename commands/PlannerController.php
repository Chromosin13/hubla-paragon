<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
 
namespace app\commands;
 
use yii\console\Controller;
 
/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PlannerController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
    }
    
    public function actionAutoUpdateNamaFg()
    {
        $sql = "select id,sediaan, koitem_fg,nama_fg,odoo_code from scm_planner where (odoo_sync is null or odoo_sync = 0) and timestamp > ('now'::text::date - '2 weeks'::interval) ";
        $jadwals = ScmPlanner::findBySql($sql)->all();

        

        foreach ($jadwals as $jadwal)
        {
            if ($jadwal['sediaan']=='V'){
                $connection2 = Yii::$app->db_varcos;
                $command2 = $connection2->createCommand("

                    SELECT name,default_code FROM product_template
                      WHERE old_koitem=:koitem_fg; 
                    ",
                    [':koitem_fg'=> $jadwal['koitem_fg']]);

                $result2 = $command2->queryOne();

                $nama_fg = $result2['name'];
                $odoo_code = $result2['default_code'];
            }else{
                $connection2 = Yii::$app->db2;
                $command2 = $connection2->createCommand("

                    SELECT name,description FROM product_template
                      WHERE default_code=:koitem_fg; 
                    ",
                    [':koitem_fg'=> $jadwal['koitem_fg']]);

                $result2 = $command2->queryOne();

                $nama_fg = $result2['name'];
                $odoo_code = $result2['description'];
            }
           
            if (!empty($nama_fg)){
                if (empty($jadwal['odoo_code'])||$jadwal['odoo_code']==""){

                    $connection = Yii::$app->db;
                    $command = $connection->createCommand("

                        UPDATE scm_planner
                        SET
                          odoo_code=:odoo_code,
                          nama_fg=:nama_fg,
                          odoo_sync=:odoo_sync
                        WHERE
                          id=:id;
                        ",
                        [':odoo_code'=> $odoo_code,
                          ':odoo_sync'=> 1,
                          ':nama_fg'=> $nama_fg,
                          ':id'=>$jadwal['id']
                        ]);

                    $result = $command->queryAll();
                }else{
                    $connection = Yii::$app->db;
                    $command = $connection->createCommand("

                        UPDATE scm_planner
                        SET
                          nama_fg=:nama_fg,
                          odoo_sync=:odoo_sync
                        WHERE
                          id=:id;
                        ",
                        [
                          ':nama_fg'=> $nama_fg,
                          ':odoo_sync'=> 1,
                          ':id'=>$jadwal['id']
                        ]);

                    $result = $command->queryAll();
                }
            }
        }
        
    }
}


